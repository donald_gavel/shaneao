/*************************************************************************
 Module:        sharcs_sidecar.c

 Author:        Jason Weiss

 Date:          2003/07/09

 Description:   This module contains routines that handle Leach II I/O 
*************************************************************************/

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <syslog.h> 
#include <errno.h>
#include <fcntl.h>
#include <time.h>
#include <math.h>    /* log10, abs, floor */
#include <unistd.h>  /* usleep() */
#include <inttypes.h> /* int64_t */

#include "ktl.h"
#include "ktl_keyword.h"

#ifdef MR_SERVER
    #include "mr_server/mr_server.h"
#else
    #include "errlog.h"
#endif

#include "sharcs.h"
#include "sharcs_sidecar.h"
#include "SidecarIceClient.h"

/*
 *  Global variables
 */ 
extern int  Simulate;  

const char *getSidecarErrorMsg(int err) {
  if (err == ERROR_SIDECAR_ICE_TIMEOUT) {
    return "Timeout in communicating with SIDECAR ICE Server.";
  } else if (err == ERROR_SIDECAR_ICE_GENERIC) {
    return "Generic SIDECAR ICE Server error.";
  } else {
    return "Unknown SIDECAR ICE Server error.";
  }
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_open_driver

 * Description: opens connection to sidecar system

 * Inputs: 

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_open_driver(const char* host, unsigned int port) {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Connecting to server on host %s, port %d.", __FILE__, __LINE__, host, port);
  if ((err = sidecar_connectToServer(host, port)) < 0) {
    
    errlog(SERROR, "%s %d: Error connecting to server.", __FILE__, __LINE__); 
    if (err == -1) {
      errlog(SERROR, "%s %d: connectToServer error starting callback thread.", __FILE__, __LINE__);
    } else if (err == -2) {
      errlog(SERROR, "%s %d: connectToServer Ice exception.", __FILE__, __LINE__);
    } else if (err == -3) {
      errlog(SERROR, "%s %d: connectToServer general error.", __FILE__, __LINE__);
    } else {
      errlog(SERROR, "%s %d: connectToServer unknown error.", __FILE__, __LINE__);
    }
    return SIDECAR_ERROR_CONNECT_TO_SERVER;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Connected to server on host %s, port %d.", __FILE__, __LINE__, host, port);

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return 0;
}


void sharcs_sidecar_addCallbackListener(struct ValueListener *l) {
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  sidecar_addCallbackListener(l);
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
}

void sharcs_sidecar_server_shutdown() {
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  sidecar_server_shutdown();
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
}

int sharcs_sidecar_validate_filename(const char *fullfile) {
  int err;
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  if ((err = sidecar_validateFilename(fullfile)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_ICE_ERROR;
    }
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return err;
}

int sharcs_sidecar_get_ignored_reads() {
  int err;
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  if ((err = sidecar_broadcastValue(SIDECAR_CALLBACK_IGNORED_READS)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_ICE_ERROR;
    }
    /* err will be -1 on other failure */
    err = SIDECAR_ERROR_CANNOT_BROADCAST;
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return err;
}

int sharcs_sidecar_get_heartbeat_period() {
  int err;
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  if ((err = sidecar_broadcastValue(SIDECAR_CALLBACK_HEARTBEAT_PERIOD)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_ICE_ERROR;
    }
    /* err will be -1 on other failure */
    err = SIDECAR_ERROR_CANNOT_BROADCAST;
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return err;
}

int sharcs_sidecar_get_exposing() {
  int err;
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  if ((err = sidecar_broadcastValue(SIDECAR_CALLBACK_EXPOSING)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_ICE_ERROR;
    }
    /* err will be -1 on other failure */
    err = SIDECAR_ERROR_CANNOT_BROADCAST;
  }
 
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return err;
}

int sharcs_sidecar_get_fits_writing_extension() {
  int err;
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  if ((err = sidecar_broadcastValue(SIDECAR_CALLBACK_FITS_WRITING_EXTENSION)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_ICE_ERROR;
    }
    /* err will be -1 on other failure */
    err = SIDECAR_ERROR_CANNOT_BROADCAST;
  }
 
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);
  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_init

 * Description: initializes system, loads timing file

 * Inputs: 

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
      2010/01/11 JLW -- Initialization now done on ICE server
 *-
*************************************************************************/
int sharcs_sidecar_init(char *configuration, int downloadFirmware, int force) {
  const char* response;
  char *status;
  int err=0;
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Initializing SIDECAR...", __FILE__, __LINE__);
  /* have server read configuration file */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Configuring SIDECAR server with configuration file %s...", __FILE__, __LINE__, configuration);
  
  if ((err = sidecar_readSidecarControlConfiguration(configuration)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error initializing SIDECAR.", __FILE__, __LINE__);
    return SIDECAR_ERROR_READ_SIDECAR_CONFIGURATION;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: SIDECAR server successfully configured with configuration file %s...", __FILE__, __LINE__, configuration);


  /* init if forces, or */
  /* err from readSidecarControlConfiguration is positive:
   * err & 1 = 1 if application has changed, 
   * err & 2 = 2 if SidecarConfiguration has changed */

  if ((force == 1) || (err > 0)) {

    if ((err & SIDECAR_READ_CONFIG_STATUS_APPLICATION_CHANGED) != 0) {
      log_msg(SINFO, LOGLVL_USER4, "%s %d: SIDECAR Application file has changed.", __FILE__, __LINE__);
    } 
    if ((err & SIDECAR_READ_CONFIG_STATUS_SIDECAR_CONFIG_CHANGED) != 0) {
      log_msg(SINFO, LOGLVL_USER4, "%s %d: SIDECAR configuration has changed.", __FILE__, __LINE__);
    } 

    /* start initialization sequence */
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting SIDECAR initialization sequence...", __FILE__, __LINE__);
    response = sidecar_initializeSidecar(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, downloadFirmware);
    
    /* wait until complete */
    if (strcmp(response, "initializing") == 0) {
      while (strcmp(response, "initializing") == 0) {
	sleep(1);
	log_msg(SINFO, LOGLVL_USER4, "%s %d: Waiting for SIDECAR initialization to complete.  status = <%s>", __FILE__, __LINE__, response);
	if (sharcs_get_sidecar_init_status(&status) < 0) {
	  errlog(SERROR, "%s %d: Error getting SIDECAR initialization status.", __FILE__, __LINE__);
	  return SIDECAR_ERROR_INITIALIZE_SIDECAR;
	}
	response = strdup(status);
      }
    } else {
      errlog(SERROR, "%s %d: Error starting SIDECAR initialization sequence.  Response = %s", __FILE__, __LINE__, response);
      return SIDECAR_ERROR_INITIALIZE_SIDECAR;
    }
    if (strcmp(response, SIDECAR_SUCCESSFUL_RESULT) != 0) {    
      errlog(SERROR, "%s %d: SIDECAR initialization started, but did not succeed. Response=%s", __FILE__, __LINE__, response);
      if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
	sharcs_set_connected(SHARCS_CONNECTED_FALSE);
	return SIDECAR_ERROR_ICE_ERROR;
      }
      return SIDECAR_ERROR_INITIALIZE_SIDECAR;
    }
    log_msg(SINFO, LOGLVL_USER1, "%s %d: SIDECAR successfully initialized.", __FILE__, __LINE__);
  } else {
      log_msg(SINFO, LOGLVL_USER4, "%s %d: Not initializing hardware because readConfig did not return a positive value and force is off.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_get_read_time

 * Description: Gets read time from electronics

 * Inputs: none.

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
      2010/01/12 JLW -- Changed to get read time directly from ICE server
 *-
*************************************************************************/
int sharcs_sidecar_get_read_time(int *readTime)  {
  int result;
  
  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  
  /* get readTime in microsecs */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Getting read time from SIDECAR...", __FILE__, __LINE__);
  result = sidecar_getReadTimeInMicroseconds(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME);
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Read Time In Microsecs = %d.", __FILE__, __LINE__, result);
  
  if (result > 0) {
    //. convert to ms
    *readTime = result;
    result = 0;
  } else {
    *readTime = -1;
    if (((ERROR_SIDECAR_ICE_BASE - result) < 100) && ((ERROR_SIDECAR_ICE_BASE - result) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, result, getSidecarErrorMsg(result));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    } 
    errlog(SERROR, "%s %d: Error getting read time from SIDECAR.", __FILE__, __LINE__);
    return SIDECAR_ERROR_NON_POSITIVE_READ_TIME;
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return result;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_echo

 * Description: Performs echo to test availability of SidecarServer

 * Inputs: none.

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2011/03/19 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_echo(char *echoString)  {
  const char *response;
  int result;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  
  /* get readTime in microsecs */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Echoing %s to SidecarServer", __FILE__, __LINE__, echoString);
  response = sidecar_echo((const char *)echoString);
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Echo response = <%s>.", __FILE__, __LINE__, response);
  

  result = 0;
  if (strcmp(response, echoString) != 0) {
      if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
	sharcs_set_connected(SHARCS_CONNECTED_FALSE);
	result = SIDECAR_ERROR_ICE_ERROR;
      } else {
	errlog(SERROR, "%s %d: Echo mismatch: sent=<%s>, received=<%s>", __FILE__, __LINE__, echoString, response);
	result = SIDECAR_ERROR_ECHO_MISMATCH;
      }
  } 
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return result;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_download_mcd_file

 * Description: loads timing mcd file

 * Inputs: const char *mcdFile - filename of timing mcd file

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
      2010/01/12 JLW -- changed for downloading personality files (don't
        touch dataOnAck)
 *-
*************************************************************************/
int sharcs_sidecar_download_mcd_file(const char* mcdFile) {
		
  const char* response;
  
  /* note JLW: new purpose of this code is to 
   * download a personality file, which is a supplemental
   * timing file that is downloading after the normal one.
   * therefore, i don't think dataOnAck should be touched.
   */
  int err=0;
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  /* check to see if ASIC is available */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Getting available asics...", __FILE__, __LINE__);
  response = sidecar_getAvailableAsics(SIDECAR_DEFAULT_JADE_NAME);
  log_msg(SINFO, LOGLVL_USER1, "%s %d: GetAvailableAsics response=%s.", __FILE__, __LINE__, response);

  if (strstr(response, SIDECAR_DEFAULT_ASIC_NAME) == NULL) {
    errlog(SERROR, "%s %d: Error downloading timing mcd file: %s not available.", __FILE__, __LINE__, SIDECAR_DEFAULT_ASIC_NAME);
    if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    return SIDECAR_ERROR_ASIC_NOT_AVAILABLE;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: ASIC %s is available.", __FILE__, __LINE__, SIDECAR_DEFAULT_ASIC_NAME);

  usleep(SIDECAR_PAUSE_BETWEEN_COMMANDS);
	
  /* download MCD */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Downloading MCD file: %s.", __FILE__, __LINE__, mcdFile);
  response = sidecar_downloadMCDFile(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, mcdFile);
  
  if (strcmp(response, SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE) != 0) {    
    errlog(SERROR, "%s %d: Error downloading MCD file: %s.", __FILE__, __LINE__, response);
    if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    return SIDECAR_ERROR_DOWNLOAD_MCD_FILE;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: dowloadMCDFile success.  response=%s.", __FILE__, __LINE__, response);

  usleep(SIDECAR_PAUSE_BETWEEN_COMMANDS);
		
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_power_down

 * Description: Powers down asic

 * Inputs: none.

 * Outputs: Returns Sidecar error code.

 * Modification History:
      2009/08/28 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_power_down() {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Powering down ASIC.", __FILE__, __LINE__);

   if ((err = sidecar_powerDownASIC(SIDECAR_DEFAULT_JADE_NAME)) < 0) {
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error powering down ASIC: err=%d", __FILE__, __LINE__, err);
    return SIDECAR_ERROR_POWER_DOWN_ASIC;
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;

}

/*************************************************************************
 */
int sharcs_sidecar_write_asic_register(
    const char *address,
    const char *value)
{
    const char *response;

    response = sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME,
			    SIDECAR_DEFAULT_ASIC_NAME, address, value);

    if (strstr(response, "successful") == 0) {
	log_msg(SINFO, LOGLVL_USER4,
	    "Error setting ASIC register %s to %s\n", address, value, 0);
	return -1;
    } else {
	return 0;
    }
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_abort_exposure

 * Description: aborts exposure 

 * Inputs: int abortLevel - level for aborting (1 = immediately, 2 = after read, 3 = after coadd)

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
 *-
*************************************************************************/

int sharcs_sidecar_abort_exposure(int abortLevel) {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Aborting exposure with level <%d>.", __FILE__, __LINE__, abortLevel);

  /* TODO: for now, save images (second arg is 1) */
  if ((err = sidecar_abortExposure(abortLevel, 1)) < 0) {  
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error aborting exposure. err=%d", __FILE__, __LINE__, err);
    return SIDECAR_ERROR_ABORT_EXPOSURE;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Exposure successfully aborted with level <%d>.", __FILE__, __LINE__, abortLevel);

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}



/*************************************************************************
 *+
 * Function name: sharcs_sidecar_setExposureExtents

 * Description: sets the extents of the subarray in hardware

 * Inputs: int startx - left most column of subregion
           int starty - bottom most row of subregion
           int endx   - right most column of subregion
           int endy   - top most row of subregion

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2010/01/25 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_setExposureExtents(int startx, int starty, int endx, int endy) {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting exposure extents to [%d,%d] to [%d,%d]", __FILE__, __LINE__, startx, starty, endx, endy);
  
  if ((err = sidecar_setExposureExtents(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, startx, starty, endx, endy)) < 0) {
    
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error setting exposure extents.  err=%d", __FILE__, __LINE__, err);
    /* todo: return error based on retval? (-2 = invalid exp settings) */
    return SIDECAR_ERROR_SET_EXPOSURE_EXTENTS;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: Exposure extents successfully set to [%d,%d] to [%d,%d]", __FILE__, __LINE__, startx, starty, endx, endy);
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_setReferencePixelSubtractionType

 * Description: sets the reference pixel subtraction type

 * Inputs: int type - type of RPS.  uses a bit pattern.  see sidecar software for deatils.

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2010/09/01 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_setReferencePixelSubtractionType(int type) {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting reference pixel subtraction type to %d.", __FILE__, __LINE__, type);
  
  if ((err = sidecar_setReferencePixelSubtractionType(type)) < 0) {
    
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error setting reference pixel subtraction type.  err=%d", __FILE__, __LINE__, err);
    /* todo: return error based on retval? (-2 = invalid exp settings) */
    return SIDECAR_ERROR_SET_REFERENCE_PIXEL_SUBTRACTION_TYPE;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: Reference pixel subtraction type successfully set to %d", __FILE__, __LINE__, type);
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}
/*************************************************************************
 *+
 * Function name: sharcs_sidecar_writeRawFrames

 * Description: sets the write raw frames

 * Inputs: int enable - enable writing of raw frames

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2010/09/01 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_writeRawFrames(int enable) {
  int err=0;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting write raw frames to %d.", __FILE__, __LINE__, enable);
  
  if ((err = sidecar_writeRawFrames(enable)) < 0) {
    
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error enabling/disabling write raw frames.  err=%d", __FILE__, __LINE__, err);
    /* todo: return error based on retval? (-2 = invalid exp settings) */
    return SIDECAR_ERROR_WRITE_RAW_FRAMES;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: write raw frames successfully set to %d", __FILE__, __LINE__, enable);
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}
/*************************************************************************
 *+
 * Function name: sharcs_sidecar_take_exposure

 * Description: takes exposure based on exposure configuration passed in

 * Inputs: ExposureConfiguration config - sturcture containing exposure parameters

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2007/10/25 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_takeExposure(struct ExposureConfiguration config) {
  int err=0;
  uint64_t sidecarItime;

  log_msg( SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting exposure with output file <%s>.", __FILE__, __LINE__, config.fitsFilename);
  log_msg(SINFO, LOGLVL_USER4, "%s %d: exposure params: itime = %d ms, coadds = %d, resets = %d, sampling mode = %d [%s], reads = %d, groups = %d.", __FILE__, __LINE__, config.integrationTime, 
	  config.numberOfCoadds, config.numberOfResets, 
	  config.samplingMode, SamplingModeNames[config.samplingMode], 
	  config.numberOfReads, config.numberOfGroups);
  
  sidecarItime = (uint64_t)config.integrationTime * 1000;

  if ((err = sidecar_takeExposure(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, config.numberOfResets, sidecarItime, config.numberOfCoadds, config.samplingMode, config.numberOfReads, config.numberOfGroups, config.fitsFilename)) < 0) {

    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      return SIDECAR_ERROR_ICE_ERROR;
    }
    errlog(SERROR, "%s %d: Error starting sidecar exposure.  err=%d", __FILE__, __LINE__, err);
    /* todo: return error based on retval? (-2 = invalid exp settings) */
    return SIDECAR_ERROR_TAKE_EXPOSURE;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Exposure successfully started with output file <%s>.", __FILE__, __LINE__, config.fitsFilename);
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_getSidecarPower

 * Description: Gets power status of SIDECAR

 * Inputs: 

 * Outputs:  status [out] power status (0:off, 1:on)

     Returns Sidecar error code.

 * Modification History:
      2010/09/03 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_sidecar_getSidecarPower(int *status) {
  int err=0;
  const char* response;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "Reading register 0x0020...\n");
  response = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0020");
  log_msg(SINFO, LOGLVL_USER1, "Current Jade register 0x0020 value=%s.\n", response);
  
  if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
    sharcs_set_connected(SHARCS_CONNECTED_FALSE);
    err = SIDECAR_ERROR_ICE_ERROR;
  } else {
    if (strcasecmp(response, "0x0000") != 0) {
      *status=1;
      return 0;
    }
  
    log_msg(SINFO, LOGLVL_USER4, "Reading register 0x0021...\n");
    response = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0021");
    log_msg(SINFO, LOGLVL_USER1, "Current Jade register 0x0021 value=%s.\n", response);
  
    if (strstr(response, ERROR_SIDECAR_EXCEPTION_STRING)) {
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err = SIDECAR_ERROR_GET_POWER_STATUS;
    } else {
      if (strcasecmp(response, "0x0000") != 0) {
	*status=1;
	return 0;
      } else {
	*status=0;
      }
    }
  }
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

void freeKeys( struct FitsHeaderKey *keys, int numKeys) {

  int ii = 0;

  for (; ii<numKeys; ii++) {
    if ( keys[ii].value != NULL )
      free(keys[ii].value);
  }
}

/*************************************************************************
 *+
 * Function name: sharcs_sidecar_addFitsHeader

 * Description: Sends FITS header to sidecar ice server to be added to current exposure

 * Inputs: FITS_HEAD_CARD *card - First card in linked list of cards

 * Outputs:  Returns Sidecar error code.

 * Modification History:
      2009/05/01 JLW -- Initial.
 *-
*************************************************************************/

int sharcs_sidecar_addFitsHeader(char *fitsFilename, FITS_HEAD_CARD *cardList) {
  int err=0;
  int ii, jj, keynum;
  struct FitsHeaderKey *keys;
  int numlen;
  int numKeys, numCards, keyCount;
  FITS_HEAD_CARD *firstCard;
  KEY_INFO *currentKey;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  /* first, go through cards and get number of keys */
  
  /* keep reference to first card */
  firstCard = cardList;
  numKeys=0;
  numCards=0;
  while (cardList != NULL) {
    numKeys += cardList->keyCount;
    numCards++;
    cardList = cardList->next;
  }

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Add Fits Header. numCards = %d, numKeys = %d", __FILE__, __LINE__, numCards, numKeys);

  keys = (struct FitsHeaderKey *)calloc(numKeys, sizeof(struct FitsHeaderKey));

  cardList = firstCard;
  keynum=0;
  /* go through linked list and create header */
  for (ii=0; ii<numCards; ii++) {
    if (cardList == NULL) {
      free (keys);
      errlog(SERROR, "%s %d: Error adding FITS header card.  Card is null.", __FILE__, __LINE__);
      return SIDECAR_ERROR_FITS_HEADER_NULL_CARD;
    }

    keyCount = (int)(cardList->keyCount);
    for (jj=0; jj<keyCount; jj++) {
      currentKey = &(cardList->keys[jj]);
      
      log_msg(SINFO, LOGLVL_USER4, "%s %d: keynum=%d of %d (%d of %d in this card). current key: <%s>", __FILE__, __LINE__, keynum, numKeys, jj, keyCount, currentKey->name);

      if (currentKey->comment == 1) {
	keys[keynum].name = "COMMENT";
	keys[keynum].type = KeyTypeComment;
	keys[keynum].value= strdup("");
	keys[keynum].comment = currentKey->strVal;
      } else {
	keys[keynum].name = currentKey->name;
	keys[keynum].comment = currentKey->remark;
	
	switch (currentKey->type) {
	case KTL_BOOLEAN: 
	  keys[keynum].type = KeyTypeBoolean;
	  keys[keynum].value = (currentKey->value.i == 0) ? strdup("0") : strdup("1");
	  break;
	case KTL_INT: 
        case KTL_ENUM:
        case KTL_ENUMM:
        case KTL_MASK:
	  keys[keynum].type = KeyTypeInteger;
	  if (currentKey->value.i == 0) {
	    numlen=1;
	  } else if (currentKey->value.i < 0) {
	    numlen = (int)floor(log10(abs(currentKey->value.i))) + 2;
	  } else {
	    numlen = (int)floor(log10(currentKey->value.i)) + 1;
	  }
	  keys[keynum].value = (char *)malloc(sizeof(char) * (numlen+1));
	  snprintf(keys[keynum].value, (numlen+1), "%d", currentKey->value.i);
	  break;
	case KTL_FLOAT: 
	  keys[keynum].type = KeyTypeFloat;
	  numlen = MAX_VALUE_LENGTH_FLOAT;
	  keys[keynum].value = (char *)malloc(sizeof(char) * (numlen+1));
	  snprintf(keys[keynum].value, numlen+1, "%0.8f", currentKey->value.f);
	  break;
	case KTL_DOUBLE: 
	  keys[keynum].type = KeyTypeDouble;
	  numlen = MAX_VALUE_LENGTH_DOUBLE;
	  keys[keynum].value = (char *)malloc(sizeof(char) * (numlen+1));
	  snprintf(keys[keynum].value, numlen+1, "%0.8lf", currentKey->value.d);
	  break;
	case KTL_STRING: 
	  keys[keynum].type = KeyTypeString;
	  keys[keynum].value = strdup(currentKey->strVal);
	  break;
	default: 
	  free(keys);
	  errlog(SERROR, "%s %d: Invalid FITS header card datatype.", __FILE__, __LINE__);
	  return SIDECAR_ERROR_FITS_HEADER_INVALID_DATATYPE;
	};

      log_msg(SINFO, LOGLVL_USER4, "%s %d: key info(%d) %s->%s", __FILE__, __LINE__, keynum, keys[keynum].name,keys[keynum].value);

      }
      keynum++;
    }

    cardList = cardList->next;
  }
  
  log_msg( SINFO, LOGLVL_USER4, "%s %d: adding %d FITS header keys.", __FILE__, __LINE__, numKeys);

  if ((err = sidecar_addFitsHeader(fitsFilename, keys, numKeys)) < 0) {  
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
    }
    errlog(SERROR, "%s %d: Error adding %d FITS header keys.  Err=%d ", __FILE__, __LINE__, numKeys, err);
    err = SIDECAR_ERROR_ADD_FITS_HEADER;
  } else {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: %d FITS header keys successfully added.", __FILE__, __LINE__, numKeys);
  }

  freeKeys(keys, numKeys);
  free(keys);

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}

int sharcs_sidecar_addFitsHeader_KeyBased(char *fitsFilename, struct FitsHeaderKey *keys, int numKeys) {

  int err=0;
  if ((err = sidecar_addFitsHeader(fitsFilename, keys, numKeys)) < 0) {  
    if (((ERROR_SIDECAR_ICE_BASE - err) < 100) && ((ERROR_SIDECAR_ICE_BASE - err) > 0)) {
      errlog(SERROR, "%s %d: SIDECAR Error %d <%s>. Setting connected as false.", __FILE__, __LINE__, err, getSidecarErrorMsg(err));
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
    }
    errlog(SERROR, "%s %d: Error adding %d FITS header keys.  Err=%d ", __FILE__, __LINE__, numKeys, err);
    err = SIDECAR_ERROR_ADD_FITS_HEADER;
  } else {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: %d FITS header keys successfully added.", __FILE__, __LINE__, numKeys);
  }

  return err;
}
