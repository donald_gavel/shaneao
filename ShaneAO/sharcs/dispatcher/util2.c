


/*
 * For glibc, strptime() is available if _XOPEN_SOURCE is defined.
 * But defining it messes up many other things -- for example,
 * snprintf() wasn't available when we defined _XOPEN_SOURCE
 * early in our #include sequence, and when we defined it late
 * in the #include sequence, it didn't trigger availability of
 * strptime().  So we isolate it all here.
 */

#define _XOPEN_SOURCE
#include <time.h>

char *
call_strptime(
    const char *timestring,
    const char *fmt,
    struct tm *tm)
{
    return strptime(timestring, fmt, tm);
}

