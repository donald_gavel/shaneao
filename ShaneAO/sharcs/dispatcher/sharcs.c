/*+***************************************************************************

 *  File:     sharcs.c

 *  Purpose:  The functions herein are specific to the SHARCS Detector
              server (sharcs).

 *  Modification history:
 *    2002/12/27 Jason L. Weiss - Initial
 *
 *-**************************************************************************/
#include "config.h"
RCSID_DEF(rcsid,"$Id: sharcs.c,v 1.19 2014/07/16 21:14:44 will Exp $");

#include <stdio.h>
#include <glob.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <signal.h>
#include <syslog.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include <sys/stat.h>  /* times, tms */
#include <sys/times.h>  /* times, tms */


#ifdef MR_SERVER
    #include "mr_server/mr_server.h"
#else
    // Not using mr_server
    #include "rpcKey.h"
#endif

#include "ktl.h"
#include "ktl_keyword.h"
#include "util2.h"

#ifdef MR_SERVER
#else
    // Not using mr_server
    #include "rpc_if.h"
    #include "util_prototypes.h"

    #include "rpc_service.h"
    extern char *MyService; /* for broadcasting (key_sendMonitor) */
#endif


#include "sharcs.h"
#include "sharcs_sidecar.h"
#include "sharcs_fits.h"
#include "SidecarIceClient.h"

extern int Prognum;    /* in rpcKey_server.c */
extern unsigned int Verbose;    /* in errlog.c */

extern int Simulate;

struct exposure_struct *last_exp;
struct ValueListener sharcsValueListener;

int readTimeInMicros = 0;
char *lastMCDFileDownloaded;

extern volatile int64 rampStartTime;  /* in sharcs_if.c */
extern volatile int64_t dateBeg;	/* in sharcs_if.c */
extern volatile int isExposing;  /* in sharcs_if.c */
extern volatile float exposureProgress;   /* in sharcs_if.c */
extern volatile int heartbeatPrimed; /* in sharcs_if.c */

/*************************************************************************/


/*************************************************************************
 *+
 * Function name: sharcs_open_driver

 * Description: Opens the sdsu driver

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2005/05/10 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_open_driver(char *hostname, int port)
{
  int err=0;
  int lasterr=0;
  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);


  errlog(SINFO, "Opening ICE connection to SIDECAR server on host %s port %d", hostname, port);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Opening driver: host=%s, port=%d", __FILE__, __LINE__, hostname, port);
  /* open sidecar driver */
  if ( (err=sharcs_sidecar_open_driver(hostname, port)) < 0 ) {
    errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    lasterr=SHARCS_ERROR_OPEN_DRIVER;
  } else {
    log_msg(SINFO, LOGLVL_USER1, "%s %d: Connection made to ICE server on host=%s, port=%d.  Adding callback listener.", __FILE__, __LINE__, hostname, port);
    /* set listener */
    sharcsValueListener.setValueFunc = sharcs_value_callback;

    sharcs_sidecar_addCallbackListener(&sharcsValueListener);

    if (( err = sharcs_sidecar_get_heartbeat_period()) < 0) {

      errlog(SERROR, "%s %d: error requesting heartbeat period value broadcast", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0 - err]);
      lasterr=SHARCS_ERROR_REQUEST_HEARTBEAT_PERIOD;
    }

    if (( err = sharcs_sidecar_get_exposing()) < 0) {

      errlog(SERROR, "%s %d: error requesting exposing value broadcast", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0 - err]);
      lasterr=SHARCS_ERROR_REQUEST_EXPOSING;
    }

    if (( err = sharcs_sidecar_get_fits_writing_extension()) < 0) {

      errlog(SERROR, "%s %d: error requesting fits writing extension value broadcast", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0 - err]);
      lasterr=SHARCS_ERROR_REQUEST_FITS_WRITING_EXT;
    }

    if ((err = sharcs_get_asic_power()) < 0) {
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,SHARCSErrMsg[SHARCS_ERROR_CODE0-err]);
      lasterr=err;
    }

  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

  return lasterr;
}

/*************************************************************************
 *+
 * Function name: sharcs_hardware_init

 * Description: Opens serial port and begins initialization sequence
                of hardware

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_hardware_init(char *sidecar_cfg, int downloadFirmware, char *timing_file, int force)
{
  pthread_t init_thread;
  pthread_attr_t thread_attr;
  struct HardwareInit *initStruct;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Creating hardware init structure.  doInit=%d, sidecarcfg=%s, timing_file=%s, dlfirmware=%d", __FILE__, __LINE__, 1, sidecar_cfg, timing_file, downloadFirmware);

  if (force == 0) {
    /* if timing file has changed since last download, force hardware init */
    if (strcmp(timing_file, lastMCDFileDownloaded) != 0) {
      force = 1;
    }
  }

  initStruct = (struct HardwareInit *)malloc(sizeof(struct HardwareInit));

  initStruct->doInit=1;
  strncpy(initStruct->sidecarCfg, sidecar_cfg, SHARCS_FILENAME_LENGTH);
  strncpy(initStruct->mcdFile, timing_file, SHARCS_FILENAME_LENGTH);
  initStruct->downloadFirmware = downloadFirmware;
  initStruct->forceInit=force;

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting sharcs_hardware_init_thread", __FILE__, __LINE__);
  if ( pthread_attr_init( &thread_attr ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_init failed (%d) so the hardware init thread could not be started.",__FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }

  if ( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_setdetachstate failed (%d) so the hardware init thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }


  if ( pthread_create(&init_thread, &thread_attr, sharcs_hardware_init_thread, (void *)initStruct) < 0) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_create failed (%d) so the hardware init  thread could not be started.", __FILE__, __LINE__, errno );


    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: hardware init thread started.", __FILE__, __LINE__);

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return 0;
}

void *sharcs_hardware_init_thread(void *pHardwareInitStruct) {
  int err=0;
  int ii;
  int notEmpty = 0;
  struct HardwareInit *initStruct = (struct HardwareInit *) pHardwareInitStruct;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  sharcs_set_initing(SHARCS_INITING_TRUE);

  if (initStruct->doInit > 0) {
    /* start hardware initialization sequence */
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Initializing sidecar...", __FILE__, __LINE__);
    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_INITIALIZING_SIDECAR);
    if ( (err=sharcs_sidecar_init(initStruct->sidecarCfg, initStruct->downloadFirmware, initStruct->forceInit)) < 0 ) {
      sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_ERROR_INITIALIZING_SIDECAR);
      errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
      err=SHARCS_ERROR_HARDWARE_INIT;
    } else {
      if ((err > 0) || (initStruct->forceInit > 0)) {
	/* reset error */
	err=0;
	log_msg(SINFO, LOGLVL_USER1, "%s %d: Sidecar successfully initialized.  Checking to see if timing mcd file should be downloaded.", __FILE__, __LINE__);

	/* load timing mcd file if item is not empty */
	for (ii = 0; ii< strlen(initStruct->mcdFile); ii++) {
	  if (isspace(initStruct->mcdFile[ii]) == 0) {
	    notEmpty = 1;
	    break;
	  }
	}

	if (notEmpty) {
	  sharcs_set_exposing(SHARCS_FALSE);
	  sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_DOWNLOADING_MCD_FILE);
	  log_msg(SINFO, LOGLVL_USER4, "%s %d: Downloading sidecar timing mcd file %s", __FILE__, __LINE__, initStruct->mcdFile);
	  if ( (err=sharcs_sidecar_download_mcd_file(initStruct->mcdFile)) < 0) {
	    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_ERROR_DOWNLOADING_MCD_FILE);
	    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
	    err=SHARCS_ERROR_DOWNLOAD_MCD_FILE;
	  } else {
	    free(lastMCDFileDownloaded);
	    lastMCDFileDownloaded = strdup(initStruct->mcdFile);
	    log_msg(SINFO, LOGLVL_USER1, "%s %d: Timing mcd file %s successfully downloaded.", __FILE__, __LINE__, initStruct->mcdFile);
	  }
	} else {
	  free(lastMCDFileDownloaded);
	  lastMCDFileDownloaded = strdup(initStruct->mcdFile);
	  log_msg(SINFO, LOGLVL_USER1, "%s %d: Not downloading timing mcd file. No file specified.", __FILE__, __LINE__);
	}
      } else {
	log_msg(SINFO, LOGLVL_USER4, "%s %d: Not downloading timing mcd file because hardware was not initialized.", __FILE__, __LINE__);
      }
    }
  } else {
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Skipping hardware initialization.", __FILE__, __LINE__);
  }
  /* if no errors so far, make sure device is properly
   * inited by checking frame read time */
  if (err == 0) {

    setIntKeyword("asicpwr", 1, BROADCAST_TRUE);

    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_GETTING_IGNORED_READS);
    err = sharcs_sidecar_get_ignored_reads();

    if (err < 0) {
      sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_ERROR_GETTING_IGNORED_READS);
      errlog(SERROR, "%s %d: error requesting ignored reads value broadcast", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0 - err]);
      err=SHARCS_ERROR_HARDWARE_INIT;
    }

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Checking read time.", __FILE__, __LINE__);

    setIntKeyword("dlfirmware", 0, BROADCAST_TRUE);

    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_GETTING_READ_TIME);
    err = sharcs_get_read_time();

    if (err < 0) {
      err=SHARCS_ERROR_HARDWARE_INIT;
    } else {

      sharcs_set_status(SHARCS_STATUS_IDLING);
      sharcs_set_main_status(SHARCS_STATUS_IDLING);
      sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_READY);
      sharcs_set_ready(SHARCS_READY_YES);

    }
  }

  sharcs_set_initing(SHARCS_INITING_FALSE);

  free(pHardwareInitStruct);

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

  pthread_exit((void *) ((long) err));
}

/*************************************************************************
 * Retrieve read time.
 * On success, store in readTimeInMicros, set readtime keyword, and return 0.
 * On error, set detailed status, log error, and return -1.
 */
int
sharcs_get_read_time()
{
    int err;
    int readTime;

    err = sharcs_sidecar_get_read_time(&readTime);

    if (err < 0) {
      sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_ERROR_GETTING_READ_TIME);
      errlog(SERROR, "%s %d: error getting read time", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,
      				SidecarErrMsg[SIDECAR_ERROR_CODE0 - err]);
      return -1;

    } else {
      errlog(SINFO,
	  "%s %d: Read time successfully retrieved, = %d microseconds.",
				  __FILE__, __LINE__, readTime);
      readTimeInMicros = readTime;
      sharcs_set_readtime(readTimeInMicros/1000000.);
      return 0;
    }
}

/*************************************************************************
 *+
 * Function name: sharcs_hardware_resume

 * Description: Attempts to resume use of sidecar without
       initializing hardware.  therefore, just call
       download timing code function, since that tests for
       ASICs availabilty, and it can be assumed that if
       timing code is redownloaded (which is quick), the
       ASIC is ready to go for exposures.

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_hardware_resume()
{
  pthread_t init_thread;
  pthread_attr_t thread_attr;
  struct HardwareInit *initStruct;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  initStruct = (struct HardwareInit *)malloc(sizeof(struct HardwareInit));

  initStruct->doInit=0;

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting sharcs_hardware_init_thread (no init, just check read time)", __FILE__, __LINE__);
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting sharcs_hardware_init_thread", __FILE__, __LINE__);
  if ( pthread_attr_init( &thread_attr ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_init failed (%d) so the hardware init thread could not be started.",__FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }

  if ( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_setdetachstate failed (%d) so the hardware init thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }


  if ( pthread_create(&init_thread, &thread_attr, sharcs_hardware_init_thread, (void *)initStruct) < 0) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_create failed (%d) so the hardware init  thread could not be started.", __FILE__, __LINE__, errno );


    return SHARCS_ERROR_START_HARDWARE_INIT_THREAD;
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

  return 0;
}

/*************************************************************************
 *+
 * Function name: sharcs_power_down_asic

 * Description: Call sidecar command to power down asic

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2009/08/28 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_power_down_asic()
{
  int err=0;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  log_msg( SINFO, LOGLVL_USER4, "%s %d: powering down asic.", __FILE__, __LINE__);
  /* call power down command  */
  if ( (err=sharcs_sidecar_power_down()) < 0) {
    errlog(SERROR, "%s %d: error powering down asic", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    err=SHARCS_ERROR_POWER_DOWN_ASIC;
  } else {
  log_msg( SINFO, LOGLVL_USER1, "%s %d: ASIC successfully powered down.", __FILE__, __LINE__);
    setIntKeyword("asicpwr", 0, BROADCAST_TRUE);
    free(lastMCDFileDownloaded);
    lastMCDFileDownloaded = strdup("none");
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_abort_exposure

 * Description:

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_abort_exposure(int abortLevel)
{
  int err=0;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);
  log_msg( SINFO, LOGLVL_USER4, "%s %d: Sending abort command.", __FILE__, __LINE__);
  /* abort exposure  */
  if ( (err=sharcs_sidecar_abort_exposure(abortLevel)) < 0 ) {
    errlog(SERROR, "%s %d: error sending abort command.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    err=SHARCS_ERROR_ABORT_EXPOSURE;
  } else {
  log_msg( SINFO, LOGLVL_USER1, "%s %d: Abort exposure command successfully sent.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_set_reference_pixel_subtraction_type

 * Description:

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_set_reference_pixel_subtraction_type(int type)
{
  int err=0;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);
  log_msg( SINFO, LOGLVL_USER4, "%s %d: Setting reference pixel subtraction type.", __FILE__, __LINE__);
  /* set ref pixel subtraction type  */
  if ( (err=sharcs_sidecar_setReferencePixelSubtractionType(type)) < 0 ) {
    errlog(SERROR, "%s %d: error setting reference pixel subtraction type.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    err=SHARCS_ERROR_SET_REFERENCE_PIXEL_SUBTRACTION_TYPE;
  } else {
  log_msg( SINFO, LOGLVL_USER1, "%s %d: Reference pixel subtraction successfully set.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}

/*************************************************************************
 *+
 * Function name: sharcs_write_raw_frames

 * Description:

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_write_raw_frames(int enable)
{
  int err=0;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);
  log_msg( SINFO, LOGLVL_USER4, "%s %d: Setting write raw frames.", __FILE__, __LINE__);
  /* set write raw frames  */
  if ( (err=sharcs_sidecar_writeRawFrames(enable)) < 0 ) {
    errlog(SERROR, "%s %d: error enabling/disabling writing of raw frames.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    err=SHARCS_ERROR_WRITE_RAW_FRAMES;
  } else {
  log_msg( SINFO, LOGLVL_USER1, "%s %d: Writing of raw frames successfully enabled/disabled.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}
/*************************************************************************
 *+
 * Function name: sharcs_monitor_sidecar_server

 * Description: gets current time and compares with heartbeat from
                SidecarServer to confirm SidecarServer is still
                running

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2011/03/19 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_monitor_sidecar_server()
{
  int err=0;
  time_t        caltime;
  struct tm     *tm;
  struct tm     heartbeat_tm;
  char timestamp[20];
  char *heartbeat;
  time_t heartbeatTime;
  int heartbeatperiod;
  int timediff;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);
  /* get last heartbeat */
  getStringKeyword("heartbeat", &heartbeat);

  /* heartbeat pattern is YYYY/MM/DD hh:mm:ss */
  call_strptime(heartbeat, "%Y/%m/%d %H:%M:%S", &heartbeat_tm);

  /* get system time */
  caltime=time(NULL);
  tm=localtime(&caltime);

  /* see Stevens p. 158 for formatting info */
  strftime(timestamp, 20, "%Y/%m/%d %H:%M:%S", tm);

  /* make sure mktime checks for DST */
  heartbeat_tm.tm_isdst=-1;

  getIntKeyword("heartbeatperiod", &heartbeatperiod);

  log_msg( SINFO, LOGLVL_USER4, "%s %d: Comparing current time <%s> with last heartbeat <%s>, heartbeatperiod=<%d>.", __FILE__, __LINE__,  timestamp, heartbeat, heartbeatperiod);

  heartbeatTime = mktime(&heartbeat_tm);

  if ((timediff = abs((int)(caltime - heartbeatTime))) > (heartbeatperiod/500. + SHARCS_CLOCK_OFFSET_ALLOWANCE_SECONDS)) {
    errlog(SERROR, "%s %d: Heartbeat time difference %d is greater then twice heartbeatperiod (%8.2f s) plus clock offset allowance (%d s).", __FILE__, __LINE__, timediff, heartbeatperiod/500., SHARCS_CLOCK_OFFSET_ALLOWANCE_SECONDS);
    if (heartbeatPrimed == 1) {
      sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      err=SHARCS_ERROR_CHECK_FOR_SERVER;
    }
  } else {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: SidecarServer heartbeat ok.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}
/*************************************************************************
 *+
 * Function name: sharcs_get_asic_power

 * Description:

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_get_asic_power()
{
  int err=0;
  int status;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  /* set write raw frames  */
  if ( (err=sharcs_sidecar_getSidecarPower(&status)) < 0 ) {
    errlog(SERROR, "%s %d: error getting SIDECAR power.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s\n", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
    err=SHARCS_ERROR_GET_POWER;
    status = -1;
  } else {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: SIDECAR Power successfully obtained (%d)", __FILE__, __LINE__, status);
  }
  setIntKeyword("asicpwr", status, BROADCAST_TRUE);

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return err;
}
int sharcs_start_headerTest(struct ExposureConfiguration *pExp)
{
  pthread_attr_t thread_attr;

  struct ExposureConfiguration *fits_header_exp;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  fits_header_exp=(struct ExposureConfiguration *)malloc(sizeof(struct ExposureConfiguration));
  *fits_header_exp = *pExp;

  /* Start fits header thread */
  if ( pthread_attr_init( &thread_attr ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_init failed (%d) so the fits header thread could not be started.",__FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }

  if ( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_setdetachstate failed (%d) so the fits header thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }

  if (   pthread_create(&(pExp->fits_header_thread), &thread_attr, sharcs_fits_header, (void *)fits_header_exp) < 0) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_create failed (%d) so the fits header  thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: Fits header thread started.", __FILE__, __LINE__);

  return 0;
}

/*************************************************************************
 *+
 * Function name: sharcs_start_exposure

 * Description:

 * Inputs:
     struct exposure_struct *pExp - a pointer to a structure that
             contains information about the exposure.  see sharcs.h

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2003/07/09 JLW -- Initial.
      2007/06/21 JLW -- Modified from ods.c
 *-
*************************************************************************/
int sharcs_start_exposure(struct ExposureConfiguration *pExp)
{
  int retval;
  pthread_attr_t thread_attr;

  struct ExposureConfiguration *fits_header_exp;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  /* set exposure extents */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting exposure extents", __FILE__, __LINE__);
  if ((retval = sharcs_sidecar_setExposureExtents(pExp->startx, pExp->starty, pExp->endx, pExp->endy)) < 0) {
    errlog(SERROR, "%s %d: error setting exposure extents", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-retval]);
    return SHARCS_ERROR_START_EXPOSURE;
  }


#if 1
  if (window_changed(pExp->startx, pExp->starty, pExp->endx, pExp->endy)) {
errlog(SDEBUG, "A 1");
      /* Update readtime keyword, and save the changed window.
       */

      if (sharcs_get_read_time() < 0) {
	  // Ignore error; problem was reported in detailed status,
	  // and (at this writing) there's nothing more to do.
      }

  }
#endif
  // Always (re)save the current window.
  save_window(pExp->startx, pExp->starty, pExp->endx, pExp->endy);

  fits_header_exp=(struct ExposureConfiguration *)malloc(sizeof(struct ExposureConfiguration));
  *fits_header_exp = *pExp;

  sharcs_clear_rampStartTime_and_dateBeg();

  errlog(SINFO, "%s %d: Starting exposing", __FILE__, __LINE__);

  if ((retval = sharcs_sidecar_takeExposure(*pExp)) < 0) {
    errlog(SERROR, "%s %d: error starting exposure", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-retval]);
    return SHARCS_ERROR_START_EXPOSURE;
  }
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Exposure started.", __FILE__, __LINE__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting fits header thread for %s", __FILE__, __LINE__, pExp->fitsFilename);

  /* Start fits header thread */
  if ( pthread_attr_init( &thread_attr ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_init failed (%d) so the fits header thread could not be started.",__FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }

  if ( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_setdetachstate failed (%d) so the fits header thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }


  if (   pthread_create(&(pExp->fits_header_thread), &thread_attr, sharcs_fits_header, (void *)fits_header_exp) < 0) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_create failed (%d) so the fits header  thread could not be started.", __FILE__, __LINE__, errno );


    return SHARCS_ERROR_START_FITS_HEADER_THREAD;
  }


  log_msg(SINFO, LOGLVL_USER1, "%s %d: Fits header thread started.", __FILE__, __LINE__);

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

  /* return */
  return 0;
}

void sharcs_value_callback(const char *name, const char *value) {
  static char *mainStatus = "";
  char *status;
  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: SIDECAR callback received.  Name=%s, value=%s", __FILE__, __LINE__, name, value);

  if (strcmp(name, SIDECAR_CALLBACK_EXPOSURE_STATUS) == 0) {
    errlog(SINFO, " - %s.", value);
    mainStatus=strdup(value);
    sharcs_set_status(mainStatus);
    sharcs_set_main_status((char *)value);
    sharcs_set_detailed_status(" ");
  } else if (strcmp(name, SIDECAR_CALLBACK_EXPOSURE_STATUS_DETAIL) == 0) {
    errlog(SINFO, "   - %s.", value);
    sharcs_set_detailed_status((char *)value);
    status = (char *)malloc(sizeof(char)*(strlen(mainStatus)+strlen(value)+3));
    strcpy(status, mainStatus);
    status=strcat(status, ": ");
    status=strcat(status, value);
    sharcs_set_status(status);
    free(status);
  } else {
    if (strcmp(name, SIDECAR_CALLBACK_HEARTBEAT) != 0) {
	// Log anything except heartbeat.
	errlog(SDEBUG, "*** from sidecar: %s = %s.", name, value);
    }

    if (strcmp(name, SIDECAR_CALLBACK_ICE_SERVER) == 0) {
      if (strcmp(value, "shutdown") == 0) {
	sharcs_sidecar_server_shutdown();
	sharcs_set_connected(SHARCS_CONNECTED_FALSE);
      }
    } else if (strcmp(name, SIDECAR_CALLBACK_EXPOSURE_IN_PROGRESS) == 0) {
      if ((int)atoi(value) == 1) {
	  sharcs_set_rampStartTime_and_dateBeg();
      }
    } else if (strcmp(name, SIDECAR_CALLBACK_EXPOSING) == 0) {
      sharcs_set_exposing((int)atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_IGNORED_READS) == 0) {
      sharcs_set_ignored_reads((int)atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_GROUPS_DONE) == 0) {
      sharcs_set_groups_done((int)atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_READS_DONE) == 0) {
      sharcs_set_reads_done((int)atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_COADDS_DONE) == 0) {
      sharcs_set_coadds_done((int)atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_IMAGE_DONE) == 0) {
       char *file;
       char *ext;
       int image_done_value = atoi(value);
       if (getStringKeyword("lastfile", &file) < 0) {
	   errlog(SERROR, "%s %d: unable to get LASTFILE.",
			    __FILE__, __LINE__);
       } else if (image_done_value == 0) {
	  // Image not yet done.
	  // Simply record this value.
	  sharcs_set_image_done(image_done_value);

       } else {
	  /* Before we set IMAGEDONE, we'll adjust the file permissions
	   * to our normal set (without the execute permission set
	   * by stealthie), and we'll add the setuid bit to indicate
	   * to the Lick archiver that this file can be archived
	   * immediately.
	   */
	   int mode = S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH|S_ISUID;
	   if (chmod(file,mode) == -1) {
	       errlog(SERROR, "%s %d: unable to set mode 0%o on file %s: %s",
		    __FILE__, __LINE__, mode, file, strerror(errno));
	   }
	   /* Are there other files to modify, too?
	    * That is, in addition to xxx.fits, check for
	    * xxx_*.fits.
	    */
	   if (getStringKeyword("extension", &ext) < 0) {
	       errlog(SERROR, "%s %d: unable to get EXTENSION.",
			    __FILE__, __LINE__);
	   } else {
		char pat[1000];
		int n;
		glob_t globbed;

		// Build a glob pattern based on the last filename and extension:
		n = strlen(file) - strlen(ext) - 1;
		if (n > 0) {
		    // Construct the glob pattern:
		    snprintf(pat, sizeof(pat), "%.*s_*.%s", n, file, ext);
		    // Glob the files:
		    if (glob(pat, 0, NULL, &globbed) == 0) {
			// Found some files
			int i;
			for (i = 0; i < globbed.gl_pathc; i++) {
			    chmod(globbed.gl_pathv[i], mode);
			}
		    }
		    globfree(&globbed);
		}
	   }
       }

       sharcs_set_image_done(image_done_value);

       // Increment framenum
       if (image_done_value == 1) {
	   int fn;
	   getIntKeyword("framenum", &fn);
	   errlog(SDEBUG,
	       "End of exposure auto-increment frame number from %d to %d",
      								fn, fn+1);
	   setIntKeyword("framenum", fn+1, BROADCAST_TRUE);
	}

    } else if (strcmp(name, SIDECAR_CALLBACK_PROGRESS) == 0) {
      if ((float)atof(value) >= 1.0) {
	sharcs_set_exposure_complete(1.0);
      }
      /*      sharcs_set_exposure_complete((float)atof(value)); */
    } else if (strcmp(name, SIDECAR_CALLBACK_HEARTBEAT) == 0) {
      sharcs_set_heartbeat((char *)value);
    } else if (strcmp(name, SIDECAR_CALLBACK_HEARTBEAT_PERIOD) == 0) {
      sharcs_set_heartbeat_period(atoi(value));
    } else if (strcmp(name, SIDECAR_CALLBACK_LAST_FILENAME) == 0) {
      sharcs_set_lastfile((char *)value);
    } else if (strcmp(name, SIDECAR_CALLBACK_INITIALIZE_SIDECAR) == 0) {
      sharcs_set_sidecar_init((char *)value);
    } else if (strcmp(name, SIDECAR_CALLBACK_FITS_WRITING_EXTENSION) == 0) {
      sharcs_set_fits_writing_extension((char *)value);
    } else if (strcmp(name, SIDECAR_CALLBACK_ESTIMATED_OBSERVATION_TIME) == 0) {
      sharcs_set_observation_time((uint64_t)atoll(value));
    } else if (strcmp(name, "SidecarError") == 0) {
      char combo_status[2000];
      sharcs_set_main_status(name);
      sharcs_set_detailed_status(value);
      snprintf(combo_status, sizeof(combo_status), "%s: %s", name, value);
      sharcs_set_status(combo_status);
    }
  }
  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
  return;
}


/*************************************************************************
 *+
 * Function name: sharcs_start_exposure_time

 * Description: starts a thread that increments progress at a regular rate

 * Inputs: none

 * Outputs: Returns SHARCS error code.

 * Modification History:
      2010/09/01 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_start_exposure_timer(uint64_t totaltime) {

  pthread_t init_thread;
  pthread_attr_t thread_attr;
  struct ExposureTimer *timerStruct;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Starting sharcs_exposure_timer_thread, totaltime=%lld", __FILE__, __LINE__, totaltime);
  if ( pthread_attr_init( &thread_attr ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_init failed (%d) so the exposure timer thread could not be started.",__FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_EXPOSURE_TIMER_THREAD;
  }

  if ( pthread_attr_setdetachstate( &thread_attr, PTHREAD_CREATE_DETACHED ) < 0 ) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_attr_setdetachstate failed (%d) so the exposure timer thread could not be started.", __FILE__, __LINE__, errno );

    return SHARCS_ERROR_START_EXPOSURE_TIMER_THREAD;
  }

  timerStruct = (struct ExposureTimer *)malloc(sizeof(struct ExposureTimer));
  timerStruct->totaltime = totaltime;


  if ( pthread_create(&init_thread, &thread_attr, sharcs_exposure_timer_thread, (void *)timerStruct) < 0) {

    errlog(LOG_ERR, "%s %d: !ERROR! pthread_create failed (%d) so the exposure timer  thread could not be started.", __FILE__, __LINE__, errno );


    return SHARCS_ERROR_START_EXPOSURE_TIMER_THREAD;
  }

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

  return 0;
}

/* todo:  just one thread?  */
void *sharcs_exposure_timer_thread(void *pTimerStruct) {
  long icount;
  struct tms tmsstart, tmsend;
  clock_t start, end;
  double elapsed_time, remaining_time;
  static long clktck=0;
  float percentComplete;
  struct ExposureTimer *timerStruct = (struct ExposureTimer *) pTimerStruct;

  uint64_t totaltime = timerStruct->totaltime;

  free(pTimerStruct);

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: exposure timer thread, total time=%lld micros, period=%d ms, sleeptime=%d micros", __FILE__, __LINE__, totaltime, SHARCS_EXPOSURE_TIMER_UPDATE_PERIOD_MS, SHARCS_EXPOSURE_TIMER_READ_TIME_PERIOD_MICROS);

  /* determin clock ticks per second */
  if (clktck == 0)
    clktck=sysconf(_SC_CLK_TCK);

  /* initialize internal count */
  icount = 0;

  /* get start time */
  start = times(&tmsstart);

  while (isExposing) {
    /* get curent time */
    end = times(&tmsend);

    /* get elasped time in milliseconds */
    elapsed_time = 1000.0 * (double)(end-start)/(double)clktck;
    remaining_time = (totaltime/1000.) - elapsed_time;

    /* if elapsed time is a multiple of the period, update progress counter */
    if (elapsed_time > ((icount+1)*SHARCS_EXPOSURE_TIMER_UPDATE_PERIOD_MS)) {

      percentComplete = elapsed_time/(totaltime/1000.);

      if (percentComplete > 1.0) {
	percentComplete = 1.0;
	sharcs_set_elapsed_time(totaltime/1000000.);
	sharcs_set_remaining_time(0.);
      } else {
	sharcs_set_elapsed_time(elapsed_time/1000.);
	sharcs_set_remaining_time(remaining_time/1000.);
      }

      log_msg(SINFO, LOGLVL_USER4, "%s %d: exposure progress: %f %%", __FILE__, __LINE__, percentComplete*100.);

      sharcs_set_exposure_complete(percentComplete);

      if (exposureProgress >= 1.0) {
	break;
      }

      icount++;

    }
    usleep(SHARCS_EXPOSURE_TIMER_READ_TIME_PERIOD_MICROS);
  }

 log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);
    return (void *) NULL;
}
/*************************************************************************
 *+
 * Function name: sharcs_append_mask_extension

 * Description:

 * Inputs: char *lastfile -- windows path of last written file

 * Outputs: none

 * Modification History:
      2011/03/17 JLW -- Initial.
 *-
*************************************************************************/
int sharcs_append_mask_extension(char *lastfile)
{
  int err=0;
  char *extfile;
  int doAppend=0;
  char statusMsg[300];
  char *extFileCopy;
  char *extFileTok;
  char *extFileFile = NULL;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s.", __FILE__, __LINE__, __func__);


  getIntKeyword("addcsuext", &doAppend);

  if (doAppend == 1) {

    getStringKeyword("csuextname", &extfile);

    if (strcasecmp(extfile, "none") == 0) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Extension filename is %s. Skipping extension appending.", __FILE__, __LINE__, extfile);
    } else {
      log_msg( SINFO, LOGLVL_USER4, "%s %d: Appending %s to %s.", __FILE__, __LINE__, extfile, lastfile);

      /* get filename from ext file path */
      extFileCopy = strdup(extfile);
      extFileTok = strtok(extFileCopy, "/");
      while (extFileTok) {
	extFileFile = extFileTok;
	extFileTok = strtok(NULL, "/");
      }
      free(extFileCopy);

      snprintf(statusMsg, 300, "Appending CSU extension %s", extFileFile);
      sharcs_set_detailed_status(statusMsg);

      /* todo: thread? */
      /* probably not, since takes very short amount of time,
       * and is normally run in the callback thread anyway.
       * also might be good to block callback thread, since
       * imagedone comes immediately after, and it would be
       * good to block image done until this is complete. */
      if ((err = sharcs_fits_appendCSUFitsExtension(lastfile, extfile)) < 0) {
	errlog(SERROR, "%s %d: Error appending CSU extension to science data", __FILE__, __LINE__);
	errlog(SERROR, "%s %d: science file: <%s>, extension file: <%s>", __FILE__, __LINE__, lastfile, extfile);
	errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
	snprintf(statusMsg, 300, "Error Appending CSU extension (%d)", err);
	sharcs_set_detailed_status(statusMsg);
      } else {
	sharcs_set_detailed_status("CSU extension appended successfully");
      }
    }
  } else {
    log_msg(SINFO, LOGLVL_USER1, "%s %d: Not appending CSU extension because addcsuext keyword is set to 0.", __FILE__, __LINE__);
  }

 log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s.", __FILE__, __LINE__, __func__);

 return err;

}

/*************************************************************************
 *+
 * Function name: sharcs_windowsToUnixPath

 * Description:

 * Inputs: char *windowsPath -- windows filename
           char *unitRoot -- Unix directory to replace windows drive

 * Outputs: unix path

 * Modification History:
      2011/03/18 JLW -- Initial.
 *-
*************************************************************************/
char *sharcs_windowsToUnixPath(const char *windowsPath, const char *unixRoot) {
  int colonFound = 0;
  char *unixPath;
  char *unixPathStart;

  unixPath = (char *)calloc(1,sizeof(char) * (strlen(windowsPath) + strlen(unixRoot)+3));

  strcpy(unixPath, unixRoot);
  unixPathStart = unixPath;
  unixPath += strlen(unixRoot)-2;
  while (*windowsPath != '\0') {
    if (colonFound) {
      if (*windowsPath == '\\') {
	*unixPath = '/';
	while (*(windowsPath+1) == '\\') {
	  windowsPath++;
	}
      } else {
	*unixPath = *windowsPath;
      }
      if (*windowsPath == '\0') {
	break;
      }
    } else {
      if (*windowsPath == ':') {
	colonFound=1;
      }
    }

    unixPath++;
    windowsPath++;
  }
  *unixPath = '\0';
  return unixPathStart;
}

/** Remove the data root directory/drive from filename.
 *
 * @param origPath [mod] full path of filename to remove the root from
 * @param origRoot [in] root directory/drive to remove
 *
 * @return -1 if origPath does not start with origRoot
 * @return 0 otherwise
 */
int sharcs_removeRoot(char **origPath, char *origRoot) {
  int ok = 0;
  char *p;

  p = *origPath;

  while (*origRoot != '\0') {
    if (**origPath != *origRoot) {
      *origPath = p;
      return -1;
    }
    (*origPath)++;
    origRoot++;
  }
  return ok;
}
/** Converts between windows and linux paths
 *
 * @param oldpath [in] relative path of filename with root directory removed
 * @param root [in] new root directory
 * @param origDirSep [in] directory separator in original filename
 * @param targetDirSep [in] directory separator in target filename
 *
 * @return full path of converted filename
 */
char *sharcs_convertPath(char *oldpath, char *root, char origDirSep, char targetDirSep) {
  char *p;
  char *out;
  char *save_oldpath;

  save_oldpath = oldpath;
  out = (char *)calloc(1,sizeof(char) * (strlen(oldpath) + strlen(root)+3));

  strcpy(out, root);
  p = out;
  out += strlen(root);
  while (*oldpath != '\0') {
    if (*oldpath == origDirSep) {
      *out = targetDirSep;
    } else {
      *out = *oldpath;
    }
    if (*oldpath == '\0') {
      break;
    }

    out++;
    oldpath++;
  }
  *out = '\0';

  log_msg( SINFO, LOGLVL_USER4, "%s %d: converted oldpath=%s, root=%s to %s",
  		__FILE__, __LINE__, save_oldpath, root, p );

  return p;
}
