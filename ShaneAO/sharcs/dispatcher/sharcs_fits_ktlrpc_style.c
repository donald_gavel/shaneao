/*************************************************************************
 Module:	sharcs_fits.c

 Author:	Jason Weiss

 Date:		2007/10/26

 Description:  	This module contains routines that handle FITS I/O 
*************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#include <syslog.h> 
#include <errno.h>
#include <fcntl.h>
#include <time.h>

#include "fitsio/fitsio.h"

#include "ktl.h"
#include "ktl_keyword.h"

#include "sharcs.h"
#include "sharcs_sidecar.h"
#include "sharcs_fits.h"

#ifdef MR_SERVER
    #include "mr_server/mr_server.h"
#else
    #include "rpcKey.h"
    #include "rpc_if.h"            /* for LOGLVL */
    #include "util_prototypes.h"   /* for log_msg */
#endif


/*
 *  Global variables
 */ 
extern int  Simulate;  
extern char *MyService;

int sharcs_fits_update_wcs_keywords(FITS_HEAD_CARD *cardHead,
				double ra, double dec, double pa);

pthread_mutex_t fitsfile_mutex = PTHREAD_MUTEX_INITIALIZER;

FITS_HEAD_CARD *headerKeyList = NULL;

void *sharcs_fits_header(void *pExposure_params) {
  int err=0;
  int errcode=0;
  double ra, dec, pa = -999;

  struct ExposureConfiguration *exp = (void *) pExposure_params;

  int cardEntry = 0;
  FITS_HEAD_CARD *card = NULL;
  int keywordsFound=0;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  /* JLW? initialize card */
  card = NULL;

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Assembling FITS header...", __FILE__, __LINE__);

  if ( headerKeyList == NULL ) {
    log_msg(SINFO, LOGLVL_USER4, "%s %d: No FITS header key list.  Creating one.", __FILE__, __LINE__);
    /* 
     * This will only occur the first time through, however, if the
     * thread, which obtains the keywords, requires an additional 'card'
     * then it will allocate that and chain the cards together.
     */
    headerKeyList = (FITS_HEAD_CARD *) calloc( 1, sizeof(FITS_HEAD_CARD));
    log_msg(SINFO, LOGLVL_USER1, "%s %d: Created FITS header key list %x.", __FILE__, __LINE__, headerKeyList);

    headerKeyList->next = NULL;
    headerKeyList->keyCount = 0;
  }

  /* construct default FITS header */
  
  /* append detector server keywords */
  log_msg(SINFO, LOGLVL_USER4, "%s %d: calling sharcs_fits_update_header for %s with card %x", __FILE__, __LINE__, MyService, headerKeyList );
 
  if ((err=sharcs_fits_update_header(headerKeyList, &cardEntry, &card, MyService)) <0){
    errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
    errcode=SHARCS_FITS_ERROR_GETTING_LOCAL_FITS_KEYWORDS;
  } else {
    keywordsFound++;
  }

  /* get instrument keywords */
  if (exp->getMOSFIREkw == 1) {
  /* do a oneshot to the global server */
    log_msg(SINFO, LOGLVL_USER4, "%s %d: calling sharcs_fits_update_header for %s with card %x", __FILE__, __LINE__, MGS_SERVICE_NAME, card);
 
    /* JLW? set card to front of headerKeyList if it hasn't been initialized */
    if (card == NULL) {
      card = headerKeyList;
    }

    if ((err=sharcs_fits_update_header(card, &cardEntry, &card, MGS_SERVICE_NAME)) <0){
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
      errcode=SHARCS_FITS_ERROR_GETTING_MOSFIRE_FITS_KEYWORDS;
    } else {
      log_msg(SINFO, LOGLVL_USER1, "%s %d: MOSFIRE FITS header keywords retrieved.", __FILE__, __LINE__);
      keywordsFound++;
    }
  }

  /* get DCS keywords */
  if (exp->getDCSkw == 1) {
    /* do a oneshot to DCS */
    log_msg(SINFO, LOGLVL_USER4, "%s %d: calling sharcs_fits_update_header for %s with card %x", __FILE__, __LINE__, DCS_SERVICE_NAME, card);
 
    if (card == NULL) {
      card = headerKeyList;
    }
    FITS_HEAD_CARD *dcsStartCard = card;

    if ((err=sharcs_fits_update_header(card, &cardEntry, &card, DCS_SERVICE_NAME)) <0){
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
      errcode=SHARCS_FITS_ERROR_GETTING_DCS_FITS_KEYWORDS;
    } else {
      log_msg(SINFO, LOGLVL_USER1, "%s %d: DCS FITS header keywords retrieved.", __FILE__, __LINE__);
      keywordsFound++;
    }
    if ((err = sharcs_fits_get_dcs_info(dcsStartCard, &ra, &dec, &pa)) < 0) {
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
    } else {
      if ((err = sharcs_fits_update_wcs_keywords(headerKeyList, ra, dec, pa)) < 0) {
	errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
      }
    }
  }

  /* get ACS keywords */
  if  (exp->getACSkw == 1) {
    /* do a oneshot to ACS */
    log_msg(SINFO, LOGLVL_USER4, "%s %d: calling sharcs_fits_update_header for %s with card %x", __FILE__, __LINE__, ACS_SERVICE_NAME, card);
 
    if (card == NULL) {
      card = headerKeyList;
    }

    if ((err=sharcs_fits_update_header(card, &cardEntry, &card, ACS_SERVICE_NAME)) <0){
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
      errcode=SHARCS_FITS_ERROR_GETTING_ACS_FITS_KEYWORDS;
    } else {
      log_msg(SINFO, LOGLVL_USER1, "%s %d: ACS FITS header keywords retrieved.", __FILE__, __LINE__);
      keywordsFound++;
    }
  } 

  if ( (keywordsFound > 0) && (!exp->headerTest) ) {
    log_msg(SINFO, LOGLVL_USER1, "%s %d: %s", __FILE__, __LINE__, "FITS header assembled.");
    /* do something with header */
    if ((err = sharcs_fits_write_header(exp->fitsFilename, headerKeyList)) < 0) {
      errlog(SERROR, "%s %d: error writing FITS header", __FILE__, __LINE__);
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-err]);
    } else {
      log_msg(SINFO, LOGLVL_USER1, "%s %d: FITS header written (thread exiting): %d.", __FILE__, __LINE__, err);
    }
  }

  free(pExposure_params);
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  pthread_exit((void *) ((long) errcode));
}


int sharcs_fits_add_header_card ( FITS_HEAD_CARD *card )
{
  FITS_HEAD_CARD *newCard;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  if ( card == NULL ) {
    errlog(SERROR, "%s %d: Attempted to append to a null header card!", __FILE__, __LINE__);
    return SHARCS_FITS_ERROR_RETRIEVING_KEYWORDS;
  }

  if (card->next == NULL ) {

    newCard =  (FITS_HEAD_CARD *) calloc( 1, sizeof(FITS_HEAD_CARD));
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Created another FITS header keyword card %x.",newCard);

    newCard->next = NULL;
    newCard->keyCount = 0;
    card->next = newCard;
  } 

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return 0;
}

int sharcs_fits_write_header( char *fitsFilename, FITS_HEAD_CARD *firstCard)
{
  int status = 0;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  if ( firstCard == NULL ) {
  
    errlog( SERROR, "%s %d: FITS header is NULL! No keywords will exist.", __FILE__, __LINE__ );

    return SHARCS_FITS_ERROR_RETRIEVING_KEYWORDS;
  }

  log_msg(SINFO, LOGLVL_USER4, "%s %d: adding cards to FITS header.", __FILE__, __LINE__);

  /* have sidecar code send header across ICE link to current exposure */
  /*
  */
  if ((status = sharcs_sidecar_addFitsHeader(fitsFilename, firstCard)) < 0) {
    errlog(SERROR, "%s %d: error adding cards to FITS header.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-status]);
    return SHARCS_FITS_ERROR_SIDECAR_ERROR;
  } else {

    log_msg(SINFO, LOGLVL_USER4, "%s %d: FITS cards successfully added to header.", __FILE__, __LINE__);
  }

  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return status; 
}
 
int sharcs_fits_update_header(FITS_HEAD_CARD *start_card, int *currEntry, FITS_HEAD_CARD **end_card, char *service) {

  KTL_HANDLE *khand;
  int num_keywords=0;
  char **keywords=NULL, **remarks=NULL;
  KTL_POLYMORPH *values=NULL;
  KTL_DATATYPE *types=NULL;

  int ii;
//  void *value=NULL;
  int status=0;
  int filler_size;
  char comment_filler[] = 
    "*****************************************************************";

  FITS_HEAD_CARD *card = start_card;

  int currKey = *currEntry;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);
  log_msg(SINFO, LOGLVL_USER4, "%s %d: Trying to open service %s.", __FILE__, __LINE__, service);

  if ((ktl_open(service, "keyword", 0, &khand))< 0) {

    errlog(SERROR, "%s %d: Open of service %s failed.", __FILE__, __LINE__, service);
    errlog(SERROR, "%s %d: ktl_get_errtxt = <%s>", __FILE__, __LINE__, ktl_get_errtxt());
    printf("error opening keyword library: <%s>\n", ktl_get_errtxt());
    return SHARCS_FITS_ERROR_OPENING_SERVICE;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: Service %s opened.", __FILE__, __LINE__, service);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Performing ktl_ioctl on KTL_HEADERS to get keywords from service %s.", __FILE__, __LINE__, service);

  if (ktl_ioctl(khand, KTL_HEADERS, &num_keywords, &keywords, &types, &values, &remarks) < 0 ) {

    errlog(SERROR, "%s %d: Headers for %s failed.", __FILE__, __LINE__, service);

    /* JLW? close ktl connection */
    ktl_close(khand);
    /*    printf("error getting keywords\n"); */
    return SHARCS_FITS_ERROR_RETRIEVING_KEYWORDS;
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: Service %s returned %d keywords.", __FILE__, __LINE__, service, num_keywords);

  ktl_close(khand);
   
  if ( card == NULL ) {

    errlog(SERROR, "%s %d: Error getting keywords (header card is NULL).", __FILE__, __LINE__);
    return SHARCS_FITS_ERROR_RETRIEVING_KEYWORDS;
  }
 
  if ( currKey > (KEYS_PER_CARD - 1) ) {

    log_msg(SINFO, LOGLVL_USER4, "%s %d: This header card is full.  Going to next one.", __FILE__, __LINE__);
    sharcs_fits_add_header_card( card );

    card->keyCount = KEYS_PER_CARD;

    card = card->next;
    currKey = 0;
  }
 
  log_msg(SINFO, LOGLVL_USER1, "%s %d: New header card created.", __FILE__, __LINE__);
 
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Creating comment key for separator.", __FILE__, __LINE__);

  /* format separator comment */
  filler_size=(MAX_COMMENT_LENGTH-(strlen(service)+19))/2;
 
  comment_filler[filler_size]='\0';

/* 
  snprintf(comment, MAX_COMMENT_LENGTH, "<%s - %s Keywords - %s>", comment_filler, service, comment_filler); 
*/

  card->keys[currKey].comment = 1;
  card->keys[currKey].type = KTL_STRING;
  sprintf(card->keys[currKey].name, "%s", "");  
  sprintf(card->keys[currKey].remark, "%s", "");
  snprintf(card->keys[currKey].strVal, MAX_COMMENT_LENGTH, "<%s - %s Keywords - %s>", comment_filler, service, comment_filler);
  currKey++;

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Adding keywords to card.", __FILE__, __LINE__);

  /* do keywords in reverse order, since they are gotten 
     in reverse order (for some reason). */
  /* 
   * 03/22/2011 A. Honey
   * The order of the keywords, in the RPC keyword services, is determined 
   * by the order they exist in the pertinent keyword config file, in reverse. 
   * This is because the information for each keyword is essentially retained 
   * in a linked list and are inserted at the front of that list, as each line 
   * in the config file is processed. This technique is employed for efficiency 
   * whilst parsing the file. An application programmer could order the config
   * file to get the effect they desire.
   */
  for (ii=num_keywords-1; ii>=0; ii--) {
    log_msg(SINFO, LOGLVL_USER4, "%s %d: FITS Header Keyword: %s <%s>", __FILE__, __LINE__, keywords[ii], remarks[ii]);
    /*
     *  Get each keyword field
     */
    card->keys[currKey].type = types[ii];
    switch ( types[ii] ) {
    /*
     * 03/21/2011 A.Honey
     * Added BOOLEAN, ENUM, and MASK as all their corresponding values 
     * should be integer. In the case where a service returns strings for:
     * ENUM, ENUMM, MASK, or BOOLEAN, the service is expected to return
     * KTL_STRING as the keywords' types. Such is the case for CA 
     * (within cake.c) for DCS and AO keywords.
     */
    case KTL_INT:
    case KTL_BOOLEAN:
    case KTL_ENUM:
    case KTL_ENUMM:
    case KTL_MASK:
//      value=&values[ii].i;

      card->keys[currKey].value.i = values[ii].i;
      break;
    case KTL_DOUBLE:
//      value=&values[ii].d;

      card->keys[currKey].value.d = values[ii].d;
      break;
    case KTL_FLOAT:
//      value=&values[ii].f;

      card->keys[currKey].value.f = values[ii].f;
      break;

    case KTL_STRING:
//      value=(void *)values[ii].s;

      if ( values[ii].s == NULL )
        sprintf(card->keys[currKey].strVal,"%s", " ");
      else if ( strlen( values[ii].s ) == 0 )
        sprintf(card->keys[currKey].strVal,"%s", " ");
      else
        snprintf(card->keys[currKey].strVal, MAX_KEY_STRING, "%s", values[ii].s);
      break;
    default:
      /* if not one of these datatype, skip it */
      continue;
    } 
   
    card->keys[currKey].comment = 0;
    snprintf(card->keys[currKey].name, KEY_NAME_LEN, "%s", keywords[ii]);
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Added keyword %s to card %x", __FILE__, __LINE__, card->keys[currKey].name, card);
    snprintf(card->keys[currKey].remark, MAX_COMMENT_LENGTH, "%s", remarks[ii]);

    currKey++;

    if ( currKey > (KEYS_PER_CARD - 1) ) {

      log_msg(SINFO, LOGLVL_USER4, "%s %d: Card full, going to next header card", __FILE__, __LINE__);
      sharcs_fits_add_header_card( card );

      card->keyCount = KEYS_PER_CARD;

      card = card->next;
      currKey = 0;
    }
    
    if (status)
      errlog(SERROR, "%s %d: Error setting keyword %s in header", __FILE__, __LINE__, keywords[ii]);
      
    if ( types[ii] == KTL_STRING) free( values[ii].s );
    free( remarks[ii] );
    free( keywords[ii] );
  }

  log_msg(SINFO, LOGLVL_USER1, "%s %d: All header keys added to card.", __FILE__, __LINE__);


  card->keyCount = currKey;

  *currEntry = currKey;
  *end_card = card;

  free(keywords);
  free(remarks);
  free(values);
  free(types);
    
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return 0;
}
int sharcs_fits_update_wcs_keywords(FITS_HEAD_CARD *cardHead, double ra, double dec, double pa) {
  int err=0;
  int ii,jj;
  FITS_HEAD_CARD *card;
  KEY_INFO *currentKey;
  int numKeys, numCards, keyCount;
  int crval1Found=0;
  int crval2Found=0;
  int cd1_1Found=0;
  int cd1_2Found=0;
  int cd2_2Found=0;
  int cd2_1Found=0;
  float pscale;
  double paRad = pa * M_PI / 180.;

  getFloatKeyword("pscale", &pscale);

  pscale /= 3600.;

  log_msg(SINFO, LOGLVL_USER1, "%s %d: update wcs: ra=%f, dec=%f, pa=%f (%f rad), pscale=%f", __FILE__, __LINE__, ra, dec, pa, paRad, pscale);

  card = cardHead;
  numKeys=0;
  numCards=0;
  while (card != NULL) {
    numKeys += card->keyCount;
    numCards++;
    card = card->next;
  }

  card = cardHead;
  for (ii=0; ii<numCards; ii++) {
    if (card == NULL) {
      errlog(SERROR, "%s %d: Error updating WCS keywords.  Card is null.", __FILE__, __LINE__);
      return SHARCS_FITS_ERROR_NULL_CARD;
    }

    keyCount = (int)(card->keyCount);
    for (jj=0; jj<keyCount; jj++) {
      currentKey = &(card->keys[jj]);

      if (strcasecmp(currentKey->name, "crval1") == 0) {
	currentKey->value.d = ra;
	crval1Found=1;
      } else if (strcasecmp(currentKey->name, "crval2") == 0) {
	currentKey->value.d = dec;
	crval2Found=1;
      } else if (strcasecmp(currentKey->name, "cd1_1") == 0) {
	currentKey->value.d = pscale * (-1 * cos(paRad));
	cd1_1Found=1;
      } else if (strcasecmp(currentKey->name, "cd1_2") == 0) {
	currentKey->value.d = pscale * (sin(paRad));
	cd1_2Found=1;
      } else if (strcasecmp(currentKey->name, "cd2_1") == 0) {
	currentKey->value.d = pscale * (sin(paRad));
	cd2_1Found=1;
      } else if (strcasecmp(currentKey->name, "cd2_2") == 0) {
	currentKey->value.d = pscale * (cos(paRad));
	cd2_2Found=1;
      }
      if (crval1Found && crval2Found && cd1_1Found && cd1_2Found && cd2_1Found && cd2_2Found) {
	break;
      }
    }
    if (crval1Found && crval2Found && cd1_1Found && cd1_2Found && cd2_1Found && cd2_2Found) {
      break;
    }
    card = card->next;
  }

  if (crval1Found == 0) {
    errlog(SERROR, "%s %d: crval1 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (crval2Found == 0) {
    errlog(SERROR, "%s %d: crval2 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (cd1_1Found == 0) {
    errlog(SERROR, "%s %d: cd1_1 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (cd2_1Found == 0) {
    errlog(SERROR, "%s %d: cd2_1 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (cd1_2Found == 0) {
    errlog(SERROR, "%s %d: cd1_2 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (cd2_2Found == 0) {
    errlog(SERROR, "%s %d: cd2_2 keyword not found in SHARCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;

}

int sharcs_fits_get_dcs_info(FITS_HEAD_CARD *card, double *ra, double *dec, double *pa) {
  int err=0;
  int ii,jj;
  FITS_HEAD_CARD *firstCard;
  KEY_INFO *currentKey;
  int numKeys, numCards, keyCount;
  int raFound=0;
  int decFound=0;
  int rotposnFound=0;
  int rotpposnFound=0;
  int rotmodeFound=0;
  double rotposn = 0.0;		/* Initialize these to shut up gcc. */
  double rotpposn = 0.0;
  char *rotmode = "";

  firstCard = card;
  numKeys=0;
  numCards=0;
  while (card != NULL) {
    numKeys += card->keyCount;
    numCards++;
    card = card->next;
  }

  card = firstCard;
  for (ii=0; ii<numCards; ii++) {
    if (card == NULL) {
      errlog(SERROR, "%s %d: Error parsing DCS keywords.  Card is null.", __FILE__, __LINE__);
      return SHARCS_FITS_ERROR_NULL_CARD;
    }

    keyCount = (int)(card->keyCount);
    for (jj=0; jj<keyCount; jj++) {
      currentKey = &(card->keys[jj]);

      /*      log_msg( SINFO, LOGLVL_USER4, "%s %d: get_dcs_info: currentKey=%s.", __FILE__, __LINE__, currentKey->name); */
      if (strcasecmp(currentKey->name, "ra") == 0) {
	*ra = currentKey->value.d;
	raFound=1;
	log_msg(SINFO, LOGLVL_USER4, "%s %d: found dcs.ra=%f", __FILE__, __LINE__, currentKey->value.d);
      } else if (strcasecmp(currentKey->name, "dec") == 0) {
	*dec = currentKey->value.d;
	decFound=1;
	log_msg(SINFO, LOGLVL_USER4, "%s %d: found dcs.dec=%f", __FILE__, __LINE__, currentKey->value.d);
      } else if (strcasecmp(currentKey->name, "rotposn") == 0) {
	rotposn = currentKey->value.d;
	rotposnFound=1;
	log_msg(SINFO, LOGLVL_USER4, "%s %d: found dcs.rotposn=%f", __FILE__, __LINE__, currentKey->value.d);
      } else if (strcasecmp(currentKey->name, "rotpposn") == 0) {
	rotpposn = currentKey->value.d;
	rotpposnFound=1;
	log_msg(SINFO, LOGLVL_USER4, "%s %d: found dcs.rotpposn=%f", __FILE__, __LINE__, currentKey->value.d);
      } else if (strcasecmp(currentKey->name, "rotmode") == 0) {
	rotmode = currentKey->strVal;
	rotmodeFound=1;
	log_msg(SINFO, LOGLVL_USER4, "%s %d: found dcs.rotmode=%s", __FILE__, __LINE__, currentKey->strVal);
      }

      if (raFound && decFound && rotposnFound && rotpposnFound && rotmodeFound) {
	break;
      }
    }
    if (raFound && decFound && rotposnFound && rotpposnFound && rotmodeFound) {
      break;
    }
    card = card->next;
  }

  if (raFound == 0) {
    errlog(SERROR, "%s %d: RA keyword not found in DCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  } 
  if (decFound == 0) {
    errlog(SERROR, "%s %d: RA keyword not found in DCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  }
  if (rotposnFound == 0) {
    errlog(SERROR, "%s %d: rotposn keyword not found in DCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  }
  if (rotpposnFound == 0) {
    errlog(SERROR, "%s %d: rotpposn keyword not found in DCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  }
  if (rotmodeFound == 0) {
    errlog(SERROR, "%s %d: rotmode keyword not found in DCS header.", __FILE__, __LINE__);
    err = SHARCS_FITS_ERROR_MISSING_KEYWORD;
  }

  if (err == 0) {
    if (strcasecmp(rotmode, "position angle") == 0) {
      *pa = rotposn;
    } else {
      *pa = rotpposn;
    }
  }
  
  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
  
}
  
int sharcs_fits_appendCSUFitsExtension(char *scienceFilename, char *extensionFilename) {
  int err=0;
  int fits_status=0;
  int hduType;
  fitsfile *extFitsfile;
  fitsfile *dataFitsfile;

  log_msg(SINFO, LOGLVL_USER3, "%s %d: entering %s.", __FILE__, __LINE__, __FUNCTION__);

  log_msg(SINFO, LOGLVL_USER4, "%s %d: Opening science file <%s> to append CSU Mask description FITS Extension.", __FILE__, __LINE__, scienceFilename);
  fits_open_file(&dataFitsfile, scienceFilename, READWRITE, &fits_status);

  if (fits_status) {
    errlog(SERROR, "%s %d: Error opening %s: %d", __FILE__, __LINE__, scienceFilename, fits_status);
    err = SHARCS_FITS_ERROR_OPENING_SCIENCE_FILE;
  } else {
  
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Opening CSU Mask description FITS Extension <%s>.", __FILE__, __LINE__, extensionFilename);
    fits_open_file(&extFitsfile, extensionFilename, READONLY, &fits_status);

    if (fits_status) {
      errlog(SERROR, "%s %d: Error opening %s: %d", __FILE__, __LINE__, extensionFilename, fits_status);
      err = SHARCS_FITS_ERROR_OPENING_EXT_FILE;
    } else {

      log_msg(SINFO, LOGLVL_USER4, "%s %d: Moving to second HDU (first table) in extension file.", __FILE__, __LINE__);
      fits_movabs_hdu(extFitsfile, 2, &hduType, &fits_status);
      
      if (fits_status) {
	errlog(SERROR, "%s %d: Error moving to first HDU: %d", __FILE__, __LINE__, fits_status);
	err = SHARCS_FITS_ERROR_MOVING_EXT_FILE_HDU;
      } else {

	log_msg(SINFO, LOGLVL_USER4, "%s %d: Copying current and all followings HDUs from extension file to science file.", __FILE__, __LINE__);
	fits_copy_file(extFitsfile, dataFitsfile, 0, 1, 1, &fits_status);

	if (fits_status) {
	  errlog(SERROR, "%s %d: Error copying hdus: %d", __FILE__, __LINE__, fits_status);
	  err = SHARCS_FITS_ERROR_COPYING_CSU_EXT;
	}
      }
      log_msg(SINFO, LOGLVL_USER4, "%s %d: Closing extension file.", __FILE__, __LINE__);
      fits_status=0;
      fits_close_file(extFitsfile, &fits_status);
      
      
      if (fits_status) {
	errlog(SERROR, "%s %d: Error closing extension file %d", __FILE__, __LINE__, fits_status);
	if (err == 0) {
	  err = SHARCS_FITS_ERROR_CLOSING_EXT_FILE;
	}
      }
    }
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Closing science file.", __FILE__, __LINE__);
    fits_status=0;
    fits_close_file(dataFitsfile, &fits_status);
  

    if (fits_status) {
      errlog(SERROR, "%s %d: Error closing science file %d", __FILE__, __LINE__, fits_status);
      if (err == 0) {
	err = SHARCS_FITS_ERROR_CLOSING_SCIENCE_FILE;
      }
    }
  }

  if (err) {
    log_msg(SINFO, LOGLVL_USER1, "%s %d: CSU Mask description FITS extensions could NOT be copied to science file.", __FILE__, __LINE__);
  } else {
    log_msg(SINFO, LOGLVL_USER1, "%s %d: CSU Mask description FITS extensions copied to science file.", __FILE__, __LINE__);
  }


  log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.", __FILE__, __LINE__, __FUNCTION__);

  return err;
}
