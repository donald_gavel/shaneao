#ifndef SHARCS_SIDECAR_H
#define SHARCS_SIDECAR_H

#include "sharcs_fits.h"
#include "../masICE/Sidecar.h"

#define SIDECAR_SUCCESSFUL_RESULT                       "Success!"
#define SIDECAR_SUCCESS_OPEN_APPLICATION     "Application opened."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_FIRMWARE \
                            "Download of jade firmware succeeded."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS \
                            "Download of jade registers complete."
#define SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE \
                            "Download MCD Succeeded"

/* writing to Jade registers returns "Success!" when successful
   writing to ASIC registers returns 
   "Write to asic register @<XXXX> successful." when successful, where
   <XXXX> is the address in hex (w/o preceding 0x)
*/
#define SIDECAR_SUCCESS_WRITE_ASIC_REGISTER          "successful"
#define SIDECAR_SUCCESS_WRITE_JADE_REGISTER            "Success!"


#define SIDECAR_PAUSE_AFTER_DATA_ON_ACK                 2000000

#define FILENAME_MAX_LENGTH 256

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif


#define SIDECAR_DEFAULT_JADE_NAME                       "JADE2"
#define SIDECAR_DEFAULT_ASIC_NAME                       "ASIC1"

#define SIDECAR_DEFAULT_APPLICATION_FILENAME \
  "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\Slow Mode.soln"

#define SIDECAR_DEFAULT_H2RG_MCD_FILENAME \
  "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\H2RG_DevBrd_Warm.mcd"

#define SIDECAR_TEST_PATTERN_MCD_FILENAME \
  "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\TestPattern2.mcd"


#define SIDECAR_ERROR_CODE0                                           -7500
#define SIDECAR_ERROR_CONNECT_TO_SERVER           SIDECAR_ERROR_CODE0 -  1
#define SIDECAR_ERROR_INITIALIZE_SIDECAR          SIDECAR_ERROR_CODE0 -  2
#define SIDECAR_ERROR_JADE_NOT_AVAILABLE          SIDECAR_ERROR_CODE0 -  3
#define SIDECAR_ERROR_READ_SIDECAR_CONFIGURATION  SIDECAR_ERROR_CODE0 -  4
#define SIDECAR_ERROR_DOWNLOAD_JADE_REGISTERS     SIDECAR_ERROR_CODE0 -  5
#define SIDECAR_ERROR_ASIC_NOT_AVAILABLE          SIDECAR_ERROR_CODE0 -  6
#define SIDECAR_ERROR_DOWNLOAD_MCD_FILE           SIDECAR_ERROR_CODE0 -  7
#define SIDECAR_ERROR_NON_POSITIVE_READ_TIME      SIDECAR_ERROR_CODE0 -  8
#define SIDECAR_ERROR_ABORT_EXPOSURE              SIDECAR_ERROR_CODE0 -  9
#define SIDECAR_ERROR_SET_DATA_ON_ACK             SIDECAR_ERROR_CODE0 - 10
#define SIDECAR_ERROR_RESET_ASIC                  SIDECAR_ERROR_CODE0 - 11
#define SIDECAR_ERROR_POWER_DOWN_ASIC             SIDECAR_ERROR_CODE0 - 12
#define SIDECAR_ERROR_ADD_FITS_HEADER             SIDECAR_ERROR_CODE0 - 13
#define SIDECAR_ERROR_FITS_HEADER_NULL_CARD       SIDECAR_ERROR_CODE0 - 14
#define SIDECAR_ERROR_FITS_HEADER_INVALID_DATATYPE SIDECAR_ERROR_CODE0 - 15
#define SIDECAR_ERROR_TAKE_EXPOSURE               SIDECAR_ERROR_CODE0 -  16
#define SIDECAR_ERROR_SET_EXPOSURE_EXTENTS        SIDECAR_ERROR_CODE0 -  17
#define SIDECAR_ERROR_SET_REFERENCE_PIXEL_SUBTRACTION_TYPE  SIDECAR_ERROR_CODE0 - 18
#define SIDECAR_ERROR_WRITE_RAW_FRAMES            SIDECAR_ERROR_CODE0 - 19
#define SIDECAR_ERROR_GET_POWER_STATUS            SIDECAR_ERROR_CODE0 - 20
#define SIDECAR_ERROR_ICE_ERROR                   SIDECAR_ERROR_CODE0 - 21
#define SIDECAR_ERROR_ECHO_MISMATCH               SIDECAR_ERROR_CODE0 - 22
#define SIDECAR_ERROR_CANNOT_BROADCAST            SIDECAR_ERROR_CODE0 - 23


#ifdef CREATOR
char *SidecarErrMsg[] = {
  "Sidecar Error (00): Generic Sidecar error.",
  "Sidecar Error (01): Error connecting to server.",
  "Sidecar Error (02): Error initializing SIDECAR.",
  "Sidecar Error (03): Jade is not available.",
  "Sidecar Error (04): SIDECAR server error reading configuration file.",
  "Sidecar Error (05): Error downloading jade registers.",
  "Sidecar Error (06): ASIC is not available.",
  "Sidecar Error (07): Error downloading timing application.",
  "Sidecar Error (08): Read time is non-positive.  Initialize.",
  "Sidecar Error (09): Error aborting exposure.",
  "Sidecar Error (10): Error setting data on ack.",
  "Sidecar Error (11): Error resetting ASIC.",
  "Sidecar Error (12): Error powering down ASIC.",
  "Sidecar Error (13): Error adding FITS Header.",
  "Sidecar Error (14): Error assembling FITS Header. Unexpected null card.",
  "Sidecar Error (15): Error assembling FITS Header. invalid KTL datatype.",
  "Sidecar Error (16): Error taking exposure.",
  "Sidecar Error (17): Error setting exposure extents.",
  "Sidecar Error (18): Error setting reference pixel subtraction type.", 
  "Sidecar Error (19): Error enabling/disabling writing of all frames.", 
  "Sidecar Error (20): Error getting SIDECAR power status.", 
  "Sidecar Error (21): Error communicating to ICE server.  Check server.", 
  "Sidecar Error (22): Error communicating to ICE server.  Echo mismatch", 
  "Sidecar Error (23): Value cannot be broadcasted.", 
  NULL
};
#else
extern char *SidecarErrMsg[];
#endif

int sharcs_sidecar_open_driver(const char* host, unsigned int port);
void sharcs_sidecar_addCallbackListener(struct ValueListener *l);
void sharcs_sidecar_server_shutdown();
int sharcs_sidecar_validate_fileanme(char *fullfile);
int sharcs_sidecar_get_ignored_reads();
int sharcs_sidecar_init(char *configuration, int downloadFirmware, int force);
int sharcs_sidecar_get_read_time(int *readTime);
int sharcs_sidecar_download_mcd_file(const char* mcdFile);
int sharcs_sidecar_power_down();
int sharcs_sidecar_abort_exposure(int abortLevel);
int sharcs_sidecar_setExposureExtents(int startx, int starty, int endx, int endy);
int sharcs_sidecar_takeExposure(struct ExposureConfiguration config);
int sharcs_sidecar_setReferencePixelSubtractionType(int type);
int sharcs_sidecar_writeRawFrames(int enable);
int sharcs_sidecar_getSidecarPower(int *status);
int sharcs_sidecar_addFitsHeader(char *fitsFilename, FITS_HEAD_CARD *firstCard);
int sharcs_sidecar_addFitsHeader_KeyBased(char *fitsFilename, struct FitsHeaderKey *keys, int numKeys);
int sharcs_sidecar_write_asic_register( const char *address, const char *value);
int sharcs_sidecar_get_heartbeat_period();
int sharcs_sidecar_get_exposing();
int sharcs_sidecar_get_fits_writing_extension();
#endif  /* SHARCS_SIDECAR_H */
