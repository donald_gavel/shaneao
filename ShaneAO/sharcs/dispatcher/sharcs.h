
/*
 * %W% %E% %U%;
 */
#ifndef _SHARCS_H
#define _SHARCS_H

#include <pthread.h>
#include <inttypes.h> /* uint64_t */

#define OUTDIR_LENGTH       256
#define FILENAME_LENGTH     256
#define EXTENSION_LENGTH    8
#define TEMP_EXTENSION_LENGTH 16
#define SHARCS_FILENAME_LENGTH OUTDIR_LENGTH+FILENAME_LENGTH+EXTENSION_LENGTH+TEMP_EXTENSION_LENGTH+3
#define TIMFILE_LENGTH      OUTDIR_LENGTH+FILENAME_LENGTH+EXTENSION_LENGTH+3

/* detector status values */
#define SHARCS_STATUS_RESET            "Reset"
#define SHARCS_STATUS_IDLING           "Idling"
#define SHARCS_STATUS_ERROR            "Error"
#define SHARCS_STATUS_INITIALIZING     "Initializing hardware"
#define SHARCS_STATUS_RESUMING         "Attemping resume"
#define SHARCS_STATUS_POWERED_DOWN     "Powered down"

#define SHARCS_DETAILED_STATUS_READY   "Ready to take exposures"
#define SHARCS_DETAILED_STATUS_INITIALIZING_SIDECAR  "Initializing SIDECAR"
#define SHARCS_DETAILED_STATUS_DOWNLOADING_MCD_FILE "Downloading timing file"
#define SHARCS_DETAILED_STATUS_GETTING_IGNORED_READS "Getting number of ignored reads"
#define SHARCS_DETAILED_STATUS_GETTING_READ_TIME  "Getting frame read time"
#define SHARCS_DETAILED_STATUS_ERROR_GETTING_READ_TIME "Error getting frame read time"
#define SHARCS_DETAILED_STATUS_ERROR_GETTING_IGNORED_READS "Error getting number of ignored reads"
#define SHARCS_DETAILED_STATUS_ERROR_DOWNLOADING_MCD_FILE "Error downloading timing file"
#define SHARCS_DETAILED_STATUS_ERROR_INITIALIZING_SIDECAR "Error initializing SIDECAR"
#define SHARCS_DETAILED_STATUS_POWERED_DOWN "Initialize hardware to take exposures again"
#define SHARCS_DETAILED_STATUS_DISCONNECTED "Lost communications to SIDECAR Server"
#define SHARCS_DETAILED_STATUS_CONNECTED "Connected to SIDECAR Server"

#define SHARCS_CONNECTED_TRUE          1
#define SHARCS_CONNECTED_FALSE         0

#define SHARCS_INITING_TRUE          1
#define SHARCS_INITING_FALSE         0

#define SHARCS_SAMPMODE_SINGLE          1
#define SHARCS_SAMPMODE_CDS             2
#define SHARCS_SAMPMODE_MCDS            3
#define SHARCS_SAMPMODE_UTR             4

#define SHARCS_IMAGE_TYPE_ABORT_TEST   -2
#define SHARCS_IMAGE_TYPE_ABORT_GO     -1
#define SHARCS_IMAGE_TYPE_GO            1
#define SHARCS_IMAGE_TYPE_TEST          2

#define SHARCS_IMAGE_DONE               1
#define SHARCS_IMAGE_DONE_RESET         0

#define SHARCS_READY_YES   1
#define SHARCS_READY_NO    0

#define SHARCS_TRUE        1
#define SHARCS_FALSE       0

#define SHARCS_EXPOSURE_TIMER_UPDATE_PERIOD_MS            1000
#define SHARCS_EXPOSURE_TIMER_READ_TIME_PERIOD_MICROS   200000

#define SHARCS_CLOCK_OFFSET_ALLOWANCE_SECONDS 2

#define SHARCS_FILECHECK_INVALID_DATAROOT                   -5
#define SHARCS_FILECHECK_FILE_EXISTS_READ_ONLY              -4
#define SHARCS_FILECHECK_DIRECTORY_NOT_WRITEABLE            -3
#define SHARCS_FILECHECK_DIRECTORY_DOESNT_EXIST             -2
#define SHARCS_FILECHECK_INVALID_UNKNOWN                    -1
#define SHARCS_FILECHECK_OK                                  0



/*
 *  SHARCS Error code 
 */
#define SHARCS_ERROR_CODE0                                       -7000
#define SHARCS_ERROR_GENERIC                     SHARCS_ERROR_CODE0 -  1
#define SHARCS_ERROR_KEYWORD_LOCKED              SHARCS_ERROR_CODE0 -  2
#define SHARCS_ERROR_OPEN_DRIVER                 SHARCS_ERROR_CODE0 -  3
#define SHARCS_ERROR_HARDWARE_INIT               SHARCS_ERROR_CODE0 -  4
#define SHARCS_ERROR_DOWNLOAD_MCD_FILE           SHARCS_ERROR_CODE0 -  5
#define SHARCS_ERROR_ABORT_EXPOSURE              SHARCS_ERROR_CODE0 -  6
#define SHARCS_ERROR_INIT_DURING_EXPOSURE        SHARCS_ERROR_CODE0 -  7
#define SHARCS_ERROR_RESUME_DURING_EXPOSURE      SHARCS_ERROR_CODE0 -  8
#define SHARCS_ERROR_CONNECT_DURING_EXPOSURE     SHARCS_ERROR_CODE0 -  9
#define SHARCS_ERROR_INIT_DURING_INIT            SHARCS_ERROR_CODE0 - 10
#define SHARCS_ERROR_RESUME_DURING_INIT          SHARCS_ERROR_CODE0 - 11
#define SHARCS_ERROR_CONNECT_DURING_INIT         SHARCS_ERROR_CODE0 - 12
#define SHARCS_ERROR_NOT_CONNECTED               SHARCS_ERROR_CODE0 - 13
#define SHARCS_ERROR_NOT_IDLING                  SHARCS_ERROR_CODE0 - 14
#define SHARCS_ERROR_NO_WRITE_PERMISSION         SHARCS_ERROR_CODE0 - 15
#define SHARCS_ERROR_INVALID_OUTDIR              SHARCS_ERROR_CODE0 - 16
#define SHARCS_ERROR_POWER_DOWN_ASIC             SHARCS_ERROR_CODE0 - 17
#define SHARCS_ERROR_POWER_DOWN_DURING_EXPOSURE  SHARCS_ERROR_CODE0 - 18
#define SHARCS_ERROR_POWER_DOWN_DURING_INIT      SHARCS_ERROR_CODE0 - 19
#define SHARCS_ERROR_START_EXPOSURE              SHARCS_ERROR_CODE0 - 20
#define SHARCS_ERROR_INVALID_SUBARRAY_COLUMNS    SHARCS_ERROR_CODE0 - 21
#define SHARCS_ERROR_INVALID_SUBARRAY_ROWS       SHARCS_ERROR_CODE0 - 22
#define SHARCS_ERROR_INVALID_READ_TIME           SHARCS_ERROR_CODE0 - 23
#define SHARCS_ERROR_BAD_KEYWORD                 SHARCS_ERROR_CODE0 - 24
#define SHARCS_ERROR_BAD_KEYWORD_VALUE           SHARCS_ERROR_CODE0 - 25
#define SHARCS_ERROR_START_HARDWARE_INIT_THREAD  SHARCS_ERROR_CODE0 - 26
#define SHARCS_ERROR_START_FITS_HEADER_THREAD    SHARCS_ERROR_CODE0 - 27
#define SHARCS_ERROR_START_EXPOSURE_TIMER_THREAD SHARCS_ERROR_CODE0 - 28
#define SHARCS_ERROR_SET_REFERENCE_PIXEL_SUBTRACTION_TYPE  SHARCS_ERROR_CODE0 - 29
#define SHARCS_ERROR_WRITE_RAW_FRAMES            SHARCS_ERROR_CODE0 - 30
#define SHARCS_ERROR_ICE_ERROR                   SHARCS_ERROR_CODE0 - 31
#define SHARCS_ERROR_GET_POWER                   SHARCS_ERROR_CODE0 - 32
#define SHARCS_ERROR_CHECK_FOR_SERVER            SHARCS_ERROR_CODE0 - 33
#define SHARCS_ERROR_REQUEST_HEARTBEAT_PERIOD    SHARCS_ERROR_CODE0 - 34
#define SHARCS_ERROR_REQUEST_EXPOSING            SHARCS_ERROR_CODE0 - 35
#define SHARCS_ERROR_REQUEST_FITS_WRITING_EXT    SHARCS_ERROR_CODE0 - 36
#define SHARCS_ERROR_INVALID_OUTDIR_ROOT         SHARCS_ERROR_CODE0 - 37

#ifdef CREATOR
char *SHARCSErrMsg[] = {
  "",
  "SHARCS Error (01): Generic SHARCS error.",
  "SHARCS Error (02): Error setting keyword, because keyword(s) locked.",
  "SHARCS Error (03): Error creating client to ICE server.",
  "SHARCS Error (04): Error initializing hardware.", 
  "SHARCS Error (05): Error downloading timing code.",
  "SHARCS Error (06): Error aborting exposure.", 
  "SHARCS Error (07): Cannot initialize while exposing.  Try abort.",
  "SHARCS Error (08): Cannot resume while exposing.  Try abort.",
  "SHARCS Error (09): Cannot connect while exposing.  Try abort.",
  "SHARCS Error (10): Cannot initialize while already initializing.",
  "SHARCS Error (11): Cannot resume while initializing.",
  "SHARCS Error (12): Cannot connect while initializing.",
  "SHARCS Error (13): Not connected to ICE server.  Try connect.", 
  "SHARCS Error (14): Error starting exposure.  Try initializing hardware.", 
  "SHARCS Error (15): No write permission.  Check filename and outdir.", 
  "SHARCS Error (16): Specified directory doesn't exist or is not writeable.", 
  "SHARCS Error (17): Error powering down ASIC.", 
  "SHARCS Error (18): Cannot power down while exposing.  Try aborting first.",
  "SHARCS Error (19): Cannot power down while initializing.",
  "SHARCS Error (20): Error starting exposure.", 
  "SHARCS Error (21): Error with subarray extents.  StartX is bigger then EndX.", 
  "SHARCS Error (22): Error with subarray extents.  StartY is bigger then EndY.", 
  "SHARCS Error (23): Frame read time is non-positve.  Try initializing.", 
  "SHARCS Error (24): Error - specified keyword is invalid.", 
  "SHARCS Error (25): Error - specified keyword value is invalid.", 
  "SHARCS Error (26): Error starting hardware init thread.", 
  "SHARCS Error (27): Error starting fits header thread.", 
  "SHARCS Error (28): Error starting exposure timer thread.", 
  "SHARCS Error (29): Error setting reference pixel subtraction type.", 
  "SHARCS Error (30): Error enabling/disabling writing of all frames.", 
  "SHARCS Error (31): Error communicating to ICE server.  Check server.", 
  "SHARCS Error (32): Error getting SIDECAR power.", 
  "SHARCS Error (33): Error checking ICE server connection.", 
  "SHARCS Error (34): Error requesting heartbeat period from ICE server.", 
  "SHARCS Error (35): Error requesting exposing status from ICE server.",
  "SHARCS Error (36): Error requesting fits writing extension from ICE server.",
  "SHARCS Error (37): Error setting outdir.  Absolute paths must start with value of unixrootdir keyword.",
  NULL
};

char *SamplingModeNames[] = {
  "",
  "Single", 
  "CDS", 
  "MCDS",
  "UTR"
};

#else
extern char *SHARCSErrMsg[];
extern char *SamplingModeNames[];

#endif

/* exposure info struct */
struct ExposureConfiguration {
  int integrationTime;   /* in ms */
  int numberOfResets;
  int samplingMode;
  int numberOfReads;
  int numberOfGroups;
  int numberOfCoadds;
  char fitsFilename [SHARCS_FILENAME_LENGTH];
  int getDCSkw;
  int getACSkw;
  int getMOSFIREkw;
  int imageType;
  int startx;
  int starty;
  int endx;
  int endy;
  pthread_t fits_header_thread;
  int headerTest;
};

/* hardware initialization struct */ 
struct HardwareInit {
  int doInit;
  char sidecarCfg[SHARCS_FILENAME_LENGTH];
  char mcdFile[SHARCS_FILENAME_LENGTH];
  int downloadFirmware;
  int forceInit;
};

/* exposure timer struct */
struct ExposureTimer {
  uint64_t totaltime;
};

/* function prototypes */
int sharcs_memory_init(int cols, int rows, long readblocksize);
int sharcs_open_driver();
int sharcs_hardware_init(char *sidecar_cfg, int downloadFirmware, char *timing_file, int force);
void *sharcs_hardware_init_thread(void *pHardwareInitStruct);
int sharcs_hardware_resume();
int sharcs_power_down_asic();
int sharcs_abort_exposure(int abortLevel);
int sharcs_set_reference_pixel_subtraction_type(int type);
int sharcs_write_raw_frames(int enable);
int sharcs_get_asic_power();
int sharcs_get_read_time(void); /* in sharcs.c */
int sharcs_start_exposure(struct ExposureConfiguration *pExp);
void sharcs_value_callback(const char *name, const char *value);
int sharcs_start_exposure_timer(uint64_t totaltime);
void *sharcs_exposure_timer_thread(void *pTime);
int window_changed(int startx, int starty, int endx, int endy);
void save_window(int startx, int starty, int endx, int endy);
int is_full_frame(int startx, int starty, int endx, int endy);
int was_full_frame(void);
void sharcs_set_status3(const char *main, const char *detail); /* in sharcs_if.c */
int sharcs_set_status(const char *message); /* in sharcs_if.c */
int sharcs_set_main_status(const char *message); /* in sharcs_if.c */
int sharcs_set_detailed_status(const char *message); /* in sharcs_if.c */
int sharcs_set_ready(int status); /* in sharcs_if.c */  
void sharcs_clear_rampStartTime_and_dateBeg(void); /* in sharcs_if.c */
void sharcs_set_rampStartTime_and_dateBeg(void); /* in sharcs_if.c */
int sharcs_set_connected(int status); /* in sharcs_if.c */  
int sharcs_set_initing(int status); /* in sharcs_if.c */  
int sharcs_set_exposing(int status); /* in sharcs_if.c */  
int sharcs_set_image_type(int type); /* in sharcs_if.c */  
int sharcs_set_image_done(int status); /* in sharcs_if.c */  
int sharcs_set_coadds_done(int coadds); /* in sharcs_if.c */  
int sharcs_set_reads_done(int reads); /* in sharcs_if.c */  
int sharcs_set_groups_done(int groups); /* in sharcs_if.c */  
int sharcs_set_ignored_reads(int reads); /* in sharcs_if.c */  
int sharcs_set_sidecar_init(const char *initstring);
int sharcs_set_exposure_complete(float percent); /* in sharcs_if.c */
int sharcs_set_lastfile(const char *fullfile); /* in sharcs_if.c */
int sharcs_set_readtime(float readtime); /* in sharcs_if.c */
int sharcs_set_elapsed_time(float time); /* in sharcs_if.c */
int sharcs_set_remaining_time(float time); /* in sharcs_if.c */
int sharcs_get_sidecar_init_status(char **status); /* in sharcs_if.c */
int sharcs_set_fits_writing_extension(const char *ext); /* in sharcs_if.c */
int sharcs_set_observation_time(uint64_t time); /* in sharcs_if.c */  
int sharcs_set_heartbeat(const char *heartbeat);  /* in sharcs_if.c */  
int sharcs_make_palette_entry(struct ExposureConfiguration *pExp);
int sharcs_set_heartbeat_period(int period);
int sharcs_construct_filenames(int frame_number_increment); /* in sharcs_if.c */
int sharcs_filecheck(char *fullfile);
int sharcs_writecheck(char *fullfile);
int sharcsCheckForRequiredKeywords(char *keyword);
int sharcs_append_mask_extension(char *lastfile);
int sharcs_start_headerTest(struct ExposureConfiguration *pExp);
char *sharcs_windowsToUnixPath(const char *windowsPath, const char *unixRoot);
int sharcs_sidecar_validate_filename(const char *fullfile);
int sharcs_monitor_sidecar_server();
int sharcs_removeRoot(char **origPath, char *origRoot);
char *sharcs_convertPath(char *oldpath, char *root, char origDirSep, char targetDirSep);
#endif /* _SHARCS_H */

