/*+***************************************************************************
                                                                       
    File:     sharcs_if.c                                                 
                                                                        
    Purpose:  The functions herein are the interface between the KTL server
              and the SHARCS Detector server. 

 * This code is mostly from:
 *  2007-06-21 JLW: modified from ods_if.c, from OSIRIS detector server
 *
 * but it has been heavily edited for form and structure.
 *                             
 *-**************************************************************************/
#include "config.h"
RCSID_DEF(rcsid,"$Id: sharcs_if.c,v 1.39 2014/07/16 21:14:44 will Exp $");

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <syslog.h>
#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>
#include <inttypes.h>

#include <dlfcn.h>

#ifdef MR_SERVER
    #include "mr_server/mr_server.h"
#else
    #include "rpcKey.h"
#endif

#include "ktl.h"
#include "ktl_keyword.h"

#ifndef MR_SERVER
    #include "rpc_if.h"
    #include "util_prototypes.h"
    #include "errlog.h"
#endif

#define CREATOR
#ifndef MR_SERVER
    #include "rpc_service.h"
#endif
#include "sharcs.h"
#include "sharcs_sidecar.h"
#include "sharcs_fits.h"

#ifdef MR_SERVER
    #include "../common/tablemap.h"
#endif

// Define PASTE in two steps, so that the
// macro named in the first name (KTLSERVICE)
// gets expanded before the actual pasting.
#define PASTE(a,b) a ## b
#define XPASTE(a,b) PASTE(a,b)

extern unsigned int Verbose;    /* in errlog.c */

int Simulate;

FILE *logfile;

extern char *MyService;

#ifdef MR_SERVER
    #define CHECK_FOR_KEYWORD(key, dummy1, dummy2) \
    if (keyExists(key, NULL) == 0) return -1;

#else
    #define CHECK_FOR_KEYWORD( key, keyInfo, failedptr ) \
      strncpy(failedptr, key, MAX_NAME); \
      if (KEYEXISTS( key, keyInfo ) < 0) return -1;
#endif

int if_abort=0;   /* abort exposure variable */

volatile int64_t rampStartTime = -1;
volatile int64_t dateBeg = -1;
volatile int isExposing;
volatile uint64_t totaltime;
volatile float exposureProgress;
volatile int heartbeatPrimed;

extern long Iteration;

extern int ro_abort;
extern int reading_out;
extern int readTimeInMicros;

extern char *lastMCDFileDownloaded;

struct {
    int firstTime;
    int startx;
    int starty;
    int endx;
    int endy;
} last_window_info = { 1 };

static void clear_status_keywords(void);
static void logAndSetErrstr(const char *kwd,
		int errcode, KTL_SUPERPOLY *data,
		const char *file, int line);

int sharcs_write_engr(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_init(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_resume(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_connect(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_reset(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_outdir(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_powerdown(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_setrpstype(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_writeall(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_framenum(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_window(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_getasicpower(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_abort(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_TestOrGo(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_headertest(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_simulate(const char *svc, const char *key, KTL_SUPERPOLY *);
int sharcs_write_verbose(const char *svc, const char *key, KTL_SUPERPOLY *);

int sharcs_make_palette_entry(struct ExposureConfiguration *pExp);


/*************************************************************************
 *+
 * Function name: <svc>_init

 * Function: void <svc>_init( char *service )    
 
 * Author:   Jason Weiss
 
 * Date:     2007-06-21

 * Abstract: Initialize the application.
     
 * Description: This is called from main within rpcKey_server.c 

             A child is created	to generate a trigger message every 
             'timerperiod' so as to read temperatures. 
             This is essentially an RPC message back to the parent so 
	     that <svc>_trigger will ultimately be invoked.

 * Modification History:
 *-
*************************************************************************/
int XPASTE(KTLSERVICE, _init)( char *service )
{
  int err;
  char failedKW[MAX_NAME];
  char *str1;
  int int1;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s for %s",
  			__FILE__, __LINE__, __FUNCTION__, service );

  heartbeatPrimed = 0;

  /* set line by line buffering for stdout and stderr */
  if (setvbuf(stdout, NULL, _IOLBF, 80) != 0) {
    errlog(SERROR, "%s %d: Warning: error setting stdout line buffering.",
    			__FILE__, __LINE__);
  }
  if (setvbuf(stderr, NULL, _IOLBF, 80) != 0) {
    errlog(SERROR, "%s %d: Warning: error setting stdout line buffering.",
    			__FILE__, __LINE__);
  }

  if ( getenv( "SHARCS_SIMULATE" ) == NULL )
    Simulate = FALSE;
  else
    Simulate = TRUE;

  log_msg( SINFO, LOGLVL_USER1, "%s %d: Server running in %s mode.",
	    __FILE__, __LINE__,
	    ((Simulate == TRUE) ? "simulated" : "normal (non-simulated)") );

  /* check for required keywords */
  log_msg(SDEBUG, LOGLVL_USER4, "%s %d: Checking for required keywords",
  			__FILE__, __LINE__);

  if (sharcsCheckForRequiredKeywords(failedKW) < 0) {
    errlog(SERROR, "%s %d: Error with keyword configuration.",
    			__FILE__, __LINE__);
    errlog(SERROR, "%s %d: "
    "Keyword configuration does not contain required keyword <%s>.  Exiting.",
			__FILE__, __LINE__, failedKW);
    exit( -1 );
  } else { 
    log_msg(SINFO, LOGLVL_USER1, "%s %d: All required keywords found.",
    			__FILE__, __LINE__);
  }

  lastMCDFileDownloaded = strdup("none");

  getStringKeyword("asichost", &str1);
  getIntKeyword("asicport", &int1);
  /* initialize connection to sidecar */
  log_msg(SDEBUG, LOGLVL_USER4,
  	"%s %d: Connecting to SIDECAR ICE Server...", __FILE__, __LINE__);
  if ( (err=sharcs_open_driver(str1, int1)) < 0) { 
    errlog(SERROR, "%s %d: Error connecting to SIDECAR.", __FILE__, __LINE__);
    errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,
    		SHARCSErrMsg[SHARCS_ERROR_CODE0-err]);
    sharcs_set_connected(SHARCS_CONNECTED_FALSE);
  } else {
      log_msg(SINFO, LOGLVL_USER1,
      	"%s %d: Successfully connected to SIDECAR ICE Server.",
	    __FILE__, __LINE__);
    sharcs_set_connected(SHARCS_CONNECTED_TRUE);
    /* initialize filenames */
    #if 0
	// Use this line if you want to force exposure numbers to reset to 0
	// on every restart of the sharcs dispatcher.
	sharcs_construct_filenames(-1);
    #else
	// Use this line if you want to start with the last exposure number
	// used from the last time the dispatcher was running.
	sharcs_construct_filenames(0);
    #endif

  }
  
  log_msg(SINFO, LOGLVL_USER1, "%s %d: Server is started for %s.",
	  __FILE__, __LINE__, service );

 /*
  *  The mosfire startup of a timer thread has been removed from
  *  this _init function, because mr_server always starts the timer thread
  *  as part of its own startup.  [[ For backwards-compatibility with code
  *  that may invoke timerThread(), the function is defined in mr_server
  *  as a no-op. ]]
  */

  log_msg( SINFO, LOGLVL_USER2, "%s %d: exiting %s for %s",
		  __FILE__, __LINE__, __FUNCTION__, service );

  return 1;
}

/*************************************************************************
 *+
 * Function name: <svc>_trigger

 * Function: int <svc>_trigger( char *service )     
 
 * Author:   Jason Weiss
 
 * Date:     2007-06-21

 * Abstract: does nothing.  no need for this loop.  kept for consistency
    
 * Description: This is stimulated by the message from a forked
             child which essentially implements a polling loop.   

 * Modification History:
 *-
*************************************************************************/
int XPASTE(KTLSERVICE, _trigger)( char *service )
{
  KTL_SUPERPOLY data;
  int err=0; 
  int connected;
  static int firstTime = 1;
  static int inProgress = 0;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: enter %s()",
  				__FILE__, __LINE__, __FUNCTION__);

  /* check to see if trigger loop is already in progress */
  if (++inProgress > 1) {

    errlog( SERROR, "%s %d - %s: Trigger loop already in progress.  Exiting.",
    	__FILE__, __LINE__, __FUNCTION__);

  } else {

    data.magic = KTL_SUPERPOLYMAGIC;
    data.version = KTL_SUPERPOLYVERSION;
    data.status = 0;
    data.errstr = NULL;
    data.type = KTL_INT;
    data.value.i = Iteration;
    key_sendMonitor( service, NULL, 0, "iteration", 
		     (KTL_ANYPOLY *)&data, KTL_INT );


    if (firstTime == 0) {
      getIntKeyword("connected", &connected);
  
      if (connected == 1) {
	if ((err = sharcs_monitor_sidecar_server()) < 0) {
	  errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,
	  			SHARCSErrMsg[SHARCS_ERROR_CODE0 - err]);
	  errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,
	  			"Error monitoring sidecar server");
	}
      }
    } else {
      firstTime--;
    }
  }

  --inProgress;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: %s() return",
  				__FILE__, __LINE__, __FUNCTION__);

  /* do nothing */
  return 0;
}

/*************************************************************************
 *+
 * Function name: <svc>_write

 * Function: void <svc>_write( char *service,
                             char *keyName,
			     KTL_SUPERPOLY *data )
 
 * Author:    Jason Weiss
 
 * Date:      2007-06-21

 * Abstract:  Processing keyword modifications.
    
 * Description:   

 * Modification History:
                
		Modified for the RPC upgrades: Allan Honey, Sept 2002
		   this was split out of rpc_write_1 which itself was
		   renamed api_write.

 *-
*************************************************************************/

/* --------------------------------------------------------------------- */

int
sharcs_write_engr(
    const char *service,
    const char *keyName,
    KTL_SUPERPOLY *data)
{
    updateKeyword(keyName, data, BROADCAST_TRUE);
    return 0;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_init(const char *service,
		   const char *keyName, KTL_SUPERPOLY *data)
{
    int status;
    int exposing, initing, connected, dlfirmware, forceflag;
    char *asiccfg_str, *timfile_str;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i != SHARCS_INIT_YES &&
			data->value.i != SHARCS_INIT_FORCE) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }
    // Assertion: keyword value == Yes or Force.

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are currently exposing.",
	    __FILE__, __LINE__);
    getIntKeyword("exposing", &exposing);
    if (exposing > 0) {
	status = SHARCS_ERROR_INIT_DURING_EXPOSURE;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->value.i = 0;
	return status;
    }
    // Assertion: not currently exposing

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Not exposing.  "
	    "Checking to see if we are currently already initializing.",
	    __FILE__, __LINE__);

    /* Make sure we are not currently initing */
    getIntKeyword("initing", &initing);
    if (initing > 0) {
	status = SHARCS_ERROR_INIT_DURING_INIT;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->value.i = 0;
	return status;
    }
    // Assertion: not currently initializing

    log_msg(SINFO, LOGLVL_USER4,
		"%s %d: Not initializing.  "
		"Checking to see if we are connected to the ICE server.",
		__FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }
    // Assertion: yes, we are connected

    sharcs_set_ready(SHARCS_READY_NO);
    sharcs_set_status(SHARCS_STATUS_INITIALIZING);
    sharcs_set_main_status(SHARCS_STATUS_INITIALIZING);
    sharcs_set_detailed_status(" ");
    getStringKeyword("asiccfg", &asiccfg_str);
    getIntKeyword("dlfirmware", &dlfirmware);
    getStringKeyword("timfile", &timfile_str);

    forceflag = (data->value.i == SHARCS_INIT_FORCE);

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Connected.  "
	    "Starting hardware init with "
	    "asiccfg=%s, dlfirmware=%d, timfile=%s.",
	    __FILE__, __LINE__, asiccfg_str, dlfirmware, timfile_str);
    if ((status = sharcs_hardware_init(asiccfg_str, dlfirmware, timfile_str,
							 forceflag)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	sharcs_set_status(SHARCS_STATUS_ERROR);
	sharcs_set_main_status(SHARCS_STATUS_ERROR);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Hardware init successfully started.",
		__FILE__, __LINE__);
    }
    data->value.i = 0;
    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_resume(const char *service,
		     const char *keyName, KTL_SUPERPOLY * data)
{
    /* this keyword attempts to resume use of sidecar without 
       initializing hardware.
     */

    int status;
    int exposing, initing, connected;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i != SHARCS_RESUME_YES) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are currently exposing.",
	    __FILE__, __LINE__);
    getIntKeyword("exposing", &exposing);
    if (exposing > 0) {
	status = SHARCS_ERROR_RESUME_DURING_EXPOSURE;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->value.i = 0;
	return status;
    }
    // Assertion: not currently exposing

    /* Make sure we are not currently initing */
    getIntKeyword("initing", &initing);
    if (initing > 0) {
	status = SHARCS_ERROR_RESUME_DURING_INIT;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }
    // Assertion: not currently initializing

    log_msg(SINFO, LOGLVL_USER4,
		"%s %d: Not initializing.  "
		"Checking to see if we are connected to the ICE server.",
		__FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }
    // Assertion: yes, we are connected

    sharcs_set_ready(SHARCS_READY_NO);
    log_msg(SINFO, LOGLVL_USER4, "%s %d: Connected.  Performing resume.",
	    __FILE__, __LINE__);
    sharcs_set_status(SHARCS_STATUS_RESUMING);
    sharcs_set_main_status(SHARCS_STATUS_RESUMING);
    sharcs_set_detailed_status(" ");
    if ((status = sharcs_hardware_resume()) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	sharcs_set_status(SHARCS_STATUS_ERROR);
	sharcs_set_main_status(SHARCS_STATUS_ERROR);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1, "%s %d: Resume successful.",
		__FILE__, __LINE__);
    }
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_connect(const char *service,
		      const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected, exposing, initing, asicport;
    char *asichost;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i != SHARCS_CONNECT_YES) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

	/* Jay's todo notes: 
	   - does exposing go to 0 on error/disconnect 
	   - don't connect if connected is true?
	 */

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are currently exposing.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    getIntKeyword("exposing", &exposing);
    if (exposing > 0 && connected == 1) {
	status = SHARCS_ERROR_CONNECT_DURING_EXPOSURE;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->value.i = 0;
	return status;
    }
    // Assertion: not currently exposing

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Not exposing.  "
	    "Checking to see if we are currently already initializing.",
	    __FILE__, __LINE__);

    /* Make sure we are not currently initing */
    getIntKeyword("initing", &initing);
    if (initing > 0) {
	status = SHARCS_ERROR_CONNECT_DURING_INIT;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }
    // Assertion: not currently initializing

    getStringKeyword("asichost", &asichost);
    getIntKeyword("asicport", &asicport);
    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Not initializing.  "
	    "Connecting to Sidecar ICE server with asichost=%s, asicport=%d.",
	    __FILE__, __LINE__, asichost, asicport);

    /* initialize connection to sidecar */
    if ((status = sharcs_open_driver(asichost, asicport)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	sharcs_set_connected(SHARCS_CONNECTED_FALSE);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Connect successful.", __FILE__,
		__LINE__);
	sharcs_set_connected(SHARCS_CONNECTED_TRUE);
    }
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_reset(const char *service,
		    const char *keyName, KTL_SUPERPOLY * data)
{
    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    log_msg(SINFO, LOGLVL_USER1,
	    "%s %d: Resetting exposure status "
	    "(ready=no, exposing=false, initing=false, image_done=reset)",
	    __FILE__, __LINE__);
    /* reset exposure status */
    sharcs_set_ready(SHARCS_READY_NO);
    sharcs_set_exposing(SHARCS_FALSE);
    sharcs_set_initing(SHARCS_INITING_FALSE);
    sharcs_set_image_done(SHARCS_IMAGE_DONE_RESET);
    sharcs_set_sidecar_init("reset");
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return 0;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_outdir(const char *service,
		     const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected;

    clear_status_keywords();

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);

    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Setting outdir to %s.  Checking write permission.",
	    __FILE__, __LINE__, data->value.s);

    /* check for permission */
    if ((status = sharcs_writecheck(data->value.s)) == SHARCS_FILECHECK_OK) {
	status = 0;
	setStringKeyword(keyName, data->value.s, BROADCAST_TRUE);
	sharcs_construct_filenames(0);
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Outdir successfully set to %s.", __FILE__,
		__LINE__, data->value.s);
    } else {
	if (status == SHARCS_FILECHECK_INVALID_DATAROOT) {
	    status = SHARCS_ERROR_INVALID_OUTDIR_ROOT;
	} else {
	    status = SHARCS_ERROR_INVALID_OUTDIR;
	}
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    }
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_powerdown(const char *service,
			const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int exposing, initing, connected;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i != SHARCS_POWERDOWN_YES) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are currently exposing.",
		__FILE__, __LINE__);
    /* don't allow while exposing */
    getIntKeyword("exposing", &exposing);
    if (exposing > 0) {
	status = SHARCS_ERROR_POWER_DOWN_DURING_EXPOSURE;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	"%s %d: Not exposing.  "
	"Checking to see if we are currently already initializing.",
	__FILE__, __LINE__);
    /* make sure we are not currently initing */
    getIntKeyword("initing", &initing);
    if (initing > 0) {
	status = SHARCS_ERROR_POWER_DOWN_DURING_INIT;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	"%s %d: Not initializing.  "
	"Checking to see if we are connected to the ICE server.",
	__FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Connected.  Powering down ASIC.",
	    __FILE__, __LINE__);
    sharcs_set_ready(SHARCS_READY_NO);
    if ((status = sharcs_power_down_asic()) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	sharcs_set_status(SHARCS_STATUS_ERROR);
	sharcs_set_main_status(SHARCS_STATUS_ERROR);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Successfully powered down.",
		__FILE__, __LINE__);
	sharcs_set_status(SHARCS_STATUS_POWERED_DOWN);
	sharcs_set_main_status(SHARCS_STATUS_POWERED_DOWN);
	sharcs_set_detailed_status
	    (SHARCS_DETAILED_STATUS_POWERED_DOWN);

    }
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_setrpstype(const char *service,
			 const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected;

    clear_status_keywords();

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting rpstype.", __FILE__,
		__LINE__);
    if ((status =
	     sharcs_set_reference_pixel_subtraction_type(data->value.i)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1, "%s %d: RPS type successfully set.",
		    __FILE__, __LINE__);
	setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    }
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_writeall(const char *service,
		       const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected;

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting writeall.",
		__FILE__, __LINE__);

    if ((status = sharcs_write_raw_frames(data->value.i)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Writeall successfully set.",
		__FILE__, __LINE__);
	setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    }
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_framenum(const char *service,
			   const char *keyName, KTL_SUPERPOLY * data)
{
    int status, connected;
    int fn;

    // Have to be connected to verify the filename.
    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    // Set the value...
    fn = data->value.i;
    if (fn < 0) {
	fn = 0;
    }
    setIntKeyword(keyName, fn, BROADCAST_TRUE);

    // ...and test/adjust it to be valid.
    sharcs_construct_filenames(0);

    return 0;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_window(const char *service,
			   const char *keyName, KTL_SUPERPOLY * data)
{
    int status, connected;
    int err;
    int startx, starty, endx, endy, arraycols, arrayrows;

    // Have to be connected to update the readtime.
    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    // Set the new value...
    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);

#if 0
// THIS BLOCK TURNED OFF BECAUSE THE SIDECAR SERVER
// HANGS IF WE INVOKE IT TWICE, QUICKLY... SIGH.
    // ...and, if the window is valid (it may be temporarily invalid
    // as several points are being changed), and has changed,
    // update the readtime.
    getIntKeyword("startx", &startx);
    getIntKeyword("endx", &endx);
    getIntKeyword("starty", &starty);
    getIntKeyword("endy", &endy);
    getIntKeyword("arraycols", &arraycols);
    getIntKeyword("arrayrows", &arrayrows);
    // errlog(SDEBUG, "B 0:  %d %d  %d %d  %d %d",
    		startx, endx, starty, endy, arraycols, arrayrows);
    if (startx >= 0 && startx <= endx && endx < arraycols &&
		    starty >= 0 && starty <= endy && endy < arraycols &&
			window_changed(startx, starty, endx, endy)) {
	// Update the readtime.
	errlog(SDEBUG, "B 1");
        if ((err = sharcs_sidecar_setExposureExtents(startx, starty,
							    endx, endy)) < 0) {
	    errlog(SERROR, "%s %d: Error setting exposure extents.  err=%d",
	    			__FILE__, __LINE__, err);
	    /* todo: return error based on retval?
	     * (-2 = invalid exp settings)
	     */
	    errlog(SDEBUG, "B 3");
	    return SIDECAR_ERROR_SET_EXPOSURE_EXTENTS;

	} else if (sharcs_get_read_time() < 0) {
	  // Ignore error; problem was reported in detailed status,
	  // and (at this writing) there's nothing more to do.
	  errlog(SDEBUG, "B 5");
	} else {
	    // Got readtime.  Save window.
	    save_window(startx, starty, endx, endy);
	    usleep(250000);
	    errlog(SDEBUG, "B 6");
	}
    }
#endif

    return 0;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_getasicpower(const char *service,
			   const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i == SHARCS_GETASICPOWER_NO) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	"%s %d: Sending get asic power command.",
	__FILE__, __LINE__);
    if ((status = sharcs_get_asic_power()) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Get Asic Power successfully initiated.",
		__FILE__, __LINE__);
    }
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_abort(const char *service,
		    const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int connected;

    clear_status_keywords();

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);
    if (data->value.i == SHARCS_ABORT_NO) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if we are connected to the ICE server.",
	    __FILE__, __LINE__);
    getIntKeyword("connected", &connected);
    if (connected == 0) {
	status = SHARCS_ERROR_NOT_CONNECTED;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Sending abort command.",
		    __FILE__, __LINE__);
    if ((status = sharcs_abort_exposure(data->value.i)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    } else {
	status = 0;
	log_msg(SINFO, LOGLVL_USER1,
		"%s %d: Abort successfully initiated.",
		__FILE__, __LINE__);
    }
    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_TestOrGo(const char *service,
		       const char *keyName, KTL_SUPERPOLY * data)
{
    int status;
    int is_go, ready, sampmode, calcreads, groups, numreads;
    int coadds, endx, endy, getacskw, getdcskw, getkw;
    int itime, maxutrreads, resets, startx, starty;
    int writeall;
    int imageType;
    float minutrrditime;
    char *outdir, *file;
    char *dirsep, *extension;
    char *hostDataRoot, *winDataRoot;
    char *outdirKW, *fileKW;
    char fullfile[SHARCS_FILENAME_LENGTH];
    char fileCopy[SHARCS_FILENAME_LENGTH];
    char *filePtr;
    char *convertedFilename;
    struct ExposureConfiguration *pExp;

    clear_status_keywords();

    if (isExposing) {
	// requesting another exposure would abort the
	// in-progress exposure, so refuse to do this.
	status = SHARCS_ERROR_INIT_DURING_EXPOSURE;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);

    is_go = (strcasecmp(keyName, "go") == 0);

    if ((is_go && data->value.i != SHARCS_GO_YES) ||
	(!is_go && data->value.i != SHARCS_TEST_YES)) {
	setIntKeyword(keyName, 0, BROADCAST_TRUE);
	return 0;
    }

    log_msg(SINFO, LOGLVL_USER1, "%s %d: Request to take exposure.",
	    __FILE__, __LINE__);

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to make sure system is ready for exposures.",
	    __FILE__, __LINE__);

    /* ready keyword must be SHARCS_READY_YES */
    getIntKeyword("ready", &ready);
    if (ready != SHARCS_READY_YES) {
	status = SHARCS_ERROR_NOT_IDLING;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: System is ready.  Checking filename...",
	    __FILE__, __LINE__);

    /* get filename and path */
    if (is_go) {
	outdirKW = "outdir";
	fileKW = "datafile";
	imageType = SHARCS_IMAGE_TYPE_GO;
    } else {
	outdirKW = "testdir";
	fileKW = "testfile";
	imageType = SHARCS_IMAGE_TYPE_TEST;
    }

    /* check to make sure outdir is writeable */
    getStringKeyword(outdirKW, &outdir);
    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking to see if outdir <%s> is writable.",
	    __FILE__, __LINE__, outdir);
    if (sharcs_writecheck(outdir) != SHARCS_FILECHECK_OK) {
	status = SHARCS_ERROR_NO_WRITE_PERMISSION;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	return status;
    }
    sharcs_construct_filenames(0);

    getStringKeyword("dirsep", &dirsep);
    getStringKeyword(fileKW, &file);
    getStringKeyword("extension", &extension);
    snprintf(fullfile, SHARCS_FILENAME_LENGTH,
	     "%s%s%s.%s", outdir, dirsep, file, extension);
    setStringKeyword("filename", fullfile, BROADCAST_TRUE);

    log_msg(SINFO, LOGLVL_USER1, "%s %d: exposure filename=%s.",
	    __FILE__, __LINE__, fullfile);

    sharcs_set_coadds_done(0);
    sharcs_set_reads_done(0);
    sharcs_set_groups_done(0);
    sharcs_set_image_done(SHARCS_IMAGE_DONE_RESET);

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Getting number of reads.",
	    __FILE__, __LINE__);
    /* determine numreads */
    getIntKeyword("sampmode", &sampmode);
    getIntKeyword("calcreads", &calcreads);
    if (calcreads == SHARCS_CALCREADS_YES) {
	if (sampmode == SHARCS_SAMPMODE_UTR) {
	    /* todo: take into account multiple reads per groups */

	    /* if calcreads is 1, determine number of reads for user
	     *
	     * use read per minutrrditime seconds up to max reads.  then 
	     * use max reads and set itime accordingly */
	    getIntKeyword("itime", &itime);
	    getIntKeyword("maxutrreads", &maxutrreads);
	    getFloatKeyword("minutrrditime", &minutrrditime);
	    if (readTimeInMicros <= 0) {
		status = SHARCS_ERROR_INVALID_SUBARRAY_ROWS;
		logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
		data->status = status;
		return status;
	    }

	    groups =
		(int) floor(itime * 1000. /
			    (minutrrditime * readTimeInMicros));

	    //. if exceeding max utr reads
	    if (groups > maxutrreads) {
		//. set numread to max utr reads
		groups = maxutrreads;
	    }

	    /* make sure numreads at least 2 TODO: check */
	    if (groups < 2)
		groups = 2;
	}
    } else {
	/*
	 * calcreads != SHARCS_CALCREADS_YES
	 */
	if ((sampmode == SHARCS_SAMPMODE_SINGLE) ||
				    (sampmode == SHARCS_SAMPMODE_CDS)) {
	    numreads = 1;
	    groups = 2;
	} else {
	    if (sampmode == SHARCS_SAMPMODE_MCDS) {
		groups = 2;
	    } else {
		getIntKeyword("groups", &groups);
	    }
	    getIntKeyword("numreads", &numreads);
	}
    }

    /* set numreads keyword */
    setIntKeyword("numreads", numreads, BROADCAST_TRUE);
    log_msg(SINFO, LOGLVL_USER4, "%s %d: number of reads = %d.",
		__FILE__, __LINE__, numreads);
    setIntKeyword("groups", groups, BROADCAST_TRUE);
    log_msg(SINFO, LOGLVL_USER4, "%s %d: number of groups = %d.",
		__FILE__, __LINE__, groups);

    /* itime is in milliseconds, which is what is sent to controller */
    getIntKeyword("itime", &itime);

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Getting memory for new exposure configuration structure.",
	    __FILE__, __LINE__);

    /* create exposure structure */
    /* allocate memory for readout structure */
    pExp = (struct ExposureConfiguration *)
			malloc(sizeof(struct ExposureConfiguration));

    log_msg(SINFO, LOGLVL_USER4,
	    "%s %d: Checking subarray extents.", __FILE__, __LINE__);

    /* check subarray extents */
    getIntKeyword("startx", &startx);
    getIntKeyword("endx", &endx);

    if (startx > endx) {
	status = SHARCS_ERROR_INVALID_SUBARRAY_COLUMNS;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->status = status;
	return status;
    }
    pExp->startx = startx;
    pExp->endx = endx;

    getIntKeyword("starty", &starty);
    getIntKeyword("endy", &endy);

    if (starty > endy) {
	status = SHARCS_ERROR_INVALID_SUBARRAY_ROWS;
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	data->status = status;
	return status;
    }
    pExp->starty = starty;
    pExp->endy = endy;

  if (window_changed(pExp->startx, pExp->starty, pExp->endx, pExp->endy)) {
      /* Check if we need to adjust bias voltage.  */
      int result;

      if (last_window_info.firstTime ||
	      (was_full_frame() !=
		      is_full_frame(startx, starty, endx, endy))) {
	  // Have to adjust bias voltage.
	  char *bias;

	  if (is_full_frame(startx, starty, endx, endy)) {
	      bias = "8258";
	  } else {
	      bias = "8290";
	  }
	  if ((result=sharcs_sidecar_write_asic_register("602c", bias)) != 0) {
	    sharcs_set_status3("Failed to set asic register addr=602c",
			"can't set bias voltage; try re-initializing");
	    return (result < 0) ? result : -result;

	  } else if ((result=sharcs_sidecar_write_asic_register("6900",
	  						"0x8002")) != 0) {
	    sharcs_set_status3("Failed to set asic register addr=6900",
	    		"can't set bias voltage; try re-initializing");
	    return (result < 0) ? result : -result;
	  } else {
	      errlog(SERROR, "%s %d: set bias voltage to %s",
					       __FILE__, __LINE__, bias);
	  }
      }

      // Do NOT save the updated window here!
      // Instead, let it be saved in sharcs_set_exposure(),
      // after it invokes sharcs_sidecar_setExposureExtents()
      // and has an opportunity to read the new readTime.

  }
    log_msg(SINFO, LOGLVL_USER1,
		"%s %d: subarray extents: x=[%d,%d], y=[%d,%d].",
		__FILE__, __LINE__, pExp->startx, pExp->endx,
		pExp->starty, pExp->endy);

    strncpy(fileCopy, fullfile, SHARCS_FILENAME_LENGTH);
    filePtr = fileCopy;

    if (fullfile[0] == '/') {
	getStringKeyword("unixrootdir", &hostDataRoot);
	if (sharcs_removeRoot(&filePtr, hostDataRoot) < 0) {
	    log_msg(SINFO, LOGLVL_USER1,
		    "%s %d: Error converting path to target format.  "
		    "File %s does not start with host data root directory %s",
		    __FILE__, __LINE__, fullfile, hostDataRoot);
	    status = SHARCS_ERROR_INVALID_OUTDIR_ROOT;
	    logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	    data->status = status;
	    return status;
	}
    }

    getStringKeyword("winrootdir", &winDataRoot);

    convertedFilename =
	sharcs_convertPath(filePtr, winDataRoot, '/', '\\');

    pExp->fitsFilename[0] = '\0';
    strncpy(pExp->fitsFilename, convertedFilename, SHARCS_FILENAME_LENGTH);
    getIntKeyword("resets", &resets);
    pExp->numberOfResets = resets;
    pExp->integrationTime = itime;
    pExp->numberOfReads = numreads;
    pExp->numberOfGroups = groups;
    getIntKeyword("coadds", &coadds);
    pExp->numberOfCoadds = coadds;
    getIntKeyword("sampmode", &sampmode);
    pExp->samplingMode = sampmode;
#ifdef MR_SERVER
    // We don't care about these; we'll get keywords differently.
    getIntKeyword("getkw", &getkw);
    pExp->getMOSFIREkw = getkw;
    getIntKeyword("getacskw", &getacskw);
    pExp->getACSkw = getacskw;
    getIntKeyword("getdcskw", &getdcskw);
    pExp->getDCSkw = getdcskw;
#endif
    pExp->imageType = imageType;
    pExp->headerTest = 0;

    getIntKeyword("writeall", &writeall);

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Setting writeall.",
					    __FILE__, __LINE__);
    if ((status = sharcs_write_raw_frames(writeall)) < 0) {
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
	// better to keep going; don't return error code.
    }

    log_msg(SINFO, LOGLVL_USER1,
	    "%s %d: other exposure parameters: "
	    "resets=%d, itime=%d, coadds=%d, sampmode=%d (%s), "
	    "reads=%d, groups=%d, getkw=%d, getacskw=%d, getdcskw=%d, "
	    "imageType=%d (%s).",
	    __FILE__, __LINE__, pExp->numberOfResets,
	    pExp->integrationTime, pExp->numberOfCoadds,
	    pExp->samplingMode,
	    SamplingModeNames[pExp->samplingMode],
	    pExp->numberOfReads, pExp->numberOfGroups,
	    pExp->getMOSFIREkw, pExp->getACSkw, pExp->getDCSkw,
	    pExp->imageType,
	    ((imageType == SHARCS_IMAGE_TYPE_GO) ? "Go" : "Test"));

    errlog(SINFO, "%s %d: Starting exposure...", __FILE__, __LINE__);
    if ((status = sharcs_start_exposure(pExp)) < 0) {
	errlog(SERROR, "%s %d: Error starting exposure.", __FILE__,
	       __LINE__);
	logAndSetErrstr(keyName, status, data, __FILE__, __LINE__);
    } else {
	status = 0;
	errlog(SINFO,
		"%s %d: Exposure started successfully.", __FILE__,
		__LINE__);
	sharcs_set_exposing(SHARCS_TRUE);
	sharcs_make_palette_entry(pExp);
	sharcs_set_ready(SHARCS_READY_NO);
    }

    setIntKeyword(keyName, 0, BROADCAST_TRUE);
    return status;
}

/* --------------------------------------------------------------------- */

int
sharcs_write_headertest(const char *service,
			 const char *keyName, KTL_SUPERPOLY * data)
{
    int getkw, getacskw, getdcskw;
    struct ExposureConfiguration *pExp;

    clear_status_keywords();

    pExp = (struct ExposureConfiguration *)
	malloc(sizeof(struct ExposureConfiguration));

    getIntKeyword("getkw", &getkw);
    pExp->getMOSFIREkw = getkw;
    getIntKeyword("getacskw", &getacskw);
    pExp->getACSkw = getacskw;
    getIntKeyword("getdcskw", &getdcskw);
    pExp->getDCSkw = getdcskw;
    pExp->headerTest = 1;

    sharcs_start_headerTest(pExp);
    return 0;
}

int
sharcs_write_simulate(const char *service,
		       const char *keyName, KTL_SUPERPOLY * data)
{
    clear_status_keywords();

    log_msg(SDEBUG, LOGLVL_USER4,
	    "%s %d: Setting simulate to  %d (%s)... ", __FILE__, __LINE__,
	    data->value.i,
	    ((data->value.i == SHARCS_SIMULATE_OFF) ? "off" : "on"));
    setIntKeyword(keyName, data->value.i, BROADCAST_TRUE);

    Simulate = data->value.i;
    return 0;
}

/*************************************************************************/
/* If main_str is non-null, it replaces STATUSMAIN.
 * If detail-str is non-null, it replaces STATUSDETAIL.
 *
 * Then set STATUS to the combination of STATUSMAIN and STATUSDETAIL.
 */
void
sharcs_set_status3(
    const char *main_str,
    const char *detail_str)
{
    char *mp, *dp, status[2000];

    if (main_str) {
	sharcs_set_main_status(main_str);
	mp = (char *) main_str;
    } else {
	getStringKeyword("statusmain", &mp);
    }

    if (detail_str) {
	sharcs_set_detailed_status(detail_str);
	dp = (char *) detail_str;
    } else {
	getStringKeyword("statusdetail", &dp);
    }

    if (dp && *dp) {
	snprintf(status, sizeof(status), "%s: %s", mp, dp);
    } else {
	snprintf(status, sizeof(status), "%s", mp);
    }
    sharcs_set_status(status);
}

int sharcs_set_status(const char *message) {  

  return setStringKeyword("status", message, BROADCAST_TRUE);
}

int sharcs_set_main_status(const char *message) {  

  return setStringKeyword("statusmain", message, BROADCAST_TRUE);
}

int sharcs_set_detailed_status(const char *message) {  

  return setStringKeyword("statusdetail", message, BROADCAST_TRUE);
}

int sharcs_set_dlfirmware(int status) {  

  return setIntKeyword("dlfirmware", status, BROADCAST_TRUE);
}

int sharcs_set_sidecar_init(const char *message) {  

  return setStringKeyword("sidecarinit", message, BROADCAST_TRUE);
}

int sharcs_set_connected(int status) {  
  if (status == 0) {
    setIntKeyword("ready", status, BROADCAST_TRUE);
    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_DISCONNECTED);
  } else {
    sharcs_set_detailed_status(SHARCS_DETAILED_STATUS_CONNECTED);
  }
  heartbeatPrimed=0;
  return setIntKeyword("connected", status, BROADCAST_TRUE);
}

int sharcs_set_initing(int status) {  

  return setIntKeyword("initing", status, BROADCAST_TRUE);
}

int sharcs_set_ready(int status) {  

  return setIntKeyword("ready", status, BROADCAST_TRUE);
}

void sharcs_clear_rampStartTime_and_dateBeg() {  
    rampStartTime = -1;
    dateBeg = -1;
}

void sharcs_set_rampStartTime_and_dateBeg() {  
    struct timeval tv;
    gettimeofday(&tv, NULL);
    // Record timestamp in a int64_t, in ms.
    rampStartTime = (int64_t) (tv.tv_sec * 1000) +
    			(int64_t) (tv.tv_usec / 1000);

    // Record DATE-BEG in ms.
    dateBeg = rampStartTime + (readTimeInMicros / 1000);
}

int sharcs_set_exposing(int status) {  
  int oldIsExposing = isExposing;
  isExposing = status;
  if ((oldIsExposing == 0) && (status == 1)) {
    /* start exposure progress timer */
    sharcs_start_exposure_timer(totaltime);
  } else if ((oldIsExposing == 1) && (status == 0)) {
    sharcs_set_ready(SHARCS_READY_YES);
  }
  errlog(SINFO, "setting kwd EXPOSING = %d", status);
  return setIntKeyword("exposing", status, BROADCAST_TRUE);
}

int sharcs_set_image_type(int type) {  
  
  return setIntKeyword("imagetype", type, BROADCAST_TRUE);
}

int sharcs_set_image_done(int status) {  

  return setIntKeyword("imagedone", status, BROADCAST_TRUE);
}

int sharcs_set_coadds_done(int coadds) {  

  return setIntKeyword("coaddsdone", coadds, BROADCAST_TRUE);
}

int sharcs_set_reads_done(int reads) {  
  errlog(SINFO, "readsdone: %d", reads);
  return setIntKeyword("readsdone", reads, BROADCAST_TRUE);
}

int sharcs_set_groups_done(int groups) {  
  errlog(SINFO, "groupsdone: %d", groups);
  return setIntKeyword("groupsdone", groups, BROADCAST_TRUE);
}

int sharcs_set_exposure_complete(float percent) {  
  int intPercent;

  exposureProgress = percent;
  intPercent = (int)floor(percent*100.0);
  // errlog(SINFO, "exposure complete: %d %% (%f)", intPercent, percent);
  return setIntKeyword("progress", intPercent, BROADCAST_TRUE);
}

int sharcs_set_lastfile(const char *fullfile) {
  char *root;
  char *unixfile;

  /* convert lastfile (a windows path) to unix path */
  getStringKeyword("unixrootdir", &root);
    
  unixfile = sharcs_windowsToUnixPath(fullfile, (const char *) root);

  /* append mask extension */
  sharcs_append_mask_extension(unixfile);
  
  return setStringKeyword("lastfile", unixfile, BROADCAST_TRUE);
}

int sharcs_set_heartbeat(const char *heartbeat) {
  heartbeatPrimed=1;
  return setStringKeyword("heartbeat", heartbeat, BROADCAST_TRUE);
}

int sharcs_set_heartbeat_period(int period) {
  return setIntKeyword("heartbeatperiod", period, BROADCAST_TRUE);
}

int sharcs_set_readtime(float readtime) {
  return setFloatKeyword("readtime", readtime, BROADCAST_TRUE);
}

int sharcs_set_elapsed_time(float time) {
  return setFloatKeyword("elapsedtime", time, BROADCAST_TRUE);
}

int sharcs_set_remaining_time(float time) {
  return setFloatKeyword("remainingtime", time, BROADCAST_TRUE);
}

int sharcs_set_ignored_reads(int num) {
  return setIntKeyword("ignoredreads", num, BROADCAST_TRUE);
}

int sharcs_get_sidecar_init_status(char **status) {
  return getStringKeyword("sidecarinit", status);
}

int sharcs_set_fits_writing_extension(const char *ext) {
  return setStringKeyword("tempfitsext", ext, BROADCAST_TRUE);
}

int sharcs_set_observation_time(uint64_t time) {
  totaltime=time;
  return setFloatKeyword("totaltime", time/1000000., BROADCAST_TRUE);
}

int sharcs_construct_filenames(int frame_number_increment) {
  /* variable for date for filename */
  char instr_char;
  char fileroot[FILENAME_LENGTH];
  char fullfile[SHARCS_FILENAME_LENGTH];
  char fullfilex[SHARCS_FILENAME_LENGTH];
  int fileok=0;
  int filexok=0;
  int int1;
  char *str1, *str2, *str3, *str4;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s", __FILE__, __LINE__, __FUNCTION__);

  /* 
   *  format for filename:
   *  
   *  <instrument><framenum>.<extension>
   *  
   *  <m><0000>.<ext>
   *
   *  example:
   *    s0011.fits: 
   */


  log_msg( SINFO, LOGLVL_USER4, "%s %d: getting current date.", __FILE__, __LINE__);

  instr_char = 's';

  /* if reset flag is set, set value to 0, otherwise, increment */
  if (frame_number_increment < 0) {
    log_msg( SINFO, LOGLVL_USER4, "%s %d: resetting frame number to 0.", __FILE__, __LINE__);
    flogerror("Reset frame number to 0 (because called with frame_number_increment < 0)");
    setIntKeyword("framenum", 0, BROADCAST_TRUE);
  } else if (frame_number_increment == 0) {
    log_msg( SINFO, LOGLVL_USER4, "%s %d: not incrementing frame number.", __FILE__, __LINE__);
  } else {
    log_msg( SINFO, LOGLVL_USER4, "%s %d: incrementing frame number.", __FILE__, __LINE__);
    getIntKeyword("framenum", &int1);
    flogerror("Increment frame number from %d to %d", int1, int1+frame_number_increment);
    setIntKeyword("framenum", int1+frame_number_increment, BROADCAST_TRUE);
  }
  
  /* add path and extension */
  getStringKeyword("outdir", &str1);
  getStringKeyword("dirsep", &str2);
  getStringKeyword("extension", &str3);
  getStringKeyword("tempfitsext", &str4);
  
  log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking if outdir %s is writable.", __FILE__, __LINE__, str1);
  /* check writability of outdir */  
  if ( sharcs_writecheck(str1) != SHARCS_FILECHECK_OK) {
    /* just return for now.  filechecking will be done
       again during exposure */
    return -1;
  }

  log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking frame number", __FILE__, __LINE__);
  fileok = -1;
  /* check for available frame number */
  while (fileok != SHARCS_FILECHECK_OK) {
    /* if frame number is not in range 0 .. 99999, flip it to 0 */
    getIntKeyword("framenum", &int1);
    if (int1 < 0 || int1 >= 99999) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: frame number out of range. Resetting to 0.", __FILE__, __LINE__);
      setIntKeyword("framenum", 0, BROADCAST_TRUE);
    }

    /* construct root of filename */
    snprintf(fileroot, FILENAME_LENGTH, "%c%04d", 
	     instr_char, int1);

    snprintf(fullfile, SHARCS_FILENAME_LENGTH, "%s%s%s.%s", 
	     str1, str2, fileroot, str3);

    snprintf(fullfilex, SHARCS_FILENAME_LENGTH, "%s%s%s.%s%s", 
	     str1, str2, fileroot, str3, str4);

    /* check for file existence */
    /* TODO: provide check for all 10000 frame nums used */
    log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking for file existence of %s...", __FILE__, __LINE__, fullfilex);
    if ((filexok=sharcs_filecheck(fullfilex)) < 0) {
      /* if file exists, increment frame num */
      log_msg( SINFO, LOGLVL_USER1, "%s %d: %s exists. Incrementing filename.", __FILE__, __LINE__, fullfilex);
      getIntKeyword("framenum", &int1);
      setIntKeyword("framenum", int1+1, BROADCAST_TRUE);
    } else {
      log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking for file existence of %s...", __FILE__, __LINE__, fullfile);
      if ((fileok=sharcs_filecheck(fullfile)) < 0) {
	/* if file exists, increment frame num */
	log_msg( SINFO, LOGLVL_USER1, "%s %d: %s exists. Incrementing filename.", __FILE__, __LINE__, fullfile);
	getIntKeyword("framenum", &int1);
	setIntKeyword("framenum", int1+1, BROADCAST_TRUE);
      }
    }
  }
  
  /* set keyword values */
  log_msg( SINFO, LOGLVL_USER1, "%s %d: Using %s as filename.", __FILE__, __LINE__, fullfile);
  setStringKeyword("datafile", fileroot, BROADCAST_TRUE);

  log_msg(SINFO, LOGLVL_USER2, "%s %d: exiting %s", __FILE__, __LINE__, __FUNCTION__);

  return 0;
}

int sharcs_make_palette_entry(
    struct ExposureConfiguration *pExp)
{
    int *palette_nexp;
    int *palette_resets;
    int *palette_nreads;
    int *palette_coadds;
    int *palette_exptime;
    int empty_slot;
    int size, size_nexp;

    int integTime;

    int i;

    if (getIntArrayKeyword("PALCOUNT", &palette_nexp, &size_nexp) < 0) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: Failed to find PALCOUNT kwd",
	__FILE__, __LINE__);
	return -1;
    }

    if (getIntArrayKeyword("PALRESETS", &palette_resets, &size) < 0) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: Failed to find PALRESETS kwd",
	__FILE__, __LINE__);
	return -1;
    } else if (size != size_nexp) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: PALRESETS size=%d, "
		"but PALCOUNT size=%d!", __FILE__, __LINE__, size, size_nexp);
	return -1;
    }

    if (getIntArrayKeyword("PALREADS", &palette_nreads, &size) < 0) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: Failed to find PALREADS kwd",
	__FILE__, __LINE__);
	return -1;
    } else if (size != size_nexp) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: PALREADS size=%d, "
		"but PALCOUNT size=%d!", __FILE__, __LINE__, size, size_nexp);
	return -1;
    }

    if (getIntArrayKeyword("PALCOADDS", &palette_coadds, &size) < 0) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: Failed to find PALCOADDS kwd",
	__FILE__, __LINE__);
	return -1;
    } else if (size != size_nexp) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: PALCOADDS size=%d, "
		"but PALCOUNT size=%d!", __FILE__, __LINE__, size, size_nexp);
	return -1;
    }

    if (getIntArrayKeyword("PALEXPTIME", &palette_exptime, &size) < 0) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: Failed to find PALEXPTIME kwd",
	__FILE__, __LINE__);
	return -1;
    } else if (size != size_nexp) {
	log_msg( SINFO, LOGLVL_USER1, "%s %d: PALEXPTIME size=%d, "
		"but PALCOUNT size=%d!", __FILE__, __LINE__, size, size_nexp);
	return -1;
    }

    /* was:
     * integTime = pExp->integrationTime;
     */
    /* was:
     * integTime = (int) (totaltime/1000. + 0.5);
     */
    integTime = pExp->integrationTime;

    /* Find an existing entry, else create a new one. */
    for (i=0, empty_slot=-1; i < size_nexp; i++) {
	if ( palette_resets[i] == pExp->numberOfResets &&
		    palette_nreads[i] == pExp->numberOfReads &&
		    palette_coadds[i] == pExp->numberOfCoadds &&
		    palette_exptime[i] == integTime ) {
	    // This one matches us.
log_msg( SINFO, LOGLVL_USER1, "Incremented palette count in slot %d", i);
	    palette_nexp[i]++;
	    key_sendMonitor(NULL, NULL, 0L, "PALCOUNT", NULL, 0);
	    return 0;
	} else if ( empty_slot < 0 &&
		    palette_nexp[i] == 0 &&
		    palette_resets[i] == 0 && palette_nreads[i] == 0 &&
		    palette_coadds[i] == 0 && palette_exptime[i] == 0 ) {
	    // This is the first empty slot we found.
log_msg( SINFO, LOGLVL_USER1, "first empty palette slot is %d", empty_slot);
	    empty_slot = i;
	}
    }
    // If here, we didn't find our entry.  Is there an all-zeros
    // line that we can use?
    if (empty_slot >= 0) {
	// Yes, use it.
	palette_resets[empty_slot] = pExp->numberOfResets;
	palette_nreads[empty_slot] = pExp->numberOfReads;
	palette_coadds[empty_slot] = pExp->numberOfCoadds;
	palette_exptime[empty_slot] = integTime;
	palette_nexp[empty_slot] = 1;
	key_sendMonitor(NULL, NULL, 0L, "PALRESETS", NULL, 0);
	key_sendMonitor(NULL, NULL, 0L, "PALREADS", NULL, 0);
	key_sendMonitor(NULL, NULL, 0L, "PALCOADDS", NULL, 0);
	key_sendMonitor(NULL, NULL, 0L, "PALEXPTIME", NULL, 0);
	key_sendMonitor(NULL, NULL, 0L, "PALCOUNT", NULL, 0);
	return 0;
    }

    // If here, we didn't find a suitable slot.
    log_msg( SINFO, LOGLVL_USER1, "%s %d: "
    		"All %d exposure palette slots already in use; "
		"no slot available for exptime=%d, nreads=%d, resets=%d!",
		__FILE__, __LINE__, size_nexp,
		integTime, pExp->numberOfReads,
		pExp->numberOfResets);
    return -1;
}

int sharcs_filecheck(char *fullfile) {
  int err;
  int retval;
  char *convertedFilename;
  char fileCopy[SHARCS_FILENAME_LENGTH];
  char *hostDataRoot;
  char *winDataRoot;
  char *filePtr;
  

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s", __FILE__, __LINE__, __FUNCTION__);

  log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking validity of filename %s.", __FILE__, __LINE__, fullfile);

  strncpy(fileCopy, fullfile, SHARCS_FILENAME_LENGTH);
  filePtr = fileCopy;

  if (fullfile[0] == '/') {
    getStringKeyword("unixrootdir", &hostDataRoot);
    if (sharcs_removeRoot(&filePtr, hostDataRoot) < 0) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Error converting path to target format.  File %s does not start with host data root directory %s", __FILE__, __LINE__, fullfile, hostDataRoot);
      return SHARCS_FILECHECK_INVALID_DATAROOT;
    }
  }

  getStringKeyword("winrootdir", &winDataRoot);

  convertedFilename = sharcs_convertPath(filePtr, winDataRoot, '/', '\\');

  /* return true if file does not exist and dir is writable */
  if ((err = sharcs_sidecar_validate_filename(convertedFilename)) == 0) {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: Filename %s does not exist and directory is writable.", __FILE__, __LINE__, fullfile);
    retval = SHARCS_FILECHECK_OK;
  } else {
    if (err == SIDECAR_VALIDATE_FILENAME_FILE_EXISTS_READ_ONLY) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Filename %s exists and is not writable.", __FILE__, __LINE__, fullfile);
      retval = SHARCS_FILECHECK_FILE_EXISTS_READ_ONLY;
    } else if (err == SIDECAR_VALIDATE_FILENAME_DIRECTORY_NOT_WRITEABLE) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Filename %s directory is not writable.", __FILE__, __LINE__, fullfile);
      retval = SHARCS_FILECHECK_DIRECTORY_NOT_WRITEABLE;
    } else if (err == SIDECAR_VALIDATE_FILENAME_DIRECTORY_DOESNT_EXIST) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Filename %s directory does not exist.", __FILE__, __LINE__, fullfile);
      retval = SHARCS_FILECHECK_DIRECTORY_DOESNT_EXIST;
    } else {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Filename %s not valid for unknown reasons.", __FILE__, __LINE__, fullfile);
      retval = SHARCS_FILECHECK_INVALID_UNKNOWN;
    }
  }
  log_msg(SINFO, LOGLVL_USER2, "%s %d: exiting %s", __FILE__, __LINE__, __FUNCTION__);

  return retval;
}

int sharcs_writecheck(char *fullfile) {
  /* right now, function is only used for directory 
     i.e. checking outdir. append a dirsep if it 
     doesn't already end with one */
  char *sep;
  char fileCopy[SHARCS_FILENAME_LENGTH];
  char *filePtr;
  char *convertedFilename;
  char *hostDataRoot;
  char *winDataRoot;
  int retval;
  int err;

  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s", __FILE__, __LINE__, __FUNCTION__);

  strncpy(fileCopy, fullfile, SHARCS_FILENAME_LENGTH);

  /* make a copy of fullfile, so it isn't altered by strcat */
  getStringKeyword("dirsep", &sep);

  if (strcmp(fileCopy+(strlen(fileCopy)-strlen(sep)), sep) != 0) {

    strcat(fileCopy, sep);

  }
  filePtr = fileCopy;

  if (fullfile[0] == '/') {
    getStringKeyword("unixrootdir", &hostDataRoot);
    if (sharcs_removeRoot(&filePtr, hostDataRoot) < 0) {
      log_msg( SINFO, LOGLVL_USER1, "%s %d: Error converting path to target format.  File %s does not start with host data root directory %s", __FILE__, __LINE__, fullfile, hostDataRoot);
      return SHARCS_FILECHECK_INVALID_DATAROOT;
    }
  }
  getStringKeyword("winrootdir", &winDataRoot);
  log_msg( SINFO, LOGLVL_USER4, "%s %d: got winrootdir=%s.", __func__, __LINE__, winDataRoot);

  convertedFilename = sharcs_convertPath(filePtr, winDataRoot, '/', '\\');

  log_msg( SINFO, LOGLVL_USER4, "%s %d: Checking validity of directory %s.", __FILE__, __LINE__, convertedFilename);

  /* return true if file does not exist and dir is writable */
  if ((err = sharcs_sidecar_validate_filename(convertedFilename)) < 0) {
    if (err == SIDECAR_ERROR_ICE_ERROR) {
      errlog(SERROR, "%s %d: %s", __FILE__, __LINE__, SidecarErrMsg[SIDECAR_ERROR_CODE0-err]);
      return SHARCS_ERROR_ICE_ERROR;
    }
    log_msg( SINFO, LOGLVL_USER1, "%s %d: Directory %s is not writable.", __FILE__, __LINE__, convertedFilename);
    retval = SHARCS_FILECHECK_DIRECTORY_NOT_WRITEABLE;
  } else {
    log_msg( SINFO, LOGLVL_USER1, "%s %d: Directory %s is writable.", __FILE__, __LINE__, convertedFilename);
    retval = SHARCS_FILECHECK_OK;
  }
  log_msg(SINFO, LOGLVL_USER2, "%s %d: exiting %s", __FILE__, __LINE__, __FUNCTION__);

  return retval;

}


/*************************************************************************/

int sharcsCheckForRequiredKeywords(char *keyword) {

#ifndef MR_SERVER
  KEYWORD_INFO *info;
#endif
  
  log_msg( SINFO, LOGLVL_USER2, "%s %d: entering %s", __FILE__, __LINE__, __FUNCTION__);

  CHECK_FOR_KEYWORD( "addcsuext", info, keyword);
  CHECK_FOR_KEYWORD( "asiccfg", info, keyword);
  CHECK_FOR_KEYWORD( "asichost", info, keyword);
  CHECK_FOR_KEYWORD( "asicport", info, keyword);
  CHECK_FOR_KEYWORD( "asicpwr", info, keyword);
  CHECK_FOR_KEYWORD( "calcreads", info, keyword);
  CHECK_FOR_KEYWORD( "coadds", info, keyword);
  CHECK_FOR_KEYWORD( "connect", info, keyword);
  CHECK_FOR_KEYWORD( "connected", info, keyword);
  CHECK_FOR_KEYWORD( "coaddsdone", info, keyword);
  CHECK_FOR_KEYWORD( "csuextname", info, keyword);
  CHECK_FOR_KEYWORD( "datafile", info, keyword);
  CHECK_FOR_KEYWORD( "dlfirmware", info, keyword);
  CHECK_FOR_KEYWORD( "dirsep", info, keyword);
  CHECK_FOR_KEYWORD( "elapsedtime", info, keyword);
  CHECK_FOR_KEYWORD( "endx", info, keyword);
  CHECK_FOR_KEYWORD( "endy", info, keyword);
  CHECK_FOR_KEYWORD( "exposing", info, keyword);
  CHECK_FOR_KEYWORD( "extension", info, keyword);
  CHECK_FOR_KEYWORD( "filename", info, keyword);
  CHECK_FOR_KEYWORD( "framenum", info, keyword);
  CHECK_FOR_KEYWORD( "getkw", info, keyword);
  CHECK_FOR_KEYWORD( "getACSkw", info, keyword);
  CHECK_FOR_KEYWORD( "getDCSkw", info, keyword);
  CHECK_FOR_KEYWORD( "groups", info, keyword);
  CHECK_FOR_KEYWORD( "groupsdone", info, keyword);
  CHECK_FOR_KEYWORD( "heartbeat", info, keyword);
  CHECK_FOR_KEYWORD( "heartbeatperiod", info, keyword);
  CHECK_FOR_KEYWORD( "ignoredreads", info, keyword);
  CHECK_FOR_KEYWORD( "imagedone", info, keyword);
  CHECK_FOR_KEYWORD( "imagetype", info, keyword);
  CHECK_FOR_KEYWORD( "itime", info, keyword);
  CHECK_FOR_KEYWORD( "lastfile", info, keyword);
  CHECK_FOR_KEYWORD( "maxutrreads", info, keyword);
  CHECK_FOR_KEYWORD( "minutrrditime", info, keyword);
  CHECK_FOR_KEYWORD( "numreads", info, keyword);
  CHECK_FOR_KEYWORD( "outdir", info, keyword);
  CHECK_FOR_KEYWORD( "progress", info, keyword);
  CHECK_FOR_KEYWORD( "readsdone", info, keyword);
  CHECK_FOR_KEYWORD( "readtime", info, keyword);
  CHECK_FOR_KEYWORD( "ready", info, keyword);
  CHECK_FOR_KEYWORD( "remainingtime", info, keyword);
  CHECK_FOR_KEYWORD( "resets", info, keyword);
  CHECK_FOR_KEYWORD( "sampmode", info, keyword);
  CHECK_FOR_KEYWORD( "status", info, keyword);
  CHECK_FOR_KEYWORD( "startx", info, keyword);
  CHECK_FOR_KEYWORD( "starty", info, keyword);
  CHECK_FOR_KEYWORD( "testfile", info, keyword);
  CHECK_FOR_KEYWORD( "testdir", info, keyword);
  CHECK_FOR_KEYWORD( "timerperiod", info, keyword);
  CHECK_FOR_KEYWORD( "timfile", info, keyword);
  CHECK_FOR_KEYWORD( "tempfitsext", info, keyword);
  CHECK_FOR_KEYWORD( "unixrootdir", info, keyword);
  CHECK_FOR_KEYWORD( "winrootdir", info, keyword);

  log_msg(SINFO, LOGLVL_USER2, "%s %d: exiting %s", __FILE__, __LINE__, __FUNCTION__);

  return 0;
}

/* --------------------------------------------------------------------- */

static void
logAndSetErrstr(
    const char *keyword,
    int errcode,
    KTL_SUPERPOLY *data,	/* Can be null, in which case error
    				 * is logged but no attempt to assign to poly.
				 */
    const char *file,
    int line)
{
    char m_status[1000];
    char *d_status;
    char c_status[2000];

    char *old_mval;
    char *old_dval;

    if (data) {
	data->errstr = SHARCSErrMsg[SHARCS_ERROR_CODE0 - errcode];
    }
    errlog(SERROR, "%s %d: %s", file, line,
			   SHARCSErrMsg[SHARCS_ERROR_CODE0 - errcode]);

    getStringKeyword("statusmain", &old_mval);
    if (*old_mval && data) {
	snprintf(m_status, sizeof(m_status), "Error setting keyword %s: %s ; %s",
			keyword, data->errstr, old_mval);
    } else if (*old_mval) {
	snprintf(m_status, sizeof(m_status), "Error setting keyword %s: %s",
			keyword, old_mval);
    } else if (data) {
	snprintf(m_status, sizeof(m_status), "Error setting keyword %s: %s",
	    				keyword, data->errstr);
    } else {
	snprintf(m_status, sizeof(m_status), "Error setting keyword %s", keyword);
    }
    sharcs_set_main_status(m_status);

    getStringKeyword("statusdetail", &old_dval);
    if (*old_dval) {
	// just use the existing detail value.
	d_status = old_dval;
    } else {
	d_status = NULL;
	sharcs_set_detailed_status(ktl_get_errtxt());
    }

    snprintf(c_status, sizeof(c_status), "%s: %s", m_status,
				    d_status ? d_status : ktl_get_errtxt());
    sharcs_set_status(c_status);
}

static void clear_status_keywords()
{
    sharcs_set_main_status("");
    sharcs_set_detailed_status("");
    sharcs_set_status("");
}

/*
 * Save readout window.
 */
void save_window(
    int startx,
    int starty,
    int endx,
    int endy)
{
    last_window_info.firstTime = 0;
    last_window_info.startx = startx;
    last_window_info.starty = starty;
    last_window_info.endx = endx;
    last_window_info.endy = endy;
}

/*
 * Return 1 [0] if last readout window was [was not] full frame.
 */
int was_full_frame()
{
    return is_full_frame(last_window_info.startx,
    			last_window_info.starty,
    			last_window_info.endx,
    			last_window_info.endy);
}

/*
 * Return 1 [0] if this readout window is [is not] full frame.
 */
int is_full_frame(
    int startx,
    int starty,
    int endx,
    int endy)
{
    return ((endx + endy + 2 - startx - starty) == 4096);
}

/*
 * Return 1 [0] if this readout window is [is not]
 * different from the last saved readout window.
 */
int window_changed(
    int startx,
    int starty,
    int endx,
    int endy)
{
    if (last_window_info.firstTime) {
	return 1;
    }

    if (startx != last_window_info.startx || starty != last_window_info.starty ||
	    endx != last_window_info.endx || endy != last_window_info.endy) {
	return 1;
    }
    return 0;
}
