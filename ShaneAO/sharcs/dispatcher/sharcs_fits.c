/*************************************************************************
 Module:	sharcs_fits.c

 Original Author:	Jason Weiss
 Original Date:		2007/10/26

 MUSIC-compatible Author:	Will Deich
 Date:		2014/3/17


 Description:  	This module contains routines that handle FITS I/O
*************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <syslog.h>
#include <errno.h>
#include <fcntl.h>
#include <time.h>

#include "fitsio/fitsio.h"

#include "ktl.h"
#include "ktl_keyword.h"

#include "sharcs.h"
#include "sharcs_sidecar.h"
#include "sharcs_fits.h"

#ifdef MR_SERVER
    #include "mr_server/mr_server.h"
#else
    #include "rpcKey.h"
    #include "rpc_if.h"            /* for LOGLVL */
    #include "util_prototypes.h"   /* for log_msg */
#endif

extern volatile int64 rampStartTime;  /* in sharcs_if.c */
extern volatile int64_t dateBeg;	/* in sharcs_if.c */

static int splittab(char *str, char **field, int maxfields);

/*
 *  Global variables
 */
extern int  Simulate;
extern char *MyService;

pthread_mutex_t fitsfile_mutex = PTHREAD_MUTEX_INITIALIZER;

FITS_HEAD_CARD *headerKeyList = NULL;

/*
 * Call this if both value and comment are malloc'd.
 * (Compare with freeKeys(), which only free()'s the value.)
 */
void freeHeaderKeys(struct FitsHeaderKey *keys, int numKeys) {

    int i;

    for (i=0; i<numKeys; i++) {
	if ( keys[i].value != NULL) {
	    free(keys[i].value);
	}
	if ( keys[i].comment != NULL) {
	    free(keys[i].comment);
	}
    }
}

void *
sharcs_fits_header(
    void *pExposure_params)
{

    int errcode=0;
    struct FitsHeaderKey *keys = NULL;
    char fitsRecord[4000];
    int nAllocated = 0;
    int numKeys = 0;
    int nBadCards;
    int nTotalCards = 0;
#define MAXFIELD 5
    char *field[MAXFIELD];
    int nf;

    struct ExposureConfiguration *exp = (void *) pExposure_params;

    FILE *keyhdr;
    static char *keyheader_command = "keyheader -c " KEYHEADER_CONF_FILE
			" -m -s ExposureBegin -t now";

    errlog(SINFO, "Entering %s.", __FUNCTION__);

    log_msg(SINFO, LOGLVL_USER4, "%s %d: Assembling FITS header...",
    					__FILE__, __LINE__);

    /* Collect the keywords that are monitored by the 'keyheader' application,
     * which returns card images upon request:
     * Start the keyheader application immediately, since it takes relatively
     * long to run.
     */
    keyhdr = popen(keyheader_command, "r");
    if (!keyhdr) {
	errcode = SHARCS_FITS_ERROR_RETRIEVING_KEYWORDS;
	errlog(SERROR, "%s %d: %s", __FILE__, __LINE__,
			SHARCSFitsErrMsg[SHARCS_FITS_ERROR_CODE0-errcode]);
    } else {
	nAllocated = 100;
	keys = (struct FitsHeaderKey *)calloc(nAllocated,
					sizeof(struct FitsHeaderKey));
	if (!keys) {
	    nAllocated = 0;
	    errlog(SERROR, "%s %d: failed to realloc to (n=%d * size=%d)",
			__FILE__, __LINE__, nAllocated,
			sizeof(struct FitsHeaderKey));
	    errcode = SHARCS_FITS_ERROR_GETTING_LOCAL_FITS_KEYWORDS;
	}
    }

    {
	/* While keyheader is running, insert rampStartTime
	 * and optionally dateBeg.  That way, we'll always have those
	 * keywords, even if the keyheader output fails to be included
	 * because it arrives late.
	 * This may not be known for a bit of time;
	 * wait for up to 1 second, then give up.
	 */
	struct FitsHeaderKey SomeKeys[10];
	char buf0[100], buf1[100];
	clock_t wait_start;
	struct timeval sleeptime;
	struct tm *tm_p;
	time_t t;
	int nwait, millis;

	wait_start = clock();
	nwait = 0;
	millis = 1;
	while (rampStartTime < 0 && (clock() - wait_start) < CLOCKS_PER_SEC) {
	    nwait++;
	    sleeptime.tv_sec = 0;
	    sleeptime.tv_usec = millis * 1000;
	    select(0, NULL, NULL, NULL, &sleeptime);
	}
	errlog(SINFO, "(Waited %d ms for rampStartTime to be available)", nwait * millis);

	// Convert rampStartTime to seconds and millis.
	if (rampStartTime > 0) {
	    t = (time_t) (rampStartTime / 1000);
	    millis = rampStartTime % 1000;
	} else {
	    // didn't get rampStartTime.
	    t = 0;
	    millis = 0;
	}

	// Format it into FITS timestamp.
	tm_p = gmtime(&t);
	strftime(buf0, sizeof(buf0), "%Y-%m-%dT%T", tm_p);
	sprintf(buf0+strlen(buf0), ".%03d", millis);

	// And stuff into a struct for the sidecar server.
	SomeKeys[0].type = KeyTypeString;
	SomeKeys[0].name = "RMPSTART";
	SomeKeys[0].value = buf0;
	SomeKeys[0].comment = "Ramp start time, UT";

	// Convert dateBeg to seconds and millis.
	if (dateBeg > 0) {
	    t = (time_t) (dateBeg / 1000);
	    millis = dateBeg % 1000;
	} else {
	    // didn't get dateBeg.
	    t = 0;
	    millis = 0;
	}

	// Format it into FITS timestamp.
	tm_p = gmtime(&t);
	strftime(buf1, sizeof(buf1), "%Y-%m-%dT%T", tm_p);
	sprintf(buf1+strlen(buf1), ".%03d", millis);

	SomeKeys[1].type = KeyTypeString;
	SomeKeys[1].name = "DATE-BEG";
	SomeKeys[1].value = buf1;
	SomeKeys[1].comment = "Beginning of exposure, UT";

	sharcs_sidecar_addFitsHeader_KeyBased(exp->fitsFilename,
							&SomeKeys[0], 2);
    }

    while (keys && fgets(fitsRecord, sizeof(fitsRecord), keyhdr)) {
	nTotalCards++;
	// Strip newline at end of record.
	char *s = fitsRecord + strlen(fitsRecord);	// pts to nul char
	if (s > fitsRecord && *(s-1) == '\n') {
	    *(s-1) = '\0';
	} else {
	}
	if (nAllocated <= numKeys) {
	    nAllocated *= 2;
	    keys = realloc(keys, nAllocated * sizeof(struct FitsHeaderKey));
	    if (!keys) {
		errlog(SERROR, "%s %d: failed to realloc to (n=%d * size=%d)",
			__FILE__, __LINE__, nAllocated,
			sizeof(struct FitsHeaderKey));
		errcode=SHARCS_FITS_ERROR_GETTING_LOCAL_FITS_KEYWORDS;
		continue;
	    }
	}
	/* Now expect either
	 *	Comment\tCOMMENT\t<text>
	 * or
	 *	<dataType>\t<keyword>\t<value>\t<remark>
	 */
	nf = splittab(fitsRecord, field, MAXFIELD);
	if (strcmp("Comment", field[0]) == 0) {
	    keys[numKeys].type = KeyTypeComment;
	    keys[numKeys].name = field[1] ? strdup(field[1]) : strdup("");
	    keys[numKeys].value = strdup("");
	    keys[numKeys].comment = field[2] ? strdup(field[2]) : strdup("");
	    if (nf != 3) {
		errlog(SERROR, "%s %d: should only have 3 fields in "
				"COMMENT from keyheader, but got %d",
			__FILE__, __LINE__, nf);
		nBadCards++;
		continue;
	    } else if (!field[1]) {
		errlog(SERROR, "%s %d: Comment is missing 'COMMENT' keyword",
			__FILE__, __LINE__);
		nBadCards++;
		continue;
	    }
	} else {
	    if (strcmp("Boolean", field[0]) == 0) {
		keys[numKeys].type = KeyTypeBoolean;
	    } else if (strcmp("Integer", field[0]) == 0) {
		keys[numKeys].type = KeyTypeInteger;
	    } else if (strcmp("Float", field[0]) == 0) {
		keys[numKeys].type = KeyTypeFloat;
	    } else if (strcmp("Double", field[0]) == 0) {
		keys[numKeys].type = KeyTypeDouble;
	    } else if (strcmp("String", field[0]) == 0) {
		keys[numKeys].type = KeyTypeString;
	    } else {
		errlog(SERROR, "%s %d: "
				"Invalid FITS header card datatype \"%s\".",
					__FILE__, __LINE__, field[0]);
		nBadCards++;
		continue;
	    }
	    keys[numKeys].name = field[1] ? strdup(field[1]) : strdup("");
	    keys[numKeys].value = field[2] ? strdup(field[2]) : strdup("");
	    keys[numKeys].comment = field[3] ? strdup(field[3]) : strdup("");
	    if (!field[1]) {
		errlog(SERROR, "%s %d: FITS keyword is missing the name field",
			__FILE__, __LINE__);
		nBadCards++;
		continue;
	    }
	    if (!field[2]) {
		errlog(SERROR, "%s %d: "
			"FITS keyword '%s' is missing the value field",
			__FILE__, __LINE__, keys[numKeys].name);
		nBadCards++;
		continue;
	    }
	    if (nf != 4) {
		errlog(SERROR, "%s %d: "
			    "type=<%d> key=<%s> value=<%s> comment=<%s> : "
			    "should only have 4 fields in "
			    "COMMENT from keyheader, but got %d",
			__FILE__, __LINE__, keys[numKeys].type,
			keys[numKeys].name, keys[numKeys].value,
			keys[numKeys].comment, nf);
		continue;
	    }
	    if (keys[numKeys].type == KeyTypeString) {
		/* Limit string length to 68 characters */
		if (strlen(keys[numKeys].value) > 68) {
		    keys[numKeys].value[68] = '\0';
		}
	    }
	}
	numKeys++;
    }
    if (keyhdr) {
	fclose(keyhdr);
    }

    if ( numKeys > 0 && (!exp->headerTest) ) {
	log_msg(SINFO, LOGLVL_USER1, "%s %d: %s",
			__FILE__, __LINE__, "FITS header assembled.");
	sharcs_sidecar_addFitsHeader_KeyBased(
			    exp->fitsFilename, keys, numKeys);
    }
    nBadCards = nTotalCards - numKeys;
    setIntKeyword("nc_invalid", nBadCards, BROADCAST_TRUE);
    setIntKeyword("nc_keyheader", numKeys, BROADCAST_TRUE);

    if (keys) {
	freeHeaderKeys(keys, numKeys);
	free(keys);
    }

    free(pExposure_params);

    log_msg( SINFO, LOGLVL_USER3, "%s %d: exiting %s.",
			__FILE__, __LINE__, __FUNCTION__);

    errlog(SINFO, "Exiting %s via pthread_exit().", __FUNCTION__);
    pthread_exit((void *) ((long) errcode));
}

/*
 * Split a string on tabs: replace tabs with nul's,
 * a put pointers to the fields into field[].
 * Don't return more than maxfields.
 */
static int
splittab(
    char *str,
    char **field,
    int maxfields)
{
    char *p, *q;
    int i = 0;
    for (p=str, q=strchr(p, '\t'); q && i < maxfields; q=strchr(p, '\t')) {
	*q = '\0';
	field[i++] = p;
	p = q+1;
    }
    if (i < maxfields) {
	field[i++] = p;
    }
    return i;
}

