/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarIceValuelistener.h
 *
 *  @brief
 *  Header file specific to the Sidecar ICE Valuelistener.
 *
 *  This module contains function prototypes for the Sidecar ICE ValueListener
 *  pure abstract class.
 *
 *  @sa SidecarIceValueListenerAdapter.cpp
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _SIDECAR_ICE_VALUELISTENER_H_
#define _SIDECAR_ICE_VALUELISTENER_H_

#ifdef __cplusplus

#include <string>

using namespace std;

class SidecarIceValueListener {
  public:
  virtual ~SidecarIceValueListener() {};
  virtual void setValue(string name, string value) const = 0;
};

#endif /* __cplusplus */

#endif /* _SIDECAR_ICE_VALUELISTENER_H_ */
