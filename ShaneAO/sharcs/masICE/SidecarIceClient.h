/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarIceClient.h
 *
 *  @brief
 *  Header file specific to the Sidecar ICE client.
 *
 *  This module contains function prototypes for the Sidecar ICE client
 *  for both c and c++.
 *
 *  @sa SidecarAdapter.c
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _GPIFSIDECAR_ICE_CLIENT_H_
#define _GPIFSIDECAR_ICE_CLIENT_H_

#include <Sidecar.h>
#include <inttypes.h>

#define ERROR_SIDECAR_EXCEPTION_STRING   "Error:"

#define ERROR_SIDECAR_ICE_BASE                           -4000
#define ERROR_SIDECAR_ICE_TIMEOUT   ERROR_SIDECAR_ICE_BASE - 1
#define ERROR_SIDECAR_ICE_GENERIC   ERROR_SIDECAR_ICE_BASE - 2
#define ERROR_SIDECAR_ICE_UNKNOWN   ERROR_SIDECAR_ICE_BASE - 3


#ifdef __cplusplus

#include <IceUtil/IceUtil.h>
#include <Ice/Ice.h>
#include <SidecarIce.h>
#include <SidecarIceCallback.h>
#include <SidecarIceValueListener.h>
#include <SidecarIceValueListenerAdapter.h>

using namespace std;
using namespace Sidecar::SidecarIce;


void *callbackThread(void *obj);
class SidecarIceClient {
private:
  Ice::CommunicatorPtr ic;
  ISidecarIceComPrx sidecarProxy;
  ISidecarIceCallbackPrx callbackProxy;
  SidecarIceCallback callback;
  static SidecarIceClient *single;
  int proxyTimeout;
  SidecarIceClient();
  SidecarIceValueListenerAdapter valueListenerAdapter;
  bool isCallbackThreadRunning; /* volatile? */
  bool getNewProxy;
  bool callbackThreadInitializing;
 public:
     
     ~SidecarIceClient();
     static SidecarIceClient* getInstance();
     void setCallbackThreadRunning(bool status);
     void server_shutdown();
     friend void *callbackThread(void *obj);
     const char *echo(const char *s);
     void addCallbackListener(struct ValueListener *l);
    int connectToServer(const char *hostname, unsigned int port);
    int setExposureExtents(const char *sJade, const char *sAsic, int startx, int starty, int endx, int endy);
    int takeExposure(const char *sJade, const char *sAsic, int resets, uint64_t itimeInMicros, int coadds, int sampMode, int reads, int groups, const char *filename);
    int abortExposure(int abortLevel, bool saveImage);
    int validateFilename(const char *sFilename);
    int resetASIC(const char *sJade);
    int powerDownASIC(const char *sJade);
    int addFitsHeader(const char *fitsFilename, struct FitsHeaderKey *keys, int numKeys);
    int broadcastValue(const char *sValueName);
    int getReadTimeInMicroseconds(const char *sJade, const char *sAsic);
    const char *initializeSidecar(const char *sJade, const char *sAsic, bool downloadFirmware);
    int readSidecarControlConfiguration(const char *configFile);
    int setReferencePixelSubtractionType(int type);
    int writeRawFrames(bool enable);
    const char *downloadAllConfiguredAsics(const char *sJade, bool mcds, bool registers);
    const char *downloadASICRegisterFile(const char *sJade, const char *sAsic, const char *sFilename);
    const char *downloadASICRegisters(const char *sJade, const char *sAsic);
    const char *downloadJADEFirmware(const char *sJade);
    const char *downloadJADEFirmwareFile(const char *sJade, const char *sFilename);
    const char *downloadJADERegisterFile(const char *sJade, const char *sFilename);
    const char *downloadJADERegisters(const char *sJade);
    const char *downloadMCD(const char *sJade, const char *sAsic);
    const char *downloadMCDFile(const char *sJade, const char *sAsic, const char *sMCDFilename);
    const char *getAsics(const char *sJade);
    int getAutoWrite();
    const char *getAvailableAsics(const char *sJade);
    const char *getAvailableJades();
    int getDMA();
    const char *getJades();
    int getMemoryBlockMode();
    const char *openApp(const char *appFilename);
    long readAsicReg(const char *sJade, const char *sAsic, long addr, bool is24bit);
    const char *readAsicRegister(const char *sJade, const char *sAsic, const char *sMem, bool is24bit); 
    const char *readAsicRegisterBlock(const char *sJade, const char *sAsic,  bool is24bit, bool FIFOreset, int *addrArray, long *valueArray, int blockSize);
    long readJadeReg(const char *sJade, long addr);
    const char *readJadeRegister(const char *sJade, const char *sMem);
    const char *readScienceData(const char *sJade, int numWords, int *targetArray);
    long readUSBPort(const char *sJade, long address);
    const char *readUSBPortDirectionAsString(const char *sJade, const char *addr);
    long readUSBPortDirectionAsLong(const char *sJade, long addr);
    const char *setAutoWrite(int state);
    const char *setBlockMemoryMode(int mode);
    const char *setDMA(int state);
    const char *uploadASICRegisters(const char *sJade, const char *sAsic);
    const char *uploadJADERegisters(const char *sJade);
    const char *writeAsicReg(const char *sJade, const char *sAsic, long addr, long value);
    const char *writeAsicRegister(const char *sJade, const char *sAsic, const char *addr, const char *value);
    const char *writeAsicRegisterBlock(const char *sJade, const char *sAsic, bool is24bit, int *addrArray, long *valueArray, int blockSize);
    const char *writeJadeReg(const char *sJade, long addr, long value);
    const char *writeJadeRegister(const char *sJade, const char *sMem, const char *value);
    const char *writeScienceData(const char *sJade, int *targetArray, int blockSize);
    const char *writeUSBPort(const char *sJade, long address, unsigned char data);
    const char *writeUSBPortDirection(const char *sJade, long address, unsigned char data);
};
#endif

#ifdef __cplusplus
extern "C" {
#endif
  extern void sidecar_server_shutdown();
  extern int sidecar_connectToServer(const char *hostname, unsigned int port);
  extern const char *sidecar_echo(const char *echo);
  extern void sidecar_addCallbackListener(struct ValueListener *l);
  extern int sidecar_setExposureExtents(const char *sJade, const char *sAsic, int startx, int starty, int endx, int endy);
  extern int sidecar_takeExposure(const char *sJade, const char *sAsic, int resets, uint64_t itimeInMicros, int coadds, int sampMode, int reads, int groups, const char* filename);
  extern int sidecar_abortExposure(int abortLevel, int saveImage);
  extern int sidecar_validateFilename(const char *sFilename);
  extern int sidecar_resetASIC(const char *sJade);
  extern int sidecar_powerDownASIC(const char *sJade);
  extern int sidecar_addFitsHeader(const char *fitsFilename, struct FitsHeaderKey *keys, int numKeys);
  extern int sidecar_broadcastValue(const char *sValueName);
  extern int sidecar_getReadTimeInMicroseconds(const char *sJade, const char *sAsic);
  extern const char *sidecar_initializeSidecar(const char *sJade, const char *sAsic, int downloadFirmware);
  extern int sidecar_readSidecarControlConfiguration(const char *configFile);
  extern int sidecar_setReferencePixelSubtractionType(int type);
  extern int sidecar_writeRawFrames(int enable);
  extern const char *sidecar_downloadAllConfiguredAsics(const char *sJade, int mcds, int registers);
  extern const char *sidecar_downloadASICRegisterFile(const char *sJade, const char *sAsic, const char *sFilename);
  extern const char *sidecar_downloadASICRegisters(const char *sJade, const char *sAsic);
  extern const char *sidecar_downloadJADEFirmware(const char *sJade);
  extern const char *sidecar_downloadJADEFirmwareFile(const char *sJade, const char *sFilename);
  extern const char *sidecar_downloadJADERegisterFile(const char *sJade, const char *sFilename);
  extern const char *sidecar_downloadJADERegisters(const char *sJade);
  extern const char *sidecar_downloadMCD(const char *sJade, const char *sAsic);
  extern const char *sidecar_downloadMCDFile(const char *sJade, const char *sAsic, const char *sMCDFilename);
  extern const char *sidecar_getAsics(const char *sJade);
  extern int sidecar_getAutoWrite();
  extern const char *sidecar_getAvailableAsics(const char *sJade);
  extern const char *sidecar_getAvailableJades();
  extern int sidecar_getDMA();
  extern const char *sidecar_getJades();
  extern int sidecar_getMemoryBlockMode();
  extern const char *sidecar_openApp(const char *appFilename);
  extern long sidecar_readAsicReg(const char *sJade, const char *sAsic, long addr, int is24bit);
  extern const char *sidecar_readAsicRegister(const char *sJade, const char *sAsic, const char *sMem, int is24bit); 
  extern const char *sidecar_readAsicRegisterBlock(const char *sJade, const char *sAsic,  int is24bit, int FIFOreset, int *addrArray, long *valueArray, int blockSize);
  extern long sidecar_readJadeReg(const char *sJade, long addr);
  extern const char *sidecar_readJadeRegister(const char *sJade, const char *sMem);
  extern const char *sidecar_readScienceData(const char *sJade, int numWords, int *targetArray);
  extern long sidecar_readUSBPort(const char *sJade, long address);
  extern const char *sidecar_readUSBPortDirectionAsString(const char *sJade, const char *addr);
  extern long sidecar_readUSBPortDirectionAsLong(const char *sJade, long addr);
  extern const char *sidecar_setAutoWrite(int state);
  extern const char *sidecar_setBlockMemoryMode(int mode);
  extern const char *sidecar_setDMA(int state);
  extern const char *sidecar_uploadASICRegisters(const char *sJade, const char *sAsic);
  extern const char *sidecar_uploadJADERegisters(const char *sJade);
  extern const char *sidecar_writeAsicReg(const char *sJade, const char *sAsic, long addr, long value);
  extern const char *sidecar_writeAsicRegister(const char *sJade, const char *sAsic, const char *addr, const char *value);
  extern const char *sidecar_writeAsicRegisterBlock(const char *sJade, const char *sAsic, int is24bit, int *addrArray, long *valueArray, int blockSize);
  extern const char *sidecar_writeJadeReg(const char *sJade, long addr, long value);
  extern const char *sidecar_writeJadeRegister(const char *sJade, const char *sMem, const char *value);
  extern const char *sidecar_writeScienceData(const char *sJade, int *targetArray, int blockSize);
  extern const char *sidecar_writeUSBPort(const char *sJade, long address, unsigned char data);
  extern const char *sidecar_writeUSBPortDirection(const char *sJade, long address, unsigned char data);

#ifdef __cplusplus
}
#endif


#endif /* _GPIFSIDECAR_ICE_CLIENT_H_ */
