/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarIceValueListenerAdapter.h
 *
 *  @brief
 *  Header file specific to the Sidecar ICE ValueListener Adapter.
 *
 *  This module contains function prototypes for the Sidecar ICE ValueListener
 *  Adapter class.  Takes a ValueListener structure, and passes on 
 *  the setValue call to the function specified in this structure.
 *
 *  @sa SidecarAdapter.c
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _GPIFSIDECAR_ICE_VALUE_LISTENER_ADAPTER_H_
#define _GPIFSIDECAR_ICE_VALUE_LISTENER_ADAPTER_H_

#ifdef __cplusplus

#include <Sidecar.h>
#include <SidecarIceValueListener.h>

using namespace std;

class SidecarIceValueListenerAdapter : public SidecarIceValueListener {
  struct ValueListener listener;
  public:
  SidecarIceValueListenerAdapter();
  virtual ~SidecarIceValueListenerAdapter() {};
  void setValueListener(struct ValueListener *l);
  void setValue(string name, string value) const;
  struct ValueListener getValueListener();
};

#endif /* __cplusplus */

#endif /* _GPIFSIDECAR_ICE_VALUE_LISTENER_ADAPTER_H_ */
