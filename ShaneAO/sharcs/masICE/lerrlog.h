/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/** @file lerrlog.h
 *
 *  @brief 
 *  Header file with log level masks for error logging
 *
 *  This module contains log level masks for the common error logging
 *  functions of the GPI IFS software.
 *
 *  @sa lerrlog.c
 *
 *  @author Allan Honey at W.M. Keck Observatory
 */
#ifndef _LERRLOG_H
#define _LERRLOG_H

/*
 *  Logging level masks.
 */
#define LOGLVL_DEBUG1		1
#define LOGLVL_DEBUG2		2
#define LOGLVL_DEBUG3		4
#define LOGLVL_DEBUG4	        8
#define LOGLVL_DEBUG5	       16
#define LOGLVL_DEBUG6	       32
#define LOGLVL_DEBUG7	       64
#define LOGLVL_DEBUG8	      128
#define LOGLVL_USER1	      256
#define LOGLVL_USER2	      512
#define LOGLVL_USER3	     1024
#define LOGLVL_USER4	     2048
#define LOGLVL_TRIGGER1      4096
#define LOGLVL_TRIGGER2      8192
#define LOGLVL_TRIGGER3     16384
#define LOGLVL_TRIGGER4     32768

#define LOGLVL_FATAL       32
#define LOGLVL_ERROR       16
#define LOGLVL_WARN         8
#define LOGLVL_INFO         4
#define LOGLVL_DEBUG        2
#define LOGLVL_TRACE        1
#define LOGLVL_OFF          0

#define LOGLVL_ALL    LOGLVL_FATAL | LOGLVL_ERROR | \
                      LOGLVL_WARN  | LOGLVL_INFO  | \
                      LOGLVL_DEBUG | LOGLVL_TRACE

#define LOGLVL_INFO_AND_ABOVE    LOGLVL_FATAL | LOGLVL_ERROR | \
                                 LOGLVL_WARN  | LOGLVL_INFO 


#define LOGLVL_ERROR_AND_ABOVE   LOGLVL_FATAL | LOGLVL_ERROR

#define LOGLVL_NONE LOGLVL_OFF
#define LOGLVL_MIN  LOGLVL_ERROR_AND_ABOVE
#define LOGLVL_FULL LOGLVL_INFO_AND_ABOVE
#define LOGLVL_MAX  LOGLVL_ALL


#define LOGTO_STDOUT       1
#define LOGTO_STDERR       2
#define LOGTO_SYSLOG       4

/* function prototypes */
void llog_msg( 
             unsigned int  mask,
             char         *pFormat, ... /* variable arg */
  );
void setLogTarget(int target);
void setLogLevel(int level);

#endif  /* _LERRLOG_H */
