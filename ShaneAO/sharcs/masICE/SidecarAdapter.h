/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarAdapter.h
 *
 *  @brief
 *  Header file for the Sidecar Adapter hardware.
 *
 *  This module contains definitions and structures specific to the
 *  GPI IFS Sidecar Adapter hardware.
 *
 *  @sa SidecarAdapter.c
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _GPIFSIDECAR_ADAPTER_H_
#define _GPIFSIDECAR_ADAPTER_H_
  
#define SIDECAR_SUCCESSFUL_RESULT                       "Success!"
#define SIDECAR_SUCCESS_OPEN_APPLICATION     "Application opened."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_FIRMWARE \
                            "Download of jade firmware succeeded."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS \
                            "Download of jade registers complete."
#define SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE \
                            "Download MCD Succeeded"
#define SIDECAR_DEFAULT_JADE_NAME                     "JADE2"
#define SIDECAR_DEFAULT_ASIC_NAME                     "ASIC1"
#define SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS 1  /* seconds */

#define FILENAME_MAX_LENGTH 256
#define HOSTNAME_MAX_LENGTH 1024

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif


#define SIDECAR_DEFAULT_APPLICATION_FILENAME \
   "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\Slow Mode.soln" 
/* #define SIDECAR_DEFAULT_APPLICATION_FILENAME \
   "C:\\mosfire\\sidecar\\MOSFIRE_H2RG\\MOSFIRE_H2RG.soln" */
#define SIDECAR_SIMPLE_APPLICATION_FILENAME \
   "C:\\Program Files\\Rockwell Scientific\\SidecarASIC IDE\\FirstImplementationExample\\FirstImplementationExample.soln"

#define SIDECAR_DEFAULT_JADE_REGISTERS_FILENAME \
  "C:\\Program Files\\Rockwell Scientific\\SidecarASIC IDE\\FirstImplementationExample\\DefaultJadeRegisters.xml"
/* #define SIDECAR_DEFAULT_H2RG_MCD_FILENAME \
   "C:\\mosfire\\sidecar\\MOSFIRE_H2RG\\H2RG_DevBrd_Warm.mcd" */

/* OLD TIMING FILE */
/* #define SIDECAR_DEFAULT_H2RG_MCD_FILENAME				\
   "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\H2RG_DevBrd_Cold.mcd" */

/* TIMING FILE */
#define SIDECAR_DEFAULT_H2RG_MCD_FILENAME \
  "C:\\Program Files\\Teledyne Scientific\\SidecarASIC IDE\\Slow Mode\\H2RG_DevBrd_Cold_hyvisi.mcd"

#define SIDECAR_SERVER_HOSTNAME    "stealthie.ucolick.org" 
#define SIDECAR_SERVER_PORT    10051


#define SIDECAR_ERROR_BASE                                           -100
#define SIDECAR_ERROR_OPEN_APPLICATION            SIDECAR_ERROR_BASE -  1
#define SIDECAR_ERROR_JADE_NOT_AVAILABLE          SIDECAR_ERROR_BASE -  2
#define SIDECAR_ERROR_DOWNLOAD_JADE_FIRMWARE      SIDECAR_ERROR_BASE -  3
#define SIDECAR_ERROR_DOWNLOAD_JADE_REGISTERS     SIDECAR_ERROR_BASE -  4
#define SIDECAR_ERROR_ASIC_NOT_AVAILABLE          SIDECAR_ERROR_BASE -  5
#define SIDECAR_ERROR_HARDWARE_NOT_INITIALIZED    SIDECAR_ERROR_BASE -  6
#define SIDECAR_ERROR_DOWNLOAD_MCD_FILE           SIDECAR_ERROR_BASE -  7
#define SIDECAR_ERROR_TIMING_CODE_NOT_DOWNLOADED  SIDECAR_ERROR_BASE -  8
#define SIDECAR_ERROR_CREATE_FITS_FILE            SIDECAR_ERROR_BASE -  9
#define SIDECAR_ERROR_CREATE_FITS_IMAGE           SIDECAR_ERROR_BASE - 10
#define SIDECAR_ERROR_WRITE_FITS_FILE             SIDECAR_ERROR_BASE - 11
#define SIDECAR_ERROR_CLOSE_FITS_FILE             SIDECAR_ERROR_BASE - 12
#define SIDECAR_ERROR_INVALID_NUMBER_OF_RESETS    SIDECAR_ERROR_BASE - 13
#define SIDECAR_ERROR_INVALID_SAMPLING_MODE       SIDECAR_ERROR_BASE - 14
#define SIDECAR_ERROR_INVALID_ITIME               SIDECAR_ERROR_BASE - 15
#define SIDECAR_ERROR_INVALID_NUMBER_OF_COADDS    SIDECAR_ERROR_BASE - 16
#define SIDECAR_ERROR_INVALID_NUMBER_OF_READS     SIDECAR_ERROR_BASE - 17
#define SIDECAR_ERROR_INVALID_FITS_FILENAME       SIDECAR_ERROR_BASE - 18
#define SIDECAR_ERROR_RESET_ASIC                  SIDECAR_ERROR_BASE - 19
#define SIDECAR_ERROR_POWER_DOWN_ASIC             SIDECAR_ERROR_BASE - 20

struct ExposureConfiguration {
  int integrationTime;   /* in ms */
  int numberOfResets;
  int samplingMode;
  int numberOfReads;
  int numberOfCoadds;
};

/* should be enum? */
#define SAMPLING_MODE_SINGLE  1
#define SAMPLING_MODE_CDS     2
#define SAMPLING_MODE_MCDS    3
#define SAMPLING_MODE_UTR     4

#ifdef CREATOR
char *SamplingModeNames[] = {
  "",
  "Single", 
  "CDS", 
  "MCDS",
  "UTR"
};

#endif /* CREATOR */

/* function prototypes */
int Sidecar_initializeHardware();
double Sidecar_getReadTime();
int Sidecar_downloadTimingFile(const char* mcdFile, int doDataOnAck);
void Sidecar_prepFifoForExposure();
int Sidecar_startExposure(struct ExposureConfiguration config, char *fitsFilename);
int Sidecar_takeExposure(struct ExposureConfiguration config, char *fitsFilename);
void Sidecar_setTimingCodeDownloaded(int status);
void Sidecar_skipInitialize();

#endif   /* _GPIFSIDECAR_ADAPTER_H_ */

