/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarAdapter.c
 *
 *  @brief
 *  Functions specific to the Sidecar Adapter.
 *
 * This module contains the hardware specific functions handling the
 * interface with the Sidecar Adapter.
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 */
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>   /* bzero */
#include <math.h>

/** CREATOR tag indicate this file solely creates arrays
 *
 * CREATOR tag is used so that only one file attempts to create
 * arrays shared by multiple modules within a single component.
 * It should only be defined for one module.  Other modules will
 * use an extern command to get access to the array created by this module.
 */
#define CREATOR
#include "SidecarIceClient.h"
#include "SidecarAdapter.h"
#include "lerrlog.h"

#include "fitsio.h"

static int dataOnAckOn = 0;
static int hardwareInitialized = 0;
static int timingCodeDownloaded = 0;
static double readTimeInMillis = 0.;
static int detectorSetupFlag = 0;

int *blockArray;
int *firstReads;
int *secondReads;

long totalNumberOfPixels;
int arraySize;
		


/** Initializes the Sidecar hardware
 *
 * Initializes the sidecar hardware by loading application, downloading firmware,
 * downloading registers, checking to see if dataOnAck is on, and checking if ASIC
 * is available.
 *
 * @param none
 *
 * @return 0 on success
 * @return SIDECAR_ERROR_OPEN_APPLICATION on error opening application
 * @return SIDECAR_ERROR_JADE_NOT_AVAILABLE on error if jade not available
 * @return SIDECAR_ERROR_DOWNLOAD_JADE_FIRMWARE on error downloading firmware
 * @return SIDECAR_ERROR_DOWNLOAD_JADE_REGISTERS on error downloading registers
 * @return SIDECAR_ERROR_ASIC_NOT_AVAILABLE on error if asic not available
 */
int Sidecar_initializeHardware() {
  const char* response;
  const char* dataOnAck;
  int newDataOnAck;
  char* dataOnAckStop;
  char newDataOnAckString[10];
  int p1Exist;
  int p2Exist;
  int p3Exist;
  int p4Exist;
  int partCount;  /*number of existing parts */

  llog_msg(LOGLVL_INFO, "Initializing SIDECAR...\n");

  /* when triggering the frame to start, and you 
   * wait for a flag value to be some value based on presence of detectors
   * detector definition in file specified in c:\RSI\init_files\paramfile.txt
   * which points to C:\Data\JWST\DevBrdH2RGForAsic.acq
   * which specifies
   * PartSN1:H2RG-C001-IR25-SC2-B-010-JWST
   * PartSN2:
   * PartSN3:
   * PartSN4:
   * 
   * this makes P1Exist = 1
   * and the other P#Exist = 0
   * and partcount = 1
   * 
   * this stuff is from prepare2readasicdata.pro
   */
  detectorSetupFlag = 0;
  p1Exist = TRUE;
  p2Exist = FALSE;
  p3Exist = FALSE;
  p4Exist = FALSE;
  partCount=1;  /* number of existing parts */
		
  if (p1Exist == TRUE) detectorSetupFlag = detectorSetupFlag | 1;
  if (p2Exist == TRUE) detectorSetupFlag = detectorSetupFlag | 2;
  if (p3Exist == TRUE) detectorSetupFlag = detectorSetupFlag | 4;
  if (p4Exist == TRUE) detectorSetupFlag = detectorSetupFlag | 8;

		
  /* load application */
  llog_msg(LOGLVL_INFO, "Opening application: %s.\n", SIDECAR_DEFAULT_APPLICATION_FILENAME);
  response = sidecar_openApp(SIDECAR_DEFAULT_APPLICATION_FILENAME);
  if (strcmp(response, SIDECAR_SUCCESS_OPEN_APPLICATION) != 0) {
    llog_msg(LOGLVL_ERROR, "Error opening application %s: %s\n", SIDECAR_DEFAULT_APPLICATION_FILENAME, response);
    return SIDECAR_ERROR_OPEN_APPLICATION;
  }
  llog_msg(LOGLVL_DEBUG, "Application %s opened. Response = %s.\n", SIDECAR_DEFAULT_APPLICATION_FILENAME, response);  
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);
		
  llog_msg(LOGLVL_TRACE, "Checking to see if jade is available...\n");
  response = sidecar_getAvailableJades();
  llog_msg(LOGLVL_DEBUG, "GetAvailableJades response=%s.\n", response);

  if (strstr(response, SIDECAR_DEFAULT_JADE_NAME) == NULL) {
    llog_msg(LOGLVL_ERROR, "Error: %s not available.\n", SIDECAR_DEFAULT_JADE_NAME);
    return SIDECAR_ERROR_JADE_NOT_AVAILABLE;
  }
  llog_msg(LOGLVL_DEBUG, "%s is available.\n", SIDECAR_DEFAULT_JADE_NAME);
		
		
  /* download firmware */
  llog_msg(LOGLVL_INFO, "Downloading JADE Firmware...\n");
  response = sidecar_downloadJADEFirmware(SIDECAR_DEFAULT_JADE_NAME);
  if (strcmp(response, SIDECAR_SUCCESS_DOWNLOAD_JADE_FIRMWARE) != 0) {    
    llog_msg(LOGLVL_ERROR, "Error downloading Jade firmware: %s.\n", response);
    return SIDECAR_ERROR_DOWNLOAD_JADE_FIRMWARE;
  }
  llog_msg(LOGLVL_DEBUG, "DownloadJADEFirmware response=%s.\n", response);
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);

  /* download registers */
  llog_msg(LOGLVL_INFO, "Downloading JADE Registers...\n");
  response = sidecar_downloadJADERegisters(SIDECAR_DEFAULT_JADE_NAME);
  if (strcmp(response, SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS) != 0) {    
    llog_msg(LOGLVL_ERROR, "Error downloading Jade registers: %s.\n", response);
    return SIDECAR_ERROR_DOWNLOAD_JADE_REGISTERS;
  }
  llog_msg(LOGLVL_DEBUG, "DownloadJADERegisters response=%s.\n", response);
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);

  /* check to see if dataOnAck is on */
  llog_msg(LOGLVL_TRACE, "Getting current dataOnAck value...\n");
  dataOnAck = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008");
  llog_msg(LOGLVL_DEBUG, "Current dataOnAck value=%s.\n", dataOnAck);
  if ((strtol(dataOnAck+2, &dataOnAckStop, 16) & 0x0001) == 0x0001) {
    llog_msg(LOGLVL_TRACE, "dataOnAck is on.\n");
    dataOnAckOn = TRUE;
  } else {
    llog_msg(LOGLVL_TRACE, "dataOnAck is off.\n");
    dataOnAckOn = FALSE;
  }

  if (dataOnAckOn == TRUE) {
    /* turn dataOnAck off */
    /*  dataOnAck in form of 0x0000 */
    llog_msg(LOGLVL_INFO, "Turning dataOnAck off...\n");
    newDataOnAck = strtol(dataOnAck+2, &dataOnAckStop, 16) & 0xFFFE;

    sprintf(newDataOnAckString, "0x%4x", newDataOnAck);
    llog_msg(LOGLVL_TRACE, "newDataonAck: %s.\n", newDataOnAckString);

    llog_msg(LOGLVL_TRACE, "Writing dataOnAck to Jade register...\n");
    response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", newDataOnAckString);
    llog_msg(LOGLVL_TRACE, "WriteJadeRegister response = %s.\n", response);
    sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);
  
    dataOnAckOn = FALSE;
  }

  /* reset ASIC */
  llog_msg(LOGLVL_INFO, "Resetting ASIC...\n");
  if (sidecar_resetASIC(SIDECAR_DEFAULT_JADE_NAME) < 0) {
    return SIDECAR_ERROR_RESET_ASIC;
  }
  sleep (1);
		

  /* check to see if ASIC is available */
  llog_msg(LOGLVL_INFO, "Getting available asics...\n");
  response = sidecar_getAvailableAsics(SIDECAR_DEFAULT_JADE_NAME);
  llog_msg(LOGLVL_DEBUG, "GetAvailableAsics response=%s.\n", response);
  if (strstr(response, SIDECAR_DEFAULT_ASIC_NAME) == NULL) {
    llog_msg(LOGLVL_ERROR, "Error: %s not available.\n", SIDECAR_DEFAULT_ASIC_NAME);
    return SIDECAR_ERROR_ASIC_NOT_AVAILABLE;
  }
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);

  hardwareInitialized = 1;

  return 0;
}

/** Get read time from sidecar
 *
 * @param none
 *
 * @return read time in milliseconds
 */
double Sidecar_getReadTime()  {
  int lineClockTimeInMicrosecs;
  int numLinesPerFrame;
  const char* response;
  char* strtolStop;
  int h000CVal;
  int h4003Val;
  float JadeFeqFrequency;
  float AsicFrequency;
  float clkTime;
		
  llog_msg(LOGLVL_INFO, "Getting read time...\n");
		
  /* get lineClockTime */
  llog_msg(LOGLVL_TRACE, "Getting lineClockTime from ASIC register...\n");
  response = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4008", FALSE);
  llog_msg(LOGLVL_DEBUG, "GetAsicRegister response=%s.\n", response);
  lineClockTimeInMicrosecs = strtol(response+2, &strtolStop, 16);
  /* pcnt number */
  llog_msg(LOGLVL_DEBUG, "lineClockTimeInMicrosecs = %d.\n", lineClockTimeInMicrosecs);
		
  /* get numLinesPerFrame */
  llog_msg(LOGLVL_TRACE, "Getting numLinesPerFrame from ASIC register...\n");
  response = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4009", FALSE);
  llog_msg(LOGLVL_DEBUG, "GetAsicRegister response=%s.\n", response);
  numLinesPerFrame = strtol(response+2, &strtolStop, 16);
  /* lcnt number */
  llog_msg(LOGLVL_DEBUG, "numLinesPerFrame = %d.\n", numLinesPerFrame);

  arraySize = numLinesPerFrame;
  totalNumberOfPixels = arraySize * arraySize;

  llog_msg(LOGLVL_DEBUG, "totalNumberOfPixels = %d.\n", totalNumberOfPixels);

		
  /* get jade freq */
  llog_msg(LOGLVL_TRACE, "Getting Jade freq from Jade register...\n");
  response = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000C");
  llog_msg(LOGLVL_DEBUG, "GetJadeRegister response=%s.\n", response);
  h000CVal = strtol(response+2, &strtolStop, 16);
  /* freq = 480 Mhz / (count + 4) */
  JadeFeqFrequency  = (480.)/(float)((h000CVal & 0x0FFF) + 4);
  llog_msg(LOGLVL_DEBUG, "Jade frequency=%f.\n", JadeFeqFrequency);

  /* get ASIC freq */
  llog_msg(LOGLVL_TRACE, "Getting Asic freq from ASIC register...\n");
  response = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4003", FALSE);
  llog_msg(LOGLVL_DEBUG, "GetASICRegister response=%s.\n", response);
  h4003Val = strtol(response+2, &strtolStop, 16);
  /* freq = JadeFreq / h4003Val */
  if (h4003Val == 0) 
    AsicFrequency = JadeFeqFrequency/100.;
  else
    AsicFrequency = JadeFeqFrequency/h4003Val;
  llog_msg(LOGLVL_DEBUG, "Asic frequency=%f.\n", AsicFrequency);

  /* convert to milliseconds */
  clkTime = 1./(AsicFrequency*1000);

  /* return read time in milliseconds */
  return lineClockTimeInMicrosecs*numLinesPerFrame*clkTime;
}

/** Download Timing File to sidecar
 *
 * Make sure the hardware has been initialized and check that data
 * ack is off for testpattern mcd, and (so far) on for all others. Then
 * check to make sure ASIC is available and download MCD. Update read
 * time.
 *
 * @param mcdFile [in] Timing file to read
 * @param doDataOnAck [in] Set off for testpattern mcd, on for all others (so far)
 *
 * @return 0 on success
 * @return SIDECAR_ERROR_HARDWARE_NOT_INITIALIZED on error if hardware not initialized
 * @return SIDECAR_ERROR_SET_DATA_ON_ACK on error setting data on ACK
 * @return SIDECAR_ERROR_ASIC_NOT_AVAILABLE on error if asic not available
 * @return SIDECAR_ERROR_DOWNLOAD_MCD_FILE on error downloading MCD file
 * @return SIDECAR_ERROR_NON_POSITIVE_READ_TIME on error if read time is non positive
 */
int Sidecar_downloadTimingFile(const char* mcdFile, int doDataOnAck) {
  /* data on ack should be off for testpattern mcd, 
     and, so far, on for all others */
		
  const char* response;
  const char* dataOnAck;
  int newDataOnAck;
  char* dataOnAckStop;
  char newDataOnAckString[10];

  /* make sure hardware has been initialized */
  if (hardwareInitialized == 0) {
    llog_msg(LOGLVL_ERROR, "Hardware not initialized.\n");
    return SIDECAR_ERROR_HARDWARE_NOT_INITIALIZED;
  }
	
  /* TODO validate mcdFile */

  
  if ((dataOnAckOn == TRUE) && (1==0)) {
    /* turn dataOnAck off */
    llog_msg(LOGLVL_TRACE, "Getting current dataOnAck value...\n");
    dataOnAck = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008");
    llog_msg(LOGLVL_DEBUG, "Current dataOnAck value=%s.\n", dataOnAck);
    /*  dataOnAck in form of 0x0000 */
    newDataOnAck = strtol(dataOnAck+2, &dataOnAckStop, 16) & 0xFFFE;
    sprintf(newDataOnAckString, "0x%4x", newDataOnAck);
    llog_msg(LOGLVL_DEBUG, "newDataonAck: %s.\n", newDataOnAckString);

    llog_msg(LOGLVL_TRACE, "Writing dataOnAck to Jade register...\n");
    response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", newDataOnAckString);
    llog_msg(LOGLVL_DEBUG, "WriteJadeRegister response = %s.\n", response);
    sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);
    dataOnAckOn = 0;
  }

  /* check to see if ASIC is available */

  llog_msg(LOGLVL_TRACE, "Getting available asics...\n");
  response = sidecar_getAvailableAsics(SIDECAR_DEFAULT_JADE_NAME);
  llog_msg(LOGLVL_DEBUG, "GetAvailableAsics response=%s.\n", response);
  if (strstr(response, SIDECAR_DEFAULT_ASIC_NAME) == NULL) {
    llog_msg(LOGLVL_ERROR, "Error: %s not available.\n", SIDECAR_DEFAULT_ASIC_NAME);
    return SIDECAR_ERROR_ASIC_NOT_AVAILABLE;
  }
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);

  /* download MCD */
  llog_msg(LOGLVL_TRACE, "Downloading MCD file: %s.\n", mcdFile);
  response = sidecar_downloadMCDFile(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, mcdFile);
  /*
  response = sidecar_downloadMCD(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME);
  */
  
  if (strcmp(response, SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE) != 0) {    
    llog_msg(LOGLVL_ERROR, "Error downloading MCD file: %s.\n", response);
    return SIDECAR_ERROR_DOWNLOAD_MCD_FILE;
  }
  llog_msg(LOGLVL_DEBUG, "dowloadMCDFile response=%s.\n", response);
  sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);


  /* check to see if dataOnAck is on */
  llog_msg(LOGLVL_TRACE, "Getting current dataOnAck value...\n");
  dataOnAck = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008");
  llog_msg(LOGLVL_DEBUG, "Current dataOnAck value=%s.\n", dataOnAck);
  if ((strtol(dataOnAck+2, &dataOnAckStop, 16) & 0x0001) == 0x0001) {
    llog_msg(LOGLVL_TRACE, "dataOnAck is on.\n");
    dataOnAckOn = TRUE;
  } else {
    llog_msg(LOGLVL_TRACE, "dataOnAck is off.\n");
    dataOnAckOn = FALSE;
  }

  /* turn data on ack on */
  if (dataOnAckOn == FALSE) {
			
    /* turn dataOnAck on */
    /*  dataOnAck in form of 0x0000 */
    llog_msg(LOGLVL_TRACE, "Turning dataOnAck on...\n");
    newDataOnAck = strtol(dataOnAck+2, &dataOnAckStop, 16) | 0x0001;

    sprintf(newDataOnAckString, "0x%4x", newDataOnAck);
    /*   strcat("0x", newDataOnAckString); */
    llog_msg(LOGLVL_DEBUG, "newDataonAck: %s.\n", newDataOnAckString);

    llog_msg(LOGLVL_TRACE, "Writing dataOnAck to Jade register...\n");
    response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", newDataOnAckString);
    llog_msg(LOGLVL_DEBUG, "WriteJadeRegister response = %s.\n", response);
    sleep(SIDECAR_ADAPTER_PAUSE_BETWEEN_COMMANDS);
    dataOnAckOn = TRUE;
  }
		
  /* update read time */
  readTimeInMillis = Sidecar_getReadTime();
  llog_msg(LOGLVL_DEBUG, "readTimeInMillis = %f.\n", readTimeInMillis);
		
  timingCodeDownloaded = 1;

  return 0;
}


/** Prepare sidecar FIFO for exposure
 *
 * Prepare the sidecar FIFO for exposure by getting current value
 * of jade register 0x000D, converting it to an int, clearing bits
 * 3-2, 7-4, 11-8, writing initial values to jade register 0x000D.
 *
 * @param none
 *
 */
void Sidecar_prepFifoForExposure() {
  const char* response;
  const char* sValue;
  char hexValue[10];
  int intVal;
  char *strtolStop;

  /* configure the jade fifo (also from prepare2readasicdata.pro) */
  /* create 16-bit value to write to jade register 0x000d */
		
  llog_msg(LOGLVL_INFO, "Prepping FIFO for exposure...\n");
		
  /* start with current value */
	
  llog_msg(LOGLVL_TRACE, "Getting current value of jade register 0x000D.\n");
  sValue = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000D");
  llog_msg(LOGLVL_DEBUG, "Jade register 0x000D = %s\n", sValue);
		
  /* convert to int */
  intVal = strtol(sValue+2, &strtolStop, 16);
		
  /* clear bits 3-2, 7-4, 11-8 */
  intVal = intVal & 0xF003;
		
  /* bit2: Enable/disable Quad_FIFO or single_FIFO.  0 for partcount=1 */
  /* bit3: Rotate to next FIFO after RotateLength words. 0 for partcount=1 */
  /* bits 9-11: block length for autoRotation. 0 for partcount=1 */
  /* bits 4-7: same as detectorSetupFlag */
		
  intVal = intVal | (detectorSetupFlag << 4) | 0x100;
		
  /* write to jade register */
  sprintf(hexValue, "0x%x", intVal);
  llog_msg(LOGLVL_DEBUG, "Writing %s to Jade register 0x000D.\n", hexValue);
  response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000D", 
				       hexValue);
  llog_msg(LOGLVL_DEBUG, "WriteJadeRegister response=%s.\n", response);
}

int Sidecar_takeExposure(struct ExposureConfiguration config, char *fitsFilename) {
  int err=0;

  llog_msg(LOGLVL_INFO, "Starting exposure with output file <%s>.\n", fitsFilename);
  llog_msg(LOGLVL_INFO, "exposure params: itime = %d, coadds = %d, resets = %d, sampling mode = %d [%s], reads = %d, groups = %d.\n", config.integrationTime, 
	  config.numberOfCoadds, config.numberOfResets, 
	  config.samplingMode, SamplingModeNames[config.samplingMode], 
	   config.numberOfReads, config.numberOfGroups);

  err = sidecar_takeExposure(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, config.numberOfResets, config.integrationTime, config.numberOfCoadds, config.samplingMode, config.numberOfReads, config.numberOfGroups, 0,0,2047,2047, fitsFilename);


  
  return err;
}

/** Start exposure on sidecar
 *
 * Starts exposure based on previously defined ramp.
 *
 * @param config [in] Exposure configuation
 * @param fitsFilename [in] Filename to write data to
 *
 * @return 0 on success
 * @return SIDECAR_ERROR_INVALID_NUMBER_OF_RESETS if number of resets is invalid
 * @return SIDECAR_ERROR_INVALID_NUMBER_OF_COADDS if number of coadds is invalid
 * @return SIDECAR_ERROR_INVALID_NUMBER_OF_READS if number of reads is invalid
 * @return SIDECAR_ERROR_INVALID_SAMPLING_MODE if sampling mode is invalid
 * @return SIDECAR_ERROR_INVALID_ITIME if integration time is invalid
 * @return SIDECAR_ERROR_TIMING_CODE_NOT_DOWNLOADED on error downloading time code
 * @return SIDECAR_ERROR_INVALID_FITS_FILENAME if fits filename is invalid
 * @return SIDECAR_ERROR_CREATE_FITS_FILE on error creatings fits file
 * @return SIDECAR_ERROR_CREATE_FITS_IMAGE on error creating fits image
 */
int Sidecar_startExposure(struct ExposureConfiguration config, char *fitsFilename) {
  const char* response;
  int err=0;

  /* iterators */
  int currentCoadd;
  int currentGroup;
  int currentRead;
  int currentBlock;
  int ii, jj;

  char hexString[10];
  char *strtolStop;
  int valInt;
  int waitingForData = FALSE;
  const char *waitVal;
  const char *blockVal;
  const char *blockVal2;
  int blockIntVal;
  int blockIntVal2;
  int fifoCounter;

  int minItime=0;
  char *filenameCheck;
  char filenameCheckTemp[FILENAME_MAX_LENGTH];

  /* ramp parameters */
  int rampNumDrop;
  int rampNumResets;
  int rampNumReads;
  int rampNumGroups;
  int rampPulseLED = FALSE;
  int utrReads;
  float rditime;
  float trueItime;
  int h4002Val;

   /* TODO make these more generic */
  /* TODO make array sized to fit all returned data */
  float *finalArray;

  /* determine block size */
  int blockSize;
  int numBlocks;

		
  /* fits file vars */
  int fits_status=0;
  int bitpix=FLOAT_IMG;
  int naxes=2;
  long axes[2];
  int dtype=TFLOAT;
  long fpixel[2];

  fitsfile *finalFitsFile;

  llog_msg(LOGLVL_INFO, "Starting exposure with output file <%s>.\n", fitsFilename);

  /* blockSize = 512*512; */
  blockSize = totalNumberOfPixels;
  /* blockSize = totalNumberOfPixels / 4; */
  numBlocks = (totalNumberOfPixels)/blockSize;

  blockArray = (int *)malloc(blockSize * sizeof(int));
  finalArray = (float *)malloc(totalNumberOfPixels * sizeof(float));

  firstReads = (int *)malloc(totalNumberOfPixels * sizeof(int));
  secondReads = (int *)malloc(totalNumberOfPixels * sizeof(int));


  llog_msg(LOGLVL_DEBUG, "arraySize = %d, totalNumberOfPixels = %d.\n", arraySize, totalNumberOfPixels);
  axes[0] = (long)arraySize;
  axes[1] = (long)arraySize;

  fpixel[0]=fpixel[1]=1;

  bzero(finalArray, totalNumberOfPixels*sizeof(float));

  llog_msg(LOGLVL_INFO, "starting exposure: itime = %d, coadds = %d, resets = %d, sampling mode = %d [%s], reads = %d.\n", config.integrationTime, 
	  config.numberOfCoadds, config.numberOfResets, 
	  config.samplingMode, SamplingModeNames[config.samplingMode], 
	  config.numberOfReads);

  /* error check configuration */
  /* number of Resets must non-negative */
  if (config.numberOfResets < 0) {
    llog_msg(LOGLVL_ERROR, "Invalid number of resets: %d.\n", config.numberOfResets);
    return SIDECAR_ERROR_INVALID_NUMBER_OF_RESETS;
  }

  /* number of coadds must > 0 */
  if (config.numberOfCoadds < 1) {
    llog_msg(LOGLVL_ERROR, "Invalid number of coadds: %d.\n", config.numberOfCoadds);
    return SIDECAR_ERROR_INVALID_NUMBER_OF_COADDS;
  }

  /* number of reads must > 0 (ignored for single and cds) */
  if (config.numberOfReads < 1) {
    llog_msg(LOGLVL_ERROR, "Invalid number of reads: %d.\n", config.numberOfReads);
    return SIDECAR_ERROR_INVALID_NUMBER_OF_READS;
  }

  /* sampling mode must be allowed */
  if (config.samplingMode == SAMPLING_MODE_SINGLE) {
    /* itime must be positive */
    minItime = 0;
  } else if (config.samplingMode == SAMPLING_MODE_CDS) {
    /* itime must be > readTime */
    minItime = readTimeInMillis;
  } else if (config.samplingMode == SAMPLING_MODE_MCDS) {
  /* integration time must be > readTime * numberOfReads */
    minItime = readTimeInMillis*config.numberOfReads;
  } else if (config.samplingMode == SAMPLING_MODE_UTR) {
  /* integration time must be > readTime * numberOfReads */
    minItime = readTimeInMillis*config.numberOfReads;
  } else {
    llog_msg(LOGLVL_ERROR, "Invalid sampling mode: %d.\n", config.samplingMode);
    return SIDECAR_ERROR_INVALID_SAMPLING_MODE;
  }

  /* number of reads must > 0 (ignored for single and cds) */
  if (config.integrationTime < minItime) {
    llog_msg(LOGLVL_ERROR, "Invalid integration time: %d ms. Must be larger than %d ms.\n", config.integrationTime, minItime);
    return SIDECAR_ERROR_INVALID_ITIME;
  }



  /* timing code must have been downloaded (so that readTime has a value) */
  if (timingCodeDownloaded == FALSE) {
    llog_msg(LOGLVL_ERROR, "Timing code not downloaded.\n");
    return SIDECAR_ERROR_TIMING_CODE_NOT_DOWNLOADED;
  }


  strcpy(filenameCheckTemp, fitsFilename);

  /* check fitsFilename validity */
  if (fitsFilename[0] == '!') {   /* overwrite existing file */
    filenameCheck = (filenameCheckTemp+1);
  } else {
    filenameCheck = filenameCheckTemp;
  }

  llog_msg(LOGLVL_DEBUG, "Checking write permission on file %s.\n", filenameCheck);
  /* check to see if file exists */
  /* if so, check writability of file */
  /* otherwise, check writability of directory */
  if (access( filenameCheck, F_OK) == 0) {
    if (access( filenameCheck, W_OK ) != 0) {
      llog_msg(LOGLVL_ERROR, "No write permission to file: %s.\n", fitsFilename);
      return SIDECAR_ERROR_INVALID_FITS_FILENAME; 
    }
  } else {
    for (ii=strlen(filenameCheck); ii > 0; ii--) {
      /* printf("ii=%d, c=%c\n", ii, filenameCheck[ii-1]); */
      if (filenameCheck[ii-1] == '/') {
	filenameCheck[ii-1] = '\0';
	break;
      }
    }
    llog_msg(LOGLVL_DEBUG, "Checking permission on dir: %s.\n", filenameCheck);
    if (access( filenameCheck, W_OK ) != 0) {
      llog_msg(LOGLVL_ERROR, "No write permission to file: %s.\n", fitsFilename);
      return SIDECAR_ERROR_INVALID_FITS_FILENAME;     
    }
  }


  /* initialize fits file */
  llog_msg(LOGLVL_INFO, "Creating fits file with filename %s.\n", fitsFilename);
  fits_create_file(&finalFitsFile, fitsFilename, &fits_status);
  if (fits_status != 0) {
    llog_msg(LOGLVL_ERROR, "Error creating fits file %s.\n", fitsFilename);
    return SIDECAR_ERROR_CREATE_FITS_FILE;
  }

  llog_msg(LOGLVL_INFO, "Creating fits image...\n");
  llog_msg(LOGLVL_DEBUG, "bitpix = %d (FLOAT_IMG = %d), naxes = %d, axes[0]=%d, axes[1]=%d\n", bitpix, FLOAT_IMG, naxes, axes[0], axes[1]);
  fits_create_img(finalFitsFile, bitpix, naxes, axes, &fits_status);
  if (fits_status != 0) {
    llog_msg(LOGLVL_ERROR, "Error creating fits image.\n");
    return SIDECAR_ERROR_CREATE_FITS_IMAGE;
  }

#ifdef SIMULATE_DATA
  for (ii=0; ii< totalNumberOfPixels; ii++) {
    finalArray[ii]=ii;
  }
#else		
  /* configure ramp based on sampling mode
   *
   *  each ramp has a resets, reads, and drops
   *  drops are used for integration time
   *  reads and drops are collectively called groups
   *  a group can be called multiple times in a ramp
   *  
   *	each time a ramp is used, do the following
   *
   *    1) set ramp
   *    2) prep fifo
   *    3) trigger ramp (exposure)
   *    4) for each group:
   *      4a) wait on bit, which is set when data is available
   *      4b) read science data
   *    
   */		
  
		
  /*
   * 	
   * Create the ramp based on exposure settings
   * 
   * Sampling modes:
   *
   * single: R resets, N drop, read
   *   ramp: R reset, 1 read, N drop, 2 group, stop ramp after reads 
   *           of second group, discard first read
   * 
   *    cds: R resets, 1 read, N drop, 1 read
   *   ramp: R reset, 1 read, N drop, 2 group, stop ramp after reads 
   *           of second group
   *     
   *   mcds: R resets, M reads, N drop, M reads 
   *   ramp: R reset, M read, N drop, 2 groups, stop ramp after reads
   *           of second group
   *   
   *    utr: R resets, M reads, N drop, M reads, N drop, ..., M reads 
   *           (total sets of reads: G)
   *   ramp: R reset, M read, N drop, G groups, stop ramp after reads 
   *           of last group
   */

  rampNumResets = config.numberOfResets;
  
  switch (config.samplingMode) {
  case SAMPLING_MODE_SINGLE:    /* SINGLE */
    if (config.integrationTime < readTimeInMillis) {
      rampNumDrop=0;
      rampNumReads = 1;
      rampNumGroups=1;
      break;
    }
  case SAMPLING_MODE_CDS:
    rampNumDrop = (int)(floor(config.integrationTime/readTimeInMillis) - 1);
    rampNumReads = 1;
    rampNumGroups = 2;
    break;
  case SAMPLING_MODE_MCDS:
    rampNumDrop = (int)(floor(config.integrationTime/readTimeInMillis) - config.numberOfReads);
    /* make sure numDrop is non-negative  TODO better reporting */
    if (rampNumDrop < 0) rampNumDrop = 0;
    rampNumReads = config.numberOfReads;
    rampNumGroups = 2;
    break;			
  case SAMPLING_MODE_UTR:
    /* for now, all UTR reads are one read at a time.  */
    utrReads = 1;
    rampNumDrop = (int)(floor((config.integrationTime/config.numberOfReads-1)/readTimeInMillis) - utrReads);
    rampNumReads = utrReads;
    rampNumGroups = config.numberOfReads;
    rditime = (rampNumDrop+1)*readTimeInMillis/1000.;
    break;
  }

  if (rampPulseLED == TRUE) {
    h4002Val = 0x0002;
  } else {
    h4002Val = 0;
  }	

  /* h4002
   *   bit 0: Enable reset frame data output
   *   bit 1: Enable Enable h6900<8> pulse at end of each read-drop frame. 
   *          h6900<8> sets the level on IO1, it can be used as LED trigger
   *   bit 2: Enable clocking of SCA in drop frames.
   *   bit 3-4: 0 -> Do nothing in Idle mode.
   *            1 -> continuously take reset frames in Idle mode
   *            2 -> continuously take reset-read frames in Idle mode
   */
		
  h4002Val = h4002Val | 0x0014;  /*bit4 is for reset-read idle */
  
  llog_msg(LOGLVL_TRACE, "Writing 0x8000 to ASIC register 0x6900...\n");
  /* sequence immediately returns to idle if the bit 
   * is cleared before sequence is completed. */
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x6900", "0x8000");
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x6900 response=%s.\n", response);

  sprintf(hexString, "0x%x", rampNumResets);
  llog_msg(LOGLVL_TRACE, "Writing %s to ASIC register 0x4000...\n", hexString);
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4000", hexString); 
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4000 response=%s.\n", response);
    
  sprintf(hexString, "0x%x", rampNumReads);
  llog_msg(LOGLVL_TRACE, "Writing %s to ASIC register 0x4001...\n", hexString);
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4001", hexString);
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4001 response=%s.\n", response);

  sprintf(hexString, "0x%x", h4002Val);
  llog_msg(LOGLVL_TRACE, "Writing %s to ASIC register 0x4002...\n", hexString);
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4002", hexString);
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4002 response=%s.\n", response);

  /* from this version we don't need config h4003
   * llog_msg(LOGLVL_TRACE, "Writing 0x0002 to ASIC register 0x4003...\n");
   * response=sidecar_WriteAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4003", "0002" );
   * llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4003 response=%s.\n", response);
   */

  sprintf(hexString, "0x%x", rampNumGroups);
  llog_msg(LOGLVL_TRACE, "Writing %s to ASIC register 0x4004...\n", hexString);
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4004", hexString);
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4004 response=%s.\n", response);

  sprintf(hexString, "0x%x", rampNumDrop);
  llog_msg(LOGLVL_TRACE, "Writing %s to ASIC register 0x4005...\n", hexString);
  response=sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x4005", hexString);
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x4005 response=%s.\n", response);

  /* do later, if necessary.
   * ret = setAsicGain(PGAGain)
   */	


  for (currentCoadd=0; currentCoadd<config.numberOfCoadds; currentCoadd++) {
	
    llog_msg(LOGLVL_DEBUG, "currentCoadd = %d.\n", currentCoadd);
		
    /* clear frame buffers */
    bzero(firstReads, totalNumberOfPixels*sizeof(int));
    bzero(secondReads, totalNumberOfPixels*sizeof(int));

    /* prep fifo */
    Sidecar_prepFifoForExposure();
		
    /* trigger frame */
    llog_msg(LOGLVL_INFO, "Triggering frame.\n");
    response = sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x6900", "0x8001");
  llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x6900 response=%s.\n", response);

    /*wait for data on jade */
    llog_msg(LOGLVL_TRACE, "Waiting for data...\n");

    /* check for abort within each loop */
    /* add timeouts */
    for (currentGroup=0; currentGroup<rampNumGroups; currentGroup++) {

      waitingForData = TRUE;  
      while (waitingForData == TRUE) {
				
	waitVal = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000E");
	llog_msg(LOGLVL_DEBUG, "waiting for data, 0x000E = %s, detectorSetupFlag = %d\n", waitVal, detectorSetupFlag);
	usleep(1000);
					
	valInt = strtol(waitVal+2, &strtolStop, 16);
	if ((valInt & detectorSetupFlag) == 0)
	  waitingForData = FALSE;
      }
			
      llog_msg(LOGLVL_INFO, "Data available... reading...\n");

      for (currentRead=0; currentRead<rampNumReads; currentRead++) {

	llog_msg(LOGLVL_INFO, "Now reading out for read %d of %d.\n", currentRead+1, rampNumReads);
					
	currentBlock=0;
	while(currentBlock < numBlocks) {

	  llog_msg(LOGLVL_DEBUG, "currentBlock = %d.\n", currentBlock);

	  /* bits 9-15 of val give part of fifosize
	   * if val = binary XXXXXXXYYYYYYYYY
	   * we want XXXXXXX0000000000000000 = 0xNN0000
	   *				
	   * val2 = get jadeReg 0x000F.  
	   * val2 = 0xZZZZ

	   * we want 0xNNZZZZ
						
	   * fifo counter = (val & 0xFE00) << 7 | (getJadeReg("0x000F"))
	   * fifo counter give twice the number of pixels available
	   */

	  blockVal = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000E");
	  llog_msg(LOGLVL_DEBUG, "waiting for data, 0x000E = %s\n", blockVal);
	  usleep(1000);
	
	  blockIntVal = strtol(blockVal+2, &strtolStop, 16);

						
	  blockVal2 = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x000F");
	  llog_msg(LOGLVL_DEBUG, "waiting for data, 0x000F = %s\n", blockVal2);
	  usleep(1000);
	
	  blockIntVal2 = strtol(blockVal2+2, &strtolStop, 16);
						
	  fifoCounter = ((blockIntVal & 0xFE00) << 7) | (blockIntVal2);
			
	  llog_msg(LOGLVL_DEBUG, "fifo counter = %d, block = %d.\n", fifoCounter, blockSize);
		  
	  /* max fifo size is 0x7FFFFF (32-bit words i.e. 2 pixels)
	   *  = (4 x 2048 x 2048) - 2 pixels
	   * when fifosize == blockSize/2 then read science data
	   */
	  if (fifoCounter >= (blockSize)/2) {

	    /* write to jade register to start science data transfer
	     * jing said this was done for an older version 
	     */
	    /* response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0005", "0x0600"); */

	    for (jj=0; jj<((int)floor(fifoCounter/(blockSize/2))); jj++) {

	      llog_msg(LOGLVL_TRACE, "Reading science data: block size = %d, currentBlock = %d\n", blockSize, currentBlock);

	      /* can read by blocks (must be integer factor of size of array)
	       * not too small or too much overhead */
	      response = sidecar_readScienceData(SIDECAR_DEFAULT_JADE_NAME, blockSize, blockArray);
	      llog_msg(LOGLVL_DEBUG, "readScienceData response=%s.\n", response);

	      /* llog_msg(LOGLVL_DEBUG, "blockArray[204900] = %d\n", blockArray[204900]); */

	      /* do something with each read */
	      switch (config.samplingMode) {
	      case SAMPLING_MODE_SINGLE:
	      case SAMPLING_MODE_CDS:
	      case SAMPLING_MODE_MCDS:
		if (currentGroup == 0) {
		  for (ii = 0; ii < blockSize; ii++) {
		    firstReads[ii+currentBlock*blockSize] += blockArray[ii];
		  }
		} else {
		  for (ii = 0; ii < blockSize; ii++) {
		    secondReads[ii+currentBlock*blockSize] += blockArray[ii];
		  }
		}
		break;
	      case SAMPLING_MODE_UTR:
		/*
		for (ii = 0; ii < blockSize; ii++) {
		  utr_sf[ii+currentBlock*blockSize] += blockArray[ii];
		  utr_stf[ii+currentBlock*blockSize] += (rampNumReads-1)*(rditime)*blockArray[ii];
		}
		*/
		break;
	      }
	      jj++;
	      currentBlock++;
	      if (currentBlock == numBlocks) {
		break;
	      }
	    }
	  }  /* end current block available */
	}  /* endfor currentBlock  */
      }  /* endfor currentRead */
      if (currentGroup == rampNumGroups-1) {
	/* kill ramp  (this should also be used for aborts) */
	llog_msg(LOGLVL_INFO, "Killing ramp...%d\n", currentGroup);
	/* try setting 0x6900 to 0x8000 */
	response = sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x6900", "0x8000");	
	llog_msg(LOGLVL_DEBUG, "WriteAsicRegister 0x6900 response=%s.\n", response);
      }
    }  /* endfor currentGroup */
			

    /* to clear fifo:
     * stop ramp
     * read jade 0x000D
     * and val with FEFF
     * set back
     */
				
    llog_msg(LOGLVL_INFO, "Science data read... reforming...\n");

    llog_msg(LOGLVL_DEBUG, "secondReads[204900] = %d\n", secondReads[204900]);
    llog_msg(LOGLVL_DEBUG, "firstReads[204900] = %d\n", firstReads[204900]);
	
    llog_msg(LOGLVL_DEBUG, "totalNumberOfPixels = %d\n", totalNumberOfPixels);

    /* sampling arithmetic */
    switch (config.samplingMode) {
    case SAMPLING_MODE_SINGLE:
      if (rampNumGroups == 1) {
	for (ii = 0; ii < totalNumberOfPixels; ii++) {
	  finalArray[ii] += (float)(firstReads[ii]);
	} 
      } else {
	for (ii = 0; ii < totalNumberOfPixels; ii++) {
	  finalArray[ii] += (float)(secondReads[ii]);
	}
      }
      break;
    case SAMPLING_MODE_CDS:
      for (ii = 0; ii < totalNumberOfPixels; ii++) {
	finalArray[ii] += (float)(secondReads[ii] - firstReads[ii]);
      }
      break;
    case SAMPLING_MODE_MCDS:
      for (ii = 0; ii < totalNumberOfPixels; ii++) {
	finalArray[ii] += (float)((secondReads[ii] - firstReads[ii])/config.numberOfReads);
      }
      break;
    case SAMPLING_MODE_UTR:
      
      break;
    }
			
  } /* end coadds */

		
  /* TODO divde by item and coadds to give number of counts per second per coadd */
		

#endif

  llog_msg(LOGLVL_DEBUG, "finalArray[204900] = %f\n", finalArray[204900]);

  /* write data to fits file  */
  llog_msg(LOGLVL_INFO, "Writing fits file to %s.\n", fitsFilename);

  llog_msg(LOGLVL_DEBUG, "dtype = %d (TFLOAT = %d), fpixel[0]=%d, fpixel[1]=%d, totalNumberOfPixels = %d, finalArray[last] = %f\n", dtype, TFLOAT, fpixel[0], fpixel[1], totalNumberOfPixels, finalArray[totalNumberOfPixels-1]);

  fits_write_pix(finalFitsFile, dtype, fpixel, totalNumberOfPixels, finalArray, &fits_status);
  if (fits_status != 0) {
    llog_msg(LOGLVL_ERROR, "Error writing fits file.\n");
    err = SIDECAR_ERROR_WRITE_FITS_FILE;
  }

  trueItime = (rampNumDrop+rampNumReads)*(rampNumGroups-1)*readTimeInMillis;
  /* add fits header info */
  fits_write_date(finalFitsFile, &fits_status);
  fits_write_key(finalFitsFile, TFLOAT, "TRUITIME", &trueItime, 
		 "True integration time in ms", &fits_status);
  fits_write_key(finalFitsFile, TINT, "COADDS", &config.numberOfCoadds, 
		 "Number of coadditions", &fits_status);
  fits_write_key(finalFitsFile, TINT, "SAMPMODE", &config.samplingMode, 
		 "Sampling mode (1:Single, 2:CDS, 3:MCDS, 4:UTR)", &fits_status);
  fits_write_key(finalFitsFile, TINT, "READS", &config.numberOfReads, 
		 "Sampling mode reads", &fits_status);
  fits_write_key(finalFitsFile, TINT, "RMPRESET", &rampNumResets, 
		 "Number of resets in ramp", &fits_status);
  fits_write_key(finalFitsFile, TINT, "RMPREADS", &rampNumReads, 
		 "Number of reads in ramp", &fits_status);
  fits_write_key(finalFitsFile, TINT, "RMPDROP", &rampNumDrop, 
		 "Number of drop frames in ramp", &fits_status);
  fits_write_key(finalFitsFile, TINT, "RMPGROUP", &rampNumGroups, 
		 "Number of groups in ramp", &fits_status);
  fits_write_key(finalFitsFile, TDOUBLE, "READTIME", &readTimeInMillis,
		 "Read time for a frame in ms", &fits_status);
  

  llog_msg(LOGLVL_DEBUG, "Closing fits file...\n");
  fits_close_file(finalFitsFile, &fits_status);
  if (fits_status != 0) {
    llog_msg(LOGLVL_ERROR, "Error closing fits file.\n");
    err = SIDECAR_ERROR_CLOSE_FITS_FILE;
    
  }

  llog_msg(LOGLVL_INFO, "Fits writing complete.\n");
  llog_msg(LOGLVL_INFO, "True Itime = %f, coadds = %d, sampmode = %d, reads = %d, resets = %d, rampReads = %d, rampDrops = %d, rampGroups = %d\n", trueItime, config.numberOfCoadds, config.samplingMode, config.numberOfReads, rampNumResets, rampNumReads, rampNumDrop, rampNumGroups); 

  return err;
}		

/** Set Timing Code Downloaded
 *
 * @param status [in] Value to set for timingCodeDownloaded
 *
 */
void Sidecar_setTimingCodeDownloaded(int status) {
  timingCodeDownloaded=status;
}

/** Skip initialization
 *
 * Set detectorSetupFlag so that initialization of sidecar will be skipped.
 *
 * @param none
 *
 */
void Sidecar_skipInitialize() {
  detectorSetupFlag = 1;

  timingCodeDownloaded = TRUE;
  readTimeInMillis = Sidecar_getReadTime();
}

