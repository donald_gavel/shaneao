/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/** 
 *  @file errlog.c
 *
 *  @brief 
 *  This module logs error for various functions of the GPI IFS software.
 *
 *  @author Allan Honey at W.M. Keck Observatory
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>

#include "lerrlog.h"


int LogTarget = 1;
int Verbose = 0;


/** Duplicate percent characters in a string to avoid problems if it is
 * passed through printf() again.
 *
 * @author William Lupton
 *
 * Scans the specified string duplicating any percent sign
 * characters encountered. Note that the original string 
 * must be expandable!
 *
 * @param str [in] [out] String to operate on
 *
 */
static void
duplicate_percent( str )

char    *str;           /* (!) string to operate on                         */
/*-*/
{
    int  i, j;          /* counters                                         */
    char buff[2048];    /* work buffer                                      */

    /*
     *  Copy string to buffer and copy back to original string, duplicating
     *  single percent characters.
     */
    strcpy( buff, str );

    for ( i = j = 0; i < strlen( buff ); i++, j++ ) {

	str[j] = buff[i];

	if ( ( i == 0 || buff[i-1] != '%' )
	     && buff[i] == '%' && buff[i+1] != '%' )
	  str[++j] = buff[i];
    }

    str[j] = '\0';

}

/** Log an error.
 *
 * Derived from log_error() in ktl/sys/kernel/log.c
 * Format the arguments and call syslog().
 *
 * @author Allan Honey
 *
 * @param logLevel [in] Log Level specified
 * @param mask [in] Mask specified
 * @param fmt [in] printf format string
 */
/*
 Invocation:
 */
void
lerrlog(       
       int      logLevel,
       int      mask,
       char    *fmt, ... /* (>) A printf() format string, followed by extra */
			 /*     arguments as implied by the string          */
       )
{
    va_list ap;         /* see varargs man page.                            */

    char buff[2048];    /* buffer for formatting string.                    */
    
    int  length = sizeof(buff);
    

    /*
     *  Format a string just like printf does it.
     */
    va_start( ap, fmt );
    vsprintf( buff, fmt, ap );
    va_end( ap );

    buff[length-1]='\0';

    /*
     *  Duplicate percent characters.
     */
    if ( strchr( buff, '%' ) )
	 duplicate_percent( buff );

    //printf("e: t = %d\n", LogTarget);
    /*
     *  Log using syslog().
     */
    if (LogTarget & LOGTO_SYSLOG) {
      syslog( logLevel, buff );
    }
    if (LogTarget & LOGTO_STDOUT) {
      fprintf(stdout, "%4d: %s", mask, buff);
    }
    if (LogTarget & LOGTO_STDERR) {
      fprintf(stderr, "%4d: %s", mask, buff);
    }
}

/** Logs an arbitrary string.
 *
 * This function is intended for user feedback when the 'verbose' logging
 * is enabled. The verbose mode is controlled via a keyword where the
 * value is a bit mask.
 *
 * The callers of this function must pass in a logging level and a
 * logging mask. If the current verbose has a value which yields 1 when
 * bitwise anded with the specified mask then the message is logged. 
 *
 * Note that bit masks above IRMV_TRIGGER1 are considered to be one-shot
 * logging masks such that the verbose mask is completely zeroed out
 * after this function completes.
 * 
 * @author Allan Honey
 *
 * @param loglevel [in] Log Level specified
 * @param mask [in] Mask Specified
 * @param pFormat [in] Variable argument list
 */
void syslog_msg( 
	     int           loglevel,
	     unsigned int  mask,
	     char         *pFormat, ... /* variable arg */
	     )
{
  va_list arg;            /* var arg required */

  char msg[2048];

  int length = sizeof(msg);
  //  printf("l: V = %d, m = %d\n", Verbose, mask);
   /*
   *  If debug mode is enabled then print.
   */
  if ( Verbose & mask ) {

    va_start(arg, pFormat);
    vsnprintf(msg, length, pFormat, arg);
    va_end(arg);

    msg[length-1] = '\0';
    lerrlog( loglevel, mask, msg );   

    if ( mask >= LOGLVL_TRIGGER1 ) Verbose = 0;

  }
}

/** Log an arbitrary string
 *
 * This function is intended for user feedback when the 'verbose' logging
 * is enabled. The verbose mode is controlled via a keyword where the
 * value is a bit mask.
 *
 * The callers of this function must pass in a logging level and a logging
 * mask. If the current verbose has a value which yields 1 when bitwise
 * anded with the specified mask then the message is logged. 
 *
 * Note that bit masks above IRMV_TRIGGER1 are considered to be one-shot
 * logging masks such that the verbose mask is completely zeroed out after
 * this function completes.
 *
 * @author Allan Honey
 *
 * @param mask [in] Specified Mask
 * @param pFormat [in] Variable argument list
 */
void llog_msg( 
	     unsigned int  mask,
	     char         *pFormat, ... /* variable arg */
	     )
{
  va_list arg;            /* var arg required */

  char msg[2048];

  int length = sizeof(msg);
  //  printf("l: V = %d, m = %d\n", Verbose, mask);
   /*
   *  If debug mode is enabled then print.
   */
  if ( Verbose & mask ) {

    va_start(arg, pFormat);
    vsnprintf(msg, length, pFormat, arg);
    va_end(arg);

    msg[length-1] = '\0';
    lerrlog( 0, mask, msg );   

    if ( mask >= LOGLVL_TRIGGER1 ) Verbose = 0;

  }
}

/** Specifies where log messages get written to
 *
 * This function specifies where messages written to for llog_msg calls.
 * Targets are specified in lerrlog.h
 *
 * @sa lerrlog.h
 * 
 *  @param target [in] where to log messsage to.  must be one of LOGTO_STDOUT, LOGTO_STRERR, LOGTO_SYSLOG.  Values can be Or'd to use more than one target.
 * 
 */
void setLogTarget(int target) {
  LogTarget = target;
}

/** Specifies current log level
 *
 * This function specifies the log level for the current program.  
 * Messages using llog_msg calls are only sent to target if the 
 * log level specified in the llog_msg call is currently set for the program. 
 * Levels are specified in lerrlog.h
 *
 * @sa lerrlog.h
 * 
 *  @param level [in] log messsage level for program. Values can be Or'd to use more than one level.
 * 
 */
void setLogLevel(int level) {
  Verbose = level;
}
