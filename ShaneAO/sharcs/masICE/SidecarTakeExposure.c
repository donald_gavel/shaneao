#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <termios.h>
#include <unistd.h>

#include "Sidecar.h"
#include "SidecarIceClient.h"
#include "lerrlog.h"

#define FILENAME_MAX_LENGTH 256
#define HOSTNAME_MAX_LENGTH 1024
#define SIDECAR_SERVER_HOSTNAME    "stealthie.ucolick.org" 
#define SIDECAR_SERVER_PORT    10051

#define SAMPLING_MODE_SINGLE  1
#define SAMPLING_MODE_CDS     2
#define SAMPLING_MODE_MCDS    3
#define SAMPLING_MODE_UTR     4


#define USAGE "USAGE:\n  SidecarTakeExposure [-i itime] [-c coadds] [-s sampling_mode]\n\t[-r reads] [-g groups] [-o outdir] [-f filename] [-z resets]\n\t[-x minX] [-X maxX] [-y minY] [-Y maxY]\n\t[-h serverHost] [-p serverPort] [-C serverConfig] [-P mcdfile]\n\t[-N] [-poweroff]\n\n  itime:\t[10000000] integration time in micros (non-negative)\n  coadds:\t[1] coadditions ( >0 )\n  samp_mode:\t[2] sampling mode (number or word: 1=single, 2=cds, 3=mcds,\n\t\t4=utr)\n  reads:\t[1] reads for utr, reads pairs for mcds, ignored for others\n  groups:\t[2] groups for utr, ignored for others\n  minX:\t\t[0] start column (0-2047) for subregion.\n  maxX:\t\t[2047] end column (0-2047) for subregion\n  minY:\t\t[0] start row (0-2047) for subregion\n  maxY:\t\t[2047] end row (0-2047) for subregion\n  filename:\t[sidecarAsic_YYMMDD_hhmmss.fits] valid filename (no checking).\n\t\tpreface with ! to overwrite.  If filename starts with a /,\n\t\tit is assumed it is an absolute path, and the outdir is ignored.\n  outdir:\t[/net/hydrogen/data/projects/gpi/data/sidecar/] output directory\n  resets:\t[1] number of reset frames ( >0 )\n  serverConfig:\tSidecar Control configuration filename (must be proper\n\t\tWindows filename)\n  mcdfile:\t[none] Windows path to personality file to download\n\t\tafter timing file\n  serverHost:\tHost name of server running ICE client.\n  serverPort:\tPort on ICE server is running on\n  -N:\t\tif this flag is set, there is no confirmation of settings, and\n\t\tthe hardware is not initialized.\n  -poweroff:\tpower off SIDECAR.  All other flags are ignored.\n\n"

volatile int exposureInProgress;
volatile char initializingStatus[SIDECAR_MAX_STRING_INITIALZING];

int getch() {
  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;
}

void displayValue(const char *name, const char *value) {
  
  if (strcmp(name, "exposureStatus") == 0) {
    printf(" - %s.\n", value);
  } else if (strcmp(name, "exposureStatusDetail") == 0) {
    printf("   - %s.\n", value);
  } else if (strcmp(name, "heartbeat") == 0) {
    /* ignore */
  } else {
    printf("*** %s = %s.\n", name, value);

    if (strcmp(name, "exposureInProgress") == 0) {
      exposureInProgress = (int)atoi(value);
    } else if (strcmp(name, "initializeSidecar") == 0) {
      strncpy((char *)initializingStatus, value, SIDECAR_MAX_STRING_INITIALZING);
    }
  }
}

int main(int argc, char *argv[]) {
  int retval;
  char s;
  char fitsFilename[FILENAME_MAX_LENGTH];
  char configFilename[FILENAME_MAX_LENGTH];
  char timingFilename[FILENAME_MAX_LENGTH];
  char outdir[FILENAME_MAX_LENGTH];
  char serverHost[HOSTNAME_MAX_LENGTH];
  int serverPort;
  int tempSampmode;
  int argi;
  const char* dataOnAck;
  int newDataOnAck;
  char* dataOnAckStop;
  char newDataOnAckString[10];
  int confirm=1;
  int poweroff=0;
  const char* response;
  int downloadFirmware = 0;
  long integrationTime;
  int numberOfResets;
  int numberOfReads;
  int numberOfCoadds;
  int numberOfGroups;
  int samplingMode;
  int startx;
  int starty;
  int endx;
  int endy;

  time_t        caltime;
  struct tm     *tm;
  char tempFilename[FILENAME_MAX_LENGTH];

  struct ValueListener l;

  l.setValueFunc = displayValue;

  setLogTarget(LOGTO_STDOUT);
/*  setLogLevel(LOGLVL_INFO_AND_ABOVE);  */
  setLogLevel(LOGLVL_ALL);
  

  /* fill configuration defaults */
  integrationTime = 10000000;
  numberOfResets = 1;
  numberOfReads = 1;
  numberOfCoadds = 1;
  numberOfGroups = 2;
  samplingMode = SAMPLING_MODE_CDS;
  startx = 0;
  starty = 0;
  endx = 2047;
  endy = 2047;

  outdir[0]='\0';
  fitsFilename[0]='\0';
  tempFilename[0]='\0';
  configFilename[0]='\0';
  timingFilename[0]='\0';

  strncpy(serverHost, SIDECAR_SERVER_HOSTNAME, HOSTNAME_MAX_LENGTH);
  serverPort = SIDECAR_SERVER_PORT;

  /* parse arguments  

 USAGE: SidecarTakeExposure [-i itime] [-c coadds] [-s sampling_mode] 
	  [-r reads] [-o outdir] [-f filename] [-z resets] [-h host] -[-p port] [-C serverConfigFile] [-P personalityTimingFile] [-x startx] [-X endx] [-y starty] [-Y endy] [-N]
  */
  for (argi = 1; argi<argc; argi++) {
    if (strcmp(argv[argi], "-i") == 0) {
      argi++;
      integrationTime = (long)atol(argv[argi]);
    } else if (strcmp(argv[argi], "-c") == 0) {
      argi++;
      numberOfCoadds = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-g") == 0) {
      argi++;
      numberOfGroups = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-s") == 0) { 
      argi++;     
      if ((tempSampmode = atoi(argv[argi])) != 0) {
	if ((tempSampmode < 1) || (tempSampmode > 4)) {
	  printf("sampmode must be between 1 and 4\n\n%s\n", USAGE);
	  return -1;
	}
	samplingMode = tempSampmode;
      } else if (strcasecmp(argv[argi], "single") == 0) {
	samplingMode=1;
      } else if (strcasecmp(argv[argi], "cds") == 0) {
	samplingMode=2;
      } else if (strcasecmp(argv[argi], "mcds") == 0) {
	samplingMode=3;
      } else if (strcasecmp(argv[argi], "utr") == 0) {
	samplingMode=4;
      } else {
	  printf("Invalid sampling mode\n\n%s\n", USAGE);
	  return -1;
      }
    } else if (strcmp(argv[argi], "-r") == 0) {
      argi++;
      numberOfReads = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-z") == 0) {
      argi++;
      numberOfResets = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-x") == 0) {
      argi++;
      startx = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-X") == 0) {
      argi++;
      endx = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-y") == 0) {
      argi++;
      starty = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-Y") == 0) {
      argi++;
      endy = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-o") == 0) {
      argi++;
      strncpy(outdir, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-f") == 0) {
      argi++;
      strncpy(tempFilename, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-C") == 0) {
      argi++;
      strncpy(configFilename, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-P") == 0) {
      argi++;
      strncpy(timingFilename, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-h") == 0) {
      argi++;
      strncpy(serverHost, argv[argi], HOSTNAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-p") == 0) {
      argi++;
      serverPort = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-N") == 0) {
      confirm = 0;
    } else if (strcmp(argv[argi], "-poweroff") == 0) {
      poweroff = 1;
    } else {
      printf("invalid argument: %s\n\n%s", argv[argi], USAGE);
      return 0;
    }
  }

  if ((integrationTime < 0) ||
    (numberOfCoadds < 1) ||
    (numberOfReads < 1) || 
    (numberOfResets < 1) ||
    (numberOfGroups < 2) ||
    (samplingMode < 1) || 
    (samplingMode > 4) ||
    (startx < 0) ||
    (starty < 0) ||
    (endx > 2047) ||
    (endy > 2047) ||
    (startx > endx) ||
    (starty > endy)) {
    printf("%s", USAGE);
    return -1;
  }

  if (strcmp(outdir, "") == 0) {
/*    strcpy(outdir, "/net/hydrogen/data/projects/gpi/data/sidecar"); */
    strcpy(outdir, "\\\\Hydrogen\\data\\projects\\gpi\\data\\sidecar");
  }

  if (strcmp(tempFilename, "") == 0) {

    /* construct fits filename in the form:
       sidecarAdapter_YYMMDD_HHmmss.fits */

    /* get system time */
    caltime=time(NULL);
    tm=localtime(&caltime);
  
    /* see Stevens p. 158 for formatting info */
    strftime(tempFilename, 32, "sidecarAsic_%y%m%d_%H%M%S.fits", tm);

  }
  
  /* 92 = '\', 58 = ':' */
  if ((tempFilename[0] != 92) && (tempFilename[1] != 58)) {

    sprintf(fitsFilename, "%s\\\\%s", outdir, tempFilename);
  } else {
    strcpy(fitsFilename, tempFilename);
  }

  llog_msg(LOGLVL_INFO, "Test program for Sidecar.\n");
  
  if (poweroff == 1) {

    llog_msg(LOGLVL_INFO, "POWER DOWN SIDECAR ASIC?\n");

  } else {

    llog_msg(LOGLVL_INFO, "Coadds = %d.\n", numberOfCoadds);
    llog_msg(LOGLVL_INFO, "Itime = %ld.\n", integrationTime);
    llog_msg(LOGLVL_INFO, "Sampling Mode = %d. (1=Single, 2=CDS, 3=MCDS, 4=UTR)\n", samplingMode);
    llog_msg(LOGLVL_INFO, "Reads = %d.\n", numberOfReads);
    llog_msg(LOGLVL_INFO, "Groups = %d.\n", numberOfGroups);
    llog_msg(LOGLVL_INFO, "Resets = %d.\n", numberOfResets);
    llog_msg(LOGLVL_INFO, "StartX = %d.\n", startx);
    llog_msg(LOGLVL_INFO, "StartY = %d.\n", starty);
    llog_msg(LOGLVL_INFO, "EndX = %d.\n", endx);
    llog_msg(LOGLVL_INFO, "EndY = %d.\n", endy);
    llog_msg(LOGLVL_INFO, "Fits filename = %s.\n", fitsFilename);
    
  }
  if (confirm != 0) {
    printf("confirm? (y/N):");
    
    s=getch();
  } else {
    s='y';
  }
  if (s != 'y') {
    printf("\nexiting...\n");
    return 0;
  }


  if ((sidecar_connectToServer(serverHost, serverPort)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
    return -1;
  }

  sidecar_addCallbackListener(&l);

  if (poweroff == 1) {
    llog_msg(LOGLVL_TRACE, "Powering down SIDECAR...\n");
    if ((retval = sidecar_powerDownASIC(SIDECAR_DEFAULT_JADE_NAME)) < 0) {
      llog_msg(LOGLVL_FATAL, "Error powering down SIDECAR.  Error code: %d\n", retval);
    } else {
      llog_msg(LOGLVL_INFO, "SIDECAR succesfully powered down.\n");
    }
    return retval;
  }

  if (strcmp(configFilename, "") != 0) {
    llog_msg(LOGLVL_INFO, "Setting server configuration file: %s.\n", configFilename);
    if ((sidecar_readSidecarControlConfiguration(configFilename)) < 0) {
      llog_msg(LOGLVL_ERROR, "Error reading configuration file %s.\n", configFilename);
      return -2;
    }
    /* server must be initialized for effect */
    s = 'y';
  } else {

    /* if server config file not pass in, ask if should init hardware */
    if (confirm != 0) {
      printf("Initialize hardware? (y/N):");
      s = getch();
    } else {
      s='n';
    }
  }
  if (s == 'y') {

    printf("Download Firmware? (y/N):");
    s = getch();

    if (s == 'y') {
      downloadFirmware = 1;
    }

    llog_msg(LOGLVL_TRACE, "Initializing Hardware...\n");
    response = sidecar_initializeSidecar(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, downloadFirmware);
    /* wait until complete */
    if (strcmp(response, "initializing") == 0) {
      while (strcmp(response, "initializing") == 0) {
	sleep(1);
	llog_msg(LOGLVL_TRACE, "Waiting for SIDECAR initialization to complete.  status = <%s>\n", response);
	response = strdup((const char *)initializingStatus);
      }
    } else {
      return -1;
    }
    if (strcmp(response, SIDECAR_SUCCESSFUL_RESULT) != 0) {    
      llog_msg(LOGLVL_ERROR, "Error initializing SIDECAR: %s.\n", response);
      return -1;
    }
    llog_msg(LOGLVL_TRACE, "initializeSidecar response=%s.\n", response);

    llog_msg(LOGLVL_TRACE, "Hardware Initialized.\n");

    if (strlen(timingFilename) > 0) {

      llog_msg(LOGLVL_TRACE, "Downloading timing file.\n");
      response = sidecar_downloadMCDFile(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, timingFilename);
      if (strcmp(response, SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE) != 0) {    
	llog_msg(LOGLVL_ERROR, "Error downloading MCD file: %s.\n", response);
	return -2;
      }
      llog_msg(LOGLVL_DEBUG, "dowloadMCDFile response=%s.\n", response);
      usleep(SIDECAR_PAUSE_BETWEEN_COMMANDS);

      /* turn dataOnAck back on */
      llog_msg(LOGLVL_TRACE, "Getting current dataOnAck value...\n");
      dataOnAck = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008");
      llog_msg(LOGLVL_DEBUG, "Current dataOnAck value=%s.\n", dataOnAck);
      /*  dataOnAck in form of 0x0000 */
      newDataOnAck = strtol(dataOnAck+2, &dataOnAckStop, 16) | 0x0001;
      sprintf(newDataOnAckString, "0x%4x", newDataOnAck);
      llog_msg(LOGLVL_DEBUG, "newDataonAck: %s.\n", newDataOnAckString);
      
      llog_msg(LOGLVL_TRACE, "Writing dataOnAck to Jade register...\n");
      response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", newDataOnAckString);
      llog_msg(LOGLVL_DEBUG, "WriteJadeRegister response = %s.\n", response);

      usleep(SIDECAR_PAUSE_BETWEEN_COMMANDS);
      
      llog_msg(LOGLVL_TRACE, " Timing file downloaded.\n");
    }
  }

  llog_msg(LOGLVL_TRACE, "Setting exposure extents...\n");
  if ((retval = sidecar_setExposureExtents(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, startx, starty, endx, endy)) < 0) {
    llog_msg(LOGLVL_FATAL, "Error setting exposure extents.  Error code: %d\n", retval);
    return retval;
  }

  llog_msg(LOGLVL_TRACE, "Starting exposure...\n");
  if ((retval = sidecar_takeExposure(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, numberOfResets, integrationTime, numberOfCoadds, samplingMode, numberOfReads, numberOfGroups, fitsFilename)) < 0) {
    llog_msg(LOGLVL_FATAL, "Error starting exposure.  Error code: %d\n", retval);
    return retval;
  }
  llog_msg(LOGLVL_TRACE, "Exposure started.\n");

  /* wait for exposure to be complete */
  exposureInProgress = 1;
  while (exposureInProgress) {
    usleep(100000);
  }

  llog_msg(LOGLVL_INFO, "Exposure complete.\n");


  llog_msg(LOGLVL_INFO, "Finished\n");

  return 0;
}



