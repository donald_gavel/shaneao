/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *
 *  @file SidecarIceCallback.cpp
 *
 *  @brief
 *  Functions specific to the Sidecar ICE callback.
 *
 *  This module contains routines to receive a callback 
 *  from the Windows Sidecar ICE server.
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 *  
 */
#include "SidecarIceCallback.h"

/** Callback routine for ICE server.
 *
 * Receives name-value pair callback from ICE server, and 
 * calls setValue function on list of registered listeners.
 * 
 * @param name [in] name of parameter that is changes
 * @param value [in] new value of parameter
 *
 * @return none
 */
void SidecarIceCallback::setValue(const string &name, const string &value, const Ice::Current&) {
  if (listeners.empty()) {
    cout << "Callback: " << name << " = " << value << endl;
  } else {
    for (vector<SidecarIceValueListener *>::iterator iter = listeners.begin() ; iter != listeners.end(); ++iter) {
      (*iter)->setValue(name, value);
    }
  } 
}

/** Register a callback listener
 *
 * Adds a new SidecarIceValueListener to the list of register
 * callback listeners.  The setValue method of these
 * listeners is called on an ICE callback.
 *
 * @param l [in] SidecarIceValueListener to add to list
 *
 * @return none
 */
void SidecarIceCallback::addValueListener(SidecarIceValueListener *l) {
  for (vector<SidecarIceValueListener *>::iterator iter = listeners.begin() ; iter != listeners.end(); ++iter) {
    if ((*iter) == l) {
      cout << "SidecarIceCallback: listener already added." << endl;
      return;
    }
  }
  
  cout << "SidecarIceCallback: new listener added." << endl;
  listeners.push_back(l);
}

/** Remover a callback listener
 *
 * Removes a SidecarIceValueListener from the list of register
 * callback listeners.  This function is not currently functional.
 *
 * @param l [in] SidecarIceValueListener to remove from list
 *
 * @return none
 */
void SidecarIceCallback::removeValueListener(SidecarIceValueListener *l) {
  /* todo */
}
