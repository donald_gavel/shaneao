/*  to compile:

gcc -o SidecarGetAsicRegister SidecarGetAsicRegister.c lerrlog.c -I/opt/Ice-3.3.1/include -I. -g -fPIC -Wall -DHAVE_CONFIG_H -L. -lSidecarIceClient -L/opt/Ice-3.3.1/lib -lIce -lIceUtil -lstdc++

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <time.h>

#include "Sidecar.h"
#include "SidecarIceClient.h"
#include "lerrlog.h"

#include "SidecarAdapter.h"

int readASICRegister(char *address) {
  const char *currentVal;

  currentVal = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, address, 0);

  llog_msg(LOGLVL_DEBUG, "ASIC register %s = %s\n", address, currentVal);

  return 0;
}


int main(int argc, char *argv[]) {
  int err=0;

  setLogTarget(LOGTO_STDOUT);
  setLogLevel(LOGLVL_ALL);
  
  llog_msg(LOGLVL_INFO, "Test program for Sidecar.\n");
  
  if ((sidecar_connectToServer(SIDECAR_SERVER_HOSTNAME, SIDECAR_SERVER_PORT)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
  } else {


    err=readASICRegister(argv[1]);
    
    llog_msg(LOGLVL_INFO, "Finished\n");
  }
  return err;
}
