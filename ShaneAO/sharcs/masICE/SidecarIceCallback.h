/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file SidecarIceCallback.h
 *
 *  @brief
 *  Header file specific to the Sidecar ICE callback.
 *
 *  This module contains function prototypes for the Sidecar ICE callback
 *  for both c and c++.
 *
 *  @sa SidecarAdapter.c
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _GPIFSIDECAR_ICE_CALLBACK_H_
#define _GPIFSIDECAR_ICE_CALLBACK_H_

#ifdef __cplusplus

#include <Ice/Ice.h>
#include <SidecarIce.h>
#include <SidecarIceValueListener.h>
#include <vector>

using namespace std;
using namespace Sidecar::SidecarIce;

class SidecarIceCallback : public ISidecarIceCallback {
  private:
  vector<SidecarIceValueListener *> listeners;
  public:
  SidecarIceCallback() {};
  virtual void setValue(const string &name, const string &value, const Ice::Current&);
  void addValueListener(SidecarIceValueListener *l);
  void removeValueListener(SidecarIceValueListener *l);
};
#endif /* __cplusplus */

#endif /* _GPIFSIDECAR_ICE_CALLBACK_H_ */
