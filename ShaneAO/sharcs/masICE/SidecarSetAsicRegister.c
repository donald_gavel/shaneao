/*
  to compile:

  gcc -o SidecarSetAsicRegister SidecarSetAsicRegister.c lerrlog.c -I/opt/Ice-3.3.1/include -I. -g -fPIC -Wall -DHAVE_CONFIG_H -L. -lSidecarIceClient -L/opt/Ice-3.3.1/lib -lIce -lIceUtil -lstdc+

 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <time.h>
#include <string.h>

#include "Sidecar.h"
#include "SidecarIceClient.h"
#include "lerrlog.h"

#include "SidecarAdapter.h"


int setASICRegister(char *address, char *value) {
  const char *result;;
  int err=0;

  result = sidecar_writeAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, address, value);
  
  llog_msg(LOGLVL_DEBUG, "response = %s\n", result);

  if (strstr(result, "successful") == 0) {
    llog_msg(LOGLVL_DEBUG, "Error setting ASIC register %s to %s\n", address, value, 0);
    err = -1;
  } else {
    llog_msg(LOGLVL_DEBUG, "ASIC register %s set to %s\n", address, value);
  }

  return err;

}

int main(int argc, char *argv[]) {
  int err=0;

  setLogTarget(LOGTO_STDOUT);
  setLogLevel(LOGLVL_ALL);
  
  llog_msg(LOGLVL_INFO, "Test program for Sidecar.\n");
  
  if ((sidecar_connectToServer(SIDECAR_SERVER_HOSTNAME, SIDECAR_SERVER_PORT)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
  } else {


    err=setASICRegister(argv[1], argv[2]);
    
    llog_msg(LOGLVL_INFO, "Finished\n");
  }
  return err;
}
