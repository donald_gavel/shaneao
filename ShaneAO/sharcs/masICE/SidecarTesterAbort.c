#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "SidecarIceClient.h"
#include "Sidecar.h"
#include "lerrlog.h"

#include "SidecarAdapter.h"

#define HOSTNAME_MAX_LENGTH 1024

int main(int argc, char *argv[]) {

  int abortLevel=1;
  char serverHost[HOSTNAME_MAX_LENGTH];
  int serverPort;
  int argi;
  
  setLogTarget(LOGTO_STDOUT);
  setLogLevel(LOGLVL_INFO_AND_ABOVE);

  strncpy(serverHost, SIDECAR_SERVER_HOSTNAME, HOSTNAME_MAX_LENGTH);
  serverPort = SIDECAR_SERVER_PORT;

  for (argi = 1; argi<argc; argi++) {
    if (strcmp(argv[argi], "-h") == 0) {
      argi++;
      strncpy(serverHost, argv[argi], HOSTNAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-p") == 0) {
      argi++;
      serverPort = (int)atoi(argv[argi]);
    } else {
      abortLevel = atoi(argv[argi]);
    }
  }

  if ((sidecar_connectToServer(serverHost, serverPort)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
    return -1;
  }

  if (sidecar_abortExposure(abortLevel, 1) < 0) {
    llog_msg(LOGLVL_ERROR, "Error sending abort (level = %d)\n", abortLevel);
  }

  return 0;

}
