#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <time.h>

#include "Sidecar.h"
#include "SidecarIceClient.h"
#include "SidecarAdapter.h"
#include "lerrlog.h"


void test1() {
      llog_msg(LOGLVL_TRACE, "calling openApp\n");
      llog_msg(LOGLVL_DEBUG, "openApp returned %s\n", sidecar_openApp(SIDECAR_DEFAULT_APPLICATION_FILENAME));

     llog_msg(LOGLVL_DEBUG, "Download Jade Firmware: %s\n", sidecar_downloadJADEFirmware(SIDECAR_DEFAULT_JADE_NAME));
      llog_msg(LOGLVL_DEBUG, "Download Jade Registers: %s\n", sidecar_downloadJADERegisters(SIDECAR_DEFAULT_JADE_NAME));

      llog_msg(LOGLVL_DEBUG, "Turn data on ack on: %s\n", sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", "0x3e81"));

      usleep(1000000);
/*
      llog_msg(LOGLVL_DEBUG, "Download MCD: %s\n", sidecar_downloadMCDFile(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, SIDECAR_DEFAULT_H2RG_MCD_FILENAME));
*/
      llog_msg(LOGLVL_DEBUG, "Download MCD: %s\n", sidecar_downloadMCD(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME));

      
}

void test2() {
  int addr[3];
  long values[3];
  int ii;

  addr[0]=0x4000;
  addr[1]=0x4001;
  addr[2]=0x4002;

  llog_msg(LOGLVL_TRACE, "reading register block\n");
  llog_msg(LOGLVL_DEBUG, "readAsicRegisterBlock returned %s\n", sidecar_readAsicRegisterBlock(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, 0, 0, addr, values, 3));
  
  for (ii=0; ii<3; ii++) {
    printf("Value[%d] = %ld.\n", ii, values[ii]);
  }


}

int test3() {
  int err;
  int words = 2048*2048;

  int *array;

  clock_t start, end;

  array = (int *)malloc(words * sizeof (int));

  err=sidecar_takeExposure(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, 1,10000,1,3,4,2,"temp.fits"); 

  start = clock();
  sidecar_readScienceData(SIDECAR_DEFAULT_JADE_NAME, words, array);
  end = clock();

  printf("time: %f.\n", (double)(end-start)/CLOCKS_PER_SEC);

  return err;
}

int test4() {
  int err;

  err=sidecar_takeExposure(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, 1,10000,1,3,4,2,"temp.fits"); 

  return err;
}

int test5() {
  int err;

/*  err = sidecar_readSidecarControlConfiguration("C:\\sidecar\\h2rgTestingSidecarControlConfig.xml"); */
  err = sidecar_readSidecarControlConfiguration("C:\\sidecar\\defaultSidecarControlConfig.xml");

  return err;
}

int test6() {
      llog_msg(LOGLVL_DEBUG, "Turn data on ack off: %s\n", sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0008", "0x3e80"));

  llog_msg(LOGLVL_DEBUG, "readAsicReg 0x6000: %s\n", sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x6000", 0));

  return 0;

}

int test7() {
  const char *currentVal;
  int newVal;
  char *newValStop;
  char newValString[10];

  currentVal = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0020");

  llog_msg(LOGLVL_DEBUG, "Jade Reg 0x0020 = %s\n", currentVal);

  newVal = strtol(currentVal+2, &newValStop, 16) & 0xF0FF;
  sprintf(newValString, "0x%4x", newVal);

  sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0020", newValString);

  return 0;

}

int main(int argc, char *argv[]) {
  int err=0;

  setLogTarget(LOGTO_STDOUT);
  setLogLevel(LOGLVL_ALL);
  
  llog_msg(LOGLVL_INFO, "Test program for Sidecar.\n");
  
  if ((sidecar_connectToServer(SIDECAR_SERVER_HOSTNAME, SIDECAR_SERVER_PORT)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
  } else {
/*
    err=test5();
    test1();
*/ 
    /*err=test7();*/
/*   
*/
      llog_msg(LOGLVL_INFO, "\nFinished\n");
  }
  return err;
}
