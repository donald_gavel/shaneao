#ifndef SIDECAR_ICE
#define SIDECAR_ICE

#include <Ice/Identity.ice>

module Sidecar {
  module SidecarIce {
    sequence<int> intSeq;
    sequence<long> longSeq;
    
    enum SidecarIceFitsHeaderKeyType {	
	SidecarIceKeyTypeBoolean,	
	SidecarIceKeyTypeInteger,
	SidecarIceKeyTypeLong,
	SidecarIceKeyTypeFloat,
	SidecarIceKeyTypeDouble,
	SidecarIceKeyTypeString,
	SidecarIceKeyTypeComment
    };	

    struct SidecarIceFitsHeaderKey {
	string name;
	string value;
	SidecarIceFitsHeaderKeyType type;
	string comment;
    };

    ["clr:generic:List"] sequence<SidecarIceFitsHeaderKey> SidecarIceFitsHeader;

    interface ISidecarIceCallback
    {
	void setValue(string name, string value);
    };

    interface ISidecarIceCom
    {
	string echo(string s);
	int validateFilename(string sFilename);
	void addCallbackClient(Ice::Identity ident);
	int setExposureExtents(string sJade, string sAsic, int startx, int starty, int endx, int endy);
	int takeExposure(string sJade, string sAsic, int resets, long itimeInMicros, int coadds, int sampMode, int reads, int groups, string filename);
	int abortExposure(int abortLevel, bool saveImage);
	int getExposureStatus(out double percentComplete, out string statusMessage);
	int readSidecarControlConfiguration(string configFile);
	int resetASIC(string sJade);
	int addFitsHeader(string fitsFilename, SidecarIceFitsHeader header);
	string initializeSidecar(string sJade, string sAsic, bool downloadFirmware);
	int powerDownASIC(string sJade);
	int broadcastValue(string valueName);
	int getReadTimeInMicroseconds(string sJade, string sAsic);
	int setReferencePixelSubtractionType(int type);
	int writeRawFrames(bool enable);
	 string DownloadAllConfiguredAsics(string sJade, bool MCDs, bool Registers);
	 string DownloadASICRegisterFile(string sJade, string sAsic, string sFilename);
	 string DownloadASICRegisters(string sJade, string sAsic);
	 string DownloadJADEFirmware(string sJade);
	 string DownloadJADEFirmwareFile(string sJade, string sFilename);
	 string DownloadJADERegisterFile(string sJade, string sFilename);
	 string DownloadJADERegisters(string sJade);
	 string DownloadMCD(string sJade, string sAsic);
	 string DownloadMCDFile(string sJade, string sAsic, string sMCDFileName);
	 string GetAsics(string sJade);
	 int GetAutoWrite();
	 string GetAvailableAsics(string sJade);
	 string GetAvailableJades();
	 int GetDMA();
	 string GetJades();
	 int GetMemoryBlockMode();
	 string OpenApp(string sFileName);
	 long ReadAsicReg(string sJade, string sAsic, long addr, bool is24bit);
	 string ReadAsicRegister(string sJade, string sAsic, string sMem, bool is24bit); 
	 string ReadAsicRegisterBlock(string sJade, string sAsic,  bool is24bit, bool FIFOreset, intSeq addrArray, out longSeq valueArray);
	 long ReadJadeReg(string sJade, long addr);
	 string ReadJadeRegister(string sJade, string sMem);
	 string ReadScienceData(string sJade, int numWords, out intSeq targetArray);
	 long ReadUSBPort(string sJade, long Address);
	 string ReadUSBPortDirectionAsString(string sJade, string addr);
	 long ReadUSBPortDirectionAsLong(string sJade, long addr);
	 string SetAutoWrite(int state);
	 string SetBlockMemoryMode(int mode);
	 string SetDMA(int state);
	 string UploadASICRegisters(string sJade, string sAsic);
	 string UploadJADERegisters(string sJade);
	 string WriteAsicReg(string sJade, string sAsic, long addr, long Value);
	 string WriteAsicRegister(string sJade, string sAsic, string addr, string Value);
	 string WriteAsicRegisterBlock(string sJade, string sAsic, bool is24bit, intSeq addrArray, longSeq valueArray);
	 string WriteJadeReg(string sJade, long addr, long Value);
	 string WriteJadeRegister(string sJade, string sMem, string Value);
	 string WriteScienceData(string sJade, intSeq targetArray);
	 string WriteUSBPort(string sJade, long Address, byte Data);
	 string WriteUSBPortDirection(string sJade, long Address, byte Data);
     };
  };
};

#endif
