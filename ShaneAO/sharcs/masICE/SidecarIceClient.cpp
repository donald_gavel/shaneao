/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *
 *  @file SidecarIceClient.cpp
 *
 *  @brief
 *  Functions specific to the Sidecar ICE client.
 *
 *  This module contains routines to connect from the linux host
 *  to the Windows Sidecar ICE server.
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 *  
 */
#include "SidecarIceClient.h"

#include <vector>

SidecarIceClient* SidecarIceClient::single = NULL;
SidecarIceClient* SidecarIceClient::getInstance()

{
    if(single == NULL)
    {
        single = new SidecarIceClient();
        return single;
    }
    else
    {
        return single;
    }
}

/** Constructor for SidecarIceClient
 *
 * @param none
 *
 * @return SidecarIceClient object
 *
 */
SidecarIceClient::SidecarIceClient() {
  proxyTimeout=5000;  /* ms */
  isCallbackThreadRunning = false;
  getNewProxy = true;
  /*  try {
    cout << "Initializing ICE connection...";
    Ice::PropertiesPtr props = Ice::createProperties();
    
    //. set max size of message to be one 2048x2048x4 byte frame
    //. plus a bit more for message overhead  (in kB)
    //.  = 2048 x 2048 x 4 bytes / 1024 bytes/kb + 4 kb
    props->setProperty("Ice.MessageSizeMax", "16388");
    props->setProperty("Ice.ACM.Client", "0");
    props->setProperty("Ice.ThreadPool.Client.SizeMax", "10");
    props->setProperty("Ice.ThreadPool.Server.SizeMax", "10");

    Ice::InitializationData id;
    id.properties = props;
	
    ic = Ice::initialize(id);
    cout << "success." << endl;
  } catch (const Ice::Exception & ex) {
      cout << "Error initializing communicator." << endl;
      cerr << "Ice Exception " << ex << endl;
  }
  */
}

/** Connect to ICE client to sidecar server
 *
 * @param hostname [in] Host to connect to
 * @param port [in] Port to connect to
 *
 * @return 0 on success
 * @return -1 on Error starting callback thread
 * @return -2 on Ice Exception
 * @return -3 on General error
 */
int
SidecarIceClient::connectToServer(const char *hostname, unsigned int port)
{
  //. todo: handle memory? possible leak
  int status = 0;
  pthread_t t;

  try {
    /* if server is currently running, shut it down and restart */
    if (ic != NULL) {
      if (!ic->isShutdown()) {
	cout << "Shutting down ICE Communicator Server" << endl;
	server_shutdown();
      }
    }

    cout << "Initializing ICE connection...";
    Ice::PropertiesPtr props = Ice::createProperties();

    //. set max size of message to be one 2048x2048x4 byte frame
    //. plus a bit more for message overhead  (in kB)
    //.  = 2048 x 2048 x 4 bytes / 1024 bytes/kb + 4 kb
    props->setProperty("Ice.MessageSizeMax", "16388");
    props->setProperty("Ice.ACM.Client", "0");
    props->setProperty("Ice.ThreadPool.Client.SizeMax", "10");
    props->setProperty("Ice.ThreadPool.Server.SizeMax", "10");
      
    Ice::InitializationData id;
    id.properties = props;
	
    ic = Ice::initialize(id);
    cout << "success." << endl;
    
    if (getNewProxy) {
      char url[1024];
      sprintf(url, "SidecarIce:default -h %s -p %d -t %d", hostname, port, proxyTimeout);
      cout << "Getting ICE base... using " << url << endl;

      Ice::ObjectPrx iceBase = ic->stringToProxy(url);
      cout << " success." << endl;

      if (!iceBase) {
        cout << "Error creating ObjectPrx." << endl;
      } else {
        cout << "ObjectPrx OK." << endl;

	cout << "Creating sidecar object..." << endl;
	sidecarProxy = ISidecarIceComPrx::checkedCast(iceBase);
	cout << "Cast OK" << endl;

	if (!sidecarProxy) {
	  cout << "Error creating sidecar object." << endl;
	  throw "Invalid proxy";
	}

	cout << "Sidecar object created." << endl;
	getNewProxy = false;
	
	if (!isCallbackThreadRunning) {
	  cout << "Starting callback thread..." << endl;
	  
	  if (pthread_create(&t, NULL, callbackThread, this) < 0) {
	    cout << "Error starting callback thread." << endl;
	    status = -1;
	  } else {
	    /* isCallbackThreadRunning set to true in thread */
	    do {
	      sleep(1);
	    } while (callbackThreadInitializing);
	  }
	} else {
	  cout << "Callback thread already running." << endl;
	}
	cout << "Initialization complete." << endl; 
      }      
    }
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception " << ex << endl;
    status = -2;
  } catch (const char * msg) {
    cerr << "General error " <<msg << endl;
    status = -3;
  }
  cout << "Returning with status " << status << endl;
  return status;
}

/** Method to shutdown ICE communicator.
 *
 * @param none
 *
 * @return none
 */
void
SidecarIceClient::server_shutdown() {
  ic->shutdown();
  /*  ic->waitForShutdown();
      ic->destroy();  */
  getNewProxy = true;
}

/** Accessor method to set whether callback thread is running or not
 *
 * @param status [in] Whether callback thread is running
 *
 * @return none
 */
void
SidecarIceClient::setCallbackThreadRunning(bool status) {
  isCallbackThreadRunning=status;
}

/** Callback thread for SIDECAR Ice Client
 *
 * Registers the callback method of the SidecarIceClient object 
 *  with Sidecar Server as function to be called for callbacks.
 * 
 * @param obj [in] pointer to SidecearIceClient object
 *
 * @sa gpIfSidecarIceCallback.cpp
 *
 * @return none
 */
void *callbackThread(void *obj) {
  int err=0;
  SidecarIceClient *client;

  client = (SidecarIceClient *) obj;

  client->callbackThreadInitializing = true;

  try {
    cout << "Creating object adapter for callback." << endl;
    Ice::ObjectAdapterPtr adapter = client->ic->createObjectAdapter("");
    Ice::Identity ident;
    ident.name = IceUtil::generateUUID();
    ident.category = "";
    cout << "Adding callback to adapter." << endl;
    adapter->add(&(client->callback), ident);
    cout << "Activating callback adapter." << endl;
    adapter->activate();
    cout << "Setting callback adapter in proxy object" << endl;
    client->sidecarProxy->ice_getConnection()->setAdapter(adapter);
    cout << "Adding callback client to SidecarServer server" << endl;
    client->sidecarProxy->addCallbackClient(ident);

    client->callbackThreadInitializing = false;

    /* todo: synchronize? */
    cout << "Callback Thread running..." << endl;
    client->setCallbackThreadRunning(true);

    client->ic->waitForShutdown();
    
    cout << "Callback Thread exiting..." << endl;
    client->setCallbackThreadRunning(false);
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception " << ex << endl;
  } catch (const char * msg) {
    cerr << "General error " <<msg << endl;
  }
 
  pthread_exit((void *)err);
}

/** Creates a new SidecarIceValueListenerAdapter object from passed in ValueListener and registers this object as callback function
 *
 * @param l [in] valueListener structure with reference to callback method
 *
 * @sa SidecarIceCallback.cpp
 *
 * @return none
 */
void 
SidecarIceClient::addCallbackListener(struct ValueListener *l) {
  /* todo: allow client to have multiple ValueListeners 
   * this would involve removing valueListenerAdapter as a member 
   * problem: how do you prevent listeners added that have same
   * callback function?
   */
  valueListenerAdapter.setValueListener(l);
  
  callback.addValueListener(&valueListenerAdapter);
}

/**
 * Echo string in server
 *
 * @param s string to echo
 * 
 * @return same string that was echoed
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *SidecarIceClient::echo(const char *s) {
  try {
    return sidecarProxy->echo(s).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Sets the detector subarray extents for next exposure.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param startx  leftmost column of detector subarray
 * @param starty  bottommost row of detector subarray
 * @param endx    rightmost column of detector subarray
 * @param endy    topmost row of detector subarray
 *
 * @return Error code fo setExposureExtents call
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */

int SidecarIceClient::setExposureExtents(const char *sJade, const char *sAsic, int startx, int starty, int endx, int endy) {
  try {
    return sidecarProxy->setExposureExtents(sJade, sAsic, startx, starty, endx, endy);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Take an exposure on server side.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param resets  Number of detector resets at start of integration
 * @param itimeInMicros  Integration time in microseconds
 * @param coadds     Number of coadditions
 * @param sampMode   Sampling mode (1=Single, 2=CDS, 3=MCDS/Folwer, 4=UTR)
 * @param reads      Number of sampled reads (pairs for CDS and MCDS)
 * @param groups      Number of sampled groups (2 for CDS and MCDS)
 * @param filename   Name of FITS file to be written.  Must be a valid Windows filename
 *
 * @return Error code for takeExposure call
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::takeExposure(const char *sJade, const char *sAsic, int resets, uint64_t itimeInMicros, int coadds, int sampMode, int reads, int groups, const char *filename) {
  try {
    return sidecarProxy->takeExposure(sJade, sAsic, resets, itimeInMicros, coadds, sampMode, reads, groups, filename);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Abort an exposure on server side.
 *
 * @param abortLevel [in] Level for abort (1=immediately, 2=after current read, 3=after current coadd)
 * @param saveImage [in] Whether or not to save the abort image to disk
 * 
 * @return Error code from abortExposure call
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::abortExposure(int abortLevel, bool saveImage) {
  try {
    return sidecarProxy->abortExposure(abortLevel, saveImage);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}


/**
 * Validate a filename (for fits file)
 *
 * @param sFilename name of file to check
 * 
 * @return -1 if file not writable
 * @return  0 if file does not exist and directory is writable
 * @return  1 if file exists and is writable
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::validateFilename(const char *sFilename) {
  try {
    return sidecarProxy->validateFilename(sFilename);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Ask SidecarServer to rebroadcast a value
 *
 * @param sValueName  Name of value
 * 
 * @returns Error code
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::broadcastValue(const char *sValueName) {
  try {
    return sidecarProxy->broadcastValue(sValueName);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Get the read time for a frame in microseconds
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * 
 * @returns Read time for a single frame in microseconds
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::getReadTimeInMicroseconds(const char *sJade, const char *sAsic) {
  try {
    return sidecarProxy->getReadTimeInMicroseconds(sJade, sAsic);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Reset ASIC
 *
 * @param sJade   Name of Jade
 * 
 * @return Error code
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::resetASIC(const char *sJade) {
  try {
    return sidecarProxy->resetASIC(sJade);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Power Down ASIC
 *
 * @param sJade   Name of Jade
 * 
 * @return Error code
 */
int SidecarIceClient::powerDownASIC(const char *sJade) {
  try {
    return sidecarProxy->powerDownASIC(sJade);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Adds a list of FITS header keywords to the current exposure FITS file
 *
 * @param keys - array for Fit Header Keys
 * @param numKeys - number of Keys
 * 
 * @return Error code
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::addFitsHeader(const char *fitsFilename, struct FitsHeaderKey *keys, int numKeys) {
  SidecarIceFitsHeader header;

  try {
    //. preallocate space in header vector to speed up push back
    header.reserve(numKeys);
  
    SidecarIceFitsHeaderKey iceKey;
    /* cast to FitsHeader object */
    for (int ii=0; ii<numKeys; ii++) {
      /*      cout << "Working on key, name=<" << keys[ii].name << ">, value=<" << keys[ii].value << ">, comment=<" << keys[ii].comment << ">." << endl; */
      /* convert FitsHeaderkey to SidecarIceFitsHeaderKey */

      iceKey.name.assign(keys[ii].name);
      iceKey.value.assign(keys[ii].value);
      iceKey.comment.assign(keys[ii].comment);

      switch (keys[ii].type) {
      case KeyTypeBoolean: iceKey.type = SidecarIceKeyTypeBoolean;
	break;
      case KeyTypeInteger: iceKey.type = SidecarIceKeyTypeInteger;
	break;
      case KeyTypeLong: iceKey.type = SidecarIceKeyTypeLong;
	break;
      case KeyTypeFloat: iceKey.type = SidecarIceKeyTypeFloat;
	break;
      case KeyTypeDouble: iceKey.type = SidecarIceKeyTypeDouble;
	break;
      case KeyTypeComment: iceKey.type = SidecarIceKeyTypeComment;
	break;
      default: iceKey.type = SidecarIceKeyTypeString;
      };

      /* add to header */
      header.push_back(iceKey);
    }
  
    return sidecarProxy->addFitsHeader(fitsFilename, header);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Initialize the SIDECAR.  Included downloading firmware (optional),
 * downloading Jade registers, resetting ASIC, and  downloading MCD file.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param downloadFirmware whether or not to download Jade firmware
 *
 * @return  the string "initializing" if initializing has started.
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *SidecarIceClient::initializeSidecar(const char *sJade, const char *sAsic, bool downloadFirmware) {
  try {
    return sidecarProxy->initializeSidecar(sJade, sAsic, downloadFirmware).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Causes server to read configuration file for SIDECAR control.
 *
 * @param configFile  Sidecar Control configuration filename (XML)
 *
 * @return Error code 
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::readSidecarControlConfiguration(const char *configFile) {
  try {
    return sidecarProxy->readSidecarControlConfiguration(configFile);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Set reference pixel subtraction mode
 *
 * @param type reference pixel subtraction type
 * 
 * @return Error code 
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::setReferencePixelSubtractionType(int type) {
  try {
    return sidecarProxy->setReferencePixelSubtractionType(type);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Specify whether or not to write raw frames to disk
 *
 * @param enable flag specifying whether or not to write raw frames
 * 
 * @return Error code 
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int SidecarIceClient::writeRawFrames(bool enable) {
  try {
    return sidecarProxy->writeRawFrames(enable);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}
/**
 * Downloads all configurations specified in the solution to the specified Jade. 
 * 
 * @param sJade      A string containing the name of the Jade
 * @param mcds	     Instructs the system to download the mcd files that are specified in the download configuration
 * @param registers	 Instructs the system to download the registers that are specified in the download configuration
 *
 * @return           A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadAllConfiguredAsics(const char *sJade, bool mcds, bool registers) {
  try {
    return sidecarProxy->DownloadAllConfiguredAsics(sJade, mcds, registers).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the ASIC registers in the specified file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sAsic			 A string containing the name of the Asic or Asics separated by commas
 * @param sFilename  A string containing the name of the regiser file
 *
 * @return           A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadASICRegisterFile(const char *sJade, const char *sAsic, const char *sFilename) {
  try {
    return sidecarProxy->DownloadASICRegisterFile(sJade, sAsic, sFilename).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the ASIC registers specified in the solution.
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sAsic  A string containing the name of the Asic or Asics separated by commas
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadASICRegisters(const char *sJade, const char *sAsic) {
  try {
    return sidecarProxy->DownloadASICRegisters(sJade, sAsic).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the specified JADE firmware specified in the solution
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadJADEFirmware(const char *sJade) {
  try {
    return sidecarProxy->DownloadJADEFirmware(sJade).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }

}

/**
 * Downloads the specified JADE firmware file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sFilename  A string containing the name of a file containing the Jade firmware
 *
 * @return           A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadJADEFirmwareFile(const char *sJade, const char *sFilename) {
  try {
    return sidecarProxy->DownloadJADEFirmwareFile(sJade, sFilename).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the JADE registers in the specified file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sFilename  A string containing the name of an xml file containing the Jade registers
 *
 * @return           A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadJADERegisterFile(const char *sJade, const char *sFilename) {
  try {
    return sidecarProxy->DownloadJADERegisterFile(sJade, sFilename).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the specified JADE register specified in the open solution.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string cointaining the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadJADERegisters(const char *sJade) {
  try {
    return sidecarProxy->DownloadJADERegisters(sJade).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the MCD file(s) that were configured in the solution to the jade and asic specified.
 * 
 * @param sJade         A string containing the name of the Jade
 * @param sAsic         A string containing the name of the Asic or Asics separated by commas
 *
 * @return              A string cointaining the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadMCD(const char *sJade, const char *sAsic) { 
  try {
    return sidecarProxy->DownloadMCD(sJade, sAsic).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Downloads the MCD file that is specified to the jade and asic specified.
 * 
 * @param sJade         A string containing the name of the Jade
 * @param sAsic         A string containing the name of the Asic or Asics separated by commas
 * @param sMCDFileName  A string containing the name of an MCD file
 *
 * @return              A string cointaining the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::downloadMCDFile(const char *sJade, const char *sAsic, const char *sMCDFilename) {
  try {
    return sidecarProxy->DownloadMCDFile(sJade, sAsic, sMCDFilename).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Get names of Asics for a Jade
 *
 * @param sJade         A string containing the name of the Jade
 *
 * @return              A string containing the names of asics in comma-delimited list
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::getAsics(const char *sJade) {
  try {
    return sidecarProxy->GetAsics(sJade).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Determine whether AutoWrite is enabled.
 *
 * @param none
 * 
 * @return  An integer indicating whether AutoWrite is enabled: 1 - Enabled, 0 - Disabled
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int 
SidecarIceClient::getAutoWrite() {
  try {
    return sidecarProxy->GetAutoWrite();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Gets a list of strings separated by commas that represents the names of the asics that are currently connected/available/configured.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A comma separated list of asic names or an empty string if no asics are available
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::getAvailableAsics(const char *sJade) {
  try {
    return sidecarProxy->GetAvailableAsics(sJade).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Gets a comma separated list of available Jades.
 *
 * @param none
 *
 * @return   A comma separated list of Jades
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::getAvailableJades() {
  try {
    return sidecarProxy->GetAvailableJades().c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Determine whether DMA is enabled.
 * 
 * @param none
 *
 * @return  An integer indicating whether DMA is enabled: 1 - Enabled, 0 - Disabled
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int 
SidecarIceClient::getDMA() {
  try {
    return sidecarProxy->GetDMA();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Get names of Jades
 * 
 * @param none
 *
 * @return  Names of the Jades in a comma-delimited list. 
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::getJades() {
  try {
    return sidecarProxy->GetJades().c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Determine whether MemoryBlockMode is enabled.
 * 
 * @param none
 *
 * @return  An integer indicating whether MemoryBlockMode is enabled: 1 - Enabled, 0 - Disabled
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int 
SidecarIceClient::getMemoryBlockMode() {
  try {
    return sidecarProxy->GetMemoryBlockMode();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Opens the specified solution which will be used for register definitions and download configurations. 
 * 
 * @param sFilename  A string containing the name of an application (solution) file
 *
 * @return           A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char*
SidecarIceClient::openApp(const char* appFilename) 
{
  try {
    cout << "Opening application: " << appFilename << "." << endl;
    return sidecarProxy->OpenApp(appFilename).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
  
}

/**
 * Reads the Asic register. 
 *
 * @param sJade    A string containing the name of the Jade
 * @param sAsic    A string containing the name of the Asic
 * @param sMem     The address of the register
 * @param is24bit  Indicator that tells the system to read as a 24 bit register or a 16 bit register
 *
 * @return Value of specified ASIC register
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
long 
SidecarIceClient::readAsicReg(const char *sJade, const char *sAsic, long addr, bool is24bit) {
  try {
    return sidecarProxy->ReadAsicReg(sJade, sAsic, addr, is24bit);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Reads the Asic register. 
 *
 * @param sJade    A string containing the name of the Jade
 * @param sAsic    A string containing the name of the Asic
 * @param sMem     The address of the register
 * @param is24bit  Indicator that tells the system to read as a 24 bit register or a 16 bit register
 *
 * @return A string containing value of specified ASIC register
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::readAsicRegister(const char *sJade, const char *sAsic, const char *sMem, bool is24bit) {
  try {
    return sidecarProxy->ReadAsicRegister(sJade, sAsic, sMem, is24bit).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Block reads the registers from Jade specified. 
 * 
 * @param sJade       A string containing the name of the Jade
 * @param sAsic       A string containing the name of the Jade
 * @param addrArray   Array containing addresses to be downloaded (uint16[])
 * @param valueArray  Array containing values to be downloaded (uint32[])
 * @param is24bit     Boolean indicating whether the register is 24 bit should be used
 * @param fifoReset   Boolean indicating whether we should reset the FIFO
 *
 * @return            A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::readAsicRegisterBlock(const char *sJade, const char *sAsic,  bool is24bit, bool FIFOreset, int *addrArray, long *valueArray, int blockSize) {
  try{
    Sidecar::SidecarIce::intSeq addrVector;   
    Sidecar::SidecarIce::longSeq valueVector; 

    //. copy addresses into vector
    for (int ii=0; ii<blockSize; ii++) {
      addrVector.push_back(*addrArray);
      addrArray++;
    }

    string result;
    result = sidecarProxy->ReadAsicRegisterBlock(sJade, sAsic, is24bit, FIFOreset, addrVector, valueVector);
    
    //. copy values in vector back to out array
    for (int jj=0; jj<blockSize; jj++) {
      *(valueArray+jj) = valueVector.at(jj);
    }

    return result.c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Reads a value from specified Jade register address
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sMem   Address of the Jade register
 *
 * @return Value of the specified Jade register
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */ 
long 
SidecarIceClient::readJadeReg(const char *sJade, long addr) {
  try {
    return sidecarProxy->ReadJadeReg(sJade, addr);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Reads a value from specified Jade register address
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sMem   Address of the Jade register
 *
 * @return A string containing the value of the specified Jade register
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */ 
const char *
SidecarIceClient::readJadeRegister(const char *sJade, const char *sMem) {
  try {
    return sidecarProxy->ReadJadeRegister(sJade, sMem).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Reads the science data into the array that was passed by reference
 * 
 * @param sJade        A string containing the name of the Jade
 * @param numWords     The number of words to read
 * @param targetArray  An array for the science data to be placed in (uint16[])
 *
 * @return             A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::readScienceData(const char *sJade, int numWords, int *targetArray) {
  try {
    string result;
    Sidecar::SidecarIce::intSeq targetVector (numWords, 0);

    result = sidecarProxy->ReadScienceData(sJade, numWords, targetVector);

    for (int jj=0; jj<numWords; jj++) {
      *(targetArray+jj) = targetVector.at(jj);
    }

    return result.c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade    A string containing the name of the Jade
 * @param Address  Address of the USB register (uint32)
 *
 * @return         A string containing the result of the operation
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
long 
SidecarIceClient::readUSBPort(const char *sJade, long address) {
  try {
    return sidecarProxy->ReadUSBPort(sJade, address);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade     A string containing the name of the Jade
 * @param sAddress  Address of the Jade register
 *
 * @return          A string cointaining the value of the register
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::readUSBPortDirectionAsString(const char *sJade, const char *addr) {
  try {
    return sidecarProxy->ReadUSBPortDirectionAsString(sJade, addr).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade     A string containing the name of the Jade
 * @param addr      Address of the Jade register
 *
 * @return          A string cointaining the value of the register
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
long 
SidecarIceClient::readUSBPortDirectionAsLong(const char *sJade, long addr) {
  try {
    return sidecarProxy->ReadUSBPortDirectionAsLong(sJade, addr);
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return ERROR_SIDECAR_ICE_TIMEOUT;
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return ERROR_SIDECAR_ICE_GENERIC;
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return ERROR_SIDECAR_ICE_UNKNOWN;
  }
}

/**
 * Sets AutoWrite to on or off. 
 * 
 * @param state  An integer indicating the new state: 1 - Enabled, 0 - Disabled
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::setAutoWrite(int state) {
  try {
    return sidecarProxy->SetAutoWrite(state).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Sets MemoryBlockMode to on or off.
 * 
 * @param mode  An integer indicating the new mode: 1 - Enabled, 0 - Disabled
 *
 * @return      A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::setBlockMemoryMode(int mode) {
  try {
    return sidecarProxy->SetBlockMemoryMode(mode).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Sets the DMA state.
 * 
 * @param state  An integer indicating the new state: 1 - Enabled, 0 - Disabled
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::setDMA(int state) {
  try {
    return sidecarProxy->SetDMA(state).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Causes registers to be uploaded from hardware into memory. Calls to DownloadASICRegisters(string sJade, string sAsic) can be used to redownload those values from memory.
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sAsic  A string containing the name of the Asic or Asics separated by commas
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::uploadASICRegisters(const char *sJade, const char *sAsic) {
  try {
    return sidecarProxy->UploadASICRegisters(sJade, sAsic).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Uploads all registers specified in the solution for the specified jade into memory.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::uploadJADERegisters(const char *sJade) {
  try {
    return sidecarProxy->UploadJADERegisters(sJade).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Writes an ASIC register
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sAsic   A string containing the name of the Asic or Asics separated by commas
 * @param sAddr   The address of the register
 * @param sValue  The value to be written
 *
 * @return        A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeAsicReg(const char *sJade, const char *sAsic, long addr, long value) {
  try {
    return sidecarProxy->WriteAsicReg(sJade, sAsic, addr, value).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Writes an ASIC register
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sAsic   A string containing the name of the Asic or Asics separated by commas
 * @param addr   The address of the register
 * @param value  The value to be written
 *
 * @return        A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeAsicRegister(const char *sJade, const char *sAsic, const char *addr, const char *value) {
  try {
    return sidecarProxy->WriteAsicRegister(sJade, sAsic, addr, value).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Block writes the registers to the Jade specified.
 * 
 * @param sJade       A string containing the name of the Jade
 * @param sAsic       A string containing the name of the Asic or Asics separated by commas
 * @param addrArray   Array containing addresses to be downloaded (uint16[])
 * @param valueArray  Array containing values to be downloaded (uint32[])
 * @param is24bit     Boolean indicating whether the register is 24 bit should be used
 *
 * @return            A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeAsicRegisterBlock(const char *sJade, const char *sAsic, bool is24bit, int *addrArray, long *valueArray, int blockSize) {
  try {
    Sidecar::SidecarIce::intSeq addrVector;   
    Sidecar::SidecarIce::longSeq valueVector; 

    for (int ii=0; ii<blockSize; ii++) {
      addrVector.push_back(*(addrArray+ii));
      valueVector.push_back(*(valueArray+ii));
    }

    return sidecarProxy->WriteAsicRegisterBlock(sJade, sAsic, is24bit, addrVector, valueVector).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Writes a value to the specified Jade register address.
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sMem    Address of the Jade register
 * @param sValue  Value to be written
 *
 * @return        A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeJadeReg(const char *sJade, long addr, long value) {
  try {
    return sidecarProxy->WriteJadeReg(sJade, addr, value).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Writes a value to the specified Jade register address.
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sMem    Address of the Jade register
 * @param value   Value to be written
 *
 * @return        A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeJadeRegister(const char *sJade, const char *sMem, const char *value) {
  try {
    return sidecarProxy->WriteJadeRegister(sJade, sMem, value).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 * Writes the science data from the array that was passed by reference. 
 * 
 * @param sJade        A string containing the name of the Jade
 * @param targetArray  An array for the science data to be placed in (uint16[])
 *
 * @return             A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeScienceData(const char *sJade, int *targetArray, int blockSize) {
  try {
      
    Sidecar::SidecarIce::intSeq targetVector;

    for (int ii=0; ii<blockSize; ii++) {
      targetVector.push_back(*(targetArray+ii));
    }

    return sidecarProxy->WriteScienceData(sJade, targetVector).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 *  Writes value to the specified USB register address
 *  
 * @param sJade    A string containing the name of the Jade
 * @param address  Address of the USB register (uint32)
 * @param data     Value to be written
 *
 * @return         A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeUSBPort(const char *sJade, long address, unsigned char data) {
  try {
    //. todo: convert data to ICE::Byte
    return sidecarProxy->WriteUSBPort(sJade, address, data).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

/**
 *  Writes value to the specified USB register address
 *  
 * @param sJade    A string containing the name of the Jade
 * @param address  Address of the USB register (uint32)
 * @param data     Value to be written
 *
 * @return         A string containing the result of the operation
 * @return "Error: Ice Timeout" on ICE timeout error
 * @return "Error: Ice Exception" on generic ICE error
 * @return "Error: Unknown Exception" on unknown error
 */
const char *
SidecarIceClient::writeUSBPortDirection(const char *sJade, long address, unsigned char data) {
  try {
    //. todo: convert data to ICE::Byte
    return sidecarProxy->WriteUSBPortDirection(sJade, address, data).c_str();
  } catch (const Ice::TimeoutException & toEx) {
    cerr << "Ice Timeout Exception: " << toEx << endl;
    return "Error: Ice Timeout";
  } catch (const Ice::Exception & ex) {
    cerr << "Ice Exception: " << ex << endl;
    return "Error: Ice Exception";
  } catch (...) {
    cerr << "Unknown Exception" << endl;
    return "Error: Unknown Exception";
  }
}

////////////////////////////////////////////////
//  C ACCESS FUNCTIONS
////////////////////////////////////////////////

/** Connect to ICE client to sidecar server
 *
 * @param hostname [in] Host to connect to
 * @param port [in] Port to connect to
 *
 * @return result from sidecarIceClient->connectToServer() call
 */
int sidecar_connectToServer( const char *hostname, unsigned int port) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    cout << "hostname = " << hostname << endl;
    cout << "port = " << port << endl;

    return sidecarIceClient->connectToServer(hostname, port);
 
}

/**
 * Handle ICE server shutdown
 *
 * @param none
 * 
 * @return none
 */
void sidecar_server_shutdown() {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  sidecarIceClient->server_shutdown();
}

/**
 * Echo string in server
 *
 * @param s string to echo
 * 
 * @return same string that was echoed
 */
const char *sidecar_echo(const char *s) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->echo(s);
}

/** Add a value listener to sidecar server
 *
 * @param l [in] Value listener structure
 *
 * @return none
 */
void sidecar_addCallbackListener( struct ValueListener *l) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    sidecarIceClient->addCallbackListener(l);
    return;
}

/**
 * Sets the detector subarray extents for next exposure.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param startx  leftmost column of detector subarray
 * @param starty  bottommost row of detector subarray
 * @param endx    rightmost column of detector subarray
 * @param endy    topmost row of detector subarray
 *
 * @return Error code
 */

int sidecar_setExposureExtents(const char *sJade, const char *sAsic, int startx, int starty, int endx, int endy) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->setExposureExtents(sJade, sAsic, startx, starty, endx, endy);
}

/**
 * Take an exposure on server side.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param resets  Number of detector resets at start of integration
 * @param itimeInMicros  Integration time in microseconds
 * @param coadds     Number of coadditions
 * @param sampMode   Sampling mode (1=Single, 2=CDS, 3=MCDS/Folwer, 4=UTR)
 * @param reads      Number of sampled reads (pairs for CDS and MCDS)
 * @param groups     Number of sampled groups (2 for CDS and MCDS)
 * @param filename   Name of FITS file to be written.  Must be a valid Windows filename
 *
 * @return Error code
 */
int sidecar_takeExposure(const char *sJade, const char *sAsic, int resets, uint64_t itimeInMicros, int coadds, int sampMode, int reads, int groups, const char *filename) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->takeExposure(sJade, sAsic, resets, itimeInMicros, coadds, sampMode, reads, groups, filename);
}

/**
 * Abort an exposure on server side.
 *
 * @param abortLevel [in] Level for abort (1=immediately, 2=after current read, 3=after current coadd)
 * @param saveImage [in] Whether or not to save the abort image to disk 
 * 
 * @return Error code
 */
int sidecar_abortExposure(int abortLevel, int saveImage) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->abortExposure(abortLevel, saveImage);
}

/**
 * Validate a filename (for fits file)
 *
 * @param sFilename name of file to check
 * 
 * @return -1 if file not writable
 * @return  0 if file does not exist and directory is writable
 * @return  1 if file exists and is writable
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int sidecar_validateFilename(const char *sFilename) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->validateFilename(sFilename);
}

/**
 * Ask SidecarServer to rebroadcast a value
 *
 * @param sValueName  Name of value
 * 
 * @returns Error code
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int sidecar_broadcastValue(const char *sValueName) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->broadcastValue(sValueName);
}
/**
 * Get the read time for a frame in microseconds
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * 
 * @returns Read time for a single frame in microseconds
 * @return ERROR_SIDECAR_ICE_TIMEOUT on ICE timeout
 * @return ERROR_SIDECAR_ICE_GENERIC on generic ICE error
 * @return ERROR_SIDECAR_ICE_UNKNOWN on unknown ICE error
 */
int sidecar_getReadTimeInMicroseconds(const char *sJade, const char *sAsic) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->getReadTimeInMicroseconds(sJade, sAsic);
}

/**
 * Reset ASIC
 *
 * @param sJade   Name of Jade
 * 
 * @return Error code
 */
int sidecar_resetASIC(const char *sJade) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->resetASIC(sJade);
}

/**
 * Power Down ASIC
 *
 * @param sJade   Name of Jade
 * 
 * @return Error code
 */
int sidecar_powerDownASIC(const char *sJade) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->powerDownASIC(sJade);
}

/**
 * Adds a list of FITS header keywords to the current exposure FITS file
 *
 * @param keys - array for Fit Header Keys
 * @param numKeys - number of Keys
 * 
 * @return Error code
 */
int sidecar_addFitsHeader(const char *fitsFilename, struct FitsHeaderKey *keys, int numKeys) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->addFitsHeader(fitsFilename, keys, numKeys);
}

/**
 * Initialize the SIDECAR.  Included downloading firmware (optional),
 * downloading Jade registers, resetting ASIC, and  downloading MCD file.
 *
 * @param sJade   Name of Jade
 * @param sAsic   Name of ASIC
 * @param downloadFirmware whether or not to download Jade firmware
 *
 * @return           A string containing the result of the operation
 */
const char *sidecar_initializeSidecar(const char *sJade, const char *sAsic, int downloadFirmware) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->initializeSidecar(sJade, sAsic, downloadFirmware);
}

/**
 * Causes server to read configuration file for SIDECAR control.
 *
 * @param configFile  Sidecar Control configuration filename (XML)
 *
 * @return Error code 
 */
int sidecar_readSidecarControlConfiguration(const char *configFile) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->readSidecarControlConfiguration(configFile);
}

/**
 * Set reference pixel subtraction mode
 *
 * @param type reference pixel subtraction type
 * 
 * @return Error code 
 */
int sidecar_setReferencePixelSubtractionType(int type) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->setReferencePixelSubtractionType(type);
}

/**
 * Specify whether or not to write raw frames to disk
 *
 * @param enable flag specifying whether or not to write raw frames
 * 
 * @return Error code 
 */
int sidecar_writeRawFrames(int enable) {
  SidecarIceClient *sidecarIceClient;
  sidecarIceClient = SidecarIceClient::getInstance();
  return sidecarIceClient->writeRawFrames(enable);
}

/**
 * Downloads all configurations specified in the solution to the specified Jade. 
 * 
 * @param sJade      A string containing the name of the Jade
 * @param mcds	     Instructs the system to download the mcd files that are specified in the download configuration
 * @param registers	 Instructs the system to download the registers that are specified in the download configuration
 *
 * @return           A string containing the result of the operation
 */
const char *sidecar_downloadAllConfiguredAsics(const char *sJade, int mcds, int registers) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadAllConfiguredAsics(sJade, mcds, registers);
}

/**
 * Downloads the ASIC registers in the specified file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sAsic      A string containing the name of the Asic or Asics separated by commas
 * @param sFilename  A string containing the name of the regiser file
 *
 * @return           A string containing the result of the operation
 */
const char *sidecar_downloadASICRegisterFile(const char *sJade, const char *sAsic, const char *sFilename) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadASICRegisterFile(sJade, sAsic, sFilename);
}

/**
 * Downloads the ASIC registers specified in the solution.
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sAsic  A string containing the name of the Asic or Asics separated by commas
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_downloadASICRegisters(const char *sJade, const char *sAsic) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadASICRegisters(sJade, sAsic);
}

/**
 * Downloads the specified JADE firmware specified in the solution
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_downloadJADEFirmware(const char *sJade) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadJADEFirmware(sJade);
}

/**
 * Downloads the specified JADE firmware file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sFilename  A string containing the name of a file containing the Jade firmware
 *
 * @return           A string containing the result of the operation
 */
const char *sidecar_downloadJADEFirmwareFile(const char *sJade, const char *sFilename) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadJADEFirmwareFile(sJade, sFilename);
}

/**
 * Downloads the JADE registers in the specified file.
 * 
 * @param sJade      A string containing the name of the Jade
 * @param sFilename  A string containing the name of an xml file containing the Jade registers
 *
 * @return           A string containing the result of the operation
 */
const char *sidecar_downloadJADERegisterFile(const char *sJade, const char *sFilename) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadJADERegisterFile(sJade, sFilename);
}

/**
 * Downloads the specified JADE register specified in the open solution.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string cointaining the result of the operation
 */
const char *sidecar_downloadJADERegisters(const char *sJade) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadJADERegisters(sJade);
}

/**
 * Downloads the MCD file(s) that were configured in the solution to the jade and asic specified.
 * 
 * @param sJade         A string containing the name of the Jade
 * @param sAsic         A string containing the name of the Asic or Asics separated by commas
 *
 * @return              A string cointaining the result of the operation
 */
const char *sidecar_downloadMCD(const char *sJade, const char *sAsic) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadMCD(sJade, sAsic);
}

/**
 * Downloads the MCD file that is specified to the jade and asic specified.
 * 
 * @param sJade         A string containing the name of the Jade
 * @param sAsic         A string containing the name of the Asic or Asics separated by commas
 * @param sMCDFilename  A string containing the name of an MCD file
 *
 * @return              A string cointaining the result of the operation
 */
const char *sidecar_downloadMCDFile(const char *sJade, const char *sAsic, const char *sMCDFilename) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->downloadMCDFile(sJade, sAsic, sMCDFilename);
}

/**
 * Get names of Asics for a Jade
 *
 * @param sJade         A string containing the name of the Jade
 *
 * @return              A string containing the names of asics in comma-delimited list
 */
const char *sidecar_getAsics(const char *sJade) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getAsics(sJade);
}

/**
 * Determine whether AutoWrite is enabled.
 *
 * @param none
 * 
 * @return  An integer indicating whether AutoWrite is enabled: 1 - Enabled, 0 - Disabled
 */
int sidecar_getAutoWrite() {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getAutoWrite();
}

/**
 * Gets a list of strings separated by commas that represents the names of the asics that are currently connected/available/configured.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A comma separated list of asic names or an empty string if no asics are available
 */
const char *sidecar_getAvailableAsics(const char *sJade) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getAvailableAsics(sJade);
}

/**
 * Gets a comma separated list of available Jades.
 *
 * @param none
 *
 * @return   A comma separated list of Jades
 */
const char *sidecar_getAvailableJades() {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getAvailableJades();
}

/**
 * Determine whether DMA is enabled.
 * 
 * @param none
 *
 * @return  An integer indicating whether DMA is enabled: 1 - Enabled, 0 - Disabled
 */
int sidecar_getDMA() {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getDMA();
}

/**
 * Get names of Jades
 * 
 * @param none
 *
 * @return  Names of the Jades in a comma-delimited list. 
 */
const char *sidecar_getJades() {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getJades();
}

/**
 * Determine whether MemoryBlockMode is enabled.
 * 
 * @param none
 *
 * @return  An integer indicating whether MemoryBlockMode is enabled: 1 - Enabled, 0 - Disabled
 */
int sidecar_getMemoryBlockMode() {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->getMemoryBlockMode();
}

/**
 * Opens the specified solution which will be used for register definitions and download configurations. 
 * 
 * @param appFilename  A string containing the name of an application (solution) file
 *
 * @return           A string containing the result of the operation
 */
const char* sidecar_openApp(const char* appFilename) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->openApp(appFilename);
}

/**
 * Reads the Asic register. 
 *
 * @param sJade    A string containing the name of the Jade
 * @param sAsic    A string containing the name of the Asic
 * @param addr     The address of the register
 * @param is24bit  Indicator that tells the system to read as a 24 bit register or a 16 bit register
 *
 * @return         A string containing the result of the operation
 */
long sidecar_readAsicReg(const char *sJade, const char *sAsic, long addr, int is24bit) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readAsicReg(sJade, sAsic, addr, is24bit);
}

/**
 * Reads the Asic register. 
 *
 * @param sJade    A string containing the name of the Jade
 * @param sAsic    A string containing the name of the Asic
 * @param sMem     The address of the register
 * @param is24bit  Indicator that tells the system to read as a 24 bit register or a 16 bit register
 *
 * @return         A string containing the result of the operation
 */
const char *sidecar_readAsicRegister(const char *sJade, const char *sAsic, const char *sMem, int is24bit) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readAsicRegister(sJade, sAsic, sMem, is24bit);
}

/**
 * Block reads the registers from Jade specified. 
 * 
 * @param sJade       A string containing the name of the Jade
 * @param sAsic       A string containing the name of the Jade
 * @param is24bit     Boolean indicating whether the register is 24 bit should be used
 * @param FIFOreset   Boolean indicating whether we should reset the FIFO
 * @param addrArray   Array containing addresses to be downloaded (uint16[])
 * @param valueArray  Array containing values to be downloaded (uint32[])
 * @param blockSize   Block size
 *
 * @return            A string containing the result of the operation
 */
const char *sidecar_readAsicRegisterBlock(const char *sJade, const char *sAsic,  int is24bit, int FIFOreset, int *addrArray, long *valueArray, int blockSize) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readAsicRegisterBlock(sJade, sAsic, is24bit, FIFOreset, addrArray, valueArray, blockSize);
}

/**
 * Reads a value from specified Jade register address
 * 
 * @param sJade  A string containing the name of the Jade
 * @param addr   Address of the Jade register
 *
 * @return       A string containing the value of the register
 */ 
long sidecar_readJadeReg(const char *sJade, long addr) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readJadeReg(sJade, addr);
}

/**
 * Reads a value from specified Jade register address
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sMem   Address of the Jade register
 *
 * @return       A string containing the value of the register
 */ 
const char *sidecar_readJadeRegister(const char *sJade, const char *sMem) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readJadeRegister(sJade, sMem);
}

/**
 * Reads the science data into the array that was passed by reference
 * 
 * @param sJade        A string containing the name of the Jade
 * @param numWords     The number of words to read
 * @param targetArray  An array for the science data to be placed in (uint16[])
 *
 * @return             A string containing the result of the operation
 */
const char *sidecar_readScienceData(const char *sJade, int numWords, int *targetArray) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readScienceData(sJade, numWords, targetArray);
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade    A string containing the name of the Jade
 * @param address  Address of the USB register (uint32)
 *
 * @return         A string containing the result of the operation
 */
long sidecar_readUSBPort(const char *sJade, long address) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readUSBPort(sJade, address);
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade     A string containing the name of the Jade
 * @param addr      Address of the Jade register
 *
 * @return          A string cointaining the value of the register
 */
const char *sidecar_readUSBPortDirectionAsString(const char *sJade, const char *addr) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readUSBPortDirectionAsString(sJade, addr);
}

/**
 * Reads a value from specified USB register address.
 * 
 * @param sJade     A string containing the name of the Jade
 * @param addr      Address of the Jade register
 *
 * @return          A long value containing the value of the register
 */
long sidecar_readUSBPortDirectionAsLong(const char *sJade, long addr) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->readUSBPortDirectionAsLong(sJade, addr);
}

/**
 * Sets AutoWrite to on or off. 
 * 
 * @param state  An integer indicating the new state: 1 - Enabled, 0 - Disabled
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_setAutoWrite(int state) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->setAutoWrite(state);
}

/**
 * Sets MemoryBlockMode to on or off.
 * 
 * @param mode  An integer indicating the new mode: 1 - Enabled, 0 - Disabled
 *
 * @return      A string containing the result of the operation
 */
const char *sidecar_setBlockMemoryMode(int mode) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->setBlockMemoryMode(mode);
}

/**
 * Sets the DMA state.
 * 
 * @param state  An integer indicating the new state: 1 - Enabled, 0 - Disabled
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_setDMA(int state) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->setDMA(state);
}

/**
 * Causes registers to be uploaded from hardware into memory. Calls to DownloadASICRegisters(string sJade, string sAsic) can be used to redownload those values from memory.
 * 
 * @param sJade  A string containing the name of the Jade
 * @param sAsic  A string containing the name of the Asic or Asics separated by commas
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_uploadASICRegisters(const char *sJade, const char *sAsic) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->uploadASICRegisters(sJade, sAsic);
}

/**
 * Uploads all registers specified in the solution for the specified jade into memory.
 * 
 * @param sJade  A string containing the name of the Jade
 *
 * @return       A string containing the result of the operation
 */
const char *sidecar_uploadJADERegisters(const char *sJade) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->uploadJADERegisters(sJade);
}

/**
 * Writes ASIC register
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sAsic   A string containing the name of the Asic or Asics separated by commas
 * @param addr    The address of the register
 * @param value   The value to be written
 *
 * @return        A string containing the result of the operation
 */
const char *sidecar_writeAsicReg(const char *sJade, const char *sAsic, long addr, long value) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeAsicReg(sJade, sAsic, addr, value);
}

/**
 * Writes ASIC register
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sAsic   A string containing the name of the Asic or Asics separated by commas
 * @param addr    The address of the register
 * @param value   The value to be written
 *
 * @return        A string containing the result of the operation
 */
const char *sidecar_writeAsicRegister(const char *sJade, const char *sAsic, const char *addr, const char *value) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeAsicRegister(sJade, sAsic, addr, value);
}

/**
 * Block writes the registers to the Jade specified.
 * 
 * @param sJade       A string containing the name of the Jade
 * @param sAsic       A string containing the name of the Asic or Asics separated by commas
 * @param addrArray   Array containing addresses to be downloaded (uint16[])
 * @param valueArray  Array containing values to be downloaded (uint32[])
 * @param is24bit     Boolean indicating whether the register is 24 bit should be used
 * @param blockSize   Block size
 *
 * @return            A string containing the result of the operation
 */
const char *sidecar_writeAsicRegisterBlock(const char *sJade, const char *sAsic, int is24bit, int *addrArray, long *valueArray, int blockSize) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeAsicRegisterBlock(sJade, sAsic, is24bit, addrArray, valueArray, blockSize);
}

/**
 * Writes a value to the specified Jade register address.
 * 
 * @param sJade   A string containing the name of the Jade
 * @param addr    Address of the Jade register
 * @param value   Value to be written
 *
 * @return        A string containing the result of the operation
 */
const char *sidecar_writeJadeReg(const char *sJade, long addr, long value) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeJadeReg(sJade, addr, value);
}

/**
 * Writes a value to the specified Jade register address.
 * 
 * @param sJade   A string containing the name of the Jade
 * @param sMem    Address of the Jade register
 * @param value   Value to be written
 *
 * @return        A string containing the result of the operation
 */
const char *sidecar_writeJadeRegister(const char *sJade, const char *sMem, const char *value) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeJadeRegister(sJade, sMem, value);
}

/**
 * Writes the science data from the array that was passed by reference. 
 * 
 * @param sJade        A string containing the name of the Jade
 * @param targetArray  An array for the science data to be placed in (uint16[])
 * @param blockSize    Block size
 *
 * @return             A string containing the result of the operation
 */
const char *sidecar_writeScienceData(const char *sJade, int *targetArray, int blockSize) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeScienceData(sJade, targetArray, blockSize);
}

/**
 *  Writes value to the specified USB register address
 *  
 * @param sJade    A string containing the name of the Jade
 * @param address  Address of the USB register (uint32)
 * @param data     Value to be written
 *
 * @return         A string containing the result of the operation
 */
const char *sidecar_writeUSBPort(const char *sJade, long address, unsigned char data) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeUSBPort(sJade, address, data);
}

/**
 *  Writes value to the specified USB register address
 *  
 * @param sJade    A string containing the name of the Jade
 * @param address  Address of the USB register (uint32)
 * @param data     Value to be written
 *
 * @return         A string containing the result of the operation
 */
const char *sidecar_writeUSBPortDirection(const char *sJade, long address, unsigned char data) {
    SidecarIceClient *sidecarIceClient;
    sidecarIceClient = SidecarIceClient::getInstance();
    return sidecarIceClient->writeUSBPortDirection(sJade, address, data);
}
