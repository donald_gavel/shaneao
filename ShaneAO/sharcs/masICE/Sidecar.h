/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *  @file Sidecar.h
 *
 *  @brief
 *  Header file specific to the Sidecar ICE.
 *
 *  This module contains types and definitions for the SIDECAR software
 *  for both c and c++.
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 * 
 */
#ifndef _SIDECAR_H_
#define _SIDECAR_H_

#define SIDECAR_SUCCESSFUL_RESULT                       "Success!"
#define SIDECAR_SUCCESS_OPEN_APPLICATION     "Application opened."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_FIRMWARE \
                            "Download of jade firmware succeeded."
#define SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS \
                            "Download of jade registers complete."
#define SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE \
                            "Download MCD Succeeded"

/* writing to Jade registers returns "Success!" when successful
   writing to ASIC registers returns 
   "Write to asic register @<XXXX> successful." when successful, where
   <XXXX> is the address in hex (w/o preceding 0x)
*/
#define SIDECAR_SUCCESS_WRITE_ASIC_REGISTER          "successful"
#define SIDECAR_SUCCESS_WRITE_JADE_REGISTER            "Success!"
#define SIDECAR_SUCCESS_READ_SCIENCE_DATA              "Success!"

#define SIDECAR_DEFAULT_JADE_NAME                       "JADE2"
#define SIDECAR_DEFAULT_ASIC_NAME                       "ASIC1"

#define SIDECAR_PAUSE_BETWEEN_COMMANDS   100000   /* micros */

#define SIDECAR_PIXEL_CLOCK_TIME_MICROS  10

#define SIDECAR_MAX_STRING_INITIALZING  256

#define SIDECAR_CALLBACK_HEARTBEAT                           "heartbeat"
#define SIDECAR_CALLBACK_HEARTBEAT_PERIOD              "heartbeatPeriod"
#define SIDECAR_CALLBACK_PROGRESS                             "progress"
#define SIDECAR_CALLBACK_READS_DONE                          "readsDone"
#define SIDECAR_CALLBACK_GROUPS_DONE                        "groupsDone"
#define SIDECAR_CALLBACK_COADDS_DONE                        "coaddsDone"
#define SIDECAR_CALLBACK_CURRENT_READ                      "currentRead"
#define SIDECAR_CALLBACK_IGNORED_READS                    "ignoredReads"
#define SIDECAR_CALLBACK_EXPOSURE_IN_PROGRESS       "exposureInProgress"
#define SIDECAR_CALLBACK_OBSERVATION_IN_PROGRESS "observationInProgress"
#define SIDECAR_CALLBACK_ABORT_IN_PROGRESS             "abortInProgress"
#define SIDECAR_CALLBACK_READOUT_IN_PROGRESS         "readoutInProgress"
#define SIDECAR_CALLBACK_EXPOSING                             "exposing"
#define SIDECAR_CALLBACK_IMAGE_DONE                          "imageDone"
#define SIDECAR_CALLBACK_EXPOSURE_STATUS                "exposureStatus"
#define SIDECAR_CALLBACK_EXPOSURE_STATUS_DETAIL   "exposureStatusDetail"
#define SIDECAR_CALLBACK_LAST_FILENAME                    "lastFilename"
#define SIDECAR_CALLBACK_DOWNLOAD_FIRMWARE            "downloadFirmware"
#define SIDECAR_CALLBACK_INITIALIZE_SIDECAR          "initializeSidecar"
#define SIDECAR_CALLBACK_WRITING_DATA                      "writingData"
#define SIDECAR_CALLBACK_ICE_SERVER                          "ICEServer"
#define SIDECAR_CALLBACK_ESTIMATED_OBSERVATION_TIME    "observationTime"
#define SIDECAR_CALLBACK_FITS_WRITING_EXTENSION   "fitsWritingExtension"

#define SIDECAR_READ_CONFIG_STATUS_APPLICATION_CHANGED    1
#define SIDECAR_READ_CONFIG_STATUS_SIDECAR_CONFIG_CHANGED 2

#define SIDECAR_VALIDATE_FILENAME_FILE_EXISTS_WRITEABLE    1
#define SIDECAR_VALIDATE_FILENAME_DIRECTORY_WRITEABLE      0
#define SIDECAR_VALIDATE_FILENAME_FILE_EXISTS_READ_ONLY   -1
#define SIDECAR_VALIDATE_FILENAME_DIRECTORY_DOESNT_EXIST  -2
#define SIDECAR_VALIDATE_FILENAME_DIRECTORY_NOT_WRITEABLE -3


struct ValueListener {
  void (*setValueFunc)(const char*, const char*);  /* function called 
						      on SIDECAR 
						      callback */
};

enum FitsHeaderKeyType {
        KeyTypeBoolean,
        KeyTypeInteger,
        KeyTypeLong,
        KeyTypeFloat,
        KeyTypeDouble,
        KeyTypeString,
        KeyTypeComment
};

struct FitsHeaderKey {
  char *name;
  char *value;
  enum FitsHeaderKeyType type;
  char *comment;
};

#define MAX_VALUE_LENGTH_FLOAT 16
#define MAX_VALUE_LENGTH_DOUBLE 20

#endif /* _SIDECAR_H_ */
