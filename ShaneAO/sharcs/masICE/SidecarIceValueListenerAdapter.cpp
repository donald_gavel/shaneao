/*****************************************************************************
 *
 *      Copyright (C) 2008. The Regents of the University of California.
 *      All Rights Reserved. Created by Jason Weiss, UCLA Infrared Laboratory.
 *
 *      ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ****************************************************************************/
/**
 *
 *  @file SidecarIceValueListenerAdapter.cpp
 *
 *  @brief
 *  Functions specific to the Sidecar ICE Value ListenerAdapter.
 *
 *  This module contains routines for the base class Value ListenerAdapter,
 *  called in callbacks, if registered, from the Windows Sidecar ICE server.
 *  It essentially allows a normal C setValue routine to be called via
 *  ICE callbacks.  The pointer to the C routine is stored in a local member
 *  of the ValueListener structure type.
 *
 *  @author Jason L. Weiss (weiss@astro.ucla.edu)
 *  
 */
#include "SidecarIceValueListenerAdapter.h"

#include <iostream>
#include <string.h>

/** Constructor for SidecarIceValueListenerAdapter 
 *
 */
SidecarIceValueListenerAdapter::SidecarIceValueListenerAdapter() {
}

/** Returns the ValueListener structure associated with this object
 *
 * @return ValueListener structure associated with this object
 */
struct ValueListener 
SidecarIceValueListenerAdapter::getValueListener() {
  return listener;
}

/** Sets the ValueListener structure associated with this object
 *
 * @param l [in] ValueListener structure containing pointer to setValue structure to be called on ICE callback
 *
 * @return none
 */
void 
SidecarIceValueListenerAdapter::setValueListener(struct ValueListener *l) {
  listener = *l;  
}

/** Set value routine adapter.
 * 
 * This method is called on ICE callbacks, and in turns calls the normal C
 * setValue method pointed to by the ValueListener structure associated with
 * this class.
 * 
 * @param name [in] name of parameter in callback
 * @param value [in] value of parameter in callback
 *
 * @return none
 */
void 
SidecarIceValueListenerAdapter::setValue(string name, string value) const {
  char *n, *v;
  void (*func)(const char*, const char*) = NULL;

  func = listener.setValueFunc;

  if (func == NULL) {
    cout << "ListenerAdapter: " << name << " = " << value << endl;
  } else {
    n = new char[name.size()+1];
    v = new char[value.size()+1];
    
    strcpy(n, name.c_str());
    strcpy(v, value.c_str());

    (*func)(n, v);
  }
}
