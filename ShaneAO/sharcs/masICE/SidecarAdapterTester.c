#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include <termios.h>
#include <unistd.h>

#include "Sidecar.h"
#include "SidecarIceClient.h"
#include "SidecarAdapter.h"
#include "lerrlog.h"

#define USAGE "USAGE:\n  SidecarAdapterTester [-i itime] -[c coadds] [-s sampling_mode]\n\t[-r reads] [-o outdir] [-f filename] [-z resets] [-h serverHost] [-p serverPort] [-C serverConfig] [-X]\n\n  itime:\t[10000] integration time in ms (non-negative)\n  coadds:\t[1] coadditions ( >0 )\n  samp_mode:\t[2] sampling mode (number or word: 1=single, 2=cds, 3=mcds,\n\t\t4=utr)\n  reads:\t[1] reads for utr, reads pairs for mcds, ignored for others\n  filename:\t[sidecarAsic_YYMMDD_hhmmss.fits] valid filename (no checking).\n\t\tpreface with ! to overwrite.  If filename starts with a /,\n\t\tit is assumed it is an absolute path, and the outdir is ignored.\n  outdir:\t[/net/hydrogen/data/projects/gpi/data/sidecar/] output directory\n  resets:\t[1] number of reset frames ( >0 )\n  serverConfig:\tSidecar Control configuration filename (must be proper Windows filename)\n  serverHost:\tHost name of server running ICE client.\n  serverPort:\tPort on ICE server is running on\n  -X:\t\tif this flag is set, there is no confirmation of settings, and\n\t\tthe hardware is not initialized.\n\n"

volatile int exposureInProgress;

int getch() {
  /* 
   * this works on linux, but not for Solaris.
   * simple solution below, but a CR is required to enter 
   * response.

  struct termios oldt, newt;
  int ch;
  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  ch = getchar();
  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  return ch;

  */
  fflush(stdin);
  return getchar();
}

void displayValue(const char *name, const char *value) {
  
  if (strcmp(name, "exposureStatus") == 0) {
    printf(" - %s.\n", value);
  } else if (strcmp(name, "exposureStatusDetail") == 0) {
    printf("   - %s.\n", value);
  } else {
    printf("*** %s = %s.\n", name, value);

    if (strcmp(name, "exposureInProgress") == 0) {
      exposureInProgress = (int)atoi(value);
    }
  }
}

int main(int argc, char *argv[]) {
  int retval;
  char s;
  struct ExposureConfiguration config;
  char fitsFilename[FILENAME_MAX_LENGTH];
  char configFilename[FILENAME_MAX_LENGTH];
  char outdir[FILENAME_MAX_LENGTH];
  char serverHost[HOSTNAME_MAX_LENGTH];
  int serverPort;
  int tempSampmode;
  int argi;
  int powerVal;
  char* powerStop;
  char powerString[10];
  int confirm=1;
  int poweroff=0;
  const char* response;

  time_t        caltime;
  struct tm     *tm;
  char tempFilename[FILENAME_MAX_LENGTH];

  struct ValueListener l;

  l.setValueFunc = displayValue;

  setLogTarget(LOGTO_STDOUT);
  /*   setLogLevel(LOGLVL_INFO_AND_ABOVE);  */
  setLogLevel(LOGLVL_ALL); 
  

  /* fill configuration defaults */
  config.integrationTime = 10000;
  config.numberOfResets = 1;
  config.numberOfReads = 1;
  config.numberOfCoadds = 1;
  config.samplingMode = SAMPLING_MODE_CDS;

  outdir[0]='\0';
  fitsFilename[0]='\0';
  tempFilename[0]='\0';
  configFilename[0]='\0';

  strncpy(serverHost, SIDECAR_SERVER_HOSTNAME, HOSTNAME_MAX_LENGTH);
  serverPort = SIDECAR_SERVER_PORT;

  /* parse arguments  

 USAGE: SidecarAdapterTester [-i itime] [-c coadds] [-s sampling_mode] 
	  [-r reads] [-o outdir] [-f filename] [-z resets] [-h host] -[-p port] [-C serverConfigFile] [-X]
  */
  for (argi = 1; argi<argc; argi++) {
    if (strcmp(argv[argi], "-i") == 0) {
      argi++;
      config.integrationTime = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-c") == 0) {
      argi++;
      config.numberOfCoadds = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-s") == 0) { 
      argi++;     
      if ((tempSampmode = atoi(argv[argi])) != 0) {
	if ((tempSampmode < 1) || (tempSampmode > 4)) {
	  printf("sampmode must be between 1 and 4\n\n%s\n", USAGE);
	  return -1;
	}
	config.samplingMode = tempSampmode;
      } else if (strcasecmp(argv[argi], "single") == 0) {
	config.samplingMode=1;
      } else if (strcasecmp(argv[argi], "cds") == 0) {
	config.samplingMode=2;
      } else if (strcasecmp(argv[argi], "mcds") == 0) {
	config.samplingMode=3;
      } else if (strcasecmp(argv[argi], "utr") == 0) {
	config.samplingMode=4;
      } else {
	  printf("Invalid sampling mode\n\n%s\n", USAGE);
	  return -1;
      }
    } else if (strcmp(argv[argi], "-r") == 0) {
      argi++;
      config.numberOfReads = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-z") == 0) {
      argi++;
      config.numberOfResets = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-o") == 0) {
      argi++;
      strncpy(outdir, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-f") == 0) {
      argi++;
      strncpy(tempFilename, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-C") == 0) {
      argi++;
      strncpy(configFilename, argv[argi], FILENAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-h") == 0) {
      argi++;
      strncpy(serverHost, argv[argi], HOSTNAME_MAX_LENGTH);
    } else if (strcmp(argv[argi], "-p") == 0) {
      argi++;
      serverPort = (int)atoi(argv[argi]);
    } else if (strcmp(argv[argi], "-X") == 0) {
      confirm = 0;
    } else if (strcmp(argv[argi], "-poweroff") == 0) {
      poweroff = 1;
    } else {
      printf("invalid argument: %s\n\n%s", argv[argi], USAGE);
      return 0;
    }
  }

  if ((config.integrationTime < 0) ||
      (config.numberOfCoadds < 1) ||
      (config.numberOfReads < 1) || 
      (config.numberOfResets < 1) ||
      (config.samplingMode < 1) || 
      (config.samplingMode > 4)) {
    printf("%s", USAGE);
    return -1;
  }

  if (strcmp(outdir, "") == 0) {
/*    strcpy(outdir, "/net/hydrogen/data/projects/gpi/data/sidecar"); */
    strcpy(outdir, "\\\\Hydrogen\\data\\projects\\gpi\\data\\sidecar");
  }

  if (strcmp(tempFilename, "") == 0) {

    /* construct fits filename in the form:
       sidecarAdapter_YYMMDD_HHmmss.fits */

    /* get system time */
    caltime=time(NULL);
    tm=localtime(&caltime);
  
    /* see Stevens p. 158 for formatting info */
    strftime(tempFilename, 32, "sidecarAsic_%y%m%d_%H%M%S.fits", tm);

  }
  
  /* 92 = '\', 58 = ':' */
  if ((tempFilename[0] != 92) && (tempFilename[1] != 58)
      && (tempFilename[0] != '/')) {

    sprintf(fitsFilename, "%s\\\\%s", outdir, tempFilename);
  } else {
    strcpy(fitsFilename, tempFilename);
  }

  llog_msg(LOGLVL_INFO, "Test program for Sidecar.\n");
  
  if (poweroff == 1) {

    llog_msg(LOGLVL_INFO, "POWER DOWN SIDECAR ASIC?\n");

  } else {

    llog_msg(LOGLVL_INFO, "Coadds = %d.\n", config.numberOfCoadds);
    llog_msg(LOGLVL_INFO, "Itime = %d.\n", config.integrationTime);
    llog_msg(LOGLVL_INFO, "Reads = %d.\n", config.numberOfReads);
    llog_msg(LOGLVL_INFO, "Sampling Mode = %d. (1=Single, 2=CDS, 3=MCDS, 4=UTR)\n", config.samplingMode);
    llog_msg(LOGLVL_INFO, "Resets = %d.\n", config.numberOfResets);
    llog_msg(LOGLVL_INFO, "Fits filename = %s.\n", fitsFilename);
    
  }
  if (confirm != 0) {
    printf("confirm? (y/N):");
    
    s=getch();
    
  } else {
    s='y';
  }
  if (s != 'y') {
    printf("\nexiting...\n");
    return 0;
  }


  if ((sidecar_connectToServer(serverHost, serverPort)) < 0) {
    llog_msg(LOGLVL_ERROR, "Error connecting to server.\n");
    return -1;
  }

  sidecar_addCallbackListener(&l);

  if (poweroff == 1) {
    response = sidecar_readJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0020");

    llog_msg(LOGLVL_DEBUG, "Jade Register 0x0020 = %s\n", response);

    powerVal = strtol(response+2, &powerStop, 16) & 0xF0FF;
    sprintf(powerString, "0x%4x", powerVal);

    llog_msg(LOGLVL_DEBUG, "Setting Jade Register 0x0020 to %s\n", powerString);
    response = sidecar_writeJadeRegister(SIDECAR_DEFAULT_JADE_NAME, "0x0020", powerString);
    llog_msg(LOGLVL_DEBUG, "WriteJadeRegister response = %s.\n", response);
    
    if (strcmp(response, SIDECAR_SUCCESSFUL_RESULT) == 0) {
      llog_msg(LOGLVL_INFO, "Sidecar successfully powered down.\n");
    } else {
      llog_msg(LOGLVL_ERROR, "Error powering down Sidecar: %s.\n", response);
    }

    return 0;
  }

  if (strcmp(configFilename, "") != 0) {
    llog_msg(LOGLVL_INFO, "Setting server configuration file: %s.\n", configFilename);
    if ((sidecar_readSidecarControlConfiguration(configFilename)) < 0) {
      llog_msg(LOGLVL_ERROR, "Error reading configuration file %s.\n", configFilename);
      return -2;
    }
  }

  if (confirm != 0) {
    printf("Initialize hardware? (y/N):");
    s = getch();
  } else {
    s='n';
  }
  if (s == 'y') {


    llog_msg(LOGLVL_TRACE, "Initializing Hardware...\n");
    if ((retval = Sidecar_initializeHardware()) < 0) {
      llog_msg(LOGLVL_FATAL, "Error Initializing Hardware.  Error code: %d\n", retval);
      return retval;
    }
    
    llog_msg(LOGLVL_TRACE, "Hardware Initialized.\n");


    llog_msg(LOGLVL_TRACE, "Downloading timing file.\n");
    if ((retval = Sidecar_downloadTimingFile(SIDECAR_DEFAULT_H2RG_MCD_FILENAME, TRUE)) < 0) {
      llog_msg(LOGLVL_FATAL, "Error downloading timing file %s.  Error code: %d\n", SIDECAR_DEFAULT_H2RG_MCD_FILENAME, retval);
      return retval;
    }
    llog_msg(LOGLVL_TRACE, " Timing file downloaded.\n");

  }  else {
    Sidecar_skipInitialize();
  }

  llog_msg(LOGLVL_TRACE, "Starting exposure...\n");

  llog_msg(LOGLVL_TRACE, "Getting Bias Register 15 (0x601E) from ASIC register...\n");
  response = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x601E", FALSE);
  llog_msg(LOGLVL_DEBUG, "GetAsicRegister (0x601E) response=%s.\n", response);

  llog_msg(LOGLVL_TRACE, "Getting Bias Register 15 (0x601F) from ASIC register...\n");
  response = sidecar_readAsicRegister(SIDECAR_DEFAULT_JADE_NAME, SIDECAR_DEFAULT_ASIC_NAME, "0x601F", FALSE);
  llog_msg(LOGLVL_DEBUG, "GetAsicRegister (0x601F) response=%s.\n", response);

  if ((retval = Sidecar_takeExposure(config, fitsFilename)) < 0) {
    llog_msg(LOGLVL_FATAL, "Error starting exposure.  Error code: %d\n", retval);
    return retval;
  }
  llog_msg(LOGLVL_TRACE, "Exposure started.\n");

  /* wait for exposure to be complete */
  exposureInProgress = 1;
  while (exposureInProgress) {
    usleep(100000);
  }

  llog_msg(LOGLVL_INFO, "Exposure complete.\n");


  llog_msg(LOGLVL_INFO, "Finished\n");

  return 0;
}



