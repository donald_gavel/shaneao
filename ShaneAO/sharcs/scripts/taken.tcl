namespace eval takeN {

variable w takeN

onScriptAbort taken taken::setAborted
proc setAborted {} {
    taken::setscriptstate "aborted" "Script aborted"
}

proc takeNCreate {} {
    variable w
    global object
    variable obj
    catch {destroy .$w}
    toplevel .$w
    wm title .$w "takeN"

set instruct "Take N exposures\n\n\
  1) Configure exposure parameters\n\
  2) Choose number of exposures to take\n\
  3) Press Go\
"

    label .$w.instructt -text $instruct -justify left
    grid .$w.instructt -row 1 -column 0 -columnspan 4


    #label .$w.lscale -text " (\")"
    #entry .$w.escale -textvariable takeN::scale -width 4
    #grid .$w.lscale -row 2 -column 1
    #grid .$w.escale -row 2 -column 2

    #label .$w.lnexpose -text "Exposures"
    #entry .$w.enexpose -textvariable ::exposures -width 4

    button .$w.bGo -command takeN::takeNGo -text "Go"
    grid .$w.bGo -row 3 -column 1

    button .$w.bClose -command takeN::takeNClose -text "Close"
    grid .$w.bClose -row 3 -column 2

    label .$w.scriptstate -textvariable takeN::scriptstate
    grid .$w.scriptstate -row 6 -column 1 -columnspan 2

    #set takeN::scale 5
    #set takeN::obj $object

};

proc takeNClose {} {
    variable w
    destroy .$w
    onScriptAbort taken ""
}


proc setscriptstate { status txt } {
    set takeN::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}


proc takeNGo {} {
    variable w
    variable obj
    # Make sure we're not paused.
    scriptPause unpause

        set cmdlist [subst {
	    { global object entry_object }
	    { takeN::setscriptstate Running "Starting to take images" }
	    { takeN::setscriptstate Exposing "Taking images" }
	    { expose_nexpose }
	    { takeN::setscriptstate Done "Script Done" }
	} ]


        enter_logsheetstr script "takeN: take N exposures with common settings"

        .$w.bGo configure -state disabled
        .$w.bClose configure -state disabled
        scriptListProc $cmdlist
        .$w.bGo configure -state normal
        .$w.bClose configure -state normal
}

global ircaluimb
$ircaluimb.file add command -label "takeN" -command takeN::takeNCreate

takeN::takeNCreate


};

