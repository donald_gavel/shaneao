namespace eval takeNJHKs {

set ::Kroot [expr {[info exists ::env(KROOT)] ? "$::env(KROOT)" : "@KROOT@"}]
set ::Lroot [expr {[info exists ::env(LROOT)] ? "$::env(LROOT)" : "@LROOT@"}]
if {[info exists ::env(RELDIR)]} {
    set ::Reldir $::env(RELDIR)
} elseif {[info exists ::env(RELNAM)] && [string compare $::env(RELNAM) ""]} {
    set ::Reldir $::Kroot/rel/$::env(RELNAM)    ; # Keck-style installation
} elseif {[string match */rel/* "@prefix@"]} {
    set ::Reldir $::Kroot/rel/default           ; # Keck-style installation
} else {
    set ::Reldir $::Kroot                       ; # Lick-style installation
}
lappend auto_path $::Reldir

package require ktlkit

variable w takeNJHKs

onScriptAbort takeNJHKs takeNJHKs::setAborted
proc setAborted {} {
    takeNJHKs::setscriptstate "aborted" "Script aborted"
}

proc takeNJHKsCreate {} {
    variable w
    global object
    variable obj
    catch {destroy .$w}
    toplevel .$w
    wm title .$w "takeNJHKs"

set instruct "Take N exposures at Ks, H, and J\n\n\
  1) Configure exposure parameters\n\
  2) Choose number of exposures to take\n\
  3) Press Go\
" 

    label .$w.instructt -text $instruct -justify left
    grid .$w.instructt -row 1 -column 0 -columnspan 4


    #label .$w.lscale -text " (\")"
    #entry .$w.escale -textvariable takeNJHKs::scale -width 4
    #grid .$w.lscale -row 2 -column 1
    #grid .$w.escale -row 2 -column 2

    #label .$w.lnexpose -text "Exposures"
    #entry .$w.enexpose -textvariable ::exposures -width 4

    button .$w.bGo -command takeNJHKs::takeNJHKsGo -text "Go"
    grid .$w.bGo -row 3 -column 1

    button .$w.bClose -command takeNJHKs::takeNJHKsClose -text "Close"
    grid .$w.bClose -row 3 -column 2

    label .$w.scriptstate -textvariable takeNJHKs::scriptstate
    grid .$w.scriptstate -row 6 -column 1 -columnspan 2

    #set takeNJHKs::scale 5
    #set takeNJHKs::obj $object

};

proc takeNJHKsClose {} {
    variable w
    destroy .$w
    onScriptAbort takeNJHKs ""
}

#                          [list exec gwaitfor -s saomot -timeout 60 {$DWFILT1STA == "Ready"}] \
#                          [list exec gwaitfor -s saomot -timeout 60 {$DWFILT2STA == "Ready"}] \


proc setscriptstate { status txt } {
    set takeNJHKs::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}


proc takeNJHKsGo {} {
    variable w
    variable obj

    # Make sure we're not paused.
    scriptPause unpause

    set cmdlist [subst {
	{ global object entry_object}
	{ takeNJHKs::setscriptstate Running "Script Beginning" }
	{ setfilt1 Ks }
	{ setfilt2 Open }
	{ takeNJHKs::setscriptstate Exposing "Starting to take images in Ks" }
	{ expose_nexpose}
	{ takeNJHKs::setscriptstate SetFilter "Set filter H" }
	{ setfilt1 H }
	{ takeNJHKs::setscriptstate Exposing "Starting to take images in H" }
	{ expose_nexpose}
	{ takeNJHKs::setscriptstate SetFilter "Set filter J" }
	{ setfilt1 J }
	{ takeNJHKs::setscriptstate Exposing "Starting to take images in J" }
	{ expose_nexpose}
	{ takeNJHKs::setscriptstate Done "Script Done" }
    } ]


        enter_logsheetstr script "takeNJHKs: take N exposures in J H Ks"

        .$w.bGo configure -state disabled
        .$w.bClose configure -state disabled
        scriptListProc $cmdlist
        .$w.bGo configure -state normal
        .$w.bClose configure -state normal
}

global ircaluimb
$ircaluimb.file add command -label "takeNJHKs" -command takeNJHKs::takeNJHKsCreate

takeNJHKs::takeNJHKsCreate


};

