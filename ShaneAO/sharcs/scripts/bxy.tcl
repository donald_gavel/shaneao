namespace eval bxy {

variable w bxy

variable Row 0

proc incrRow {} {
    variable Row
    incr Row
}

onScriptAbort bxy bxy::setAborted
proc setAborted {} {
    bxy::setscriptstate "aborted" "Script aborted"
}

proc bxyCreate {} {
    variable w
    global object
    variable Row
    variable obj
    catch {destroy .$w}
    toplevel .$w
    wm title .$w "bxy"

set instruct "Box Dither\n\n\
  1) Center target\n\
  2) Configure exposure parameters\n\
  3) Choose whether to expose at starting position\n\
  4) Choose Object Dither Scale\n\
  5) Press Go\
"
    set Row 0

    label .$w.instructt -text $instruct	-justify left
    grid .$w.instructt -row [incrRow] -column 0 -columnspan 4


    create_NExpose_row .$w [incrRow] 1

    label .$w.lscale -text "Object Dither Scale (\")"
    entry .$w.escale -textvariable bxy::scale -width 4
    grid .$w.lscale -row [incrRow] -column 1
    grid .$w.escale -row $Row -column 2

    label .$w.lnexpose -text "Exposures"
    entry .$w.enexpose -textvariable ::exposures -width 4 

    label .$w.lexposefirst -text "Expose at starting position?"
    checkbutton .$w.eexposefirst -variable bxy::exposefirst 
    grid .$w.lexposefirst -row [incrRow] -column 1
    grid .$w.eexposefirst -row $Row -column 2


    button .$w.bGo -command bxy::bxyGo -text "Go"
    grid .$w.bGo -row [incrRow] -column 1

    button .$w.bClose -command bxy::bxyClose -text "Close"
    grid .$w.bClose -row $Row -column 3

    label .$w.scriptstate -textvariable bxy::scriptstate
    grid .$w.scriptstate -row [incrRow] -column 1 -columnspan 2

    set bxy::scale 5
    set bxy::obj $object

};


proc setscriptstate { status txt } {
    set bxy::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}


proc bxyClose {} {
    variable w
    destroy .$w
    onScriptAbort bxy ""
}

proc bxyGo {} {
    variable w
    variable obj
    if {[traffic::is_float $bxy::scale] && ([bxyCheckNodSize $bxy::scale] > 0)} {
        set cmdlist [subst {
	    { global object entry_object}
	    { bxy::setscriptstate Running "Script starting" }
	    { if {$bxy::exposefirst > 0} {
		set entry_object "$obj (Starting Pos)" 
		set object "$obj (Starting Pos)" 
		bxy::setscriptstate Exposing "Starting Pos'n Exposure" 
		expose_nexpose
	    }}
	    { bxy::setscriptstate Moving "Moving to position 1" }
	    { WFSnodAbs $bxy::scale $bxy::scale}
	    { bxy::setscriptstate Waiting "Waiting at position 1" }
	    { waitForAO }
	    { set entry_object "$obj (Pos 1)" }
	    { set object "$obj (Pos 1)" }
	    { bxy::setscriptstate Exposing "Exposure at position 1" }
	    { expose_nexpose}
	    { bxy::setscriptstate Moving "Moving to position 2" }
	    { WFSnodAbs 0 [expr -2.*$bxy::scale]}
	    { bxy::setscriptstate Waiting "Waiting at position 2" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 2)" }
	    { set object "$obj (Pos 2)" }
	    { bxy::setscriptstate Exposing "Exposure at position 2" }
	    { expose_nexpose}
	    { bxy::setscriptstate Moving "Moving to position 3" }
	    { WFSnodAbs [expr -2.*$bxy::scale] 0}
	    { bxy::setscriptstate Waiting "Waiting at position 3" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 3)" }
	    { set object "$obj (Pos 3)" }
	    { bxy::setscriptstate Exposing "Exposure at position 3" }
	    { expose_nexpose}
	    { bxy::setscriptstate Moving "Moving to position 4" }
	    { WFSnodAbs 0 [expr 2.*$bxy::scale]}
	    { bxy::setscriptstate Waiting "Waiting at position 4" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 4)" }
	    { set object "$obj (Pos 4)" }
	    { bxy::setscriptstate Exposing "Exposure 4" }
	    { expose_nexpose}
	    { bxy::setscriptstate Moving "Moving to original position" }
	    { WFSnodAbs $bxy::scale [expr -1.*$bxy::scale]}
	    { set entry_object "$obj" }
	    { set object "$obj" }
	    { waitForAO}
	    { bxy::setscriptstate Done "Script Done" }
	} ]

        enter_logsheetstr script "bxy: box dither by $bxy::scale arcsec"

        .$w.bGo configure -state disabled
        .$w.bClose configure -state disabled
        scriptListProc $cmdlist
        .$w.bGo configure -state normal
        .$w.bClose configure -state normal
    } else {
        tk_messageBox -type ok -message "Invalid scale $bxy::scale arcsec"
    }
}

global ircaluimb
$ircaluimb.file add command -label "bxy" -command bxy::bxyCreate

bxy::bxyCreate


};
