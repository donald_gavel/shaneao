#! /usr/bin/wish
# Derived from 
#   Script processor for IRCAL/AEOS user interfaces
#   Code originally by Jamie Lloyd, as modified by Marshall Perrin
# and 
#   emirater
# Reldir is either <kroot> (Lick-style) or
# <kroot>/rel/<relname> (Keck-style):
#
set ::Kroot [expr {[info exists ::env(KROOT)] ? "$::env(KROOT)" : "@KROOT@"}]
set ::Lroot [expr {[info exists ::env(LROOT)] ? "$::env(LROOT)" : "@LROOT@"}]
if {[info exists ::env(RELDIR)]} {
    set ::Reldir $::env(RELDIR)
} elseif {[info exists ::env(RELNAM)] && [string compare $::env(RELNAM) ""]} {
    set ::Reldir $::Kroot/rel/$::env(RELNAM)    ; # Keck-style installation
} elseif {[string match */rel/* "@prefix@"]} {
    set ::Reldir $::Kroot/rel/default           ; # Keck-style installation
} else {
    set ::Reldir $::Kroot                       ; # Lick-style installation
}
lappend auto_path $::Reldir

set d $::Reldir/lib/tcl
if {[lsearch -exact $::auto_path $d] == -1} { lappend ::auto_path $d }
# -----------------------------

package require ktlkit
package require traffic

proc main {} {
    #
    # NOTE NOTE NOTE: elsewhere, the code assumes that service xxxx is
    # mapped to array ::xxxx.
    #
    ktlkit::kopen poco ::poco -uppercase -noprime
    ktlkit::kopen sharcs ::sharcs -uppercase -noprime
    ktlkit::kopen mako ::mako -uppercase -noprime
    ktlkit::kopen saocon ::saocon -uppercase -noprime
    ktlkit::kopen saomot ::saomot -uppercase -primetimeout 5
    ktlkit::kopen sharcsdisplay ::sharcsdisplay -uppercase -noprime

    trace add variable ::saomot(DWFILT1NAM) write {trace_filt1}
    trace add variable ::saomot(DWFILT2NAM) write {trace_filt2}

    set tmp [ktlkit::kread poco POCHEART]
    puts "poco heartbeat $tmp"

    set tmp [ktlkit::kread sharcs ABORT]
    puts "sharcs ABORT $tmp"


    set ::testMode 0
    set ::pi [expr 2*asin(1.0)]


    set ::object test

    set ::scriptProcRunning 0
    set ::scriptPaused 0
    set ::showScriptProc 1
    array set ::scriptAbortAction {}
    set ::nexpose 1		; # number of exposures from service
    set ::nexpose_en $::nexpose	; # number of exposures in entry box.
    set ::exposuresTaken 0
    set ::nexposing 0
    set ::SharcsAbortActive 0

    set ::nodTele 0
    set ::nodWFS 0


    set ::defaultBG #d9d9d9
    set ::defaultABG #ececec


    set ::configure_nexpose [list]


    trace variable ::scriptProcRunning w trace_scriptProc
    WFSinit

    set ::libdir .
    set ::libdir /usr/local/lick/data/ShaneAO/sharcs/scripts/
    create_main_window

    # trace/watch these:
    set keyList {
	mako.NEXPOSE
	mako.NOD_TELE
	mako.NOD_WFS
	mako.AUTOOPENLOOP
	mako.AUTOCLOSELOOP
	saocon.SUBSTATE
	sharcs.ABORT
	poco.POCTUBPA
    }
    foreach sk $keyList {
	foreach {svc kwd} [split $sk .] {break}
	trace add variable ::${svc}($kwd) write traceKey
    }

    # This is not the normal way to prime-read keywords, but
    # it's the quick-and-dirty adaptation of the way the code
    # was originally written.
    foreach sk $keyList {
	foreach {svc kwd} [split $sk .] {break}
	ktlkit::kread $svc $kwd
	if {[catch {ktlkit::kread $svc $kwd} result]} {
	    puts "ktl-read failed for $svc.$kwd: $result"
	    # Act as if the keyword just updated:
	    traceKey $svc $kwd w
	}
    }
    # Whenever ::nexpose_en changes, make sure colors change correctly.
    trace add variable ::nexpose_en write adjust_nexpose_colors

    # Now that we know the actual ::nexpose, initialize nexpose_en,
    # the entry box value, to match actual:
    set ::nexpose_en $::nexpose
}

proc onScriptAbort {myname action} {
    if {$action eq ""} {
	catch {unset ::scriptAbortAction($myname)}
    } else {
	set ::scriptAbortAction($myname) $action
    }
}
proc trace_scriptProc {args} {
     global defaultBG defaultABG
     if {$::scriptProcRunning} {
        .ircaluimenubar entryconfigure 2 -background green -activebackground lightgreen
     } else {
        .ircaluimenubar entryconfigure 2 -background $defaultBG -activebackground $defaultABG
     }
}

# Can be used as a variable trace.
proc adjust_nexpose_colors {args} {
    if {$::nexpose != $::nexpose_en} {
	set bg yellow
    } else {
	set bg white
    }
    foreach w $::configure_nexpose {
	$w configure -bg $bg
    }
}

proc traceKey {ary idx op} {
    upvar $ary Ary
    set val [string trim $Ary($idx)]

    set svc [string trimleft $ary ":"]
    switch -exact -- "$svc.$idx" {
	mako.NEXPOSE {
	    if {$val != $::nexpose} {
		set ::nexpose $val 
		puts "nexpose $val"
	    }
	    adjust_nexpose_colors
	}

	mako.NOD_TELE {
	    if {$val == "True"} {
		set val 1
	    } else {
		set val 0
	    }
	    if {$val != $::nodTele} {
		set ::nodTele $val
		puts "nodTele $val"
	    }
	}

	mako.NOD_WFS {
	    if {$val == "True"} {
		set val 1
	    } else {
		set val 0
	    }
	    if {$val != $::nodWFS} {
		set ::nodWFS $val
		puts "nodWFS $val"
	    }
	}

	mako.AUTOOPENLOOP {
	    if {$val == "True"} {
		set val 1
	    } else {
		set val 0
	    }
	    if {$val != $::autoOpenLoops} {
		set ::autoOpenLoops $val
		puts "autoOpenLoops $val"
	    }
	}

	mako.AUTOCLOSELOOP {
	    if {$val == "True"} {
		set val 1
	    } else {
		set val 0
	    }
	    if {$val != $::autoCloseLoops} {
		set ::autoCloseLoops $val
		puts "autoCloseLoops $val"
	    }
	}

	saocon.SUBSTATE {
	    regexp "NGS" $val x
	    if {$val ne "NGS"} {
		set val "LGS"
	    }
	    if {$val ne $::aomode} {
		set ::aomode $val
		puts "AOMode $val"
	    }
	}

	sharcs.ABORT {
	    # ABORT can be No, Now, afterCoadd, afterGroup
	    if {$val ne "No"} {
		# Last time the ABORT keyword became active:
		set ::SharcsAbortActive [clock seconds]
	    }
	}

	poco.POCTUBPA {
	    set ::tub $val
	    set offset 74.0/180.*$::pi
	    set ::pa [expr $::tub - $offset]
	}

	default {
	    puts "ERROR: [info level [info level]] was invoked, but\
	    		this routine has no handler for $ary.$idx"
	}
    }
}


proc scriptListProc {cmdlist} {
    global scriptPaused
    global scriptPosition

    puts cmdlist:
    puts $cmdlist
    puts cmdlst_end

    if {$::scriptProcRunning} {
        tk_messageBox -type ok -message "A script is already running" -icon error
		return
    }

    set ::scriptProcRunning 1
    set showScriptProc 1
    set aonotreadyBG red
    set aonotreadyBG_default defaultBG
    #trace_aoready
    # (to trip the aoready trace to update colors

    if {$showScriptProc} {
        catch {destroy .scriptList}
        toplevel .scriptList
        set lb [listbox .scriptList.listbox]
        set i 0
        foreach  cmd $cmdlist {
            incr i
            $lb insert end "$i: $cmd"
        }
        pack $lb -expand 1
        set ncmds $i
        label .scriptList.status -text "[expr $ncmds]/$ncmds commands left"
        pack .scriptList.status
	button .scriptList.pause -text "Pause Execution" -command scriptPause
        pack .scriptList.pause -fill x -padx 10
	button .scriptList.abort -text "Abort Script" -command scriptAbort
        pack .scriptList.abort -fill x -padx 10

        update idletasks
    }
    set scriptPosition 0
    set scriptPaused 0
    set scriptlen [llength $cmdlist]

    writeSCRIPTSTATUS Running "Script starting"
    while {$scriptPosition < $scriptlen} {
        update
        set i [expr $scriptPosition+1]
        if {$showScriptProc} {
            $lb selection clear 0 $ncmds
	    # selection set syntax is "selection set <first> <last>"
            $lb selection set $i $i
            $lb yview [expr $i-3]
	    if {!$scriptPaused} {
	        .scriptList.status configure -text "[expr $ncmds-$i-1]/$ncmds commands left"
	    } else {
	        .scriptList.status configure -text " Paused before [expr $i+1]/$ncmds"
	        after 200
	    }
            update idletasks
        }
        # Check if aborted *before* performing any more actions
        # i.e. in case we were paused and then aborted.
	if {!$::scriptProcRunning} {
	    # The script must have been aborted
	    break
	}
	# If not paused, execute the next line of script.
        if {!$::scriptPaused} {
            incr ::scriptPosition
            puts [lindex $cmdlist $scriptPosition]
            eval [lindex $cmdlist $scriptPosition]

	    # Check if abort occurred during execution of the above line.
	    if {!$::scriptProcRunning} {
		# An abort must have been requested.
		break
	    }
        }
    }
    if {$scriptPosition == $scriptlen} {
	enter_logsheetstr script "Script completed OK."
    }

    catch {destroy .scriptList}
    writeSCRIPTSTATUS Done "Script finished"

   
    

    set aonotreadyBG $aonotreadyBG_default
    #trace_aoready
    # Indicate script is done.
    set ::scriptProcRunning 0
}


proc writeSCRIPTSTATUS { status_value {txt ""} } {
    enter_logsheetstr script "STATUS/MESSAGE = $status_value / $txt"
    if {[catch {ktlkit::kwrite mako SCRIPTSTATUS $status_value} status]} {
            puts "ktl-write failed SCRIPTSTATUS $status_value : $status"
    }
    if {[catch {ktlkit::kwrite mako SCRIPTMESSAGE $txt} status]} {
            puts "ktl-write failed SCRIPTMESSAGE $txt : $status"
    }
}

 
proc scriptPause {{action "toggle"}} {
    global scriptPaused
    global scriptPosition

    if {$action eq "toggle"} {
	if {$scriptPaused} {
	    set action "unpause"
	} else {
	    set action "pause"
	}
    }
    if {$action eq "unpause"} {
        # Unpause
        set scriptPaused 0

	# Do the related window stuff if the app has set up scriptList and pause button.
	if {[winfo exists .scriptList.pause]} {
	    .scriptList.pause configure -relief raised
	    .scriptList.pause configure -text "Pause Execution"
	    pack forget  .scriptList.fwd
		destroy .scriptList.fwd
	    pack forget  .scriptList.back
		destroy .scriptList.back
		enter_logsheetstr script "Script Resumed"
	}
    } else {
        #pause
        set scriptPaused 1

	# Do the related window stuff if the app has set up scriptList and pause button.
	if {[winfo exists .scriptList.pause]} {
	    .scriptList.pause configure -relief raised
	    .scriptList.pause configure -text "Resume Script"
	    button .scriptList.fwd -text "Skip Forward" -command {incr scriptPosition}
	    pack .scriptList.fwd -fill x -padx 10
	    button .scriptList.back -text "Skip Backward" -command {incr scriptPosition -1}
	    pack .scriptList.back -fill x -padx 10
		after 100
		enter_logsheetstr script "Script Paused"
	}
    }
}

proc scriptAbort {{msg ""}} {
    # Abort a script.
    #
    #
    set ::scriptProcRunning 0
    enter_logsheetstr "script" "Script Aborted!  $msg"

    if {$msg ne ""} {
	puts "scriptAbort -- $msg"
    }

    # Execute all abort actions requested by scripts.
    foreach name [array names ::scriptAbortAction] {
	set action $::scriptAbortAction($name)
	eval $action
    }

    if {$msg ne ""} {
	tk_messageBox -title "Script Aborted" -message "scriptproc \[scriptAbort]: $msg"
    }
}

proc scriptFileProc {} {
    global libdir

    set types {
       {{TCL scripts}   {.tcl}    }
       {{IRCAL scripts} {.script} }
       {{All Files}     *         }
    }
    puts $libdir
    set filename [tk_getOpenFile -initialdir $libdir \
                  -filetypes $types]

    if {$filename != ""} {
        source $filename
    }
}

proc create_NExpose_row {win row columnoffset {addGo false}} {
    label $win.nexpose_lb -text "NExpose" -justify left
    grid $win.nexpose_lb -row $row -column [expr {0 + $columnoffset}]
    entry $win.nexpose_en -textvariable ::nexpose_en -width 8
    bind $win.nexpose_en <Return> "writeNExpose"
    lappend ::configure_nexpose $win.nexpose_en
    grid $win.nexpose_en -row $row -column [expr {1 + $columnoffset}]
    button $win.nexpose_bt -text Set  -command writeNExpose
    grid $win.nexpose_bt -row $row -column [expr {2 + $columnoffset}]
    if {$addGo} {
	button $win.nexpose_go_bt -text Go -command NExposeGo
	grid $win.nexpose_go_bt -row $row -column [expr {3 + $columnoffset}]
    }
}

proc create_main_window {} {
    set uimb [menu .ircaluimenubar -tearoff 0 ]
    $uimb add cascade -label File -menu [menu $uimb.file -tearoff 0 ]
    $uimb.file add command -label "Source..." -command scriptFileProc
    $uimb.file add command -label Quit -command exit
    . configure -menu $uimb
    set ::ircaluimb $uimb

    label .testmode_lb -text "Test Mode" -justify left
    grid .testmode_lb -row 5 -column 0 
    checkbutton .testmode_cb -variable ::testMode -justify left
    grid .testmode_cb -row 5 -column 1

    create_NExpose_row "" 10 0 true

    label .exposure_lb -text "Exposure #"
    grid .exposure_lb -row 11 -column 0
    entry .exposure_en -textvariable ::exposuresTaken -state disabled
    grid .exposure_en -row 11 -column 1 -columnspan 3

    label .nexposing_lb -text "Exposing"
    grid .nexposing_lb -row 12 -column 0
    entry .nexposing_en -textvariable ::nexposing -state disabled
    grid .nexposing_en -row 12 -column 1 -columnspan 3
    
    label .aomodelgs_lb -text "AO mode"
    grid .aomodelgs_lb -row 20 -column 0
    label .aomodeval -textvariable ::aomode 
    grid .aomodeval -row 20 -column 1
    
    label .nodtele_lb -text "Nod-Telescope"
    grid .nodtele_lb -row 25 -column 0
    checkbutton .nodtele_cb -variable ::nodTele -command writeNodTele
    grid .nodtele_cb -row 25 -column 1     

    label .nodWFS_lb -text "Nod-WFS"
    grid .nodWFS_lb -row 30 -column 0
    checkbutton .nodWFS_cb -variable ::nodWFS -command writeNodWFS
    grid .nodWFS_cb -row 30 -column 1 

    label .autoOpen_lb -text "AutoOpenLoops"
    grid .autoOpen_lb -row 35 -column 0
    checkbutton .autoOpen_cb -variable ::autoOpenLoops -command writeAutoOpenLoop
    grid .autoOpen_cb -row 35 -column 1  

    label .autoClose_lb -text "AutoCloseLoops"
    grid .autoClose_lb -row 40 -column 0
    checkbutton .autoClose_cb -variable ::autoCloseLoops -command writeAutoCloseLoop
    grid .autoClose_cb -row 40 -column 1     
}

proc getAOMode {} {
    if {[catch {set strval [ktlkit::kread saocon SUBSTATE]} status]} {
	puts "ktl-read failed SUBSTATE $strval : $status"
    }
    if {[regexp "Unknown" $strval] != 0} {
	scriptAbort "AO mode Unknown" 
    }
    if {[regexp "NGS" $strval] != 0} {
	set val "NGS"
    } else {
	set val "LGS"
    }
    return $val
}

proc writeNExpose {} {
    set val $::nexpose_en
    if {[catch {ktlkit::kwrite mako NEXPOSE $val} status]} {
        puts "ktl-write failed NEXPOSE $val : $status"
	traceKey ::mako NEXPOSE w	; # ensure prev value restored.
    }   
}


proc writeNodTele {} {
    if {$::nodTele == 0} {
        set val "False"
    } else {
        set val "True"
    }

    puts "writeNodTele $val $::nodTele"
    if {[catch {ktlkit::kwrite mako NOD_TELE $val} status]} {
        puts "ktl-write failed NOD_TELE $val : $status"
	traceKey ::mako NOD_TELE w	; # ensure prev value restored.
    }   
}

proc writeNodWFS {} {
    if {$::nodWFS == 0} {
        set val "False"
    } else {
        set val "True"
    }

    puts "writeNodWFS $val $::nodWFS"
    if {[catch {ktlkit::kwrite mako NOD_WFS $val} status]} {
        puts "ktl-write failed NOD_WFS $val : $status"
	traceKey ::mako NOD_WFS w	; # ensure prev value restored.
    }   
}

proc writeAutoOpenLoop {} {
    if {$::autoOpenLoops == 0} {
        set val "False"
    } else {
        set val "True"
    }

    puts "writeautoOpenLoop $val $::autoOpenLoops"
    if {[catch {ktlkit::kwrite mako AUTOOPENLOOP $val} status]} {
        puts "ktl-write failed AUTOOPENLOOP $val : $status"
	traceKey ::mako AUTOOPENLOOP w	; # ensure prev value restored.
    }   
}

proc writeAutoCloseLoop {} {
    
    if {$::autoCloseLoops == 0} {
	set val "False"
    } else {
	set val "True"
    }

    # writing autocloseloop also takes care of setting 
    # loopsettledelay to the correct value.  mako does this for you.
    puts "writeAutoCloseLoop $val $::autoCloseLoops"
    if {[catch {ktlkit::kwrite mako AUTOCLOSELOOP $val} status]} {
        puts "ktl-write failed AUTOCLOSELOOP $val : $status"
	traceKey ::mako AUTOCLOSELOOP w	; # ensure prev value restored.
    }   
}

proc enter_logsheetstr {type str} {

    set now [clock format [clock seconds] ]

    if {$str == ""} { 
# catch the obsolete legacy case of only one argument provided
	set str $type
	set $type "unknown:"
    }

    set logstr [format "%-8s \[$now\]   $str" $type]
    puts $logstr
#	putLogsheet $logstr
#	if {!$speed_mode} { flush $logsheet }
}


proc WFSinit {} {
    #globals
    set ::ao 1
    set ::aoLoopStatus 0
    set ::aomode "NGS"

    #WFS variables
    set ::autoOpenLoops 1
    set ::autoCloseLoops 0
    set ::ircalOpenedLoopsLast 0 
    set ::closedLoopNodSize -1.0
    set ::entry_scale 0.0
   
    set ::nod_is_done 1
    set ::telSettleWait 1000
}

proc waitForTelMove {} {
    # Loop until the POCOCMD keyword is 0, which is the commanded
    # or offset position.  (Alternatively, one could write POCMVFIN
    # and wait for it to return success.)
    #
    # Also check ::scriptProcRunning: it will be set to 0 upon script abort.
    #
    set val 2
    while { $val != 0 && $::scriptProcRunning} {
        after  250 
	update idletasks
        set val [ktlkit::kread poco POCOCMD]
    }
}


# val must be Open or Close
proc loopOpenClose { val } {
    puts "loopOpenClose $val"
    if {!(($val eq "Open") || ($val eq "Close"))} {
	scriptAbort "invalid argument  $val to loopOpenClose"
    }

    if {$::testMode} {
        return
    }

    if {[getAOMode] eq "NGS"} {
	if {[catch {ktlkit::kwrite saocon LOOP $val} status]} {
	    scriptAbort "ktl-write failed LOOP $val : $status"
	}
    }
    if {[getAOMode] eq "LGS"} {
	if {$val eq "Open"} {
	    if {[catch {ktlkit::kwrite saocon TTOPEN 1} status]} {
		scriptAbort "ktl-write failed TTOPEN 1 : $status"
	    }
	} 
	if {$val eq "Close"} {
	    if {[catch {ktlkit::kwrite saocon TTCLOSE 1} status]} {
		scriptAbort "ktl-write failed TTCLOSE 1 : $status"
	    }
	}
    }
}


proc WFSopenLoops {} {
    loopOpenClose Open
}

proc WFScloseLoops {} {
    loopOpenClose Close
}


proc WFSPoint {dir size} {
    puts "WFSPoint $dir $size"
    if { $dir == {east}} {
        set x $size
    }
    if { $dir == {north}} {
        set y $size
    }
    
    WFSPointAbs $x $y
}

proc TTPoint {dir size} {
    puts "TTPoint $dir $size"
    if { $dir == {east}} {
        set x $size
    }
    if { $dir == {north}} {
        set y $size
    }
    
    TTPointAbs $x $y
}


proc WFSPointNod {x y} {
    set fs_x [ktlkit::kread mako FIELDPOSX]
    set fs_y [ktlkit::kread mako FIELDPOSY]
    set new_x [expr $fs_x + $x]
    set new_y [expr $fs_y + $y]
    puts "WFSPointNod x $new_x = $fs_x + $x" 
    puts "WFSPointNod y $new_y = $fs_y + $y"
    WFSPointAbs $new_x $new_y
} 

proc TTPointNod { x y} {
    set fs_x [ktlkit::kread mako TTFIELDPOSX]
    set fs_y [ktlkit::kread mako TTFIELDPOSY]
    set new_x [expr $fs_x + $x]
    set new_y [expr $fs_y + $y]
    puts "TTPointNod x $new_x = $fs_x + $x" 
    puts "TTPointNod y $new_y = $fs_y + $y"
    TTPointAbs $new_x $new_y
} 

# Move the steering mirrors to absolute point x,y. 
proc WFSPointAbs { x y } {
    puts "WFSPointAbs $x $y"
    # Write keyword values
    if {$::testMode} {
        puts "testMode ; not sending WFSPointAbs $x $y"
	return
    }

    if {[catch {ktlkit::kwrite mako FIELDPOSX $x} status]} {
	scriptAbort "ktl-write failed FIELDPOSX $x : $status"
    }
    if {[catch {ktlkit::kwrite mako FIELDPOSY $y} status]} {
	scriptAbort "ktl-write failed FIELDPOSY $y : $status"
    }
    if {[catch {ktlkit::kwrite mako FIELDPOS_GO 1} status]} {
	scriptAbort "ktl-write failed FIELDPOS_GO 1 : $status"
    }
}

# Move the steering mirrors to absolute point x,y. 
proc TTPointAbs { x y } {
    puts "TTPointAbs $x $y"

    # Write keyword values
    if {$::testMode} {
        puts "testMode ; not sending TTPointAbs $x $y"
	return
    }

    if {[catch {ktlkit::kwrite mako TTFIELDPOSX $x} status]} {
	scriptAbort "ktl-write failed TTFIELDPOSX $x : $status"
    }
    if {[catch {ktlkit::kwrite mako TTFIELDPOSY $y} status]} {
	scriptAbort "ktl-write failed TTFIELDPOSY $y : $status"
    }
    if {[catch {ktlkit::kwrite mako TTFIELDPOS_GO 1} status]} {
	scriptAbort "ktl-write failed TTFIELDPOS_GO 1 : $status"
    }
}


proc telmove { ew ns } {
    puts "telmove $ew $ns"
    puts "RA POCRARC $ew [ConvertArcsecToRad $ew]"
    puts "Dec POCDERE $ns [ConvertArcsecToRad $ns]"

    if {$::testMode} {
    	puts "testMode ; not sending"
	return
    }

    # set RA offset, instant
    if {[catch {ktlkit::kwrite poco POCDERE [ConvertArcsecToRad $ns] -binary} status]} {
	scriptAbort "ktl-write failed POCDERE $ns [ConvertArcsecToRad $ns] : $status"
    }
    #set Dec offset, instant
    if {[catch {ktlkit::kwrite poco POCRARC [ConvertArcsecToRad $ew] -binary} status]} {
	scriptAbort "ktl-write failed POCRARC $ew [ConvertArcsecToRad $ew] : $status"
    }
    # Wait for move to finish. This should take a few seconds and we need to wait for it to finish.
    if {[catch {ktlkit::kwrite poco POCMVFIN 1} status]} {
	scriptAbort "ktl-write failed POCMVFIN 1  : $status"
    }
    puts "telmove Finished"
}


proc telmoveAbs { ew ns } {
    puts "telmoveAbs $ew $ns"
    puts "RA POCRARCT $ew [ConvertArcsecToRad $ew]"
    puts "Dec POCDERET $ns [ConvertArcsecToRad $ns]"

    if {$::testMode} {
    	puts "testMode ; not sending"
	return
    }

    # set RA offset, instant
    if {[catch {ktlkit::kwrite poco POCDERET [ConvertArcsecToRad $ns] -binary} status]} {
	scriptAbort "ktl-write failed POCDERET $ns [ConvertArcsecToRad $ns] $status"
    }
    #set Dec offset, instant
    if {[catch {ktlkit::kwrite poco POCRARCT [ConvertArcsecToRad $ew] -binary} status]} {
	scriptAbort "ktl-write failed POCRARCT $ew [ConvertArcsecToRad $ew] $status"
    }
    # Wait for move to finish. This should take a few seconds and we need to wait for it to finish.
    if {[catch {ktlkit::kwrite poco POCMVFIN 1} status]} {
	scriptAbort "ktl-write failed POCMVFIN 1  $status"
    }
    puts "telmoveAbs Finished"
}

proc WFSnodAbs {EW NS} {
    global ao
    global aoLoopStatus
    
    global autoOpenLoops
    global autoCloseLoops
    global closedLoopNodSize
    global telSettleWait
    global nod_is_done
    global ircalOpenedLoopsLast

    set nod_is_done 0

    puts "WFSnodAbs making relative move $EW $NS"
	
    if {$autoOpenLoops} {
	# only remember to close loops later if we actually open them here.
# What is the check on aoLoopStatus for?
#	if $aoLoopStatus && $autoCloseLoops
	if  {$autoCloseLoops} {
	# puts "Setting IRCALOpenedLoopsLast"
	    set ircalOpenedLoopsLast 1
	} 
	WFSopenLoops
    }

    if {$::nodWFS} {
	set wfs_ew [expr 1*$EW]
	set wfs_ns [expr 1.*$NS]
	if {[getAOMode] eq "NGS"} {
	    WFSPointNod $wfs_ew $wfs_ns
	} 
	if {[getAOMode] eq "LGS"} {
	    TTPointNod $wfs_ew $wfs_ns
	}
        after 1000  # Don Gavel says this should be done within a second
    }

    if {$::nodTele} {
	#if TUB is rotated so that PA != 0, convert to actual EW and NS moves
	set ns_pa [expr $NS*cos($::pa)-$EW*sin($::pa)]
	set ew_pa [expr $NS*sin($::pa)+$EW*cos($::pa)]

        puts "Nodding Telescope EW=$ew_pa NS=$ns_pa"
        telmove $ew_pa $ns_pa
    }
 
    waitForTelMove
    if {$autoCloseLoops && $ircalOpenedLoopsLast} {    
        after $::telSettleWait
        WFScloseLoops
        set ircalOpenedLoopsLast 0
   }
    set nod_is_done 1
};

proc checkNodSize { ew ns } {

    if {[getAOMode] eq "LGS"} {    
	set sizeok [checkLGSnodsize $ew $ns]
    }
    if {[getAOMode] eq "NGS"} {
	set sizeok [checkNGSnodsize $ew $ns]
    }
    puts "checkNodSize sizeok: $sizeok"
    return $sizeok
}

proc bxyCheckNodSize { size } {
    set ok 1
    if {([checkNodSize $size $size] < 0) || ([checkNodSize -1.*$size -1.*$size] < 0) || ([checkNodSize -1.*$size $size] < 0) || ([checkNodSize $size -1.*$size] < 0)} {
	set ok -1
	puts "failed in bxyCheckNodSize"
    } 
    return $ok
}

proc rbxyCheckNodSize { ewsize nssize } {
    set ok 1
    if {([checkNodSize $ewsize $nssize] < 0) || ([checkNodSize -1*$ewsize -1.*$nssize] < 0) || ([checkNodSize -1.*$ewsize $nssize] < 0) || ([checkNodSize $ewsize -1.*$nssize] < 0)} {
	set ok -1
	puts "failed in rbxyCheckNodSize"
    } 
    return $ok
}
    

proc checkLGSnodsize { ew ns } {

    set sizeok 1
    set cur_ew [ktlkit::kread mako TTFIELDPOSX]
    set cur_ns [ktlkit::kread mako TTFIELDPOSY]
    set new_ew [expr $cur_ew + $ew]
    set new_ns [expr $cur_ns + $ns]
    #TTCam FOV is an ellipse, see if point falls inside or outside ellipse:
    set p0 53.9
    set p1 69.1
    set p2 2.33
    set p3 -0.35
    set p4 -0.377
    # formula from http://stackoverflow.com/questions/7946187/point-and-ellipse-rotated-position-test-algorithm
    #
    set termA [expr ( cos($p4) * ($new_ew-$p2) + sin($p4) * ($new_ns-$p3) ) / $p0 ]
    set termB [expr ( sin($p4) * ($new_ew-$p2) - cos($p4) * ($new_ns-$p3) ) / $p1 ]
    set test [expr $termA * $termA + $termB * $termB ]
    puts stdout "$termA $termB $test"
    if {$test > 1.0} {
	tk_messageBox -message "Move is outside of motor range.  Aborting move."
	set sizeok -1
	puts "failed checkLGSnodsize"
    }

    return $sizeok
}

proc checkNGSnodsize { ew ns } {

    set ngs_ew_max 6.8
    set ngs_ns_max 10.0

    set sizeok 1

    set cur_ew [ktlkit::kread mako FIELDPOSX]
    set cur_ns [ktlkit::kread mako FIELDPOSY]
    set newew [expr $cur_ew + $ew]
    set newns [expr $cur_ns + $ns]
    if {([expr abs($newew)] > $ngs_ew_max) || ([expr abs($newns)] > $ngs_ns_max)} {
	set sizeok -1
	puts "failed checkNGSnodsize cur_ew $cur_ew cur_ns $cur_ns ew $ew ns $ns newew $newew newns $newns"
    }

    return $sizeok
}
    

proc waitforfilts { } {

    puts "Desired positions are $::filt1_desired $::filt2_desired"
    #puts "Upon starting, actual positions are $::saomot(DWFILT1NAM) $::saomot(DWFILT2NAM)"

    set filt1 [ktlkit::kread saomot DWFILT1NAM]
    set filt2 [ktlkit::kread saomot DWFILT2NAM]
    puts "Upon starting, actual positions are $filt1 $filt2"

    while { $filt1 ne $::filt1_desired || $filt2 ne $::filt2_desired } {
	set filt1 [ktlkit::kread saomot DWFILT1NAM]
    	set filt2 [ktlkit::kread saomot DWFILT2NAM]
        puts "Actual positions are $filt1 $filt2"
        after 2000
    }
    puts "Reached desired filter positions, exitting waitforfilts"
}

proc setfilt1 { new_posn } {
    
    set ::filt1_desired $new_posn

    if {![info exists ::Tracing(filt1)]} {
        trace add variable ::saomot(DWFILT1NAM) write {trace_filt1}
        set ::Tracing(filt1) 1
    }

    ktlkit::kwrite saomot DWFILT1NAM $::filt1_desired 

    set filt False
    after 250
    while { $filt ne $::filt1_desired } {
        after 250
        set filt [ktlkit::kread saomot DWFILT1NAM]
    }

    puts "setfilt1"

    #puts " $::saomot(DWFILT1NAM) "
    #after 1000
    #ktlkit::kwrite saomot DWFILT1NAM $new_posn
    #after 1000
    #puts "$::saomot(DWFILT1NAM) $::saomot(DWFILT2NAM)"
    #trace_filt1
}

proc trace_filt1 { ary idx op } {

    puts "trace_filt1 activated, $::saomot(DWFILT1NAM)" 

    #global filt1
    #puts "$filt1"
    #puts "$ary $idx $op"
    #set checkfilt $::saomot(DWFILT1NAM)
    #puts "$checkfilt yow"
    #while { $checkfilt != $filt1 } {
    ##while { $::saomot(DWFILT1NAM) != $arg } {}
    #    set checkfilt $::saomot(DWFILT1NAM)
    #    puts "DWFILT1NAM is $checkfilt, not $filt1 "
    #    #puts "DWFILT1NAM is $::saomot(DWFILT1NAM), not $arg "
    #    #set $::saomot(DWFILT2NAM) {ktlkit::kread saomot DWFILT2}
    #    update idletasks
    #    after 2000
    # }
}

proc setfilt2 { new_posn } {

    set ::filt2_desired $new_posn
    
    if {![info exists ::Tracing(filt2)]} {
        trace add variable ::saomot(DWFILT2NAM) write {trace_filt2}
        set ::Tracing(filt2) 1
    }

    ktlkit::kwrite saomot DWFILT2NAM $::filt2_desired 

    #ktlkit::kwrite saomot DWFILT2NAM $arg
    #after 1000
    #trace_filt2 $arg
}

proc trace_filt2 { ary idx op  } {

    puts "trace_filt2 activated, $::saomot(DWFILT2NAM)" 

    #while { $::saomot(DWFILT2NAM) != $arg } {
    #    puts "DWFILT2NAM is $::saomot(DWFILT2NAM), not $arg "
    #    #set $::saomot(DWFILT2NAM) {ktlkit::kread saomot DWFILT2}
    #    update idletasks
    #    after 500
    # }
}


proc ConvertArcsecToRad {val_arcsec} {
    global globArr
    set val_rad [expr $val_arcsec * $::pi / (180.0 * 3600)]
    return $val_rad
}

proc waitForAO {} {

    global autoCloseLoops

    if {$autoCloseLoops != 1} {
	exec tellSoundboard -T :1 photon_torp.wav
    }
    set val "foo"
    # Also check ::scriptProcRunning: it will be set to 0 upon script abort.
    #
    while { ($val ne "Settled") && $::scriptProcRunning} {
	update idletasks
        after  250 
	set val [ktlkit::kread saocon LOOPSTATE]
    }
}

# Return 1 [0] if sharcs.ABORT became active in the past $delta_t seconds.
proc checkSharcsAbort {delta_t} {
    if {([clock seconds] - $::SharcsAbortActive) <= $delta_t} {
	return 1
    } else {
	return 0
    }
}


proc expose {{runningvar ::scriptProcRunning}} {
    upvar $runningvar is_running
    puts "expose"
    if { !$is_running } {
	# Script must have been aborted
	return
    }
    if {$::testMode} {
	puts "testMode ; not sending ; sleeping for 1 sec instead."
	after 1000
    } else {
	ktlkit::kwrite sharcs GO 1
	after 500

	# Normally, keyword IMAGEDONE is set upon sharcs
	# exposure completion.  However, if one puts invalid
	# settings into sharcs, then tells it to expose,
	# sharcs will set IMAGEDONE to false before it notices
	# the wrong settings.  In that case, IMAGEDONE will not
	# ever be set True.  Therefore, check for a recent
	# sharcs.ABORT as well as the usual script abort.
	#
	set imagedone False
	while {[checkSharcsAbort 1] == 0 && ($imagedone eq "False")} {
	    if { !$is_running } {
		break
	    }
	    update 
	    after 250
	    set imagedone [ktlkit::kread sharcs IMAGEDONE]
	}
    }
}

proc expose_nexpose {{runningvar ::scriptProcRunning}} {
    upvar $runningvar is_running

    puts "expose_nexpose"    

    set ::exposuresTaken 0
    set ::nexposing 1
    set starttime [clock clicks -milliseconds]

    #check if everything ready
    # There is no means provided to tell if steering motors are done,
    # so remind user to eventually add some real 'ready' checks as
    # they become available.
    puts "TODO - add real ready checks as keywords become available,\
    						sleeping for a sec now"
    after 1000

    # Also check is_running: it will be set to 0 upon script abort.

    while {$::exposuresTaken < $::nexpose && $is_running} {

	# Wait for possibly-paused script to unpause before starting
	# next exposure, but also response to script abort.
        while {$::scriptPaused && $is_running} {
	    # Waiting for unpause...
            update
            after 250
        }
	if {!$is_running} {
	    # script must have been aborted
	    break
	}
	# If here: script is not paused, and script is running.
	incr ::exposuresTaken
	puts "Starting exposure $::exposuresTaken of $::nexpose" 
	expose is_running

        # Do things between exposures.
        update
    }    
    set endtime [clock clicks -milliseconds]
    set deltat [expr ($endtime*1.0-$starttime)/1000]
    set ::nexposing 0
    puts "   ** Done with nexpose **   Time elapsed:  $deltat"
    
}

proc NExposeGo {} {
    variable w
    variable obj
        set cmdlist [subst {
	    { global object entry_object }
	    { writeSCRIPTSTATUS Running "Starting to take images" }
	    { writeSCRIPTSTATUS Exposing "Taking images" }
	    { expose_nexpose }
	    { writeSCRIPTSTATUS Done "Script Done" }
	} ]


        enter_logsheetstr script "takeN: take N exposures with common settings"

        scriptListProc $cmdlist
}

main
