namespace eval imagesharpen {

variable w imagesharpen

variable Row 0

proc incrRow {} {
    variable Row
    incr Row
}

package require ktlkit

onScriptAbort imagesharpen imagesharpen::setAborted
proc setAborted {} {
    imagesharpen::setscriptstate "aborted" "Script aborted"
}

variable scale
variable rastep
variable decstep
variable going
variable nsteps
variable nexp
variable m1
variable m2
variable m3
variable m4
variable m5
variable m6
variable m7
variable m8
global modes
set modesDefault {0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0. 0.}
set modes $modesDefault
variable autosave ; set autosave 1
variable v1
variable v2
variable v3
variable v4
variable v5
variable v6
variable v7
variable v8
variable v9
variable v10
variable v11
variable v12


proc setscriptstate { status txt } {
    set imagesharpen::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}

proc hartmannmodeset { modeNum value} {
    set modeNum [expr $modeNum -1]
    lset imagesharpen::modes $modeNum $value
    set v1 [lindex $imagesharpen::modes 0]
    set v2 [lindex $imagesharpen::modes 1]
    set v3 [lindex $imagesharpen::modes 2]
    set v4 [lindex $imagesharpen::modes 3]
    set v5 [lindex $imagesharpen::modes 4]
    set v6 [lindex $imagesharpen::modes 5]
    set v7 [lindex $imagesharpen::modes 6]
    set v8 [lindex $imagesharpen::modes 7]
    set v9 [lindex $imagesharpen::modes 8]
    set v10 [lindex $imagesharpen::modes 9]
    set v11 [lindex $imagesharpen::modes 10]
    set v12 [lindex $imagesharpen::modes 11]
    # update mode display
    set imagesharpen::v1 $v1
    set imagesharpen::v2 $v2
    set imagesharpen::v3 $v3
    set imagesharpen::v4 $v4
    set imagesharpen::v5 $v5
    set imagesharpen::v6 $v6
    set imagesharpen::v7 $v7
    set imagesharpen::v8 $v8
    set imagesharpen::v9 $v9
    set imagesharpen::v10 $v10
    set imagesharpen::v11 $v11
    set imagesharpen::v12 $v12
    # send command to shaneao
    set cmd "ssh -Y gavel@shade local/bin/imageSharpen -s $v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12"
    puts stdout $cmd
    eval exec $cmd

}

proc hswritefile {} {
    #write modes to Refcent File
    set v1 [lindex $imagesharpen::modes 0]
    set v2 [lindex $imagesharpen::modes 1]
    set v3 [lindex $imagesharpen::modes 2]
    set v4 [lindex $imagesharpen::modes 3]
    set v5 [lindex $imagesharpen::modes 4]
    set v6 [lindex $imagesharpen::modes 5]
    set v7 [lindex $imagesharpen::modes 6]
    set v8 [lindex $imagesharpen::modes 7]
    set v9 [lindex $imagesharpen::modes 8]
    set v10 [lindex $imagesharpen::modes 9]
    set v11 [lindex $imagesharpen::modes 10]
    set v12 [lindex $imagesharpen::modes 11]
    set cmd "ssh -Y gavel@shade local/bin/imageSharpen -f $imagesharpen::filename -s $v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12"
    puts stdout $cmd
    eval exec $cmd
    #generate Sky refcents file
    set saomode [ktlkit::kread saocon MODE]
    set filename $imagesharpen::filename\_$saomode.fits
    if {$saomode == "16x" || $saomode == "16xLGS"} { set subaps 16x } {set subaps 8x}
    set cmd "ssh user@real /usr/local/lick/bin/genSkyRefcents $subaps $filename 1.0"
    puts stdout $cmd
    eval exec $cmd
}

proc imagesharpenCreate {} {
    variable Row
    variable w
	global object
    variable obj
    variable showHelp

    catch {destroy .$w}
    toplevel .$w
    wm title .$w "imagesharpen"

set instruct "Image Sharpening\n\n\
Performs iterative image sharpening using\n\
the AO system's centdiag and sharcsdisplay.\n\
  1) Center target\n\
  2) Configure exposure parameters\n\
  3) Press Go\
"
    set Row 0

    label .$w.instructt -text $instruct	-justify left
    grid .$w.instructt -row [incrRow] -column 0 -columnspan 4

    create_NExpose_row .$w [incrRow] 1

    label .$w.lscale -text "Image Sharpening Step Size:"
    entry .$w.escale -textvariable imagesharpen::scale -width 4
    grid .$w.lscale -row [incrRow] -column 1
    grid .$w.escale -row $Row -column 2

    label .$w.lmodes -text "Select Modes:"
    checkbutton .$w.m1 -text Focus -variable imagesharpen::m1
    checkbutton .$w.m2 -text Astigmatism -variable imagesharpen::m2
    checkbutton .$w.m3 -text Astigmatism45 -variable imagesharpen::m3
    checkbutton .$w.m4 -text Tricoma -variable imagesharpen::m4
    checkbutton .$w.m5 -text Tricoma60 -variable imagesharpen::m5
    checkbutton .$w.m6 -text Coma -variable imagesharpen::m6
    checkbutton .$w.m7 -text Coma90 -variable imagesharpen::m7
    checkbutton .$w.m8 -text Spherical44- -variable imagesharpen::m8
    checkbutton .$w.m9 -text Spherical44+ -variable imagesharpen::m9
    checkbutton .$w.m10 -text Spherical40 -variable imagesharpen::m10
    checkbutton .$w.m11 -text Spherical42+ -variable imagesharpen::m11
    checkbutton .$w.m12 -text Spherical42- -variable imagesharpen::m12

    entry .$w.lm1 -width 6 -textvariable imagesharpen::v1 -relief flat
    entry .$w.lm2 -width 6 -textvariable imagesharpen::v2 -relief flat
    entry .$w.lm3 -width 6 -textvariable imagesharpen::v3 -relief flat
    entry .$w.lm4 -width 6 -textvariable imagesharpen::v4 -relief flat
    entry .$w.lm5 -width 6 -textvariable imagesharpen::v5 -relief flat
    entry .$w.lm6 -width 6 -textvariable imagesharpen::v6 -relief flat
    entry .$w.lm7 -width 6 -textvariable imagesharpen::v7 -relief flat
    entry .$w.lm8 -width 6 -textvariable imagesharpen::v8 -relief flat
    entry .$w.lm9 -width 6 -textvariable imagesharpen::v9 -relief flat
    entry .$w.lm10 -width 6 -textvariable imagesharpen::v10 -relief flat
    entry .$w.lm11 -width 6 -textvariable imagesharpen::v11 -relief flat
    entry .$w.lm12 -width 6 -textvariable imagesharpen::v12 -relief flat

    grid .$w.lmodes -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.m1 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm1 -row $Row -column 3 -sticky w
    grid .$w.m2 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm2 -row $Row -column 3 -sticky w
    grid .$w.m3 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm3 -row $Row -column 3 -sticky w
    grid .$w.m4 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm4 -row $Row -column 3 -sticky w
    grid .$w.m5 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm5 -row $Row -column 3 -sticky w
    grid .$w.m6 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm6 -row $Row -column 3 -sticky w
    grid .$w.m7 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm7 -row $Row -column 3 -sticky w
    grid .$w.m8 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm8 -row $Row -column 3 -sticky w
    grid .$w.m9 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm9 -row $Row -column 3 -sticky w
    grid .$w.m10 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm10 -row $Row -column 3 -sticky w
    grid .$w.m11 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm11 -row $Row -column 3 -sticky w
    grid .$w.m12 -row [incrRow] -column 1 -columnspan 2 -sticky w
    grid .$w.lm12 -row $Row -column 3 -sticky w 

    button .$w.bZero -text "Zero Modes" -command { 
	set imagesharpen::modes $imagesharpen::modesDefault
	imagesharpen::hartmannmodeset 1 0
    } 
    grid .$w.bZero -row [incrRow] -column 1 
 
    checkbutton .$w.autoSave -text "Auto Save Modes" -variable imagesharpen::autosave
    grid .$w.autoSave -row $Row -column 3

    label .$w.lfilename -text "Output File Name Base:"
    entry .$w.efilename -textvariable imagesharpen::filename -width 30
    grid .$w.lfilename -row [incrRow] -column 1 
    grid .$w.efilename -row [incrRow] -column 1 -columnspan 2
    button .$w.bSaveFile -text "Save File" -command imagesharpen::hswritefile
    grid .$w.bSaveFile -row $Row -column 3 -sticky news
    
    button .$w.bGo -command imagesharpen::imagesharpenGo -text "Go"
    grid .$w.bGo -row [incrRow] -column 1

    button .$w.bStop -command imagesharpen::Stop -text "Stop"
    grid .$w.bStop -row $Row -column 2

    button .$w.bClose -command imagesharpen::imagesharpenClose -text "Close"
    grid .$w.bClose -row $Row -column 3
    label .$w.scriptstate -textvariable imagesharpen::scriptstate
    grid .$w.scriptstate -row [incrRow] -column 1 -columnspan 3

    set imagesharpen::scale 0.15
    set imagesharpen::nsteps 6
    set imagesharpen::nexp 1
    set imagesharpen::filename hs-

    set imagesharpen::m1 1
    set imagesharpen::m2 1
    set imagesharpen::m3 1
    set imagesharpen::m4 0
    set imagesharpen::m5 0
    set imagesharpen::m6 0
    set imagesharpen::m7 0
    set imagesharpen::m8 0
    set imagesharpen::m9 0
    set imagesharpen::m10 0
    set imagesharpen::m11 0
    set imagesharpen::m12 0
 };

proc Stop {} {
    variable w
    variable going
    set going 0
}


proc imagesharpenClose {} {
    variable w
    destroy .$w
    onScriptAbort imagesharpen ""
}

#
# Use this to clean up before returning from imagesharpenGo.
# It doesn't return anything useful, but you can embed it as
# the return value from [return] to simplify coding, e.g.
#	return [imagesharpenDone $w 1]
#
proc imagesharpenDone {w {early_stop 0}} {

    if {$early_stop} {
	setscriptstate Done "Script Stopped Early"
    } else {
	setscriptstate Done "Script Done"
    }

    .$w.bGo configure -state normal
    .$w.bClose configure -state normal
}

proc imagesharpenGo {} {
    variable w
    variable obj
    variable going
    variable nsteps
    variable nexp
    variable scale
    variable m1
    variable m2
    variable m3
    variable m4
    variable m5
    variable m6
    variable m7
    variable m8
    variable m9
    variable m10
    variable m11
    variable m12

    # Make sure we're not paused.
    scriptPause unpause

    set going 1
    enter_logsheetstr script "imagesharpening script"

    .$w.bGo configure -state disabled
    .$w.bClose configure -state disabled

    # Set parameters are for which modes to sharpen
    set zernickes [list 1 2 3 4 5 6 7 8 9 10 11 12]
    set zdo [list $m1 $m2 $m3 $m4 $m5 $m6 $m7 $m8 $m9 $m10 $m11 $m12]
    set nzernickes [llength $zernickes]

#Image Sharpening
    for {set z 0} {$z < $nzernickes} {incr z} {

	set mode [lindex $zernickes $z]
	set zdoYN [lindex $zdo $z]

	if {$zdoYN > 0} {
	    set n 0
	    set minpos [expr -2*$scale]
	    set maxpos [expr 2*$scale]

	    if {!$going} {return [imagesharpenDone $w 0]}
	    for {set pos $minpos} {$pos < [expr $scale * 3]} {set pos [expr $pos + $scale]} {
		setscriptstate SetMode "Mode $mode, value $pos"
		hartmannmodeset $mode $pos
		ktlkit::kwrite sharcsdisplay FOCVAL $pos
		update ; after 1000
		expose_nexpose going
		if {!$going} {return [imagesharpenDone $w 0]}
		update ; after 2000
		if {!$going} {return [imagesharpenDone $w 0]}
		ktlkit::kwrite sharcsdisplay ACTION FWHMauto
		update ; after 1000
		if {!$going} {return [imagesharpenDone $w 0]}
		if {$n == 0} {
		    ktlkit::kwrite sharcsdisplay ACTION FocFitInit
		} else {
		    ktlkit::kwrite sharcsdisplay ACTION FocFit
		}
		incr n
	    }
	    while {$n < 10} {
		if {!$going} {return [imagesharpenDone $w 0]}
		set bestfit [ktlkit::kread sharcsdisplay BESTFIT]
		if {$bestfit < [expr $minpos+$scale] || $bestfit > [expr $maxpos-$scale]} {
		    if {$bestfit < [expr $minpos+(2*$scale)]} {
			set pos [expr $minpos - $scale]
			set minpos $pos
		    }
		    if {$bestfit > [expr $maxpos-(2*$scale)]} {
			set pos [expr $maxpos + $scale]
			set maxpos $pos
		    }
		    setscriptstate SetMode "Mode $mode, value $pos"
		    hartmannmodeset $mode $pos
		    ktlkit::kwrite sharcsdisplay FOCVAL $pos
		    update ; after 1000
		    if {!$going} {return [imagesharpenDone $w 0]}
		    expose_nexpose going
		    if {!$going} {return [imagesharpenDone $w 0]}
		    update ; after 2000
		    if {!$going} {return [imagesharpenDone $w 0]}
		    ktlkit::kwrite sharcsdisplay ACTION FWHMauto
		    update ; after 1000
		    if {!$going} {return [imagesharpenDone $w 0]}
		    ktlkit::kwrite sharcsdisplay ACTION FocFit
		    incr n
		} else {
		    set n 10
		}
	    }
	    set bestfit [ktlkit::kread sharcsdisplay BESTFIT]
	    set pos $bestfit
	    setscriptstate SetMode "Mode $mode, bestfit $pos"
	    hartmannmodeset $mode $pos
	    expose_nexpose going
	    if {!$going} {return [imagesharpenDone $w 0]}
	}
    }

    # Done Setting modes, write output to file automatically
    # Need to finish implementing w/ checkbox to write file when done
    if {!$going} {return [imagesharpenDone $w 0]}
    if {$imagesharpen::autosave} {
	set time [clock seconds]
	set date [clock format $time -format %Y-%m-%d]
	set cent [ktlkit::kread saocon WFS_CENTROID]
	set imagesharpen::filename hs-$date-$cent
	hswritefile
    }
    if {!$going} {return [imagesharpenDone $w 0]}

    imagesharpenDone $w
}

global ircaluimb
$ircaluimb.file add command -label "imagesharpen" -command imagesharpen::imagesharpenCreate

imagesharpen::imagesharpenCreate

}
