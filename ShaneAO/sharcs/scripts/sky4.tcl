namespace eval sky4 {

variable w sky4

variable Row 0

proc incrRow {} {
    variable Row
    incr Row
}

onScriptAbort sky4 sky4::setAborted
proc setAborted {} {
    sky4::setscriptstate "aborted" "Script aborted"
}

proc sky4Create {} {
    variable w
    variable Row
    catch {destroy .$w}
    toplevel .$w
    wm title .$w "sky4"

set instruct "Box Dither\n\n\
  1) Center target\n\
  2) Configure exposure parameters\n\
  3) Choose whether to expose at starting position\n\
  4) Choose Object Dither Scale\n\
  5) Press Go\
"
    label .$w.instructt -text $instruct	-justify left
    grid .$w.instructt -row [incrRow] -column 0 -columnspan 4

    create_NExpose_row .$w [incrRow] 1

    label .$w.lscale -text "Object Dither Scale (\")"
    entry .$w.escale -textvariable sky4::scale -width 4
    grid .$w.lscale -row [incrRow] -column 1
    grid .$w.escale -row $Row -column 2

    label .$w.lnexpose -text "Exposures"
    entry .$w.enexpose -textvariable ::exposures -width 4 

    label .$w.lexposefirst -text "Expose at starting position?"
    checkbutton .$w.eexposefirst -variable sky4::exposefirst 
    grid .$w.lexposefirst -row [incrRow] -column 1
    grid .$w.eexposefirst -row $Row -column 2


    button .$w.bGo -command sky4::sky4Go -text "Go"
    grid .$w.bGo -row [incrRow] -column 1

    button .$w.bClose -command sky4::sky4Close -text "Close"
    grid .$w.bClose -row $Row -column 2

    label .$w.scriptstate -textvariable sky4::scriptstate
    grid .$w.scriptstate -row [incrRow] -column 1 -columnspan 2

    set sky4::scale 5

};


proc setscriptstate { status txt } {
    set sky4::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}


proc sky4Close {} {
    variable w
    destroy .$w
    onScriptAbort sky4 ""
}

proc sky4Go {} {
    variable w

    # Make sure we're not paused.
    scriptPause unpause

    if {[traffic::is_float $sky4::scale]} {
        set cmdlist [subst {
	    { sky4::setscriptstate Running "Script starting" }
	    { if {$sky4::exposefirst > 0} {
		sky4::setscriptstate Exposing "Starting Pos'n Exposure" 
		expose_nexpose
	    }}
	    { sky4::setscriptstate Moving "Moving to position 1" }
	    { telmove $sky4::scale $sky4::scale}
	    { sky4::setscriptstate Exposing "Exposure at position 1" }
	    { expose_nexpose}
	    { sky4::setscriptstate Moving "Moving to position 2" }
	    { telmove 0 [expr -2.*$sky4::scale]}
	    { sky4::setscriptstate Exposing "Exposure at position 2" }
	    { expose_nexpose}
	    { sky4::setscriptstate Moving "Moving to position 3" }
	    { telmove [expr -2.*$sky4::scale] 0}
	    { sky4::setscriptstate Exposing "Exposure at position 3" }
	    { expose_nexpose}
	    { sky4::setscriptstate Moving "Moving to position 4" }
	    { telmove 0 [expr 2.*$sky4::scale]}
	    { sky4::setscriptstate Waiting "Waiting at position 4" }
	    { sky4::setscriptstate Exposing "Exposure 4" }
	    { expose_nexpose}
	    { sky4::setscriptstate Moving "Moving to original position" }
	    { telmove $sky4::scale [expr -1.*$sky4::scale]}
	    { sky4::setscriptstate Done "Script Done" }
	} ]

        enter_logsheetstr script "sky4: box dither by $sky4::scale arcsec"

        .$w.bGo configure -state disabled
        .$w.bClose configure -state disabled
        scriptListProc $cmdlist
        .$w.bGo configure -state normal
        .$w.bClose configure -state normal
    } else {
        tk_messageBox -type ok -message "Invalid scale $sky4::scale arcsec"
    }
}

global ircaluimb
$ircaluimb.file add command -label "sky4" -command sky4::sky4Create

sky4::sky4Create


};
