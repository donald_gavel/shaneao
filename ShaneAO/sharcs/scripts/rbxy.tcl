namespace eval rbxy {

variable w rbxy

variable Row 0

proc incrRow {} {
    variable Row
    incr Row
}

onScriptAbort rbxy rbxy::setAborted
proc setAborted {} {
    rbxy::setscriptstate "aborted" "Script aborted"
}

proc rbxyCreate {} {
    variable Row
    variable w
    global object
    variable obj
    catch {destroy .$w}
    toplevel .$w
    wm title .$w "rbxy"

set instruct "4 Position Rectangle Dither\n\n\
  1) Center target\n\
  2) Configure exposure parameters\n\
  3) Choose whether to expose at starting position\n\
  4) Choose Object Dither Scale in EW and NS\n\
  5) Press Go\
"

    set Row 0

    label .$w.instructt -text $instruct	-justify left
    grid .$w.instructt -row [incrRow] -column 0 -columnspan 4


    create_NExpose_row .$w [incrRow] 1

    label .$w.lewscale -text "Object Dither Scale EW(\")"
    entry .$w.eewscale -textvariable rbxy::ewscale -width 4
    grid .$w.lewscale -row [incrRow] -column 1
    grid .$w.eewscale -row $Row -column 2

    label .$w.lnsscale -text "Object Dither Scale NS(\")"
    entry .$w.ensscale -textvariable rbxy::nsscale -width 4
    grid .$w.lnsscale -row [incrRow] -column 1
    grid .$w.ensscale -row $Row -column 2

    label .$w.lnexpose -text "Exposures"
    entry .$w.enexpose -textvariable ::exposures -width 4 

    label .$w.lexposefirst -text "Expose at starting position?"
    checkbutton .$w.eexposefirst -variable rbxy::exposefirst 
    grid .$w.lexposefirst -row [incrRow] -column 1
    grid .$w.eexposefirst -row $Row -column 2

    button .$w.bGo -command rbxy::rbxyGo -text "Go"
    grid .$w.bGo -row [incrRow] -column 1

    button .$w.bClose -command rbxy::rbxyClose -text "Close"
    grid .$w.bClose -row $Row -column 2

    label .$w.scriptstate -textvariable rbxy::scriptstate
    grid .$w.scriptstate -row [incrRow] -column 1 -columnspan 2

    set rbxy::ewscale 3
    set rbxy::nsscale 5
    set rbxy::obj $object

};


proc setscriptstate { status txt } {
    set rbxy::scriptstate $txt
    writeSCRIPTSTATUS $status $txt
}


proc rbxyClose {} {
    variable w
    destroy .$w
    onScriptAbort rbxy ""
}

proc rbxyGo {} {
    variable w
    variable obj

    # Make sure we're not paused.
    scriptPause unpause

    if {[traffic::is_float $rbxy::ewscale] && [traffic::is_float $rbxy::nsscale] && ([rbxyCheckNodSize $rbxy::ewscale $rbxy::nsscale] > 0)} {
        set cmdlist [subst {
	    { global object entry_object}
	    { rbxy::setscriptstate Running "Script starting" }
	    { if {$rbxy::exposefirst > 0} {
		set entry_object "$obj (Starting Pos)" 
		set object "$obj (Starting Pos)" 
		rbxy::setscriptstate Exposing "Starting Pos'n Exposure" 
		expose_nexpose
	    }}
	    { rbxy::setscriptstate Moving "Moving to position 1" }
	    { WFSnodAbs $rbxy::ewscale $rbxy::nsscale}
	    { rbxy::setscriptstate Waiting "Waiting at position 1" }
	    { waitForAO }
	    { set entry_object "$obj (Pos 1)" }
	    { set object "$obj (Pos 1)" }
	    { rbxy::setscriptstate Exposing "Exposure 1" }
	    { expose_nexpose}
	    { rbxy::setscriptstate Moving "Moving to position 2" }
	    { WFSnodAbs 0 [expr -2.*$rbxy::nsscale]}
	    { rbxy::setscriptstate Waiting "Waiting at position 2" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 2)" }
	    { set object "$obj (Pos 2)" }
	    { rbxy::setscriptstate Exposing "Exposure 2" }
	    { expose_nexpose}
	    { rbxy::setscriptstate Moving "Moving to position 3" }
	    { WFSnodAbs [expr -2.*$rbxy::ewscale] 0}
	    { rbxy::setscriptstate Waiting "Waiting at position 3" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 3)" }
	    { set object "$obj (Pos 3)" }
	    { rbxy::setscriptstate Exposing "Exposure 3" }
	    { expose_nexpose}
	    { rbxy::setscriptstate Moving "Moving to position 4" }
	    { WFSnodAbs 0 [expr 2.*$rbxy::nsscale]}
	    { rbxy::setscriptstate Waiting "Waiting at position 4" }
	    { waitForAO}
	    { set entry_object "$obj (Pos 4)" }
	    { set object "$obj (Pos 4)" }
	    { rbxy::setscriptstate Exposing "Exposure 4" }
	    { expose_nexpose}
	    { rbxy::setscriptstate Moving "Moving to original position" }
	    { WFSnodAbs $rbxy::ewscale [expr -1.*$rbxy::nsscale]}
	    { set entry_object "$obj" }
	    { set object "$obj" }
	    { rbxy::setscriptstate Done "Script Done" }
	} ]

        enter_logsheetstr script "rbxy: 4 pos box dither by EW $rbxy::ewscale arcsec and NS by $rbxy::nsscale"

        .$w.bGo configure -state disabled
        .$w.bClose configure -state disabled
        scriptListProc $cmdlist
        .$w.bGo configure -state normal
        .$w.bClose configure -state normal
    } else {
        tk_messageBox -type ok -message "Invalid scale $bxy::scale arcsec"
    }
}

global ircaluimb
$ircaluimb.file add command -label "rbxy" -command rbxy::rbxyCreate

rbxy::rbxyCreate


};
