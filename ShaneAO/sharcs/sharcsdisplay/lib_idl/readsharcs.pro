function readsharcs,imnum,header,path=path,singleframe=singleframe,$
  framename=framename, badpixfix=badpixfix, bug=bug

   common sharcs_comm, imagetitle, img, data, imghdr,badpix
   common sharcsdisp_common,state,field
   ; function to read SHARCS images from an arbitrary Lick machine
   ; and divide them by the number of coadds and nreads
   ; quick version        Bruce Macintosh          June 1999

   imghdr = strarr(5)
   IF ~keyword_set(bug) THEN bug=0 ELSE bug=1
   IF ~keyword_set(badpixfix) THEN badpixfix=0 ELSE badpixfix=1

   if n_elements(imnum) eq 0 then begin
        er = dialog_message("You must supply a valid image number!",/error)
        return,img
   endif

   if n_elements(path) eq 0 then path='/data/sharcs'
   ; rmcgurk - will want to change the below once a real filename pattern is set
   IF keyword_set(framename) THEN BEGIN $
     fname = strsplit(framename,'sharcs/',/regex,/extract) 
     IF n_elements(fname) NE 1 THEN fname = strsplit(framename,path+'/',/regex,/extract) 
   ENDIF ELSE $
     fname='s'+string(imnum,format="(I4.4)")+'.fits'

   ;if n_elements(path) eq 0 then path='/net/sharcsbox/data'
   ; change as SHARCS data moves
   ;fname='sharcs'+string(imnum,format="(I4.4)")+'.fits'
   imagetitle=fname[0]
   ;   print,fname
   if ~(file_test(path+'/'+fname)) then begin
      er = dialog_message("The requested file "+path+"/"+fname+" does "+$
        "not exist!",/error)
      return,img
   endif


   ; rmcgurk - badpix fixing code from Connie
   ; python -m sharcsbfixpix rawdatafilename outfilename  badpixmaskfilename rmin rmax cmin cmax npix_average
   ;   - rmin rmax cmin cmax = 298 1130 622 1454 to get a square image enclosing the ShARCS FoV
   ;                          ymin ymax xmin xmax  ???
   ;   - a good starting value for npix_average is 4
   ;   - currently code can't handle windowed frames -> avoid it for now :-)
   newheader=headfits(path+'/'+fname)
   windowed=strsplit(sxpar(newheader,'HXRGCFG2'),' ',/extract)
   ;; if windowed, EQ 0002 --- currently the windowed files lack all header info 
   IF windowed[0] NE '0002' THEN state.windowed = 0 ELSE state.windowed = 1

   IF badpixfix AND state.windowed EQ 0 THEN BEGIN ; can only deal with full frames currently
       spawn,'kpython -m sharcsbfixpix '+path+'/'+fname+$
          ' /usr/local/lick/var/sharcsdisplay/fixed.fits '+$
          state.badpixfile+' 332 1164 683 1515 4'
          ;'/usr/local/lick/data/sharcs/sharcsdisplay/badpix_g9_slopes.fits '+$
          ;'298 1130 622 1454 4'
       path = '/usr/local/lick/var/sharcsdisplay'
       fname = 'fixed.fits'
   ENDIF

   newimage=readfits(path+'/'+fname,newheader)
   if (n_elements(newimage) eq 1) then begin
      er = dialog_message("The requested file "+path+"/"+fname+" is "+$
          "not a valid FITS file!",/error)
      return,img
   endif 
   header=temporary(newheader)
   imghdr = header ; save in the common block
   image=temporary(newimage)
   ;coadds=sxpar(header,'ncoadds')
   ;nreads=sxpar(header,'nreads')
   ; rmcgurk  
   coadds=sxpar(header,'COADDONE')
   nreads=1.
   state.startx = sxpar(header,'STARTX')
   state.endx   = sxpar(header,'ENDX')
   state.starty = sxpar(header,'STARTY')
   state.endy   = sxpar(header,'ENDY')
   state.angle  = 74.-sxpar(header,'TUBANGLE')*180./!pi ;74 is (relative) 0 E of N
   IF windowed[0] NE '0002' AND $
       (((state.endx-state.startx+1) EQ 2048) AND ((state.endy-state.starty+1) EQ 2048)) $
       THEN state.windowed = 0 ELSE state.windowed = 1

   ; TODO add error checking here?
   
   ptr_free, state.head_ptr
   state.head_ptr = ptr_new(header)

   objname = sxpar(header,"OBJECT")
   IF objname EQ '       0' THEN objname='no header'
   IF bug then print,objname
   ; rmcgurk - no object name available yet
   ;objname = 'Not Available Yet' 

   state.imagename=imagetitle[0]  ;fname[0]
   widget_control, state.name_label, set_value = imagetitle[0]  ;fname[0]
   IF state.windowed EQ 0 THEN widget_control, state.object_label, set_value = objname
   ; blank out the Strehl of the previous image when loading a new one.
   widget_control, state.strehl_label, set_value = "        "

   ; convert image to double precision:
   image=double(image)
   ;recover number of coadds
   ;image=image/coadds
   ; ShARCS already outputs flux per coadd, so no division should be performed
   ; rmcgurk 2014-09-14
   
; rmcgurk why divide by the number of reads?
   ;; and divide by nreads
   ;   nreads=sxpar(header,'nreads') 
   ;   image=image/nreads
   ;decoadd
   ;   exptime=sxpar(header,'coaddtim')
   ;   filter1=sxpar(header,'filter1')
   ;   filter2=sxpar(header,'filter2')
   ;   print,fname,coadds,exptime/1000.,filter1,filter2,$
   ;   format="(A14,2X,I3,' x ',F7.3,' s',4x,A8,4x,A8)"
   exptime=sxpar(header,'TRUITIME')
   reqtime=sxpar(header,'ITIME')
   print,fname,"  actual itime = ",exptime,"   requested itime = ",float(reqtime)/1000.
   
   if keyword_set(singleframe) then begin
     print, "  singleframe"
     ; if the image is a stack (eg noCDS mode), then return a single frame
     sz=size(image)
     if sz[0] eq 3 then begin
        ; take the last frame from the stack:
        image=image[*,*,sz[3]-1]
     endif
   endif     

   ; ShARCS!
   ; On sky tests show that the native file has north left and east up.
   ; That is confusing to astronomers (and everyone, so we will be rotating 
   ; flipping the image so that North is up and East is left.
   ;IF state.axes LE 2 THEN image = rotate(image, 6)

   return,image
end
