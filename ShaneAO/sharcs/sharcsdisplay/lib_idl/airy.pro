;+
; NAME: airy
;
;	calculate an airy function for a circular (possibly obscured) 
;	aperture.
;
;	Optionally overlay a scaled gaussian to simulate an AO
;	Airy+Halo PSF
;
; INPUTS:
; 	teldiam		in meters
; 	lambda		in meters
; 	platescale	in arcsec/pixel, specify the desired platescale for the
; 				returned result
; 	arraysize	specify the desired size of the returned PSF. The PSF
; 				will be centered in the array.
; KEYWORDS:
; 	r = r		returns radius in arcseconds
; 	obscuration	input the fractional obscuration of the telescope 
; 				i.e. SecDiam/TelDiam. Assumed =0 if not specified.
; 	strehl		Strehl ratio for simulated PSF
; 	seeing		FWHM for gaussian seeing disk
; OUTPUTS:
;
; HISTORY:
; 	Began 2003-05-02 17:43:08 by Marshall Perrin 
;
; 	2003-09-28	changed lambda from microns to meters for consistency.
;-

function airy,teldiam,lambda,platescale,arraysize,r=r,obscuration=obscuration,$
		strehl=strehl,seeing=seeing,xcenter=xcenter,ycenter=ycenter,v=v,oversample=oversample

	if not(keyword_set(obscuration)) then obscuration=0
	if not(keyword_set(xcenter)) then xcenter=arraysize/2
	if not(keyword_set(ycenter)) then ycenter=arraysize/2

	
	if (lambda gt 1e-4) then $
		message,"lambda should be in METERS. It looks like you tried to specify it in some other unit??"

	if keyword_set(oversample) then begin
		oversample = oversample*1.0 ; /make it a float
		arraysize *= oversample
		platescale /= oversample
		xcenter = xcenter*oversample + (oversample-1.)/2.
		ycenter = ycenter*oversample + (oversample-1.)/2.
	endif

	x = ((findgen(arraysize)-xcenter) ) *platescale
	y = ((findgen(arraysize)-ycenter) ) *platescale
	x = rebin(x,arraysize,arraysize)
	y = rebin(transpose(y),arraysize,arraysize)
	r = sqrt(x^2+y^2)
	wz = where (r eq 0,rzc)
	if rzc gt 0 then r[wz] = 0.000001 ; avoid divide by zero

	; platescale = 206265/(D*f#) = 206265/Focal length  ["/mm]
	;
	; v = 2pi/lambda (R/Focal_len) * linear offset
	;   = 2pi/lambda (R/Focal_len) * linear offset / platescale
	;    =2pi/lambda (R) * angular offset/206265
	; then lose the factor of 2 to switch from radius to diameter:

	v = !dpi*(r/206256.)*teldiam/(lambda)
	e = obscuration


	airy =  1./(1-e^2)^2* ((2*beselj(v,1) - e*2*beselj(e*v,1))/v )^2
	; see e.g. Schroeder, Astronomical Optics, 2nd ed. page 248

	if keyword_set(strehl) and keyword_set(seeing) then begin
		; for a gaussian exp(-(r/s)^2) then the FWHM will be
		; 2 sqrt(ln(2))*s
		;
		; alternatively, s = FWHM/(2*sqrt(ln(2)))
		sigma = seeing/(2.*sqrt(alog(2.)))
		gaussian = exp(-1.0*(r/sigma)^2)
		gaussian = gaussian/total(gaussian)
		tg = total(gaussian)
		ta = total(airy)
		;aopsf = airy/ta*strehl + gaussian/tg*(1.0 - strehl)
				; now scale things to keep total flux the same while fixing the
		; strehl appropriately for the combined image
		peakratio = max(airy)/total(airy)
		y = (strehl - peakratio*ta)/(max(gaussian) - peakratio*tg)
		x = (ta - y*tg)/ta

		aopsf = airy*x + gaussian*y
		;print,total(airy),total(aopsf)
		;print,max(airy),max(aopsf)
		airy = aopsf
	endif 


if keyword_set(oversample) then airy = rebin(airy,arraysize/oversample,arraysize/oversample)

	return,airy

end
