;+
; NAME: setup_coords
;
; 	Sets up coordinates just like plot does, but without plotting anything.
;
; 	See http://idlastro.gsfc.nasa.gov/idl_html_help/plotting5.html
;
; INPUTS:
; KEYWORDS:
; OUTPUTS:
;
; HISTORY:
; 	Began 2004-11-29 01:31:29 by Marshall Perrin 
;-
PRO setup_coords,position=position,xrange=xrange,yrange=yrange

	if not(keyword_set(position)) then position = !p.position
	if total(abs(position)) eq 0 then message,"Error: no position supplied!"
	
	!x.type=0
	!y.type=0
	!x.s[1] = (position[2]-position[0])/(xrange[1]-xrange[0])
	!y.s[1] = (position[3]-position[1])/(yrange[1]-yrange[0])

	!x.s[0] = position[0] - !x.s[1]*xrange[0]
	!y.s[0] = position[1] - !y.s[1]*yrange[0]

end
