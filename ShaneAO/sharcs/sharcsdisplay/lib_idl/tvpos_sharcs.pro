PRO tvpos_sharcs,xpos_out,ypos_out,silent=silent

	common sharcsdisp_common,state,field
  	common sharcs_comm


	if not(keyword_set(silent)) then $
          PRINT,'Mark the selected point with the cursor.'
	CURSOR,XX,YY,/DEVICE

	res = convert_coord(XX,YY,/device,/to_data)
	xpos_out = round(res[0])
	ypos_out = round(res[1])
	;sharcsdisp_redraw
			 
;stop
end
