pro minmax_sharcs,image,imin,imax
    ; this is the full-range scaling for sharcsdisp2
    
    ;COMMON SHARCS_COMM, imagetitle
    common sharcsdisp_common,state,field

    state.scaling = 0
    state.min_value = imin
    state.max_value = imax
    sharcsdisp_settitle
    IF state.windowed EQ 0 THEN sharcsdisp_redraw ELSE sharcsdisp_fullimage
    ;; if full image, just redraw;   if windowed, redo coords
end
