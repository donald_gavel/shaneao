;+
; NAME: alogscale.pro
;
; 	intelligently logarithmicly scale an image for 
;	display. 
;
;	Based on the log scale code from ATV.pro
;
; INPUTS:
; KEYWORDS:
; OUTPUTS:
;
; HISTORY:
; 	Began 2003-10-20 01:23:02 by Marshall Perrin 
;-

FUNCTION alogscale,image,minval,maxval
	if (n_elements(minval) eq 0) then minval = min(image)
	if (n_elements(maxval) eq 0) then maxval = max(image)
	

    offset = minval - (maxval - minval) * 0.01
      
     scaled_image = $
          bytscl( alog10(image - offset), $
                  min=alog10(minval - offset), /nan, $
                  max=alog10(maxval - offset))

   return,scaled_image 

end
