pro sigdisp,image,sigrange

common sharcsdisp_common,state,field
	
if n_params() lt 2 then sigrange=3

state.scaling = 3
state.sigma = sigrange
sharcsdisp_settitle

IF state.windowed EQ 0 THEN sharcsdisp_redraw ELSE sharcsdisp_fullimage
;; if full image, just redraw;   if windowed, redo coords

;  sig=sigma(image)
;  av=avg(image)
;  print,av,sig,format='("Average = ",f9.1,"  Std Dev = ",f9.2)'
;  print,sigrange
;  ;exptv,image
;  ;tvimage,image > (av-sigrange*sig)  < (av+sigrange*sig) ,pos=state.position
;  ;tvimage,image > (av-sigrange*sig)  < (av+sigrange*sig) ,pos=state.position
;  imdisp,image,range=[(av-sigrange*sig),(av+sigrange*sig)],pos=state.position
;  ;exptv,image,/data,/nobox,min=av-sigrange*sig,max=av+sigrange*sig
;  return
end
