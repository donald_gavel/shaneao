;$Id: fwhmlick.pro,v 1.5 2014/05/22 04:21:24 rmcgurk Exp $
;$Log: fwhmlick.pro,v $
;Revision 1.5  2014/05/22 04:21:24  rmcgurk
;added plotfits to make oplotcompass possible in the future
;
;Revision 1.4  2014/05/21 03:57:50  rmcgurk
;ongoing updates of sharcsdisplay
;
;Revision 1.3  2014/05/10 23:53:41  rmcgurk
;Zooming and windowing problems fixed mostly.  Additional FWHMs output added in fwhmlick.pro.
;
;Revision 1.2  2014/05/08 20:32:11  egates
;Modified by Rosalie, committing for current general install.
;
;Revision 1.1  2014/04/03 21:32:40  will
;*** empty log message ***
;
;
;Revision 1.19 2008/11/04 17:06:25  egates
;Added ircaldisplay idl routines.
;
;Revision 1.18  2006/10/14 04:06:14  lgs
;*** empty log message ***
;
;Revision 1.17  2005/10/16 23:37:01  mperrin
;modified to clean up code and add mpfit2dpeak
;
;Revision 1.16  2005/10/16 23:27:44  lgs
;various mods
;
;Revision 1.14  2005/08/02 07:29:45  mperrin
;added code to use findmaxstar and restriction to left hand side for Wollaston
;auto-fwhm peak finding.
;
;Revision 1.13  2005/08/01 21:32:14  lgs
;update
;
;Revision 1.12  2005/07/29 18:56:47  kermit
;move
;
;Revision 1.11  2005/02/17 05:18:25  ircal
;Bugfixes for ircaldisplay. I believe it's all working properly now!
;
;Revision 1.10  2005/02/17 04:09:01  mperrin
;Fixed a few CVS conflicts on the ircalui and leachd code;
;no substantial changes.
;
;Bug fixes to ircaldisplay, tested on whitestar (my Mac laptop) but not
;yet on ircalbox2.
;
;Revision 1.9  2004/11/29 11:18:38  ircal
;Greatly revamped ircal display system! Now uses pipes, not SWIG, to control
;IDL from Tcl, with the result that we can now have both event loops going at once.
;Added widget-based IDL display tool, and modified IDL and Tcl code to use it.
;
;Revision 1.8  2001/08/17 03:19:44  ccdev
;Modified to make room for error handling, though currently error handling
;is handled by a "catch" statement in fwhmlick
;
;Revision 1.7  2001/06/14 19:34:57  ccdev
;Added a function to popup zoomed-in psf images on linear and sqrt scale.
;Currently this is disabled by default because it messes up coordinate systems
;needed for interaction with the "main" ircal image
;
;Bruce Macintosh
;
;Revision 1.6  2001/06/12 23:03:43  ccdev
;modified to correctly output the fitted parameters
;
;Revision 1.5  2000/09/21 19:05:46  ccdev
;
;Modifications to fwhmlick for robustness - bmac
;
;Revision 1.3  2000/05/25 11:14:13  ccdev
;minor bug fixes.  Add /autofind to fwhmlick.pro
;
;Revision 1.2  2000/05/18 08:07:00  ccdev
;*** empty log message ***
;


pro fwhmlick,image,outparams,radius=radius,single=single,autofind=autofind,$
  photrad=photrad,popim=popim


;fwhm/strehl calculator, hardwired for SHARCS at K'
;could be enhanced in its treatment of background normalization
;input keywords:
;radius=radius of gaussian fit (default=6)
;single=only do one star 
;autofind=centroid on the max value in the image
;photrad=radius for photometric normalization (default=12)
;popim= pop up a zoomed in image of the star on a linear scale
;popim current screws up the exptv coordinate systems; this needs to be
;looked at.


;rmcgurk change(to[
;outputs: 
;outparams[0]=x centroid from gaussian fit
;outparams[1]=y centroid from gaussian fit
;outparams[2]=x FWHM from fit
;outparams[3]=y FWHM from fit
;outparams[4]=FWHM from pixel-counting 
;outparams[5]=strehl from gaussian fit peak
;outparams[6]=strehl from cubic-spline image rebinning
;outparams[7]=avg of the above 2 strehls
;on_error,2 ; semi-graceful error handling

     common sharcsdisp_common,state,field
     common SHARCS_COMM ; for fits header in imghdr variable

if n_elements(radius) eq 0 then radius=6
if n_elements(photrad) eq 0 then photrad=12

continue=1
if NOT keyword_set(autofind) then print,'left button for fwhm, right button to exit'

windex=!D.WINDOW
wset,windex

while continue do begin
     if (keyword_set(single)) then continue=0
     sz=size(image)     
     if NOT keyword_set(autofind) then begin
          tvpos_sharcs,xcur,ycur

     endif else begin

          if strc(sxpar(imghdr,"FILT2NAM")) eq "Wollaston" then begin
               ; FIXME restrict to left hand FOV
               print,' '
               print," fwhmlick doesn't know where the wollaston lies on the new detector, update!"
               print,' '
               findmaxstar,image[0:127,*],xcur,ycur
          endif else begin
               findmaxstar,image,xcur,ycur
               ;maxval = max(median(image,3),index)
               ;xcur = index mod sz(1)
               ;ycur = index / sz(2)
          endelse
     endelse


     if !err eq 4 then continue = 0
     if (!err eq 1) or keyword_set(autofind) then begin
          box=radius*2+1
          print,xcur,ycur,radius
          cntrd,image,xcur,ycur,x,y,radius,/silent,/keepcenter
          print,x,y
          if (x ne -1 and y ne -1) then begin
              ;     outparams=params
              ;if keyword_set(nomask) then kmask=0 else kmask=1
              kmask = ~(keyword_set(nomask))
              ;aper,image,x,y,mag,magerr,sky,skyerr,8.6,$
                       ;   [radius*2],[radius*3,radius*3+5],$
                       ;   [-32765,32765],/flux,/silent
              starflux=bmacaper(image,photrad,x,y,photrad+7,$
                   photrad+10,skyout=apersky,/draw,crad=radius*2)


              ; Check that we're not too close to the edge of the image
              ; Do this check *AFTER* the bmacaper call, since it's draws the photometry
              ; radii on the image.
              if (x lt radius) or (y lt radius) or (x gt sz[1]-radius) or (y gt sz[2]-radius) then begin
                  continue=0
                  widget_control, state.strehl_label, set_value = "Too close to image edge!"
                  print, "That point is too close to the image edge to measure the Strehl!"
                  print, "  Go to 'Config->Configure' to change the radii and try again."
                  return
              endif

              starflux=starflux[0]
              subim=image[x-radius+1:x+radius,y-radius+1:y+radius]
              subim=congrid(subim,radius*8,radius*8,/cubic)
              submax=max(subim)

              if (x lt 2*radius) or (y lt 2*radius) or (x gt sz[1]-2*radius) or (y gt sz[2]-2*radius) then begin
                  continue=0
                  widget_control, state.strehl_label, set_value = "Too close to image edge!"
                  print, "That point is too close to the image edge to measure the Strehl!"
                  print, "  Go to 'Config->Configure' to change the radii and try again."
                  return
              endif

              congridstrehl=(submax/starflux) / (0.162)
              subim2=image[x-2*radius+1:x+2*radius,y-2*radius+1:y+2*radius]
              subpop=subim2 ; save for later display

              subim2=congrid(subim2,radius*16,radius*16,/cubic)
              npixfwhm=n_elements(where(subim2 gt 0.5*submax))
              npixfwhm=2*sqrt(npixfwhm/!pi)/4.
              newrad=npixfwhm*1.5
              box=2*(newrad)+1

              ; Compute FWHM via Gaussian fitting
              fwhmastro,image,x,y,box,params
              sky=params[0]
              peak=params[1]
              xsig=params[2]
              ysig=params[3]
              xc=params[4]
              yc=params[5]
              ;rot=params[6]*180./!pi ; UNUSED
              
              strehl=(peak/starflux) / (0.162)
              strehlav=(strehl+congridstrehl)/2.
              strehlstr=string(strehlav,format="('Strehl=',F4.2)")
              ;xyouts,0.85,0.02,strehlstr,/normal,align=1.0
              widget_control, state.strehl_label, set_value = strehlstr
              ps = state.platescale

              print,' xcen      ycen     peak( g / r )     (x / y)fwhm npfwhm'+$
                  '  (x / y)fwhm"  npfwhm"  '
              print,   xc,   yc,       peak,      submax,xsig*2.35,ysig*2.35,npixfwhm,$
                  xsig*2.35*ps,ysig*2.35*ps,npixfwhm*ps,$
                  format='(1x,f7.1,1x,f7.1,2x,f7.0," / ",f7.0,2X,f5.2," / ",f5.2,1x,f5.2,2X,f5.2," /",f5.2,1x,f5.2)'

              print,'      flux     sky   strehl / congridstrehl'
              print, starflux,apersky,strehl,congridstrehl,$
               format='(2x,F9.0,2x,f6.1,2x,F5.2,"  /  ",F5.2)'
               outparams=fltarr(8)
               outparams = [xc, yc, xsig*2.35, ysig*2.35, npixfwhm, strehl, congridstrehl,strehlav ]
               
              wset,28

              plotgauss,image,params,newrad*1.5
              if keyword_set(popim) then begin
                 wset,29
                 put,subpop,1,2
                 put,sqrt(subpop>0),2,2
              endif   
              wset,windex
              sharcsdisp_setup_coords ; needed to fix coord systems after above plots.
          endif else begin
              continue=0
              widget_control, state.strehl_label, set_value = "Centroid error!"
              print,'Centroider error; rerun with bigger aperture.'
          endelse
     endif
     ;wait,0.25
endwhile
end
