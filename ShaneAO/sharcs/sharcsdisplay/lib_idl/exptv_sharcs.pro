PRO exptv_sharcs,img,outparams

  common sharcsdisp_common,state

  ;size of display box
  s=30

  if (state.axes eq 3) then begin
     xmax=state.endx-state.startx
     ymax=state.endy-state.starty
     x=outparams[0]
     y=outparams[1]
  endif else begin
     ;convert to proper coordinates for displaying zoomed PSF
     xmax=state.endy-state.starty
     ymax=state.endx-state.startx
     x=xmax-outparams[1]
     y=ymax-outparams[0]
  endelse

  ;check that display region is in range
  x1=x-s
  x2=x+s
  y1=y-s
  y2=y+s

  if (x1 lt 0) then x1=0
  if (y1 lt 0) then y1=0
  if (x2 gt xmax) then x2=xmax
  if (y2 gt ymax) then y2=ymax

  ;display image region
  exptv,sqrt(img[x1:x2,y1:y2]>0)

end

