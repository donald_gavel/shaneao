pro plotgauss,image0,gaussparams,radius
	;program to overlay a gaussian measured by gaussfit2d onto a radial plot of
	;an image
	;on_error,2   ; not completely graceless error recovery but we'll handle with 
	;fwhmlick for now
	if n_elements(radius) eq 0 then radius=7
	
	
	blx=floor(gaussparams[4]-radius-2) > 0
	bly=floor(gaussparams[5]-radius-2) > 0
	urx=blx+2*radius+4 < 2047
	ury=bly+2*radius+4 < 2047
	
	;image=image0(blx:blx+2*radius+4,bly:bly+2*radius+4)
	image=image0[blx:urx,bly:ury]
	
	radii=radmap(image,gaussparams[4]-blx,gaussparams[5]-bly)
	valid=where(radii lt radius)
	plot,[radii[valid]],[image[valid]],psym=1,xtitle='Pixels',ytitle='DN/coadd'
	
	avsigs=0.5*(gaussparams[2]+gaussparams[3])
	rads=findgen(200.)/200.*radius
	gaussvals=gaussparams[0]+gaussparams[1]*exp(-(rads/gaussparams[2])^2/2)
	oplot,rads,gaussvals
	
	
	
	
end
