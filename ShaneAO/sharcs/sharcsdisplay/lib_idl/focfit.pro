function focfit,newfoc,newstr,focs,strehls,plot=plot,nfit=nfit,reset=reset
;function to fit a nfit-order polynomial to an array of (focus, strehl)
;values 
;call multiple times with the newest value of the focus and strehl and it
;will automatically extend the array of old values and begin fitting when it
;has enough points
;developmental version   bmac jun 2001

;weird error problem: returns a 
;% Program caused arithmetic error: Floating divide by 0
;when it returns for the first time. maybe related to polyfit

if n_elements(nfit) eq 0 then nfit=2

ncurve=200

nvals=n_elements(focs)+1
if keyword_set(reset) then nvals=1
if nvals gt 1 then begin 
 oldfoc=focs
 oldstr=strehls
 focs=fltarr(nvals)
 strehls=fltarr(nvals)
 focs(0:nvals-2)=oldfoc
 strehls(0:nvals-2)=oldstr
 focs(nvals-1)=newfoc
 strehls(nvals-1)=newstr
endif else begin
  focs=fltarr(nvals)
 strehls=fltarr(nvals)
 focs(nvals-1)=newfoc
 strehls(nvals-1)=newstr
endelse

if nvals le nfit then begin
	print,nfit-nvals+1,' more points needed to fit'
        bestfoc=-9999
endif else begin
        pfit=poly_fit(focs,strehls,nfit)
;	print,pfit
        plotmin=min(focs)-0.2*(max(focs)-min(focs))
        plotmax=max(focs)+0.2*(max(focs)-min(focs))
        plotrange=plotmax-plotmin
;	print,plotmin,plotmax,plotrange
        vals=findgen(ncurve)*(plotrange/ncurve)+plotmin
	outvals=fltarr(ncurve)
        for i=0,nfit do begin
		outvals=outvals+pfit(i)*vals^i
	endfor
	
	if keyword_set(plot) then begin
		windex=!D.WINDOW
;		window,29,xsize=400,ysize=300
                wset,29
		strmin=min(outvals)<min(strehls)
		strmax=max(outvals)>max(strehls)
		plot,vals,outvals,ystyle=16,yrange=[strmin,strmax]
		oplot,focs,strehls,psym=1
		wset,windex
		sharcsdisp_setup_coords
	endif
	beststr=max(outvals)
	bestfoc=vals(where(outvals eq beststr))
	;print,'best fit: str=',beststr,' at ',bestfoc
endelse
return,bestfoc
end
