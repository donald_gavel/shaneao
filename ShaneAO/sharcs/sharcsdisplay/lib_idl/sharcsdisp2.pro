;  Display images in a GUI
;
;  Usage:
;    sharcsdisp, DataArray2or3d, [Name], [size=[x,y]]
;
;  Inputs:
;    DataArray2or3d - the data, must be type real
;    Name - optional string to display at the top of the window
;    size - optional initial window size.  Default: 300x300
;
;  Needs:
;    exptv, tvdevice, etc. from the Thompson graphics library
;     (eg. http://sohowww.nascom.nasa.gov/solarsoft/gen/idl/image/)
;
;  Version: 3
;    Allows pointer argument.  If data is passed in directly,
;    a local copy is made.
;    3d data (datacube) is displayed one x-y plane
;    at a time, with a scroll bar on the left for picking the z
;    coordinate.
;
;  To Do:
;    Window resize - Done
;    Auto-shape to original data aspect ratio - Done
;    Automatic display of pixel values - Done
;    Slice plots
;    Menu to allow chosing a file
;    Log, Lin, HistEq scaling
;    Help
;    Hybrid mode target regions
;
;-

;==========================================================
pro sharcsdisp_initcommon

     common sharcsdisp_common,state,field
     common sharcs_comm, imagetitle, img, data, imghdr, badpix

     state = {$
          version: " sharcsdisplay 2.0 ",$
          title: "sharcsdisplay", $
          image : fltarr(10,10),$
          imagename: "test image ",$     ; currently displayed image
          head_ptr: ptr_new(), $         ; pointer to image header
          astr_ptr: ptr_new(), $         ; pointer to astrometry info structure
          wcstype: 'none', $             ; coord info type (none/angle/lambda)
          equinox: 'J2000', $            ; equinox of coord tdepth: 8, $                 ; 8 or 24 bit color mode?
          ncolors: !d.table_size, $      ; image colors (!d.table_size - 9)
          draw_window_size: [400L, 400L], $    ; size of main draw window
          aspect: 1.18, $                 ; aspect of image displayed
          mousemode: "Zoom", $           ; what action to take on mouse click? Zoom, FWHM, MoveTel, others?
          movetel_pos: [-1,-1], $        ; coordinates for offset
          bitdepth: 8, $                 ; 8 or 24 bit color mode?
          brightness: 0.5, $             ; initial brightness setting
          contrast: 0.5, $               ; initial contrast setting
          image_min: 0.0, $              ; min(main_image)
          image_max: 0.0, $              ; max(main_image)
          min_value: 0.0, $              ; min data value mapped to colors
          max_value: 30000.0, $          ; max data value mapped to colors
          median: 0.0, $                 ; djs_median(current region image)
          log_min: 0.0, $                ; min value for log scale
          log_max: 30000.0, $            ; max value for log scale
          sigma: 1.0, $                  ; How many sigma for Sig Disp
          scaling: 0L, $                 ; 0=linear, 1=log, 2=histeq, 3=sigdisp, 5 = fullrange
          scaleparam: 1.0,$              ; any parameters for the scaling routines
          cstretch: 0B, $                ; flag = 1 while stretching colors
          nframes: 1,$                   ; number of images in the stack
          frame: 0, $                    ; which frame is current?
          win:0,$                        ; drawable window ID
          widgetbase: 0L,$
          box: findgen(2,2), $           ; display window coords
          windowed: 0, $                 ; 0 if full frame, 1 if windowed
          startx: 0, $                   ; start x pixels in detector mode, for windowing
          starty: 0, $                   ; start y pixels in detector mode, for windowing
          endx: 2047, $                  ; end x pixels in detector mode, for windowing
          endy: 2047, $                  ; end y pixels in detector mode, for windowing
          angle: 0., $                   ; angle east of north in degrees!
          compass: 0, $                  ; show compass rose = 0, hide=1
          platescale: 0.033, $
          platescale_x: 0.033, $
          platescale_y: 0.033, $
          rel: 0.8, $                    ; relative fraction of display window to use
          color_r: fltarr(256),$
          color_b: fltarr(256),$
          color_g:fltarr(256), $         ; color table arrays
          position: [0.125,0.075,0.99,0.94], $
          axes: 1, $                     ; 0=NoAxes, 1=Arcsec(sky),2=Pixels(sky),3=Pixels(detector)
          name_label: 0L, $              ; widget ID
          strehl_label: 0L,$             ; widget ID
          object_label: 0L,$             ; widget ID
          headinfo_base_id: 0L,$         ; widget ID
          badpixfile: "/usr/local/lick/data/sharcs/sharcsdisplay/badpix_g9_slopes.fits", $ ; badpix filename
          ends: 0B $
     }
imghdr=strarr(5)

end
;==========================================================
pro sharcsdisp_help

end

;==========================================================
pro sharcsdisp_startup

  common sharcsdisp_common,state,field
  sharcsdisp_initcommon

  defsysv, "!image", exists = exists
  if (exists eq 0) then imagelib

  defsysv, "!aspect", exists=exists
  if (exists eq 0) then devicelib

  if (!d.n_colors LE 256) then begin
     state.bitdepth = 8
  endif else begin
     state.bitdepth = 24
     device, decomposed=0
     device,retain=2
  endelse
     
  badpix = readfits(state.badpixfile)

  sharcsdisp_savect

  sharcsdisp_base = widget_base(group_leader=Group, /base_align_left, $
      /column, /map, app_mbar = top_menu, /tlb_size_events, $
      title=title,uvalue={name:'sharcsdisp_base'},$
      xoffset=900,yoffset=30)

  state.widgetbase = sharcsdisp_base

  tmp   = { cw_pdmenu_s, flags:0, name:'' }
  top_menu_desc = [ $
                  {cw_pdmenu_s, 1, 'File'}, $         ; file menu
                  {cw_pdmenu_s, 0, 'Show Header'}, $
                  {cw_pdmenu_s, 2, 'ReadFits'}, $
                  {cw_pdmenu_s, 1, 'ColorMap'}, $     ; color menu
                  {cw_pdmenu_s, 0, 'Default'}, $
                  {cw_pdmenu_s, 0, 'Grayscale'}, $
                  {cw_pdmenu_s, 0, 'Stern Special'}, $
                  {cw_pdmenu_s, 0, 'Red Temperature'}, $
                  ;{cw_pdmenu_s, 0, 'Unstretch'}, $
                  {cw_pdmenu_s, 2, 'XLoadCT'}, $
                  ;{cw_pdmenu_s, 0, 'Save Current'}, $
                  ;{cw_pdmenu_s, 2, 'Invert'}, $
                  {cw_pdmenu_s, 1, 'Axes'}, $      ; axes menu
                  {cw_pdmenu_s, 0, 'No Axes'}, $
                  {cw_pdmenu_s, 0, 'Sky Arcseconds'}, $
                  {cw_pdmenu_s, 0, 'Sky Pixels'}, $
                  {cw_pdmenu_s, 0, 'Detector Pixels'}, $
                  {cw_pdmenu_s, 4, 'Show Compass'}, $
                  {cw_pdmenu_s, 2, 'Hide Compass'}, $
                  {cw_pdmenu_s, 1, 'ColorScaling'}, $      ; scaling menu
                  {cw_pdmenu_s, 0, 'Full Range'}, $
                  {cw_pdmenu_s, 0, 'Min Max'}, $
                  {cw_pdmenu_s, 0, 'Log'}, $
                  {cw_pdmenu_s, 0, 'SigDisp'}, $
                  {cw_pdmenu_s, 0, 'SigDisp (zoomed region only)'}, $
                  {cw_pdmenu_s, 2, 'HistEq'}, $
                  {cw_pdmenu_s, 1, 'Windows'}, $
                  {cw_pdmenu_s, 0, 'VNCview'}, $
                  {cw_pdmenu_s, 2, 'SquareView'}, $
                  {cw_pdmenu_s, 1, 'Help'}, $
                  {cw_pdmenu_s, 2, 'Help'} $
                ]

  top_menu = cw_pdmenu(top_menu, top_menu_desc, $
                      ids = menu_ids, $
                      /mbar, $
                      /return_id, $
                      uvalue = 'top_menu')

  sharcsdisp_row_base = widget_base(sharcsdisp_base,$
            /row,/base_align_center,/ALIGN_left)
  state.name_label = widget_label( sharcsdisp_row_base, $
      uvalue='name_label',/dynamic_resize, $
      value="test image")
  space_label = widget_label( sharcsdisp_row_base, $
      uvalue='space_label',/dynamic_resize, $
      value="    ")
  state.strehl_label = widget_label( sharcsdisp_row_base, $
      uvalue='strehl_label',/dynamic_resize, $
      value='       ')
  space_label = widget_label( sharcsdisp_row_base, $
      uvalue='space2_label',/dynamic_resize, $
      value="    ")
  state.object_label = widget_label( sharcsdisp_row_base, $
      uvalue='object_label',/dynamic_resize, $
      value='       ')


  base1 = widget_base( sharcsdisp_base, row=1, map=1, uvalue='base1' )

  if state.nframes gt 1 then begin
    frame_slider = widget_slider( base1,maximum = nframes-1, scroll = 1, $
           ysize = 300, value = 0, title='frame', /vertical, uvalue = 'frame_slider')
  endif else begin
    frame_slider = 0
  endelse

  dw = state.draw_window_size

  draw_window = widget_draw( base1, $
        /motion_events, /button_events, /retain, $
        uvalue='draw_window',$
       ; uvalue={name:'draw_window', data:data, box:[0,s(1)-1,0,s(2)-1], $
       ;         nframes: nframes, frame: frame, wsize:wsize}, $
        /no_copy, xsize=state.draw_window_size[0], ysize=state.draw_window_size[1])

  ;slide_base = widget_base(sharcsdisp_base, /row, /map, uvalue='slide_base')
  value_display = widget_label(sharcsdisp_base,frame=1,/dynamic_resize, $
      value='('+string(0)+','+string(0)+','+string(0)+')  '+$
      'Min='+string(0)+' Max='+string(0) )

  baseButtons = widget_base(sharcsdisp_base,/row,/map,uvalue='baseButtons')

  radio_buttons = [ 'Recenter ', 'Zoom   ', 'Unzoom ']

  radio_button_group = cw_bgroup( baseButtons, radio_buttons, $
      /row, /exclusive, set_value = 1, $
      uvalue='radio_button_group')

  revert_button = widget_button( baseButtons, $
      uvalue = 'revert_button', value = 'FullImage')

  widget_control, sharcsdisp_base, /realize

  field = {base: sharcsdisp_base, $
           frameslide: frame_slider, $
           draw: draw_window, $
           pad: [0,0], $
           valuedisplay: value_display, $
           ;minslide: min_slider, $
           ;maxslide: max_slider, $
           buttongroup: radio_button_group}

  basegeom = widget_info(field.base, /geometry)
  drawgeom = widget_info(field.draw, /geometry)
;  print,'basegeom.size ',basegeom.xsize,basegeom.ysize
  field.pad = [(basegeom.xsize-drawgeom.xsize),(basegeom.ysize-drawgeom.ysize)+33]

  ;widget_control, sharcsdisp_base, get_uvalue = uv
  ;uv = {name:uv.name, field:field}
  ;widget_control, sharcsdisp_base, set_uvalue = uv

  ; Get drawable window index
  widget_control, draw_window, get_value=draw_window_id
;  state.draw_window_id = draw_window_id

  if n_elements(name) ne 0 then $
     widget_control, state.name_label, set_value = name
  widget_control, field.draw, get_value = win
  state.win=win
  print, "Draw window is ",win

  print," IDL: created sharcsdisp2 widgets"
 

end




;==========================================================
pro sharcsdisp_savect
; saves the current color table to the state structure so
; that it will be used when you adjust brightness and contrast

common sharcsdisp_common,state,field


tvlct, r, g, b,  /get
state.color_r = r
state.color_g = g
state.color_b = b

end




;==========================================================
pro sharcsdisp_stretchct, brightness, contrast,  getmouse = getmouse,default=default

; shamelessly stolen from ATV.
; routine to change color stretch for given values of 
; brightness and contrast.
; Complete rewrite 2000-Sep-21 - Doug Finkbeiner
; This routine is now shorter and easier to understand.  

common sharcsdisp_common,state,field

if keyword_set(default) then  begin
     ;display the unstretched version
     tvlct, state.color_r, state.color_g, state.color_b
     return
endif


; if GETMOUSE then assume mouse position passed; otherwise ignore
; inputs

if (keyword_set(getmouse)) then begin
   state.brightness = brightness/float(state.draw_window_size[0])
   state.contrast = contrast/float(state.draw_window_size[1])
endif

x = state.brightness*(state.ncolors-1)
y = state.contrast*(state.ncolors-1) > 2   ; Minor change by AJB 
high = x+y & low = x-y
diff = (high-low) > 1

slope = float(state.ncolors-1)/diff ;Scale to range of 0 : nc-1
intercept = -slope*low
p = long(findgen(state.ncolors)*slope+intercept)<(n_elements( state.color_r)-1) ;subscripts to select
p[0]=0 ; force bottom color to always be black.
tvlct, state.color_r[p], state.color_g[p], state.color_b[p]


end




;==========================================================
pro sharcsdisp_settitle
    common sharcsdisp_common,state,field

     titles = ["Min Max","Log","Hist Eq","Sigma Disp (zoomed region)","Sigma Disp","Full Range"]

     widget_control, state.widgetbase,tlb_set_title="sharcsdisp / "+titles[state.scaling]
     
end


;==========================================================
PRO sharcsdisp_setup_coords
     common sharcsdisp_common,state,field
     common sharcs_comm
     ; establish data coordinates
     IF state.axes LE 2 THEN BEGIN
         data = rotate(img,6)
         sz = size(data)
         x0 = sz[1]-state.box[3]-1
         x1 = sz[1]-state.box[2]-1
         y0 = sz[2]-state.box[1]-1
         y1 = sz[2]-state.box[0]-1
     ENDIF ELSE BEGIN
         x0 = state.box[0]
         x1 = state.box[1]
         y0 = state.box[2]
         y1 = state.box[3]
     ENDELSE
     setup_coords,xrange=[x0,x1],yrange=[y0,y1], $
           pos=[state.position[0]/state.aspect, state.position[1],$
           state.position[2]/state.aspect, state.position[3] ]

end



;==========================================================
pro sharcsdisp_redraw,noerase=noerase,image2=image2

     common sharcsdisp_common,state,field
     common sharcs_comm
     widget_control, field.draw, get_value = win
     tvdevice, win
     wset, win


     erase = ~(keyword_set(noerase))
     ;widget_control,field.minslide, get_value = mind
     ;widget_control,field.maxslide, get_value = maxd
     frame = 0
     if field.frameslide ne 0 then $
         widget_control,field.frameslide, get_value = frame
     IF state.axes LE 2 THEN BEGIN
         data = rotate(img,6) 
         sz = size(data)
         x0 = sz[1]-state.box[3]-1
         x1 = sz[1]-state.box[2]-1
         y0 = sz[2]-state.box[1]-1
         y1 = sz[2]-state.box[0]-1
     ENDIF ELSE BEGIN
         data=img
         x0 = state.box[0]
         x1 = state.box[1]
         y0 = state.box[2]
         y1 = state.box[3]
     ENDELSE
;print,state.box
;help,data,x0,x1,y0,y1
     ;;; fullframe is windowed=0, currently can't handled windowed (=1)
     IF state.windowed EQ 0 THEN dispdata=data[x0:x1,y0:y1] ELSE $
     dispdata = data[x0:x1,y0:y1,state.frame]

     ; let the user override the display image briefly, if desired
     if keyword_set(image2) then dispdata = image2
     ; establish data coordinates
     setup_coords,xrange=[x0,x1],yrange=[y0,y1],$
       pos=[state.position[0]/state.aspect, state.position[1],$
            state.position[2]/state.aspect, state.position[3] ]
     
     case state.scaling of
       0: begin; linear
           ;exptv, (dispdata > state.min_value) < state.max_value, /noexact, $
           ;    /data, relative=state.rel
          imdisp,dispdata, range=[(state.min_value),(state.max_value)],$
             erase=erase, aspect=state.aspect,$
             pos=[state.position[0]/state.aspect, state.position[1],$
             state.position[2]/state.aspect, state.position[3] ]
       end
       1: begin; log
          imdisp,alogscale(dispdata,state.log_min,state.log_max),$
             erase=erase, aspect=state.aspect,$
             pos=[state.position[0]/state.aspect, state.position[1],$
             state.position[2]/state.aspect, state.position[3] ]
          empty
       end
       2: begin; histeq
          print, "****** hist eq not implemented yet, sorry! *******"
       end
       3: begin; sigdisp of whole image
          sig=sigma(data)
          av=avg(data)
          ;print,av,sig,state.sigma,format='("Average = ",f8.1,"     "+$
          ; "Std Dev = ",f8.2,"    # Std Devs = ",f8.2)'
          ;exptv,dispdata
          print,"sigdisp: ",av,sig,state.sigma,av-sig*state.sigma
          imdisp,dispdata,range=[(av-sig*state.sigma),(av+sig*state.sigma)],$
             erase=erase, aspect=state.aspect,$
             pos=[state.position[0]/state.aspect, state.position[1],$
             state.position[2]/state.aspect, state.position[3] ]
          ;;exptv,dispdata,/data,/noexact,min=av-state.scaleparam*sig,$
          ;;  max=av+state.scaleparam*sig,relative=state.rel
       end
       4: begin; sigdisp of local region
          sig=sigma(dispdata)
          av=avg(dispdata)
          ;print,av,sig,state.sigma,format='("Average = ",f8.1,"     "+$
          ;   "Std Dev = ",f8.2,"    # Std Devs = ",f8.2)'
          imdisp,dispdata,range=[(av-sig*sig),(av+sig*sig)],$
             erase=erase, aspect=state.aspect,$
             pos=[state.position[0]/state.aspect, state.position[1],$
             state.position[2]/state.aspect, state.position[3] ]
          ;exptv,dispdata
          ;exptv,dispdata,/data,/noexact,min=av-state.scaleparam*sig,$
          ;   max=av+state.scaleparam*sig,relative=state.rel
       end
       5: begin; fullrange
          imdisp,dispdata,erase=erase, aspect=state.aspect,$
             pos=[state.position[0]/state.aspect, state.position[1],$
             state.position[2]/state.aspect, state.position[3] ]
       end
     endcase
     ; draw the axes
     ;print,state.box

     if ~(keyword_set(noerase)) then begin

          if state.axes eq 1 then  begin
              ; arcseconds
              sz = size(data)
              plot,[0],/nodata,/noerase, /xs, /ys, $
                xrange=([x0,x1]-sz[1]/2)*state.platescale,$
                yrange=([y0,y1]-sz[2]/2)*state.platescale,$
                xtit='Sky Normal X (arcsec)',ytit='Sky Normal Y (arcsec)',$
                pos=[state.position[0]/state.aspect, state.position[1],$
                state.position[2]/state.aspect, state.position[3] ]

              ; do this again because we just trashed the coords with arcseconds.
              setup_coords,xrange=[x0,x1],yrange=[y0,y1],$
                  pos=[state.position[0]/state.aspect, state.position[1],$
                  state.position[2]/state.aspect, state.position[3] ]

               IF state.compass EQ 0 THEN BEGIN
                   devicecoords = [state.draw_window_size[0]*state.position[0],$
                                   state.draw_window_size[1]*state.position[1],$
                                   state.draw_window_size[0]*state.position[2],$
                                   state.draw_window_size[1]*state.position[3]]
                   oplotcompass,devicecoords,0,rotang=state.angle,factor=0.8
               ENDIF

          endif
          if state.axes eq 2 then begin
               ; pixels in sky normal coordinates
               ;stop
               ;plot,[0],/nodata,/noerase, /xs,/ys, $
               ;   xrange=[x0,x1], yrange=[y0,y1],$
               ;   xtit='Sky Normal X (pixels)',ytit='Sky Normal Y (pixels)',$
               ;   pos=[state.position[0]/state.aspect, state.position[1],$
               ;   state.position[2]/state.aspect, state.position[3] ]
               sz = size(data)
               plot,[0],/nodata,/noerase, xs=5,ys=5, $
                  xrange=[x0,x1], yrange=[y0,y1],$
                  xtit='Sky Normal X (pixels)',ytit='Sky Normal Y (pixels)',$
                  pos=[state.position[0]/state.aspect, state.position[1],$
                  state.position[2]/state.aspect, state.position[3] ]
               axis,xaxis=0,xrange=[x0,x1],/xs,xtit='Sky Normal X (pixels)'
               axis,yaxis=0,yrange=[y0,y1],/ys,ytit='Sky Normal Y (pixels)'
               axis,xaxis=1,xrange=state.starty+[state.box[3],state.box[2]],/xs,xtit='Real Detector Y (pixels)'
               axis,yaxis=1,yrange=state.startx+[state.box[1],state.box[0]],/ys,ytit='Real Detector X (pixels)'

               IF state.compass EQ 0 THEN BEGIN
                   devicecoords = [state.draw_window_size[0]*state.position[0],$
                                   state.draw_window_size[1]*state.position[1],$
                                   state.draw_window_size[0]*state.position[2],$
                                   state.draw_window_size[1]*state.position[3]]
                   oplotcompass,devicecoords,0,rotang=state.angle,factor=0.8
               ENDIF

          endif
          if state.axes eq 3 then  begin
               ; pixels in detector normal coordinates
               ;stop
               plot,[0],/nodata,/noerase, xs=5,ys=5, $
                  xrange=[x0,x1], yrange=[y0,y1], $
                  xtit='Detector X (pixels)',ytit='Detector Y (pixels)', $
                  pos=[state.position[0]/state.aspect, state.position[1],$
                  state.position[2]/state.aspect, state.position[3] ]
               axis,xaxis=0,xrange=[x0,y1],/xs,xtit='Detector X (pixels)'
               axis,yaxis=0,yrange=[y0,y1],/ys,ytit='Detector Y (pixels)'
               axis,xaxis=1,xrange=state.startx+[x0,y1],/xs,xtit='Real Detector X (pixels)'
               axis,yaxis=1,yrange=state.starty+[y0,y1],/ys,ytit='Real Detector Y (pixels)'

          endif
     endif
 
     ;axis,xrange=[x0,x1],xaxis=0,xtitle="pixels",charsize=1
     ;axis,xrange=[x0,x1]*state.platescale,xaxis=1,ytitle="arcsec",charsize=1
     ;axis,yrange=[y0,y1],yaxis=0,ytitle="pixels"
     ;axis,yrange=[y0,y1]*state.platescale,yaxis=1,ytitle="arcsec"

     ;tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
     ;tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]


end




;==========================================================
pro sharcsdisp_Event, Event

  common sharcsdisp_common,state,field
  common sharcs_comm

  widget_control, Event.Id, get_uvalue=Ev
  t = size(ev)
  ;rmcgurk
  ;if (t(0) eq 0) then evname = ev else evname = ev.name
  if (t[0] eq 0) then evname = ev else evname = ev.name
  ;widget_control, event.top, get_uvalue = uv
  ;field = uv.field
  widget_control, field.draw, get_value = win
  tvdevice, win
  ;widget_control, field.draw, get_uvalue = uval


  case evname of

  'sharcsdisp_base': begin   ; resize event
     ;print,'requested window size: ',event.x,event.y
     s = size(data)
     ;rmcgurk
     ;n = s(1)
     ;m = s(2)
     n = s[1]
     m = s[2]
     aspect = float(n)/float(m)
     minsize = [310, 356]
     newsize = (minsize > [event.x, event.y]) - field.pad
     ;newsize = (minsize > [event.x, event.y]) 
     ; force the window to be a square
     thesize =  newsize[1]>newsize[0]
     newsize = [thesize,thesize]
     widget_control, field.draw, $
        xsize=newsize[0], ysize=newsize[1]
     sharcsdisp_redraw
  end

  'top_menu': begin
     widget_control, event.value, get_value = event_name
     parent = widget_info(event.value, /parent)
     widget_control, parent, get_value = parent_name

     case parent_name of
       'File': begin
           case event_name of
             'Quit': begin
                 ;widget_control,event.top,/destroy
                 print, "NO! Quit this from the Tcl/TK GUI instead!"
             end
             'Show Header' : begin
                 sharcsdisp_headinfo
             end
             'ReadFits': begin
                 print,"Now querying for which file to read"
                 fitsfile="/data/LabData/raw140227/m140227_003.fits" 
                 ;; rmcgurk changed
                 ;fitsfile="/data/2005.02.08/sharcs0002.fits"

                 fitsfile = dialog_pickfile(filter = '*.fits*', $
                      group = state.widgetbase, $
                      /must_exist, $
                      /read, $
                      path = "/data/sharcs", $
                      ;path = "/data/today", $ ;; rmcgurk changed
                      title = 'Select Fits Image')
                 print,"Got filename: ",fitsfile

                 
                 img = readsharcs(0,path=path, framename=fitsfile,badpixfix=0) ; badpix off for now...

                 ;img =  readfits(fitsfile, hdr)
                 ;imghdr = hdr

                 ;;; hybrid mode
                 ;if (sxpar(hdr, 'READMODE') eq 4) then begin
                 ;    print, 'reading hybrid data...'
                 ;    ;; load target region dat
                 ;    hybdata = readfits(fitsfile, /exten)
                 ;    ;; compute representative images for target regions
                 ;    sz = size(hybdata)
                 ;    tim = reform(hybdata[*, *, *, sz[4]-1, 0]-hybdata[*, *, *, 0, 0])
                 ;    ;; store target region data into img array
                 ;    ;; FIXME  check this mapping
                 ;    img[*, 0:sz[2]-1] = hybdata[*, *, 0]
                 ;    img[*, 128:128+sz[2]-1] = hybdata[*, *, 1]
                 ;endif

                 sharcsdisp_redraw 
             end
             else: begin
                 Print, event_name, " not yet implemented!!! "
             end
           endcase
       end
       'ColorMap': begin
           case event_name of
            'Default': begin
               loadct,5
               sharcsdisp_savect
            end
            'Grayscale': begin
               loadct,0
               sharcsdisp_savect
            end
            'Stern Special': begin
               loadct,15
               sharcsdisp_savect
            end
            'Red Temperature': begin
               loadct,3
               sharcsdisp_savect
            end
            'Unstretch': begin
               sharcsdisp_stretchct,/default
            end
            'XLoadCT': begin
               xloadct,/block
               sharcsdisp_savect
            end
            'Save Current': begin
               sharcsdisp_savect
            end
            'Invert': begin
               empty
            end
           endcase
           sharcsdisp_redraw 
       end
       'Axes': begin
           case event_name of
            'No Axes': state.axes=0
            'Sky Arcseconds': state.axes=1
            'Sky Pixels': state.axes=2
            'Detector Pixels': state.axes=3
            'Show Compass': state.compass=0
            'Hide Compass': state.compass=1
           endcase
           sharcsdisp_redraw 
       end
       'ColorScaling': begin
           case event_name of
            'Full Range': begin
               state.scaling =5
            end
            'Min Max': begin 
               state.scaling=0 
            end
            'Log':   begin
               state.scaling=1 
            end
            'SigDisp': begin
               state.scaling=3 ; use standard deviation of whole image 
            end
            'SigDisp (zoomed region only)': begin 
               state.scaling=4 ; use standard deviation of only the currently displayed bit
            end
            'HistEq': begin 
               print,"Not implemented yet!"
            end
           endcase
           sharcsdisp_redraw 
           sharcsdisp_settitle
       end
       'Windows': begin
           case event_name of
             'VNCview': begin
               state.aspect=1.18
               sharcsdisp_redraw
             end
             'SquareView': begin
               state.aspect=1.00
               sharcsdisp_redraw
             end
           endcase
       end

       else: begin
          Print, parent_name
       end
     endcase
  end ; end of Top menu options

  'frame_slider': begin
        ; Print, 'Event for frame_slider'
        frame = event.value
        ; Print, 'frame = ', frame
        ; widget_control, field.draw, get_uvalue = uval
        state.frame = frame
        sharcsdisp_redraw
  end
  'draw_window': begin
      if (event.type eq 2) then begin  ; mouse motion, print values
         if state.cstretch eq 1 then begin
            ; if we're currently stretching color, then 
            ; modify the color table and redisplay if needed
            sharcsdisp_stretchct, event.x, event.y, /getmouse
            if (state.bitdepth EQ 24) then sharcsdisp_redraw, /noerase
         endif else begin
            ; otherwise, 
            ; update the (x, y, value) display

            res = convert_coord(event.x,event.y,/device,/to_data)
            x = round(res[0])
            y = round(res[1])
            
            IF state.axes LE 2 THEN BEGIN
                data = rotate(img,6)
                sz = size(data)
                x0 = sz[1]-state.box[3]-1
                x1 = sz[1]-state.box[2]-1
                y0 = sz[2]-state.box[1]-1
                y1 = sz[2]-state.box[0]-1
            ENDIF ELSE BEGIN
                data=img
                sz = size(data)
                x0 = state.box[0]
                x1 = state.box[1]
                y0 = state.box[2]
                y1 = state.box[3]
            ENDELSE
            minish = min(data[x0:x1,y0:y1])
            maxish = max(data[x0:x1,y0:y1])

            if ((x ge 0) and (x lt sz[1]) and (y ge 0) and (y lt sz[2])) then begin
               
               z = data[x,y]
               if field.frameslide ne 0 then begin
                  widget_control,field.frameslide, get_value = frame
                  z = data[x,y,frame]
               endif
               txtstr = '(' + strn(x,length=6,padtype=1,padchar=' ') + ',' +$
                 strn(y,length=6,padtype=1,padchar=' ') + ',' + string(z) +')'+$
                 '  Min= '+strn(minish,length=8,padtype=0,padchar=' ')+$
                 '  Max= '+strn(maxish,length=8,padtype=0,padchar=' ')

               widget_control, field.valuedisplay, set_value = txtstr
            endif
         endelse
      endif ; end of motion events

      if (event.press gt 0) then begin
         ;print,"Press:",event.press
         if (event.press eq 1b) or (event.press eq 3b) then begin ; that's a 1-bit byte mask which selects the Left button
            ; figure out where the user clicked, in IMAGE COORDINATES
            res = convert_coord(event.x,event.y,/device,/to_data)
            x = round(res[0])
            y = round(res[1])
            sz = size(img)
            
            IF state.axes LE 2 THEN BEGIN
                holdx = sz[1]-y-1
                holdy = sz[2]-x-1
                x = holdx
                y = holdy
            ENDIF 
 
            ; if the user has clicked outside the image box, then do
            ; nothing.
            if ~((x ge 0) and (x lt sz[1]) and (y ge 0) and (y lt sz[2])) then begin
               print,'your cursor click and your image size do not agree... returning!'
               return
            endif
    
            case state.mousemode of
               "Zoom": begin
                    print,"zooming"
                    ;   determine what to do!
                    if event.press eq 1b then begin
                        buttongroup = field.buttongroup
                        widget_control, buttongroup, get_value = bindex
                    endif else if event.press eq 2b then begin
                        bindex=2 ; right button only unzooms
                    endif
                    
                    rangex = state.box[1] - state.box[0] + 1
                    rangey = state.box[3] - state.box[2] + 1
                    aspect = float(rangey) / float(rangex)
              
                    case bindex of
                      0: begin    ; Pan
                         rangex = rangex
                         rangey = rangey
                      end
                      1: begin    ; Zoom
                         rangex = round(rangex/2) > 2
                         rangey = round(rangey/2) > 2
                      end
                      2: begin    ; Unzoom
                         rangex = round(rangex*2) < sz[1]
                         rangey = round(rangey*2) < sz[2]
                      end
                    endcase 

                    x0 = (x - round(rangex/2)) > 0
                    x1 = x0 + rangex - 1
                    if x1 gt sz[1]-1 then begin
                        x1 = sz[1]-1
                        x0 = (x1 - rangex + 1) > 0
                    endif
                    ;rangey = round(aspect*rangex) < sz[2]
                    y0 = (y - round(rangey/2)) > 0
                    y1 = y0 + rangey - 1
                    if y1 gt sz[2]-1 then begin
                         y1 = sz[2]-1
                         y0 = (y1 - rangey + 1) > 0
                    endif
                    state.box = [x0,x1,y0,y1]
                    state.median = djs_median(img[state.box[0]:$
                       state.box[1],state.box[2]:state.box[3]])

                    sharcsdisp_redraw

               end
               "FWHM": begin
                  print,"Position: ",x,y
               end
               "MoveTel": begin
                  if state.movetel_pos[0] eq -1 then begin
                      ; save the first position
                      state.movetel_pos[0]=x
                      state.movetel_pos[1]=y
                      sharcsdisp_circle,x,y,10,220 ;0.85*!D.N_COLORS-1
                  endif else begin
                      ; compute the offset and print it
                      sharcsdisp_circle,x,y,10,220 ;0.85*!D.N_COLORS-1
                      dx = (x-state.movetel_pos[0])*state.platescale_x
                      dy = (state.movetel_pos[1]-y)*state.platescale_y
                      ; round to 0.01 arcsec
                      dx = 0.01*round(dx/0.01)
                      dy = 0.01*round(dy/0.01)
                      ;print,"% MoveTel Offset: "
                      print,dx,dy
                      ; now reset to normal mode.
                      state.movetel_pos = [-1,-1]
                      state.mousemode="Zoom"
                  endelse

               end
            endcase
                 
         endif ; for button 1 down

         if (event.press eq 2b) then begin
            ; mouse button 2 down puts us into color table stretching mode
            sharcsdisp_stretchct,event.x, event.y, /getmouse
            state.cstretch = 1
            ;print,"Middle down"
         endif ; for button 2 down
      endif ; for any press event

      if event.release gt 0 then begin
         if (event.release eq 2b) then begin
            ; mouse button 2 up takes us out of color table stretching mode.
            state.cstretch=0
            if (state.bitdepth EQ 24) then sharcsdisp_redraw
         endif
      endif ; for any release event
  end ; case for draw widget events
  'radio_button_group': begin
      case Event.Value of
        0: ;Print,'Button Pan       Pressed'
        1: ;Print,'Button Zoom      Pressed'
        2: ;Print,'Button Unzoom  Pressed'
        3: ;Print,'Button Values Pressed'
        else: Message,'Unknown button pressed'
      endcase
  end
  'revert_button': Begin
      ; Print, 'Event for Revert'
      widget_control, field.draw, get_value = windex
      sharcsdisp_fullimage
  end
  endcase

  return
end



;======================================================================
PRO sharcsdisp_fullimage,noredraw=noredraw
    common sharcsdisp_common,state,field
    common sharcs_comm

    s = size(img)
    state.box = [0,s[1]-1,0,s[2]-1]

    goodcount = 0 
    goodpix = where( badpix EQ 0, goodcount) + state.frame*s[1]*s[2]
    IF goodcount NE 0 THEN BEGIN
        state.min_value = min(img[goodpix])
        state.max_value = max(img[goodpix])
    ENDIF ELSE BEGIN
        state.min_value = min(img[*,*,state.frame])
        state.max_value = max(img[*,*,state.frame])
    ENDELSE
    ;sharcsdisp_stretchct,/default
    if not(keyword_set(noredraw)) then sharcsdisp_redraw

end


;======================================================================
PRO sharcsdisp_circle,x,y,r,color

  common sharcsdisp_common,state
                                ; This is really really easy now that
                                ; I have data coordinates set up!
; Note, this puts circles in the right place for Detector Pixel
; Display, but not for Sky Arcsecs or Sky Pixels.  Need to adjust for
; those cases: check state.axes and apply coordinate transform to x, y 
  if (state.axes eq 3) then tvcircle, r, x, y, /data,color=color else begin
     s=2047 ; size of detector
     x1=2047-y
     y1=2047-x
     tvcircle,r,x1,y1,/data,color=color
  endelse
end


;======================================================================

PRO sharcsdisp_moveTel,xmove,ymove
     
     common sharcsdisp_common

;    er = dialog_message( 'Click where you want to move FROM, then '+$
;          'click where you want to move it TO.')
;
     state.mousemode = "MoveTel"

     ;tvpos_sharcs,xfrom,yfrom,/silent
     ;sharcsdisp_circle,xfrom,yfrom,5,0.85*!D.N_COLORS-1
     ;tvpos_sharcs,xto,yto,/silent
     ;sharcsdisp_circle,xto,yto,5,0.85*!D.N_COLORS-1

;     xmove = (xto-xfrom)*state.platescale_x
;     ymove = (yto-yfrom)*state.platescale_y
;     print,xmove,ymove

end




;======================================================================
pro sharcsdisp_headinfo

    common sharcsdisp_common

    ; If there's no header, kill the headinfo window and exit this
    ; routine.
    if (not(ptr_valid(state.head_ptr))) then begin
        if (xregistered('sharcsdisp_headinfo')) then begin
            widget_control, state.headinfo_base_id, /destroy
        endif

        er = dialog_message( 'No header information available for '+$
          'this image!', /error)
        return
    endif

    ; If the user asks to redisplay the header info, and the header window
    ; is already up, then kill the old one so we can create the new.
    if (xregistered('sharcsdisp_headinfo')) then begin
        widget_control, state.headinfo_base_id, /destroy
    endif


    ; If there is header information but not headinfo window,
    ; create the headinfo window.
    if (not(xregistered('sharcsdisp_headinfo', /noshow))) then begin

       headinfo_base = widget_base(/base_align_right, $
                  group_leader = state.widgetbase, $
                  /column, $
                  title = 'image header for'+state.imagename, $
                  uvalue = 'headinfo_base')
       state.headinfo_base_id = headinfo_base

       h = *(state.head_ptr)

       headinfo_text = widget_text(headinfo_base, $
                            /scroll, $
                            value = h, $
                            xsize = 85, $
                            ysize = 24)

       headinfo_done = widget_button(headinfo_base, $
                              value = 'Done', $
                              uvalue = 'headinfo_done')

       widget_control, headinfo_base, /realize
       xmanager, 'sharcsdisp_headinfo', headinfo_base, /no_block

    endif


end




;---------------------------------------------------------------------

pro sharcsdisp_headinfo_event, event

  common sharcsdisp_common

  widget_control, event.id, get_uvalue = uvalue

  case uvalue of
     'headinfo_done': widget_control, event.top, /destroy
     else:
  endcase

end





;======================================================================
pro sharcsdisp2, dataarg, name, group=Group, size = wsize,$
     scaleparms=scaleparms,linear=linear,sigdisp=sigdisp,$
     lsigdisp=lsigdisp,log=log

     common sharcsdisp_common,state,field
     common sharcs_comm

     ;print,"scaleparms is",scaleparms

     if (!d.name NE 'X' AND !d.name NE 'WIN' AND !d.name NE 'MAC') then begin
        print, 'Graphics device must be set to X, WIN, or MAC for sharcsdisp to work.'
        retall
     endif

     if (not (xregistered('sharcsdisp', /noshow))) then begin
        userwindow = !d.window
        print,"calling startup"
        sharcsdisp_startup
     endif
 
     Print, 'sharcsdisp -- v0.11 experimental'

     ;defsysv, "!image", exists = exists
     ;if (exists eq 0) then imagelib
     ;defsysv, "!aspect", exists=exists
     ;if (exists eq 0) then devicelib

     if n_elements(Group) eq 0 then group=0
     if n_elements(wsize) eq 0 then wsize = 285

     ;junk   = { cw_pdmenu_s, flags:0, name:'' }

     if n_elements(name) eq 0 then begin
       state.title = "sharcsdisp"
     endif else begin
       state.title = name
     endelse

     goodcount=0
     goodpix = where(badpix EQ 0, goodcount)

     state.scaling=0
     IF goodcount NE 0 THEN BEGIN 
         state.min_value = min(img[goodpix])
         state.max_value = max(img[goodpix])
     ENDIF ELSE BEGIN
         state.min_value = min(img)
         state.max_value = max(img)
     ENDELSE

     if keyword_set(linear) then begin 
        state.scaling=0
        state.title="sharcsdisp / Linear"
     endif
     if keyword_set(log) then begin 
        state.scaling=1
        state.title="sharcsdisp / Log"
     endif
     if keyword_set(histeq) then state.scaling=2
     if keyword_set(sigdisp) then begin 
        state.scaling=3
        state.scaleparam=sigdisp
     endif
     if keyword_set(lsigdisp) then begin 
        state.scaling=4
        state.scaleparam=lsigdisp
     endif

     if state.scaling eq 0 then begin
        if keyword_set(scaleparms) then begin
           print, "setting minmax"
           state.min_value = scaleparms[0]
           state.max_value = scaleparms[1]
        endif ;else begin  ; commented out because this is set above
        ;   state.min_value = min(img)
        ;   state.max_value = max(img)
        ;endelse

        if state.min_value eq state.max_value then state.max_value = state.min_value+1
     endif

     s = size(img)

     ;rmcgurk
     ;if s(0) eq 3 then state.nframes = s(3) else state.nframes = 0
     if s[0] eq 3 then state.nframes = s[3] else state.nframes = 0
     frame = 0

     state.box = [0,s[1]-1,0,s[2]-1]

     ;min_slider = cw_fslider( slide_base,maximum = maxd, minimum = mind, $
           ;edit = 1, $
           ;value = mind, title='min', uvalue = 'min_slider')

     ;max_slider = cw_fslider( slide_base,maximum = maxd, minimum = mind, $
           ;edit = 1, $
           ;value = maxd, title='max', uvalue = 'max_slider')

     tvdevice, state.win

     ;mind = min((*state.data)[*,*,0])
     ;maxd = max((*data)[*,*,0])
     ;widget_control,field.minslide, set_value = mind
     ;widget_control,field.maxslide, set_value = maxd
     sharcsdisp_savect
     sharcsdisp_redraw
     sharcsdisp_settitle

     if (not (xregistered('sharcsdisp', /noshow))) then begin
       xmanager, 'sharcsdisp', state.widgetbase, /no_block
     endif

end
