pro fullrange,image
    ; this is the full-range scaling for sharcsdisp2
    
    ;COMMON SHARCS_COMM, imagetitle
    common sharcsdisp_common,state,field
    state.scaling = 5
    sharcsdisp_settitle

    IF state.windowed EQ 0 THEN sharcsdisp_redraw ELSE sharcsdisp_fullimage
    ;; if full image, just redraw;   if windowed, redo coords

end
