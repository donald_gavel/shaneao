pro logscale,image,imin,imax
    ; this is the full-range scaling for sharcsdisp2
    
    ;COMMON SHARCS_COMM, imagetitle
    common sharcsdisp_common,state,field
    state.scaling = 1
    state.log_min = imin
    state.log_max = imax

    sharcsdisp_settitle
    IF state.windowed EQ 0 THEN sharcsdisp_redraw ELSE sharcsdisp_fullimage
    ;; if full image, just redraw;   if windowed, redo coords

end
