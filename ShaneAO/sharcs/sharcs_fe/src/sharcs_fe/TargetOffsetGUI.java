package sharcs_fe;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.TreeMap;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.uco.gui_bits.ButtonMenu;
import org.uco.gui_bits.EasyButton;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.LogWind;
import org.uco.gui_bits.OffsetGrid;
import org.uco.gui_bits.TextFieldDual;
import org.uco.ktljxml.KTLClientT;

import org.uco.java_util.CLog;
import org.uco.java_util.GKey;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyDouble;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GKeyString;
import org.uco.java_util.GTag;
import org.uco.java_util.KTLKeyword;
import org.uco.java_util.NKMsg;
import org.uco.java_util.StaticUtil;



public class TargetOffsetGUI extends FrameCloseCaptured implements GKeyConnection {
    
	TargetOffsetFECentral     m_Central;    // Our central thread.
    KTLClientT                m_ktlclient;  // CLient thread communicating with sdiagmir server.
    
    boolean m_GUIBuilt = false; // This will only be set to true once the GUI has been completed.
    
    GTag m_tag = new GTag(TargetOffsetGUI.class.getName());
    
    // The following map allows the GUI to quickly locate which GUI objects should be notified
    // when a keyword value is changed.
    @SuppressWarnings("rawtypes") // GKey needs to be generic
	TreeMap<String, GKey> m_GUIKeys = new TreeMap<String, GKey>();
    GKeyDouble m_gkwPOCRARCT;
    GKeyDouble m_gkwPOCDERET;
    GKeyInt    m_gkwPOCSETAT;
    
    
    TrigThrd  m_Trigger;
    
    LogWind        m_LogWind;
    
    final static int LOG_WIND    = 2;
    final static int RESIZE_FRM  = 3;
    
    // menu bar (menu item and status label)
    JPanel       m_jplMenuBar;
    ButtonMenu   m_jbtFunctions;
    JPopupMenu   m_popFunctions;
    
    // 
    JPanel      m_jplMain;  // Main panel
    
    // Offset items
    TextFieldDual   m_offsetRATf   = new TextFieldDual(9);
    TextFieldDual   m_offsetDecTf  = new TextFieldDual(9);
    OffsetSetButton m_offsetSetBtn = new OffsetSetButton();
    JButton         m_setTargToCur = new JButton("Set Target");
    
    JTextField      m_boxSizeTf    = new JTextField();
    JTextField      m_gridColsTf   = new JTextField();
    JTextField      m_gridRowsTf   = new JTextField();
    JButton         m_gridSetBtn   = new JButton("Set");
    TeleOffsetGrid  m_offsetGrid;   
    

    /**
     * Constructor.
     * @param central
     * @param ktlclient
     */
    TargetOffsetGUI(TargetOffsetFECentral central, KTLClientT ktlclient) {
        super("targetoffset_fe");
        m_Central   = central;
        m_ktlclient = ktlclient;

        @SuppressWarnings("rawtypes") LinkedList<GKey> gkl = new LinkedList<GKey>();
        m_gkwPOCRARCT = (GKeyDouble) m_ktlclient.getKeyBy("POCRARCT");
        m_gkwPOCDERET = (GKeyDouble) m_ktlclient.getKeyBy("POCDERET");
        for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
    		String name = gk.getName();
    		CLog.notice(this.getClass()+" adding GUI keyword "+name);
    		TogNKMsg tognkmsg = new TogNKMsg();
    		gk.putNotify(getTag(), tognkmsg);
    		m_GUIKeys.put(name, gk);
    	}
        
        m_gkwPOCSETAT = (GKeyInt) m_ktlclient.getKeyBy("POCSETAT");
        m_setTargToCur.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		m_gkwPOCSETAT.queWrite(1, null);
        	}
        });
        
        // Create (but do not show) the LogWind
        m_LogWind = new LogWind("targetOffset_fe", 20);
        GKeyString gkwPOCMSG  = (GKeyString) m_ktlclient.getKeyBy("POCMSG");
        GKeyString gkwPOCMSER = (GKeyString) m_ktlclient.getKeyBy("POCMSER");
        m_LogWind.addKeyword(gkwPOCMSG);
        m_LogWind.addKeywordPopup(gkwPOCMSER);
       
        
        // create the frame
        GridBagLayout      framegrid = new GridBagLayout();
        GridBagConstraints framegbc  = new GridBagConstraints();
        getContentPane().setLayout(framegrid);
        
        
        // Make the menu bar.
        // make the popup menu 
        m_popFunctions     = new JPopupMenu("Functions");

        JMenuItem itemLog  = new JMenuItem(new ActionCreateWindow("Log...", LOG_WIND, this));
        JMenuItem itemResz = new JMenuItem(new ActionCreateWindow("resize", RESIZE_FRM, this));
        
        m_popFunctions.add(itemLog);
        m_popFunctions.addSeparator();
        m_popFunctions.add(itemResz);
        
        // Make the button to show the functions popupmenu
        m_jbtFunctions = new ButtonMenu("Functions", m_popFunctions);
        //Border empty = BorderFactory.createEmptyBorder();
        //m_jbtFunctions.setBorder(empty);
        m_jbtFunctions.setBackground(null);
   
        
        // assemble the pieces of the menu bar
        m_jplMenuBar = new JPanel();
        // grid layout manager
        GridBagLayout menugrid     = new GridBagLayout();
        GridBagConstraints menugbc = new GridBagConstraints();
        m_jplMenuBar.setLayout(menugrid);
        m_jplMenuBar.setOpaque(true);
        menugbc.weightx = 0.0;
        menugbc.ipady   = 0;
        menugbc.anchor  = GridBagConstraints.WEST;
        menugbc.gridx = 0; menugbc.gridy = 0;
        menugrid.setConstraints(m_jbtFunctions, menugbc);
        menugbc.gridx  = 1;
        menugbc.gridx  = 2;
        menugbc.anchor = GridBagConstraints.EAST;
        //m_jplMenuBar.add(m_jbtFunctions);

        
        // Assemble the pieces of the XYStage pane.
        // Setup the main panel.
        m_jplMain = new JPanel();
        GridBagLayout maingrid     = new GridBagLayout();
        GridBagConstraints maingbc = new GridBagConstraints();
        m_jplMain.setLayout(maingrid);
        m_jplMain.setOpaque(true);
        
        maingbc.weightx = 100.0;
        maingbc.weighty = 100.0;
        maingbc.ipady   = 0;
        maingbc.anchor  = GridBagConstraints.WEST;
        maingbc.fill    = GridBagConstraints.BOTH;
        
        
        // Setup the OffsetGrid
        m_offsetGrid = new TeleOffsetGrid(); 
        maingbc.gridx = 10; maingbc.gridy = 10;
        maingbc.gridwidth = 10;
        maingbc.gridheight = GridBagConstraints.REMAINDER;
        m_jplMain.add(m_offsetGrid, maingbc);
        
        // Setup the offset panel
        JPanel offsets = new JPanel();
        maingbc.gridx = 0;
        maingbc.gridy = 9;
        maingbc.gridwidth = GridBagConstraints.REMAINDER;
        maingbc.gridheight = 1;
        maingbc.weighty = 0.0;
        m_jplMain.add(offsets, maingbc);
        
        offsets.setLayout(new GridBagLayout());
        GridBagConstraints offsets_gbc = new GridBagConstraints();
        GridBagConstraints gbc = offsets_gbc;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridy = 0;
        gbc.gridx = 0;
        offsets.add(new JLabel("Offset RA="), gbc);
        gbc.gridx++;
        gbc.weightx = 100.0;
        offsets.add(m_offsetRATf, gbc);
        gbc.gridx++;
        gbc.weightx = 0.0;
        offsets.add(new JLabel("  Dec="), gbc);
        gbc.gridx++;
        gbc.weightx = 100.0;
        offsets.add(m_offsetDecTf, gbc);
        gbc.gridx++;
        gbc.weightx = 0.0;
        offsets.add(m_offsetSetBtn, gbc);
        gbc.gridx++;
        offsets.add(m_setTargToCur, gbc);
        
        // Grid info panel
        JPanel gridInfo = new JPanel();
        maingbc.gridx = 50; maingbc.gridy = 10;
        maingbc.gridwidth = 1;
        maingbc.gridheight = GridBagConstraints.REMAINDER;
        maingbc.weightx = 0.0;
        m_jplMain.add(gridInfo, maingbc);
        
        gridInfo.setLayout(new GridBagLayout());
        GridBagConstraints gridInfo_gbc = new GridBagConstraints();
        gbc = gridInfo_gbc;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.weightx = 0.0;
        gbc.weighty = 0.0;
        gbc.gridy = 0;
        gbc.gridx = 0;
        
        {
        	int col_lb = 0;
        	int col_tf = 1;
        	int col_bt = 0;
        	double weight_lb = 0.0;
        	double weight_tf = 10.0;

        	updateTextFields();       	
        	
        	gbc.gridx = col_lb;  gbc.weightx = weight_lb;
        	gbc.gridwidth = 2;
        	gridInfo.add(new JLabel("Box size"), gbc);
        	gbc.gridwidth = 1;
        	gbc.gridy++;
        	gridInfo.add(new JLabel(" "), gbc);
        	gbc.gridx = col_tf;  gbc.weightx = weight_tf;
        	gridInfo.add(m_boxSizeTf, gbc);
        	gbc.gridy++;
        	gridInfo.add(new JLabel(" "), gbc);
        	
        	gbc.gridy++; 
        	gbc.gridx = col_lb;  gbc.weightx = weight_lb;
        	gbc.gridwidth = 2;
        	gridInfo.add(new JLabel("Columns"), gbc);
        	gbc.gridwidth = 1;
        	gbc.gridy++;
        	gridInfo.add(new JLabel(" "), gbc);
        	gbc.gridx = col_tf;  gbc.weightx = weight_tf;
        	gridInfo.add(m_gridColsTf, gbc);
        	gbc.gridy++;
        	gridInfo.add(new JLabel(" "), gbc);
        	
        	gbc.gridy++;
        	gbc.gridwidth = 2;
        	gbc.gridx = col_lb;  gbc.weightx = weight_lb;
        	gridInfo.add(new JLabel("Rows"), gbc);
        	gbc.gridwidth = 1;
        	gbc.gridy++;
        	gbc.gridx = col_tf;  gbc.weightx = weight_tf;
        	gridInfo.add(m_gridRowsTf, gbc);
        	gbc.gridy++;
        	gridInfo.add(new JLabel(" "), gbc);

        	gbc.gridy++; gbc.gridx = col_bt;
        	gbc.gridwidth = GridBagConstraints.REMAINDER;
        	gridInfo.add(m_gridSetBtn, gbc); 
        	m_gridSetBtn.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					double boxSize = 10;
					try {
						boxSize = Double.parseDouble(m_boxSizeTf.getText());
					} catch (NumberFormatException ne) {
						boxSize = m_offsetGrid.getBoxSize();
					}
					m_offsetGrid.setBoxSize(boxSize);
					
					int cols = 5;
					try {
						cols = Integer.parseInt(m_gridColsTf.getText());
					} catch (NumberFormatException ne) {
						cols = m_offsetGrid.getGridCols();
					}
					m_offsetGrid.setGridCols(cols);
					
					int rows = 5;
					try {
						rows = Integer.parseInt(m_gridRowsTf.getText());
					} catch (NumberFormatException ne) {
						rows = m_offsetGrid.getGridRows();
					}
					m_offsetGrid.setGridRows(rows);
					
					updateTextFields();
				}
        		
        	});
        }
        
        // Put the frame together and display it.
        framegbc.gridx = 0; framegbc.gridy = 0;
        framegbc.ipady = 4;
        framegbc.anchor = GridBagConstraints.NORTHWEST;
        getContentPane().add(m_jplMenuBar, framegbc);
        
        framegbc.weightx = 100.0;
        framegbc.weighty = 100.0;
        framegbc.ipady   = 0;
        framegbc.anchor  = GridBagConstraints.WEST;
        framegbc.fill    = GridBagConstraints.BOTH;
        framegbc.gridx = 0; framegbc.gridy = 1;
        framegbc.anchor = GridBagConstraints.WEST;
        getContentPane().add(m_jplMain, framegbc);
        
        pack();

        setVisible(true);

        // All the pieces are ready, setup the initial display.
        m_GUIBuilt = true;
        
        // create and start the trigger thread
        m_Trigger = new TrigThrd();
        m_Trigger.start();
        
    }
    
    private void updateTextFields() {
    	m_boxSizeTf.setText(Double.toString(m_offsetGrid.getBoxSize()));
    	m_gridColsTf.setText(Integer.toString(m_offsetGrid.getGridCols()));
    	m_gridRowsTf.setText(Integer.toString(m_offsetGrid.getGridRows()));
	}

	/**
     * When this frame is closed; kill the program in a controlled fashion. 
     * This includes closing child frames.
     */
    @Override
    public void doThisOnFrameClose() {
        CLog.notice(".doThisOnFrameClose");
        removeNotifyTag();
        stopTrigger();
        if (m_LogWind != null) {
            m_LogWind.doThisOnFrameClose();
            m_LogWind = null;
        }
        m_Central.queHalt(); // this should stop the central thread and result in all client threads being stopped.
        super.doThisOnFrameClose();
    }
    
    /** 
     * This method should stop m_Trigger. 
     */
    public void stopTrigger() {
        CLog.notice("__GUI.stopTrigger()");
        if (m_Trigger != null) {
            // This should stop the trigger thread and cause it to be deleted.
            m_Trigger.m_bLoop = false;
            m_Trigger = null;
        }
    }
    

    /**
     * It is hard to get this function called. If you actually want code to run,
     * put it in doThisOnFrameClose.
     */
    public void finalize() throws Throwable {
        CLog.notice("__GUI.finalize()");
        super.finalize();
    }
    

    
    /**
     * This method will enable or disable several controls according to 
     * the value of enable. If enable is true, components will be enabled. 
     * If it is false, components will be disabled.
     */
    void enableControls(boolean enable) {
        if (m_GUIBuilt) {
            m_offsetGrid.setEnabled(enable);
        }
    }
    
    class ActionCreateWindow extends AbstractAction {
        String m_text;
        int    m_WindowType;
        TargetOffsetGUI m_parentFrm;
        public ActionCreateWindow(String text, int windowType, TargetOffsetGUI parentFrm) {
            super(text);
            m_text = text;
            m_WindowType = windowType;
            m_parentFrm  = parentFrm;
        }

        public void actionPerformed(ActionEvent ae) {
            switch (m_WindowType) {
            
            case LOG_WIND:
                m_LogWind.showWind();
                break;
            case RESIZE_FRM:
                CLog.notice("Resizing frame");
                sendPack();
                break;
            
            default:
                CLog.err("ActionCreateWindow unknown "+m_text+" "+m_WindowType);
            }
        }
        
        private static final long serialVersionUID = 1L;
    }
    
    
    /**
     * This class extends the OffsetGrid class so that it displays the 
     * offset positions for poco when tracking is on.
     * @author jgates
     *
     */
    class TeleOffsetGrid extends OffsetGrid implements GKeyConnection {
		GKeyDouble m_gkwPOCRARCT;
        GKeyDouble m_gkwPOCDERET;
        GKeyDouble m_gkwPOCRAT;  
        GKeyDouble m_gkwPOCDECT; 
        GKeyDouble m_gkwPOCRAA;
        GKeyDouble m_gkwPOCDECA;
        
        @SuppressWarnings("rawtypes")
		TreeMap<String, GKey> m_gridKeys = new TreeMap<String, GKey>();
        
        double m_serverDotRadius;
        Color m_serverDotColor  = Color.BLUE;
        Color m_velDotColor  = Color.BLACK;
        
        GTag m_tag = new GTag(TeleOffsetGrid.class.getName());
        
        TeleOffsetGrid() {
            super();
            setGridRows(5); // Number of grid boxes vertical (should be an odd number)
            setGridCols(5);
            @SuppressWarnings("rawtypes") LinkedList<GKey> gkl = new LinkedList<GKey>();
           	gkl.add(m_gkwPOCRARCT = (GKeyDouble) m_ktlclient.getKeyBy("POCRARCT")); // total RA plate scale radians
        	gkl.add(m_gkwPOCDERET = (GKeyDouble) m_ktlclient.getKeyBy("POCDERET")); // total Dec plate scale radians
        	gkl.add(m_gkwPOCRAT   = (GKeyDouble) m_ktlclient.getKeyBy("POCRAT")); // Target RA in radians
        	gkl.add(m_gkwPOCDECT  = (GKeyDouble) m_ktlclient.getKeyBy("POCDECT")); // Target Dec in radians
        	gkl.add(m_gkwPOCRAA   = (GKeyDouble) m_ktlclient.getKeyBy("POCRAA")); // Actual RA in radians
        	gkl.add(m_gkwPOCDECA  = (GKeyDouble) m_ktlclient.getKeyBy("POCDECA")); // Actual Dec in radians
        	GridNKMsg gridnkmsg = new GridNKMsg();
        	for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
        		String name = gk.getName();
        		CLog.notice(this.getClass()+" adding GUI keyword "+name);
        		m_gridKeys.put(name, gk);
        		gk.putNotify(getTag(), gridnkmsg);
        	}
            setPositions();
            updateDisplay();
        }
        
        /**
         * Simply update the display with latest positions.
         * Inefficient but simple and we aren't concerned with losing values
         * as we just want to display the latest information.
         */
        class GridNKMsg extends NKMsg implements Runnable {
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				SwingUtilities.invokeLater(this);
			}
			@Override
			public void run() {
				setPositions();
				updateDisplay();
			}
        }

        
        private void setPositions() {
        	// Get target RA and Dec into arcseconds.
        	double raTargOffsetArcsec  = StaticUtil.convRadToArcsec(m_gkwPOCRARCT.getVal());
        	double decTargOffsetArcsec = StaticUtil.convRadToArcsec(m_gkwPOCDERET.getVal());
        	setPosTarget(raTargOffsetArcsec, decTargOffsetArcsec);
        	
        	// Get actual RA and Dec Offset in arcseconds. This needs to be calculated.
        	double raTarg    = StaticUtil.convRadToArcsec(m_gkwPOCRAT.getVal());
        	double decTarg   = StaticUtil.convRadToArcsec(m_gkwPOCDECT.getVal());
        	double raAct     = StaticUtil.convRadToArcsec(m_gkwPOCRAA.getVal());
        	double decActRad = m_gkwPOCDECA.getVal();
        	double decAct    = StaticUtil.convRadToArcsec(decActRad);
        	double raActOffset  = raAct - raTarg;
        	double decActOffset = decAct - decTarg;
        	raActOffset = raActOffset * Math.cos(decActRad);
        	setPosActual(raActOffset, decActOffset);
		}

		/**
         * Write the position of the block clicked on to keywords so that
         * the server can move the telescope.
         */
        @Override
        protected void applyPosition() {
        	// Convert m_pos to radians from acrsecs
        	double raRad  = StaticUtil.convArcsecToRad(m_posUser.m_x);
        	double decRad = StaticUtil.convArcsecToRad(m_posUser.m_y);
        	m_gkwPOCRARCT.queWrite(raRad, null);
        	m_gkwPOCDERET.queWrite(decRad, null);
        }

		@Override
		public GTag getTag() {
			return m_tag;
		}

		@Override
		public void removeNotifyTag() {
			for(@SuppressWarnings("rawtypes") GKey gk : m_gridKeys.values()) {
				gk.removeNotifyTag(getTag());
			}
		}  
		
		private static final long serialVersionUID = 1L;
    }
    
 
    class OffsetSetButton extends EasyButton {
		OffsetSetButton() {
    		super("Set");
    	}
    	
    	/**
    	 * If the text fields for RA and Dec have user entered values in them, set them.
    	 */
    	@Override
    	public void doThisOnClick() {
    		if (!m_offsetRATf.getShowSystem()) {
    			String raStr = m_offsetRATf.getTextUser();
    			double ra_hours = StaticUtil.sexaParse(raStr);
    			double ra_rads = ra_hours*(Math.PI/12.0);
    			m_gkwPOCRARCT.queWrite(ra_rads, null);
    			m_offsetRATf.setShowSystem(true);
    		}
    		if (!m_offsetDecTf.getShowSystem()) {
    			String decStr = m_offsetRATf.getTextUser();
    			double dec_deg = StaticUtil.sexaParse(decStr);
    			double dec_rads = dec_deg*(Math.PI/180.0);
    			m_gkwPOCDERET.queWrite(dec_rads, null);
    			m_offsetDecTf.setShowSystem(true);
    		}
    	}
    	
		private static final long serialVersionUID = 1L;
    }
    
    
    /**
     * This thread simply sleeps for a while and then calls appropriate GUI
     * functions when it wakes up.
     * @author jgates
     *
     */
    class TrigThrd extends Thread {
        public volatile boolean m_bLoop = true;

        public void run() {
            try {
                while (m_bLoop) {
                    sleep(5000);
                    if (m_bLoop) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                // TODO add stuff to run every 5 seconds 
                            }
                        });
                    }
                }
            } catch (Exception e) {
                CLog.exceptionStr(e);
            }
        }
    }

    protected void sendPack() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                pack();
            }
        });
    }



	class TogNKMsg extends NKMsg implements Runnable {
		@Override
		public void run(KTLKeyword kd, Object val, String valStr) {
			SwingUtilities.invokeLater(this);
		}
		@Override
		public void run() {
			updateDisplay();
		}
	}
	
	public void updateDisplay() {
		double raRad = m_gkwPOCRARCT.getVal(); // value already corrected for declination
		double raArcsec = StaticUtil.convRadToArcsec(raRad);
		m_offsetRATf.setTextSystem(Double.toString(raArcsec));
		double decRad = m_gkwPOCDERET.getVal();
		double decArcsec = StaticUtil.convRadToArcsec(decRad);
		m_offsetDecTf.setTextSystem(Double.toString(decArcsec));
	}

	@Override
	public GTag getTag() {
		return m_tag;
	}

	@Override
	public void removeNotifyTag() {
		if (m_LogWind != null) {
			m_LogWind.removeNotifyTag();
		}
		if (m_offsetGrid != null) {
			m_offsetGrid.removeNotifyTag();
		}
		for(@SuppressWarnings("rawtypes") GKey gk  : m_GUIKeys.values()) {
			gk.removeNotifyTag(getTag());
		}
	}
	private static final long serialVersionUID = 1L;
}


