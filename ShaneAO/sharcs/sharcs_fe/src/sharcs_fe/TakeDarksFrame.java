package sharcs_fe;

import java.util.Arrays;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;


import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyStringLabel;
import org.uco.java_util.CLog;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyEnum;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GKeyString;
import org.uco.java_util.GTag;
import org.uco.java_util.KTLNotifyWrite;
import org.uco.java_util.StaticUtil;
import org.uco.ktljna_j.KTLJNALibInstance;






public class TakeDarksFrame extends FrameCloseCaptured implements GKeyConnection {
	KTLJNALibInstance m_ktlclient;
	SharcsGUI m_sharcsGUI;
	
	GTag    m_gtag = new GTag(SharcsGUI.class.getName());
	
	// Table variables
	TakeDarksTableModel m_tblModel = new TakeDarksTableModel(null, new ExposureMap());
	JTable                    m_jtable;
	int[]                     m_selectedRows = new int[0];
	
	JTextField m_allTakes_tf;
	
	volatile boolean m_continue = true;
	
	GKeyInt m_kwITIME;
	GKeyInt m_kwGROUPS; 
	GKeyInt m_kwNUMREADS;
	GKeyInt m_kwCOADDS;
	GKeyInt m_kwRESETS;  
	GKeyInt m_kwGO;
	GKeyString m_kwOBJECT;
	
	private volatile boolean m_takingDarks = false;
	private GKeyString m_gkwDWFILT1NAM;
	private GKeyEnum m_gkwDWFILT1SWT;
	private GKeyString m_gkwDWFILT2NAM;
	private GKeyEnum m_gkwDWFILT2SWT;
	private GKeyString m_gkwDWAPERNAM;
	private GKeyEnum m_gkwDWAPERSWT;
	private FilterLabel m_filt1;
	private FilterLabel m_filt2;
	private FilterLabel m_aper;
	private JButton m_filtGoButton;
	
	private SetFilters m_filtSetThrd = null;

	
	public TakeDarksFrame(SharcsGUI sharcsGUI) {
		super("Take Darks");
		m_sharcsGUI = sharcsGUI;
		m_ktlclient = m_sharcsGUI.m_ktl;
		
    	m_kwITIME    = m_sharcsGUI.m_gkwITIME;
		m_kwGROUPS   = m_sharcsGUI.m_gkwGROUPS; 
		m_kwNUMREADS = m_sharcsGUI.m_gkwNUMREADS;
		m_kwCOADDS   = m_sharcsGUI.m_gkwCOADDS;
		m_kwRESETS   = m_sharcsGUI.m_gkwRESETS;  
    	m_kwGO       = m_sharcsGUI.m_gkwGO;
    	m_kwOBJECT   = m_sharcsGUI.m_gkwOBJECT;
    	
    	m_gkwDWFILT1NAM = m_sharcsGUI.m_gkwDWFILT1NAM;
        m_gkwDWFILT1SWT = m_sharcsGUI.m_gkwDWFILT1SWT;
        m_gkwDWFILT2NAM = m_sharcsGUI.m_gkwDWFILT2NAM;
        m_gkwDWFILT2SWT = m_sharcsGUI.m_gkwDWFILT2SWT;
        m_gkwDWAPERNAM  = m_sharcsGUI.m_gkwDWAPERNAM;
        m_gkwDWAPERSWT  = m_sharcsGUI.m_gkwDWAPERSWT;
    	
    	
        // make the frame and layout
		Container pan = getContentPane();
        pan.setLayout(new BorderLayout()); 
        pan.setSize(580,750);  
        
        JPanel topPanel = new JPanel();
        pan.add(topPanel, BorderLayout.NORTH);
        topPanel.setLayout(new GridLayout(1,0));
        //topPanel.add(new JLabel("Filt1"));
        m_filt1 = new FilterLabel(m_gkwDWFILT1NAM, "Filter 1:", "BrG-2.16");
        m_filt2 = new FilterLabel(m_gkwDWFILT2NAM, "Filter 2:", "blank25");
        m_aper  = new FilterLabel(m_gkwDWAPERNAM, "Aperture:", "pinhole-100um");
        topPanel.add(m_filt1);
        topPanel.add(m_filt2);
        topPanel.add(m_aper);
        m_filtGoButton = new JButton("Set Filters");
        topPanel.add(m_filtGoButton);
        m_filtGoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setFilters();
			}
        });
        
        

        JPanel bottomPanel = new JPanel();
        pan.add(bottomPanel, BorderLayout.SOUTH);
        bottomPanel.setLayout(new GridBagLayout());
        GridBagConstraints gbcbot = new GridBagConstraints();
        gbcbot.fill = GridBagConstraints.BOTH;
        gbcbot.weightx = 0.0;
        // refresh button
        gbcbot.gridx = 0; gbcbot.gridy = 0;
        JButton btnRefresh = new JButton("Reload");
        bottomPanel.add(btnRefresh, gbcbot);
        btnRefresh.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		if (inputEraseWarn()) {
        			refreshFromPalette();
        		}
        	}
        });
        // clear counts button
        gbcbot.gridx = 0; gbcbot.gridy = 1;
        JButton btnClearCounts = new JButton("Clear Counts");
        bottomPanel.add(btnClearCounts, gbcbot);
        btnClearCounts.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		setAllCountsToZero();
        		m_sharcsGUI.m_paletteFrm.update();
        	}
        });
        //  clear non-default exposures
        gbcbot.gridx = 0; gbcbot.gridy = 2; 
        gbcbot.gridwidth = 1;
        JButton btnDeleteExpInfo = new JButton("Delete Exp Info");
        bottomPanel.add(btnDeleteExpInfo, gbcbot);
        btnDeleteExpInfo.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		removeNonDefaultsFromPalette();
        		m_sharcsGUI.m_paletteFrm.update();
        	}
        });
        // start button
        gbcbot.gridx = 4; gbcbot.gridy = 0; 
        gbcbot.gridwidth = 1;
        JButton btnStart = new JButton("Start");
        bottomPanel.add(btnStart, gbcbot);
        btnStart.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		startTakeAllDarksThrd();
        	}
        });
       
        // stop button
        gbcbot.gridx = 4; gbcbot.gridy = 1; 
        gbcbot.gridwidth = 1;
        JButton btnStop = new JButton("Stop");
        bottomPanel.add(btnStop, gbcbot);
        btnStop.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		m_continue = false;
        	}
        });
        
        // close button
        gbcbot.gridx = 4; gbcbot.gridy = 2; 
        gbcbot.gridwidth = 1;
        JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn, gbcbot);
        
        
        // Remove rows with 0 count
        gbcbot.gridx = 2; gbcbot.gridy = 2;
        JButton removeZCounts = new JButton("Remove Rows with Count==0");
        bottomPanel.add(removeZCounts, gbcbot);
        removeZCounts.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		if (inputRemoveRowsWarn()) {
        			removeRowsWithCountEqZero();
        		}
        	}
        });
        
        // Panel with all takes and all coadds
        JPanel allTakes_jp = new JPanel();
        allTakes_jp.setLayout(new GridBagLayout());
        GridBagConstraints gbc_t = new GridBagConstraints();
        m_allTakes_tf = new JTextField();
        JButton allTakes_bt = new JButton("Set All Takes");
        gbc_t.fill = GridBagConstraints.BOTH;
        gbc_t.weightx = 100.0;
        gbc_t.gridx = 0; gbc_t.gridy = 0;
        allTakes_jp.add(m_allTakes_tf, gbc_t);
        gbc_t.weightx = 0.0;
        gbc_t.gridx = 1;
        allTakes_jp.add(allTakes_bt, gbc_t);
        allTakes_bt.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		setAllTakesTo();
        	}
        });
        
     
        
        gbcbot.gridx = 2; gbcbot.gridy = 0;
        gbcbot.weightx = 100.0;
        gbcbot.gridheight = 2;
        bottomPanel.add(allTakes_jp, gbcbot);
        gbcbot.gridheight = 1;
        
        // spacer
        gbcbot.gridx = 1; gbcbot.gridy = 0; 
        gbcbot.gridheight = 2;
        gbcbot.weightx = 20.0;
        bottomPanel.add(new JLabel(""), gbcbot);
        gbcbot.gridx = 3;
        bottomPanel.add(new JLabel(""), gbcbot);
        
        m_tblModel = new TakeDarksTableModel(m_sharcsGUI, m_sharcsGUI.m_paletteFrm.m_tblModel.m_expMap);
        
        m_jtable = new JTable(m_tblModel);
        //m_jtable.setDefaultRenderer(null, new TakeDarksTableModel.TakeDarksCellRenderer());
        for (int cc=0; cc < m_tblModel.getColumnCount(); cc++) {
        	m_jtable.getColumnModel().getColumn(cc).setCellRenderer(new TakeDarksTableModel.TakeDarksCellRenderer());
        }
        JScrollPane tabTestScroll = new JScrollPane(m_jtable);
        m_jtable.setPreferredScrollableViewportSize(new Dimension(500,350));
        pan.add(tabTestScroll);
        pan.setVisible(true);
        
        int cw = 80;
        m_jtable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        m_jtable.getColumnModel().getColumn(0).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(1).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(2).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(3).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(4).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(5).setPreferredWidth(cw/2);
        m_jtable.getColumnModel().getColumn(6).setPreferredWidth(cw);
        m_jtable.getColumnModel().getColumn(7).setPreferredWidth(cw/2);
        m_jtable.getColumnModel().getColumn(8).setPreferredWidth(cw);
        m_jtable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        // we need a list selection model so we can listen for events
        ListSelectionModel tblListSel = m_jtable.getSelectionModel();
        m_jtable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        //m_jtable.setColumnSelectionAllowed(false);
        //m_jtable.setRowSelectionAllowed(false);
        
        
        tblListSel.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent ev) {
                String s = "selected:";
                int[] rows = m_jtable.getSelectedRows();
                int[] cols = m_jtable.getSelectedColumns();
                for(int i=0; i<rows.length; i++) s += rows[i]+" ";
                CLog.notice("rows l="+rows.length+"  "+s);
                s = "selected:";
                for(int i=0; i<cols.length; i++) s += cols[i]+" ";
                CLog.notice("cols l="+cols.length+"  "+s);
                m_selectedRows = rows;
           
            }
        });

	}
	
	
	protected void startTakeAllDarksThrd() {
		if (m_takingDarks == false) {
			m_takingDarks = true;
			TakeAllDarksThrd t = new TakeAllDarksThrd(this);
			t.start();
		}
	}
	
	protected void takeAllDarks() {
		m_continue = true;
		KTLNotifyWrite n = m_kwOBJECT.queWrite("dark", null);
		n.waitComplete();
		DarkRow[] drs = m_tblModel.m_darks;
		for (int j=0; j<drs.length; j++) {
			DarkRow dr = drs[j];
			while (dr.m_takes > dr.getTaken() && m_continue) {
				takeDark(dr);
			}
		}
		m_takingDarks = false;
	}
	
	protected void takeDark(DarkRow dr) {
		// Need to watch for exposure end, we'll get the key info now.
		GKeyInt kw = m_sharcsGUI.m_gkwIMAGEDONE;
		// set keyword values for exposure
		m_kwITIME.queWrite(dr.m_exposure.getExposure(), null);
		m_kwGROUPS.queWrite( 1, null); 
		m_kwNUMREADS.queWrite(dr.m_exposure.getReads(), null);
		m_kwCOADDS.queWrite(dr.m_exposure.getCoadds(), null);
		m_kwRESETS.queWrite(dr.m_exposure.getResets(), null);  
    	m_kwGO.queWrite( 1, null);
    	CLog.notice("takeDark exposure started ex="+dr.m_exposure.getExposure()+" reads="+dr.m_exposure.getReads()+
    			" resets="+dr.m_exposure.getResets()+" taken="+dr.getTaken());
    	// wait for exposure to end
    	try { Thread.sleep(1000); } catch (InterruptedException e) { CLog.exceptionStr(e);}
    	while (kw.getVal() != 1) { // 1 = true, 0 = false
    		if (!m_continue) {
    			CLog.warn("takeDarks stopped");
    			return;
    		}
    		try {Thread.sleep(250);} catch (InterruptedException e) {CLog.exceptionStr(e);}
    	}
    	int i = dr.incrTaken(1);
    	m_tblModel.fireTableDataChanged();
    	CLog.notice("takeDark exposure done taken="+i);
    	
	}
	
	class TakeAllDarksThrd extends Thread {
		TakeDarksFrame m_takeDarksFrm;
		TakeAllDarksThrd(TakeDarksFrame tdf) {
			m_takeDarksFrm = tdf;
		}
		@Override
		public void run() {
			m_takeDarksFrm.takeAllDarks();
		}
	}
	



	protected void setAllTakesTo() {
		String s = m_allTakes_tf.getText();
		int takes = 0;
		try {
			takes = Integer.parseInt(s);
		} catch (NumberFormatException ne) {
			CLog.notice("setAllTakesTo()could not parse "+s);
			CLog.exceptionStr(ne);
			return;
		}
		m_tblModel.setAllTakesTo(takes);
	}

	/**
	 * remove all rows from the take darks frame where the counts == 0. This
	 * does not affect the palette.
	 */
	protected void removeRowsWithCountEqZero() {
		m_tblModel.removeRowsWithCountEqZero();
	}


	protected void removeNonDefaultsFromPalette() {
		if (inputEraseCompleteWarn()) {
			m_sharcsGUI.m_paletteFrm.removeNonDefaults();
			refreshFromPalette();
		}
	}


	@Override
	public void showWind() {
		super.showWind();
		m_tblModel.updateDataIfNeeded(m_sharcsGUI.m_paletteFrm.m_tblModel.m_expMap);
	}

	/**
	 * Update the table model with the latest information from the palette.
	 */
	void refreshFromPalette() {
		m_tblModel.updateData(m_sharcsGUI.m_paletteFrm.m_tblModel.m_expMap);
	}
	
	/**
	 * Set the counts of all exposures in the exposure palette to zero.
	 */
	void setAllCountsToZero() {
		if (inputEraseCountsWarn()) {
			m_sharcsGUI.m_paletteFrm.clearAllCounts();
			refreshFromPalette();
		}
	}
	
	/**
	 * Returns true if the user wants to continue despite the warning.
	 * @return
	 */
	boolean inputEraseWarn() {
		Object[] options = {"Continue", "Cancel"};
		int n = JOptionPane.showOptionDialog(this,
				"This will reset Takes and Taken.\nOther values will remain unchanged.",
				"Input will be erased.",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,     //do not use a custom Icon
				options,  //the titles of buttons
				options[0]); //default button title
		return (n == JOptionPane.YES_OPTION);
	}
	
	/**
	 * Returns true if the user wants to continue despite the warning.
	 * @return
	 */
	boolean inputEraseCountsWarn() {
		Object[] options = {"Continue", "Cancel"};
		int n = JOptionPane.showOptionDialog(this,
				"This will reset Counts, Takes, and Taken.",
				"Input will be erased.",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,     //do not use a custom Icon
				options,  //the titles of buttons
				options[0]); //default button title
		return (n == JOptionPane.YES_OPTION);
	}
	
	
	/**
	 * Returns true if the user wants to continue despite the warning.
	 * @return
	 */
	boolean inputRemoveRowsWarn() {
		Object[] options = {"Continue", "Cancel"};
		int n = JOptionPane.showOptionDialog(this,
				"Rows with 0 Counts will be removed from this table but left in the palette.",
				"Display simplify",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,     //do not use a custom Icon
				options,  //the titles of buttons
				options[0]); //default button title
		return (n == JOptionPane.YES_OPTION);
	}
	
	
	/**
	 * Returns true if the user wants to continue despite the warning.
	 * @return
	 */
	boolean inputEraseCompleteWarn() {
		Object[] options = {"Continue", "Cancel"};
		int n = JOptionPane.showOptionDialog(this,
				"ALL exposure data will be deleted!",
				"Input will be erased.",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.WARNING_MESSAGE,
				null,     //do not use a custom Icon
				options,  //the titles of buttons
				options[0]); //default button title
		return (n == JOptionPane.YES_OPTION);
	}
	
	private static final long serialVersionUID = 1L;


	public void fireTableDataChanged() {
		m_tblModel.fireTableDataChanged();
	}


	@Override
	public GTag getTag() {
		return m_gtag;
	}


	@Override
	public void removeNotifyTag() {
		m_filt1.removeNotify();
        m_filt2.removeNotify();
        m_aper.removeNotify();
	}

	
	class SetFiltButtonColorMsg implements Runnable {
		Color m_c;
		boolean m_o;
		SetFiltButtonColorMsg(Color c, boolean o) {
			m_c = c;
			m_o = o;
		}
		@Override
		public void run() {
			m_filtGoButton.setBackground(m_c);
			m_filtGoButton.setOpaque(m_o);
		}
	}
	
	class SetFilters extends Thread {
		public volatile boolean m_cancelled = false;
		public void run() {
			SwingUtilities.invokeLater(new SetFiltButtonColorMsg(Color.YELLOW, true));
			KTLNotifyWrite f1 = m_filt1.queWrite();
			KTLNotifyWrite f2 = m_filt2.queWrite();
			KTLNotifyWrite a  = m_aper.queWrite();
			
			if (f1 == null || f2 == null || a == null) {
				SwingUtilities.invokeLater(new SetFiltButtonColorMsg(Color.RED, true));
				CLog.err(SetFilters.class.getName()+" at least one queWrite to filter returned null");
				m_sharcsGUI.m_LogWind.sendMsg("Set Filters failed, possibly no saomot service connection.", true);
				return;
			}
			
			int j = 0;
			while (!m_cancelled && j < 30 && 
				    !f1.getComplete() &&
				    !f2.getComplete() &&
				    !a.getComplete()) {
				StaticUtil.sleepy(500);
			}
			if (m_cancelled) { return; }
			if (f1.getSuccess() && f2.getSuccess() && a.getSuccess()) {
				// SUCCESS
				SwingUtilities.invokeLater(new SetFiltButtonColorMsg(Color.GRAY, false));
			} else {
				SwingUtilities.invokeLater(new SetFiltButtonColorMsg(Color.RED, true));
			}
		}
	}
	
	private void setFilters() {
		if (m_filtSetThrd != null) {
			m_filtSetThrd.m_cancelled = true;
			m_filtSetThrd = null;
		}
		m_filtSetThrd = new SetFilters();
		m_filtSetThrd.start();
	}
	
	
}



class DarkRow {
	Exposure m_exposure;     // exposure info including counts
	int      m_takes = 0;    // darks to take
	double   m_singleTakeTime = 0.0;
	double   m_totalTime = 0.0;
	
	private int      m_taken = 0;
	public final double RESET_T = 0.1;
	public final double READ_T  = 0.15;
	
	public synchronized int getTaken() { return m_taken; }
	public synchronized void setTaken(int taken) { m_taken = taken;}
	public synchronized int incrTaken(int incr) {
		m_taken += incr;
		return m_taken;
	}
	
	public DarkRow(Exposure ex) {
		// Dark row is same as original exposure,
		// except we set coadds to 1.
		m_exposure = new Exposure(ex.getIndex(),
			ex.getExposure(), ex.getReads(),
			ex.getResets(), 1,
			ex.getStandard(), ex.m_count);
	}
	
	public double timePerExposure() {
		Exposure ex = m_exposure;
		if (ex == null) return 0.0;
		// 12.0 is processing overhead (mainly writing to disk).
		m_singleTakeTime = ex.getExposureSec() + ex.getReads()*READ_T + ex.getResets()*RESET_T + 12.0;
		return m_singleTakeTime;
	}
	
	public double timePerRow() {
		int takes = m_takes - m_taken;
		if (takes < 0) takes = 0;
		int coadds = m_exposure.getCoadds();
		m_totalTime = m_singleTakeTime*coadds*takes;
		return m_totalTime;
	}
}


class TakeDarksTableModel extends AbstractTableModel {
	
	static class TakeDarksCellRenderer extends DefaultTableCellRenderer {
		@Override
		  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
		    //Cells are by default rendered as a JLabel.
		    Component c =  super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, col);
		    //Get the status for the current row.
		    TakeDarksTableModel tableModel = (TakeDarksTableModel) table.getModel();
		    if (tableModel.isCellEditable(row, col)) {
		      c.setBackground(Color.ORANGE);
		    } else {
		      c.setBackground(Color.WHITE);
		    }
		  //Return the component.
		  return c;

		}
		private static final long serialVersionUID = 1L;
	}
	
	DarkRow[] m_darks = new DarkRow[0];
	double    m_totalTime = 0.0;
	String[] m_columnNames = { "Count", "Exp Time", "Reads", "Resets", "Coadds", "", "Takes", "", "Taken", "Total Time" };
	
	boolean m_needUpdate = true;
	private SharcsGUI m_sharcsGUI;

	public TakeDarksTableModel(SharcsGUI sharcsGUI, ExposureMap em) {
		m_sharcsGUI = sharcsGUI;
		updateData(em);
		m_needUpdate = true;
	}
	


	public void setAllTakesTo(int takes) {
    	for (int j=0; j<m_darks.length; j++) {
    		DarkRow dr = m_darks[j];
    		dr.m_takes = takes;
    		recalc();
    	}
	}
	


	public void removeRowsWithCountEqZero() {
    	LinkedList<DarkRow> lldr = new LinkedList<DarkRow>();
    	for (int j=0; j<m_darks.length; j++) {
    		DarkRow dr = m_darks[j];
    		if (dr != null && dr.m_exposure.m_count > 0) {
    			lldr.add(dr);
    		}
    	}
    	DarkRow[] ndr = new DarkRow[lldr.size()];
    	Iterator<DarkRow> iter = lldr.iterator();
    	int j = 0;
    	while (iter.hasNext()) {
    		ndr[j] = iter.next();
    		j++;
    	}
    	m_darks = ndr;
    	recalc();
	}

	public void update() {
    	fireTableDataChanged();
    }
	
    public void updateDataIfNeeded(ExposureMap em) {
    	if (m_needUpdate) {
    		updateData(em);
    	}
    }
    
	public void updateData(ExposureMap em) {
		Exposure[] ea = em.getArray();

		// Allocate a possibly-oversize array.
		// (Oversize, because we don't keep rows that
		// are same except for coadds.)
		// After we populate it, we replace it with
		// a right-sized array.
		DarkRow[] ndrs = new DarkRow[ea.length];

		// Only create rows for unique time/reads/resets.
		// Always set coadds to 1.
		int ndark = 0;
		for (int j=0; j<ea.length; j++) {
		    boolean alreadySeen = false;
		    for (int i=0; i<j && !alreadySeen; i++) {
			if (ea[j].compareExReRe(ea[i]) == 0) {
			    // Already got one of these
			    alreadySeen = true;
			}
		    }
		    if (!alreadySeen) {
			ndrs[ndark++] = new DarkRow(ea[j]);
		    }
		}
		DarkRow[] ndrs2 = Arrays.copyOf(ndrs, ndark);
		m_darks = ndrs2;
		m_needUpdate = false;
		recalc();
	}
	
	public void recalc() {
		for (int j=0; j<m_darks.length; j++) {
			DarkRow dr = m_darks[j];
			dr.timePerExposure();
			dr.timePerRow();
		}
		totalTime();
		update();
	}
	

	
	public double totalTime() {
		double tt = 0.0;
		for (int j=0; j<m_darks.length; j++) {
			tt += m_darks[j].m_totalTime;
		}
		m_totalTime = tt;
		return tt;
	}
	
	@Override
	public int getRowCount() {
		return m_darks.length + 2;
	}

	@Override
	public int getColumnCount() {
		return m_columnNames.length;
	}

	
    @Override
    public String getColumnName(int c) {
        return m_columnNames[c];
    }
    

	
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (rowIndex >= m_darks.length) {
			int r = rowIndex - m_darks.length;
			if (r == 1) {
				switch (columnIndex) {
				case 5:
					return "Hrs:";
				case 6:
					return m_totalTime/(60.0*60.0);
				case 8: 
					return "Secs:";
				case 9:
					return m_totalTime;
				}
				return "";
			}
		}
		
		DarkRow dr = null;
		try {
			dr = m_darks[rowIndex];
		} catch (ArrayIndexOutOfBoundsException e) {
			return "";
		}
		switch(columnIndex) {
		case 0:
			return dr.m_exposure.m_count;
		case 1:
			if (m_sharcsGUI != null && m_sharcsGUI.isTimeInSeconds()) {
				return dr.m_exposure.getExposureSec();
			} else {
				return dr.m_exposure.getExposure();
			}
		case 2:
			return dr.m_exposure.getReads();
		case 3:
			return dr.m_exposure.getResets();
		case 4:
			return dr.m_exposure.getCoadds();
		case 5:
			return "->";
		case 6:
			return dr.m_takes;
		case 7: 
			return "->";
		case 8:
			return dr.getTaken();
		case 9:
			return dr.m_totalTime;
		}
		return "";
	}
	
	
    @Override
    public boolean isCellEditable(int row, int col) {
    	if (col == 6 || col == 8) {
    		if (row < m_darks.length) {
    			return true;
    		}
    	}
        return false;
    }
	
	
	@Override
	public void setValueAt(Object val, int row, int col) {
		DarkRow dr = m_darks[row];
		if (col == 6) {
			// takes
			dr.m_takes = Integer.parseInt((String)val);
			fireTableCellUpdated(row, col);
			recalc();
		} else if (col==8) {
			int i = Integer.parseInt((String)val);
			dr.setTaken(i);
			fireTableCellUpdated(row, col);
			recalc();
		}
	
	}
	
	private static final long serialVersionUID = 1L;
}


class FilterLabel extends KeyStringLabel {
	private static final long serialVersionUID = 1L;
	String m_targStr = "";
	
	public FilterLabel(GKeyString key, String text, String targStr) {
		super(key, text);
		m_targStr = targStr;
		updateDisplay();
	}

	@Override
	public void updateDisplay() {
		super.updateDisplay();
		String val = getValueString();
		if (val != null && m_targStr.compareToIgnoreCase(val) == 0) {
			setBackground(Color.GREEN);
			setOpaque(true);
		} else {
			setBackground(Color.YELLOW);
			setOpaque(true);
		}
	}
	
	public KTLNotifyWrite queWrite() {
		return getKey().queWriteAscii(m_targStr, null);
	}
	
}
