package sharcs_fe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.uco.gui_bits.BaseLabel;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyFloatLabel;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.gui_bits.KeyStringLabel;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyFloat;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GKeyString;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNAService;


public class StatusFrame  extends FrameCloseCaptured implements GKeyConnection {

	
	SharcsGUI mGui;
	
	KeyIntLabel    mREADY_val;
	KeyIntLabel    mEXPOSING_val;
	KeyIntLabel    mIMAGEDONE_val;
	KeyIntLabel    mABORTING_val;
	KeyFloatLabel  mREADTIME_val;
	KeyFloatLabel  mELAPSEDTIME_val;
	KeyFloatLabel  mTOTALTIME_val;
	KeyIntLabel    mREADSDONE_val;
	KeyIntLabel    mGROUPSDONE_val;
	KeyIntLabel    mCOADDSDONE_val;
	KeyStringLabel mLASTFILE_val;
	
	LinkedList<BaseLabel> mStats = new LinkedList<BaseLabel>();

	private GTag mGTag = new GTag(StatusFrame.class.getName());
	
	
	public StatusFrame(SharcsGUI gui) throws GeneralException {
		super("Status");
		mGui = gui;
		KTLJNAService serv = mGui.getServSharcs();
		
		@SuppressWarnings("unused")
		LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();
		
		Container pan = getContentPane();
		pan.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		pan.add(bottomPanel, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn);
		

		JPanel main = new JPanel();
		pan.add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 100.0; gbc.weighty = 100.0;
		
		
		
		mStats.add(mREADY_val       = new KeyIntLabel((GKeyInt) serv.getGKey("READY"), "Ready"));
		mStats.add(mEXPOSING_val    = new KeyIntLabel((GKeyInt) serv.getGKey("EXPOSING"), "Exposing"));
		mStats.add(mIMAGEDONE_val   = new KeyIntLabel((GKeyInt) serv.getGKey("IMAGEDONE"), "ImageDone"));
		mStats.add(mABORTING_val    = new KeyIntLabel((GKeyInt) serv.getGKey("ABORTING"), "Aborting"));
		mStats.add(mREADTIME_val    = new KeyFloatLabel((GKeyFloat) serv.getGKey("READTIME"), "ReadTime"));
		mStats.add(mELAPSEDTIME_val = new KeyFloatLabel((GKeyFloat) serv.getGKey("ELAPSEDTIME"), "ElapsedTime"));
		mStats.add(mTOTALTIME_val   = new KeyFloatLabel((GKeyFloat) serv.getGKey("TOTALTIME"), "TotalTime"));
		mStats.add(mREADSDONE_val   = new KeyIntLabel((GKeyInt) serv.getGKey("READSDONE"), "ReadsDone"));
		mStats.add(mGROUPSDONE_val  = new KeyIntLabel((GKeyInt) serv.getGKey("GROUPSDONE"), "GroupsDone"));
		mStats.add(mCOADDSDONE_val   = new KeyIntLabel((GKeyInt) serv.getGKey("COADDSDONE"), "CoaddsDone"));
		mStats.add(mLASTFILE_val    = new KeyStringLabel((GKeyString) serv.getGKey("LASTFILE"), "LastFile"));
		
		gbc.gridy = 5;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill    = GridBagConstraints.BOTH;

		for (BaseLabel bl : mStats) {
			String s = bl.getLeadText();
			bl.setLeadText("");
			bl.updateDisplay();
			bl.setBorder(new EmptyBorder(0,15,0,0));
			//bl.setHorizontalAlignment(SwingConstants.CENTER);
			gbc.gridx = 0;
			main.add(new JLabel(s), gbc);
			gbc.gridx = 1;
			main.add(bl, gbc);
			gbc.gridy++;
		}
		
	}
	
	
	private static final long serialVersionUID = 1L;


	@Override
	public GTag getTag() {
		return mGTag ;
	}


	@Override
	public void removeNotifyTag() {
		for (BaseLabel bl : mStats) {
			bl.removeNotifyTag();
		}
		
	}
}









