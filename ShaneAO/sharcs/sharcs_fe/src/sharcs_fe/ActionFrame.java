package sharcs_fe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JPanel;

import org.uco.gui_bits.ButtonGKeyWriteVal;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyEnumMenuButton;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyEnum;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNALibInstance;
import org.uco.ktljna_j.KTLJNAService;


public class ActionFrame  extends FrameCloseCaptured implements GKeyConnection {

	SharcsGUI m_gui;
	
	private GTag mGTag = new GTag(ActionFrame.class.getName());
	private ButtonGKeyWriteVal<Integer, GKeyInt> mRESUME_btn;
	private ButtonGKeyWriteVal<Integer, GKeyInt> mRESET_btn;
	private KeyIntLabel mASICPOWER_lb;
	private ButtonGKeyWriteVal<Integer, GKeyInt> mGETASICPOWER_btn;
	private KeyEnumMenuButton mDLFIRMWARE_btn;
	private ButtonGKeyWriteVal<Integer, GKeyInt> mPOWERDOWN_btn;
	
	
	
	@SuppressWarnings("unchecked")
	public ActionFrame(SharcsGUI gui) throws GeneralException  {
		super("Actions");
		m_gui = gui;
		KTLJNAService serv = m_gui.getServSharcs();

		
		mRESUME_btn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(m_gui.m_gkwRESUME, "Resume", 1);
		mRESET_btn  = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(m_gui.m_gkwRESET, "Reset", 1);
		
		mASICPOWER_lb = new KeyIntLabel((GKeyInt) serv.getGKey("ASICPWR"), "AsicPwr = ");
		mGETASICPOWER_btn  = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(serv.getGKey("GETASICPOWER"), "GetAsicPower", 1);
		
		mDLFIRMWARE_btn = new KeyEnumMenuButton((GKeyEnum) serv.getGKey("DLFIRMWARE"), "DLFirmware = ", null);
		mPOWERDOWN_btn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(serv.getGKey("POWERDOWN"), "PowerDown", 1);
			

		Container pan = getContentPane();
		pan.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		pan.add(bottomPanel, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn);
		
		
		JPanel main = new JPanel();
		pan.add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 100.0; gbc.weighty = 100.0;
		gbc.gridwidth = 2;
		
		int col1 = 0;
		int col2 = 1;

		gbc.gridy = 0;
		gbc.gridwidth = 2;
		gbc.gridx = col1;
		main.add(mRESUME_btn, gbc);

		gbc.gridy++;
		main.add(mRESET_btn, gbc);
		
		gbc.gridy++;
		gbc.gridwidth = 1;
		main.add(mASICPOWER_lb, gbc);
		gbc.gridx = col2;
		main.add(mGETASICPOWER_btn, gbc);
		gbc.gridwidth = 2;
		gbc.gridx = col1;
		
		gbc.gridy++;
		main.add(mDLFIRMWARE_btn, gbc);
		
		gbc.gridy++;
		main.add(mPOWERDOWN_btn, gbc);
			
	}
	
	
	private static final long serialVersionUID = 1L;

	@Override
	public GTag getTag() {
		return mGTag;
	}


	@Override
	public void removeNotifyTag() {
		mASICPOWER_lb.removeNotifyTag();
	}
}



