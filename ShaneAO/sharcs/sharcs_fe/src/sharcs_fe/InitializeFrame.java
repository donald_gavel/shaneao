package sharcs_fe;





import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedList;


import javax.swing.JPanel;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNAService;




//INIT, INITING, SIDECARINIT
//RESUME
//CONNECT, CONNECTED

//DLFIRMWARE
//RESET
//POWERDOWN

/**
 * TODO delete this class
 * @author jgates
 *
 */
public class InitializeFrame  extends FrameCloseCaptured {
	SharcsGUI m_gui;

	public InitializeFrame(SharcsGUI gui) throws GeneralException {
		super("Initialize");
		m_gui = gui;
		LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();
		KTLJNAService serv = m_gui.getServSharcs();

		JPanel main = new JPanel();
		getContentPane().add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 100.0; gbc.weighty = 100.0;

		KwLine kwlINIT = null, kwlINITING = null, kwlSIDECARINIT = null, 
		kwlRESUME = null,
		kwlCONNECT = null, kwlCONNECTED = null,
		kwlDLFIRMWARE = null,
		kwlRESET = null,
		kwlPOWERDOWN = null;


		kwlINIT        = new KwLine("INIT",        serv.getGKey("INIT"),        m_gParms);
		kwlINITING     = new KwLine("INITING",     serv.getGKey("INITING"),     m_gParms);
		kwlSIDECARINIT = new KwLine("SIDECARINIT", serv.getGKey("SIDECARINIT"), m_gParms);
		kwlRESUME      = new KwLine("RESUME",      serv.getGKey("RESUME"),      m_gParms);
		kwlCONNECT     = new KwLine("CONNECT",     serv.getGKey("CONNECT"),     m_gParms);
		kwlCONNECTED   = new KwLine("CONNECTED",   serv.getGKey("CONNECTED"),   m_gParms);
		kwlDLFIRMWARE  = new KwLine("DLFIRMWARE",  serv.getGKey("DLFIRMWARE"),  m_gParms);
		kwlRESET       = new KwLine("RESET",       serv.getGKey("RESET"),       m_gParms);
		kwlPOWERDOWN   = new KwLine("POWERDOWN",   serv.getGKey("POWERDOWN"),   m_gParms);


		int ii = 0;
		int colName   = ii++;
		int colCurr   = ii++;
		int colTF     = ii++;
		int colBtn    = ii++;
		gbc.gridy = 5;
		gbc.weightx = 100.0;
		gbc.weighty = 100.0;
		gbc.fill    = GridBagConstraints.BOTH;
		gbc.anchor  = GridBagConstraints.CENTER;
		ii = gbc.gridy;
		KwLine kwlItems[] = { 	kwlINIT, kwlINITING, kwlSIDECARINIT, kwlRESUME,
				kwlCONNECT, kwlCONNECTED, kwlDLFIRMWARE, kwlRESET, kwlPOWERDOWN };
		for (KwLine kwl:kwlItems) {
			gbc.gridy = ii++;
			kwl.place(main, gbc, colName, colCurr, colTF, colBtn);
		}

		
	}
	private static final long serialVersionUID = 1L;
}














