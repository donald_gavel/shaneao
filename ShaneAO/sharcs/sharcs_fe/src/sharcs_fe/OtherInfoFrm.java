package sharcs_fe;





import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedList;


import javax.swing.JPanel;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.CLog;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNAService;
import org.uco.ktljxml.KTLClientT;




/**
 * TODO delete this class
 * @author jgates
 *
 */
public class OtherInfoFrm  extends FrameCloseCaptured {
	// EXPOSING, ELAPSEDTIME
	// READSDONE
	// GROUPSDONE
	// COADDSDONE
	// RESETS
	
	// READY
	// ABORTING
	// IMAGEDONE
	// IMAGETYPE
	// HEARTBEATPERIOD
	
	// LASTFILE, OUTDIR, UNIXROOTDIR, WINROOTDIR, WRITEALL, DIRSEP, DATAFILE
	
	
	// Autocalculate
	// MAXUTRREADS, MINUTRRDITIME
	
	// TEST, TESTDIR, TESTFILE, TIMFILE
	
	// Engineering
	// SETRPSTYPE, SIMULATE
	
	// ASICCFG, ASICHOST, ASICPORT, ASICPWR
	
	SharcsGUI m_gui;
	
	public OtherInfoFrm(SharcsGUI gui) throws GeneralException {
		super("OtherInfo");
		m_gui = gui;
		KTLJNAService serv = m_gui.getServSharcs();
		LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();

		JPanel main = new JPanel();
		getContentPane().add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 100.0; gbc.weighty = 100.0;

		KwLine kwlEXPOSING = null, kwlELAPSEDTIME = null, kwlREADSDONE = null, kwlGROUPSDONE = null, 
		kwlCOADDSDONE = null, kwlRESETS = null, 
		kwlREADY = null, kwlABORTING = null, kwlIMAGEDONE = null, kwlIMAGETYPE = null, 
		kwlHEARTBEATPERIOD = null, 
		kwlLASTFILE = null, kwlOUTDIR = null, kwlUNIXROOTDIR = null, kwlWINROOTDIR = null, 
		kwlWRITEALL = null, kwlDIRSEP = null, kwlDATAFILE = null,
		// autocalc
		kwlMAXUTRREADS = null, kwlMINUTRRDITIME = null, 
		kwlTEST = null, kwlTESTDIR = null, kwlTESTFILE = null, kwlTIMFILE = null, 
		// engineering
		kwlSETRPSTYPE = null, kwlSIMULATE = null, 
		kwlASICCFG = null, kwlASICHOST = null, kwlASICPORT = null, kwlASICPWR = null;

		kwlEXPOSING    = new KwLine("EXPOSING",    serv.getGKey("EXPOSING"),    m_gParms);
		kwlELAPSEDTIME = new KwLine("ELAPSEDTIME", serv.getGKey("ELAPSEDTIME"), m_gParms);
		kwlREADSDONE   = new KwLine("READSDONE",   serv.getGKey("READSDONE"),   m_gParms);
		kwlGROUPSDONE  = new KwLine("GROUPSDONE",  serv.getGKey("GROUPSDONE"),  m_gParms);
		kwlCOADDSDONE  = new KwLine("COADDSDONE",  serv.getGKey("COADDSDONE"),  m_gParms);
		kwlRESETS      = new KwLine("RESETS",      serv.getGKey("RESETS"),      m_gParms);

		kwlREADY       = new KwLine("READY",       serv.getGKey("READY"),       m_gParms);
		kwlABORTING    = new KwLine("ABORTING",    serv.getGKey("ABORTING"),    m_gParms);
		kwlIMAGEDONE   = new KwLine("IMAGEDONE",   serv.getGKey("IMAGEDONE"),   m_gParms);
		kwlIMAGETYPE   = new KwLine("IMAGETYPE",   serv.getGKey("IMAGETYPE"),   m_gParms);
		kwlHEARTBEATPERIOD= new KwLine("HEARTBEATPERIOD",   serv.getGKey("HEARTBEATPERIOD"),m_gParms);

		kwlLASTFILE    = new KwLine("LASTFILE",    serv.getGKey("LASTFILE"),    m_gParms);
		kwlOUTDIR      = new KwLine("OUTDIR",      serv.getGKey("OUTDIR"),      m_gParms);
		kwlUNIXROOTDIR = new KwLine("UNIXROOTDIR", serv.getGKey("UNIXROOTDIR"), m_gParms);
		kwlWINROOTDIR  = new KwLine("WINROOTDIR",  serv.getGKey("WINROOTDIR"),  m_gParms);
		kwlWRITEALL    = new KwLine("WRITEALL",    serv.getGKey("WRITEALL"),    m_gParms); 
		kwlDIRSEP      = new KwLine("DIRSEP",      serv.getGKey("DIRSEP"),      m_gParms);
		kwlDATAFILE    = new KwLine("DATAFILE",    serv.getGKey("DATAFILE"),    m_gParms);

		kwlMAXUTRREADS   = new KwLine("MAXUTRREADS",  serv.getGKey("MAXUTRREADS"),    m_gParms);
		kwlMINUTRRDITIME = new KwLine("MINUTRRDITIME",serv.getGKey("MINUTRRDITIME"),  m_gParms); 
		kwlTEST          = new KwLine("TEST",         serv.getGKey("TEST"),        m_gParms);
		kwlTESTDIR       = new KwLine("TESTDIR",      serv.getGKey("TESTDIR"),     m_gParms);
		kwlTESTFILE      = new KwLine("TESTFILE",     serv.getGKey("TESTFILE"),    m_gParms);
		kwlTIMFILE       = new KwLine("TIMFILE",      serv.getGKey("TIMFILE"),     m_gParms);
		kwlSETRPSTYPE    = new KwLine("SETRPSTYPE",   serv.getGKey("SETRPSTYPE"),  m_gParms);
		kwlSIMULATE      = new KwLine("SIMULATE",     serv.getGKey("SIMULATE"),    m_gParms);
		kwlASICCFG       = new KwLine("ASICCFG",      serv.getGKey("ASICCFG"),     m_gParms);
		kwlASICHOST      = new KwLine("ASICHOST",     serv.getGKey("ASICHOST"),    m_gParms);
		kwlASICPORT      = new KwLine("ASICPORT",     serv.getGKey("ASICPORT"),    m_gParms);
		kwlASICPWR       = new KwLine("ASICPWR",      serv.getGKey("ASICPWR"),     m_gParms);


		int ii = 0;
        int colName   = ii++;
        int colCurr   = ii++;
        int colTF     = ii++;
        int colBtn    = ii++;
        gbc.gridy = 5;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.anchor  = GridBagConstraints.CENTER;
        ii = gbc.gridy;
        KwLine kwlItems[] = { 	kwlEXPOSING, kwlELAPSEDTIME, kwlREADSDONE, kwlGROUPSDONE, 
    			kwlCOADDSDONE, kwlRESETS, 
    			kwlREADY, kwlABORTING, kwlIMAGEDONE, kwlIMAGETYPE, 
    			kwlHEARTBEATPERIOD, 
    			kwlLASTFILE, kwlOUTDIR, kwlUNIXROOTDIR, kwlWINROOTDIR, 
    			kwlWRITEALL, kwlDIRSEP, kwlDATAFILE,
    			kwlMAXUTRREADS, kwlMINUTRRDITIME, 
    			kwlTEST, kwlTESTDIR, kwlTESTFILE, kwlTIMFILE, 
    			// engineering
    			kwlSETRPSTYPE, kwlSIMULATE, 
    			kwlASICCFG, kwlASICHOST, kwlASICPORT, kwlASICPWR};
        for (KwLine kwl:kwlItems) {
        	gbc.gridy = ii++;
        	kwl.place(main, gbc, colName, colCurr, colTF, colBtn);
        }
        
		
	}
	
	
	private static final long serialVersionUID = 1L;
}









