package sharcs_fe;




import java.text.DecimalFormat;


import javax.swing.JLabel;

import org.uco.gui_bits.KeyFloatLabel;
import org.uco.java_util.CLog;
import org.uco.java_util.GKeyFloat;






public interface SecsDisplay {
	public enum Units {SECS, MILLIS};
	public void setBaseType(Units b);
	public void setDisplayType(Units t);
	public Units getBaseType();
	public Units getDisplayType();
}

/**
 * A class for storing the predicted time so that it can be displayed in seconds and milliseconds.
 * @author jgates
 *
 */
class PredictedTimeLabel extends JLabel implements SecsDisplay {
	double m_time = 0.0;
	private SecsDisplay.Units m_unitsBase = SecsDisplay.Units.SECS;
	private SecsDisplay.Units m_unitsDisplay = SecsDisplay.Units.SECS;
	private DecimalFormat m_decimalFormat = new DecimalFormat("#.##");
	
	public PredictedTimeLabel() {
		super();
		setSeconds(0.0);
	}
	
	public void setSeconds(double secs) {
		m_time = secs;
		setBaseType(SecsDisplay.Units.SECS);
		update();
	}
	
	public void update() {
		double v = m_time;
		if (m_unitsBase == SecsDisplay.Units.SECS) {
			if (m_unitsDisplay == SecsDisplay.Units.MILLIS) {
				v = m_time * 1000.0;
			}
		} else { // m_unitsBase == SecsDisplay.Units.MILLIS
			if (m_unitsDisplay == SecsDisplay.Units.SECS) {
				v = m_time / 1000.0;
			}
		}
		String s = m_decimalFormat.format(v);
		setText(s);
	}

	@Override
	public void setBaseType(Units b) {
		m_unitsBase = b;
		update();
	}

	@Override
	public void setDisplayType(Units t) {
		m_unitsDisplay = t;
		update();
	}

	@Override
	public Units getBaseType() {
		return m_unitsBase;
	}
	
	@Override
	public Units getDisplayType() {
		return m_unitsDisplay;
	}
	
	private static final long serialVersionUID = 1L;
}

/**
 * Switch between second and millisecond display
 * @author jgates
 *
 */
class KTLFloatLabelSecs extends KeyFloatLabel implements SecsDisplay {
	private SecsDisplay.Units m_unitsBase = null;
	private SecsDisplay.Units m_unitsDisplay = null;
	private float m_scale = 1.0F;
	private boolean m_hideUnits = false;
	private DecimalFormat m_decimalFormat = new DecimalFormat("#.##");
	
	public KTLFloatLabelSecs(GKeyFloat key, String text, SecsDisplay.Units baseUnits) {
		super(key, text);
		setBaseType(baseUnits);
		updateDisplay();
	}

	@Override
	public void setBaseType(Units b) {
		m_unitsBase = b;
	}

	
	public void setHideUnits(boolean hide) {
		m_hideUnits = true;
	}
	
	@Override 
	public void setTxt(String txt) {
		if (m_hideUnits) {
			super.setTxt("");
		} else {
			super.setTxt(txt);
		}
	}
	
	@Override
	public void setDisplayType(Units units) {
		m_unitsDisplay = units;
		if (m_unitsBase == null || m_unitsDisplay == null) {
			setTxt("Secs:");
			m_scale = 1.0F;
			updateDisplay();
			return;
		}

		switch (m_unitsBase) {
		case SECS:
			if (m_unitsDisplay == m_unitsBase) {
				setTxt("Secs:");
				m_scale = 1.0F;
			} else {
				if (m_unitsDisplay == SecsDisplay.Units.MILLIS) {
					setTxt("Millis:");
					m_scale = 1000.0F;
				}
			}
			break;
		case MILLIS:
			if (m_unitsDisplay == m_unitsBase) {
				setTxt("MSecs:");
				m_scale = 1.0F;
			} else {
				if (m_unitsDisplay == SecsDisplay.Units.SECS) {
					setTxt("Secs:");
					m_scale = 0.001F;
				}
			}
		}
		updateDisplay();
	}
	
	void setScale() {
		switch (m_unitsBase) {
		case SECS:
			if (m_unitsDisplay == m_unitsBase) {
				setTxt("Secs:");
				m_scale = 1.0F;
			} else {
				if (m_unitsDisplay == SecsDisplay.Units.MILLIS) {
					setTxt("Millis:");
					m_scale = 1000.0F;
				}
			}
			break;
		case MILLIS:
			if (m_unitsDisplay == m_unitsBase) {
				setTxt("MSecs:");
				m_scale = 1.0F;
			} else {
				if (m_unitsDisplay == SecsDisplay.Units.SECS) {
					setTxt("Secs:");
					m_scale = 0.001F;
				}
			}
		}
	}
	
    @Override
    public String getValueString() {
    	String s = "--";
    	if (getBadData()) {
    		return s;
    	}
  
    	float val = getValue() * m_scale;
    	if (m_decimalFormat != null) {
    		s = m_decimalFormat.format((double) (val));
    	} else {
    		s = Float.toString(val);
    	}

    	return s;
    }
	
	
	@Override
    public void updateDisplay() {
		String vs = getValueString();
    	String s = getLeadText() + vs;
    	setText(s);
        if (!getBadData()) {
            CLog.notice(getClass()+".updateDisplay "+getKey().getName()+" s="+s+" ascii="+m_valStr+" ="+vs);
        }
    }

	@Override
	public Units getBaseType() {
		return m_unitsBase;
	}
	
	@Override
	public Units getDisplayType() {
		return m_unitsDisplay;
	}
	
	private static final long serialVersionUID = 1L;
}




