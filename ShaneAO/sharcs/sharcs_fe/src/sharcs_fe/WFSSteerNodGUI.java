package sharcs_fe;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.util.LinkedList;
import java.util.TreeMap;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.uco.gui_bits.ButtonGKeyWriteVal;
import org.uco.gui_bits.ButtonMenu;
import org.uco.gui_bits.EasyButton;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyDoubleLabel;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.gui_bits.KwLine;
import org.uco.gui_bits.LogWind;
import org.uco.java_util.CLog;
import org.uco.java_util.GKey;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyDouble;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GTag;
import org.uco.java_util.KTLKeyword;
import org.uco.java_util.NKMsg;
import org.uco.java_util.SimpleTimer;
import org.uco.java_util.StaticUtil;
import org.uco.ktljxml.KTLClientT;



public class WFSSteerNodGUI extends FrameCloseCaptured implements GKeyConnection {
	private static final long serialVersionUID = 1L;
	SteeringNoddingCentral     m_Central;    // Our central thread.
    KTLClientT          m_ktlPoco;  // Client thread communicating with poco server.
    KTLClientT          m_ktlMako;  // Client thread communicating with mako server.
    KTLClientT          m_ktlSaoCon; // Client thread communicating with saocon server.
    
    enum Direction {NW, N, NE, W, E, SW, S, SE};
    
    // GKey fields for tracking which keywords we have given messages to run.
    GTag    m_tag = new GTag(WFSSteerNodGUI.class.getName());
    
    @SuppressWarnings("rawtypes")
	LinkedList<GKey> m_gkeyList = new LinkedList<GKey>(); // Keep list of keywords that we passed notify messages to.
	LinkedList<GKeyConnection> m_gkeyConnList = new LinkedList<GKeyConnection>(); // keep this list to remove connections later.
    
    
    boolean m_GUIBuilt = false; // This will only be set to true once the GUI has been completed.
    
    
    StateLabel m_stateLabel;
    
   
    LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();
    
    TrigThrd  m_Trigger;
    
    LogWind         m_LogWind;
    
    final static int LOG_WIND      =  2;
 
    // Field steering
    GKeyDouble m_FIELDPOSX;
    GKeyDouble m_FIELDPOSY;
    GKeyInt    m_FIELDPOS_GO;
    boolean    m_fieldSteerMove = true;
    
    // Poco Moves
    GKeyDouble m_POCDERE;
    GKeyDouble m_POCRARC;
    
    // Loop
    GKeyInt m_LOOP;
    
    boolean m_pocoMove = false;
    
    // menu bar (menu item and status label)
    JPanel       m_jplMenuBar;
    ButtonMenu   m_jbtMFile;
    JPopupMenu   m_popFile;
 
    // 
    JPanel      m_jplMain;      // Main panel
    
    // direction panel
    DirectionButton m_nw;
    DirectionButton m_n;
    DirectionButton m_ne;
    DirectionButton m_w;
    DirectionButton m_e;
    DirectionButton m_se;
    DirectionButton m_s;
    DirectionButton m_sw;
    JTextField m_dirMagTF; // direction magnitude 
   
    // Loop panel
    //KeyIntLabel m_loopStatusLb;
    //ButtonGKeyWriteVal<Integer, GKeyInt> m_loopOpenBtn; 
    //ButtonGKeyWriteVal<Integer, GKeyInt> m_loopCloseBtn;
    JLabel m_loopStatusLb;
    JButton m_loopOpenBtn; 
    JButton m_loopCloseBtn;
    
    // Control which things are moved
    JCheckBox m_fieldSteerCB;
    JCheckBox m_pocoCB;
    
    @SuppressWarnings("rawtypes")
	TreeMap<String,GKey> m_GUIKeysCalc = new TreeMap<String,GKey>();
    
   
 
    

    
    /**
     * Constructor.
     * @param central
     * @param ktlclient
     */
    @SuppressWarnings("unchecked")
	WFSSteerNodGUI(SteeringNoddingCentral central, KTLClientT ktlPoco, KTLClientT ktlMako, KTLClientT ktlSaoCon) {
        super("WFSSteeringNodding");
        m_Central   = central;
        m_ktlPoco   = ktlPoco;
        m_ktlMako   = ktlMako;
        m_ktlSaoCon = ktlSaoCon;
        
        
        // setup keywords we are going to use
        m_gkeyList.add(m_FIELDPOSX = (GKeyDouble) m_ktlMako.getKeyBy("FIELDPOSX"));
        m_gkeyList.add(m_FIELDPOSY = (GKeyDouble) m_ktlMako.getKeyBy("FIELDPOSY"));
        m_gkeyList.add(m_FIELDPOS_GO = (GKeyInt) m_ktlMako.getKeyBy("FIELDPOS_GO"));
        
        // poco keywords
        m_gkeyList.add(m_POCDERE = (GKeyDouble) m_ktlPoco.getKeyBy("POCDERE"));
        m_gkeyList.add(m_POCRARC = (GKeyDouble) m_ktlPoco.getKeyBy("POCRARC"));
        
        
        // saocon keywords
        m_gkeyList.add(m_LOOP = (GKeyInt) m_ktlSaoCon.getKeyBy("LOOP"));
        
        // Create (but do not show) the LogWind
        m_LogWind = new LogWind("sharcs_fe", 20);
        m_gkeyConnList.add(m_LogWind);
        
                
        // create the frame
        GridBagLayout      framegrid = new GridBagLayout();
        GridBagConstraints framegbc  = new GridBagConstraints();
        getContentPane().setLayout(framegrid);
        
        
        // Make the menu bar.
        Color menuBtnBG = null;
        // Make the File menu 
        m_popFile     = new JPopupMenu("File");
        m_popFile.add(new JMenuItem(new ActionCreateWindow("Log...", LOG_WIND, this)));
        m_popFile.addSeparator();
        m_popFile.add(new JMenuItem(new AbstractAction("Exit"){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
        }));  
        m_jbtMFile = new ButtonMenu(m_popFile.getLabel(), m_popFile);
        m_jbtMFile.setBackground(menuBtnBG);
   
 
        // Add the Status Label
        m_stateLabel = new StateLabel("-----Steering Nodding-----", this);
        m_gkeyConnList.add(m_stateLabel);
        m_stateLabel.setPreferredSize(new Dimension(130, 25));

        
        // assemble the pieces of the menu bar
        m_jplMenuBar = new JPanel();
        // grid layout manager
        GridBagLayout menugrid     = new GridBagLayout();
        GridBagConstraints menugbc = new GridBagConstraints();
        m_jplMenuBar.setLayout(menugrid);
        m_jplMenuBar.setOpaque(true);
        menugbc.weightx = 0.0;
        menugbc.ipady   = 0;
        menugbc.anchor  = GridBagConstraints.WEST;
        menugbc.gridx = 0; menugbc.gridy = 0;
        m_jplMenuBar.add(m_jbtMFile, menugbc);
        menugbc.gridx  += 1;
        menugbc.ipadx = 20;
        menugbc.weightx = 100.0;
        menugbc.fill = GridBagConstraints.HORIZONTAL;
        m_jplMenuBar.add(m_stateLabel, menugbc);
        
        // Setup the main panel. (contains everything but the menu bar)
        m_jplMain = new JPanel();
        GridBagLayout maingrid     = new GridBagLayout();
        GridBagConstraints maingbc = new GridBagConstraints();
        m_jplMain.setLayout(maingrid);
        m_jplMain.setOpaque(true);
        maingbc.weightx = 100.0;
        maingbc.weighty = 100.0;
        maingbc.ipady   = 0;
        maingbc.anchor  = GridBagConstraints.WEST;
        maingbc.fill    = GridBagConstraints.BOTH;
        
        
        // Setup direction button panel
        JPanel jpDir = new JPanel();
        m_nw = new DirectionButton("",  Direction.NW);
        m_n  = new DirectionButton("N", Direction.N); 
        m_ne = new DirectionButton("",  Direction.NE);
        m_w  = new DirectionButton("W", Direction.W);
        m_e  = new DirectionButton("E", Direction.E);
        m_se = new DirectionButton("",  Direction.SE);
        m_s  = new DirectionButton("S", Direction.S);
        m_sw = new DirectionButton("",  Direction.SW);
        m_dirMagTF = new JTextField(4);
        m_dirMagTF.setText("5");
        m_dirMagTF.setHorizontalAlignment(JTextField.CENTER);
        GridBagLayout grid     = new GridBagLayout();
        GridBagConstraints gbc = new GridBagConstraints();
        jpDir.setLayout(grid);
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.ipady   = 0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        
        jpDir.add(m_nw,gbc);
        gbc.gridx++;
        jpDir.add(m_n, gbc);
        gbc.gridx++;
        jpDir.add(m_ne, gbc);
        
        gbc.gridx = 0;
        gbc.gridy++;
        jpDir.add(m_w,gbc);
        gbc.gridx++;
        jpDir.add(m_dirMagTF, gbc);
        gbc.gridx++;
        jpDir.add(m_e, gbc);
        
        gbc.gridx = 0;
        gbc.gridy++;
        jpDir.add(m_sw,gbc);
        gbc.gridx++;
        jpDir.add(m_s, gbc);
        gbc.gridx++;
        jpDir.add(m_se, gbc);
        
        jpDir.setBorder(new EmptyBorder(5,0,5,5));
        
        maingbc.gridx = 0;
        maingbc.gridy = 0;
        m_jplMain.add(jpDir,maingbc);
        
        
        // Field pos
        JPanel jpFieldPos = new JPanel();
        KeyDoubleLabel fieldPosX = new KeyDoubleLabel(m_FIELDPOSX, "X: ");
        KeyDoubleLabel fieldPosY = new KeyDoubleLabel(m_FIELDPOSY, "Y: ");
        fieldPosX.setHorizontalAlignment(JTextField.CENTER);
        fieldPosY.setHorizontalAlignment(JTextField.CENTER);
        jpFieldPos.setLayout(new GridBagLayout());
        gbc.weightx = 100.0;
        gbc.weighty = 0.0;
        gbc.ipady   = 0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        jpFieldPos.add(fieldPosX, gbc);
        gbc.gridx++;
        jpFieldPos.add(fieldPosY, gbc);
        maingbc.gridx = 0;
        maingbc.gridy = 1;
        m_jplMain.add(jpFieldPos,maingbc);
        
        // Loop panel
        if (m_LOOP != null) {
        	m_loopStatusLb = new KeyIntLabel(m_LOOP, "LOOP=");
        	m_loopOpenBtn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(m_LOOP, "Open Loop", 0);
        	m_loopCloseBtn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(m_LOOP, "Close Loop", 1);
        } else {
        	m_loopStatusLb = new JLabel("dummy l");
        	m_loopOpenBtn = new DummyButton("dummy open", "restart with saocon up");
        	m_loopCloseBtn = new DummyButton("dummy close", "restart with saocon up");
        }
        JPanel jpLoop = new JPanel();
        jpLoop.setLayout(new GridBagLayout());
        gbc.weightx = 100.0;
        gbc.weighty = 0.0;
        gbc.ipady   = 0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        jpLoop.add(m_loopStatusLb, gbc);
        gbc.gridy++;
        jpLoop.add(m_loopOpenBtn, gbc);
        gbc.gridy++;
        jpLoop.add(m_loopCloseBtn, gbc);
        
        jpLoop.setBorder(new EmptyBorder(5,5,5,5));
        
        maingbc.gridx = 1;
        maingbc.gridy = 0;
        m_jplMain.add(jpLoop,maingbc);
        
        
        // move panel
        JPanel jpMove = new JPanel();
        jpMove.setLayout(new GridBagLayout());
        m_fieldSteerCB = new JCheckBox("Field Steer", true);
        m_pocoCB = new JCheckBox("poco Offext", false);
        gbc.weightx = 100.0;
        gbc.weighty = 0.0;
        gbc.ipady   = 0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        jpMove.add(m_fieldSteerCB, gbc);
        gbc.gridy++;
        jpMove.add(m_pocoCB, gbc);
        
        maingbc.gridx = 2;
        maingbc.gridy = 0;
        m_jplMain.add(jpMove,maingbc);
        
        // Put the frame together and display it.
        framegbc.gridx = 0; framegbc.gridy = 0;
        framegbc.ipady = 4;
        framegbc.anchor = GridBagConstraints.NORTHWEST;
        framegbc.fill    = GridBagConstraints.HORIZONTAL;
        getContentPane().add(m_jplMenuBar, framegbc);
        
        framegbc.weightx = 100.0;
        framegbc.weighty = 100.0;
        framegbc.ipady   = 0;
        framegbc.anchor  = GridBagConstraints.WEST;
        framegbc.fill    = GridBagConstraints.BOTH;
        framegbc.gridx = 0; framegbc.gridy = 1;
        framegbc.anchor = GridBagConstraints.WEST;
        getContentPane().add(m_jplMain, framegbc);
        
        pack();

        setVisible(true);

        // All the pieces are ready, setup the initial display.
        m_GUIBuilt = true;
        
        updateDisplay();

        
        // create and start the trigger thread
        m_Trigger = new TrigThrd();
        m_Trigger.start();
        
 
    }
    

    /**
     * When this frame is closed; kill the program in a controlled fashion. 
     * This includes closing child frames.
     */
    @Override
    public void doThisOnFrameClose() {
        CLog.notice("XYStage.doThisOnFrameClose");
        removeNotifyTag();
        stopTrigger();
        
        if (m_LogWind != null) {
            m_LogWind.doThisOnFrameClose();
            m_LogWind = null;
        }

        m_Central.queHalt(); // this should stop the central thread and result in all client threads being stopped.
        super.doThisOnFrameClose();
    }
    

	/** 
     * This method should stop m_Trigger. 
     */
    public void stopTrigger() {
        CLog.notice("__GUI.stopTrigger()");
        if (m_Trigger != null) {
            // This should stop the trigger thread and cause it to be deleted.
            m_Trigger.m_bLoop = false;
            m_Trigger = null;
        }
    }
    

    /**
     * It is hard to get this function called. If you actually want code to run,
     * put it in doThisOnFrameClose.
     */
    public void finalize() throws Throwable {
        CLog.notice("__GUI.finalize()");
        super.finalize();
    }
    

    
 
    
	public void updateDisplay() {

	}
    
	@Override
	public GTag getTag() {
		return m_tag;
	}

	@Override
	public void removeNotifyTag() {
		try {
			for (@SuppressWarnings("rawtypes") GKey gk : m_gkeyList) {
				if (gk != null) {gk.removeNotifyTag(getTag());}
			}
			for (GKeyConnection gkc : m_gkeyConnList) {
				if (gkc!= null) {gkc.removeNotifyTag();}
			}
		} catch (Exception e) {
			CLog.err("WFSSteerNodGUI.removeNotifyTag error");
			CLog.exceptionStr(e);
		}
	}

	/**
	 * Move the field steering mirrors in Direction dir and mag in arcseconds.
	 * @param mag
	 * @param dir
	 */
	void fieldSteerMove(double mag, Direction dir) {
		double x = m_FIELDPOSX.getVal();
		double y = m_FIELDPOSY.getVal();

		switch (dir) {
		case NW:
			x -= mag;
			y += mag;
			break;
		case N:
			y += mag;
			break;
		case NE:
			x += mag;
			y += mag;
			break;
		case W:
			x -= mag;
			break;
		case E:
			x += mag;
			break;
		case SW:
			x -= mag;
			y -= mag;
			break;
		case S:
			y -= mag;
			break;
		case SE:
			x += mag;
			y -= mag;
			break;
		default:
			JOptionPane.showMessageDialog(m_jplMain, "Not sent. Unknown direction "+dir, "Inane error",
					JOptionPane.ERROR_MESSAGE, null);
		}
		m_FIELDPOSX.queWrite(x, null);
		m_FIELDPOSY.queWrite(y, null);
		m_FIELDPOS_GO.queWrite(1, null);
	}
	

    /**
     * Move poco
     * TODO
     * @param arcsec
     * @param dir
     */
    void pocoMove(double arcsec, Direction dir) {
    	CLog.err("MUST IMPLEMENT pocoMove");
    	double rad = StaticUtil.convArcsecToRad(arcsec);
    	double dec = 0.0;
    	double ra = 0.0;
    	switch (dir) {
    	case NW:
    		dec = rad;
    		ra  = rad;
    		break;
    	case N:
    		dec = rad;
    		break;
    	case NE:
    		dec = rad;
    		ra  = -rad; 
    		break;
    	case W:
    		ra = rad;
    		break;
    	case E:
    		ra = -rad;
    		break;
    	case SW:
    		dec = -rad;
    		ra  = rad;
    		break;
    	case S:
    		dec = -rad;
    		break;
    	case SE:
    		dec = -rad;
    		ra  = -rad;
    		break;
    	default:
    		JOptionPane.showMessageDialog(m_jplMain, "Not sent to poco. Unknown direction "+dir, "Inane error",
    				JOptionPane.ERROR_MESSAGE, null);
    		return;
    	}
    	m_POCDERE.queWrite(dec, null);
    	m_POCRARC.queWrite(ra, null);
    }
	

    class ActionCreateWindow extends AbstractAction {
        String m_text;
        int    m_WindowType;
        WFSSteerNodGUI m_parentFrm;
        public ActionCreateWindow(String text, int windowType, WFSSteerNodGUI parentFrm) {
            super(text);
            m_text = text;
            m_WindowType = windowType;
            m_parentFrm  = parentFrm;
        }

        public void actionPerformed(ActionEvent ae) {
            switch (m_WindowType) {
            case LOG_WIND:
                m_LogWind.showWind();
                break;
      
            default:
                CLog.err("ActionCreateWindow unknown "+m_text+" "+m_WindowType);
            }
        }
        
        private static final long serialVersionUID = 1L;
    }
    


    
 
    /**
     * This class produces a label that indicates if we've lost the connection
     * to the server or if the server has lost its connection to the controller.
     * @author jgates
     *
     */
    class StateLabel extends JLabel implements GKeyConnection {

		WFSSteerNodGUI m_mainFrame;
		
		GTag       m_tag = new GTag(StateLabel.class.getName());   
        GKeyInt m_gkwPOCHEART;
        GKeyInt m_gkwMakoDISPCLK;
        
        
        
        @SuppressWarnings("rawtypes")
		TreeMap<String, GKey> m_GUIKeys = new TreeMap<String, GKey>();
        
        final int OK                = 0;
        final int LOST_CON          = 1;
        final int LOST_SERV         = 2;
        int m_nLastStatus           = -1;

        TrigThrd  m_TrigThrd;
        
        String m_baseText = "basetxt";
        
        // Heartbeat items
        private int m_pocheart = -1;
        private int m_makoDispclk = -1;
        private int  m_prevPOCHEART = -1;  
        private int  m_prevMakoDISPCLK = -1;
        SimpleTimer m_TimedOutPoco = new SimpleTimer(12.0);
        SimpleTimer m_TimedOutMako = new SimpleTimer(12.0);

        StateLabel(String label, WFSSteerNodGUI mainFrame) {
            super("   "+label+"    ", CENTER);
            m_baseText = this.getText();
            m_mainFrame = mainFrame;
            
            // The state of this label is controlled by KTL keywords.
            // This is where we get information about these keywords.
            StatusIntMsg stnkmsg = new StatusIntMsg();
            @SuppressWarnings("rawtypes")
			LinkedList<GKey> gkl = new LinkedList<GKey>();
            gkl.add(m_gkwPOCHEART  = (GKeyInt) m_ktlPoco.getKeyBy("POCHEART"));
            gkl.add(m_gkwMakoDISPCLK  = (GKeyInt) m_ktlMako.getKeyBy("DISPCLK"));
            for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
            	String name = gk.getName();
            	CLog.notice(this.getClass()+" adding GUI keyword "+name);
            	gk.putNotify(getTag(), stnkmsg);
            	m_GUIKeys.put(name, gk);
            }
            
            updateDisplay();
        }
        
        class StatusIntMsg extends NKMsg implements Runnable {
			GKeyInt   m_gkw = null;
        	Integer   m_val = null;
        	StatusIntMsg() {};
        	StatusIntMsg(GKeyInt gkw, Integer val) {
        		m_gkw = gkw; m_val = val;
        	}
        	
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				// Run in client thread
				// Must create a new message to avoid race conditions.
				GKeyInt gki = (GKeyInt)kd;
				SwingUtilities.invokeLater(new StatusIntMsg(gki, gki.getVal()));
			}

			@Override
			public void run() {
				// Run in swing thread
				if (m_gkw == m_gkwPOCHEART) {
					if (m_val instanceof Integer) {
						m_pocheart = (Integer)m_val;
					}
				} 
				if (m_gkw == m_gkwMakoDISPCLK) {
					if (m_val instanceof Integer) {
						m_makoDispclk = (Integer)m_val;
					}
				}
				updateDisplay();
			}
        }



        public void updateDisplay() {
        	if ( m_pocheart != m_prevPOCHEART ) {
        		m_prevPOCHEART = m_pocheart;
    			m_TimedOutPoco.reset(); 
    		}
        	if ( m_makoDispclk != m_prevMakoDISPCLK ) {
        		m_prevMakoDISPCLK = m_makoDispclk;
    			m_TimedOutMako.reset(); 
    		}
        	
            if (m_TimedOutPoco.waitIsOver()) {
                setText("Lost Server Poco");
                setBackground(Color.RED);
                setOpaque(true);
                if (m_nLastStatus != LOST_SERV) {
                    m_nLastStatus = LOST_SERV;
                    m_LogWind.addMsg("Lost connection to the server");
                    //m_mainFrame.sendPack();
                }
            } else if (m_TimedOutMako.waitIsOver()) {
                setText("Lost Server Mako");
                setBackground(Color.RED);
                setOpaque(true);
                if (m_nLastStatus != LOST_SERV) {
                    m_nLastStatus = LOST_SERV;
                    m_LogWind.addMsg("Lost connection to the server");
                    //m_mainFrame.sendPack();
                }
            } else {
                setText(m_baseText);
                setBackground(null);
                setOpaque(false);
                if (m_nLastStatus != OK) {
                    m_nLastStatus = OK;
                    m_LogWind.addMsg("Connected");
                    //m_mainFrame.sendPack();
                }
            } 
        }
        
		@Override
		public GTag getTag() {
			return m_tag;
		}
		@Override
		public void removeNotifyTag() {
			try {	
				m_gkwPOCHEART.removeNotifyTag(getTag());
				m_gkwMakoDISPCLK.removeNotifyTag(getTag());
			} catch (Exception e) {
				CLog.err("WFSSteerNodGUI.removeNotifyTag error");
				CLog.exceptionStr(e);
			}
		}
		
		private static final long serialVersionUID = 1L;
    }

        
    
    class DirectionButton extends EasyButton {
		private static final long serialVersionUID = 1L;
		final public int NW = 1;
		
		private Direction m_dir;

		DirectionButton(String text, Direction dir) {
    		super(text);
    		m_dir = dir;
    	}
    	
    	@Override
    	public void doThisOnClick() {
    		super.doThisOnClick();
    		String txt = m_dirMagTF.getText();
    		double mag = 0;
    		try {
    			mag = Double.parseDouble(txt);
    		} catch (NumberFormatException ne) {
    			JOptionPane.showMessageDialog(m_jplMain, "Not sent. Could not convert "+txt, "Inane error",
    					JOptionPane.ERROR_MESSAGE, null);
    			return;
    		}
    		
    		if (m_fieldSteerCB.isSelected()) {
    			fieldSteerMove(mag, m_dir);
    		}
    		
    		if (m_pocoCB.isSelected()) {
    			pocoMove(mag, m_dir);
    		}
    	}

    }
  
    class DummyButton extends EasyButton {
		private static final long serialVersionUID = 1L;
		private String m_msg = "placeholder";
    	
    	DummyButton(String text, String msg) {
    		super(text);
    		m_msg = msg;
    	}
    	
    	@Override
    	public void doThisOnClick() {
    		super.doThisOnClick();
    		JOptionPane.showMessageDialog(m_jplMain, "This is a place holder:"+m_msg, "Inane error",
					JOptionPane.ERROR_MESSAGE, null);
    	}
    }
    
    
    /**
     * This thread simply sleeps for a while and then calls appropriate GUI
     * functions when it wakes up.
     * @author jgates
     *
     */
    class TrigThrd extends Thread {
        public volatile boolean m_bLoop = true;

        public void run() {
            try {
                while (m_bLoop) {
                    sleep(5000);
                    if (m_bLoop) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                // things to run every 5 seconds 
                            	m_stateLabel.updateDisplay();
                            }
                        });
                    }
                }
            } catch (Exception e) {
                CLog.exceptionStr(e);
            }
        }
    }

    protected void sendPack() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                pack();
            }
        });
    }

    
    

}

