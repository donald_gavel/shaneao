package sharcs_fe;

import javax.swing.SwingUtilities;

import org.uco.java_util.CLog;
import org.uco.ktljxml.CentralT;
import org.uco.ktljxml.KTLClientT;
import org.uco.ktljxml.KTLClientTStatus;


public class TargetOffsetFE  {
    /** 
     * main program entry, it just creates and starts the central thread.
     */
    public static void main(String argv[]) 
    {
    	String progName = "targetoffset_fe";
        int logType = CLog.SYSLOG;
        for (int j = 0; j < argv.length; j++) {
            String arg = argv[j];        
            if (arg.compareTo("-system") == 0)  {
                logType = CLog.SYSTEM;
            }
        }
        if (logType == CLog.SYSLOG) {
            CLog.setup(progName, CLog.SYSLOG);
        }
        
        try {
            CLog.err("********* "+progName+" starting **********");
            TargetOffsetFECentral cent = new TargetOffsetFECentral("CentralThrd");

            cent.start();

            cent.join();
            CLog.notice("Central thread joined.");
            CLog.err("********* "+progName+" ending *********");
            System.exit(0);
        } catch (Exception e) {
            CLog.exceptionStr(e);
        }
    }
}




/**
 * This class is the central thread for xystage_fe. It creates the KTLClientThrd
 * m_ClientThrd to talk to the the sharcs server. Then it creates the GUI thread.
 * After that, it mostly passes keyword value updates it receives from m_ClientThrd
 * and passes them to the GUI thread. The GUI thread usually sends keyword change 
 * requests directly to m_ClientThrd.
 * @author jgates
 *
 */
class TargetOffsetFECentral extends CentralT
{
    // Create the client thread for communicating with the xystage server.
    KTLClientT m_ClientThrd;             
    KTLClientTStatus m_ClientThrdStatus; // An object for mirroring the status of m_ClientThrd.

    volatile TargetOffsetGUI m_GUI; // The GUI.

    String m_service = "poco";

    TargetOffsetFECentral(String name) {
        super(name);
    }


    
    @Override
    protected void setupKTLClients() {
        //      Setup our KTL client thread for the offsetGrid
        m_ClientThrdStatus = new KTLClientTStatus();
        m_ClientThrd = new KTLClientT(m_service, this);
        m_ClientThrd.setupNames("KTLClient", m_service, "");
        
        if (m_ClientThrd.setupKTLServiceFromXML() < 0) {
            CLog.err("FAILED to setup m_ClientThrd TERMINATING");
            System.exit(-1);
        }
        
        m_mapClients.put(m_ClientThrd, new KTLClientElement(m_ClientThrd, m_ClientThrdStatus));
    }
    
    @Override
    protected void setupGUI() {
        class tmpMsg implements Runnable {
            public TargetOffsetFECentral m_central;
            public KTLClientT  m_clientThrd;
            tmpMsg(TargetOffsetFECentral central, KTLClientT clientThrd) {
                m_central    = central;
                m_clientThrd = clientThrd;
            }
            public void run() {
                m_GUI = new TargetOffsetGUI(m_central, m_clientThrd);
            }
        }
        
        // Setup the GUI
        SwingUtilities.invokeLater(new tmpMsg(this, m_ClientThrd));
    }

 
}

