package sharcs_fe;










import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.TreeMap;


import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.CLog;
import org.uco.java_util.GKey;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.java_util.KTLKeyword;
import org.uco.java_util.NKMsg;
import org.uco.ktljxml.KTLClientT;



/**
 * TODO delete this class
 * @author jgates
 *
 */

public class WindowFrameOld extends FrameCloseCaptured implements GKeyConnection {
	// STARTX, STARTY, ENDX, ENDY
	// CRPIX1, CRPIX2
	// CRVAL1, CRVAL2
	// READTIME
	// ARRAYCOLS, ARRAYROWS
	SharcsGUI m_gui;
	WindowDrawing m_wd = null;
	LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();
	
	KwLine m_kwlSTARTX;
	KwLine m_kwlENDX;
	KwLine m_kwlSTARTY;
	KwLine m_kwlENDY;
	KwLine m_kwlCRPIX1;
	KwLine m_kwlCRPIX2;
	KwLine m_kwlCRVAL1;
	KwLine m_kwlCRVAL2;
	KwLine m_kwlREADTIME;
	KwLine m_kwlARRAYCOLS;
	KwLine m_kwlARRAYROWS;
	
	private GTag m_tag = new GTag(WindowFrameOld.class.getName());

	
	public WindowFrameOld(SharcsGUI gui, KTLClientT ktlclient) {
		super("Sharcs Window");
		m_gui = gui;
		
		JPanel main = new JPanel();
		getContentPane().add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints main_gbc = new GridBagConstraints();
		main_gbc.fill = GridBagConstraints.BOTH;
		main_gbc.weightx = 100.0; main_gbc.weighty = 100.0;
		
		main_gbc.gridx = 5; main_gbc.gridy = 0;
		JPanel dataPnl = new JPanel();
		main.add(dataPnl, main_gbc);
		dataPnl.setLayout(new GridBagLayout());
		GridBagConstraints data_gbc = new GridBagConstraints();
		GridBagConstraints gbc = data_gbc;

		try {
			m_kwlSTARTX    = new KwLine("start x",   ktlclient.getKeyBy("STARTX"),    m_gParms);
			m_kwlENDX      = new KwLine("end x",     ktlclient.getKeyBy("ENDX"),      m_gParms);
			m_kwlSTARTY    = new KwLine("stary y",   ktlclient.getKeyBy("STARTY"),    m_gParms);
			m_kwlENDY      = new KwLine("end y",     ktlclient.getKeyBy("ENDY"),      m_gParms);
			m_kwlCRPIX1    = new KwLine("CRPIX1",    ktlclient.getKeyBy("CRPIX1"),    m_gParms);
			m_kwlCRPIX2    = new KwLine("CRPIX2",    ktlclient.getKeyBy("CRPIX2"),    m_gParms);
			m_kwlCRVAL1    = new KwLine("CRVAL1",    ktlclient.getKeyBy("CRVAL1"),    m_gParms);
			m_kwlCRVAL2    = new KwLine("CRVAL2",    ktlclient.getKeyBy("CRVAL2"),    m_gParms);
			m_kwlREADTIME  = new KwLine("READTIME",  ktlclient.getKeyBy("READTIME"),  m_gParms);
			m_kwlARRAYCOLS = new KwLine("ARRAYCOLS", ktlclient.getKeyBy("ARRAYCOLS"), m_gParms);
			m_kwlARRAYROWS = new KwLine("ARRAYROWS", ktlclient.getKeyBy("ARRAYROWS"), m_gParms);
		} catch (GeneralException ge) {
        	CLog.err(this+".WindowFrame caught exception in parameters panel.");
        	CLog.exceptionStr(ge);
        	System.exit(-1);
        }
		
		int ii = 0;
        int colName   = ii++;
        int colCurr   = ii++;
        int colTF     = ii++;
        int colBtn    = ii++;
        gbc.gridy = 5;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.anchor  = GridBagConstraints.CENTER;
        ii = gbc.gridy;
        KwLine kwlItems[] = { m_kwlSTARTX, m_kwlSTARTY, m_kwlENDX, m_kwlENDY, m_kwlCRPIX1, m_kwlCRPIX2, 
        		m_kwlCRVAL1, m_kwlCRVAL2, m_kwlREADTIME, m_kwlARRAYCOLS, m_kwlARRAYROWS };
        for (KwLine kwl:kwlItems) {
        	gbc.gridy = ii++;
        	kwl.place(dataPnl, gbc, colName, colCurr, colTF, colBtn);
        }
        
        
        m_wd = new WindowDrawing();
        main_gbc.gridx = 1; main_gbc.gridy = 0;
        main.add(m_wd, main_gbc);
	}

	

	@Override
	public GTag getTag() {
		return m_tag;
	}
	@Override
	public void removeNotifyTag() {
		// no messages in this class, but there are some in the WindowDrawing class
		m_wd.removeNotifyTag();
	}
	

	/**
	 * Draw the part of the chip that is being used to record the image. 
	 *
	 */
	class WindowDrawing extends JPanel implements GKeyConnection{
		Color m_color = Color.black;
		
		private GTag m_wtag = new GTag(WindowDrawing.class.getName());
		
		double m_maxENDX = 2047;
		double m_maxENDY = 2047;
		

		
		@SuppressWarnings("rawtypes")
		TreeMap<String, GKey> m_GUIKeys = new TreeMap<String, GKey>();

		private double m_left;
		private double m_right;
		private double m_top;
		private double m_bottom;
		
		WindowDrawing() {
			super();
			setPreferredSize(new Dimension(75,75));
			@SuppressWarnings("rawtypes")
			LinkedList<GKey> gkl = new LinkedList<GKey>();
			gkl.add(m_kwlSTARTX.getKeyword());
			gkl.add(m_kwlENDX.getKeyword());
			gkl.add(m_kwlSTARTY.getKeyword());
			gkl.add(m_kwlENDY.getKeyword());
			for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
				String name = gk.getName();
				CLog.notice(this.getClass()+" adding GUI keyword "+name);
				NKMsg nmsg = new WindowDrawMsg();
				gk.putNotify(m_tag, nmsg);
				m_GUIKeys.put(name, gk);
			}


			setBackground(Color.white);
			setOpaque(true);
			updateDisplay();
		}
		
		class WindowDrawMsg extends NKMsg implements Runnable{
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				// if any fields are set here, a new instance would need to be made.
				SwingUtilities.invokeLater(this);
			}
			@Override
			public void run() {
				updateDisplay();
			}			
		}
		

		public void updateDisplay() {
			m_left   = ((GKeyInt)(m_kwlSTARTX.getKeyword())).getVal() / m_maxENDX;
			m_right  = ((GKeyInt)(m_kwlENDX.getKeyword())).getVal() / m_maxENDX;
			m_bottom = (m_maxENDY -  ((GKeyInt)(m_kwlSTARTY.getKeyword())).getVal()) / m_maxENDY ;
			m_top    = (m_maxENDY - ((GKeyInt)(m_kwlENDY.getKeyword())).getVal()) / m_maxENDY ;
			repaint();
		}
	      
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g;

			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_OFF);

			double w = getWidth();
			double h = getHeight();
			
			// adjust for JPanel size.
			double left   = m_left * (w -1);  // -1 shows edge of box
			double right  = m_right * (w -1);
			double top    = m_top * (h - 1);
			double bottom = m_bottom * (h - 1);

			// Save the original stroke and make our own.
			Stroke strokeOriginal = g2.getStroke();
			BasicStroke s = new BasicStroke(1.0f,
					BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER);
			g2.setStroke(s);
			g2.setColor(m_color);

			// Draw a box
			Rectangle2D rect = new Rectangle2D.Double(left, top, right - left, bottom - top);
			g2.setColor(Color.green);
			g2.fill(rect);
			g2.setColor(Color.black);
			g2.draw(rect);
			
			// Draw box with red X to indicate bad part of chip
			s = new BasicStroke(2.0f,
					BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER);
			g2.setStroke(s);
			g2.setColor(Color.RED);
			left   = (0.0 / m_maxENDX)       * (w -1); // -1 shows edge of box
			right  = (m_maxENDX / m_maxENDX) * (w -1);
			top    = ((m_maxENDY - m_maxENDY)/m_maxENDY) * (h - 1);
			bottom = ((m_maxENDY - 1458)/m_maxENDY) * (h - 1);
			rect = new Rectangle2D.Double(left, top, right - left, bottom - top);
			g2.draw(rect);
			Line2D.Double ln = new Line2D.Double(left,top,right,bottom);
			g2.draw(ln);
			ln = new Line2D.Double(right,top,left,bottom);
			g2.draw(ln);
			
			// Restore original stroke.
			g2.setStroke(strokeOriginal);
		}
		
		@Override
		public GTag getTag() {
			return m_wtag;
		}
		@Override
		public void removeNotifyTag() {
			for (@SuppressWarnings("rawtypes") GKey gkw : m_GUIKeys.values()) {
				gkw.removeNotifyTag(m_wtag);
			}
		}
		
		private static final long serialVersionUID = 1L;


	}
	private static final long serialVersionUID = 1L;


}

