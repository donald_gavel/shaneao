package sharcs_fe;

import javax.swing.SwingUtilities;

import org.uco.java_util.CLog;
import org.uco.java_util.StaticUtil;
import org.uco.ktljxml.CentralT;
import org.uco.ktljxml.KTLClientT;
import org.uco.ktljxml.KTLClientTStatus;
import org.uco.ktljxml.CentralT.KTLClientElement;

public class WFSSteeringNodding {
    /** 
     * main program entry, it just creates and starts the central thread.
     */
    public static void main(String argv[]) 
    {
    	StaticUtil.setLROOT();
    	StaticUtil.setKROOT();

    	
    	String progName = "WFSSteeringNodding_UI";
        CLog.setup(progName, CLog.FILES, argv);
        
        
        try {
            CLog.err("********* "+progName+" starting **********");
            SteeringNoddingCentral cent = new SteeringNoddingCentral("CentralThrd", progName);

            cent.start();

            cent.join();
            CLog.notice("Central thread joined.");
            CLog.err("********* "+progName+" ending *********");
            System.exit(0);
        } catch (Exception e) {
            CLog.exceptionStr(e);
        }
    }
}



/**
 * This class is the central thread for xystage_fe. It creates the KTLClientThrd
 * m_ClientThrd to talk to the the sharcs server. Then it creates the GUI thread.
 * After that, it mostly passes keyword value updates it receives from m_ClientThrd
 * and passes them to the GUI thread. The GUI thread usually sends keyword change 
 * requests directly to m_ClientThrd.
 * @author jgates
 *
 */
class SteeringNoddingCentral extends CentralT
{
    // Create the client threads
    KTLClientT m_clientThrdPoco;             
    KTLClientTStatus m_clientThrdPocoStatus; // An object for mirroring the status of m_ClientThrd.
    
    KTLClientT m_clientThrdMako;             
    KTLClientTStatus m_clientThrdMakoStatus;
    
    KTLClientT m_clientThrdSaoCon;             
    KTLClientTStatus m_clientThrdSaoConStatus; // An object for mirroring the status of m_ClientThrd.

    volatile WFSSteerNodGUI m_GUI; // The GUI.

    String m_servicePoco = "poco";
    String m_serviceMako = "mako";
    String m_serviceSaoCon = "saocon";
    
    private String mProgName = "";

    SteeringNoddingCentral(String threadname, String progName) {
        super(threadname);
        mProgName = progName;
    }


    
    @Override
    protected void setupKTLClients() {
    	// poco
        m_clientThrdPocoStatus = new KTLClientTStatus();
        m_clientThrdPoco = new KTLClientT(m_servicePoco, this);
        m_clientThrdPoco.setupNames(mProgName, m_servicePoco, "");
        if (m_clientThrdPoco.setupKTLServiceFromXML() < 0) {
            CLog.err("FAILED to setup m_clientThrdPoco TERMINATING");
            System.exit(-1);
        }
        m_mapClients.put(m_clientThrdPoco, new KTLClientElement(m_clientThrdPoco, m_clientThrdPocoStatus));
        
        // mako
        m_clientThrdMakoStatus = new KTLClientTStatus();
        m_clientThrdMako = new KTLClientT(m_serviceMako, this);
        m_clientThrdMako.setupNames(mProgName, m_serviceMako, "");
        if (m_clientThrdMako.setupKTLServiceFromXML() < 0) {
            CLog.err("FAILED to setup m_clientThrdMako TERMINATING");
            System.exit(-1);
        }
        m_mapClients.put(m_clientThrdMako, new KTLClientElement(m_clientThrdMako, m_clientThrdMakoStatus));
        
        // saocon
        m_clientThrdSaoConStatus = new KTLClientTStatus();
        m_clientThrdSaoCon = new KTLClientT(m_serviceSaoCon, this);
        m_clientThrdSaoCon.setupNames(mProgName, m_serviceSaoCon, "");
        if (m_clientThrdSaoCon.setupKTLServiceFromXML() < 0) {
            CLog.err("FAILED to setup m_clientThrdSaoCon TERMINATING");
            System.exit(-1);
        }
        m_mapClients.put(m_clientThrdSaoCon, new KTLClientElement(m_clientThrdSaoCon, m_clientThrdSaoConStatus));
        
    }
    
    @Override
    protected void setupGUI() {
        class tmpMsg implements Runnable {
            public SteeringNoddingCentral m_central;
            public KTLClientT  m_clientPocoT;
            public KTLClientT  m_clientMakoT;
            public KTLClientT  m_clientSaoConT;
            tmpMsg(SteeringNoddingCentral central, KTLClientT clientPocoT, KTLClientT clientMakoT, KTLClientT clientSaoConT) {
                m_central    = central;
                m_clientPocoT = clientPocoT;
                m_clientMakoT = clientMakoT;
                m_clientSaoConT = clientSaoConT;
            }
            public void run() {
                m_GUI = new WFSSteerNodGUI(m_central, m_clientPocoT, m_clientMakoT, m_clientSaoConT);
            }
        }
        
        // Setup the GUI
        SwingUtilities.invokeLater(new tmpMsg(this, m_clientThrdPoco, m_clientThrdMako,m_clientThrdSaoCon));
    }
 
}
