package sharcs_fe;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.util.LinkedList;
import java.util.TreeMap;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.CLog;
import org.uco.java_util.GKey;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.java_util.KTLKeyword;
import org.uco.java_util.NKMsg;
import org.uco.ktljna_j.KTLJNAService;


public class WindowFrame extends FrameCloseCaptured implements GKeyConnection {

	SharcsGUI mGui;
	WindowDrawing mWd = null;
	LinkedList<KwLine> mGParms = new LinkedList<KwLine>();
	
	final static int mMaxX = 2047;
	final static int mMinX = 0;
	final static int mMaxY = 2047;
	final static int mMinY = 0;
	
	KwLine mKwlSTARTX;
	KwLine mKwlENDX;
	KwLine mKwlSTARTY;
	KwLine mKwlENDY;
	
	GKeyInt   m_gkwSTARTX;
	GKeyInt   m_gkwENDX;
	GKeyInt   m_gkwSTARTY;
	GKeyInt   m_gkwENDY;


	KeyIntLabel mARRAYCOLS_val;
	KeyIntLabel mARRAYROWS_val;
	
	private GTag m_tag = new GTag(WindowFrameOld.class.getName());
	private KwLine[] mKwlItems;

	public boolean fullWindow() {
		return (
			m_gkwSTARTX.getVal() == mMinX &&
			m_gkwENDX.getVal()   == mMaxX &&
			m_gkwSTARTY.getVal() == mMinY &&
			m_gkwENDY.getVal()   == mMaxY
			);
	}
	
	public WindowFrame(SharcsGUI gui) throws GeneralException {
		super("Sharcs Window");
		mGui = gui;
		KTLJNAService serv = mGui.getServSharcs();
		
		Container pan = getContentPane();
		pan.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		pan.add(bottomPanel, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn);
		
		
		JPanel main = new JPanel();
		pan.add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints main_gbc = new GridBagConstraints();
		main_gbc.fill = GridBagConstraints.BOTH;
		main_gbc.weightx = 100.0; main_gbc.weighty = 100.0;
		
		main_gbc.gridx = 5; main_gbc.gridy = 0;
		JPanel dataPnl = new JPanel();
		main.add(dataPnl, main_gbc);
		dataPnl.setLayout(new GridBagLayout());
		GridBagConstraints data_gbc = new GridBagConstraints();
		GridBagConstraints gbc = data_gbc;
		
		mARRAYCOLS_val = new KeyIntLabel((GKeyInt) serv.getGKey("ARRAYCOLS"), "");
		mARRAYROWS_val = new KeyIntLabel((GKeyInt) serv.getGKey("ARRAYROWS"), "");
		m_gkwSTARTX  = (GKeyInt) serv.getGKey("STARTX");
		m_gkwENDX    = (GKeyInt) serv.getGKey("ENDX");
		m_gkwSTARTY  = (GKeyInt) serv.getGKey("STARTY");
		m_gkwENDY    = (GKeyInt) serv.getGKey("ENDY");
		mKwlSTARTX    = new KwLine("start x",   m_gkwSTARTX,    mGParms);
		mKwlENDX      = new KwLine("end x",     m_gkwENDX,      mGParms);
		mKwlSTARTY    = new KwLine("stary y",   m_gkwSTARTY,    mGParms);
		mKwlENDY      = new KwLine("end y",     m_gkwENDY,      mGParms);
		
		int ii = 0;
        int colName   = ii++; // name column
        int colCurr   = ii++; // current value column
        int colTF     = ii++; // text field column
        int colBtn    = ii++; // set button column
        Border border = new EmptyBorder(0,10,0,0);
        
        gbc.gridy = 5;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.anchor  = GridBagConstraints.CENTER;
        ii = gbc.gridy;
        KwLine kwlItems[] = { mKwlSTARTX, mKwlENDX, mKwlSTARTY, mKwlENDY};
        mKwlItems = kwlItems;
        for (KwLine kwl:kwlItems) {
        	gbc.gridy = ii++;
        	kwl.m_current.setBorder(border);
        	kwl.place(dataPnl, gbc, colName, colCurr, colTF, colBtn);
        }
        
        gbc.gridy++;
        gbc.gridx = colName;
        gbc.weightx = 0.0;
        dataPnl.add(new JLabel("ArrayCols"), gbc);
        gbc.gridx = colCurr;
        gbc.weightx = 40.0;
        mARRAYCOLS_val.setBorder(border);
        dataPnl.add(mARRAYCOLS_val, gbc);
        
        gbc.gridy++;
        gbc.gridx = colName;
        gbc.weightx = 0.0;dataPnl.add(new JLabel("ArrayRows"), gbc);
        gbc.gridx = colCurr;
        gbc.weightx = 40.0;
        mARRAYROWS_val.setBorder(border);
        dataPnl.add(mARRAYROWS_val, gbc);
         
        // Add panel to show active window
        mWd = new WindowDrawing();
        main_gbc.gridx = 1; main_gbc.gridy = 0;
        main.add(mWd, main_gbc);
        
        // Make default window size button panel
        JButton fullBtn = new JButton("Full");
        fullBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setFull();
			}
        });
        JButton centerZoomBtn = new JButton("Center Zoom");
        centerZoomBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				setCenterZoom();
			}
        });
        JPanel defaultBtns = new JPanel();
        defaultBtns.setLayout(new GridBagLayout());
        GridBagConstraints dbgbc = new GridBagConstraints();
        dbgbc.weightx = 100.0;
        dbgbc.weighty = 5.0;
        dbgbc.fill    = GridBagConstraints.BOTH;
        dbgbc.gridx = 0; dbgbc.gridy = 0;
        defaultBtns.add(centerZoomBtn, dbgbc);
        dbgbc.gridx++;
        defaultBtns.add(fullBtn, dbgbc);
        
        main_gbc.gridx = 0; main_gbc.gridy = 2;
        main_gbc.gridwidth = 14;
        main_gbc.weighty = 1.0;
        main.add(defaultBtns, main_gbc);
        
        this.pack();
	}

	

	@Override
	public GTag getTag() {
		return m_tag;
	}
	@Override
	public void removeNotifyTag() {
		// no messages in this class, but there are some in the WindowDrawing class
		mWd.removeNotifyTag();
		for (KwLine kwl:mKwlItems) {
        	kwl.removeNotifyTag();
        }
		mARRAYCOLS_val.removeNotifyTag();
		mARRAYROWS_val.removeNotifyTag();
	}
	
	/**
	 * Set keywords to use the full window.
	 */
	private void setFull() {
		mKwlSTARTX.m_kw.queWriteAscii(Integer.toString(mMinX), null);
		mKwlENDX.m_kw.queWriteAscii(Integer.toString(mMaxX), null);
		mKwlSTARTY.m_kw.queWriteAscii(Integer.toString(mMinY), null);
		mKwlENDY.m_kw.queWriteAscii(Integer.toString(mMaxY), null);
	}
	
	/**
	 * startx=1125, endx=1274, starty=625, endy=774
	 */
	private void setCenterZoom() {
		mKwlSTARTX.m_kw.queWriteAscii("1125", null);
		mKwlENDX.m_kw.queWriteAscii("1274", null);
		mKwlSTARTY.m_kw.queWriteAscii("625", null);
		mKwlENDY.m_kw.queWriteAscii("774", null);
	}

	/**
	 * Draw the part of the chip that is being used to record the image. 
	 *
	 */
	class WindowDrawing extends JPanel implements GKeyConnection{
		Color m_color = Color.black;
		
		private GTag m_wtag = new GTag(WindowDrawing.class.getName());
		
		double m_maxENDX = 2047;
		double m_maxENDY = 2047;
		
		@Override
		public Dimension getPreferredSize() {
			Dimension d = super.getPreferredSize();
			Container c = getParent();
			if (c != null) {
				d = c.getSize();
			} else {
				return new Dimension(90, 90);
			}
			int w = (int) d.getWidth();
			int h = (int) d.getHeight();
			int s = (w < h ? w : h);
			return new Dimension(s, s);
		}
		
		@SuppressWarnings("rawtypes")
		TreeMap<String, GKey> m_GUIKeys = new TreeMap<String, GKey>();

		private double m_left;
		private double m_right;
		private double m_top;
		private double m_bottom;
		
		WindowDrawing() {
			super();
			setPreferredSize(new Dimension(75,75));
			@SuppressWarnings("rawtypes") LinkedList<GKey> gkl = new LinkedList<GKey>();
			gkl.add(mKwlSTARTX.getKeyword());
			gkl.add(mKwlENDX.getKeyword());
			gkl.add(mKwlSTARTY.getKeyword());
			gkl.add(mKwlENDY.getKeyword());
			for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
				String name = gk.getName();
				CLog.notice(this.getClass()+" adding GUI keyword "+name);
				NKMsg nmsg = new WindowDrawMsg();
				gk.putNotify(m_tag, nmsg);
				m_GUIKeys.put(name, gk);
			}


			setBackground(Color.white);
			setOpaque(true);
			updateDisplay();
		}
		
		class WindowDrawMsg extends NKMsg implements Runnable{
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				// if any fields are set here, a new instance would need to be made.
				SwingUtilities.invokeLater(this);
			}
			@Override
			public void run() {
				updateDisplay();
			}			
		}
		

		public void updateDisplay() {
			m_left   = ((GKeyInt)(mKwlSTARTX.getKeyword())).getVal() / m_maxENDX;
			m_right  = ((GKeyInt)(mKwlENDX.getKeyword())).getVal() / m_maxENDX;
			m_bottom = (m_maxENDY -  ((GKeyInt)(mKwlSTARTY.getKeyword())).getVal()) / m_maxENDY ;
			m_top    = (m_maxENDY - ((GKeyInt)(mKwlENDY.getKeyword())).getVal()) / m_maxENDY ;
			repaint();
		}
	      
		
		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);
			Graphics2D g2 = (Graphics2D)g;

			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
					RenderingHints.VALUE_ANTIALIAS_OFF);

			double w = getWidth();
			double h = getHeight();
			
			if (w > h) {
				w = h;
			} else if (h > w) {
				h = w;
			}
			
			if ( m_left >= m_right || m_top >= m_bottom  ) {
				// Draw a red rectangle.
				Rectangle2D rect = new Rectangle2D.Double(0.0, 0.0, w, h);
				g2.setColor(Color.RED);
				g2.fill(rect);
				String s= "INVALID SIZE";
				g2.setColor(Color.WHITE);
				g2.drawChars(s.toCharArray(), 0, s.length(), 10, 20);
				return;
			}
			
			// adjust for JPanel size.
			double left   = m_left * (w -1);  // -1 shows edge of box
			double right  = m_right * (w -1);
			double top    = m_top * (h - 1);
			double bottom = m_bottom * (h - 1);

			// Save the original stroke and make our own.
			Stroke strokeOriginal = g2.getStroke();
			BasicStroke s = new BasicStroke(1.0f,
					BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER);
			g2.setStroke(s);
			g2.setColor(m_color);

			// Draw a black box indicating whole chip, will be covered by green box.
			Rectangle2D rect = new Rectangle2D.Double(0.0, 0.0, w, h);
			g2.setColor(Color.black);
			g2.fill(rect);
			// Draw a green box to show active part of the chip
			rect = new Rectangle2D.Double(left, top, right - left, bottom - top);
			g2.setColor(Color.green);
			g2.fill(rect);
			// Draw black outline around edge on top of green, looks better
			g2.setColor(Color.black);
			g2.draw(rect);
			
			// Draw box with red X to indicate bad part of chip
			s = new BasicStroke(2.0f,
					BasicStroke.CAP_BUTT,
					BasicStroke.JOIN_MITER);
			g2.setStroke(s);
			g2.setColor(Color.RED);
			left   = (0.0 / m_maxENDX)       * (w -1); // -1 shows edge of box
			right  = (m_maxENDX / m_maxENDX) * (w -1);
			top    = ((m_maxENDY - m_maxENDY)/m_maxENDY) * (h - 1);
			bottom = ((m_maxENDY - 1458)/m_maxENDY) * (h - 1);
			rect = new Rectangle2D.Double(left, top, right - left, bottom - top);
			g2.draw(rect);
			Line2D.Double ln = new Line2D.Double(left,top,right,bottom);
			g2.draw(ln);
			ln = new Line2D.Double(right,top,left,bottom);
			g2.draw(ln);
			
			// Restore original stroke.
			g2.setStroke(strokeOriginal);
		}
		
		@Override
		public GTag getTag() {
			return m_wtag;
		}
		@Override
		public void removeNotifyTag() {
			for (@SuppressWarnings("rawtypes") GKey gkw : m_GUIKeys.values()) {
				gkw.removeNotifyTag(m_wtag);
			}
		}
		
		private static final long serialVersionUID = 1L;


	}
	private static final long serialVersionUID = 1L;


}
