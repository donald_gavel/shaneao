package sharcs_fe;


import javax.swing.SwingUtilities;


import org.uco.java_util.CLog;
import org.uco.java_util.EventThrd;
import org.uco.java_util.StaticUtil;
import org.uco.ktljna_j.KTLJNALibInstance;
import org.uco.ktljxml.CentralT;
import org.uco.ktljxml.KTLClientT;
import org.uco.ktljxml.KTLClientTStatus;



public class SharcsFE {
    /** 
     * main program entry, it just creates and starts the central thread.
     */
    public static void main(String argv[]) 
    {
    	StaticUtil.setLROOT();
    	StaticUtil.setKROOT();

    	
    	String progName = "sharcs_fe";
        CLog.setup(progName, CLog.FILES, argv);
        
        // Check arguments for advanced mode, otherwise it runs in AUTO mode.
        boolean advancedMode = false;
        for(String s:argv) {
        	if (s.compareToIgnoreCase("-advanced") == 0) {
        		advancedMode = true;
        	}
        }
        
        
        try {
            CLog.err("********* "+progName+" starting **********");
            SharcsFECentral cent = new SharcsFECentral("CentralThrd", advancedMode);

            cent.start();

            cent.join();
            CLog.notice("Central thread joined.");
            CLog.err("********* "+progName+" ending *********");
            System.exit(0);
        } catch (Exception e) {
            CLog.exceptionStr(e);
        }
    }
}




/**
 * This class is the central thread for xystage_fe. It creates the KTLClientThrd
 * m_ClientThrd to talk to the the sharcs server. Then it creates the GUI thread.
 * After that, it mostly passes keyword value updates it receives from m_ClientThrd
 * and passes them to the GUI thread. The GUI thread usually sends keyword change 
 * requests directly to m_ClientThrd.
 * @author jgates
 *
 */
class SharcsFECentral extends EventThrd {
    
	// The rubber meets the road here. Open the ktljna library. 
	private KTLJNALibInstance m_ktlInst = new KTLJNALibInstance(StaticUtil.getKROOT()+"/lib/");
	
	
    volatile SharcsGUI m_GUI; // The GUI.


	private boolean m_advancedMode;

    SharcsFECentral(String name, boolean advancedMode) {
        super(name);
        m_advancedMode = advancedMode;
        
        // ktlInst is a subclass of EventThrd, it must be started so
        // that it can handle events.
        m_ktlInst.start();  
    }


    @Override
    protected void thrdStartup() {
    	super.thrdStartup();
    	
        class tmpMsg implements Runnable {
            public SharcsFECentral m_central;
            tmpMsg(SharcsFECentral central) {
                m_central    = central;
            }
            public void run() {
                m_GUI = new SharcsGUI(m_central, SharcsGUI.UserMode.AUTO);
            }
        }
        
        // Setup the GUI
        SwingUtilities.invokeLater(new tmpMsg(this));
    }
 
    
	@Override
	protected void thrdEnd() {
		super.thrdEnd();
		m_ktlInst.queHalt();
		try {
			m_ktlInst.join();
		} catch (InterruptedException e) {
			CLog.exceptionStr(e);
		}
	}

	public synchronized KTLJNALibInstance getKtlInst() {
		return m_ktlInst;
	}
}








