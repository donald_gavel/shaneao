package sharcs_fe;



import java.math.RoundingMode;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;


import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import org.uco.gui_bits.ButtonMenu;
import org.uco.gui_bits.EasyRadioButton;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyEnumMenuButton;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.gui_bits.KeyProgressBarInt;
import org.uco.gui_bits.KeyStringLabel;
import org.uco.gui_bits.KwLine;
import org.uco.gui_bits.LogWind;
import org.uco.gui_bits.SeparatorBar;
import org.uco.java_util.CLog;
import org.uco.java_util.GKey;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyEnum;
import org.uco.java_util.GKeyFloat;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GKeyIntArray;
import org.uco.java_util.GKeyString;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.java_util.ImmutableArray;
import org.uco.java_util.ImmutableArray.IInteger;
import org.uco.java_util.KTLKeyword;
import org.uco.java_util.NKMsg;
import org.uco.java_util.SimpleTimer;
import org.uco.java_util.StaticUtil;
import org.uco.ktljna_j.KTLJNALibInstance;
import org.uco.ktljna_j.KTLJNAService;
import org.uco.ktljna_j.KTLKeywordJNA;
import org.uco.ktljna_j.KTLKeywordJNA.KIntArray;
import org.uco.ktljna_j.KTLNotifyOpen;


import sharcs_fe.SecsDisplay.Units;





public class SharcsGUI extends FrameCloseCaptured implements GKeyConnection {
	private static final long serialVersionUID = 1L;
	SharcsFECentral     m_central;    // Our central thread.
	KTLJNALibInstance   m_ktl;
	
	private KTLJNAService m_servSharcs;
	private KTLJNAService m_servSaomot;
    
    // GKey fields for tracking which keywords we have given messages to run.
    GTag    m_tag = new GTag(SharcsGUI.class.getName());
    
    @SuppressWarnings("rawtypes")
	LinkedList<GKey> m_gkeyList = new LinkedList<GKey>(); // Keep list of keywords that we passed notify messages to.
	LinkedList<GKeyConnection> m_gkeyConnList = new LinkedList<GKeyConnection>(); // keep this list to remove connections later.
    
    
    boolean m_GUIBuilt = false; // This will only be set to true once the GUI has been completed.
    
    
    StateLabel m_stateLabel;
    
    public enum UserMode {AUTO, NORMAL, ADVANCED };
    UserMode m_userMode = UserMode.AUTO;
    
    SecsDisplay.Units m_timeUnits = SecsDisplay.Units.SECS;
   
    LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();
    
    TrigThrd  m_Trigger;
    
    LogWind         m_LogWind;
    SetupFrame      mSetupFrame = null;
    WindowFrame     mWindowFrame = null;
    ActionFrame     mActionFrame = null;
    StatusFrame     mStatusFrame = null;
    
    

    
    final static int LOG_WIND      =  2;
    final static int RESIZE_FRM    =  3;
    final static int WINDOW_WIND   =  4;
    //final static int INIT_WIND     =  5;
    //final static int O_INFO_WIND   =  6;
    final static int PALETTE_WIND  =  7;
    final static int TAKEDARKS_WIND=  8; 
    final static int SETUP_WIND    =  9;
    final static int ACTION_WIND   = 10;
    final static int STATUS_WIND   = 11;
    final static int CONFIG_OPEN   = 20;
    final static int CONFIG_SET    = 21;
    final static int CONFIG_CREATE = 22;
    final static int SCRIPTS_RUN   = 100;
    
    // menu bar (menu item and status label)
    JPanel       m_jplMenuBar;
    ButtonMenu   m_jbtMFile;
    JPopupMenu   m_popFile;
    ButtonMenu   m_jbtMConfig;
    JPopupMenu   m_popConfig;
    ButtonMenu   m_jbtMScripts;
    JPopupMenu   m_popScripts;
    ButtonMenu   m_jbtMWindows;
    JPopupMenu   m_popWindows;
    
    
    JPanel		 m_jplProgress; // Panel containing exposure progress info
    JProgressBar m_ProgTotal;   // Progress of total exposure (which may consist of multiple images)
    
    
    // 
    JPanel      m_jplMain;      // Main panel
    
    private JLabel m_estMinITime;
	private JLabel m_estActualTime;
	private JLabel m_estTotalTime;
    PredictedTimeLabel      m_lbPredictedTime;
    JButton     m_setAll;             // Need available to change its color
    JButton     m_expose;
    
    JLabel         m_modeLb;
    ModeMenuButton m_modeBtn; 
    
    SecsKwLine m_kwlExpo = null;
    ShKwLine  m_kwlReads = null, m_kwlGroups = null, m_kwlReadsIgnored = null, m_kwlFrameNum = null, m_kwlResets = null, m_kwlCoadds = null;
    ShKwLine  m_kwlObserver = null, m_kwlObject = null, m_kwlFile = null, m_kwlProgram = null;
    
    GKeyInt   m_gkwGO;
    GKeyEnum  m_gkwABORT;
    GKeyInt   m_gkwITIME;
    GKeyInt   m_gkwGROUPS;
    GKeyInt   m_gkwNUMREADS;
    GKeyInt   m_gkwIGNOREDREADS;
    GKeyFloat m_gkwREADTIME;
    GKeyInt   m_gkwFRAMENUM;
    GKeyInt   m_gkwRESETS;
    GKeyInt   m_gkwCOADDS;
    
    GKeyInt   m_gkwSTARTX;
    GKeyInt   m_gkwENDX;
    GKeyInt   m_gkwSTARTY;
    GKeyInt   m_gkwENDY;

    
    LinkedList<SecsDisplay> m_secsDisplayList = new LinkedList<SecsDisplay>();
    
    // members for the palette frame and darks frame.
    ExposurePaletteFrame m_paletteFrm = null;
    KTLKeywordJNA.KIntArray m_ktlPALCOUNT;
    GKeyIntArray m_gkwPALCOUNT;   
    GKeyIntArray m_gkwPALEXPTIME;   
    GKeyIntArray m_gkwPALREADS;     
    GKeyIntArray m_gkwPALRESETS;
    GKeyIntArray m_gkwPALCOADDS;
    GKeyEnum     m_gkwSAMPMODE; 
    
    GKeyString m_gkwSTATUSMAIN;
    GKeyString m_gkwSTATUS;
    GKeyString m_gkwSTATUSDETAIL;   
    GKeyString m_gkwHEARTBEAT;
    GKeyString m_gkwOBJECT;
    GKeyString m_gkwFILENAME;
    GKeyInt    m_gkwPROGRESS;
    GKeyFloat  m_gkwELAPSEDTIME;
    GKeyFloat  m_gkwREMAININGTIME;
    GKeyInt    m_gkwIMAGEDONE; 
    GKeyInt    m_gkwRESUME;
    GKeyInt    m_gkwRESET;
    
    //@SuppressWarnings("rawtypes")
	//TreeMap<String,GKey> m_GUIKeysCalc = new TreeMap<String,GKey>();
    
    @SuppressWarnings("rawtypes")
	TreeMap<String,GKey> m_paletteKeys = new TreeMap<String,GKey>();
    TakeDarksFrame m_takeDarksFrm = null;
    
    // saomot keywords
	GKeyString m_gkwDWFILT1NAM;
	GKeyEnum   m_gkwDWFILT1SWT;
	GKeyString m_gkwDWFILT2NAM;
	GKeyEnum   m_gkwDWFILT2SWT;
	GKeyString m_gkwDWAPERNAM;
	GKeyEnum   m_gkwDWAPERSWT;
	
	
    

    
    /**
     * Constructor.
     * @param central
     * @param ktlclient
     */
    SharcsGUI(SharcsFECentral central, UserMode userMode) {
        super("sharcs_fe");
        m_central   = central;
        m_userMode = userMode;
        m_ktl = m_central.getKtlInst();
        
        // Open service 
    	// TODO - (Better way to do this would be in separate thread and create cancel/progress window here.
    	//  Then, instead of waiting on n.waitComplete, have a message run that creates the real window.)
    	KTLNotifyOpen n = m_ktl.queOpen("sharcs", "");
    	m_servSharcs = n.waitComplete();
    	
    	
    	
    	if (m_servSharcs == null) {
    		CLog.err("Could not connect to sharcs, terminating");
    		System.out.println("Could not connect to sharcs");
    		System.exit(-1);
    	}
    	
    	StaticUtil.sleepy(500);
    	
    	n = m_ktl.queOpen("saomot", "");
    	m_servSaomot = n.waitComplete();
    	
    	if (m_servSaomot == null) {
    		CLog.err("Could not connect to saomot, terminating");
    		System.out.println("Could not connect to saomot");
    		System.exit(-1);
    	}
    
    	try {
    		buildGUI();
    	} catch (Exception e) {
    		CLog.exceptionStr(e);
    		System.err.println(e.toString());
    		System.exit(-1);
    	}
    
    	// create and start the trigger thread
        m_Trigger = new TrigThrd();
        m_Trigger.start();
        
        
    }
    
    private void buildGUI() throws GeneralException {

    	// Values to calculate the expected exposure time.
    	@SuppressWarnings("rawtypes")
    	LinkedList<GKey> gkl = new LinkedList<GKey>();
    	gkl.add(m_gkwITIME        = (GKeyInt)  m_servSharcs.getGKey("ITIME"));
    	gkl.add(m_gkwGROUPS       = (GKeyInt)  m_servSharcs.getGKey("GROUPS"));
    	gkl.add(m_gkwNUMREADS     = (GKeyInt)  m_servSharcs.getGKey("NUMREADS"));
    	gkl.add(m_gkwIGNOREDREADS = (GKeyInt)  m_servSharcs.getGKey("IGNOREDREADS"));
    	gkl.add(m_gkwREADTIME     = (GKeyFloat)m_servSharcs.getGKey("READTIME"));
    	gkl.add(m_gkwSTARTX       = (GKeyInt)  m_servSharcs.getGKey("STARTX"));
    	gkl.add(m_gkwENDX         = (GKeyInt)  m_servSharcs.getGKey("ENDX"));
    	gkl.add(m_gkwSTARTY       = (GKeyInt)  m_servSharcs.getGKey("STARTY"));
    	gkl.add(m_gkwENDY         = (GKeyInt)  m_servSharcs.getGKey("ENDY"));  
    	

    	m_gkwFRAMENUM   = (GKeyInt)  m_servSharcs.getGKey("FRAMENUM");
    	m_gkwRESETS     = (GKeyInt)  m_servSharcs.getGKey("RESETS");
    	m_gkwCOADDS     = (GKeyInt)  m_servSharcs.getGKey("COADDS");
    	m_gkwGO         = (GKeyInt)  m_servSharcs.getGKey("GO");


    	m_gkwDWFILT1NAM = (GKeyString) m_servSaomot.getGKey("DWFILT1NAM");
    	m_gkwDWFILT1SWT = (GKeyEnum)   m_servSaomot.getGKey("DWFILT1SWT");
    	m_gkwDWFILT2NAM = (GKeyString) m_servSaomot.getGKey("DWFILT2NAM");
    	m_gkwDWFILT2SWT = (GKeyEnum)   m_servSaomot.getGKey("DWFILT2SWT");
    	m_gkwDWAPERNAM  = (GKeyString) m_servSaomot.getGKey("DWAPERNAM");
    	m_gkwDWAPERSWT  = (GKeyEnum)   m_servSaomot.getGKey("DWAPERSWT");


    	m_ktlPALCOUNT = (KTLKeywordJNA.KIntArray) m_servSharcs.getGKey("PALCOUNT");
    	m_gkwPALCOUNT   = (GKeyIntArray) m_ktlPALCOUNT;
    	m_gkwPALEXPTIME = (GKeyIntArray) m_servSharcs.getGKey("PALEXPTIME");   
    	m_gkwPALREADS   = (GKeyIntArray) m_servSharcs.getGKey("PALREADS");     
    	m_gkwPALRESETS  = (GKeyIntArray) m_servSharcs.getGKey("PALRESETS");
    	m_gkwPALCOADDS  = (GKeyIntArray) m_servSharcs.getGKey("PALCOADDS");

    	m_gkwSAMPMODE     = (GKeyEnum)   m_servSharcs.getGKey("SAMPMODE");
    	m_gkwSTATUSMAIN   = (GKeyString) m_servSharcs.getGKey("STATUSMAIN");
    	m_gkwSTATUS       = (GKeyString) m_servSharcs.getGKey("STATUS");
    	m_gkwSTATUSDETAIL = (GKeyString) m_servSharcs.getGKey("STATUSDETAIL");
    	m_gkwHEARTBEAT    = (GKeyString) m_servSharcs.getGKey("HEARTBEAT");

    	m_gkwOBJECT       = (GKeyString) m_servSharcs.getGKey("OBJECT");
    	m_gkwFILENAME     = (GKeyString) m_servSharcs.getGKey("FILENAME");
    	m_gkwPROGRESS     = (GKeyInt)    m_servSharcs.getGKey("PROGRESS");

    	m_gkwELAPSEDTIME   = (GKeyFloat) m_servSharcs.getGKey("ELAPSEDTIME");
    	m_gkwREMAININGTIME = (GKeyFloat) m_servSharcs.getGKey("REMAININGTIME");
    	m_gkwABORT         = (GKeyEnum)  m_servSharcs.getGKey("ABORT");
    	m_gkwIMAGEDONE     = (GKeyInt)   m_servSharcs.getGKey("IMAGEDONE");
    	m_gkwRESUME        = (GKeyInt)   m_servSharcs.getGKey("RESUME");
    	m_gkwRESET         = (GKeyInt)   m_servSharcs.getGKey("RESET");


    	// Setup the palette frame
		m_paletteFrm = new ExposurePaletteFrame(this);
        m_paletteKeys.put(m_gkwPALCOUNT.getName(), m_gkwPALCOUNT);
        m_paletteKeys.put(m_gkwPALEXPTIME.getName(), m_gkwPALEXPTIME);
        m_paletteKeys.put(m_gkwPALREADS.getName(), m_gkwPALREADS);
        m_paletteKeys.put(m_gkwPALRESETS.getName(), m_gkwPALRESETS);
        m_paletteKeys.put(m_gkwPALCOADDS.getName(), m_gkwPALCOADDS);
        
        
        // Load the palette and have it update when the PALCOUNT keyword is updated
        class PaletteKeysUpdate extends NKMsg implements Runnable {
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				SwingUtilities.invokeLater(this);
			}
			@Override
			public void run() {
				updatePaletteKeys();
			}
        }
        
        m_gkwPALCOUNT.putNotify(getTag(), new PaletteKeysUpdate());
        m_gkeyList.add(m_gkwPALCOUNT);
        
        m_takeDarksFrm = new TakeDarksFrame(this); // must be defined after m_palettFrm
        
        
        
        // Create (but do not show) the LogWind
        m_LogWind = new LogWind("sharcs_fe", 20);
        m_gkeyConnList.add(m_LogWind);
        m_LogWind.addKeyword(m_gkwSTATUS);
        m_LogWind.addKeyword(m_gkwSTATUSDETAIL);
        m_LogWind.addKeyword(m_gkwSTATUSMAIN);
        
        // Create but do not show the setup frame.
        mSetupFrame = new SetupFrame(this);
        m_gkeyConnList.add(mSetupFrame);
        mWindowFrame = new WindowFrame(this);
        m_gkeyConnList.add(mWindowFrame);
        mActionFrame = new ActionFrame(this);
        m_gkeyConnList.add(mActionFrame);
        mStatusFrame = new StatusFrame(this);
        m_gkeyConnList.add(mStatusFrame);
        

        
        // create the frame
        GridBagLayout      framegrid = new GridBagLayout();
        GridBagConstraints framegbc  = new GridBagConstraints();
        getContentPane().setLayout(framegrid);
        
        
        // Make the menu bar.
        Color menuBtnBG = null;
        // Make the File menu 
        m_popFile     = new JPopupMenu("File");
        m_popFile.add(new JMenuItem(new ActionCreateWindow("Log...", LOG_WIND, this)));
        m_popFile.addSeparator();
        m_popFile.add(new JMenuItem(new ActionCreateWindow("Resize", RESIZE_FRM, this)));  
        m_popFile.addSeparator();
        m_popFile.add(new JMenuItem(new ActionAdvancedMode("Auto Mode", UserMode.AUTO, this)));
        m_popFile.add(new JMenuItem(new ActionAdvancedMode("Normal Mode", UserMode.NORMAL, this)));
        m_popFile.add(new JMenuItem(new ActionAdvancedMode("Advanced Mode", UserMode.ADVANCED, this)));
        m_popFile.addSeparator();
        m_popFile.add(new JMenuItem(new AbstractAction("Exit"){
			private static final long serialVersionUID = 1L;
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
        }));  
        m_jbtMFile = new ButtonMenu(m_popFile.getLabel(), m_popFile);
        m_jbtMFile.setBackground(menuBtnBG);
   
        // Make the Config menu
        m_popConfig = new JPopupMenu("Config");
        m_popConfig.add(new JMenuItem(new ActionCreateWindow("Open Config...",   CONFIG_OPEN, this)));
        m_popConfig.add(new JMenuItem(new ActionCreateWindow("Set Config...",    CONFIG_SET, this)));
        m_popConfig.add(new JMenuItem(new ActionCreateWindow("Create Config...", CONFIG_CREATE, this)));
        m_jbtMConfig = new ButtonMenu(m_popConfig.getLabel(), m_popConfig);
        m_jbtMConfig.setBackground(menuBtnBG);
        
        // Make the Scripts menu
        m_popScripts = new JPopupMenu("Scripts");
        m_popScripts.add(new JMenuItem(new ActionCreateWindow("Run Script...",    SCRIPTS_RUN, this)));
        m_jbtMScripts = new ButtonMenu(m_popScripts.getLabel(), m_popScripts);
        m_jbtMScripts.setBackground(menuBtnBG);
        
        // Make the Windows menu
        m_popWindows = new JPopupMenu("Windows");
        m_jbtMWindows = new ButtonMenu(m_popWindows.getLabel(), m_popWindows);
        m_jbtMWindows.setBackground(menuBtnBG);	
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Exp Palette",PALETTE_WIND,   this)));
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Take Darks", TAKEDARKS_WIND, this)));
        m_popWindows.add(new JSeparator());
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Setup",      SETUP_WIND,     this)));
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Status",     STATUS_WIND,    this)));
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Action",     ACTION_WIND,    this)));
        m_popWindows.add(new JMenuItem(new ActionCreateWindow("Window",     WINDOW_WIND,    this)));
        m_popWindows.add(new JSeparator());
        //m_popWindows.add(new JMenuItem(new ActionCreateWindow("Initialize", INIT_WIND,      this))); TODO delete
        //m_popWindows.add(new JMenuItem(new ActionCreateWindow("Other Info", O_INFO_WIND,    this))); TODO delete
        
        // Add the Status Label
        m_stateLabel = new StateLabel("--------ShARCS--------", this);
        m_gkeyConnList.add(m_stateLabel);
        m_stateLabel.setPreferredSize(new Dimension(130, 25));

        
        // assemble the pieces of the menu bar
        m_jplMenuBar = new JPanel();
        // grid layout manager
        GridBagLayout menugrid     = new GridBagLayout();
        GridBagConstraints menugbc = new GridBagConstraints();
        m_jplMenuBar.setLayout(menugrid);
        m_jplMenuBar.setOpaque(true);
        menugbc.weightx = 0.0;
        menugbc.ipady   = 0;
        menugbc.anchor  = GridBagConstraints.WEST;
        menugbc.gridx = 0; menugbc.gridy = 0;
        m_jplMenuBar.add(m_jbtMFile, menugbc);
        menugbc.gridx  += 1;
        m_jplMenuBar.add(m_jbtMConfig, menugbc);
        menugbc.gridx  += 1;
        m_jplMenuBar.add(m_jbtMScripts, menugbc);
        menugbc.gridx  += 1;
        m_jplMenuBar.add(m_jbtMWindows, menugbc);
        menugbc.gridx  += 1;
        menugbc.ipadx = 20;
        menugbc.weightx = 100.0;
        menugbc.fill = GridBagConstraints.HORIZONTAL;
        m_jplMenuBar.add(m_stateLabel, menugbc);
        
        // Setup the main panel. (contains everything but the menu bar)
        m_jplMain = new JPanel();
        GridBagLayout maingrid     = new GridBagLayout();
        GridBagConstraints maingbc = new GridBagConstraints();
        m_jplMain.setLayout(maingrid);
        m_jplMain.setOpaque(true);
        maingbc.weightx = 100.0;
        maingbc.weighty = 100.0;
        maingbc.ipady   = 0;
        maingbc.anchor  = GridBagConstraints.WEST;
        maingbc.fill    = GridBagConstraints.BOTH;
        
        
        // Assemble the progress panel
        m_jplProgress = new JPanel();
        GridBagLayout proggrid     = new GridBagLayout();
        GridBagConstraints proggbc = new GridBagConstraints();
        m_jplProgress.setLayout(proggrid);
        maingbc.weightx = 100.0;
        maingbc.weighty = 100.0;
        maingbc.fill    = GridBagConstraints.BOTH;
        maingbc.gridx = 0;
        maingbc.gridwidth = 20;
        maingbc.gridy = 0;
        //m_jplMain.setPreferredSize(new Dimension(500,350)); TODO delete
        //setPreferredSize();   TODO delete
        m_jplMain.add(m_jplProgress, maingbc);
       	
        
        m_ProgTotal = new KeyProgressBarInt(m_gkwPROGRESS);
        m_ProgTotal.setForeground(Color.GREEN);
        
		
        m_ProgTotal.setString("Total");
        {
        	KeyStringLabel objn   = new KeyStringLabel(m_gkwOBJECT, "");
        	KeyStringLabel status = new KeyStringLabel(m_gkwSTATUSDETAIL, "");
        	KeyStringLabel statusm = new KeyStringLabel(m_gkwSTATUSMAIN, "");
        	KeyStringLabel fileN  = new KeyStringLabel(m_gkwFILENAME, "");
        	objn.setHtmlWrap(true);
        	status.setHtmlWrap(true);
        	statusm.setHtmlWrap(true);
        	fileN.setHtmlWrap(true);
        	//status.setPreferredSize(new Dimension(45, 300));
        	String spaces = "  ";
        	m_ProgTotal.setLayout(new GridBagLayout());
        	GridBagConstraints pgbc = new GridBagConstraints();
        	pgbc.fill = GridBagConstraints.BOTH;
        	pgbc.gridx = 1; pgbc.gridy = 1;
        	pgbc.weightx = 100.0; pgbc.weighty = 0.0;
        	m_ProgTotal.add(new JLabel(spaces));   // It looked funny pressed right up against the left edge of the panel, so I added some spaces in front of it.
        	pgbc.gridx++;
        	m_ProgTotal.add(objn, pgbc); 
        	pgbc.gridy++;
        	pgbc.gridheight = 2;
        	pgbc.weighty = 100.0;
        	m_ProgTotal.add(statusm, pgbc);
        	pgbc.gridy += 2;
        	m_ProgTotal.add(status, pgbc);
        	pgbc.weighty = 0.0;
        	pgbc.gridy += 2;
        	pgbc.gridheight = 1;
        	m_ProgTotal.add(fileN, pgbc);
        	pgbc.gridx += 3;
        	pgbc.weightx = 0.0;
        	m_ProgTotal.add(new JLabel(" "), pgbc);

        	
        	pgbc.weightx = 5.0;
        	pgbc.gridy = 1;
        	KTLFloatLabelSecs ttime = new KTLFloatLabelSecs(m_gkwELAPSEDTIME, "Elapsed: ", SecsDisplay.Units.SECS);
        	KTLFloatLabelSecs rtime = new KTLFloatLabelSecs(m_gkwREMAININGTIME, "Remaining: ", SecsDisplay.Units.SECS);
        	m_secsDisplayList.add(ttime);
        	m_secsDisplayList.add(rtime);
        	ttime.setHorizontalAlignment(JLabel.RIGHT);
        	ttime.setHideUnits(true);
        	rtime.setHorizontalAlignment(JLabel.RIGHT);
        	KeyIntLabel obsNum = new KeyIntLabel(m_gkwFRAMENUM, "Obs#: ");
        	obsNum.setHorizontalAlignment(JLabel.RIGHT);
        	
        	int lb_col = pgbc.gridx + 1;
        	int val_col = pgbc.gridx + 2;
        	
        	pgbc.gridx = val_col;
        	m_ProgTotal.add(rtime, pgbc);
        	
        	pgbc.gridy++;
        	pgbc.weighty = 100.0;
        	m_ProgTotal.add(ttime, pgbc);
        	pgbc.weighty = 0.0;
        	
        	pgbc.gridy += 4;
        	pgbc.gridx = val_col;
        	//m_ProgTotal.add(obsNum, pgbc); // TODO delete obsNum
        	
        	
        	
        	pgbc.gridx = 2; pgbc.gridy++;
        	pgbc.weighty = 0.0;
        	m_ProgTotal.add(new JLabel(spaces), pgbc);
        	pgbc.gridy = 0;
        	m_ProgTotal.add(new JLabel(spaces), pgbc);
        	
        	
        	// Create a message to turn the progress bar blue when the image is writing out.
            class WritingMsg extends NKMsg implements Runnable {
            	String m_val;
            	JProgressBar m_progTotal;
            	WritingMsg(JProgressBar progTotal, String val) {
            		m_progTotal = progTotal;
            		m_val = val;
            	}
            	@Override
        		public void run(KTLKeyword kd, Object val, String valStr) {
        			// Called when keyword value is updated in client thread
        			m_val = (String) val;
        			// Need to make a new message for GUI or there's a race condition
        			SwingUtilities.invokeLater(new WritingMsg(m_progTotal, m_val));
        		}
        		@Override
        		public void run() {
        			// The following string is set when the fits file starts reading out.
        			// "Fits writing complete" is sent when the writing is complete, but
        			//  just using any other message to turn the bar green again may be good enough.
        			//  Time will tell if this assumption was correct.
        			String writing = "Writing FITS file";
        			int len = (m_val.length() < writing.length()) ? m_val.length() : writing.length();  
        			String val = m_val.substring(0, len);
        			if (val.equalsIgnoreCase(writing)) {
        				float[] hsbvals = Color.RGBtoHSB(100, 100, 255, null);
        				m_ProgTotal.setForeground(Color.getHSBColor(hsbvals[0], hsbvals[1], hsbvals[2]));
        			} else {
        				m_progTotal.setForeground(Color.GREEN);
        			}
        		}
            }
            
            m_gkwSTATUSMAIN.putNotify(getTag(), new WritingMsg(m_ProgTotal, ""));
        	
        }
        proggbc.weightx = 100.0;
        proggbc.weighty = 100.0;
        proggbc.ipady   = 40;
        proggbc.gridx = 0; proggbc.gridy = 0;
        proggbc.fill = GridBagConstraints.BOTH;
        m_jplProgress.add(m_ProgTotal, proggbc);
        proggbc.gridy++;
        //m_jplProgress.add(m_ProgImage, proggbc);
        

        
        // Make the time estimate Panel
        m_estMinITime = new JLabel("");
        m_estActualTime = new JLabel("");
        m_estTotalTime = new JLabel("");
        JPanel estimate = new JPanel();
        maingbc.gridy++;
        maingbc.fill = GridBagConstraints.HORIZONTAL;
        maingbc.weighty = 0.0;
        m_jplMain.add(estimate, maingbc);
        estimate.setLayout(new GridLayout(1,0));
        estimate.add(m_estMinITime);
        estimate.add(m_estActualTime);
        estimate.add(m_estTotalTime);
        
        // Make the parameters panel
        // Primary parameters
        m_kwlExpo         = new ExposureKwLine("Exposure", m_gkwITIME, m_gParms, SecsDisplay.Units.MILLIS);  // All modes
        m_kwlGroups       = new ShKwLine("NGroups",  m_gkwGROUPS,         m_gParms);  // UTR 
        m_kwlReads        = new ShKwLine("NReads",   m_gkwNUMREADS,       m_gParms);  // CDS, MCDS, UTR
        m_kwlReadsIgnored = new ShKwLine("NIgnored", m_gkwIGNOREDREADS,   m_gParms);  // UTR
        m_kwlCoadds       = new ShKwLine("NCoadds",  m_gkwCOADDS,         m_gParms);  // All modes
        m_kwlResets       = new ShKwLine("Resets",   m_gkwRESETS,         m_gParms);  // UTR
        m_kwlFrameNum     = new ShKwLine("Obs #",    m_gkwFRAMENUM,       m_gParms);  // All modes

        for (KwLine kwl:m_gParms) {
        	kwl.m_current.setBorder(new EmptyBorder(0,10,0,0));
        }

        // Other Parameters
        m_kwlObserver = new ShKwLine("Observer:", m_servSharcs.getGKey("OBSERVER"),   m_gParms);
        m_kwlObject   = new ShKwLine("Object:",   m_servSharcs.getGKey("OBJECT"),   m_gParms);
        m_kwlProgram  = new ShKwLine("Program:",  m_servSharcs.getGKey("PROGRAM"), m_gParms);


        m_secsDisplayList.add(m_kwlExpo);
        
        JPanel params = new JPanel();
        maingbc.gridy++;
        maingbc.fill = GridBagConstraints.HORIZONTAL;
        maingbc.weighty = 0.0;
        m_jplMain.add(params, maingbc);
        GridBagLayout paramgrid     = new GridBagLayout();
        GridBagConstraints paramgbc = new GridBagConstraints();
        GridBagConstraints gbc = paramgbc;
        params.setLayout(paramgrid);
        int ii = 0;
        int colName   = ii++;
        int colCurr   = ii++;
        int colTF     = ii++;
        int colBtn    = ii++;
        int colSepV   = ii++;
        int colExpBtn = ii++;
        
        // Put Mode line in parameter panel
        gbc.gridx = colName;
        gbc.gridy = 0;
        gbc.fill = GridBagConstraints.BOTH;
        m_modeLb = new JLabel("Mode");
        params.add(m_modeLb, gbc);
        m_modeBtn = new ModeMenuButton(m_gkwSAMPMODE, "");
        m_gkeyConnList.add(m_modeBtn);
        gbc.gridx = colCurr;
        gbc.fill = GridBagConstraints.NONE;
        params.add(m_modeBtn, gbc);
        gbc.gridx = colTF;
        gbc.gridwidth = 2;
        JPanel timeRad = new JPanel();
        params.add(timeRad, gbc);
        {
        	timeRad.setLayout(new GridBagLayout());
        	GridBagConstraints gb = new GridBagConstraints();
        	gb.fill = GridBagConstraints.BOTH;
        	gb.weightx = gb.weighty = 100.0;
        	gb.gridx = 0; gb.gridy = 0;
        	RadioButtonSecMillisec rbmilli = new RadioButtonSecMillisec("Millisecs"); // text must be "millisecs" or it wont work.
        	RadioButtonSecMillisec rbsec   = new RadioButtonSecMillisec("Seconds"); 
        	timeRad.add(rbmilli, gb);
        	gb.gridx++;
        	timeRad.add(rbsec, gb);
        	ButtonGroup group = new ButtonGroup();
        	group.add(rbmilli);
        	group.add(rbsec);
        	rbsec.setSelected(true);
        }
        gbc.gridwidth = 1;
       
        
        // Put the main lines into the parameter panel
        gbc.gridy++;
        gbc.weightx = 100.0;
        gbc.weighty = 100.0;
        gbc.fill    = GridBagConstraints.BOTH;
        gbc.anchor  = GridBagConstraints.CENTER;
        ii = gbc.gridy;
        ShKwLine kwlItems[] = { m_kwlExpo, m_kwlGroups, m_kwlReads, m_kwlReadsIgnored, m_kwlResets, m_kwlCoadds, m_kwlFrameNum };
        for (ShKwLine kwl:kwlItems) {
        	gbc.gridy = ii++;
        	kwl.place(params, gbc, colName, colCurr, colTF, colBtn);
        }
        
        // Add parameter panel Vertical separator
        SeparatorBar expSep = new SeparatorBar(SeparatorBar.VERTICAL);
        expSep.setExtent( 0.8 );
        //expSep.setBackground(Color.black);
        expSep.setPreferredSize(new Dimension(3, 10));
        gbc.weightx = 0.0;
        gbc.weighty = 100.0;
        gbc.gridheight = GridBagConstraints.REMAINDER;
        gbc.gridx = colSepV;
        gbc.gridy = 1;
        gbc.ipadx = 7;
        gbc.ipady = 0;
        gbc.fill = GridBagConstraints.VERTICAL;
        params.add(expSep, gbc);
        
        // Add exposure buttons
        gbc.gridheight = 1;
        gbc.ipadx      = 0;
        gbc.weighty    = 0.0;
        gbc.weightx    = 0.0;
        gbc.gridx      = colExpBtn;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        
        m_lbPredictedTime = new PredictedTimeLabel();  // TODO delete this and the class
        m_lbPredictedTime.setHorizontalAlignment(SwingConstants.CENTER); // TODO delete
        m_secsDisplayList.add(m_lbPredictedTime); // TODO delete
        gbc.gridy = 0;
        
        
        JPanel expBlock = new JPanel();
        gbc.gridheight = 7;
        gbc.gridy++;
        params.add(expBlock,gbc);
        expBlock.setLayout(new GridBagLayout());
        GridBagConstraints expgbc = new GridBagConstraints();
        expgbc.weightx = 100.0;
        expgbc.weighty = 100.0;
        expgbc.fill    = GridBagConstraints.BOTH;
        expgbc.gridx = 0;
        expgbc.gridwidth = 20;
        expgbc.gridy = 0;
        
        m_expose = new JButton("Expose");
        //gbc.gridy++;
        expBlock.add(m_expose, expgbc);
        m_expose.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		exposureStart();
        	}
        });
        
        
        expgbc.gridy++;
        //expBlock.add(new JLabel(""), expgbc);
        expBlock.add(new SeparatorBar(SeparatorBar.HORIZONTAL), expgbc);
        
        m_setAll = new JButton("Set All");
        expgbc.gridy++;
        expBlock.add(m_setAll, expgbc);
        m_setAll.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		setAll();
        	}
        });
        
        
        JButton clearAllBtn = new JButton("Clear All");
        expgbc.gridy++;
        expBlock.add(clearAllBtn, expgbc);
        clearAllBtn.addActionListener(new ActionListener() {
        	@Override
        	public void actionPerformed(ActionEvent e) {
        		clearAll();
        	}
        });
        
        KeyEnumMenuButton abortBtn = new KeyEnumMenuButton(m_gkwABORT, "Abort:", null);
        expgbc.gridy++;
        expBlock.add(abortBtn, expgbc);
        
        // Make details panel
        JPanel details = new JPanel();
        maingbc.gridy++;
        maingbc.fill = GridBagConstraints.HORIZONTAL;
        maingbc.weighty = 0.0;
        m_jplMain.add(details, maingbc); 
        GridBagLayout detailsgrid     = new GridBagLayout();
        GridBagConstraints detailsgbc = new GridBagConstraints();
        gbc = detailsgbc;
        details.setLayout(detailsgrid);
        
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 100.0;
        gbc.weighty = 0.0;
        gbc.gridwidth = GridBagConstraints.REMAINDER; 
        SeparatorBar sepA = new SeparatorBar(SeparatorBar.HORIZONTAL);
        sepA.setExtent(0.0);
        details.add(sepA, gbc);
        gbc.gridy++;
        JPanel sepName = new JPanel();
        details.add(sepName, gbc);
        gbc.gridwidth = 1;
        SeparatorBar sepHor = new SeparatorBar(SeparatorBar.HORIZONTAL);
        sepHor.setExtent(0.95);
        sepName.setLayout(new GridBagLayout());
        GridBagConstraints sepgbc = new GridBagConstraints();
        sepgbc.fill = GridBagConstraints.BOTH;
        sepgbc.weightx = 5.0;
        sepgbc.weighty = 0.0;
        sepgbc.gridx = 0;
        sepgbc.gridy = 0;
        //sepName.add(obsName, sepgbc);
        sepgbc.gridx = 1;
        sepgbc.weightx = 100.0;
        sepName.add(sepHor, sepgbc);
        
        // add in detail elements
        ShKwLine kwlDetails[] = { m_kwlObserver, m_kwlProgram, m_kwlObject };
        gbc.gridy = 5;
        for (ShKwLine kwl:kwlDetails) {
        	kwl.placeLS(details, gbc, colName);
        }
        
        // Add status messages
        gbc.gridx = colName;
        gbc.gridwidth = 1;
        gbc.weightx = 0.0;
        gbc.gridx += 2;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 100.0;
        gbc.gridy++;

        
        
        // Put the frame together and display it.
        framegbc.gridx = 0; framegbc.gridy = 0;
        framegbc.ipady = 4;
        framegbc.anchor = GridBagConstraints.NORTHWEST;
        framegbc.fill    = GridBagConstraints.HORIZONTAL;
        getContentPane().add(m_jplMenuBar, framegbc);
        
        framegbc.weightx = 100.0;
        framegbc.weighty = 100.0;
        framegbc.ipady   = 0;
        framegbc.anchor  = GridBagConstraints.WEST;
        framegbc.fill    = GridBagConstraints.BOTH;
        framegbc.gridx = 0; framegbc.gridy = 1;
        framegbc.anchor = GridBagConstraints.WEST;
        getContentPane().add(m_jplMain, framegbc);
        
        pack();

        setVisible(true);

        // All the pieces are ready, setup the initial display.
        m_GUIBuilt = true;
        
        setAdvancedMode(m_userMode);
                     
        updateDisplay();

        // Add the calculation message to the keywords used in the calculation.
        CalcMsg c_nkmsg = new CalcMsg();
    	for (@SuppressWarnings("rawtypes") GKey gk:gkl) {
    		String name = gk.getName();
    		CLog.notice(this.getClass()+" adding GUI keyword "+name);
    		gk.putNotify(getTag(), c_nkmsg);
    		m_gkeyList.add(gk);
    	}
        
        // Hide the entry fields.
        //m_kwlObject.m_edit.doThisOnClick();
    	//m_kwlComment.m_edit.doThisOnClick();
        m_kwlObserver.m_edit.setVisible(true);
        m_kwlObserver.m_edit.doThisOnClick(); // hide the entry box
        m_kwlProgram.m_edit.setVisible(true);
        m_kwlProgram.m_edit.doThisOnClick(); // hide the entry box
        m_kwlObject.m_edit.setVisible(false);
        
        // set units to seconds.
        setAllSecDisplay(SecsDisplay.Units.SECS);
        
        updatePaletteKeys();
    }
    


	/**
     * Update the exposure Palette based on the PAL keywords. This should be called when
     * PALCOUNT is updated as it intentionally only updated after the other palette keywords
     * by the server.
     */
    void updatePaletteKeys() {
    	ImmutableArray.IInteger counts   = m_gkwPALCOUNT.getVal();
    	ImmutableArray.IInteger expTimes = m_gkwPALEXPTIME.getVal();   
    	ImmutableArray.IInteger reads    = m_gkwPALREADS.getVal();     
    	ImmutableArray.IInteger resets   = m_gkwPALRESETS.getVal();
    	ImmutableArray.IInteger coadds   = m_gkwPALCOADDS.getVal();
    	
    	int sz = counts.size();
    	LinkedList<Exposure> exps = new LinkedList<Exposure>();
    	
    	for (int j=0; j<sz; j++) {
    		int count = counts.get(j);
    		int expTime = expTimes.get(j);
    		int read = reads.get(j);
    		int reset = resets.get(j);
    		int coadd = coadds.get(j);
    		Exposure ex = new Exposure(j, expTime, read, reset, coadd, count);	
    		if (expTime != 0) {
    			exps.add(ex);
    		}
    	}
    	
    	m_paletteFrm.updateMap(exps);
    }
    
    /**
     * Write the values from the palette to the server.
     */
    void sendServerPaletteKeys() {
    	ExposureMap m = m_paletteFrm.m_tblModel.getMap();
    	// create arrays with the size of m_PALCOUNT
    	int sz = m_gkwPALCOUNT.getVal().size();
    	CLog.notice("sendServerPaletteKeys size="+sz);
    	
    	// create arrays and fill them with zeroes
       	Integer[] counts   = new Integer[sz];
    	Integer[] expTimes = new Integer[sz];   
    	Integer[] reads    = new Integer[sz];     
    	Integer[] resets   = new Integer[sz];
    	Integer[] coadds   = new Integer[sz];
    	
    	Integer z = 0;
    	for (int j=0; j<sz; j++) {
    		counts[j] = expTimes[j] = reads[j] = resets[j] = coadds[j] = z;
    	}
    	
    	// put in the values from our palette
    	Iterator<Exposure> iter = m.m_map.values().iterator();
    	while (iter.hasNext()) {
    		Exposure ex = iter.next();
    		int index = ex.getIndex();
    		counts[index]   = ex.m_count;
    		expTimes[index] = ex.getExposure();   
        	reads[index]    = ex.getReads();     
        	resets[index]   = ex.getResets();
        	coadds[index]   = ex.getCoadds();
    	}
    	
    	// send them to the server
    	m_gkwPALEXPTIME.queWrite(new IInteger(expTimes), null);
    	m_gkwPALREADS.queWrite(new IInteger(reads), null);
    	m_gkwPALRESETS.queWrite(new IInteger(resets), null);
    	m_gkwPALCOADDS.queWrite(new IInteger(coadds), null);
    	m_gkwPALCOUNT.queWrite(new IInteger(counts), null);
    }
    
    /**
     * When this frame is closed; kill the program in a controlled fashion. 
     * This includes closing child frames.
     */
    @Override
    public void doThisOnFrameClose() {
        CLog.notice("doThisOnFrameClose");
        removeNotifyTag();
        stopTrigger();
        
        if (m_LogWind != null) {
            m_LogWind.doThisOnFrameClose();
            m_LogWind = null;
        }
        if (mWindowFrame != null) {
        	mWindowFrame.doThisOnFrameClose();
        	mWindowFrame = null;
        }
        m_central.queHalt(); // this should stop the central thread and result in all client threads being stopped.
        super.doThisOnFrameClose();
    }
    
    /**
     * Set all values that have been entered by the user.
     */
    public void setAll() {
	// Keep the internal itime keyword value, if any, from being
	// reset during this time.
	m_kwlExpo.freeze_m_sent(true);

    	for (KwLine kwl:m_gParms) {
    		if (kwl.getUserInput()) {
    			kwl.sendValToServer();
    		}
    	}
CLog.notice("setAll calling setupExp");
    	setupExp(); // if in normal mode, this would fix damage of a manually entered NGroup, NReads, etc.

	// OK, restore normal handling of the internal itime keyword value, if any.
	m_kwlExpo.freeze_m_sent(false);

    	updateSetAllBtn();
    }
    
    /**
     * Clear all of the text field entries
     */
    public void clearAll() {
    	for (KwLine kwl:m_gParms) {
    		kwl.m_tf.setText("");
    	}
    	updateSetAllBtn();
    }
    
    /**
     * Turn set all button yellow if there are any user set entries.
     */
    protected void updateSetAllBtn() {
    	boolean userInput = false;
    	for (KwLine kwl:m_gParms) {
    		if (kwl.getUserInput()) {
    			userInput = true;
    			break;
    		}
    	}
    	if (userInput) {
    		m_setAll.setBackground(Color.yellow);
    	} else {
    		m_setAll.setBackground(null);
    	}
    }
    
    
    /**
     * Send the keyword request to start an exposure.
     */
    void exposureStart() {
CLog.notice("exposureStart calling setupExp");
    	setupExp(); // Set values if normal mode exposure.
    		
    	CLog.notice("exposureStart ITIME="+m_gkwITIME.getVal()+ 
    			" NGroups(GROUPS)="+m_gkwGROUPS.getVal()+ 
    			" NReads(NUMREADS)="+m_gkwNUMREADS.getVal()+ 
    			" NIgnored(IGNOREDREADS)="+m_gkwIGNOREDREADS.getVal()+ 
    			" NCoadds(COADDS)="+m_gkwCOADDS.getVal()+ 
    			" RESETS"+m_gkwRESETS.getVal()+ 
    			" Obs#(FRAMENUM)="+m_gkwFRAMENUM.getVal());

    	m_gkwGO.queWrite(1, null);
    }
    
    
    public boolean isTimeInSeconds() {
    	return ( m_timeUnits == SecsDisplay.Units.SECS );
    }
    
    
    private void sendExposurePaletteToServer() {		
		ExposureArrays ea = m_paletteFrm.m_tblModel.getExposureArrays();
		m_gkwPALEXPTIME.queWrite(new ImmutableArray.IInteger(ea.m_ex_times), null);
		m_gkwPALEXPTIME.queWrite(new ImmutableArray.IInteger(ea.m_ex_times), null);
		m_gkwPALREADS.queWrite(new ImmutableArray.IInteger(ea.m_reads), null);
		m_gkwPALRESETS.queWrite(new ImmutableArray.IInteger(ea.m_resets), null);
		m_gkwPALCOUNT.queWrite(new ImmutableArray.IInteger(ea.m_counts), null);
	}

	/** 
     * This method should stop m_Trigger. 
     */
    public void stopTrigger() {
        CLog.notice("__GUI.stopTrigger()");
        if (m_Trigger != null) {
            // This should stop the trigger thread and cause it to be deleted.
            m_Trigger.m_bLoop = false;
            m_Trigger = null;
        }
    }
    

    /**
     * It is hard to get this function called. If you actually want code to run,
     * put it in doThisOnFrameClose.
     */
    public void finalize() throws Throwable {
        CLog.notice("__GUI.finalize()");
        super.finalize();
    }
    

    /**
     * Calculate the minimum exposure time, actual exposure time and total exposure time.
     * @return
     */
    double calcExposureTimes() { 
    	
		double iTime      = (double) m_gkwITIME.getVal(); // integer milliseconds
	   	int	   m_ngroups  = m_gkwGROUPS.getVal();
    	int	   m_nreads   = m_gkwNUMREADS.getVal();
    	int	   m_ndrops   = m_gkwIGNOREDREADS.getVal();
    	int    nresets    = m_gkwRESETS.getVal();
    	//float  m_readtime = m_gkwREADTIME.getVal();
    		
    	int	wStartx = m_gkwSTARTX.getVal();
    	int	wEndx   = m_gkwENDX.getVal();
    	int	wStarty = m_gkwSTARTY.getVal();
    	int	wEndy   = m_gkwENDY.getVal();
		
    	// Figure out which iTime to use, keyword value or user entered value
		String txt = m_kwlExpo.m_tf.getText();
		boolean writeNReads = false;
		if (txt.compareTo("") != 0) {
			try {
				double v = Double.parseDouble(txt);
				if (v > 0.0) {
					Units t = m_kwlExpo.getDisplayType();
					if (t == Units.SECS) { 
						// convert to millisecs
						v *= 1000.0;
					}
					writeNReads = true;
					iTime = v;
				}
			} catch (NumberFormatException e) {	}
		}
		// convert iTime to seconds from milliseconds
		iTime *= 0.001;
		
		// User entered groups?
		Integer i = validInt(m_kwlGroups.m_tf.getText()) ;
		if (i != null) { m_ngroups = i; }
		
		// User entered nreads?
		i = validInt(m_kwlReads.m_tf.getText()) ;
		if (i != null) { m_nreads = i; }
		if (m_userMode == UserMode.AUTO) {
			m_nreads = calcAutoReads(iTime*1000.0);
			if (writeNReads) {
				m_kwlReads.m_tf.setText(Integer.toString(m_nreads));
			} else {
				m_kwlReads.m_tf.setText("");
			}
		}
		
		// User entered ndrops?
		i = validInt(m_kwlReadsIgnored.m_tf.getText()) ;
		if (i != null) { m_ndrops = i; }
		
		// User entered nresets?
		i = validInt(m_kwlResets.m_tf.getText()) ;
		if (i != null) { nresets = i; }
		
	
    	// Calculate minimum exposure time
    	double min_itime = 0.0;
    	double readTime = 1.45479; // normal time to read out whole chip (seconds)
    	if (mWindowFrame.fullWindow()) {
    		// Full size window
    		min_itime = (m_ngroups - 1) * (m_nreads + m_ndrops) * readTime; 
    	} else {
    		// smaller window
    		readTime = getWindowedReadTime();
    		min_itime = (m_ngroups - 1) * (m_nreads + m_ndrops) * readTime;
    	}
    	
    	
    	// Calc Actual time = requested_exposure - (requested_exposure mod READTIME)
    	double actual_time = iTime - (iTime % readTime);
    	
    	// Calc Total time = Min_itime (from above 1) + Actual_itime (from above 2) + NResets*READTIME + 12 sec (processing time)
    	double total_time = min_itime + actual_time + nresets*readTime + 12.0;
    	
    	DecimalFormat fmt = new DecimalFormat("#.##");
	fmt.setRoundingMode(RoundingMode.CEILING);
    	m_estMinITime.setText("Min ITime="+fmt.format(min_itime));
    	m_estActualTime.setText("Actual="+fmt.format(actual_time));
    	m_estTotalTime.setText("Total="+fmt.format(total_time));
    	
    	
    	// 
    	return min_itime;
    }
    
    /**
     * Return expected read time for a sub-window in seconds.
     * @return
     */
    double getWindowedReadTime() {
    	int	wStartx = m_gkwSTARTX.getVal();
    	int	wEndx   = m_gkwENDX.getVal();
    	int	wStarty = m_gkwSTARTY.getVal();
    	int	wEndy   = m_gkwENDY.getVal();
    	double readTime = (wEndy - wStarty + 1) * ( (wEndx - wStartx + 1) * 9.9964499e-6 + 8.105010e-5);
    	return readTime;
    }
    
	public void updateDisplay() {
		double exp = calcExposureTimes();
		m_lbPredictedTime.setSeconds(exp);
	}
    
	@Override
	public GTag getTag() {
		return m_tag;
	}

	@Override
	public void removeNotifyTag() {
		for (@SuppressWarnings("rawtypes") GKey gk : m_gkeyList) {
			gk.removeNotifyTag(getTag());
		}
		for (GKeyConnection gkc : m_gkeyConnList) {
			gkc.removeNotifyTag();
		}
	}

	public void setAdvancedMode(UserMode userMode) {
		m_userMode = userMode;
		int h = 435;
		switch (userMode) {
		case ADVANCED: 
			m_modeLb.setVisible(true);
			m_modeBtn.setVisible(true);
			m_kwlGroups.setVisible(true);
			m_kwlReads.setVisible(true); 
			m_kwlReads.setEnable(true);
			m_kwlReadsIgnored.setVisible(true);
			m_kwlResets.setVisible(true);
			m_jplMain.setPreferredSize(new Dimension(500,h+70));
			break;
		case NORMAL:
			m_modeLb.setVisible(false);
			m_modeBtn.setVisible(false);
			m_kwlGroups.setVisible(false);
			m_kwlReads.setVisible(true); 
			m_kwlReads.setEnable(true);
			m_kwlReadsIgnored.setVisible(false);
			m_kwlResets.setVisible(false);
			m_jplMain.setPreferredSize(new Dimension(500,h));
			break;
		case AUTO:
		default:
			m_modeLb.setVisible(false);
			m_modeBtn.setVisible(false);
			m_kwlGroups.setVisible(false);
			m_kwlReads.setVisible(true); 
			m_kwlReads.setEnable(false);
			m_kwlReadsIgnored.setVisible(false);
			m_kwlResets.setVisible(false);
			m_jplMain.setPreferredSize(new Dimension(500,h));
		}
		sendPack();
	}
	
	
	Integer validInt(String txt) {
		if (txt.compareTo("") != 0) {
			try {
				Integer v = Integer.parseInt(txt);
				return v;
			} catch (NumberFormatException e) {	}
		}
		return null;
	}
	
	/**
	 * In AUTO mode, set SAMPMODE, GROUPS, IGNOREDREADS, and NUMREADS according to ITIME (exposure time)
	 * In NORMAL mode, set SAMPMODE, GROUPS, and IGNOREDREADS according to ITIME (exposure time)
	 * In ADVANCED mode, don't set anything.
	 *
	 * Because of the m_sent flag (see AUTO case, below), things can go wrong if we call
	 * this routine twice per button push -- and that will happen in setAll (once per
	 * entry with user input, and once after all entries have been handled).
	 */
	protected void setupExp() {
		switch (m_userMode) {
		case AUTO:
			setModeDefaults();
			// Server may not have accepted latest ITIME value set in this GUI,
			// which would make the ITIME keyword value out of date.
			double iTime = m_gkwITIME.getVal(); // integer milliseconds
			if (m_kwlExpo.m_sent != null) {
				// see comments in SecsKwLine.m_sent declaration
				iTime = m_kwlExpo.m_sent;
				if (!m_kwlExpo.m_sentFrozen) {
				    m_kwlExpo.m_sent = null;
				}
			}
			int nreads = calcAutoReads(iTime);
			CLog.notice("setupExp Obs#="+m_gkwFRAMENUM.getVal()+" ITIME="+iTime+" setting NUMREADS to "+nreads);
			m_gkwNUMREADS.queWrite(nreads, null);
			break;
		case NORMAL:
			setModeDefaults();
			break;
		case ADVANCED:
		default:
			break;
		}
	}
	
	/**
	 * Get number of reads from itime value in milliseconds
	 * @param itime
	 * @return
	 */
	int calcAutoReads(double itime_millis) {
		int nreads = m_gkwNUMREADS.getVal();
		double iTime = itime_millis/1000.0;
		// Full window needs different nread values than partial window
		if (mWindowFrame.fullWindow()) { 
			nreads = 1;
			if (iTime <= 2.91) {
				nreads = 1;
			} else if (iTime <= 5.82) {
				nreads = 2;
			} else if (iTime <= 11.64) {
				nreads = 4;
			} else if (iTime <= 23.28) {
				nreads = 8;
			} else {
				nreads = 16;
			}
		} else {
			double readTime = getWindowedReadTime();
			if (iTime < 2*readTime ) {
				nreads = 1;
			} else if (iTime < 4*readTime ) {
				nreads = 2;
			}  else if (iTime < 8*readTime ) {
				nreads = 4;
			}  else if (iTime < 16*readTime ) {
				nreads = 8;
			}  else {
				nreads = 16;
			}
		}
		return nreads;
	}
	
	/**
	 * Set defaults for Auto and NORMAL user modes.
	 */
	private void setModeDefaults() {
		if (m_gkwSAMPMODE.getValString().compareTo("MCDS") != 0) {
			m_gkwSAMPMODE.queWriteAscii("MCDS", null); // MCDS = 3
		}
		if (m_gkwGROUPS.getVal() != 2) {
			m_gkwGROUPS.queWrite(2, null);
		}
		if (m_gkwIGNOREDREADS.getVal() != 0 ) {
			m_gkwIGNOREDREADS.queWrite(0, null);
		}
	}
	
	

	

	class ActionAdvancedMode extends AbstractAction {
		private UserMode m_uMode = UserMode.ADVANCED;
		SharcsGUI m_parentFrm;
		public ActionAdvancedMode(String text, UserMode userMode, SharcsGUI parentFrm) {
            super(text);
            m_uMode = userMode;
            m_parentFrm  = parentFrm;
        }
		@Override
		public void actionPerformed(ActionEvent e) {
			m_parentFrm.setAdvancedMode(m_uMode);
			
		}
		private static final long serialVersionUID = 1L;
	}


    class ActionCreateWindow extends AbstractAction {
        String m_text;
        int    m_WindowType;
        SharcsGUI m_parentFrm;
        public ActionCreateWindow(String text, int windowType, SharcsGUI parentFrm) {
            super(text);
            m_text = text;
            m_WindowType = windowType;
            m_parentFrm  = parentFrm;
        }

        public void actionPerformed(ActionEvent ae) {
            switch (m_WindowType) {
            case LOG_WIND:
                m_LogWind.showWind();
                break;
            case RESIZE_FRM:
                CLog.notice("Resizing frame");
                sendPack();
                break;
            case WINDOW_WIND:
            	mWindowFrame.showWind();
            	break;
            case SETUP_WIND:
            	mSetupFrame.showWind();
            	break;
            case ACTION_WIND:
            	mActionFrame.showWind();
            	break;
            case STATUS_WIND:
            	mStatusFrame.showWind();
            	break;
          
            case PALETTE_WIND:
            	m_paletteFrm.showWind();
            	break;
            case TAKEDARKS_WIND:
            	m_takeDarksFrm.showWind();
            	break;
            case CONFIG_OPEN: 
            case CONFIG_SET:    
            case CONFIG_CREATE: 
            case SCRIPTS_RUN: 
            	CLog.err("Menu action for:"+m_text+" needs code!!!");
            	break;

            default:
                CLog.err("ActionCreateWindow unknown "+m_text+" "+m_WindowType);
            }
        }
        
        private static final long serialVersionUID = 1L;
    }
    
    
    public KTLJNAService getServSharcs() {
    	return m_servSharcs;
    }
    
    public KTLJNAService getServSaomot() {
    	return m_servSaomot;
    }
    
 
    /**
     * This class produces a label that indicates if we've lost the connection
     * to the server or if the server has lost its connection to the controller.
     * @author jgates
     *
     */
    class StateLabel extends JLabel implements GKeyConnection {

		SharcsGUI m_mainFrame;
		
		GTag       m_tag = new GTag(StateLabel.class.getName());
        
        private String m_statusmain = "";
        private String m_heart = "";
        
        @SuppressWarnings("rawtypes")
		TreeMap<String, GKey> m_GUIKeys = new TreeMap<String, GKey>();
        
        final int OK                = 0;
        final int LOST_CON          = 1;
        final int LOST_SERV         = 2;
        int m_nLastStatus           = -1;

        TrigThrd  m_TrigThrd;
        
        String m_baseText = "basetxt";
        
        // Heartbeat items
        String m_prevHeart = "z"; // Just an unlikely value to start. As long as it changes over time, it's ok. 
        SimpleTimer m_TimedOut = new SimpleTimer(12.0);

        StateLabel(String label, SharcsGUI mainFrame) {
            super("   "+label+"    ", CENTER);
            m_baseText = this.getText();
            m_mainFrame = mainFrame;
            
            // The state of this label is controlled by KTL keywords.
            // This is where we get information about these keywords.
            StatusMsg stnkmsg = new StatusMsg();
            LinkedList<GKeyString> gkl = new LinkedList<GKeyString>();
            gkl.add(m_gkwSTATUSMAIN);
            gkl.add(m_gkwHEARTBEAT);
            for (GKeyString gk:gkl) {
            	String name = gk.getName();
            	CLog.notice(this.getClass()+" adding GUI keyword "+name);
            	gk.putNotify(getTag(), stnkmsg);
            	m_GUIKeys.put(name, gk);
            }
            
            updateDisplay();
        }
        
        class StatusMsg extends NKMsg implements Runnable {
        	@SuppressWarnings("rawtypes")
			GKey   m_gkw = null;
        	String m_val = null;
        	StatusMsg() {};
        	StatusMsg(@SuppressWarnings("rawtypes") GKey gkw, String val) {
        		m_gkw = gkw; m_val = val;
        	}
        	
			@Override
			public void run(KTLKeyword kd, Object val, String valStr) {
				// Run in client thread
				// Must create a new message to avoid race conditions.
				SwingUtilities.invokeLater(new StatusMsg(kd, kd.getValString()));
			}

			@Override
			public void run() {
				// Run in swing thread
				if (m_gkw == m_gkwHEARTBEAT) {
					m_heart = m_val;
				} else if (m_gkw == m_gkwSTATUSMAIN) {
					m_statusmain = m_val;
				}
				updateDisplay();
			}
        }



        public void updateDisplay() {
        	String heart = m_heart;
        	if ( heart.compareTo(m_prevHeart) != 0 ) {
        		m_prevHeart = heart;
    			m_TimedOut.reset(); 
    		}
            if (m_TimedOut.waitIsOver()) {
                setText("Lost Server");
                setBackground(Color.RED);
                setOpaque(true);
                if (m_nLastStatus != LOST_SERV) {
                    m_nLastStatus = LOST_SERV;
                    m_LogWind.addMsg("Lost connection to the server");
                    //m_mainFrame.sendPack();
                }
            } else if (m_statusmain.compareTo("") != 0) {
                setText(m_statusmain);
                setBackground(null);
                setOpaque(false);
                setToolTipText(m_statusmain);
            } else {
                setText(m_baseText);
                setToolTipText(m_statusmain);
                setBackground(null);
                setOpaque(false);
                if (m_nLastStatus != OK) {
                    m_nLastStatus = OK;
                    m_LogWind.addMsg("Connected to server and controller");
                    //m_mainFrame.sendPack();
                }
            } 
        }
        private static final long serialVersionUID = 1L;

		@Override
		public GTag getTag() {
			return m_tag;
		}
		@Override
		public void removeNotifyTag() {
			m_gkwSTATUSMAIN.removeNotifyTag(getTag());
			m_gkwHEARTBEAT.removeNotifyTag(getTag());
		}
    }

        
    
    /**
     * This class will set the keyed axis name to the name of
     * the DataList represented by its row.
     */
    class RadioButtonSecMillisec extends EasyRadioButton {

		RadioButtonSecMillisec(String milliOrSec) {
            super(milliOrSec);
        }
        
        public void doThisOnClick() {
        	//SecsDisplay.Units units = SecsDisplay.Units.SECS;
        	SecsDisplay.Units units = m_timeUnits;
            if (getText().compareToIgnoreCase("Millisecs") == 0) {
                units = SecsDisplay.Units.MILLIS;
            } else {
                units = SecsDisplay.Units.SECS;
            }
            m_timeUnits = units;
            setAllSecDisplay(units);
        }
        private static final long serialVersionUID = 1L;
    }
    
    /**
     * Set all the time items that show seconds or milliseconds to the same unit type so
     * the GUI has consistent unit displays.
     * @param units
     */
    public void setAllSecDisplay(SecsDisplay.Units units) {
    	Iterator<SecsDisplay> iter = m_secsDisplayList.iterator();
        while(iter.hasNext()) {
        	SecsDisplay sd = iter.next();
        	sd.setDisplayType(units);
        }
        
    	if (m_paletteFrm != null) m_paletteFrm.update();
    	if (m_takeDarksFrm != null) m_takeDarksFrm.fireTableDataChanged();
    }
    
    
    
    
    /**
     * This thread simply sleeps for a while and then calls appropriate GUI
     * functions when it wakes up.
     * @author jgates
     *
     */
    class TrigThrd extends Thread {
        public volatile boolean m_bLoop = true;

        public void run() {
            try {
                while (m_bLoop) {
                    sleep(250);
                    if (m_bLoop) {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                            	updateDisplay();
                            	m_stateLabel.updateDisplay();
                            	
                            }
                        });
                    }
                }
            } catch (Exception e) {
                CLog.exceptionStr(e);
            }
        }
    }

    protected void sendPack() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                pack();
            }
        });
    }

    
    
    /**
     * Make this class purely so we can override the setUserInput function to turn the set all button yellow
     * when user input is entered. 
     */
    class ShKwLine extends KwLine {
    	public ShKwLine(String label, @SuppressWarnings("rawtypes") GKey key, LinkedList<KwLine> gParms)
    	throws GeneralException {
    		super(label, key, gParms);
    	}
    	

		@Override 
    	protected void setUserInput(boolean tf) {
    		super.setUserInput(tf);
    		updateSetAllBtn();
    	}

		public void setTextField(String str) {
			getTextField().setText(str);
		}
    }


    class KeyTimeLabel extends KeyIntLabel implements SecsDisplay {
    	
		SecsDisplay.Units m_unitsBase    = null;
    	SecsDisplay.Units m_unitsDisplay = null;
    	DecimalFormat m_decimalFormat    = null;
    	private double m_scale = 1.0;
		public KeyTimeLabel(GKeyInt key, String text, SecsDisplay.Units unitsBase) {
			super(key, text);
			setBaseType(unitsBase);
		}
		
	    @Override
	    public String getValueString() {
	    	String s = "--";
	    	if (getBadData()) {
	    		return s;
	    	}
	    	double v = (double) super.getValue();
	    	v = v * m_scale;
	    	if (m_decimalFormat == null) {
	    		// Have to define this here as super.constructor calls this function.
	    		m_decimalFormat = new DecimalFormat("#.###");
	    	}
	    	s = m_decimalFormat.format(v);
	    	return s;
	    }
	    
	  
	    public int getValue() {
	    	double v = (double) super.getValue();
	    	v = v * m_scale;
	        return (int) v;
	    }
		
		@Override
		public void setBaseType(Units b) {
			m_unitsBase = b;
			updateDisplay();
		}
		@Override
		public void setDisplayType(Units t) {
			m_unitsDisplay = t;
			if (m_unitsBase == SecsDisplay.Units.SECS) {
				if (m_unitsDisplay == SecsDisplay.Units.MILLIS) {
					m_scale = 1000.0 ;
				} else {
					m_scale = 1.0;
				}
			} else { // m_unitsBase == SecsDisplay.Units.MILLIS
				if (m_unitsDisplay == SecsDisplay.Units.SECS) {
					m_scale  =  0.001;
				} else {
					m_scale = 1.0;
				}
			}
			updateDisplay();
		}
		@Override
		public Units getBaseType() {
			return m_unitsBase;
		}
		
		@Override
		public Units getDisplayType() {
			return m_unitsDisplay;
		}
		
		public double getScale() {
			return m_scale;
		}
		private static final long serialVersionUID = 1L;
    }
    


    /**
     * Switch between display of Seconds and Milliseconds
     * @author jgates
     *
     */
    class SecsKwLine extends ShKwLine implements SecsDisplay {
    	KeyTimeLabel m_currentTime = null; // m_current and m_currentTime will point to the same object
    	                                   // one the constructor runs. The old m_current object is abandoned.
    	Double m_sent = null; // There's a race condition where a the exposure time (ITIME keyword) is
    	  // set and then used to calculate the reads and resets. The problem is that the calculation
    	  // is done before the server can broadcast the value back to this client. This value is 
    	  // set to a the most recent value sent to the server and is set back to null if the
    	  // value is used to calculate normal reset values.
    	  // You can't just set it up to apply the read and reset values when ITIME changes as
    	  // you don't want that happening to the other client when 2 clients are open.
    	  // Doing this in the client is kind of hacky, the read and reset times should relly be happening
    	  // in the server.
	boolean m_sentFrozen = false;	// If true, the internal m_sent value is not set to null
					// after use by setupExp().
	public void freeze_m_sent(boolean freeze) {
	    if (freeze) {
		m_sentFrozen = true;
	    } else {
		m_sentFrozen = false;
		m_sent = null;
	    }
	}

    	public SecsKwLine(String label, GKeyInt kw, LinkedList<KwLine> gParms, SecsDisplay.Units unitsBase) 
    	throws GeneralException {
    		super(label, kw,  gParms);
    		// m_current is set to the wrong type, it needs to be time dependent.
    		// disconnect it and assign correct type
    		m_currentTime = new KeyTimeLabel(kw, "", unitsBase);
    		replaceCurrentLabelWith(m_currentTime);
    		setBaseType(unitsBase);
    	}

    	@Override
    	public void setBaseType(Units b) {
    		m_currentTime.setBaseType(b);
    	}

    	@Override
    	public void setDisplayType(Units units) {
    		m_currentTime.setDisplayType(units);
    	}

    	@Override
    	public void sendValToServer() {
    		String en = m_tf.getText();
    		System.out.print("send val to server:"+en+"\n");
    		double scale = m_currentTime.getScale();
    		try {
    			// see comments in SecsKwLine.m_sent declaration
    			m_sent = Double.parseDouble(en) / scale;
    			double val = m_sent; 
    			((GKeyInt)m_currentTime.getKey()).queWrite((int)val, null);
    		} catch (NumberFormatException ne) {
    			CLog.notice(this+".sendValToServer key:"+m_kw.getName()+" exception="+ne.getLocalizedMessage());
    			JOptionPane.showMessageDialog(this.m_tf, "Not sent. Could not convert "+en, "Inane error",
    					JOptionPane.ERROR_MESSAGE, null);
    			return;
    		}
    		setUserInput(false);
    		if (m_clearTextWhenSet) {
    			m_tf.setText("");
    		}
    	}
    	
    	@Override
    	public Units getBaseType() {
    		return m_currentTime.getBaseType();
    	}
    	
    	@Override
    	public Units getDisplayType() {
    		return m_currentTime.getDisplayType();
    	}

    	
		public void setTextFieldMilliIn(int exposure) {
			// Incoming units are in milliseconds, if the display type
			// is seconds, we need to multiply by 0.001
			Units disp = m_currentTime.getDisplayType();
			if (disp == Units.SECS) {
				// use double for seconds
				double v = exposure;
				v = v * 0.001;
				setTextField(Double.toString(v));
			} else { // in Units.MILLIS
				// use integer for milliseconds
				setTextField(Integer.toString(exposure));
			}
		}
    }

    public class ExposureKwLine extends SecsKwLine {
    	public ExposureKwLine(String label, GKeyInt kw,
    			LinkedList<KwLine> gParms, Units unitsBase) throws GeneralException {
    		super(label, kw, gParms, unitsBase);
    	}

    	@Override
    	public void sendValToServer() {
    		super.sendValToServer();
CLog.notice("sendValToServer calling setupExp");
    		setupExp();
    	}
    	
    }
	
    class ModeMenuButton extends KeyEnumMenuButton {

		ModeMenuButton(GKeyEnum kw, String text) {
    		super(kw, text, null);
    	}
    	
    	@Override 
    	public void updateDisplay() {
    		super.updateDisplay();
    		String s = getKeyValStr();
    		
    		// Enable all parameters and disable later as needed.
    		for (KwLine kwl:m_gParms) {
    			kwl.enable(true);
        	}
            if (s.compareToIgnoreCase("MCDS") == 0) {
            	m_kwlGroups.enable(false);
            } else if (s.compareToIgnoreCase("UTR") == 0) {
            	; // all available
            } else if (s.compareToIgnoreCase("CDS") == 0) {
            	m_kwlGroups.enable(false);
            	m_kwlReads.enable(false);
            }
            // otherwise everything available
    	}
		private static final long serialVersionUID = 1L;
    }

    class CalcMsg extends NKMsg implements Runnable{
    	@Override
		public void run(KTLKeyword kd, Object val, String valStr) {
			SwingUtilities.invokeLater(this);
		}
		@Override
		public void run() {
			updateDisplay();
		}  
    }
}











