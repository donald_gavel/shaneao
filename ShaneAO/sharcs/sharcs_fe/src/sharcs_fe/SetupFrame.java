package sharcs_fe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import org.uco.gui_bits.ButtonGKeyWriteVal;
import org.uco.gui_bits.EasyRadioButton;
import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.gui_bits.KeyEnumRadioButtons;
import org.uco.gui_bits.KeyIntLabel;
import org.uco.gui_bits.KeyStringLabel;
import org.uco.gui_bits.KwLine;
import org.uco.java_util.GKeyConnection;
import org.uco.java_util.GKeyEnum;
import org.uco.java_util.GKeyInt;
import org.uco.java_util.GKeyString;
import org.uco.java_util.GTag;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNAService;

public class SetupFrame extends FrameCloseCaptured implements GKeyConnection {
	SharcsGUI mGui;
	
	ButtonGKeyWriteVal<Integer, GKeyInt> mCONNECT_btn;
	KeyIntLabel mCONNECTED_val;

	KeyIntLabel mINITING_val;
	ButtonGKeyWriteVal<Integer, GKeyInt> mINIT_btn;

	private KeyStringLabel mSIDECARINIT_val;
	private KeyEnumRadioButtons mWRITEALL_btn;
	private KwLine mSETRPSTYPE_kwl;
	private KwLine mUNIXROOTDIR_kwl;
	private KwLine mWINROOTDIR_kwl;
	private KwLine mOUTDIR_kwl;
	private KwLine mASICCFG_kwl;
	private KwLine mASICHOST_kwl;
	private KwLine mASICPORT_kwl;

	private GTag mGTag = new GTag(SetupFrame.class.getName());
	
	@SuppressWarnings("unchecked") // has to do with ButtonGKeyWriteVal casting
	public SetupFrame(SharcsGUI gui) throws GeneralException {
		super("Setup");
		mGui = gui;
		KTLJNAService serv = mGui.getServSharcs();
		LinkedList<KwLine> m_gParms = new LinkedList<KwLine>();

		Container pan = getContentPane();
		pan.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		pan.add(bottomPanel, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn);
		
		JPanel main = new JPanel(); 
		pan.add(main);
		main.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 100.0; gbc.weighty = 100.0;

		mCONNECTED_val = new KeyIntLabel((GKeyInt) serv.getGKey("CONNECTED"), "");
		mCONNECT_btn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(serv.getGKey("CONNECT"), "Connect", 1);
		
		mINITING_val = new KeyIntLabel((GKeyInt) serv.getGKey("INITING"), "");
		mINIT_btn = (ButtonGKeyWriteVal<Integer, GKeyInt>) ButtonGKeyWriteVal.create(serv.getGKey("INIT"), "Init", 1);
		
		mSIDECARINIT_val = new KeyStringLabel((GKeyString) serv.getGKey("SIDECARINIT"), "");
		
		GKeyEnum writeAll = (GKeyEnum) serv.getGKey("WRITEALL");
		mWRITEALL_btn = new KeyEnumRadioButtons(writeAll, null, null);

		mSETRPSTYPE_kwl  = new KwLine("SetRpsType",   serv.getGKey("SETRPSTYPE"),   m_gParms);
		mUNIXROOTDIR_kwl = new KwLine("UnixRootDir",  serv.getGKey("UNIXROOTDIR"),  m_gParms);
		mWINROOTDIR_kwl  = new KwLine("WinRootDir",   serv.getGKey("WINROOTDIR"),   m_gParms);
		mOUTDIR_kwl      = new KwLine("OutDir",       serv.getGKey("OUTDIR"),       m_gParms);
		mASICCFG_kwl     = new KwLine("AsicCfg",      serv.getGKey("ASICCFG"),      m_gParms);
		mASICHOST_kwl    = new KwLine("AsicHost",     serv.getGKey("ASICHOST"),     m_gParms);
		mASICPORT_kwl    = new KwLine("AsicPort",     serv.getGKey("ASICPORT"),     m_gParms);

		int ii = 0;
		int colName   = ii++; // name column
		int colCurr   = ii++; // current value column
		int colTF     = ii++; // text field column
		int colBtn    = ii++; // set button column
		Border border = new EmptyBorder(0,10,0,0);
		
		gbc.gridy++;
		gbc.gridx = colName;
		gbc.weightx = 0.0;
		main.add(new JLabel("Connected"), gbc);
		gbc.gridx = colCurr;
		gbc.weightx = 100.0;
		mCONNECTED_val.setHorizontalAlignment(JLabel.LEFT);
		mCONNECTED_val.setBorder(border);
		main.add(mCONNECTED_val, gbc);
		gbc.gridwidth = 1;
		gbc.gridx = colTF;
		gbc.weightx = 0.0;
		main.add(mCONNECT_btn, gbc);

		gbc.gridy++;
		gbc.gridx = 0;
		gbc.weightx = colName;
		main.add(new JLabel("Initing"), gbc);
		gbc.gridx = colCurr;
		gbc.gridwidth = 1;
		gbc.weightx = 100.0;
		mINITING_val.setHorizontalAlignment(JLabel.LEFT);
		mINITING_val.setBorder(border);
		main.add(mINITING_val, gbc);
		gbc.gridwidth = 1;
		gbc.gridx = colTF;
		gbc.weightx = 0;
		main.add(mINIT_btn, gbc);
		
		gbc.gridy++;
		gbc.gridx = colName;
		gbc.weightx = 0.0;
		main.add(new JLabel("SideCarInit"), gbc);
		gbc.gridx = colCurr;
		gbc.gridwidth = 3;
		gbc.weightx = 100.0;
		mSIDECARINIT_val.setHorizontalAlignment(JLabel.LEFT);
		mSIDECARINIT_val.setBorder(border);
		main.add(mSIDECARINIT_val, gbc);
		gbc.gridwidth = 1;
		
		gbc.gridy++;
		gbc.gridx = 0;
		gbc.weightx = colName;
		main.add(new JLabel("WriteAll"), gbc);
		gbc.gridx = colCurr;
		gbc.weightx = 100.0;
		EasyRadioButton[] rbts = mWRITEALL_btn.getButtons();
		for (EasyRadioButton erb : rbts) {
			main.add(erb, gbc);
			gbc.gridx++;
		}
		
		gbc.gridy++;
		gbc.gridx = 0;
		

		KwLine[] kwlItems = { mSETRPSTYPE_kwl, mUNIXROOTDIR_kwl, mWINROOTDIR_kwl, mOUTDIR_kwl,
				mASICCFG_kwl, mASICHOST_kwl,  mASICPORT_kwl};
		ii = gbc.gridy;
		gbc.weightx = 0.0;
		for (KwLine kwl:kwlItems) {
			gbc.gridy = ii++;
			kwl.m_current.setBorder(border);
			kwl.place(main, gbc, colName, colCurr, colTF, colBtn);
		}
		
	
	}
	private static final long serialVersionUID = 1L;

	@Override
	public GTag getTag() {
		return mGTag;
	}
	@Override
	public void removeNotifyTag() {
		mCONNECTED_val.removeNotifyTag();

		mINITING_val.removeNotifyTag();

		mSIDECARINIT_val.removeNotifyTag();
		mWRITEALL_btn.removeNotifyTag();
		mSETRPSTYPE_kwl.removeNotifyTag();
		mUNIXROOTDIR_kwl.removeNotifyTag();
		mWINROOTDIR_kwl.removeNotifyTag();
		mOUTDIR_kwl.removeNotifyTag();
		mASICCFG_kwl.removeNotifyTag();
		mASICHOST_kwl.removeNotifyTag();
		mASICPORT_kwl.removeNotifyTag();
	}
}


