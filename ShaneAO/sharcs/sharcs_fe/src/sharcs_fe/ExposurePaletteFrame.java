package sharcs_fe;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.TreeMap;



import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import javax.swing.table.AbstractTableModel;

import org.uco.gui_bits.FrameCloseCaptured;
import org.uco.java_util.CLog;
import org.uco.java_util.GeneralException;
import org.uco.ktljna_j.KTLJNALibInstance;








public class ExposurePaletteFrame extends FrameCloseCaptured {
	KTLJNALibInstance m_ktl;
	SharcsGUI m_sharcsGUI;

	// Table variables
	ExposurePaletteTableModel m_tblModel = new ExposurePaletteTableModel(this, new ExposureMap());
	JTable                    m_jtable;
	int[]                     m_selectedRows = new int[0];

	// Handling keyword updates. There are 4 array keywords and there should be 1 length keyword
	//ExposureArrays m_expArrays = new ExposureArrays(0);  // arrays of values as read from the server


	public ExposurePaletteFrame(SharcsGUI sharcsGUI) {
		super("Exposure Palette");
		m_sharcsGUI = sharcsGUI;
		m_ktl = m_sharcsGUI.m_ktl;

		//setDefaultExposures(); 

		// make the frame and layout
		Container pan = getContentPane();
		pan.setLayout(new BorderLayout()); 
		pan.setSize(580,750);           

		JPanel bottomPanel = new JPanel();
		pan.add(bottomPanel, BorderLayout.SOUTH);
		JButton closeBtn = new JButton("Close");
		closeBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ExposurePaletteFrame.this.doThisOnFrameClose();
			}
		});
		bottomPanel.add(closeBtn);

		m_jtable = new JTable(m_tblModel);
		JScrollPane tabTestScroll = new JScrollPane(m_jtable);
		m_jtable.setPreferredScrollableViewportSize(new Dimension(200,300));
		pan.add(tabTestScroll);
		pan.setVisible(true);

		// we need a list selection model so we can listen for events
		ListSelectionModel tblListSel = m_jtable.getSelectionModel();
		m_jtable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblListSel.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent ev) {
				String s = "selected rows:";
				int[] rows = m_jtable.getSelectedRows();
				for(int i=0; i<rows.length; i++) s += rows[i]+" ";
				System.out.println("l="+rows.length+" s:"+s);
				m_selectedRows = rows;
				// Copy values of selected Exposure to the text entry boxes for next exposure.
				if (m_selectedRows.length > 0) {
					Exposure exp = m_tblModel.m_exposures[m_selectedRows[0]];
					m_sharcsGUI.m_kwlExpo.setTextFieldMilliIn(exp.getExposure());
					m_sharcsGUI.m_kwlReads.setTextField(Integer.toString(exp.getReads()));
					m_sharcsGUI.m_kwlResets.setTextField(Integer.toString(exp.getResets()));
					m_sharcsGUI.m_kwlCoadds.setTextField(Integer.toString(exp.getCoadds()));
				}
			}
		});

	}

	/**
	 * This should be for testing purposes only. The server should really maintain the true list
	 * of exposures. TODO
	 */
	void setDefaultExposures() {
		ExposureMap em = m_tblModel.m_expMap;
		em.put(new Exposure(0, 1500,1,1,1,true));
		
		// the rest are just for testing
		/*
		em.put(new Exposure(5, 100,1,2,4,false));
		em.put(new Exposure(6, 100,1,2,3,false));
		em.put(new Exposure(7, 200,2,2,8,false));
		em.put(new Exposure(8, 400,4,4,4,false));
		em.put(new Exposure(9, 500,8,8,16,false));
		em.put(new Exposure(10, 1000,16,16,8,false));
		em.put(new Exposure(50, 2000,4,4,4,false));
		em.put(new Exposure(51, 2000,16,16,4,false));
		em.put(new Exposure(52, 2000,4,8,4,false));
		em.put(new Exposure(53, 2000,8,4,4,false));
		em.put(new Exposure(54, 3000,16,16,4,false));
		em.put(new Exposure(55, 3000,1,16,4,false));
		em.put(new Exposure(40, 4000,16,16,4,false));
		em.put(new Exposure(42, 5000,16,16,4,false));
		*/
		
		m_tblModel.update();
	}



	
	/**
	 * clear and rebuild the table model using the values in exps.
	 * @param exps
	 * @return
	 */
	public int  updateMap(LinkedList<Exposure> exps) {
		ExposureMap expMap = m_tblModel.m_expMap;
		expMap.clear();
		for (Exposure ex: exps) {
			expMap.put(ex);
		}
		m_tblModel.update();
		return m_tblModel.getRowCount();
	}

	private static final long serialVersionUID = 1L;



	public void removeNonDefaults() {
		CLog.notice(this+".removeNonDefaults() NEEDS CODE"); 
		ExposureMap em = m_tblModel.m_expMap;
		em.removeNonStandard();
		m_sharcsGUI.sendServerPaletteKeys();
	}

	public void clearAllCounts() {
		ExposureMap em = m_tblModel.m_expMap;
		em.clearAllCounts();
		m_sharcsGUI.sendServerPaletteKeys();
	}
	
	public void update() {
		m_tblModel.update();
	}
	
}



/**
 * Basic entry in the exposure palette.
 * Aside from m_count, it is immutable.
 * @author jgates
 *
 */
class Exposure implements Comparable<Exposure>{
	// x millisecond exposure, y reads, z resets
	private int m_exposureMillisec;
	private int m_reads;
	private int m_resets;
	private int m_coadds;
	int m_count = 0;    // number of images taken
	private int m_index; // index into the array keywords

	private boolean m_standard = false;

	public Exposure(int index, int exposureMillisec, int reads, int resets, int coadds, int count) {
		m_index            = index;
		m_exposureMillisec = exposureMillisec;
		m_reads            = reads;
		m_resets           = resets;
		m_coadds           = coadds;
		m_count            = count;
		m_standard         = false;
	}

	public Exposure(int index, int exposureMillisec, int reads, int resets, int coadds, boolean standard) {
		m_index            = index;
		m_exposureMillisec = exposureMillisec;
		m_reads            = reads;
		m_resets           = resets;
		m_coadds           = coadds;
		m_standard         = standard;
	}

	public Exposure(int index, int exposureMillisec, int reads, int resets, int coadds, boolean standard, int count) {
		m_index            = index;
		m_exposureMillisec = exposureMillisec;
		m_reads            = reads;
		m_resets           = resets;
		m_coadds           = coadds;
		m_standard         = standard;
		m_count            = count;
	}

	public Exposure(Exposure ex) {
		m_index            = ex.m_index;
		m_exposureMillisec = ex.m_exposureMillisec;
		m_reads            = ex.m_reads;
		m_resets           = ex.m_resets;
		m_coadds           = ex.m_coadds;
		m_standard         = ex.m_standard;
		m_count            = ex.m_count;
	}

	public double getExposureSec() {
		return ((double)m_exposureMillisec)/1000.0;
	}
	public double getExposureMiilisec() {
		return (double)m_exposureMillisec;
	}

	public int getIndex() {return m_index;}
	public int getExposure() {return m_exposureMillisec;}
	public int getReads() { return m_reads;}
	public int getResets() { return m_resets; }
	public int getCoadds() { return m_coadds; } 
	public boolean getStandard() {return m_standard;}

	@Override 
	public String toString() {
		String s = "exposure="+m_exposureMillisec+" reads="+m_reads+" resets="+m_resets+" coadds="+m_coadds+" count="+m_count+" standard="+m_standard;
		return s;
	}

	@Override
	public int compareTo(Exposure o) {
		// priority exposure, reads, resets, coadds.  Counts don't matter for this
		if (this.m_exposureMillisec > o.m_exposureMillisec) {
			return 1;
		} else if (this.m_exposureMillisec < o.m_exposureMillisec) {
			return -1;
		} else if (this.m_reads > o.m_reads) {
			return 1;
		} else if (this.m_reads < o.m_reads) {
			return -1;
		} else if (this.m_resets > o.m_resets) {
			return 1;
		} else if (this.m_resets < o.m_resets) {
			return -1;
		} else if (this.m_coadds > o.m_coadds) {
			return 1;
		} else if (this.m_coadds < o.m_coadds) {
			return -1;
		}
		// they must be equal
		return 0;
	}

	// Compare only Exposuretime, Reads, Resets.
	public int compareExReRe(Exposure o) {
		// compare exposure, reads, resets.
		if (this.m_exposureMillisec > o.m_exposureMillisec) {
			return 1;
		} else if (this.m_exposureMillisec < o.m_exposureMillisec) {
			return -1;
		} else if (this.m_reads > o.m_reads) {
			return 1;
		} else if (this.m_reads < o.m_reads) {
			return -1;
		} else if (this.m_resets > o.m_resets) {
			return 1;
		} else if (this.m_resets < o.m_resets) {
			return -1;
		}
		// they must be equal
		return 0;
	}

}




class ExposureMap {
	TreeMap<Exposure, Exposure> m_map = new TreeMap<Exposure,Exposure>();

	/**
	 * Returns true if 'ex' was added to the exposure palette map.
	 * Returns false is 'ex' was already in the map.
	 */
	public boolean put(Exposure ex) {
		if (m_map.containsKey(ex)) {
			return false;
		}
		m_map.put(ex, ex);
		return true;
	}

	public boolean put(Exposure ex, int count) {
		put(ex);
		Exposure e = getExposure(ex);
		if (e != null) {
			e.m_count = count;
			return true;
		} 
		return false;
	}
	
	public void clear() {
		m_map.clear();
	}

	/**
	 * Returns the exposure in the map with matching exposure time, reads, and resets. It does not care about counts.
	 * If a matching instance cannot be found, it returns null.
	 */
	public Exposure getExposure(Exposure ex) {
		Exposure e = m_map.get(ex);
		return e;
	}
	/**
	 * Returns the exposure in the map with matching exposure time, reads, and resets. It does not care about counts or index.
	 * If a matching instance cannot be found, it returns null.
	 */
	public Exposure getExposure(int millisec, int reads, int resets, int coadds) {
		// use an invalid index
		Exposure ex = new Exposure(-1, millisec, reads, resets, coadds, false);
		return getExposure(ex);
	}

	/**
	 * Find (or create) the exposure palette value and increment it's count. Returns the new count value, -1 indicates failure.
	 */
	public int incrementExposure(Exposure ex) {
		Exposure j = getExposure(ex);
		if (j == null) {
			put(ex);
			j = getExposure(ex);
			if (j == null) {
				CLog.err("ExposureMap.IncrementExposure failure, failed to insert "+ex.toString());
				return -1;
			}
		}
		j.m_count++;
		return j.m_count;
	}

	/**
	 * Remove exposure ex from the map.
	 * @param ex
	 */
	private void remove(Exposure ex) {
		m_map.remove(ex);
	}

	/**
	 * Set the count of all Exposures to 0.
	 */
	public void clearAllCounts() {
		for (Exposure ex : m_map.values()) {
			ex.m_count = 0;
		}
	}

	/**
	 * Remove all entries with Exposure m_standard == false
	 */
	public void removeNonStandard() {
		LinkedList<Exposure> ll = getList();
		Iterator<Exposure> iter = ll.iterator();
		while(iter.hasNext()) {
			Exposure ex = iter.next();
			if (ex.getStandard() == false) {
				remove(ex);
			}
		}
	}


	public LinkedList<Exposure>  getList() {
		LinkedList<Exposure> ll = new LinkedList<Exposure>();
		Iterator<Exposure> ex_i = m_map.values().iterator();
		while (ex_i.hasNext()) {
			Exposure ex = ex_i.next();
			ll.add(ex);
		}
		return ll;
	}

	public Exposure[] getArray() {
		LinkedList<Exposure> ll = getList();
		Exposure[] exa = new Exposure[ll.size()];
		Iterator<Exposure> iter = ll.iterator();
		int j = 0;
		while (iter.hasNext()) {
			exa[j] = iter.next();
			j++;
		}
		return exa;
	}

}



/**
 * Table model for the palette grid.
 * @author jgates
 *
 */
class ExposurePaletteTableModel extends AbstractTableModel {
	ExposureMap m_expMap  = null;
	Exposure[] m_exposures = new Exposure[0];
	String[] m_ColumnNames = { "Time", "Reads", "Resets", "Coadds", "Exposures" };
	
	ExposurePaletteFrame m_exposurePaletteFrame;

	ExposurePaletteTableModel(ExposurePaletteFrame exposurePaletteFrame, ExposureMap em) {
		super();
		m_exposurePaletteFrame = exposurePaletteFrame;
		setMap(em);
	}

	public int getRowCount() {return m_exposures.length;}
	public int getColumnCount() {return m_ColumnNames.length;}

	public void setMap(ExposureMap em) {
		m_expMap = em;
		m_exposures = m_expMap.getArray();
	}

	ExposureMap getMap() {
		return m_expMap;
	}

	public void update() {
		m_exposures = m_expMap.getArray();
		fireTableDataChanged();
	}

	public void clearAllCounts() {
		m_expMap.clearAllCounts();
		update();
	}

	@Override
	public String getColumnName(int c) {
		return m_ColumnNames[c];
	}

	public Object getValueAt(int row, int col) {   
		CLog.notice(this+".getValueAt row="+row+" col="+col);
		Exposure ex = m_exposures[row];
		if (ex == null) return "";
		switch (col) {
		case 0:
			if (m_exposurePaletteFrame != null && m_exposurePaletteFrame.m_sharcsGUI != null &&
				m_exposurePaletteFrame.m_sharcsGUI.isTimeInSeconds()) {
				return ex.getExposureSec();
			} else {
				return ex.getExposure();
			}		
		case 1:
			return ex.getReads();
		case 2:
			return ex.getResets();
		case 3:
			return ex.getCoadds();
		case 4:
			return ex.m_count;
		default:
			return "nada";
		}
	}

	public ExposureArrays getExposureArrays() {
		int[] expT   = new int[m_exposures.length];
		int[] reads  = new int[m_exposures.length];
		int[] resets = new int[m_exposures.length];	
		int[] coadds = new int[m_exposures.length];
		int[] counts = new int[m_exposures.length];
		for (int j=0; j<m_exposures.length; j++) {
			Exposure e = m_exposures[j];
			expT[j]   = e.getExposure();
			reads[j]  = e.getReads();
			resets[j] = e.getResets();
			counts[j] = e.m_count;
		}
		ExposureArrays res = null;
		try {
			res = new ExposureArrays(expT, reads, resets, coadds, counts);
		} catch (GeneralException e) {
			CLog.err(this+".getExposureArrays made arrays of different lengths");
			CLog.exceptionStr(e);
		}
		return res;
	}

	public boolean isCellEditable(int row, int col) {
		return false;
	}



	private static final long serialVersionUID = 1L;


}


class ExposureArrays {
	public int[] m_ex_times;
	public int[] m_reads;
	public int[] m_resets;
	public int[] m_coadds;
	public int[] m_counts;

	ExposureArrays(int length) {
		m_ex_times = new int[length];
		m_reads    = new int[length];
		m_resets   = new int[length];
		m_coadds   = new int[length];
		m_counts   = new int[length];
	}

	ExposureArrays(Integer[] ex_times, Integer[] reads, Integer[] resets, Integer[] coadds, Integer[] counts ) throws GeneralException {
		this(convertArr(ex_times), convertArr(reads), convertArr(resets), convertArr(coadds), convertArr(counts));
	}

	static int[] convertArr(Integer[] inArr) {
		int[] outArr = new int[inArr.length];
		for (int j = 0; j < inArr.length; j++) {
			outArr[j] = inArr[j]; 
		}
		return outArr;
	}


	ExposureArrays(int[] ex_times, int[] reads, int[] resets, int[] coadds, int[] counts ) throws GeneralException {
		if (ex_times.length == reads.length && resets.length == counts.length && coadds.length == counts.length && counts.length == ex_times.length) {
			m_ex_times = ex_times;
			m_reads    = reads;
			m_resets   = resets;
			m_coadds   = coadds;
			m_counts   = counts;
		} else {
			throw new GeneralException("Array lengths do not match expT="+ex_times.length+" reads="+reads.length+
					" resets="+resets.length+" coads="+coadds.length+" counts="+counts.length);
		}
	}
}


