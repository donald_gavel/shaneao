#! /bin/csh -f

if ($#argv < 2) then
   echo "SetUp.csh [-g gainLevel] [-f fowler#] [-mint 8] "
   echo " -g gainLevel:   set gain level to a HEX value (1, 5, 7, 9, b supported) "
   echo " -f fowler#:     set fowler minimum read time and number of reads"
   echo " -mint 8:        report the minimum read time for the current settings. Replace 8 with anything"
   echo " "
   exit
endif


alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l`'

set scriptdir=`dirname $0`
set set_asic="$scriptdir/SetAsic.csh"

set i = 0;
while ( $#argv )
  @ i = $i + 1;
  switch( $argv[1] )
   case -f:
     shift
     switch( $argv[1] )
        case 1:
          modify -s sharcs itime=1600
          modify -s sharcs numreads=1
          breaksw
        case 2:
          modify -s sharcs itime=3100
          modify -s sharcs numreads=2
          breaksw
        case 4:
          modify -s sharcs itime=5900
          modify -s sharcs numreads=4
          breaksw
        case 8:
          modify -s sharcs itime=11700
          modify -s sharcs numreads=8
          breaksw
        case 16:
          modify -s sharcs itime=23300
          modify -s sharcs numreads=16
          breaksw
        case 32:
          modify -s sharcs itime=46600
          modify -s sharcs numreads=32
          breaksw
        default: echo "Your gain setting is not found, program it in"
     endsw
     breaksw
   case -g:
     shift
     switch( $argv[1] )
#        case 1:
#           $set_asic -setGain  1
#           $set_asic -setVref 8328
#           breaksw
        case 5:  
           $set_asic -setGain  5 
           $set_asic -setVref 82c8
           breaksw
        case 6:
           $set_asic -setGain  6
           $set_asic -setVref 82c8
           breaksw
        case 7:  
           $set_asic -setGain  7 
           $set_asic -setVref 8288
           breaksw
        case 8:
           $set_asic -setGain  8
           $set_asic -setVref 8288
           breaksw
        case 9:  
           $set_asic -setGain  9 
           $set_asic -setVref 8258
           breaksw
        case a:
           $set_asic -setGain  a
           $set_asic -setVref 8258
           breaksw
        case 10:
           $set_asic -setGain  a
           $set_asic -setVref 8258
           breaksw
         case b:  
           $set_asic -setGain  b 
           $set_asic -setVref 8238
           breaksw
        case 11:
           $set_asic -setGain  b
           $set_asic -setVref 8238
           breaksw
         case c:
           $set_asic -setGain  c
           $set_asic -setVref 8238
           breaksw
         case 12:
           $set_asic -setGain  c
           $set_asic -setVref 8238
           breaksw
        default: echo "Your gain setting is not found, program it in"
     endsw
   breaksw
   case -mint: 
     set ngroups = `gshow -s sharcs groups       | cut -c17-20 ` ;
     set nreads  = `gshow -s sharcs numreads     | cut -c17-20 ` ;
     set ndrops  = `gshow -s sharcs ignoredreads | cut -c17-20 ` ;
     set readtt  = `gshow -s sharcs readtime     | cut -c17-20 ` ;
     set sampmode = `gshow -s sharcs sampmode    | cut -c16-18 ` ;
     #@ time = ( ( $ngroups - 1 ) * ( $nreads + $ndrops ) ) * 3 / 2
     #echo $time
     MATH time = ( ( $ngroups - 1 ) * ( $nreads + $ndrops ) ) * $readtt
     echo "Time (in sec) with the current settings:       " $time
     if ( $ngroups > 2 &&  "$sampmode" != "UTR"  ) then 
        echo "Remember, if the sampmode is not UTR, the exposure time will be different "
        echo " than calculated ...  CHANGE IT!"
     endif
     breaksw
   default: echo "Your options is not recognized yet, program it in or try again!"
  endsw
  shift
end
