#! /bin/csh -f

if ($#argv < 2) then
   echo "linearity.csh  number_to_take  skips "
   echo " "
   exit
endif


alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l`'

#modify -s sharcs groups=2 sampmode=4 numreads=1

#modify -s sharcs ignoredreads=0 itime=1500

set num = $argv[1];
shift
set skips = $argv[1];
echo " Taking linearity data with " $num " total steps and " $skips " skipped pairs"

set ngroups = 2;
set nreads = 1;
set sampmode = 4;
set readtt  = `gshow -s sharcs readtime     | cut -c17-20 ` ;


set i = 0;
while ($i<$num)
  echo "Ignored set to "$i
  MATH time = (( ( $ngroups - 1 ) * ( $nreads + $i ) ) * $readtt + 0.1) * 1000.
  #modify -s sharcs ignoredreads=$i itime=$time
  #modify -s sharcs go=1
  sleep $time+15.
  @ i = $i + 1 + $skips
end


