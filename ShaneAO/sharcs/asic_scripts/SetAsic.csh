#! /bin/csh -f

if ($#argv < 2) then
   echo "SetAsic.csh [-i itime] [-c coadds] [-r reads] [-g groups] [-z resets] [-d drops] "
   echo "   [-s sampling_mode] [-x minX] [-X maxX] [-y minY] [-Y maxY] "
   echo "   [-n ObjectName]  [-asiccfg serverConfig]  [-m mcdfile] "
   echo "   [-defdir] [-unixrootdir unixrootdir]  [-winrootdir winrootdir]  [-o outdir]"
   echo "   [-getVref] [-getGain] [-setVref vrefHexValue] [-setGain gainLevel]"
   echo "   [-getReg register]  [-setReg register hexvalue]"
   echo "  "
   echo " -i itime: 	[10000] integration time in millisec (non-negative)"
   echo " -c coadds:	[1] coadditions ( >0 )"
   echo " -r reads:   	[1] reads for utr, reads pairs for mcds, ignored for others"
   echo " -g groups:	[2] groups for utr, ignored for others"
   echo " -z resets:	[1] resets for all modes ( >0 )"
   echo " -d drops:  	[0] drops for utr, ignored for others"
   echo " -s samp_mode:	[2] sampling mode (number or word: 1=single, 2=cds, 3=mcds, 4=utr)"
   echo " -x minX	[0] start column (0-2047) for subregion."
   echo " -X maxX:	[2047] end column (0-2047) for subregion"
   echo " -y minY:	[0] start row (0-2047) for subregion"
   echo " -Y maxY:	[2047] end row (0-2047) for subregion"
   echo " -n ObjName	Object Name in header"
   echo " -getVref:     Get the current hex value of Vref"
   echo " -getGain:     Get the current hex value of the gain"
   echo " -setVref vrefHexValue:        set Vref to a hex value"
   echo " -setGain gainLevel:   	set gain level to a hex value (0 - f)"
   echo " -getReg register:     	Get the value of the specified register printed to the terminal"
   echo " -setReg register hexvalue:    Set a user-specified register to a specific hexvalue"
   echo " -defdir: 			Sets the default directory structure as detailed the below dir keywords"
   echo " -unixrootdir unixrootdir:	['/data'] Unix root directory"
   echo " -winrootdir winrootdir:	['Z:\'] Windows root directory"
   echo " -o outdir:			['/sdata1300'] output directory"
   echo " -asiccfg serverConfig:	Sidecar Control configuration filename (must be proper Windows filename)"
   echo " -m mcdfile: 			[none] Windows path to personality file to download after timing file"
   echo "  "
   exit
endif
 
set tmpdir=/tmp
set datefmt '+%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DATE: %y-%m-%d  TIME:%H:%M:%S %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%'
set i = 0;
while ( $#argv )
  @ i = $i + 1;
  switch( $argv[1] ) 
    case -i:
#      echo "itime found"
      shift
#      @ itime = $argv[1] # integ time of each coadd in microsec
#      echo $itime #$argv[1]
      modify -s sharcs itime=$argv[1]
      breaksw
    case -c:
      shift
      modify -s sharcs coadds=$argv[1] 
      breaksw
    case -r:
      shift
      modify -s sharcs numreads=$argv[1]
      breaksw
    case -g:
      shift
      modify -s sharcs groups=$argv[1]
      breaksw
    case -z:
      shift
      modify -s sharcs resets=$argv[1]
      breaksw
    case -d:
      shift
      modify -s sharcs ignoredreads=$argv[1]
      breaksw
    case -s:
      shift
      modify -s sharcs sampmode=$argv[1]
      breaksw
    case -x:
      shift
      @ startx=$argv[1]
      breaksw
    case -X:
      shift
      @ endx = $argv[1]
      breaksw
    case -y:
      shift
      @ starty = $argv[1]
      breaksw
    case -Y:
      shift
      @ endy = $argv[1]
      breaksw
    case -n:
      shift
      modify -s sharcs object="$argv[1]"
      breaksw
    case -asiccfg:
      shift
      modify -s sharcs asiccfg="$argv[1]"
      breaksw
    case -m:
      shift #mcdfile set
      modify -s sharcs timfile="$argv[1]"
      breaksw
    case -defdir:
      modify -s sharcs unixrootdir="/data" winrootdir="Z:\" outdir=sdata1300  #"
      breaksw
    case -unixrootdir:
      shift
      modify -s sharcs unixrootdir="$argv[1]"
      breaksw
    case -winrootdir:
      shift
      modify -s sharcs winrootdir="$argv[1]"
      breaksw
    case -o:
      shift
      modify -s sharcs outdir="$argv[1]"
      breaksw
    case -get[Vv]ref:
      SidecarGetAsicRegister 602c > $tmpdir/tmp_ref
      grep "   2: ASIC register" $tmpdir/tmp_ref
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_ref >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_ref
      breaksw
    case -get[gG]ain:
      SidecarGetAsicRegister 401a > $tmpdir/tmp_gain
      grep "   2: ASIC register" $tmpdir/tmp_gain 
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_gain >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_gain
      breaksw
    case -set[Vv]ref:
      shift
      SidecarSetAsicRegister 602c $argv[1] > $tmpdir/tmp_ref
      grep "   2: ASIC register" $tmpdir/tmp_ref      
      echo "  " >> $tmpdir/log_SetAsic      
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_ref >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_ref
      SidecarSetAsicRegister 6900 8002 > $tmpdir/tmp_ref
      grep "   2: ASIC register" $tmpdir/tmp_ref
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_ref >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_ref
      breaksw
    case -set[gG]ain:
      shift
#      if ( ($argv[1] < 10 && $argv[1] > -1) ) then
        SidecarSetAsicRegister 401a $argv[1] > $tmpdir/tmp_gain
        grep "   2: ASIC register" $tmpdir/tmp_gain   
        echo "  " >> $tmpdir/log_SetAsic
        date "$datefmt" >> $tmpdir/log_SetAsic
        cat $tmpdir/tmp_gain >> $tmpdir/log_SetAsic
        rm $tmpdir/tmp_gain
        SidecarSetAsicRegister 6900 8002 > $tmpdir/tmp_gain
        grep "   2: ASIC register" $tmpdir/tmp_gain
        echo "  " >> $tmpdir/log_SetAsic
        date "$datefmt" >> $tmpdir/log_SetAsic
        cat $tmpdir/tmp_gain >> $tmpdir/log_SetAsic
        rm $tmpdir/tmp_gain
#      else 
#        foreach l (a b c d e f) 
#          if ("$argv[1]" == $l) then
#            SidecarSetAsicRegister 401a $argv[1] > $tmpdir/tmp_gain
#            grep "   2: ASIC register" $tmpdir/tmp_gain
#            echo "  " >> $tmpdir/log_SetAsic
#            date "$datefmt" >> $tmpdir/log_SetAsic
#            cat $tmpdir/tmp_gain >> $tmpdir/log_SetAsic
#            rm $tmpdir/tmp_gain
#          else
#            echo "Gain must be set to levels between 0-9,a,b,c,d,e,f, try again"
#          endif
#        end
#      endif
      breaksw
    case -get[rR]eg:
      shift
      SidecarGetAsicRegister $argv[1]  > $tmpdir/tmp_reg
      grep "   2: ASIC register" $tmpdir/tmp_reg
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_reg >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_reg
      breaksw
    case -set[rR]eg:
      shift
      SidecarSetAsicRegister $argv[1] $argv[2] > $tmpdir/tmp_reg
      grep "   2: ASIC register" $tmpdir/tmp_reg
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_reg >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_reg
      SidecarSetAsicRegister 6900 8002 > $tmpdir/tmp_reg
      grep "   2: ASIC register" $tmpdir/tmp_reg
      echo "  " >> $tmpdir/log_SetAsic
      date "$datefmt" >> $tmpdir/log_SetAsic
      cat $tmpdir/tmp_reg >> $tmpdir/log_SetAsic
      rm $tmpdir/tmp_reg
      shift
      breaksw
    default:
      echo $argv[1] " not found as an argument" 
  endsw
  #echo $i "counts" $argv[1]
  shift
end


#set itime = 20000 # integ time of each coadd in microsec
#@ sampmode = 2 # 1:Single, 2:CDS, 3:MCDS, 4:UTR OR can type name!
#set asiconfig = "C:\sidecar\HxRG_Config\MOSFIRESidecarControlConfig.xml"

if ( $?startx || $?starty || $?endx || $?endy ) then
  if ( $?startx && $?starty && $?endx && $?endy ) then
    if ( ($startx < $endx) && ($starty < $endy)) then
      modify -s sharcs startx=$startx starty=$starty endx=$endx endy=$endy
    else
      echo "If specifying a window, minX must be < maxX and minY must be < maxY"
    endif
  else
    echo "If specifying a window, all 4 coordinates must be specified"
  endif
endif


## ENGINEERING KEYWORDS
# dirsep=/
#
#


