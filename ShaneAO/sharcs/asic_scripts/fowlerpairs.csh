#! /bin/csh -f

if ($#argv < 2) then
   echo "fowlerpairs.csh  number_to_take  upto "
   echo " "
   exit
endif


alias MATH 'set \!:1 = `echo "\!:3-$" | bc -l | xargs `'

#modify -s sharcs groups=2 sampmode=4 numreads=1

#modify -s sharcs ignoredreads=0 itime=1500

set num = $argv[1];
shift
set upto = $argv[1];
MATH tt = $num+1
set number=`printf "%d\n" $tt`;
MATH tt = $upto+1
set uptothismany=`printf "%d\n" $tt`;
echo " Taking " $num " fowler pairs data at each setting up until this many pairs" $upto 

set ngroups = 2;
set nreads = 1;
set sampmode = 4;
set readtt  = `gshow -s sharcs readtime     | cut -c17-20 ` ;
set ign = 0;
modify -s sharcs sampmode=$sampmode
printf "\n"

set c = 1;
set i = 1;
set j = 1;
while ($j < $uptothismany)
  set nreads = $j ;
  while ( $c < 2)
    modify -s sharcs writeall=1
    @ c = 3
  end
  #setup is up here so that it only happens once - more efficient!
  MATH time = ((( ( $ngroups - 1 ) * ( $nreads + $ign ) ) * $readtt + 0.2) * 1000.)
  MATH sleeptime = (( ( $ngroups - 1 ) * ( $nreads + $ign ) + $nreads ) * $readtt + 17.1 )  
  set atime=`printf "%d\n" $time`;
  modify -s sharcs ignoredreads=$ign 
  modify -s sharcs itime=$atime
  modify -s sharcs numreads=$nreads
  while ($i < $number)
    #echo $j 
    #echo $i
    echo " "
    echo "Taking Fowler pairs "$j", exposure number "$i
    #set time=`echo "scale=1; $readtt + 2.2" | bc `;
    modify -s sharcs go=1
    printf " Now sleeping %.2f seconds...\n" $sleeptime
    sleep $sleeptime
    @ i = $i + 1 
  end
  while ( $c < 4 )
    modify -s sharcs writeall=0
    @ c = 5
  end
  @ i = 1
  @ j = $j * 2
end

