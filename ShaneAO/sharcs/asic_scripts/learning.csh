#! /bin/csh -f

if ($#argv < 2) then
   echo "setup -i itime -c coadds -r reads -g groups -z resets -s sampling_mode" 
   echo "   -d drops -x minX -X maxX -y minY -Y maxY "
   echo "   -n ObjectName  -C serverConfig  -m mcdfile "
   echo "   -unixrootdir unixrootdir  -winrootdir winrootdir  -o outdir"
   echo "   -vref vrefhexvalue -gain gainlevel  -getreg register  -setreg register hexvalue"
   exit
endif
 
set i = 0;
while ( $#argv )
  @ i = $i + 1;
  switch( $argv[1] ) 
    case -i:
#      echo "itime found"
      shift
      @ itime = $argv[1] # integ time of each coadd in microsec
#      echo $itime #$argv[1]
      breaksw
    case -c:
      shift
      @ ncoadds = $argv[1] #
      breaksw
    case -r:
      shift
      @ nreads = $argv[1]
      breaksw
    case -g:
      shift
      @ ngroups = $argv[1]
      breaksw
    case -z:
      shift
      @ nresets = $argv[1]
      breaksw
    case -d:
      shift
      @ ndrops = $argv[1]
      breaksw
    case -s:
      shift
      @ sampmode = $argv[1]
      breaksw
    case -x:
      shift
      @ startx = $argv[1]
      breaksw
    case -X:
      shift
      @ endx = $argv[1]
      breaksw
    case -y:
      shift
      @ starty = $argv[1]
      breaksw
    case -Y:
      shift
      @ endy = $argv[1]
      breaksw
    case -n:
      shift
      set object = "$argv[1]"
      breaksw
    case -C:
      shift
      set asiccfg = "$argv[1]"
      breaksw
    case -m:
      shift
      set mcdfile = "$argv[1]"
      breaksw
    case -unixrootdir:
      shift
      set unixrootdir = "$argv[1]"
      breaksw
    case -winrootdir:
      shift
      set winrootdir = "$argv[1]"
      breaksw
    case -o:
      shift
      set outdir = "$argv[1]"
      breaksw
    case -vref:
      shift
      SidecarSetAsicRegister 602c $argv[1]
      breaksw
    case -gain:
      shift
      if ($argv[1] < 16 && $argv[1] > -1) then
        SidecarSetAsicRegister 401a $argv[1]
      else
        echo "Gain must be set to levels between 0-15, try again"
      endif
      breaksw
    case -getreg:
      shift
      SidecarGetAsicRegister $argv[1]
      breaksw
    case -setreg:
      shift
      SidecarSetAsicRegister $argv[1] $argv[2]
      breaksw
    default:
      echo $argv[1] " not found as an argument" 
  endsw
  #echo $i "counts" $argv[1]
  shift
end


#set object = 'Object Name'
#@ nreads = 4
#@ nresets = 1
#@ ngroups = 2
#@ ndrops = 0
#@ ncoadds = 1
#set itime = 20000 # integ time of each coadd in microsec
#@ sampmode = 2 # 1:Single, 2:CDS, 3:MCDS, 4:UTR OR can type name!
#set asiconfig = "C:\sidecar\HxRG_Config\MOSFIRESidecarControlConfig.xml"

#@ startx = 0
#@ starty = 0
#@ endx = 2047
#@ endy = 2047

if ( $?nreads) then
  modify -s ircalx numreads=$nreads 
endif
if ( $?nresets) then
  modify -s ircalx resets=$nresets 
endif
if ( $?ngroups) then
  modify -s ircalx groups=$ngroups 
endif
if ( $?ncoadds) then
  modify -s ircalx coadds=$ncoadds
endif
if ( $?ndrops) then
modify -s ircalx ignoredreads=$ndrops 
endif
if ( $?sampmode) then
  modify -s ircalx sampmode=$sampmode
endif
if ( $?object) then
  modify -s ircalx object="$object"
endif
if ( $?asiccfg) then
  modify -s ircalx asiccfg="$asiccfg"
endif
if ( $?mcdfile) then
  modify -s ircalx timfile="$mcdfile"
endif
if ( $?unixrootdir) then
  modify -s ircalx unixrootdir="$unixrootdir"
#  modify -s ircalx unixrootdir='/data' 
endif
if ( $?winrootdir) then
  modify -s ircalx winrootdir="$winrootdir"
#  modify -s ircalx winrootdir='Z:\'  #'
endif
if ( $?outdir) then
  modify -s ircalx outdir="$outdir"
#  modify -s ircalx outdir=/sdata1300  
endif
if ( $?itime) then
  modify -s ircalx itime=$itime
endif
if ( $?startx || $?starty || $?endx || $?endy ) then
  if ( $?startx && $?starty && $?endx && $?endy ) then
    if ( ($startx < $endx) && ($starty < $endy)) then
      modify -s ircalx startx=$startx starty=$starty endx=$endx endy=$endy
    else
      echo "If specifying a window, minX must be < maxX and minY must be < maxY"
    endif
  else
    echo "If specifying a window, all 4 coordinates must be specified"
  endif
endif


## ENGINEERING KEYWORDS
# dirsep=/
#
#


