#! @KPYTHON@
#
# kpython safely sets RELDIR, KROOT, LROOT, and PYTHONPATH before invoking
# the actual Python interpreter.

# Overall control daemon for the Shane AO adaptive optics realtime data
# analysis pipeline, from taking images on the wavefront sensor and tip/tilt
# cameras, performing centroiding and wavefront reconstruction, to commanding
# shapes to the deformable mirrors to compensate for the calculated aberrations.

#
# #
# Required libraries.
# #
#

import atexit		# enforce a clean shutdown
import ConfigParser	# parse a 'standard' Python configuration file
import DFW		# provided by kroot/util/dfw
import dpio2		# provided by lroot/ShaneAO/unreal/dpio
import edt		# provided by lroot/ShaneAO/unreal/edt
import ktl		# provided by kroot/ktl/keyword/python
import logging		# use common logging severity levels
import getopt		# parse command-line options
import os		# probe whether files exist
import select		# probe whether descriptors are ready for read/write
import signal		# enforce a clean shutdown
import socket		# direct connection to the woofer dispatcher
import sys		# access command-line arguments
import threading	# threads will be spawned for background processing
import time		# acquire timestamps


#
# #
# Main execution, invoked by a check at the tail end of this file.
# #
#

def main ():

	parseCommandLine ()	# Need to know where the config file is.
	parseConfigFile ()
	parseCommandLine ()	# Parse the command line again, so that it
				# overwrites options set by the config file.
				# This functionality is not currently used.

	checkSanity ()


	# Set handlers to shut down cleanly in all situations.

	atexit.register (shutdown)
	signal.signal (signal.SIGINT,  shutdown)
	signal.signal (signal.SIGTERM, shutdown)


	# Initialize hardware interfaces.

	main.tweeter = Tweeter ()
	main.tiptilt = TipTiltCamera ()
	### main.wfscam = WFSCamera ()


	# Start up our KTL backend.

	main.Service = DFW.Service (main.config.get ('main', 'service'),
				main.config.get ('main', 'stdiosvc'),
				setupKeywords)

	# Initialize the woofer interface, which makes direct references
	# to main.Service.

	main.woofer = Woofer ()


	# Everything is now running.

	main.Service['DISPSTA'].set ('ready')


	while main.shutdown.isSet () == False:
		try:
			main.shutdown.wait (300)

		except KeyboardInterrupt, SystemExit:
			break

	# End of execution.


# Many of the following parameters will be modified by command-line or
# configuration file options.

main.config = ConfigParser.ConfigParser ()
main.config_file = None
main.Service = None
main.shutdown = threading.Event ()
main.tiptilt = None
main.tweeter = None
main.version = '$Revision: 1.27 $'.split ()[1]
main.wfscam = None
main.woofer = None


def shutdown (*ignored):

	main.shutdown.set ()

	if main.Service != None:
		main.Service['DISPSTA'].set ('shutting down')
		main.Service.shutdown ()
		main.Service = None

	if main.tiptilt != None:
		main.tiptilt.shutdown ()

	if main.wfscam != None:
		main.wfscam.shutdown ()



#
# #
# All Keyword values, with the exception of those that are handled
# purely by the stdiosvc backend, are instantiated in the following
# function, which is itself called when our DFW.Service instance
# is initialized in main().
# #
#

def setupKeywords (service):

	DFW.Keyword.Enumerated ('DISPSTA', service, 'initializing')

	DFW.Keyword.String ('VERSION', service, main.version)
	StopKeyword ('DISPSTOP', service, 'no')

	TweeterBufferKeyword ('TWEETER_1',  service, 0,   512)
	TweeterBufferKeyword ('TWEETER_2',  service, 512, 512)
	TweeterMapKeyword ('TWEETER_MAP_1', service, 0,   512)
	TweeterMapKeyword ('TWEETER_MAP_2', service, 512, 512)

	TweeterPokeKeyword ('TWEETER_POKE', service)


	CameraStatusKeyword ('TIPTILT_STATUS', service)
	CameraPauseKeyword ('TIPTILT_PAUSE', service, main.tiptilt)

	CameraPerformanceKeyword ('TIPTILT_INTERVAL', service, main.tiptilt, 'interval')
	CameraPerformanceKeyword ('TIPTILT_PROCESSING', service, main.tiptilt, 'processing')

	CameraSerialKeyword ('TIPTILT_SERIAL', service, main.tiptilt)
	CameraSerialKeyword ('TIPTILT_SERIAL_COMMAND', service, main.tiptilt)
	CameraSerialKeyword ('TIPTILT_SERIAL_RESPONSE', service, main.tiptilt)


	###CameraStatusKeyword ('WFSCAM_STATUS', service)
	###CameraPauseKeyword ('WFSCAM_PAUSE', service, main.wfscam)

	###CameraPerformanceKeyword ('WFSCAM_PROCESSING', service, main.wfscam)

	###CameraSerialKeyword ('WFSCAM_SERIAL', service, main.wfscam)
	###CameraSerialKeyword ('WFSCAM_SERIAL_COMMAND', service, main.wfscam)
	###CameraSerialKeyword ('WFSCAM_SERIAL_RESPONSE', service, main.wfscam)


#
# #
# Classes that integrate functionality used by multiple keywords.
# #
#

class Camera:

	def __init__ (self, section, prefix):

		self.section = section

		config = main.config.get (section, 'config')
		device = main.config.getint (section, 'device')

		self.edt = edt.Camera (device, config)
		self.prefix = prefix

		self.messenger = CameraMessenger (self)


		# Always request 'safe' initial conditions.

		self.edt.stop ()

		try:
			program = main.config.get (section, 'safeprogram')

		except ConfigParser.NoOptionError:
			program = '@RCL 1'

		self.edt.serial (program)


	def shutdown (self):

		self.edt.stop ()
		self.messenger.stop ()


# end of class Camera



class TipTiltCamera (Camera):

	def __init__ (self, *args, **kwargs):

		kwargs['prefix'] = 'TIPTILT'
		kwargs['section'] = 'tiptilt'

		Camera.__init__ (self, *args, **kwargs)


# end of class TipTiltCamera



class WFSCamera (Camera):

	def __init__ (self, *args, **kwargs):

		kwargs['prefix'] = 'WFSCAM'
		kwargs['section'] = 'wfscam'

		Camera.__init__ (self, *args, **kwargs)


# end of class WFSCamera



class CameraMessenger (DFW.Thread):

	def __init__ (self, camera):

		self.camera = camera
		self.wake = threading.Event ()

		camera.edt.notify (self.wake.set)

		DFW.Thread.__init__ (self, "CameraMessenger/%s" % (camera.prefix.lower ()))


	def run (self):

		pending = []
		timeout = 1

		while self.stop_running == False:

			self.wake.wait (timeout=1)

			if self.wake.isSet ():
				self.wake.clear ()
			elif pending == None:
				continue


			# It's harmless to invoke edt.Camera.messages() when
			# there are no messages to retrieve.

			messages = self.camera.edt.messages ()

			if pending != None:
				if main.Service == None:
					# Hold onto these message events until
					# there is a service available to handle
					# broadcasts. This is necessary because
					# the Camera instances are created
					# before main.Service.

					pending.extend (messages)
					continue

				else:
					# main.Service is now ready.
					messages = pending + messages
					pending = None
					timeout = 300

			elif main.Service == None:
				# pending is None, and main.Service is also
				# None. This happens as part of the shutdown
				# sequence, and the Service is shut down first.
				# Because the Service instance is gone, there
				# is nothing for us to do with these messages,
				# or any further messages. Might as well stop
				# now, rather than wait for the explicit
				# notification via self.stop().

				break


			for message in messages:

				time, type, text = message

				if type == 'serial_command':
					self.handleSerialCommand (text, time)

				elif type == 'serial_response':
					self.handleSerialResponse (text, time)

				else:
					print "%.6f (%s): Unhandled message from camera, type '%s' with text '%s'" % (time, self.camera.prefix.lower (), type, text)


		# Clean up.

		self.camera.edt.notify (None)
		self.camera = None


	def stop (self):

		DFW.Thread.stop (self)
		self.wake.set ()


	def handleSerialCommand (self, value, timestamp):

		prefix = self.camera.prefix

		command = "%s_SERIAL_COMMAND" % (prefix)

		command = main.Service[command]

		### TODO: enable setting timestamp when available
		DFW.Keyword.String.set (command, value, force=True)


		value = value.upper ()

		if '@SEQ ' in value:
			pause = "%s_PAUSE" % (prefix)
			pause = main.Service[pause]

			pause.setSerial (value)


	def handleSerialResponse (self, value, timestamp):

		prefix = self.camera.prefix

		dual = "%s_SERIAL" % (prefix)
		response = "%s_SERIAL_RESPONSE" % (prefix)

		dual = main.Service[dual]
		response = main.Service[response]

		### TODO: enable setting timestamp when available
		DFW.Keyword.String.set (dual, value, force=True)
		DFW.Keyword.String.set (response, value, force=True)


# end of class CameraMessenger



class Tweeter:

	def __init__ (self):

		actuators = main.config.getint ('tweeter', 'actuators')
		device = main.config.getint ('tweeter', 'device')

		self.count = actuators
		self.output = dpio2.Output (device, self.count * 2)

		pulse_keyword = main.config.get ('tweeter', 'pulsekeyword')
		service, keyword = pulse_keyword.split ('.')

		frequency = main.config.getint ('tweeter', 'pulsefrequency')

		### This will need to be enabled at some point.
		###self.power = TweeterPowerPump (service, keyword, frequency)

		self.buffer = [0,] * self.count
		self.mapping = []

		# Initialize a unity translation map.

		count = 0

		while count < self.count:

			self.mapping.append (count)
			count += 1


	def prewriteBuffer (self, values, start):

		start = int (start)

		if start > self.count:
			raise ValueError, "cannot splice in a sequence off the end of the buffer"

		if isinstance (values, str):
			values = values.strip ()
			values = values.split ()

		if len (values) + start > self.count:
			raise ValueError, "buffer segment has too many fields (%d) to start at %d" % (len (values), start)

		position = 0
		count = len (values)

		different = False

		while position < count:

			value = int (values[position])
			values[position] = value

			if value != self.buffer[start + position]:
				different = True

			position += 1


		if different == False:

			raise DFW.Keyword.NoWriteNecessary, "buffer segment matches the active buffer"

		return values


	def prewriteMap (self, values, start):

		start = int (start)

		if start > self.count:
			raise ValueError, "cannot splice in a sequence off the end of the map"

		if isinstance (values, str):
			values = values.strip ()
			values = values.split ()

		if len (values) + start > self.count:
			raise ValueError, "translation sequence has too many fields (%d) to start at %d" % (len (values), start)


		position = 0
		count = len (values)

		different = False

		while position < count:

			value = int (values[position])
			values[position] = value

			if value != self.mapping[start + position]:
				different = True

			position += 1


		if different == False:

			raise DFW.Keyword.NoWriteNecessary, "map segment matches the active map"

		return values


	def writeBuffer (self, values, start):

		position = start

		for value in values:

			self.buffer[position] = value
			position += 1

		values = self.translate (self.buffer)
		self.output.write (values)


	def writeMap (self, values, start):
		''' Splice *values* into the translation map starting
		    at index *start*.
		'''

		position = start

		for value in values:
			self.mapping[position] = value
			position += 1


	def postwriteBuffer (self):

		for keyword in ('TWEETER_1', 'TWEETER_2', 'TWEETER_POKE'):

			keyword = main.Service[keyword]
			keyword.set (keyword.read ())


	def postwriteMap (self):

		# Reload self.buffer according to the new map.

		self.reload ()

		for keyword in ('TWEETER_1', 'TWEETER_2', 'TWEETER_POKE', 'TWEETER_MAP_1', 'TWEETER_MAP_2'):

			keyword = main.Service[keyword]
			keyword.set (keyword.read ())


	def reload (self):
		''' Query the DPIO device for its current buffer, and
		    cache it locally. :func:`reload` would be especially
		    relevant if the DPIO device were manipulated without
		    handling writes via a :class:`Tweeter` instance, or
		    if the translation map were updated.
		'''

		raw_buffer  = self.output.current ()
		self.buffer = self.translate (raw_buffer, invert=True)


	def translate (self, values, invert=False):

		size = len (values)

		index = 0
		translated = [None] * self.count

		while index < size:

			mapped_index = self.mapping[index]

			if invert == False:
				translated[mapped_index] = values[index]
			else:
				translated[index] = values[mapped_index]

			index += 1

		return translated


	def write (self, values=None):
		''' Wrapper function to simplify the calling sequence when
		    invoked internally.
		'''

		if values != None:
			if len (values) != len (self.buffer):
				raise ValueError, "values must be a sequence of length %d" % (len (self.buffer))

			values = self.prewriteBuffer (values, start=0)


		self.writeBuffer (values, start=0)


		if values != None:
			self.postwriteBuffer ()


# end of class Tweeter



class Woofer:

	def __init__ (self):

		self.retries = 3
		self.socket = None

		self.status = main.Service['WOOFER_STATUS']

		service = main.config.get ('woofer', 'service')
		hostname = main.config.get ('woofer', 'hostkeyword')
		port = main.config.get ('woofer', 'portkeyword')

		self.service = ktl.Service (service)
		self.hostname = self.service[hostname]
		self.port = self.service[port]

		self.hostname.monitor ()
		self.port.monitor ()

		self.connect ()


	def connect (self):

		if self.socket != None:
			raise IOError, 'already connected'


		self.status.set ('connecting')
		error = None

		try:
			hostname = self.hostname['ascii']
			port = self.port['binary']

		except ktl.ktlError:
			self.status.set ('not connected')
			main.Service.log (logging.ERROR, "Woofer.connect(): unable to read woofer host/port keywords:\n%s" % (traceback.format_exc ()))
			raise


		# Use a tight timeout of 5 milliseconds when talking to the
		# woofer dispatcher.

		self.socket = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
		self.socket.settimeout (0.005)

		try:
			self.socket.connect ((hostname, port))

		except socket.timeout:
			error = "connection to %s:%d timed out" % (hostname, port)

		except socket.error:
			error = "connection to %s:%d failed" % (hostname, port)

		except:
			error = "connection to %s:%d failed" % (hostname, port)


		if error != None:
			self.socket = None
			self.status.set ('not connected')
			main.Service.log (logging.ERROR, "Woofer.connect(): %s" % (error))
			raise IOError, error

		main.Service.log (logging.INFO, "Woofer.connect(): connected to %s:%d" % (hostname, port))


                # Test the socket to make sure everything's a-OK. It should
                # be ready to write, and otherwise quiescent. The select()
                # statement has a timeout of one second, but if all is well,
                # select() will return immediately.

                check = (self.socket,)

                readers,writers,errors = select.select (check, check, check, 1)

                if self.socket in readers:
                        error = 'new connection unexpectedly has output waiting'

		if self.socket in errors:
                        error = 'new connection indicating an error'

                if self.socket in writers:
                        # Good, it should be ready to receive commands.
                        pass
                else:
                        error = 'new connection not ready to receive commands'

		if error != None:
			self.socket = None
			self.status.set ('not connected')
			main.Service.log (logging.ERROR, "Woofer.connect(): %s" % (error))
			raise IOError, error

		self.status.set ('connected')


	def disconnect (self):

		if self.socket == None:
			return

		self.socket.close ()
		self.socket = None

		self.status.set ('not connected')


	def reconnect (self):

		if self.socket != None:
			self.disconnect ()

		self.connect ()


	def send (self, message):
		''' Send the given *message* out on self.socket; return
		    the number of characters sent. By construction, this
		    is always the length of *message*.
		'''

		if self.socket == None:
			self.connect ()

		sockets = (self.socket,)

		readers, writers, errors = select.select ((), sockets, (), 1)

		if self.socket in writers:
			pass
		else:
			raise IOError, "self.socket not ready to receive data"

		length = len (message)
		sent = 0

		while sent < length:
			newly_sent = self.socket.send (message[sent:])
			if newly_sent == 0:
				raise IOError, "connection to controller lost"

			sent += newly_sent

		return sent


	def receive (self):
		''' Read a message on self.socket, one character at a time.
		    This could be made more efficient by reading at least a
		    line at a time; the responses from the woofer dispatcher
		    are delimited not only by pointy brackets, but also with
		    an intermediate newline for precisely this reason.
		    Return the result.
		'''

		characters = []

		while True:
			character = self.socket.recv (1)

			if character == '':
				# Bad socket.
				raise IOError, "can't read data from self.socket"

			characters.append (character)

			if character == '}':
				# End of message delimiter.
				break


		message = ''.join (characters)
		message = message.strip ()

		return message


	def command (self, command):
		''' Send the *command* string over the direct connection to
		    the woofer dispatcher; return the command response.
		'''

		if command[0] != '{' or command[-1] != '}' or \
		   (command[1] != 'm' and command[1]  != 't'):
			raise ValueError, "malformed command: %s" % (command)

		# Always append a newline character, to allow the woofer
		# dispatcher to read contents a line at a time.

		command = "%s\n" % (command)


		attempts = 0
		response = None

		while attempts < self.retries:

			attempts += 1

			try:
				self.send (command)

			except (IOError, socket.error):
				main.Service.log (logging.ERROR, 'Woofer.command() failed to send, reconnecting')

				try:
					self.reconnect ()
				except IOError:
					main.Service.log (logging.ERROR, 'Woofer.command() unable to reconnect, aborting')
					return None

				continue


			try:
				response = self.receive ()

			except (socket.timeout, socket.error, IOError):
				main.Service.log (logging.ERROR, 'Woofer.command() failed to receive response, reconnecting')

				try:
					self.reconnect ()
				except IOError:
					main.Service.log (logging.ERROR, 'Woofer.command() unable to reconnect, aborting')
					return None

			if response == '':
				main.Service.log (logging.ERROR, 'Woofer.command() received empty response, retrying')
				response = None
			else:
				# Got a response.
				break

		# If we don't have a response by now, there was a communication
		# error that persisted through self.retries attempts.

		if response == None:
			raise IOError, "unable to communicate with woofer host"

		return response


# end of class Woofer

#
# #
# Keyword classes.
# #
#

class StopKeyword (DFW.Keyword.Boolean):

	def write (self, value):

		if value == '1':
			main.shutdown.set ()


# end of class StopKeyword



class CameraPauseKeyword (DFW.Keyword.Boolean):

	def __init__ (self, name, service, camera):

		self.camera = camera

		DFW.Keyword.Boolean.__init__ (self, name, service, initial='0')


	def setSerial (self, value):
		''' :func:`setSerial` assumes that the value is all upper-case.
		'''

		if 'SEQ 0' in value:
			self.set ('True')

		elif 'SEQ 1' in value:
			self.set ('False')

		else:
			main.Service.log (logging.ERROR, "CameraPauseKeyword (%s): unhandled serial input: '%s'" % (self.name, value))


	def set (self, value, force=False):

		DFW.Keyword.Boolean.set (self, value, force)

		if main.Service == None:
			return

		status = "%s_STATUS" % (self.camera.prefix)
		status = main.Service[status]

		# self.value is normalized to '0' or '1' for the
		# DFW.Keyword.Boolean class. To get back False/True,
		# convert it first to an int, and then to a bool.

		status.setPaused (bool (int (self.value)))


	def write (self, value):

		if value == '0':
			self.camera.edt.start ()
		else:
			self.camera.edt.stop ()


	def postwrite (self, *args, **kwargs):
		''' Do nothing. :func:`setSerial` will be invoked if
		    the write succeeds.
		'''

		return


# end of class CameraPauseKeyword



class CameraPerformanceKeyword (DFW.Keyword.Double):

	def __init__ (self, name, service, camera, attribute):

		self.attribute = attribute
		self.edt = camera.edt

		if hasattr (self.edt, attribute):
			pass
		else:
			raise ValueError, "edt.Camera instance does not have an attribute '%s'" % (attribute)

		DFW.Keyword.Double.__init__ (self, name, service, period=1)


	def read (self):

		value = getattr (self.edt, self.attribute)

		# The value is an integer number of tenths of microseconds.
		# Losslessly convert to fractional microseconds.

		microseconds = value / 10
		tenths = value - microseconds

		value = "%d.%d" % (microseconds, tenths)

		return value


	def update (self):

		self.set (self.read ())


# end of class CameraPerformanceKeyword



class CameraSerialKeyword (DFW.Keyword.String):

	def __init__ (self, name, service, camera):

		self.camera = camera

		DFW.Keyword.String.__init__ (self, name, service, initial='')


	def prewrite (self, value):
		''' Allow repeated requests of the same value; do not raise
		    DFW.Keyword.NoWriteNecessary.
		'''

		return value


	def write (self, value):
		''' Write requests will only come from the write-enabled
		    %_SERIAL and %_SERIAL_COMMAND keywords. Broadcasts of
		    both commands and responses are acquired via
		    self.camera.edt.messages(), which has the nice side-effect
		    of only broadcasting serial commands that actually succeed.
		'''

		self.camera.edt.serial (value)


	def postwrite (self, *args, **kwargs):
		''' Do nothing. The serial interface is wholly driven by
		    messages passed up from the edt Python/C layer.
		'''

		return


# end of class CameraSerialKeyword



class CameraStatusKeyword (DFW.Keyword.Enumerated):

	def __init__ (self, *args, **kwargs):

		self.paused = True
		self.processing = False

		DFW.Keyword.Enumerated.__init__ (self, *args, **kwargs)


	def setPaused (self, value):

		self.paused = value
		self.update ()


	def setProcessing (self, value):

		self.processing = value
		self.update ()


	def update (self):

		if self.paused == True:
			value = 'paused'

		elif self.processing == True:
			value = 'processing'

		else:
			value = 'exposing'

		self.set (value)


# end of class CameraStatusKeyword



class TweeterBufferKeyword (DFW.Keyword.IntegerArray):

	def __init__ (self, name, service, start, count):

		if main.tweeter == None:
			raise RuntimeError, "main.tweeter hasn't been instantiated yet"

		if start > main.tweeter.count:
			raise IndexError, "cannot map a sequence off the end of the buffer"

		if count + start > main.tweeter.count:
			raise IndexError, "too many fields (%d) requested to start at position %d" % (count, start)


		self.count = count
		self.start = start

		# The DAC output is 16 bits.

		self.maximum = 2 ** 16 - 1

		DFW.Keyword.IntegerArray.__init__ (self, name, service, None)

		# Use self.read() to set an initial value.

		self.set (self.read ())


	def check (self, value):

		DFW.Keyword.IntegerArray.check (self, value)

		elements = value.split ()

		for element in elements:

			element = int (element)

			if element < 0:
				raise ValueError, "element value %d is less than zero" % (element)

			if element > self.maximum:
				raise ValueError, "element value %d is greater than %d" % (element, self.maximum)


	def read (self):

		begin = self.start
		end   = self.start + self.count

		# Use map() to cast all the integers to strings,
		# as join() requires a sequence of strings.

		return ' '.join (map (str, main.tweeter.buffer[begin:end]))


	def prewrite (self, value):

		DFW.Keyword.IntegerArray.prewrite (self, value)

		return main.tweeter.prewriteBuffer (value, start=self.start)


	def write (self, value):

		main.tweeter.writeBuffer (value, self.start)


	def postwrite (self, value):

		main.tweeter.postwriteBuffer ()


# end of class TweeterBufferKeyword



class TweeterMapKeyword (TweeterBufferKeyword):

	def __init__ (self, *args, **kwargs):

		TweeterBufferKeyword.__init__ (self, *args, **kwargs)

		self.maximum = main.tweeter.count - 1


	def read (self):

		begin = self.start
		end   = self.start + self.count

		# Use map() to cast all the integers to strings,
		# as join() requires a sequence of strings.

		return ' '.join (map (str, main.tweeter.mapping[begin:end]))


	def prewrite (self, value):

		DFW.Keyword.IntegerArray.prewrite (self, value)

		return main.tweeter.prewriteMap (value, start=self.start)


	def write (self, value):

		main.tweeter.writeMap (value, self.start)


	def postwrite (self, value):

		main.tweeter.postwriteMap ()


# end of class TweeterMapKeyword



class TweeterPokeKeyword (DFW.Keyword.Integer):

	not_poked = '-1'

	def __init__ (self, *args, **kwargs):

		kwargs['initial'] = TweeterPokeKeyword.not_poked

		DFW.Keyword.Integer.__init__ (self, *args, **kwargs)

		# The DAC output from the DPIO2 board is 16 bits,
		# so, 0 -> 65535. The DAC output is intepreted to
		# 0 -> 300 V on the Cambridge Innovations box, but
		# it clips to 200 V. Thus, the maximum commandable
		# DAC value is 43,690.

		# If this value ever needs to be changed, it should
		# instead be set via a separate keyword.

		self.amplitude = 43000


	def read (self):

		poked = None

		position = 0

		while position < main.tweeter.count:

			value = main.tweeter.buffer[position]

			if value == self.amplitude:
				if poked != None:
					# At least two actuators are 'poked'.
					# Therefore, it is not a true 'poke'.
					return TweeterPokeKeyword.not_poked

				poked = position

			elif value == 0:
				pass

			else:
				# Some other value besides zero or 'poked'.
				# Therefore, there is no active 'poke'.
				return TweeterPokeKeyword.not_poked

			position += 1


		if poked == None:
			return TweeterPokeKeyword.not_poked

		return str (poked)


	def prewrite (self, value):

		DFW.Keyword.Integer.prewrite (self, value)

		value = int (value)

		if value < 0:
			raise ValueError, "actuator position must be > 0"

		if value >= main.tweeter.count:
			raise ValueError, "actuator position must be < %d" % (main.tweeter.count)

		return str (value)


	def write (self, value):

		value = int (value)

		values = [0,] * main.tweeter.count
		values[value] = self.amplitude

		main.tweeter.write (values)


# end of class TweeterPokeKeyword


#
# #
# Other background threads.
# #
#

class TweeterPowerPump (DFW.Thread):
	''' The Cambridge Innovations driver electronics for the Boston
	    Micromachines MEMS device have a power cutoff circuit that
	    will drop power if a heartbeat pulse is not received within
	    a given amount of time. The generation of those pulses is
	    handled by a Galil controller; the program code generating
	    the pulses is enabled as long as a specific KTL keyword is
	    receiving keyword writes.

	    This thread is responsible for issuing keyword writes (any
	    value is OK) to that specific keyword.
	'''

	def __init__ (self, service, keyword, frequency):

		self.service = ktl.Service (service)
		self.keyword = self.service[keyword]

		# Number of seconds to wait between keyword write requests.

		self.interval = 1.0 / frequency

		DFW.Thread.__init__ (self, 'TweeterPowerPump')


	def run (self):

		count = 0

		while True:

			if main.shutdown.isSet ():
				break

			self.keyword.write (count, wait=False)
			count += 1

			if count == 1000000:
				count = 0

			main.shutdown.wait (self.interval)


# end of class TweeterPowerPump


#
# #
# Helper functions.
# #
#

def usage (verbose=False):
	''' How to invoke this program.
	'''

	output = "Usage: %s -c config_file [options]" % (sys.argv[0])

	if verbose == False:
		print output
		return

	output = output + '''

Required flags:

	-c filename		Overall configuration file location
	--config=filename		Default: None

Optional flags:

	-h,-?			Print verbose help (this output)
	--help
'''

	print output



def parseCommandLine ():

	executable = sys.argv[0]
	arguments  = sys.argv[1:]

	if len (arguments) == 0:
		help = True
	else:
		help = False

	flags = '?c:h'
	long_options = ('config=', 'help')

	options, arguments = getopt.gnu_getopt (arguments, flags, long_options)


	for option, value in options:

		if option == '-?' or option == '-h' or option == '--help':
			help = True

		elif option == '-c' or option == '--config':
			main.config_file = validateFile (value)


	if help == True:
		usage (verbose=True)
		sys.exit (0)



def parseConfigFile ():

	if main.config_file == None:
		return

	main.config.read (main.config_file)



def checkSanity ():
	''' Raise exceptions if something is wrong with the runtime
	    configuration, as specified by the configuration file and
	    on the command line.
	'''

	if main.config_file == None:
		sys.stderr.write ("Warning, no configuration file specified.\n")

	sections = ('main', 'tiptilt', 'tweeter', 'wfscam', 'woofer')

	for section in sections:

		if main.config.has_section (section):
			pass
		else:
			raise ConfigParser.NoSectionError, "[%s]" % (section)


	main.config.get ('main', 'stdiosvc')

	main.config.getint ('tiptilt', 'device')

	main.config.getint ('tweeter', 'device')
	main.config.getint ('wfscam', 'device')

	# main.config.get ('woofer', 'hostname')
	# main.config.getint ('woofer', 'port')



def validateFile (filename):

	if os.path.isabs (filename):
		pass
	else:
		filename = os.path.abspath (filename)

	if os.path.isfile (filename):
		pass
	else:
		raise ValueError, "file does not exist: '%s'" % (filename)

	return filename



#
# #
# Execute the main() function defined at the head of the file.
# #
#

if __name__ == '__main__':

	main ()

# EOF
