"""woofer_client
defines a socket client interface for the real-time controller to send commands to the woofer.
The woofer object will initialize the socket interface
to musher.ucolick.org and then allow 52-element actuator command
vectors to be sent to the woofer server. The server on musher can be set up to run either
the ALPAO woofer DM itself, or a simulator LED box connected
to the ALPAO controller.
"""

import socket
import os
import sys
import time
import numpy as np
import readfits as fits
import threading
from threading import Thread

class woofer_client:
    """The woofer_client class handles the client (RTC) side of the
    connection to the woofer server on another machine (currently called
    musher) and also coordinates tilt-offload commands to the telescope.
    """
    def __init__(self,aw,config='real'):
        """The argument aw is the pointer into the :ref:`rtc2 <rtc2>` state vector, offset to
        the start of the woofer's state.
        Keyword argument config can be either "real" or "simulated" with
        "real" causing an actual socket connection and "simulated" creating
        a virtual socket to the :ref:`sauce simulator <sauce>`.
        """
        valid_configs = ['real','simulated']
        if not (config in valid_configs):
            raise ValueError('config must be one of '+str(valid_configs))
        self.config = config
        self.host = 'musher.ucolick.org'
        self.port = 5000
        self.rate = 200 # rate for sending commands to the woofer, Hz
        self.socket = None
        self.connected = False

        self.offload_host = 'shred.ucolick.org'
        self.offload_port = 5700
        self.offload_socket = None
        self.offloading = False
        #self.A = np.matrix(np.identity(2)) # offloading direction-cosine matrix
        self.A = np.matrix([[0,1],[1,0]])
        self.woofer_tilt_basis = fits.readfits('data/woofer_tilt_basis.fits')
        self.last_offload_time = time.time()
        self.offload_delta_t = 10. # offloads at this time interval, seconds
        self.offload_tipTilt_threshold = 2. # offloads if the built up tip/tilt is greater that this, arcsec
        self.offload_filter_timeconstant = 100. # moving average horizon, in woofer time cycles
        self.offload_gain = 0.5 # fraction of the woofer to send to the telescope offload
        self.tip_ma = 0.
        self.tilt_ma = 0.
        self.debug = False
        self.lasterr = 'no errors'

        self.woofer_map = fits.readfits(
            os.path.join(
                os.environ['HOME'],'parameterFiles','woofer_map.fits'
            )
        ).astype(int)
        self.seq = 1
        self.n = 52
        self.aw = aw # the woofer commands, 52 elements in the range -1 to +1
        self.thread = woofer_thread(aw=self.aw, config=self.config)

    def start_thread(self):
        """begin the background thread that sends commands to the woofer on a
        regular clock cycle (default is 200 Hz)
        """
        self.thread = woofer_thread(aw=self.aw, config=self.config)
        self.thread.wc = self
        mu = 1./self.rate
        self.thread.set_sample(mu)
        self.thread.start()

    def stop_thread(self):
        """kill the background thread that is sending commands to the woofer server
        """
        self.thread.stop()

    def set_rate(self,hz):
        """change the rate at which the thread sends commands to the woofer server.
        Argument is in updates per second (Hz). It is recommended that it be
        no higher than 200 Hz and no lower than 1 Hz. The communication overhead has
        been determined experimentally to be around 1 ms and the jitter around 0.5 ms.
        """
        self.rate = hz

    def set_callback(self,callback_function):
        """set the callback routine for the simulator
        This should only be called if you configured the client in simulate mode
        """
        if self.config != 'simulated':
            raise Exception('<woofer_client.set_callback> can only be used in simulation mode')
        self.socket.update_woofer = callback_function

    def hello(self):
        """Send a hello message to the server, which checks if it is running.
        If it's running, the server responds with technobabble.
        If it's not, it will print a timeout error
        """
        self.seq += 1
        self.seq = self.seq % 10000
        dtNow = str(time.time())
        data = '{t,'+str(self.seq)+',' + dtNow + '}'

        response = self.communicate(data)
        print '<woofer_client.woofer_client.hello> response = '+response

    def send(self,command):
        """Send a command vector to the woofer. The argument command must be
        a 52-element list, with values between -1 and +1
        """
        if len(command) != 52:
            raise ValueError, '<woofer_client> can only send a 52 element vector'

        self.seq += 1
        self.seq = self.seq % 10000
        now = str(time.time())

        command_mapped = np.zeros(52)
        command_mapped[self.woofer_map] = command
        command_string = map(str,command_mapped)
        command_string = ','.join(command_string)

        data = '{m,'  +str(self.seq)+','+ now + ',' + command_string + '}\n'
        self.communicate(data)

    def communicate(self,data):
        if self.socket == None:
            try:
                self.connect()
            except:
                self.disconnect()
                return

        try:
            self.socket.sendall(data)
            received = self.socket.recv(1024)
        except:
            self.disconnect()
            received = ''
        else:
            if received == None:
                received = ''

        return received

    def reconnect(self):
        self.disconnect()
        self.connect()

    def disconnect(self):
        if self.socket == None:
            return

        self.socket.close()
        self.socket = None
        self.connected = False

    def connect(self):
        if self.socket != None:
            return

        if self.config == 'simulated':
            self.socket = virtual_woofer_server()
            return

        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.connect((self.host, self.port))
        self.socket.settimeout(1)
        try:
            received = self.socket.recv(1024)
        except socket.timeout:
            pass

        self.connected = True
        print '<woofer_client.woofer_client.connect> connection succeeeded'

    def zero(self):
        """Send a zero command vector to the woofer.
        """
        return self.send([0.0] * 52)

    def connect_offload(self):
        print '<woofer_client.woofer_client.connect_offload> initiating connection to offload host'
        if self.config == 'real':
            try:
                offload_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                offload_socket.connect((self.offload_host, self.offload_port))
                offload_socket.settimeout(5)
                offload_socket.sendall('INSTRUMENT AO\n')
            except:
                raise socket.error, '<woofer_client.connect_offload> trouble connecting to offload host'
            else:
                self.offload_socket = offload_socket

        elif self.config == 'simulated':
            print '<woofer_client.connect_offload> (simulated): '+'INSTRUMENT AO'
            self.offload_socket = None

    def set_offload(self,start = True):
        """Tell the offloader to start or stop watching for tilt on the woofer and generating
        the appropriate commands to the offload server if they exceed threshold.
        """
        if self.offload_socket != None:
            try:
                self.offload_socket.close()
                print '<woofer_client.set_offload> offload socket closed'
            except:
                print '<woofer_client.set_offload> problem closing previous connection to offload host'
            self.offload_socket = None
        if start == True:
            self.connect_offload()
            print '<woofer_client.set_offload> offloading started'
        self.offloading = start

    def set_offload_direction_cosine(self,A):
        """2 x 2 transformation matrix from tip/tilt on the woofer to RA and DEC of the telescope.
        Since the Shane is on an equatorial mount, this only needs to be updated if the
        tub angle is changed
        """
        if (type(A) is np.matrix) and (A.shape == (2,2)):
            self.A = A
        else:
            raise ValueError,'<set_offload_direction_cosine> argument must be a 2x2 matrix'

    def offload(self,tip,tilt):
        """convert tip and tilt to RA and DEC and tell the telescope to move that far
        """
        if self.offloading == True:
            tt_vec = np.matrix([tip,tilt]).T
            ra_dec = self.A * tt_vec
            ra, dec = ra_dec[0,0],ra_dec[1,0]
            commandX = 'X '+str(ra)+'\n'
            commandY = 'Y '+str(dec)+'\n'
            if self.config == 'real':
                self.offload_socket.sendall(commandX)
                self.offload_socket.sendall(commandY)
            else:
                print '<woofer_client.offload> sent: '+commandX
                print '<woofer_client.offload> sent: '+commandY

class woofer_thread(Thread):
    """This is the background thread that sends commands periodically to the woofer.
    Argument aw is a pointer into the live :ref:`rtc2 <rtc2>` state vector at the
    woofer state.
    The woofer thread is implemented nominally
    for 200 Hz operation, so mu=0.005. If you want to change this, use the
    set_sample method
    """
    def __init__(self,aw=None,config='real'):
        super(woofer_thread,self).__init__()
        valid_configs = ['real','simulated']
        if not (config in valid_configs):
            raise ValueError('config must be one of '+str(valid_configs))
        self.config = config
        overhead = 0.001
        self.mu = 0.005 - overhead
        self.aw = aw
        self.alive = False

    def run(self):
        """defines woofer_thread activity.
        Do not call this method directly. Instead, use the start() method to
        initiate the background thread.
        """
        self.alive = True
        t0 = time.time()
        tlast = t0
        home = os.environ['HOME']
        debug = self.wc.debug
        if debug == True:
            f = file(os.path.join(home,'local','tmp','log'),'a')
            f.write('woofer thread started\n'); f.flush()
        print '<woofer_client.woofer_thread.run> woofer thread started'
        while self.alive:
            # ===============================================================
            #         Woofer Control Loop
            #
            self.wc.send(self.aw)

            # check and report if the transmits aren't on schedule:
            t = time.time()
            dt = t-tlast
            tl = time.localtime(t)
            day = time.strftime('%Y-%m-%d',tl)
            tim = time.strftime('%H:%M:%S',tl)
            if dt > 50*self.mu:
                self.wc.lasterr = '<woofer_client.woofer_thread.run> '+day+' '+tim+' woofer thread TIMING ERROR, dt '+str(dt)+' greater than expected '+str(self.mu)
            if debug == True:
                f.write(day+' '+tim+' '+str(t-t0)+' seconds, dt = '+str(t-tlast)+'\n'); f.flush()
            tlast = t
            # =============================================================
            #         Telescope Offload
            #
            # use a moving average filter on the tips and tilts
            tip = self.wc.tip_ma
            tilt = self.wc.tilt_ma
            alpha = 1./self.wc.offload_filter_timeconstant
            gain = self.wc.offload_gain
            tip  = (1-alpha)*tip  + alpha*np.dot(self.wc.aw,self.wc.woofer_tilt_basis[:,0])
            tilt = (1-alpha)*tilt + alpha*np.dot(self.wc.aw,self.wc.woofer_tilt_basis[:,1])
            self.wc.tip_ma = tip
            self.wc.tilt_ma = tilt
            #  check the time since the last offload and, if beyond a threshold:
            if (t - self.wc.last_offload_time) > self.wc.offload_delta_t:
            #    check the tip and tilt on the woofer and, if beyond a limit:
                tiptiltr = np.sqrt(tip**2 + tilt**2)
                if tiptiltr > self.wc.offload_tipTilt_threshold:
                    self.wc.offload(gain*tip,gain*tilt) # where tip and tilt are in arcsec
                    self.wc.last_offload_time = t
            if self.config == 'real':
                time.sleep(self.mu)
            else:
                #print '<woofer_client.woofer_thread.run> sleeping'
                time.sleep(self.mu) # after one minute of operation on the mac, it never wakes up!
                #print '<woofer_client.woofer_thread.run> awake'
        print '<woofer_client.woofer_thread.run> confirmed - woofer thread stopped'
        sys.stdout.flush()
        if debug == True:
            f.close()
        return

    def set_sample(self,mu):
        """set the sample interval of the thread, in seconds.
        """
        overhead = 0.001
        self.mu = mu - overhead

    def stop(self):
        """stops the woofer communication background thread
        """
        print '<woofer_client.woofer_thread.stop> stopping woofer thread'
        self.alive = False
        print '<woofer_client.woofer_thread.stop> waiting for thread to terminate'
        sys.stdout.flush()

k = 0
buf = []
def upl(s,chan_num,v=5,config='real'):
    """Send voltages to the RIO D/A box
    The default is 5 volts which is midrange on the fast steering mirror
    """
    global k, buf
    chan = str(chan_num)
    volts = str(v)
    eos = '\r\n'
    pd = 'AO'+chan+','+volts+eos
    if config == 'real':
        try:
            s.send(pd)
            u = s.recv(500)
        except:
            pass
    else:
        if k == 0:
            buf = []
        buf.append(pd)
        k = (k+1)%10

class virtual_woofer_server:
    """functions like the real woofer server, but uses the :ref:`sauce simulator <sauce>` instead.
    It has methods equivalent to a socket object, so it behaves compatably in
    simulation mode.
    """
    def __init__(self):
        self.message = '<virtual_woofer_server> initialized'
        self.update_woofer = None  # callback into sauce
        self.connected = False
        self.verbose = False
        self.woofer_map = fits.readfits(
            os.path.join(
                os.environ['HOME'],'parameterFiles','woofer_map.fits'
            )
        ).astype(int)

    def connect(self,params):
        """make a virtual connection"""
        self.connected = True

    def settimeout(self,dt):
        """set a virtual timeout"""
        pass

    def recv(self,n):
        """receive a message"""
        m = self.message
        self.message = None
        return m

    def sendall(self,data):
        """send a message"""
        u = data.rsplit(',')
        msg_tag = u[0].strip('{')
        if msg_tag == 't':
            self.message = '<virtual_woofer_server> got hello message'
        elif msg_tag == 'm':
            self.message = '<virtual_woofer_server> got data message'
            aw = np.zeros((52))
            u[54] = u[54].strip('}\n')
            for i in range(52):
                aw[i] = float(u[i+3])
            aw_remap = aw[self.woofer_map.astype(int)]
            self.update_woofer(aw_remap)
        else:
            self.message = '<virtual_woofer_server> got unknown message'
        if self.verbose == True:
            print self.message

    def close(self):
        """close the virtual connection"""
        print '<woofer_client.virtual_woofer_server.close> close'
        self.connected = False

#"""Top level test code
#import rtc
#import sauce
#self = rtc.rtc('16x')
#wc = rtc.woofer_connect(config='simulated')
#wc.thread.set_sample(2.0)
#wc.set_callback(sauce.update_woofer)
#"""

# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
