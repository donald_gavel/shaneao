""" Sauce is the Secret Sauce hardware simulator
    for testing the ShaneAO real-time control software.
    Sauce can also assist in AO system calibration and
    in preparing the rtc parameter files.
    
    Sauce simulates everthing beyond the low-level hardware drivers
    including the hardware itself and the physical optics. Sauce:
    
        1 Pre-generates representative phase screen sets for use as input aberrations

        2 Propagates the wavefront from the phase aberration, adding the DM's
        phase correction, and generates a representative Hartmann CCD image.
    
    The Sauce package can generate certain prerequisite matrices
    ( Aw, At, Mw, Mt, and Ctw ) that are used
    in support of AO system calibration and building rtc parameter files.
    To use sauce to generate precursor matrices,
    invoke sauce.init(). The calculations
    take a few minutes. Save the generated matrices to disk
    with sauce.save().
"""
module_file_name = __name__+'.py'

import os
import sys

home = os.environ['HOME']
os.chdir(home)

import datetime
from screen import Screen
import numpy as np
import readfits as fits
import pyfits
import copy
import zernike
from itertools import repeat as rep
import woofer as woofer_class
import scipy
import scipy.interpolate
from scipy import ndimage
import time
import thread

param_dir = home + '/parameterFiles/'
data_dir = home + '/data/'

#============== physical constants ==============
arcsec = np.pi/(180.*3600.)
microns = 1.e-6

#============= simulation parameters =============
# see spreadsheet sauce_worksheet.xls
# and 3m_WFS_v1.xlsx in KT document archive

tweeter_stroke = 3.5
lam = 0.589
dn_range = 65536.
radian_per_dn = (2*np.pi*tweeter_stroke/lam)/dn_range
dn_per_radian = 1. / radian_per_dn    
wfs_modes = ['8x','16x','30x']

D_tele = 3.0
D_sec = 0.8
mode = 1
imap = fits.readfits(param_dir+'i_map.fits')
tweeter_map = fits.readfits(param_dir+'tweeter_map.fits')
a_tw = D_tele/(31.-3.)      # tweeter actuator spacing
a_w = D_tele/6              # woofer actuator spacing
oversample = 16             # fine pixels per tweeter actuator spacing
du_fine = a_tw/oversample   # fine pixel size
n_fine = 512                # total fine pixels across wavefront
m_fine = 2*n_fine           # 2x wider for blowing screen

if (mode == 1):
    n_across = 14           # subaps across pupil
    d_wfs = D_tele/n_across # subap diameter
    nsa = oversample*2      # fine pixels per subaperture
    oversample_p = 6 
    lam = 0.54*microns
    theta_pixel = 1.56*arcsec
    umap = fits.readfits(param_dir+'u_map_subaps_16x.fits')
elif (mode == 0):
    n_across = 7            # subaps across pupil
    d_wfs = D_tele/n_across # subap diameter
    nsa = oversample*4      # fine pixels per subaperture
    oversample_p = 5
    lam = 0.54*microns
    theta_pixel = 1.3*arcsec
    umap = fits.readfits(param_dir+'u_map_subaps_8x.fits')
elif (mode == 2):
    n_across = 30           # subaps across pupil
    d_wfs = D_tele/n_across # subap diameter
    nsa = oversample*1      # fine pixels per subaperture
    oversample_p = 9
    lam = 0.5425*microns
    theta_pixel = 2.35*arcsec
    umap = fits.readfits(param_dir+'u_map_subaps_30x.fits')
    
# aperture
L = n_fine*du_fine/2.
x1 = np.linspace(-L,L,n_fine)
x,y = np.meshgrid(x1,x1)
r = np.sqrt(x**2 + y**2)
ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)

# find the locations of subaps
sloc = []

if (mode == 1 or mode == 2):
    delta = 0
    x1 = (np.arange(0,n_across)-(n_across/2)+0.5)*d_wfs
    brightness = (2**15)/3e6
elif (mode == 0):
    delta = d_wfs*0.2
    x1 = (np.arange(0,n_across+1)-(n_across/2)-0.5)*d_wfs
    brightness = (2**15)/3e7

for x in x1:
    for y in x1:
        r = np.sqrt(x**2 + y**2)
        if (r < (D_tele/2+delta)) and (r > D_sec/2):
            sloc.append((x,y))

isloc = np.array(sloc)/du_fine + n_fine/2
ns = len(sloc)

def ctr(v):
    """Roll the 2-d array so that the (0,0) point
    is at the center
    """
    n = v.shape
    r = np.roll(np.roll(v,n[0]/2,axis=0),n[1]/2,axis=1)
    return r

def zeropad(u,n):
    """
    r = zeropad(u,n)
    
    Zero-pad the array by centering it in a larger
    array and padding with zeros
    
    u - original array
    n - size of new array (2-tuple)
    """
    m = u.shape
    r = np.zeros(n).astype(u.dtype)
    r[n[1]/2-m[1]/2:n[1]/2+m[1]/2,n[0]/2-m[0]/2:n[0]/2+m[0]/2] = u
    return r

def zeropad_a(u,n):
    """
    This is a special zero-pad that assumes the data
    is initially centered with 0,0 in the middle of
    the array but in the end is centered with 0,0 at
    the corner of the array. Tricky, so use this only
    internally.
    """
    m = u.shape
    r = np.zeros(n).astype(u.dtype)
    r[0:m[1],0:m[0]] = u
    r = np.roll(np.roll(r,-m[1]/2,axis=0),-m[0]/2,axis=1)
    return r

def crop(u,n):
    """
    Crop an image to a given size (centered on the middle)
    """
    n0 = u.shape
    dx = (n0[0]-n[0])/2
    dy = (n0[1]-n[1])/2
    r = u[dx:-dx,dy:-dy]
    return r

# create a set of imaged Hartmann dots
pixel = np.ones((oversample_p,oversample_p))
pixel = zeropad_a(pixel,(nsa*2,nsa*2))
fpixel = np.fft.fft2(pixel)
readoff = np.mod((np.array(range(5))-2)*oversample_p,nsa*2)

x1 = np.arange(-d_wfs/2,d_wfs/2,du_fine)
tx, ty = np.meshgrid(x1,x1)
if (mode == 0):
    tx = tx*(2*np.pi/lam)*theta_pixel*(-0.35)
    ty = ty*(2*np.pi/lam)*theta_pixel*(-0.35)
elif (mode == 1):
    tx = tx*(2*np.pi/lam)*theta_pixel*(-0.59) # tilt adjustment to put dot on crosshair
    ty = ty*(2*np.pi/lam)*theta_pixel*(-0.59)
elif (mode == 2):
    tx = tx*(2*np.pi/lam)*theta_pixel*0
    ty = ty*(2*np.pi/lam)*theta_pixel*0

wfs_lock = thread.allocate_lock()
def set_lenslet(lenslet_mode):
    global mode, n_across, d_wfs, nsa, oversample_p, lam, theta_pixel, umap
    global sloc, isloc, ns, readoff, fpixel, tx, ty, brightness, freeze_wfs
    
    wfs_lock.acquire()
    if (lenslet_mode not in wfs_modes):
        raise ValueError,'mode '+str(lenslet_mode)+' not supported'
    mode = wfs_modes.index(lenslet_mode)
    if (mode == 1):
        n_across = 14           # subaps across pupil
        d_wfs = D_tele/n_across # subap diameter
        nsa = oversample*2      # fine pixels per subaperture
        oversample_p = 6 
        lam = 0.54*microns
        theta_pixel = 1.56*arcsec
        umap = fits.readfits(param_dir+'u_map_subaps_16x.fits')
        brightness = (2**15)/3e6
    elif (mode == 0):
        n_across = 7            # subaps across pupil
        d_wfs = D_tele/n_across # subap diameter
        nsa = oversample*4      # fine pixels per subaperture
        oversample_p = 5
        lam = 0.54*microns
        theta_pixel = 1.3*arcsec
        umap = fits.readfits(param_dir+'u_map_subaps_8x.fits')
        brightness = (2**15)/3e7
    elif (mode == 2):
        n_across = 30           # subaps across pupil
        d_wfs = D_tele/n_across # subap diameter
        nsa = oversample*1      # fine pixels per subaperture
        oversample_p = 9
        lam = 0.5425*microns
        theta_pixel = 2.35*arcsec
        umap = fits.readfits(param_dir+'u_map_subaps_30x.fits')
        brightness = (2**15)/3e5
    sloc = []
    if (mode == 1 or mode == 2):
        delta = 0
        x1 = (np.arange(0,n_across)-(n_across/2)+0.5)*d_wfs
    elif (mode == 0):
        delta = d_wfs*0.2
        x1 = (np.arange(0,n_across+1)-(n_across/2)-0.5)*d_wfs
    for x in x1:
        for y in x1:
            r = np.sqrt(x**2 + y**2)
            if (r < (D_tele/2+delta)) and (r > D_sec/2):
                sloc.append((x,y))
    isloc = np.array(sloc)/du_fine + n_fine/2
    ns = len(sloc)    
    # create a set of imaged Hartmann dots
    pixel = np.ones((oversample_p,oversample_p))
    pixel = zeropad_a(pixel,(nsa*2,nsa*2))
    fpixel = np.fft.fft2(pixel)
    readoff = np.mod((np.array(range(5))-2)*oversample_p,nsa*2)
    
    x1 = np.arange(-d_wfs/2,d_wfs/2,du_fine)
    tx, ty = np.meshgrid(x1,x1)
    if (mode == 0):
        tx = tx*(2*np.pi/lam)*theta_pixel*(-0.35)
        ty = ty*(2*np.pi/lam)*theta_pixel*(-0.35)
    elif (mode == 1):
        tx = tx*(2*np.pi/lam)*theta_pixel*(-0.59) # tilt adjustment to put dot on crosshair
        ty = ty*(2*np.pi/lam)*theta_pixel*(-0.59)
    elif (mode == 2):
        tx = tx*(2*np.pi/lam)*theta_pixel*0
        ty = ty*(2*np.pi/lam)*theta_pixel*0
    wfs_lock.release()
    
#================ aberation set generators =============
def gen_atmos_t_set(seed=None):
    """Generate simulated Kolmogorov phase screen set.
    """
    from sauce import n_fine, m_fine, du_fine
    screen = Screen((n_fine,m_fine),r0=0.12,du=du_fine,seed=seed).setup()
    tmax = 500; # must be less than m_fine-n_fine
    sset = []
    for t in range(tmax):
        s = screen.screen[:,t:t+n_fine]
        sset.append(s)
    return sset

def gen_ref_t_set():
    """Generate a time-sequence of static flat wavefronts,
    for taking reference centroids
    """
    screen = np.zeros((n_fine,n_fine))
    tmax = 100
    aber_set = []
    for t in range(tmax):
        aber_set.append(screen)
    return aber_set

def zernset(n):
    """Generate a set of Zernike screens up to 5th order
    
    n = size of screen (n,n)
    """
    r,th = zernike.polar_array(Rmax=n/2.-.55,DS=1,pr=n/2)
    nmset = [(1,1),(1,-1),
        (2,0),(2,2),(2,-2),
        (3,1),(3,-1),(3,3),(3,-3),
        (4,0),(4,2),(4,-2),(4,4),(4,-4),
        (5,1),(5,-1),(5,3),(5,-3),(5,5),(5,-5)]
    
    zset = []
    for n,m in nmset:
        a = zernike.zernike(n,m,r,th,limit=False)
        zset.append(a)
    return zset

def tiltxy(tx,ty):
    """Generate one fine grid of tilt phase
    """
    n = n_fine
    r,th = zernike.polar_array(Rmax=n/2.-.55,DS=1,pr=n/2)
    phx = tx*zernike.zernike(1,1,r,th,limit=False)
    phy = ty*zernike.zernike(1,-1,r,th,limit=False)
    return phx+phy

def gen_f_set(aber):
    """Generate a time sequence of aberations based on
    a fixed single aberation
    """
    t0 = 0
    dt = 1
    tf = 100
    atset = []
    for t in range(t0,tf+1,dt):
        atset.append(aber)
    return atset

def gen_t_set(aber,period=30.,amplitude=2,length=100):
    """Generate a time sequence of aberations based on
    sinusoidal variation of the input aberation
    
    aber = the base aberation
    """
    t0 = 0
    dt = 1
    tf = length
    per = float(period)
    ztset = []
    for t in range(t0,tf+1,dt):
        amp = amplitude*np.cos(2*np.pi*(t/per))
        s = amp*aber
        ztset.append(s)
    return ztset

def pattern(kind,pos=(0,0)):
    """
    pattern(kind,pos=(0,0)) creates a pattern of phase on the fine grid.
    
    kind = "cross" or "poke"
    pos = position of the poke
    
    Note that the optional pos argument is (x,y) counter to python row = y because
    sauce will use a transposed umap
    """
    from sauce import du_fine, n_fine
    
    x1 = arange(0,n_fine,1)
    x,y = meshgrid(x1,x1)
    sig = 20.
    g = exp(-((x-n_fine/2)**2+(y-n_fine/2)**2)/sig**2)
    g = np.roll(np.roll(g,(n_fine/2),axis=0),n_fine/2,axis=1)
    fg = np.fft.fft2(g)
    p = np.array(pos)/du_fine + n_fine/2
    
    u = np.zeros((n_fine,n_fine))
    if (kind == "cross"):
        u[:,n_fine/2] = 1.
        u[n_fine/2,:] = 1.
        fu = np.fft.fft2(u)
        fug = fu*fg
        ug = real(np.fft.ifft2(fug))
        ug = (ug/ug.max())*35.
        return ug
    if (kind == "poke"):
        u[p[1],p[0]] = 1.
        fu = np.fft.fft2(u)
        fug = fu*fg
        ug = real(np.fft.ifft2(fug))
        ug = (ug/ug.max())*35.
        return ug
    return u

t = 0
dt = 1
print '<sauce> creating zernike set'
zset = zernset(32) #zernset(n_fine)
print '<sauce> creating aberation time sets'

aber_set_ref = gen_ref_t_set()
aber_set_atmos = gen_atmos_t_set()
aber_set = aber_set_ref

def aber(kind=None,scale=1.,zern=3,period=25,length=100):
    """Insert a simulated aberation.
    No argument gives a list of kinds to choose from.
    """
    global aber_set
    allowed = ['ref','sinz','atmos','fixz','fixa']
    if (kind == None):
        print 'choose one of '+str(allowed)
        return
    if (not (kind in allowed)):
        print 'invalid choice. choose one of '+str(allowed)
        return
    if (kind == 'ref'):
        aber_set_ref = gen_ref_t_set()
        a = aber_set_ref
    if (kind == 'sinz'):
        aber_set_z = gen_t_set(tweeter(zset[zern]*dn_per_radian),period=period,length=length,amplitude=4.)
        a = aber_set_z
    if (kind == 'atmos'):
        a = aber_set_atmos
    if (kind == 'fixa'):
        a = aber_set_atmos
        n = len(a)
        k = int(np.random.uniform(low=0,high=n-1))
        for i in range(len(aber_set)):
            aber_set[i] = a[k]*scale
        return
    if (kind == 'fixz'):
        if isinstance(zern,list):
            if (len(scale) != len(zern)):
                raise ValueError,'<sauce.aber> lengths of zern and scale must match'
            aber = tweeter(0.*zset[0])
            for s,z in zip(scale,zern):
                aber += s*tweeter(2*zset[z]*dn_per_radian)
            aber_set_f = gen_f_set(aber)
            aber_set = copy.deepcopy(aber_set_f)
            return
        else:
            aber_set_f = gen_f_set(tweeter(2*zset[zern]*dn_per_radian))
        a = aber_set_f
    aber_set = copy.deepcopy(a)
    for i in range(len(aber_set)):
        aber_set[i] = aber_set[i]*scale

#========== calibration light source =============
light_on = True
brightness_knob = 1.0
sky_brightness = 15000.
on_sky = False
tt_brightness = 2.**13 # half way to full range
tt_sky_brightness = 1000.
tt_fraction = 1.

def light(q=None,knob=100.):
    """Turn on or off the simulated light source.
    Argument: 'on' or 'off'
    Optionally, set the brightness 'knob' from 0 to 100 percent
    """
    global brightness_knob
    global light_on
    if (q == None):
        if (light_on):
            print 'light is on at '+str(brightness_knob*100.) + ' percent'
        else:
            print 'light is off'
        return
    if q == 'on':
        light_on = True
        brightness_knob = knob/100.
    elif q == 'off':
        light_on = False
    else:
        print '<sauce.light> argument must be "on" or "off" '

def sky(q=None,brightness=15000.):
    """Tell the WFS it has sky background
    Argument is 'on' or 'off'
    sky() returns the state
    """
    global sky_brightness, on_sky
    if (q == None):
        if (on_sky):
            print 'on sky with a brightness of '+str(sky_brightness)+' per WFS pixel'
        else:
            print 'not on sky'
        return
    if (q == 'on'):
        on_sky = True
        sky_brightness = brightness
    elif (q == 'off'):
        on_sky = False
    else:
        print '<sauce.sky> argument must be "on" or "off" '

def tt_light(frac = None):
    """Set the tt light, in percent of the source brightness seen by the WFS
    This is a fraction of the guide star light only, not the sky
    (The sky brightness = sauce.tt_sky_brightness)
    And it doesn't take light away from the WFS
   
    tt_light() returns the current tt light fraction
    """
    global tt_fraction
    if frac == None:
        print 'tt light fraction is '+str(tt_fraction*100)+' percent'
        return
    tt_fraction = frac/100.
    
#========== wavefront sensor and ccd =============
freeze_wfs = False
def wfs(s):
    """
    Create the Hartmann sensor CCD image given the phase of the wavefront
    
    s = the phase on Sauce's pre-defined fine grid (n_fine x n_fine at
    du_fine sampling), in units of radians at 589 nm wavelength
    Returns the 160x160 CCD image
    
    You will have to normalize the CCD image to any brightness you like
    but it will have to be in the range of 'uint16' if it is sent to the rtc
    """
    wfs_lock.acquire()
    dotsb = []
    for iloc in isloc:
        ph = -s[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
        mag = ap[iloc[0]-nsa/2:iloc[0]+nsa/2,iloc[1]-nsa/2:iloc[1]+nsa/2]
        # indexing departs from python y,x convention because umap is transposed
        # minus sign makes positive phase bump out Hartmann dots
        wf = mag*np.exp(1j*(ph+tx+ty))
        wf = zeropad_a(wf,(nsa*2,nsa*2))
        u = abs(np.fft.fft2(wf))**2
        fu = np.fft.fft2(u)
        fubin = fu*np.conj(fpixel)
        ubin = np.real(np.fft.ifft2(fubin))
        v = np.zeros((5,5))
        for k in range(5):
            for j in range(5):
                v[k,j] = ubin[readoff[k],readoff[j]]
        dotsb.append(v)
    
    # now put the dot images on the wfs ccd
    ccd = np.zeros((160*160))
    dotsa = np.array(dotsb).reshape((ns*5*5))
    ccd[umap] = dotsa
    ccd[imap] = ccd.copy()
    ccd = ccd.reshape((160,160))
    wfs_lock.release()
    return ccd

cam_backgnd = fits.readfits(os.path.join(home,'parameterFiles','wfs_dark.fits')) # a represenative background image
cam_backgnd_s = np.zeros((160*160))
cam_backgnd_s[imap] = cam_backgnd.reshape((160*160)) # scramble the background
cam_backgnd = cam_backgnd_s.reshape((160,160))
ne = 32. # noise counts per pixel - this is 8x the photon noise because of a 3-bit shift in the frame-grabber A/D

#=============== tip tilt sensor and ccd ================
tt_pixel_size = 0.3*arcsec
tt_wavelength = 0.7*microns
fine_pixel_size = (tt_wavelength/(2*n_fine*du_fine))
tt_oversample = 15 # this is approximately 0.3 arcsec / fine_pixel_size
tt_pixel = np.ones((tt_oversample,tt_oversample))
tt_pixel = zeropad_a(tt_pixel,(2*n_fine,2*n_fine))
f_tt_pixel = np.fft.fft2(tt_pixel)
n_plucks = 2*n_fine/tt_oversample
x_pluck = (np.arange(n_plucks)-n_plucks/2)*tt_oversample + n_fine
tt_pluck_xy = np.meshgrid(x_pluck,x_pluck)
tt_pluck = (tt_pluck_xy[0] + 2*n_fine*tt_pluck_xy[1]).reshape((n_plucks*n_plucks)) # these are the indices from which to pluck binned values after pixel convolution
x1 = np.arange(-n_fine/2,n_fine/2)*du_fine
tt_tx, tt_ty = np.meshgrid(x1,x1)
#tt_tilt_adjust = np.array([0.425,0.425]) # centers on the central crosshair of the ccd array
#tt_tilt_adjust=np.array([0.615,0.215]) # for -24 degrees
tt_tilt_adjust = np.array([1.17,-1.115]) # for +24.3 degrees
tt_tx = tt_tx*(2*np.pi/tt_wavelength)*tt_pixel_size
tt_ty = tt_ty*(2*np.pi/tt_wavelength)*tt_pixel_size
tt_field_rotation = 24.3 # in degrees
# proof:
# aber('fixz',12.8,zern=0)
# aber('fixz',0,zern=0)
# aber('fixz',-12.8,zern=1)
# matches data taken in lab with cal stage moves on 3/27/14 TT_Sensor_Calibration.pptx

tts_lock = thread.allocate_lock()
def tts(ph):
    """
    Create the Tip/Tilt sensor CCD image give the phase of the wavefront

    ph = the phase on Sauce's pre-defined fine grid (n_fine x n_fine at
    du_fine sampling), in units of radians at 589 nm wavelength
    Returns the 80x80 ttCCD image
    
    You will have to normalize the CCD image to any brightness you like
    but it will have to be in the range of 14 bits if it is sent to the rtc
    """
    global fp
    tts_lock.acquire()
    norm_factor = 1./141.e9 # this makes the peak of the PSF approximately 1
    
    tx = tt_tx*tt_tilt_adjust[0]
    ty = tt_ty*tt_tilt_adjust[1]
    wf = ap*np.exp(1j*(ph+tx+ty))
    wf = zeropad(wf,(2*n_fine,2*n_fine))
    fp = np.fft.fftshift(abs(np.fft.fft2(wf))**2)
    fp = np.flipud(fp)
    fp = np.abs(ndimage.rotate(fp,tt_field_rotation,reshape=False))
    fp = np.fft.ifftshift(fp)
    
    f_fp_pixelConvolved = np.fft.fft2(fp)*np.conj(f_tt_pixel)
    fp_pixelConvolved = np.real(np.fft.ifft2(f_fp_pixelConvolved))
    fp_pixelConvolved = np.fft.fftshift(fp_pixelConvolved)
    fp_binned = fp_pixelConvolved.reshape((2*n_fine*2*n_fine))[tt_pluck].reshape((n_plucks,n_plucks))
        
    ccd = zeropad(fp_binned,(80,80))*norm_factor
    
    tts_lock.release()
    return ccd

tt_cam_backgnd = fits.readfits(os.path.join(home,'parameterFiles','tt_dark.fits')) # a represenative background image
ne_tt = 6. # noise counts per pixel

#=============== tweeter ====================
def tweeterPokes(sigma=1.4):
    """generate a generic poke for the tweeter
    and a set of tweeter locations on the sauce fine grid
    """
    poke = np.zeros((n_fine,n_fine))
    n = 32
    x = (np.array(range(-(n/2),(n/2)))*a_tw + a_tw/2.)/du_fine
    x = np.round(x).astype('int')
    x,y = np.meshgrid(x,x)
    locs_xy = zip(x.reshape((n**2)),y.reshape((n**2)))
    x = np.array(range(-n_fine/2,n_fine/2))*du_fine
    x,y = np.meshgrid(x,x)
    r2 = x**2 + y**2
    s2 = (sigma*a_tw)**2
    poke = np.sinc(x/a_tw)*np.sinc(y/a_tw)*np.exp(-r2/s2)
    return poke,locs_xy

tweeterModeSet = []
tweeterPoke, tweeterActLocs = tweeterPokes()
p = tweeterPoke
p = np.roll(p,n_fine/2,axis=0)
p = np.roll(p,n_fine/2,axis=1)
fPoke = np.fft.fft2(p)

def tweeter(a_vec,old=False):
    """
    Generate the wavefront phase on Sauce's pre-defined fine grid
    given the set of actuator commands, a
    
        a = actuator commands in a 32 x 32 numpy array, given in the natural units
            of the DM driver, ranging from 0 to 65535
        returns the wavefront on a n_fine x n_fine grid at du_fine sampling, in
            units of radians at 589 nm wavelength
    """
    if (old):
        return tweeter_oldVersion(a_vec)
    a_vec = np.array(a_vec)
    dim = a_vec.shape
    if (len(dim) != 1):
        a_vec = a_vec.reshape((np.product(dim)))
    q = np.zeros((n_fine,n_fine))
    for a_val,loc in zip(a_vec,tweeterActLocs):
        j,k = np.array(loc)+n_fine/2
        q[k,j] = a_val*radian_per_dn
    fq = np.fft.fft2(q)
    r = np.real(np.fft.ifft2(fq*fPoke))
    return(r)

def argsort(seq):
    # http://stackoverflow.com/questions/3071415/efficient-method-to-calculate-the-rank-vector-of-a-list-in-python
    return sorted(range(len(seq)), key = seq.__getitem__)

def A_tweeter(mode=1,save=False):
    """
    Create a set of tweeter modes as expressed in
    tweeter command vectors
    """
    if isinstance(mode,str):
        imode = wfs_modes.index(mode)
    else:
        imode = mode
    nm = [8,16,32][imode]
    
    na = 32
    dk = 2*np.pi/float(na)
    
    # Quadrant I
    kx0 = np.array(range(1,nm/2))
    ky0 = np.array(range(1,nm/2))
    kx,ky = np.meshgrid(kx0,ky0)
    kx = kx.reshape(((nm/2-1)*(nm/2-1)))
    ky = ky.reshape(((nm/2-1)*(nm/2-1)))
    
    modeset = []
    modeset.extend(zip(kx0,ky0*0,rep(0)))
    modeset.extend(zip(kx0*0,ky0,rep(1)))
    modeset.extend(zip(kx0,ky0*0,rep(2)))
    modeset.extend(zip(kx0*0,ky0,rep(3)))
    modeset.extend(zip(kx,ky,rep(0)))
    modeset.extend(zip(kx,ky,rep(2)))
    modeset.extend(zip(kx,ky,rep(3)))
    modeset.extend(zip(kx,ky,rep(1)))
    
    metric = []
    for m in modeset:
        metric.append(m[0]**2+m[1]**2+0.1*m[2])
    
    isort = argsort(metric)
    smodeset = []
    for k in range(len(modeset)):
        smodeset.append(modeset[isort[k]])
    
    x = np.array(range(-na/2,na/2))
    x,y = np.meshgrid(x,x)
    x = x.reshape((na*na))
    y = y.reshape((na*na))
    
    kx0 = kx0*dk
    ky0 = ky0*dk
    kx = kx*dk
    ky = ky*dk
    
    argp = np.outer(kx,x)+np.outer(ky,y)
    argm = np.outer(kx,x)-np.outer(ky,y)
    argx = np.outer(kx0,x)
    argy = np.outer(ky0,y)
    A = np.vstack((2*np.cos(argx),2*np.cos(argy),2*np.sin(argx),2*np.sin(argy),
                   np.cos(argp)+np.cos(argm),np.cos(argp)-np.cos(argm),
                   np.sin(argp)+np.sin(argm),np.sin(argp)-np.sin(argm)))
    As = A[isort,:].reshape(len(modeset),na,na)
    
    if (save):
        filename = os.path.join(home,'parameterFiles','reconMatrix_'+wfs_modes[imode],'At.fits')
        hdu = pyfits.PrimaryHDU(As)
        hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
        hdu.header.append(('ITYPE','RTCParam'))
        hdu.header.append(('DEVICE','Tweeter'))
        hdu.header.append(('FUNC','Mode Matrix'))
        hdu.header.append(('SYMBOL','At'))
        hdu.header.append(('MODE',wfs_modes[imode]))
        hdu.header.append(('SEC',True,' Secondary obscuration'))
        hdu.header.append(('NM',len(modeset),' # of modes'))
        hdu.header.append(('NA',na,' # of actuators'))
        hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'))
        hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
        hdu.header.append(('GENER',module_file_name,'Generator code'))
        fits.protectFile(filename)
        hdu.writeto(filename)
    
    return As

def tweeter_oldVersion(a):
    """
    Generate the wavefront phase on Sauce's pre-defined fine grid
    given the set of actuator commands, a
    
        a = actuator commands in a 32 x 32 numpy array, given in the natural units
            of the DM driver, ranging from 0 to 65535
        returns the wavefront on a n_fine x n_fine grid at du_fine sampling, in
            units of radians at 589 nm wavelength
    """
    from sauce import n_fine, nsa
    
    s = n_fine/nsa
    fa = np.fft.fft2(a*radian_per_dn)
    fa = np.fft.fftshift(fa)
    fwf = zeropad(fa,(n_fine,n_fine))
    fwf = np.fft.ifftshift(fwf)
    wf = np.fft.ifft2(fwf)
    wf = np.real(wf)*s**2
    return wf

def tweeterModes(verbose=0):
    """produce the tweeter mode set, given a set of
    actuator vectors defining the modes. The mode
    set is defined on the sauce fine grid
    """
    tweeterModeSet = []
    k = 0
    for a in At:
        tweeterModeSet.append(tweeter(a))
        if (verbose):
            print k,
            sys.stdout.flush()
        k += 1
    if (verbose): print 'done'
    return tweeterModeSet

def M_tweeter(tweeterModeSet,band=None,verbose=0):
    """create the tweeter modes self-integral matrix
    """
    nm = len(tweeterModeSet)
    Mt = np.zeros((nm,nm))
    for j in range(nm):
        if (verbose):
            print j,
            sys.stdout.flush()
        if (band != None):
            kmax = min((nm,j+band))
        else:
            kmax = nm
        for k in range(j,kmax):
            Mt[j,k] = np.sum(tweeterModeSet[j]*tweeterModeSet[k]*ap)
            Mt[k,j] = Mt[j,k]
    if (verbose): print 'done'
    return Mt

a = np.zeros((32,32))
global wf_tweeter
wf_tweeter = tweeter(a)

# some more aberration sets based on tweeter
aber_set_z = gen_t_set(tweeter(zset[3]*dn_per_radian),period=25,amplitude=4.)
aber_set_f = gen_f_set(tweeter(2*zset[3]*dn_per_radian))
aber_set_fz = []
for a,b in zip(aber_set_z,aber_set_f):
    aber_set_fz.append(a+b)

#================= woofer ===================
woof = woofer_class.woofer()
def wooferPokes(verbose=0):
    """resample the woofer pokes on sauce's fine grid (du_fine)
    given the woofer object's sampling (woof.dx)
    """
    n,m = woof.ap.shape
    c = woof.center
    r = (woof.beamSize/woof.dx) / (D_tele/du_fine) * n_fine / 2.
    x = np.array(range(n)-c[0])
    y = np.array(range(n)-c[1])
    nx = scipy.linspace(-r,r,n_fine) 
    ny = nx
    wooferPokeSet = []
    k=0
    for u in woof.rSet:
        if (verbose):
            print k,
            sys.stdout.flush()
        k += 1
        spl = scipy.interpolate.RectBivariateSpline(x,y,u)
        ph = spl(nx,ny)
        wooferPokeSet.append(ph)
    u = woof.ap
    spl = scipy.interpolate.RectBivariateSpline(x,y,u)
    ap = spl(nx,ny)
    if (verbose): print 'done'
    return ap,wooferPokeSet

global wooferPokeSet, apw
wooferPokeSet = None

if (os.path.isfile(data_dir+'wooferPokes.fits')):
    print '<sauce> reading the woofer poke set from disk file'
    wooferPokeSet = fits.readfits(data_dir+'wooferPokes.fits',verbose=1)

wooferModeSet = []
def wooferModes(nmodes=52,verbose=0):
    """create the woofer modes on the fine grid, given the set of
    woofer mode vectors already in the woof object
    """
    global wooferModeSet
    
    wooferModeSet = []
    k = 0
    for a in woof.modeCoefSet[0:nmodes]:
        wooferModeSet.append(woofer(a))
        if (verbose):
            print k,
            sys.stdout.flush()
        k += 1
    if (verbose): print 'done'
    return wooferModeSet

def woofer(a):
    """Generate the wavefront phase on Sauce's pre-defined fine grid
    given the set of woofer actuator commands, a
    
    a must be a 52-element list
    """
    global wooferPokeSet, apw
    if (wooferPokeSet == None):
        print '<sauce.woofer> interpolating the woofer poke set from the lab-measured data...'
        apw,wooferPokeSet = wooferPokes(verbose=1)        
    crSet = zip(a,wooferPokeSet) # influence function set
    ph = 0*crSet[0][1]
    rad_per_poke = (2*np.pi/0.589)*2*(1./0.2) # pokes are in microns surface, for 0.2 amp command
    for c,r in crSet:
        ph += c*r*rad_per_poke
    return ph

def A_woofer(nmodes=52):
    """return the woofer A matrix, which is the
    matrix form of the woofer mode coefficient set
    """
    Aw = np.array(woof.modeCoefSet[0:nmodes])
    return Aw

def M_woofer(wooferModeSet,nmodes=52,verbose=0):
    """create the M_w matrix, the cross-integrals of all the woofer modes
    with each other
    """    
    naw = len(wooferModeSet)
    nf = nmodes
    M = np.zeros((nf,nf))
    for k in range(nf):
        for j in range(k+1):
            M[k,j] = np.sum(wooferModeSet[j]*wooferModeSet[k]*ap)
            M[j,k] = M[k,j]
        if (verbose):
            print k,
            sys.stdout.flush()
    if (verbose): print 'done'
    return M

aw = np.zeros((52))
global wf_woofer
wf_woofer = woofer(aw)

def C_wooferTweeter(wooferModeSet,tweeterModeSet,verbose=0):
    """create the cross-integral matrix between woofer
    and tweeter mode sets
    """
    nw = len(wooferModeSet)
    nt = len(tweeterModeSet)
    Cwt = np.zeros((nw,nt))
    for k in range(nw):
        if (verbose):
            print k,
            sys.stdout.flush()
        for j in range(nt):
            Cwt[k,j] = np.sum(wooferModeSet[k]*tweeterModeSet[j]*ap)
    if (verbose): print 'done'
    return Cwt

def corCoef(M):
    """calculate the correlation coefficients for
    a symmetric positive definite matrix
    """
    n,m = M.shape
    c = np.zeros((n,n))
    for k in range(n):
        for j in range(k+1):
            c[k,j] = M[k,j]/(np.sqrt(M[k,k]*M[j,j]))
            c[j,k] = c[k,j]
    return c

#============= callback routines for rtc2 =============
heartbeat = 0
ph_atmos = np.zeros((n_fine,n_fine))
ph_e = np.zeros((n_fine,n_fine))
laser_tilt = [0.,0.]

def cb():
    """provides the callback for edt_simulator's pdv_wait_image call
    in the case of the wavefront sensor ccd
    """
    global ph_atmos, ph_e
    global ccd, t,dt
    global heartbeat
    
    t = t+dt
    if (t >= len(aber_set)):
        t = 0
    ph_atmos = aber_set[t]
    ph_e = ph_atmos - wf_tweeter - wf_woofer + tiltxy(laser_tilt[0],laser_tilt[1])
    ccd = wfs(ph_e)
    noise = np.random.normal(0.,ne,(160,160))
    r = cam_backgnd + noise
    if (light_on):
        r += ccd*brightness*brightness_knob
    if (on_sky):
        sky_noise = np.random.normal(0.,np.sqrt(sky_brightness),(160,160))
        r += sky_brightness + sky_noise
    r = r.clip(0,2**16-1)
    heartbeat = 1
    return r.astype('uint16')    

def ttcb():
    """provides the callback for edt_simulator's pdv_wait_image call
    in the case of the tip/tilt sensor ccd
    """
    global tt_ccd
    ph = ph_atmos - wf_woofer # the tilt sensor sees only the correction by the woofer
    ccd = tts(ph)
    noise = np.random.normal(0.,ne_tt,(80,80)) # read noise
    r = tt_cam_backgnd + noise
    if (light_on):
        r += ccd*tt_brightness*brightness_knob*tt_fraction
    if (on_sky):
        sky_noise = np.random.normal(0.,np.sqrt(tt_sky_brightness),(80,80))
        r += tt_sky_brightness + sky_noise
    r = r.clip(0,2**14-1)
    return r.astype('uint16')

buffer_peek = np.zeros((32,32)) # this is reset in shaneo.py to point in to the dpio2.Output object
said_it_once = False

def update_tweeter():
    """provides the callback for dpio2_simulator's DPIO2_CMD_DMA_START call
    """
    global wf_tweeter, buffer_peek, said_it_once

    wf_tweeter = tweeter(buffer_peek.reshape((1024))[tweeter_map])
    if (not said_it_once):
        print '<update_tweeter> successfully called'
        said_it_once = True

def update_woofer(aw):
    """provides the callback for the `virtual woofer server <woofer_client.virtual_woofer_server>` in sumulate mode
    """
    global wf_woofer
    wf_woofer = woofer(aw)

#========== generate precursor matrices ===========
def init_tweeter(verbose=1):
    global At, tweeterModeSet, Mt

    print '<sauce.init> generating tweeter mode command vectors'
    At = A_tweeter(mode)
    print '<sauce.init> generating tweeter mode set...'
    tweeterModeSet = tweeterModes(verbose=verbose)
    print '<sauce.init> generating tweeter self-integral matrix...'
    Mt = M_tweeter(tweeterModeSet,band=100,verbose=verbose)

def init_woofer(nw = 52,verbose=1):
    global Aw, wooferModeSet, Mw

    print '<sauce.init> generating woofer mode command vectors'
    Aw = A_woofer(nmodes = nw)
    print '<sauce.init> generating woofer mode set...'
    wooferModeSet = wooferModes(nmodes = nw,verbose=verbose)
    print '<sauce.init> generating woofer self-integral matrix...'
    Mw = M_woofer(wooferModeSet,nmodes = nw,verbose=verbose)
    
def init(nw=52,verbose=1):
    """This initializes the mode sets, mode command vector sets,
    and self- and cross-integral matrices to support parameter
    matrix generation for rtc. The calculations take several minutes
    """
    global At, tweeterModeSet, Mt
    global Aw, wooferModeSet, Mw
    global Cwt
    
    init_woofer(nw=nw,verbose=verbose)
    init_tweeter(verbose=verbose)
    print '<sauce.init> generating woofer-tweeter cross-integral matrix...'
    Cwt = C_wooferTweeter(wooferModeSet,tweeterModeSet,verbose=verbose)
    
    print '<sauce.init> done'
    
def save(dir=None):
    """Save the generated reconstructor support matrices to disk files.
    The directory is the default one for such matrices, unless the
    argument dir is given.
    """
    if (dir == None):
        dir = os.path.join(home,'parameterFiles','reconMatrix_'+wfs_modes[mode])+'/'
    if (not dir.endswith('/')):
        dir = dir + '/'
    fits.writefits(At,dir+'At.fits',protect=True,verbose=True)
    fits.writefits(np.array(tweeterModeSet),dir+'tweeterModes.fits',protect=True,verbose=True)
    fits.writefits(Mt,dir+'Mt.fits',protect=True,verbose=True)
    fits.writefits(np.array(wooferPokeSet),dir+'wooferPokes.fits',protect=True,verbose=True)
    fits.writefits(Aw,dir+'Aw.fits',protect=True,verbose=True)
    fits.writefits(np.array(wooferModeSet),dir+'wooferModes.fits',protect=True,verbose=True)
    fits.writefits(Mw,dir+'Mw.fits',protect=True,verbose=True)
    fits.writefits(Cwt,dir+'Cwt.fits',protect=True,verbose=True)

def load(dir=None):
    """Read the reconstructor support matrices from disk files.
    The directory from which it reads is the default one for such matrices,
    unless the argument dir is given
    """
    global At, tweeterModeSet, Mt
    global wooferPokeSet, Aw, wooferModeSet, Mw
    global Cwt
    
    if (dir == None):
        dir = os.path.join(home,'parameterFiles','reconMatrix_'+wfs_modes[mode])+'/'
    if (not dir.endswith('/')):
        dir = dir + '/'
    At = fits.readfits(dir+'At.fits',verbose=True)
    tweeterModeSet = fits.readfits(dir+'tweeterModes.fits',verbose=True)
    Mt = fits.readfits(dir+'Mt.fits',verbose=True)
    wooferPokeSet = fits.readfits(dir+'wooferPokes.fits',verbose=True)
    Aw = fits.readfits(dir+'Aw.fits',verbose=True)
    wooferModeSet = fits.readfits(dir+'wooferModes.fits',verbose=True)
    Mw = fits.readfits(dir+'Mw.fits',verbose=True)
    Cwt = fits.readfits(dir+'Cwt.fits',verbose=True)

print '<sauce> ready to go'

#
#  Note the indexing of arrays is row, column = y, x
#
