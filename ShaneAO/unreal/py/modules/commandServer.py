"""The commandServer is :py:mod:`shaneao` command socket connection.
It allows an external operator (GUI, code on remote machine, etc.) to
remotely run commands in the shaneao top-level environment
"""
import socket
import thread
import sys
import time

if ('_commandServer_stop' in globals()):
    _commandServer_stop()
    print 'stopping old commandServer'
    time.sleep(1)

def _commandServer_clientthread(conn,addr):
    """Function for handling initial client connections. This will be used to create threads
    """
    global rval
    conn.settimeout(4)
    while _commandServer_client:
        try:
            line = conn.recv(1024)
            if len(line) == 0:
                break
        except:
            line = None
        if (line != None):
            line = line.strip()
            if len(line) == 0:
                continue
            if (line != 'rval=status()'):
                print '<commandServer _clientthread> got command '+line # debug only
            rval = 'OK'
            try:
                exec line in globals()
            except:
                print sys.exc_info()[0]
                rval = 'ERR'
            if (rval == None):
                rval = 'OK'
            conn.send(str(rval))
    print '<commandServer _clientthread> '+addr[0] + ':' + str(addr[1]) + ' disconnected'
    conn.close()

def say_hi():
    print 'Hi!'
    
def _commandServer_listenerthread():
    """Once the client connection is established,
    a dedicated listenerthread is spun off to serve commands
    """
    global _commandServer_s, _commandServer_conn
    
    HOST = ''   # Symbolic name meaning all available interfaces
    PORT = 8889 # Arbitrary non-privileged port
    
    _commandServer_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    _commandServer_s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print 'Socket created'
    #Bind socket to local host and port
    try:
        _commandServer_s.bind((HOST, PORT))
    except socket.error , msg:
        print '<dataServer listenerthread> Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        raise
         
    print 'Socket bind complete'
    
    #Start listening on socket
    _commandServer_s.listen(10)
    print 'Socket now listening'
    
    #now keep listening for client connections, spawning new threads every time one connects
    _commandServer_s.settimeout(4)
    while _commandServer_listen:
        #print 'waiting to accept a connection - non-blocking call?'
        try:
            _commandServer_conn, addr = _commandServer_s.accept()
            print 'Connected with ' + addr[0] + ':' + str(addr[1])
         
            #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
            thread.start_new_thread(_commandServer_clientthread ,(_commandServer_conn,addr))
        except:
            pass
    print '<_commandServer_listenerthread> socket closing'
    _commandServer_s.close()

def _commandServer_start():
    global _commandServer_listen
    
    _commandServer_listen = True
    thread.start_new_thread(_commandServer_listenerthread,())

def _commandServer_stop():
    global _commandServer_listen, _commandServer_client
    
    _commandServer_listen = False
    _commandServer_client = False
    print 'data server stopped'

_commandServer_client = True

print 'starting new commandServer'
_commandServer_start()
    