#!/usr/bin/env python
# Socket server example in python
# http://www.binarytides.com/python-socket-programming-tutorial/
# http://stackoverflow.com/questions/9914393/sending-and-receiving-data-between-python-and-tcl
"""The dataServer is the
:py:mod:`shaneao` telemetry socket connection. It provides telemetry information
to the :py:mod:`peeko <peeko_client2>` client. The data server
is started automatically when :py:mod:`shaneao` starts.
"""

import socket
import sys
import string
import thread
import numpy as np
import threading
import time
import struct

ccd = np.zeros((160,160))
sub = np.zeros((3600))
a = np.zeros((32,32))

#Function for handling connections. This will be used to create threads
listen = True
t_poll = 4. # seconds
s = None
conn = None
alive = [0,0]

pause = False

def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

def clientthread(conn,addr):
    """Function for handling initial client connections. This will be used to create threads
    """
    global alive
    
    alive[1] += 1
    conn.settimeout(t_poll)
    while listen:
        
        if (pause):
            time.sleep(1)
            continue
        #Receiving from client
        try:
            data = conn.recv(1024)
            if len(data) == 0:
                break
        except:
            data = None
        if (data != None):
            data = data.strip()
            if len(data) == 0:
                continue
            #print '<dataServer.clientthread> got command '+data # debug only
            if (data == 'status'):
                rdata = str(status())
                rdata_size = struct.pack('1l',len(rdata)) # even though we pack as 4 byte, the socket sends 8 bytes! so unpack with '1q'
                #print '<dataServer.clientthread> len(rdata) = '+str(len(rdata))
                conn.sendall(rdata_size)
                conn.sendall(rdata)
            elif (data.startswith('command:')):
                command(data)
            elif (data == 'wfs'): # raw WFS
                rdata = ccd.reshape((160*160))[i_map]
                conn.sendall(rdata.data)
            elif (data == 'wfs_ma'): # moving average WFS
                rdata = wfs_ma.reshape((160*160))[i_map]
                conn.sendall(rdata.data)
            elif (data == 'sub'): # just the subaps (background removed in rtc2)
                rdata = sub
                conn.sendall(rdata.data)
            elif (data == 'wfs_br'): # WFS with background subtracted
                rdata = ccd.reshape((160*160))[i_map] - wfs_dark.reshape((160*160))
                conn.sendall(rdata.data)
            elif (data == 'wfs_ff'): # WFS with background subtracted and flat-fielded
                rdata = (ccd.reshape((160*160))[i_map] - wfs_dark.reshape((160*160)))*wfs_flat.reshape((160*160))
                conn.sendall(rdata.data)
            elif (data == 'wfs_mabr'): # WFS moving average with background subtracted
                rdata = wfs_ma.reshape((160*160))[i_map] - wfs_dark.reshape((160*160))
                conn.sendall(rdata.data)
            elif (data == 'wfs_maff'): # WFS with background subtracted and flat-fielded
                rdata = (wfs_ma.reshape((160*160))[i_map] - wfs_dark.reshape((160*160)))*wfs_flat.reshape((160*160))
                conn.sendall(rdata.data)
            elif (data == 'inten'):
                rdata_size = struct.pack('1l',len(inten))
                conn.sendall(rdata_size)
                rdata = inten
                conn.sendall(rdata.data)
            elif (data == 'da'):
                rdata = da
                conn.sendall(rdata.data)
            elif (data == 'a'):
                rdata = a
                conn.sendall(rdata.data)
            elif (data == 'roi'):
                rdata = [rtc_instance.tt_roi_center[0],
                         rtc_instance.tt_roi_center[1],
                         rtc_instance.tt_roi_size[0],
                         rtc_instance.tt_roi_size[1]]
                sdata = ''
                for d in rdata:
                    sdata += struct.pack('1d',d)
                conn.sendall(sdata)
            elif (data == 'tt'):
                rdata = tts
                conn.sendall(rdata.data)
            elif (data == 'ttbr'):
                rdata = tts - ttbg
                conn.sendall(rdata.data)
            elif (data == 'tts_ma'):  # tip/tilt moving average
                rdata = tts_ma
                conn.sendall(rdata.data)
            elif (data == 'tts_dk'):  # tip/tilt sensor dark
                rdata = tts_dark
                conn.sendall(rdata.data)
            elif (data.startswith('ttw')): # tip/tilt roi window
                args = data.split(' ')
                c = np.fromstring(args[1].strip('[]'))
                s = np.fromstring(args[2].strip('[]'))
                algo = args[3]
                rtc_instance.set_tt_centWts(c,s,algo)
                conn.sendall('ok')
            elif (data == 'sharpen'): # support image sharpening slider GUI
                print '<dataServer.clientthread> acting on sharpen command'
                conn.sendall('ok')
                data = recv_all(conn,48) # exactly 12 float (4-byte) numbers
                count = len(data)/4
                print '<dataServer.clientthread> received count = '+str(count)+' coefficients'
                coefs = struct.unpack(str(count)+'f',data)
                print '<dataServer.clientthread> coefficients are: '+str(coefs)
                slide(coefs)
            elif (data == 'save_sharp'):
                print '<dataServer.clientthread> acting on save sharp command'
                conn.sendall('ok')
                c = recv_all(conn,8)
                count = struct.unpack('1q',c)[0]
                print '<dataServer.clientthread> got cocunt ='+str(count)
                name = recv_all(conn,count)
                print '<dataServer.clientthread> with name = '+name
                refcent_save(name)
            elif (data == 'e-stop'):
                print '<dataServer.clientthread> acting on e-stop command'
                conn.sendall('ok')
                estop()
            elif (string.rstrip(data) == 'bye'):
                break
     
    print '<dataServer.clientthread> '+addr[0] + ':' + str(addr[1]) + ' disconnected'
    conn.close()
    alive[1] -= 1

def listenerthread():
    """Once the client connection is established,
    a dedicated listenerthread is spun off to serve the telemetry
    data.
    """
    global s, conn
    global alive
    
    HOST = ''   # Symbolic name meaning all available interfaces
    PORT = 8888 # Arbitrary non-privileged port
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print 'Socket created'
    #Bind socket to local host and port
    try:
        s.bind((HOST, PORT))
    except socket.error , msg:
        print '<dataServer listenerthread> Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1]
        raise
         
    print 'Socket bind complete'
    
    #Start listening on socket
    s.listen(10)
    print 'Socket now listening'
    alive[0] += 1
    
    #now keep listening for client connections, spawning new threads every time one connects
    s.settimeout(t_poll)
    while listen:
        #print 'waiting to accept a connection - non-blocking call?'
        try:
            conn, addr = s.accept()
            print 'Connected with ' + addr[0] + ':' + str(addr[1])
         
            #start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
            thread.start_new_thread(clientthread ,(conn,addr))
        except:
            pass
    print '<dataServer.listenerthread> socket closing'
    s.close()
    alive[0] -= 1

def start():
    if (alive == [0,0]):
        thread.start_new_thread(listenerthread,())
    else:
        print 'listener is already running, alive = '+str(alive)

def stop():
    global listen
    
    maxCount = 2
    listen = False
    print 'turning off data server'; sys.stdout.flush()
    count = 0
    while (alive != [0,0]):
        print 'waiting for data server threads to exit, '+str(t_poll)+' seconds'
        if (alive == [0,0]): break
        for k in range(int(t_poll)):
            time.sleep(1)
            print '.'
            if (alive == [0,0]): break
        count += 1
        if (count == maxCount):
            print "thread won't respond, quitting anyway"
            return
    print 'data server stopped'

try:
    start()
except socket.error, err_str:
    print '<dataServer> '+err_str
    print '<dataServer> will try again once in 10 seconds'
    t = threading.Timer(10.,start)
    t.start()
    