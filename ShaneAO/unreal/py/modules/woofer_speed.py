"""woofer_speed.py
does a test of the speed with which python can
clock out woofer commands
"""
import time
#import matplotlib
#matplotlib.use('TkAgg')
#import matplotlib.pyplot as plt
import Tkinter as Tk

root = Tk.Tk()
#plt.ion()
#plt.figure()

tap_count = 0
tapper = 0
t_set = []
def tap(start=True):
    global tap_count, tapper, t_set
    if ((start == True) and (tapper == 1)):
        tap_count += 1
        t_set.append(time.time()-t0)
        root.after(5,tap,(True))
    else:
        tapper = 0

t0 = 0
def go():
    global tapper,tap_count,t0,root,t_set
    t_set = []
    tap_count = 0
    tapper = 1
    t0 = time.time()
    tap(True)

def stop():
    tap(False)
    t1 = time.time()
    t = t1-t0
    dt = t/tap_count
    hz = tap_count/t
    print 'tap_count = ',tap_count,' time = ',str(t1-t0)
    print 'rate: '+str(hz)+' hz,'+' dt: '+str(dt)
#    plt.plot(t_set)

#root.destroy()
