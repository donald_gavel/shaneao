"""
futil - file utilities
 search for files in paths or the pythonpath
"""

import os
import sys
import fnmatch

def find(name, path=None):
    if (path == None):
        for path in sys.path:
            r = find(name,path)
            if (r != None):
                print str(r)
        return
    for root, dirs, files in os.walk(path):
        if name in files:
            return os.path.join(root, name)

def find_all(name, path=None):
    if (path == None):
        for path in sys.path:
            r = find_all(name,path)
            if (r != []):
                print str(r)
        return
    result = []
    for root, dirs, files in os.walk(path):
        if name in files:
            result.append(os.path.join(root, name))
    return result

def find_pattern(pattern, path=None):
    if (path == None):
        for path in sys.path:
            r = find_pattern(pattern,path)
            if (r != []):
                print str(r)
        return
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def whereis(filename):
    """find out where a file is in the sys.path (the path for module imports)
    """
    for path in sys.path:
        fullname = path + '/' + filename
        if (os.path.isfile(fullname)):
            return path
