"""wfs.py
routines in support of the ShaneAO Hartmann WFS

    To check the configuration:
    
import wfs
w = wfs.wfs(secondary=True)
tw = wfs.tweeter()
dimg(w.show(tw))
    
    To generate a reconstructor matrix (takes a while):
    
import wfs
w = wfs.wfs(secondary=True)
tw = wfs.tweeter()
w.Hmatrix()
w.m2a(tw)
w.Rmatrix()
"""

import zernike
import numpy as np
import tweeter
import sys

class wfs:
    """
    wfs is the wavefront sensor class. This is configured specifically for
    ShaneAO, where subapertures on the wavefront sensor are aligned to actuators
    on the tweeter DM. See also :py:class:`tweeter`.
    
    The purpose of the wavefront sensor class is to generate reasonable system (H) and
    control (R) matrices for the real-time controller, based on a selection of the
    wfs geometry and some internal mode set.
    
    The current version of this module implements a subset of Zernike modes as
    the internal mode set.
    """

    n_across_select = [7, 14, 30] # subapertures across the pupil, in 8x, 16x, and 30x modes
    n_zmodes_select = [15, 45, 136]
    n_sec_select = [2, 5, 8] 
    d_select = [4, 2, 1]   # spacing of subapertures, in units of tweeter actuator spacings
    mode_select = {'8x': 0, '16x': 1, '30x': 2}
    
    def __init__(self,mode=1,secondary=False):
        microns = 1.0e-6
        micron = microns
        
        if isinstance(mode,str):
            mode = wfs.mode_select[mode]
        
        self.mode = mode
        self.f_number = 28.
        self.wavelength = 0.65*microns
        self.dpri = wfs.n_across_select[mode]
        self.d = wfs.d_select[mode]
        self.secondary = secondary
        if (self.secondary):
            self.dsec = wfs.n_sec_select[mode]
        else:
            self.dsec = 0
        n = 32
        r,th = zernike.polar_array(Rmax=n/2,DS=1.,pr=self.dpri/2,c=[-0.5,-0.5])
        mask = np.where(r[0:n,0:n]<1.,False,True)
        if (self.secondary):
            rs,ths = zernike.polar_array(Rmax=n/2,DS=1.,pr=self.dsec/2,c=[-0.5,-0.5])
            mask = np.where(rs[0:n,0:n]<1,True,mask)
        x = np.arange(-n/2,n/2)*self.d
        y = x
        x,y = np.meshgrid(x,y)
        x = np.ma.MaskedArray(x,mask=mask)
        y = np.ma.MaskedArray(y,mask=mask)
        #self.subapLocs = [x.compressed(),y.compressed()]
        self.subapLocs = [y.compressed(),x.compressed()]        
        self.subaps()

    def type(self):
        """
        Returns the type of the object, which is 'wfs'
        """
        return self.__class__.__name__

    def zmodes(self,nmodes=0,res=10):
        """
        Generate the Zernike mode set, up to <nmodes> number of modes, excluding piston.
        The modes are generated on a fine grid with dx = (actuator spacing)/res.
        (The unit of length on the grid is tweeter actuator spacing.)
        The result is stored in the 3-d data cube self.modeset
        """
        if (nmodes == 0):
            nmodes = wfs.n_zmodes_select[self.mode]
        self.modeiset = np.array(zernike.i2nm(range(1,nmodes)))
        dx = 1./res
        rad = self.dpri*self.d/2.
        r,th = zernike.polar_array(Rmax=rad,DS=dx,pr=rad,c=[-0.5*dx,-0.5*dx])
        x = np.arange(-rad,rad,dx)
        y = x
        self.x,self.y = np.meshgrid(x,y)
        self.dx = dx
        self.nmodes = nmodes-1
        z0 = zernike.zernike(0,0,r,th)
        self.ap = (np.ones_like(z0.mask)-z0.mask)
        self.modeset = []
        for i in range(0,nmodes-1):
            n = int(self.modeiset[0,i])
            m = int(self.modeiset[1,i])
            mode = zernike.zernike(n,m,r,th,limit=False)
            self.modeset.append(mode)

    def subaps(self):
        """
        Generate a set of windows that define the subaperture regions on the pupil.
        The result is stored in a 3-d data cube self.subapset.
        Another array called self.wap is the 2-d array that is the union of all the subaps
        """
        if (not hasattr(self,'modeset')):
            self.zmodes()
        self.n_subaps = len(self.subapLocs[0])
        subap0 = np.zeros_like(self.modeset[0])
        wap = np.zeros_like(subap0)
        n = subap0.shape[0]
        self.subapset = []
        ns = self.d/self.dx
        for i in range(0,self.n_subaps):
            x0 = self.subapLocs[0][i]/self.dx + n/2
            y0 = self.subapLocs[1][i]/self.dx + n/2
            subap = subap0.copy()
            subap[x0:x0+ns,y0:y0+ns] = np.ones((ns,ns))
            self.subapset.append(subap)
            wap += subap
        self.wap = wap

    def gmodes(self):
        """
        Calculaate the gradients of each of the modes that were generated using zmodes.
        These gradients of modes will subsequently be used to determine the slope signals
        that each mode causes on the Hartmann wavefront sensor.
        The numerical approach is to take a central difference on the fine grid scale.
        The results are stored in 3-d data cubes self.gxset and self.gyset for x and y
        slopes respectively.
        """
        if (not hasattr(self,'modeset')):
            self.zmodes()
        self.gxset = []
        self.gyset = []
        mask = np.ones_like(self.modeset[0])
        n = mask.shape[0]
        mask[0,:] *= 0.
        mask[n-1,:] *= 0.
        mask[:,n-1] *= 0.
        mask[:,0] *= 0.
        for i in range(0,self.nmodes):
            mode = self.modeset[i]
            gx = -((np.roll(mode,1,axis=1) - mode) + (mode - np.roll(mode,-1,axis=1)))/(2.*self.dx)
            gy = -((np.roll(mode,1,axis=0) - mode) + (mode - np.roll(mode,-1,axis=0)))/(2.*self.dx)
            gx *= mask
            gy *= mask
            self.gxset.append(gx)
            self.gyset.append(gy)
            
    def Hmatrix(self):
        """
        Hmatrix creates the system matrix, H, sometimes called the forward matrix,
        for the Hartmann sensor. H is a mapping from mode coeffients of wavefront phase
        to Hartmann slope signals.
        
        The math is
        
          s = H c
        
        where c is the vector of mode coefficients and s is the vector of wfs slopes,
        stacked as [s_x, s_y]
        
        The result of the calculation is the matrix self.H which has dimensions
        number of slopes by number of modes.
        """
        if (not hasattr(self,'gxset')):
            self.gmodes()
        if (not hasattr(self,'n_subaps')):
            self.subaps()
        H = np.empty((self.n_subaps*2,self.nmodes))
        i = 0
        for mode in self.modeset:
            gx = self.gxset[i]
            gy = self.gyset[i]
            j = 0
            for subap in self.subapset:
                nf = float(np.sum(subap))
                H[j,i] = np.sum(subap*gx)/nf
                H[j+self.n_subaps,i] = np.sum(subap*gy)/nf
                j += 1
            i += 1
        self.H = np.matrix(H)
            
    def sense(self,phi):
        """
        Simulate the Hartmann sensor by calculating the
        local slopes on phase sheet phi averaged
        on subapertures. The slopes are returned in a vector
        of length number of subaps x 2: all of the x slopes
        followed by all of the y slopes.
        
        The input array, phi, must be the same shape and on the same
        sampling as the modes in the wfs mode set.
        """
        if (not hasattr(self,'n_subaps')):
            self.subaps()
        n,m = phi.shape
        mask = np.ones_like(self.modeset[0])
        nc,mc = mask.shape
        if (not(n == nc) or not(m == mc)):
            print "<wfs.sense> ERROR input array must be same size as mode"
            return 0
        mask[0,:] *= 0.
        mask[n-1,:] *= 0.
        mask[:,n-1] *= 0.
        mask[:,0] *= 0.
        gx = -((np.roll(phi,1,axis=1) - phi) + (phi - np.roll(phi,-1,axis=1)))/(2.*self.dx)
        gy =  ((np.roll(phi,1,axis=0) - phi) + (phi - np.roll(phi,-1,axis=0)))/(2.*self.dx)
        gx *= mask
        gy *= mask
        ns = self.n_subaps
        s = np.empty((ns*2))
        j = 0
        for subap in self.subapset:
            nf = float(np.sum(subap))
            s[j] = np.sum(subap * gx)/nf
            s[j+ns] = np.sum(subap * gy)/nf
            j += 1
        return s
            
    def show(self,tw):
        """
        Create a diagnostic image that shows the positions
        of the actuators, subapertures, and pupil overlayed
        
        Requires a tweeter object, tw, as input.
        
        Returns a 2-d array, the diagnostic layout image
        
        .. image:: wfs-show.png
        """
        u = np.array(self.ap+self.wap)
        dx = tw.dx
        dy = dx
        n,m = u.shape
        n2 = int(n/2)
        i = 0
        for x,y in tw.actLocs:
            ix = int(x/dx)+n2
            iy = int(y/dy)+n2
            if ((ix >= 0) and (iy >= 0) and (ix < n) and (iy < n)):
                u[ix,iy] += 1.
        return u
        
    def m2a(self,tw,method = 'average'):
        """
        The method m2a creates the matrix A, which converts from mode coefficients into
        actuator command vectors.
        
        The result is stored in a matrix self.A which has dimensions
        number of actuators by number of modes.
        
        The present numerical method is to simply set the actuator to the
        average value of the mode within a box surrounding the actuator that extends
        half way to neighboring actuators all around. A fancier technique might take
        into account the influence function of the actuator, so this method depends
        on the tweeter object, tw, given in the argument list.
        """
        # averaging method. also could be Fourier
        nacts = len(tw.actLocs)
        nmodes = self.nmodes
        n,m = self.modeset[0].shape
        n2 = int(n/2)
        Dx = int(tw.da/tw.dx)/2
        A = np.zeros((nmodes,nacts))
        nf = (Dx*2)**(-2) # factor for averaging
        j = 0
        for mode in self.modeset:
            i = 0
            for x,y in tw.actLocs:
                box = np.rint(np.array([[x,y],[x,y]])/tw.dx) + np.array([[-Dx,-Dx],[Dx,Dx]]) + n2
                box = np.clip(box,0,n-1)
                A[j,i] = np.sum(mode[box[0,0]:box[1,0],box[0,1]:box[1,1]])*nf
                i += 1
            j += 1
        self.A = np.matrix(A)
    
    def Rmatrix(self):
        """
        Calculate the reconstruction matrix, R, given the system matrix, H, and the
        mode-to-actuator matrix, A.
        
        The math is:
        
         R = A H :sup:`+`
         
        where H :sup:`+` is the pseudo-inverse of H.
        
        Note this is the most general of reconstruction matrix formulations. The
        internal mode set is any, not necessarily orthonormal, set of basis functions
        that are convenient and spanning the Hilbert subspace you want to
        restrict control solutions to. For example, the mode set can be Zernike functions,
        Fourier function (sines and cosines), natural DM modes, actuator influence
        functions, and so on, or any combination of these. The A matrix simply
        maps these basis functions to actuator command vectors.
        For example A could be the identity matrix
        in the case where the 'mode set' is the set of actuator influence functions.
        (not recommended in the ShaneAO case, since the actuators over-sample what
        is visible to the wavefront sensor).
        .
        The pseudo-inverse of H could be computed according to regularization, penalty
        function, or Bayesian statistical priors methodologies, i.e.
        
         H :sup:`+` = (Q + H :sup:`T` W H) :sup:`-1` H :sup:`T`
         
        """
        self.R = np.transpose(self.A) * np.linalg.pinv(self.H)


def test_wfs():

    w = wfs()
    print 'creating H matrix'; sys.stdout.flush()
    if (not hasattr(w,'H')):
        w.Hmatrix()
    tw = tweeter.tweeter()
    print 'creating A matrix'; sys.stdout.flush()
    if (not hasattr(w,'A')):
        w.m2a(tw)
    print 'creating R matrix'; sys.stdout.flush()
    w.Rmatrix()
    print 'computing a random phase'; sys.stdout.flush()
    c = np.random.normal(0.,1.,(w.nmodes,1))
    n,m = w.modeset[0].shape
    i = 0
    phi = np.zeros((n,m))
    for mode in w.modeset:
        phi += c[i]*mode
        i += 1
    print 'sensing and reconstructing'; sys.stdout.flush()
    s = w.sense(phi)
    s = np.matrix(np.reshape(s,(w.n_subaps*2,1)))
    a = w.R * s
    a = np.array(np.reshape(a,(tw.na,tw.na)))
    return (w,tw,phi,a)
    
    