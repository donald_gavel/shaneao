#!/usr/bin/env python
"""Differential Image Motion analysis for ShaneAO

Translated from Reni Kupke's DIMM_seeing.pro IDL routine
"""
import os,sys
import numpy as np
import readfits as fits
import img
import datetime

machine = os.uname()[1]
if (machine != 'real.ucolick.org'):
    import matplotlib.pyplot as plt
    plt.ion()

arcsec = np.pi/(180.*3600)

home = os.environ['HOME']

dataHome_local = {
    'real':'/local/data/telemetry',
    'other':'/Users/donaldgavel/Dropbox/seeing'
    }

def test():
    dataDir = os.path.join(home,'data_2014-10-05')
    dataFile = os.path.join(dataDir,'Data_0000.fits')
    l,t = process(dataFile)
    if (machine != 'real.ucolick.org'):    
        hplot(l,t)

def set_pos():
    x=np.zeros(144)

    x[0:6]=[5,6,7,8,9,10]
    x[6:14]=[4,5,6,7,8,9,10,11]
    x[14:24]=[3,4,5,6,7,8,9,10,11,12]
    x[24:36]=[2,3,4,5,6,7,8,9,10,11,12,13]
    x[36:50]=[1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    x[50:56]=[1,2,3,4,5,6]
    x[56:62]=[9,10,11,12,13,14]
    x[62:67]=[1,2,3,4,5]
    x[67:72]=[10,11,12,13,14]
    x[72:77]=[1,2,3,4,5]
    x[77:82]=[10,11,12,13,14]
    x[82:88]=[1,2,3,4,5,6]
    x[88:94]=[9,10,11,12,13,14]
    x[94:108]=[1,2,3,4,5,6,7,8,9,10,11,12,13,14]
    x[108:120]=[2,3,4,5,6,7,8,9,10,11,12,13]
    x[120:130]=[3,4,5,6,7,8,9,10,11,12]
    x[130:138]=[4,5,6,7,8,9,10,11]
    x[138:144]=[5,6,7,8,9,10]

    y=np.zeros(144)
    
    y[0:6]=1
    y[6:14]=2
    y[14:24]=3
    y[24:36]=4
    y[36:50]=5
    y[50:62]=6
    y[62:72]=7
    y[72:82]=8
    y[82:94]=9
    y[94:108]=10
    y[108:120]=11
    y[120:130]=12
    y[130:138]=13
    y[138:144]=14
    
    return x,y

def process(dataFile=None,plot=False):
    
    global _l,_t
    
    if (not dataFile):
        dataDir = os.path.join(home,'data_2014-10-05')
        dataFile = os.path.join(dataDir,'Data_0000.fits')
        
    print '<process> reading the data: '+dataFile+' ...'; sys.stdout.flush()
    AO_teldata = fits.readfits(dataFile)    # read in data from FITS files
    print '<process> done'; sys.stdout.flush()
    
    xvalues,yvalues = set_pos()   # set x and y values for subapertures from lookup table

    wfs_x = AO_teldata[1:4097,0:144]        # extract centroids from telemetry data
    wfs_y = AO_teldata[1:4097,144:288]      #  ignore first point in data set, which is bogus
    
    dimm_x = np.zeros((4096,144,144))         # differential image motion from x-centroids
    dimm_y = np.zeros((4096,144,144))         # differential image motion from y-centroids
    #dimm_t = np.zeros((4096,144,144))         # transverse differential image motion
    #dimm_l = np.zeros((4096,144,144))         # longitudinal differential image motion
    baseline = np.zeros((144,144))            # distance between differenced subaps
    temptheta = np.zeros(144)         
    theta = np.zeros((144,144))               # angle baseline makes with x-axis

    print '<process> calculating differences'; sys.stdout.flush()
    for ii in range(144):
        dimm_x[:,ii,:] = (wfs_x - img.shift(wfs_x, (ii, 0)))*(1.3*arcsec)    # calculate differences in centroids between pairs
        dimm_y[:,ii,:] = (wfs_y - img.shift(wfs_y, (ii, 0)))*(1.3*arcsec)    # of subapertures, convert to radians
        xdiff = xvalues - img.shift(xvalues, ii) 
        ydiff = yvalues - img.shift(yvalues, ii)
        baseline[ii,:] = np.sqrt(xdiff**2 + ydiff**2)      # calculate baseline distances for each pair
        theta[ii,:] = np.arctan2(ydiff,xdiff)
    
    print '<process> rotating to transverse and longitudinal'; sys.stdout.flush()
    #for jj in range(4096):
    #    dimm_t[jj,:,:] = dimm_y[jj,:,:]*np.cos(theta) + dimm_x[jj,:,:]*np.sin(theta)  # rotate x and y differences to 
    #    dimm_l[jj,:,:] = dimm_x[jj,:,:]*np.cos(theta) - dimm_y[jj,:,:]*np.sin(theta)  # get longitudinal and transverse
    
    dimm_t = dimm_y*np.cos(theta) + dimm_x*np.sin(theta)  # rotate x and y differences to 
    dimm_l = dimm_x*np.cos(theta) - dimm_y*np.sin(theta)  # get longitudinal and transverse

    print '<process> calculating variances'; sys.stdout.flush()
    sigmasq_l = np.zeros((144,144))
    sigmasq_t = sigmasq_l
    sigmasq_l = np.var(dimm_l,axis=0) # find variance of differences
    sigmasq_t = np.var(dimm_t,axis=0)

    kl = np.zeros((144, 144))
    kt = np.zeros((144, 144))
    fwhm_l = np.zeros((144,144))
    fwhm_t = np.zeros((144,144))

    print '<process> calculating full-width half-maxes'; sys.stdout.flush()
    kl[baseline != 0] = 0.358*(1 - 0.541*(baseline[baseline != 0])**(-1./3.)) # calculate K_l and K_t for each baseline
    kt[baseline != 0] = 0.358*(1 - 0.810*(baseline[baseline != 0])**(-1./3.))
    
    fwhm_l[ kl != 0] = 0.98*((214.3/0.7e-3)**(0.2))*(sigmasq_l[ kl != 0]/kl[ kl != 0])**(0.6) # calculate FWHM_l and FWHM_t
    fwhm_t[ kt != 0] = 0.98*((214.3/0.7e-3)**(0.2))*(sigmasq_t[ kt != 0]/kt[ kt != 0])**(0.6)

    # convert to arcseconds
    
    fwhm_l = fwhm_l/arcsec  # convert from radians to arcseconds
    fwhm_t = fwhm_t/arcsec
    
    print '<process> done'; sys.stdout.flush()
    l,t = fwhm_l.flatten(),fwhm_t.flatten()
    if (plot):
        hplot(l,t)
        
    _l,_t = l,t # save latest result in private variables
    return l,t

if (machine != 'real.ucolick.org'):
    
    def hplot(l,t):
        plt.hist(l,bins=49,range=(0.3,1.3),histtype='step',align='left',color='black')
        plt.hist(t,bins=49,range=(0.3,1.3),histtype='step',align='left',color='black',linestyle='dashed')
        plt.hist(t,bins=49,range=(0.3,1.3),histtype='step',align='left',color='blue',alpha=0.2,linewidth=4)
        plt.grid('on')
        plt.xlabel('FWHM, arcsec')
        plt.ylabel('Counts/bin')
        plt.title('ShaneAO DIMM histogram')
    
if __name__ == '__main__':
    usageStr = """
    dimm [-h --help | -d date ] [ filenum | fullFileName ]
    
    Use DIMM analysis to produce a histogram of possible seeing FWHM
    given an open-loop data telemetry file (result from a ShaneAO 'data()' command)
    
    Arguments:
        -h, --help: print help
        -d date: set the date of the desired file directory using the format
            YYYY-MM-DD. Default is today's date
        filenum: an integer. Data files are named Data_xxxx.fits where xxxx is
            the file number. You don't need the leading 0s.
    
    Outputs:
        Histogram of FWHM estimates
        Prints the means of longitudinal (l) and transverse (t) based estimates

    Examples:
        dimm 2                <- evaluate file Data_0002.fits in the present night's directory
        dimm 0002             <- leading zeros are ok
        dimm -d 2015-06-29 2  <- evaluate file Data_0002.fits in June 29, 2015 directory
        dimm -d 6-29 2        <- present year is implied
        dimm -d 29 2          <- present year and month is implied
        dimm /local/data/telemetry/2015-06-30/Data_0002.fits  <- evaluate a specific file
    """
    def _parse_date(dateStr=None):
        date = datetime.datetime.now()
        # if past midnight, use the previous date as the sub directory
        if (date.hour < 12):
            date = datetime.datetime.now() - datetime.timedelta(hours=12)
        year = '%4d'%date.year
        month = '%02d'%date.month
        day = '%02d'%date.day
        if (dateStr):
            dateList = dateStr.split('-')
            if (len(dateList) >= 1):
                day = '%02d'%int(dateList[-1])
            if (len(dateList) >= 2):
                month = '%02d'%int(dateList[-2])
            if (len(dateList) >= 3):
                year = '%4d'%int(dateList[-3])
        dateStr = year+'-'+month+'-'+day
        return dateStr
    
    doit = False
    testMode = False
    if (len(sys.argv) == 1):
        print usageStr
    if (len(sys.argv) > 1):
        if (machine == 'real.ucolick.org'):
            plotFlag = False
            dataHome = dataHome_local['real']
        else:
            plotFlag = True
            dataHome = dataHome_local['other']

        getting_date = False
        dateStr = _parse_date()
            
        for keyword in sys.argv[1:]:
            if (getting_date):
                dateStr =_parse_date(keyword)
                getting_date = False
                continue
            if (keyword in ['--help', '-h']):
                print usageStr
                doit = False
                continue
            if (keyword in ['--date', '-d']):
                getting_date = True
                continue
            if (keyword == '-t'):
                testMode = True
                dataHome = dataHome_local['real'] # as if on real
                continue
            filenum = keyword
            if (not filenum.isdigit()):
                # file arg not an integer, interpret as a full path
                dataHome,filename = os.path.split(filenum)
                dateStr = ''
                doit = True
            else:
                filenum = '%04d'%int(filenum.lstrip('0') or '0')
                filename = 'Data_'+filenum+'.fits'
                doit = True
                
    if (doit):
        filename = os.path.join(dataHome,dateStr,filename)
            
        if (testMode):
            print 'filename = '+filename
        else:
            l,t = process(filename,plot=plotFlag)
            print 'mean seeing (l) is %4.2f arcsec'%np.mean(l)
            print 'mean seeing (t) is %4.2f arcsec'%np.mean(t)

        if (plotFlag):
            raw_input('hit return to exit > ')

