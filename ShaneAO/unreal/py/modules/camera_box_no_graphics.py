"""camera_box.py
Runs the cameras independent of the shaneao RTCode.
The RTCode can not be running the same camera at the same time as this code is.
It can fulfil serveral purposes:

1. Perform 'out-of-the-box' tests with the SciMeasure Camera and kpython
2. Grab blocks of images and display a movie of them
3. Show images 'live' from the camera
4. Archive image data as fits files

This only works with edt/recon version of edt.so:
cd ~gavel/local/lib
ln -s ~gavel/cvs/lroot/Shaneao/unreal/edt/recon/build/lib/linux-x86.64-2.6/edt.so edt.so
===========================================================
For the purpose of data taking during the flexure tests:

1. tt_cam_live() to adjust brightness and exposure (through the rate parameter 1000,500,125,or 40 Hz)
2. tt_cam_run() to grab a burst of frames and save temporarily to disk
3. tt_archive() to put the data in today's archive - it will ask for a comment line that goes into the fits header
4. note the sequence number as tt_archive writes the fits files. enter these in the logs

"""
module_file_name = __name__+'.py'

import edt
import os
import numpy as np
import readfits as fits
import struct
import time
import pyfits
import subprocess
#import dimg
import datetime

home = os.environ['HOME']
param_dir = os.path.join(home,'parameterFiles')
config_dir = os.path.join(home,'config')

imap = fits.readfits(os.path.join(param_dir,'i_map.fits'))

def cam_run(n_frames=5, delta_t=1.0, head='WFS_Data'):
    """ run the WFS camera for some period and save raw files to disk
    """
    configFile = os.path.join(config_dir,'WFS_BigJoe.cfg')
    foo = edt.Camera(1,configFile)
    h = edt.Handler(foo,'saveAsRaw')
    h.configure([head,delta_t],'configSaveAsRaw')
    foo.handler(h)
    foo.start()
    for k in range(n_frames):
        time.sleep(delta_t)
        print '<cam_run> frame '+str(k)
    foo.stop()

def get_frames(head='WFS_Data'):
    """get the WFS camera images saved as raw files on disk
    and return them in a list
    """
    frameSet = []
    file_list = os.listdir('.')
    for fileName in file_list:
        if (fileName.startswith(head) and fileName.endswith('.raw')):
            file = open(fileName)
            u = file.read()
            file.close()
            v = struct.unpack('25600h',u)
            v = np.array(v)
            w = v[imap]
            frameSet.append(w)
    return frameSet

def tt_cam_run(n_frames=5, delta_t = 1.0, head='TT_Data'):
    """ run the TT Camera for some period and save raw files to disk
    """
    configFile = os.path.join(config_dir,'TipTilt_LittleJoe.cfg')
    foo = edt.Camera(0,configFile)
    h = edt.Handler(foo,'saveAsRaw')
    h.configure([head,delta_t],'configSaveAsRaw')
    foo.handler(h)
    foo.start()
    for k in range(n_frames):
        time.sleep(delta_t)
        print '<cam_test> frame '+str(k)
    foo.stop()

# def tt_cam_live(n_frames=10,delta_t = 0.5,rate=125,log=False):
#     """display the TT Camera live display, running
#     for some fixed number of frames
#     
#     delta_t is the time between displays of a camera frame (a 'human' rate of about 1/2 sec)
#     rate is the camera exposure rate. available rates are 1000, 500, 125, and 40 Hz
#     log is True or False depending on whether the display should be log stretched
#     """
#     head = 'TT_Data_Temp'
#     rates = [1000,500,125,40]
#     if (rate in rates):
#         seq_no = rates.index(rate)
#     else:
#         raise ValueError, 'invalid rate, use one of '+str(rates)
#     plt = dimg.plt
#     plt.ion()
#     plt.figure(1)
#     plt.title('Live TT Cam')
#     configFile = os.path.join(config_dir,'TipTilt_LittleJoe.cfg')
#     foo = edt.Camera(0,configFile)
#     h = edt.Handler(foo,'saveAsRaw')
#     h.configure([head,delta_t],'configSaveAsRaw')
#     foo.handler(h)
#     resp = foo.serial('@RCL '+str(seq_no))
#     print '<tt_cam_live> response from camera: '+resp
#     time.sleep(.5)
#     foo.start()
#     for k in range(n_frames):
#         time.sleep(delta_t*1.1)
#         file_list = os.listdir('.')
#         q = []
#         for fileName in file_list:
#             if (fileName.startswith(head) and fileName.endswith('.raw')):
#                 q.append(fileName)
#         q.sort()
#         fileName = q.pop()
#         file = open(fileName)
#         print fileName
#         u = file.read()
#         file.close()
#         v = struct.unpack('6400h',u)
#         v = np.array(v).reshape((80,80))
#         plt.clf()
#         if (log):
#             dimg.show(np.log10(v+1),fig=1,origin='lower')
#             plt.title(fileName + ' log scale')
#         else:
#             dimg.show(v,fig=1,origin='lower')
#             plt.title(fileName)
#         plt.draw()
#         os.remove(fileName)
#         for fileName in q:
#             os.remove(fileName)
#     foo.stop()
#     time.sleep(.5)
#     del foo
#     file_list = os.listdir('.')
#     for fileName in file_list:
#         if (fileName.startswith(head) and fileName.endswith('.raw')):
#             os.remove(fileName)
    
def tt_get_frames(head='TT_Data'):
    """get the TT camera images saved as raw files on disk
    and return them in a list
    """
    frameSet = []
    file_list = os.listdir('.')
    for fileName in file_list:
        if (fileName.startswith(head) and fileName.endswith('.raw')):
            file = open(fileName)
            u = file.read()
            file.close()
            v = struct.unpack('6400h',u)
            v = np.array(v).reshape((80,80))
            frameSet.append(v)
    return frameSet

def tt_archive(head='TT_Data',comment=None,delraw=True):
    """archive all the TT Camera raw files in the present
    directory (presumably from tt_cam_run operations)
    and store them in an archive.
    The archive is a subdirectory with the name 'data_'+todays date
    and each file is uniquely named with a sequence number.
    """
    tt_archive_dir = 'data_'+time.strftime('%Y-%m-%d')
    if (not os.path.exists(tt_archive_dir)):
        os.mkdir(tt_archive_dir)
    if (not os.path.exists(os.path.join(tt_archive_dir,'seq_no'))):
        seq_no = np.array([0])
        seqhdu = pyfits.PrimaryHDU(seq_no)
        seqhdu.header['seq_no'] = (0,'sequence number')
        seqhdu.writeto(os.path.join(tt_archive_dir,'seq_no'))
    if (comment == None):
        comment = raw_input('Enter a description of this data: ')
    seqhdu = pyfits.open(os.path.join(tt_archive_dir,'seq_no'),mode='update')
    seq_no = seqhdu[0].data[0]
    file_list = os.listdir('.')
    for fileName in file_list:
        if (fileName.startswith(head) and fileName.endswith('.raw')):
            file = open(fileName)
            u = file.read()
            file.close()
            t = time.localtime(os.path.getctime(fileName))
            day = time.strftime('%Y-%m-%d',t)
            tim = time.strftime('%H:%M:%S',t)
            v = struct.unpack('6400h',u)
            v = np.array(v).reshape((80,80))
            hdu = pyfits.PrimaryHDU(v)
            hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
            hdu.header.append(('DEVICE','TT Camera'))
            hdu.header.append(('DATE',day,'File generation date'))
            hdu.header.append(('TIME',tim,'file generation time hhmmss'))
            hdu.header.append(('GENER',module_file_name,'tt_archive'))
            hdu.header['comment'] = comment
            fitsFileName = os.path.join(tt_archive_dir,'TT_Data_'+ '%04d' % seq_no +'.fits')
            print fileName + ' --> ' + fitsFileName
            hdu.writeto(fitsFileName)
            time.sleep(.1)
            seq_no += 1
            if (delraw):
                os.remove(fileName)
    seqhdu[0].header['seq_no'] = (seq_no,'sequence number')
    seqhdu[0].data[0] = seq_no
    seqhdu.close()

# def movie(u,n_times=1,delta_t=.5,cmap='hot'):
#     disp = dimg.show
#     plt = dimg.plt
#     plt.ion()
#     n = np.sqrt(u[0].shape[0]).astype(int)
#     plt.figure(0)
#     plt.clf()
#     nf = len(u)
#     k = 0
#     for j in range(n_times*nf):
#         disp(u[k],fig=0,cmap=cmap)
#         plt.title('Frame '+str(k))
#         plt.draw()
#         k = (k+1)%nf
#         time.sleep(delta_t)

