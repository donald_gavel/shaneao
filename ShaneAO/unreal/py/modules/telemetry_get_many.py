# -*- coding: utf-8 -*-
"""
Get many telemetry items.
"""

import __main__
import time
import ktl
telemetry = __main__.telemetry

def capture_sharcs_image(sharcs):
    """Command to capture a sharcs image."""
    sharcs['IMAGEDONE'].waitfor("=1")
    sharcs['IMAGEDONE'].write(0)
    sharcs['GO'].write(1)
    time.sleep(1.0)
    sharcs['IMAGEDONE'].waitfor("=1")

def telemetry_collect_func(n, func, values, original, zero=True, n_images=0, displayname=None):
    """Collect telemetry iterating through values and applying a function.
    
    :param n: Number of telemetry frames to take per step.
    :param func: The function to use.
    :param values: The values to use as args for the function.
    :param original: The value to use as args for the function when finished.
    :param bool zero: Zero the mirrors before each telemetry collection.
    
    """
    sharcs = ktl.Service('sharcs')
    displayname = displayname or func.__name__
    try:
        for v in values:
            func(v)
            for i in range(n):
                if zero:
                    shaneao.zero()
                shaneao.close()
                print("Recording closed loop telemetry at {0:s} = {1!r}".format(displayname, v))
                time.sleep(2.0)
                telemetry.recorder.press_shutter()
                for s in range(n_images):
                    capture_sharcs_image(sharcs)
                telemetry.recorder.wait_for_completion()
                shaneao.open()
                if zero:
                    shaneao.zero()
                print("Recording open loop telemetry at {0:s} = {1!r}".format(displayname, v))
                time.sleep(2.0)
                telemetry.recorder.press_shutter()
                for s in range(n_images):
                    capture_sharcs_image(sharcs)
                telemetry.recorder.wait_for_completion()
    finally:
        print("Resetting {0:s} = {1!r}".format(displayname, original))
        func(original)
    print("Done.")
    
def telemetry_collect_alpha(n, values, zero=True):
    """Collect different alpha values."""
    original = shaneao.alpha()
    telemetry_collect_func(n, shaneao.alpha, values, original, zero=zero)

def telemetry_collect_keyword(n, keyword, values, displayname=None, **kwargs):
    """Collect telemetry across keywords.
    
    :param n: Number of telemetry frames to take per step.
    :param keyword: The keyword name to use for iterating.
    :param values: The values to use for the keyword.
    :param displayname: A name to show in messages.
    :param bool zero: Zero the mirrors before each telemetry collection.
    
    """
    service = ktl.Service('saocon')
    keyword = service[keyword]
    original = keyword.read()
    displayname = keyword.name if displayname is None else displayname
    
    def keyword_write(v):
        return keyword.write(v)
    telemetry_collect_func(n, keyword_write, values, original, displayname=displayname, **kwargs)

def walk_gain(n, gains, **kwargs):
    """Walk through many gain steps."""
    telemetry_collect_keyword(n, 'SMART_GAIN', gains, displayname='gain', **kwargs)