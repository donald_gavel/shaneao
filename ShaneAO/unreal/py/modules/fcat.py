#!/usr/bin/env python
"""Print the header of a fits file
"""

import os
import sys
import pyfits
from pprint import pprint as pp

def fcat(fileName,ext=0):
    if os.path.exists(fileName):
        print '*'*10+' '+fileName+': '+'*'*10
        hdu = pyfits.open(fileName)
        hdr = hdu[ext].header
        pp(hdr)
        hdu.close()
    else:
        print 'unfortunately the file named '+fileName+' does not exist in this directory'

helpstr = """usage: fcat file [n]
    where file is a fits file
    optional n is the fits extension number"""

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print helpstr
    else:
        fileName = sys.argv[1]
        ext = 0
        if (len(sys.argv) == 3):
            ext = int(sys.argv[2])
        if (fileName == '--help'):
            print helpstr
        else:
            for fileName in sys.argv[1:]:
                fcat(fileName,ext=ext)

