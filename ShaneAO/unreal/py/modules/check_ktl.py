# check_ktl.py
#  run as kpython check_ktl.py
#
import ktl
saomot = ktl.Service('saomot')
saomot.heartbeat ('DISP3CLK') # MEMSPULSE is provided by dispatcher #3
memspulse = saomot['MEMSPULSE']
memspower = saomot['MEMSPOWER']
memspower.write('enable')
memspulse.write(1,wait=False)
memspower.write('disable')
