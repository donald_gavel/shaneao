#!/usr/bin/python
"""
Field steering requires the coordinated move of two mirrors. Since there are two degrees of freedom
(in each axis) there are two things that can be controlled given the appropriate
decoupling function applied to the mirror motions:

    * **pointing** where the field is moved without shifing the exit pupil, and
    * **centering** where the pupil is moved without shifting the field

Both kinds of move may be needed during alignment, but, hopefully, the decoupling functions are
well enough modeled and calibrated so that during on-sky operations only pointing moves are needed.

This module defines the decoupling functions and can generate
lookup table maps or apply the functions directly during field steer operations.

To use this code to print out motor commands for certain pointing or centering moves::

    from fieldSteer import *
    point(x,y)
    center(u,v)

The output of these calculations can be directly connected to the motor control dispatchers via the
command() function, for example::

    command(point(x,y))
    command(center(u,v))

Note: the moves (x,y), (u,v) are absolute, because it is a nonlinear model. (x,y) are
in arecseconds from the nominal center, and (u,v) are in fractions of a pupil from the
nominal center.

To use the code to generate lookup tables::

    from fieldSteer import *
    mp = genPTable()
    mc = genCTable()

"""
import os
import sys
import numpy as np
np.set_printoptions(linewidth = 120)
import scipy.linalg
import scipy.optimize
import subprocess
import fieldSteering_interpolate as fsi

home = os.environ['HOME']
dataDir = os.path.join(home,'data')
machine = os.uname()[1]

if (machine == 'shade.ucolick.org'):
    modifyCommand = 'modify'
else:
    modifyCommand = 'ssh gavel@shade modify'

usage_str = """
====== Field Steering for NGS and LGS mode =====

fieldSteer mode=ngs pos=name
    move the wavefront sensor field steering pair to a named position

fieldSteer -f mode=ngs north=n east=e
    find the nearest point in the database to n,e and
    moves the steering mirrors to that position

fieldSteer -e mode=ngs north=n east=e [cx=cx cy=cy]
    move the ngs mode field steering mirrors
    to the exact requested position.
    The danger of this is there is no pre-calibrated centering
    correction. Also, the requested field positions may exceed
    the limits of the motors. +/- 8 arcsec in each direction
    should be save.
    
fieldSteer -i mode=ngs north=n east=e
    move the ngs mode field steering mirrors
    to the interpolated calculation that puts the
    object at a given position on the ShARCS detector
    and does a fair job of pupil centering on the WFS.
    The requested field positions may not exceed
    the limits of the interpolation data set, which
    is currently about +/- 4 arcsec in each direction.
    
fieldSteer mode=lgs north=n east=e
    move the tip/tilt sensor stage to a given field position

fieldSteer -l (or --list) lists the valid ngs mode moves

fieldSteer -h (or --help) prints this message

-n added to the command line does the computations and reports
results, but does not move the motors.

================================================
"""

# skip_matplotlib is set by the fieldSteer wrapper script,
# which never wants to do plotting, and the error messages
# from a failed import of matplotlib are distracting.

try:
    skip_matplotlib
except NameError:
    skip_matplotlib = False

if skip_matplotlib == False:
    try:
        import matplotlib.pyplot as plt
    except:
        # The user does not have a display. Plotting is not
        # possible without a display, but not all functions
        # in this module require plotting.
        pass
    else:
        plt.ion()

# units & conventions
mm = 0.001
mr = 0.001
I = np.eye(3)
i = 1j
pi = np.pi
cos = np.cos
sin = np.sin
degrees = pi/180.

def vector(l):
    """create a column vector given a list
    """
    return np.matrix(l).transpose()

x = vector([1,0,0])
y = vector([0,1,0])
z = vector([0,0,1])
origin = vector([0,0,0])

def rotate(dir,alpha):
    """return the active rotation matrix about an axis
    dir = 'x', 'y', or 'z'
    alpha = angle, in radians
    """
    if (dir == 'x'):
        return np.matrix([
            [1.,0.,0.],
            [0., cos(alpha),-sin(alpha)],
            [0., sin(alpha), cos(alpha)]
            ])
    elif (dir == 'y'):
        return np.matrix([
            [cos(alpha), 0., sin(alpha)],
            [0.,1.,0.],
            [-sin(alpha), 0., cos(alpha)]
            ])
    elif (dir == 'z'):
        return np.matrix([
            [cos(alpha),-sin(alpha), 0.],
            [sin(alpha), cos(alpha), 0.],
            [0.,0.,1.]])
    return None

def normalized(v):
    """return the normalized version of a vector
    """
    return v / np.linalg.norm(v)

def cross(v,w=None):
    """cross product of 2 3-vectors
    """
    if (w == None):
        vx,vy,vz = v
        c = [[ 0., -vz,  vy ],
            [  vz,  0., -vx ],
            [ -vy,  vx,  0. ]]
        return np.matrix(c)
    else:
        return np.matrix(np.cross(v.transpose(),w.transpose()).transpose())

def euler_rotate(v,w,theta=1.):
    """general rotation of the vector(s) v about an arbitrary vector w, by an amount theta
    """
    c = cross(w)
    T = scipy.linalg.expm(c*theta)
    return T*v

def intersect(ray,plane):
    """Intersect a ray with a plane mirror
    ray = tuple (position, direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
    in which case the 3ed column is treated as the ray direction
    plane = tuple (position, normal) both 3-vectors
    """
    p,z = ray
    if (z.shape[1] > 1):
        z3 = z[:,2]
    else:
        z3 = z
    q,n = plane
    n = n[:,0]
    M = ( z3 * n.transpose() ) / ( z3.transpose() * n )
    p = p + M * (q - p)
    return (p,z)

def reflect(ray,mirror):
    """Reflect a ray off a mirror
    ray = ray tuple (position,direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
    The vectors are all treated as directions. The position is unchanged
    mirror = mirror tuple (position, normal) both 3-vectors, or a normalized 3-vector
    
    It is implicitly assumed that the ray position is a point on the mirror plane, but this is not checked.
    """
    p,v = ray
    q,n = mirror
    n = n[:,0]
    H = I - 2 * n * n.transpose()
    v = H*v
    return (p,v)

def propagate(ray,distance):
    """Propagate a ray for a distance
    ray = tuple (position, direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
    in which case the 3ed column is treated as the ray direction
    distance = scalar distance
    """
    p, z = ray
    if (z.shape[1] > 1):
        z3 = z[:,2]
    else:
        z3 = z
    p = p + distance*z3
    return(p,z)

## parameters
mirror_pivot_radius = 13.82*mm
"""distance from the center of rotation to the mirror surface"""

alpha = 0.*degrees
"""this is the departure from right angle of the WFS path"""

d01 = 100.*mm
"""distance origin -> m1"""
d12 = 35.*mm
"""distance, m1 -> m2 (meters)"""
d23 = 87.*mm
"""distance, m2 -> focus (meters)"""
plate_scale = 0.31
"""plate scale, mm per arcsecond"""
fov_arcsec = 20
"""field of view, arcseconds on sky"""
fov_mm = fov_arcsec*plate_scale
"""field of view, millimeters"""
f_number = 21.
"""f/number going into WFS"""
pdiam = 1./f_number
"""pupil diameter, radians"""

def calibrate():
    """define the mirrors and nominal angles of the system.
    """
    global s0, mirror1, mirror2, pivot1, pivot2, focalPlane
    
    p1 = origin + z*d01
    pivot1 = p1 + vector((-mirror_pivot_radius,0,0))
    rot1 = rotate('y',135*degrees)
    nab = rot1*np.hstack([x,y,z])
    mirror1 = (p1, nab)
    pivot1 = rot1*(pivot1 - p1) + p1
    
    p2 = mirror1[0] + d12*(-x)
    pivot2 = p2 + vector((-mirror_pivot_radius,0,0))
    rot2 = rotate('x',alpha)*rotate('z',-45*degrees)
    nab = rot2*np.hstack([x,y,z])
    mirror2 = (p2, nab)
    pivot2 = rot2*(pivot2 - p2) + p2
    
    # find the focal plane position and normal
    r0 = (origin,z)
    r0 = intersect(r0,mirror1)
    r0 = reflect(r0,mirror1)
    r0 = intersect(r0,mirror2)
    r0 = reflect(r0,mirror2)
    r0 = propagate(r0,d23)
    focalPlane = r0
    
    # define a coordinate system at the focal plane -
    #  x - perpundicular to table; y - parallel to table = z cross x; z - along ray;
    s0 = np.hstack((x,y,z))
    s0 = rotate('x',90*degrees + alpha)*s0
    
    # propagate a gut ray and a full axis-set (3 orthonormal unit vectors)
    s = np.hstack((x,y,z))
    ray = (origin, s)
    ray = intersect(ray,mirror1)
    ray = reflect(ray,mirror1)
    ray = intersect(ray,mirror2)
    ray = reflect(ray,mirror2)
    ray = intersect(ray,focalPlane)
    
    # figure rotation of original x,y on focal plane coordinates
    r = s0.transpose() # reference coordinates on focal plane
    v = ray[1] # original x and y vectors after reflections
    a = r*v # 3x3 transformation matrix; row&col 3 should be (0,0,1). 2x2 upper block should be the rotation
    theta = np.arctan2(a[1,0],a[0,0]) # this is the rotation about z of the original axis set

def shoot(r,m1=(0.,0.),m2=(0.,0.)):
    """Shoots a ray starting at point r (and telecentric direction z) to the focal plane.
    argument r is a 3-vector
    
    m1 and m2 are optional commands that move the two steering mirrors. the command is given as two
    angles, (ra, rb) in radians. The ra and rb axes are afixed to each mirror and orthogonal
    to the mirror's normal, as defined in the calibration.
    
    returns (position,rotation matrix) = tuple(3-vector,3x3 matrix) denoting the ray as it hit focal plane,
        expressed in focal plane coordinates
    
    The intercept point is the first element of the returned tuple
    The intercept angle is encoded in the z-vector (3ed column) of the second element of the returned tuple
    """
    v1 = mirror1[1] * np.vstack((0,vector(m1)))
    p1 = euler_rotate(mirror1[0] - pivot1,v1) + pivot1
    v2 = mirror2[1] * np.vstack((0,vector(m2)))
    p2 = euler_rotate(mirror2[0] - pivot2,v2) + pivot2
    mirror1_moved = (p1, euler_rotate(mirror1[1],v1))
    mirror2_moved = (p2, euler_rotate(mirror2[1],v2))
    
    ray = (r,np.hstack((x,y,z)))
    ray = intersect(ray,mirror1_moved)
    ray = reflect(ray,mirror1_moved)
    ray = intersect(ray,mirror2_moved)
    ray = reflect(ray,mirror2_moved)
    ray = intersect(ray,focalPlane)
    p = s0.transpose() * (ray[0] - focalPlane[0])
    ax = s0.transpose() * ray[1]
    
    return (p,ax)

def pshoot(m1,m2,relative=False,verbose=False):
    """pea-shoot: do a dead reckoning command of the four
    motors and find out where the beam ends up.
    Input is the 4 motor commands, in encoder counts, given as 2 2-tuples
    corresponding to motors (C,D),(A,B) or (WFS1RX, WFS1RY),(WFS2RY,WFS2RX)
    The input can be relative to the nominal aligned position if the
    keyword relative=True.
    Output is position on the focal plane (x,y) in mm, and
    angle on the focal plane (x,y) in fractions of a pupil diameter.
    """
    m = np.array([m1,m2])
    a = commandsToAngles(m,relative)[0]
    a1 = a[0]
    a2 = a[1]
    p,ax = shoot(origin,a1,a2)
    pr = (p.item(0)/(plate_scale*mm),p.item(1)/(plate_scale*mm))
    cr = (ax[:,2].item(0)/pdiam,ax[:,2].item(1)/pdiam)
    if (verbose):
        print 'focal plane x = '+str(pr[0]*plate_scale)+' mm; '+str(pr[0])+' arcsec'
        print 'focal plane y = '+str(pr[1]*plate_scale)+' mm; '+str(pr[1])+' arcsec'
        print 'pupil shift x = '+str(cr[0])+' %'
        print 'pupil shift y = '+str(cr[0])+' %'
    return (pr,cr)
    
def func(m,px,py):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    px, py is the desized position, in mm on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (px - p.item(0), py - p.item(1),ax[:,2].item(0),ax[:,2].item(1))
    return v

def cfunc(m,cx,cy):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    cx, cy is the desired centering, in radians incident on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (p.item(0),p.item(1), cx - ax[:,2].item(0), cy - ax[:,2].item(1))
    return v

def pcfunc(m,px,py,cx,cy):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    px, py is the desized position, in mm on focal plane
    cx, cy is the desired centering, in radians incident on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (px - p.item(0),py - p.item(1), cx - ax[:,2].item(0), cy - ax[:,2].item(1))
    return v

def Rmatrix():
    """form a 4x4 matrix of focal plane spot position and angle of arrival sensitivity to mirror moves
    """
    r_set = []
    delta = 0.001
    spot0 = shoot(origin)
    
    spot = shoot(origin,(delta,0),(0.,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((spot[0][0:2],spot[1][0:2,2])))
    
    spot = shoot(origin,(0,delta),(0.,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    
    spot = shoot(origin,(0,0),(delta,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    
    spot = shoot(origin,(0,0),(0.,delta))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    r = np.hstack(r_set)/delta
    
    return r,r_set

def pcSolve(p,c):
    pc = np.vstack((vector(p),vector(c)))
    R, r_set = Rmatrix()
    Rinv = np.linalg.inv(R)
    m = Rinv * pc
    m1 = (m.item(0),m.item(1))
    m2 = (m.item(2),m.item(3))
    return (m1,m2)

def show(ray,n=4):
    """display the ray point and its orientation triplet in an easy to read form
    """
    print np.round(np.hstack(ray),n)

def scan(m):
    """scan the p & c pair along +/- 3 x the unit motor commands, m
    m is a 2-tuple of 2-tuples describing mirror1 and mirror 2 motor commands
    """
    ma = np.array(m)
    amp_set = np.arange(-3,3,.5)
    x_set = []
    y_set = []
    cx_set = []
    cy_set = []
    for amp in amp_set:
        m = amp*ma
        m1 = m[0]
        m2 = m[1]
        spot = shoot(origin,m1,m2)
        px = spot[0].item(0)
        py = spot[0].item(1)
        cx = spot[1][:,2].item(0)
        cy = spot[1][:,2].item(1)
        x_set.append(px)
        y_set.append(py)
        cx_set.append(cx)
        cy_set.append(cy)
        
    return x_set,y_set,cx_set,cy_set

calibrate()

def drawPlot(kind):
    """plot the various motion results.
    
    Pointing is shown on the focal plane, in focal plane coordinates.
    Units are meters, and generally shown on a +/- 10mm scale
    
    Centering is shown on the focal plane as an incident angle at the focal plane.
    Units are radians, generally shown on a +/- 10mr scale
    The pupil diameter is ~50mr (=1/f#)
    
    Raw Motion:
    +/- 30 milliradian of motion of each mirror channel in turn.
    
    Linear Solved:
    Computes the sensitivity of p and c to mirror motions
    (see Rmatrix), using deltas of 1 mr mirror commands
    Then 'solves' for the mirror commands using the inverse
    of this matrix as desired position is scanned from
    -3 to +3 mm
    
    Experimental Motion:
    A 'hand waving' argument that non-decentering pointing moves
    are coordinated motions of the mirror motors related by
    a factor of sqrt(2) and axes are independent.
    The mirror1 is scanned +/- 60 mr in each axis, with
    mirror2 following according to the scale factor.
    
    Optimizer:
    A least-squares solution to the motor moves that produce
    non-decentering position moves. The desired positions are
    scanned +/- 5mm. Two additional plots show the mirror
    commands that do this.
    """
    kinds = ['Raw Motion',
             'Linear Solved',
             'Experimental Motions',
             'Optimizer']
    if (not (kind in kinds)):
        print '<drawPlots> ERROR'+kind+' not in '+str(kinds)
        return None
    
    c_title = 'centering ('+str(int(pdiam*1000))+'mr = full pupil)'
    calibrate()
    if (kind == 'Optimizer'):
        plt.figure(kind,figsize=(12,12))
    
        plt.subplot(2,2,1)
        plt.title('position')
        plt.xlabel('focal plane position x, meters')
        plt.ylabel('focal plane position y, meters')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
    
        plt.subplot(2,2,2)
        plt.title(c_title)
        plt.xlabel('centering x, radians')
        plt.ylabel('centering y, radians')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
        
        plt.subplot(2,2,3)
        plt.title('mirror 1')
        plt.xlabel('a command, radians')
        plt.ylabel('b command, radians')
        plt.grid('on')
        
        plt.subplot(2,2,4)
        plt.title('mirror 2')
        plt.xlabel('a command, radians')
        plt.ylabel('b command, radians')
        plt.grid('on')
    
        plt.subplots_adjust(wspace = 0.6)
    
    else:
        plt.figure(kind,figsize=(12,6))
    
        plt.subplot(1,2,1)
        plt.title('position')
        plt.xlabel('focal plane position x, meters')
        plt.ylabel('focal plane position y, meters')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
    
        plt.subplot(1,2,2)
        plt.title(c_title)
        plt.xlabel('centering x, radians')
        plt.ylabel('centering y, radians')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
        
        plt.subplots_adjust(wspace = 0.4)
                
    if (kind == 'Linear Solved'):
        delta = 1*mm
        p = (delta, 0)
        c = (0, 0)
        m1,m2 = pcSolve(p,c)
        px = np.matrix((m1,m2))
        
        p = (0, delta)
        c = (0, 0)
        m1,m2 = pcSolve(p,c)
        py = np.matrix((m1,m2))
        
        x_set,y_set,cx_set,cy_set = scan(px)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(px+py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(px-py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Raw Motion'):
        d = 0.01
        m1 = ((d,0),(0,0))
        m2 = ((0,d),(0,0))
        m3 = ((0,0),(d,0))
        m4 = ((0,0),(0,d))
        
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m2)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m3)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m4)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Experimental Motions'):
        d = 0.02
        
        m1 = ((d,0),(1.414*d,0))
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        
        m1 = ((0,d),(0,-d*.707))
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Optimizer'):
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,0))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')

        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for py in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(0,py))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            p,ax = shoot(origin,m1,m2)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')
        
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,px))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')
        
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,-px))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')

kinds = ['Raw Motion',
         'Linear Solved',
         'Experimental Motions',
         'Optimizer']

doit = 0
if (doit):
    for kind in kinds:
        drawPlot(kind)

def genPTable(limit=5*mm,delta=None):
    """generate a lookup table for pointing motions
    """
    if (delta == None):
        delta = limit*2./10.
    plt.figure('Lookup Table - Pointing',figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    m_set = []
    p_set = []
    for px in np.arange(-limit,limit+.01*mm,delta):
        for py in np.arange(-limit,limit+.01*mm,delta):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,py))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            p_set.append((px,py))
            m_set.append((m1,m2))
            
    p = np.array(p_set)
    m = np.array(m_set)
    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
        
    return p,m

def genCTable(limit=10*mr,delta=None):
    """generate a lookup table for centering motions
    """
    if (delta == None):
        delta = 2.*limit/10.
    plt.figure('Lookup Table - Centering',figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    m_set = []
    c_set = []
    for cx in np.arange(-limit,limit+.01*mr,delta):
        for cy in np.arange(-limit,limit+.01*mr,delta):
            u,nr = scipy.optimize.leastsq(cfunc,(0,0,0,0),(cx,cy))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            c_set.append((cx,cy))
            m_set.append((m1,m2))
            
    c = np.array(c_set)
    m = np.array(m_set)
    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
        
    return c,m

# Load encoder count data - from data taken Dec 3, 2013
motorA = {'name':'WFS2RY','mid':-38766,'scale':12766*2./(2.94*degrees),'limits':[13466,-110346]}
motorB = {'name':'WFS2RX','mid':-32792,'scale':-15000*2./(3.25*degrees),'limits':[18194,-115753]}
motorC = {'name':'WFS1RX','mid':-40696,'scale':15000*2./(3.35*degrees),'limits':[7786,-124997]}
motorD = {'name':'WFS1RY','mid':-23125,'scale':15000*2./(3.37*degrees),'limits':[22551,-94530]}

def anglesToCommands(a):
    """Convert the angles in the list a, given in radians, to load encoder counts.
    Each element of the list is a 2 x 2 array, representing the motor commands
    in the order [[mirror1rot+y, mirror1rot+z], [mirror2rot+y, mirror2rot-z]].
    The function returns a list in the same order, but with angles replaced by
    motor counts.
    """
    if (len(a.shape) == 2):
        al = [a]
    else:
        al = list(a)
    cl = []
    motorList = [motorC,motorD,motorA,motorB]
    for u in al:
        u = u.flatten()
        v = np.zeros((4))
        for k in range(4):
            motor = motorList[k]
            v[k] = int(motor['mid'] + motor['scale']*u[k])
        v = v.reshape((2,2))
        cl.append(v)
    return np.array(cl)

def commandsToAngles(c,relative=False):
    """Convert the motor commands in the list c to mirror angles, delta from nominal, in radians.
    Each element of the list is a 2 x 2 array, representing the motor commands
    in the order [[mirror1rot+y, mirror1rot+z], [mirror2rot+y, mirror2rot-z]].
    The function returns a list in the same order, but with commands replaced with
    angles.
    """
    if (len(c.shape) == 2):
        cl = [c]
    else:
        cl = list(c)
    al = []
    motorList = [motorC,motorD,motorA,motorB]
    for v in cl:
        v = v.flatten()
        u = np.zeros((4))
        for k in range(4):
            motor = motorList[k]
            if (relative):
                u[k] = v[k]/motor['scale']
            else:
                u[k] = (v[k]-motor['mid'])/motor['scale']
        u = u.reshape((2,2))
        al.append(u)
    return np.array(al)    
    
def plotTable(m,title='name me',units='radians'):
    """plot the already generated lookup table for pointing motions
    """
    plt.figure(title,figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('C command, '+units)
    plt.ylabel('D command, '+units)
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('A command, '+units)
    plt.ylabel('B command, '+units)
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
    
    if (units == 'counts'):
        plt.subplot(1,2,1)
        c = np.array(motorC['limits'])
        d = np.array(motorD['limits'])
        x = [ c.min(), c.max() ]
        y = [ d.min(), d.min() ]
        plt.plot(x,y,'k')
        y = [ d.max(), d.max() ]
        plt.plot(x,y,'k')
        y = [ d.min(), d.max() ]
        x = [ c.min(), c.min() ]
        plt.plot(x,y,'k')
        x = [ c.max(), c.max() ]
        plt.plot(x,y,'k')
        
        plt.subplot(1,2,2)
        c = np.array(motorA['limits'])
        d = np.array(motorB['limits'])
        x = [ c.min(), c.max() ]
        y = [ d.min(), d.min() ]
        plt.plot(x,y,'k')
        y = [ d.max(), d.max() ]
        plt.plot(x,y,'k')
        y = [ d.min(), d.max() ]
        x = [ c.min(), c.min() ]
        plt.plot(x,y,'k')
        x = [ c.max(), c.max() ]
        plt.plot(x,y,'k')

def _point(pxa,pya,verbose=False,sendCommand=False):
    """calculate the motor positions, A,B,C,D in load encoder counts,
    required to point to a given field position, pxa, pya, given in arcseconds.

    This is an absolute move. The global p state is set to (pxa,pya)
    """
    global state
    px = pxa*plate_scale*mm
    py = pya*plate_scale*mm
    u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,py))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(pxa,pya),'c':(0,0),'motors':c}
    if (sendCommand):
        command(c)
    if (verbose):
        _verbose(c,m1,m2)
    else:
        return c

def oldpoint(pxa,pya):
    """this is a wrapper intended to be an interface for the Shane keyword system
    to use in scripts coordinated with telescope motion.
    
    It determines the nearest point
    in the database to the requested point, given as
    (east,south)
    telescope moves from the boresight.
    
    The function then returns 3 2-tuples:
    (wfs1rx,wfs1ry),(wfs2rx,wfs2ry),(east,south)
    where
    (wfs1rx,wfs1ry),(wfs2rx,wfs2ry)
    are the motor moves that will
    counter steer the light into the wavefront sensor, and
    (east, south)
    is the actual field point chosen from the database.
    
    Field points (both input and output) are given in arcseconds relative
    to the centerline or boresight position.
    """
    pos,pc = lookup(pxa,pya)
    px,py,cx,cy = pc
    mc = pnc(px,py,cx,cy)

    # Note that the positions are taken out of order.
    # The lookup() function returns the values in
    # 2RY,2RX order, rather than 2RX,2RY.

    return (mc[0,0],mc[0,1]),(mc[1,1],mc[1,0]), (px,py)

def point(pxa,pya,verbose=False):
    """wrapper for interpolated pointing
    """
    n = pya
    e = pxa
    if (verbose):
        print '<point> pointing to north='+str(n)+' east='+str(e)
    try:
        ni,ei,cxi,cyi = fsi.fieldSteer(n,e)
        pxi = ei
        pyi = -ni
    except:
        raise ValueError, 'north,east '+str(n,e)+'outside limits of interpolation'
    mc = pnc(pxi,pyi,cxi,cyi)
    return (mc[0,0],mc[0,1]),(mc[1,1],mc[1,0]), (pxa,pya)    
    
def center(cxa,cya,verbose=False,sendCommand=False):
    """calculate the motor positions, A,B,C,D in load encoder counts,
    required to center to a given pupil position, cxa, cya, given in fractions
    of a pupil diameter

    This is an absolute move. The global c state is set to (cxa,cya)
    """
    global state
    cx = cxa*pdiam
    cy = cya*pdiam
    u,nr = scipy.optimize.leastsq(cfunc,(0,0,0,0),(cx,cy))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(0,0),'c':(cxa,cya),'motors':c}
    if (sendCommand):
        command(c)
    if (verbose):
        _verbose(c,m1,m2)
    else:
        return c

def pnc(pxa,pya,cxa,cya,verbose=False,sendCommand=False):
    """calculate the motor positions, A,B,C,D in load encoder counts,
    required to both point and center to a given
    position, pxa, pya, in arcsec on the focal plane and
    centring, cxa, cya, in fractions of pupil
    """
    global state
    px = pxa*plate_scale*mm
    py = pya*plate_scale*mm
    cx = cxa*pdiam
    cy = cya*pdiam
    u,nr = scipy.optimize.leastsq(pcfunc,(0,0,0,0),(px,py,cx,cy))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(pxa,pya),'c':(cxa,cya),'motors':c}
    if (sendCommand):
        command(c)
    if (verbose):
        _verbose(c,m1,m2)
    else:
        return c

state = {'p':(0,0),'c':(0,0),'motors':pnc(0,0,0,0)}
"""global variable for absolute pointing and centering position"""

def deltaP(dpx,dpy,verbose=False,sendCommand=False):
    """adjust the pointing by a delta amount, dpx, dpy,
    given in arcseconds
    
    This updates the global 'state' which is p=(0,0), c=(0,0)
    initially and reset on an absolute move.
    """
    global state
    px = state['p'][0] + dpx
    py = state['p'][1] + dpy
    cx = state['c'][0]
    cy = state['c'][1]
    c = pnc(px,py,cx,cy,verbose=verbose,sendCommand=sendCommand)
    return c

def deltaC(dcx,dcy,verbose=False,sendCommand=False):
    """adjust the centering by a delta amount, dcx, dxy,
    given in fractions of a pupil
    
    This updates the global 'state' which is p=(0,0), c=(0,0)
    initially and reset on an absolute move.
    """
    global state
    px = state['p'][0]
    py = state['p'][1]
    cx = state['c'][0] + dcx
    cy = state['c'][1] + dcy
    c = pnc(px,py,cx,cy,verbose=verbose,sendCommand=sendCommand)
    return c

def gshow(test=False):
    s = 'gshow -s saomot steer1rxraw steer1ryraw steer2rxraw steer2ryraw'
    if (test == False):
        subprocess.call(s,shell=True)
    _state()

def _state():
    print state
    
def command(c,test=False):
    """send the calculsted motor moves to the motor dispatcher.
    This command 'connects' the pointing and centering calculations to
    the actual motor controllers, for example::
    
        command(point(x,y))
        command(center(u,v))
    
    The order of the c matrix is::
    
        [[ 1Rx, 1Ry],[2rx, 2ry]] = [[C,D],[B,A]]
        
    """
    s = modifyCommand+' -s saomot'
    s = s + ' STEER1RXRAW='+str(int(c[0,0]))
    s = s + ' STEER1RYRAW='+str(int(c[0,1]))
    s = s + ' STEER2RXRAW='+str(int(c[1,1]))
    s = s + ' STEER2RYRAW='+str(int(c[1,0]))
    s = s + ' notify'
    
    if (test==False):
        subprocess.call(s,shell=True)
    
    else:
        print('subprocess.call('+s+'shell=True)')

def _verbose(c,m1,m2):
    print('A: '+motorA['name']+' = '+str(int(c[1,0]))+' ( '+str(m2[0]*1000)+' milliradians)')
    print('B: '+motorB['name']+' = '+str(int(c[1,1]))+' ( '+str(m2[1]*1000)+' milliradians)')
    print('C: '+motorC['name']+' = '+str(int(c[0,0]))+' ( '+str(m1[0]*1000)+' milliradians)')
    print('D: '+motorD['name']+' = '+str(int(c[0,1]))+' ( '+str(m1[1]*1000)+' milliradians)')
    print('---')
    print(motorC['name']+' = '+str(int(c[0,0]))+' ( '+str(m1[0]*1000)+' milliradians)')
    print(motorD['name']+' = '+str(int(c[0,1]))+' ( '+str(m1[1]*1000)+' milliradians)')
    print(motorB['name']+' = '+str(int(c[1,1]))+' ( '+str(m2[1]*1000)+' milliradians)')
    print(motorA['name']+' = '+str(int(c[1,0]))+' ( '+str(m2[0]*1000)+' milliradians)')

#==================
#  Handle commands from the operator

#      --- Tip/Tilt Sensor stage motion ---
def ttMoveTo(n,e,live=False):

    # This calculation should be broken out as a separate function;
    # that function should be used by ttpoint(), and ttMoveTo() should
    # be modified to only set the TTFIELDPOS% keywords in the mako service.

    x0 = -23867. # nominal zero position, boresight centered on pixel[49, 37]
    y0 = 4018.
    #x_scale = 4320.*1.105 # encoder counts / arcsec on sky
    #y_scale = 4360.*.94
    #th = 24.3*np.pi/180.
    # New x_scale, yscale, and th calculated from M13 data taken 2014 Sept - EG
    x_scale = 4271.54
    y_scale = 4256.0
    th = 23.4 *np.pi/180.
    #x = e*np.cos(th) + n*np.sin(th)
    #y = e*np.sin(th) - n*np.cos(th)
    x = -e*np.cos(th) - n*np.sin(th)  # moves are really counter-steers, so go negative of telescope's field steer
    y = -e*np.sin(th) + n*np.cos(th)
    tt_cam_x = np.round(x0 + x_scale*x).astype('int')
    tt_cam_y = np.round(y0 + y_scale*y).astype('int')

    # This really should be using mako.TTFIELDPOS_GO rather than issuing
    # motor commands directly.

    cs = modifyCommand+' -s saomot'
    cs += ' TTCAMXRAW='+str(tt_cam_x)
    cs += ' TTCAMYRAW='+str(tt_cam_y)
    cs2 = modifyCommand+' -s mako TTFIELDPOSX='+str(e)+' TTFIELDPOSY='+str(n)
    if (live):
        subprocess.call(cs,shell=True)
        subprocess.call(cs2,shell=True)
    else:
        print '-- not live, but would send...'
        print cs
        print cs2
    print '<fieldSteer.ttMoveTo> Actually moved to '+str(n)+' arcsec North, '+str(e)+' arcsec East'
    return (tt_cam_x, tt_cam_y)

def ttpoint(n,e):
    """interface for the Shane keyword system to use
    in scripts coordinated with telescope motion.
    
    Input: north and east telescope move, in arcseconds
    Output: TTCAMXRAW and TTCAMYRAW absolute encoder counts
    """
    return ttMoveTo(n,e,live=False)

#      --- Wavefront Sensor PnC mirrors ---
cardinal_pos = {}
cardinal_pos['South'] = (0,7./2.,.035/2.,-0.059/2.)
cardinal_pos['North'] = (2/2.,-7/2.,0.131/2.,-0.0075/2.)
cardinal_pos['Middle'] = (0,0,0.01/2.,-0.03/2.)
cardinal_pos['East'] = (7/2.,1/2.,0.058/2.,0.021/2.)
cardinal_pos['West'] = (-8/2.,0,0,0.008/2.)

def south():
    command(pnc(0,7./2.,.035/2.,-0.059/2.))

def north():
    command(pnc(2/2.,-7/2.,0.131/2.,-0.0075/2.))

def middle():
    command(pnc(0,0,0.01/2.,-0.03/2.))

def east():
    command(pnc(7/2.,1/2.,0.058/2.,0.021/2.))

def west():
    command(pnc(-8/2.,0,0,0.008/2.))

# Establish the other calibrated field steer positions

dset = [
	[1.0,	u'Centered',	0.0,	0.0,	0.005,	-0.015,	''],
	[2.0,	u'E2',		2.0,	0.0,	0.02,	0.0,	''],
	[3.0,	u'E4',		4.0,	0.25,	0.026,	0.025,	''],
	[4.0,	u'E6',		6.0,	0.5,	0.045,	0.055,	''],
	[5.0,	u'E8',		7.79,	1.9,	0.036,	0.0985,	u'0.6 gain'],
	[6.0,	u'E10',		10.0,	2.35,	0.0635,	0.1735,	u'0.5 gain'],
	[7.0,	u'W2',		-2.0,	0.0,	0.01,	-0.01,	''],
	[8.0,	u'W4',		-4.0,	0.0,	0.01,	-0.01,	''],
	[9.0,	u'W6',		-6.0,	0.0,	-0.005,	0.0,	''],
	[10.0,	u'W8',		-8.0,	0.0,	-0.005,	0.0,	''],
	[11.0,	u'S2',		0.0,	2.0,	0.0,	0.0,	''],
	[12.0,	u'S4',		0.0,	4.0,	0.0,	-0.02,	''],
	[14.0,	u'S6',		0.0,	6.2,	0.035,	-0.029,	''],
	[15.0,	u'S8',		0.0,	7.5,	0.065,	-0.035,	u'gain 0.6'],
	[17.0,	u'N2',		0.0,	-2.0,	0.02,	0.0,	''],
	[18.0,	u'N4',		0.0,	-4.0,	0.054,	0.0,	''],
	[19.0,	u'N6',		1.0,	-6.0,	0.12,	0.0,	''],
	[20.0,	u'N8',		2.5,	-8.0,	0.175,	0.0,	''],
	[21.0,	u'N10',		4.5,	-10.0,	0.27,	0.0185,	u'gain 0.5'],
	[23.0,	u'S4,E4',	4.0,	4.0,	0.0,	0.0,	''],
	[24.0,	u'S4,E6',	6.0,	4.0,	0.0,	0.01,	''],
	[25.0,	u'S6,E4',	4.0,	6.0,	0.0,	-0.02,	''],
	[26.0,	u'S6,E6',	6.0,	6.0,	0.0,	-0.02,	''],
	[28.0,	u'S4,W4',	-4.0,	4.0,	0.014,	0.02,	u'gain .5'],
	[29.0,	u'S4,W6',	-6.0,	4.0,	0.018,	0.058,	u'gain .3'],
	[30.0,	u'S6,W4',	-4.0,	6.0,	0.0367,	0.035,	u'gain .3'],
	[31.0,	u'S6,W6',	-6.0,	6.0,	0.0367,	0.077,	u'gain .3'],
	[34.0,	u'N4,W4',	-4.0,	-4.0,	0.02,	0.0,	u'gain .7'],
	[35.0,	u'N4,W6',	-6.0,	-4.0,	0.02,	0.0,	u'gain .7'],
	[36.0,	u'N6,W4',	-4.0,	-6.0,	0.054,	0.0,	u'gain .7'],
	[37.0,	u'N6,W6',	-6.0,	-6.0,	0.04,	0.0,	u'gain .7'],
	[38.0,	u'N6,E4',	4.0,	-3.5,	0.107,	0.04,	u'gain .7'],
	[39.0,	u'N2,E6',	6.0,	-2.0,	0.105,	0.08,	u'gain .5'],
	[40.0,	u'N3.5,E5.5',	5.5,	-3.5,	0.132,	0.07,	u'gain .5'],
	[41.0,	u'S3,E7',	7.0,	-3.0,	0.15,	0.1,	u'gain .4']
]


cardinalSet = ['Middle','North','South','East','West']

def lookup(px,py):
    """Find the nearest point in the data base and return
    its name and actual position
    """
    min_e = 100. # arcsecons. no entry in the database is larger than this
    for pos in cardinalSet:
        p = cardinal_pos[pos]
        e = np.sqrt((px-p[0])**2+(py-p[1])**2)
        if (e < min_e):
            min_e = e
            found_pos = pos
    found_pnc = cardinal_pos[found_pos]
    found_in_dset = False
    for entry in dset:
        pos = entry[1]
        p = (entry[2],entry[3],entry[4],entry[5])
        e = np.sqrt((px-p[0])**2+(py-p[1])**2)
        if (e < min_e):
            found_in_dset = True
            min_e = e
            found_pos = pos
            found_pnc = p
    if (found_in_dset):
        print '<lookup> found best point in data base '+found_pos
    else:
        print '<lookup> found best point in cardinal points '+found_pos
    return found_pos, found_pnc

def moveTo(pos,live=False):
    found = False
    if (pos in cardinalSet):
        pnc_vals = cardinal_pos[pos]
        found = True
    else:
        for k in dset:
            if (k[1] == pos):
                found = True
                pnc_vals = (k[2],k[3],k[4],k[5])
    if (not found):
        print 'Position name '+str(pos)+' not in data set'
        if (__name__ == '__main__'):
            print 'type fieldSteer --list to see a list of the valid moves'
        else:
            print 'type validMoves() to list the valid moves'
        return
    p1,p2,c1,c2 = pnc_vals
    e = p1
    n = -p2
    if live:
        command(pnc(p1,p2,c1,c2))
        cs = modifyCommand+' -s mako FIELDPOSX='+str(e)+' FIELDPOSY='+str(n)
        subprocess.call(cs,shell=True)
    else:
        print '<moveTo> '+str(pnc_vals)
        pnc(p1,p2,c1,c2,verbose=True)
    #print '<fieldSteer.moveTo> Position name is '+str(pos)
    #print '<fieldSteer.moveTo> Actually moved to '+str(n)+' arcsec North, '+str(e)+' arcsec East'

def moveToAbs(n,e,cx=0,cy=0,live=False):
    if live:
        command(pnc(e,-n,cx,cy))
        cs = modifyCommand+' -s mako FIELDPOSX='+str(e)+' FIELDPOSY='+str(n)
        subprocess.call(cs,shell=True)
    else:
        print '<moveToAbs> '+str(n)+' north, '+str(e)+' east'
        pnc(e,-n,cx,cy,verbose=True)
    #print '<fieldSteer.moveTo> Exactly moved to '+str(n)+' arcsec North, '+str(e)+' arcsec East'
        
def validMoves():
    for k in cardinalSet:
        print k
    for k in dset:
        print k[1]
    
# when called from the command line
if (__name__ == '__main__'):
    mode = None
    posName = None
    n = None
    e = None
    cx = 0
    cy = 0
    find = None
    exact_ngs = None
    interp_ngs = None
    action = True
    if (len(sys.argv) < 1):
        print usage_str
        exit()
    for arg in sys.argv:
        if (arg == '-h' or arg == '--help'):
            print usage_str
            exit()
        if (arg == '-l' or arg == '--list'):
            validMoves()
            exit()
        if (arg == '-n'):
            action = False
        if (arg == '-f'):
            find = True
        if (arg == '-e'):
            exact_ngs = True
        if (arg == '-i'):
            interp_ngs = True
        if (arg.startswith('mode')):
            mode = arg.split('=')[1]
        if (arg.startswith('pos')):
            posName = arg.split('=')[1]
        if (arg.startswith('north')):
            n = arg.split('=')[1]
        if (arg.startswith('east')):
            e = arg.split('=')[1]
        if (arg.startswith('cx')):
            cx = arg.split('=')[1]
        if (arg.startswith('cy')):
            cy = arg.split('=')[1]

    if (mode == None):
        print 'invalid options for fieldSteer. Type fieldSteer -h for assistance.'
        exit()
    elif (mode == 'ngs'):
        if find:
            if (n == None or e == None):
                print 'invalid options. north and east need to be specified for -f'
                print 'type fieldSteer -h for assistance'
                exit()
            posName,found_pnc = lookup(float(e),-float(n))
            moveTo(posName,live=action)
            exit()
        if exact_ngs:
            if (n == None or e == None):
                print 'invalid options. north and east need to be specified for -e'
                print 'type fieldSteer -h for assistance'
                exit()
            moveToAbs(float(n),float(e),float(cx),float(cy),live=action)
            exit()
        if interp_ngs:
            if (n == None or e == None):
                print 'invalid options. north and east need to be specified for -i'
                print 'type fieldSteer -h for assistance'
                exit()
            try:
                ni,ei,cxi,cyi = fsi.fieldSteer(n,e)
            except:
                print str((n,e))+' is outside limits for interpolation'
                exit()
            print 'interpolated: '+str((ni,ei,cxi,cyi))
            moveToAbs(ni,ei,cxi,cyi,live=action)
            exit()            
        if (posName == None):
            print 'invalid options for fieldSteer. Type fieldSteer -h for assistance.'
            exit()
        moveTo(posName,live=action)
        exit()
    elif (mode == 'lgs'):
        if (n == None or e == None):
            print 'invalid options. north and east need to be specified for mode=lgs'
            print 'type fieldSteer -h for assistance'
            exit()
        ttMoveTo(float(n),float(e),live=action)
        exit()
    
    exit()

