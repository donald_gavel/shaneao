"""This is a script that uses only the Sci-Measure camera driver to check that
the BigJoe camera is basically working. It will
initialize and then run the camera
for a few frames, saving them to disk as "raw" files. These are then picked up and displayed
with ds9.
"""
file = 'wfscam.py'

import edt
import readfits as fits
import numpy as np
import time
import dimg

def checkcam():
    """Check the camera for basic operation 'Out-of-the-box'
    """
    if ('wfs' in globals()):
        del wfs
        del h
        del u
    
    map = fits.readfits('/home/gavel/cvs/lroot/ShaneAO/unreal/recon/parameterFiles/i_map.fits')
    map = map.reshape((160*160))
    
    wfs = edt.Camera(1,'/home/gavel/WFS_BigJoe.cfg')
    h = edt.Handler(wfs,'saveAsRaw')
    h.configure(['wfs0',2.0],'configSaveAsRaw')
    wfs.handler(h)
    
    wfs.start()
    time.sleep(4)
    wfs.stop()
    
    u = np.fromfile('wfs0_00000001.raw',dtype=np.uint16)
    v = u[map].reshape((160,160)).astype('float')
    dimg.movie([v])

