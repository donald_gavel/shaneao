#
#  shaneao.py
#     top level command language for ShaneAO operations
#
"""ShaneAO module
provides a simple rtc command set at the top level for the operator to use
during on-sky operations.
ShaneAO runs on the real-time computer. Commands to it
are typed on a terminal remotely connected to the real time computer.

Start up the rtc from the home directory on the rtc computer::

    shaneao

See also :py:mod:`RTC Status Display <peeko_client2>`

Commands to shaneao are typed at the terminal. The
command names are exposed at the top level, so there
is no need to type the leading ``shaneao.`` string.
Python demands however that you type the trailing ``()``
string on commands, even if there are no arguments.
"""

import os
import sys
import subprocess
import time
from pprint import pprint as pp
import __main__

default_config = {
    'simulator': False,
    'tweeter': True,
    'woofer': True,
    'wfsCam': True,
    'ttCam': True,
    'heartbeat': True,
    'mode':'16x',
    'use_publisher':False
    }

for arg in sys.argv[1:]:
    if (arg == '-b'):
        break

    if (arg == '--no_tweeter'):
        default_config['tweeter'] = False
        print '<shaneao> running with no tweeter'

    elif (arg == '--cam_only'):
        default_config['tweeter'] = False
        default_config['woofer'] = False

    elif (arg == '--simulator'):
        default_config['simulator'] = True

    elif (arg == '--help'):
        print 'ShaneAO runs the RTCode supervisor'
        print 'Command line arguments are:'
        print '   --help - get this help'
        print '   --no_tweeter - run without loading the tweeter driver (dpoi2)'
        print '   --cam_only - run only the cameras, no DM drivers'
        print '   --simulator - run in simulation mode, with no hardware drivers'
        print 'with no argument: run the code in the default configuration, which is'
        pp(default_config)
        os._exit(1)

    else:
        print 'unrecognized command line argument '+arg
        print 'shaneao --help'
        print '  gives a list of valid arguments'
        os._exit(1)

#--------------------
# Reconcile the importing module's config with the default config defined above.
# The importing module's config overrides default_config.
# The importing module is typically the executable script "shaneao" in .../bin.

config = default_config.copy()

try:
    config.update(__main__.config)
except AttributeError:
    pass

# from now on, only check config[...]
#--------------------

# Before taking any further actions, rearrange stdin, stdout, and stderr
# so that they can be captured for client applications.

if config['simulator']:
    pass
else:
    import shaneaoIO


sys.stdout.write("\x1b]2;ShaneAO\x07")

def dpioSanityCheck():
    dmesg = subprocess.Popen(('dmesg',),stdout=subprocess.PIPE)
    stdout,stderr = dmesg.communicate()
    lines = stdout.split('\n')
    lines.reverse()

    for line in lines:
        if 'SW-IOMMU' in line:
            return False
        if 'Requested' in line:
            return True


RTComputer = 'real.ucolick.org'
thisMachine = os.uname()[1]

if (thisMachine == RTComputer):
    import dpio2

    attempts = 0
    max_attempts = 100

    while True:
        tweeter = dpio2.Output(1,2048)
        tweeter.write([0,]*1024)

        if dpioSanityCheck() == True:
            break
        else:
            attempts += 1
            if attempts > max_attempts:
                print "Unable to successfully initialize dpio2 output after %d attempts." % (attempts)
                print "It may be necessary to reboot the host computer."
                os._exit(1)

            if attempts % 20 == 0:
                print "Attempting to initialize dpio2... (%d of %d attempts)" % (attempts, max_attempts)

            del tweeter
            time.sleep(0.01)

    print "Successfully initialized the dpio2 output after %d retries." % (attempts)
    mems_ok = True

if config['simulator']:
    import dpio2
    tweeter = dpio2.Output(1,2048)
    tweeter.write([0,]*1024)
    mems_ok = True

import rtc
import edt
import numpy as np
import atexit
import pydoc
import readfits as fits
import pyfits
import threading

home = os.environ['HOME']
lroot = os.environ['LROOT']

if (thisMachine == RTComputer) and (config['simulator']):
    raise ValueError, 'cannot run in simulator mode on '+thisMachine

if (thisMachine != RTComputer) and (config['simulator'] == False):
	print '<shaneao> cannot run in real mode on '+thisMachine+' which is not designated as a real-time machine'
	print 'continuing on in simulation mode in case this module is being imported by the sphinx documentation generator'
	config['simulator'] = True

global self
self = rtc.rtc(config['mode'])
#self.rate = 'unknown'  # rate is the wfs camera rate
#self.ttrate = 'unknown' # ttrate is the tt camera rate
global wfs
wfs = None

default_state = {'camera': 'stopped',
                 'rate': 'unknown',
                 'ttcamera': 'stopped',
                 'ttrate': 'unknown',
                 'mems': 'unknown',
                 'mems_ok': mems_ok,
                 'heartbeat': False,
                 'woofer': 'off',
                 'wrate': 'none',
                 'frozen': False,
                 'rtc_so':'rtc2.so',
                 'handler':'c_recon',
                }

if config['simulator']:
    state = default_state.copy()
else:
    import ktl
    import shaneaoKeyword

    state = shaneaoKeyword.StatusDictionary()
    state.update(default_state)

    if thisMachine == RTComputer:
        # Once the dpio2.Output instance has been instantiated,
        # it is safe to power up the MEMS device. That instantiation
        # occurred at the beginning of this module; it is thus
        # safe to initiate power heartbeats.
        #
        # Note that the power isn't actually turned on until the
        # saomot.MEMSPOWER keyword is set to 'enable'.

        ktl.write('tweetbeat', 'START', True)

if config['wfsCam']:
    configFile = os.path.join(lroot,'data','ShaneAO','WFS_BigJoe.cfg')
    wfs = edt.Camera(1,configFile)
    print '<shaneao> created wfs: ',wfs
    if config['simulator']:
        h = edt.Handler(wfs,state['handler'])
    else:
        h = edt.Handler(wfs,state['handler'],state['rtc_so'])
    wfs.handler(h)

if config['ttCam']:
    configFile = os.path.join(lroot,'data','ShaneAO','TipTilt_LittleJoe.cfg')
    tt = edt.Camera(0,configFile)
    print '<shaneao> created tt: ',tt
    if config['simulator']:
        h = edt.Handler(wfs,'c_tipTiltRecon')
    else:
        h = edt.Handler(wfs,'c_tipTiltRecon',state['rtc_so'])
    tt.handler(h)

if config['tweeter']:
    print "<shaneao> dpio2.Output = "+str(tweeter)
    tweeter.write([0,]*1024)
    # Once the dpio2 device is initialized as an output device, the card
    # should no longer be putting out noise on its outputs. It is now safe
    # to enable power to the driver electronics, and should remain so until
    # the control host is reset or power-cycled.
    rtc.rtc2.set_dpio2_output(tweeter)
else:
    rtc.rtc2.set_dpio2_output(None)

if (config['woofer'] and not config['simulator']):
    woofer = rtc.woofer_connect()
    #woofer.peek = rtc.rtc2.peek # allows a peek into the state vector to do uplink tip/tilt
    state['wrate'] = woofer.rate

if (config['simulator']):
    import sauce
    if config['wfsCam']:
        wfs.set_callback(sauce.cb)
    if config['ttCam']:
        tt.set_callback(sauce.ttcb)
    if config['tweeter']:
        #import dpio2
        sauce.buffer_peek = tweeter.peek()
        tweeter.set_callback(sauce.update_tweeter)
    if config['woofer']:
        woofer = rtc.woofer_connect('simulated')
        woofer.connect()
        woofer.set_callback(sauce.update_woofer)
        #woofer.peek = rtc.rtc2.peek
    __main__.sauce = sauce


pydir = os.path.join(lroot,'lib','python')
scripts = os.path.join(pydir,'scripts.py')
__main__.scripts = scripts

def load(filename):
    """load a script file into the main namespace"""
    execfile(filename, __main__.__dict__)

__main__.load = load

#-----------

helper = {
    'camstart':'start the camera',
    'camstop':'stop the camera',
    'ttcamstart':'start the tt camera',
    'ttcamstop':'stop the tt camera',
    'mems':'turn the MEMS (tweeter) power on or off',
    'open':'open the loop',
    'close':'close the loop',
    'status':'print the loop status',
    'gain':'set the gain',
    'bleed':'set the integrator bleeds',
    'rate':'set the wfs frame rate',
    'ttrate':'set the tt frame rate',
    'refcent':'take the reference centroids',
    'zero':'send the DMs to zero-point (a0)',
    'estop':'emergency stop - opens loops and turns off MEMS power',
    'peek':'peek at a variable in the real-time code',
    'test':'run a regression test',
    'configq':'show the software configuration',
    'dark':'take a camera background image',
    'ttdark':'take a tt camera background image',
    'help':'get quick help'
}

def crib(item=None):
    """crib is a quick help listing:
    crib() lists the whole list of ShaneAO commands
    or crib(item) to get help on a particular item
    """
    bling = '=========================================='
    if (item == None):
        print bling
        for k in sorted(helper.keys()):
            print k+' -- '+ helper[k]
        crib(crib)
        print bling
        return
    #print item.__doc__
    #print pydoc.render_doc(item,title='%s')
    object,name = pydoc.resolve(item,0)
    text = pydoc.text.document(object,name)
    print text

__main__.crib = crib
__main__.help = crib

def camstart():
    """Start the camera
    """
    global wfs

    if (not config['wfsCam']):
        print '<camstart> not configured for wfs camera'
        return
    if (wfs == None):
        configFile = os.path.join(lroot,'data','ShaneAO','WFS_BigJoe.cfg')
        wfs = edt.Camera(1,configFile)
        print '<camstart> created wfs: ',wfs
        h = edt.Handler(wfs,state['handler'],state['rtc_so'])
        wfs.handler(h)
        if (config['simulator']):
            wfs.set_callback(sauce.cb)

    wfs.start()
    state['camera'] = 'running'

__main__.camstart = camstart
__main__.wfscamstart = camstart

def ttcamstart():
    """Start the tt camera
    """
    global tt

    if (not config['ttCam']):
        print '<ttcamstart> not configured for tt camera'
        return
    if (tt == None):
        configFile = os.path.join(lroot,'data','ShaneAO','TipTilt_LittleJoe.cfg')
        tt = edt.Camera(0,configFile)
        print '<ttcamstart> created tt: ',tt
        h = edt.Handler(tt,'c_tipTiltRecon',state['rtc_so'])
        tt.handler(h)

    tt.start()
    state['ttcamera'] = 'running'

__main__.ttcamstart = ttcamstart

def camstop():
    """Stop the camera
    """
    global wfs
    if (wfs == None):
        return 'ok, but camera was never started'
    wfs.stop()
    state['camera'] = 'stopped'

__main__.camstop = camstop
__main__.wfscamstop = camstop

def ttcamstop():
    """Stop the camera
    """
    global tt
    if (tt == None):
        return 'ok, but tt camera was never started'
    tt.stop()
    state['ttcamera'] = 'stopped'

__main__.ttcamstop = ttcamstop

def heartbeat_check(cycle=False):

    if thisMachine == RTComputer:
        try:
            pulses = ktl.read ('tweetbeat', 'PULSES', binary=True)
        except:
            alive = False
        else:
            if pulses != 0:
                alive = True
    else:
        alive = True

    state['heartbeat'] = alive

    return alive

__main__.heartbeat = heartbeat_check
__main__.hb = heartbeat_check

def mems(power=None):
    """Set or query the MEMS (tweeter) power state
    """

    if (power == None):
        return state['mems']

    power = power.lower ()
    if (power == True or power == 'on'):
        if (not state['mems_ok']):
            mems('off')
            print 'cannot power MEMS in SW-IOMMU error state'
            return state['mems']
        hb = heartbeat_check()
        if (not hb):
            print 'cannot power MEMS without a heartbeat'
            return state['mems']

        if (config['heartbeat'] and thisMachine == RTComputer):
            ktl.write('saomot', 'MEMSPOWER', 'enable', wait=False)
        state['mems'] = 'on'

    else:
        if (config['heartbeat'] and thisMachine == RTComputer):
            ktl.write('saomot', 'MEMSPOWER', 'disable', wait=False)

        state['mems'] = 'off'

    return state['mems']

__main__.mems = mems

mems('off')

def dmesg():
    """Retrieve the last DPIO2 related message from the kernel message log.
    If it indicates a kernel memory problem, turn off the mems."""

    p1 = subprocess.Popen(['dmesg'],stdout=subprocess.PIPE)
    lines = p1.communicate()[0].split('\n')
    k = len(lines)-1
    while (k >= 0):
        if ('DPIO2: Requested IRQ' in lines[k]): break
        if ('Out of SW-IOMMU space' in lines[k]):
            mems('off')
            break
        k -= 1
    return lines[k]

__main__.dmesg = dmesg

def mode(aMode=None):
    """sets or requests the mode
    no arg returns the current mode
    arg = '?' returns the list of valid modes
    """
    global self, wfs_flat_name
    valid_modes = ['16x','8x','16xLGS','8xLGS']
    if (aMode == None):
        return self.mode
    if (aMode == '?'):
        print 'valid modes are: '+str(valid_modes)
        print 'current mode is:'+self.mode
        return None
    if (aMode in valid_modes):
        state.update(default_state)
        dataServer.pause = True
        woof('off')
        mems('off')
        self = rtc.rtc(aMode)
        the_wfs_flat[:] = self.wfs_flat
        wfs_flat_name = 'Default '+self.mode+' '+self.filename_dict['wfs_flat']
        open()
        if (config['simulator']):
            sauce.set_lenslet(aMode.replace('LGS',''))
        dataServer.sub = rtc.rtc2.peek('wfs_subaps')
        dataServer.inten = rtc.rtc2.peek('inten')
        dataServer.rtc_instance = self
        dataServer.pause = False
        if ('defaults' in dir(__main__)):
            __main__.defaults()
        #if (self.mode.endswith('LGS')): # initialize the laser uplink connection
        #    print '<shaneao.mode> initializing the laser uplink connection'
        #    woofer.init_laser_uplink()
        #else:
        #    woofer.drop_laser_uplink()
        return 'changed to mode '+self.mode
    else:
        raise ValueError, "invalid mode '%s'" % (str(aMode))
    return None

__main__.mode = mode

def config_query():
    """List the configuration of the supervisory software
    """
    return config

__main__.configq = config_query
__main__.config_query = config_query

def status():
    """print a brief report of the state of the system
    """
    s = self.status()
    state.update(s)
    return state.copy()

dum = status()
__main__.status = status

def _state(tag=None):
    """pretty-print the state"""
    status()
    if (tag == None):
        pp(state.copy())
        return None
    else:
        return state[tag]

__main__.state = _state

def _command(theCommand):
    if (theCommand.startswith('command:')):
        exec theCommand.replace('command:','print',1)
    else:
        raise ValueError,'ill formed command: "'+theCommand+'"'

def woof(woof_state=None):
    """turn the woofer on or off. This begins (or stops) a background process
    that regularly sends commands to the woofer.
    """
    if (woof_state == None):
        if (woofer.thread.alive):
            state['woofer'] = 'on'
        else:
            state['woofer'] = 'off'
        return state['woofer']

    woof_state = woof_state.lower()
    if (woof_state == 'on'):
        try:
            woofer.stop_thread()
        except:
            pass
        woofer.start_thread()
        state['woofer'] = 'on'
    elif (woof_state == 'off'):
        try:
            woofer.stop_thread()
        except:
            pass
        state['woofer'] = 'off'
    else:
        raise ValueError, "argument must be 'on' or 'off'"
    return state['woofer']

__main__.woof = woof
__main__.woofer = woof

def wrate(woofer_update_rate=None):
    """set the woofer update rate, in Hz. currently, the rate can be no
    more than 200 Hz
    """
    if (woofer_update_rate == None):
        return woofer.rate
    if ((woofer_update_rate < 0) or (woofer_update_rate > 200)):
        raise ValueError, 'woofer rate must be between 0 and 200 Hz'
    woofer.set_rate(woofer_update_rate)
    state['wrate'] = woofer_update_rate

__main__.wrate = wrate

if (config['woofer']):
    if (config['simulator']):
        wrate(0.5)
    woof('off')

helper['offload'] = 'turn on or off telescope guide offloading'
def offload(arg=None,interval = None,threshold = None):
    """begin offloading the excess woofer tip and tilt to the telescope
    """
    valid = ['on','off',True,False]
    if (arg == None):
        state['offloading'] = woofer.offloading
        return woofer.offloading

    try:
        arg = arg.lower ()
    except AttributeError:
        pass

    if (arg not in valid):
        print 'offload argument must be one of '+str(valid)
    if (arg == 'on'): arg = True
    if (arg == 'off'): arg = False
    if (interval != None):
        if (interval < 1):
            raise ValueError, 'offload interval must be greater than one second'
            return
        woofer.offload_delta_t = interval
    if (threshold != None):
        if (threshold < 1):
            raise ValueError, 'offload threshold must be greater than one arcsecond'
            return
        woofer.offload_tipTilt_threshold = threshold
    woofer.set_offload(arg)
    state['offloading'] = woofer.offloading
    return woofer.offloading

__main__.offload = offload

def gain(g=None,wg=None):
    """set or request the gain
    where g is the gain (I recommend this be positive, for stability reasons)
    NOTE: if the loop is open, g > 0 will close the loop!
    """
    if (g == None and wg == None):
        gaind = { 'gain':(self.gain,self.savedGain),
                 'woofer gain':(self.wgain,self.saved_wGain),
                 'alpha':self.alpha }
        if (self.mode.endswith('LGS')):
            gaind['tip tilt gain']=(self.tipTilt_rgain,self.saved_ttgain)
            gaind['wfs switch']=self.wfs_switch
            gaind['uplink gain'] = (self.ugain,self.saved_uGain)
        print '---(gain, saved gain)---'
        pp(gaind)
        return
    if (wg==None):
        wg = g
    #wg = np.clip(wg,0,0.2) # this is to account for the bad woofer actuator
    self.set_gain(float(g),float(wg))
    if (state['frozen']):
        state['frozen'] = False
    if (self.loop == 'open'):
        return 'gain = '+str(self.gain)+', '+str(self.wgain)+' ; '+'savedGain = '+str(self.savedGain)+', '+str(self.saved_wGain)
    return 'gain = '+ str(self.gain)+', '+str(self.wgain)

__main__.gain = gain

def bleed(b=None,w='all'):
    """set or request the integrator bleed values
    bleed(b,'tweeter') sets the tweeter integrator bleeds to b.
    b can be a scalar, to set all the bleeds to the same value, or a list.
    Similarly for bleed(b,'woofer') and bleed(b,'all'). The default is 'all'.

    Querys are of the form bleed('tweeter'), bleed('woofer'), bleed('all'),
    or bleed(), which the same as bleed('all')
    """
    _bleed = rtc.rtc2.peek('integrator_bleeds').copy()
    if (isinstance(b,str) and w != 'all'):
        return bleed(b=w,w=b)
    if (isinstance(b,str)):
        w = b
        b = None
    if (b == None):
        if (w == 'all' or w == '?'):
            return (bleed('tweeter'),bleed('woofer'))
        if (w == 'tweeter'):
            return _bleed[0:1024].reshape((32,32))
        if (w == 'woofer'):
            return _bleed[1024:1024+52]
    else:
        if (w == 'all'):
            if (isinstance(b,tuple)):
                #if (isinstance(b[0],np.array)):
                #    bb=np.hstack([b[0].flatten(),b[1]]).flatten()
                #    _bleed[0:1024+52] = bb
                #else:
                bleed('tweeter',b[0])
                bleed('woofer',b[1])
                return
            else:
                _bleed[0:1024+52] = b
        if (w == 'tweeter'):
            if (type(b) is np.ndarray):
                b = b.flatten()
            _bleed[0:1024] = b
            if (isinstance(b,np.ndarray)):
                print 'b shape = ' + str(b.shape)
                state['tweeter_bleed'] = b.reshape((32,32))[12,12]
            else:
                state['tweeter_bleed'] = b
        if (w == 'woofer'):
            _bleed[1024:1024+52] = b
            if (isinstance(b,np.ndarray)):
                print 'b shape = ' + str(b.shape)
                state['woofer_bleed'] = b[0]
            else:
                state['woofer_bleed'] = b
        rtc.rtc2.load('integrator_bleeds',_bleed.astype('double'))
        return

__main__.bleed = bleed

def alpha(al=None):
    """set alpha, the filter time constant.
    alpha must be between 0 and 1,
    where 0 means immediate response, i.e. a time constant of one step
    and 1 means no response, i.e. a time constant of infinity.
    The filter behavior is: v = alpha*v + (1-alpha)*u
    """
    if (al == None):
        return self.alpha
    if ((al < 0) or (al > 1)):
        return '<shaneao.alpha> ERROR alpha must be between 0 and 1'
    self.set_alpha(al)

__main__.alpha = alpha

def uplink_gain(g = None):
    """set or request the uplink gain when in LGS mode
    where g is the gain (I recommend this be positive, for stability reasons)
    NOTE: if the loop is open, g > 0 will close the loop!
    This does not affect the state of the downlink loop

    WARNING: This function is not usually invoked, as the shaneao script
    first loads this module, and then runs execfile() on scripts.py, which
    defines its own uplink_gain() function.
    """
    if (g == None):
        return 'gain = '+str(self.ugain)
    self.set_uplink_gain(float(g))
    if (self.uplink_loop == 'open'):
        print 'uplink loop open'
        return
    return 'uplink loop open'

__main__.uplink_gain = uplink_gain

def close_uplink():
	"""Close the uplink tip/tilt loop, using the saved uplink gain
	"""
	uplink_gain(self.saved_uGain)

__main__.close_uplink = close_uplink

def tt_only(ttgain):
    """Close only using the tip/tilt sensor (LGS mode only)
    This command takes one argument, the tip/tilt gain
    """
    self.tt_only(ttgain)

__main__.tt_only = tt_only
__main__.tt_gain = tt_only

def tt_close():
    """Close the tip/tilt only loop, using the previous
    tip/tilt gain
    """
    tt_only(self.saved_ttgain)

__main__.tt_close = tt_close

def tt_open():
    tt_only(0)

__main__.tt_open = tt_open

def rate(r=None):
    """set or get the wavefront sensor frame rate
    where r = a valid frame rate
    type rate(0) to see a list of valid rates
    """
    rates = [50,100,250,500,700,1000,1300,1500]
    seq_list = [0,0,0,1,2,3,4,5]
    if (r == None):
        if (self.rate == 'unknown'):
            rate(0)
        return 'rate = '+ str(self.rate)
    elif (r == 0 or r == '?'):
        try:
            resp1 = wfs.serial('@RCL?')
            print 'camera says: '+resp1
            resp2 = wfs.serial('@REP?')
            print '             '+resp2
            if (resp1 == "@RCL! 0; '0 - 273 Hz"):
                if (resp2 == '@REP! $001EDD'):
                    self.rate = 50
                elif (resp2 == '@REP! $000B55'):
                    self.rate = 100
                elif (resp2 == '@REP! $000000'):
                    self.rate = 250
            elif (resp1 == "@RCL! 1; '1 - 475 Hz"):
                self.rate = 500
            elif (resp1 == "@RCL! 2; '2 - 659 Hz"):
                self.rate = 700
            elif (resp1 == "@RCL! 3; '3 - 1089 Hz"):
                self.rate = 1000
            elif (resp1 == "@RCL! 4; '4 - 1345 Hz"):
                self.rate = 1300
            elif (resp1 == "@RCL! 5; '5 - 1524 Hz"):
                self.rate = 1500
        except:
            print '<shaneao.rate> WARNING no or bad response from camera'
        state['rate'] = self.rate
        print 'current rate is '+str(self.rate)
        return 'valid rates: '+str(rates)
    elif (r in rates):
        seq_no = seq_list[rates.index(r)]
        try:
            resp = wfs.serial('@RCL '+str(seq_no))
            print '<shaneao.rate> response from camera = '+resp
        except:
            print '<shaneao.rate> ERROR setting rate'
            return 'rate remains at '+str(self.rate)
        if (r == 50):
            resp = wfs.serial('@REP 7901')
            print '<shaneao.rate> response from camera = '+resp
        if (r == 100):
            resp = wfs.serial('@REP 2901')
            print '<shaneao.rate> response from camera = '+resp
        self.rate = r
        state['rate'] = r

        if wfs.pause == True:
            restart = False
        else:
            restart = True

        reset(restart)

        return 'rate set to '+str(r)
    else:
        return 'invalid rate '+str(r)+ '. Select from '+str(rates)

__main__.rate = rate
__main__.wfsrate = rate

def ttrate(r=None):
    """set or get the Tip Tilt sensor frame rate
    where r = a valid frame rate
    type rate(0) to see a list of valid rates
    """
    rates = [40,125,250,500,1000]
    seq_list = [3,2,1,1,0]
    if (r == None):
        if (self.ttrate == 'unknown'):
            ttrate(0)
        return 'ttrate = '+ str(self.ttrate)
    elif (r == 0 or r == '?'):
        try:
            resp1 = tt.serial('@RCL?')
            print 'camera says: '+resp1
            resp2 = tt.serial('@REP?')
            print '             '+resp2
            if (resp1 == "@RCL! 3; '40 a"):
                self.ttrate = 40
            elif (resp1 == "@RCL! 2; '125 a"):
                self.ttrate = 125
            elif (resp1 == "@RCL! 1; '500 a"):
                if (resp2 == '@REP! $000000'):
                    self.ttrate = 500
                elif (resp2 == '@REP! $000015'):
                    self.ttrate = 250
                else:
                    self.ttrate = '250?'
            elif (resp1 == "@RCL! 0; '1000 a"):
                self.ttrate = 1000
        except:
            print '<shaneao.ttrate> WARNING no or bad response from camera'
        state['ttrate'] = self.ttrate
        print 'current rate is '+str(self.ttrate)
        return 'valid rates: '+str(rates)
    elif (r in rates):
        seq_no = seq_list[rates.index(r)]
        try:
            resp = tt.serial('@RCL '+str(seq_no))
        except:
            print '<shaneao.ttrate> ERROR setting ttrate'
            return 'ttrate remains at '+str(self.ttrate)
        print '<shaneao.ttrate> response from camera = '+resp
        if (r == 250):
            resp = tt.serial('@REP 21')
            print '<shaneao.ttrate> response from camera ='+resp
        self.ttrate = r
        state['ttrate'] = r

        if tt.pause == True:
            restart = False
        else:
            restart = True

        ttreset(restart)

        return 'ttrate set to '+str(r)
    else:
        return 'invalid ttrate '+str(r)+ '. Select from '+str(rates)

__main__.ttrate = ttrate

def open():
    """open the AO loop
    """
    self.open_loop()
    return 'loop open'

__main__.open = open

def close():
    """close the AO loop
    """
    if (state['frozen']):
        print 'restoring previous bleed values from prior to the freeze'
        thaw()
        return 'loop closed'
    self.close_loop()
    return 'loop closed'

__main__.close = close

def freeze():
    """freeze the DMs in their present positions.
    This does a special thing with the bleeds. First it saves the present
    bleeds (integrator leak factors), then it sets all the bleeds to 1, then opens the loop.
    This effectively freezes the DMs to their present values.
    Use the 'resume' command to restore both the bleeds and the gain and closed-loop status.
    Just 'close' alone will not reset the bleeds and may drive the loop unstable.
    """
    if (state['frozen']):
        print 'already frozen'
        return
    freeze.bleed = bleed()
    bleed(1)
    open()
    state['frozen'] = True

freeze.bleed = 1
__main__.freeze = freeze

def thaw():
    """Resume closed loop from a frozen position.
    It restores the original bleeds before closing the loop

    This function is only ever invoked via scripts.thaw(), which turns
    around and opens the loop right after calling this function. If you
    just open the loop after a freeze, the integrator gets left at 1.0;
    by closing the loop first, the bleed gets restored to its original
    value.
    """
    if (state['frozen'] == False):
        print 'not frozen'
        return
    bleed(freeze.bleed)
    state['frozen'] = False
    close()


# WFS mapping
parameter_directory = os.path.join(home,'parameterFiles')
map_file = 'i_map.fits' # real cam data needs to be deinterlaced
hdulist = pyfits.open(os.path.join(parameter_directory,map_file))
i_map = hdulist[0].data.reshape((160*160))
hdulist.close()

helper['dark'] = 'take a wfs camera background image'
def dark(z = None, nframes = 100):
	"""Take a wfs camera dark

	dark() averages the wfs camera readings for 100 frames and sets the background to that average
	dark(nframes=nframes) averages for the given number of frames (e.g. dark(nframes=500))
	dark(0) sets the camera background to zero
	"""	
	if (z == None):
		pass
	elif (z == 0):
		d = rtc.rtc2.peek('wfs_background')
		bg = np.zeros(d.shape)
		self.open_loop()
		time.sleep(.1)
		rtc.rtc2.load('wfs_background',bg.astype('double'))
		time.sleep(.1)
		return
	else:
		print 'dark argument invalid'
		crib(dark)
		return

	if (self.rate == 'unknown'):
		dt = 0.01
	else:
		dt = 1.1/self.rate

	dark(0)
	d = rtc.rtc2.peek('wfs')
	i_map_sub = peek('i_map')

	bg = np.zeros(d.shape)
	print 'averaging...'+'estimate completion in '+str(nframes*dt)+'seconds'
	for k in range(nframes):
		bg = bg + d.copy()/float(nframes)
		time.sleep(dt)

	the_wfs_dark[:] = bg[i_map].reshape((160,160)) # save in a global
	bg_subs = bg[i_map_sub]
	rtc.rtc2.load('wfs_background',bg_subs.astype('double'))
	print 'dark done'
	return

__main__.dark = dark
__main__.wfsdark = dark

helper['ttdark'] = 'take a tt camera background image'
def ttdark(z = None, nframes = 100):
	"""Take a tip/tilt camera dark

	ttdark() averages the wfs camera readings for 100 frames and sets the background to that average
	ttdark(nframes=nframes) averages for the given number of frames (e.g. dark(nframes=500))
	ttdark(0) sets the camera background to zero
	"""	
	if (z == None):
		pass
	elif (z == 0):
		d = rtc.rtc2.peek('tt_background')
		bg = np.zeros(d.shape)
		self.open_loop()
		time.sleep(.1)
		rtc.rtc2.load('tt_background',bg.astype('double'))
		time.sleep(.1)
		return
	else:
		print 'ttdark argument invalid'
		crib(ttdark)
		return

	if (self.ttrate == 'unknown'):
		dt = 0.01
	else:
		dt = 1.1/self.ttrate

	ttdark(0)
	d = rtc.rtc2.peek('tts')

	bg = np.zeros(d.shape)
	print 'averaging...'+'estimate completion in '+str(nframes*dt)+'seconds'
	for k in range(nframes):
		bg = bg + d.copy()/float(nframes)
		time.sleep(dt)

	the_tts_dark[:] = bg.reshape((80,80)) # save in a global
	rtc.rtc2.load('tt_background',bg.astype('double'))
	print 'ttdark done'
	return

__main__.ttdark = ttdark

helper['wfs_flat'] = 'load in a new WFS flat field'
wfs_flat_name = 'Default '+self.mode+' '+self.filename_dict['wfs_flat']

def wfs_flat(ff=None,name=None):
	"""Load in a new flat field
	
	wfs_flat() returns the flat field array
	wfs_flat('?') returns the name of the last flat field file
	wfs_flat(filename) argument is a string; loads new flat field from a file
	wfs_flat(data,[name=<name>]) argument is an array; loads new flat field from data, with optional name
	"""
	global wfs_flat_name, u_map

	u_map = fits.readfits(os.path.join(parameter_directory,'u_map_subaps_'+self.mode.replace('LGS','')+'.fits'))
	n = 160
	if (ff == None):
		return the_wfs_flat.copy()
	elif (ff == '?'):
		return wfs_flat_name
	elif ((type(ff) is np.ndarray)):
		if (ff.shape == (n,n)):
			the_wfs_flat[:] = ff # save in a global
			flat_u = ff.reshape((n*n))[u_map]
			rtc.rtc2.load('wfs_flat',flat_u.astype('double'))
			if (name != None):
				wfs_flat_name = str(name)
			else:
				wfs_flat_name = 'wfs_flat(data)'
			return
		else:
			raise ValueError, 'argument array must be 160 x 160'
	elif (isinstance(ff,str)):
		fileName = ff
		if (os.path.exists(fileName)):
			flat = fits.readfits(fileName)
			the_wfs_flat[:] = flat # save in a global
			flat_u = flat.reshape((n*n))[u_map]
			rtc.rtc2.load('wfs_flat',flat_u.astype('double'))
			wfs_flat_name = fileName
			return
		else:
			raise IOError, 'cannot open file '+str(fileName)
	else:
		raise ValueError, 'argument must be a string or numpy array'

__main__.wfs_flat = wfs_flat

state['cent'] = 'unknown'
helper['cent'] = 'Set the centroider type'
def cent(kind=None):
    """Set the centroider type.
    cent('?') lists the valid types of centroiders
    cent() tells the current centroider
    """
    if (kind == None):
        return state['cent']

    kind = kind.upper ()
    valid_kinds = ('CM','COG','QUAD','BIN','BINQUAD')

    if (kind == '?'):
        return valid_kinds
    if (kind not in valid_kinds):
        raise ValueError, "cent argument '%s' must be one of: %s" % (kind, str(valid_kinds))

    if kind == 'CM':
        kind = 'COG'
    elif kind == 'BIN':
        kind = 'BINQUAD'

    self.set_centWts(kind)
    s = self.status()
    state.update(s)

__main__.cent = cent


state['ttcent'] = 'unknown'
helper['ttcent'] = 'Set the TT centroider type'
def ttcent(kind=None):
    """Set the TT centroider type.
    ttcent('?') lists the valid types of centroiders
    ttcent() returns the current centroider
    """
    if (kind == None):
        return state['ttcent']

    kind = kind.upper ()
    valid_kinds = ('CM','COG','QUAD')

    if (kind == '?'):
        return valid_kinds
    if (kind not in valid_kinds):
        raise ValueError('ttcent argument must be one of '+str(valid_kinds))

    if kind == 'CM':
        kind = 'COG'

    self.set_tt_centWts(algo=kind)
    s = self.status()
    state.update(s)

__main__.ttcent = ttcent


helper['centReg'] = 'Set the centroider regularization numbers'
def centReg(vals=None):
	"""Set the centroider regularization numbers.
	This is a tuple of 2 numbers: (reg for Hartmann centroider, reg for Tip/Tilt centroider)
	centReg() tells the current setting.

	Set the regularization to be the standard deviation of the
	total background noise in the intensity-weighted subaperture
	= sqrt(# wighted pixels)*pixel noise
	"""
	if (vals == None):
		return rtc.rtc2.peek('centReg').copy()
	else:
		if (len(vals) != 2):
			raise ValueError('argument to centReg must be a 2-tuple')
		self.set_reg(np.array(vals))

__main__.centReg = centReg

def refcent(z = None):
    """Take the reference centroids

    refcent() averages the present centroids for one second and sets the refcents to that average
    refcent(0) sets the reference centoids to zero
    refcent(filename) loads new refcent from a file
    refcent(data) uses data as the new ref cents
    refcent('?') returns the name of the last ref cent file or command
    """
    ns = self.ns
    if z == '?':
        state['refCent_filename'] = self.refCent_filename
        return self.refCent_filename
    if z == None or z == '':
        pass
    elif ((type(z) is not np.ndarray) and (z == 0)):
        pass
    elif isinstance(z, str):
        ns = self.ns
        nf = self.nfilt
        if (os.path.exists(z)):
            s_ref = fits.readfits(z)
            if (s_ref.ndim != 1 or (s_ref.shape[0] != ns*2) and (s_ref.shape[0] != ns*2+nf)):
                print 'invalid size array '+str(s_ref.shape[0])+' expecting '+str(ns*2)+' or '+str(ns*2+nf)
                return
            self.s_ref = s_ref[0:ns*2]
            rtc.rtc2.load('s_ref',self.s_ref.astype('double'))
            self.refCent_filename = z
            state['refCent_filename'] = self.refCent_filename
            print 'new refcent loaded: '+z
            return
        else:
            print 'file '+z+' does not exist'
            return
    elif (type(z) is np.ndarray):
        s_ref = z
        if (s_ref.ndim != 1 or s_ref.shape[0] != ns*2):
            print 'invalid size array '+str(s_ref.shape[0])+' expecting '+str(ns*2)
            return
        self.s_ref = s_ref
        rtc.rtc2.load('s_ref',self.s_ref.astype('double'))
        self.refCent_filename = 'refcent(data)'
        state['refCent_filename'] = self.refCent_filename
        print 'new refcent loaded (data)'
        return
    else:
        print 'refcent argument not valid'
        crib(refcent)
        return

    self.s_ref *= 0.
    rtc.rtc2.load('s_ref',self.s_ref.astype('double'))

    if (z == 0):
        self.refCent_filename = 'refcent(0)'
        state['refCent_filename'] = self.refCent_filename
        return

    self.open_loop()
    time.sleep(.5)
    print 'averaging...'
    s = rtc.rtc2.peek('s')
    sr = s.copy()*0.0
    for k in range(100):
        sr = sr + 0.01*s.copy()
        time.sleep(.01) # hopefully the rtc thread is running during this second

    rtc.rtc2.load('s_ref',sr.astype('double'))
    self.refCent_filename = 'refcent()'
    state['refCent_filename'] = self.refCent_filename
    print 'refcent done'

__main__.refcent = refcent

helper['refcent_save'] = 'save the current reference centroids to a file'
def refcent_save(filename,comment=None):
	"""Save the current reference centroids to a file.
	The file will always be stored in $HOME/data, so do not provide a directory path.
	"""
	filename = os.path.split(filename)[-1] # just get the last part
	if (not filename.endswith('.fits')):
		raise ValueError('filename '+filename+' is supposed to end with .fits ')
	bad_chars = [' ','/','\\',':']
	for c in bad_chars:
		if (c in filename):
			raise ValueError('filename '+filename+' has a bad character '+c)

	fullFileName = os.path.join(home,'data',filename)
	fits.protectFile(fullFileName,verbose=True)
	hdu = pyfits.PrimaryHDU(self.s_ref)
	tl = time.localtime(time.time())
	day = time.strftime('%Y-%m-%d',tl)
	tim = time.strftime('%H:%M:%S',tl)
	hdu.header.append(('DATE',day,'File generation date'))
	hdu.header.append(('TIME',tim,'file generation time hhmmss'))
	hdu.header.append(('SYSTEM','ShaneaAO','UC Lick Observatory Shane LGS AO'))
	hdu.header.append(('ITYPE','RTCParam'))
	hdu.header.append(('DEVICE','WFSCam'))
	hdu.header.append(('FUNC','reference centroids'))
	hdu.header.append(('SYMBOL','s_ref'))
	hdu.header.append(('MODE',self.mode))
	hdu.header.append(('SEC',True,'Secondary obscuration'))
	hdu.header.append(('NS',self.ns,'Number of subaps'))
	hdu.header.append(('ASTEP','1.3','Algorithm step in RTC Software Definitions doc'))
	hdu.header.append(('GENER','shaneao.refcent_save','generator code'))
	if (comment):
		hdu.header['comment'] = str(comment)
	hdu.writeto(fullFileName)
	self.refCent_filename = fullFileName  # tell the system we've saved it in a file
	state['refCent_filename'] = fullFileName
	self.refCent_sliders = (0.,) # and there are no slider modifications
	print '<refcent_save> refcents saved to '+fullFileName

__main__.refcent_save = refcent_save

def slide(coefs):
	"""Add offsets to the reference centroids given a vector of coefficients.
	The centroid offsets that define the modes are given in the Hw calibration matrix.
	"""
	if (os.path.isfile(os.path.join(home,self.refCent_filename))):
		startFile = self.refCent_filename
		refcent(startFile) # start from file-stored refcents. this erases the prior offsets
	elif (self.refCent_filename == 'refcent(0)'):
		startFile = self.refCent_filename
		refcent(0)
	else:
		print '<slide> ERROR starting point refcent(data) is not saved in a file.'
		print '<slide> It is highly recommended you start with a saved refcent file'
		print '<slide> to avoid losing the starting point.'
		return
	the_mode = self.mode
	if (the_mode == '16x' or the_mode == '16xLGS'):
		Hw = fits.readfits(os.path.join(parameter_directory,'reconMatrix_16x','Hw.fits'),raiseError=True)
	elif (the_mode == '8x' or the_mode == '8xLGS'):
		Hw = fits.readfits(os.path.join(parameter_directory,'reconMatrix_8x','Hw.fits'),raiseError=True)
	ns2,nmodes = Hw.shape
	ncoefs = len(coefs)
	if (ncoefs > nmodes-2): ncoefs = nmodes-2
	s_slide = np.zeros((ns2))
	for k in range(ncoefs):
		s_slide += coefs[k]*Hw[:,k+2] # not including tip and tilt
	s_ref_mod = self.s_ref + s_slide
	refcent(s_ref_mod)
	self.refCent_filename = startFile # restore the starting filename
	state['refCent_filename'] = startFile # also indicate in the state
	self.refCent_sliders = coefs # but there are modification coefficients

__main__.slide = slide

helper['set_CM'] = 'load a new control matrix'
def set_CM(file=None):
	"""Read a new control matrix from a file.
	set_CM() lists the valid files to choose from
	set_CM('?") returns the name of the currently loaded control Matrix
	"""
	the_mode = mode()
	if (the_mode == '16x'):
		valid_files = ['controlMatrix_16x.fits',
					   'controlMatrix_16x_wls.fits',
					   'controlMatrix_16x_dim.fits',
					   'controlMatrix_16x_zero.fits']
	if (the_mode == '16xLGS'):
		valid_files = ['controlMatrix_16xLGS.fits',
					   'controlMatrix_16x_wlsLGS.fits',
					   'controlMatrix_16x_dimLGS.fits']
	if (the_mode == '8x'):
		valid_files = ['controlMatrix_8x.fits',
					   'controlMatrix_8x_wls.fits',
					   'controlMatrix_8x_dim.fits']
	if (the_mode == '8xLGS'):
		valid_files = ['controlMatrix_8xLGS.fits',
					   'controlMatrix_8x_wlsLGS.fits',
					   'controlMatrix_8x_dimLGS.fits']
	if (config['simulator']):
		if (the_mode == '16x'):
			valid_files = ['controlMatrix_16x_sim.fits']
		if (the_mode == '16xLGS'):
			valid_files = ['controlMatrix_16xLGS_sim.fits']
		if (the_mode == '8x'):
			valid_files = ['controlMatrix_8x_sim.fits']
		if (the_mode == '8xLGS'):
			valid_files = ['controlMatrix_8xLGS_sim.fits']
	if (file == '?'):
		state['controlMatrix_filename'] = self.controlMatrix_filename
		return self.controlMatrix_filename
	if (file == None):
		print '<set_CM> valid files are '+str(valid_files)
		return
	if (file not in valid_files):
		print '<set_CM> WARNING file '+str(file)+' is not in the list of recommended files '+str(valid_files)+'.'
		print '<set_CM> but continuing with the load anyway. You should double check that this is the one you really want.'
	cm = fits.readfits(os.path.join(parameter_directory,file),raiseError=True)
	self.controlMatrix = cm
	self.controlMatrix_filename = file
	state['controlMatrix_filename'] = file
	print '<shaneao.set_CM> new control matrix loaded: '+file

__main__.set_CM = set_CM

def zero():
    """send the tweeter DM to a zero-point shape (defined by a0)
    The loop is opened
    """
    self.open_loop()
    a0 = rtc.rtc2.peek('a0').copy()
    rtc.rtc2.load('a',a0.astype('double'))
    print 'DMs at zero point (a0)'

__main__.zero = zero

def estop():
    """emergency stop
    stop the control loop, turn off the mems and woofer thread
    and cut power to the mems controller.
    This is a quick shutdown procedure that is designed to protect the
    MEMS in case something goes wrong.
    """
    zero()
    mems('off')
    woof('off')

__main__.estop = estop

def peek(item=None):
    """Peek(item)
    Peek at a specific variable in the real-time c-code
    Peek() prints a list of valid items
    """
    item_list = ['s','s_ref','a','a0','wfs','wfs_subaps','wfs_background','inten','i_map',
				 'tts','tt_inten','tt_background','dtt','tt_centWts']
    if (item == None):
        print 'can peek at: '+str(item_list)
        return
    elif (item in item_list):
        r = rtc.rtc2.peek(item).copy()
    else:
        print 'invalid item, choose from '+str(item_list)
        return
    return r

__main__.peek = peek

def whereis(filename):
    """find out where a file is in the sys.path (the path for module imports)
    """
    for path in sys.path:
        fullname = path + '/' + filename
        if (os.path.isfile(fullname)):
            return path

__main__.whereis = whereis

def test():
    """initialize the regression tests.
    After initializing, crib(test) again to get a list of available regression tests
    """
    import regtest
    regtest.init()
    __main__.test = regtest

__main__.test = test

def tweeter_poke(pattern=None,n=None,scale=1.):
    """will poke a given pattern on the tweeter.
    This function puts values directly into the `rtc2 <rtc2>` integrator,
    (so bleed settings and loop gain matter)

    tweeter_poke() lists the available patterns.
    tweeter_poke('mode',n) pokes a pre-calculated mode.
    the third argument, scale, must be between -1 and +1, and defaults to +1
    """
    global At
    pattern_list = ['cross','mode']
    if (pattern == None):
        print 'argument must be one of '+str(pattern_list)
        return
    if (pattern in pattern_list):
        a = rtc.rtc2.peek('a')
        at = a[0:1024].reshape((32,32))
        if (pattern == 'cross'):
            a[0:1024] = 21000.
            at[16:18,:] = 21000.*(1.+scale)
            at[:,16:18] = 21000.*(1.+scale)
        if (pattern == 'mode'):
            mode = self.mode.replace('LGS','')
            if not ('At' in globals()):
                print '<tpoke> reading in the mode set'
                At_file = os.path.join(parameter_directory,'reconMatrix_'+mode,'At.fits')
                At = fits.readfits(At_file,raiseError=True)
            n_modes = len(At)
            if (n == None):
                print '<tpoke> second argument, n, must be between 0 and '+str(n_modes-1)
                return
            if ((n < 0) or (n >= n_modes)):
                print '<tpoke> second argument, n, must be between 0 and '+str(n_modes-1)
                return
            a[0:1024] = At[n].reshape((1024))*21000.*scale + 21000.
    else:
        tweeter_poke()

__main__.tweeter_poke = tweeter_poke
__main__.poke_tweeter = tweeter_poke

def woofer_poke(pattern=None,n=None,scale=1.):
    """will poke a given pattern on the woofer.
    This function puts values directly into the `rtc2 <rtc2>` integrator,
    (so bleed settings and loop gain matter)

    woofer_poke() lists the available patterns.
    woofer_poke('mode',n) pokes a pre-calculated mode.
    the third argument, scale, must be between -1 and +1, and defaults to +1
    """
    global Aw
    pattern_list = ['act','mode']
    if (pattern == None):
        print 'argument must be one of '+str(pattern_list)
        return
    if (pattern in pattern_list):
        a = rtc.rtc2.peek('a')
        aw = a[1024:1024+52]
        if (pattern == 'act'):
            if ((n<0) or (n>=52)):
                print '<tpoke> n must be between 0 and 51'
                return
            aw[0:52] = 0.
            aw[n] = scale
        if (pattern == 'mode'):
            if not ('Aw' in globals()):
                print '<tpoke> reading in the mode set'
                mode = self.mode.replace('LGS','')
                Aw_file = os.path.join(parameter_directory,'reconMatrix_'+mode,'Aw.fits')
                Aw = fits.readfits(Aw_file,raiseError=True)
            n_modes = len(aw)
            if (n == None):
                print '<tpoke> second argument, n, must be between 0 and '+str(n_modes-1)
                return
            if ((n < 0) or (n >= n_modes)):
                print '<tpoke> second argument, n, must be between 0 and '+str(n_modes-1)
                return
            aw[0:52] = Aw[n]*scale
    else:
        woofer_poke()

__main__.woofer_poke = woofer_poke
__main__.poke_woofer = woofer_poke

def reset(restart=False):
    """do this to reset the camera if it hangs
    """
    global wfs
    camstop()
    if (config['simulator']):
        time.sleep(.5)
        wfs.set_callback(None)
    del wfs
    wfs = None
    if (restart):
        camstart()

__main__.reset = reset

def ttreset(restart=False):
    """do this to reset the tt camera if it hangs
    """
    global tt
    ttcamstop()
    del tt
    tt = None
    if (restart):
        ttcamstart()

__main__.ttreset = ttreset

if (config['simulator']):

	__main__.light = sauce.light
	__main__.tt_light = sauce.tt_light
	__main__.aber = sauce.aber
	__main__.sky = sauce.sky

import dataServer
the_wfs_dark = np.zeros((160,160))
the_tts_dark = np.zeros((80,80))
dataServer.i_map = i_map
dataServer.ccd = rtc.rtc2.peek('wfs').reshape((160,160))
dataServer.sub = rtc.rtc2.peek('wfs_subaps')
dataServer.inten = rtc.rtc2.peek('inten')
dataServer.a = rtc.rtc2.peek('a')
dataServer.da = rtc.rtc2.peek('da')
dataServer.tts = rtc.rtc2.peek('tts').reshape((80,80))
dataServer.ttbg = rtc.rtc2.peek('tt_background').reshape((80,80))
dataServer.wfs_dark = the_wfs_dark
dataServer.tts_dark = the_tts_dark
the_wfs_flat = np.zeros((160,160))
the_wfs_flat[:] = self.wfs_flat
dataServer.wfs_flat = the_wfs_flat

config['has tts_ma'] = True
if (config['has tts_ma']):
	print '<shaneao> setting up for tip/tilt moving average'
	tts_ma = np.zeros((80*80))
	rtc.rtc2.load('tts_ma',tts_ma.astype(np.double))
	tts_ma_alpha = np.ones((1))*0.99
	rtc.rtc2.load('tts_ma_alpha',tts_ma_alpha.astype(np.double))
	dataServer.tts_ma = rtc.rtc2.peek('tts_ma').reshape((80,80))  # tip/tilt sensor moving average

config['has wfs_ma'] = True
if (config['has wfs_ma']):
	print '<shaneao> setting up for wavefront sensor moving average'
	wfs_ma = np.zeros((160*160))
	rtc.rtc2.load('wfs_ma',wfs_ma.astype(np.double))
	wfs_ma_alpha = np.ones((1))*0.99
	rtc.rtc2.load('wfs_ma_alpha',wfs_ma_alpha.astype(np.double))
	dataServer.wfs_ma = rtc.rtc2.peek('wfs_ma').reshape((160,160))  # wavefront sensor moving average

dataServer.rtc_instance = self
dataServer.status = status
dataServer.command = _command
dataServer.slide = slide  # callback for image sharpening refcent modes slider
dataServer.refcent_save = refcent_save # callback for saving the refcents and zeroing the sliders
dataServer.estop = estop # callback for emergency stop sent from a remote GUI

if 'use_publisher' in config:
    if config['use_publisher']:
        # The publisher is now treated as a plug-in module
        import publisher
        shaneao = sys.modules[__name__]
        publisher.go_shaneao(shaneao)

cleaned_up = False
def _goodbye():
    """cleanup on exit
    """
    global woofer
    global cleaned_up
    global wfs

    if cleaned_up == True:
    	return

    zero()
    time.sleep(0.5)
    print 'turning off mems'; sys.stdout.flush()
    mems('off')
    print 'turning off woofer'; sys.stdout.flush()
    woof('off')
    time.sleep(0.5) # wait for the woofer thread to stop
    print 'stopping the wfs camera'
    camstop()
    print 'waiting for real-time thread to go idle'
    time.sleep(1.) # wait for the thread to go idle
    try:
        print 'releasing wfs camera driver'
        del wfs; wfs = None
    except:
        pass
    print 'releasing DPIO2 tweeter driver'
    if ('tweeter' in locals()): del tweeter
    print 'stopping tt camera'
    ttcamstop()
    print 'waiting for real-time thread to go idle'
    time.sleep(1.)
    try:
        print 'releasing tt camera driver'
        del tt; tt = None
    except:
        pass
    time.sleep(0.1)
    print 'stopping dataServer'
    dataServer.stop()
    try:
        print 'stopping woofer communication thread'
        woofer.stop_thread()
    except:
        pass
    try:
        print 'releasing woofer client'
        del woofer
    except:
        pass
    #root.destroy()
    print "goodbye at time %.1f" % (time.time())
    sys.stdout.flush()
    time.sleep(0.1)
    cleaned_up = True
    if config['simulator']:
        os._exit(1)
    sys.exit()

#__main__.goodbye = _goodbye
__main__.exit = _goodbye
if config['simulator']:
	def _simulator_goodbye():
		print '_simulator_goodbye called'
		answer = raw_input('kill subprocesses? [y|n]: ')
		if (answer == 'y'):
			for pid in pids:
				pid.kill()
		os._exit(1)

	__main__.exit = _simulator_goodbye

def _goofedup():
    if (cleaned_up == False):
        print "oh.. you shouldn't have done that"
        print "If this crashes, it's your fault."

# Register _goodbye() with the atexit module first, so that it gets called
# after _goofedup().

atexit.register(_goodbye)
atexit.register(_goofedup)

# Having defined all of the functions, provide the shaneaoKeyword module
# with direct access to said defined functions.

try:
    state.setNamespace(__main__)
except AttributeError:
    pass

# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
