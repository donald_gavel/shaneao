"""Publisher - Provide regular ShaneAO telemetry data
to a PUB zmq socket.
It substitutes for dataServer, which was a demand based
data provider.
Publisher can be set up to provide data to the new peeko_tk GUI.
It can also publish regular data for on-line analysis and tuning
"""
import zmq
import time
import threading
import json
import numpy_json2 as numpy_json
import numpy as np

class Publisher (object):
    def __init__(self,port = 'tcp://*:6000'):
        self.tcp_port = port
        self.ctx = zmq.Context.instance()
        self.socket = self.ctx.socket(zmq.PUB)
        self.socket.bind(self.tcp_port)
        self.state = 'running'
        self.items = {'testx':'hello world'}
    
    def close(self):
        self.socket.close()
        self.state = '<defunct>'
        
    def _send(self,msg):
        self.socket.send(msg)
    
    def post_items(self,dic=None):
        """posts several items on the pub socket.
        dic must be a dictionary with keys and data.
        The first 5 letters of the key (padded with blanks if necessary)
        form the pub ID, which is used for subscription by SUB clients.
        Each item must be JSON encodable, or numpy_json encodable (i.e.
        any numpy ndarray).
        Example:
          {'acts':a[0:1024].reshape((32,32)),'ccd':ccd,'mode':'16x'}
        """
        if dic is None:
            dic = self.items
        for key in dic.keys():
            hdr = key.ljust(5)[:5]
            msg = hdr+numpy_json.dumps(dic[key])
            self.socket.send(msg)
    
class PubThread(threading.Thread):
    def __init__(self,publisher,dt = 0.5):
        threading.Thread.__init__(self)
        self.name = 'PubThread'
        self.publisher = publisher
        self.stopped = threading.Event()
        self.t0 = time.time()
        self.time_in_thread = 0.
        self.dt = dt
        self.daemon = True
        self.state = 'ready'

    def run(self):
        """this will use the Publisher to post volatile data
        """
        self.state = 'running'
        while not self.stopped.wait(self.dt):
            t = time.time() - self.t0
            self.publisher.post_items()
            self.time_in_thread = t

    def stop(self):
        self.stopped.set()
        self.state = 'stopped'

    def status(self):
        t = time.time() - self.t0
        dt = t - self.time_in_thread
        print self.name, self.state, ' last execution: ',dt,' seconds ago, with dt = ',self.dt

def go_shaneao(sao_module,dt=0.33):
    """start up the publisher specfically for shaneao
    """
    global publer, pubThread
    rtc = sao_module.rtc
    publer = Publisher()
    pubThread = PubThread(publer,dt=dt)
    items = {'wfs':rtc.rtc2.peek('wfs').reshape((160,160)),
             'inten':rtc.rtc2.peek('inten'),
             'a': rtc.rtc2.peek('a')[0:1024],
             'aw':rtc.rtc2.peek('a')[1024:1024+52],
             'mode':sao_module.self.mode
             }
    publer.items = items
    pubThread.start()

def test():
    global publer
    publer = Publisher()
    pubThread = PubThread(publer,dt=0.5)
    pubThread.start()

    ctx = zmq.Context.instance()
    client = ctx.socket(zmq.SUB)
    client.connect('tcp://localhost:6000')
    client.setsockopt(zmq.SUBSCRIBE,'')
    
    msg = client.recv()
    print msg
    
    pubThread.stop()
    publer.close()

def test_server(dt=0.5,n=10, stop=False):
    global publer, pubThread
    if stop:
        pubThread.stop()
        publer.close()
        print 'stopping test server'
        time.sleep(.2)
        return
    publer = Publisher()
    if n > 0:
        u = np.random.normal(size=(n))
        items = {'stuff':u}
        publer.items = items
    pubThread = PubThread(publer,dt=dt)
    print 'starting test server'
    pubThread.start()

def test_client(n=10,host='localhost'):
    ctx = zmq.Context.instance()
    client = ctx.socket(zmq.SUB)
    client.connect('tcp://'+host+':6000')
    client.setsockopt(zmq.SUBSCRIBE,'')
    client.setsockopt(zmq.RCVTIMEO,1000)
    msg = '(none)'
    for k in range(n):
        try:
            msg = client.recv()
        except:
            print 'timeout'
            msg = '(none)'
        if len(msg) < 100:
            print msg
        else:
            print msg[:100]

    client.close()
