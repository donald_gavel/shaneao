#!/usr/bin/python
"""The peeko client is run on a "remote" machine
(usually the operators console). It connects to
:py:mod:`shaneao's data server <dataServer>` running on the real-time
computer via a socket connection. It displays
the WFS raw data, subap intensities, and the woofer
and tweeter commands.

Start up the client from the home directory on the
host computer::

    peeko

Commands to peeko are typed at the terminal. The
command names are exposed at the top level, so there
is no need to type the leading ``peeko_client2.`` string.
Python demands however that you type the trailing ``()``
string on commands, even if there are no arguments.
"""
#====================================================
# peeko_client2.py
#
# http://stackoverflow.com/questions/13366057/matplotlib-2-figures-in-subplot-1-is-animation

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import socket
import struct
import signal
import atexit
import readline
import pyfits
import readfits as fits
import pydoc
import os
import sys
from threading import Thread, Timer
import time
import pprint
import pylab as P
import __main__

#root = Tk.Tk()
os.chdir(os.environ['HOME'])

print '__name__ is '+str(__name__)
__main__.peeko = sys.modules[__name__]

sys.stdout.write("\x1b]2;Peeko\x07")

helper = {}

def crib(item=None):
    """Quick help:
    help() lists all the commands with brief description.
    help(item) to get help on a particular command
    """
    bling = '============================'
    if (item == None):
        print bling
        print '   Peeko help: \n'
        for k in helper.keys():
            print k+' -- '+ helper[k]
        crib(crib)
        print bling
        
        return
    print pydoc.render_doc(item,title='%s')

__main__.crib = crib
helper['help'] = 'get help on peeko commands'
__main__.help = crib

helper['start'] = 'start (or restart) the peeko display'
def start():
    """start the peeko display
    """
    global win_list
    plt.close('all')
    win_list = []
    start_display()

__main__.start = start
__main__.restart = start
helper['connect'] = 'connect to the real-time code (same as restart)'
__main__.connect = start

helper['wfs_only'] = 'display only the wavefront sensor'
def wfs_only():
    """Show only the wavefront sensor, taking up the whole
    display area of the peeko window.
    """
    zoomed = wfs.zoomed
    start_display(wfs_only = True)
    if (zoomed):
        zoom()
    if (auto_ranging):
        drange('auto')

__main__.wfs_only = wfs_only

helper['all4'] = 'show all the displays (counters wfs_only())'
def all4():
    """show all 4 displays in sub-windows of the peeko display.
    """
    zoomed = wfs.zoomed
    start_display(wfs_only = False)
    if (zoomed):
        zoom()
    if (auto_ranging):
        drange('auto')

__main__.all4 = all4

helper['wfs_bg'] = 'change the wfs display from raw to background-remomved'
def wfs_bg(bg_removed = True):
    """show the wfs display as background-removed, or not'
    """
    n = len(win_list)
    if (n == 1): wfs_only=True
    elif (n == 4): wfs_only=False
    zoomed = wfs.zoomed
    if bg_removed:
        start_display(wfs_only=wfs_only,wfs_background_removed=True)
    else:
        start_display(wfs_only=wfs_only)
    if (zoomed):
        zoom()
    if (auto_ranging):
        drange('auto')

__main__.wfs_bg = wfs_bg

helper['update'] = 'set the update interval, in milliseconds'
def update(p=None):
    """set or query the display update rate
    update(milliseconds) - set the rate
    update() - print the current rate
    """
    global disp_dt
    if (p == None):
        return disp_dt
    disp_dt = float(p)
    start_display(showing_wfs_only)
    return 'display rate =' + str(disp_dt)

__main__.update = update

def get_status():
    """Query the status of the real-time code"""
    if (paused):
        print '<peeko.get_status> no connection to server '+host
        return None
    s.sendall('status')
    b_count = recv_all(s,8)
    r_count = int(struct.unpack('1q',b_count)[0]) # was '1l'
    b = recv_all(s,r_count)
    state = eval(b)
    return state

__main__.get_status = get_status

def _state(tag=None):
    """Query and print out the status of the real-time code
    """
    state = get_status()
    if (tag == None):
        pprint.pprint(state)
        return None
    else:
        return state[tag]

helper['state'] = 'print the state of the real-time code'
__main__.state = _state

def _command(theCommand):
    """send a command to the RTCode"""
    wfc = 'command: '+theCommand
    s.sendall(wfc)

__main__.command = _command        

helper['zoom'] = 'set the wfs and inten displays to zoomed'
def zoom():
    """set the wfs and inten displays to zoomed,
    that is, showing only the portion of the wfs camera array
    illuminated by the pupil.
    """
    plt.figure(fig.get_label())
    wfs.zoom(1)
    if (not showing_wfs_only):
        inten.zoom(1)
    return

__main__.zoom = zoom

helper['unzoom'] = 'set the wfs and inten displays to unzoomed'
def unzoom():
    """set the wfs and inten displays to unzoomed -
    i.e. showing the entire wfs camera array
    """
    plt.figure(fig.get_label())
    wfs.zoom(0)
    if (not showing_wfs_only):
        inten.zoom(0)
    return

__main__.unzoom = unzoom

helper['cmap'] = 'change the color map for one or all displays'
def cmap(win='all',cmap = None):
    """change the color map for a particular display
    usage:
    cmap() prints the current color maps as set for each window
    cmap('?') prints lists of the valid windows and color maps
    cmap(cmap) changes all windows to the given color map
    cmap(win) prints the current color map for the given window
    cmap(win,cmap) changes the color map for the given window
    """
    if (win == 'all' and cmap == None):
        info = []
        for w in win_list:
            info.append((w.title,w.cmap))
        print str(info)
        return

    if (win in win_list):
        win = win.title
        
    if (win in cmap_set):
        cmap = win
        win = 'all'
       
    if (win == 'all'):
        for w in win_list:
            w.set(cmap=cmap)
            drange(w,w.drange)
        return
        
    win_titles = []
    for w in win_list:
        win_titles.append(w.title)
    
    if (win in win_titles and cmap == None):
        w = win_list[win_titles.index(win)]
        print str((w.title,w.cmap))
        return
    
    if (win not in win_titles or cmap not in cmap_set):
        print 'window choices: '+str(win_titles)
        print 'color map choices: '+str(cmap_set)
        return
    
    w = win_list[win_titles.index(win)]
    w.set(cmap = cmap)
    drange(w,w.drange)

__main__.cmap = cmap

helper['drange'] = 'set the display range (stretch) for one or all displays'
def drange(win=None,r=None):
    """change the display range for a particular window
    usage:
    drange() prints the current ranges and max ranges for all the windows
    drange(win) print the current range of the win (where win = wfs,inten,tweeter, or woofer)
    drange(win,r) changes the display range. r must be a 2-tuple (display min, diaplay max)
    drange(win,'max') changes the display range to the full allowable range
    drange(win,'auto') changes the range to 20% below min to 20% above max
    drange('max') sets all displays to max range
    drange('auto') sets all displays to auto range
    """
    global auto_ranging
    
    if (win == None):
        for w in win_list:
            print w.title, w.drange, 'max = ', w.range
        return
    
    if (isinstance(win,str)):
        if (win == 'all'):
            if (r == None):
                drange()
                return
            if (r == 'auto'):
                for w in win_list:
                    drange(w.title,'auto')
                auto_ranging = True
                return
            if (r == 'max'):
                for w in win_list:
                    drange(w.title,'max')
                return
        if (win == 'max'):
            drange('all','max')
            return
        if (win == 'auto'):
            drange('all','auto')
            return
        for w in win_list:
            if (win == w.title):
                win = w
                break
    
    if (win in win_list):
        if (r == None):
            print(win.drange)
            return
        if (r == 'max'):
            win.drange = win.range
            win.autorange = False
            return
        if (r == 'auto'):
            span = win.data.max() - win.data.min()
            if (span == 0):
                span = win.range[1] - win.range[0]
            top = win.data.max() + 0.2*span
            bot = win.data.min() - 0.2*span
            drange(win,(bot,top))
            win.autorange = True
            return
        win.drange = r
        win.autorange = False
        return
    if (win not in win_list):
        win_titles = []
        for w in win_list:
            win_titles.append(w.title)
        print 'window choices: '+str(win_titles)
        return

__main__.drange = drange

helper['sethost'] = 'set the host machine'
def sethost(ahost=None):
    """set or query the server host machine name
    sethost('?") returns a list of valid server names
    """
    global host
    valid_hosts = ['real','real.ucolick.org','scab','scab.ucolick.org','morely','morely.ucolick.org','rtc','rtc.ucolick.org','localhost']
    if (ahost == '?'):
        return valid_hosts
    if (ahost == None):
        return host
    if (ahost in valid_hosts):
        host = ahost
        print 'host set to '+str(host)
    else:
        print 'invalid host '+str(host)+'; must be one of '+str(valid_hosts)

__main__.sethost = sethost

if (__name__ == 'peeko_client2'):
    print 'plt.ion()'
    plt.ion()

global s

cmap_set = ['autumn','bone','cool','copper',
            'flag','gray','hot','hsv','jet','pink',
            'prism','spring','summer']

def demo_update(num, img1, img2):
    w1 = np.random.normal(0,1,size=(10,10))
    plt.subplot(211)
    plt.xlabel('frame '+str(num))
    img1.set_data(w1)
    d = np.random.rand(2,25)
    nhist, xedges, yedges = np.histogram2d(d[0,:], d[1,:])
    #plt.subplot(212)
    img2.set_data(nhist)
    return img1, img2

def demo_do():
	global img1, img2, ax1, ax2
	
	fig1 = plt.figure('demo')
	
	data = np.random.rand(2, 25)
	ax1=plt.subplot(211)
	w1 = np.random.normal(0,1,size=(10,10))
	img1 = plt.imshow(w1,interpolation='nearest',cmap='bone')
	ax2=plt.subplot(212)
	nhist, xedges, yedges = np.histogram2d(data[0,:], data[1,:])
	img2 = plt.imshow(nhist, aspect='auto', origin='lower')
	#img.set_visible(True)
	line_ani = animation.FuncAnimation(fig1, demo_update, 25, fargs=(img1, img2),interval=30, blit=False)
	line_ani.repeat = True
	return line_ani

def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

def show_data(i,win_list):
    rlist = []
    for win in win_list:
        win.get_and_show(i)
        rlist.append(win.im)
    return rlist

def catch_timeout(signum,frame):
    global paused, s, rst
    print '<catch_timeout> signal '+str(signum)
    paused = True
    if (s != None):
        s.close()
        s = None
    try:
        rst.stop()
    except:
        pass
    rst = restart_thread()
    rst.start()

signal.signal(signal.SIGALRM,catch_timeout)

mode = '16x'
parameter_directory = os.path.join(os.environ['HOME'],'parameterFiles')

map_file = 'u_map_subaps_'+mode+'.fits' # subap cam data has already been deinterlaced by rtc2
hdulist = pyfits.open(os.path.join(parameter_directory,map_file))
ns = hdulist[0].header['NS']
npix = hdulist[0].header['NAXIS1'] # total number of pixels in map
u_map_subaps = hdulist[0].data
hdulist.close()

map_file = 'i_map.fits' # real cam data needs to be deinterlaced
hdulist = pyfits.open(os.path.join(parameter_directory,map_file))
i_map = hdulist[0].data
hdulist.close()

actuator_defaults_file = 'actuator_defaults.fits'
hdulist = pyfits.open(os.path.join(parameter_directory,actuator_defaults_file))
na_tweeter = hdulist[0].header['NAT']
na_woofer = hdulist[0].header['NAW']
n_filter = hdulist[0].header['NF']
hdulist.close()

def change_modes(new_mode):
    global ns, u_map_subaps, mode, npix

    mode = new_mode
    map_file = 'u_map_subaps_'+mode+'.fits' # subap cam data has already been deinterlaced by rtc2
    hdulist = pyfits.open(os.path.join(parameter_directory,map_file))
    ns = hdulist[0].header['NS']
    npix = hdulist[0].header['NAXIS1'] # total number of pixels in map
    u_map_subaps = hdulist[0].data
    hdulist.close()
    for w in win_list:
        if (w.kind == 'sub'):
            w.raw_size = npix
            w.nbytes = self.raw_size*8
            w.form = str(self.raw_size)+'d'
    if (wfs.zoomed):
        zoom()

class dwindow(object):
    """A display-window within the peeko window.
    The peeko window is presently set up as a 2x2 window
    display of raw wfs image, subap intensities,
    tweeter commands, woofer commands. Each of these
    has parameters such as range, color-map, and zoom.
    
    self = dwindow(kind)
    where kind is one of 'wfs','inten','a','aw'
    """
    
    kinds = ['wfs','inten','a','aw','sub']
    pnames = ['range','vmin','vmax','cmap','x_lim','y_lim']
    woofer_data = np.zeros((52)) # class level, so 'a' can assign to 'aw'
    woofer_tilt_basis = fits.readfits('data/woofer_tilt_basis.fits')
    
    def __init__(self,kind,sub):
        self.kind = kind
        self.sub = sub
        if (kind == 'wfs'):
            self.title = 'wfs'
            n = 160
            self.raw_size = n*n
            self.size = (n,n)
            self.range = [0,65535]
            self.vmin = 0
            self.vmax = 30000
            self.cmap = 'hot'
            self.fiducial = True
            self.zoomed = False
        elif (kind == 'inten'):
            self.title = 'inten'
            n = ns
            self.raw_size = n
            self.size = (160,160)
            self.range = [0,25*65535.]
            self.vmin = 0
            self.vmax = 120000
            self.cmap = 'hot'
            self.zoomed = False
        elif (kind == 'a'):
            self.title = 'tweeter'
            n = 32
            self.raw_size = na_tweeter + na_woofer + n_filter
            self.size = (n,n)
            self.range = [0,43000]
            self.vmin = 0
            self.vmax = 43000
            self.cmap = 'hot'
        elif (kind == 'aw'):
            self.title = 'woofer'
            n = 8
            self.raw_size = na_woofer
            self.size = (n,n)
            self.range = [-1.,1.]
            self.vmin = -1.
            self.vmax = 1.
            self.cmap = 'hot'
            self._populate_actlocs()
        elif (kind == 'sub'):
            self.title = 'wfs-bg' # wfs - background (subaps only)
            self.raw_size = npix
            self.size = (160,160)
            self.range = [-10000,65535]
            self.vmin = 0
            self.vmax = 30000
            self.cmap = 'hot'
            self.fiducial = True
            self.zoomed = False
            
        self.form = str(self.raw_size)+'d'
        self.nbytes = self.raw_size*8
        self.x_lim = (0,self.size[0]-1)
        self.y_lim = (0,self.size[1]-1)
        self.defaults = [self.range,self.vmin,self.vmax,self.cmap,self.x_lim,self.y_lim]
        self.im = None
        self.data = np.zeros(self.size)
        self.colorbar = None
        self.ax = plt.subplot(self.sub)
        def format_coord(x,y):
            col = int(x+0.5)
            row = int(y+0.5)
            numrows, numcols = self.data.shape
            if col>=0 and col<numcols and row>=0 and row<numrows:
                z = self.data[row,col]
                return '%s [%1.0f, %1.0f] %1.2f'%(self.title, x, y, z)
            else:
                return '%s [%1.0f, %1.0f]'%(self.title, x, y)
        self.ax.format_coord = format_coord
        self.autorange = False
        self.show()
    
    @property
    def color(self):
        return self.cmap
    
    @color.setter
    def color(self,value):
        self.cmap = value
        #self.show()
    
    @property
    def drange(self):
        return [self.vmin,self.vmax]
    
    @drange.setter
    def drange(self,value):
        if (value[0] >= value[1]):
            print 'drange max is less than min, setting to max range'
            value = [self.range[0],self.range[1]]           
        value = np.clip(value,self.range[0],self.range[1])
        self.vmin,self.vmax = value
        self.im.set_clim(value)
    
    def zoom(self, way=1):
        if (self.kind == 'wfs' or self.kind == 'inten' or self.kind == 'sub'):
            zooms = ['out','in']
            if isinstance(way,str):
                if (way in zooms):
                    way = zooms.index(way)
                else:
                    raise ValueError
            if way:
                if (mode == '16x'):
                    self.x_lim = (40,120)
                    self.y_lim = (40,120)
                    self.zoomed = True
                elif (mode == '8x'):
                    self.x_lim = (58,100)
                    self.y_lim = (58,100)
                    self.zoomed = True
            else:
                self.x_lim = (0,160-1)
                self.y_lim = (0,160-1)
                self.zoomed = False
            ax = plt.subplot(self.sub)
            ax.set_xlim(self.x_lim)
            ax.set_ylim(self.y_lim)
            #self.show()
    
    def show(self):
        #ax = plt.subplot(self.sub)
        ax = self.ax
        ax.set_xlim(self.x_lim)
        ax.set_ylim(self.y_lim)
        plt.title(self.title)
        if (self.im == None):
            self.im = plt.imshow(self.data,interpolation='nearest',cmap=self.cmap,vmin=self.vmin,vmax=self.vmax)
        if (self.colorbar == None):
            self.colorbar = plt.colorbar(self.im)
    
    def _populate_actlocs(self):
        if (self.kind != 'aw'):
            return None
        x_list = list(np.arange(-3.5,3.5+1))
        y0_list = list(np.array([3,5,7,7,7,7,5,3])/2.)
        xy0_list = zip(x_list,y0_list)
        alocs = []
        k = 0
        for x,y0 in xy0_list:
            y_list = np.arange(-y0,y0+1)
            for y in y_list:
                #alocs.append((x,y))
                alocs.append(int((y+3.5)*8+(x+3.5)))
        self.actlocs = alocs
     
    def reset(self):
        self.range, self.vmin, self.vmax, self.cmap,self.x_lim,self.y_lim = self.defaults
        stop_display()
        start_display(showing_wfs_only)
    
    def set(self,vmin=None,vmax=None,cmap = None):
        if (vmin): self.vmin = max(vmin,disp_range[0])
        if (vmax): self.vmax = min(vmax,disp_range[1])
        if (cmap): self.cmap = cmap
        plt.subplot(self.sub)
        plt.set_cmap(cmap)
        self.im.set_clim((self.vmin,self.vmax))
        
    def get_and_show(self,i):
        global paused
        plt.figure(fig.get_label())
        if (not paused):
            if (self.kind == 'aw'):
                n,m = self.size
                self.data.reshape((n*m))[self.actlocs] = dwindow.woofer_data
            else:
                try:
                    s.sendall(self.kind)
                    signal.alarm(2)
                    if (self.kind == 'inten'):
                        b_count = recv_all(s,8)
                        r_count = int(struct.unpack('1q',b_count)[0]) # was '1l'
                        if (r_count != self.raw_size):
                            if (r_count == 144):
                                change_modes('16x')
                            elif (r_count == 40):
                                change_modes('8x')
                            else:
                                raise ValueError, 'invalid subap count: '+str(r_count)
                            print 'changed modes to '+mode
                            self.raw_size = r_count
                            self.nbytes = self.raw_size*8
                            self.form = str(self.raw_size)+'d'
                    b = recv_all(s,self.nbytes)
                    signal.alarm(0)
                    r = np.array(struct.unpack(self.form,b))
                    if (self.kind == 'a'):
                        dwindow.woofer_data = r[1024:1024+52]
                        if (showing_wfs_only == False): woofer.ttshow()
                        self.data = r[0:1024].reshape(32,32)
                        self.data[0,0] = 0
                        self.data[0,31] = 0
                        self.data[31,0] = 43000
                        self.data[31,31] = 43000
                    elif (self.kind == 'inten'):
                        self.data[:] = 0
                        for k in range(self.raw_size):
                            nsub = 5
                            n,m = self.size
                            self.data.reshape((n*m))[u_map_subaps[k*nsub*nsub:(k+1)*nsub*nsub]] = r[k]
                    elif (self.kind == 'wfs'):
                        u = r[i_map]
                        self.data = u.reshape(self.size)
                        dmax = self.data.max()
                        dmin = self.data.min()
                        fid_bright = (dmax+dmin)/2.
                        if self.fiducial:
                            self.data[79,:] = fid_bright
                            self.data[:,79] = fid_bright
                    elif (self.kind == 'sub'):
                        n,m = self.size
                        self.data.reshape((n*m))[u_map_subaps] = r
                        dmax = self.data.max()
                        dmin = self.data.min()
                        fid_bright = (dmax+dmin)/2.
                        if self.fiducial:
                            self.data[79,:] = fid_bright
                            self.data[:,79] = fid_bright
                    else:
                        self.data = r.reshape(self.size)
                except:
                    print '<dwindow.show> failed to get data: '+self.kind
                    paused = True
                    #start_display(showing_wfs_only)
                    #raise # if you want to bug out, otherwise show the disconnect pattern
        if (paused):   
            if (self.kind == 'wfs' or self.kind == 'sub'):
                self.data = np.random.normal(15000,10000,size=self.size)
            if (self.kind == 'a'):
                self.data *= 0.
                self.data[31,0] = 43000
                self.data[31,31] = 43000
                self.data.reshape((1024))[i % 1024] = 21500
        
        self.im.set_array(self.data)
    
    def ttshow(self):
        """show the tip and tilt components of the woofer data beneath the woofer display
        """
        if (self.title == 'woofer'):
            x_coef = np.dot(dwindow.woofer_data,dwindow.woofer_tilt_basis[:,0])
            y_coef = np.dot(dwindow.woofer_data,dwindow.woofer_tilt_basis[:,1])
            txt = 'x: %+5.1f,    y: %+5.1f arcsec' % (x_coef, y_coef)
            plt.figure(fig.get_label())
            plt.subplot(woofer.sub)
            plt.xlabel(txt)
    
paused = False

win_list = []
showing_wfs_only = False
auto_ranging = False
host = 'localhost'
ani = None
s = None
disp_dt = 50
autorange_timer = None
autorange_dt = 2.0

def start_display(wfs_only=False,wfs_background_removed=False):
    global s, n, fig, ani, paused, win_list, remote_ip, port, rst
    global wfs, inten, tweeter, woofer, showing_wfs_only
    global autorange_timer

    stop_display()
    
    fig = plt.figure('ShaneAO - peeko')
    if (wfs_only != showing_wfs_only):
        plt.clf()
        win_list = []
    try:
        if (wfs.title == 'wfs' and wfs_background_removed==True) or (wfs.title == 'wfs-bg' and wfs_background_removed==False):
            plt.clf()
            win_list = []
    except:
        pass

    try:
        s.close()
    except:
        pass
    try:
        rst.stop()
    except:
        pass
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(4)
    port = 8888
    remote_ip = socket.gethostbyname(host)
    try:
        s.connect((remote_ip,port))
        s.settimeout(None)
        paused = False
    except:
        print '<start_display> no connection to server '+host
        paused = True
        try:
            rst.stop()
        except:
            pass
        rst = restart_thread()
        rst.start()
    
    if (win_list == []):
        if (wfs_only):
            if (wfs_background_removed):
                wfs = dwindow('sub',111)
            else:
                wfs = dwindow('wfs',111)
            __main__.wfs = wfs
            showing_wfs_only = True
            win_list = [wfs]
        else:
            if (wfs_background_removed):
                wfs = dwindow('sub',221)
            else:
                wfs = dwindow('wfs',221)
            inten = dwindow('inten',222)
            tweeter = dwindow('a',223)
            woofer = dwindow('aw',224)
            win_list = [wfs,inten,tweeter,woofer]
            __main__.wfs = wfs
            __main__.inten = inten
            __main__.tweeter = tweeter
            __main__.woofer = woofer
            showing_wfs_only = False
    else:
        for win in win_list:
            win.show()
    
    if (ani):
        try:
            ani._stop()
        except:
            pass
    ani = animation.FuncAnimation(fig, show_data, np.arange(1024), fargs = (win_list,), init_func=None,interval=disp_dt, blit=False)
    autorange_timer = Timer(autorange_dt,autorange_check)
    autorange_timer.start()
    return ani

def autorange_check():
    global autorange_timer
    for win in win_list:
        if win.autorange:
            drange(win,'auto')
    autorange_timer = Timer(autorange_dt,autorange_check)
    autorange_timer.start()

class restart_thread(Thread):
    """Try every so often to restore communications to the shaneao system
    """
    def __init__(self):
        super(restart_thread,self).__init__()
        self.wait = 2.0 # seconds
        self.alive = True
        print '<restart_thread.__init__> ',self
    
    def run(self):
        global paused, s
        while (paused):
            try:
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                s.settimeout(2)
                s.connect((remote_ip,port))
                s.settimeout(None)
                print '<restart_thread> connected'
                #time.sleep(self.wait)
                paused = False
            except:
                #print '<restart_thread> tried... no connection'
                time.sleep(self.wait)
                if (not self.alive):
                    break
        return
    
    def stop(self):
        #print 'stopping restart thread'
        self.alive = False
        #print 'waiting for thread to terminate'
        print '<restart_thread.stop> ',self
        self.join()
    
def stop_display():
    global ani, s
    
    try:
        #print '<stop_display> stopping animation'
        ani._stop()
    except:
        pass #print '<stop_display> stopping animation FAILED'
    try:
        #print '<stop_display> stopping autorange_timer'
        autorange_timer.cancel()
    except:
        pass #print '<stop_display> stopping autorange_timer FAILED'
    try:
        #print '<stop_display> closing socket connection'
        s.sendall('bye')
        s.close()
        s = None
    except:
        pass #print '<stop_display> closing socket connection FAILED'
    try:
        #print '<stop_display> stopping auto-reconnect'
        rst.stop()
    except:
        pass #print '<stop_display> stopping auto-reconnect FAILED'

def zerns(state='on'):
    """turns on the Zernike analysis of the WFS centroids
    """
    global root, labs, lab_names
    
    if (state == 'on'):
        root = Tk.Tk()
        topline = Tk.Label(root,text='WFS Zernikes',width=15)
        topline.pack()
        lab_names = ['tip','tilt','focus']
        labs = []
        for name in lab_names:
            labs.append(Tk.Label(root,text=name))
        for l in labs:
            l['width'] = 15
            l['relief']='groove'
            l.pack()
        root.update()

__main__.zerns = zerns

helper['fid'] = 'turn on or off fiducials'
def fid(win='wfs',state=True):
    """Turn on or off the ficuials for this display
    """
    for w in win_list:
        if (w.title == win):
            w.fiducial = state

__main__.fid = fid

helper['stats'] = 'reports the number of saturated actuators'
def stats(pc=.1):
    """Report the number of saturated actuators, in the form:
    [ [ # tweeter acts low, # tweeter acts high ],
      [ # woofer acts low, # woofer acts high ]]
    where thresholds are set as a given percentage,
    pc, of the full range. The default is 10%,
    so the low acts are those in the first 10% of the range
    and the high acts are those in the top 90% of the range.
    """
    b = tweeter.range[1] - tweeter.range[0]
    high = (1-pc)*b + tweeter.range[0]
    low = pc*b + tweeter.range[0]
    sat_high = np.sum(np.where(tweeter.data > high,1,0)) - 2
    sat_low = np.sum(np.where(tweeter.data < low,1,0)) - 2
    
    b = woofer.range[1] - woofer.range[0]
    high = (1-pc)*b + woofer.range[0]
    low = pc*b + woofer.range[0]
    wsat_high = np.sum(np.where(woofer.data > high,1,0))
    wsat_low = np.sum(np.where(woofer.data < low,1,0))
    
    return np.array([[sat_low,sat_high],[wsat_low,wsat_high]])

__main__.stats = stats

helper['hist'] = 'display a histogram of the actuator values'
def hist(clear=False):
    """Plot a histogram of the most recent tweeter and woofer actuator
    commands. Repeated calls overlay new histograms.
    Clear the display of old histograms with the keyword clear = True.
    """
    P.figure('act hist')
    if (clear):
        P.clf()
    ax = P.subplot(211)
    n, bins, patches = P.hist(dwindow.woofer_data,10)
    ax.set_xlim(-1,1)
    P.title('woofer')
    P.grid('on')
    P.subplot(212)
    n, bins, patches = P.hist(tweeter.data.flatten(),50,range=(0,43000))
    P.title('tweeter')
    P.grid('on')

__main__.hist = hist

darkBackColors = ['red','green','blue','black','gray','grey']
def Tkpost(name,text,color=None):
    l = labs[lab_names.index(name)]
    l['text']=text
    if (color == None):
        l['bg']='white'
        l['fg']='black'
    else:
        l['bg']=color
        if (color in darkBackColors):
            l['fg']='white'
        else:
            l['fg']='black'
    root.update()

def history():
    for i in range(readline.get_current_history_length()):
        print readline.get_history_item(i)

__main__.history = history

thisOS = os.uname()[0]
def _goodbye():
    global cleaned_up
    stop_display()
    plt.close()
    print 'goodbye'
    cleaned_up = True
    sys.exit()

__main__.goodbye = _goodbye
__main__.exit = _goodbye

def _goofedup():
    if (not cleaned_up):
        print "oh.. you shouldn't have done that"
        print "If this crashes, it's your fault."

atexit.register(_goofedup)

from matplotlib.widgets import Button, RadioButtons
def action_buttons():
    """The action buttons are:
    AutoRange, WFSonly/All4, Connect, Quit
    """
    #global quit_button_ax, quit_button, quit_button_action
    #global autoRange_button_ax, autoRange_button, autoRange_button_action
    global wfsOnly_button_ax, wfsOnly_button, wfsOnly_button_action
    global rax, rad, range_action
    
    plt.subplots_adjust(bottom=.1,left=.05)
    
    #quit_button_ax = plt.axes([.8,.02,.12,.05])
    #def quit_button_action(event):
    #    stop_display()
    #    plt.close()
    #    _goodbye()
    #quit_button = Button(quit_button_ax, 'Quit', color='white', hovercolor='0.7')
    #quit_button.on_clicked(quit_button_action)

    rax = plt.axes([.92,.06,.075,.1])
    try:
        rax.get_frame().set_alpha(0)
    except:
        pass
    rad = RadioButtons(rax,('Auto','Max'),active=0)
    def range_action(label):
        if (label == 'Auto'):
            drange('auto')
        elif (label == 'Max'):
            drange('max')
    rad.on_clicked(range_action)

    #autoRange_button_ax = plt.axes([.65,.02,.12,.05])
    #def autoRange_button_action(event):
    #    name = autoRange_button.label.get_text()
    #    if (name == 'Auto'):
    #        drange('auto')
    #        name = 'Max'
    #    else:
    #        drange('max')
    #        name = 'Auto'
    #    autoRange_button.label.set_text(name)
    #autoRange_button = Button(autoRange_button_ax, 'Auto', color='white', hovercolor='0.7')
    #autoRange_button.on_clicked(autoRange_button_action)
    
    wfsOnly_button_ax = plt.axes([.895,.01,.1,.05])
    def wfsOnly_button_action(event):
        name = wfsOnly_button.label.get_text()
        if (name == 'WFS Only'):
            wfs_only()
            name = 'All4'
        else:
            all4()
            name = 'WFS Only'
        action_buttons()
        wfsOnly_button.label.set_text(name)
        
    wfsOnly_button = Button(wfsOnly_button_ax, 'WFS Only', color='white', hovercolor='0.7')
    wfsOnly_button.on_clicked(wfsOnly_button_action)
    
    
#=======================
# at startup

start()
zoom()
drange('auto')
action_buttons()

if (__name__ == '__main__'):
    def close_event(event):
        print 'closed window'
        _goodbye()
    fig.canvas.mpl_connect('close_event', close_event)
    plt.show()
