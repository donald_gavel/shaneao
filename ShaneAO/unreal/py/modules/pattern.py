"""Write patterns to the tweeter or woofer DM.
This code can be run intependent of the rtc
and the wavefront sensor cameras:

.. code-block:: python

    bash$ python
    >>> import pattern
    
"""

import os
import readfits as fits
import numpy as np
import tweeter
#import zernike
import __main__

config = {'realDM': True,
          'livePlot': False}

if config['realDM']:
    import dpio2
    foo = dpio2.Output(1,2048)
    foo.write([0,]*1024)
    foo.write([0,]*1024)
    print '**********************************************************'
    print '* IMPORTANT - now re-cycle power to the Cambridge driver *'
    print '**********************************************************'
    #send.foo = foo

if config['livePlot']:
    import matplotlib.pyplot as plt
    plt.ion()
    plt.figure('DM')

home = os.environ['HOME']
param_dir = 'parameterFiles'
data_dir = 'data'

map = fits.readfits(os.path.join(home,param_dir,'tweeter_map.fits'))
map = map.flatten()

a = np.zeros((32,32))
__main__.a = a
aw = np.zeros((52))

buffer = [0.]*1024
tw = tweeter.tweeter()

def files():
    l = os.listdir(".")
    for u in l:
        if (u.endswith("fits")):
            print u

__main__.files = files

def reset():
    global foo
    del foo
    foo = dpio2.Output(1,2048)
    foo.write([0,]*1024)

__main__.reset = reset

def _send():
    global a,buffer

    a[0,0] = 0
    a[0,31] = 0
    a[31,0] = 0
    a[31,31] = 0
    a_clipped = np.clip(a,0,65535)
    a *= 0
    a += a_clipped
    b = np.zeros(1024).astype(long)
    b[map] = a_clipped.astype(long).reshape(1024)
    buffer = list(b)

    if config['realDM']:
        foo.write(buffer)
    
    if config['livePlot']:
        plt.figure('DM')
        plt.clf()
        plt.imshow(a,interpolation='nearest',cmap='bone',origin='lower')
        plt.draw()

def send(file,DM='tweeter',units='range'):
    """send a pattern from a fits file to the DM
    
    If units = 'range' (the default) then the values in the
    fits between 0 and 1 represent the full range of the DM.
    If units = 'DN' then the values are assumed absolute (0 to 65535 for
    the tweeter).
    """
    global a

    dm = fits.readfits(file)
    
    if (dm.size != 1024):
        return
    dm = dm.reshape((32,32))
    a += 0.
    a += np.clip(dm,0.,1.) * 65535
    _send()

__main__.send = send
    
def zero(DM='tweeter'):
    """send all zeros (no voltage) to the DM.
    For the tweeter this is the relaxed, or highest possible surface
    position. For the woofer, this is the middle of it's [-1,+1]
    command range.
    """
    global a

    a *= 0.
    _send()
    
__main__.zero = zero

def sphere(DM='tweeter'):
    """flatten the DM by applying its default_actuator.fits data
    """
    global a, a0, aw0

    #ad = fits.readfits(os.path.join(home,param_dir,'actuator_defaults.fits'))
    ad = fits.readfits(os.path.join(home,data_dir,'BestFlat_FullAp_Bias132_wPower.fits'))
    ad = ad.reshape((1024))*65535
    a0 = ad[0:1024].reshape((32,32))
    aw0 = ad[1024:1024+52]

    a *= 0
    a += a0
    _send()

__main__.sphere = sphere

def flat(DM='tweeter'):
    """flatten the DM by applying its default_actuator.fits data
    """
    global a, a0, aw0

    #ad = fits.readfits(os.path.join(home,param_dir,'actuator_defaults.fits'))
    ad = fits.readfits(os.path.join(home,data_dir,'BestFlat_FullAp_Bias132.fits'))
    ad = ad.reshape((1024))*65535
    a0 = ad[0:1024].reshape((32,32))
    aw0 = ad[1024:1024+52]

    a *= 0
    a += a0
    _send()

__main__.flat = flat

def poke(kind,pos=None,val=1,DM='tweeter',units='range'):
    """
    creates a pattern of pokes on the deformable mirror.
    
    kind = "-", "|", "+", "x", "#", "||", "=", "F", ".", or "*"

    * val = value of poke. This is normalized to the range of the DM.
    val can take on settings between 0 (less
    voltage than midrange) to +1 (more voltage than midrange).
    * pos = position of the poke(s), this can be a 1, 2 or 4 element list,
    depending on the kind of poke::
    
        - : a horizontal line at pos=[row]
        | : a vertical line at pos=[column]
        + : a cross at pos=[row,column]
        x : a 45 cross, pos not used
        # : two vertical and two horizontal lines at pos=[row1,row2,col1,col2]
        || : two vertical lines at pos = [col1,col2]
        = : two horizontal lines at pos = [row1,row2]
        F : a unique orientation pattern, pos not used
        . : a single actuator at pos = [row,col]
        * : all actuators, pos not used
    
    * units = 'range' or 'DN'. If 'range' use the fancy 0 to +1
    of full range, as described above.
    If 'DN" assume that val is given in digital numbers sent to the
    DN (ranging from 0 to 65535).
    
    The pokes are cumulative. To reset, use the zero() or flat()
    command.
    """
    global a
    validUnits = ['range','DN']
    validPatterns = ['-','|','+','x','#','||','=','F','.','*','all']

    if (kind not in validPatterns):
        raise ValueError("unsupported pattern. choose from "+str(validPatterns))
    
    if (units == 'range'):
        val = val*65535
    elif (units == 'DN'):
        pass
    else:
        raise ValueError("invalid units. choose from "+str(validUnits))

    if (kind == '-'):
        if (pos == None):
            pos = [16]
        y = pos[0]
        a[y,:] = val
    elif (kind == '|'):
        if (pos == None):
            pos = [16]
        x = pos[0]
        a[:,x] = val
    elif (kind == '+'):
        if (pos == None):
            pos = [16,16]
        x,y = pos
        a[y,:] = val
        a[:,x] = val
    elif (kind == "x"):
        for k in range(32):
            a[k,k] = val
            a[k,31-k] = val
    elif (kind == "="):
        if (pos == None):
            pos = [11,21]
        y1,y2 = pos
        a[y1,:] = val
        a[y2,:] = val
    elif (kind == "||"):
        if (pos == None):
            pos = [11,21]
        x1,x2 = pos
        a[:,x1] = val
        a[:,x2] = val
    elif (kind == "#"):
        if (pos == None):
            pos = [11,21,11,21]
        y1,y2,x1,x2 = pos
        a[y1,:] = val
        a[y2,:] = val
        a[:,x1] = val
        a[:,x2] = val
    elif (kind == 'F'):
        a[4:27,8] = val
        a[16,8:16] = val
        a[27,8:21] = val
    elif (kind == '.'):
        if (pos == None):
            pos = [16,16]
        x,y = pos
        a[y,x] = val
    elif (kind == '*' or kind == 'all'):
        a *= 0
        a += val
    
    _send()

__main__.poke = poke

def zern(n,m,val=1,ap=True,units='range'):
    """Put a zernike pattern on the DM
    val is the peak-to-valley, as a fraction of range.
    negative val is allowed.
    This pattern is added cumulatively, so to start
    at mid range use flat() or poke('all',*mid*)
    """
    global a
    
    na = tw.na
    r,th = zernike.polar_array(Rmax=na/2.-.55,DS=1,pr=na/2)
    z = zernike.zernike(n,m,r,th,limit=False)
    z = z/(z.max()-z.min())
    z *= val
    
    if (units=='range'):
        z *= 65535
    if (ap):
        z *= tw.ap
    a += z
    _send()

__main__.zern = zern

