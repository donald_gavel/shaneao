"""
Flip-map - exchange and/or reverse axes on the tweeter map
to make it correspond and align with the wavefront sensor

The tweeter map is used as::

    buffer[map] = a

where a is the data as stored in the state vector and buffer is
the array sent to the tweeter driver

Since map = [[386,919,...,12]...[20,228, ... ,925]] this means
buffer[386] = a[0]
buffer[919] = a[1]
...
buffer[12] = a[31]
...
buffer[925] = a[1023]

i.e.,  map(x) is the address in the buffer
in which to put data(x)

relevant routines:
* np.swapaxes
* np.fliplr
* np.flipud

"""
import os
import numpy as np
import readfits as fits
from readfits import readfits, writefits
import datetime
import pyfits

module_file_name = __name__+'.py'

def gen_map():
    map = fits.readfits(
        os.path.join(
            os.environ['HOME'],'parameterFiles','tweeter_map.fits'
            )
        )
    
    map_swap = np.swapaxes(map,0,1)
    
    hdu = pyfits.PrimaryHDU(map_swap)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','MEMS DM'))
    hdu.header.append(('FUNC','tweeter descramble map'))
    hdu.header.append(('SYMBOL','tweeter_map'))
    hdu.header.append(('ASTEP','1.6',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    hdu.header['COMMENT'] = 'tweeter_map with x and y axes flipped'
    
    filename = os.path.join(
        os.environ['HOME'],'parameterFiles','tweeter_map_flipxy.fits'
        )
    fits.protectFile(filename)
    hdu.writeto(filename)

