#! /usr/bin/env kpython
#
#  This routine will read the tweeter map and the desired tweeter shape
#  and output it to the dpio2 driver
#
import dpio2
from readfits import *
from numpy import *
import os

def files():
    l = os.listdir(".")
    for u in l:
        if (u.endswith("fits")):
            print u

def send(file):
    map = readfits("tweeter_map.fits")
    if (map.size != 1024):
        return
    map = map.reshape((32,32))
    dm = readfits(file)
    if (dm.size != 1024):
        return
    dm = dm.reshape((32,32))
    dm = clip(dm,0.,1.) * 65535
    dm = dm.astype(long)
    b = zeros(1024).astype(long)
    b[map] = dm.reshape(1024)
    buffer = list(b)
    send.foo.write(buffer)
    
def send_flat():
    send(file)
    
def send_zero():
    buffer = [0,]*1024
    send.foo.write(buffer)

foo = dpio2.Output(1,2048)
send.foo = foo
send_zero()
send_zero()
file = '2013_11_19_BestFlat_FullAp_Bias132.fits'
print '**********************************************************'
print '* IMPORTANT - now re-cycle power to the Cambridge driver *'
print '**********************************************************'

