# -*- coding: utf-8 -*-
"""
This file contains things which are relevant to rtc3.so, the 3rd RTC incarnation which handles hybrid reconstruction etc.
"""

import os

def try_rtc3():
    """Try RTC3"""
    import rtc3
    # De-allocate previous camera interface.
    shaneao.wfs = None
    
    # Munge the internal reference to the rtc module.
    shaneao.rtc.rtc2 = rtc3
    shaneao.self = shaneao.rtc.rtc(shaneao.config['mode'])
    _rtc_startup()
    rtc3.hybrid(np.zeros((160,160), dtype=np.uint16))

def activate_rtc3():
    """Activate rtc3"""
    import rtc3
    # De-allocate previous camera interface.
    shaneao.wfs = None
    
    # Munge the internal reference to the rtc module.
    shaneao.rtc.rtc2 = rtc3
    shaneao.state['rtc'] = 'rtc3.c'
    shaneao.state['rtc_so'] = 'rtc3.so'
    shaneao.state['handler'] = 'c_hybrid'
    
    shaneao.self = shaneao.rtc.rtc(shaneao.config['mode'])
    _rtc_startup()
    
    
    # Allocate a new camera interface.
    _camera_setup()
    _tweeter_setup()
    _woofer_setup()
    
    _data_server_setup()
    
    try:
        state.setNamespace(__main__)
    except AttributeError:
        pass
    
    # Needed to synchronize rate parameter.
    rate()

def deactivate_rtc3():
    """Deactivate the RTC3 module."""
    import rtc2
    # De-allocate previous camera interface.
    shaneao.wfs = None
    
    # Munge the internal reference to the rtc module.
    shaneao.rtc.rtc2 = rtc2
    shaneao.state['rtc'] = 'rtc2.c'
    shaneao.state['rtc_so'] = 'rtc2.so'
    shaneao.state['handler'] = 'c_recon'
    
    
    for key in ['kalman_bleed', 'hybridcm', 'hybrid_mode']:
        shaneao.state.pop(key, None)
    
    shaneao.self = shaneao.rtc.rtc(shaneao.config['mode'])
    
    # Allocate a new camera interface.
    _camera_setup()
    _tweeter_setup()
    _woofer_setup()
    
    _data_server_setup()
    
    try:
        state.setNamespace(__main__)
    except AttributeError:
        pass
    

def setup_rtc():
    """Set up the RTC data."""
    hybridcm()
    kalman_bleed()
    shaneao.rtc.rtc2.set_size("ng", 32)
    shaneao.rtc.rtc2.allocate_telemetry()
    shaneao.state['ng'] = 32
    # Initialize arrays that self won't otherwise touch.
    hybridzero()
    
def hybridcm(name='hybridMatrix.fits'):
    """Hybrid controlmatrix."""
    # Load control matricies
    import pyfits
    
    parameterFiles = shaneao.rtc.paramsDir
    
    hdr = pyfits.getheader(os.path.join(parameterFiles, name))
    shaneao.self.controlMatrix_phase = pyfits.getdata(os.path.join(parameterFiles, name), ext=1)
    shaneao.rtc.rtc2.load("controlMatrix_phase",shaneao.self.controlMatrix_phase.astype(np.double))
    
    shaneao.self.controlMatrix_slopes =  pyfits.getdata(os.path.join(parameterFiles, name), ext=0)
    shaneao.rtc.rtc2.load("controlMatrix_slopes", shaneao.self.controlMatrix_slopes.astype(np.double))
    shaneao.state['hybridcm'] = name
    shaneao.state['hybrid_mode'] = hdr['OUTPUTS']
    
def kalman_bleed(bleed=0.995):
    """Set the internal bleed."""
    shaneao.rtc.rtc2.load("kalman_bleeds", np.ones((1038,), dtype=np.double) * bleed)
    shaneao.state['kalman_bleed'] = float(bleed)
    
def hybridzero():
    """Zero in hybrid mode."""
    e = np.zeros((1038,), dtype=np.double)
    shaneao.rtc.rtc2.load("e", e.flatten())
    shaneao.rtc.rtc2.load("ei", e.flatten())
    shaneao.rtc.rtc2.load("ef", e.flatten())
    shaneao.rtc.rtc2.load("ez", e.flatten())
    shaneao.rtc.rtc2.load("et", e.flatten())
    
def patched_set_gain(gain=None, wgain=None):
    """Monkeypatch the set_gain method"""
    self = shaneao.self
    patched_set_gain.original(gain, wgain)
    na, nw, nf, ns, ntts = (self.na_tw, self.na_woof, self.nfilt, self.ns, self.ntts)
    self.cmp = self.controlMatrix_phase.copy() #: cm is the control matrix after it has been multiplied by the gain
    self.cmp[0:na,:] *= self.gain
    self.cmp[na:na+nw,:] *= self.wgain
    self.cmp[na+nw:na+nw+nf,:] *= (1.-self.alpha)
    if (self.mode.endswith('LGS')):
        self.cmp[na+nw+nf:na+nw+nf+ntts,:] *= self.ugain
        p_tt = 2*ns + nf
        self.cmp[:,p_tt:p_tt+2] *= self.tipTilt_rgain
        self.cmp[:,0:2*ns] *= self.wfs_switch
    shaneao.rtc.rtc2.load("controlMatrix_phase",self.cmp.astype(np.double))

def _rtc_startup():
    """Do all the things Don does at the module level required to start the RTC.
    
    Also, yo, see here: https://filippo.io/instance-monkey-patching-in-python/
    This is so evil. I can't stand it.
    """
    import types
    setup_rtc()
    
    # Monkey patch at least set-gain.
    patched_set_gain.original = shaneao.self.set_gain
    shaneao.self.set_gain = patched_set_gain
    patched_set_gain(shaneao.self.gain, shaneao.self.wgain)
    
def _woofer_setup():
    """Set up the woofer."""
    if (shaneao.config['woofer'] and not shaneao.config['simulator']):
        woof('off')
        shaneao.woofer.aw = shaneao.rtc.rtc2.peek('a')[1024:1024+52]
        #woofer.peek = rtc.rtc2.peek # allows a peek into the state vector to do uplink tip/tilt
        shaneao.state['wrate'] = shaneao.woofer.rate
        woof('on')
    
def _tweeter_setup():
    """Set up for the tweeter."""
    if shaneao.config['tweeter']:
        print "<shaneao> dpio2.Output = "+str(shaneao.tweeter)
        shaneao.tweeter.write([0,]*1024)
        # Once the dpio2 device is initialized as an output device, the card
        # should no longer be putting out noise on its outputs. It is now safe
        # to enable power to the driver electronics, and should remain so until
        # the control host is reset or power-cycled.
        shaneao.rtc.rtc2.set_dpio2_output(shaneao.tweeter)
    else:
        shaneao.rtc.rtc2.set_dpio2_output(None)

def _camera_setup():
    """Set up things for the camera."""
    shaneao.reset()
    shaneao.ttreset()
    shaneao.rtc.rtc2.load('wfs_background',shaneao.the_wfs_dark.astype('double'))
    shaneao.rtc.rtc2.load('tt_background',shaneao.the_tts_dark.astype('double'))
        
def _data_server_setup():
    """Set up the data server."""
    import dataServer
    rtc = shaneao.rtc
    config = shaneao.config
    
    dataServer.i_map = shaneao.i_map
    dataServer.ccd = rtc.rtc2.peek('wfs').reshape((160,160))
    dataServer.sub = rtc.rtc2.peek('wfs_subaps')
    dataServer.inten = rtc.rtc2.peek('inten')
    dataServer.a = rtc.rtc2.peek('a')
    dataServer.da = rtc.rtc2.peek('da')
    dataServer.tts = rtc.rtc2.peek('tts').reshape((80,80))
    dataServer.ttbg = rtc.rtc2.peek('tt_background').reshape((80,80))
    dataServer.wfs_dark = shaneao.the_wfs_dark
    dataServer.tts_dark = shaneao.the_tts_dark
    shaneao.the_wfs_flat = np.zeros((160,160))
    shaneao.the_wfs_flat[:] = shaneao.self.wfs_flat
    dataServer.wfs_flat = shaneao.the_wfs_flat

    config['has tts_ma'] = True
    if (config['has tts_ma']):
        print '<shaneao> setting up for tip/tilt moving average'
        shaneao.tts_ma = np.zeros((80*80))
        rtc.rtc2.load('tts_ma',shaneao.tts_ma.astype(np.double))
        shaneao.tts_ma_alpha = np.ones((1))*0.99
        rtc.rtc2.load('tts_ma_alpha',shaneao.tts_ma_alpha.astype(np.double))
        dataServer.tts_ma = rtc.rtc2.peek('tts_ma').reshape((80,80))  # tip/tilt sensor moving average

    config['has wfs_ma'] = True
    if (config['has wfs_ma']):
        print '<shaneao> setting up for wavefront sensor moving average'
        shaneao.wfs_ma = np.zeros((160*160))
        rtc.rtc2.load('wfs_ma',shaneao.wfs_ma.astype(np.double))
        shaneao.wfs_ma_alpha = np.ones((1))*0.99
        rtc.rtc2.load('wfs_ma_alpha',shaneao.wfs_ma_alpha.astype(np.double))
        dataServer.wfs_ma = rtc.rtc2.peek('wfs_ma').reshape((160,160))  # wavefront sensor moving average

    dataServer.rtc_instance = shaneao.self
    dataServer.status = shaneao.status
    dataServer.command = shaneao._command
    dataServer.slide = shaneao.slide  # callback for image sharpening refcent modes slider
    dataServer.refcent_save = shaneao.refcent_save # callback for saving the refcents and zeroing the sliders
    dataServer.estop = shaneao.estop # callback for emergency stop sent from a remote GUI
    