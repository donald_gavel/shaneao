"""woofer_speed2.py
tests a threading approach to handling woofer ticks
"""
import time
from threading import Thread
import numpy as np
import woofer_client

t_set = []
alive = 1
mu = .0035
aw = np.zeros((52))

class worker(Thread):
	def run(self):
		print 'worker thread started'
		t0 = time.time()
		for x in xrange(0,20000):
			tau = time.time()-t0
			t_set.append(tau)
			w.zero()
			time.sleep(mu)
			if (not alive):
				print 'worker thread stopped'
				return

class waiter(Thread):
	def run(self):
		global w
		time.sleep(.1)
		show()
		del w

def run():
	global t0, alive, t_set,w
	w = woofer_client.woofer_client(aw)
	t_set = []
	alive = 1
	t0 = time.time()
	aworker = worker()
	aworker.start()
	time.sleep(5)
	print len(t_set)
	stop()
	show()
	plot()

def stop():
	global alive
	alive = 0
	waiter().start()

def show():
	global dt
	n = len(t_set)
	dt = []
	for k in range(n-1):
		dt.append(t_set[k+1]-t_set[k])
	print 'average: '+str(np.average(dt))
	print 'std dev: '+str(np.std(dt))

import imp
try:
	imp.find_module('matplotlib')
	found = True
except:
	found = False
	
if (found):
	import matplotlib.pyplot as plt

	def plot():
		plt.ion()
		plt.figure(1)
		plt.plot(t_set,'.')
		plt.grid('on')
		plt.figure(2)
		n = len(dt)/10
		(y,x) = np.histogram(dt,bins=n,range=(0,.015))
		x = x[0:len(x)-1]
		plt.plot(x,y)
		plt.grid('on')


