# take data
import time
import threading

def data():
	print 'collecting telemetry data'
	shaneao.rtc.rtc2.telemetry_data()
	wait_time = 4096./shaneao.self.rate
	wait_time *= 1.2
	t = threading.Timer(wait_time, data_saveit)
	t.start()

def data_saveit():
	shaneao.rtc.rtc2.telemetry_ready()
	tdata = shaneao.rtc.rtc2.telemetry_peek()
	print 'saving telemetry data to disk'
	data_archive(tdata)

def data_archive(u,comment=''):
	archive_dir = 'data_'+time.strftime('%Y-%m-%d')
	if (not os.path.exists(archive_dir)):
		os.mkdir(archive_dir)
	if (not os.path.exists(os.path.join(archive_dir,'data_seq_no'))):
		seq_no = np.array([0])
		seqhdu = pyfits.PrimaryHDU(seq_no)
		seqhdu.header['seq_no'] = (0,'sequence number')
		seqhdu.writeto(os.path.join(archive_dir,'data_seq_no'))
	if (comment == None):
		comment = raw_input('Enter a description of this data: ')
	seqhdu = pyfits.open(os.path.join(archive_dir,'data_seq_no'),mode='update')
	seq_no = seqhdu[0].data[0]
	tl = time.localtime(time.time())
	day = time.strftime('%Y-%m-%d',tl)
	tim = time.strftime('%H%M%S',tl)
	hdu = pyfits.PrimaryHDU(u)
	hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
	hdu.header.append(('DEVICE','WFS Camera'))
	hdu.header.append(('RATE',shaneao.self.rate,' frame rate'))
	hdu.header.append(('DATE',day,'File generation date'))
	hdu.header.append(('TIME',tim,'file generation time hhmmss'))
	hdu.header.append(('GENER','data.py','data_archive'))
	hdu.header['comment'] = comment
	fitsFileName = os.path.join(archive_dir,'data_'+ '%04d' % seq_no +'.fits')
	print 'writing to ' + fitsFileName
	hdu.writeto(fitsFileName)
	time.sleep(.1)
	seq_no += 1
	seqhdu[0].header['seq_no'] = (seq_no,'sequence number')
	seqhdu[0].data[0] = seq_no
	seqhdu.close()
