# -*- coding: utf-8 -*-
"""
Telemetry recorder tools.

The interface for the telemetry recorder is only available in the ShaneAO
command line interface for now. Please use the box next to the ``>>>``
symbol in ShaneAO to enter telemetry commands.

The telemetry recorder will:
- Only record closed loop telemetry when the system appears settled.
- Only start once the user calls ``telemetry.start()``.
- Won't record additional telemetry if disk space appears low.
- Will stop recording at 6am local time each morning, pending another
  user initiated ``telemetry.start()`` command.

For Operators
=============

To run the telemetry recorder script in the background, use the following::
    
    >>> telemetry.start()

At the end of the night, run::

    >>> telemetry.stop()

To see the status of the telemetry recorder, use::
    
    >>> telemetry.status()
    
If there is ever a problem, and you'd like to immediately stop the script, use::
    
    >>> telemetry.stop(block=False)
    
At the start of the night, please use record open loop telemetry. The telescope
should be pointed at a relatively bright star (one used for calibration is fine)
but with the AO loops open. Then you can call::
    
    >>> telemetry.openloop()
    
Configuring the Telemetry Recorder
==================================

The telemetry recorder is designed to with a few configurable keywords, which can be adjusted through
``telemetry.configure``::
    
    >>> telemetry.configure(ndumps=10, interval=5)
    
Arguments to ``telemetry.configure`` are:

- `ndumps`: The number of consecutive telemetry datasets to record at a time. (Default: ``5``)
- `interval`: The interavel, in minutes, between telemetry data recording. (Default: ``15``)
- `settled`: Either ``True`` or ``False``. When ``True``, it requires that the AO system think
    that the loop is "settled" when starting to record telemetry. (Default: ``True``)
- `max_datasets`: The maximum number of telmetry datasets to record in a single session. (Default: ``128``)

For Engineers
=============

This module provides telemetry recording tools for a multi-threaded environemnt where telemetry recording happens in the background.

The telemetry recorder is an object which maintains two threads, a timing thread and a recording thread. The recording thread maintains a single telemetry recording session, and can be invoked from other threads to properly start and save telemtry data. The timing thread triggers recording of telemetry data continuously in the background. Interfacing with both threads is done through the :class:`TelemetryRecorder` thread.

To use the telemetry recorder::
    
    >>> telemetry = TelemetryRecorder()
    >>> telemetry
    <TelemetryRecorder ndumps=1 interval=1 0/100 stopped>
    >>> telemetry.start()
    
The telemetry recorder uses the parameters *interval* and *ndumps* to control the timer. These two parameters control how many telemetry dumps (*ndumps*) to take every *interval* minutes. These parameters can be adjusted with the :meth:`configure` method, or with the constructor for :class:`TelemetryRecorder`::
    
    >>> telemetry.configure(ndumps=2, interval=15)
    
To record a block of telemetry on demand, use the record method::
    
    >>> telemetry.record()
    
or call the :class:`TelemetryRecorder` instance itself::
    
    >>> telemetry()
    
When calling the instance iteslf, you can record a number of sequential blocks, and you can set a timeout for each record in seconds.::
    
    >>> telemetry(n=5, timeout=40.0)

Background telemetry recording can be paused and resumed at any time::
    
    >>> telemetry.pause()
    >>> telemetry.resume()

The *settled* keyword controls whether the recorder requires the loop to be settled in order to write telemetry data.

"""

try:
    threading
except NameError as e:
    import threading

try:
    telemetry
except NameError as e:
    telemetry = None

class TelemetryShutdown(Exception):
    """Raised to shut down the telemetry auto-recorder."""
    pass
    
def _force_kill_telemetry():
    """Using threading enumerate."""
    for thread in threading.enumerate():
        if thread.name == "TelemetryTimerThread":
            thread.stop()
        if thread.name == "TelemetryRecorderThread":
            thread.stop()
        
    

class TelemetryDataThread(threading.Thread):
    """This thread handles the recording of telemetry at the lowest level.
    
    This class takes no parameters.
    """
    def __init__(self):
        super(TelemetryDataThread, self).__init__(name="TelemetryRecorderThread")
        import logging
        self.daemon = True
        
        self.shutter = threading.Event()
        # Triggered to start recording.
        self.recording = threading.Event()
        # Set while recording is active.
        self.done = threading.Event()
        # Triggered to mark the end of recording.
        self.success = threading.Event()
        # Triggered to mark successful recording.
        
        self.shutdown = threading.Event()
        # Triggered to shutdown the thread.
        
        self.invalid = threading.Event()
        # Triggered to invalidate a dataset.
        
        self.log = logging.getLogger("TelemetryRecorder")
        # Logger to record information about gathered telemetry.
        
        # KTL interface objects
        self._service = ktl.Service('saocon')
        self.keyword = self._service['LOOPSTATE']
        self._loopstate = self.keyword.read()
        
    def _invalidate_callback(self, keyword):
        """A callback to invalidate this particular telemetry case."""
        
        # We only invalidate if we are in the middle of recording telemetry,
        # and the loopstate actually changed from the last state.
        if self.recording.isSet() and self._loopstate != keyword.ascii:
            self.log.debug("Kewyord invalidates telemetry: %r, was %r" % (keyword.ascii, self._loopstate))
            self._loopstate = keyword.ascii
            self.invalid.set()
    
    def run(self):
        """Run this thread."""
        
        # While running the thread, monitor for invalidation.
        self.keyword.callback(self._invalidate_callback)
        self.keyword.monitor(start=True)
        
        try:
            
            # Inner loop to repeatedly call _record when
            # the shutter event is triggered.
            while not self.shutdown.isSet():
                self.shutter.wait(300)
                self._check_shutdown(0.0)
                if self.shutter.isSet():
                    self._record()
        except TelemetryShutdown:
            pass
        finally:
            self.shutter.clear()
            self.recording.clear()
            self.done.set()
            self.keyword.callback(self._invalidate_callback, remove=True)
            self.keyword.monitor(start=False)
        self.log.info("Done with recorder thread.")
        
    def stop(self):
        """Shut down the recording thread."""
        self.shutdown.set()
        self.shutter.set()
        self.invalid.set()
        self.done.set()
        
    @property
    def telemetry_ready(self):
        """Wait for a ready flag."""
        return bool(shaneao.rtc.rtc2.telemetry_ready()) 
        
    def _wait_for_ready_flag(self):
        """Wait for the recorder to be ready."""
        if not self.telemetry_ready:
            self.log.debug("RTC2 Telemetry isn't ready. Waiting...")
            while not self.telemetry_ready:
                self._check_shutdown(1.0)
        
    def _check_shutdown(self, wait=0.0):
        """Check for a shutdown sentinel."""
        self.shutdown.wait(wait)
        if self.shutdown.isSet():
            raise TelemetryShutdown()
        if not _enough_disk_available():
            self.log.error("Not enough disk space free for automated telemetry recording.")
            raise TelemetryShutdown()
    
    def _record(self):
        """Record a single telemetry instance."""
        self.done.clear()
        self.success.clear()
        self._wait_for_ready_flag()
        
        if not self.telemetry_ready:
            return self.success.isSet()
        try:
            
            # Reset the validator flag 
            # and record the initial loop state.
            self.invalid.clear()
            start_status = dict(shaneao.status().items())
            self._loopstate = self.keyword.read()
        
            try:
            
                self.recording.set()
                self.log.debug("Recording...")
                # Fire the telemetry recorder.
                shaneao.self.data()

                # Wait for the telemetry recorder to finish.
                self._check_shutdown(_get_wait_time())
                while not self.telemetry_ready:
                    self._check_shutdown(0.1 * _get_wait_time())
            finally:
                self.recording.clear()
            
            # At the end, check that nothing invalidates this telemetry set.
            end_status = dict(shaneao.status().items())
            if self.compare_status(start_status, end_status):
                shaneao.self.save_data(shaneao.state)
                self.success.set()
        finally:
            # Ensure that we have cleared all of the relevant events.
            self.invalid.clear()
            self.recording.clear()
            self.shutter.clear() # Since we are waiting on "done, we must clear shutter first."
            self.done.set()
        
        return self.success.isSet()
        
    def compare_status(self, start, end):
        """Compare status dictionaries."""
        if not (self.invalid.isSet()) and start == end:
            return True
        
        self.log.info("Loop settings changed during telemetry, ignoring dump...")
        if self.invalid.isSet():
            self.log.debug("LOOPSTATE invalidated telemetry.")
        for key in start.keys():
            if key not in end:
                self.log.debug("Key %s missing at end." % key)
            elif start[key] != end[key]:
                self.log.debug("Key %s different: %r != %r" % (key, start[key], end[key]))
        for key in end.keys():
            if key not in start:
                self.log.debug("Key %s missing at start." % key)
        return False
        
    
    def press_shutter(self):
        """Press the telemetry recording shutter once."""
        if self.shutdown.isSet():
            self.shutter.set()
            self.done.set()
            return False
        elif self.recording.isSet():
            self.log.info("Already recording telemetry.")
        else:
            self.shutter.set()
        return True
    
    def wait_for_completion(self, timeout=None):
        """Wait for telemetry recording to complete."""
        self.done.wait(timeout)
        success = self.success.isSet()
        if self.done.isSet():
            self.done.clear()
            self.success.clear()
        return success
    
    def record(self, timeout=None):
        """Record a single telemetry frame, and block.
        
        This method is designed to be called from an extenral thread
        and used to trigger a single telemetry record.
        """
        if not self.press_shutter():
            return False
        return self.wait_for_completion(timeout=timeout)
        
class TelemetryTimer(threading.Thread):
    """A telemetry timer thread."""
    def __init__(self, recorder):
        super(TelemetryTimer, self).__init__()
        import logging
        self.daemon = True
        
        self.recording = threading.Event()
        # Signal to let parents know we are in the middle
        # of a recording session.
        
        self.shutdown = threading.Event()
        # Triggered to shutdown the thread.
        self._resume = threading.Event()
        self._resume.set()
        # Triggered to resume after a pasue.
                
        self.log = logging.getLogger("TelemetryRecorder")
        # Logger to record information about gathered telemetry.
        
        self.recorder = recorder
        self.datasets = _identify_sequence_number()
        self.next_telemetry = 0
    
    def run(self):
        """Run the timer thread."""
        try:
            while not self.shutdown.isSet():
                
                while not self._resume.isSet():
                    self._resume.wait(300)
                    self.datasets = _identify_sequence_number()
                    
                
                # Immediately record when the next interval should go off.
                self.next_telemetry = (self.interval*60.0) + time.time()
                self.datasets =  _identify_sequence_number()
                
                # Record telemetry using the recorder.
                self.recording.set()
                for i in range(self.ndumps):
                    
                    if not self.ready:
                        self.log.debug("Waiting for AO system.")
                    while not (self.ready or self.shutdown.isSet()):
                        self._check_shutdown(1.0)
                    self.datasets =  _identify_sequence_number()
                    self.log.info("Recording telemetry #%d" % (self.datasets + 1))
                    if self.recorder.record():
                        self.datasets += 1
                self.recording.clear()
                self._check_shutdown(0.1)
                
                # Increment the next_telemetry timer, in case
                # we ran through out allotted telemetry
                # dump time on the last round.
                now = time.time()
                if self.next_telemetry < now:
                    self.next_telemetry = (self.interval*60.0) + now
                
                # Wait until we shuold record the next telemetry.
                self.log.debug("Waiting for %.0fs until %s for next telemetry dump.",
                    max(self.next_telemetry - time.time(), 0.1), 
                    time.strftime("%X", time.gmtime(self.next_telemetry)))
                
                while self.next_telemetry > time.time():
                    self._check_shutdown(max(self.next_telemetry - time.time(), 0.1))
            
        except TelemetryShutdown:
            pass
        finally:
            # Clear any shutdown events.
            self.shutdown.clear()
        
        self.log.info("Finished telemetry timer recording.")
        return
        
    @property
    def ready(self):
        """Wait for a ready flag."""
        return ((not self.recorder.recording.isSet()) 
            and bool(shaneao.rtc.rtc2.telemetry_ready()) 
            and (not self.settled or (self.recorder.keyword.read() == 'Settled')))
    
    def _check_shutdown(self, wait=0.0):
        """Check for a shutdown sentinel."""
        self.shutdown.wait(wait)
        if self.shutdown.isSet():
            raise TelemetryShutdown()
        if self.datasets >= self.max_datasets:
            raise TelemetryShutdown()
        if hasattr(self, 'expire') and time.time() >= self.expire:
            raise TelemetryShutdown()
        if not _enough_disk_available():
            self.log.error("Not enough disk space free for automated telemetry recording.")
            raise TelemetryShutdown()
            
    def stop(self):
        """Stop the timer object."""
        self.shutdown.set()
        self._resume.set()
            
    def pause(self):
        """Pause this thread."""
        self._resume.clear()
        
    def resume(self):
        """Resume thread operations."""
        self._resume.set()
    
class TelemetryRecorder(object):
    """A class to manage long-term telemetry recording.
    
    Parameters
    ----------
    interval : float
        The interval between automated recording sessions, in minutes.
    ndumps : int
        The number of consecutive telemetry dumps to take.
    
    """
        
    def __init__(self, interval=15, ndumps=4, settled=True, max_datasets=128, level="INFO", expire=True):
        super(TelemetryRecorder, self).__init__()
        
        self._setup_logging(level)
        
        # This should be a long running thread that
        # works for the entire time the telemetry recorder is alive.
        self.recorder = TelemetryDataThread()
        self.recorder.start()
        
        # This is the timer thread which runs in the backround.
        self.timer = TelemetryTimer(self.recorder)
        
        # Set the timer parameters.
        self.timer.interval = interval
        self.timer.ndumps = ndumps
        self.timer.settled = settled
        self.timer.max_datasets = max_datasets
        self._expire = None
        
        # Reset and launch everything.
        self._reset()
        
        if expire:
            # Expire this telemetry recorder at 6am
            self.expire("06:00:00")
        
    def expire(self, _time):
        """Set an expiration for the timer thread.
        
        Expirations are set in LOCAL TIME (including accounting for DST).
        Times which occur before right now are set to tomorrow.
        """
        import datetime, time
        self._expire = datetime.datetime.combine(datetime.date.today(), 
            datetime.datetime.strptime(_time, "%H:%M:%S").time())
        self._update_expire()
        
    def _update_expire(self):
        """Update expiration time, including bumping the date if necessary."""
        import datetime, time
        
        # If the expiration time has already past, add 24hrs.
        if self._expire < datetime.datetime.now():
            self._expire += datetime.timedelta(days=1)
        
        self.log.info("Set to expire at {0:s}.".format(self._expire.strftime("%H:%M:%S")))
        self.timer.expire = time.mktime(self._expire.timetuple())
        
    def _setup_logging(self, level="INFO"):
        """Set up logging"""
        import logging
        self.log = logging.getLogger(self.__class__.__name__)
        self._handler = logging.StreamHandler()
        self._handler.setFormatter(logging.Formatter("%(levelname)-8s: %(message)s"))
        if len(self.log.handlers) != 0:
            del self.log.handlers[:]
        self.log.addHandler(self._handler)
        
        self.level = level
        
    @property
    def level(self):
        """Get logger level."""
        return self.log.getEffectiveLevel()
    
    @level.setter
    def level(self, level):
        """Set the logger level."""
        import logging
        level = getattr(logging, level, level)
        self._handler.setLevel(level)
        self.log.setLevel(level)
        
    def _reset(self):
        """Reset all thread-like variables"""
        if not self.recorder.isAlive():
            self.recorder = TelemetryDataThread()
            self.recorder.start()
            
        if not self.timer.isAlive():
            interval, ndumps, settled, max_datasets = self.timer.interval, self.timer.ndumps, self.timer.settled, self.timer.max_datasets
            self.timer = TelemetryTimer(self.recorder)
            self.timer.interval = interval
            self.timer.ndumps = ndumps
            self.timer.settled = settled
            self.timer.max_datasets = max_datasets
            if self._expire is not None:
                self._update_expire()
            
    @property
    def datasets(self):
        """Number of datasets so far recorded."""
        return _identify_sequence_number()
    
    def start(self):
        """Start the recorder process."""
        self._reset()
        if self.timer.isAlive():
            self.log.warning("Recording is already active.")
        else:
            self.timer.start()
            
    def resume(self):
        """Resume the recorder."""
        if not self.timer.isAlive():
            self.log.warning("Recording is not active. Call .start() first.")
        self.timer.resume()
        
    def pause(self):
        """Pause the recorder."""
        if not self.timer.isAlive():
            self.log.warning("Recording is not active. Call .start() first.")
        self.timer.pasue()
        
    def record(self, timeout=None):
        """Record telemetry."""
        return self.recorder.record(timeout=timeout)
        
    def stop(self, block=True, timeout=None):
        """Stop the telemetry recorder"""
        self.recorder.stop()
        self.timer.stop()
        if block and self.timer.isAlive():
            if threading.currentThread() is not self.timer:
                print("Waiting for timer to finish.")
                self.timer.join(timeout)
        if block and self.recorder.isAlive():
            if threading.currentThread() is not self.recorder:
                print("Waiting for recorder to finish.")
                self.recorder.join(timeout)
        
    
    def configure(self, interval=None, ndumps=None, settled=None, max_datasets=None, level=None, expire=None):
        """Configure the timer."""
        if interval is not None:
            self.timer.interval = interval
        if ndumps is not None:
            self.timer.ndumps = ndumps
        if settled is not None:
            self.timer.settled = settled
        if max_datasets is not None:
            self.timer.max_datasets = max_datasets
        if level is not None:
            self.level = level
        if expire is True:
            self.expire("06:00:00")
    
    def __del__(self):
        """Destructor to ensure thread is removed."""
        self.stop(block=True)
        
    def _thread_state(self):
        """Check thread state."""
        if not self.timer.isAlive():
            return "stopped"
        elif not self.timer._resume.isSet():
            return "pasued"
        return "running"
        
    def __repr__(self):
        """An active automated telemetry dumper."""
        return "<{0} ndumps={1:d} interval={2:.0f} {3:d}/{4:d} {5:s} {6:s}>".format(
            self.__class__.__name__,
            self.timer.ndumps, self.timer.interval,
            self.datasets, self.timer.max_datasets,
            "expires at {0:s}".format(self._expire.strftime("%H:%M:%S")) if self._expire is not None else "not expiring",
            self._thread_state()
        )
        
    def __call__(self, n=1, timeout=None):
        """Call serves to record right now."""
        count = 0
        print "Recording {0:d} (#{1:d} to #{2:d}) datasets right now.".format(n, self.datasets + 1, self.datasets + 1 + n)
        for i in range(n):
            if self.record(timeout=timeout):
                count += 1
                print "Recorded dataset #{0:d}".format(self.datasets)
        return count
        
    def openloop(self):
        """Record open loop data using the current settings."""
        n = self.timer.ndumps
        print "Recording {0:d} (#{1:d} to #{2:d}) open loop datasets now.".format(
            n, self.datasets + 1, self.datasets + 1 + n
        )
        print "Please ensure that the AO system is illuminated with a natural guide star,"
        print "But that the loops are open."
        
        for i in range(n):
            self.record()
            print "Recorded dataset #{0:d}".format(self.datasets)
        print "Recorded {0:d} (#{1:d} to #{2:d}) open loop datasets.".format(
            n, self.datasets - n, self.datasets)
    
    def status(self):
        """Print a status message about the recorder/timer."""
        print repr(self)
        if not self.timer.isAlive():
            print "Stopped. Call .start() to activate."
        elif self.timer.recording.isSet():
            if not self.recorder.recording.isSet():
                print "Waiting for AO System before recording dataset #{0:d}".format(self.datasets + 1)
            else:
                print "Recording dataset #{0:d}".format(self.datasets + 1)
        else:
            print "Waiting {0:.0f}s until {1:s} to record dataset #{2:d}".format(
                max(self.timer.next_telemetry - time.time(), 0.1), 
                time.strftime("%X", time.gmtime(self.timer.next_telemetry)),
                self.datasets + 1
            )
    
# Function to get wait time.
def _get_wait_time():
    """Get telemetry wait time."""
    samples = 4097.               # number of samples taken by each data() command
    rate = float(shaneao.self.rate) # AO system rate in Hz
    dumpLength = samples/rate     # length in time of each data() dump
    waitTime   = dumpLength+1.0   # 1.0s padding for data write time
    return waitTime

def _identify_sequence_number():
    """Identify the sequence number for telemetry data."""
    import os, time
    home = os.environ['HOME']
    now = time.time()
    now_tuple = time.localtime(now)

    if now_tuple.tm_hour < 12:
        target = now - 86400
        target = time.localtime(target)
    else:
        target = now_tuple

    subdirectory = time.strftime('%Y-%m-%d', target)

    archive_dir = os.path.join(home,'telemetry', subdirectory)
    seq_no = 0

    if os.path.exists(archive_dir):
        existing = os.listdir(archive_dir)

        for filename in existing:
            digits = []
            for character in filename:
                if character.isdigit():
                    digits.append(character)

            number = ''.join(digits)
            if number != '':
                number = int(number)
                seq_no = max(number, seq_no)
    
    return seq_no
    
def _disk_available(path):
    """Get the disk usage for a path."""
    import os
    st = os.statvfs(path)
    free = st.f_bavail * st.f_frsize
    return free

def _enough_disk_available(n_margin=1000, path=None):
    """Is enough disk space available?"""
    import os, time
    home = os.environ['HOME']
    if path is None:
        path = os.path.join(home,'telemetry')
    free = _disk_available(path)
    telsize = 43 * 1024 * 1024 # 43MB per telemetry file.
    return free > n_margin * telsize

if telemetry is None:
    telemetry = TelemetryRecorder()
else:
    try:
        telemetry.stop()
        del telemetry
    except Exception:
        pass
    telemetry = TelemetryRecorder()
    