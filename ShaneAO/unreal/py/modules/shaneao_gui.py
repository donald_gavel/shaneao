#!/usr/bin/python
"""ShaneAO GUI
invoke with

shaneao_gui

graphical user interface to shaneao's commandServer
"""
#http://forums.devshed.com/python-programming-11/tkinter-set-button-bk-color-933906.html
#http://infohost.nmt.edu/tcc/help/pubs/tkinter/web/ttk-theme-layer.html

import os
import sys
import Tkinter as tk
import ttk
import socket
import threading

home = os.environ['HOME']
host = 'localhost'
port = 8889
check_dt = 1.0
check_timer = None

#================================
#   dealing with the socket
#
def connect():
    global s
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    try:
        s.connect((host,port))
    except:
        s = None

def drop():
    if (s == None): return
    s.close()

def send(command):
    if (s == None):
        print 'tried to send '+command
        print 'but not connected to shaneao commandServer'
        return 'ERR'
    s.send('rval='+command)
    r = s.recv(1024)
    return r

#===============================
#  dealing with the GUI
#
style = ttk.Style()
style.theme_use('classic')
#style.configure('TLabel',background='white')
style.configure('wfs.TButton',background='white',relief='raised',padding=30,width=10,font='helvetica 14')
style.configure('tt.TButton',background='white',relief='raised',padding=30,width=10,font='helvetica 14')
style.configure('ul.TButton',background='white',relief='raised',padding=30,width=10,font='helvetica 14')

def init():
    global root, tt_button, wfs_button, uplink_button, mode_label
    mlab = ttk.Label(text='ShaneAO Control Panel')
    mlab.pack(pady=12)
    mode_label = ttk.Label(text = 'Mode: unknown')
    mode_label.pack()
    tt_button = ttk.Button(text = 'Tip Tilt',
                           style = 'tt.TButton',
                           command = tt)
    tt_button.pack()
    wfs_button = ttk.Checkbutton(text = 'WFS',
                                style = 'wfs.TButton',
                                command = wfs)
    wfs_button.pack()
    uplink_button = ttk.Checkbutton(text = 'Uplink',
                                    style = 'ul.TButton',
                                    state='disabled')
    uplink_button.pack()
    check_state()
    
def check_state(repeat=True):
    global sao_state, check_timer
    
    rval = send('status()')
    if (rval == 'ERR'): return
    sao_state = eval(rval)
    if (sao_state['subState'] == 'ngs' or (not sao_state['mode'].endswith('LGS'))):
        mode_label.configure(text='Mode: NGS')
    else:
        mode_label.configure(text='Mode: LGS')
        
    if (sao_state['loop'] == 'closed'):
        if (sao_state['subState'] == 'tt only'):
            style.configure('tt.TButton',background='green',relief='sunken')
            style.configure('wfs.TButton',background='white',relief='raised')
            style.map('tt.TButton',background=[('active','lightgreen')])
            style.map('wfs.TButton',background=[('active','#ececec')])

        elif (sao_state['subState'] == 'wfs only'):
            style.configure('tt.TButton',background='white',relief='raised')
            style.configure('wfs.TButton',background='green',relief='sunken')
            style.map('tt.TButton',background=[('active','#ececec')])
            style.map('wfs.TButton',background=[('active','lightgreen')])

        elif (sao_state['subState'] == 'lgs' or sao_state['subState'] == 'ngs'):
            style.configure('tt.TButton',background='green',relief='sunken')
            style.configure('wfs.TButton',background='green',relief='sunken')
            style.map('tt.TButton',background=[('active','lightgreen')])
            style.map('wfs.TButton',background=[('active','lightgreen')])

    elif (sao_state['loop'] == 'open'):
        style.configure('tt.TButton',background='white',relief='raised')
        style.configure('wfs.TButton',background='white',relief='raised')
        style.map('tt.TButton',background=[('active','#ececec')])
        style.map('wfs.TButton',background=[('active','#ececec')])
    
    if (repeat):
        check_timer = threading.Timer(check_dt,check_state)
        check_timer.start()

def tt():
    """This requests a tt_close or a tt_open
    """
    loop = sao_state['loop']
    subState = sao_state['subState']
    
    if (subState == 'wfs only' or loop == 'open'):
        send('tt_close()')
    elif (loop == 'closed'):
        send('tt_open()')
    check_state(repeat=False)

def wfs():
    loop = sao_state['loop']
    subState = sao_state['subState']
    
    if (loop == 'open'):
        if (subState == 'lgs'):
            send('wfs_close()')
        elif (subState == 'ngs'):
            send('close()')
    elif (loop == 'closed'):
        if (subState == 'wfs only'):
            send('wfs_open()')
        elif (subState == 'tt only'):
            send('wfs_close()')
        elif (subState == 'lgs'):
            send('wfs_open()')
        elif (subState == 'ngs'):
            send('open()')
    check_state(repeat=False)

connect()
init()
tk.mainloop()
if (check_timer != None):
    check_timer.cancel()
drop()