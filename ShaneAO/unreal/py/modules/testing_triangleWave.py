import sys
import time
import dpio2

foo = dpio2.Output (1, 2048)

maxvalue = 43000
ncycles = 0
buffer = [0,]*1024
index = 0
while (ncycles < 100):
	print "cycle ",ncycles
	value = 0
	while value < maxvalue:
		buffer[index] = value
		foo.write (buffer)
#		time.sleep(0.001)
		value += 100
	while value > 0:
		buffer[index] = value
		foo.write (buffer)
#		time.sleep(0.001)
		value -= 100
	ncycles += 1

print "done"
