#!/usr/bin/env python -i
"""ttpeeko shows the tip/tilt sensor output
and allows user interaction to pick a centroiding region
"""
import os
os.chdir(os.environ['HOME'])
machineOS = os.uname()[0]

if ('machine' not in dir()):
    execfile('startup.py')

import numpy as np
import matplotlib
hasTkAgg = False
#if (machineOS == 'Darwin'):
#    hasTkAgg = True
#    if (hasTkAgg):
#        matplotlib.use('TkAgg')
#        if (machine == 'Mac Mini'):
#            geo = '654x536+1800+700'
#        if (machine == 'MacBook Air'):
#            geo = '654x536+780+200'
from matplotlib import pyplot as plt
from matplotlib import animation
from matplotlib.patches import Rectangle
import socket
import struct
import signal
import atexit
import readline
import sys
from threading import Thread, Timer
import time
import pydoc
import __main__

helper = {}

def crib(item=None):
    """Quick help:
    help() lists all the commands with brief description.
    help(item) to get help on a particular command
    """
    bling = '============================'
    if (item == None):
        print bling
        print '   ttpeeko help: \n'
        for k in helper.keys():
            print k+' -- '+ helper[k]
        crib(crib)
        print bling
        
        return
    print pydoc.render_doc(item,title='%s')

__main__.crib = crib
helper['help'] = 'get help on peeko commands'
__main__.help = crib

plt.ion()

sys.stdout.write("\x1b]2;TT Peeko\x07")

cam_bits = 14
cam_max = 2**cam_bits - 1
cam_size = (80,80)

def init():
    global bias, im
    rlist = []
    bias = np.zeros(cam_size)
    bias[0:40,0:40] = 500
    bias[0:40,40:80] = 503
    bias[40:80,0:40] = 497
    bias[40:80,40:80] = 498
    im = plt.imshow(bias,interpolation='nearest',cmap='bone',vmin=0,vmax=cam_max,origin='lower')
    rlist.append(im)
    return rlist

fake_camera = False
def show_noise(i):
    global data
    rlist = []
    if (fake_camera):
        noise = np.random.normal(size=cam_size,scale=6.0)
        data = bias + noise
        clim = (480,520)
    else:
        data = np.zeros(cam_size)
        for k in range(cam_size[0]):
            data[k,k] = cam_max
            data[k,cam_size[0]-k-1] = cam_max
        #data = np.random.normal(loc=cam_max/2.,scale=cam_max/4.,size=cam_size)
        #clim = (0,cam_max)
    im.set_array(data)
    #im.set_clim(clim)
    rlist.append(im)
    return rlist

rect = None
rect_user = None
def _pick(fig,ax,w=10,lw=4,color='b',alpha=.2):
    global rect_user
    print 'pick a point (escape to exit)'
    
    rl = fig.ginput() # blocking point picker
    if (rl == []):
        return None
    r, = rl
    x = np.round(r[0]+.5)
    y = np.round(r[1]+.5)
    print r
    if (rect_user != None):
        rect_user.remove()
    rect_user = Rectangle([x-w/2-.5,y-w/2-.5],w,w,facecolor=color,alpha=alpha,linewidth=lw) # draw a box there
    ax.add_patch(rect_user)
    plt.draw()
    return (r,w)

host = 'localhost'
sock = None
paused = True

helper['connect'] = 'connect to the ShaneAO data server' 
def connect():
    """Connect to the ShaneAO data server.
    
    See also: sethost()
    """
    global sock, remote_ip, port, paused, rst
    try:
        sock.close()
    except:
        pass
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(4)
    port = 8888
    remote_ip = socket.gethostbyname(host)
    try:
        sock.connect((remote_ip,port))
        sock.settimeout(None)
        paused = False
    except:
        print '<connect> no connection to server '+host
        paused = True
        try:
            rst.stop()
        except:
            pass
        rst = restart_thread()
        rst.start()

__main__.connect = connect

valid_hosts = ['real','real.ucolick.org','scab','scab.ucolick.org','morely','morely.ucolick.org','rtc','rtc.ucolick.org','localhost']
helper['sethost'] = 'set the server host'
def sethost(ahost=None):
    """set or query the server host machine name
    sethost('?") returns a list of valid server names
    """
    global host
    if (ahost == '?'):
        return valid_hosts
    if (ahost == None):
        return host
    if (ahost in valid_hosts):
        host = ahost
        print 'host set to '+str(host)
    else:
        print 'invalid host '+str(host)+'; must be one of '+str(valid_hosts)

__main__.sethost = sethost

nbytes = 80*80*8
form = str(80*80)+'d'
data = np.zeros(cam_size)
nbytes_roi = 4*8

data_request = 'tt'
helper['display'] = 'set or query what image is being displayed'
def display(item=None):
    """
    set or query the displayed image. Choices are:
    display('tts') - the raw tip/tilt sensor
    display('tts_br') - the background removed tip/tilt sensor
    display('tts_ma') - moving average of the tip/tilt sensor - background.
                        (set the averaging time constant in shaneao)
    display() - tell which image is now being displayed
    """
    global data_request
    
    if (item == None):
        if (data_request == 'tt'):
            print '<display> raw tip/tilt sensor'
        elif(data_request == 'ttbr'):
            print '<display> tip/tilt sensor, background removed'
        elif(data_request == 'tts_ma'):
            print '<display> tip/tilt sensor moving average'
        else:
            print '<display> unknown'
        return
    valid_data_list = ['tts','tts_br','tts_ma']
    if (item not in valid_data_list):
        print '<display> '+str(item)+' is not a valied display item'
        print '<display> choose from '+str(valid_data_list)
        return
    if (item == 'tts'):
        data_request = 'tt' # backward compatible; 'tt' is what the data server knows
        plt.title('tt sensor')
    elif (item == 'tts_br'):
        data_request = 'ttbr'
        plt.title('tt sensor - background')
    elif (item == 'tts_ma'):
        data_request = 'tts_ma'
        plt.title('tt sensor (moving average)')

__main__.display = display

def get_and_show(i):
    global paused, data,roi_center,roi_size
    
    if (not paused):
        try:
            sock.sendall(data_request)
            signal.alarm(2)
            b = recv_all(sock,nbytes)
            signal.alarm(0)
            r = np.array(struct.unpack(form,b))
            data = r.reshape(cam_size)
        except:
            print '<get_and_show> failed to get tt data'
            print sys.exc_info()
            paused = True
        try:
            sock.sendall('roi')
            signal.alarm(2)
            b = recv_all(sock,nbytes_roi)
            signal.alarm(0)
            r = np.array(struct.unpack('4d',b))
            roi_center = r[0:2]
            roi_size = r[2:4]
        except:
            print '<get_and_show> failed to get roi data'
            print sys.exc_info()
            paused = True

    elif (paused):
        return show_noise(i)
    rlist = []
    im.set_array(data)
    rlist.append(im)
    draw_roi()
    return rlist

def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

def catch_timeout(signum,frame):
    global paused, sock, rst
    print '<catch_timeout> signal '+str(signum)
    paused = True
    if (sock != None):
        sock.close()
        sock = None
    try:
        rst.stop()
    except:
        pass
    rst = restart_thread()
    rst.start()

signal.signal(signal.SIGALRM,catch_timeout)

rect_user = None
def onclick(event):
    global rect_user
    
    x,y = event.xdata, event.ydata
    if (x == None or y == None):
        return None
    x = np.round(x+.5)
    y = np.round(y+.5)
    print 'x=%d, y=%d'%(x,y)
    w = roi['size']
    lw = roi['line']
    color = roi['user_color']
    alpha = roi['alpha']
    if (rect_user != None):
        rect_user.remove()
    rect_user = Rectangle([x-w/2-.5,y-w/2-.5],w,w,facecolor=color,alpha=alpha,linewidth=lw) # draw a box there
    ax.add_patch(rect_user)
    plt.draw()
    roi['position'] = (x,y)
    fig.canvas.mpl_disconnect(cid)
    
    return (x,y)

showing_roi = True
rect = None
def draw_roi():
    global rect
    if (rect != None):
        rect.remove()
    x,y = roi_center
    wx,wy = roi_size
    lw = roi['line']
    color = roi['color']
    alpha = roi['alpha']
    if (showing_roi):
        rect = Rectangle([x-wx/2-.5,y-wy/2-.5],wx,wy,facecolor=color,alpha=alpha,linewidth=lw)
        ax.add_patch(rect)
        plt.draw()

compass_angle = 0.
compass_lines = None
tt_fieldRotation = 24.3 # degrees - see param_gen.py - this is the fixed rotation angle between tip/tilt sensor and sky right
base_tub_angle = 74.0 # degrees, East from North - see scripts.py - this is the reported tub rotation when ShaneAO is rectilinear with RA and DEC

helper['compass'] = 'draw the N-E compass on the tt sensor display given TUB-reported angle'
def compass(tub_angle=base_tub_angle,sky_right=True,axes='NE'):
    """draw the N-E compass on the tt sensor display
    
    Argument:
        tub_angle - angle reported by the TUB, degrees
    """
    angle = -(tub_angle - base_tub_angle)
    angle += tt_fieldRotation-90 # -90 is empirical, photo IMG_1541 Aug 12, 2014
    if (not sky_right):
        angle = -(tub_angle - base_tub_angle)
        angle -= tt_fieldRotation-90 # -90 is empirical, photo IMG_1541 Aug 12, 2014        
    draw_compass(angle,sky_right=sky_right,axes=axes)

__main__.compass = compass

def draw_compass(angle=None,sky_right=True,axes='NW'):
    """put a compass rose on the display
    N and W axes. if angle = 0, N is up, W is right
    Increasing angle is east from north
    
    Arguments:
        angle - angle of compass, ccw, in degrees
    Keyword args:
        sky_right - True: west is CW from N
                    False: west is CCW from N
        axes - which axes to show: 'NW' = North and West
                                   'NE' = North and East
    """
    global compass_lines, char_EW, char_N
    if (angle == None):
        angle = compass_angle
    degrees = np.pi/180.
    angle = angle*degrees
    if (not sky_right):
        angle = -angle
    vertex = (15,15)
    length = 10
    char_dist = 4
    if (compass_lines):
        undraw_compass()
    if (axes == 'NW'):
        #------- West ---------
        compass_lines = []
        x = np.array([0,length])
        if (not sky_right):
            x[1] = -x[1]
        y = np.array([0,0])
        xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
        yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
        compass_lines.append(plt.plot(xr,yr,color='red'))
        x = length+char_dist
        if (not sky_right):
            x = -x
        y = 0
        xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
        yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
        char_EW = plt.text(xr,yr,'W',color='red')
    if (axes == 'NE'):
        #------- East ---------
        compass_lines = []
        x = np.array([0,-length])
        if (not sky_right):
            x[1] = -x[1]
        y = np.array([0,0])
        xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
        yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
        compass_lines.append(plt.plot(xr,yr,color='red'))
        x = -(length+char_dist)
        if (not sky_right):
            x = -x
        y = 0
        xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
        yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
        char_EW = plt.text(xr,yr,'E',color='red')
    #-------- North --------
    x = np.array([0,0])
    y = np.array([0,length])
    xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
    yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
    compass_lines.append(plt.plot(xr,yr,color='red'))
    x = 0
    y = length+char_dist
    xr = x*np.cos(angle) - y*np.sin(angle) + vertex[0]
    yr = x*np.sin(angle) + y*np.cos(angle) + vertex[1]
    char_N = plt.text(xr,yr,'N',color='red')

def undraw_compass():
    global compass_lines
    if compass_lines:
        lx = compass_lines[0]
        ly = compass_lines[1]
        lx.pop(0).remove()
        ly.pop(0).remove()
        char_EW.remove()
        char_N.remove()
        compass_lines = []
        plt.draw()

helper['hide_roi'] = 'hide the roi box'
def hide_roi():
    """Turn off display of the roi box
    """
    global showing_roi, rect
    showing_roi = False
    rect.remove()
    rect = None

__main__.hide_roi = hide_roi

helper['show_roi'] = 'show the roi box'
def show_roi():
    """Turn on display of the roi box
    """
    global showing_roi
    showing_roi = True

__main__.show_roi = show_roi

def pick_old():
    global the_anim, cbar
    
    the_anim._stop()
    result = _pick(fig,ax)
    plt.clf()
    anim()
    if (result != None):
        ax.add_patch(rect)
    return result

helper['pick'] = 'pick a place to put a fiducial box'
def pick():
    """pick a place to put a fiducial box on the display. This is
    separate from the roi box and does nothing other than
    indicate an intersting point chosen by the user
    """
    global cid
    cid = fig.canvas.mpl_connect('button_press_event',onclick)
    print 'Pick a point in the image to be the center'

__main__.pick = pick

def setpick(roi_size=10, line_width = 4, roi_color='r', roi_alpha=.2, roi_user_color='b'):
    global roi
    
    roi['size'] = roi_size
    roi['line'] = line_width
    roi['color'] = roi_color
    roi['user_color'] = roi_user_color
    roi['alpha'] = roi_alpha

roi = {'size':6, 'line':4, 'color':'r', 'alpha':0.2, 'user_color':'w'}

def anim():
    global fig,ax,the_anim, cbar
    global autorange_timer
    
    fig = plt.figure('ShaneAO - tt peeko')
    the_init_func = init
    the_anim = animation.FuncAnimation(fig,get_and_show,init_func=the_init_func,interval=20,blit=False)
    cbar = plt.colorbar(im)
    plt.show()
    ax = fig.axes[0]
    ax.set_xlim(0,cam_size[0])
    ax.set_ylim(0,cam_size[1])
    def format_coord(x,y):
        col = int(x+0.5)
        row = int(y+0.5)
        numrows, numcols = data.shape
        if col>=0 and col<numcols and row>=0 and row<numrows:
            z = data[row,col]
            return '[%1.0f, %1.0f] %1.2f'%(x, y, z)
        else:
            return '[%1.0f, %1.0f]'%(x, y)
    ax.format_coord = format_coord
    plt.title('tt sensor')
    autorange_timer = Timer(autorange_dt,autorange_check)
    autorange_timer.start()
    connect()

__main__.start = anim

autorange_timer = None
autorange_dt = 2.

def stop_anim():
    global the_anim, sock
    
    try:
        the_anim._stop()
    except:
        pass
    try:
        autorange_timer.cancel()
    except:
        pass
    try:
        sock.sendall('bye')
        sock.close()
        sock = None
    except:
        pass
    try:
        rst.stop()
    except:
        pass

autorange = False
helper['drange'] = 'set the display range'
def drange(clim):
    """change the display for a particular data range:
    drange() prints the range
    drange(r) sets the display range. r must be a 2-tuple (display min, max)
    drange('max') sets the display range to the full range of the camera
    drange('auto') tells the display to auto range
    """
    global autorange
    autorange = False
    if (clim == 'max'):
        im.set_clim((0,cam_max))
        return
    if (clim == 'auto'):
        span = data.max() - data.min()
        if (span == 0):
            span = cam_max
        top = data.max() + 0.2*span
        bot = data.min() - 0.2*span
        clim = (bot,top)
        autorange = True
    im.set_clim(clim)

__main__.drange = drange

def autorange_check():
    global autorange_timer
    if autorange:
        drange('auto')
    autorange_timer = Timer(autorange_dt,autorange_check)
    autorange_timer.start()

class restart_thread(Thread):
    """Try every so often to restore communications to the shaneao system
    """
    def __init__(self):
        super(restart_thread,self).__init__()
        self.wait = 2.0 # seconds
        self.alive = True
        print '<restart_thread.__init__> ',self
    
    def run(self):
        global paused, sock
        while (paused):
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.settimeout(2)
                sock.connect((remote_ip,port))
                sock.settimeout(None)
                print '<restart_thread> connected'
                #time.sleep(self.wait)
                paused = False
            except:
                #print '<restart_thread> tried... no connection'
                time.sleep(self.wait)
                if (not self.alive):
                    break
        return
    
    def stop(self):
        #print 'stopping restart thread'
        self.alive = False
        #print 'waiting for thread to terminate'
        print '<restart_thread.stop> ',self
        self.join()
    
helper['history'] = 'list the commands history'
def history():
    """print a list of the last umpteen commands
    """
    for i in range(readline.get_current_history_length()):
        print readline.get_history_item(i)

__main__.history = history

def _goodbye():
    global cleaned_up
    stop_anim()
    print 'goodbye'
    cleaned_up = True
    sys.exit()

__main__.goodbye = _goodbye
__main__.exit = _goodbye

def _goofedup():
    if (not cleaned_up):
        print "oh.. you shouldn't have done that"
        print "If this crashes, it's your fault."

atexit.register(_goofedup)

#=======================
# at startup
usage = """
--------------------
USAGE:
    ttpeeko -p --host hostname
OPTIONS:
    -np means run without the python prompt
    --host hostname connect to the shaneao host. default is real.ucolick.org
    -h, --help print help
--------------------
"""

host = 'real.ucolick.org'
prompt = True
if (__name__ == '__main__'):
    getting_host = False
    argv = sys.argv[:]
    argv.pop(0)
    for arg in argv:
        if (getting_host):
            host = arg
            getting_host = False
            continue
        if (arg == '--host'):
            getting_host = True
            continue
        elif (arg == '-np'):
            prompt = False
            continue
        elif (arg == '-h' or arg == '--help'):
            print usage
            os._exit(0)
        else:
            print '<peeko> invalid option '+arg
            print usage
            os._exit(0)

    if (prompt):
        plt.ion()
    
    sethost(host)
    start()
    drange('auto')
    manager = plt.get_current_fig_manager()
    if (hasTkAgg):
        manager = plt.get_current_fig_manager()
        manager.window.geometry(geo)
