#!/usr/bin/env python
"""List all the fits files
in home/data that are valid refcent files

save the list to a text file

"""
import os
import sys
import pyfits

home = os.environ['HOME']
def lrf(mode):
    if (mode == '8x'):
        ns,nf = [40,14]
    elif (mode == '16x'):
        ns,nf = [144,14]
    else:
        print '<lrf> invalide mode. must be one of 8x, 16x'
        return
    
    file_list = os.listdir(os.path.join(home,'data','.'))
    name_list = []
    for fileName in file_list:
        if (fileName.endswith('.fits')):
            hdu = pyfits.open(fileName)
            hdr = hdu[0].header
            if ('NAXIS1' in hdr):
                n = hdr['NAXIS1']
                if (n == ns*2 or n == ns*2+nf):
                    name_list.append(fileName)
            hdu.close()
    name_list = sorted(name_list)
    return name_list

def save(name_list,filename=None):
    if (filename == None):
        filename = os.path.join(home,'data','refcent_file_list_'+mode+'.txt')
    f = open(filename,'w')
    for name in name_list:
        f.write(name+'\n')
    f.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "wrong number of arguments, type lrf --help"
    else:
        mode = sys.argv[1]
        if (mode == '--help'):
            print 'lrf mode ; lists all valid refcents files in the current directory'
            print '  mode must be 8x or 16x'
        else:
            save(lrf(mode))
            

