"""tweeter.py
   routines in support of the ShaneAO tweeter DM
"""

import numpy as np
import img

class tweeter:
    """
    The tweeter class defines the MEMS deformable mirror geometry for
    the purpose of reconstructor development.
    A tweeter object is used in conjuction with a :py:class:`wfs` object.
    """
    
    def __init__(self):
        
        n = 32
        self.na = n
        self.nacts = n*n
        self.name = 'BMC KiloDM'
        r = 14
        rs = 4
        self.beamSize = 2*r
        self.obscuration = 2*rs
        self.da = 1.0          # actuator spacing
        self.dx = 0.1          # fine grid for the purposes of display
        self.ap = img.circle((n,n),r=r)-img.circle((n,n),r=rs)
        x = (np.arange(0,self.na) - self.na/2)*self.da
        y = x
        self.actLocs = []
        for xx in x:
            for yy in y:
                self.actLocs.append((xx,yy))
        self.actDNRange = [0,65535]
        self.actDNRange__doc__ = """actuator valid command range, digital numbers """
        self.stroke = 3.5e-6
        self.stroke__doc__ = """surface deflection x 2, peak to valley, meters"""
        self.pitch = 350.e-6
        self.pitch__doc__ = """actuator pitch, physical, meters"""
        
    def show(self):
        n = (self.na+2)*(self.da/self.dx)
        u = np.zeros((n,n))
        dx = self.da/self.dx
        x0 = n/2
        y0 = x0
        for x in self.actLocs:
            u[x[0]*dx+x0,x[1]*dx+y0] = 1.
        ap = img.circle((n,n),r=self.beamSize*dx/2) - img.circle((n,n),r=self.obscuration*dx/2.)
        u = u + ap
        return u

    def type(self):
        return self.__class__.__name__

    