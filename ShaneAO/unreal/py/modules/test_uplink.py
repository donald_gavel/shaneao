"""raw test of an uplink control channel

usage:

import test_uplink as tu
chan = 0
volts = 5
tu.test_uplink(chan,volts)

volts should be between 0 and 10
"""

import socket
import time
import numpy as np
import threading

# globals for uplink_loop() test
volts = 5.
dt = 2.

def test_uplink(chan,volts):
    ip = '192.168.2.213'
    port=1091
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.settimeout(4.)
    s.connect((ip,port))
    #chan = str(1)
    chan=str(chan)
    #volts = str(0.0)
    volts=str(volts)
    eos = '\r\n'
    pd = 'AO'+chan+','+volts+eos
    s.send(pd)
    u = s.recv(500)
    print u
    s.close()

def upl_on():
	global s
	ip = '192.168.2.213'
	port=1091
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
	s.settimeout(4.)
	s.connect((ip,port))	

def upl_off():
	s.close()

def upl(chan_num,v=0):
	chan = str(chan_num)
	volts = str(v)
	#ip = '192.168.2.213'
	#port=1091
	#s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
	#s.settimeout(4.)
	#s.connect((ip,port))
	eos = '\r\n'
	pd = 'AO'+chan+','+volts+eos
	s.send(pd)
	u = s.recv(500)
	#print u
	#s.close()

def upl_close():
	global running
	running = True
	upl_loop()

def upl_open():
	global running
	running = False

def upl_loop():
	global volts
	if (running):
		upl(0,volts)
		if (volts == 5.):
			volts = 7.
		else:
			volts = 5.
		t = threading.Timer(dt,upl_loop)
		t.start()
