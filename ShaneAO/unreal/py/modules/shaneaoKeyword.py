''' Define the DFW.Keyword instances used by shaneao.py.
'''

import DFW
import ktl
import os
import math
import time

import shaneaoHeartbeat


saocon = ktl.Service ('saocon')
stub   = ktl.Service ('stub')



def tubAngleCallback (position):

    radians = position.binary
    passthrough = tubAngleCallback.passthrough

    if radians == None or passthrough == None:
        return

    degrees = radians * 180 / math.pi
    passthrough (degrees)

tubAngleCallback.passthrough = None



class StatusDictionary:
    ''' The StatusDictionary behaves like a dictionary for the basic
        usage in shaneao.py, but behind the scenes, it broadcasts keywords
        whenever a status value is set.
    '''

    service = None

    def __init__ (self, mapping={}, **kwargs):

        self._status = {}

        # For any dictionary functions that are not defined by this class,
        # map any calls directly to the internal status dictionary.

        for name in dir (self._status):
            if hasattr (self, name):
                pass
            else:
                setattr (self, name, getattr (self._status, name))


        if StatusDictionary.service == None:

            lroot = os.environ['LROOT']
            stdiosvc = os.path.join (lroot, 'data/saocon/stdiosvc_2.conf')
            service = DFW.Service('saocon', stdiosvc, self.setupKeywords, '+service+_dispatch_2')
            StatusDictionary.service = service


        self.update (mapping, **kwargs)


    def __getitem__ (self, key):

        return self._status[key]


    def __setitem__ (self, key, value):
        ''' In addition to setting the value in the internal dictionary,
            if there is a keyword established for this key, broadcast the
            new value via that keyword.
        '''

        self._status[key] = value

        try:
            keyword = self._lookup[key]

        except KeyError:
            pass

        else:
            keyword = StatusDictionary.service[keyword]

            if isinstance (value, float):
                value = repr (value)
            else:
                value = str (value)

            keyword.set (value)


    def update (self, mapping={}, **kwargs):

        mapping = dict (mapping)
        mapping.update (kwargs)

        for key in mapping.keys ():

            self[key] = mapping[key]


    def setupKeywords (self, service):

        lookup = {}
        self._lookup = lookup

        lookup['camera']     = 'WFSCAMSTATE'
        lookup['mems']       = 'TWEETERSTATE'
        lookup['mode']       = 'MODE'
        lookup['offloading'] = 'OFFLOAD'
        lookup['rate']       = 'WFSCAMRATE'
        lookup['subState']   = 'SUBSTATE'
        lookup['ttcamera']   = 'TTCAMSTATE'
        lookup['ttrate']     = 'TTCAMRATE'
        lookup['tt file number'] = 'TTFILENUM'
        lookup['tt rGain']   = 'TTGAIN'
        lookup['tt saved rGain'] = 'STTGAIN'
        lookup['wfs file number'] = 'WFSFILENUM'
        lookup['woofer']     = 'WOOFERSTATE'
        lookup['wrate']      = 'WOOFERRATE'

        lookup['controlMatrix_filename'] = 'MATRIXFILE'
        lookup['refCent_filename'] = 'REFCENTS'
        lookup['tweeter_bleed']    = 'TWEETERBLEED'
        lookup['uplink bleed']     = 'UPLINKBLEED'
        lookup['uplink enabled']   = 'UPLINK'
        lookup['uplink gain']      = 'UPLINKGAIN'
        lookup['uplink loop']      = 'UPLINKLOOP'
        lookup['uplink_offload_x'] = 'LASER_OFFLOAD_X'
        lookup['uplink_offload_y'] = 'LASER_OFFLOAD_Y'
        lookup['uplink savedGain'] = 'SUPLINKGAIN'
        lookup['woofer_bleed']     = 'WOOFERBLEED'

        shaneaoHeartbeat.Heartbeat ('DISP2HRT', service)

        LaserOffload ('LASER_OFFLOAD_X', service)
        LaserOffload ('LASER_OFFLOAD_Y', service)

        Bleed ('TWEETERBLEED', service, 'tweeter')
        Bleed ('WOOFERBLEED', service, 'woofer')

        CommandPassthrough ('BIASZERO', service)
        CommandPassthrough ('FLAT', service)
        CommandPassthrough ('FLATZERO', service)
        CommandPassthrough ('SAVEDATA', service)
        CommandPassthrough ('SAVEFLAT', service)
        CommandPassthrough ('THAW', service)
        CommandPassthrough ('TTCAMDARK', service)
        CommandPassthrough ('TTCAMRESET', service)
        CommandPassthrough ('TTCAMSTART', service)
        CommandPassthrough ('TTCAMSTOP', service)
        CommandPassthrough ('TTCLOSE', service)
        CommandPassthrough ('TTOPEN', service)
        CommandPassthrough ('UPLINKCLOSE', service)
        CommandPassthrough ('UPLINKOPEN', service)
        CommandPassthrough ('WFSCAMDARK', service)
        CommandPassthrough ('WFSCAMRESET', service)
        CommandPassthrough ('WFSCAMSTART', service)
        CommandPassthrough ('WFSCAMSTOP', service)
        CommandPassthrough ('WFSCLOSE', service)
        CommandPassthrough ('WFSOPEN', service)
        CommandPassthrough ('ZERO', service)
        EnumeratedPassthrough ('MODE', service)
        EnumeratedPassthrough ('SUBSTATE', service)
        EnumeratedIntegerPassthrough ('TTCAMRATE', service)
        EnumeratedPassthrough ('TWEETERSTATE', service)
        EnumeratedIntegerPassthrough ('WFSCAMRATE', service)
        EnumeratedPassthrough ('WOOFERSTATE', service)

        DoublePassthrough ('TTGAIN', service)
        DoublePassthrough ('STTGAIN', service)
        DoublePassthrough ('UPLINKBLEED', service)
        DoublePassthrough ('UPLINKGAIN', service)
        DoublePassthrough ('SUPLINKGAIN', service)
        StringPassthrough ('REFCENTS', service)

        SmartTTGain ('TT', service)
        SmartGain ('UPLINK', service)

        ControlMatrix ('CONTROLMATRIX', service)

        Filename ('TWEETERFLAT', service)
        Filename ('WOOFERFLAT', service)
        LoadFlat ('LOADFLAT', service)

        Freeze     ('FREEZE', service)
        LoopState  ('TTLOOP', service, 'tt')
        LoopState  ('WFSLOOP', service, 'wfs')
        Offload    ('OFFLOAD', service)
        Offload    ('UPLINK', service)
        WooferRate ('WOOFERRATE', service)

        OpMode ('OPMODE', service)
        Init ('INIT', service)
        SmartMode ('SMARTMODE', service)

        Interval ('TTCAMINTERVAL', service, 'tt')
        Interval ('WFSCAMINTERVAL', service, 'wfs')
        ProcessingTime ('TTCAMPROTIME', service, 'tt')
        ProcessingTime ('TWEETERPROTIME', service, 'tweeter')
        ProcessingTime ('WFSCAMPROTIME', service, 'wfs')
        Sampler ('TT', service)
        Sampler ('WFS', service)

        UplinkCounter ('UPLINKCOUNTER', service)


    def setNamespace (self, namespace, final=False):
        ''' Receive the __main__ from the shaneao module, and provide direct
            access to specific functions to specific keyword implementations.
            This enables those keywords to handle modify requests.
        '''

        service = StatusDictionary.service

        namespacers = ('TTCAMINTERVAL', 'TTCAMPROTIME', 'TWEETERPROTIME',
                       'UPLINKCOUNTER',
                       'WFSCAMINTERVAL', 'WFSCAMPROTIME')

        for keyword in namespacers:
            keyword = service[keyword]
            keyword.setNamespace (namespace)


        pairs = []
        pairs.append (('CONTROLMATRIX', 'set_CM'))
        pairs.append (('BIASZERO', 'bias0'))
        pairs.append (('FLAT', 'flat'))
        pairs.append (('FLATZERO', 'flat0'))
        pairs.append (('FREEZE', 'freeze'))     # two passthroughs for Freeze
        pairs.append (('FREEZE', 'resume'))     # two passthroughs for Freeze
        pairs.append (('INIT', 'init'))
        pairs.append (('LOADFLAT', 'get_flat'))
        pairs.append (('MODE', 'mode'))
        pairs.append (('OFFLOAD', 'offload'))
        pairs.append (('REFCENTS', 'refcent'))
        pairs.append (('SAVEDATA', 'data'))
        pairs.append (('SAVEFLAT', 'save_flat'))
        pairs.append (('STTGAIN', 'saved_tt_gain'))
        pairs.append (('SUBSTATE', 'substate'))
        pairs.append (('SUPLINKGAIN', 'saved_uplink_gain'))
        pairs.append (('THAW', 'thaw'))
        pairs.append (('TTCAMDARK', 'ttdark'))
        pairs.append (('TTCAMRATE', 'ttrate'))
        pairs.append (('TTCAMRESET', 'ttrs'))
        pairs.append (('TTCAMSTART', 'ttcamstart'))
        pairs.append (('TTCAMSTOP', 'ttcamstop'))
        pairs.append (('TTCAM_SAMPLESTART', 'tt_cam_run'))
        pairs.append (('TTCLOSE', 'tt_close'))
        pairs.append (('TTGAIN', 'tt_gain'))
        pairs.append (('TTOPEN', 'tt_open'))
        pairs.append (('TWEETERBLEED', 'bleed'))
        pairs.append (('TWEETERSTATE', 'mems'))
        pairs.append (('UPLINKBLEED', 'uplink_bleed'))
        pairs.append (('UPLINKCLOSE', 'uplink_close'))
        pairs.append (('UPLINKGAIN', 'uplink_gain'))
        pairs.append (('UPLINKOPEN', 'uplink_open'))
        pairs.append (('UPLINK', 'uplink'))
        pairs.append (('WFSCAMDARK', 'dark'))
        pairs.append (('WFSCAMRATE', 'rate'))
        pairs.append (('WFSCAMRESET', 'rs'))
        pairs.append (('WFSCAMSTART', 'camstart'))
        pairs.append (('WFSCAMSTOP', 'camstop'))
        pairs.append (('WFSCAM_SAMPLESTART', 'wfs_cam_run'))
        pairs.append (('WFSCLOSE', 'wfs_close'))
        pairs.append (('WFSOPEN', 'wfs_open'))
        pairs.append (('WOOFERBLEED', 'bleed'))
        pairs.append (('WOOFERRATE', 'wrate'))
        pairs.append (('WOOFERSTATE', 'woof'))
        pairs.append (('ZERO', 'zero'))


        for pair in pairs:
            keyword, function = pair

            try:
                function = getattr (namespace, function)
            except AttributeError:
                # setNamespace() is invoked in multiple places,
                # and the full suite of functions is not available
                # on the earlier runs.

                if final == True:
                    raise
                else:
                    continue

            try:
                service[keyword].setPassthrough (function)
            except AttributeError:
                # Map this to a different exception, because the caller
                # is catching and ignoring AttributeError exceptions as
                # a way to accomodate the simulator mode.
                raise RuntimeError, "%s must have a setPassthrough() function, but does not" % (keyword)


        # Set up callbacks for the tub angle.

        try:
            tubAngleCallback.passthrough = namespace.tub_angle
        except AttributeError:
            if final == True:
                raise
        else:
            position = stub['TUBPA']
            position.monitor (wait=False)
            position.callback (tubAngleCallback)


# end of class StatusDictionary



class DoublePassthrough (DFW.Keyword.Double):

    def __init__ (self, *args, **kwargs):

        self.passthrough = None
        DFW.Keyword.Double.__init__ (self, *args, **kwargs)


    def set (self, value, force=False):

        try:
            float (value)
        except:
            # Not a number. The bleed() function accepts arrays,
            # we don't have a good way to handle those here.
            return

        if isinstance (value, float):
            value = repr (value)
        else:
            value = str (value)

        DFW.Keyword.Double.set (self, value, force)


    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        self.passthrough = function


    def write (self, value):

        value = float (value)
        self.passthrough (value)


    def postwrite (self, *ignored):
        ''' The passthrough function triggers a broadcast.
        '''

        pass


# end of class DoublePassthrough



class Bleed (DoublePassthrough):

    def __init__ (self, name, service, flavor, *args, **kwargs):

        self.flavor = flavor
        DoublePassthrough.__init__ (self, name, service, *args, **kwargs)


    def write (self, value):

        self.passthrough (float (value), self.flavor)


# end of class Bleed



class CommandPassthrough (DFW.Keyword.Boolean):

    def __init__ (self, *args, **kwargs):

        kwargs['initial'] = '0'

        DFW.Keyword.Boolean.__init__ (self, *args, **kwargs)


    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        self.passthrough = function


    def write (self, value):

        if value == '0':
            return

        if self.passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)

        self.passthrough ()


    def postwrite (self, value):

        DFW.Keyword.Boolean.postwrite (self, value)

        # After broadcasting 'Yes' to acknowledge the command,
        # immediately reset the keyword value.

        self.set ('0')


# end of class CommandPassthrough



class Filename (DFW.Keyword.String):

    def prewrite (self, value):

        if value != '':
            value = os.path.expanduser (value)

            if os.path.exists (value):
                pass
            else:
                message = "file '%s' does not exist" % (value)
                DFW.Keyword.raiseError (message, 'ERR_OPEN_FILE')

        return DFW.Keyword.String.prewrite (self, value)


# end of class Filename



class Freeze (CommandPassthrough):

    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        name = function.__name__

        if name == 'freeze':
            self.freeze = function
        elif name == 'resume':
            self.resume = function
        else:
            raise ValueError, "%s expected a passthrough to freeze() or resume(), not %s()" % (self.name, name)


    def write (self, value):

        if value == '0':
            passthrough = self.resume
        else:
            passthrough = self.freeze

        if passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)

        passthrough ()


    # Revert to the typical behavior of a Boolean keyword, since
    # the FREEZE keyword is keeping independent state.

    postwrite = DFW.Keyword.Boolean.postwrite


# end of class Freeze



class EnumeratedPassthrough (DFW.Keyword.Enumerated):

    def __init__ (self, name, service):

        self.passthrough = None
        DFW.Keyword.Enumerated.__init__ (self, name, service)


    def sanitize (self, value):
        ''' Return a value in a format expected by the
            shaneao function that we're passing through to.
        '''

        # The value out of the mapping is a unicode string; cast it
        # to a regular string for the benefit of naive parsers down
        # the line.

        value = self.mapping[value]
        value = str (value)

        return value


    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        self.passthrough = function


    def prewrite (self, value):

        value = DFW.Keyword.Enumerated.prewrite (self, value)

        mapped = self.mapping[value]
        mapped = mapped.lower ()

        if mapped == 'unknown':
            raise ValueError, "cannot set keyword to 'Unknown'"

        return value


    def write (self, value):

        if self.passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)

        value = self.sanitize (value)
        self.passthrough (value)


    def postwrite (self, *ignored):
        ''' The passthrough function triggers a broadcast.
        '''

        pass


# end of class EnumeratedPassthrough



class EnumeratedIntegerPassthrough (EnumeratedPassthrough):

    def sanitize (self, value):

        value = self.mapping[value]
        value = int (value)

        return value


# end of class EnumeratedIntegerPassthrough



class ControlMatrix (EnumeratedPassthrough):

    lookup = {'8x': {'default': 'controlMatrix_8x.fits',
                     'weighted least squares': 'controlMatrix_8x_wls.fits',
                     'dim': 'controlMatrix_8x_dim.fits'},

              '8xLGS': {'default': 'controlMatrix_8xLGS.fits',
                        'weighted least squares': 'controlMatrix_8x_wlsLGS.fits',
                        'dim': 'controlMatrix_8x_dimLGS.fits'},

              '16x': {'default': 'controlMatrix_16x.fits',
                      'weighted least squares': 'controlMatrix_16x_wls.fits',
                      'dim': 'controlMatrix_16x_dim.fits',
                      'zero': 'controlMatrix_16x_zero.fits'},

              '16xLGS': {'default': 'controlMatrix_16xLGS.fits',
                         'weighted least squares': 'controlMatrix_16x_wlsLGS.fits',
                         'dim': 'controlMatrix_16x_dimLGS.fits'},

              'controlMatrix_8x.fits': 'default',
              'controlMatrix_8x_wls.fits': 'weighted least squares',
              'controlMatrix_8x_dim.fits': 'dim',

              'controlMatrix_8xLGS.fits': 'default',
              'controlMatrix_8x_wlsLGS.fits': 'weighted least squares',
              'controlMatrix_8x_dimLGS.fits': 'dim',

              'controlMatrix_16x.fits': 'default',
              'controlMatrix_16x_wls.fits': 'weighted least squares',
              'controlMatrix_16x_dim.fits': 'dim',
              'controlMatrix_16x_zero.fits': 'zero',

              'controlMatrix_16xLGS.fits': 'default',
              'controlMatrix_16x_wlsLGS.fits': 'weighted least squares',
              'controlMatrix_16x_dimLGS.fits': 'dim'}


    def __init__ (self, name, service):

        EnumeratedPassthrough.__init__ (self, name, service)

        filename = service.name + '.MATRIXFILE'
        DFW.Keyword.callback (filename, self.filenameCallback)


    def sanitize (self, value):

        value = self.mapping[value]
        value = str (value)
        value = value.lower ()

        service = StatusDictionary.service
        mode = service['MODE']
        mode = mode.mapped ()

        try:
            filename = ControlMatrix.lookup[mode][value]
        except KeyError:
            raise ValueError, "no control matrix available for '%s' in mode '%s'" % (value, mode)

        return filename


    def filenameCallback (self, keyword):

        if keyword == None:
            return

        filename = keyword.value

        if filename == None:
            return

        try:
            control_mode = ControlMatrix.lookup[filename]
        except KeyError:
            control_mode = 'unknown'

        self.set (control_mode)

# end of class ControlMatrix



class Init (EnumeratedPassthrough):

    def sanitize (self, value):

        value = EnumeratedPassthrough.sanitize (self, value)

        if value == '8x':
            value = 8
        elif value == '16x':
            value = 16
        else:
            raise ValueError, "unexpected initialization command: %s" % (value)

        return value

# end of class Init



class Interval (DFW.Keyword.Double):

    def __init__ (self, name, service, attribute, key='interval'):

        self.attribute = attribute
        self.namespace = None
        self.key = key

        DFW.Keyword.Double.__init__ (self, name, service, period=0.5)


    def read (self):

        if self.namespace == None:
            return

        shaneao = self.namespace.shaneao
        gizmo = getattr (shaneao, self.attribute)

        try:
            timing = gizmo.timing ()
        except AttributeError:
            return

        timing = timing[self.key]

        return "%.1f" % (timing)


    def setNamespace (self, namespace):

        self.namespace = namespace


# end of class Interval



class LoadFlat (CommandPassthrough):

    def write (self, value):

        if value == '0':
            return

        if self.passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)


        service = StatusDictionary.service
        tweeter = service['TWEETERFLAT'].value
        woofer = service['WOOFERFLAT'].value

        if tweeter == '':
            tweeter = None

        if woofer == '':
            woofer = None

        if tweeter == None:
            raise ValueError, 'TWEETERFLAT keyword is not set'


        self.passthrough (tweeter, woofer)


# end of class LoadFlat



class LaserOffload (DFW.Keyword.Double):
    ''' The LASER_OFFLOAD_% keywords will broadcast at something like
        200 Hz, if left to their own devices. Most of that is noise.
        Ellie says that the values can be rounded to three decimal
        places with no loss of useful fidelity; the noise is in the
        fifth decimal place.
    '''

    def __init__ (self, *args, **kwargs):

        self.previous_timestamp = 0
        DFW.Keyword.Double.__init__ (self, *args, **kwargs)


    def set (self, value, force=False):

        now = time.time ()

        # Even with rounding, there are too many broadcasts. The high
        # frequency causes operational problems with things like GUI
        # displays, triggering the GUI to use a lot of unnecessary CPU
        # time.
        #
        # Thus, updates are being throttled back to ~2 Hz.

        if now - self.previous_timestamp < 0.5:
            return

        value = float (value)
        value = round (value, 3)
        value = repr (value)

        # The first half of the subsequent condition is also performed
        # in Double.set(), but it is duplicated here to ensure that
        # self.previous_timestamp is not set if the value isn't changing.

        if value != self.value or force == True:
            self.previous_timestamp = now
            DFW.Keyword.Double.set (self, value, force)


# end of class LaserOffload



class LoopState (DFW.Keyword.Enumerated):

    def __init__ (self, name, service, flavor, *args, **kwargs):

        self.flavor = flavor + ' only'
        self.loopstate = None
        self.substate = None

        DFW.Keyword.Enumerated.__init__ (self, name, service, *args, **kwargs)

        substate = service.name + '.SUBSTATE'
        DFW.Keyword.callback (substate, self.substateCallback)

        loopstate = saocon['LOOPSTATE']
        loopstate.monitor (wait=False)
        loopstate.callback (self.loopstateCallback)


    def loopstateCallback (self, keyword):

        if keyword == None or keyword['populated'] == False:
            return


        self.loopstate = keyword.lower ()
        self.update ()


    def substateCallback (self, keyword):

        if keyword == None:
            return

        self.substate = keyword.mapped (lower=True)
        self.update ()


    def read (self):

        flavor = self.flavor
        loopstate = self.loopstate
        substate = self.substate


        if loopstate == None or substate == None:
            return

        if loopstate == 'open':
            return self.mapping['open']

        if substate == flavor:
            return self.mapping['closed']

        if flavor == 'wfs only' and (substate == 'lgs' or substate == 'ngs'):
            return self.mapping['closed']

        if flavor == 'tt only' and substate == 'lgs':
            return self.mapping['closed']

        return self.mapping['open']


# end of class LoopState



class Offload (EnumeratedPassthrough):

    def set (self, value, force=False):

        if value == True or value == 'True':
            value = 'on'
        elif value == False or value == 'False':
            value = 'off'

        return EnumeratedPassthrough.set (self, value, force)


# end of class Offload



class OpMode (DFW.Keyword.Enumerated):

    def __init__ (self, *args, **kwargs):

        kwargs['initial'] = '0'
        DFW.Keyword.Enumerated.__init__ (self, *args, **kwargs)


    def write (self, value):

        service = StatusDictionary.service
        command = self.mapping[value]
        command = command.lower ()

        if command == 'ngs':
            substate = 'ngs'
        else:
            substate = 'lgs'

        if command == 'lgs' or command == 'wfs only':
            uplink = 'on'
        else:
            uplink = 'off'

        # Substate has to happen first, because some operations
        # are only allowed if the substate is 'lgs'.

        service['SUBSTATE'].modify (substate)
        service['UPLINK'].modify (uplink)

        if command == 'tt only':
            service['FLATZERO'].modify ('yes')

        return DFW.Keyword.Enumerated.write (self, value)


# end of class OpMode



class ProcessingTime (Interval):

    def __init__ (self, name, service, attribute, key='processing'):

        Interval.__init__ (self, name, service, attribute, key)


# end of class ProcessingTime



class Sampler (CommandPassthrough):

    def __init__ (self, base, service):

        self.base = base
        name = base + 'CAM_SAMPLESTART'

        CommandPassthrough.__init__ (self, name, service)


    def write (self, value):

        if value == '0':
            return

        if self.passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)


        service = StatusDictionary.service
        base = self.base

        comment  = base + 'CAM_SAMPLECOMMENT'
        count    = base + 'CAM_SAMPLECOUNT'
        interval = base + 'CAM_SAMPLEINTERVAL'

        comment  = service[comment]
        count    = service[count]
        interval = service[interval]

        comment  = comment.value
        count    = count.value
        interval = interval.value

        if comment == None:
            comment = ''

        if count == None or interval == None:
            return

        count    = int (count)
        interval = float (interval)

        self.passthrough (n_frames=count, delta_t=interval, comment=comment)


# end of class Sampler



class SmartGain (DFW.Keyword.Double):

    def __init__ (self, base, service):

        name = 'SMART_' + base + 'GAIN'
        DFW.Keyword.Double.__init__ (self, name, service)

        self.loop = None
        self.open = '0.0'
        self.closed = '0.0'

        open = 'S' + base + 'GAIN'
        closed = base + 'GAIN'

        self.open_keyword = open
        self.closed_keyword = closed

        loop = "%s.%sLOOP" % (service.name, base)
        open = "%s.%s" % (service.name, open)
        closed = "%s.%s" % (service.name, closed)

        DFW.Keyword.callback (loop, self.loopCallback)
        DFW.Keyword.callback (open, self.openCallback)
        DFW.Keyword.callback (closed, self.closedCallback)


    def loopCallback (self, keyword):

        if keyword.value == None:
            return

        loop = keyword.mapped (lower=True)
        self.loop = loop

        if loop == 'open':
            self.set (self.open)
        elif loop == 'closed':
            self.set (self.closed)


    def openCallback (self, keyword):

        open = keyword.value
        self.open = open

        if self.loop == 'open':
            self.set (open)


    def closedCallback (self, keyword):

        closed = keyword.value
        self.closed = closed

        if self.loop == 'closed':
            self.set (closed)


    def write (self, value):

        loop = self.loop

        if loop == 'open':
            target = self.open_keyword
        elif loop == 'closed':
            target = self.closed_keyword
        else:
            raise ValueError, "unhandled loop state: %s" % (loop)

        target = StatusDictionary.service[target]
        target.modify (value)


# end of class SmartGain


class SmartTTGain (SmartGain):

    def __init__ (self, base, service):

        name = 'SMART_' + base + 'GAIN'
        DFW.Keyword.Double.__init__ (self, name, service)

        self.loop = None
        self.open = '0.0'
        self.closed = '0.0'

        open = 'S' + base + 'GAIN'
        closed = base + 'GAIN'

        self.open_keyword = open
        self.closed_keyword = closed

        open = "%s.%s" % (service.name, open)
        closed = "%s.%s" % (service.name, closed)

        DFW.Keyword.callback (open, self.openCallback)
        DFW.Keyword.callback (closed, self.closedCallback)
        DFW.Keyword.callback (closed, self.loopCallback)


    def loopCallback (self, keyword):

        if keyword.value == None:
            return

        gain = float (keyword.value)
        if gain == 0.0:
            loop = 'open'
        else:
            loop = 'closed'

        self.loop = loop

        if loop == 'open':
            self.set (self.open)
        elif loop == 'closed':
            self.set (self.closed)


# end of class SmartTTGain



class SmartMode (DFW.Keyword.Enumerated):

    def __init__ (self, name, service):

        DFW.Keyword.Enumerated.__init__ (self, name, service)

        mode = service.name + '.MODE'
        DFW.Keyword.callback (mode, self.modeMirror)


    def modeMirror (self, keyword):

        if keyword == None:
            return

        mode = keyword.value

        self.set (mode)


    def write (self, value):

        mapped = self.mapping[value]
        service = StatusDictionary.service
        init = service['INIT']

        if '8' in mapped:
            init.modify ('8x')
        elif '16' in mapped:
            init.modify ('16x')


# end of class SmartMode



class StringPassthrough (DFW.Keyword.String):

    def __init__ (self, name, service):

        self.passthrough = None
        DFW.Keyword.String.__init__ (self, name, service)


    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        self.passthrough = function


    def write (self, value):

        if self.passthrough == None:
            raise RuntimeError, '%s: no passthrough function set' % (self.name)

        self.passthrough (value)


    def postwrite (self, *ignored):
        ''' The passthrough function triggers a broadcast.
        '''

        pass


# end of class StringPassthrough



class UplinkCounter (DFW.Keyword.Integer):

    def __init__ (self, *args, **kwargs):

        self.namespace = None
        kwargs['period'] = 0.5

        DFW.Keyword.Integer.__init__ (self, *args, **kwargs)


    def read (self):

        namespace = self.namespace
        if namespace == None:
            return

        try:
            counter = namespace.upl.counter
        except AttributeError:
            return

        return str (counter)


    def setNamespace (self, namespace):

        self.namespace = namespace

# end of class UplinkCounter



class WooferRate (DFW.Keyword.Integer):

    def __init__ (self, *args, **kwargs):

        self.passthrough = None
        DFW.Keyword.Integer.__init__ (self, *args, **kwargs)


    def set (self, value, force=False):

        if value == 'none' or value == 'unknown':
            value = '0'

        DFW.Keyword.Integer.set (self, value, force)


    def setPassthrough (self, function):

        if callable (function):
            pass
        else:
            raise ValueError, 'function is not callable'

        self.passthrough = function


    def write (self, value):

        self.passthrough (int (value))


    def postwrite (self, *ignored):
        ''' The passthrough function triggers a broadcast.
        '''

        pass


# end of class WooferRate


# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
