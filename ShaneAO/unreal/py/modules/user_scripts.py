"""Script file for ShaneAO
execute this file with::

    load(os.path.join(pyDir,'user_scripts.py'))

"""
import time

def data():
    """
    Take telemetry data
    """
    global tbuf
    shaneao.rtc.rtc2.telemetry_data()
    tbuf = shaneao.rtc.rtc2.telemetry_peek()

def save_data(buf,state={}):
    """
    Save the telemetry to disk
    """
    archive_dir = 'data_'+time.strftime('%Y-%m-%d')
    if (not os.path.exists(archive_dir)):
        os.mkdir(archive_dir)
    if (not os.path.exists(os.path.join(archive_dir,'data_seq_no'))):
        seq_no = np.array([0])
        seqhdu = pyfits.PrimaryHDU(seq_no)
        seqhdu.header['seq_no'] = (0,'sequence number')
        seqhdu.writeto(os.path.join(archive_dir,'data_seq_no'))
    seqhdu = pyfits.open(os.path.join(archive_dir,'data_seq_no'),mode='update')
    seq_no = seqhdu[0].data[0]
    tl = time.localtime(time.time())
    day = time.strftime('%Y-%m-%d',tl)
    tim = time.strftime('%H%M%S',tl)
    hdu = pyfits.PrimaryHDU(buf)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('DEVICE','WFS+DM'))
    hdu.header.append(('DATE',day,'File generation date'))
    hdu.header.append(('TIME',tim,'file generation time hhmmss'))
    hdu.header.append(('GENER','rtc.save_data'))
    for key in state.keys():
        keyword = key.upper()[0:8].replace(' ','_')
        value = state[key]
        hdu.header.append((keyword,value,key))
    fitsFileName = os.path.join(archive_dir,'Data_'+ '%04d' % seq_no +'.fits')
    print 'writing to ' + fitsFileName
    hdu.writeto(fitsFileName)
    time.sleep(.1)
    seq_no += 1
    seqhdu[0].header['seq_no'] = (seq_no,'sequence number')
    seqhdu[0].data[0] = seq_no
    seqhdu.close()
