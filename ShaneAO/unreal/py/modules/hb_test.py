"""hb_test.py
Heartbeat scheme test
"""

import Tkinter as Tk

root = Tk.Tk()
labs = {}
topline = Tk.Label(root,text='ShaneAO status')
lab_names = ['heartbeat','camera','rate','loop','gain','saved gain']

labs = []
for name in lab_names:
    labs.append(Tk.Label(root,text=name))

topline.pack()
for l in labs:
    l['width']=15
    l['relief']='groove'
    l.pack()

heartbeat = 0
def beat():
    global heartbeat
    l = labs[lab_names.index('heartbeat')]
    if (heartbeat == 1):
        print 'modify MEMSPULSE=1'
        l["bg"]="green"
        root.after(5000,beat)
    else:
        l["bg"]="red"
        print 'heartbeat stopped'
    heartbeat = 0

labs[lab_names.index('camera')]["bg"]="grey"
labs[lab_names.index('loop')]['bg']='grey'

_gain = 0.
_saved_gain = 1.

def post(name,text,color=None):
    l = labs[lab_names.index(name)]
    l['text']=text
    if (color == None):
        l['bg']='white'
    else:
        l['bg']=color

def close():
    global _gain
    _gain = _saved_gain
    post('loop','loop closed','green')
    post('gain','gain = '+str(_gain))

def open():
    global _gain, _saved_gain
    _saved_gain = _gain
    _gain = 0
    post('loop','loop open','grey')
    post('saved gain','saved gain = '+str(_saved_gain))
    post('gain','gain = '+str(_gain))

def rate(r):
    post('rate','rate: '+str(r))

def camstart():
    post('camera','camera running','green')

def camstop():
    post('camera','camera stopped','red')

def gain(g):
    global _saved_gain
    _saved_gain = g
    close()
