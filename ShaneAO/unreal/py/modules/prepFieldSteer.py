"""Field steer prep
"""
import os
#import xlrd
import numpy as np

home = os.environ['HOME']
dataDir = os.path.join(home,'data')
fileName = os.path.join(dataDir,'Pointing and Centering Values for Observers.xlsx')

def prepData():
    book = xlrd.open_workbook(fileName)
    sh = book.sheet_by_index(0)
    dset = []
    for k in range(3,38):
        z = []
        for j in range(7):
            z.append(sh.cell_value(rowx=k,colx=j))
        dset.append(z)
    
    f = open('fieldSteering_Data.txt','w')
    f.write(str(dset))
    f.close()

def getRawTests():
    fileName = os.path.join(dataDir,'Pointing and Centering Tests.xlsx')
    book = xlrd.open_workbook(fileName)
    sh = book.sheet_by_index(0)
    dset = []
    for k in range(2,43):
        z = []
        for j in range(20):
            z.append(sh.cell_value(rowx=k,colx=j))
        dset.append(z)
    return dset

sel = [5,6,7,8,15,16,18,19]

def pull_data():
    d = getRawTests()
    n = len(d)
    r = []
    for k in range(1,n):
        z = []
        for j in sel:
            z.append(float(d[k][j]))
        r.append(z)
    r = np.array(r)
    return r

#r = pull_data()
#px = r[:,0]
#py = r[:,1]
#c1 = r[:,2]
#c2 = r[:,3]
#ax = r[:,6]
#ay = r[:,7]


from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

plt.ion()

def do_plots():
    fig = plt.figure(1)
    axis = fig.add_subplot(111, projection='3d')
    axis.scatter(ax, ay, c1, color='b')
    axis.scatter(py,px,c1,color='r')
    plt.show()
    plt.title('cx; red=command, blue=actual')
    
    fig = plt.figure(2)
    axis = fig.add_subplot(111, projection='3d')
    axis.scatter(ax, ay, c2, color='b')
    axis.scatter(py,px,c2,color='r')
    plt.show()
    plt.title('cy; red=command, blue=actual')
    
    fig = plt.figure(3)
    axis = fig.add_subplot(111, projection='3d')
    axis.scatter(py,px,ax)
    plt.title('actual vs commanded, X')
    
    fig = plt.figure(4)
    axis = fig.add_subplot(111, projection='3d')
    axis.scatter(py,px,ay)
    plt.title('actual vs commanded, Y')
    
    fig = plt.figure(5)
    plt.plot(py,'bo')
    plt.plot(ax,'ro')
    plt.title('X')
    plt.grid()
    
    fig = plt.figure(6)
    plt.plot(px,'bo')
    plt.plot(ay,'ro')
    plt.title('Y')
    plt.grid()

dset = [
    [1.0, u'Centered', 0.0, 0.0, 0.005, -0.015, '', 0.0, 0.0],
    [2.0, u'E2', 2.0, 0.0, 0.02, 0.0, '', 0.5, 2.24],
    [3.0, u'E4', 4.0, 0.25, 0.026, 0.025, '', 0.63, 4.41],
    [4.0, u'E6', 6.0, 0.5, 0.045, 0.055, '', 0.88, 6.41],
    [5.0, u'E8', 7.79, 1.9, 0.036, 0.0985, u'0.6 gain', -0.14, 8.4],
    [6.0, u'E10', 10.0, 2.35, 0.0635, 0.1735, u'0.5 gain', 0.46, 10.4],
    [7.0, u'W2', -2.0, 0.0, 0.01, -0.01, '', -0.11, -2.14],
    [8.0, u'W4', -4.0, 0.0, 0.01, -0.01, '', 0.07, -4.24],
    [9.0, u'W6', -6.0, 0.0, -0.005, 0.0, '', -0.14, -4.31],
    [10.0, u'W8', -8.0, 0.0, -0.005, 0.0, '', -0.11, -5.04],
    [11.0, u'S2', 0.0, 2.0, 0.0, 0.0, '', -2.28, -0.04],
    [12.0, u'S4', 0.0, 4.0, 0.0, -0.02, '', -4.55, -0.21],
    [14.0, u'S6', 0.0, 6.2, 0.035, -0.029, '', -7.18, -1.0],
    [15.0, u'S8', 0.0, 7.5, 0.065, -0.035, u'gain 0.6', -8.47, 1.65],
    [17.0, u'N2', 0.0, -2.0, 0.02, 0.0, '', 2.0, -0.5],
    [18.0, u'N4', 0.0, -4.0, 0.054, 0.0, '', 4.17, -1.02],
    [19.0, u'N6', 1.0, -6.0, 0.12, 0.0, '', 6.37, -0.8],
    [20.0, u'N8', 2.5, -8.0, 0.175, 0.0, '', 9.21, -0.5],
    [21.0, u'N10', 4.5, -10.0, 0.27, 0.0185, u'gain 0.5', 11.2, -0.9],
    [23.0, u'S4,E4', 4.0, 4.0, 0.0, 0.0, '', -4.5, 4.3],
    [24.0, u'S4,E6', 6.0, 4.0, 0.0, 0.01, '', -4.2, 6.6],
    [25.0, u'S6,E4', 4.0, 6.0, 0.0, -0.02, '', -7.1, 3.9],
    [26.0, u'S6,E6', 6.0, 6.0, 0.0, -0.02, '', -7.04, 6.27],
    [28.0, u'S4,W4', -4.0, 4.0, 0.014, 0.02, u'gain .5', -3.78, -4.66],
    [29.0, u'S4,W6', -6.0, 4.0, 0.018, 0.058, u'gain .3', -3.29, -6.72],
    [30.0, u'S6,W4', -4.0, 6.0, 0.0367, 0.035, u'gain .3', -5.67, -5.25],
    [31.0, u'S6,W6', -6.0, 6.0, 0.0367, 0.077, u'gain .3', -5.04, -7.32],
    [34.0, u'N4,W4', -4.0, -4.0, 0.02, 0.0, u'gain .7', 3.43, -4.87],
    [35.0, u'N4,W6', -6.0, -4.0, 0.02, 0.0, u'gain .7', 3.47, -6.72],
    [36.0, u'N6, W4', -4.0, -6.0, 0.054, 0.0, u'gain .7', 5.36, -5.5],
    [37.0, u'N6,W6', -6.0, -6.0, 0.04, 0.0, u'gain .7', 5.39, -7.28],
    [38.0, u'S6, E4', 4.0, -3.5, 0.107, 0.04, u'gain .7', 3.85, 2.77],
    [39.0, u'S2,E6', 6.0, -2.0, 0.105, 0.08, u'gain .5', 2.84, 5.04],
    [40.0, u'S3.5,E5.5', 5.5, -3.5, 0.132, 0.07, u'gain .5', 4.4, 3.92],
    [41.0, u'S3,E7', 7.0, -3.0, 0.15, 0.1, u'gain .4', 4.34, 5.39]
]

pn = []
pe = []
cx = []
cy = []
for d in dset:
    if (d[0] != 8.0):
        pn.append(-float(d[7]))
        pe.append(float(d[8]))
        cx.append(float(d[4]))
        cy.append(float(d[5]))

pn = np.array(pn)
pe = np.array(pe)
cx = np.array(cx)
cy = np.array(cy)
points = np.array([pn,pe]).T
grid_x,grid_y = np.mgrid[-10:10:100j,-10:10:100j]

from scipy.interpolate import griddata
from matplotlib import cm

gix = griddata(points, cx, (grid_x, grid_y), method='cubic')
gix = np.nan_to_num(gix)
giy = griddata(points, cy, (grid_x, grid_y), method='cubic')
giy = np.nan_to_num(giy)

fig = plt.figure(3)
axis = fig.add_subplot(111, projection='3d')
axis.scatter(pn, pe, cx, color='b')
axis.plot_surface(grid_x,grid_y,gix,cstride=4,rstride=4,cmap=cm.coolwarm,linewidth=0,antialiased=False)
axis.set_xlabel('north')
axis.set_ylabel('east')
axis.set_zlabel('cx')
plt.show()

fig = plt.figure(4)
axis = fig.add_subplot(111, projection='3d')
axis.scatter(pn, pe, cy, color='b')
axis.plot_surface(grid_x,grid_y,giy,cstride=4,rstride=4,cmap=cm.coolwarm,linewidth=0,antialiased=False)
axis.set_xlabel('north')
axis.set_ylabel('east')
axis.set_zlabel('cy')
plt.show()
