""" Extension to json
 https://docs.python.org/2.7/library/json.html#json.JSONDecoder
  To test this unit:
>>> import numpy_json
>>> numpy_json.test()
"""

import base64
import json
import numpy as np

class NumpyEncoder(json.JSONEncoder):
    """Encoder for numpy arrays.
    """
    def default(self, obj):
        """If input object is an ndarray it will be converted into a dict 
        holding dtype, shape and the data, base64 encoded.
        Otherwise, the object is JSON encoded in the standard way.
        """
        if isinstance(obj, np.ndarray):
            if obj.flags['C_CONTIGUOUS']:
                obj_data = obj.data
            else:
                cont_obj = np.ascontiguousarray(obj)
                assert(cont_obj.flags['C_CONTIGUOUS'])
                obj_data = cont_obj.data
            data_b64 = base64.b64encode(obj_data)
            return dict(__ndarray__=data_b64,
                        dtype=str(obj.dtype),
                        shape=obj.shape)
        # Let the base class default method raise the TypeError
        return json.JSONEncoder.default(self, obj)

class NumpyDecoder(json.JSONDecoder):
    """JSON Decoder for numpy arrays.
    """
    def decode(self,obj):
        """Decode the JSON string, first testing that if it is
        the encoding of a numpy array. If not, go on to do a
        standard JSON decoding
        """
        dct = json.JSONDecoder.decode(self,obj)
        #print 'type of dct is ',type(dct)
        if isinstance(dct,dict) and '__ndarray__' in dct:
            data = base64.b64decode(dct['__ndarray__'])
            return np.frombuffer(data,dct['dtype']).reshape(dct['shape'])
        return dct

def dumps(obj):
    """Serialize obj to a JSON formatted string.
    (Extention of json.dumps to allow numpy arrays)
    """
    return json.dumps(obj,cls=NumpyEncoder)

def loads(obj):
    """Decodes a JSON formatted string to a python object.
    (Extension of json.loads to allow numpy arrays)
    """
    return json.loads(obj,cls=NumpyDecoder)

def test():
    global expected,dumped,result
    expected = np.arange(100, dtype=np.float)
    dumped = dumps(expected)
    result = loads(dumped)
    
    # None of the following assertions will be broken.
    assert result.dtype == expected.dtype, "Wrong Type"
    print 'Type check ok'
    assert result.shape == expected.shape, "Wrong Shape"
    print 'Shape ok'
    assert np.allclose(expected, result), "Wrong Values"
    print 'Values ok'
