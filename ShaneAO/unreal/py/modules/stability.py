"""This module can be used to check the stability of the ShaneAO woofer-tweeter reconstructor, as defined
by calibration matrices and some 'user specified' control parameters. This assumes the *Internal Mode Space Formulation*
and the *Minimum Variance Reconstructor* described in `RTC_WindPCMeeting_PresentationV2a.pdf <RTC_WindPCMeeting_PresentationV2a.pdf>`_ 
Please also refer to this document for a discusion of the stability analysis

The 'user' parameters are
    *g*
        the feedback gain
        
    *gamma*
        the integrator bleed (forgetting factor)
        
    *alpha*
        the time constant of the woofer filter (the rate at which woofer modes are removed from the tweeter)
        
    *beta*
        the time constant of the woofer
    
    **P**
        regularization matrix: a-priori covariance of the tweeter modes or inverse penalty of modes
    
    **Q**
        regularization matrix: covariance of the measurement noise or inverse penalty of slope-fitting error

.. note::

    alpha, beta, and gamma should all be between 0 and 1
    The feedback gain is recommended to be between 0 and 1 but up to 2 may still be stable

Example use:

.. code-block:: python

    from stability import *
    pinit()
    g_scan()

"""
import readfits as fits
import numpy as np
import matplotlib.pyplot as plt
import os

na,naw,nf,ns = 1024,52,14,144
homedir = os.environ['HOME']
param_dir = homedir + '/parameterFiles/'
subdir = param_dir + 'reconMatrix_16x/'


At = fits.readfits(subdir+'At.fits')
Aw = np.matrix(fits.readfits(subdir+'Aw.fits'))
H = np.matrix(fits.readfits(subdir+'H.fits'))
Q = np.matrix(fits.readfits(subdir+'Q.fits'))
P = np.matrix(fits.readfits(subdir+'P.fits'))
Mt = np.matrix(fits.readfits(subdir+'Mt.fits'))
Mw = np.matrix(fits.readfits(subdir+'Mw.fits'))
Cwt = np.matrix(fits.readfits(subdir+'Cwt.fits'))

n_tweeter_modes = At.shape[0]
At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
At_d = np.linalg.solve(At.transpose()*At,At.transpose())
Aw = Aw[0:nf,:].transpose()
Aw_d = np.linalg.solve(Aw.transpose()*Aw,Aw.transpose())
Cwt = Cwt[0:nf,:]
Mw = Mw[0:nf,0:nf]
Mw = np.linalg.pinv(Mw)
Mt = np.linalg.pinv(Mt,rcond=.001)
Hw = H*Mt*Cwt.transpose() # should calibrate this with woofer mode pokes

H_d = np.linalg.solve(Q + H*P*H.transpose(), H*P).transpose()
R = At*H_d

global g, gamma, alpha, beta
g, gamma, alpha, beta = 0.5, 0.85, 0.8, 0.8
global pfig

def analize():
    """Generate the stability matrix T given the present static settings
    of g, gamma, alpha, and beta
    """
    sm = np.vstack(
        [np.hstack([gamma*np.identity(na) - g*At*H_d*H*At_d, np.zeros((na,naw)),        -At*Mt*Cwt.transpose(), -g*At*H_d*Hw*Aw_d               ]),
         np.hstack([-g*Aw*Mw*Cwt*H_d*H*At_d,                 gamma*np.identity(naw),    np.zeros((naw,nf)),     -g*Aw*Mw*Cwt*H_d*Hw*Aw_d        ]),
         np.hstack([-g*(1-alpha)*Mw*Cwt*H_d*H*At_d,          np.zeros((nf,naw)),        alpha*np.identity(nf),  -g*(1-alpha)*Mw*Cwt*H_d*Hw*Aw_d ]),
         np.hstack([np.zeros((naw,na)),                      (1-beta)*np.identity(naw), np.zeros((naw,nf)),     beta*np.identity(naw)           ])]
        )
    
    lam = np.linalg.eigvals(sm)
    lams = np.sort(lam)
    return lams

plt.ion()

def pinit():
    """Initialize the plot. Draw a unit circle on the complex plane
    for reference. Clear any previous eigenvalue markers.
    """
    pfig = plt.figure('Stability Analysis',figsize=(7,7))
    plt.clf()
    th = np.arange(0,2*np.pi,2*np.pi/100.)
    c = np.cos(th)
    s = np.sin(th)
    plt.plot(s,c)
    plt.grid()
    plt.draw()

def pshow(g_arg,clear=False):
    """Plot the eigenvalues of the stability matrix. The system is
    stable if they are all inside the unit circle
    """
    global g
    g = g_arg
    lams = analize()
    if (clear == True): pinit()
    plt.plot(np.real(lams),np.imag(lams),'x')
    plt.draw()
    return pset()


def pset(params=None):
    """Set the internal parameters.
    If no argument is given, the current parameter settings are printed out and
    returned as a 4-tuple
    
    If the argument is a 4-tuple, then the internal parameters are set. The 4-tuple
    is (g, gamma, alpha, beta), where
    g: the feedback gain
    gamma: the integrator gain (forgetting factor)
    alpha: the time constant of the woofer filter (the rate at which woofer modes are removed from the tweeter)
    beta: the time constant of the woofer
    """
    global g, gamma, alpha, beta
    if (params == None):
        print( '(g, gamma, alpha, beta) = ' + str((g,gamma,alpha,beta)) )
        return (g,gamma,alpha,beta)
    (g, gamma, alpha, beta) = params

def alpha_beta_scan(clear=True,alpha_set=np.arange(0,.8,.1)):
    """Plot the eigenvalues of the stability matrix while
    scanning alpha and beta simultaneously.
    This can be shown as an animation if clear is
    set to True, or as a root-locus if clear is set to False
    """
    global g,gamma,alpha, beta
    gsave = (g,gamma,alpha,beta)
    beta_set = alpha_set
    for alpha,beta in zip(alpha_set,beta_set):
        print(pshow(g,clear=clear))
    g,gamma,alpha,beta = gsave

def g_scan(clear=True,g_set=np.arange(0.,1.,.1)):
    """Plot the eigenvalues of the stability matrix while
    scanning the feedback gain.
    This can be shown as an animation if clear is
    set to True, or as a root-locus if clear is set to False
    """
    global g,gamma,alpha, beta
    gsave = (g,gamma,alpha,beta)
    g_set = np.arange(0.,1.,.1)
    for g in g_set:
        print(pshow(g,clear=clear))
    g,gamma,alpha,beta = gsave

def gamma_scan(clear=True,gamma_set=np.arange(0,1.,.1)):
    """Plot the eigenvalues of the stability matrix while
    scanning the integrator gain.
    This can be shown as an animation if clear is
    set to True, or as a root-locus if clear is set to False
    """
    global g,gamma,alpha, beta
    gsave = (g,gamma,alpha,beta)
    for gamma in gamma_set:
        print(pshow(g,clear=clear))
    g,gamma,alpha,beta = gsave

