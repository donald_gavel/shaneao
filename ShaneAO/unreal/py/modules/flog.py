#!/usr/bin/env python
"""Create a log file for this directory

List files and header info in a comma-separated-values (csv) format
suitalbe to be read by spreadsheet programs
"""

import os
import sys
import pyfits
import fnmatch
try:
    import pandas as pd
    pd.set_option('display.width',150)
    has_pandas = True
except:
    has_pandas = False

default_keys = [
    'DATE-OBS',
    'TIME-OBS',
    'DATE-BEG',
    'OBJECT',
    'RA',
    'DEC',
    'AIRMASS',
    'LOOPSTAT',
    'LOOPSUB',
    'WFSRATE',
    'TTRATE',
    'FILT1NAM',
    'ITIME',
    'COADDS'
]

verbose = False

if has_pandas:
    def pflog(keys=[], wildcard=None, only=False, directory='.'):
        """a version of flog that returns a pandas database rather than
        creating a csv file.
        """
        if not only:
            keys = keys + default_keys

        # select the files
        file_list = os.listdir(directory)
        sfile_list = []
        for fileName in file_list:
            if (fileName.endswith('.fits')):
                if wildcard is not None:
                    if fnmatch.fnmatch(fileName,wildcard):
                        sfile_list.append(fileName)
                else:
                    sfile_list.append(fileName)
        
        #print sfile_list
        sfile_list = map(lambda x: os.path.join(directory,x),sfile_list)
        db = pd.DataFrame(columns = ['FILE']+keys)
        
        for k,fileName in zip(range(1,len(sfile_list)+1),sfile_list):
            try:
                hdu = pyfits.open(fileName)
            except:
                print 'Trouble reading file '+fileName
                print sys.exc_info()
                continue
            hdr = hdu[0].header
            db.loc[k,'FILE'] = os.path.basename(fileName)
            for keyword in keys:
                if (keyword in hdr):
                    val = hdr.cards[keyword][1]
                    db.loc[k,keyword] = val
            hdu.close()
        return db
    
    def cmflog(keys=[], wildcard=None, only=False):
        """return a pandas database of all the control matrices
        """
        defaultKeys = ['DATE','NAXIS1','NAXIS2','CONTEXT','PENALTY','WEIGHT','NS','NW','NWM','GMETHOD']
        if not only:
            keys = defaultKeys + keys
        if wildcard is None:
            wildcard = 'controlMatrix*'
        db = pflog(keys=keys,wildcard=wildcard,only=True)
        for ind in db.index:
            fileName = db.loc[ind,'FILE']
            for mode in ['8x','16x','30x']:
                if mode in fileName: db.loc[ind,'MODE'] = mode
            if 'LGS' in fileName:
                db.loc[ind,'GS'] = 'LGS'
            else:
                db.loc[ind,'GS'] = 'NGS'
            if 'sim' in fileName:
                db.loc[ind,'SIM'] = True
            else:
                db.loc[ind,'SIM'] = False
        return db

def mkLog(added_keys=None, wildcard=None, only=False, outputFileName='log.csv'):
    global keys
    
    if verbose:
        print 'added_keys:'
        print added_keys
        print 'wildcard:', wildcard
        print 'only:', only
    
    if (added_keys == 'all'):
        # open the first file and glean the keys from the header
        pass
    else:
        if (added_keys is not None and len(added_keys) > 0):
            if only:
                keys = added_keys
            else:
                keys = default_keys + added_keys
            if verbose: print keys
            if len(keys) == 0:
                print '<flog.mkLog> ERROR: keyword list is empty'
                return
        else:
            keys = default_keys
    
    # select the files
    file_list = os.listdir('.')
    sfile_list = []
    for fileName in file_list:
        if (fileName.endswith('.fits')):
            if wildcard is not None:
                if fnmatch.fnmatch(fileName,wildcard):
                    sfile_list.append(fileName)
            else:
                sfile_list.append(fileName)
    
    if len(sfile_list) == 0:
        print '<flog.mkLog> ERROR: file list is empty'
        return
    
    if verbose:
        print 'matching files:'
        print sfile_list

    if added_keys == 'all':
        # open the first file and glean the keys from the header
        hdu = pyfits.open(sfile_list[0])
        keys = hdu[0].header.keys()
        hdu.close()
    
    if outputFileName is None:
        outputFileName = 'log.csv'
    f = open(outputFileName,'w')
    
    sep = ','
    sep_replace = '-' # replace sep characters in values with this character
    lin = 'FILE NAME'
    for keyword in keys:
        lin += sep+keyword

    f.write(lin+'\n')

    for fileName in sfile_list:
        lin = fileName
        try:
            hdu = pyfits.open(fileName)
        except:
            print 'Trouble reading file '+fileName
            print sys.exc_info()
            continue
        hdr = hdu[0].header
        for keyword in keys:
            if (keyword in hdr):
                val = hdr.cards[keyword][1]
                if (type(val) == str):
                    val = val.replace(sep,sep_replace)
                lin = lin + sep + str(val)
            else:
                lin = lin + sep
        if (verbose): print lin
        f.write(lin+'\n')
        hdu.close()

    f.close()

doit = True
if __name__ == '__main__' and doit:
    usageStr = """
    flog [args]
    
    Create a log file in csv format for this directory
    listing the fits files and header values specified.
    Optional arguments:
    
        -h, --help: print help
        -v, --verbose: print what flog is doing verbosely
        -k, --key [arg,arg,...]: include extra header keys (separated by commas)
        -a, --all: get all header keys from the header in the first fits file
        -e, --exclusive: use only the keys supplied by -k, and don't include the default set
        -f, --file "wildcard_string": only list files whose name matches the wildcard
                Be sure to put quotes around the wildcard string
        -o, --output file: send output to file instead of log.csv
    
    The results are put in a file log.csv
    """
    doit = True
    keymode = False
    added_keys = []
    only = False
    fileNameWildcard = None
    wildcardmode = False
    list_defaults = False
    outputFilemode = False
    outputFileName = None
    if (len(sys.argv) > 1):
        for arg in sys.argv[1:]:
            if keymode:
                added_keys = arg.split(',')
                keymode = False
            elif wildcardmode:
                fileNameWildcard = arg
                wildcardmode = False
            elif outputFilemode:
                outputFileName = arg
                outputFilemode = False
            elif (arg in ['--help', '-h']):
                print usageStr
                print ''
                print 'default keys:'
                print default_keys
                print ''
                doit = False
            elif (arg in ['--all','-a']):
                added_keys = 'all'
            elif (arg in ['--verbose', '-v']):
                verbose = True
            elif (arg in ['-k','--key']):
                keymode = True
            elif (arg in ['-e','--exclusive']):
                only = True
            elif (arg in ['-f','--file']):
                wildcardmode = True
            elif (arg in ['-o','--output']):
                outputFilemode = True
            else:
                print 'unknown option ',arg
    if (doit):
        mkLog(added_keys=added_keys,
              wildcard=fileNameWildcard,
              only=only,
              outputFileName=outputFileName)
