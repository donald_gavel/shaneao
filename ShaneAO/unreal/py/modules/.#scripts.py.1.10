"""Script file for ShaneAO
execute this file at startup with::

    load(scripts)

"""
helper = {}

if shaneao.config['simulator']:

    import numpy as np
    import img
    import dimg
    
    helper['initsim'] = 'initialize the simulator'
    def initsim():
        """initialize the simulator for operation. This is equivalent
        to the calibration operations like dark and refcents
        that must be performed on the real system"""
        global b
    
        camstart()
        light('off')
        dark()
        light('on')
        mems('on')
        wrate(3)
        woof('on')
        zero()
        aber('ref')
        refcent()
        # now done by defaults():
        #b = np.zeros((1024+52))
        #b[0:1024] = .98
        #b[1024:1024+52] = .995
        #bleed(b)
        #gain(1.)
        open()
    
    helper['strehl'] = '(simulator only) calculate the Strehl'
    def strehl():
        """calculate the rms-error implied Strehl in the simulator"""
        global phe,ap,psf
        
        D_tele = sauce.D_tele
        D_sec = sauce.D_sec
        du_fine = sauce.du_fine
        r0 = 0.12
        d = D_tele/14.
        pha = sauce.aber_set[sauce.t]
        ap = img.circle((512,512),r=D_tele/du_fine/2.-2)
        ap = ap - img.circle((512,512),r=D_sec/du_fine/2.)
        wf = sauce.wf_tweeter + sauce.wf_woofer # radians at 589 nm wavelength
        e = pha - wf
        phe = img.depiston(e,ap)*(589./(2*np.pi)) # nm
        rmse = img.rms(e,ap) # radians
        s = np.exp( -(rmse*(.589/1.))**2 )
        cwf = shaneao.sauce.zeropad(ap*np.exp(1j*e*(.589/1.6)),(1024,1024))
        psf = np.fft.fftshift(np.abs(np.fft.fft2(cwf))**2)
        wfa = wf*ap
        pv = wfa.max()-wfa.min()
        tt = pv*(.589/(2*np.pi))/D_tele/4.848
        s0 = np.exp( -0.3*(d/r0)**(5./3.)*(0.589/1.0)**2)
        print ' '
        print 'tilt = '+str(tt)+' arcsec'
        print 'rms error = '+str(rmse)+ ' radians, ('+str(rmse*(589./(2*np.pi)))+' nm)'
        print 'Strehl = '+str(s)+ ' at 1 micron'
        print 'calculated fit Strehl = '+str(s0)+' at 1 micron'

    helper['show'] = '(simulator only) show the residual wavefront'
    def show(limit=300):
        """graphically display the wavefront residual in the simulator"""
        dimg.show(np.clip(phe*ap,-limit,limit),fig='phe',origin='lower')
        dimg.show(psf[512-20:512+20,512-20:512+20]**.25,fig='PSF (1.6u)')

def defaults():
	"""Set the default gains and bleeds, as determined
	from experiments with the simulator
	"""
	if (mode() == '16x'):
		bleed(.9,'tweeter')
		bleed(.995,'woofer')
		bleed_mask(.85,.9)
		alpha(0.2)
		gain(.7) # this closes the loop, so must follow with zero() to open
		zero()
		#refcent('data/refcents.fits')
		refcent('data/refcents_16x_2014-04-15.fits')
		cmFile = 'controlMatrix_16x.fits'
		if (shaneao.config['simulator']):
			cmFile = 'controlMatrix_16x_sim.fits'
		set_CM(cmFile)
		amount = [0., 0., 0.04, 0.04, 0., 0.,0., 0., 0., 0.]
	if (mode() == '8x'):
		bleed(.95,'tweeter')
		bleed(.99,'woofer')
		bleed_mask(.85,.9)
		alpha(0.2)
		gain(.35)
		zero()
		#refcent(0)
		refcent('data/refcents_8x_2014-04-15.fits')
		cmFile = 'controlMatrix_8x.fits'
		if (shaneao.config['simulator']):
			cmFile = 'controlMatrix_8x_sim.fits'
		set_CM(cmFile)
		amount = [0, 0, 0.05, 0.02, 0, 0, 0.02,   0.0, 0.0, 0.0]
	if (mode() == '16xLGS'):
		bleed(.9,'tweeter')
		bleed(.995,'woofer')
		bleed_mask(.85,.9)
		alpha(0.2)
		gain(.7) # this closes the loop, so must follow with zero() to open
		zero()
		#refcent('data/refcents.fits')
		#refcent('data/refcents_16x_2014-04-15.fits')
		#refcent('data/hs-20140506_16x.fits')
		refcent('data/hs-20140506_16x_onSky.fits')
		cmFile = 'controlMatrix_16xLGS.fits'
		if (shaneao.config['simulator']):
			cmFile = 'controlMatrix_16xLGS_sim.fits'
		set_CM(cmFile)
		amount = [0., 0., 0.04, 0.04, 0., 0.,0., 0., 0., 0.]
		shaneao.self.set_tt_centWts(roi_center=[49,37], roi_size=[6,6],algo='COG')
		#shaneao.self.set_tt_centWts(roi_center=[42,41], roi_size=[6,6],algo='COG')
		centReg([500.,1000.])
	if (mode() == '8xLGS'):
		bleed(.95,'tweeter')
		bleed(.99,'woofer')
		bleed_mask(.85,.9)
		alpha(0.2)
		gain(.35)
		zero()
		#refcent(0)
		refcent('data/refcents_8x_2014-04-15.fits')
		cmFile = 'controlMatrix_8xLGS.fits'
		if (shaneao.config['simulator']):
			cmFile = 'controlMatrix_8xLGS_sim.fits'
		set_CM(cmFile)
		amount = [0, 0, 0.05, 0.02, 0, 0, 0.02,   0.0, 0.0, 0.0]
		shaneao.self.set_tt_centWts(roi_center=[42,41], roi_size=[6,6],algo='COG')

def _ap():
    """ aperture
    """
    D_tele = 3.0
    D_sec = 0.8
    n_fine = 32
    du_fine = 0.1
    L = n_fine*du_fine/2.
    x1 = np.linspace(-L,L,n_fine)
    x,y = np.meshgrid(x1,x1)
    r = np.sqrt(x**2 + y**2)
    _ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)
    return _ap

helper['bleed_mask'] = 'Specify bleed values for actuators outside the illuminated aperture'
def bleed_mask(fast_bleed=.07,slow_bleed=.9):
    """Specify different bleed values for actuators outside the illuminated aperture"""
    p = _ap()
    u = bleed('tweeter')
    u[np.where(p == 0)] = fast_bleed
    u = u.reshape((1024))
    v = bleed('woofer')
    v[21]=slow_bleed
    v[22]=slow_bleed
    v[29]=slow_bleed
    v[30]=slow_bleed
    b = np.hstack([u,v])
    bleed(b)

def lgs_open():
	bleed(1);open();flat()

def lgs_close(g,wb=.99):
	bleed(.9);bleed('woofer',wb);gain(g)

def tt_roi(cent,roi_size=[6,6]):
	shaneao.self.set_tt_centWts(roi_center=cent, roi_size=[6,6],algo='COG')

helper['lasterr'] = 'Check for when there last was an error in woofer communication'
def lasterr():
	print shaneao.woofer.lasterr

import readfits as fits
import numpy as np
import img

tmap_doc = """
    maps:
    map[x] is the location in the buffer where the x command goes
    the original maps are the one used during the flattening calibration process
    """
wmap0 = fits.readfits('parameterFiles/woofer_map_original.fits').astype(int)
wmap = fits.readfits('parameterFiles/woofer_map.fits').astype(int)
tmap0 = fits.readfits('parameterFiles/tweeter_map_original.fits').astype(int).reshape((1024))
tmap = fits.readfits('parameterFiles/tweeter_map.fits').astype(int).reshape((1024))

woofer_flat = fits.readfits('data/woofer_flat.fits')
tweeter_flat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
tweeter_sphere = fits.readfits('data/BestFlat_FullAp_Bias132_wPower.fits')

tweeter_flat = np.clip(tweeter_flat,0,1)*65535
u = np.zeros((1024))
u[tmap0] = tweeter_flat.reshape((1024))
a_tweeter_flat = u[tmap].copy()

tweeter_sphere = np.clip(tweeter_sphere,0,1)*65535
u = np.zeros((1024))
u[tmap0] = tweeter_sphere.reshape((1024))
a_tweeter_sphere = u[tmap].copy()

u = np.zeros((52))
u[wmap0] = woofer_flat
a_woofer_flat = u[wmap].copy()

a = shaneao.rtc.rtc2.peek('a')

helper['flat'] = 'Set actuators to best known flat'
def flat(dm='both'):
    """Set actuators on one or both DMs to the best known flat position
    """
    if (dm == 'both'):
            a[0:1024] = a_tweeter_flat
            a[1024:1024+52] = a_woofer_flat
    if (dm == 'tweeter'):
            a[0:1024] = a_tweeter_flat
    if (dm == 'woofer'):
            a[1024:1024+52] = a_woofer_flat

helper['flat0'] = 'Set the tweeter actuator zero points to the best known flat'
def flat0():
    """Set the tweeter actuator zero points (a0) to the best-fit-flat shape"""
    a0 = shaneao.self.a0
    a0[0:1024] = a_tweeter_flat
    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

helper['sphere'] = 'Set the tweeter actuators to the best-fit-to-sphere shape'
def sphere():
    """Set the tweeter actuators to the best-fit-to-sphere shape"""
    a[0:1024] = a_tweeter_sphere

helper['sphere0'] = 'Set the tweeter actuator zero points to the best-fit-to-sphere shape'
def sphere0():
    """Set the tweeter actuator zero points (a0) to the best-fit-to-sphere shape"""
    a0 = shaneao.self.a0
    a0[0:1024] = a_tweeter_sphere
    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

helper['bias'] = 'Set the tweeter actuators to the bias (a0)'
def bias():
    """Set the tweeter actuators to the bias (a0)"""
    zero()

helper['bias0'] = 'Set the tweeter actuator zero points (a0) to a bias'
def bias0(b=21500.):
    """Set the tweeter actuator zero points (a0) to a bias"""
    bias_min = 0
    bias_max = 43000
    a0 = shaneao.self.a0
    a0[0:1024] = np.clip(b,bias_min,bias_max)
    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

helper['poke'] = 'Put a Gaussian bump on the tweeter centered at a position'
def poke(loc,val):
    """Put a Gaussian bump on the tweeter centered at a given position"""
    m = loc
    u = img.gauss2((32,32),2,c=m)
    a[0:1024] = (u*val + 12500).reshape((1024))

helper['wpoke'] = 'Poke one actuator on the woofer'
def wpoke(loc,val):
    """Poke one actuator on the woofer"""
    b = np.zeros((52))
    b[loc] = val
    a[1024:1024+52] = b.copy()

helper['scan'] = 'Poke one actuator at a time, cycling through all actuators'
def scan(dm='woofer',dt=None):
    """Poke one actuator at a time in raster scan order on the deformable mirror
    ('woofer' or 'tweeter'), pausing dt seconds between each poke."""
    zero()
    if (dm == 'woofer'):
        if (dt == None): dt = .3
        time.sleep(dt)
        for k in range(52):
            wpoke(k,.3)
            time.sleep(dt)
    if (dm == 'tweeter'):
        if (dt == None): dt = .04
        time.sleep(dt)
        for y in range(32):
            for x in range(32):
                poke((x,y),10000)
                time.sleep(dt)
    zero()

def show_woof(aw):
    """Convert a 52-element list of woofer commands into an 8x8
    array, for display purposes"""
    wmask = np.ones((8*8))
    wmask[[0,1,6,7,8,15,48,55,56,57,62,63]]=0
    aw2 = np.zeros((8*8))
    i = 0
    for k in range(8*8):
        if wmask[k]:
            aw2[k] = aw[i]
            i += 1	
    aw2 *= wmask
    aw2 = aw2.reshape((8,8))
    return aw2

import readline
def history():
    hist_set = []
    n = readline.get_current_history_length()
    for i in range(n):
        item = readline.get_history_item(i)
        hist_set.append(readline.get_history_item(i))
        print str(i)+'    '+str(item)

import os
import time
import readfits as fits
import pyfits

def wfs_cam_run(n_frames=5, delta_t = 1.0):    
    """run the WFS Camera for some period and save
    files to archive
    """
    wfs = shaneao.rtc.rtc2.peek('wfs')
    imap = fits.readfits('parameterFiles/i_map.fits')
    u = []
    t = []
    for k in range(n_frames):
	time.sleep(delta_t)
	u.append(wfs.copy()[imap].reshape((160,160)))
	t.append(time.time())
	print 'frame '+str(k)
    wfs_cam_archive(u,t)

def wfs_cam_archive(u,t,comment=None):
    archive_dir = 'data_'+time.strftime('%Y-%m-%d')
    if (not os.path.exists(archive_dir)):
	os.mkdir(archive_dir)
    if (not os.path.exists(os.path.join(archive_dir,'wfs_seq_no'))):
        seq_no = np.array([0])
        seqhdu = pyfits.PrimaryHDU(seq_no)
        seqhdu.header['seq_no'] = (0,'sequence number')
        seqhdu.writeto(os.path.join(archive_dir,'wfs_seq_no'))
    if (comment == None):
        comment = raw_input('Enter a description of this wfs data: ')
    seqhdu = pyfits.open(os.path.join(archive_dir,'wfs_seq_no'),mode='update')
    seq_no = seqhdu[0].data[0]
    n_frames = len(u)
    for k in range(n_frames):
	tl = time.localtime(t[k])
	day = time.strftime('%Y-%m-%d',tl)
	tim = time.strftime('%H%M%S',tl)
	hdu = pyfits.PrimaryHDU(u[k])
	hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
	hdu.header.append(('DEVICE','WFS Camera'))
	hdu.header.append(('RATE',shaneao.self.rate,' frame rate'))
	hdu.header.append(('DATE',day,'File generation date'))
	hdu.header.append(('TIME',tim,'file generation time hhmmss'))
	hdu.header.append(('GENER','scripts.py','wfs_cam_archive'))
	hdu.header['comment'] = comment
	fitsFileName = os.path.join(archive_dir,'WFS_Data_'+ '%04d' % seq_no +'.fits')
	print 'writing to ' + fitsFileName
	hdu.writeto(fitsFileName)
	time.sleep(.1)
	seq_no += 1
    seqhdu[0].header['seq_no'] = (seq_no,'sequence number')
    seqhdu[0].data[0] = seq_no
    seqhdu.close()

def tt_cam_run(n_frames=5, delta_t = 1.0):    
    """run the TT Camera for some period and save
    files to archive
    """
    tts = shaneao.rtc.rtc2.peek('tts')
    u = []
    t = []
    for k in range(n_frames):
	time.sleep(delta_t)
	u.append(tts.copy().reshape((80,80)))
	t.append(time.time())
	print 'frame '+str(k)
    tt_cam_archive(u,t)

def tt_cam_archive(u,t,comment=None):
    archive_dir = 'data_'+time.strftime('%Y-%m-%d')
    if (not os.path.exists(archive_dir)):
	os.mkdir(archive_dir)
    if (not os.path.exists(os.path.join(archive_dir,'tts_seq_no'))):
        seq_no = np.array([0])
        seqhdu = pyfits.PrimaryHDU(seq_no)
        seqhdu.header['seq_no'] = (0,'sequence number')
        seqhdu.writeto(os.path.join(archive_dir,'tts_seq_no'))
    if (comment == None):
        comment = raw_input('Enter a description of this tip-tilt data: ')
    seqhdu = pyfits.open(os.path.join(archive_dir,'tts_seq_no'),mode='update')
    seq_no = seqhdu[0].data[0]
    n_frames = len(u)
    for k in range(n_frames):
	tl = time.localtime(t[k])
	day = time.strftime('%Y-%m-%d',tl)
	tim = time.strftime('%H%M%S',tl)
	hdu = pyfits.PrimaryHDU(u[k])
	hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
	hdu.header.append(('DEVICE','TT Camera'))
	hdu.header.append(('RATE',shaneao.self.ttrate,' frame rate'))
	hdu.header.append(('DATE',day,'File generation date'))
	hdu.header.append(('TIME',tim,'file generation time hhmmss'))
	hdu.header.append(('GENER','scripts.py','tts_cam_archive'))
	hdu.header['comment'] = comment
	fitsFileName = os.path.join(archive_dir,'TT_Data_'+ '%04d' % seq_no +'.fits')
	print 'writing to ' + fitsFileName
	hdu.writeto(fitsFileName)
	time.sleep(.1)
	seq_no += 1
    seqhdu[0].header['seq_no'] = (seq_no,'sequence number')
    seqhdu[0].data[0] = seq_no
    seqhdu.close()
    
wfs = shaneao.rtc.rtc2.peek('wfs')
tts = shaneao.rtc.rtc2.peek('tts')
s_ref = shaneao.rtc.rtc2.peek('s_ref')
a_flat = fits.readfits('data/a_flat_3-18.fits')
a_flat_doc = """The Tweeter and Woofer DM voltages when held closed loop
    on 3/18/14 on the 16x ref cents that were recorded with a flat in place of woofer
    and tweeter at interferometer flat."""
    
ref_cents_flat = fits.readfits('data/refcents.fits')
ref_cents_flat_doc = """The 16x reference centroids taken on 3/18/14
    with a flat in place of woofer and tweeter at interferometer flat."""
