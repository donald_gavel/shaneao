"""Generate a parameter set for ShaneAO and store it as
a set of FITS files
"""
module_file_name = __name__+'.py'

import sys
import os
from astropy.io import fits
import numpy as np
import datetime
import time
from img_3 import circle
import tweeter_3 as tweeter
import warnings

# suppress the user wanings generated when saving intermediate fits files
#warnings.resetwarnings()
warnings.filterwarnings('ignore', category=UserWarning, append=True)
warnings.filterwarnings('ignore', category=DeprecationWarning, append=True)

home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')
dataDir = os.path.join(home,'data')
valid_modes = ['8x','16x','30x']

def idisk(shape,c=None,r=None,rs=-1):
    '''illuminated disk. Create an image of a disk with the given center and radius.
    The values of each pixel are the "fraction of illumination" that is, the area overlap integrals
    of each pixel square with the disk.
    
    args
    shape - 2-tuple of ints
        2 dimensional shape of the image: (width,height)
    c - 2-tuple of floats
        The center of the disk, (x,y). Positions on the image start at the lower left corner of pixel 0,0 being
        position (0.,0.), each pixel size being 1.0,1.0, and the upper right corner of pixel n,m
        being position (n+1.0, m+1.0). Defaults to the center of the array.
    r - float
        The radius of the disk. Defaults to half the array's minimum dimension.
    rs - float
        The radius of the 'center obscuration'. Defaults to -1 which means no obscuration.
    '''    
    n,m = shape # width x height
    if c is None:
        c = (n//2,m//2)
    if r is None:
        r = np.min(n,m)//2
    cx,cy = c
    nn = 10 # 10x10 subsample of a partially illuminated pixel
    nf = nn**2 # total number of subpixels in a pixel
    ss = 1/nn # the subpixel size (in units of pixels)
    
    x = np.arange(n)
    y = np.arange(m)
    x,y = np.meshgrid(x,y)
    v = [np.sqrt((xx-cx)**2+(yy-cy)**2) for xx in [x,x+1] for yy in [y,y+1]]
    q = [np.where( (vv<=r) & (vv>rs),1,0) for vv in v]
    q2 = np.array(q).sum(axis=0)
    iset = np.where(q2==4) # pixels where all 4 corners are completely inside the circle
    niset = np.where(q2==0) # pixels where all 4 corners are completely outside the circle
    piset = np.where( (q2>0) & (q2<4) ) # pixels where the circle boundary crosses through the pixel: "partially illuminated"
    
    z = np.zeros((m,n))
    z[iset] = 1.
    z[niset] = 0.

    for a in zip(*piset):
        j,k = a
        x,y = [np.arange(nn)*ss+ss/2+a[ax] for ax in [1,0]]
        x,y = np.meshgrid(x,y)
        rr = np.sqrt((x-cx)**2+(y-cy)**2)
        rr = np.where((rr<=r) & (rr>rs),1,0)
        z[j,k] = rr.sum()/nf
    
    return z

def readfits(filename,verbose=False,raiseError=True,isHST=False,return_header=False):
    """Reads a FITS file from disk given the file name."""
    if (verbose):
        print('<readfits> reading file '+filename + '...',end='')
        sys.stdout.flush()
    if (os.path.isfile(filename) == False):
        err_str = "<readfits> no such file "+filename
        if (raiseError):
            raise IOError(err_str)
            return
        warnings.warn(err_str)
        return np.array([0.])
    hdulist = fits.open(filename)
    if (isHST):
        data = hdulist[1].data
        hdr = hdulist[1].header
    else:
        data = hdulist[0].data
        hdr = hdulist[0].header
    hdulist.close()
    if (verbose):
        print('done')
    if (return_header):
        return data,hdr
    return data

def protectFile(filename,verbose=False):
    if (os.path.isfile(filename)):
        dirname = os.path.dirname(filename)
        basename = os.path.basename(filename)
        archivedir = os.path.join(dirname,'archive')
        if (not os.path.exists(archivedir)):
            os.mkdir(archivedir)
        protectedName = os.path.join(archivedir,basename)+'.'+time.strftime('%Y-%m-%dT%H:%M:%S')
        if (verbose):
            print('<fits.protectFile> protecting file '+filename + ' to '+protectedName)
            sys.stdout.flush()            
        os.rename(filename,protectedName)

gen_set = []

def gen(item=None):
    if (item == None):
        print('<param_gen.gen> choose from '+str(gen_set))
        print('<param_gen.gen> or "all" to generate everything')
        return
    
    if (item == 'all'):
        for generator_func in gen_set:
            generator_func()
        return

def gen_dark():
    """Generate the WFS Background
    using previously collected dark data
    """
    print('<gen_dark> generating wfs_dark')
    data_filename = '/Users/donaldgavel/Dropbox/ShaneAO/Lab_Tests/wfs_frames_2-22-13.fits'
    hdulist = fits.open(data_filename)
    nf = 34 # 34 frames are useful in this set, rest are zero
    header = hdulist[0].header
    data = hdulist[0].data
    n = header['NAXIS1']
    bg = np.zeros((n,n))
    for k in range(nf):
        bg += data[k,:,:]
    bg /= float(nf)
    hdulist.close()
    
    filename = os.path.join(paramsDir,'wfs_dark.fits')
    hdu = fits.PrimaryHDU(bg)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','camera background'))
    hdu.header.append(('SYMBOL','background'))
    hdu.header.append(('ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('EXP',0.01,' Exposure time, seconds'))
    hdu.header.append(('NAVE',nf,' Number of frames averaged'))
    hdu.header.append(('DINT',True,' Data is de-interlaced'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    hdu.header['COMMENT'] = 'based on data '+os.path.basename(dfilename)
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_dark> wrote '+filename)

gen_set.append(gen_dark)

def gen_flat():
    """Generate the WFS Flat
    For now we assume the flat is uniform, so it is set to all ones
    """
    print('<gen_flat> generating wfs_flat')
    filename = os.path.join(paramsDir,'wfs_flat.fits')
    flat = np.ones((160,160))
    hdu = fits.PrimaryHDU(flat)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','camera flat field'))
    hdu.header.append(('SYMBOL','flat'))
    hdu.header.append(('ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DINT',True, ' Data is de-interlaced'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_flat> wrote '+filename)

gen_set.append(gen_flat)

def gen_weights():
    """Generate the centroider weights
    Center of Gravity, Quad cell, and Binned Quad cell
    """
    # ---- Centroider Weights
    print('<gen_weights> generating cent_weights_cog')
    filename = os.path.join(paramsDir,'cent_weights_cog.fits')
    w = np.zeros((3,5,5))
    w[0,0:4,0:4] = [[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5]]
    w[1,:,:] = w[0,:,:].transpose()
    w[2,0:4,0:4] = np.ones((4,4))  
    hdu = fits.PrimaryHDU(w)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','centroider weights'))
    hdu.header.append(('SYMBOL','w_[x,y,i]'))
    hdu.header.append(('ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('ALGO','COG',' Center of gravity'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_weights> wrote '+filename)
    
    print('<gen_weights> generating cent_weights_q2')
    filename = os.path.join(paramsDir,'cent_weight_q2.fits')
    w = np.zeros((3,5,5))
    w[0,0:4,0:4] = [[0,-1,1,0],[0,-1,1,0],[0,-1,1,0],[0,-1,1,0]]
    w[1,:,:] = w[0,:,:].transpose()
    w[2,1:3,1:3] = np.ones((2,2))
    hdu = fits.PrimaryHDU(w)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','centroider weights'))
    hdu.header.append(('SYMBOL','w_[x,y,i]'))
    hdu.header.append(('ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('ALGO','QUAD2',' 2x2 quad cell'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_weights> wrote '+filename)
    
    print('<gen_weights> generating cent_weights_q4')
    filename = os.path.join(paramsDir,'cent_weight_q4.fits')
    w = np.zeros((3,5,5))
    w[0,0:4,0:4] = [[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1]]
    w[1,:,:] = w[0,:,:].transpose()
    w[2,0:4,0:4] = np.ones((4,4))
    hdu = fits.PrimaryHDU(w)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','centroider weights'))
    hdu.header.append(('SYMBOL','w_[x,y,i]'))
    hdu.header.append(('ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('ALGO','QUAD4',' 4x4 (binned) quad cell'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_weights> wrote '+filename)

gen_set.append(gen_weights)

def gen_actuator_defaults(mode):
    nt = 1024 # number of tweeter actuators
    abias = 43000/2 # 100 volts in D/A units
    at0 = np.ones((nt))*abias
    nw = 52 # number of woofer actuators
    wbias = 0. # assuming [-1..1] is full range
    aw0 = np.ones((nw))*wbias
    nf = 14 # number of woofer mode filters (up to zernike order 4, minus piston)
    af0 = np.zeros((nf))
    if mode.endswith('LGS'):
        ntt = 2
        ubias = 5.
        att0 = np.ones((ntt))*ubias
        a0 = np.hstack([at0,aw0,af0,att0])
        n = nt+nw+nf+ntt
        filename = os.path.join(paramsDir,'actuator_defaults_LGS.fits')
    else:
        ntt = 0
        a0 = np.hstack([at0,aw0,af0])
        n = nt+nw+nf
        filename = os.path.join(paramsDir,'actuator_defaults.fits')
    hdu = fits.PrimaryHDU(a0)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','DMs'))
    hdu.header.append(('FUNC',' default actuator and filter states'))
    hdu.header.append(('SYMBOL','a0'))
    hdu.header.append(('NAT',nt,' Number of tweeter actuators'))
    hdu.header.append(('NAW',nw,' Number of woofer actuators'))
    hdu.header.append(('NF',nf,' Number of woofer mode filters'))
    hdu.header.append(('NTT',ntt,' Number of uplink tip/tilt states'))
    hdu.header.append(('ASTEP','1.4, 1.5',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)

def gen_integrator_bleeds(mode):
    nt, nw, nf = (1024,52,14)
    if (mode.endswith('LGS')):
        ntt = 2
        filename = os.path.join(paramsDir,'integrator_bleeds_LGS.fits')
    else:
        ntt = 0
        filename = os.path.join(paramsDir,'integrator_bleeds.fits')
    b = np.ones((nt+nw+nf+ntt))
    hdu = fits.PrimaryHDU(b)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','DMs'))
    hdu.header.append(('FUNC',' integrator bleed coefficients'))
    hdu.header.append(('SYMBOL','b'))
    hdu.header.append(('NAT',nt,' Number of tweeter actuators'))
    hdu.header.append(('NAW',nw,' Number of woofer actuators'))
    hdu.header.append(('NF',nf,' Number of woofer mode filters'))
    hdu.header.append(('NTT',ntt,' Number of uplink tip/tilt states'))
    hdu.header.append(('ASTEP','1.5',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)

def gen_actuator_limits(mode):
    nt, nw, nf = (1024,52,14)
    if (mode.endswith('LGS')):
        ntt = 2
        filename = os.path.join(paramsDir,'actuator_limits_LGS.fits')
    else:
        ntt = 0
        filename = os.path.join(paramsDir,'actuator_limits.fits')
    n = nt+nw+nf+ntt
    a_limit = np.zeros((2,n)) # stores both upper and lower limits of all integrators
    a_limit[0,0:nt] = 0. # min on tweeter
    a_limit[1,0:nt] = 43000 # max on tweeter
    a_limit[0,nt:nt+nw] = -1. # min on woofer
    a_limit[1,nt:nt+nw] = 1. # max on woofer
    a_limit[0,nt+nw:nt+nw+nf] = -2. # min and max on filters, need to experiment with this setting
    a_limit[1,nt+nw:nt+nw+nf] = 2.
    if (ntt != 0):
        a_limit[0,nt+nw+nf:nt+nw+nf+ntt] = 0.
        a_limit[1,nt+nw+nf:nt+nw+nf+ntt] = 10.  # volts sent to uplink D/A
    
    hdu = fits.PrimaryHDU(a_limit)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','DMs'))
    hdu.header.append(('FUNC',' integrator limits'))
    hdu.header.append(('SYMBOL','a_[min,max]'))
    hdu.header.append(('NAT',nt,' Number of tweeter actuators'))
    hdu.header.append(('NAW',nw,' Number of woofer actuators'))
    hdu.header.append(('NF',nf,' Number of woofer mode filters'))
    hdu.header.append(('ASTEP','1.5',' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    
def penalty(At,H,W = None):
    """Define the penalty matrix, S, for the modal based reconstructor.
    S = A^T sum_k{a_bar_k w_k a_bar_k} A
    where A maps mode coefficients to actuator vectors (modes in terms of
    actuators are the columns of A), a_bar_k are column vectors of 'bad'
    actuator patterns, and w_k is the penalty weight of the k'th bad patterns.
    S can also be expressed
    S = A^T A_bar W A_bar^T A
    where A_bar is the matrix formed by stacking the a_bar_k as columns and
    W is diag(w_k), a diagonal matrix with w_k on the diagnoal.
    
    Bad actuator patterns include:
    
    * any actuator outside the aperture
    * piston on the aperture
    * any 2x2 waffle pattern
    * derivatives (first differences) across the pupil boundary or outside the pupil

    **Inputs:** At is the matrix mapping mode coefficients to
    actuator vectors. H is the matrix mapping mode coefficients
    to Hartmann sensor slope vectors.
    
    **Returns** (S,Abar,H_d,R,U,s) where S is the penalty matrix,
    Abar is the set of bad patterns,
    H_d is the mode coefficient reconstruction matrix: H_d = (H^T H + S)^-1 * H^T
    R is the actuator reconstruction matrix: R = At * H_d
    U is the set of singular modes of the actuator reconstruction matrix
    s is the set of singular values of the actuator reconstruction matrix
    """
    Abar = []
    w = []
    ns2 = H.shape[0]
    if (W is None):
        W = np.matrix(np.identity(ns2))
    tw = tweeter.tweeter()
    nacts = tw.nacts # total number of actuators
    na = tw.na       # number of actuators across
    ap = tw.ap.reshape((nacts))
    
    # actuators outside the aperture
    z = np.where(ap == 0)[0]
    abar = np.zeros((nacts))
    for k in z:
        abar[k] = 1.
        Abar.append(abar.copy())
        abar[k] = 0.
        w.append(200.)
    
    # piston on the aperture
    Abar.append(ap.copy())
    w.append(1.)
    
    # any 2x2 waffe pattern
    ap = ap.reshape((na,na))
    al = np.array(tw.actLocs,dtype=int).copy() + na/2
    for x,y in al:
        if (ap[x,y]):
            if ( ((x-1)>=0) and ((y-1)>=0) ):
                abar = np.zeros((na,na))
                abar[x,y] = 1.
                abar[x-1,y-1] = 1.
                abar[x,y-1] = -1.
                abar[x-1,y] = -1.
                Abar.append(abar.reshape((nacts)))
                w.append(1.)

    k = 0
    for abar in Abar:
        abar *= np.sqrt(w[k])
        k += 1
    
    n_bad_modes = len(Abar)
    norm_factor = 1e-12
    
    Abar = np.matrix(np.array(Abar).transpose())
    
    n_tweeter_modes = At.shape[1]

    S = At.T * Abar * Abar.T * At
    S *= norm_factor
    H_d = (H.T*W*H + S).I*H.T*W
    R = At * H_d
    U,s,V = np.linalg.svd(H_d,full_matrices=False)
    
    Abar = np.array(Abar.T).reshape((n_bad_modes,na,na))
    U = np.array((At*U).T).reshape((n_tweeter_modes,na,na))
    
    return S,Abar,H_d,R,U,s

def weights(mode,weighting=None):
    valid_weightings = ['wls','dim','none']
    # sensor weightings
    if (mode == '16x'):
        ns = 144
        low = [0,1,4,5,36,42,43,49,
               50,61,55,56,66,67,
               76,77,82,87,88,93,
               94,100,101,107,138,139,142,143]
        mid = [2,3,62,71,72,81,140,141,
               108,120,130,137,129,119,
               6,24,14, 13,23,35]
    elif (mode == '8x'):
        ns = 40
        low = [0,1,2,7,14,19,20,25,32,37,38,39]
        mid = []
    else:
        raise ValueError("mode must be '8x' or '16x'")
        
    low.sort()
    mid.sort()

    wts = np.ones((ns*2))
    if (weighting not in ['none',None]):
        if (weighting not in valid_weightings):
            print('<param_gen.weights> '+str(weighting)+' needs to be one of '+str(valid_weightings))
            return
        if (weighting == 'wls'):
            #set the weights here
            for k in low:
                wts[k] = 0.3
                wts[k+ns] = 0.3
            for k in mid:
                wts[k] = 0.5
                wts[k+ns] = 0.5
        elif (weighting == 'dim'):
            for k in low:
                wts[k] = 0.
                wts[k+ns] = 0.
            for k in mid:
                wts[k] = 0.
                wts[k+ns] = 0.

    W = np.matrix(np.diag(wts))
    return W

def hget(hdr,key):
    if key in hdr:
        return hdr[key]
    else:
        return 'unknown'
    
def parseDir(workDir=None,subdir=''):
    """Parse a working directory string according to rules:
    ~ = home
    . = here
    ~/xxx = relative to home
    """
    if workDir is not None:
        if (workDir == '.'):
            inDir = os.getcwd()
            outDir = inDir
        elif workDir == '~':
            inDir = home
            outDir = inDir
        elif workDir.startswith('~/'):
            inDir = os.path.join(home,workDir[2:])
            outDir = inDir
        else:
            inDir = os.path.join(subdir,workDir)
            outDir = inDir
    else:
        inDir = subdir
        outDir = paramsDir
    
    assert os.path.isdir(inDir),inDir+" is not a directory"
    assert os.path.isdir(outDir),outDir+" is not a directory"

    return inDir,outDir

def resetParamsHome(home_dir):
    'use this to change the home directory for parameterFiles. Use an absolute path.'
    global paramsDir
    paramsDir = os.path.join(home_dir,'parameterFiles')
    
def gen_CM_16x(LGS_also=True,use_penalty=True,method = 1,weighting = None, workDir=None):
    """Generate the 16x control matrix.
    This uses the matrices At and Aw (the mode definitions) and the
    'push matrices' H and Hw that were generated by running calibrate.py on the
    real-time controller.
    
    References
    ----------
    [1] The algorithm is described in RTC_WindPCMeeting_PresentationV2.pdf [https://drive.google.com/file/d/0B4FdgbaIbE_YcTFLaXg5Z091Tmc/view?usp=sharing]
    [2] and in Gavel & Norton, SPIE 9148, 2014 [https://drive.google.com/file/d/0B4FdgbaIbE_YazZ1ODNuR2JwZUE/view?usp=sharing]
    [3] The process of calibrating (generating H and Hw) is explained in calibrationGuide.pdf [https://drive.google.com/file/d/1py-Je5GDD8EMsypdvTMOZClQfP33kTBK/view?usp=sharing]
    
    Parameters
    ----------
    LGS_also : Boolean
        Triggers gen_CM_16xLGS to generate the LaserGuideStar control matrix also
    use_penalty : Boolean
        Use the 'actuator penalty' method to generate the tweeter reconstructor.
        This is *highly recommented* to prevent waffle modes from occuring in closed loop.
        Otherwise use user-provided reqularization matrices Q and P
    method : int
        Only method=1 is allowed. (There used to be a method 2, but it didn't work)
    weighting : str or None
        Sensor weighting scheme, either
            'wls' - weighted least squares, which weights subapertures according to illumination
            'dim' - dim star sets partially illuminated subaperture weights to zero
            or None - all subapertures equally weighted
    workDir : str
        The working directory that contains the push-matrix FITS files,
        and will also contain the results.
        This is the name of a subdirectory under
        [HOME]/parameterFiles/reconMatrix_16x/
        and is typically named the present date, as in 'Mar12'.
        HOME is the home directory of the user running this routine.
        If you need to change the definition of HOME because the parameter files
        are located elsewhere, you can use
        the function resetParamsHome(home_dir) where home_dir is the absolute path
        to the directory that contains the 'parameterFiles' directory.

    """
    method = 1
    
    # ---- control matrix
    print('<param_gen> generating controlMatrix_16x')
    # 16x case:
    na,naw,nf,ns = 1024,52,14,144
    subdir = os.path.join(paramsDir,'reconMatrix_16x')
    
    inDir,outDir = parseDir(workDir=workDir,subdir = subdir)
        
    At = readfits(os.path.join(subdir,'At.fits'))
    Aw = np.matrix(readfits(os.path.join(subdir,'Aw.fits')))
    if (not use_penalty):
        Q = np.matrix(readfits(os.path.join(subdir,'Q.fits')))
        P = np.matrix(readfits(os.path.join(subdir,'P.fits')))
    H,H_hdr = readfits(os.path.join(inDir,'H.fits'),return_header=True)
    H = np.matrix(H)
    if (method == 1):
        Hw,Hw_hdr = readfits(os.path.join(inDir,'Hw.fits'),return_header=True)
        Hw = np.matrix(Hw)
    if (method == 2):
        Mt = np.matrix(readfits(os.path.join(subdir,'Mt.fits')))
        Mw = np.matrix(readfits(os.path.join(subdir,'Mw.fits')))
        Cwt = np.matrix(readfits(os.path.join(subdir,'Cwt.fits')))

    # sensor weightings
    W = weights('16x',weighting)

    # determine the date and context of the input data
    context = hget(H_hdr,'CONTEXT')
    assert context in ['Simulator','Real']
    hdate = hget(H_hdr,'DATE')
    h_timeStamp = hget(H_hdr,'TSTAMP')
    hamp = hget(H_hdr,'AMP')
    hbias = hget(H_hdr,'BIAS')
    hdelay = hget(H_hdr,'DELAY')
    hwdate = hget(Hw_hdr,'DATE')
    hw_timeStamp = hget(Hw_hdr,'TSTAMP')
    hwamp = hget(Hw_hdr,'AMP')
    hwbias = hget(Hw_hdr,'BIAS')
    hwdelay = hget(Hw_hdr,'DELAY')

    n_tweeter_modes = At.shape[0]
    At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
    Aw = Aw[0:nf,:].transpose()

    if (method == 2):
        Cwt = Cwt[0:nf,:]
        Mw = Mw[0:nf,0:nf]
        Mw = np.linalg.pinv(Mw)
        Mt = np.linalg.pinv(Mt,rcond=.001)
        # there must be something wrong with the tweeter overlap integrals. Mt is not positive-definite
        # It's the fact that it is band-limited (100 bands) to speed up calculation
        # rcond of 0.001 throws out 36 (16%) of the eigenvectors in the 224 dimensional tweeter mode space
    
    if (use_penalty):
        S,Abar,H_d,R,U,s = penalty(At,H,W)
        
        # print("H_d: {0}".format(H_d[0,0]))
        # hdu = fits.PrimaryHDU(H_d)
        # hdu.writeto(os.path.join(outDir,"H_d.fits"), clobber=True)
        # hdu = fits.PrimaryHDU(S)
        # hdu.writeto(os.path.join(outDir,"S.fits"), clobber=True)
        # hdu = fits.PrimaryHDU(Abar)
        # hdu.writeto(os.path.join(outDir,"Abar.fits"), clobber=True)
        # hdu = fits.PrimaryHDU(W)
        # hdu.writeto(os.path.join(outDir,"W.fits"), clobber=True)
        # hdu = fits.PrimaryHDU(U)
        # hdu.writeto(os.path.join(outDir,"U.fits"), clobber=True)
        # hdu = fits.PrimaryHDU(R)
        # hdu.writeto(os.path.join(outDir,"R.fits"), clobber=True)
    else:
        H_d = H.T*(Q + H*P*H.T).I
        R = At*H_d

    if (method == 1):
        Hw = Hw[:,0:nf]
        Wsr = np.sqrt(W)
        try:
            Hw_d = (Hw.T*Hw).I*Hw.T
        except:
            print('<param_gen.gen_CM_16x> failed Hw_d = (Hw.T*Hw).I*Hw.T calculation (matrix singular?)')
            print('<param_gen.gen_CM_16x> proceding with pseudo-inverse approach')
            Hw_d = np.linalg.pinv(Wsr*Hw,rcond=.01)*Wsr
        cm = np.vstack(
            [np.hstack([ -R,         -At*H_d*Hw                     ]),
             np.hstack([ -Aw*Hw_d,    np.matrix(np.zeros((naw,nf))) ]),
             np.hstack([ -Hw_d,      -np.matrix(np.identity(nf))    ])
            ]
        )

    if (method == 2):
        Hw_d = Mw*Cwt*H_d
        Rw = Aw*Hw_d
        
        cm = np.vstack(
             [np.hstack([ -R,            -At*Mt*Cwt.transpose()          ]),
              np.hstack([ -Aw*Mw*Cwt*H_d, np.matrix(np.zeros((naw,nf)))  ]),
              np.hstack([ -Mw*Cwt*H_d,   -np.matrix(np.identity(nf))     ])]
             )
    hdu = fits.PrimaryHDU(Hw_d)
    hdu.writeto(os.path.join(outDir, "Hw_d.fits"), clobber=True)
    # the bottom row will be multplied by (1-alpha) appropriate to each frame rate,
    # where alpha is the time constant of the woofer
    
    #remove woofer and tweeter piston
    pw = np.matrix(np.ones(naw))
    tw = tweeter.tweeter()
    pt = tw.ap.reshape((na))
    ptd = np.matrix(np.diag(pt))
    pt = np.matrix(pt)
    n = np.sum(pt)
    Pr_t = ptd - pt.transpose()*pt/float(n)
    Pr_w = np.matrix(np.identity(naw)) - pw.transpose()*pw/float(naw)
    I_tt = np.matrix(np.identity(na))
    I_ww = np.matrix(np.identity(naw))
    I_ff = np.matrix(np.identity(nf))
    O_tw = np.matrix(np.zeros((na,naw)))
    O_wt = O_tw.transpose()
    O_tf = np.matrix(np.zeros((na,nf)))
    O_ft = O_tf.transpose()
    O_wf = np.matrix(np.zeros((naw,nf)))
    O_fw = O_wf.transpose()
    
    #hdu = fits.PrimaryHDU(Pr_w)
    #hdu.writeto(os.path.join(outDir, "Pr_w.fits"), clobber=True)
    
    Pr = np.vstack(
        [
            np.hstack([I_tt, O_tw, O_tf]), # I_tt <-> Pr_t: Identity or Piston Removed on tweeter
            np.hstack([O_wt, Pr_w, O_wf]),
            np.hstack([O_ft, O_fw, I_ff])
        ]
    )
    
    cm = Pr*cm
    
    # Format of a file name = "controlMatrix_"mode{"LGS"}{_weighting}{"_sim"}".fits"
    #                  mode = ["8x" | "16x" | "30x"]
    #                  weighting = ["wls" | "dim"]
    parsedBaseName = ['controlMatrix','16x']
    if (weighting not in ['none',None]):
        parsedBaseName.append(weighting)
    if context == 'Simulator':
        parsedBaseName.append('sim')
    filename = '_'.join(parsedBaseName)+'.fits'
    filename = os.path.join(outDir,filename)
    if weighting is None:
        weighting = 'none'
    
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')
    
    hdu = fits.PrimaryHDU(cm)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'),end=True)
    hdu.header.append(('ITYPE','RTCParam'),end=True)
    hdu.header.append(('DEVICE','DMs'),end=True)
    hdu.header.append(('FUNC','Control Matrix'),end=True)
    hdu.header.append(('SYMBOL','M'),end=True)
    hdu.header.append(('MODE','16x'),end=True)
    hdu.header.append(('SEC',True,' Secondary obscuration'),end=True)
    hdu.header.append(('NS',ns,' # of subapertures'),end=True)
    hdu.header.append(('NW',naw,' # of woofer actuators'),end=True)
    hdu.header.append(('NWM',nf,' # of woofer modes controlled'),end=True)
    hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'),end=True)
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'),end=True)
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'),end=True)
    hdu.header.append(('GENER',module_file_name,'Generator code'),end=True)
    hdu.header.append(('PENALTY',use_penalty,'applied a penalty fuction in H pseudo-inverse'),end=True)
    hdu.header.append(('GMETHOD',method,     'uses 1) H & Hw data or 2) H & woofer poke data'),end=True)
    hdu.header.append(('CONTEXT',context,    'H and Hw created with live system or simulator'),end=True)
    hdu.header.append(('WEIGHT',weighting,   'weighting of subapertures'),end=True)
    hdu.header.append(('COMMENT','------- H Matrix ---------'),end=True)
    hdu.header.append(('HDATE',hdate,  'H creation date'),end=True)
    hdu.header.append(('HTS',h_timeStamp, 'H calibration time stamp'),end=True)
    hdu.header.append(('HAMP',hamp,    'H calibration amplitude'),end=True)
    hdu.header.append(('HBIAS',hbias,  'H calibration bias'),end=True)
    hdu.header.append(('HDELAY',hdelay,'H calibration sample delay (sec)'),end=True)
    hdu.header.append(('COMMENT','------- Hw Matrix ---------'),end=True)    
    hdu.header.append(('HWDATE',hwdate,'Hw creation date'),end=True)
    hdu.header.append(('HWTS',hw_timeStamp,'Hw calibration time stamp'),end=True)
    hdu.header.append(('HWAMP',hwamp,  'Hw calibration amplitude'),end=True)
    hdu.header.append(('HWBIAS',hwbias,'Hw calibration bias'),end=True)
    hdu.header.append(('HWDELAY',hwdelay, 'Hw calibration sample delay (sec)'),end=True)

    protectFile(filename,verbose=True)
    hdu.writeto(filename)
    print('<param_gen.gen_CM_16x> wrote '+filename)
    
    if LGS_also:
        filenameLGS = gen_CM_16xLGS(baseName = filename,workDir=workDir)
        return (filename,filenameLGS)
    return filename

gen_set.append(gen_CM_16x)

def gen_CM_16xLGS(baseName = 'controlMatrix_16x.fits',ttFieldRotation = 24.3, workDir=None):
    """Generate the 16xLGS control matrix.
    This matrix is a modification of the 16x matrix, removing
    tip/tilt from the wavefront sensor signals and substituting
    with tip/tilt from the tip/tilt sensor, and sending WFS tip/tilt
    signals to the laser uplink control.
    
    The starting point matrix can be selected with the baseName argument.
    The ttFieldRotation is the fixed rotation angle, in degrees, between the tip/tilt sensor
    and the wavefront sensor. The field rotation is caused by the periscope in the
    tip/tilt light path. There is also an x-axis flip caused by the dichroic splitter.
    """
    newBaseName = os.path.splitext(baseName)[0]
    parsedBaseName = newBaseName.split('_')
    assert '16x' in parsedBaseName
    if any(['LGS' in x for x in parsedBaseName]):
        raise ValueError('base control matrix cannot be an LGS matrix: '+baseName)

    subdir = os.path.join(paramsDir,'reconMatrix_16x/')
    inDir,outDir = parseDir(workDir=workDir,subdir=subdir)
    filename = os.path.join(inDir,baseName)
    #hdu = fits.open(filename)
    cm,cm_hdr = readfits(filename,return_header=True)
    #cm = hdu[0].data
    
    na,naw,nf,ns = 1024,52,14,144
    At = readfits(os.path.join(subdir,'At.fits'))
    Aw = np.matrix(readfits(os.path.join(subdir,'Aw.fits')))
    n_tweeter_modes = At.shape[0]
    At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
    Aw = Aw[0:nf,:].transpose()
    H = np.matrix(readfits(os.path.join(inDir,'H.fits')))
    Hw = np.matrix(readfits(os.path.join(inDir,'Hw.fits')))
    Hw = Hw[:,0:nf]
    Ts = Hw[:,0:2]  # Tip/Tilt modes, as seen on the wavefront sensor
                    # to check this, add these modes to the reference centroids and
                    # verify that the PSF moves but Strehl is preserved

    Tr_s = np.matrix(np.identity(2*ns)) - Ts*(Ts.T*Ts).I*Ts.T
    O_sv = np.matrix(np.zeros((2*ns,nf)))
    O_vs = O_sv.T
    I_vv = np.matrix(np.identity(nf))
    
    Tr = np.vstack(
        [
            np.hstack([Tr_s, O_sv]),
            np.hstack([O_vs, I_vv])
        ]
    )
    
    cm_timeStamp = hget(cm_hdr,'TSTAMP')
    use_penalty = hget(cm_hdr,'PENALTY')
    method = hget(cm_hdr,'GMETHOD')
    context = hget(cm_hdr,'CONTEXT')
    weighting = hget(cm_hdr,'WEIGHT')
    hdate = hget(cm_hdr,'HDATE')
    h_timeStamp = hget(cm_hdr,'HTS')
    hamp = hget(cm_hdr,'HAMP')
    hbias = hget(cm_hdr,'HBIAS')
    hdelay = hget(cm_hdr,'HDELAY')
    hwdate = hget(cm_hdr,'HWDATE')
    hw_timeStamp = hget(cm_hdr,'HWTS')
    hwamp = hget(cm_hdr,'HWAMP')
    hwbias = hget(cm_hdr,'HWBIAS')
    hwdelay = hget(cm_hdr,'HWDELAY')
        
    if (use_penalty):
        W = weights('16x',weighting)
        Wsr = np.sqrt(W)
        S,Abar,H_d,R,U,s = penalty(At,H,W)
    else:
        W = np.matrix(np.identity(2*ns))
        Wsr = W
        Q = np.matrix(readfits(subdir+'Q.fits'))
        P = np.matrix(readfits(subdir+'P.fits'))
        H_d = H.T*(Q + H*P*H.T).I

    try:
        Hw_d = (Hw.T*W*Hw).I*Hw.T*Wsr
    except:
        print('<param_gen.gen_CM_16xLGS> failed Hw_d = (Hw.T*Hw).I*Hw.T calculation (matrix singular?)')
        print('<param_gen.gen_CM_16xLGS> proceding with pseudo-inverse approach')
        Hw_d = np.linalg.pinv(Wsr*Hw,rcond=.01)*Wsr

    Htt_d = H_d*Ts
    Hwt_d = Hw_d*Ts
    tilt_processing = np.vstack(
        [
            -At*Htt_d,
            -Aw*Hwt_d,
            -Hwt_d
        ]
    )
    
    th = ttFieldRotation*(np.pi/180.)
    #directionCosineMatrix = np.matrix([[-np.cos(th), np.sin(th)],
    #                                   [ np.sin(th), np.cos(th)]])
    directionCosineMatrix = np.matrix([[-np.cos(th), np.sin(th)],
                                       [ np.sin(th), -np.cos(th)]])
    tilt_processing = tilt_processing*directionCosineMatrix
    
    O_2xnf = np.matrix(np.zeros((2,nf)))
    uplink_processing = np.hstack(
        [ (Ts.T*Ts).I*Ts.T, O_2xnf ]
    )
    
    O_2x2 = np.matrix(np.zeros((2,2)))
    cm_LGS = np.vstack(
        [
            np.hstack([cm*Tr, tilt_processing]),
            np.hstack([uplink_processing, O_2x2])
        ]
    )
    
    if context == 'Simulator':
        assert 'sim' in parsedBaseName
    parsedBaseName[parsedBaseName.index('16x')]='16xLGS'
    newBaseName = '_'.join(parsedBaseName)
    newBaseName += '.fits'

    filename = os.path.join(outDir,newBaseName)
    
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')
    
    hdu = fits.PrimaryHDU(cm_LGS)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'),end=True)
    hdu.header.append(('ITYPE','RTCParam'),end=True)
    hdu.header.append(('DEVICE','DMs'),end=True)
    hdu.header.append(('FUNC','Control Matrix'),end=True)
    hdu.header.append(('SYMBOL','M'),end=True)
    hdu.header.append(('MODE','16xLGS'),end=True)
    hdu.header.append(('SEC',True,' Secondary obscuration'),end=True)
    hdu.header.append(('NS',ns,' # of subapertures'),end=True)
    hdu.header.append(('NW',naw,' # of woofer actuators'),end=True)
    hdu.header.append(('NWM',nf,' # of woofer modes controlled'),end=True)
    hdu.header.append(('NUTT',2,' # of uplink tip tilt modes controlled'),end=True)
    hdu.header.append(('NTT',2,' # of tip tilt modes sensed'),end=True)
    hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'),end=True)
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'),end=True)
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'),end=True)
    hdu.header.append(('GENER',module_file_name,'Generator code'),end=True)
    hdu.header.append(('PENALTY',use_penalty,'applied a penalty fuction in H pseudo-inverse'),end=True)
    hdu.header.append(('GMETHOD',method,     'uses 1) H & Hw data or 2) H & woofer poke data'),end=True)
    hdu.header.append(('CONTEXT',context,    'H and Hw created with live system or simulator'),end=True)
    hdu.header.append(('WEIGHT',weighting,   'weighting of subapertures'),end=True)
    hdu.header.append(('COMMENT','------- Base CM Matrix ------'),end=True)
    hdu.header.append(('CMTS',cm_timeStamp,'base control matrix time stamp'),end=True)
    hdu.header.append(('COMMENT','------- H Matrix ------'),end=True)
    hdu.header.append(('HDATE',hdate, 'date of creation of the H data'),end=True)
    hdu.header.append(('HTS',h_timeStamp, 'H calibration time stamp'),end=True)
    hdu.header.append(('HAMP',hamp,    'H calibration amplitude'),end=True)
    hdu.header.append(('HBIAS',hbias,  'H calibration bias'),end=True)
    hdu.header.append(('HDELAY',hdelay,'H calibration sample delay (sec)'),end=True)
    hdu.header.append(('COMMENT','------- Hw Matrix ------'),end=True)
    hdu.header.append(('HWDATE',hwdate,'Hw creation date'),end=True)
    hdu.header.append(('HWTS',hw_timeStamp,'Hw calibration time stamp'),end=True)
    hdu.header.append(('HWAMP',hwamp,  'Hw calibration amplitude'),end=True)
    hdu.header.append(('HWBIAS',hwbias,'Hw calibration bias'),end=True)
    hdu.header.append(('HWDELAY',hwdelay, 'Hw calibration sample delay (sec)'),end=True)

    protectFile(filename,verbose=True)
    hdu.writeto(filename)
    print('<param_gen.gen_CM_16xLGS> wrote '+filename)
    
    return filename

def gen_CM_8x(LGS_also=True,use_penalty=True,method = 1,weighting = None,workDir=None):
    """Generate the 8x control matrix.
    (see gen_CM_16x)
    """
    method = 1

    print('<param_gen> generating controlMatrix_8x')
    # 8x case:
    na,naw,nf,ns = 1024,52,14,40
    subdir = os.path.join(paramsDir,'reconMatrix_8x/')

    inDir,outDir = parseDir(workDir=workDir,subdir = subdir)

    At = readfits(subdir+'At.fits')
    Aw = np.matrix(readfits(subdir+'Aw.fits'))
    if (not use_penalty):
        Q = np.matrix(readfits(subdir+'Q.fits'))
        P = np.matrix(readfits(subdir+'P.fits'))
    H,H_hdr = readfits(os.path.join(inDir,'H.fits'),return_header=True)
    H = np.matrix(H)
    # method == 1
    Hw,Hw_hdr = readfits(os.path.join(inDir,'Hw.fits'),return_header=True)
    Hw = np.matrix(Hw)
    # method != 2

    # sensor weightings
    W = weights('8x',weighting)

    # determine the date and context of the input data
    context = hget(H_hdr,'CONTEXT')
    assert context in ['Simulator','Real']
    hdate = hget(H_hdr,'DATE')
    h_timeStamp = hget(H_hdr,'TSTAMP')
    hamp = hget(H_hdr,'AMP')
    hbias = hget(H_hdr,'BIAS')
    hdelay = hget(H_hdr,'DELAY')
    hwdate = hget(Hw_hdr,'DATE')
    hw_timeStamp = hget(Hw_hdr,'TSTAMP')
    hwamp = hget(Hw_hdr,'AMP')
    hwbias = hget(Hw_hdr,'BIAS')
    hwdelay = hget(Hw_hdr,'DELAY')    

    n_tweeter_modes = At.shape[0]
    At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
    Aw = Aw[0:nf,:].transpose()

    # method != 2
    
    if (use_penalty):
        S,Abar,H_d,R,U,s = penalty(At,H,W)
    else:
        H_d = H.T*(Q + H*P*H.T).I
        R = At*H_d

    # method == 1
    Hw = Hw[:,0:nf]
    Wsr = np.sqrt(W)
    try:
        Hw_d = (Hw.T*Hw).I*Hw.T
    except:
        print('<param_gen.gen_CM_8x> failed Hw_d = (Hw.T*Hw).I*Hw.T calculation (matrix singular?)')
        print('<param_gen.gen_CM_8x> proceding with pseudo-inverse approach')
        Hw_d = np.linalg.pinv(Wsr*Hw,rcond=.01)*Wsr
    cm = np.vstack(
        [np.hstack([ -R,         -At*H_d*Hw                     ]),
         np.hstack([ -Aw*Hw_d,    np.matrix(np.zeros((naw,nf))) ]),
         np.hstack([ -Hw_d,      -np.matrix(np.identity(nf))    ])
        ]
    )
    
    # method != 2

    # the bottom row will be multplied by (1-alpha) appropriate to each frame rate,
    # where alpha is the time constant of the woofer
    
    #remove woofer and tweeter piston
    pw = np.matrix(np.ones(naw))
    tw = tweeter.tweeter()
    pt = tw.ap.reshape((na))
    ptd = np.matrix(np.diag(pt))
    pt = np.matrix(pt)
    n = np.sum(pt)
    Pr_t = ptd - pt.transpose()*pt/float(n)
    Pr_w = np.matrix(np.identity(naw)) - pw.transpose()*pw/float(naw)
    I_tt = np.matrix(np.identity(na))
    I_ww = np.matrix(np.identity(naw))
    I_ff = np.matrix(np.identity(nf))
    O_tw = np.matrix(np.zeros((na,naw)))
    O_wt = O_tw.transpose()
    O_tf = np.matrix(np.zeros((na,nf)))
    O_ft = O_tf.transpose()
    O_wf = np.matrix(np.zeros((naw,nf)))
    O_fw = O_wf.transpose()
    
    Pr = np.vstack(
        [
            np.hstack([I_tt, O_tw, O_tf]), # I_tt <-> Pr_t: Identity or Piston Removed on tweeter
            np.hstack([O_wt, Pr_w, O_wf]),
            np.hstack([O_ft, O_fw, I_ff])
        ]
    )
    
    cm = Pr*cm

    
    # Format of a file name = "controlMatrix_"mode{"LGS"}{_weighting}{"_sim"}".fits"
    #                  mode = ["8x" | "16x" | "30x"]
    #                  weighting = ["wls" | "dim"]
    parsedBaseName = ['controlMatrix','8x']
    if (weighting not in ['none',None]):
        parsedBaseName.append(weighting)
    if context == 'Simulator':
        parsedBaseName.append('sim')
    filename = '_'.join(parsedBaseName)+'.fits'
    filename = os.path.join(outDir,filename)
    if weighting is None:
        weighting = 'none'
    
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')

    hdu = fits.PrimaryHDU(cm)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'),end=True)
    hdu.header.append(('ITYPE','RTCParam'),end=True)
    hdu.header.append(('DEVICE','DMs'),end=True)
    hdu.header.append(('FUNC','Control Matrix'),end=True)
    hdu.header.append(('SYMBOL','M'),end=True)
    hdu.header.append(('MODE','8x'),end=True)
    hdu.header.append(('SEC',True,' Secondary obscuration'),end=True)
    hdu.header.append(('NS',ns,' # of subapertures'),end=True)
    hdu.header.append(('NW',naw,' # of woofer actuators'),end=True)
    hdu.header.append(('NWM',nf,' # of woofer modes controlled'),end=True)
    hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'),end=True)
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'),end=True)
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'),end=True)
    hdu.header.append(('GENER',module_file_name,'Generator code'),end=True)
    hdu.header.append(('PENALTY',use_penalty,'applied a penalty fuction in H pseudo-inverse'),end=True)
    hdu.header.append(('GMETHOD',method,     'uses 1) H & Hw data or 2) H & woofer poke data'),end=True)
    hdu.header.append(('CONTEXT',context,    'H and Hw created with live system or simulator'),end=True)
    hdu.header.append(('WEIGHT',weighting,   'weighting of subapertures'),end=True)
    hdu.header.append(('COMMENT','------- H Matrix ---------'),end=True)
    hdu.header.append(('HDATE',hdate,  'H creation date'),end=True)
    hdu.header.append(('HTS',h_timeStamp, 'H calibration time stamp'),end=True)
    hdu.header.append(('HAMP',hamp,    'H calibration amplitude'),end=True)
    hdu.header.append(('HBIAS',hbias,  'H calibration bias'),end=True)
    hdu.header.append(('HDELAY',hdelay,'H calibration sample delay (sec)'),end=True)
    hdu.header.append(('COMMENT','------- Hw Matrix ---------'),end=True)    
    hdu.header.append(('HWDATE',hwdate,'Hw creation date'),end=True)
    hdu.header.append(('HWTS',hw_timeStamp,'Hw calibration time stamp'),end=True)
    hdu.header.append(('HWAMP',hwamp,  'Hw calibration amplitude'),end=True)
    hdu.header.append(('HWBIAS',hwbias,'Hw calibration bias'),end=True)
    hdu.header.append(('HWDELAY',hwdelay, 'Hw calibration sample delay (sec)'),end=True)

    protectFile(filename,verbose=True)
    hdu.writeto(filename)
    print('<param_gen.gen_CM_8x> wrote '+filename)

    if LGS_also:
        filenameLGS = gen_CM_8xLGS(baseName = filename,workDir=workDir)
        return (filename,filenameLGS)
    return filename

gen_set.append(gen_CM_8x)

def gen_CM_8xLGS(baseName = 'controlMatrix_8x.fits',ttFieldRotation = 24.3,workDir=None):
    """Generate the 8xLGS control matrix.
    This matrix is a modification of the 16x matrix, removing
    tip/tilt from the wavefront sensor signals and substituting
    with tip/tilt from the tip/tilt sensor, and sending WFS tip/tilt
    signals to the laser uplink control.
    
    The starting point matrix can be selected with the baseName argument.
    The ttFieldRotation is the fixed rotation angle, in degrees, between the tip/tilt sensor
    and the wavefront sensor. The field rotation is caused by the periscope in the
    tip/tilt light path. There is also an x-axis flip caused by the dichroic splitter.
    """
    newBaseName = os.path.splitext(baseName)[0]
    parsedBaseName = newBaseName.split('_')
    assert '8x' in parsedBaseName
    if any(['LGS' in x for x in parsedBaseName]):
        raise ValueError('base control matrix cannot be an LGS matrix: '+baseName)

    subdir = os.path.join(paramsDir,'reconMatrix_8x/')
    inDir,outDir = parseDir(workDir=workDir,subdir=subdir)
    filename = os.path.join(inDir,baseName)
    #hdu = fits.open(filename)
    cm,cm_hdr = readfits(filename,return_header=True)
    #cm = hdu[0].data
    
    na,naw,nf,ns = 1024,52,14,40
    At = readfits(subdir+'At.fits')
    Aw = np.matrix(readfits(subdir+'Aw.fits'))
    n_tweeter_modes = At.shape[0]
    At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
    Aw = Aw[0:nf,:].transpose()
    H = np.matrix(readfits(os.path.join(inDir,'H.fits')))
    Hw = np.matrix(readfits(os.path.join(inDir,'Hw.fits')))
    Hw = Hw[:,0:nf]
    Ts = Hw[:,0:2]  # Tip/Tilt modes, as seen on the wavefront sensor
                    # to check this, add these modes to the reference centroids and
                    # verify that the PSF moves but Strehl is preserved

    Tr_s = np.matrix(np.identity(2*ns)) - Ts*(Ts.T*Ts).I*Ts.T
    O_sv = np.matrix(np.zeros((2*ns,nf)))
    O_vs = O_sv.T
    I_vv = np.matrix(np.identity(nf))
    
    Tr = np.vstack(
        [
            np.hstack([Tr_s, O_sv]),
            np.hstack([O_vs, I_vv])
        ]
    )
    
    cm_timeStamp = hget(cm_hdr,'TSTAMP')
    use_penalty = hget(cm_hdr,'PENALTY')
    method = hget(cm_hdr,'GMETHOD')
    context = hget(cm_hdr,'CONTEXT')
    weighting = hget(cm_hdr,'WEIGHT')
    hdate = hget(cm_hdr,'HDATE')
    h_timeStamp = hget(cm_hdr,'HTS')
    hamp = hget(cm_hdr,'HAMP')
    hbias = hget(cm_hdr,'HBIAS')
    hdelay = hget(cm_hdr,'HDELAY')
    hwdate = hget(cm_hdr,'HWDATE')
    hw_timeStamp = hget(cm_hdr,'HWTS')
    hwamp = hget(cm_hdr,'HWAMP')
    hwbias = hget(cm_hdr,'HWBIAS')
    hwdelay = hget(cm_hdr,'HWDELAY')
    
    if (use_penalty):
        W = weights('8x',weighting)
        Wsr = np.sqrt(W)
        S,Abar,H_d,R,U,s = penalty(At,H)
    else:
        W = np.matrix(np.identity(2*ns))
        Wsr = W
        Q = np.matrix(readfits(subdir+'Q.fits'))
        P = np.matrix(readfits(subdir+'P.fits'))
        H_d = H.T*(Q + H*P*H.T).I

    try:
        Hw_d = (Hw.T*W*Hw).I*Hw.T*Wsr
    except:
        print('<param_gen.gen_CM_8xLGS> failed Hw_d = (Hw.T*Hw).I*Hw.T calculation (matrix singular?)')
        print('<param_gen.gen_CM_8xLGS> proceding with pseudo-inverse approach')
        Hw_d = np.linalg.pinv(Wsr*Hw,rcond=.01)*Wsr

    Htt_d = H_d*Ts
    Hwt_d = Hw_d*Ts
    tilt_processing = np.vstack(
        [
            -At*Htt_d,
            -Aw*Hwt_d,
            -Hwt_d
        ]
    )
    
    th = ttFieldRotation*(np.pi/180.)
    #directionCosineMatrix = np.matrix([[-np.cos(th), np.sin(th)],
    #                                   [ np.sin(th), np.cos(th)]])
    directionCosineMatrix = np.matrix([[-np.cos(th), np.sin(th)],
                                       [ np.sin(th), -np.cos(th)]])
    tilt_processing = tilt_processing*directionCosineMatrix
    
    O_2xnf = np.matrix(np.zeros((2,nf)))
    uplink_processing = np.hstack(
        [ (Ts.T*Ts).I*Ts.T, O_2xnf ]
    )
    
    O_2x2 = np.matrix(np.zeros((2,2)))
    cm_LGS = np.vstack(
        [
            np.hstack([cm*Tr, tilt_processing]),
            np.hstack([uplink_processing, O_2x2])
        ]
    )
    
    if (context == 'Simulator'):
        assert 'sim' in parsedBaseName
    parsedBaseName[parsedBaseName.index('8x')]='8xLGS'
    newBaseName = '_'.join(parsedBaseName)
    newBaseName += '.fits'
    
    filename = os.path.join(outDir,newBaseName)
    
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')
    
    hdu = fits.PrimaryHDU(cm_LGS)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'),end=True)
    hdu.header.append(('ITYPE','RTCParam'),end=True)
    hdu.header.append(('DEVICE','DMs'),end=True)
    hdu.header.append(('FUNC','Control Matrix'),end=True)
    hdu.header.append(('SYMBOL','M'),end=True)
    hdu.header.append(('MODE','8xLGS'),end=True)
    hdu.header.append(('SEC',True,' Secondary obscuration'),end=True)
    hdu.header.append(('NS',ns,' # of subapertures'),end=True)
    hdu.header.append(('NW',naw,' # of woofer actuators'),end=True)
    hdu.header.append(('NWM',nf,' # of woofer modes controlled'),end=True)
    hdu.header.append(('NUTT',2,' # of uplink tip tilt modes controlled'),end=True)
    hdu.header.append(('NTT',2,' # of tip tilt modes sensed'),end=True)
    hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'),end=True)
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'),end=True)
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'),end=True)
    hdu.header.append(('GENER',module_file_name,'Generator code'),end=True)
    hdu.header.append(('PENALTY',use_penalty,'applied a penalty fuction in H pseudo-inverse'),end=True)
    hdu.header.append(('GMETHOD',method,     'uses 1) H & Hw data or 2) H & woofer poke data'),end=True)
    hdu.header.append(('CONTEXT',context,    'H and Hw created with live system or simulator'),end=True)
    hdu.header.append(('WEIGHT',weighting,   'weighting of subapertures'),end=True)
    hdu.header.append(('COMMENT','------- Base CM Matrix ------'),end=True)
    hdu.header.append(('CMTS',cm_timeStamp,'base control matrix time stamp'),end=True)
    hdu.header.append(('COMMENT','------- H Matrix ------'),end=True)
    hdu.header.append(('HDATE',hdate, 'date of creation of the H data'),end=True)
    hdu.header.append(('HTS',h_timeStamp, 'H calibration time stamp'),end=True)
    hdu.header.append(('HAMP',hamp,    'H calibration amplitude'),end=True)
    hdu.header.append(('HBIAS',hbias,  'H calibration bias'),end=True)
    hdu.header.append(('HDELAY',hdelay,'H calibration sample delay (sec)'),end=True)
    hdu.header.append(('COMMENT','------- Hw Matrix ------'),end=True)
    hdu.header.append(('HWDATE',hwdate,'Hw creation date'),end=True)
    hdu.header.append(('HWTS',hw_timeStamp,'Hw calibration time stamp'),end=True)
    hdu.header.append(('HWAMP',hwamp,  'Hw calibration amplitude'),end=True)
    hdu.header.append(('HWBIAS',hwbias,'Hw calibration bias'),end=True)
    hdu.header.append(('HWDELAY',hwdelay, 'Hw calibration sample delay (sec)'),end=True)

    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_CM_8xLGS> wrote '+filename)
    
    return filename

def gen_ref_cents(mode):
    """Generate a default reference centroid array
    """
    mode_str = valid_modes[mode]
    print('<param_gen> generating ref_cents for mode '+mode_str)
    ns_select = [40,144,0]
    ns = ns_select[mode]
    ref_cent = np.zeros((2*ns))
    filename = os.path.join(paramsDir,'ref_cent_'+mode_str+'.fits')
    hdu = fits.PrimaryHDU(ref_cent)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','reference centroids'))
    hdu.header.append(('SYMBOL','s_ref'))
    hdu.header.append(('MODE',mode_str))
    hdu.header.append(('SEC',True,' Secondary obscuration'))
    hdu.header.append(('NS',ns,' # of subapertures'))
    hdu.header.append(('ASTEP','1.3',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))    

    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_ref_cents> wrote '+filename)    

def gen_tt_params():
    """This generates the tip/tilt camera specific parameters:
    * tt_dark.fits
    * tt_flat.fits
    * tt_centWts
    """
    tt_dark = np.zeros((80,80))
    filename = os.path.join(paramsDir,'tt_dark.fits')
    hdu = fits.PrimaryHDU(tt_dark)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','TTSCam'))
    hdu.header.append(('FUNC','camera background'))
    hdu.header.append(('SYMBOL','tt_background'))
    hdu.header.append(('ASTEP','1.1',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_tt_param> wrote '+filename)
    
    tt_flat = np.ones((80,80))
    filename = os.path.join(paramsDir,'tt_flat.fits')
    hdu = fits.PrimaryHDU(tt_dark)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','TTSCam'))
    hdu.header.append(('FUNC','camera flat field'))
    hdu.header.append(('SYMBOL','tt_flat'))
    hdu.header.append(('ASTEP','1.1',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_tt_param> wrote '+filename)
    
    tt_centWts = np.zeros((3,80,80))
    # roi must have an even number of pixels and be completely contained in the 80x80 grid
    roi_center = [45,45]
    roi_size = [10,10]
    roi = np.zeros(roi_size)
    x = 0
    y = 1
    inten = 2
    l, r = roi_center[x]-roi_size[x]/2, roi_center[x]+roi_size[x]/2
    b, t = roi_center[y]-roi_size[y]/2, roi_center[y]+roi_size[y]/2
    mx, my = roi_center
    sx, sy = roi_size
    algo = 'QUAD'
    if (algo == 'QUAD'):
        # quad-cell
        tt_centWts[x,b:t,l:mx] = -1
        tt_centWts[x,b:t,mx:r] = +1
        tt_centWts[y,b:my,l:r] = -1
        tt_centWts[y,my:t,l:r] = +1
        tt_centWts[inten,b:t,l:r] = 1
    elif (algo == 'COG'):
        # center of mass
        x1 = np.arange(-sx/2+0.5,sx/2,1)
        y1 = np.arange(-sy/2+0.5,sy/2,1)
        x2, y2 = np.meshgrid(x1,y1)
        tt_centWts[x,b:t,l:r] = x2
        tt_centWts[y,b:t,l:r] = y2
        tt_centWts[inten,b:t,l:r] = 1
    filename = os.path.join(paramsDir,'tt_cent_weights.fits')
    hdu = fits.PrimaryHDU(tt_centWts)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','TTSCam'))
    hdu.header.append(('FUNC','tt centroider weights'))
    hdu.header.append(('SYMBOL','tt_centWts'))
    hdu.header.append(('ASTEP','1.2',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('ALGO',algo,' quad-cell or center-of-mass'))
    hdu.header.append(('ROICTR',str(roi_center),' ROI center'))
    hdu.header.append(('ROISIZE',str(roi_size),' ROI size'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_tt_param> wrote '+filename)

gen_set.append(gen_tt_params)

mode_num = {'8x':0,'16x':1,'30x':2,'Pyr':3} # translate mode string to a mode number

def illums(mode,secondary=True):
    """Define the illuminated subapertures for each mode
    
    mode
        can be a mode string '8x','16x','30x','Pyr' or its number 0,1,2,3
    """
    if isinstance(mode,str):
        mode = mode_num[mode]
    if mode == 4:
        return pyramid_illums(mode,secondary=secondary)
    camPix = 160
    subap = 5
    n_across_select = [7, 14, 30]
    n_sec_select = [2,4,8]
    delta_select = [0.5,0.,0.]
    n = camPix/subap
    d = n_across_select[mode]
    dsec = n_sec_select[mode]
    delta = delta_select[mode]
    sap = circle((n,n),c=(n/2-0.5,n/2-0.5), r=(d+delta)/2)
    if (secondary):
        sap -= circle((n,n),c=(n/2-0.5,n/2-0.5),r=(dsec+1)/2)
    illum = np.where(sap == 1)
    illum = np.array(illum).transpose()
    subapLocs = illum*subap
    return subapLocs
    
def map25(mode,secondary=True):
    """Produce a list of 1-dimensional indices into the camera image that
    identify pixels within subaps. The result can be viewed as a ns x 5 x 5 array,
    where ns is the number of 5 x 5 pixel subaps.
    
    mode
        can be a mode string '8x','16x','30x', or its number 0,1,2
    """
    if isinstance(mode,str):
        mode = mode_num[mode]
    subapLocs = illums(mode,secondary=secondary)
    camPix = 160
    subap = 5
    ns = len(subapLocs)
    m25 = np.zeros((ns*subap*subap)).astype(int)
    for k in range(ns):
        p = k*subap*subap
        q = subapLocs[k][1] + subapLocs[k][0]*camPix
        for y in range(subap):
            for x in range(subap):
                m25[p + x + y*subap] = q + x + y*camPix
    return m25

def gen_umap_mode(mode,secondary=True):
    '''
    mode
        can be a mode string '8x','16x','30x', or its number 0,1,2
    '''
    if isinstance(mode,str):
        mode = mode_num[mode]
    m25 = map25(mode,secondary=secondary)
    modeNames = list(mode_num.keys()) # ['8x','16x','30x']
    if (secondary):
        filename = 'u_map_subaps_'+modeNames[mode]+'.fits'
    else:
        filename = 'u_map_subaps_'+modeNames[mode]+'_nosec.fits'
    filename = os.path.join(paramsDir,filename)
    hdu = fits.PrimaryHDU(m25)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','Subap Map',' wfs cam to subap blocks of 25 for centroider'))
    hdu.header.append(('SCRAMBLE',False,' map into unscrambled CCD array'))
    hdu.header.append(('SYMBOL','u_map_subaps'))
    hdu.header.append(('NS',int(len(m25)/25)),'number of subaps')
    hdu.header.append(('MODE',modeNames[mode]))
    hdu.header.append(('SEC',secondary,' Secondary obscuration'))
    hdu.header.append(('ASTEP','1.1',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_umap_mode> wrote '+filename)

def gen_imap_mode(mode,secondary=True):
    '''
    mode
        can be a mode string '8x','16x','30x', or its number 0,1,2
    '''
    if isinstance(mode,str):
        mode = mode_num[mode]
    camPix = 160
    imap = readfits(os.path.join(paramsDir,'i_map.fits')).reshape((camPix*camPix))
    m25 = map25(mode,secondary=secondary)
    sm25 = imap[m25]
    modeNames = list(mode_num.keys()) #  ['8x','16x','30x']
    if (secondary):
        filename = 'i_map_subaps_'+modeNames[mode]+'.fits'
    else:
        filename = 'i_map_subaps_'+modeNames[mode]+'_nosec.fits'
    filename = os.path.join(paramsDir,filename)
    hdu = fits.PrimaryHDU(sm25)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','RTCParam'))
    hdu.header.append(('DEVICE','WFSCam'))
    hdu.header.append(('FUNC','Subap Map',' wfs cam to subap blocks of 25 for centroider'))
    hdu.header.append(('SCRAMBLE',True,' map into scrambled CCD array'))
    hdu.header.append(('SYMBOL','i_map_subaps'))
    hdu.header.append(('NS',int(len(m25)/25)),'number of subaps')
    hdu.header.append(('MODE',modeNames[mode]))
    hdu.header.append(('SEC',secondary,' Secondary obscuration'))
    hdu.header.append(('ASTEP','1.1',       ' Algorithm step in RTC Software Definitions doc'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    protectFile(filename)
    hdu.writeto(filename)
    print('<param_gen.gen_imap_mode> wrote '+filename)

# commented out on 8/12/2020
# from object import Object
# 
# class Generator(Object):
#     """Wrapper object for the gen_CM_xxx codes
#     """
#     pass

