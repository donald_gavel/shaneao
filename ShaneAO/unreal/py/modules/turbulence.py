"""
 Turbulence.py - calculates the turbulence strength of layers in the atmosphere
 according to the Hufnagel-Valley model
 Spatial correlation of phase-expansion coefficients for propagation through atmospheric turbulence
    George C Valley, Stephen M Wandzura	 (1979)
    Journal of the Optical Society of America	 69 (5)	 p. 712-717 (equation 28)
    
    Tyson, Principles of Adaptive Optics, equations 2.18 - 2.20
"""
import numpy as np

def Cn2_model(z,model='HV',r0=None,theta0=None):
    """
    Return one of the standard atmospheric Cn2 models
        z is in kilometers and mustCn be a 1-d array
        model:
            HV57 - Hufnagel Valley (default)
            HVB - Hufnagel Valley with Ulrich's boundary layer
            HVNB - My own invention, HVB with A=0
            SLC - Submarine laser communication - Night
        r0: Fried's parameter, in meters. Defaults to 5 cm
        theta0: Isoplanatic angle, in radians. Defaults to 7 microradian
            r0 and theta0 are used only in the HVB model
    """
    cn2 = 0.
    lambda0 = 0.5e-6
    microns = 1.e-6
    ur = 1.e-6
    cm = 1.e-2
    km = 1.e3
    if (r0 == None):
        r0 = 0.05
    if (theta0 == None):
        theta0 = 7.e-6
    if (model == 'HV57'):
        cn2 = Cn2_model(z,model='HVB',r0=0.05,theta0=7.e-6)
#        cn2 = 2.7*(2.2*(10**(-2.3)*z)**10.*np.exp(-z) + 10**(-16)*np.exp(-z/1.5)) Valley-Wandzura (28)
    if (model == 'HVB'):
        W = 27*(75*(theta0/ur)**(-5/3.)*(lambda0/microns)**2-0.14)**(1./2.)
        A = 1.29e-12*(r0/cm)**(-5./3.)*(lambda0/microns)**2 - 1.61e-13*(theta0/ur)**(-5./3.)*(lambda0/microns)**2-3.89e-15
        cn2 = 5.94e-23*z**10*np.exp(-z)*(W/27.) + 2.7e-16*np.exp(-2.*z/3.)+A*np.exp(-10*z) # Tyson (4.4)
    if (model == 'HVNB'):
        W = 27*(75*(theta0/ur)**(-5/3.)*(lambda0/microns)**2-0.14)**(1./2.)
        A = 0.
        cn2 = 5.94e-23*z**10*np.exp(-z)*(W/27.) + 2.7e-16*np.exp(-2.*z/3.)+A*np.exp(-10*z)
    if (model == 'SLC'):
        x = z*km
        cn2 = np.piecewise(x, [(x<=18.5),(x>18.5)*(x<=110), (x>110)*(x<=1500),(x>1500)*(x<=7200),(x>7200)*(x<20000),x>=20000],\
                    [8.4e-15, lambda x: 2.87e-12*x**(-2),  2.5e-16,lambda x: 8.87e-7*x**(-3), lambda x: 2.e-16*x**(-0.5),0.  ])
    return cn2

def Wind_model(z):
    """
    This is the Bufton wind profile
    """
    v = 5 + 30*np.exp(-((z-9.4)/4.8)**2)
    return v

dh = 0.1
lambda0 = 0.5e-6

h = np.arange(0.001,30.,dh)
km = 1.e3
cn2 = Cn2_model(h,model='HVNB',r0=0.10,theta0=10.e-6)

# equation (3.1-10) in KAON 208 AO for Keck Observatory
r0 = (0.423*(2*np.pi/lambda0)**2 *sum(cn2*dh*km))**(-3./5.)

# equation (3.1-34)
theta0 = ( 2.91*(2*np.pi/lambda0)**2 *sum(cn2*(h*km)**(5./3.)*dh*km))**(-3./5.)

le =[]
cn2_HV57 = Cn2_model(h,model='HV57')
r0_HV57 = (0.423*(2*np.pi/lambda0)**2 *sum(cn2_HV57*dh*km))**(-3./5.)
theta0_HV57 = ( 2.91*(2*np.pi/lambda0)**2 *sum(cn2_HV57*(h*km)**(5./3.)*dh*km))**(-3./5.)
le.append('HV57 '+ ('%.1f' % (r0_HV57/cm)) + ',' + ('%.1f' % (theta0_HV57/ur)) )

cn2_HVB = Cn2_model(h,model='HVB',r0=0.10,theta0=10.e-6)
r0_HVB = (0.423*(2*np.pi/lambda0)**2 *sum(cn2_HVB*dh*km))**(-3./5.)
theta0_HVB = ( 2.91*(2*np.pi/lambda0)**2 *sum(cn2_HVB*(h*km)**(5./3.)*dh*km))**(-3./5.)
le.append('HVB '+ ('%.1f' % (r0_HVB/cm)) + ',' + ('%.1f' % (theta0_HVB/ur)) )

cn2_HVNB = Cn2_model(h,model='HVNB',r0=0.10,theta0=10.e-6)
r0_HVNB = (0.423*(2*np.pi/lambda0)**2 *sum(cn2_HVNB*dh*km))**(-3./5.)
theta0_HVNB = ( 2.91*(2*np.pi/lambda0)**2 *sum(cn2_HVNB*(h*km)**(5./3.)*dh*km))**(-3./5.)
le.append('HVNB '+ ('%.1f' % (r0_HVNB/cm)) + ',' + ('%.1f' % (theta0_HVNB/ur)) )

cn2_SLC = Cn2_model(h,model='SLC',r0=0.10,theta0=10.e-6)
r0_SLC = (0.423*(2*np.pi/lambda0)**2 *sum(cn2_SLC*dh*km))**(-3./5.)
theta0_SLC = ( 2.91*(2*np.pi/lambda0)**2 *sum(cn2_SLC*(h*km)**(5./3.)*dh*km))**(-3./5.)
le.append('SLC '+ ('%.1f' % (r0_SLC/cm)) + ',' + ('%.1f' % (theta0_SLC/ur)) )

loglog(h*km,(cn2_HV57),':',h*km,(cn2_HVB),'-.',h*km,(cn2_HVNB),'b-',h*km,(cn2_SLC),'-')
legend(le,title='Model,r0,theta0')
title('Seeing Models')
xlabel('altitude, m')
ylabel('Cn2')
grid()
