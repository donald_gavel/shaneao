"""Fibertel
Move the white light fiber as if it were
starlight entering the telescope
"""
import os
import subprocess
import numpy as np
center = (-0.33,7.45) # mm (Cal X, Cal Y) Fiber stage
scale = 0.25 # mm per arcsec

machine = os.uname()[1]
if (machine == 'shade.ucolick.org'):
    modifyCommand = 'modify'
else:
    modifyCommand = 'ssh gavel@shade modify'

def move(north=0,east=0,live=False):
    """move the fiber north, east in arcseconds
    """
    X = -scale*east + center[0] # Cal X
    Y = -scale*north + center[1] # Cal Y
    print 'move the fiber stages to:'
    print ' ---------------- '
    print ' Cal X: '+('%.2f'%X)
    print ' ---------------- '
    print ' Cal Y: '+('%.2f'%Y)
    print ' ---------------- '
    s = modifyCommand+' -s saomot '
    s += 'CALXVAL='+('%.2f'%X)
    s += ' CALYVAL='+('%.2f'%Y)
    if (live):
        subprocess.call(s,shell=True)
    return s
