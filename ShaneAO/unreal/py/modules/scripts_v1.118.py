"""Script file for ShaneAO
execute this file at startup with::

    execfile(scripts)

"""
print '<scripts.py> config simulator = '+str(shaneao.config['simulator'])
if shaneao.config['simulator']==False:
    import ktl

import os
import sys
import readfits as fits
import pyfits
import datetime
import threading
import __main__

if __name__ != '__main__':
    raise ImportError, 'scripts.py cannot be imported as a module'

home = os.environ['HOME']
dataDir = os.path.join(home,'data')
paramsDir = os.path.join(home,'parameterFiles')
if shaneao.config['simulator']:
    pyDir = os.path.join(home,'cvs/lroot/ShaneAO/unreal/py/modules/')
else:
    pyDir = os.path.join(home,shaneao.pydir)
    __main__.load(os.path.join(pyDir,"telemetry.py"))
    __main__.load(os.path.join(pyDir,"telemetry_get_many.py"))
    __main__.load(os.path.join(pyDir,"tooreal.py"))

helper = {}

if shaneao.config['simulator']:

    import numpy as np
    import img
    import dimg

    helper['initsim'] = 'initialize the simulator'
    
    def echo(line):
        exec(line)
        print '<initsim> '+line; sys.stdout.flush()
        time.sleep(1.0)
    
    def initsim_original(theMode='16xLGS',theSubMode='ngs'):
        """initialize the simulator for operation. This is equivalent
        to the calibration operations like dark and refcents
        that must be performed on the real system"""
        global b

        mode(theMode)
        if (theSubMode == 'ngs'):
            ngs()
        elif (theSubMode == 'lgs'):
            lgs()
            ttcamstart()
        else:
            print '<initsim> invalid submode: '+theSubMode
            return
        camstart()
        light('off')
        dark()
        light('on')
        mems('on')
        wrate(3)
        woof('on')
        bias0()
        bias()
        aber('ref')
        refcent()
        open()

    def initsim(theMode='16xLGS',theSubMode='ngs'):
        """initialize the simulator for operation. This is equivalent
        to the calibration operations like dark and refcents
        that must be performed on the real system"""
        global b

        echo('mode("'+theMode+'")')
        if (theSubMode == 'ngs'):
            echo('ngs()')
        elif (theSubMode == 'lgs'):
            echo('lgs()')
            echo('ttcamstart()')
        else:
            print '<initsim> invalid submode: '+theSubMode
            return
        echo('camstart()')
        echo('light("off")')
        echo('dark()')
        echo('light("on")')
        echo('mems("on")')
        echo('wrate(3)')
        echo('woof("on")')
        echo('bias0()')
        echo('bias()')
        echo('aber("ref")')
        echo('refcent()')
        echo('open()')

    helper['strehl'] = '(simulator only) calculate the Strehl'
    def strehl():
        """calculate the rms-error implied Strehl in the simulator"""
        global phe,ap,psf

        D_tele = sauce.D_tele
        D_sec = sauce.D_sec
        du_fine = sauce.du_fine
        r0 = 0.12
        d = D_tele/14.
        pha = sauce.aber_set[sauce.t]
        ap = img.circle((512,512),r=D_tele/du_fine/2.-2)
        ap = ap - img.circle((512,512),r=D_sec/du_fine/2.)
        wf = sauce.wf_tweeter + sauce.wf_woofer # radians at 589 nm wavelength
        e = pha - wf
        phe = img.depiston(e,ap)*(589./(2*np.pi)) # nm
        rmse = img.rms(e,ap) # radians
        s = np.exp( -(rmse*(.589/1.))**2 )
        cwf = shaneao.sauce.zeropad(ap*np.exp(1j*e*(.589/1.6)),(1024,1024))
        psf = np.fft.fftshift(np.abs(np.fft.fft2(cwf))**2)
        wfa = wf*ap
        pv = wfa.max()-wfa.min()
        tt = pv*(.589/(2*np.pi))/D_tele/4.848
        s0 = np.exp( -0.3*(d/r0)**(5./3.)*(0.589/1.0)**2)
        print ' '
        print 'tilt = '+str(tt)+' arcsec'
        print 'rms error = '+str(rmse)+ ' radians, ('+str(rmse*(589./(2*np.pi)))+' nm)'
        print 'Strehl = '+str(s)+ ' at 1 micron'
        print 'calculated fit Strehl = '+str(s0)+' at 1 micron'

    helper['show'] = '(simulator only) show the residual wavefront'
    def show(limit=300):
        """graphically display the wavefront residual in the simulator"""
        dimg.show(np.clip(phe*ap,-limit,limit),fig='phe',origin='lower',geometry='654x536+80+707')
        dimg.show(psf[512-20:512+20,512-20:512+20]**.25,fig='PSF (1.6u)',geometry='654x536+80+107')

def defaults():
    """Set the default gains and bleeds, as determined
    from experiments with the simulator
    """
    if (mode() == '16x'):
        bleed(.9,'tweeter')
        bleed(.995,'woofer')
        bleed_mask(.85,.9)
        woof('on')
        if (not shaneao.config['simulator']): wrate(200)
        mems('on')
        flat0()
        alpha(0.2)
        gain(.7) # this closes the loop, so must follow with zero() to open
        zero()
        shaneao.self.saved_uGain = 0.5
        #refcent('data/refcents.fits')
        #refcent('data/refcents_16x_2014-04-15.fits')
        #refcent('data/hs-20140506-cog_16x.fits')
        refcent('data/hs-2014-08-13-BINQUAD_16xLGS.fits')
        cmFile = 'controlMatrix_16x.fits'
        if (shaneao.config['simulator']):
            cmFile = 'controlMatrix_16x_sim.fits'
        set_CM(cmFile)
    if (mode() == '8x'):
        bleed(.95,'tweeter')
        bleed(.99,'woofer')
        bleed_mask(.85,.9)
        woof('on')
        if (not shaneao.config['simulator']): wrate(200)
        mems('on')
        flat0()
        alpha(0.2)
        gain(.35)
        zero()
        shaneao.self.saved_uGain = 0.5
        #refcent(0)
        #refcent('data/refcents_8x_2014-04-15.fits')
        #refcent('data/hs-20140506_8x.fits')
        refcent('data/hs-2014-08-13-BINQUAD_8xLGS.fits')
        cmFile = 'controlMatrix_8x.fits'
        if (shaneao.config['simulator']):
            cmFile = 'controlMatrix_8x_sim.fits'
        set_CM(cmFile)
    if (mode() == '16xLGS'):
        bleed(.9,'tweeter')
        bleed(.995,'woofer')
        woof('on')
        if (not shaneao.config['simulator']): wrate(200)
        mems('on')
        flat0()
        bleed_mask(.85,.9)
        alpha(0.2)
        gain(.7) # this closes the loop, so must follow with zero() to open
        zero()
        if (shaneao.state['subState'] == 'lgs'):
            tt_gain(.6)
        shaneao.self.saved_uGain = 0.5
        #refcent('data/refcents.fits')
        #refcent('data/refcents_16x_2014-04-15.fits')
        #refcent('data/hs-20140506_16x.fits')
        #refcent('data/hs-20140506_16x_onSky.fits')
        refcent('data/hs-2014-08-13-BINQUAD_16xLGS.fits')
        cent('bin')
        cmFile = 'controlMatrix_16xLGS.fits'
        if (shaneao.config['simulator']):
            cmFile = 'controlMatrix_16xLGS_sim.fits'
        set_CM(cmFile)
        lgs()
        uplink_bleed(.9)
        shaneao.self.set_tt_centWts(roi_center=[49,37], roi_size=[10,10],algo='COG')
        #shaneao.self.set_tt_centWts(roi_center=[42,41], roi_size=[6,6],algo='COG')
        centReg([500.,1000.])
        tt_roi(boresight)
    if (mode() == '8xLGS'):
        bleed(.95,'tweeter')
        bleed(.99,'woofer')
        bleed_mask(.85,.9)
        woof('on')
        if (not shaneao.config['simulator']): wrate(200)
        mems('on')
        flat0()
        alpha(0.2)
        gain(.35)
        zero()
        if (shaneao.state['subState'] == 'lgs'):
            tt_gain(.6)
        shaneao.self.saved_uGain = 0.5
        #refcent(0)
        #refcent('data/refcents_8x_2014-04-15.fits')
        refcent('data/hs-2014-08-13-BINQUAD_8xLGS.fits')
        cent('bin')
        cmFile = 'controlMatrix_8xLGS.fits'
        if (shaneao.config['simulator']):
            cmFile = 'controlMatrix_8xLGS_sim.fits'
        set_CM(cmFile)
        lgs()
        uplink_bleed(.9)
        shaneao.self.set_tt_centWts(roi_center=[49,37], roi_size=[10,10],algo='COG')
        tt_roi(boresight)

def _ap():
    """ aperture
    """
    D_tele = 3.0
    D_sec = 0.8
    n_fine = 32
    du_fine = 0.1
    L = n_fine*du_fine/2.
    x1 = np.linspace(-L,L,n_fine)
    x,y = np.meshgrid(x1,x1)
    r = np.sqrt(x**2 + y**2)
    _ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)
    return _ap

helper['bleed_mask'] = 'Specify bleed values for actuators outside the illuminated aperture'
def bleed_mask(fast_bleed=.07,slow_bleed=.9):
    """Specify different bleed values for actuators outside the illuminated aperture"""
    p = _ap()
    u = bleed('tweeter')
    u[np.where(p == 0)] = fast_bleed
    u = u.reshape((1024))
    v = bleed('woofer')
    v[21]=slow_bleed
    v[22]=slow_bleed
    v[29]=slow_bleed
    v[30]=slow_bleed
    b = np.hstack([u,v])
    bleed(b)

sky = dark

#=====================================
#   State
#
def state():
    shaneao._state()
    gain()

#======================================
#   Data
#
helper['data'] = 'take telemetry data'
def data():
    """
    Take telemetry data.
    The data taker starts the moment the command is issued
    and records 4097 data vectors.
    (The first vector is 'bit bucket' data which should be ignored.)
    Each data vector consists of the sensor and state vectors
    appended, i.e.
        [wavefront sensor centroids x then y, tip/tilt sensor x and y,
        tweeter actuators, woofer actuators, filter states, uplink tip/tilt states]
    After 4096 loop cycles (this can take up to 82 seconds at 50 Hz)
    the data is written to disk.
    """
    inten = shaneao.rtc.rtc2.peek('inten').copy()
    inten_avg_start = np.mean(inten)
    shaneao.state['intenstart average intensity at start'] = inten_avg_start
    shaneao.status() # This forces the shaneao.rtc to update the shaneao.state dictionary.
    if (shaneao.self.rate == 'unknown'):
        raise ValueError, '<data> cannot take data if rate is unknown'
    ready = shaneao.rtc.rtc2.telemetry_ready()
    if (not ready):
        raise RuntimeError, '<data> previous data take still in progress'
    shaneao.self.data()
    t = (4096/shaneao.self.rate)*1.1
    print '<data> data recording started, this will take '+str(t)+' seconds'
    timer = threading.Timer(t,save_data)
    timer.start()

def save_data():
    print '<save_data> writing telemetry data to disk'
    shaneao.self.save_data(shaneao.state)
    print '<save_data> done'


#======================================
#   Dump telemetry
#
helper['dump_telemetry'] = 'Utility to automate telemetry collection'
def dump_telemetry(duration=60.0, interval=2.0, length=20.0):
    """
    Utility to automate telemetry collection from the ShaneAO system

    Inputs:
    AOrate    - AO system rate (50 Hz - 1.7 kHz). Get this info from GUI
    duration    - length of time in minutes over which telemetry dump should
          take place default = 1 hour
    interval    - sampling period for telemetry dumps in minutes. Default = 2
              minutes.
    length    - length of each dump in seconds. The data() command will be
              invoked as many times as necessary to create a dump of this
          length. For e.g. If the AO system is running at 1 khz, then
          each data() dump of 4K samples covers 4 seconds and so the
          data() command will be called 5 times at each sampling
          timestep
    Outputs:
    none. The data() command creates sequential fits filenames with the date
    and timestamp in the fits header (local time, NOT UT) and each file in a
    directory named by local date

    Each data vector consists of the sensor and state vectors appended, i.e.
    [wavefront sensor centroids x then y, tip/tilt sensor x and y,
    tweeter actuators, woofer actuators, filter states, uplink tip/tilt states
    """

    samples = 4097.               # number of samples taken by each data() command
    rate = shaneao.self.rate      # AO system rate in Hz
    dumpLength = samples/rate     # length in time of each data() dump
    durInSecs  = duration * 60.0  # total length of time over which to take telemetry
    intInSecs  = interval * 60.0  # time between each set of data dumps

    # number of times to run data() at each dump instance to get data time = length
    numInvokes = int(np.ceil(length / dumpLength))
    waitTime   = dumpLength*1.5+1.0   # 1.0s padding for data write time
    startTime  = time.time()
    stopTime   = startTime + durInSecs

    while time.time() < stopTime:
        print 'Time elapsed = ', round(time.time() - startTime), ' seconds'
    invoked = 0
    while invoked < numInvokes:
        data()
    invoked += 1
    time.sleep(waitTime)
    print 'Waiting for ', intInSecs, ' seconds'
    time.sleep(intInSecs)

    print "Done taking telemetry\a\a\a"

#======================================
#   Tip/Tilt "loop"
#
helper['tt_roi'] = 'Set or query the tt centroider box position'
def tt_roi(cent=None,roi_size=[6,6]):
    """Set the region of interest for the tip/tilt
    sensor centroiding or query what it is set to.

    tt_roi(boresight) sets the tt_roi to the boresight position
    """
    if (cent == 'boresight'):
        cent = boresight
    if (cent == None):
        print 'tt roi center: '+str(shaneao.self.tt_roi_center)
        print 'tt roi size: '+str(shaneao.self.tt_roi_size)
        print 'tt algorithm: '+shaneao.self.tt_algorithm
        return

    shaneao.self.set_tt_centWts(roi_center=cent, roi_size=roi_size,algo='COG')

# these are positions on the tip/tilt sensor
# corresponding to ngs mode field steer cardinal positions
boresight = [49,37]
north = [35,31]
south = [59,42]
east = [54,27]
west = [44,48]

if (shaneao.config['simulator']):
    boresight = [42,41]

tt_roi(boresight)

uplink_bias = 0 # used to be 5, but hardware must have changed. 2/25/2016

helper['save_flat'] = 'Save the present DM state vectors'
def save_flat():
    """Store the present actuator state vector as the 'flat' voltage
    file (~/data/(DM)_flat.fits). This also loads this state vector as the actuator
    default in the real-time code, and saves to disk as
    the default actuator settings (~/parameterFiles/actuator_defaults.fits)
    """
    global a_tweeter_flat, a_woofer_flat
    a = peek('a')
    at = a[0:1024]
    aw = a[1024:1024+52]
    a[1024+52:1024+52+14] = 0. # zero the filter coefficients
    a_tweeter_flat = at
    a_woofer_flat = aw
    fits.writefits(at,os.path.join(dataDir,'tweeter_flat.fits'),protect=True)
    fits.writefits(aw,os.path.join(dataDir,'woofer_flat.fits'),protect=True)
    shaneao.rtc.rtc2.load('a0',a.astype(np.double))
    defaultsFile = 'actuator_defaults.fits'
    if (shaneao.self.mode.endswith('LGS')):
        defaultsFile = 'actuator_defaults_LGS.fits'
        a[1024+52+14:1024+52+14+2] = uplink_bias #5. # set the uplink tt bias to 5 volts
    hdu = pyfits.open(os.path.join(paramsDir,defaultsFile))
    hdu.close()
    hdu[0].header['DATE'] = (str(datetime.date.today()),'File generation date')
    hdu[0].header['GENER'] = ('scripts.py','Generator code')
    hdu[0].data = a
    fits.protectFile(os.path.join(paramsDir,defaultsFile),verbose=True)
    hdu.writeto(os.path.join(paramsDir,defaultsFile),clobber=True)

helper['get_flat'] = 'Retrieve the flat state from a given file'
def get_flat(fileName,wFileName=None):
    """Retrieve the actuator defaults from a given file. This
    overrwrites parameterFiles/actuator_defaults.fits
    so it will later be read in at startup.

    Optionally,
    If the first file has only 1024 or 52 elements, modifiy
    just that part of the flat.
    If a second argument is given, read in two files, one with
    a tweeter 1024-element vector one with a woofer 52-element vector,
    and combine the two to make the flat.
    """
    if (not os.path.isfile(fileName)):
        raise ValueError, "file '%s' does not exist" % (fileName)

    na, nw, nf, nu = [1024, 52, 14, 0]
    if (shaneao.self.mode.endswith('LGS')): nu = 2
    a0 = peek('a0')
    a0_1 = fits.readfits(fileName,verbose=True,raiseError=True)
    print '<get_flat> loaded actuator flat file '+fileName
    if (len(a0_1) == na):
        a0[0:na] = a0_1[0:na]
    elif (len(a0_1) == nw):
        a0[na:na+nw] = a0_1[0:nw]
    elif (len(a0_1) == (na+nw+nf+nu)):
        a0 = a0_1

    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

    defaultsFile = 'actuator_defaults.fits'
    if (shaneao.self.mode.endswith('LGS')):
        defaultsFile = 'actuator_defaults_LGS.fits'
    hdu = pyfits.open(os.path.join(paramsDir,defaultsFile))
    hdu.close()
    hdu[0].header['DATE'] = (str(datetime.date.today()),'File generation date')
    hdu[0].header['GENER'] = ('scripts.get_flat','Generator code')
    hdu[0].data = a0
    fits.protectFile(os.path.join(paramsDir,defaultsFile),verbose=True)
    hdu.writeto(os.path.join(paramsDir,defaultsFile),clobber=True)

    if (wFileName != None):
        get_flat(wFileName)


    # Adjust the state after the recursive function call, so that the
    # woofer filename does not persist as the 'status' value.

    shaneao.state['flat_filename'] = fileName


helper['lasterr'] = 'Check for when there last was an error in woofer communication'
def lasterr():
    print shaneao.woofer.lasterr

import readfits as fits
import numpy as np
import img

#===========================================
#  Flat, bias on DMs
#  DM poke and scan
#

helper['flat'] = 'Set actuators to best known flat'
def flat(dm='both'):
    """Set actuators on one or both DMs to the stored default flat.
    Opens the loops.

    see also: flat0, bias, bias0, zero
    """
    global a_tweeter_flat, a_woofer_flat
    open()
    a0_file = 'actuator_defaults.fits'
    if (shaneao.self.mode.endswith('LGS')):
        a0_file = 'actuator_defaults_LGS.fits'
    a0 = fits.readfits(os.path.join(paramsDir,a0_file))
    a_tweeter_flat = a0[0:1024]
    a_woofer_flat = a0[1024:1024+52]
    open()
    a = shaneao.rtc.rtc2.peek('a')
    if (dm == 'both'):
            a[0:1024] = a_tweeter_flat
            a[1024:1024+52] = a_woofer_flat
    if (dm == 'tweeter'):
            a[0:1024] = a_tweeter_flat
    if (dm == 'woofer'):
            a[1024:1024+52] = a_woofer_flat
    if (shaneao.self.mode.endswith('LGS')):
        a[1024+52+14:1024:52+14+2] = uplink_bias # 5. # volts bias on uplink

helper['flat0'] = 'Set the tweeter actuator zero points to the stored defaults'
def flat0():
    """Set the actuator zero points (a0) to the defaults
    stored in the parameter file (~/parameterFiles/actuator_defaults)
    """
    global a_tweeter_flat, a_woofer_flat
    a0_file = 'actuator_defaults.fits'
    if (shaneao.self.mode.endswith('LGS')):
        a0_file = 'actuator_defaults_LGS.fits'
    a0 = fits.readfits(os.path.join(paramsDir,a0_file),raiseError=True)
    shaneao.self.a0 = a0
    a_tweeter_flat = a0[0:1024].reshape((32,32))
    a_woofer_flat = a0[1024:1024+52]
    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

helper['bias'] = 'Set the tweeter actuators to the bias (a0)'
def bias(b=21500):
    """Set the tweeter actuators to a bias.
    Nominally this bias is 21500 for the tweeter
    and 0 for the woofer.
    Opens the loop

    see also: bias0, flat, flat0, zero
    """
    open()
    bias_min = 0
    bias_max = 43000
    a = shaneao.rtc.rtc2.peek('a')
    a[0:1024] = np.clip(b,bias_min,bias_max)
    a[1024:1024+52] = 0.
    if (shaneao.self.mode.endswith('LGS')):
        a[1024+52+14:1024:52+14+2] = uplink_bias # 5. # volts bias on uplink

helper['bias0'] = 'Set the actuator zero points (a0) to a bias'
def bias0(b=21500.):
    """Set the tweeter and woofer actuator zero points (a0) to a bias"""
    bias_min = 0
    bias_max = 43000
    a0 = shaneao.self.a0
    a0[0:1024] = np.clip(b,bias_min,bias_max)
    a0[1024:1024+52] = 0.
    shaneao.rtc.rtc2.load('a0',a0.astype(np.double))

helper['poke'] = 'Put a Gaussian bump on the tweeter centered at a position'
def poke(loc,val):
    """Put a Gaussian bump on the tweeter centered at a given position"""
    m = loc
    u = img.gauss2((32,32),2,c=m)
    a = shaneao.rtc.rtc2.peek('a')
    a[0:1024] = (u*val + 12500).reshape((1024))

helper['wpoke'] = 'Poke one actuator on the woofer'
def wpoke(loc,val):
    """Poke one actuator on the woofer"""
    b = np.zeros((52))
    b[loc] = val
    a = shaneao.rtc.rtc2.peek('a')
    a[1024:1024+52] = b.copy()

helper['scan'] = 'Poke one actuator at a time, cycling through all actuators'
def scan(dm='woofer',dt=None):
    """Poke one actuator at a time in raster scan order on the deformable mirror
    ('woofer' or 'tweeter'), pausing dt seconds between each poke."""
    zero()
    if (dm == 'woofer'):
        if (dt == None): dt = .3
        time.sleep(dt)
        for k in range(52):
            wpoke(k,.3)
            time.sleep(dt)
    if (dm == 'tweeter'):
        if (dt == None): dt = .04
        time.sleep(dt)
        for y in range(32):
            for x in range(32):
                poke((x,y),10000)
                time.sleep(dt)
    zero()

def show_woof(aw):
    """Convert a 52-element list of woofer commands into an 8x8
    array, for display purposes"""
    wmask = np.ones((8*8))
    wmask[[0,1,6,7,8,15,48,55,56,57,62,63]]=0
    aw2 = np.zeros((8*8))
    i = 0
    for k in range(8*8):
        if wmask[k]:
            aw2[k] = aw[i]
            i += 1
    aw2 *= wmask
    aw2 = aw2.reshape((8,8))
    return aw2

#==========================================
#  Reset the cameras (short commands)
#

helper['rs'] = 'reset the wfs camera'
def rs():
    """Reset the wavefront sensor camera
    """
    reset()
    camstart()

helper['ttrs'] = 'reset the tt camera'
def ttrs():
    """Reset the tip tilt sensor camera
    """
    ttreset()
    ttcamstart()

trs = ttrs

confirm_setting = True # call ls() 'little state' at the end of each loop command
#============================================
#  subState
#

try:
    sub_state = shaneao.state['subState']
except KeyError:
    if shaneao.self.mode.endswith('LGS'):
        sub_state = 'lgs'
    else:
        sub_state = 'ngs'

    shaneao.state['subState'] = sub_state


def loop():
    """Report the loop status
    """
    print shaneao.state['loop']
    gain()

helper['lgs'] = 'load the LGS control matrix'
def lgs():
    """Return the LGS mode to LGS operating condition
    by loading the LGS control matrix

    see also: ngs
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    shaneao.self.load_lgs_matrix()
    shaneao.self.wfs_switch = 1.
    shaneao.state['subState'] = 'lgs'
    tt_gain(0.6) # empirical E. Gates 8/15/14
    open()

helper['ngs'] = 'load the NGS control matrix'
def ngs():
	"""Load the NGS control matrix while in LGS mode
	so as to operate as if in NGS mode
	"""
	if (not shaneao.self.mode.endswith('LGS')):
		print 'This command can only be done in LGS mode'
		return
	if (shaneao.self.mode == '16xLGS'):
		cm_filename = 'controlMatrix_16x.fits'
		if (shaneao.config['simulator']):
			cm_filename = 'controlMatrix_16x_sim.fits'
	elif (shaneao.self.mode == '8xLGS'):
		cm_filename = 'controlMatrix_8x.fits'
		if (shaneao.config['simulator']):
			cm_filename = 'controlMatrix_8x_sim.fits'
	cm = fits.readfits(os.path.join(paramsDir,cm_filename),raiseError=True)
	shaneao.self.load_ngs_matrix(cm)
	shaneao.self.wfs_switch = 1.
	shaneao.state['subState'] = 'ngs'
	shaneao.state['ngs_controlmatrix'] = cm_filename
	open()

def load_ngs_matrix(cm_filename):
	"""Load an NGS Matrix by filename."""
	cm = fits.readfits(os.path.join(paramsDir,cm_filename),raiseError=True)
	shaneao.self.load_ngs_matrix(cm)
	shaneao.self.wfs_switch = 1.
	shaneao.state['subState'] = 'ngs'
	shaneao.state['ngs_controlmatrix'] = cm_filename

def substate(state):
    ''' Single command to map to lgs() or ngs(). This is used
        internally to map KTL keyword write requests.
    '''

    if state == None:
        return

    state = state.strip()
    state = state.lower()

    if state == 'lgs':
        function = lgs
    elif state == 'ngs':
        function = ngs
    elif state == 'tt only':
        function = tt_only
    elif state == 'wfs only':
        function = wfs_only
    else:
        raise ValueError, "'%s' is not a valid substate" % (state)

    return function()


#====================================
#  tip/tilt "loop"
#

shaneao.state['tt rGain'] = shaneao.self.tipTilt_rgain
shaneao.state['tt saved rGain'] = shaneao.self.saved_ttgain
helper['tt_only'] = 'close on tip/tilt sensor only (turn off wfs inputs)'
def tt_only(ttgain=None):
    """In LGS mode, convert to running only the
    tip/tilt loop. This action opens the loop and then
    closes on tip tilt signals only, if the gain is larger
    than zero.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    shaneao.self.wfs_switch = 0. # tt only always turns off inputs from the wfs

    if (ttgain == None): # ttonly(): turn on inputs from the tip/tilt sensor
        shaneao.self.tipTilt_rgain = shaneao.self.saved_ttgain
        shaneao.close()

    elif (ttgain == 0): # ttonly(0): open the loop and disconnect the wfs sensor
        shaneao.self.saved_ttgain = shaneao.self.tipTilt_rgain
        shaneao.self.tipTilt_rgain = 0.
        shaneao.open()

    else: # ttonly(ttgain): close on wfs sensor
        shaneao.self.tipTilt_rgain = ttgain
        shaneao.self.saved_ttgain = ttgain
        shaneao.close()

    shaneao.state['subState'] = 'tt only'
    shaneao.state['tt rGain'] = shaneao.self.tipTilt_rgain
    shaneao.state['tt saved rGain'] = shaneao.self.saved_ttgain
    if (confirm_setting): ls()

helper['tt_close'] = 'close on tip/tilt sensor'
def tt_close():
    """In LGS mode, close only the tip/tilt loop
    Leave the other "loop" (wfs) as is
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    subState = shaneao.state['subState']
    if (shaneao.self.loop == 'open'):
        print '<tt_close> Closing tip/tilt only.'
        tt_only()
    elif (shaneao.self.loop == 'closed' and subState == 'tt only'):
        print '<tt_close> already closed'
    elif (shaneao.self.loop == 'closed' and subState == 'wfs only'):
        print '<tt_close> Closing tip/tilt'
        shaneao.self.tipTilt_rgain = shaneao.self.saved_ttgain
        shaneao.state['tt rGain'] = shaneao.self.tipTilt_rgain
        shaneao.close()
        shaneao.state['subState'] = 'lgs'
    elif (shaneao.self.loop == 'closed' and subState == 'lgs'):
        print '<tt_close> tt already closed'

    if (confirm_setting): ls()

helper['tt_open'] = 'open tip tilt loop'
def tt_open():
    """In LGS mode, open the tip/tilt loop. Leave the wfs loop
    as is
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    if (shaneao.self.tipTilt_rgain != 0):
        shaneao.self.saved_ttgain = shaneao.self.tipTilt_rgain

    shaneao.self.tipTilt_rgain = 0.
    shaneao.state['tt rGain'] = shaneao.self.tipTilt_rgain
    shaneao.state['tt saved rGain'] = shaneao.self.saved_ttgain

    shaneao.state['subState'] = 'lgs'

    if (shaneao.self.loop == 'closed'): # force application of the tt gain
        if (shaneao.self.wfs_switch == 1):
            shaneao.close()
            shaneao.state['subState'] = 'wfs only'
        else:
            shaneao.open()
            shaneao.state['subState'] = 'lgs'
    if (confirm_setting): ls()


def saved_tt_gain(ttgain):
    """ Set the saved TT gain. Only valid if the TT loop is open.
    """

    if ttgain < 0 or ttgain > 1:
        raise ValueError, 'saved ttgain must be >= 0 and <= 1'

    if shaneao.self.tipTilt_rgain != 0:
        raise RuntimeError, 'saved TT gain can only be set when TT loop is open (active gain is %s)' % (shaneao.self.tipTilt_rgain)

    shaneao.self.saved_ttgain = ttgain
    shaneao.state['tt saved rGain'] = ttgain


helper['tt_gain'] = 'set the tip/tilt relative input gain'
def tt_gain(ttgain=None):
    """Set or query the tip/tilt gain.
    This action does not close or open the loop it just adjusts
    the input gain from the tip/tilt sensor.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        raise RuntimeError, 'TT gain can only be set in LGS mode'

    if (shaneao.state['subState'] == 'ngs'):
        raise RuntimeError, 'TT gain can only be set in LGS mode'

    if (ttgain == None):
        ttgain = shaneao.self.tipTilt_rgain
        ttSavedGain = shaneao.self.saved_ttgain
        print 'tt gain: '+str(ttgain)+', saved tt gain: '+str(ttSavedGain)
        return

    if (ttgain == 0):
        raise ValueError, 'use tt_open() to open the TT loop'

    if (ttgain != 0):
        shaneao.self.saved_ttgain = ttgain
        shaneao.self.tipTilt_rgain = ttgain
        if (shaneao.self.loop == 'closed'):
            tt_close()
    if (confirm_setting): ls()
    shaneao.state['tt rGain'] = shaneao.self.tipTilt_rgain
    shaneao.state['tt saved rGain'] = shaneao.self.saved_ttgain

#====================================
#   wfs "loop"
#

helper['wfs_only'] = 'close on wfs input only (turns off tip/tilt inputs)'
def wfs_only():
    """In LGS mode, convert to running only the
    wavefront sensor loop. Open the tip/tilt loop.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    open()
    shaneao.self.wfs_switch = 1. # turn on inputs from the wfs

    if (shaneao.self.tipTilt_rgain != 0):
        shaneao.self.saved_ttgain = shaneao.self.tipTilt_rgain
    shaneao.self.tipTilt_rgain = 0. # turn off inputs from tip/tilt

    shaneao.close() # close the loop
    shaneao.state['subState'] = 'wfs only'
    if (confirm_setting): ls()

helper['wfs_close'] = 'close on wfs'
def wfs_close():
    """In LGS mode, close the wfs loop. Do not change
    the state of the other loop (tip/tilt)
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    shaneao.self.wfs_switch = 1. # turn on inputs from the wfs

    if (shaneao.self.loop == 'closed'): # do not change the tip/tilt gain
        if (shaneao.self.tipTilt_rgain != 0):
            shaneao.state['subState'] = 'lgs'
        else:
            shaneao.state['subState'] = 'wfs only'

    if (shaneao.self.loop == 'open'): # we expect the tt loop to stay open
        if (shaneao.self.tipTilt_rgain != 0):
            shaneao.self.saved_ttgain = shaneao.self.tipTilt_rgain
        shaneao.self.tipTilt_rgain = 0.
        shaneao.state['subState'] = 'wfs only'

    shaneao.close() # close the loop
    if (confirm_setting): ls()

helper['wfs_open'] = 'open wfs loop'
def wfs_open():
    """In LGS mode, open only the wavefront sensor loop.
    Do not change the state of the tip/tilt loop.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    shaneao.self.wfs_switch = 0. # turn off inputs from thw wfs

    if (shaneao.self.loop == 'open'):
        pass # leave all loops open

    if (shaneao.self.loop == 'closed'):
        if (shaneao.self.tipTilt_rgain == 0): # tt was open, so open all
            shaneao.open()
            shaneao.state['subState'] = 'lgs'
        else: # tt was closed, so leave it closed
            shaneao.close()
            shaneao.state['subState'] = 'tt only'
    if (confirm_setting): ls()

#==================================
#  both "loops"
#

helper['both'] = 'close both on wfs and tip/tilt sensor'
def both():
    """In LGS mode, close on both the tip/tilt sensor
    and the wavefront sensor
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (shaneao.state['subState'] == 'ngs'):
        print 'This command can only be done from an lgs subState'
        print 'type lgs() to change to the lgs subState'
        return

    tt_close()
    wfs_close()
    if (confirm_setting): ls()

helper['both_close'] = 'close both on wfs and tip/tilt sensor (same as both())'
def both_close():
    """In LGS mode, close on both the tip/tilt sensor
    and the wavefront sensor
    """
    both()

helper['both_open'] = 'open tip/tilt and wfs loops'
def both_open():
    """In LGS mode, open both tip/tilt and wfs loops
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    tt_open()
    wfs_open()
    if (confirm_setting): ls()

helper['close'] = 'close all loops'
def close():
    """In NGS mode, close the loop. In LGS mode,
    close both loops
    """
    if (shaneao.self.mode.endswith('LGS')):
        if (shaneao.state['subState'] == 'ngs'):
            shaneao.close()
        else:
            both_close()
    else:
        shaneao.close()
    if (confirm_setting): ls()

helper['open'] = 'open all loops, including uplink'
def open():
    """In NGS mode, open the loop. In LGS mode,
    also open the uplink loop
    """
    if (shaneao.self.mode.endswith('LGS')):
        if (shaneao.state['subState'] == 'ngs'):
            shaneao.open()
            if (confirm_setting): ls()
        else:
            shaneao.open()
            if (confirm_setting): ls()
            uplink_open()
            shaneao.state['subState'] = 'lgs'
    else:
        shaneao.open()
        if (confirm_setting): ls()

helper['freeze'] = 'freeze the DMs at their current position'
helper['resume'] = 'resume closed loop operation from a frozen state'
def resume():
    """Resume closed loop operation from a frozen position
    """
    shaneao.thaw()
    if (confirm_setting): ls()

helper['thaw'] = 'thaw the DMs from their frozen state (open and let sag to DM flat)'
def thaw():
    """Open the loop afer a freeze
    """
    shaneao.thaw()
    open()
    if (confirm_setting): ls()

helper['gain'] = 'set or query the AO loop gain settings'
def gain(g=None,wg=None):
    """In NGS mode, set the woofer and tweeter gains and close the loop.
    In LGS mode, set the woofer and tweeter gains and close both loops.
    """
    shaneao.gain(g,wg)
    if (g == None and wg == None): # report gains
        return
    if (g == 0 and (wg == None or wg == 0)): # open the loop on zero gains
        if (shaneao.state['subState'] == 'ngs'):
            shaneao.open()
        else:
            both_open()
        return
    if (g != None or wg != None): # push gain into the saved gain and close the loop
        if (shaneao.self.mode.endswith('LGS')):
            if (shaneao.state['subState'] == 'ngs'):
                shaneao.open()
                shaneao.close()
            elif (shaneao.state['subState'] == 'lgs'):
                shaneao.open()
                shaneao.close()
                both_close()
            elif (shaneao.state['subState'] == 'tt only'):
                print 'setting gains while in tt only mode is ambiguous'
                print 'use tt_gain to adjust the tt input gain if this is what you intend'
            elif (shaneao.state['subState'] == 'wfs only'):
                shaneao.open()
                shaneao.close()
                wfs_close()
    if (confirm_setting): ls()

helper['ls'] = 'print a fomatted display of the loop state and gains'
def ls():
    """Little state. Just display the state of the loops and gains
    """
    dum = shaneao.status() # this updates the status dictionary
    b = bleed()
    if (not shaneao.self.mode.endswith('LGS')):
        shaneao.state['subState'] = 'ngs'

    if (shaneao.state['subState'] == 'ngs'):
        print '================================='
        print 'loop is\t\t'+shaneao.state['loop']
        print 'mode is\t\t'+shaneao.state['mode']
        print 'subState is\t'+shaneao.state['subState']
        print 'tweeter gain is\t'+str(shaneao.self.gain)
        print 'woofer gain is\t'+str(shaneao.self.wgain)
        print ' '
        print 'saved gains are:'
        print '    tweeter: \t'+str(shaneao.self.savedGain)
        print '    woofer: \t'+str(shaneao.self.saved_wGain)
        print ' '
        print 'bleeds are:'
        print '    tweeter: \t'+str(b[0][10,10])
        print '    woofer: \t'+str(b[1][7])
    else:
        print '================================='
        print 'loop is\t\t'+shaneao.state['loop']
        print 'mode is\t\t'+shaneao.state['mode']
        print 'subState is\t'+shaneao.state['subState']
        print 'tweeter gain is\t'+str(shaneao.self.gain)
        print 'woofer gain is\t'+str(shaneao.self.wgain)
        print ' '
        print 'tt input gain\t'+str(shaneao.self.tipTilt_rgain)
        print 'wfs switch is\t'+str(shaneao.self.wfs_switch)
        print ' '
        print 'saved gains are:'
        print '    tweeter: \t'+str(shaneao.self.savedGain)
        print '    woofer: \t'+str(shaneao.self.saved_wGain)
        print '  tt input: \t'+str(shaneao.self.saved_ttgain)
        print ' '
        print 'bleeds are:'
        print '    tweeter: \t'+str(b[0][10,10])
        print '    woofer: \t'+str(b[1][7])
        print ' '

    if (offload()):
        ols = 'on'
    else:
        ols = 'off'
    print 'offloading:\t'+ols
    print '================================='

#===============================================
#         uplink tip/tilt
#
import socket
import time
import numpy as np

uplink_sock = None
uplink_dt = 0.01 # 100 Hz rate
uplink_enabled = False
uplink_fieldRotation = -45. # degrees
uplink_fieldFlipped = False
uplink_ma = np.matrix(np.array([0.,0.])).T
uplink_ma_alpha = 0.99 # 1 sec for 100 Hz rate

uplink_directionCosine = np.matrix(np.identity((2)))

if ('uplink enabled' not in shaneao.state):
    shaneao.state['uplink enabled'] = False
    shaneao.state['uplink loop'] = 'open'
    shaneao.state['uplink gain'] = shaneao.self.ugain
    shaneao.state['uplink savedGain'] = shaneao.self.saved_uGain
    shaneao.state['uplink bleed'] = 'unknown'
    shaneao.state['uplink angle'] = uplink_fieldRotation

helper['uplink'] = 'Enable or disable laser uplink tip/tilt control'
def uplink(cmd=None,dt=0.01):
    """In LGS mode, enable or disable the laser uplink tip/tilt system.
    This command opens the communication socket to the uplink driver
    which must be done prior to closing the uplink loop.

    uplink('on') enables the uplink communications
    uplink('off') disables the uplink communications
    uplink() gives the uplink status

    Related uplink commands are:
    uplink_open()
    uplink_close()
    uplink_gain()
    uplink_bleed()
    uplink_angle()

    """
    global uplink_sock, uplink_dt, uplink_enabled
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return

    if (cmd == None):
        if (uplink_enabled):
            print 'laser uplink is enabled'
        else:
            print 'laser uplink is disabled'
        if (confirm_uplink_setting): uplink_state()
        return

    cmd = cmd.lower()

    if (cmd == 'on'): # initialize the laser uplink connection
        print '<uplink> initializing the laser uplink connection'
        uplink_open()
        if (shaneao.config['simulator']):
            pass
        else:
            if (uplink_sock != None):
                uplink_sock.close()
            ip = '192.168.2.213'
            port=1091
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
            s.settimeout(4.)
            s.connect((ip,port))
            uplink_sock = s
        uplink_dt = dt
        uplink_enabled = True
        upl_loop()
        shaneao.state['uplink enabled'] = True
    elif (cmd == 'off'):
        uplink_open()
        if (uplink_sock != None):
            uplink_sock.close()
        uplink_enabled = False
        shaneao.state['uplink enabled'] = False
    else:
        print '<uplink> invalid argument '+str(cmd)
        return
    if (confirm_uplink_setting): uplink_state()

def upl(c1,v1=0,c2=None,v2=0):
    counter = upl.counter + 1
    if counter > 1000000:
        counter = 0

    upl.counter = counter

    pd = "AO%s,%s;" % (c1, str(v1))
    if c2 != None:
        pd += "AO%s,%s;" % (c2, str(v2))
    pd += "ZC%d\r\n" % (counter)

    if uplink_simulator == False:
        uplink_sock.send(pd)
        uplink_sock.recv(500)

upl.counter = 0
uplink_command = np.matrix(np.zeros((2))).T
uplink_simulator = shaneao.config['simulator']
def upl_loop():
    global uplink_command, uplink_ma
    if (uplink_enabled):
        a = peek('a')
        a0 = peek('a0')
        u = np.matrix(a[1024+52+14:1024+52+14+2].copy()).T
        u0 = np.matrix(a0[1024+52+14:1024+52+14+2].copy()).T
        uplink_command = uplink_directionCosine * (u-u0) + u0
        upl('2',uplink_command[0,0],'3',uplink_command[1,0])
        t = threading.Timer(uplink_dt,upl_loop)
        uplink_ma = uplink_ma_alpha * uplink_ma + (1 - uplink_ma_alpha) * (u - u0)
        shaneao.state['uplink_offload_x'] = uplink_ma[0,0]
        shaneao.state['uplink_offload_y'] = uplink_ma[1,0]
        t.start()

helper['uplink_close'] = 'close the uplink tip/tilt loop'
def uplink_close():
    """In LGS mode, close the laser uplink tip/tilt loop.
    The uplink must be enabled first, using the uplink('on') command
    """
    if (not shaneao.self.mode.endswith('LGS')):
        raise RuntimeError, 'uplink_close() can only be called in LGS mode'
    if (not shaneao.state['uplink enabled']):
        raise RuntimeError, 'uplink is not enabled'

    if shaneao.self.ugain == 0:
        shaneao.self.ugain = shaneao.self.saved_uGain
        shaneao.state['uplink gain'] = shaneao.self.ugain

    shaneao.self.set_gain()

    # +++ patch +++
    # patch around self.set_gain()
    na,nw,nf,ntts = (1024,52,14,2)
    ns = shaneao.self.ns
    cm0 = shaneao.self.controlMatrix
    cm = shaneao.self.cm
    cm[na+nw+nf:na+nw+nf+ntts,0:2*ns] = cm0[na+nw+nf:na+nw+nf+ntts,0:2*ns]*shaneao.self.ugain
    shaneao.self.cm = cm
    shaneao.rtc.rtc2.load('controlMatrix',cm.astype(np.double))
    print 'applied patch'
    # +++ patch +++

    shaneao.self.uplink_loop = 'closed'
    shaneao.state['uplink loop'] = 'closed'
    if (confirm_uplink_setting): uplink_state()

helper['uplink_open'] = 'open the uplink tip/tilt loop'
def uplink_open():
    """In LGS mode, open the laser uplink tip/tilt loop
    """
    global uplink_ma
    if (shaneao.self.mode.endswith('LGS')):
        if (shaneao.self.ugain != 0):
            shaneao.self.saved_uGain = shaneao.self.ugain
            shaneao.state['uplink savedGain'] = shaneao.self.saved_uGain
        shaneao.self.ugain = 0.
        shaneao.state['uplink gain'] = shaneao.self.ugain
        shaneao.self.set_gain()
        shaneao.self.uplink_loop = 'open'
        shaneao.state['uplink loop'] = 'open'
        uplink_ma = np.matrix(np.array([0,0])).T
        if (confirm_uplink_setting): uplink_state()


def saved_uplink_gain(ugain):
    """ Set the saved uplink gain. Only valid if the uplink loop is open.
    """

    if ugain < 0 or ugain > 1:
        raise ValueError, 'saved ugain must be >= 0 and <= 1'

    if shaneao.self.ugain != 0:
        raise RuntimeError, 'saved uplink gain can only be set when uplink loop is open'

    shaneao.self.saved_uGain = ugain
    shaneao.state['uplink savedGain'] = ugain


helper['uplink_gain'] = 'set or query the uplink tip/tilt control gain'
def uplink_gain(ugain=None):
    """In LGS mode, set or query the uplink gain. Close the loop
    if the gain is set.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (ugain == None):
        ugain = shaneao.self.ugain
        sug = shaneao.self.saved_uGain
        print 'uplink gain: '+str(ugain)+', saved uplink gain: '+str(sug)
        if (confirm_uplink_setting): uplink_state()
        return
    if (ugain == 0):
        uplink_open()
    else:
        shaneao.self.ugain = ugain
        shaneao.self.saved_uGain = ugain
        shaneao.state['uplink gain'] = ugain
        shaneao.state['uplink savedGain'] = ugain
        uplink_close()
    if (confirm_uplink_setting): uplink_state()

helper['uplink_bleed'] = 'set or query the uplink tip/tilt bleed'
def uplink_bleed(b=None):
    """In LGS mode, set or query the uplink tip/tilt loop
    integrator bleed values. The argument must be a single number,
    which will be the bleeds for both x and y channel of the uplink
    controller. The query uplink_bleed() will a tuple containing x and y
    channel bleeds.
    """
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    bpeek = shaneao.rtc.rtc2.peek('integrator_bleeds')
    na,nw,nf,ntts = (1024,52,14,2)
    if (b==None):
        r = bpeek[na+nw+nf:na+nw+nf+ntts].copy()
        return r
    bpeek[na+nw+nf:na+nw+nf+ntts] = b
    shaneao.state['uplink bleed'] = b
    if (confirm_uplink_setting): uplink_state()

helper['uplink_angle'] = 'adjust the field rotation ange of uplink tip/tilt'
def uplink_angle(theta=None,flip=False):
    """In LGS mode, this is used to set or query the present
    setting of the uplink tip/tilt field rotation angle
    """
    global uplink_directionCosine, uplink_fieldRotation, uplink_fieldFlipped
    if (not shaneao.self.mode.endswith('LGS')):
        print 'This command can only be done in LGS mode'
        return
    if (theta == None):
        print 'uplink field rotation angle = '+str(uplink_fieldRotation)+' degrees'
        print 'uplink field flipped = '+str(uplink_fieldFlipped)
        if (confirm_uplink_setting): uplink_state()
        return
    uplink_fieldRotation = theta
    shaneao.state['uplink angle'] = uplink_fieldRotation
    uplink_fieldFlipped = flip
    th = theta*(np.pi/180.)
    dcm = np.matrix(np.array([[ np.cos(th), np.sin(th)],
                              [-np.sin(th), np.cos(th)]]))
    if (flip):
        flipper = np.matrix(np.array([[1,0],[0,-1]]))
        dcm = flipper*dcm
    uplink_directionCosine = dcm
    if (confirm_uplink_setting): uplink_state()

confirm_uplink_setting = False
uplink_angle(uplink_fieldRotation,uplink_fieldFlipped) # determined from simulation
confirm_uplink_setting = True

def uplink_state():
    """Report on the uplink state
    """
    print '======================================='
    print '        Laser Uplink Tip/Tilt'
    print ' '
    print 'enabled: \t'+str(uplink_enabled)
    print 'dt\t\t'+str(uplink_dt)+' ('+str(1/uplink_dt)+'Hz)'
    if (uplink_fieldFlipped):
        ffstr = 'flipped'
    else:
        ffstr = 'not flipped'
    print 'field angle\t'+str(uplink_fieldRotation)+' degrees, '+ffstr
    print ' '
    print 'loop\t\t'+shaneao.self.uplink_loop
    print 'gain\t\t'+str(shaneao.self.ugain)
    print 'saved gain\t'+str(shaneao.self.saved_uGain)
    b = uplink_bleed()
    print 'bleed\t\t'+str(b[0])
    print ' '
    print 'current commands:'
    print '   ux: '+str(uplink_command[0,0])+'   \t uy:  '+str(uplink_command[1,0])
    print 'offloads: (mid=0,0 offload if >2 departure from mid)'
    print '   x:   '+str(uplink_ma[0,0])+'   \t y:   '+str(uplink_ma[1,0])
    print '======================================='

#================================================
#   Tip/Tilt moving average (ma)
#
    def set_tt_ma(nframes=100):
        """set the tip/tilt moving average time constant
        alpha = 0.99 <=> 100 frame average
        alpha = 0.999 <=> 1000 frame average

        combine with taking longer ttdark(nframes=...)
        """
        alpha = 1.-1./float(nframes)
        tts_ma_alpha = np.ones((1))*alpha
        shaneao.rtc.rtc2.load('tts_ma_alpha',tts_ma_alpha.astype(np.double))

#=================================================
#  Initialization - user types this first thing

helper['init'] = 'initialize - user defined list of commands to do at startup'
def init(num=16,sub=None):
    """Do all those things you want to do at startup
    Type show_init() to see the list
    """

    cache = {}
    state = shaneao.state

    if sub == None:
        # Restore the current substate after
        # setting the mode.
        sub = state['subState']

    to_cache = ('cent',
            'controlMatrix_filename',
            'refCent_filename',
            'savedGain',
            'saved_wGain',
            'tweeter_bleed',
            'uplink bleed',
            'uplink savedGain',
            'woofer_bleed')

    for key in to_cache:
        cache[key] = state[key]

    if cache['cent'].lower () == 'unknown':
        # Default to COG.
        cache['cent'] = 'COG'


    # Invoking mode() causes the rtc.rtc instance to be
    # destroyed and recreated.

    if (num == 16):
        mode('16xLGS')
        keyword_value = '16x'
    elif (num == 8):
        mode('8xLGS')
        keyword_value = '8x'

    if shaneao.config['simulator'] != True:
        ktl.write('saomot', 'LENSLETNAM', keyword_value, wait=False)

    try:
        service = state.service
    except AttributeError:
        pass
    else:
        service['INIT'].set(keyword_value)
        service['INIT'].set('waiting for command')


    camstart()
    ttcamstart()
    mems('on')
    woof('on')
    rate()
    ttrate()
    tt_roi([52,40],[10,10])
    substate(sub)
    offload('on')


    # Restore cached values.

#    shaneao.self.savedGain = cache['savedGain']
#    shaneao.self.saved_uGain = cache['uplink savedGain']
#    shaneao.self.saved_wGain = cache['saved_wGain']

#    cent(cache['cent'])
#    bleed(cache['tweeter_bleed'], 'tweeter')
#    bleed(cache['woofer_bleed'], 'woofer')

#    cached_bleed = cache['uplink bleed']
#    if cached_bleed != 'unknown':
#        uplink_bleed(cached_bleed)

#    refcent(cache['refCent_filename'])
#    set_CM(cache['controlMatrix_filename'])


    # The cameras frequently need to be reset at this
    # stage. Might as well do it all the time, you don't
    # lose anything by doing so.

    ttrs()
    rs()

    print "you'll need to do new darks"

#====================================================
#     Tub Angle
#   Set the tub angle and propagate it to the
#   direction cosines for telescope guider offload
#   and laser uplink tip/tilt
the_tub_angle = 'unknown'
helper['tub_angle'] = 'set the tub rotation angle'
def tub_angle(th = None):
    """Set the tub rotation angle. 74.0 degrees is nominally aligned position
    of ShaneAO on the tub, rectilinear with Ra and Dec.
    Rotation is in degrees, east from north
    """
    global the_tub_angle, uplink_directionCosine

    if (th == None):
        print '<tub_angle> '+str(the_tub_angle)+' degrees'
        return
    the_tub_angle = th
    base_angle = 74.0
    th = th - base_angle

    theta = th*(np.pi/180.)
    dcm_offload = np.matrix([[ np.cos(theta), np.sin(theta) ], [ -np.sin(theta), np.cos(theta) ]])
    dcm_offload = dcm_offload * np.matrix([[ 0, 1 ],[ 1, 0] ])  # telescope offload has an axis interchange

    try:
        woofer = shaneao.woofer
    except AttributeError:
        # Still initializing. This code can be reached via
        # keyword callback of the actual tub position angle
        # before initialization completes.
        return
    else:
        woofer.set_offload_direction_cosine(dcm_offload)

    th_uplink = uplink_fieldRotation - th
    theta = th_uplink*(np.pi/180.)
    dcm_uplink = np.matrix([[ np.cos(theta),np.sin(theta) ], [ -np.sin(theta), np.cos(theta) ]])
    if (uplink_fieldFlipped):
        dcm_uplink = np.matrix([[1,0],[0,-1]])*dcm_uplink
    uplink_directionCosine = dcm_uplink


#====================================================
#      Help
#
import inspect
def inspect_function(func):
    print func.__name__+' was defined in '+inspect.getfile(func)
    line_no = inspect.getsourcelines(func)[1]
    print 'starting at line # '+str(line_no)
    print '-------------------------'
    print inspect.getsource(func)
    print '-------------------------'

def show_init():
    inspect_function(init)

def help(item=None,src_info=False):
    """help is a quick online help system for ShaneAO
    help() lists all the ShaneAO commands with
        one-line descriptions for each
    help(item) gives detailed help on any particular item
    help(item,True) gives help followed by the source listing
    find_help('any string') seaches the help listings
    """
    if (isinstance(item,str)):
        find_help(item)
        return
    if (item == init): src_info = True
    if (src_info):
        print '-------------------------'
        shaneao.crib(item)
        print '-------------------------'
        inspect_function(item)
    else:
        shaneao.crib(item)

def find_help(fstr):
    print '----------------------'
    found = False
    for key in shaneao.helper.keys():
        if (fstr in key or fstr in shaneao.helper[key]):
            print key+' -- '+shaneao.helper[key]
            found = True
    if (not found):
        print ' nothing found'
    print '----------------------'

if ('offloading' not in shaneao.state):
    shaneao.state['offloading'] = False

#===============================================
#   History
#

import readline
helper['history'] = 'print a list of prior commands'
def history():
    hist_set = []
    n = readline.get_current_history_length()
    for i in range(n):
        item = readline.get_history_item(i)
        hist_set.append(readline.get_history_item(i))
        print str(i)+'    '+str(item)

#==============================================
#   Command Server
#
execfile(os.path.join(pyDir,'commandServer.py'))

#===============================================
#   List refcent files
import lrf

#==============================================
#   Camera data grabbing
#
import os
import time
import readfits as fits
import pyfits

def wfs_cam_run(n_frames=5, delta_t=1.0, comment=None):
    """run the WFS Camera for some period and save
    files to archive
    """
    inten = shaneao.rtc.rtc2.peek('inten').copy()
    inten_avg_start = np.mean(inten)
    shaneao.state['intenstart average intensity at start'] = inten_avg_start
    shaneao.status() # This forces the shaneao.rtc to update the shaneao.state dictionary.
    wfs = shaneao.rtc.rtc2.peek('wfs')
    imap = fits.readfits(os.path.join(paramsDir,'i_map.fits'),raiseError=True)
    u = []
    t = []
    for k in range(n_frames):
        time.sleep(delta_t)
        u.append(wfs.copy()[imap].reshape((160,160)))
        t.append(time.time())
        print 'frame '+str(k)
    wfs_cam_archive(u,t,comment)

def wfs_cam_archive(u,t,comment=None):
    now = time.time()
    now_tuple = time.localtime(now)

    if now_tuple.tm_hour < 12:
        target = now - 86400
        target = time.localtime(target)
    else:
        target = now_tuple

    subdirectory = time.strftime('%Y-%m-%d', target)

    archive_dir = os.path.join(home,'data','sample',subdirectory)
    seq_no = 0

    if os.path.exists(archive_dir):
        existing = os.listdir(archive_dir)

        for filename in existing:
            digits = []
            for character in filename:
                if character.isdigit():
                    digits.append(character)

            number = ''.join(digits)
            if number != '':
                number = int(number)
                seq_no = max(number, seq_no)


        seq_no += 1

    else:
        os.makedirs(archive_dir)

    if (comment == None):
        comment = raw_input('Enter a description of this wfs data: ')

    n_frames = len(u)
    for k in range(n_frames):
        tl = time.localtime(t[k])
        day = time.strftime('%Y-%m-%d',tl)
        tim = time.strftime('%H%M%S',tl)
        hdu = pyfits.PrimaryHDU(u[k])
        hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
        hdu.header.append(('DEVICE','WFS Camera'))
        hdu.header.append(('RATE',shaneao.self.rate,' frame rate'))
        hdu.header.append(('DATE',day,'File generation date'))
        hdu.header.append(('TIME',tim,'file generation time hhmmss'))
        hdu.header.append(('GENER','scripts.py','wfs_cam_archive'))
        for key in shaneao.state.keys():
            keyword = key.upper()[0:8].replace(' ','_')
            value = shaneao.state[key]
            hdu.header.append((keyword,value,key))
        hdu.header['comment'] = comment
        fitsFileName = os.path.join(archive_dir,'WFS_Data_'+ '%04d' % seq_no +'.fits')
        print 'writing to ' + fitsFileName
        hdu.writeto(fitsFileName)
        shaneao.state['wfs file number'] = seq_no
        seq_no += 1
        time.sleep(.1)

def tt_cam_run(n_frames=5, delta_t=1.0, comment=None):
    """run the TT Camera for some period and save
    files to archive
    """
    inten = shaneao.rtc.rtc2.peek('inten').copy()
    inten_avg_start = np.mean(inten)
    shaneao.state['intenstart average intensity at start'] = inten_avg_start
    shaneao.status() # This forces the shaneao.rtc to update the shaneao.state dictionary.
    tts = shaneao.rtc.rtc2.peek('tts')
    u = []
    t = []
    for k in range(n_frames):
        time.sleep(delta_t)
        u.append(tts.copy().reshape((80,80)))
        t.append(time.time())
        print 'frame '+str(k)
    tt_cam_archive(u,t,comment)

def tt_cam_archive(u,t,comment=None):
    now = time.time()
    now_tuple = time.localtime(now)

    if now_tuple.tm_hour < 12:
        target = now - 86400
        target = time.localtime(target)
    else:
        target = now_tuple

    subdirectory = time.strftime('%Y-%m-%d', target)

    archive_dir = os.path.join(home,'data','sample',subdirectory)
    seq_no = 0

    if os.path.exists(archive_dir):
        existing = os.listdir(archive_dir)

        for filename in existing:
            digits = []
            for character in filename:
                if character.isdigit():
                    digits.append(character)

            number = ''.join(digits)
            if number != '':
                number = int(number)
                seq_no = max(number, seq_no)


        seq_no += 1

    else:
        os.makedirs(archive_dir)

    if (comment == None):
        comment = raw_input('Enter a description of this tip-tilt data: ')

    n_frames = len(u)
    for k in range(n_frames):
        tl = time.localtime(t[k])
        day = time.strftime('%Y-%m-%d',tl)
        tim = time.strftime('%H%M%S',tl)
        hdu = pyfits.PrimaryHDU(u[k])
        hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
        hdu.header.append(('DEVICE','TT Camera'))
        hdu.header.append(('RATE',shaneao.self.ttrate,' frame rate'))
        hdu.header.append(('DATE',day,'File generation date'))
        hdu.header.append(('TIME',tim,'file generation time hhmmss'))
        hdu.header.append(('GENER','scripts.py','tts_cam_archive'))
        for key in shaneao.state.keys():
            keyword = key.upper()[0:8].replace(' ','_')
            value = shaneao.state[key]
            hdu.header.append((keyword,value,key))
        hdu.header['comment'] = comment
        fitsFileName = os.path.join(archive_dir,'TT_Data_'+ '%04d' % seq_no +'.fits')
        print 'writing to ' + fitsFileName
        hdu.writeto(fitsFileName)
        shaneao.state['tt file number'] = seq_no
        seq_no += 1
        time.sleep(.1)

shaneao.rtc.state = shaneao.state
shaneao.helper.update(helper)

# Having defined all of the functions, provide the shaneaoKeyword module
# with direct access to said defined functions.

try:
    shaneao.state.setNamespace(__main__,final=True)
except AttributeError:
    pass

# Ensure that the stdio keyword interface has access to the full namespace
# of this interpreter. The command is echo'd back to the local terminal
# output for completeness's sake.

def execKeywordCommand(command):
    print command

    try:
        instance = getattr(__main__,command)
    except AttributeError:
        pass
    else:
        print repr(instance)
        return


    try:
        result = eval(command,__main__.__dict__,__main__.__dict__)

    except SyntaxError:
        # Could be something like a print statement, which can't be
        # eval()'d, but can be exec'd.
        try:
            exec command in __main__.__dict__,__main__.__dict__
        except:
            import traceback
            trace = traceback.format_exc()
        else:
            # exec never returns results, which is why the call
            # to eval() is tried first.
            result = None
            trace = None

    except SystemExit:
        exception = sys.exc_info()[1]
        status = str(exception)

        if status == '':
            status = 0
        else:
            status = int(exception)

        # This can only be invoked via a remote keyword invocation
        # shaneao.exit(), which goes to some length to shut things
        # down cleanly before exiting. Invoking sys.exit(status)
        # here (or in shaneao.exit()) does not cause the running
        # interpreter to actually exit, because the SystemExit
        # exception gets caught by the KTL keyword callback machinery,
        # logged, and then ignored. Hence, the invocation of os._exit().

        os._exit(status)

    except:
        # Exceptions raised here get buried by the KTL callback
        # machinery. We want that traceback to go to stderr, just
        # like a normal Python exception. No further handling is
        # realistic in this context.

        import traceback
        trace = traceback.format_exc()
    else:
        trace = None

    if trace != None:
        sys.stderr.write(trace + '\n')
        return

    try:
        if result is None:
            pass
        else:
            print result
    except TypeError:
        # TypeError sometimes gets raised if the result object
        # does not have a __cmp__() method that handles comparisons
        # with None.
        print result



if shaneao.config['simulator'] != True:
    import shaneaoIO
    shaneaoIO.executor = execKeywordCommand
