"""shaneao idea testing
"""
import woofer_client
import numpy as np
a = np.zeros((1090))
aw = a[1024:1024+52]
wc = woofer_client.woofer_client(aw,dt=5,config='simulated')
wc.hello()

def fleep(arg):
	global u
	u = arg

wc.set_callback(fleep)

wc.thread.start()

--------
import rtc
import sauce
self = rtc.rtc('16x')
wc = rtc.woofer_connect(config='simulated')
wc.thread.set_sample(2.0)
wc.set_callback(sauce.update_woofer)

n = 128
dkx = 1
dky = 1
kx,ky = np.meshgrid((np.arange(n) - n//2) * dkx,(np.arange(m) - m//2) * dky,indexing='ij')

-------
import thread
import time

alive = True
def fcn():
	while alive:
		print 'thread going'
		time.sleep(1)
	print 'thread exiting'

thread.start_new_thread(fcn,())

alive = False

thread.exit()

--------
camstart()
light('off')
dark()
light('on')
refcent()
gain(.1)
open()
bleed(.99)
mems('on')
wrate(3)
woof('on')

gain(.25);flat()

b = bleed()
b[0:1024] = .9
b[1024:1024+52] = .995
bleed(b)

#b[0:1024]=.9;b[1024:1024+52]=.995;bleed(b);bleed_mask(.1,.7)

close()

---------
import numpy as np
def ap():
	""" aperture
	"""
	D_tele = 3.0
	D_sec = 0.8
	n_fine = 32
	du_fine = 0.1
	L = n_fine*du_fine/2.
	x1 = np.linspace(-L,L,n_fine)
	x,y = np.meshgrid(x1,x1)
	r = np.sqrt(x**2 + y**2)
	_ap = np.where(r<(D_tele/2.),1.,0.) - np.where(r<(D_sec/2.),1.,0.)
	return _ap

def bleed_mask(fast_bleed=.07,slow_bleed=.9):
	p = ap()
	b = bleed()
	u = b[0:1024].reshape((32,32))
	u[np.where(p == 0)] = fast_bleed
	v = b[1024:1024+52]
	v[21]=slow_bleed
	v[22]=slow_bleed
	v[29]=slow_bleed
	v[30]=slow_bleed
	bleed(b)



---------
# analysis
import img
import numpy as np

def strehl():
	D_tele = 3.
	r0 = 0.12
	d = D_tele/14.
	pha = sauce.aber_set[sauce.t]
	ap = sauce.ap
	wf = sauce.wf_tweeter + sauce.wf_woofer
	e = pha - wf
	rmse = img.rms(e,ap)
	s = np.exp( -(rmse*(.589/1.))**2 )
	wfa = wf*ap
	pv = wfa.max()-wfa.min()
	tt = pv*(.589/(2*np.pi))/D_tele/4.848
	s0 = np.exp( -0.3*(d/r0)**(5./3.)*(0.589/1.0)**2)
	print ' '
	print 'tilt = '+str(tt)+' arcsec'
	print 'rms error = '+str(rmse)+ ' radians, ('+str(rmse*(589./(2*np.pi)))+' nm)'
	print 'Strehl = '+str(s)+ ' at 1 micron'
	print 'calculated fit Strehl = '+str(s0)+' at 1 micron'


# centroider
n = 160
dir = '/Users/donaldgavel/parameterFiles/'
i_map = fits.readfits(dir+'i_map_subaps_16x.fits')
i_map2 = fits.readfits(dir+'i_map.fits')
centWts = fits.readfits(dir+'cent_weights_cog.fits').reshape((3,25))
wfs_background = fits.readfits(dir+'wfs_dark.fits').reshape((n*n))[i_map]
wfs_flat = fits.readfits(dir+'wfs_flat.fits').reshape((n*n))[i_map]
ns = 144

wfs = ccd
s = np.zeros((2*ns))
inten = np.zeros((ns))

pix = (wfs[i_map] - wfs_background) * wfs_flat

wx = centWts[0,:]
wy = centWts[1,:]
wi = centWts[2,:]
for k in range(ns):
    p = pix[k*25:(k+1)*25]
    x = np.dot(p,wx)
    y = np.dot(p,wy)
    i = np.dot(p,wi) + 1.
    inten[k] = i
    s[k] = x/i
    s[k+ns] = y/i

#s[0:2*ns] -= s_ref
-----------

home = os.environ['HOME']
moduleDir = os.path.join(home,'Dropbox/py/modules')
file = os.path.join(moduleDir,'cm_test.py')
execfile(file)

g = gain()
shaneao.rtc.rtc2.load("controlMatrix",(cm*g).astype(double))

aw = np.array([-0.0194197 , -0.0246959 , -0.02387583, -0.02046225, -0.01546099,
       -0.01895192, -0.01722795, -0.016935  , -0.020204  , -0.01710383,
       -0.00839031, -0.0110611 , -0.00018925,  0.01606568,  0.01520731,
       -0.00026519, -0.01281239, -0.01089886, -0.00414743, -0.00263895,
        0.00535548,  0.00799398,  0.00834365,  0.00673735, -0.00224632,
       -0.00544983,  0.0015699 ,  0.00428072, -0.00320756, -0.02002492,
       -0.02185233, -0.00158834,  0.00653261,  0.00256622,  0.00481044,
        0.01109213,  0.0070702 , -0.0090057 , -0.01020762,  0.00835141,
        0.01474171,  0.00761668,  0.01297743,  0.02034158,  0.02275223,
        0.02121603,  0.02141756,  0.01544208,  0.01697103,  0.02066599,
        0.02112873,  0.01707533])

wap = img.circle((8,8),c=[3.5,3.5],r=4)
wap1 = wap.reshape((64))
w_map = []
k = 0
for i in range(64):
	if wap1[i]:
		w_map.append(i)

aw2 = np.zeros((8*8))
aw2[w_map] = aw
aw2 = aw2.reshape((8,8))
aw2 = np.swqpaxes(aw2,0,1)
dimg.show(aw2,origin='lower')

---------------
"""Generates a new mode set for the woofer,
changing the first few mirror modes to
zernike modes
"""
import os
import readfits as fits
import numpy as np
import woofer
import zernike

home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')
subdir = os.path.join(paramsDir,'reconMatrix_16x/')
Aw = fits.readfits(subdir+'Aw_old.fits')
Aw = np.matrix(Aw)
Mw = fits.readfits(subdir+'Mw_old.fits')
Mw = np.matrix(Mw)
wooferModeSet = list(fits.readfits(subdir+'wooferModes_old.fits'))
wooferPokeSet = list(fits.readfits(subdir+'wooferPokes.fits'))

def calcM(w):
	nf = len(w)
	M = np.zeros((nf,nf))
	ap = sauce.ap
	nap = 1./np.sum(ap)
	for k in range(nf):
		u = w[k]
		for j in range(k+1):
			v = w[j]
			M[k,j] = np.sum(u*v*ap)*nap
			M[j,k] = M[k,j]
	M = np.matrix(M)
	return M

def calc_a(M,w,ph):
	nf = len(w)
	ap = sauce.ap
	nap = 1./np.sum(ap)
	b = np.zeros((nf))
	for k in range(nf):
		b[k] = np.sum(ph*w[k]*ap)*nap
	b = np.matrix(b).T
	a = M.I*b
	return a

def zern(n,m):
	ap = sauce.ap
	nn = sauce.ap.shape[0]
	rad = sauce.D_tele/sauce.du_fine/2
	r,th = zernike.polar_array(Rmax=nn/2.-.55,DS=1,pr=rad)
	z = zernike.zernike(n,m,r,th,limit=False)
	return z

def corrCoef(M):
	n,m = M.shape
	s = np.zeros((n))
	C = np.zeros((n,m))
	for k in range(n):
		s[k] = M[k,k]
		for j in range(m):
			C[k,j] = M[k,j]/(np.sqrt(M[k,k])*np.sqrt(M[j,j]))
	return C,s

# using the full poke set
w = wooferPokeSet
Mp = calcM(w)
ph = zern(1,1)
a = calc_a(Mp,w,ph)
a = np.array(a).flatten()
phr = sauce.woofer(a)
e = (ph - phr/52)*ap
dimg.show(np.clip(e,-.2,.2),fig='fit using pokes')

# using the full mode set - better fit, to about ~<10%
w = wooferModeSet
Mm = calcM(w)
ph = zern(1,1)
am = calc_a(Mm,w,ph)
a = Aw.T*am
a = np.array(a).flatten()
phr = sauce.woofer(a)
e = (ph - phr)*ap
dimg.show(np.clip(e,-.2,.2),fig = 'fit using modes')

# using only 14 modes - not so good
w = wooferModeSet[0:14]
Mm = calcM(w)
ph = zern(1,1)
am = calc_a(Mm,w,ph)
a = Aw.T[:,0:14]*am
a = np.array(a).flatten()
phr = sauce.woofer(a)
e = (ph - phr)*ap
dimg.show(np.clip(e,-.2,.2),fig = 'fit using 14 modes')

# generate new mode set and mode coefficient set
woof = woofer.woofer()
ap = sauce.ap
w = wooferModeSet
Mm = calcM(w)
zset = [[1,1],[1,-1],[2,0],[2,2],[2,-2]]
wModeSet_new = []
AwSet = list(np.array(Aw))
Aw_new=[]
# tip, tilt, focus
alocs = np.array(woof.alocs)
x = alocs[:,0]
y = alocs[:,1]
# tip
a = x/np.sqrt(np.dot(x,x))
ph = sauce.woofer(a)
wModeSet_new.append(ph*ap)
Aw_new.append(a)
# tilt
a = y/np.sqrt(np.dot(y,y))
ph = sauce.woofer(a)
wModeSet_new.append(ph*ap)
Aw_new.append(a)
# focus
a = np.sqrt(x**2+y**2)
a = a/np.sqrt(np.dot(a,a))
ph = sauce.woofer(a)
wModeSet_new.append(ph*ap)
Aw_new.append(a)

for z in zset[3:]:
	n,m = z
	ph = zern(n,m)
	am = calc_a(Mm,w,ph)
	a = Aw.T*am
	a = a/float(np.sqrt(a.T*a))
	a = np.array(a).flatten()
	ph = sauce.woofer(a)
	wModeSet_new.append(ph*ap)
	Aw_new.append(a)

for k in range(len(zset),52):
	wModeSet_new.append(w[k])
	Aw_new.append(AwSet[k])

nap = np.sum(ap)
Mw_new = calcM(wModeSet_new)*nap

pa = []
for a in Aw_new:
	pa.append(woof.show(a))

Aw_new = np.array(Aw_new)

tModeSet = list(fits.readfits(subdir+'tweeterModes.fits'))
Cwt_new = sauce.C_wooferTweeter(wModeSet_new,tModeSet,verbose=True)

fits.writefits(Aw_new,subdir+'Aw_new.fits',protect=True,verbose=True)
fits.writefits(np.array(wModeSet_new),subdir+'wooferModes_new.fits',protect=True,verbose=True)
fits.writefits(Mw_new,subdir+'Mw_new.fits',protect=True,verbose=True)
fits.writefits(Cwt_new,subdir+'Cwt_new.fits',protect=True,verbose=True)

--------------
"""Try to define a better tilt mode
"""
--------------
"""Testing the woofer
"""
import socket
import time

HOST, PORT = "musher.ucolick.org", 5000

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

arrLen = 52
mirPos = [0] * arrLen
mirPosStr = [''] * arrLen
for j in xrange(0, arrLen):
	mirPos[j] = float(j+1) * 0.001
	mirPosStr[j] = str(mirPos[j])

mirPosZerStr = [''] * arrLen
for j in xrange(0, arrLen):
	mirPosZerStr[j] = '0.0'

sock.connect((HOST, PORT))
sock.settimeout(5)
	
seq = 1

nowStr = str(time.time())
data = '{t,' +str(seq)+','+ nowStr + '}\n'
sock.sendall(data)
received = sock.recv(1024)
print received
time.sleep(1)
received = sock.recv(1024)
print received

seq = 2
nowStr = str(time.time())
data = '{m,'  +str(seq)+','+ nowStr + ',' + ','.join(mirPosStr) + '}\n'
sock.sendall(data)
received = sock.recv(1024)
print received

seq = 3
nowStr = str(time.time())
data = '{m,'  +str(seq)+','+ nowStr + ',' + ','.join(mirPosZerStr) + '}\n'
sock.sendall(data)
received = sock.recv(1024)
print received

sock.close()
-------------------
"""Test for putting the flat on MEMS
from within RTCode
2/26/2014
"""
import numpy as np
import readfits as fits
f = np.zeros((32,32))
f[4:27,8] =50000
f[16,8:16] = 50000
f[27,8:21] = 50000
f = f.reshape((1024))
a = shaneao.rtc.rtc2.peek('a')
at = a[0:1024]
aw = a[1024:1024+52]

# done in singe-step mode
map = fits.readfits('parameterFiles/tweeter_map.fits')
flat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
flat = np.clip(flat,0,1)*65535
flat[15,:] = 50000
flat[:,15] = 50000
c = np.zeros((160,160)).astype(np.uint16)
a[0:1024] = flat.reshape((1024))
shaneao.rtc.rtc2.recon(c)

# done in real-time mode
import readfits as fits
import numpy as np
flat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
flat = np.clip(flat,0,1)*65535
tu = flat.copy()
val = 50000
tu[4:27,8:10] = val
tu[16:18,8:16] = 0
tu[25:27,8:21] = 0
#c = np.zeros((160,160)).astype(np.uint16)
a = shaneao.rtc.rtc2.peek('a')
a[0:1024] = tu.reshape((1024))
#shaneao.rtc.rtc2.recon(c)

# central actuator?
import img
a = shaneao.rtc.rtc2.peek('a')
m = [15,15]
u = img.gauss2((32,32),2,c=m)
a[0:1024] = (u*30000 + 12500).reshape((1024))

# camera?
shaneao.wfs.serial('@RCL 1')

# woofer?
shaneao.woofer = shaneao.rtc.woofer_connect()
woof('on')
a[1024:1024+52]=-.5

# woofer tests 2/27/2014
import readfits as fits
wflat = fits.readfits('data/woofer_flat.fits')
u = wflat.copy()
a = shaneao.rtc.rtc2.peek('a')
a[1024:1024+52] = wflat
# poke central 4
u[[21,22,29,30]] = 0.1
a[1024:1024+52] = u
a[1024:1024+52] = wflat

#====================================
# Feb 27, 2014
"""Had trouble with MEMS driver again. Reloaded driver:
/sbin/rmmod dpio2       
/usr/local/lick/etc/init.d/modprobe.dpio2        
/sbin/lsmod | grep ^dpio2

Had trouble again after lunch. Restarted
software and repowered cambridge driver fixed it.
WFS camera lost sync going from 500 to 1500 Hz.

Had major trouble restarting rtcode. cambridge driver+dpio2
wouldn't start. had to reboot rtc. then restarted using
pattern.py, then shaneao.
"""
import readfits as fits
import numpy as np
a = shaneao.rtc.rtc2.peek('a')
# tweeter
flat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
flat = np.clip(flat,0,1)*65535
a[0:1024] = flat.reshape((1024))
# woofer
wflat = fits.readfits('data/woofer_flat.fits')
a[1024:1024+52] = wflat

woofer_map = np.arange(52)
wmask = np.ones((8*8))
wmask[[0,1,6,7,8,15,48,55,56,57,62,63]]=0
w = np.zeros((8*8))
i = 0
for k in range(8*8):
	if wmask[k]:
		w[k] = i
		i += 1

wf = np.rot90(w.reshape((8,8)),3).reshape((8*8))
woofer_map_rot90 = wf[np.where(wmask)[0]]

# done in real-time mode
# this flattens the DMs
import readfits as fits
import numpy as np
wflat = fits.readfits('data/woofer_flat.fits')
wmap = fits.readfits('parameterFiles/woofer_map.fits').astype(int)
tflat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
tflat = np.clip(tflat,0,1)*65535
tu = tflat.copy()
wu = np.zeros((52))
wu[wmap] = wflat
a = shaneao.rtc.rtc2.peek('a')

def flat(dm='both'):
	shaneao.flat()
	if (dm == 'both'):
		a[0:1024] = tu.reshape((1024))
		a[1024:1024+52] = wu
	if (dm == 'tweeter'):
		a[0:1024] = tu.reshape((1024))
	if (dm == 'woofer'):
		a[1024:1024+52] = wu


zero = shaneao.flat

def poke(loc,val):
	m = loc
	u = img.gauss2((32,32),2,c=m)
	a[0:1024] = (u*val + 12500).reshape((1024))

def wpoke(loc,val):
	b = np.zeros((52))
	b[loc] = val
	a[1024:1024+52] = b.copy()

def scan(dm='woofer'):
	zero()
	if (dm == 'woofer'):
		for k in range(52):
			wpoke(k,.3)
			time.sleep(.1)
	if (dm == 'tweeter'):
		for y in range(32):
			for x in range(32):
				poke((x,y),10000)
				time.sleep(.04)
	zero()


#------------------------------------------
# Feb 28 -
# dpio2 would not start under any circumstances

def show_woof(a):
	wmask = np.ones((8*8))
	wmask[[0,1,6,7,8,15,48,55,56,57,62,63]]=0
	ar = np.zeros((8*8))
	i = 0
	for k in range(8*8):
		if wmask[k]:
			ar[k] = a[i]
			i += 1
	
	ar *= wmask
	ar = ar.reshape((8,8))
	return ar

from readfits import readfits
import numpy as np
n_modes = 30
Hw = readfits('data/Hw.fits')
Hw = np.matrix(Hw)
Hw = Hw[:,0:n_modes]
Hw_d = (Hw.T*Hw).I*Hw.T
s = np.zeros((288))
s[0:144] = 1
s = np.matrix(s).T
c1 = Hw_d*s
s = np.zeros((288))
s[144:288] = 1
s = np.matrix(s).T
c2 = Hw_d*s
Aw = readfits('parameterFiles/reconMatrix_16x/Aw.fits')
import dimg
disp = dimg.show
Aw = Aw[0:n_modes,:]
Aw = np.matrix(Aw).T
a1 = Aw*c1
a2 = Aw*c2
w1 = show_woof(a1)
w2 = show_woof(a2)
disp(w1,fig='w1')
disp(w2,fig='w2')

------
import readfits as fits
import numpy as np
import os
map = fits.readfits(os.path.join('parameterFiles','tweeter_map.fits'))
map = map.flatten()
ad = fits.readfits(os.path.join('data','BestFlat_FullAp_Bias132.fits'))
ad = ad.reshape((1024))*65535
ad = ad.astype(int)
buffer = [0.]*1024
b = np.zeros(1024).astype(long)
b[map] = ad.astype(long).reshape(1024)
buffer = list(b)
shaneao.tweeter.write([30000,]*1024)
shaneao.tweeter.write(buffer)
#a = shaneao.rtc.rtc2.peek('a')
#a[0:1024] = ad
--------
# March 2
#  dpio2 on morely
import readfits as fits
import numpy as np
import os
home = os.environ['HOME']
param_dir = 'parameterFiles'
data_dir = 'data'
ad = fits.readfits(os.path.join(home,data_dir,'BestFlat_FullAp_Bias132.fits'))
map = fits.readfits(os.path.join(home,param_dir,'tweeter_map.fits'))
map = map.flatten()
b = np.zeros(1024).astype(long)
b[map] = (ad*65535.).astype(long).reshape(1024)
shaneao.tweeter.write(b)

bz = np.zeros(1024).astype(long)
shaneao.tweeter.write(bz)

>>> shaneao.tweeter.write(bz)
>>> shaneao.tweeter.write(b)
>>> shaneao.tweeter.stdout()
7533 7c76 789c 79db 754d 9626 72bb 80d5 7300 761e 7495 6bd1 8615 8e99 9243 8bf8 875f 8380 791e 900e 8615 808d 94d0 83aa 92c4 8ba9 8ba8 8721 8fee 85f2 9622 99f7
7f6a 7c6a 7174 714b 6266 7f01 8bf6 8ea8 80a5 69ad 6a2d 7f41 778a 8ce3 897f 7a91 939b 937a 8b12 8f64 86a2 9458 8f03 9216 8684 8db5 6eef 7bf8 92a2 91ce 95e6 94b8
7935 7cef 8d10 7c31 8e51 81d3 6da4 86d0 70ca 8528 72f8 8410 8251 839b 8fb1 769b 864a 8e5d 8a08 90db 9014 8bff 886d 925a 960e 9af8 9318 90f7 8138 7aea 9056 9213
7c97 7454 8784 884b 7de0 71c9 7a1b 7c4d 8fd6 75a0 68ad 72bc 82cc 8270 7f4e 85c6 8d54 9498 82c9 8c24 89af 962c 9118 894f 71bb 8803 651c 81e0 8b7d 970c 7e86 810a
88c6 7d6b 814c 76ca 7c92 7d03 82ab 7907 7452 7d8d 75d2 7e74 714e 7763 91f2 8d4d 8c35 93f2 8d45 8d5d 804c 946e 885c 8da1 9867 7e9f 9112 9258 8dac 9412 94a5 87d1
8a07 8778 7dc7 803d 7ea2 886e 7e6b 9016 84a0 7b07 7cfa 78c6 85cc 8f29 8894 9328 842f 8f26 7c06 7e4f 7d54 8ed0 8c48 93d4 8cb0 9387 8901 8748 949f 8bbc 81da 9675
8e5a 737d 8731 78dd 8894 81bf 87ec 8dc1 7c46 8692 8103 8ba2 83fe 86cc 8cc7 7eab 7bb5 813f 874d 7dab 7584 9383 8bdc 8090 8ecc 9392 8d3a 9439 9590 85e2 8d78 8edd
77f1 725f 7db5 8195 70fb 7a08 8588 862f 8b16 91ba 784a 7bd8 7c6a 83d6 8f13 93ab 810f 990e 92ac 952c 7af9 8248 88ce 8e68 901d 7f0a 8ab5 82af 9097 9741 8a25 936f
81d1 7df9 7f54 70e1 7078 74aa 6e91 81a4 7ebf 6f12 81f8 7644 7fc6 8994 7a02 9287 8790 931b 8ed7 90af 9053 8b63 8d11 81cc 9459 9523 967f 7d55 95f2 94c2 8926 94fb
79a2 7c57 6dfb 85c5 6a66 91c3 8f53 8daf 78db 7331 85a0 6f0f 8ca4 9473 92b8 94c4 7a4e 8bd6 816d 939d 9192 9219 7859 89a8 9327 994b 8775 95ed 7f40 95a4 8f4d 8b3a
8328 76ed 84b4 7e08 853b 8716 855c 8e59 78f5 8039 825c 8c70 8bcc 7f8d 91d1 8aff 7923 7ff8 7aff 8ae7 882f 92ce 851a 9313 80e4 8e66 915d 88e4 5eb8 94d9 93cd 8b26
7282 7deb 7500 7edb 763d 82f1 78d3 8c5f 8231 8647 8dfe 9037 6f6e 7c76 8f24 837b 8a7d 8067 8efc 93c9 8ea4 95f2 9279 7ec3 9609 954e 8490 944c 95b1 803f 8f2f 916b
8605 78e4 8615 7b6f 82e2 7e2d 84b7 898f 7b4d 8491 8c6d 8bbf 88d8 8560 8197 7bc4 8041 947d 8d5f 907f 915f 915f 76d1 8b68 93a1 766a 9002 8d50 838f 80a9 87c7 941c
8429 679c 7af9 710f 8020 7a1e 79f4 82ff 7f71 91e4 8908 8af5 8d77 93d3 919a 8b55 8a79 92a7 8da2 85d8 9185 98ab 8bc9 864a 7b0d 8014 8a69 8521 8892 96a5 86d1 9640
9247 7c43 765b 7e6c 7930 89e8 7b66 8f89 79a0 76c0 8a7c 85d2 824f 811b 8e56 7ec5 8923 8923 900b 8a80 8a0a 829a 79dc 8e79 882f 9173 916c 6dec 8e30 97dd 8766 952d
86c4 79fa 8129 7f32 820f 654e 87ac 73ad 8701 8566 7392 7ceb 7aa5 7ea3 9166 7a71 9164 87b3 7db9 994b 8337 7f3b 8a54 7ec3 8916 8fd0 8370 9537 9708 89cb 9568 802f
745d 7da9 6d98 8691 8aee 8962 7603 87fc 78dc 8274 7cde 896a 7e49 7e6c 8283 8540 911c 8b85 7c7e 9478 7e0b 8eef 689f 8c45 9763 8d75 9351 92f6 90d5 91fe 9513 9710
7de5 93b8 9387 7a0d 8d5f 8a68 9052 84d5 7082 9607 816c 7ff9 7c12 75e1 76fb 6bf8 9488 8c23 83e8 8e23 891d 7f51 837f 8656 8d41 8256 8ed1 8e8c 7da2 8cb7 9577 946c
7707 6c93 8b2f 7c7a 7503 8f1c 9168 5d98 7a4b 6708 784f 73ec 7ae4 91c4 7a08 8087 8abc 952c 8b3a 74d9 8979 87b4 85ec 8e26 8322 830f 863b 9304 8e70 8ccd 90ca 8d9c
8269 7cfa 812c 7d87 75d3 83df 7808 855a 7eb8 7514 8934 90de 821e 93a7 868a 7f3c 7a28 7603 94b1 8879 86ff 9532 9893 7cb1 921e 958e 8a2d 92ab 9756 89be 80b9 7d6d
8a5b 84c8 7dce 8186 829f 8a55 6fa3 921a 94da 894f 71b7 83f6 8148 8532 7f6e 83cb 9135 8dea 8f7a 8c23 8262 8738 7cde 8970 9496 94a1 81c2 8a75 7f15 8f29 8f57 8cfd
8188 8b9b 7c5e 7772 74c9 8eca 66cb 8e17 8d96 8c28 78da 8006 814f 7c7b 834e 8868 8e87 92c7 8228 8aa5 7a6e 89c3 7c09 83ac 891d 8e50 8d42 8d0a 90a8 8751 96b3 9336
61d3 92dd 7def 7ee4 873e 6b2b 84de 8800 8c22 79b1 7bdf 8347 9270 7da1 8fbc 8595 911f 9365 7197 703d 8445 7aea 7143 9227 8f1b 904b 947d 8b18 92ee 86b1 7e5f 90b5
913d 761e 705d 6f9a 8950 9017 6d6c 7226 8202 7e23 9481 8b8f 89da 7de6 72b3 764b 8d54 994a 8810 910a 853b 7f24 62e0 6d60 8aeb 8425 9509 9180 97c1 8b8a 8aae 92ca
8a3f 7afc 8597 895d 8ed9 857d 8645 9047 83a2 78b4 8754 7a45 7b96 87b5 8435 7ae6 8590 790b 80fe 940d 8349 8026 920f 8b6b 84e4 897b 8ed9 91f5 8373 8fa9 8eb8 93f6
7f25 8e19 7a42 8291 8a46 7d4a 73ae 938a 78b9 892d 6535 71e0 6ebc 82d4 8261 76d0 9067 8b13 8da4 817b 7fc3 8e2e 81fd 8e51 8f46 965c 8496 8094 964b 8913 8545 870b
7549 8dda 76ae 7cb8 82cd 6866 6aea 85ca 86c8 8f76 7e03 8175 7e2f 9549 7be5 87ec 9209 7a82 8235 8c0d 7d39 8192 7b69 8b8f 937a 8f8b 86e9 9175 7e41 931d 9024 908b
78b3 7e7f 7f2e 87a2 7327 7f38 736c 90d7 8434 856a 7931 8226 9008 84c2 731f 78d0 9417 8ef2 8602 9026 830d 92d6 72cf 95f9 884c 843f 7ea5 80ef 9563 81b8 8721 9882
7edc 78ba 8249 7b92 8211 8b93 79de 7bba 8f5b 8474 7d6e 8c42 79ea 731c 76bb 6b02 8693 7f66 8267 8354 82ae 934f 7822 71f6 8dd6 8132 9aa8 7ddb 930f 8615 91f9 818d
7ce3 73be 860e 6a87 7c76 7c96 838a 8a7f 8a4f 7a2b 82bf 89bb 7f59 81ec 8b43 6fe8 8243 75bd 7b3e 8cdf 7cac 83f9 9795 8860 94db 7e0f 82ce 89e4 91c4 81e2 8417 8ca7
6df3 87e3 933e 76e0 7db3 854b 91f3 7da5 84cb 8cdc 7230 85c7 7812 8429 753b 74cd 8595 9100 700d 7c03 8c15 83cc 7059 8c90 86d4 9212 8f2f 7f93 927e 9229 7d8a 8bd4
8934 8afb 6d77 7fd0 71ad 7897 69e9 8e31 7e56 9379 8878 9316 6559 8ced 6b8f 7413 875b 8ab2 98f4 8c32 7bb0 8b53 87ad 793e 82e0 9682 885e 80f4 8973 91b2 946e 9949

import readfits as fits
import numpy as np
import os
home = os.environ['HOME']
param_dir = 'parameterFiles'
data_dir = 'data'
ad = fits.readfits(os.path.join(home,data_dir,'BestFlat_FullAp_Bias132.fits'))
map = fits.readfits(os.path.join(home,param_dir,'tweeter_map.fits'))
map = map.flatten()
b = np.zeros(1024).astype(long)
b[map] = (ad*65535.).astype(long).reshape(1024)
#shaneao.tweeter.write(b)

bz = np.zeros(1024).astype(long)
#shaneao.tweeter.write(bz)

shaneao.rtc.rtc2.set_dpio2_output(shaneao.tweeter)
shaneao.rtc.rtc2.set_dpio2_output(None)

# reset on pattern
del pattern.foo
pattern.foo = pattern.dpio2.Output(1,2048)

----------------------
# March 3
#  Morely crashed hard. Not running DM:
# shaneao --no_tweeter
# rate(1500)

import dpio2
foo = dpio2.Output(1,2048)
buffer = [0,]*1024
foo.write(buffer)
buffer = [30000L,]*1024
foo.write(buffer)

import camera_box
tt_cam_test()
u = tt_get_frames()
movie(u)
----------------------------
# March 4
#  HP600 converted to 'scab' the new rtc to replace Morely
#  DM would not respond
#  rebooted scab and it responeds okay
#    excellent closed loop woofer/tweeter control!!

----------------------------
# March 5
import rtc
import edt
self = rtc.rtc('16x')
wfs = edt.Camera(1,'config/WFS_BigJoe.cfg')
h = edt.Handler(wfs,'c_recon','rtc2.so')
wfs.handler(h)
import dpio2
tweeter = dpio2.Output(1,2048)
tweeter.write([0,]*1024)

import dpio2
tweeter = dpio2.Output(1,2048)
tweeter.write([0,]*1024)
import edt
import rtc
wfs = edt.Camera(1,'config/WFS_BigJoe.cfg')
self = rtc.rtc('16x')
h = edt.Handler(wfs,'c_recon','rtc2.so')
wfs.handler(h)
tweeter.write([0,]*1024)

------------------------------
# March 8
# generated new control matrix for simulator on mac-mini

shaneao
load(scripts)
initsim()
import calibrate as cal
cal.calibrate(amp=1000,dm='tweeter')
cal.calibrate(amp=.2,bias=0,delay=1.,dm='woofer')
saveH()
saveHw()
import param_gen
param_gen.gen_CM_16x(method=2) # method 1 (Hw) doesn't work on the simulator
param_gen.gen_CM_16x() # method 1 works now - need to use data from consistent directory: sudir reconMatrix_16x
# added fits header info on context (real or simulator), creation date, method, and penalty

#    Tips on how to set the gains:

wrate(3); woof('on') #for some reason the mac forgest about the woofer tread after 1 minute
alpha(.2) # larger alphas do better as ratio of woofer rate to tweeter rate goes down
bleed(.9,'tweeter') # heavily dampen the tweeter low freq response. there is still some high spatfreq building up
# not really necessary to dial back tweeter bleed if the recontructor is built from new poke data
bleed(.999,'woofer') # make the woofer do the low freq work
bleed_mask()
gain(.5,.3) # this is about the best strehl on the simulator

cm2 = fits.readfits('parameterFiles/controlMatrix_16x.fits')
cm1 = fits.readfits('parameterFiles/archive/controlMatrix_16x.fits.20140308210712')

import os
import pyfits
home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles/')
dataDir = os.path.join(home,'data/')
subdir = os.path.join(home,'parameterFiles','reconMatrix_16x/')
hdulist = pyfits.open(subdir+'H.fits',mode='update')
hdulist[0].header.append(('context','Simulator','data created with the simulator'))
hdulist.close()

-------------------
# March 17, 2014
#  in Zenith position
#  closed loop, then
# open();bleed(1)
# to freeze
import readfits as fits
a = shaneao.rtc.rtc2.peek('a')
a_save = a.copy()
fits.writefits(a_save,'data/flat_acts_zenith.fits')

# analysis of QSI data
import img
u = readfits('Desktop/QSI Camera/2014_03_02 QSI closed loop.fits')
v = readfits('Desktop/QSI Camera/2014_03_17/2014_03_17_ClosedLoop_QSI_#1.fit')
v2 = v[39-25:39+25,49-25:49+25]
ur=(np.roll(np.roll(u,-4,axis=1),-1,axis=0))
ubg = 240
vbg = 230
# peak to total:
tu = np.sum(np.clip(ur-ubg,0,ur.max()))
tv = np.sum(np.clip(v2-vbg,0,v2.max()))
print '3/2: '+str(ur.max()/tu)
print '3/17: '+str(v2.max()/tv)
a = img.airy(50,50,4)
ta = np.sum(a)
print 'airy: '+str(a.max()/ta)

ap = img.circle((50,50),r=8.) - img.circle((50,50),r=8.*(0.8/3.))
#ap = img.circle((50,50),r=8.) - img.circle((50,50),r=2.4)
fap = np.abs(img.shift(np.fft.fft2(ap),(25,25)))**2
tfap = np.sum(fap)
print 'ap ft: '+str(fap.max()/tfap)

-------------------
# March 18, 2014
# in Zenith position
#  took data in WFS, TTS, QSI for baseline beam
#  closed loop - rembmber to take WFS dark!
# reference positions for X and Y directions on TT cam
#
# problems:
#  test pattern not showing up on tt display (prior to ttcamstart)
#  write more things to the fits header:
#    frame rate
#    ao loop status, gain, bleed ((2,2),(10,10),w(10))

--------------
reload(sauce)
from sauce import *
s = np.zeros((512,512))
ccd = wfs(s)
ccdu = ccd.reshape((160*160))[imap].reshape((160,160))
dimg.show(ccdu,origin='lower')

# find the locations of subaps
sloc = []
delta = 0
if (mode == 0):
	delta = d_wfs*.2

x1 = (np.arange(0,n_across+1)-(n_across/2)-0.5)*d_wfs
for x in x1:
    for y in x1:
        r = np.sqrt(x**2 + y**2)
        if (r < (D_tele/2+delta)) and (r > D_sec/2):
            sloc.append((x,y))

isloc = np.array(sloc)/du_fine + n_fine/2
u = np.zeros((512,512))
for xy in isloc:
	u[xy[0]-30:xy[0]+30,xy[1]-30:xy[1]+30] = 1

dimg.show(u+ap)

mode = 0
camPix = 160
subap = 5
n_across_select = [7, 14, 30]
n_sec_select = [2,4,8]
delta_select = [0.5,0.,0.]
n = camPix/subap
d = n_across_select[mode]
dsec = n_sec_select[mode]
delta = delta_select[mode]
sap = img.circle((n,n),c=(n/2-0.5,n/2-0.5), r=(d+delta)/2)
sap -= img.circle((n,n),c=(n/2-0.5,n/2-0.5),r=(dsec+1)/2)

illum = np.where(sap == 1)

-----
import socket
import numpy as np
import struct

def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

ns = 144
nbytes = 8*ns
form = str(ns)+'d'
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.settimeout(4)
host = 'localhost'
port = 8888
remote_ip = socket.gethostbyname(host)
s.connect((remote_ip,port))

s.sendall('inten')
b = recv_all(s,8)
r_count = struct.unpack('1d',b)
ru = int(r_count[0])
ru

b = recv_all(s,nbytes)
r = np.array(struct.unpack(form,b))
r[0:3]

s.sendall('bye')
s.close()
------------------------
# March 25, 2014
# Testing the 8x calibration with the simulator
#
load(scripts)
camstart()

mode('16x')
initsim()
aber('atmos')
bleed(.9,'tweeter')
bleed(.95,'woofer')
bleed_mask(.8,.8)
wrate(10)
woof('on')
mems('on')
gain(.7)
strehl();show(300) # strehl of 0.7
zero()

mode('8x')
initsim()
aber('atmos')
bleed(.99)
bleed_mask(.95,.9)
wrate(10)
woof('on')
mems('on')
gain(.7)
strehl();show(300) # strehl of 0.4
zero()

-----------------
# March 26, 2014
#  Talking to the RIO
# there's a 10mv noise, ~5kHz * 60 Hz on the output
#  Mike S put a 1uF cap across and it cut it down to ~1mv
#  and didn't affect a 20 Hz sinusoid from +/- 3V
import socket
import time
import numpy as np

def upl_on():
	global s
	ip = '192.168.2.213'
	port=1091
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
	s.settimeout(4.)
	s.connect((ip,port))	

def upl_off():
	s.close()

def upl(chan_num,v=0):
	chan = str(chan_num)
	volts = str(v)
	#ip = '192.168.2.213'
	#port=1091
	#s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
	#s.settimeout(4.)
	#s.connect((ip,port))
	eos = '\r\n'
	pd = 'AO'+chan+','+volts+eos
	s.send(pd)
	u = s.recv(500)
	print u
	#s.close()

ip = '192.168.2.213'
port=1091
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
s.settimeout(4.)
s.connect((ip,port))
chan = str(1)
volts = str(0.0)
eos = '\r\n'
pd = 'AO'+chan+','+volts+eos
s.send(pd)
u = s.recv(500)
print u
s.close()

t = np.arange(0,1,.002)
w = np.sin(2*np.pi*t*20.)

for k in range(10):
	for ws in w:
		volts = str(3*ws)
		pd = 'AO'+chan+','+volts+eos
		n = s.send(pd)
		u = s.recv(500)
		time.sleep(0.002)

------------------
# March 27, 2014
# Trying to make ttpeeko and color bars work properly

import matplotlib
matplotlib.use('TkAgg')
import numpy as np
from matplotlib import pyplot as plt

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(np.random.rand(10))

def onclick(event):
    print 'button=%d, x=%d, y=%d, xdata=%f, ydata=%f'%(
        event.button, event.x, event.y, event.xdata, event.ydata)

cid = fig.canvas.mpl_connect('button_press_event', onclick)
-----------------
# March 29
# Getting the status display over to peeko client
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

plt.ion()
fig = plt.figure()
#ax = fig.add_subplot(111)
ax = fig.add_axes([0,0,1,1])
ax.text(.5,.5,'hello')
ax.text(.5,.6,'hello',bbox={'facecolor':'red', 'alpha':0.5, 'color':'white'})
plt.draw()

# try tcl instead
set server localhost
set port 8888
set sock [socket $server $port]
fconfigure $sock -buffering none; # -blocking 0
set request "status"

puts $sock "$request"
set u [read $sock 8]
binary scan $u i count
set v [read $sock $count]

puts $sock "bye"
close $sock

---------------------
# April 5
# calibrate the tip and tilt components on the woofer
#
import numpy as np
import readfits as fits

woofer_basis = np.zeros((52,2))
aw = a[1024:1024+52]
aber('fixz',1,zern=0)
close()
time.sleep(4) # wait a few seconds to settle
woofer_x = aw.copy()
normalization_factor = np.dot(woofer_x,woofer_x)
woofer_basis[:,0] = woofer_x / normalization_factor

aber('fixz',1,zern=1)
time.sleep(4) # wait a few seconds to settle
woofer_y = aw.copy()
normalization_factor = np.dot(woofer_y,woofer_y)
woofer_basis[:,1] = woofer_y / normalization_factor

fits.writefits(woofer_basis,'data/woofer_tilt_basis.fits')

# add the focus component
# these will be used to display t/t/f components on the peeko display
woofer_basis = fits.readfits('data/woofer_tilt_basis.fits')
Aw = fits.readfits('parameterFiles/reconMatrix_16x/Aw.fits')
Aw = Aw[0:14,:].transpose()
Aw[:,0:2] = woofer_basis[:,0:2]
fits.writefits(Aw,'data/woofer_basis.fits')

--------------------
# April 6
#  measure the time spent computing the offload
#
f = open('log','r')
l1 = f.readline()
d1 = []
while True:
	l = f.readline()
	if (l == 'woofer thread started\n'): break
	u = l.split(' ')
	d1.append(float(u[6]))

d2 = []
while True:
	l = f.readline()
	if (l == 'woofer thread started\n'): break
	u = l.split(' ')
	d2.append(float(u[6]))

f.close()

import pylab as P
P.figure(2)
n,bins,patches=P.hist(d2,500)
n,bins,patches=P.hist(d1,500)
P.grid()
# its pretty clear that the offloading
# takes approximately 100 us, compared
# to about 250 us taken for everything else
# in the woofer control loop (in woofer_client.py)
# i.e. preparing the string to send out the socket
# to the woofer server on musher

---------------------
# April 8
#   Codes broken on transport: peeko_client2.py
#     won't accept data from dataServer
def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = 'real'
port = 8888
remote_ip = socket.gethostbyname(host)
s.connect((remote_ip,port))
s.sendall('inten')
b_count = recv_all(s,8)
r_count = int(struct.unpack('1q',b_count)[0])
b = recv_all(s,r_count*8)
form = str(r_count)+'d'
r = np.array(struct.unpack(form,b))

>>> r_count = int(struct.unpack('1l',b_count)[0])
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
struct.error: unpack requires a string argument of length 4

s.sendall('a')
raw_size = 32*32+52+14
nbytes = raw_size*8
b = recv_all(s,nbytes)
form = str(raw_size)+'d'
r = np.array(struct.unpack(form,b))

# problem with heartbeat
# 
Unix: gshow -s saomot disp%sta
   each of the dispatchers are alive and communicating with the galil

This is now "check_ktl.py"
import ktl
saomot = ktl.Service('saomot')
saomot.heartbeat ('DISP3CLK') # MEMSPULSE is provided by dispatcher #3
memspulse = saomot['MEMSPULSE']
memspower = saomot['MEMSPOWER']
memspower.write('enable')
memspulse.write(1,wait=False)
memspower.write('disable')

----------
# April 25, 2014
# appropriately head the H and Hw files
import os
import pyfits
home = os.environ['HOME']
subdir = os.path.join(home,'cvs/lroot/ShaneAO/unreal/recon/parameterFiles/reconMatrix_16x')
filename = os.path.join(subdir,'H.fits')
mode = '16x'
ns = 144
n_tweeter_modes = 224
hdu = pyfits.open(filename)
hdu[0].header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
hdu[0].header.append(('ITYPE','ParamGen',' Parameter Generator'))
hdu[0].header.append(('FUNC','InfluenceMat','Modal Influence Matrix'))
hdu[0].header.append(('SYMBOL','H'))
hdu[0].header.append(('MODE',mode))
hdu[0].header.append(('SEC',True,' Secondary obscuration'))
hdu[0].header.append(('NS',ns,' # of subapertures'))
hdu[0].header.append(('NA',n_tweeter_modes,' # of tweeter modes'))
hdu[0].header.append(('DATE','2014-04-13','File generation date'))
hdu[0].header.append(('GENER','calibrate.py','Generator code'))
hdu[0].header.append(('CONTEXT','Real','data created using the live system'))
protectFile(filename)
hdu.writeto(filename)

ns = 144
n_woofer_modes = 52
filename = os.path.join(subdir,'Hw.fits')
hdu = pyfits.open(filename)
hdu[0].header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
hdu[0].header.append(('ITYPE','ParamGen',' Parameter Generator'))
hdu[0].header.append(('FUNC','InfluenceMat','Woofer Modal Influence Matrix'))
hdu[0].header.append(('SYMBOL','Hw'))
hdu[0].header.append(('MODE',mode))
hdu[0].header.append(('SEC',True,' Secondary obscuration'))
hdu[0].header.append(('NS',ns,' # of subapertures'))
hdu[0].header.append(('NA_W',n_woofer_modes,' # of tweeter modes'))
hdu[0].header.append(('DATE','2014-04-13','File generation date'))
hdu[0].header.append(('GENER','calibrate.py','Generator code'))
hdu[0].header.append(('CONTEXT','Real','data created using the live system'))
protectFile(filename)
hdu.writeto(filename)

subdir = os.path.join(home,'cvs/lroot/ShaneAO/unreal/recon/parameterFiles')
filename = os.path.join(subdir,'controlMatrix_16x.fits')
hdu = pyfits.open(filename)
hdu[0].header['CONTEXT'] = ('Real','data created using the live system')
hdu[0].header['HDATE'] = ('2014-04-13','date of creation of the H data')
protectFile(filename)
hdu.writeto(filename)

-------------------
# April 28, 2014
# raw data from first light run
import readfits as fits
import numpy as np
import dimg
a = fits.readfits('magcomb140416_PhiGem_loopclosed_BrG.fits')
ac = [277,376]
b = fits.readfits('cem140416_0950.fits')
bc = [189,237]
s = 100
ua = np.clip((a-250)[ac[0]-s/2:ac[0]+s/2,ac[1]-s/2:ac[1]+s/2],10.,200000.)
ub = np.clip((b+200)[bc[0]-s/2:bc[0]+s/2,bc[1]-s/2:bc[1]+s/2],1.,100000.)

dimg.show(ua)
dimg.show(ua,cmap='jet')
dimg.show(np.sqrt(ua))
dimg.show(np.log10(ua))

dimg.show(ub)
dimg.show(ub,cmap='jet')
dimg.show(np.sqrt(ub))
dimg.show(np.log10(ub))

-------------------
# April 29, 2014
# Test LGS tip/tilt and uplink processing
import pyfits
import readfits as fits
import numpy as np
import os
home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')
baseName = 'controlMatrix_16x.fits'
filename = os.path.join(paramsDir,baseName)
hdu = pyfits.open(filename)
cm = hdu[0].data

na,naw,nf,ns = 1024,52,14,144
subdir = os.path.join(paramsDir,'reconMatrix_16x/')
At = fits.readfits(subdir+'At.fits')
Aw = np.matrix(fits.readfits(subdir+'Aw.fits'))
n_tweeter_modes = At.shape[0]
At = np.matrix(At.reshape((n_tweeter_modes,na))).transpose()
Aw = Aw[0:nf,:].transpose()
H = np.matrix(fits.readfits(subdir+'H.fits'))
Hw = np.matrix(fits.readfits(os.path.join(subdir,'Hw.fits')))
Hw = Hw[:,0:nf]
Ts = Hw[:,0:2]

Tr_s = np.matrix(np.identity(2*ns)) - Ts*(Ts.T*Ts).I*Ts.T
O_sv = np.matrix(np.zeros((2*ns,nf)))
O_vs = O_sv.T
I_vv = np.matrix(np.identity(nf))

Tr = np.vstack(
	[
		np.hstack([Tr_s, O_sv]),
		np.hstack([O_vs, I_vv])
	]
)

import param_gen
use_penalty = hdu[0].header['PENALTY']
S,Abar,H_d,R,U,s = param_gen.penalty(At,H)
Hw_d = (Hw.T*Hw).I*Hw.T
Htt_d = H_d*Ts
Hwt_d = Hw_d*Ts

tilt_processing = np.vstack(
	[
		-At*Htt_d,
		-Aw*Hwt_d,
		-Hwt_d
	]
)

O_2xnf = np.matrix(np.zeros((2,nf)))
uplink_processing = np.hstack(
	[ (Ts.T*Ts).I*Ts.T, O_2xnf ]
)

O_2x2 = np.matrix(np.zeros((2,2)))

cm_LGS = np.vstack(
	[
		np.hstack([cm*Tr, tilt_processing]),
		np.hstack([uplink_processing, O_2x2])
	]
)

-------------------
# April 30, 2014
# testing refcent tilt shift
import pyfits
import readfits as fits
import numpy as np
import os
home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')
subdir = os.path.join(paramsDir,'reconMatrix_16x/')
Hw = np.matrix(fits.readfits(os.path.join(subdir,'Hw.fits')))
na,naw,nf,ns = 1024,52,14,144
Hw = Hw[:,0:nf]
Ts = Hw[:,0:2]
tip = np.array(Ts[:,0]).reshape((288))
tilt = np.array(Ts[:,1]).reshape((288))
s_ref = shaneao.rtc.rtc2.peek('s_ref').copy()
refcent(s_ref+tip)
#
# To calibrate:
# Method 1
#  load the best refcents and close the loop, image sharpen
#  record the spot image on the tt sensor
#  open/freeze the DMs (might want to test how long the Strehl lasts)
#  move the source a prescribed amount (one unit of tilt)
#  take reference centroids
#  close the loop
#  record the spot image on the tt sensor
#  the unit tilt s-vector is 2nd refcents - 1st refcents
#
# Method 2
#  close the loop on best refcents, image sharpen
#  record the spot image on the tt sensor
#  add a unit of tilt from column 0 of the Hw matrix to the refcents
#  record the spot image on the tt sensor
#  compare the Strehl

--------------------
#May 4, 2014
# updating sauce to include tip/tilt sensing
wf = ap*np.exp(1j*ph)
wf = zeropad(wf,(2*n_fine,2*n_fine))
#fp = abs(np.fft.fft2(wf))**2
fp = np.fft.fftshift(abs(np.fft.fft2(wf))**2)
fp = np.abs(ndimage.rotate(fp,tt_field_rotation,reshape=False))
fp = np.fft.ifftshift(fp)

f_fp_pixelConvolved = np.fft.fft2(fp)*np.conj(f_tt_pixel)
fp_pixelConvolved = np.real(np.fft.ifft2(f_fp_pixelConvolved))
fp_pixelConvolved = np.fft.fftshift(fp_pixelConvolved)
fp_binned = fp_pixelConvolved.reshape((2*n_fine*2*n_fine))[tt_pluck].reshape((n_plucks,n_plucks))
	
ccd = zeropad(fp_binned,(80,80))    

----------------------
#May 5, 2014
# Test the tip/tilt centroider in simulation
#
mode('16xLGS')
a_peek = shaneao.rtc.rtc2.peek('a')
c = [42.,41.]
rs = [6.,6.]
algo = 'COG'
shaneao.self.set_tt_centWts(roi_center=c, roi_size=rs,algo=algo)
s = shaneao.rtc.rtc2.peek('s')
dtt_s = s[288+14:]

tmp = np.zeros((2))
shaneao.rtc.rtc2.load('dtt',tmp.astype(np.double))
dtt = shaneao.rtc.rtc2.peek('dtt')

ttcw = shaneao.rtc.rtc2.peek('tt_centWts').reshape((3,80,80))
tts = shaneao.rtc.rtc2.peek('tts').reshape((80,80))

tmp = np.zeros((1))
shaneao.rtc.rtc2.load('tt_inten',tmp.astype(np.double))
tt_inten = shaneao.rtc.rtc2.peek('tt_inten')

inten = np.sum(tts*ttcw[2,:,:])
tt_inten
dtt
r = 500
cx = np.sum(tts*ttcw[0,:,:])/(inten+r)
cy = np.sum(tts*ttcw[1,:,:])/(inten+r)
cx, cy

th = 24.3*(np.pi/180.)
dcm = np.matrix([[np.cos(th),-np.sin(th)],[np.sin(th),np.cos(th)]])
dtt = np.matrix(peek('dtt')).T
dxy = dcm*dtt
dxy - dxy0
------------------------
# May 6, 2014
#  Test LGS tip/tilt mode with the simulator
mode('16xLGS')
initsim()
ttcamstart()
light('off')
time.sleep(.3)
ttdark()
light('on')

th = 24.3*(np.pi/180.)
#dcm = np.matrix([[-np.cos(th),-np.sin(th)],[-np.sin(th),np.cos(th)]])
dcm = np.matrix([[-np.cos(th),np.sin(th)],[np.sin(th),np.cos(th)]])

aber('ref')
time.sleep(2.)
dtt0 = peek('dtt')
s = peek('s')
ns = 144
sx0 = s[0:ns]
sy0 = s[ns:2*ns]
cw = peek('tt_centWts').reshape((3,80,80))

aber('fixz',-6,zern=0) # 1/2 pixel +x move on wfs in cent('cg')
time.sleep(2.)
tts = peek('tts').reshape((80,80))
s = peek('s')
sx = s[0:ns]
sy = s[ns:2*ns]
sx, sy
dtt = np.matrix(peek('dtt')).T
dtt
dttr = dcm*dtt
dttr

aber('fixz',6,zern=1) # 1/2 pixel +y move on wfs in cent('cg')
time.sleep(2.)
tts = peek('tts').reshape((80,80))
s = peek('s')
sx = s[0:ns]
sy = s[ns:2*ns]
sx, sy
dtt = np.matrix(peek('dtt')).T
dtt
dttr = dcm*dtt
dttr

cmFile = 'controlMatrix_16xLGS.fits'
set_CM(cmFile)
open()

dtt = np.matrix(peek('dtt')).T
dttr = dcm*dtt

ns,nf = (shaneao.self.ns,shaneao.self.nfilt)
cm = np.matrix(shaneao.self.controlMatrix)
s = np.matrix(peek('s')).T
a = cm*s
at = a[0:1024].reshape((32,32))
aw = show_woof(a[1024:1024+52])
fc = a[1024+52:1024+52+14]
#dimg.show(at,'twweer',origin='lower')
dimg.show(aw,'woofer',origin='lower')

subdir = 'parameterFiles/reconMatrix_16x/'
Aw = fits.readfits(subdir+'Aw.fits')
dimg.show(show_woof(Aw[0,:]),'woofer mode 0',origin='lower')
dimg.show(show_woof(Aw[1,:]),'woofer mode 1',origin='lower')

----------------------
# May 8, 2014
# testing LGS mode in simulator
import os
home = os.environ['HOME']

mode('16xLGS')
light('off')
dark()
light('on')
aber('ref')
refcent()

# test the tip/tilt feedback from the tt sensor only
nat,naw = (shaneao.self.na_tw,shaneao.self.na_woof)
ns,nf = (shaneao.self.ns,shaneao.self.nfilt)
cmFile = 'controlMatrix_16xLGS.fits'
set_CM(cmFile)
cm = np.matrix(shaneao.self.controlMatrix)
cm[:,0:2*ns] = 0. # zero all inputs from the wavefront sensor
shaneao.self.controlMatrix = cm
shaneao.self.set_tt_centWts(roi_center=[42,41],roi_size=[4,4],algo='COG',plateScaleFactor=0.33)

# test there is no ti/tilt feedback from the wf sensor
nat,naw = (shaneao.self.na_tw,shaneao.self.na_woof)
ns,nf = (shaneao.self.ns,shaneao.self.nfilt)
cmFile = 'controlMatrix_16xLGS.fits'
set_CM(cmFile)
cm = np.matrix(shaneao.self.controlMatrix)
cm[:,2*ns+nf:2*ns+nf+2] = 0. # zero all inputs from the tip/tilt sensor
shaneao.self.controlMatrix = cm

cmFile = 'controlMatrix_16x.fits'
cm0 = fits.readfits(os.path.join(home,'parameterFiles',cmFile))
cmFile = 'controlMatrix_16xLGS.fits'
set_CM(cmFile)
cm = np.matrix(shaneao.self.controlMatrix)
cm[0:1090,0:302] = cm0 # rest of matrix is as for NGS mode
cm[:,2*ns+nf:2*ns+nf+2] = 0. # zero all inputs from the tip/tilt sensor
shaneao.self.controlMatrix = cm

-----------
import os
import readfits as fits
import numpy as np
import time

home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')

shaneao.sauce.cam_backgnd *= 0
shaneao.sauce.ne = 1.e-10
i_map = fits.readfits(os.path.join(home,'parameterFiles','i_map.fits'))
aber('fixz',3,zern=0)

time.sleep(1)
wfs = peek('wfs')[i_map].reshape((160,160))
wfs_scramb = wfs*0.
wfs_scramb.reshape((160*160))[i_map] = wfs.reshape((160,160))

def test_16x():
	zero()
	mode('16x')
	wfs_background = fits.readfits(os.path.join(paramsDir,'wfs_dark_sim.fits'))
	shaneao.rtc.rtc2.load('wfs_background',wfs_background.astype(np.double))
	s_ref = fits.readfits(os.path.join(paramsDir,'ref_cent_16x_sim.fits'))
	shaneao.rtc.rtc2.load('s_ref',s_ref.astype(np.double))
	camstart()
	woof('on')
	bleed('all',0)
	
	time.sleep(1)
	camstop()
	
	time.sleep(1)
	cmFile = 'controlMatrix_16x_sim.fits'
	set_CM(cmFile)
	
	gain(1)
	shaneao.rtc.rtc2.recon(wfs_scramb.astype(np.uint16))

#---
def test_16xLGS():
	zero()
	mode('16xLGS')
	wfs_background = fits.readfits(os.path.join(paramsDir,'wfs_dark_sim.fits'))
	shaneao.rtc.rtc2.load('wfs_background',wfs_background.astype(np.double))
	s_ref = fits.readfits(os.path.join(paramsDir,'ref_cent_16x_sim.fits'))
	shaneao.rtc.rtc2.load('s_ref',s_ref.astype(np.double))
	camstart()
	ttcamstart()
	woof('on')
	bleed('all',0)
	
	time.sleep(1)
	camstop()
	
	time.sleep(1)
	
	ns,nf = (shaneao.self.ns,shaneao.self.nfilt)
	cmFile = 'controlMatrix_16x_sim.fits'
	cm0 = fits.readfits(os.path.join(home,'parameterFiles',cmFile))
	cmFile = 'controlMatrix_16xLGS.fits'
	set_CM(cmFile)
	cm = np.matrix(shaneao.self.controlMatrix)
	cm[0:1090,0:302] = cm0 # rest of matrix is as for NGS mode
	cm[:,2*ns+nf:2*ns+nf+2] = 0. # zero all inputs from the tip/tilt sensor
	shaneao.self.controlMatrix = cm
	
	gain(1)
	shaneao.rtc.rtc2.recon(wfs_scramb.astype(np.uint16))

wfs_background = fits.readfits(os.path.join(paramsDir,'wfs_dark_sim.fits'))
shaneao.rtc.rtc2.load('wfs_background',wfs_background.astype(np.double))
s_ref = fits.readfits(os.path.join(paramsDir,'ref_cent_16x_sim.fits'))
shaneao.rtc.rtc2.load('s_ref',s_ref.astype(np.double))

ns,nf = (shaneao.self.ns,shaneao.self.nfilt)
cmFile = 'controlMatrix_16x_sim.fits'
cm0 = fits.readfits(os.path.join(home,'parameterFiles',cmFile))
cmFile = 'controlMatrix_16xLGS.fits'
set_CM(cmFile)
cm = np.matrix(shaneao.self.controlMatrix)
cm[0:1090,0:302] = cm0 # rest of matrix is as for NGS mode
#cm[:,2*ns+nf:2*ns+nf+2] = 0. # zero all inputs from the tip/tilt sensor
shaneao.self.controlMatrix = cm

cmFile = 'controlMatrix_16xLGS_sim.fits'
set_CM(cmFile)

shaneao.self.tipTilt_rgain = 0.1

--------------------------
May 13
Testing blocks of the control matrix

na,nw,nf,ns,ntts,nu = (1024,52,14,144,2,2)
cm = np.zeros((na+nw+nf+nu,2*ns+nf+ntts))
gain, wgain, ttgain, alpha, ugain = (.7,.3,.8,1,.5)
cm[0:na,0:2*ns] = gain
cm[na:na+nw,0:2*ns] = wgain
cm[0:na,2*ns:2*ns+2] = ttgain
cm[0:na,2*ns+2:2*ns+nf] = gain
cm[na+nw:na+nw+nf,:] = alpha
cm[0:na+nw,2*ns+nf:2*ns+nf+2] = ttgain
cm[na+nw+nf:na+nw+nf+ntts,0:2*ns] = ugain

na, nw, nf, ns, ntts = (shaneao.self.na_tw, shaneao.self.na_woof, shaneao.self.nfilt, shaneao.self.ns, shaneao.self.ntts)
shaneao.self.cm = shaneao.self.controlMatrix.copy() #: cm is the control matrix after it has been multiplied by the gain

shaneao.self.cm[0:na,    0:2*ns] *= shaneao.self.gain   # [ tweeter, wfs ]
shaneao.self.cm[na:na+nw,0:2*ns] *= shaneao.self.wgain  # [ woofer, wfs ]

shaneao.self.cm[0:na,     2*ns:2*ns+2        ] *= 0.          # [ tweeter, filter_tt_modes ]
shaneao.self.cm[0:na,     2*ns+2:2*ns+nf     ] *= shaneao.self.gain   # [ tweeter, filter ]
shaneao.self.cm[na:na+nw, 2*ns:2*ns+nf       ] *= 0           # [ woofer, filter ]
shaneao.self.cm[na:na+nw, 2*ns+nf:2*ns+nf+2  ] *= shaneao.self.ttgain # [ woofer, tt ]

shaneao.self.cm[na+nw:na+nw+nf, 0:2*ns+nf        ] *= (1.-shaneao.self.alpha) # [ filter, wfs|filter ]
shaneao.self.cm[na+nw:na+nw+nf, 2*ns+nf:2*ns+nf+2] *= 0.              # [ filter, tt ]

shaneao.self.cm[na+nw+nf:na+nw+nf+ntts, 0:2*ns           ] *= shaneao.self.ugain  # [ uplink, wfs ]
shaneao.self.cm[na+nw+nf:na+nw+nf+ntts, 2*ns:2*ns+nf+ntts] *= 0           # [ uplink, filter|tt ]

--------------------------------------------
# May 15
# debugging param_gen.py
import os
import pyfits
import readfits as 
import numpy as np
import param_gen

home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')

baseName = 'controlMatrix_16x_sim.fits'
filename = os.path.join(paramsDir,baseName)
hdu = pyfits.open(filename)
hdu[0].header['CONTEXT'] = 'Simulator'
hdu.writeto(filename,clobber=True)

--------------------------------------------
# May 16
#  Testing uplink

init()
light('off')
dark()
ttdark()
light('on')
bias0()
bias()
tt_roi(boresight)
uplink_bleed(.6)
shaneao.sauce.laser_tilt = [5.,0]
uplink_angle(-45.)

uplink('on')
uplink_gain(10.)
uplink_open()
uplink('off')


a = peek('a')
a0 = peek('a0')
u = np.matrix(a[1024+52+14:1024+52+14+2].copy()).T
u0 = np.matrix(a0[1024+52+14:1024+52+14+2].copy()).T
uplink_directionCosine * (u-u0) + u0

------------------------------------------
# May 17
#  Testing a moving-average tip/tilt calculation in rtc2

tts_ma = np.zeros((80,80))
tts_ma_alpha = np.array([.99])

shaneao.rtc.rtc2.load('tts_ma',tts_ma.astype(np.double))
shaneao.rtc.rtc2.load('tts_ma_alpha',tts_ma_alpha.astype(np.double))

tts_ma_peek = shaneao.rtc.rtc2.peek('tts_ma')
dimg.show( np.clip( tts_ma_peek.reshape((80,80)), -10,10), 2, origin='lower')

# setting lab flats
wmap0 = fits.readfits('parameterFiles/woofer_map_original.fits').astype(int)
wmap = fits.readfits('parameterFiles/woofer_map.fits').astype(int)
tmap0 = fits.readfits('parameterFiles/tweeter_map_original.fits').astype(int).reshape((1024))
tmap = fits.readfits('parameterFiles/tweeter_map.fits').astype(int).reshape((1024))

woofer_flat = fits.readfits('data/woofer_flat.fits')
tweeter_flat = fits.readfits('data/BestFlat_FullAp_Bias132.fits')
tweeter_sphere = fits.readfits('data/BestFlat_FullAp_Bias132_wPower.fits')

tweeter_flat = np.clip(tweeter_flat,0,1)*65535
u = np.zeros((1024))
u[tmap0] = tweeter_flat.reshape((1024))
a_tweeter_flat = u[tmap].copy()

tweeter_sphere = np.clip(tweeter_sphere,0,1)*65535
u = np.zeros((1024))
u[tmap0] = tweeter_sphere.reshape((1024))
a_tweeter_sphere = u[tmap].copy()

u = np.zeros((52))
u[wmap0] = woofer_flat
a_woofer_flat = u[wmap].copy()

import datetime
import pyfits
home = os environ['HOME']
nt,nw,nf,ntt = (1024,52,14,2)
the_flat = np.zeros((nt+nw+nf+ntt))
the_flat[0:nt] = a_tweeter_flat[0:nt]
the_flat[nt:nt+nw] = a_woofer_flat[0:nw]
the_flat[nt+nw+nf:nt+nw+nf+ntt] = np.array([5,5])
filename = os.path.join(home,'data','Lab_Flat_actuator_defaults_LGS.fits')
hdu = pyfits.PrimaryHDU(the_flat)
hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
hdu.header.append(('DEVICE','DMs'))
hdu.header.append(('FUNC',' default actuator and filter states'))
hdu.header.append(('SYMBOL','a0'))
hdu.header.append(('NAT',nt,' Number of tweeter actuators'))
hdu.header.append(('NAW',nw,' Number of woofer actuators'))
hdu.header.append(('NF',nf,' Number of woofer mode filters'))
hdu.header.append(('NTT',ntt,' Number of uplink tip/tilt states'))
hdu.header.append(('ASTEP','1.4, 1.5',' Algorithm step in RTC Software Definitions doc'))
hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
hdu.header.append(('DDATE','2014-02-24','Date data was taken'))
hdu.header.append(('GENER','toast.py','Generator code'))
protectFile(filename)
hdu.writeto(filename)

--------------------
# May 19
#  Rough estimate of Strehl from
#  May 15 images

# H band (FeII):
import readfits as fits
import numpy as np
a = fits.readfits('m140516_0067.fits')
b = fits.readfits('m140516_0182.fits')
ca = [54,44]
aa = img.circle(a.shape,ca,12)
cab = [42,67]
abg_ap = img.circle(a.shape,cab,5)
abg = np.sum(abg_ap*a)/np.sum(abg_ap)
a_bgr = a - abg

cb = [54,44]
ba = img.circle(b.shape,cb,12)
bbg = np.sum(abg_ap*b)/np.sum(abg_ap)
b_bgr = b - bbg

fluxa = np.sum(a_bgr*aa)
an = a_bgr/fluxa
fluxb = np.sum(b_bgr*ba)
bn = b_bgr/fluxb
-------------------------------

from __future__ import print_function

import matplotlib
#matplotlib.use("WxAgg")
#matplotlib.use("TkAgg")
matplotlib.use("GTKAgg")
#matplotlib.use("Qt4Agg")
#matplotlib.use("CocoaAgg")
#matplotlib.use("MacOSX")
import matplotlib.pyplot as plt
plt.ion()

print("***** TESTING WITH BACKEND: %s"%matplotlib.get_backend() + " *****")

def OnClick(event):
    if event.dblclick:
        print("DBLCLICK", event)
    else:
        print("DOWN    ", event)

def OnRelease(event):
    print("UP      ", event)

def OnClose(event):
	print ("CLOSE  ", event)

fig = plt.gcf()
cid_up = fig.canvas.mpl_connect('button_press_event', OnClick)
cid_down = fig.canvas.mpl_connect('button_release_event', OnRelease)
cid_close = fig.canvas.mpl_connect('close_event',OnClose)

plt.gca().text(0.5, 0.5, "Click on the canvas to test mouse events.",
               ha="center", va="center")

plt.show()

#==========================================
# Nov 28, 2015 - recalibration
#calibrate.calibrate(dm='tweeter',delay=1.0)
fileName = os.path.join(home,'parameterFiles','reconMatrix_16x','archive','H.fits.20151128235925'
#fileName = os.path.join(home,'parameterFiles','reconMatrix_16x','archive','H.fits.20151129001118')
H = fits.readfits(fileName)
fileName = os.path.join(home,'parameterFiles','reconMatrix_16x','H_sim.fits')
H_prev = fits.readfits(fileName)
dimg.show(np.hstack([H,H_prev,H-H_prev]))

#calibrate.calibrate(dm='woofer',delay=1.0)
fileName = os.path.join(home,'parameterFiles','reconMatrix_16x','Hw.fits')
Hw = fits.readfits(fileName)
fileName = os.path.join(home,'parameterFiles','reconMatrix_16x','archive','Hw.fits.20151128220222')
Hw_prev = fits.readfits(fileName)
dimg.show(np.hstack([Hw,Hw_prev,Hw-Hw_prev]))
