"""data analysis of LGS frames on 4/15/2014
"""
from futil import *
sdir = whereis('analyze.py')
#execfile(sdir+'/'+'analyze.py')

import matplotlib
matplotlib.use('TkAgg')
import pylab

import os
import readfits as fits
import numpy as np
import dimg
import matplotlib.pyplot as plt
import __main__

home = os.environ['HOME']
pdir = os.path.join(home,'parameterFiles')

def wgrid(mode,color='red'):
    """mode can be 8 or 16
    """
    
    if (mode == 8):
        umap = fits.readfits(os.path.join(pdir,'u_map_subaps_8x.fits'))
        ns = 40
    if (mode == 16):
        umap = fits.readfits(os.path.join(pdir,'u_map_subaps_16x.fits'))
        ns = 144
    
    ll = []
    for k in range(ns):
        p = umap[k*25]
        ll.append([float(p/160)-0.5,float(p-(p/160)*160)-0.5])
    ll = np.array(ll)

    w = 4
    for p in ll:
        plt.plot([p[0],p[0]+w],[p[1],p[1]],color=color) # bottom
        plt.plot([p[0],p[0]],[p[1],p[1]+w],color=color) # left
        plt.plot([p[0],p[0]+w],[p[1]+w,p[1]+w],color=color) # top
        plt.plot([p[0]+w,p[0]+w],[p[1],p[1]+w],color=color) #right


def init():
    global filelist
    global imset
    global ddir
    
    ddir = os.path.join(home,'data_2014-04-15')
    filelist = []
    for i in range(60):
        filename = 'WFS_Data_'+format(i,'04d')+'.fits'
        filelist.append(filename)
    
    imset = []
    for filename in filelist:
        a = fits.readfits(os.path.join(ddir,filename))
        imset.append(a)

def init_photom():
    """Initialize the photometric data processing of a
    standard star on ShaneAO WFS and TTS
    """
    global filelist, filelist_tt
    global imset, imset_tt
    global skyset, skyset_tt
    global ddir
    
    print '----------- '+__name__+'.init_photom -----------'
    print init_photom.__doc__
    ddir = os.path.join(home,'data_2014-05-12')
    print 'File directory is '+ddir
    print 'files are:'
    filelist = []
    filelist_tt = []
    for i in range(10):
        filename = 'WFS_Data_'+format(i,'04d')+'.fits'
        filename_tt = 'TT_Data_'+format(i,'04d')+'.fits'
        filelist.append(filename)
        filelist_tt.append(filename_tt)
    print filelist[0]+'...'+filelist[-1]
    print filelist_tt[0]+'...'+filelist_tt[-1]
    
    imset = []
    imset_tt = []
    for filename in filelist:
        im = fits.readfits(os.path.join(ddir,filename))
        imset.append(im)
    for filename in filelist_tt:
        im = fits.readfits(os.path.join(ddir,filename))
        imset_tt.append(im)
    print 'Done loading photometry WFS and TTS images'
    print 'Complete this process now with a call to do_photom'
        
zil = np.zeros((160,160))

def do(kind=None,drange=None, grid_color=None):
    """plot data from laser wfs images of 4/15/2014
    usage: do(kind)
    where kind =
        '8x50'
        '8x100'
        '16x50'
        '16x100'
    """
    valid_kinds = ['8x50','8x100','16x50','16x100']
    if (kind == None):
        help(do)
        return
    if (kind not in valid_kinds):
        print '<analyze.do> invalid argument, must be one of '+str(valid_kinds)
        return
    if (kind == '8x50'):
        kset_laser = range(5,10)
        kset_rayleigh = range(10,15)
        kset_sky = range(15,20)
        title = '8x 50 Hz'
    if (kind == '8x100'):
        kset_laser = range(20,25)
        kset_rayleigh = range(25,30)
        kset_sky = range(30,35)
        title = '8x 100 Hz'
    if (kind == '16x50'):
        kset_laser = range(40,45)
        kset_rayleigh = range(35,40)
        kset_sky = range(45,50)
        title = '16x 50 Hz'
    if (kind == '16x100'):
        kset_laser = range(60,65)
        kset_rayleigh = range(55,60)
        kset_sky = range(50,55)
        title = '16x 100 Hz'
    if (kind.startswith('8x')):
        umap = fits.readfits(os.path.join(pdir,'u_map_subaps_8x.fits'))
        ns = 40
    if (kind.startswith('16x')):
        umap = fits.readfits(os.path.join(pdir,'u_map_subaps_16x.fits'))
        ns = 144
    # -------------- laser on ----------------
    kset = kset_laser
    
    wfs = zil.copy()
    for k in kset:
        wfs += imset[k]
    
    wfs /= float(len(kset))
    subs = wfs.flatten()[umap].reshape(ns,5,5)
    intens=[]
    for sub in subs:
        inten = np.sum(sub)
        intens.append(inten)
    
    intens = np.array(intens)
    intens_laser = intens.copy()
    wfs_laser = wfs.copy()
    # -------------- laser on, tuned off wavelength -------------------
    kset = kset_rayleigh
    
    wfs = zil.copy()
    for k in kset:
        wfs += imset[k]
    
    wfs /= float(len(kset))
    subs = wfs.flatten()[umap].reshape(ns,5,5)
    intens=[]
    for sub in subs:
        inten = np.sum(sub)
        intens.append(inten)
    
    intens = np.array(intens)
    intens_detuned = intens.copy()
    wfs_detuned = wfs.copy()
    
    # -------------- sky -------------------
    kset = kset_sky
    
    wfs = zil.copy()
    for k in kset:
        wfs += imset[k]
    
    wfs /= float(len(kset))
    subs = wfs.flatten()[umap].reshape(ns,5,5)
    intens=[]
    for sub in subs:
        inten = np.sum(sub)
        intens.append(inten)
    
    intens = np.array(intens)
    intens_sky = intens.copy()
    wfs_sky = wfs.copy()
    
    #  --------- plots --------
    plt.figure()
    plt.plot(intens_laser-intens_detuned)
    plt.plot(intens_laser-intens_sky)
    plt.plot(intens_detuned-intens_sky)
    plt.title(title)
    plt.xlabel('subap #')
    plt.ylabel('counts')
    if (kind.startswith('8x')):
        plt.xlim(0,40)
        plt.ylim(0,8000)
    if (kind.startswith('16x')):
        plt.xlim(0,144)
        plt.ylim(0,2000)
    plt.grid('on')
    mgr = pylab.get_current_fig_manager()
    mgr.window.wm_geometry('600x500+10+10')
    
    if (drange == None):
        vmin = (wfs_laser-wfs_sky).min()
        vmax = (wfs_laser-wfs_sky).max()
    else:
        vmin = drange[0]
        vmax = drange[1]
    
    if (grid_color == None):
        grid_color = '0.'

    u = wfs_laser-wfs_detuned
    im = dimg.show(u,origin='lower',title=title+' laser-detuned',colorbar=True)
    im.set_clim([vmin,vmax])
    if (kind.startswith('8x')):
        plt.xlim(50,110)
        plt.ylim(50,110)
        wgrid(8,grid_color)
    if (kind.startswith('16x')):
        plt.xlim(40,120)
        plt.ylim(40,120)
        wgrid(16,grid_color)
    mgr = pylab.get_current_fig_manager()
    mgr.window.wm_geometry('500x500+10+500')
    
    u = wfs_laser-wfs_sky
    im = dimg.show(u,origin='lower',title=title+' laser-sky',colorbar=True)
    im.set_clim([vmin,vmax])
    if (kind.startswith('8x')):
        plt.xlim(50,110)
        plt.ylim(50,110)
        wgrid(8,grid_color)
    if (kind.startswith('16x')):
        plt.xlim(40,120)
        plt.ylim(40,120)
        wgrid(16,grid_color)
    mgr = pylab.get_current_fig_manager()
    mgr.window.wm_geometry('500x500+510+500')
    
    u = wfs_detuned-wfs_sky
    im = dimg.show(u,origin='lower',title=title+' Rayleigh',colorbar=True)
    im.set_clim([vmin,vmax])
    if (kind.startswith('8x')):
        plt.xlim(50,110)
        plt.ylim(50,110)
        wgrid(8,grid_color)
    if (kind.startswith('16x')):
        plt.xlim(40,120)
        plt.ylim(40,120)
        wgrid(16,grid_color)
    mgr = pylab.get_current_fig_manager()
    mgr.window.wm_geometry('500x500+1010+500')

import img
def do_photom():
    """Do photometric analysis given images from
    of a photometric star on the Tip/Tilt sensor.
    """
    
    print '---------- '+__name__+'.do_photom -------------'
    print do_photom.__doc__
    
    print 'we are doing this over files '+filelist_tt[0]+'...'+filelist_tt[-1]
    print 'in directory '+ddir
    
    u = np.average(np.array(imset_tt),0)
    n_frames = len(imset_tt)
    c = [36,48]
    print 'with center '+str(c)
    r = (7, 10, 15)
    print 'and photometry radii '+str(r)
    ap0 = img.circle((80,80),c=c,r=r[0])
    ap1 = img.circle((80,80),c=c,r=r[2]) - img.circle((80,80),c=c,r=r[1])
    bg = np.sum(ap1*u)/np.sum(ap1)
    u = u-bg
    cnt = np.sum(u*ap0)
    print 'the average count inside r='+str(r[0])+' = '+('%.2f'%cnt)+' DN'
    bsd = np.sqrt(np.sum((u*ap1)**2)/np.sum(ap1))
    print 'the std dev in the anulus = '+('%.2f'%bsd)+' DN per pixel'
    print 'which is the sky background noise in the image average'
    print 'and means that the sky-noise counts would be'
    print ('%.2f'%(np.sqrt(np.sum(ap0))*bsd))+' DN rms total in the photometry aperture'
    print 'and the photon noise counts from the star would be '+('%.2f'%np.sqrt(cnt))+' DN rms'
    
    print """
    photometric star data downloaded from
    http://www.eso.org/sci/observing/tools/standards/spectra/bd33d2642.html
    """
    stdfile = os.path.join(ddir,'fbd33d2642.dat')
    print 'standard file is '+stdfile
    print 'this is a list of the fluxes in ergs/cm2/s/A x 10^16 versus wavelength'
    f = open(stdfile,'r')
    d = f.read().split('\n')
    f.close()
    lam = []
    f1 = []
    f2 = []
    m = []
    for r in d:
        if (r == ''): continue
        q = r.split()
        lam.append(float(q[0]))
        f1.append(float(q[1])) # ergs/cm2/s/A * 10^16
        f2.append(float(q[2]))
        m.append(float(q[3]))
    h = 6.62606957e-34 # J-s
    h = h*1.e7 # erg-s
    c = 299792458. # m/s
    dtele = 3.
    dsec = 0.8
    Atele = (np.pi*(dtele/2.)**2 - np.pi*(dsec/2.)**2)*1.e4 # cm2
    fs = 40. # Hz
    t = 1./fs
    lam0 = 6250. # Angstroms
    lamf = 9000.
    lm1 = lam0
    nph = 0.
    for l,f in zip(lam,f1):
        if (l > lam0 and l < lamf):
            dlam = l-lm1
            lm1 = l
            nu = c/(l*1e-10)
            nph += (f/1.e16)*Atele*t*dlam / (h*nu)
    cnt_exp = nph
    print 'The total integrated energy in the spectrum'
    print 'from '+str(lam0)+' Angstroms to '+str(lamf)+' Angstroms is '+ \
    ('%.2f'%cnt_exp)+' photons'
    print __name__+'.do_photom returning'
    print '|        item            |  units  |      value         |'
    print '|------------------------|---------|--------------------|'
    print '| star brightness        | DN      | '+('%.2f'%cnt)
    print '| expected from standard | photons | '+('%.2f'%cnt_exp)
    print '| sky std dev after aver | DN/pix  | '+('%.2f'%bsd)
    print '|------------------------|---------|--------------------|'
    print 'This is on average per frame at Texp = '+('%.3f'%t)+' s'
    print 'with average taken over '+str(n_frames)+' frames'
    return cnt,cnt_exp,bsd

init()

print 'my name is '+__name__
if (__name__ != '__main__'):
    __main__.filelist = filelist
    __main__.imset = imset
        
