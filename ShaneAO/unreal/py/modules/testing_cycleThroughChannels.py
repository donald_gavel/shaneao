import sys
import time
import dpio2
foo = dpio2.Output (1, 2048)

value = 16384
ncycles = 0
while (ncycles < 10):
	index = 0
	while index < 1024:
		buffer = [0,]*1024
		buffer[index] = value
		foo.write (buffer)
		time.sleep (0.1)
		index += 1
	ncycles += 1

