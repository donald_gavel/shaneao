#!/usr/bin/python
"""Image sharpening
This module provides a Graphical User Interface (GUI)
for the image-sharpening process
"""

import os
import sys
import Tkinter as Tk
import readfits as fits
import numpy as np
import socket
import struct
import __main__

#--------------
slider_resolution = 0.01
layout = 'horizontal' # or 'vertical'
scale_factor = 0.1
#--------------

home = os.environ['HOME']
paramDir = os.path.join(home,'parameterFiles')
dataDir = os.path.join(home,'data')

if ('mode' in dir(__main__)):
	from __main__ import mode, mems, zero, refcent
else:
	def mode():
	    return '16x'
	def mems(arg):
		print '<mems> '+str(arg)
	def zero():
		print '<zero>'
	def refcent(vec):
		print '<refcent>'

def _init():
	global the_mode, Hw, s_ref_fileName, s_ref, ns2, nmodes
	global mode_names, mode_znums, mode_order
	global root, info, msg, w_set, startFileName, endFile_e, endFile_l, endFile_l0, message_l
	global default_background_color, primed_for_leaving
	global s
	
	the_mode = mode()
	subDir = os.path.join(paramDir,'reconMatrix_'+the_mode)
	Hw = fits.readfits(os.path.join(subDir,'Hw.fits'))
	if (the_mode == '16x'):
		s_ref_fileName = os.path.join(dataDir,'refcents_16x_2014-04-15.fits')
		ns2 = 288
	elif (the_mode == '8x'):
		s_ref_fileName = os.path.join(dataDir,'refcents_8x_2014-04-15.fits')
		ns2 = 80
	s_ref = fits.readfits(s_ref_fileName)
	
	root = Tk.Tk()
	root.wm_title("Image Sharpening")
	nmodes = 12
	
	Hw = Hw[:,2:2+nmodes] # skip tip/tilt
	mode_names = ['focus','astig','astig45','tricoma','tricoma60','coma','coma90',
				  'sphere44-','sphere44+','sphere40','sphere42+','sphere42-']
	mode_znums = ['2,0','2,+2','2,-2','3,+3','3,-3','3,+1','3,-1',
				  '4,-4','4,+4','4,0','4,+2','4,-2']
	mode_order = [2,2,2, 3,3,3,3, 4,4,4,4,4]
	msg = Tk.StringVar()
	frame = Tk.Frame(master=root,width=100,height=100)
	frame2 = Tk.Frame(master=frame,relief=Tk.GROOVE,borderwidth=2,padx=5)
	l2 = Tk.Label(master=frame2,text='- 2nd order -')
	l2.pack(side=Tk.TOP)
	frame3 = Tk.Frame(master=frame,relief=Tk.GROOVE,borderwidth=2,padx=5)
	l3 = Tk.Label(master=frame3,text='- Coma -')
	l3.pack(side=Tk.TOP)
	frame4 = Tk.Frame(master=frame,relief=Tk.GROOVE,borderwidth=2,padx=5)
	l4 = Tk.Label(master=frame4,text='- Spherical -')
	l4.pack(side=Tk.TOP)
	master_set = {'2':frame2,'3':frame3,'4':frame4}
	if (layout == 'horizontal'):
		slider_orientation = Tk.VERTICAL
		pack_side = Tk.LEFT
		slider_length = 300
		from_val = 1
		to_val = -1
	elif (layout == 'vertical'):
		slider_orientation = Tk.HORIZONTAL
		pack_side = Tk.TOP
		slider_length = 500
		from_val = -1
		to_val = 1
		
	w_set = []
	for k in range(nmodes):
		order = mode_order[k]
		the_master = master_set[str(order)]
		w = Tk.Scale(master=the_master, length=slider_length, from_=from_val, to=to_val, resolution=slider_resolution, orient=slider_orientation)
		w.name = mode_names[k]
		w.min_val = -1
		w.max_val = 1
		w.bind("<Key>",key)
		w.bind("<Button-1>",poke)
		w.bind("<Button-2>",poke)
		w.bind("<Enter>",info_say)
		w.bind("<ButtonRelease-1>",_leave)
		w.bind("<ButtonRelease-2>",_leave)
		w_set.append(w)
		w.pack(side=pack_side)
	for order in mode_order:
		master_set[str(order)].pack(side=pack_side)
	frame.pack()
	
	info = Tk.Label(master=root,width=100,text='(zernike mode)')
	info.pack()
	frame_t = Tk.Frame(master=root)
	startFile_l0 = Tk.Label(master=frame_t,text='start with:')
	startFile_l0.pack(side=Tk.LEFT)
	startFileName = Tk.StringVar()
	startFileName.set('(none)')
	startFile_l1 = Tk.Label(master=frame_t, textvariable=startFileName, anchor=Tk.E, width=25, relief=Tk.GROOVE)
	startFile_l1.pack(side=Tk.LEFT)
	message_l = Tk.Label(master=frame_t, textvariable=msg, width=30)
	message_l.pack(side=Tk.LEFT)
	endFile_l0 = Tk.Label(master=frame_t,text='save to:')
	endFile_l0.pack(side=Tk.LEFT)
	endFile_e = Tk.Entry(master=frame_t, justify=Tk.RIGHT)
	endFile_e.pack(side=Tk.LEFT)
	endFile_e.delete(0, Tk.END)
	endFile_e.insert(0, '- name me -')
	default_background_color = endFile_e.cget('bg')
	endFile_l = Tk.Label(master=frame_t, text='.fits')
	endFile_l.pack(side=Tk.LEFT)
	frame_t.pack()
	
	buttonBar = Tk.Frame(master=root)
	zero_button = Tk.Button(master=buttonBar,text='zero',width=10, command=_zero)
	zero_button.pack(side=Tk.LEFT)
	go_button = Tk.Button(master=buttonBar, text='go', width=10, command=_go)
	go_button.pack(side=Tk.LEFT)
	stop_button = Tk.Button(master=buttonBar, text = 'STOP', width=10, command=estop)
	stop_button.pack(side=Tk.LEFT)
	save_button = Tk.Button(master=buttonBar, text = 'save', command=_save)
	save_button.pack(side=Tk.LEFT)
	status_button = Tk.Button(master=buttonBar, text = 'status', command=_status)
	status_button.pack(side=Tk.LEFT)
	button = Tk.Button(master=buttonBar, text='Quit', command=_quit)
	button.pack(side=Tk.LEFT)
	buttonBar.pack(side=Tk.BOTTOM)
	
	primed_for_leaving = False
	
	#host = 'localhost'
	try:
		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.settimeout(4)
		port = 8888
		remote_ip = socket.gethostbyname(host)
		s.connect((remote_ip,port))
		s.settimeout(None)
	except:
		s = None
		print '<imageSharpen._init> failed to connect to rtc socket'
	_status()
	
	#Tk.mainloop()
	#if (s != None): s.close()

def _zero():
	for w in w_set:
		w.set(0.)
	_go()
	msg.set('all sliders set to zero')

def _quit():
    if (verbose): print '<imageSharpen._quit>'
    root.quit()
    root.destroy()

def _go():
	global s_ref_new
	if (verbose): print '<imageSharpen._go>'
	s_slide = np.zeros((ns2))
	sd = ''
	c_list = []
	for k in range(nmodes):
		c = w_set[k].get() * scale_factor
		c_list.append(c)
		sd = sd+struct.pack('1f',c)
	if (verbose): print 'sharpen: '+str(c_list)
	msg.set('sliders sent')
	if (s != None):
		s.sendall('sharpen')
		recv_all(s,2) # acknowledgment 'ok'
		if (verbose): print struct.unpack('12f',sd)
		s.sendall(sd)
		endFile_l0.config(text='*save to')
		_status()

def _status():
	global state
	if (verbose or interactive): print '<imageSharpen._status>'
	if (s != None):
		s.sendall('status')
		r = recv_all(s,8)
		count = struct.unpack('1q',r)[0]
		if (verbose or interactive): print '<status> count = '+str(count)
		state_str = recv_all(s,count)
		if (verbose or interactive): print '<status> '+state_str
		state = eval(state_str)
		mode = state['mode']
		endFile_l.config(text='_'+mode+'.fits')
		try:
			name = state['refCent_filename']
		except:
			name = 'unknown'
		if (verbose or interactive): print '<_status> current refCent file is '+name
		name = os.path.split(name)[-1]
		startFileName.set(name)

def _save():
	endFileName = endFile_e.get()
	bad_chars = [' ','/','\\',':','.']
	for c in bad_chars:
		if (c in endFileName):
			print '<imageSharpen._save> please give a sensible name for the save file (no spaces, slashes, colons, or dots)'
			endFile_e.config(background='yellow')
			msg.set('Enter a valid file name ------->')
			message_l.config(foreground='red')
			raise ValueError('file name invalid')
			return

	endFileName = endFileName+endFile_l.cget('text')
	endFile_e.config(background=default_background_color)
	msg.set(' ')
	message_l.config(foreground='black')

	if (s != None):
		s.sendall('save_sharp')
		recv_all(s,2) # acknowledge 'ok'
		cnt = len(endFileName)
		s.sendall(struct.pack('1q',cnt))
		s.sendall(endFileName)
		msg.set('file saved')
		endFile_l0.config(text='save to')
		for w in w_set:  # zero the sliders. The new file is now the start file, and it has no slider modifications
			w.set(0.)
		_status()

	if (verbose): print '<imageSharpen._save> to '+endFileName

def key(event):
    global primed_for_leaving
    primed_for_leaving = False
    c = event.char
    name = event.widget.name
    r = event.widget.cget('resolution')
    l = event.widget.min_val
    u = event.widget.max_val
    v = event.widget.get()
    if (layout == 'horizontal'):
        up_arrow = '\xef\x9c\x80'
        down_arrow = '\xef\x9c\x81'
    elif (layout == 'vertical'):
        down_arrow = '\xef\x9c\x82'
        up_arrow = '\xef\x9c\x83'
    if (c == up_arrow):
        if (v+r <= u):
            the_msg = name+' increased to '+str(event.widget.get()+r)
        else:
            the_msg = name+' at max of '+str(u)
        msg.set(the_msg)
    elif (c == down_arrow):
        if (v-r >= l):
            the_msg = name+' decreased to '+str(event.widget.get()-r)
        else:
            the_msg = name+' at min of '+str(l)
        msg.set(the_msg)

def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

def poke(event):
    global primed_for_leaving
    the_msg = 'adjusting '+ event.widget.name
    msg.set(the_msg)
    event.widget.focus_set()
    primed_for_leaving = True

def _leave(event):
    global primed_for_leaving
    if (primed_for_leaving):
        v = event.widget.get()
        the_msg = event.widget.name + ' adjusted to '+str(v)
        msg.set(the_msg)
    primed_for_leaving = False

def estop():
	if (verbose): print 'E STOP !'
	s.sendall('e-stop')
	recv_all(s,2) # acknowledgment 'ok'
	msg.set('e-stop')

def info_say(event):
    name = event.widget.name
    info.config(text=name)
    event.widget.focus_set()

helpstr = """
--------------------
USAGE:
    imageSharpen [v1,v2,...,v12] -h -f fileName --host hostname
ARGUMENTS:
    -s [...] is a list of numbers. 12 numbers separated by commas
OPTIONS:
    -f fileName is the file in which to store the new refcents
    --host hostname is optional shaneao host name; default is 'real.ucolick.org'
    -h, --help prints help
--------------------
"""
verbose = False
interactive = False

if (__name__ == '__main__'):
	getting_filename = False
	getting_sliders = False
	getting_host = False
	noneName = '- none -'
	fileName = noneName
	host = 'real.ucolick.org'
	ss = '[0,0,0,0, 0,0,0,0, 0,0,0,0]'
	for arg in sys.argv:
		if (getting_filename):
			fileName = arg
			getting_filename = False
			continue
		elif (getting_sliders):
			if (arg.endswith(']')):
				getting_sliders = False
			ss = ss + arg
			continue
		elif (getting_host):
			host = arg
			getting_host = False
			continue
		if (arg == '-h' or arg == '--help'):
			print helpstr
			exit()
		elif (arg == '-v'):
			verbose = True
			continue
		elif (arg == '--host'):
			getting_host = True
			continue
		elif (arg == '-f'):
			getting_filename = True
			continue
		elif (arg == '-i'):
			interactive = True
			continue
		elif (arg == '-s'):
			getting_sliders = True
			ss = ''
			continue
		elif (arg.startswith('[')):
			getting_sliders = True
			ss = arg.strip('[')
			continue
		
	ss = ss.strip('[]').split(',')
	sa = np.array(ss).astype(np.float)
	
	if (verbose):
		print 'fileName = '+fileName
		print 'ss: '+str(ss)
		print 'sa: '+str(sa)
	
	_init()
	
	for k in range(nmodes):
		w_set[k].set(sa[k])
	
	endFile_e.delete(0,'end')
	endFile_e.insert(0,fileName)
	
	if (len(sys.argv) == 1):
		interactive = True

	if (interactive):
		Tk.mainloop()
	else:
		_go()
		if (fileName != noneName):
			_save()

	if (s != None): s.close()

else:
	interactive = True
	verbose = True
	_init()
	Tk.mainloop()
	if (s != None): s.close()
