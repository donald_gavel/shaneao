
import DFW


class Heartbeat (DFW.Keyword.Integer):
    ''' The saocon service has two heartbeats: one associated with the
        stdiosvc KTL front-end, the other (this keyword) associated
        with the back-end code.
    '''

    def __init__ (self, *args, **kwargs):

        kwargs['initial'] = '0'
        kwargs['period']  = 1

        DFW.Keyword.Integer.__init__ (self, *args, **kwargs)


    def update (self):

        if self.value == None:
            # Not initialized yet.
            return

        value = int (self.value) + 1

        if value > self.maximum:
            value = 0

        self.set (str (value))


# end of class Heartbeat


# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
