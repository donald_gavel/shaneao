"""woofer.py

   Routines in support of the ShaneAO woofer DM

"""

import numpy as np
import os
import sys
import readfits as fits

class woofer:
    """
    :py:class:`woofer` is the woofer deformable mirror class. This is configured specifically for
    ShaneAO, where subapertures on the wavefront sensor are aligned to actuators
    on the DM. See also :py:class:`tweeter` and :py:class:`wfs`.

    The purpose of the woofer class is to read in measured influence
    and previously calculated mode sets for the woofer. It can then
    be used in conjunction with the tweeter class
    to calculate Cwt, the woofer to tweeter mode-overlap matrix.

    See the article
    A Norton et al, 'Performance assessment of competing architectures for real-time woofer-tweeter controllers:
    Simulation and experimental results'
    SPIE PW 2013.pdf
    """

    def __init__(self,mode=1,secondary=False):
        microns = 1.0e-6
        micron = microns
        mm = 1.0e-3

        if isinstance(mode,str):
            mode = wfs.mode_select[mode]

        self.na = 52 #: the number of actuators. Default: 52
        self.name = 'ALPAO DM-52' #: the manufacturer and model number. Default: 'ALPAO DM-52'
        # the file location for zygo data on poke response functions
        self.dataDir = os.path.join(os.environ['HOME'],'data')
        self.respFile = 'wooferInfluenceFunctions.fits'
        self.modeFile = 'wooferModes.fits'
        self.modeCoefFile = 'wooferModeCoefficients.fits'
        self.apFile = 'ap.fits'
	# the aperture as used by the zygo data (a 2d array)
        self.ap = fits.readfits(os.path.join(self.dataDir,self.apFile))
        ap = self.ap
        n,m = ap.shape
        d = 2*np.sqrt(np.sum(ap)/np.pi) # diameter in pixels, assumes a solid circular disk
        self.beamSize = 13.4*mm #: beamSize is the ShaneAO beam diameter on the woofer, in meters. Default: 13.4mm
        self.zygoIllumDiam = 14.3*mm #: the illuminating diameter of the zygo beam on the woofer during data collection, meters. Default: 14.3mm
        self.clearAperture = 15*mm #: clear aperture diameter of the woofer mirror surface, in meters. Default: 15mm
        self.bandwidth = 250. #: specified half-power bandwidth of the woofer, in Hertz. Default 250Hz
        self.da = 2.5*mm #: actuator spacing, in meters. Default: 2.5mm
        self.dx = self.beamSize/d #: the fine grid pixel size, in meters
        self.range = [-1.,1.] #: range of valid command for each actuator, in amperes. Default: [-1,1]
        self.responseGain = -0.51255 #: microns/ampere, straight-line fit to measured deflection of single actuator
        self.secondary = secondary #: has a secondary obscuration, True or False
        #if (self.secondary):
        #    self.dsec = ? # secondary obscuration size
        #else:
        #    self.dsec = 0
        y = np.arange(n)
        x = np.arange(m)
        x,y = np.meshgrid(x,y)
        norm = np.sum(ap)
        cx = np.sum(ap*x)/norm
        cy = np.sum(ap*y)/norm
        self.d = d #: aperture diameter, in pixels on original zygo data
        self.center = (cy,cx) #: aperture center in pixels on original zygo data
        self.modeSet = [] #: the set of woofer modes, sampled on the original zygo data grid
        self.rSet = [] #: the set of actuator response functions, sampled on the original zygo data grid
        self._modes()
        self._aloc()
        self._modeCoefs()

    def type(self):
        """
        Returns the type of the object, which is 'woofer'
        """
        return self.__class__.__name__

    def _modes(self):
        """Gets the modes and influence functions
        from files. Internal. Called at init time
        """
        modes = fits.readfits(os.path.join(self.dataDir,self.modeFile))
        self.modeSet = list(modes)
        r = fits.readfits(os.path.join(self.dataDir,self.respFile))
        self.rSet = list(r)

    def _modeCoefs(self):
        """Read in and set the modeCoefficient set
        or return the set if it has already been read in
        """
        if (not hasattr(self,'modeCoefSet')):
            modes = fits.readfits(os.path.join(self.dataDir,self.modeCoefFile))
            modes = list(modes)
            alocs = self._aloc()
            n = 8
            cset = []
            for mode in modes:
                modeVec = []
                for loc in alocs:
                    x = n/2+loc[0]-.5
                    y = n/2+loc[1]-.5
                    modeVec.append(mode[x,y])
                cset.append(np.array(modeVec).copy())
            self.modeCoefSet = cset
        return self.modeCoefSet

    def woof(self,act_vec):
        """produce the DM shape that would result from the command set a

            acts = 52-element list
            result is on the original zygo grid
        """
        act_vals = list(act_vec)
        n,m = self.rSet[0].shape
        q = np.zeros((n,m))
        for a,r in zip(act_vals,self.rSet):
            q += a*r
        return q

    def _aloc(self):
        if (not hasattr(self,'alocs')):
            x_list = list(np.arange(-3.5,3.5+1))
            y0_list = list(np.array([3,5,7,7,7,7,5,3])/2.)
            xy0_list = zip(x_list,y0_list)
            alocs = []
            k = 0
            for x,y0 in xy0_list:
                y_list = np.arange(-y0,y0+1)
                for y in y_list:
                    alocs.append((x,y))
            self.alocs = alocs
        return self.alocs

    def _showloc(self):
        """depict actuator locations
        """
        alocs = self._aloc()
        n = int(4*max(max(alocs)))
        u = np.zeros((n+1,n+1))
        for x,y in alocs:
            u[n/2+2*x,n/2+2*y] = 1
        return u

    def show(self,a):
        """Display actuator values on the geometry
        of the mirror
        """
        if (len(a) != 52):
            raise ValueError('argument must be a 52-element list')
        mir = np.zeros((8,8))
        alocs = np.array(self.alocs)+3.5
        for v,xy in zip(a,alocs):
            x,y = xy
            mir[7-y,7-x] = v
        return mir



