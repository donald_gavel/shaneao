"""ShaneAO_Calibration:
This module supports the process of determining the AO system
control matrix (see :ref:`param_gen`). Its main job is to create the H matrix, which maps
tweeter modes to wavefront sensor centroid signatures.
"""
print 'name = '+__name__
module_file_name = __name__+'.py'

import __main__
if ('shaneao' in dir(__main__)):
    print 'importing shaneao names'
    from shaneao import *
else:
    print 'DID NOT import shaneao names'

import os
import readfits as fits
import pyfits
import datetime
import time
import numpy as np
# import zernike

home = os.environ['HOME']
paramDir = os.path.join(home,'parameterFiles')

def protectFile(filename,verbose=False):
    """protect the file named filename by moving a copy of it into an archive
    subdirectory of the directory the file is currently in.
    The archived filename is appended with a date-time-stamp to make it unique and recoverable.
    """
    if (os.path.isfile(filename)):
        dirname = os.path.dirname(filename)
        basename = os.path.basename(filename)
        archivedir = os.path.join(dirname,'archive')
        if (not os.path.exists(archivedir)):
            os.mkdir(archivedir)
        protectedName = os.path.join(archivedir,basename)+'.'+time.strftime('%Y-%m-%dT%H:%M:%S')
        if (verbose):
            print '<protectFile> protecting file '+filename + ' to '+protectedName
            sys.stdout.flush()            
        os.rename(filename,protectedName)

global H,Hw

def calibrate(amp=None, bias=None, delay=0.5, dm='tweeter'):
    """Calibrate the system by putting modes on the tweeter
    and recording the centroids. Save the H matrix in a fits file
    
    parameters:
        amp - (float) amplitude of the calibration signal, in the appropriate
            units associated with the DM. Default is 500 for the tweeter,
            0.05 for the woofer
        bias - (float) bias on the mirror around which calibration takes place.
            Default is 21500 for the tweeter, 0 for the woofer.
        delay - (float) delay after each mirror command before measuring
            the response in the wavefront sensor, in seconds.
            In simulator mode, the recommended delay is 1.2 sec.
        dm - (string) either 'tweeter' or 'woofer'
    
    result:
        H or Hw - (matrix) a number of actuator by number of subaperture x 2
            matrix containing the calibration. The matrix is stored
            as a global variable in the calibrate module. Use the
            saveH() or saveHw() routine to save it to disk
    """
    global H,Hw, H_params, Hw_params
    
    if config['simulator']:
        assert delay >= 1.2, 'delay must be >1.2 sec in simulate mode'

    if (dm is None): dm = 'tweeter'
    if (dm == 'tweeter'):
        if (bias is None): bias = 21500
        if (amp is None): amp = 500
    elif (dm == 'woofer'):
        if (bias is None): bias = 0.
        if (amp is None): amp = 0.05
    
    mode = self.mode
    if mode.endswith('LGS'):
        mode = mode[:-3]
    subdir = os.path.join(paramDir,'reconMatrix_'+mode)

    simulate = config['simulator']
    if (dm == 'tweeter'):

        H_params = {'amp': amp,
                    'bias': bias,
                    'delay': delay,
                    'subdir': subdir }
    
        if (simulate):
            __main__.run_script( """
                camstart()
                light('off')
                dark()
                aber('ref')
                light('on')
                bias()
                cent('COG')
                refcent()
                """)
        
        na,naw,nf = 1024,52,14
        if (mode == '16x'): ns = 144
        elif (mode == '8x'): ns = 40
        #flat()
        bleed(1.)
        a = rtc.rtc2.peek('a')
        s = rtc.rtc2.peek('s')
        #a0 = np.zeros((na+naw+nf))
        #a0[0:na] = bias
        H = []
        filename = os.path.join(subdir,'At.fits')
        At = fits.readfits(filename)
        n_tweeter_modes = At.shape[0]
        At = np.matrix(At.reshape((n_tweeter_modes,na)))
        a[0:na] = bias
        time.sleep(delay)
        s0 =  s[0:2*ns].copy()    
        for k in range(n_tweeter_modes):
            print('<calibrate> mode '+str(k)+' of'+str(n_tweeter_modes))
            aa = amp*At[k,:] + bias
            aa = np.clip(aa,0,43000)
            a[0:na] = aa
            time.sleep(delay) # some delay for sauce to respond
            s1 = s[0:2*ns].copy()
            H.append(s1-s0)
        print('<calibrate> done')
        H = np.array(H).transpose()/amp

    if (dm == 'woofer'):

        Hw_params = {'amp': amp,
                    'bias': bias,
                    'delay': delay,
                    'subdir': subdir }
    
        if (simulate):
            __main__.run_script( """
                camstart()
                light('off')
                dark()
                aber('ref')
                light('on')
                bias()
                cent('COG')
                refcent()
                """)
        
        na,naw,nf = 1024,52,14
        if (mode == '16x'): ns = 144
        elif (mode == '8x'): ns = 40
        #flat()
        bleed(1.)
        a = rtc.rtc2.peek('a')
        s = rtc.rtc2.peek('s')
        #a0 = np.zeros((na+naw+nf))
        #a0[0:na] = bias
        Hw = []
        filename = os.path.join(subdir,'Aw.fits')
        Aw = fits.readfits(filename)
        n_woofer_modes = Aw.shape[0]
        Aw = np.matrix(Aw)
        a[na:na+naw] = bias
        time.sleep(delay)
        s0 =  s[0:2*ns].copy()    
        for k in range(n_woofer_modes):
            print('<calibrate> mode '+str(k)+' of'+str(n_woofer_modes))
            aa = amp*Aw[k,:] + bias
            aa = np.clip(aa,-1,1)
            a[na:na+naw] = aa
            time.sleep(delay) # some delay for RTC thread to respond
            s1 = s[0:2*ns].copy()
            Hw.append(s1-s0)
        print('<calibrate> done')
        Hw = np.array(Hw).transpose()/amp
    
    __main__.bias()

def saveH(workDir=None):
    """
    Save the tweeter calibration matrix to disk. Requires that calibrate()
    be run first.  The default output directory is the
    parameterFiles/reconMatrix_[mode] directory.

    parameters:
        workDir - (string) optional name of a working directory to
            put the resulting H and Hw matrices. If the string has a
            leading ~, then it is relative to the home directory,
            otherwise, it is relative to
            the default parameterFiles/reconMatrix_[mode] directory.
    """
    mode = self.mode
    subdir = mode
    if mode.endswith('LGS'):
        subdir = mode[:-3]
    subdir = os.path.join(paramDir,'reconMatrix_'+subdir)
    if workDir is not None:
        if workDir.startswith('~'):
            outDir = os.path.join(home,workDir[1:])
        else:
            outDir = os.path.join(subdir,workDir)
    else:
        outDir = subdir
    filename = os.path.join(outDir,'H.fits')
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')

    ns = H.shape[0]/2
    n_tweeter_modes = H.shape[1]
    hdu = pyfits.PrimaryHDU(H)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','ParamGen',' Parameter Generator'))
    hdu.header.append(('FUNC','InfluenceMat','Modal Influence Matrix'))
    hdu.header.append(('SYMBOL','H'))
    hdu.header.append(('MODE',mode))
    hdu.header.append(('SEC',True,' Secondary obscuration'))
    hdu.header.append(('NS',ns,' # of subapertures'))
    hdu.header.append(('NA',n_tweeter_modes,' # of tweeter modes'))
    hdu.header.append(('AMP',H_params['amp'],'size of calibration signal'))
    hdu.header.append(('BIAS',H_params['bias'],'bias during calibration'))
    hdu.header.append(('DELAY',H_params['delay'],'delay (sec) between measurements'))
    hdu.header.append(('SUBDIR',H_params['subdir'],'initial output directory'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    if (config['simulator']):
        hdu.header.append(('CONTEXT','Simulator','data created with the simulator'))
    else:
        hdu.header.append(('CONTEXT','Real','data created using the live system'))
    protectFile(filename,verbose=True)
    hdu.writeto(filename)
    print '<saveH> writing to '+filename

def saveHw(workDir=None):
    """
    Save the woofer calibration matrix to disk. Requires that calibrate()
    be run first. The default output directory is the
    parameterFiles/reconMatrix_[mode] directory.

    parameters:
        workDir - (string) optional name of a working directory to
            put the resulting Hw matrix. It is relative to
            the default parameterFiles/reconMatrix_[mode] directory.
    """
    mode = self.mode
    subdir = mode
    if mode.endswith('LGS'):
        subdir = mode[:-3]
    subdir = os.path.join(paramDir,'reconMatrix_'+subdir)
    if workDir is not None:
        if workDir.startswith('~'):
            outDir = os.path.join(home,workDir[1:])
        else:
            outDir = os.path.join(subdir,workDir)
    else:
        outDir = subdir
    filename = os.path.join(outDir,'Hw.fits')
    timeStamp = time.strftime('%Y-%m-%dT%H:%M:%S')

    ns = Hw.shape[0]/2
    n_woofer_modes = Hw.shape[1]
    hdu = pyfits.PrimaryHDU(Hw)
    hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
    hdu.header.append(('ITYPE','ParamGen',' Parameter Generator'))
    hdu.header.append(('FUNC','InfluenceMat','Woofer Modal Influence Matrix'))
    hdu.header.append(('SYMBOL','Hw'))
    hdu.header.append(('MODE',mode))
    hdu.header.append(('SEC',True,' Secondary obscuration'))
    hdu.header.append(('NS',ns,' # of subapertures'))
    hdu.header.append(('NA_W',n_woofer_modes,' # of tweeter modes'))
    hdu.header.append(('AMP',Hw_params['amp'],'size of calibration signal'))
    hdu.header.append(('BIAS',Hw_params['bias'],'bias during calibration'))
    hdu.header.append(('DELAY',Hw_params['delay'],'delay (sec) between measurements'))
    hdu.header.append(('SUBDIR',Hw_params['subdir'],'initial output directory'))
    hdu.header.append(('DATE',str(datetime.date.today()),'File generation date'))
    hdu.header.append(('TSTAMP',timeStamp,'File generation time stamp'))
    hdu.header.append(('GENER',module_file_name,'Generator code'))
    if (config['simulator']):
        hdu.header.append(('CONTEXT','Simulator','data created with the simulator'))
    else:
        hdu.header.append(('CONTEXT','Real','data created using the live system'))
    protectFile(filename,verbose=True)
    hdu.writeto(filename)
    print '<saveHw> wrting to '+filename

def calibrate_ResponseCheck(self,sauce=None,amp=10000.,bias=21500.,dt=.5):
    """This is a test to determine the
    time needed for the real-time thread to respond to a state vector
    change made by main thread, the mechanism that is used in the calibrate() function.
    The delay setting in calibrate() must be longer than this response
    time to get consistent results in measuring WFS response.
    
    For the sauce simulator, the response time is about 0.5 seconds.
    I expect that the real system will be much faster, on
    the order of a few hundred microseconds
    """
    na,ns = 1024,144 # assumes 16 across!
    self.open_loop()
    __main__.bleed(1.)
    a = rtc.rtc2.peek('a')
    a[:] = 0.
    s = rtc.rtc2.peek('s')
    if sauce is not None:
        zerns = sauce.zernset(32)
    else:
        zerns = zernike.set(32,21)[1:]
    nz = len(zerns)
    k = 0
    zset = zerns[10:15]
    H = []
    tset = []
    t = 0.
    for z in zset:
        aa = (amp*z+bias).reshape((na)).astype('double')
        aa = np.clip(aa,0,43000)
        a[0:na] = aa
        for k in range(10):
            time.sleep(dt) # some delay for sauce to respond
            H.append( s[0:2*ns].copy() )
            tset.append(t)
            t += dt
    tset = np.array(tset)
    H = np.array(H)
    return tset,H

#------------------------------------------
#  Real-time injection signal
#------------------------------------------

import ctypes
cal_measure_delay_turn_on_enabled = False

def measure_delay(arg):
    """set up the RTCode so that a test signal can be inserted
    on the deformable mirror in real time in closed loop, overriding the
    control loop update and integrator bleeds. This test
    can be used to measure the delay time around the loop, which is a critical parameter
    for a wind-predictive controller. Telemetry data is collected during the test
    where the loop delay is encoded in the phase shift of the WFS response
    with respect to the sinusoidal DM drive signal, as described in:
    
    "Note on finding the loop delay in ShaneAO", Don Gavel, 3/7/2016
    (RTC_Delay.pdf)
    
    testSig is a signal to be inserted on to the tweeter DM. Zeros
    will be inserted on to every other integrator. It is an array of size
    1024 x n_time_steps. The signal is run in a loop until the measure_delay
    test mode is turned off with the 'off' command.
    
    usage:
        measure_delay(testSig)
        measure_delay('on')
        data()
        ... after the telemetry is done:
        measure_delay('off')
    """
    global cal_testSignal, cal_testSignal_data,  cal_measure_delay_turn_on_enabled
    
    librtc2 = ctypes.cdll.LoadLibrary(rtc.rtc2.__file__)
    if isinstance(arg,str): # it is a command
        assert arg in ['start','stop','on','off']
        if arg in ['start','on']:
            assert cal_measure_delay_turn_on_enabled,'<measure_delay> a signal must be defined first'
            c_on = ctypes.c_int(1)
            librtc2['set_injectionState'](c_on)
        if arg in ['stop','off']:
            c_off = ctypes.c_int(0)
            librtc2['set_injectionState'](c_off)
    else: # its the data
        nt,n = arg.shape
        assert n == 1024, '<measure_delay> drive data must have 1024 (#tweeter actuators) stride'
        cal_testSignal = arg.copy()
        cal_testSignal_data = cal_testSignal.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        c_nt = ctypes.c_int(nt)
        librtc2['set_injectionSignal'](cal_testSignal_data,c_nt)
        cal_measure_delay_turn_on_enabled = True

def testSignalGen(mode=6,cycleTime=100,amp=10000.,bias=21500.):
    """generate an interesting test signal for testing the measure_delay scheme
    """
    subdir = os.path.join(home,'parameterFiles','reconMatrix_16x')
    filename = os.path.join(subdir,'At.fits')
    At = fits.readfits(filename)
    sig0 = At[mode,:,:].reshape((1024,))
    amp /= 2. # peak of At
    assert bias+2*amp < 43000,'<testSignalGen> signal above tweeter max limit'
    assert bias-2*amp > 0,'<testSignalGen> signal below tweeter min limit'
    T = cycleTime
    ts = np.arange(0,T)
    sig = []
    for t in ts:
        sig.append(amp*sig0*np.sin(2*np.pi*t/T)+bias)
    return np.array(sig)    

def test():
    """run a typical delay measurement test
    """
    bleed('tweeter',.95)
    bleed('woofer',.99)
    gain(.3)
    sig = testSignalGen()
    measure_delay(sig)
    measure_delay('on')

    