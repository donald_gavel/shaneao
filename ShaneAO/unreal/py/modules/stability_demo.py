#
# stability analysis
#
import matplotlib
matplotlib.use('TkAgg')

import numpy as np
import matplotlib.pyplot as plt
import time

global alpha,gamma_t,gamma_w,beta,g
global s

model = 'new_model'

if (model == 'old_model'):
    alpha = 1.0 # just turn the tweeter LPF off! 0.82
    gamma_t = .82 # instead bleed the tweeter at the woofer response rate
    gamma_w = 1.
    beta = 0.82
    g = 1.

elif (model == 'new_model'):
    alpha = 0.82
    beta = 0.82
    gamma_t = 1.0
    gamma_w = 1.0
    g = 1

def set_rate(f,fd_arg=None):
    global fs,T,alpha,beta,fd,gw_delay
    fs = f
    T = 1./fs
    alpha = beta = np.exp(-fw*T)
    fd = fs/4. # sinusoid disturbance is 1/2 Nyquist
    if (fd_arg != None):
        fd = fd_arg
    gw_delay = gw_delay_nominal*(1000./f) # nominal at 1kHz, scaled by rate
    gw_delay = min(gw_delay,0.3)

ms = 0.001 # milliseconds unit
fs = 1000. # sample frequency, Hz
fw = 200. # woofer bandwidth, Hz
gw_delay_nominal = 0.03 # woofer loop gain during the delay
set_rate(fs)

n_pts = 1000
sigma_n = 0.0 # noise standard deviation
tweeter_range = [-1.,1.] # tweeter saturation
step_amp = 4.
sin_amp = .4
x0 = np.matrix(np.random.normal(0,1,size=(4,))).transpose()
x0 = np.matrix(np.zeros((4,))).transpose()
#x0[0] = 0.5 # set the tweeter state to already be cancelling the step - will the woofer build up to replace it?
on_delay = 500.*ms # delay to allow woofer to settle before turning on tweeter
# states are: tweeter, woofer_LPF, controller_LPF, woofer

s = np.matrix(
    [[gamma_t-g, 0, -1, -g],
    [-g, gamma_w, 0, -g],
    [-g*(1-alpha), 0, alpha, -g*(1-alpha)],
    [0, (1-beta), 0, beta]])

def ev(p=False):
    lam,v = np.linalg.eig(s)
    if (p):
        print 'eigenvectors: '
        for k in range(len(lam)):
            print '-------lam = '+str(lam[k])+'----'
            print v[:,k]
            print ''
    q = abs(lam)
    r = filter(lambda x: x<0.99,q)
    return np.max(np.array(r))

def sim():
    """Simulate the feedback control system using a
    closed-loop system model
    """
    global s
    
    model = 'new_model'
    if (model == 'old_model'):  # has LPF one step ahead of woofer dynamics
        s = np.matrix(
            [[gamma_t-alpha*g, 0, -alpha, -g*alpha],
            [-g,gamma_w,0,-g],
            [-(1-alpha)*g,0,alpha,-(1-alpha)*g],
            [0,(1-beta),0,beta]] )
    elif (model == 'new_model'):  # has LPF identical (in structure) to the woofer dynamics
        s = np.matrix(
            [[gamma_t-g, 0, -1, -g],
            [-g, gamma_w, 0, -g],
            [-g*(1-alpha), 0, alpha, -g*(1-alpha)],
            [0, (1-beta), 0, beta]])
        s_delay = np.matrix(
            [[0, 0, 0, 0],
            [-gw_delay, gamma_w, 0, -gw_delay],
            [0, 0, alpha, 0],
            [0, (1-beta), 0, beta]])
   
    lam, v = np.linalg.eig(s)
    
    d = np.matrix([g,g,g*(1-alpha),0]).transpose()
    d_delay = np.matrix([0,gw_delay,0,0]).transpose()
    
    x_set = []
    d_set = []
    e_set = []
    x = x0
    x[0] = min(max([x[0],tweeter_range[0]]),tweeter_range[1])
    t_set = np.arange(n_pts)*T
    
    for t in t_set:
        phi = step_amp + sin_amp*np.sin(2*np.pi*fd*t)
        noise = np.random.normal(0,1)*sigma_n
        e = phi - (x[0] + x[3])
        if (t >= on_delay):
            x = s*x + d*phi + d*noise
        else:
            x = s_delay*x + d_delay*phi + d_delay*noise
        x[0] = min(max([x[0],tweeter_range[0]]),tweeter_range[1])
        x_set.append(x)
        d_set.append(phi)
        e_plus = phi - (x[0] + x[3])
        e_set.append(e)
    
    simplot(x_set,e_set)
    return (x_set,e_set)

def sim2():
    """Simulate the feedback control system using an
    open-loop system model
    """
    global R
    model = 'new_model'
    if (model == 'new_model'):  # has LPF identical (in structure) to the woofer dynamics
        R = np.matrix(
            [[g, -1.],
            [ g, 0.],
            [g*(1-alpha), -(1.-alpha)]])
        Rw = np.matrix(
            [[0, 0.],
            [gw_delay, 0.],
            [0, 0.]])
    
    x_set = []
    d_set = []
    e_set = []
    x0 = np.matrix(np.zeros((4,1)))
    dx = np.matrix(np.zeros((4,1)))
    x = x0.copy()
    s = np.matrix(np.zeros((2,1)))
    t_set = np.arange(n_pts)*T
    
    for t in t_set:
        phi = step_amp + sin_amp*np.sin(2*np.pi*fd*t)
        noise = np.random.normal(0,1)*sigma_n
        err = phi - (x[0] + x[3])
        s[0] = err + noise
        s[1] = x[2]
        if (t >= on_delay):
            dx[0:3] = R*s
        else:
            dx[0:3] = Rw*s
        dx[3] = (1-beta)*x[1]
        
        x[0] *= gamma_t
        x[1] *= gamma_w
        x[2] *= 1.
        x[3] *= beta
        x = x + dx
        x[0] = min(max([x[0],tweeter_range[0]]),tweeter_range[1])
        
        e_plus = phi - (x[0] + x[3])
        x_set.append(x.copy())
        d_set.append(phi)
        e_set.append(err)
    
    data = np.array(x_set).reshape(n_pts,4)
    ddata = np.array(d_set)
    #err = ddata-(data[:,0]+data[:,3]) # tracking error
    err_sd = simplot(x_set,e_set)
    return (x_set,e_set,err_sd)

def simplot(data,err,v=None):

    if (type(data) == list): data = np.array(data)[:,:,0]
    if (type(err) == list): err = np.array(err)[:,:,0]
    
    plt.ion()
    plt.figure(1)
    plt.clf()
    plt.subplots_adjust(wspace=.4)
    if (v == None):
        plt.subplot(111)
    else:
        plt.subplot(121)
    
    t = np.arange(n_pts)*T*1000 # time in ms
    
    plt.plot(t,data[:,0]) #tweeter
    plt.plot(t,data[:,3]) #woofer
    plt.plot(t,err)
    
    limit = (tweeter_range[0],)*2
    plt.plot((0,max(t)),limit,'--')
    limit = (tweeter_range[1],)*2
    plt.plot((0,max(t)),limit,'--')
    
    err_sd = np.std(err[n_pts/2:n_pts])
    #print '<sim> error std dev = '+str(err_sd)
    
    plt.title('tw (b), woof (g), err (r)')
    plt.xlabel('time, ms')
    plt.grid(True)
    #plt.axis((0,200,-0.4,1.2))
    
    if (v != None):
        plt.subplot(122)
        mdata = np.matrix(data)*v
        for k in range(4):
            plt.plot(mdata[:,k].real)
        
        plt.title('eigen-modes')
        plt.xlabel('time, ms')
    return err_sd

def bode():

    f_set = np.arange(0,1./(2.*T),.01/(2.*T))
    LTz = []
    TFz = []
    for f in f_set:
        zi = np.exp(-2j*np.pi*f*T)
        LT = (1. - zi*(1-beta)/(1-beta*zi) + zi*(1-alpha)/(1-alpha*zi))*(1./(1.-zi))*zi
        TF = 1. / (1.+LT)
        TFz.append(np.abs(TF))
        LTz.append(np.abs(LT))
    plt.figure('bode')
    plt.loglog(f_set,TFz)
    plt.grid()
    return (LTz,TFz)

def gain_scan(sim=sim2):
    """scan through various gain settings, from 0.5 to 1.7
    """
    global g
    g_save = g
    min_ev = 2.
    e_set = []
    gain_set = list(np.arange(0.5,1.7,.1)) + list(np.arange(1.6,1.7,.01))
    
    for g in gain_set:
        u = sim()
        e = np.array(u[1])
        err_sd = np.std(e[n_pts/2:n_pts])
        e_set.append(err_sd)
        print '<gain_scan> gain = '+str(g)+' err_sd = '+str(err_sd)
        plt.draw()
        lam_max = ev()
        #print 'g = '+str(g)+', lam_max = '+str(lam_max)+' sdev err = '+str(e)
        if (lam_max < min_ev):
            min_ev = lam_max
            g_opt = g
        time.sleep(.5)
    #print 'minimum abs eigenvalue of '+str(min_ev)+' at gain = '+str(g_opt)
    g = g_save
    plt.figure('gain scan')
    plt.clf()
    plt.plot(gain_set,e_set,'x-')
    plt.xlabel('gain g')
    plt.ylabel('rms tracking error')
    plt.title('gain scan result')
    plt.grid()

def gamma_scan(sim=sim2):
    """scan through some values of gamma_t, the integrator bleed value
    """
    global gamma_t
    gam_save = gamma_t
    min_ev = 2.
    gam_set = list(np.arange(.78,1.0,.02))
    gam_set += (.985,.99,.995,.999)
    e_set = []
    for gamma_t in gam_set:
        u = sim()
        e = np.array(u[1])
        err_sd = np.std(e[n_pts/2:n_pts])
        e_set.append(err_sd)
        print '<gamma_scan> gamma_t = '+str(gamma_t)+' err_sd = '+str(err_sd)
        plt.draw()
        lam_max = ev()
        #print 'gamma_t = '+str(gamma_t)+', lam_max = '+str(lam_max)+' sdev err = '+str(e)
        if (lam_max < min_ev):
            min_ev = lam_max
            gam_opt = gamma_t
        time.sleep(.25)
    #print 'minimum abs eigenvalue of '+str(min_ev)+' at alpha = '+str(gam_opt)
    gamma_t = gam_save
    plt.figure('gamma scan')
    plt.clf()
    plt.plot(gam_set,e_set,'x-')
    plt.xlabel('gamma')
    plt.ylabel('rms tracking error')
    plt.title('gamma scan result')
    plt.grid()

def alpha_scan(sim=sim2):
    """scan through alpha, the time response of the woofer
    """
    global alpha
    alpha_save = alpha
    min_ev = 2.
    alpha_set = np.arange(.1,.99,.05)
    e_set=[]
    for alpha in alpha_set:
        u = sim()
        e = np.array(u[1])
        err_sd = np.std(e[n_pts/2:n_pts])
        e_set.append(err_sd)
        print '<alpha_scan> alpha = '+str(alpha)+' err_sd = '+str(err_sd)
        plt.draw()
        lam_max = ev()
        #print 'alpha = '+str(alpha)+', lam_max = '+str(lam_max)+' sdev err = '+str(e)
        if (lam_max < min_ev):
            min_ev = lam_max
            alpha_opt = alpha
        time.sleep(.5)
    #print 'minimum abs eigenvalue of '+str(min_ev)+' at alpha = '+str(alpha_opt)
    alpha = alpha_save
    plt.figure('alpha scan')
    plt.clf()
    plt.plot(alpha_set,e_set,'x-')
    plt.xlabel('alpha')
    plt.ylabel('rms tracking error')
    plt.title('alpha scan result')
    plt.grid()

def omega_scan(sim=sim2):
    """scan through omega, the frequency of the disturbance
    """
    global fd
    fd_save = fd
    e_set = []
    omega_set = []
    perset = np.logspace(np.log(2.8)/np.log(10),np.log(500)/np.log(10))*T
    for per in perset:
        fd = 1./per
        omega_set.append(fd)
        u = sim()
        e = np.array(u[1])
        err_sd = np.std(e[n_pts/2:n_pts])/(0.707*sin_amp)
        e_set.append(err_sd)
        print '<omega_scan> omega = '+str(fd)+' err_sd = '+str(err_sd)
        plt.draw()
        time.sleep(.01)
    fd = fd_save
    plt.figure('omega_scan')
    #plt.clf()
    plt.loglog(omega_set,e_set,'x-')
    plt.xlabel('omega')
    plt.ylabel('rms tracking error')
    plt.title('omega scan result')
    plt.grid(True,which='major',ls='-')
    plt.grid(True,which="minor",ls='-',color='orange')

def beta_scan(sim=sim2):
    """scan through beta, the time response of the tweeters high-pass filter
    """
    global beta
    beta_save = beta
    min_ev = 2.
    e_set = []
    beta_set = np.arange(.1,.99,.05)
    for beta in beta_set:
        u = sim()
        e = np.array(u[1])
        err_sd = np.std(e[n_pts/2:n_pts])
        e_set.append(err_sd)
        print '<beta_scan> beta = '+str(beta)+' err_sd = '+str(err_sd)
        plt.draw()
        lam_max = ev()
        #print 'beta = '+str(beta)+', lam_max = '+str(lam_max)+' sdev err = '+str(e)
        if (lam_max < min_ev):
            min_ev = lam_max
            beta_opt = beta
        time.sleep(.5)
    #print 'minimum abs eigenvalue of '+str(min_ev)+' at beta = '+str(beta_opt)
    beta = beta_save
    plt.figure('beta scan')
    plt.clf()
    plt.plot(beta_set,e_set,'x-')
    plt.xlabel('beta')
    plt.ylabel('rms tracking error')
    plt.title('beta scan result')
    plt.grid()
