"""ShaneAO_Calibration:
This module supports the process of determining the AO system
control matrix

To use:

from ShaneAO_Calibration import *
camstart()
A,H = calibrate(shaneao.self,shaneao.sauce)

"""
import sys
import rtc
import numpy as np
import time
import __main__

def calibrate(self=None,sauce=None,amp=500.,bias=21500.,delay=.5):
    """Calibrate the system by putting modes on the tweeter
    and recording the centroids. Return the A and H matrices.
    
    self is the rtc object (it is expected the calibration
    module is to be used after loading the shaneao module)
    
    """
    if (self == None):
        self = __main__.self
    if (sauce == None):
        sauce = __main__.sauce
    #sauce.light('off')
    #__main__.dark()
    #sauce.aber_set = sauce.aber_set_ref
    #sauce.light('on')
    #__main__.refcent()
    na,ns = 1024,144 # assumes 16 across!
    self.open_loop()
    __main__.bleed(1.)
    a = rtc.rtc2.peek('a')
    a[:] = 0.
    s = rtc.rtc2.peek('s')
    zerns = sauce.zernset(32)
    nz = len(zerns)
    A = []
    H = []
    k = 0
    for z in zerns:
        aa = (amp*z+bias).reshape((na)).astype('double')
        aa = np.clip(aa,0,43000)
        a[0:na] = aa
        A.append(a[0:na].copy())
        time.sleep(delay) # some delay for sauce to respond
        H.append(s[0:2*ns].copy())
    A = np.array(A)
    H = np.array(H)
    return A,H

def testDelay(self,sauce,amp=10000.,bias=21500.,dt=.5):
    """testDelay is an internal test to measure the
    time for the plant to respond to a command. The delay
    setting in calibrate() must be longer than this response
    time to get consistent results.
    
    For the sauce simulator, the response time is about 0.5 seconds.
    I expect that the real system will be much faster, on
    the order of a few hundred microseconds
    """
    na,ns = 1024,144 # assumes 16 across!
    self.open_loop()
    __main__.bleed(1.)
    a = rtc.rtc2.peek('a')
    a[:] = 0.
    s = rtc.rtc2.peek('s')
    zerns = sauce.zernset(32)
    nz = len(zerns)
    k = 0
    zset = zerns[10:15]
    H = []
    tset = []
    t = 0.
    for z in zset:
        aa = (amp*z+bias).reshape((na)).astype('double')
        aa = np.clip(aa,0,43000)
        a[0:na] = aa
        for k in range(10):
            time.sleep(dt) # some delay for sauce to respond
            H.append( s[0:2*ns].copy() )
            tset.append(t)
            t += dt
    tset = np.array(tset)
    H = np.array(H)
    return tset,H

import sauce
print '<ShaneAO_Calibration> generating the woofer mode set'
wooferModeSet = sauce.wooferModes(verbose=1)

from img import depiston

def tweeterModes(verbose=0,amp=500,bias=21500.):
    zerns = sauce.zernset(32)
    nz = len(zerns)
    na = 1024
    ap = sauce.ap
    tweeterModeSet = []
    tweeterModeCoefSet = []
    k = 0
    for z in zerns:
        if (verbose):
            print k,
            sys.stdout.flush()
        k += 1
        aa = (amp*z+bias).reshape((na)).astype('double')
        b = sauce.tweeter(aa.reshape((32,32)))
        b = depiston(b,ap)
        tweeterModeSet.append(b)
        tweeterModeCoefSet.append(aa)
    if (verbose): print 'done'
    return tweeterModeSet, tweeterModeCoefSet

def M_tweeter(tweeterModeSet,verbose=0):
    n = len(tweeterModeSet)
    M = np.zeros((n,n))
    for k in range(n):
        if (verbose):
            print k,
            sys.stdout.flush()
        for j in range(k+1):
            M[k,j] = np.sum(tweeterModeSet[k]*tweeterModeSet[j]*sauce.ap)
            M[j,k] = M[k,j]
    if (verbose): print 'done'
    return M
    
print '<ShaneAO_Calibration> generating the tweeter mode set'
tweeterModeSet, tweeterModeCoefSet = tweeterModes(verbose=1)

print '<ShaneAO_Calibration> generating the A matrix'
nt = len(tweeterModeCoefSet)
na = 1024
A = np.array(tweeterModeCoefSet).reshape(nt,na)
print '<ShaneAO_Calibration> generating the M matrix'
M = M_tweeter(tweeterModeSet,verbose=1)
nf = 14 # number of woofer modes to be controlled
print '<ShaneAO_Calibration> generating the Aw matrix'
Aw = sauce.A_woofer(nmodes=nf)
print '<ShaneAO_Calibration> generating the Mw matrix...'
Mw = sauce.M_woofer(wooferModeSet,nmodes=nf,verbose=1)

def C_tw(verbose=0):
    """create the C_tw matrix, which is the cross-integral
    of woofer and tweeter modes
    """
    naw = len(wooferModeSet)
    na = len(tweeterModeSet)
    Ctw = np.zeros((na,nf))
    ap = sauce.ap
    for k in range(na):
        if (verbose):
            print k,
            sys.stdout.flush()
        b = tweeterModeSet[k]
        for j in range(nf):
            bw = wooferModeSet[j]
            Ctw[k,j] = np.sum(b*bw*ap)
    if (verbose): print 'done'
    return Ctw

print '<ShaneAO_Calibration> generating the Ctw matrix'
Ctw = C_tw(verbose=1)

