"""
Health status checks
"""
import os
import sys
import subprocess
import socket
import time
import numpy as np
import ktl

def health():
    # DPIO2 MEMS
    print 'checking DPIO2 MEMS driver'
    check_mems()

    # Galil dispatchers
    # this is a unix command:
    #   gshow -s saomot disp%sta
    # should show all 5 dispatchers working

    # Offloading: Shred
    print 'checking offloading connectin'
    offload_host, offload_port = ('shred.ucolick.org',5700)
    offload_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    offload_sock.connect((offload_host, offload_port))
    offload_sock.settimeout(5)
    offload_sock.sendall('INSTRUMENT AO\n')
    ra, dec = (1.0, 1.0)
    commandX = 'X '+str(ra)+'\n'
    commandY = 'Y '+str(dec)+'\n'
    offload_sock.sendall(commandX)
    offload_sock.sendall(commandY)
    offload_sock.close()

    # Woofer: Musher
    print 'checking woofer client connection to musher'
    host, port = ('musher.ucolick.org',5000)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    seq = 1
    sock.connect((self.host, self.port))
    sock.settimeout(5)
    received = sock.recv(1024)
    print '<woofer_client.woofer_client.__init__> received = '+str(received)
    sock.close()

    # Uplink: Galil RIO
    print 'checking connection to Galil RIO for uplink tip/tilt'
    host, port = ('192.168.2.213',1091)
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
    s.settimeout(4.)
    s.connect((ip,port))
    chan = str(1)
    volts = str(2.0)
    eos = '\r\n'
    pd = 'AO'+chan+','+volts+eos
    s.send(pd)
    u = s.recv(500)
    print u
    s.close()


def check_mems():
    if (thisMachine == RTComputer):
        #import dpio2
        tweeter = foo # dpio2.Output(1,2048)
        tweeter.write([0,]*1024)

        p1 = subprocess.Popen(['dmesg'],stdout=subprocess.PIPE)
        p2 = subprocess.Popen(['tail','-n2'],stdin=p1.stdout,stdout=subprocess.PIPE)
        lines = p2.communicate()[0].split('\n')
        print '*'*50
        print lines[0]
        print lines[1]
        print '*'*50
        if ('Requested' in lines[1]):
            print 'looks good to go'
            mems_ok = True
        elif ('SW-IOMMU' in lines[1]):
            mems_ok = False
            print 'this looks bad, but let me ask...'
            v = raw_input('is the kernel spewing errors? ')
            if (v == 'y'):
                print 'oh thats bad'
                os._exit(1)
        else:
            mems_ok = False
            print 'unexpected message from the kernel, so let me ask...'
            v = raw_input('is the kernel spewing errors? ')
            if (v == 'y'):
                print 'oh thats bad'
                os._exit(1)
        print 'moving on'
    else:
        mems_ok = True

