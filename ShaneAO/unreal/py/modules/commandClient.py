"""commandClient is a test code for sending commands
remotely to :py:mod:`shaneao` via its commandServer
"""
import socket
import threading

host = 'localhost'
port = 8889

def connect():
    global s
    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((host,port))

def drop():
    s.close()

def send(command):
    s.send('rval='+command)
    r = s.recv(1024)
    return r
