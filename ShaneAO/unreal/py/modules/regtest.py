"""=============== shaneao Regression Tests ==============
	"""
file = 'Dropbox/py/modules/test.py'
from shaneao import *

global a,u
def init():
	"""initialize the regression tests
	"""
	global a,u
	# initialization and reference calibration
	camstart()
	sauce.light_on = False
	dark()
	sauce.light_on = True
	sauce.aber_set = sauce.aber_set_ref
	flat()
	refcent()
	a = rtc.rtc2.peek('a')
	u = a[0:1024].reshape((32,32))

__doc__ += 'init: '+init.__doc__

def all():
	"""do all of the regression tests
	"""
	sinz()
	crosspat()
	rowcol()
	sine()
	spiral()
	fpat()

__doc__ += 'all: '+all.__doc__

def sinz():
	"""sinsoid variation of a zernike shape
	"""
	sauce.aber_set = sauce.aber_set_z
	time.sleep(3)
	bleed(.85)
	gain(5000)
	#gain(-5000)
	time.sleep(10)
	flat()
	time.sleep(1)
	gain(1)
	time.sleep(6)
	flat()
	
	sauce.aber_set = sauce.aber_set_ref
	bleed(1)

__doc__ += 'sinz: '+sinz.__doc__

def crosspat():
	""" cross pattern
	"""
	val = 30000
	u[15:17,:] = val
	u[:,15:17] = val
	time.sleep(1)
	flat()
	for i in range(32):
		u[i,i] = val
		u[31-i,i] = val
	
	time.sleep(1)
	flat()
	
	val = 30000
	u[7:9,:] = val
	u[15:17,:] = val
	u[23:25,:] = val
	u[:,7:9] = val
	u[:,15:17] = val
	u[:,23:25] = val
	time.sleep(1)
	flat()
	
	u[15,:] = val
	u[16,:] = 0
	time.sleep(3)
	bleed(.5)

__doc__ += 'crosspat: '+crosspat.__doc__

def rowcol():
	"""row-column test
	"""
	flat()
	val = 30000
	bleed(1)
	bias = 21500
	for k in range(0,32):
		u[k,:] = val
		time.sleep(.25)
		u[k,:] = bias
	
	for k in range(0,32):
		u[:,k] = val
		time.sleep(.25)
		u[:,k] = bias
	
	flat()

__doc__ += 'rowcol: '+rowcol.__doc__

def sine():
	"""spatial sinusoids
	"""
	import numpy as np
	flat
	bleed(1)
	bias =21500
	val = 30000
	k = 2*np.pi*2./32.
	c = 30.
	w = c/k
	x = np.arange(32)
	y = x
	x,y = np.meshgrid(x,y)
	dt = .5
	tf = 5.
	for t in np.arange(0,tf,dt):
		ur = (val-bias)*np.sin(k*x-w*t)+bias
		ur = ur.astype('uint16')
		a[0:1024] = ur.reshape((1024))
		time.sleep(dt)
	
	for t in np.arange(0,tf,dt):
		ur = (val-bias)*np.sin(k*y-w*t)+bias
		ur = ur.astype('uint16')
		a[0:1024] = ur.reshape((1024))
		time.sleep(dt)
	
	for t in np.arange(0,tf,dt):
		ur = (val-bias)*np.sin(k*x+k*y-w*t)+bias
		ur = ur.astype('uint16')
		a[0:1024] = ur.reshape((1024))
		time.sleep(dt)
	
	flat()

__doc__ += 'sine: '+sine.__doc__

def spiral():
	"""spiral
	"""
	sauce.aber_set = sauce.aber_set_ref
	val = 30000
	w = 2.*np.pi / 50.
	bleed(.9)
	for iter in range(2):
		r = 14.
		dr = r/100.
		for t in range(100):
			x = r*np.cos(w*t) + 15.
			y = r*np.sin(w*t) + 15.
			u[x,y] = val
			r -= dr
			time.sleep(.05)

__doc__ += 'spiral: '+spiral.__doc__

def fpat():
	"""F (unique orientation) pattern
	"""
	flat()
	bleed(1)
	ur = u.copy()
	bias = 21500
	val = 30000
	ur[7:25,7:9] = val
	ur[15:17,7:15] = val
	ur[7:9,7:23] = val
	ur = np.flipud(ur)
	a[0:1024] = ur.reshape((1024))
	time.sleep(1)
	
	ur = np.rot90(ur,k=1)
	a[0:1024] = ur.reshape((1024))
	time.sleep(1)
	
	ur = np.rot90(ur,k=1)
	a[0:1024] = ur.reshape((1024))
	time.sleep(1)
	
	ur = np.rot90(ur,k=1)
	a[0:1024] = ur.reshape((1024))
	time.sleep(1)
	
	ur = np.rot90(ur,k=1)
	a[0:1024] = ur.reshape((1024))
	time.sleep(1)
	
	bleed(.9)

__doc__ += 'fpat: '+fpat.__doc__
