#
#  peeko_client.py
#     basic real-time display of the wavefront sensor
#     (and possibly other arrays) from rtc
#
#     client-side version     
#
import socket
import struct
import numpy as np
import readfits

import matplotlib
matplotlib.rcParams['image.interpolation']='nearest'
matplotlib.rcParams['image.cmap']='bone'
from matplotlib.pyplot import *
import thread
import time
import __main__

global rate, peeko_go, s, zoomed, message
rate = 4.
peeko_go = 1
zoomed = 0
message = 'wfs'

print 'Type peeko() to start the display.'
print 'Type crib() to get help on available commands.'

helper = {
    'peeko':'start the peeko display',
    'update':'set the update rate, in seconds',
    'zoom':'set the display to zoomed (2x)',
    'unzoom':'set the display to unzoomed',
    'sethost':'set the host machine',
    'peekat':'tell the client what variable to display',
    'crib':'get quick help'
}

def crib(item=None):
    """crib is a quick help listing:
    crib() lists the whole list of ShaneAO commands
    or crib(item) to get help on a particular item
    """
    bling = '============================'
    if (item == None):
        print bling
        for k in helper.keys():
            print k+' -- '+ helper[k]
        crib(crib)
        print bling
        return
    print item.__doc__

__main__.crib = crib

def peekat(item=None):
    """tell the client to display a particular variable
    peekat('?') returns a list of valid peekable variables
    """
    global message
    allowed = ['wfs','sub','inten','da','a']
    if (item == '?'):
        return allowed
    if (item == None):
        return message
    if (item in allowed):
        message = item
        return 'peeking at '+item
    else:
        return str(item)+ ' needs to be one of '+str(allowed)

__main__.peekat = peekat

global host
host = 'rtc.ucolick.org'
def sethost(ahost=None):
    """set or query the server host machine name
    sethost('?") returns a list of valid server names
    """
    global host
    valid_hosts = ['rtc','rtc.ucolick.org','localhost']
    if (ahost == '?'):
        return valid_hosts
    if (ahost == None):
        return host
    if (ahost == 'rtc'):
        ahost = 'rtc.ucolick.org'
    if (ahost in valid_hosts):
        host = ahost
        print 'host set to '+str(host)
    else:
        print 'invalid host '+str(host)+'; must be one of '+str(valid_hosts)

__main__.sethost = sethost
global fig

def peeko():
    """start the peeko display
    """

    global host, rate, peeko_go, s, zoomed, message, fig
    
    print '<peeko> hit ctrl-c to halt the display'
    
    dt = 1./rate
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port = 8888
    remote_ip = socket.gethostbyname(host)
    s.connect((remote_ip,port))
    
    if (message == "wfs"):
        n = 160
        nbytes = n*n*8
        format = str(n*n)+'d'
    elif (message == "sub"):
        n = 160
        ns = 144
        nsub = 5
        ndata = ns*nsub*nsub
        format = str(ndata)+'d'
        nbytes = 8*ndata
        q = np.zeros((n*n))
        i_map = readfits.readfits('parameterFiles/u_map_subaps_16x.fits')
    elif (message == "inten"):
        n = 160
        ns = 144
        nsub = 5
        ndata = ns
        format = str(ndata)+'d'
        nbytes = 8*ndata
        q = np.zeros((n*n))
        i_map = readfits.readfits('parameterFiles/u_map_subaps_16x.fits')
    elif (message == 'da'):
        n = 32
        ndata = 1090
        format = str(ndata)+'d'
        nbytes = 8*ndata
    elif (message == 'a'):
        n = 32
        ndata = 1090
        format = str(ndata)+'d'
        nbytes = 8*ndata

    #ion()
    fig = figure(1)
    ctr = 10
    try:
        while (peeko_go):
            s.sendall(message)
            b = recv_all(s,nbytes)
            r = struct.unpack(format,b)
            w = np.array(r)
            if (message == "wfs"):
                w = w.reshape((n,n))
                if (zoomed == 0):
                    imshow(w)
                else:
                    imshow(w[40:120,40:120])
            elif (message == "sub"):
                q[i_map] = w
                if (zoomed == 0):
                    imshow(q.reshape((n,n)))
                else:
                    imshow((q.reshape((n,n)))[40:120,40:120])
            elif (message == "inten"):
                for k in range(ns):
                    q[i_map[k*nsub*nsub:(k+1)*nsub*nsub]] = w[k]
                if (zoomed == 0):
                    imshow(q.reshape((n,n)))
                else:
                    imshow((q.reshape((n,n)))[40:120,40:120])
            elif (message == 'da'):
                imshow(w[0:n*n].reshape((n,n)))
            elif (message == 'a'):
                w = w[0:n*n].reshape((n,n))
                w[0,0] = 0
                w[0,31] = 0
                w[31,0] = 43000
                w[31,31] = 43000
                imshow(w)
                
            draw()
            #c = sys.stdin.read(1)
            time.sleep(dt)
            ctr = ctr-1
            if (ctr == 0):
                clf()
                ctr = 10
    except:
        print sys.exc_info()
        print 'peeko exiting'
        #close()
        s.close()
        return
    print 'peeko exiting'
    #close()
    s.close()
    return
    
def recv_all(s,nbytes):
    b = ''
    nrcv = 0
    while (nrcv < nbytes):
        b = b + s.recv(nbytes-nrcv)
        nrcv = len(b)
    return b

__main__.peeko = peeko

def update(p=None):
    """set or query the display update rate
    update(seconds) - set the rate
    update() - print the current rate
    """
    global rate
    if (p == None):
        return rate
    rate = float(p)
    return 'display rate =' + str(rate)

__main__.update = update

def zoom():
    """set the display to zoomed (central pixels, 2x)
    """
    global zoomed
    zoomed = 1
    return 'zoom is on'

__main__.zoom = zoom

def unzoom():
    """set the display to unzoomed
    """
    global zoomed
    zoomed = 0
    return 'zoom is off'

__main__.unzoom = unzoom

