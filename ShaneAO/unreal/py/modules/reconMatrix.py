from numpy import *
from disp import *

ns = 20
na = 40
dx = 1./ns
dy = dx
kx1 = arange(-pi*ns,pi*ns,2*pi)
ky1 = kx1
i = 0+1j
kx = outer(ones(ns),kx1).flatten()
ky = outer(ky1,ones(ns)).flatten()
x1 = outer(ones(ns),arange(-0.5,0.5,dx)).flatten()
x2 = x1 + dx
y1 = outer(arange(-0.5,0.5,dy),ones(ns)).flatten()
y2 = y1 + dy

denom = i*outer(ones_like(x1),kx)
num = exp(i*outer(x2,kx)) - exp(i*outer(x1,kx))
fx_alt = outer((x2-x1),ones_like(kx))
fx = where(denom != 0, num/denom, fx_alt)

denom = i*outer(ones_like(y1),ky)
num = exp(i*outer(y2,ky)) - exp(i*outer(y1,ky))
fy_alt = outer((y2-y1),ones_like(ky))
fy = where(denom != 0, num/denom, fy_alt)

Hx = i*outer(ones_like(x1),kx)*fx*fy
Hy = i*outer(ones_like(y1),ky)*fx*fy

H0 = matrix(vstack((Hx,Hy)))

dxa = 1./na
dya = dxa
x = outer(ones(na),arange(-0.5,0.5,dxa)).flatten()
y = outer(arange(-0.5,0.5,dya),ones(na)).flatten()
Fx = exp(-i*outer(kx,x))
Fy = exp(-i*outer(ky,y))
F = matrix(Fx * Fy)

H = H0 * F
disp(real(H))
R = pinv(real(H),rcond=0.005)
