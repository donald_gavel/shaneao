"""rtc is the real-time code supervisor. It implements the
data and command interface
to the c code in the real-time engine (`rtc2`) and to the socket
client that commands the woofer (`woofer_client`). Typically, there is a layer
of user interface code (`shaneao`) that sits between the human operator and this supervisor.
"""

import __main__

if ('config' in dir(__main__)):
    config = __main__.config
    print '<rtc.py> found config in __main__'
else:
    print '<rtc.py> DID NOT FIND config in __main__.shaneao'
    config = {'simulator': False}

print '<rtc.py> config = '+str(config)

import os
import rtc2
import readfits as fits
import pyfits
from numpy import *
import numpy as np
import time
import woofer_client

if config['simulator']:
    dispatcher = None
else:
    try:
        import rtcKeyword
        dispatcher = rtcKeyword.Dispatcher ()
    except:
        print '<rtc> WARNING: cannot import rtcKeyword - likely because config["simulator"] is False but this is not the real-time machine'
        print 'config["simulator"] = '+str(config['simulator'])+' -->'
        print 'machine = '+os.uname()[1]
        config['simulator'] = True  # proceed with the load anyway in case the module is being imported by the sphinx document generator
        print 'config["simulator"] = '+str(config['simulator'])
        dispatcher = None

home = os.environ['HOME']
paramsDir = os.path.join(home,'parameterFiles')

class rtc:
    """
    RTC means "Real-Time Controller."

    This class implements the
    Python interface to the real-time engine.

    The work flow logic:

        * initialize the supervisor. - it reads in the parameters and puts the rtc in go state, open loop
        * a call to open_loop saves the closed loop gain and opens the loop by setting the gain to zero
        * another call to open_loop while in the open state does not destroy the saved closed loop gain
        * a call to close_loop sets the loop gain to the saved gain. if this gain is not zero, the loop is closed
        * a call to set_gain closes the loop if the gain is non-zero
        * a call to set_gain with zero gain opens the loop, but it does not save the last gain
        * there is a default gain. restore it with a call to set_gain('default').
        * the default gain is viewable as instance variable defaultGain
        * if you want to change the gain without closing the loop, modify savedGain
        * you can also modify the defaultGain

    Example start up and run code::

        u = rtc('16x')
        u.open_loop()
        u.set_gain(10.) # this also closes the loop
        u.set_gain(5.)  # this can be done on the fly

    Example system modification cycle::

        # execute codes to generate and store new matrices (using module wfs, etc.)
        w = wfs('16x')
        w.Hmatrix()
        w.save()
        # load the new parameters into rtc and go
        u.load()
        u.close_loop()

    """

    pdict16x = {
        "controlMatrix" : "controlMatrix_16x.fits",
        "wfs_background" : "wfs_dark.fits",
        "wfs_flat" : "wfs_flat.fits",
        "centWts" : "cent_weights_cog.fits",
        "centReg" : "cent_reg.fits",
        "i_map" : "i_map_subaps_16x.fits",
        "s_ref" : "ref_cent_16x.fits",
        "integrator_bleeds" : "integrator_bleeds.fits",
        "aa_filt" : "aa_filt_alpha.fits",
        "a0" : "actuator_defaults.fits",
        "a_limit" : "actuator_limits.fits",
        "tweeter_map" : "tweeter_map.fits",
        "tt_background" : "tt_dark.fits",
        "tt_flat" : "tt_flat.fits",
        "tt_centWts" : "tt_cent_weights.fits"
    } #: pdictNx, where N = 8, 16, or 30, links the parameter name to the fits file that has it.

    pdict16xLGS = {
        "controlMatrix" : "controlMatrix_16xLGS.fits",
        "wfs_background" : "wfs_dark.fits",
        "wfs_flat" : "wfs_flat.fits",
        "centWts" : "cent_weights_cog.fits",
        "centReg" : "cent_reg.fits",
        "i_map" : "i_map_subaps_16x.fits",
        "s_ref" : "ref_cent_16x.fits",
        "integrator_bleeds" : "integrator_bleeds_LGS.fits",
        "aa_filt" : "aa_filt_alpha.fits",
        "a0" : "actuator_defaults_LGS.fits",
        "a_limit" : "actuator_limits_LGS.fits",
        "tweeter_map" : "tweeter_map.fits",
        "tt_background" : "tt_dark.fits",
        "tt_flat" : "tt_flat.fits",
        "tt_centWts" : "tt_cent_weights.fits"
    }

    pdict8x = {
        "controlMatrix" : "controlMatrix_8x.fits",
        "wfs_background" : "wfs_dark.fits",
        "wfs_flat" : "wfs_flat.fits",
        "centWts" : "cent_weights_cog.fits",
        "centReg" : "cent_reg.fits",
        "i_map" : "i_map_subaps_8x.fits",
        "s_ref" : "ref_cent_8x.fits",
        "integrator_bleeds" : "integrator_bleeds.fits",
        "aa_filt" : "aa_filt_alpha.fits",
        "a0" : "actuator_defaults.fits",
        "a_limit" : "actuator_limits.fits",
        "tweeter_map" : "tweeter_map.fits",
        "tt_background" : "tt_dark.fits",
        "tt_flat" : "tt_flat.fits",
        "tt_centWts" : "tt_cent_weights.fits"
    }

    pdict8xLGS = {
        "controlMatrix" : "controlMatrix_8xLGS.fits",
        "wfs_background" : "wfs_dark.fits",
        "wfs_flat" : "wfs_flat.fits",
        "centWts" : "cent_weights_cog.fits",
        "centReg" : "cent_reg.fits",
        "i_map" : "i_map_subaps_8x.fits",
        "s_ref" : "ref_cent_8x.fits",
        "integrator_bleeds" : "integrator_bleeds_LGS.fits",
        "aa_filt" : "aa_filt_alpha.fits",
        "a0" : "actuator_defaults_LGS.fits",
        "a_limit" : "actuator_limits_LGS.fits",
        "tweeter_map" : "tweeter_map.fits",
        "tt_background" : "tt_dark.fits",
        "tt_flat" : "tt_flat.fits",
        "tt_centWts" : "tt_cent_weights.fits"
    }

    chatty = False
    defaultGain = {"8x" : 0.7, "16x" : 0.7, "30x" : 1.0, "8xLGS" : 0.7, "16xLGS" : 0.7}

    def __init__(self,mode):
        os.chdir(home)
        self.mode = mode
        self.rate = 'unknown'
        self.ttrate = 'unknown'
        self.centroider = 'unknown'
        self.ttcentroider = 'unknown'
        self.defaultGain = rtc.defaultGain[mode]
        self.gain = self.defaultGain  #:  gain is the control loop gain. It multiplies the control matrix just before the control matrix is loaded into rtc2.
        self.savedGain = self.gain
        self.wgain = self.defaultGain
        self.saved_wGain = self.wgain
        self.alpha = 0.
        self.ugain = 0. # laser uplink tip/tilt gain in LGS mode
        self.saved_uGain = self.ugain
        self.tipTilt_rgain = 1.0 # gain of tip/tilt input in LGS mode (relative scaling of wfs tip/tilt vs tts tip/tilt signals. should be near 1.)
        self.wfs_switch = 1. # switch to enable wfs input in LGS mode (intended to be 0 or 1)
        self.loop = 'open'
        self.uplink_loop = 'open'
        #self.ttgain = 0.
        self.saved_ttgain = self.tipTilt_rgain
        self.tt_roi_center = [40,40]
        self.tt_roi_size = [10,10]
        self.tt_algorithm = 'not set'

        if (not config['simulator']):
            dispatcher.setSupervisor (self)

        self.load()

    def vars(self):
        """
        Lists all the instance variables. These include all the loaded rtc parameters.
        """
        return self.__dict__.keys()

    def load(self):
        """
        Load tells the interface to read the controller
        definition files, associated with self.mode,
        into the real-time controller c-extension's memory.

        As a convenience, the definitions are also assigned
        to instance variables within the rtc object as well.
        """

        self.stop()
        exec('pdict = rtc.pdict'+self.mode)
        (self.pdict, self.ddict) = read_params(pdict)
        self.filename_dict = pdict

        # the dirty trick that creates all the instance variables
        # given the dictionaries that come back from read_params
        for key in self.pdict.keys():
            exec("self."+key + "= self.pdict['" + key + "']")
        for key in self.ddict.keys():
            exec("self."+key + "= self.ddict['" + key + "']")

        self.controlMatrix_filename = pdict['controlMatrix']
        self.refCent_filename = pdict['s_ref']
        self.open_loop()

    def set_gain(self, gain=None,wgain=None):
        """
        Set the gain of the real-time controller.
        """
        if gain == 'default':
            self.set_gain(self.defaultGain)
            return

        if gain == None:
            gain = self.gain
        else:
            self.gain = gain

            if wgain == None:
                wgain = gain

        if wgain == None:
            wgain = self.wgain
        else:
            self.wgain = wgain

        if dispatcher != None:
            if gain != None:
                dispatcher['TWEETERGAIN'].set(repr(gain))
            if wgain != None:
                dispatcher['WOOFERGAIN'].set(repr(wgain))

        if self.gain != 0.0 or self.wgain != 0.0:
            self.loop = 'closed'
            if dispatcher != None:
                dispatcher['LOOP'].set('closed')
                dispatcher['LOOPSTATE'].set('closing')
        else:
            self.loop = 'open'
            if dispatcher != None:
                dispatcher['LOOP'].set('open')
                dispatcher['LOOPSTATE'].set('open')

        na, nw, nf, ns, ntts = (self.na_tw, self.na_woof, self.nfilt, self.ns, self.ntts)
        self.cm = self.controlMatrix.copy() #: cm is the control matrix after it has been multiplied by the gain
        self.cm[0:na,:] *= self.gain
        self.cm[na:na+nw,:] *= self.wgain
        self.cm[na+nw:na+nw+nf,:] *= (1.-self.alpha)
        if (self.mode.endswith('LGS')):
            self.cm[na+nw+nf:na+nw+nf+ntts,:] *= self.ugain
            p_tt = 2*ns + nf
            self.cm[:,p_tt:p_tt+2] *= self.tipTilt_rgain
            self.cm[:,0:2*ns] *= self.wfs_switch
        rtc2.load("controlMatrix",self.cm.astype(double))

        if (rtc.chatty):
            print "<set_gain> ",gain
            self.status()

    def tt_only(self,ttgain):
        """Run in LGS mode but with tip/tilt control only
        """
        if (not self.mode.endswith('LGS')):
            raise RuntimeError, 'tt_only() can only be invoked in LGS mode'
        if (ttgain == 0):
            self.saved_ttgain = self.ttgain
        self.ttgain = ttgain

        na, nw, nf, ns, ntts = (self.na_tw, self.na_woof, self.nfilt, self.ns, self.ntts)
        self.cm = self.controlMatrix.copy()
        self.cm[:,0:2*ns] = 0 # everything reading wfs data is zeroed
        self.cm[0:na+nw,:] *= self.ttgain # everything driving the tweeter and woofer is set to the ttgain
        self.cm[na+nw:na+nw+nf,:] *= (1.-self.alpha) # the usual filter transfer rate
        self.cm[na+nw+nf:na+nw+nf+ntts,:] *= 0 # no uplink gain (because not using the laser in tt only mode)
        p_tt = 2*ns + nf
        self.cm[:,p_tt:p_tt+2] *= self.tipTilt_rgain # the relative gain still applies, for consistency
        rtc2.load("controlMatrix",self.cm.astype(double))

        if (rtc.chatty):
            print "<tt_only> ",ttgain
            self.status()

    def load_ngs_matrix(self,cm):
        """This is used in LGS mode to emulate NGS
        operation by loading the LGS control matrix sub-part
        """
        na, nw, nf, ns, ntts = (self.na_tw, self.na_woof, self.nfilt, self.ns, self.ntts)
        self.controlMatrix[0:na+nw+nf,0:2*ns+nf] = cm
        self.controlMatrix[na+nw+nf:na+nw+nf+ntts,:] = 0.
        self.controlMatrix[:,2*ns+nf:2*ns+nf+ntts] = 0.

    def load_lgs_matrix(self):
        filename = self.controlMatrix_filename
        cm = fits.readfits(os.path.join(paramsDir,filename))
        self.controlMatrix = cm

    def set_uplink_gain(self,ugain):
        """Set the laser uplink gain (LGS mode only)
        This does not affect the state of the downlink control loop (if it is open, it stays open)
        """
        if (self.mode.endswith('LGS') == False):
            raise ValueError,'<rtc.set_uplink_gain> cannot set uplink gain in '+self.mode+' mode'
        na, nw, nf, ns, ntts = (self.na_tw, self.na_woof, self.nfilt, self.ns, self.ntts)
        n = na+nw+nf
        self.ugain = ugain
        self.cm[n:n+ntts,:] = self.controlMatrix.copy()[n:n+ntts,:]*self.ugain
        if (self.ugain != 0.):
            self.saved_uGain = self.ugain
            self.uplink_loop = 'closed'
        else:
            self.uplink_loop = 'open'
        rtc2.load("controlMatrix",self.cm.astype(double))

        if (rtc.chatty):
            print "<set_uplink_gain> ",ugain
            self.status()

    def set_centWts(self,algo='COG'):
        """
        Set the WFS centroid weights
        - **algo** can be 'COG' (center-of-gravity), 'QUAD' (quad-cell), or 'BINQUAD' (2x2 binned quad cell)
        weights are always 4x4 with one guard band, to fill 5x5
        """
        if algo != None:
            algo = algo.upper ()

        w = np.zeros((3,5,5))
        if (algo == 'COG'):
            w[0,0:4,0:4] = np.array([[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5]])
            w[1,:,:] = w[0,:,:].transpose()
            w[2,0:4,0:4] = np.ones((4,4))
        elif (algo == 'QUAD'):
            w[0,0:4,0:4] = np.array([[0,0,0,0],[0,-1,1,0],[0,-1,1,0],[0,0,0,0]])*0.5
            w[1,:,:] = w[0,:,:].transpose()
            w[2,1:3,1:3] = np.ones((2,2))
        elif (algo == 'BINQUAD'):
            w[0,0:4,0:4] = np.array([[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1]])*0.5
            w[1,:,:] = w[0,:,:].transpose()
            w[2,0:4,0:4] = np.ones((4,4))
        rtc2.load('centWts',w.reshape((3*5*5)).astype(double))

        self.centroider = algo

        if (rtc.chatty):
            print '<set_centWts> '+algo

        if dispatcher != None:
            dispatcher['WFS_CENTROID'].set(algo)

    def set_tt_centWts(self,roi_center=[45,45],roi_size=[10,10],algo='COG',plateScaleFactor=0.33):
        """
        Set the tip/tilt centroider weights.
        - **roi_center** is the pixel just above and to the right of the cross hair
        bondary of pixels that will define the center to which tip/tilt
        control will aspire to.
        - **roi_size** is the box size over which there will be non-zero weights.
        The roi sizes must be even numbers
        - **algo** can be 'COG' (center-of-gravity) or 'QUAD' (quad-cell)
        The combination of roi_center and roi_size must be inside the 80x80
        tip/tilt array.
        - **plateScaleFactor** is the plate scale of the tip/tilt sensor relative to the plate scale of
        the wavefront sensor. The tip/tilt sensor has ~1/3 arcsec pixels whereas the wfs has
        ~ 1 arcsec pixels, so the plateScaleFactor is 1/3.
        """
        tt_centWts = np.zeros((3,80,80))
        # roi must have an even number of pixels and be completely contained in the 80x80 grid
        roi = np.zeros(roi_size)
        x = 0
        y = 1
        inten = 2
        l, r = roi_center[x]-roi_size[x]/2, roi_center[x]+roi_size[x]/2
        b, t = roi_center[y]-roi_size[y]/2, roi_center[y]+roi_size[y]/2
        mx, my = roi_center
        sx, sy = roi_size
        if (algo == 'QUAD'):
            # quad-cell
            tt_centWts[x,b:t,l:mx] = -0.5*plateScaleFactor
            tt_centWts[x,b:t,mx:r] = +0.5*plateScaleFactor
            tt_centWts[y,b:my,l:r] = -0.5*plateScaleFactor
            tt_centWts[y,my:t,l:r] = +0.5*plateScaleFactor
            tt_centWts[inten,b:t,l:r] = 1
        elif (algo == 'COG'):
            # center of mass
            x1 = np.arange(-sx/2+0.5,sx/2,1)*plateScaleFactor
            y1 = np.arange(-sy/2+0.5,sy/2,1)*plateScaleFactor
            x2, y2 = np.meshgrid(x1,y1)
            tt_centWts[x,b:t,l:r] = x2
            tt_centWts[y,b:t,l:r] = y2
            tt_centWts[inten,b:t,l:r] = 1
        rtc2.load('tt_centWts',tt_centWts.reshape((3*80*80)).astype(double))

        self.ttcentroider = algo
        self.tt_roi_center = roi_center
        self.tt_roi_size = roi_size
        self.tt_algorithm = algo

        if rtc.chatty:
            print '<set_tt_centWts> '+str(roi_center)+', '+str(roi_size)+', '+algo

        if dispatcher != None:
            dispatcher['TT_CENTROID'].set(algo)

    def set_reg(self,reg):
        """Set the regularization values for Hartmann and Tip/Tilt
        centroiders. The theory is, to avoid high effective gains at
        low signal-to-noise, add a number to the denominator, i.e.
            c = (A-B)/(A+B+r)
        Typically this number would be on the order of the standard
        deviation of the intensity noise.

        reg must be a 2 element array: [Hartmann_reg_value, TipTilt_reg_value]
        """
        rtc2.load('centReg',reg.astype(double))
        if rtc.chatty:
            print '<set_reg> centroid regularization set to '+str(reg)

    def close_loop(self):
        """
        Close the AO loop
        """
        #if (self.loop == 'open'):
        if dispatcher != None:
            dispatcher['LOOP'].set('closed')
            dispatcher['LOOPSTATE'].set('closing')
        self.loop = 'closed'
        self.set_gain(self.savedGain,self.saved_wGain)
        self.go()
        if (rtc.chatty):
            print '<close_loop>'
            self.status()

    def open_loop(self):
        """
        Open the AO loop
        """
        if (self.loop == 'closed'):
            self.savedGain = self.gain
            self.saved_wGain = self.wgain
        if dispatcher != None:
            dispatcher['LOOP'].set('open')
            dispatcher['LOOPSTATE'].set('open')
            dispatcher['STWEETERGAIN'].set(repr(self.savedGain))
            dispatcher['SWOOFERGAIN'].set(repr(self.saved_wGain))
        self.loop = 'open'
        self.set_gain(0.)
        self.go()
        if (rtc.chatty):
            print "<open_loop>"
            self.status()

    def close_uplink(self):
        """
        Close the laser uplink tip/tilt loop
        """
        if (self.uplink_loop == 'open'):
            self.set_uplink_gain(self.saved_uGain)
        if (rtc.chatty):
            print '<close_uplink>'
            self.status()

    def open_uplink(self):
        """
        Open the laser uplink tip/tilt loop
        """
        if (self.uplink_loop == 'closed'):
            self.saved_uGain = self.ugain
        self.set_uplink_gain(0.)
        if (rtc.chatty):
            print '<open_uplink>'
            self.status()

    def set_alpha(self,al):
        self.alpha = al
        cm = self.controlMatrix.copy()
        na, nw, nf = (self.na_tw, self.na_woof, self.nfilt)
        cm[0:na,:] *= self.gain
        cm[na:na+nw,:] *= self.wgain
        cm[na+nw:,:] *= (1 - self.alpha)
        self.cm = cm
        rtc2.load("controlMatrix",self.cm.astype(double))

        if (rtc.chatty):
            print "<set_alpha> ",self.alpha
            self.status()

    def stop(self):
        """
        Stop the controller engine (computations halted)
        """
        self.loop = 'open'
        rtc2.set_state(2)
        if (rtc.chatty):
            print "<stop>"
            self.status()

    def go(self):
        """
        Start or resume the controller engine.
        """
        rtc2.set_state(1)
        if (rtc.chatty):
            print "<go>"
            self.status()

    def status(self):
        """
        Report the current AO control system state,
        including running state of the c-exension module,
        and the loop status and gain
        """
        if (rtc.chatty):
            print 'loop = ',self.loop
            print 'gain = ',self.gain
            print 'savedGain = ', self.savedGain
            print 'saved_wGain = ', self.saved_wGain
            print 'alpha = ', self.alpha
            print 'rtc state = ', rtc2.get_state()
            print 'centroider = ',self.centroider
            print 'tt centroider = ',self.ttcentroider

        return {'cent': self.centroider,
                'mode': self.mode,
                'loop' : self.loop,
                'gain' : self.gain,
                'wgain': self.wgain,
                'savedGain' : self.savedGain,
                'saved_wGain' : self.saved_wGain,
                'alpha' : self.alpha,
                'rtc state' : rtc2.get_state(),
                'ttcent': self.ttcentroider}

    # This simulates one step of the real-time control loop, given the current parameters
    def oneStep(self):
        """
        The interface has its own RTC simulator. This
        method runs one step of it. This is handy for
        diagnostics as the rtc engine should produce
        results identical to the simulator.
        """

        # wfs camera (one would use i_map instead of u_map with the real interlaced camera data)
        pix = (self.wfs[self.i_map] - self.wfs_background) * self.wfs_flat

        # centroider
        wx = self.centWts[0,:]
        wy = self.centWts[1,:]
        wi = self.centWts[2,:]
        for k in range(self.ns):
            p = pix[k*25:(k+1)*25]
            x = dot(p,wx)
            y = dot(p,wy)
            i = dot(p,wi) + 1.
            self.inten[k] = i
            self.s[k] = x/i
            self.s[k+self.ns] = y/i
        self.s[0:2*self.ns] -= self.s_ref

        # reconstructor
        self.da = dot(self.cm,self.s)

        # integrator
        a = (self.a - self.a0)*self.integrator_bleeds + self.a0 + self.da
        self.a = clip(a,self.a_limit[0,:],self.a_limit[1,:])

        self.buf[self.tweeter_map] = self.a[0:1024]
        self.woof = self.a[1024:1024+self.na_woof]

    def manyStep(self,nSteps):
        """
        Run the rtc simulator many steps.
        """
        for k in range(nSteps):
            self.oneStep()

    def data(self):
        """
        Take telemetry data
        """
        rtc2.telemetry_data()
        self.telemetry_buffer = rtc2.telemetry_peek()

    def save_data(self,state={}):
        """
        Save the telemetry to disk
        """

        now = time.time()
        now_tuple = time.localtime(now)

        if now_tuple.tm_hour < 12:
            target = now - 86400
            target = time.localtime(target)
        else:
            target = now_tuple

        subdirectory = time.strftime('%Y-%m-%d', target)

        archive_dir = os.path.join(home,'telemetry',subdirectory)
        seq_no = 0

        if os.path.exists(archive_dir):
            existing = os.listdir(archive_dir)

            for filename in existing:
                if (filename.startswith('Data_')):
                    digits = []
                    for character in filename:
                        if character.isdigit():
                            digits.append(character)
    
                    number = ''.join(digits)
                    if number != '':
                        number = int(number)
                        seq_no = max(number, seq_no)


            seq_no += 1

        else:
            os.makedirs(archive_dir)

        tl = time.localtime(time.time())
        day = time.strftime('%Y-%m-%d',tl)
        tim = time.strftime('%H%M%S',tl)
        hdu = pyfits.PrimaryHDU(self.telemetry_buffer)
        hdu.header.append(('SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'))
        hdu.header.append(('DEVICE','WFS+DM'))
        hdu.header.append(('DATE',day,'File generation date'))
        hdu.header.append(('TIME',tim,'file generation time hhmmss'))
        hdu.header.append(('GENER','rtc.save_data'))
        for key in state.keys():
            keyword = key.upper()[0:8].replace(' ','_')
            value = state[key]
            hdu.header.append((keyword,value,key))
        fitsFileName = os.path.join(archive_dir,'Data_'+ '%04d' % seq_no +'.fits')
        print 'writing to ' + fitsFileName
        hdu.writeto(fitsFileName)

        if dispatcher != None:
            dispatcher['RTCFILENUM'].set(str(seq_no))

        time.sleep(.1)


def read_params(p_file_dict):

    #pdir = os.path.join(home,'parameterFiles/')
    pdir = paramsDir + '/'
    p_value_dict = {}
    d_value_dict = {}
    rtc.q = rtc2.peek('controlMatrix')
    for k in p_file_dict.keys():
        a = fits.readfits(pdir+p_file_dict[k])
        if (len(a.shape) == 3):
            a = reshape(a,(a.shape[0],a.shape[1]*a.shape[2]))
        if (k.endswith("_map")):
            rtc2.iload(k,a.astype(int))
        else:
            rtc2.load(k,a.astype(double))
        p_value_dict[k] = a

    filename = pdir+p_file_dict["controlMatrix"]
    hdulist = pyfits.open(filename)
    hdulist.close()

    ns = hdulist[0].header["NS"]
    nfilt = hdulist[0].header["NWM"]
    na = hdulist[0].header["NAXIS2"]
    na_woof = hdulist[0].header["NW"]
    mode = hdulist[0].header["MODE"]
    npix = 160
    na_tw = 1024
    nsub = 5
    ntt = 80
    if (mode.endswith('LGS')):
        ntts = 2
    else:
        ntts = 0
    na_check = na_tw + na_woof + nfilt + ntts
    if (na_check != na):
        print "WARNING na from controlMatrix is not the same as sum of tweeter+woofer+filters+tiptilts"

    size_dict = {'na':na, 'ns':ns, 'npix':npix, 'nsub':nsub, 'nfilt':nfilt, 'na_tw':na_tw, 'na_woof':na_woof, 'ntt':ntt, 'ntts':ntts}
    sizes = (na,ns,npix,nsub,nfilt,na_tw,na_woof,ntt)
    rtc2.set_sizes(sizes)
    rtc2.set_size('ntts',ntts)
    p_value_dict.update(size_dict)

    a = p_value_dict["a0"]
#    a = array((21500,)*na_tw + (0.5,)*na_woof + (0.,)*nfilt)
    rtc2.load("a",a.astype(double))
    d_value_dict["a"] = a

    s = array((0,)*(ns*2+nfilt+ntts)).astype(double)
    rtc2.load("s",s.astype(double))
    d_value_dict["s"] = s

    inten = array((0,)*ns).astype(double)
    rtc2.load("inten",inten.astype(double))
    d_value_dict["inten"] = inten

    if (ntts > 0):
        dtt = array((0,)*ntts).astype(double)
        rtc2.load('dtt',dtt.astype(double))
        d_value_dict['dtt'] = dtt

        tt_inten = array((0,)).astype(double)
        rtc2.load('tt_inten',tt_inten)
        d_value_dict['tt_inten'] = tt_inten

    da = 0*a
    rtc2.load("da",da.astype(double))
    d_value_dict["da"] = da

    #wfs = random.randn(npix,npix)
    wfs = fits.readfits(pdir+"sim_wfs_image.fits").reshape((npix*npix))
    wfs_s = zeros((npix*npix))
    i_map = fits.readfits(pdir+"i_map.fits").reshape((npix*npix))
    wfs_s[i_map] = wfs  # scramble-interlace the simulated data
    rtc2.load("wfs",wfs_s.reshape((npix,npix)).astype(double))
    d_value_dict["wfs"] = reshape(wfs,(npix*npix))

    tts = fits.readfits(pdir+'sim_tt_image.fits').reshape((ntt*ntt))
    rtc2.load('tts',tts.astype(double))
    d_value_dict['tts'] = tts

    # set up camera backgrounds and flats for subap access on an unscrambled map
    #  (assumes the fits data is stored already de-interlaced)

    wfs_subaps = zeros((ns*nsub*nsub))
    rtc2.load("wfs_subaps",wfs_subaps.astype(double))

    u_map = fits.readfits(pdir+"u_map_subaps_"+mode.replace('LGS','')+".fits")
    p_value_dict["u_map"] = u_map

    wfs_background = p_value_dict['wfs_background']
    wfs_background_subs = reshape(wfs_background,(npix*npix))[u_map]
    rtc2.load("wfs_background",wfs_background_subs.astype(double))
    p_value_dict["wfs_background"] = wfs_background_subs

    wfs_flat = p_value_dict["wfs_flat"]
    wfs_flat_subs = reshape(wfs_flat,(npix*npix))[u_map]
    rtc2.load("wfs_flat",wfs_flat_subs.astype(double))
    p_value_dict["wfs_flat_subs"] = wfs_flat_subs

    tweeter_map = p_value_dict["tweeter_map"]
    tweeter_map = reshape(tweeter_map,(na_tw))

    buf = array((0.,)*na_tw)
    woof = array((0.,)*na_woof)
    d_value_dict["buf"] = buf
    d_value_dict["woof"] = woof

    try:
        rtc2.allocate_telemetry()
    except:
        print sys.exc_info()[1]

    return (p_value_dict, d_value_dict)

def woofer_connect(config='real'):
    """initialize the connection from rtc supervisor to woofer.
    Returns the `woofer_client` object it connected to.
    """
    a = rtc2.peek('a')
    aw = a[1024:1024+52]

    wc = woofer_client.woofer_client(aw, config=config)
    return wc


# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
