import sys
import time
import dpio2

foo = dpio2.Output (1, 2048)

maxvalue = 43000
ncycles = 0
buffer = [0,]*1024
index = 0
while (ncycles < 100000):
#	print "cycle ",ncycles
	value = maxvalue
	buffer[index] = value
	foo.write (buffer)
#	time.sleep(0.0001)
	value = 0
	buffer[index] = value
	foo.write (buffer)
#	time.sleep(0.0001)
	ncycles += 1

print "done"
