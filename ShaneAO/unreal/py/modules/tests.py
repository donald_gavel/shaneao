#!/usr/bin/env python
#
# test rtc + sauce
#
"""Test the reconstructor
There are several tests defined: test1, test2, etc.
"""

import sauce
import rtc
import zernike
import img
import matplotlib
matplotlib.rcParams['image.interpolation']='nearest'
matplotlib.rcParams['image.cmap']='bone'
from matplotlib.pyplot import *

global self
self = rtc.rtc('16x')

stroke = 3.5 # microns
lam = 0.589 # microns
dn_per_radian = 65536 / (2*np.pi*(stroke/lam))
radian_per_dn = 1. / dn_per_radian

def refcent():
    a0 = np.zeros((32,32))
    ccd = sauce.wfs(sauce.tweeter(a0))
    mx = ccd.max()
    ccd = ccd*(65000./mx)
    self.wfs = ccd.reshape((160*160))
    self.wfs_background *= 0.
    self.s_ref *= 0.
    self.oneStep()
    self.s_ref = self.s[0:2*self.ns].copy()

def dotest(a,gain=None):
    """
    Given a 32x32 grid in radians, call the sauce simulator
    to generate a Hartmann CCD image, then call the
    rtc oneStep simulator that emulates the real time reconstructor.
    Display the WFS CCD image and the reconstructed delta-actuator array.
    Returns the delta-actuator array.
    """
    fig = figure(2)
    fig.clf()
    
    if (a.shape == (32,)*2):
        ph = sauce.tweeter(a*dn_per_radian)
    elif (a.shape == (sauce.n_fine,)*2):
        ph = a
    else:
        print '<dotest> invalid size argument. Must be (32,32) or '+str((sauce.n_fine,)*2)
        return None
    ccd = sauce.wfs(ph)
    pa1 = fig.add_subplot(121)
    pa2 = fig.add_subplot(122)
    #pa1.imshow(ccd[40:120,40:120])
    pa1.imshow(a)
    self.wfs = ccd.reshape((160*160))
    self.wfs_background *= 0.
    if gain == None:
        self.set_gain(1.0)
    else:
        self.set_gain(gain)
    
    self.oneStep()
    
    da = self.da[0:1024].reshape((32,32))
    da[0,0] = -1.
    da[31,31] = 1.
    #print (da.min(),da.max())
    pa2.imshow(da)
    draw()
    return da

def refcentr():
    a0 = np.zeros((32,32))
    ccd = sauce.wfs(sauce.tweeter(a0))
    mx = ccd.max()
    ccd = ccd*(65000./mx)
    ccd = ccd.astype('uint16')
    wfs_background = self.wfs_background*0.
    s_ref = self.s_ref*0.
    rtc.rtc2.iload('i_map',self.u_map.astype('int'))
    rtc.rtc2.load('s_ref',s_ref.astype('double'))
    rtc.rtc2.load('wfs_background',wfs_background.astype('double'))
    rtc.rtc2.recon(ccd)
    s = rtc.rtc2.peek('s')
    rtc.rtc2.load('s_ref',s.astype('double'))

def dotestr(a,gain=None):
    """
    Given a 32x32 grid in radians, call the sauce simulator
    to generate a Hartmann CCD image, then call the
    rtc2 real time reconstructor.
    Display the WFS CCD image and the reconstructed delta-actuator array.
    Returns the delta-actuator array.
    """
    fig = figure(1)
    fig.clf()
    
    ccd = sauce.wfs(sauce.tweeter(a*dn_per_radian))
    pa1 = fig.add_subplot(121)
    pa2 = fig.add_subplot(122)
    pa1.imshow(ccd[40:120,40:120])
    mx = ccd.max()
    ccd = ccd*(65000./mx)
    ccd = ccd.astype('uint16')
    if gain == None:
        self.set_gain(1.0)
    else:
        self.set_gain(gain)
    rtc.rtc2.recon(ccd)
    da = rtc.rtc2.peek('da')
    da2 = da[0:1024].reshape((32,32))
    da2[0,0] = -1.
    da2[31,31] = 1.
    pa2.imshow(da2)
    draw()
    return da2

def doscreentest():
    fig2 = figure(2,figsize=(12,12))
    fig2.show()
    t0 = 0
    dt = 4
    tf = 100
    per = 10.
    for t in arange(t0,tf+1,dt):
        fig2.clf()
        s = sauce.ss[:,t:t+n_fine]
        ccd = sauce.wfs(s)
        pa1 = fig2.add_subplot(221)
        pa1.imshow(ccd[40:120,40:120])
        self.wfs = ccd.reshape((160*160))
        self.wfs_background *= 0.
        self.set_gain(1.0)
        self.oneStep()
        da = self.da[0:1024].reshape((32,32))
        pa2 = fig2.add_subplot(222)
        pa2.imshow(da)
        pa3 = fig2.add_subplot(224)
        pa3.imshow(s*ap)
        draw()

def dofreqtest():
    fig = figure(1,figsize=(12,6))
    fset = arange(1,4,.5)
    x = arange(32)/16.-1.
    x,y = meshgrid(x,x)
    for f in fset:
        clf()
        a = cos(2*pi*f*(x+y))
        dotest(a,fig)
        draw()
        a = cos(2*pi*f*(x))
        dotest(a,fig)
        draw()
        a = cos(2*pi*f*(x-y))
        dotest(a,fig)
        draw()
        a = cos(2*pi*f*(y))
        dotest(a,fig)
        draw()

def dozerntest():
    fig = figure(1,figsize=(12,6))
    r,th = zernike.polar_array(Rmax=16.-.55,DS=1,pr=16.)
    nmset = [(1,1),(1,-1),
        (2,0),(2,2),(2,-2),
        (3,1),(3,-1),(3,3),(3,-3),
        (4,0),(4,2),(4,-2),(4,4),(4,-4),
        (5,1),(5,-1),(5,3),(5,-3),(5,5),(5,-5)]
    
    for n,m in nmset:
        clf()
        a = zernike.zernike(n,m,r,th,limit=False)
        dotest(a,fig)
        draw()

def dosmiley():
    ph = readfits('Dropbox/smiley.fits')
    ccd = sauce.wfs(ph*3.)
    self.wfs = ccd.reshape((160*160))
    self.wfs_background *= 0.
    self.set_gain(1.0)
    self.oneStep()
    da = self.da[0:1024].reshape((32,32))
    fig3 = figure(3,figsize=(12,6))
    show()
    pa1 = fig3.add_subplot(121)
    pa1.imshow(ccd[40:120,40:120])
    pa2 = fig3.add_subplot(122)
    pa2.imshow(da)
    draw()    

def test1():
    """
    This proves that the real-time reconstructor (rtc2.recon)
    matches the python reconstructor (rtc.oneStep)
    in response to the same Hartmann sensor images.
   
    SH images are generated by sauce, given zernike
    shapes on the simulated tweeter.
    """

    global self
    ion()
    #self = rtc.rtc('16x')
    s_ref_peek = rtc.rtc2.peek('s_ref')
    s_peek = rtc.rtc2.peek('s')
    inten_peek = rtc.rtc2.peek('inten')
    da_peek = rtc.rtc2.peek('da')
    
    refcent()
    refcentr()
    
    zset = sauce.zernset(32)
    for z in zset:
        dotest(z)
        draw()
        dotestr(z)
        draw()

def test2():
    """
    This proves that the real-time reconstructor (rtc2.recon)
    matches the python reconstructor (rtc.oneStep)
    in response to the same Hartmann sensor images.
   
    SH images are generated by sauce, given one zernike shape
    on the simulated tweeter, varied in amplitude sinusoidally.
    """
    global self
    ion()
    #self = rtc.rtc('16x')
    s_ref_peek = rtc.rtc2.peek('s_ref')
    s_peek = rtc.rtc2.peek('s')
    inten_peek = rtc.rtc2.peek('inten')
    da_peek = rtc.rtc2.peek('da')
    
    refcent()
    refcentr()

    zset = sauce.zernset(32)
    ztset = sauce.gen_t_set(zset[2])
    for z in ztset:
        dotest(z)
        draw()
        dotestr(z)
        draw()

# ----- formatters for graphics display
global zz
def format_coord(xx, yy):
    global zz

    numrows, numcols = z.shape
    if (extent == None):
        col = int(xx+0.5)
        row = int(yy+0.5)
    else:
        col = int((xx-x[0])*numcols/(x[numcols-1]-x[0]))
        row = int((yy-y[numrows-1])*numrows/(y[0]-y[numrows-1]))
    if col>=0 and col<numcols and row>=0 and row<numrows:
        return '[ row %1.0f, col %1.0f ]  z=%1.4f'%(row,col, zz[row,col])
    else:
        return 'x=%1.4f, y=%1.4f'%(xx, yy)

# ------ end formaters

def test3():
    global self

    extent = None
    
    ion()
    fig1 = figure(1)
    fig1.show()
    pa1 = fig1.add_subplot(2,2,1)
    title('da')
    pa2 = fig1.add_subplot(2,2,2)
    title('a')
    pa3 = fig1.add_subplot(2,2,3)
    title('CCD')
    pa4 = fig1.add_subplot(2,2,4)
    title('aber')
    #self = rtc.rtc('16x')
    s_ref_peek = rtc.rtc2.peek('s_ref')
    s_peek = rtc.rtc2.peek('s')
    inten_peek = rtc.rtc2.peek('inten')
    da_peek = rtc.rtc2.peek('da')
    a_peek = rtc.rtc2.peek('a')
    
    refcent()
    refcentr()

    zset = sauce.zernset(32)
    #ztset = sauce.gen_t_set(zset[5],period=100.)
    atset = sauce.gen_atmos_t_set() # atmospheric aberration set
    #aa = ztset[7]
    #pha = sauce.tweeter(aa*dn_per_radian)  # the aberration input, in radians of phase
    
    self.set_gain(-10000.0) # 11000 just breaks into oscillation
    
    tmax = 25
    peg = 0
    ccdset = []
    pheset = []
    aset = []
    phaset = []
    rms_record = []
    rms_phase_record = []
    for t in range(tmax):
        
        #---- get the aberration at this time ------
        #k = t%(len(ztset)) # time-varying aberation
        #aa = ztset[k]
        #pha = sauce.tweeter(aa*dn_per_radian)
        k = t%(len(atset))
        pha = atset[k]/2. #bring it closer to the range of the MEMS
        pha = img.detilt(img.depiston(pha,sauce.ap),sauce.ap)
        
        #---- AO optics and control loop ----
        a = a_peek[0:1024].copy().reshape((32,32))
        da = da_peek[0:1024].copy().reshape((32,32))
        phdm = sauce.tweeter(a)  # emulate the tweeter
        phe = pha + phdm
        ccd = sauce.wfs(phe) # emulate the wavefront sensor        
        mx = ccd.max()
        ccd = ccd*(65000./mx)
        ccd = ccd.astype('uint16')
        rtc.rtc2.recon(ccd) # real-time controller
        
        #---- record data ----
        print t
        phe = img.depiston(phe,sauce.ap)
        ccd_zoom = ccd[40:120,40:120]
        ccdset.append(ccd_zoom)
        aset.append(a)
        pheset.append(phe)
        phaset.append(pha)
        rms_record.append(img.rms(phe,sauce.ap))
        rms_phase_record.append(img.rms(pha,sauce.ap))
        
        #---- display data ----
        peg = max(peg,da.max())
        peg = 1000.
        da[0,0] = -peg
        da[31,31] = peg
        if (t%10 == 0):
            pa1=subplot(221); cla(); title('phe')
            pa2=subplot(222); cla(); title('a')
            pa3=subplot(223); cla(); title('CCD')
            pa4=subplot(224); cla(); title('aber')
        pa1.imshow(phe,extent=None); #pa1.format_coord = format_coord
        a[0,0] = 0.
        a[31,31] = 65535
        pa2.imshow(a,extent=None); #pa2.format_coord = format_coord
        pa3.imshow(ccd_zoom,extent=None); #pa3.format_coord = format_coord
        pha[0,0] = -10.
        pha[511,511] = 10.
        pa4.imshow(pha,extent=None); #pa4.format_coord = format_coord
        draw()
    
    # ---- display summary plots ---
    figure(2)
    subplots_adjust(wspace=.5)
    subplot(1,2,1)
    plot(rms_record)
    plot(rms_phase_record)
    yrange = list(axis())
    yrange[2] = 0
    axis(yrange)
    title('RMS WF error')
    xlabel('time, ms')
    ylabel('radians')
    grid('on')
    yrange[3] = yrange[3]*589./(2*np.pi)
    twinx()
    axis(yrange)
    ylabel('nm')
    
    sta = []
    st = []
    for p in rms_phase_record:
        sta.append(np.exp(-p**2))
    for p in rms_record:
        st.append(np.exp(-p**2))
    
    subplot(1,2,2)
    plot(st)
    plot(sta)
    yrange = list(axis())
    axis((yrange[0],yrange[1],0,1))
    title('Strehl')
    xlabel('time, ms')
    grid('on')
    
    return (ccdset,aset,pheset,phaset,rms_record,rms_phase_record)

def playback(u,clear=False):
    """Play back the simulation sequence that was recorded during
    test3.
    """
    (ccdset,aset,pheset,phaset,rms_record,rms_phase_record) = u
    tmax = len(u[0])
    tset = range(tmax)

    # calculate Strehls give phase errors
    sta = []
    st = []
    for p in rms_phase_record:
        sta.append(np.exp(-p**2))
    for p in rms_record:
        st.append(np.exp(-p**2))    
    
    ion()
    
    fig1 = figure(1)
    fig1.show()
    pa1 = fig1.add_subplot(2,2,1)
    title('da')
    pa2 = fig1.add_subplot(2,2,2)
    title('a')
    pa3 = fig1.add_subplot(2,2,3)
    title('CCD')
    pa4 = fig1.add_subplot(2,2,4)
    title('aber')
    
    figure(2)
    subplots_adjust(wspace=.5)
    subplot(1,2,1)
    if (clear):
        cla()
    plot(tset,rms_record,'wx')
    plot(tset,rms_record,'w-')
    axis((0,tmax,0,max(rms_record)))
    title('RMS WF error')
    ylabel('radians')
    xlabel('time, ms')
    grid('on')
    
    subplot(1,2,2)
    if (clear):
        cla()
    plot(tset,st,'wx')
    plot(tset,st,'w-')
    axis((0,tmax,0,1))
    title('Strehl')
    xlabel('time, ms')
    grid('on')
 
    for t in tset:
        ccd,a,phe,pha = ccdset[t],aset[t],pheset[t],phaset[t]
        ccd_zoom = ccd
        
        figure(1)
        if (t%10 == 0):
            pa1=subplot(221); cla(); title('phe')
            pa2=subplot(222); cla(); title('a')
            pa3=subplot(223); cla(); title('CCD')
            pa4=subplot(224); cla(); title('aber')
        
        pa1.imshow(phe,extent=None); #pa1.format_coord = format_coord_phe
        a[0,0] = 0.
        a[31,31] = 65535
        pa2.imshow(a,extent=None); #pa2.format_coord = format_coord_a
        pa3.imshow(ccd_zoom,extent=None); #pa3.format_coord = format_coord_ccd
        pha[0,0] = -10.
        pha[511,511] = 10.
        pa4.imshow(pha,extent=None); #pa4.format_coord = format_coord_pha
        draw()
        
        figure(2)
        subplot(121)
        plot([t],[rms_record[t]],'bx')
        if t != 0:
            plot(tset[t-1:t+1],rms_record[t-1:t+1],'b-')

        subplot(122)
        plot([t],[st[t]],'bx')
        if t != 0:
            plot(tset[t-1:t+1],st[t-1:t+1],'b-')
        
        draw()
    
    return

ion()
fig1 = figure(1)
fig1.canvas.manager.set_window_title('phase screens')
fig2 = figure(2)
fig2.canvas.manager.set_window_title('error plots')
