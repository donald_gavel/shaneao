''' Define the KTL keyword interface used to map sys.stderr,
    sys.stdin, and sys.stdout for the shaneao Python interpreter.
'''

import DFW
import os
import sys

import shaneaoHeartbeat


executor = None

class InputKeyword (DFW.Keyword.String):

    def write (self, value):

        DFW.Keyword.String.write (self, value)

        if value != '' and executor != None:
            executor (value)


    def set (self, value, force=False):

        DFW.Keyword.String.set (self, value, force)

        # Upon entering a command, people expect it to
        # disappear from the command window and be replaced
        # by the empty string.

        if value != '':
            self.set ('', force)


# end of class InputKeyword



class Output:

    stderr = None
    stdout = None

    def __init__ (self, type, keyword):

        if getattr (Output, type) != None:
            raise RuntimeError, "there can only be one %s" % (type)


        self.keyword = Service[keyword]
        self.original = getattr (sys, type)
        self.incomplete = None

        setattr (sys, type, self)
        setattr (Output, type, self)


        for attribute in dir (self.original):

            if hasattr (self, attribute):
                pass
            else:
                setattr (self, attribute, getattr (self.original, attribute))


    def write (self, output):

        if output == '':
            return


        # Always send the full output to the original file, regardless
        # of whether the line is complete.

        self.original.write (output)


        incomplete = self.incomplete

        if incomplete != None:
            output = incomplete + output

        if '\n' in output:

            lines = output.split ('\n')

            if lines[-1]== '':
                # That's a trailing newline.
                self.incomplete = None
                lines.pop ()

            else:
                self.incomplete = lines.pop ()


            keyword = self.keyword

            for line in lines:
                try:
                    keyword.set (line)
                except:
                    pass

        else:
            self.incomplete = output


# end of class Output



def setupKeywords (service):

    InputKeyword ('STDIN', service, initial='')
    DFW.Keyword.String ('STDOUT', service, initial='')
    DFW.Keyword.String ('STDERR', service, initial='')

    shaneaoHeartbeat.Heartbeat ('DISP3HRT', service)


lroot = os.environ['LROOT']
stdiosvc = os.path.join (lroot, 'data/saocon/stdiosvc_3.conf')
Service = DFW.Service('saocon', stdiosvc, setupKeywords, '+service+_dispatch_3')

output = Output ('stdout', 'STDOUT')
error  = Output ('stderr', 'STDERR')



# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
