''' Define the KTL infrastructure used by rtc.py. This is broken out
    as a separate file in order to simplify the "if simulation..." logic
    in rtc.py, and to avoid cluttering up the same.
'''

import DFW
import os
import time
import WeakRef

import shaneaoHeartbeat


class Dispatcher:

    running = None

    def __init__ (self):

        lroot = os.environ['LROOT']
        stdiosvc = os.path.join (lroot, 'data/saocon/stdiosvc_1.conf')
        self.service = DFW.Service ('saocon', stdiosvc, self.setupKeywords, '+service+_dispatch_1')

        Dispatcher.running = WeakRef.WeakRef (self)


    def __del__ (self):

        Dispatcher.running = None


    def __getitem__ (self, key):

        return self.service[key]


    def setSupervisor (self, supervisor):

        self.supervisor = WeakRef.WeakRef (supervisor)

        for keyword in self.service:

            try:
                keyword.setSupervisor (supervisor)
            except AttributeError:
                pass


    def setupKeywords (self, service):

        shaneaoHeartbeat.Heartbeat ('DISP1HRT', service)

        Centroider ('TT_CENTROID',  service, 'set_tt_centWts')
        Centroider ('WFS_CENTROID', service, 'set_centWts')

        DFW.Keyword.Integer ('LOOPSETTLEDELAY', service, initial=1)

        Loop (service)
        LoopState (service)

        TweeterGain (service)
        WooferGain (service)

        Gain ('STWEETERGAIN', service, 'savedGain')
        Gain ('SWOOFERGAIN', service, 'saved_wGain')

        SmartGain (service)


# end of class Dispatcher



def getDispatcher ():

    dispatcher = Dispatcher.running

    if dispatcher == None:
        raise ValueError, 'saocon dispatcher is not running'

    dispatcher = dispatcher ()
    return dispatcher



def getSupervisor ():

        dispatcher = getDispatcher ()

        try:
            supervisor = dispatcher.supervisor
        except AttributeError:
            supervisor = None

        if supervisor != None:
            supervisor = supervisor ()

        if supervisor == None:
            raise ValueError, 'rtc supervisor is not running'

        return supervisor



class Centroider (DFW.Keyword.Enumerated):

    def __init__ (self, name, service, setCentroider):

        self.setCentroider = setCentroider
        initial = 'unknown'

        DFW.Keyword.Enumerated.__init__ (self, name, service, initial=initial)


    def set (self, value, force=False):

        try:
            mapped = self.mapping[value]
        except KeyError:
            value = self.mapping['unknown']
        else:
            # Ellie is used to 'CM' (center of mass), Don's code uses
            # 'COG' (center of gravity). Map the former to the latter.

            if mapped.lower () == 'cm':
                value = self.mapping['cog']

        # The value out of the mapping is a unicode string; cast it
        # to a regular string for the benefit of naive parsers down
        # the line.

        value = str (value)

        DFW.Keyword.Enumerated.set (self, value, force)


    def prewrite (self, value):

        result = DFW.Keyword.Enumerated.prewrite (self, value)

        value = self.mapping[value]
        value = value.lower ()

        if value == 'unknown':
            raise ValueError, "'Unknown' is not a valid centroider algorithm"

        return result


    def write (self, value):

        value = self.mapping[value]
        supervisor = getSupervisor ()

        setCentroider = getattr (supervisor, self.setCentroider)
        setCentroider (algo=value)


# end of class Centroider



class Loop (DFW.Keyword.Enumerated):

    def __init__ (self, service):

        self.supervisor = None
        DFW.Keyword.Enumerated.__init__ (self, 'LOOP', service)


    def setSupervisor (self, supervisor):

        mode = supervisor.loop
        self.set (mode)


    def write (self, value):

        value = self.mapping[value].lower ()
        supervisor = getSupervisor ()

        if value == 'closed':
            supervisor.close_loop ()
        else:
            supervisor.open_loop ()


# end of class Loop



class LoopState (DFW.Keyword.Enumerated):

    def __init__ (self, service):

        # On the one second delay, Don says:
        #
        #   One doesn't -really- know when the loop state has settled,
        #   since it depends on gain and frame rate settings. If
        #   everything is set properly, I would think the state would
        #   settle within 10 to 20 iterations of the control loop, or
        #   about 10-20 ms at 1000hz frame rate, or 400-800ms at 50hz
        #   frame rate. So at most, about one second after issuing the
        #   commands, you can assume the loops are settled.

        # The delay is modified via the LOOPSETTLEDELAY keyword.

        self.delay = 1
        self.updated = time.time ()

        DFW.Keyword.Enumerated.__init__ (self, 'LOOPSTATE', service)

        loopdelay = service.name + '.LOOPSETTLEDELAY'
        DFW.Keyword.callback (loopdelay, self.delayCallback)


    def delayCallback (self, keyword):

        if keyword == None or keyword.value == None or self.value == None:
            return

        delay = int (keyword.value)

        if self.mapped (lower=True) != 'closing':
            schedule = False
        elif delay > 0 and self.delay <= 0:
            schedule = True
        elif self.delay > 0 and delay < self.delay:
            schedule = True
        else:
            schedule = False

        self.delay = delay

        if schedule == True:
            # The new delay is either shorter than the previous delay,
            # or the previous delay was infinite. Schedule an update
            # for the LOOPSTATE keyword. If the new appointment time
            # is in the past, the update will be processed immediately.

            self.schedule (self.updated + delay)


    def setSupervisor (self, supervisor):

        mode = supervisor.loop

        if mode == 'closed':
            state = 'closing'
        else:
            state = 'open'


    def set (self, value, force=False):

        now = time.time ()

        if self.value != None:
            current = self.mapped (lower=True)

            if current != 'closing':
                self.updated = now

        DFW.Keyword.Enumerated.set (self, value, force)
        delay = self.delay
        current = self.mapped (lower=True)

        if delay <= 0:
            # An update occurred, but because there's no delay there's
            # nothing to do.
            return

        if current == 'closing':
            # Check again after a delay, whereupon the value
            # will be set to 'Settled'.
            self.schedule (now + delay)


    def read (self):

        delay = self.delay

        if delay > 0 and self.mapped (lower=True) == 'closing':
            # After an arbitrary delay, report that
            # the loops have settled.
            if self.updated + delay < time.time ():
                return self.mapping['settled']

        return self.value


    def prewrite (self, value):

        value = DFW.Keyword.Enumerated.prewrite (self, value)


        mapped = self.mapping[value].lower ()

        if mapped != 'settled':
            raise ValueError, "only 'Settled' can be written to LOOPSTATE"

        if self.mapped (lower=True) != 'closing':
            raise ValueError, "can only set 'Settled' if LOOPSTATE is 'Closing'"


        return value


# end of class LoopState



class Gain (DFW.Keyword.Double):

    def __init__ (self, name, service, attribute):

        self.attribute = attribute
        DFW.Keyword.Double.__init__ (self, name, service)


    def write (self, value):

        value = float (value)
        supervisor = getSupervisor ()

        setattr (supervisor, self.attribute, value)


    def setSupervisor (self, supervisor):

        gain = getattr (supervisor, self.attribute)
        self.set (repr (gain))


# end of class Gain



class SmartGain (DFW.Keyword.Double):

    def __init__ (self, service):

        name = 'SMART_GAIN'
        DFW.Keyword.Double.__init__ (self, name, service)

        self.loop = None
        self.open = '0.0'
        self.closed = '0.0'

        loop = service.name + '.LOOP'
        open = service.name + '.STWEETERGAIN'
        closed = service.name + '.TWEETERGAIN'

        DFW.Keyword.callback (loop, self.loopCallback)
        DFW.Keyword.callback (open, self.openCallback)
        DFW.Keyword.callback (closed, self.closedCallback)


    def loopCallback (self, keyword):

        if keyword.value == None:
            return

        loop = keyword.mapped (lower=True)
        self.loop = loop

        if loop == 'open':
            self.set (self.open)
        elif loop == 'closed':
            self.set (self.closed)


    def openCallback (self, keyword):

        open = keyword.value
        self.open = open

        if self.loop == 'open':
            self.set (open)


    def closedCallback (self, keyword):

        closed = keyword.value
        self.closed = closed

        if self.loop == 'closed':
            self.set (closed)


    def write (self, value):

        loop = self.loop

        if loop == 'open':
            dispatcher = getDispatcher ()
            dispatcher['STWEETERGAIN'].modify (value)
            dispatcher['SWOOFERGAIN'].modify (value)

        elif loop == 'closed':
            value = float (value)
            supervisor = getSupervisor ()
            supervisor.set_gain (value, value)

        else:
            raise ValueError, "unhandled loop state: %s" % (loop)


# end of class SmartGain



class TweeterGain (Gain):

    def __init__ (self, service):

        Gain.__init__ (self, 'TWEETERGAIN', service, 'gain')


    def write (self, value):

        supervisor = getSupervisor ()
        supervisor.set_gain (float (value), supervisor.wgain)


# end of class TweeterGain



class WooferGain (Gain):

    def __init__ (self, service):

        Gain.__init__ (self, 'WOOFERGAIN', service, 'wgain')


    def write (self, value):

        supervisor = getSupervisor ()
        supervisor.set_gain (supervisor.gain, float (value))


# end of class WooferGain


# vim: set expandtab tabstop=8 softtabstop=4 shiftwidth=4 autoindent:
