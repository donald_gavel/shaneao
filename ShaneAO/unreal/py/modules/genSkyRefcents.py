#!/usr/bin/python
usage = """
Convert refcents calibrated on the fiber source
to versions suitable for on-sky guidestars.
Use d/r0 as a factor to reduce the values

Usage:
  genSkyRefcents <mode> <file> <seeing>

where mode is '16x' or '8x'
file is the orginal refcents file.
seeing (optional) is the seeing in arcseconds
"""
import sys
import os
import readfits as fits

home = os.environ['HOME']
subdir = os.path.join(home,'data')

mode = sys.argv[1]

if (mode == 'help'):
    print usage
    exit()

fileName = sys.argv[2]

fullFileName = os.path.join(subdir,fileName)
if (not os.path.exists(fullFileName)):
    print 'file '+fullFileName+' does not exits'
    exit()

if (len(sys.argv) > 3):
    seeing = float(sys.argv[3])
    arcsec = 3.141592654/(180.*3600.)
    r0 = 0.5e-6/(seeing*arcsec)
    print 'calculated r0 = '+str(r0)
else:
    r0 = 0.1
    print 'default r0 = '+str(r0)

rc = fits.readfits(os.path.join(subdir,fileName))

if (mode == '16x'):
    d = 0.2
elif (mode == '8x'):
    d = 0.4
else:
    print 'mode '+str(mode)+' is not a valid mode'
    print 'valid modes are 16x and 8x'
    exit()

#r0 = 0.1
d_over_r0 = d/r0
rc_mod = rc / d_over_r0

fileNameParts = os.path.splitext(fileName)

newFileName = fileNameParts[0]+'_onSky.fits'
fullNewFileName = os.path.join(subdir,newFileName)

print 'writing to '+fullNewFileName
fits.writefits(rc_mod,fullNewFileName,protect=True)

