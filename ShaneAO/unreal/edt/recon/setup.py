# setup.py: Build and instgall the rtc2 extension.

from distutils.core import setup, Extension
import numpy as np

setup(
	name='edt',
	version='1.0',
	description = 'ShaneAO edt frame-grabber SIMULATOR module',
	author='Don Gavel',
	author_email='gavel@ucolick.org',
	url='http://lao.ucolick.org/ShaneAO/',
	requires = ['numpy'],
	ext_modules = [
		Extension(
			"edt",
			sources = ["edt.c", "processingThread.c", "debug.c"],
			include_dirs = ['.','/opt/kroot/include','/usr/local/lick/include/edt',np.get_include()],
			library_dirs = ['/usr/local/lick/lib','/opt/kroot/lib'],
			extra_link_args = ['-Wl,-rpath,/usr/local/lick/lib','-Wl,-rpath,/opt/kroot/lib'],
			libraries = ['pdv','rt','pthread']
			)
		],
	)
