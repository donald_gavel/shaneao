#include "Camera.h"
#include <libgen.h>


static long
microtenths (const struct timespec minuend, const struct timespec subtrahend) {

  long result;

  result  = (minuend.tv_sec  - subtrahend.tv_sec)  * 10000000;
  result += (minuend.tv_nsec - subtrahend.tv_nsec) / 100;

  return result;
}



void *
frameProcessing (void *instance) {

  Camera *self = (Camera *)instance;
  Handler *handler;

  u_char *image_data = NULL;
  int overrun_total=0, timeout_total=0, result=0, index;
  Py_ssize_t length;


  struct timespec completion;
  struct timespec handoff;
  struct timespec previous = {0, 0};
  struct timespec waketime;

  bool recovering_from_timeout = FALSE;


  pthread_mutex_lock (&self->mutex);
  self->started = TRUE;
  pthread_mutex_unlock (&self->mutex);


  /* This next section of code is derived from the simple_take.c example
     provided by EDT. */

  /* Prime the acquisition pipeline before entering the processing loop. */

  if (self->buffers == 1) {
    pdv_start_image (self->device);
  }
  else {
    pdv_start_images (self->device, self->buffers);
  }


  while (self->shutdown == FALSE) {

    if (self->pause == TRUE) {

      self->frame_interval = 0;
      previous.tv_sec = 0;
      previous.tv_nsec = 0;

      clock_gettime (CLOCK_REALTIME, &waketime);
      waketime.tv_sec += 300;

      pthread_mutex_lock (&self->mutex);
      pthread_cond_timedwait (&self->condition, &self->mutex, &waketime);
      pthread_mutex_unlock (&self->mutex);

      continue;
    }


    image_data = pdv_wait_image (self->device);
    clock_gettime (CLOCK_MONOTONIC_RAW, &handoff);


    /* Check for overruns and/or timeouts. */

    result = edt_reg_read (self->device, PDV_STAT) & PDV_OVERRUN;

    if (result != 0) {
      overrun_total++;
      fprintf (stderr, "%d.%09d pdv_wait_image() buffer overrun #%d\n", (int)handoff.tv_sec, (int)handoff.tv_nsec, overrun_total);
    }

    /* Start the next image, so that it may be processed in parallel. */

    if (self->shutdown == FALSE && self->pause == FALSE) {
      pdv_start_image (self->device);
    }

    result = pdv_timeouts (self->device);

    if (result != timeout_total) {

      pdv_timeout_restart (self->device, TRUE);
      timeout_total = result;
      recovering_from_timeout = TRUE;

      self->frame_interval = 0;
      previous.tv_sec = 0;
      previous.tv_nsec = 0;

      /* It's possible that self->pause was set while this thread
         blocked in pdv_wait_image(); this is likely to occur if
         a serial command is issued. If this did occur, don't bother
         logging or otherwise reporting the timeout. */

      if (self->pause == FALSE) {

        fprintf (stderr, "%d.%09d pdv_wait_image() time out #%d\n", (int)handoff.tv_sec, (int)handoff.tv_nsec, result);
      }

      /* Back up to the top of the loop after a timeout. */
      continue;

    } else if (recovering_from_timeout == TRUE) {

      /* All of the example code has this handling sequence, where you invoke
         pdv_timeout_restart() once, and then invoke it a second time after
         iterating through the loop. The documentation states that this may
         be overkill, but implies that the redundancy is harmless. */

      pdv_timeout_restart (self->device, TRUE);
      recovering_from_timeout = FALSE;
    }


    /* Done with the basic image acquisition. Hand off further processing
       to the dynamically-specified handler function. */


    if (self->handler != Py_None) {

      length = PySequence_Length (self->handler);

      for (index=0; index < length; index++) {

        /* PyTuple_GET_ITEM returns a borrowed refrence. It is used here
           in favor of the PySequence family of functions, because we
           guarantee by construction that self->handler is a tuple. */

        handler = (Handler *)PyTuple_GET_ITEM (self->handler, index);

        pthread_mutex_lock (&handler->mutex);
        handler->function (handler, image_data);
        pthread_mutex_unlock (&handler->mutex);

        /* PySequence_Fast_GET_ITEM() returns a borrowed reference, do not
           call Py_DECREF() on the returned Handler instance. */
      }
    }


    /* Done with image processing. Take some (quick!) notes about execution
       time before moving on to the next frame. */

    clock_gettime (CLOCK_MONOTONIC_RAW, &completion);

    if (self->frame_processing != 0) {
      /* Weighted average, one hundred samples. */
      self->frame_processing = (self->frame_processing * 99 + microtenths (completion, handoff)) / 100;

    } else {
      self->frame_processing = microtenths (completion, handoff);
    }


    if (self->frame_interval != 0) {
      /* Weighted average, one hundred samples. */
      self->frame_interval = (self->frame_interval * 99 + microtenths (handoff, previous)) / 100;

    } else if (previous.tv_sec == 0 && previous.tv_nsec == 0) {
      /* First frame in this sequence. */
      self->frame_interval = 0;

    } else {
      /* No previous data, this is the new average. */
      self->frame_interval = microtenths (handoff, previous);
    }

    previous.tv_sec  = handoff.tv_sec;
    previous.tv_nsec = handoff.tv_nsec;

    /* Next frame, please. */
  }


  /* self->shutdown must be set to TRUE; this is where cleanup
     actions would go. */

  return NULL;
}



/* Hitting tab gets you two spaces. Actual tabs render as eight characters,
 * the way that any (sane) editor expects them to.
 *
 * vim: set expandtab tabstop=8 softtabstop=2 shiftwidth=2 autoindent:
 */

/* EOF */
