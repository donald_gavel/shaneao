
#ifndef __SHANEAO_CAMERA_H__
#define __SHANEAO_CAMERA_H__

/* Python.h must be included before any standard header files, per the
   Python/C API documentation. */

#include <Python.h>
#include <structmember.h>   /* defines PyMemberDef and supporting symbols */
#include <config.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_TIME_H
#include <time.h>               /* struct timespec */
#endif

#ifndef CLOCK_MONOTONIC_RAW     /* Not available on CentOS 5, kernel < 2.6.28 */
#define CLOCK_MONOTONIC_RAW CLOCK_MONOTONIC
#endif

#include "edtinc.h"


/* Backwards compatibility with Python 2.4 for the Py_ssize_t type,
   which is a standard definition in Python 2.5 and later. */

#if PY_VERSION_HEX < 0x02050000 && !defined (PY_SSIZE_T_MIN)
typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
#endif

#ifndef PyMODINIT_FUNC  /* boilerplate macro for DLL import/export */
#define PyMODINIT_FUNC void
#endif

#ifndef T_BOOL
#define T_BOOL T_INT          /* T_BOOL was established in Python 2.6. */
#endif

#ifndef bool
typedef int bool;
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


/******************************************************************************/
/* Internal struct definitions. */

typedef struct Message {

  double timestamp;
  char *type;
  char *text;
  struct Message *next;

} Message;


/******************************************************************************/
/* struct definitions for classes exposed to Python. */

typedef struct Camera {

  PyObject_HEAD
  PyObject *unit;
  PyObject *config;
  PyObject *notify;
  Message *messages;
  Message *last_message;
  Message *free_messages;
  pthread_mutex_t message_mutex;
  int width;
  int height;
  int depth;
  PdvDev *device;
  int buffers;
  bool started;
  bool pause;
  bool shutdown;
  pthread_t thread;
  pthread_cond_t condition;
  pthread_mutex_t mutex;
  PyObject *handler;
  long frame_interval;
  long frame_processing;
  void *data;

} Camera;


typedef struct Handler {

  PyObject_HEAD
  Camera *camera;
  char *name;
  void (*function)(struct Handler *, u_char *);
  pthread_mutex_t mutex;
  PyObject *pyobject;    /* Optional locations to reference extra data. */
  void *voidstar;        /* One could also sub-class the Handler class. */
  void (*cleanup)(void); /* Optional function to free() self->voidstar. */

} Handler;



/******************************************************************************/
/* Function prototypes. */

void appendMessage (Camera *instance, const char *type, const char *message);
void freeMessages (Camera *instance);
void releaseMessages (Camera *instance, Message *message);

void *frameProcessing (void *instance);
PyObject *serialCommand (Camera *instance, const char *command, PyObject *wait);


#endif /* __SHANEAO_CAMERA_H__ */


/* Hitting tab gets you two spaces. Actual tabs render as eight characters,
 * the way that any (sane) editor expects them to.
 *
 * vim: set expandtab tabstop=8 softtabstop=2 shiftwidth=2 autoindent:
 */

/* EOF */
