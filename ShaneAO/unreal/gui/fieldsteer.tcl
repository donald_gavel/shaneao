#! /usr/bin/wish

#GUI to do basic field steers in LGS mode (add NGS later)

#Global Variables
global cumNS ; set cumNS 0
global cumEW ; set cumEW 0
global mode ; set mode LGS
global arcsecOffset ; set arcsecOffset 0
global telNS ; set telNS 0
global telEW ; set telEW 0
global autoopen ; set autoopen 1
global movetel ; set movetel 1
global movemot ; set movemot 1
global pa ; set pa 0
global tub ; set tub 74
global telrotNS ; set telrotNS 0
global telrotEW ; set telrotEW 0
global cx ; set cx 0
global cy ; set cy 0

proc updateTelescopeOffsets {} {
    set telNS_deg [exec show -s poco -terse pocderet]
    set telEW_hr [exec show -s poco -terse pocraret]
    set tel_dec [exec show -s poco -terse pocdeca]
    #convert relative position degrees to arcsec:
    set dec [split $telNS_deg ":"] 
    set dd [lindex $dec 0]
    if {[string first - $dd]} {set decsign -1.} {set decsign 1.}
    set dm [lindex $dec 1]
    set ds [lindex $dec 2]
    set ::telNS [expr $decsign*($dd*3600.+$dm*60.+$ds)]
    
    #convert Dec to radians
    set dec [split $tel_dec ":"]
    set dd [lindex $dec 0]
    set dm [lindex $dec 1]
    set ds [lindex $dec 2]
    set cos [expr cos( 3.14159 / 180. * ($dd+$dm/60.+$ds/3600.))]

    #convert hr to arcsec:
    set ra [split $telEW_hr ":"]
    set rh [lindex $ra 0]
    if {[string first - $rh]} {set rasign -1.} {set rasign 1.}
    set rm [lindex $ra 1]
    set rs [lindex $ra 2]
    set ::telEW [expr $rasign*($rh*3600.+$rm*60.+$rs)*15.*$cos ]

}

# Initialize Telescope Offset Position
#updateTelescopeOffsets

proc moveTelescope {raOffset decOffset} {
    
#convert RA arcsec offset to hours:
set ra_hr [expr $raOffset/15./3600.]

#convert Dec arcsec offset to degrees:
set dec_deg [expr $decOffset/3600.]

# Send commands to move telescope
puts stdout "Moving telescope RA=$raOffset Dec=$decOffset."
exec modify -s poco POCDERE=$dec_deg
exec modify -s poco POCRARC=$ra_hr

puts stdout "Telescope move sent."

# For now not going to wait for Telescope Move to Complete:
set COMMENTED_OUT {
# Set command status to 5
exec modify -s poco POCOCMD = 5

# Wait for moves to complete
set test [lindex [exec show -s poco POCOCMD] 2]

while {$test > 0} {
        set test [lindex [exec show -s poco POCOCMD] 2]
        after 1000
}
puts stdout "Telescope move completed."
}

}

proc moveMotorsLGS {ns ew} {
exec fieldSteer mode=lgs north=$ns east=$ew
}


proc moveMotorsNGS {ns ew cx cy} {
#Note, these moves will not have centering corrections
exec fieldSteer -e mode=ngs north=$ns east=$ew cx=$cx cy=$cy
}

proc fieldsteer {n e} {

    if {$::pa >= 42} { set ::tub [expr $::pa + 74. - 95.5]} 
    if {$::pa < 42} { set ::tub [expr $::pa + 74. -95.5 +180]}
set rad [expr $::pa /180. * 3.14159]
#calculate rotated coords for moving telescope:
set n_pa [expr $n*cos($rad)-$e*sin($rad)]
set e_pa [expr $n*sin($rad)+$e*cos($rad)]

puts stdout "N=$n E=$e PA=$::pa N_pa=$n_pa E_pa=$e_pa"

#open AO loops before moving anything if autoopen set
if {$::autoopen} {exec modify -s saocon loop=open}

if {$::movetel} {moveTelescope $e_pa $n_pa}

if {$::movemot} {
    set ::cumNS [expr $::cumNS + $n]
    set ::cumEW [expr $::cumEW + $e]
    if {$::mode == "LGS"} {moveMotorsLGS $::cumNS $::cumEW}
    if {$::mode == "NGS"} {
	moveMotorsNGS $::cumNS $::cumEW $::cx $::cy
	#exec fieldSteer mode=ngs pos=Middle
    }

}

}

#Make GUI

button .north -text N -command { 
   fieldsteer $arcsecOffset 0
}
button .east -text E -command {
   fieldsteer 0 $arcsecOffset
}
button .west -text W -command {
    fieldsteer 0 -$arcsecOffset
}
button .south -text S -command {
    fieldsteer -$arcsecOffset 0
}

entry .arcsecOffset -textvar arcsecOffset -width 4

label .cx_l -text "cx:"
label .cy_l -text "cy:"
entry .cx_e -width 4 -textvariable cx 
entry .cy_e -width 4 -textvariable cy

label .fsoffsets -text "Field Steering Motor Offsets"

label .cumNS_l -text "N-S:"
entry .cumNS_e -width 6 -textvariable cumNS -relief flat

label .cumEW_l -text "E-W:"
entry .cumEW_e -width 6 -textvariable cumEW -relief flat

#label .teloffsets -text "Telescope Offset from Target"

#label .telNS_l -text "N-S:"
#entry .telNS_e -width 4 -textvariable telNS -relief flat

#label .telEW_l -text "E-W:"
#entry .telEW_e -width 4 -textvariable telEW -relief flat

radiobutton .mode1 -text LGS -variable mode -value LGS
radiobutton .mode2 -text NGS -variable mode -value NGS

checkbutton .moveTel -text "Move Telescope" -variable movetel
checkbutton .moveMot -text "Move Fieldsteer Motors" -variable movemot
checkbutton .autoOpenLoops -text "Auto Open Loops" -variable autoopen

button .onaxis -text "Back On-Axis" -command { 
set ::cx 0
set ::cy 0
fieldsteer -$::cumNS -$::cumEW 
}

button .aoready -text "AOReady" -command { 
exec modify -s saocon loopstate=settled
}

button .quit -text "QUIT" -command { exit }

#Add stuff to TUB and Position Angles
label .pa_l -text "Desired PA:"
entry .pa_e -width 5 -textvariable pa 
label .tub_l -text "TUB:"
entry .tub_e -width 5 -textvariable tub -relief flat
label .telrotmoves -text "PA Telescope Offsets NS EW:"
entry .telrotNS_e -width 5 -textvariable telrotNS -relief flat
entry .telrotEW_e -width 5 -textvariabl telrotEW -relief flat

#arrange GUI items

grid .north -row 0 -column 1  
grid .east -row 1 -column 0
grid .arcsecOffset -row 1 -column 1 -sticky news
grid .west -row 1 -column 2 
grid .south -row 2 -column 1

grid .cx_l -row 3 -column 0 -sticky w
grid .cx_e -row 3 -column 1 
grid .cy_l -row 3 -column 2 -sticky w
grid .cy_e -row 3 -column 3 

grid .fsoffsets -row 4 -column 0 -columnspan 4 -sticky w
grid .cumNS_l -row 5 -column 0
grid .cumNS_e -row 5 -column 1
grid .cumEW_l -row 5 -column 2
grid .cumEW_e -row 5 -column 3

grid .moveTel -row 6 -column 0 -columnspan 3 -sticky w
grid .moveMot -row 7 -column 0 -columnspan 3 -sticky w
grid .autoOpenLoops -row 8 -column 0 -columnspan 3 -sticky w

grid .mode1 -row 9 -column 0
grid .mode2 -row 9 -column 1

grid .onaxis -row 10 -column 0 -columnspan 2 -sticky w
grid .aoready -row 10 -column 2 
grid .quit -row 10 -column 3 -sticky e

grid .pa_l -row 11 -column 0
grid .pa_e -row 11 -column 1
grid .tub_l -row 11 -column 2
grid .tub_e -row 11 -column 3
grid .telrotmoves -row 12 -column 0 -columnspan 2 -sticky w
grid .telrotNS_e -row 12 -column 2 
grid .telrotEW_e -row 12 -column 3

