#!/bin/sh
# \
exec wish $0 -- $@
#
#  Create a graphical status display for the ShaneAO state, updating it by periodic poling in the background.
#  Also can send commands to ShaneAO via button presses
#  or by typing arbitrary commands in a text entry window.
#
#  It is designed to be run on a client host machine, remote from the real-time computer.
#  It communicates with shaneao, the adaptive optics real time control supervisor,
#  using a socket connection to shaneao's commandServer module.
#
#  There can be more than one client connection to the server, allowing for
#  multiple points of control. All status displays are kept current.
#
#  Tcl language makes it easier to add future upgrades such as additional action buttons.
#
#  reference:
#       for socket client connection from tcl to python: http://wiki.tcl.tk/1757

set help_str "
------------------------
USAGE:
	rtcgui \[server\] \[options\]
ARGUMENTS:
	server is an optional host name - can be real or localhost. Default is real
OPTIONS:
	-h, --help prints help
------------------------"

set verbose 0 ; # spew out messages of every attempt to communicate with the server
#set popup_goaway 1
set right_side 0

package require Tk
ttk::style theme use classic

#============================================
#   Command line argument handling
#
set remargs $::argv
if { $::argc > 0 } {
	set firstarg [lindex $::argv 0]
	if { [string index $firstarg 0] == "-" } {
		set server real.ucolick.org
	} elseif {$firstarg == "help"} {
		puts $help_str
		exit
	} else {
		set server [lindex $::argv 0]
		set remargs [lrange $::argv 1 end]
	}
} else {
    set server real.ucolick.org
}

if { [llength $remargs] > 0 } {
	set dashing 1
	foreach arg $remargs {
		if { $dashing } {
			if { $arg == "-h" || $arg == "--help" } {
				puts $help_str
				exit
			} else {
				puts "invalid argument $arg"
				puts "type rtcgui -h for help"
				exit
			}
		} else {
			
		}
	}
}

# =================================
#    The GUI

proc init {} {
	# Defines all the GUI elements
	#
	
	global lab_names state lab_text
	global tcl_precision
	
	wm geometry . +415+28
	ttk::label .title -text "ShaneAO - RTC Panel"
	.title configure -anchor center -width 20 -padding [list 0 5] -background white
	grid .title -row 0 -column 0 -columnspan 2

	ttk::style configure rs.TFrame -background White

	ttk::frame .tp; # totem pole side
	ttk::frame .rs -width 300 -relief groove -style rs.TFrame; # right side
	grid .tp -row 1 -column 0
	
	if { $::right_side } {
		grid .rs -row 1 -column 1 -sticky n		
	}
		
	# --- totem pole side ----
	
	set lab_names [list heartbeat mems mems_ok woofer \
				   camera rate ttcamera ttrate mode subState \
				   cent loop gain savedGain \
				   offloading frozen]
	
	set ::dbg gray
	foreach name $lab_names {
		set state($name) None
		set lab_text($name) "$name: None"
		ttk::label .tp.$name -text $name
		.tp.$name configure -anchor center -width 18 -relief groove -padding [list 0 2]
		if { $name == "loop" } {
			ttk::frame .tp.loop_frame -width 500 -relief groove -padding 4
			ttk::label .tp.loop_frame.tt -text "tt" -width 5 -anchor center -relief groove
				grid .tp.loop_frame.tt -row 0 -column 0
			ttk::label .tp.loop_frame.wfs -text "wfs" -width 5  -anchor center -relief groove
				grid .tp.loop_frame.wfs -row 0 -column 1
			ttk::label .tp.loop_frame.uplink -text "uplink" -width 6 -anchor center -relief groove
				grid .tp.loop_frame.uplink -row 0 -column 2
			bind .tp.loop_frame.tt <Enter> {
				set dbg [%W cget -background]
				%W configure -background White
				set name [string trim %W "."]
				set protected($name) 1
			}
			bind .tp.loop_frame.tt <Leave> {
				%W configure -background $dbg
				set name [string trim %W "."]
				set protected($name) 0			
			}
			bind .tp.loop_frame.wfs <Enter> {
				set dbg [%W cget -background]
				%W configure -background White
				set name [string trim %W "."]
				set protected($name) 1
			}
			bind .tp.loop_frame.wfs <Leave> {
				%W configure -background $dbg
				set name [string trim %W "."]
				set protected($name) 0			
			}
		}
		lappend lab_list .tp.$name
		bind .tp.$name <Enter> {
			set dbg [%W cget -background]
			%W configure -background White
			set name [string trim %W "."]
			set protected($name) 1
		}
		bind .tp.$name <Leave> {
			%W configure -background $dbg
			set name [string trim %W "."]
			set protected($name) 0			
		}
	}
	
	# darks
	ttk::frame .tp.df -relief groove -padding 4
	ttk::label .tp.df.dk -text dark -width 7 -anchor center -background gray -relief groove -padding [list 5 10 5 10]
	grid .tp.df.dk -row 0 -column 0
	ttk::label .tp.df.ttdk -text ttdark -width 7 -anchor center -background gray -relief groove -padding [list 5 10 5 10]
	grid .tp.df.ttdk -row 0 -column 1
	
	bind .tp.df.dk <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.df.dk <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}
	bind .tp.df.ttdk <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.df.ttdk <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}

	# bias/flat
	ttk::frame .tp.bff -relief groove -padding 4
	ttk::label .tp.bff.b -text bias -width 7 -anchor center -background gray -relief groove -padding 5
	ttk::label .tp.bff.f -text flat -width 7 -anchor center -background gray -relief groove -padding 5
	grid .tp.bff.b -row 0 -column 0
	grid .tp.bff.f -row 0 -column 1

	bind .tp.bff.b <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.bff.b <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}
	bind .tp.bff.f <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.bff.f <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}

	# options
	ttk::frame .tp.mf
	ttk::button .tp.mf.opt -text options -width 7 -command options
	ttk::button .tp.mf.h -text help -width 7 -command help
	grid .tp.mf.opt -row 0 -column 0 -sticky w
	grid .tp.mf.h -row 0 -column 1
	
	# connected label
	ttk::button .tp.conn -text "not connected" -width 18
	bind .tp.conn <ButtonPress> connect
	#.tp.conn configure -background white -anchor center -padding [list 0 3]
	#ttk::button .tp.bcon -text "connect" -width 18 -command { connect }
	ttk::button .tp.bquit -text "quit" -width 18 -command { exit }
	
	# command entry box and 'do' button
	ttk::frame .tp.ef
	ttk::entry .tp.ef.ce -width 14
	ttk::button .tp.ef.eb -text "do" -command { do }
	grid .tp.ef.ce -row 0 -column 0
	grid .tp.ef.eb -row 0 -column 1
	bind .tp.ef.ce <Return> ".tp.ef.eb invoke"
	
	foreach lab $lab_list {
		if { $lab == ".tp.loop" } {
			grid [ttk::label .tp.sp1 -text "Loops" -anchor center -padding [list 0 5 0 0]]
			grid $lab
			grid .tp.loop_frame
			grid [ttk::label .tp.sp2 -text "" -anchor center]
		} else {
			grid $lab
		}
	}
	grid [ttk::label .tp.lcam -text Cameras -anchor center]
	grid .tp.df
	grid [ttk::label .tp.ldm -text DMs -anchor center]
	grid .tp.bff
	grid .tp.mf
	#grid .tp.conn
	grid .tp.ef
	grid .tp.conn
	grid .tp.bquit
	
	set tcl_precision 4

	bind .title <Enter> unselect
	bind .tp.conn <Enter> unselect
	bind .tp.mems <ButtonPress> mems
	bind .tp.woofer <ButtonPress> woofer
	bind .tp.camera <ButtonPress> camera
	bind .tp.camera <Key-r> reset
	bind .tp.ttcamera <ButtonPress> ttcamera
	bind .tp.ttcamera <Key-r> ttreset
	bind .tp.cent <ButtonPress> cent
	bind .tp.loop <ButtonPress> loop
	bind .tp.loop_frame.tt <ButtonPress> tt
	bind .tp.loop_frame.wfs <ButtonPress> wfs
	bind .tp.loop_frame.uplink <ButtonPress> uplink
	bind .tp.rate <ButtonPress> rate
	bind .tp.mode <ButtonPress> mode
	bind .tp.ttrate <ButtonPress> ttrate
	bind .tp.offloading <ButtonPress> offload
	bind .tp.frozen <ButtonPress> freeze
	bind .tp.df.dk <ButtonPress> dark
	bind .tp.df.ttdk <ButtonPress> ttdark
	bind .tp.bff.b <ButtonPress> bias
	bind .tp.bff.f <ButtonPress> flat
	
	# ngs/lgs buttons in place of subState
	ttk::frame .tp.gs_frame -width 500
	grid .tp.gs_frame -row 9
	ttk::label .tp.gs_frame.ngs -text ngs -width 8 -anchor center -background gray -relief groove
	grid .tp.gs_frame.ngs -row 0 -column 0
	bind .tp.gs_frame.ngs <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.gs_frame.ngs <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}
	bind .tp.gs_frame.ngs <ButtonPress> ngs
	ttk::label .tp.gs_frame.lgs -text lgs -width 8 -anchor center -background gray -relief groove
	grid .tp.gs_frame.lgs -row 0 -column 1
	bind .tp.gs_frame.lgs <Enter> {
		set dbg [%W cget -background]
		%W configure -background White
		set name [string trim %W "."]
		set protected($name) 1
	}
	bind .tp.gs_frame.lgs <Leave> {
		%W configure -background $dbg
		set name [string trim %W "."]
		set protected($name) 0			
	}
	bind .tp.gs_frame.lgs <ButtonPress> lgs
	
	# --- right side ----

	if { $::right_side } {

		ttk::frame .rs.gf -pad 4 -style rs.TFrame -width 300 -relief groove
		ttk::label .rs.gf.gl -text "gain" -background White -relief groove -padding 4
		ttk::frame .rs.gf.gf2 -style rs.TFrame -relief groove -width 300
		ttk::entry .rs.gf.gf2.ge -textvariable ::mygain
		ttk::label .rs.gf.gf2.gset -text "close" -relief groove -padding 4 -width 5
		bind .rs.gf.gf2.gset <Button> { send $::sock "gain($::mygain)" }
		ttk::label .rs.gf.gf2.gsave -text "open" -relief groove -padding 4 -width 5
		bind .rs.gf.gf2.gsave <Button> { send $::sock "gain($::mygain); open()" }
		grid .rs.gf.gl -sticky nw
		grid .rs.gf.gf2 -sticky ew
		grid .rs.gf.gf2.ge -row 0 -column 0 -sticky ew
		grid .rs.gf.gf2.gset -row 0 -column 1 -sticky ew
		grid .rs.gf.gf2.gsave -row 0 -column 2 -sticky ew
		grid .rs.gf -row 0 -sticky ew
		
		ttk::frame .rs.rcf -style rs.TFrame -pad 4 -width 300
		ttk::label .rs.rcf.l -text "refcent file" -background White -relief groove -padding 4
		grid .rs.rcf.l -sticky nw
		ttk::combobox .rs.rcf.cb -values $::refcent_file_list -width 30
		grid .rs.rcf.cb -sticky sw
		grid .rs.rcf -row 1 -sticky sw

	}
	
}

set refcent_file_list [ list \
	hs-20140506-cog_16x.fits \
	hs-20140506_8x.fits \
	hs-20140506_16x.fits \
	hs-20140506_16x_onSky.fits \
	hs-20140515_16xLGS.fits \
	hs-20140515_16xLGS_onSky.fits \
	hs-2014-05-16_8xLGS.fits \
	hs-2014-05-16_8xLGS_onSky.fits \
	hs-2014-05-18_16xLGS.fits \
	hs-2014-05-18_16xLGS_onSky.fits \
	hs-2014-05-18_8xLGS.fits \
	hs-2014-05-18_8xLGS_onSky.fits \
	hs-2014-05-19_16xLGS.fits \
	hs-2014-05-19_16xLGS_onSky.fits \
	hs-2014-05-19_8xLGS.fits \
	hs-2014-05-19_8xLGS_onSky.fits \
	hs-2014-05-22_16xLGS.fits \
	hs-2014-05-22_16xLGS_onSky.fits \
	hs-2014-05-22_8xLGS.fits \
	hs-2014-05-22_8xLGS_onSky.fits \
	hs-2014-05-23_16xLGS.fits \
	hs-2014-05-23_16xLGS_onSky.fits \
	hs-2014-05-24_16xLGS.fits \
	hs-2014-05-24_16xLGS_onSky.fits \
	hs-2014-05-26_8xLGS.fits \
	hs-2014-05-26_8xLGS_onSky.fits \
]

proc clear_totem {} {
	# Sets the whole GUI to a 'gray' state.
	# This is a visual clue that communication with the server has dropped.
	#
    global lab_names lab_text protected
    foreach name $lab_names {
        .tp.$name configure -text $name -background gray
		set lab_text($name) $name
		if { $name == "loop" } {
			.tp.loop_frame.tt configure -background gray
			.tp.loop_frame.wfs configure -background gray
			.tp.loop_frame.uplink configure -background gray
		}
		set protected($name) 0
    }
	.tp.gs_frame.ngs configure -background gray
	.tp.gs_frame.lgs configure -background gray	
	set protected(loop_frame.tt) 0
	set protected(loop_frame.wfs) 0
	set protected(gs_frame.lgs) 0
	set protected(gs_frame.ngs) 0
}

#--------------------------------------------
#  respond to specific GUI button presses

proc mems {} {
	puts "<mems> you pressed the mems button"
	set s [.tp.mems cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "on" } {
		send $::sock "mems('off')"
	} elseif { $val == "off" } {
		send $::sock "mems('on')"
	}	
}

proc woofer {} {
	puts "<woofer> you pressed the woofer button"
	set s [.tp.woofer cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "on" } {
		send $::sock "woof('off')"
	} elseif { $val == "off" } {
		send $::sock "woof('on')"
	}	
}

proc camera {} {
	puts "<camera> you pressed the camera button"
	set s [.tp.camera cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "stopped" } {
		send $::sock "camstart()"
	} elseif { $val == "running" } {
		send $::sock "camstop()"
	}		
}

proc ttcamera {} {
	puts "<ttcamera> you pressed the ttcamera button"
	set s [.tp.ttcamera cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "stopped" } {
		send $::sock "ttcamstart()"
	} elseif { $val == "running" } {
		send $::sock "ttcamstop()"
	}		
}

proc loop {} {
	set s [.tp.loop cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1]]
	if { $val == "open" } {
		send $::sock "close()"
	} elseif { $val == "closed" } {
		send $::sock "open()"
	}
}

proc offload {} {
	set s [.tp.offloading cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1]]
	if { $val == "True" } {
		send $::sock "offload('off')"
	} elseif { $val == "False" } {
		send $::sock "offload('on')"
	}	
}

proc freeze {} {
	set s [.tp.frozen cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1]]
	if { $val == "True" } {
		send $::sock "resume()"
	} elseif { $val == "False" } {
		send $::sock "freeze()"
	}	
}

proc mon {call args} {
	puts "$call is done"
	if { $call == "dark" } {
		set path .tp.df.dk
	} elseif { $call == "ttdark" } {
		set path .tp.df.ttdk
	}
	$path configure -text $call -state enabled
	trace remove variable ::ok write "mon $call"
}

proc dark {} {
	send $::sock "dark()"
	.tp.df.dk configure -text waiting -state disabled
	trace add variable ::ok write {mon dark}
}

proc ttdark {} {
	send $::sock "ttdark()"
	.tp.df.ttdk configure -text waiting -state disabled
	trace add variable ::ok write {mon ttdark}
}

proc bias {} {
	send $::sock "bias0(); bias()"
}

proc flat {} {
	send $::sock "flat0(); flat()"
}

proc reset {} {
	puts "reset camera"
}

proc ttreset {} {
	puts "reset tt camera"
}

proc tt {} {
	global sock state
	puts "<tt> you pressed the tt button"
	if { $state(loop) == "closed" } {
		if { $state(subState) == "wfs only" } {
			send $sock "tt_close()"
		} elseif { $state(subState) == "tt only" } {
			send $sock "tt_open()"
		} elseif { $state(subState) == "lgs" } {
			send $sock "tt_open()"
		}
	} elseif { $state(loop) == "open" } {
		if { $state(subState) == "lgs" } {
			send $sock "tt_close()"
		}
	}
}

proc wfs {} {
	global sock state
	puts "<wfs> you pressed the wfs button"
	if { $state(loop) == "closed" } {
		if { $state(subState) == "tt only" } {
			send $sock "wfs_close()"
		} elseif { $state(subState) == "wfs only" } {
			send $sock "wfs_open()"
		} elseif { $state(subState) == "lgs" } {
			send $sock "wfs_open()"
		}
	} elseif { $state(loop) == "open" } {
		if { $state(subState) == "lgs" } {
			send $sock "wfs_close()"
		}
	}
}

proc rate {} {
	global sock state
	set valid_rates [list ask 1500 1300 1000 700 500 250 100 50]
	
	if { ![winfo exists .tp.rate_select] } {
		ttk::combobox .tp.rate_select -values $valid_rates -width 16 -justify center -state readonly -textvariable lab_text(rate)
		grid .tp.rate_select -row 5
	}
	bind .tp.rate_select <Leave> { unselect }
	bind .tp.rate_select <ButtonPress> {
		bind .tp.rate_select <Leave> {}
		after 10 { bind .tp.rate_select <Leave> { unselect } }		
	}
	bind .tp.rate_select <<ComboboxSelected>> {
		set val [.tp.rate_select get]
		destroy .tp.rate_select
		if { $val == "ask" } {
			send $sock "rate(0)"
		} elseif { $val != $state(rate) } {
			send $sock "rate($val)"
		}
	}
}

proc gain {} {
	#if { [winfo exists .pu] } { destroy .pu }
	#set geo [wm geometry .]
	#set geol [split $geo "x+"]
	#set x [lindex $geol 2]
	#set y [lindex $geol 3]
	#set x [expr $x + 143]
	#set y [expr $y + 171]
	#toplevel .pu
	#wm geometry .pu +$x+$y
	##wm title .pu "rate"
	#foreach r $valid_rates {
	#	ttk::radiobutton .pu.$r -text $r -width 10 -variable ::myrate -value $r
	#	grid .pu.$r
	#}
	#if { [string is double $state(rate) ] } {
	#	set ::myrate [expr round($state(rate))]		
	#}
	#ttk::button .pu.go -text "set rate" -width 8 -command {
	#	send $sock "rate($::myrate)"
	#	if { $::popup_goaway } {
	#		destroy .pu
	#	}
	#}
	#grid .pu.go
}

proc ttrate {} {
	set valid_rates [list ask 40 125 500 1000]
	
	ttk::combobox .tp.tt_rate_select -values $valid_rates -width 16 -justify center -state readonly -textvariable lab_text(ttrate)
	grid .tp.tt_rate_select -row 7
	bind .tp.tt_rate_select <Leave> { unselect }
	bind .tp.tt_rate_select <ButtonPress> {
		bind .tp.tt_rate_select <Leave> {}
		after 10 { bind .tp.tt_rate_select <Leave> { unselect } }		
	}
	bind .tp.tt_rate_select <<ComboboxSelected>> {
		set val [.tp.tt_rate_select get]
		destroy .tp.tt_rate_select
		if { $val == "ask" } {
			send $sock "ttrate(0)"
		} elseif { $val != $state(ttrate) } {
			send $::sock "ttrate($val)"
		}
	}
}

proc cent {} {
	set valid_cents [list cog bin quad]
	
	ttk::combobox .tp.cent_select -values $valid_cents -width 16 -justify center -state readonly -textvariable lab_text(cent)
	grid .tp.cent_select -row 10
	bind .tp.cent_select <Leave> { unselect }
	bind .tp.cent_select <ButtonPress> {
		bind .tp.cent_select <Leave> {}
		after 10 { bind .tp.cent_select <Leave> { unselect } }		
	}
	bind .tp.cent_select <<ComboboxSelected>> {
		set val [.tp.cent_select get]
		destroy .tp.cent_select
		if { $val != $state(cent) } {
			send $::sock "cent('$val')"
		}
	}
}

proc mode {} {
	global sock state
	set valid_modes [list 16xLGS 8xLGS 16x 8x]
	ttk::combobox .tp.mode_select -values $valid_modes -width 16 -justify center -state readonly -textvariable lab_text(mode)
	grid .tp.mode_select -row 8
	bind .tp.mode_select <Leave> { unselect }
	bind .tp.mode_select <ButtonPress> {
		bind .tp.mode_select <Leave> {}
		after 10 { bind .tp.mode_select <Leave> { unselect } }		
	}
	bind .tp.mode_select <<ComboboxSelected>> {
		set val [.tp.mode_select get]
		destroy .tp.mode_select
		if { $val != $state(mode) } {
			send $sock "mode('$val')"
		}
	}
}

proc ngs {} {
	send $::sock "ngs()"
}

proc lgs {} {
	send $::sock "lgs()"
}

proc subState {} {
	global sock state
	puts "<rate> you pressed the subState button"
}

proc unselect {} {
	if { [winfo exists .tp.rate_select] } {
		destroy .tp.rate_select
	}
	if { [winfo exists .tp.tt_rate_select] } {
		destroy .tp.tt_rate_select
	}
	if { [winfo exists .tp.mode_select] } {
		destroy .tp.mode_select
	}	
	if { [winfo exists .tp.cent_select] } {
		destroy .tp.cent_select
	}	
}

bind . <Key-Escape> { unselect }

#====================================
#  Maintain the socket connection

proc connect {} {
	# This is action in response to the 'connect' button push.
	# It also is invoked if a socket communication interchange times out.
	# 
    close_sock
    check_open_sock
}

proc open_sock {} {
	# create and configure the client-side socket
	#
    global verbose sock halt server
    set port 8889
    if { [catch {set sock [socket $server $port]} ] } {
        if $verbose {puts "<open_sock> cannot connect to $server"}
        .tp.conn configure -text "not connected"
        set halt 1
        return
    }
    fconfigure $sock -buffering none -blocking 0
	fileevent $sock readable [list recv $sock]
    .tp.conn configure -text "connected: $server"
    set halt 0
}

proc close_sock {} {
	# close the client-side socket connection
	#
    global sock halt get_state_timer
    after cancel $get_state_timer
    catch {
        puts $sock "bye";
        close $sock;  
    }
    set halt 1
    clear_totem
    .tp.conn configure -text "not connected"
    puts "<close_sock> socket dropped"
}

proc catch_timeout {} {
    puts "<totem.catch_timeout> socket timed out"
    close_sock
}

set get_state_timer 0
set server_ready_timer 0

proc check_open_sock {} {
	# Opens a socket to the shaneao commandServer and performs actions
	# to keep it connected and providing periodic status reports.
	#
	# If the socket drops, it restarts polling for server availability.
	# If the server connects, it starts polling for shaneao status reports.
	#
    global halt server server_ready_timer
    after cancel $server_ready_timer
    open_sock
    if { $halt } { set server_ready_timer [after 2000 check_open_sock]; return; }
    get_state -loop
}

#=========================================
#   interpret the state report

proc interp_state {state_text} {
	#
	# interpret the state report from the server
	#
	#  argument:
	#    text string from the server
	#  affects:
	#    1. creates 'state', a tcl array with key-value pairing
	#    2. adjusts the highlight colors on the GUI lables accordingly
	#
	global protected state lab_text
	
    set state_text [string range $state_text 1 end-1]
    set s_list [split $state_text ","]
    foreach s $s_list {
        set key_val [split $s ":"]
        set key [string trim [lindex $key_val 0] " '\n"]
        set val [string trim [lindex $key_val 1] " '\n"]
		if { [string is double $val ]} {
			set val [expr $val + 0.]
		}
        if {[winfo exists .tp.$key]} {
            .tp.$key configure -text "$key: $val"
        }
        set state($key) $val
		set lab_text($key) "$key: $val"
    }
    
    foreach key [array names state] {
        set bg gray
        set txt "$key: $state($key)"
        switch $key {
            heartbeat {
                set bg [expr { $state($key)=="True" ? "green" : "red"}]
                set txt [expr { $state($key)=="True" ? "heartbeat" : "no heartbeat"}]
            }
            loop {
                set bg [expr { $state($key)=="open" ? "gray" : "green"}]
                set txt [expr { $state($key)=="open" ? "loop: open" : "loop: closed"}]
            }
            woofer -
            mems {
                set bg [expr { $state($key)=="on" ? "green" : "gray"}]
            }
            mems_ok {
                set bg [expr { $state($key)=="True" ? "green":"red" }]
                set txt [expr { $state($key)=="True" ? "mems ok":"mems error" }]
            }
            camera {
                set bg [expr { $state($key)=="running" ? "green":"gray" }]
            }
            ttcamera {
                set bg [expr { $state($key)=="running" ? "green":"gray" }]
            }
            offloading {
                set bg [expr { $state($key)=="True" ? "green":"gray"}]
            }
            frozen {
                set bg [expr { $state($key)=="True" ? "green":"gray"}]
            }
        }
        if {[winfo exists .tp.$key]} {
            .tp.$key configure -text $txt
			if { "$protected($key)" == 0 } {
				.tp.$key configure -background $bg
			}
        }
    }
	
	if { $state(loop) == "closed" } {
		if { $state(subState) == "lgs" } {
			if { $protected(loop_frame.tt) == 0 } {
				.tp.loop_frame.tt configure -background green
			}
			if { $protected(loop_frame.wfs) == 0 } {
				.tp.loop_frame.wfs configure -background green
			}
		} elseif { $state(subState) == "tt only" } {
			if { $protected(loop_frame.tt) == 0 } {
				.tp.loop_frame.tt configure -background green
			}
			if { $protected(loop_frame.wfs) == 0 } {
				.tp.loop_frame.wfs configure -background gray
			}
		} elseif { $state(subState) == "wfs only" } {
			if { $protected(loop_frame.tt) == 0 } {
				.tp.loop_frame.tt configure -background gray
			}
			if { $protected(loop_frame.wfs) == 0 } {
				.tp.loop_frame.wfs configure -background green
			}
		}
	} elseif { $state(loop) == "open" } {
		if { $protected(loop_frame.tt) == 0 } {
			.tp.loop_frame.tt configure -background gray
		}
		if { $protected(loop_frame.wfs) == 0 } {
			.tp.loop_frame.wfs configure -background gray			
		}
	}
	
	if { $state(mode) == "16x" || $state(mode) == "8x" } {
		.tp.gs_frame.ngs configure -background gray
		.tp.gs_frame.lgs configure -background gray		
	} else {
		if { $state(subState) == "ngs" } {
			.tp.gs_frame.ngs configure -background green
			.tp.gs_frame.lgs configure -background gray
		} elseif { $state(subState) == "lgs" } {
			.tp.gs_frame.ngs configure -background gray
			.tp.gs_frame.lgs configure -background green
		}
	}
	
    return [array get state]
}

#==================================================================
#  Base level commands for sending and receiving data on the socket

proc get_state args {
	#
	#  Tells the server to send a state report
	#    The response is handled asynchronously by the recv routine
	#
    global verbose sock get_state_timer halt timeout_timer
    
    if { $halt == 1 } {
        return
    }
	set request "rval=status()"
    if $verbose {puts "status"; flush stdout}
    set timeout_timer [after 2000 catch_timeout]
    set err [catch {puts $sock "$request"}]
        if { $err } {
			puts "<get_state> failed to write socket"
			clear_totem
			connect
			return
			}
    
	set halt 0
    after cancel $timeout_timer

    if { [lindex $args 0] == "-loop"} {
        after cancel $get_state_timer
        set get_state_timer [after 1000 { get_state -loop } ]        
    }

}

proc send {chan request} {
	#
	#  Tells the server (shaneao) to execute a command
	#  given by the text in the argument
	#
	set ::ok 0
	set ::halt 1 ; #halts the periodic status loop
    set timeout_timer [after 2000 catch_timeout]
    set err [catch {puts $chan "$request"}]
        if { $err } {
			puts "<send> failed to write socket"
			clear_totem
			connect
			return
			}
    after cancel $timeout_timer	
}

proc do {} {
	#
	#  Tells the server (shaneao) to execute a command
	#  given by the text in the entry box
	#
    global verbose sock timeout_timer halt
    	
	set request [.tp.ef.ce get]
    if $verbose {puts $request; flush stdout}
    set timeout_timer [after 2000 catch_timeout]
    set err [catch {puts $sock "$request"}]
        if { $err } {
			puts "<status_monitor.do> failed to write socket"
			clear_totem
			connect
			return
			}

    after cancel $timeout_timer
	
}

proc recv {chan} {
	#
	#  Asyncronous receive from the command server socket
	#  This routine is triggered by a socket readable event (as configured by open_sock)
	#
	#     Responses beginning with '\{' are considered state reports and are
	#     sent on to the state interpreter.
	#     All other reponses are simply printed at the terminal since
	#     they are assumed to be just acknowledgements that a command was received.
	#     
	set v [read $chan]
	if {[eof $chan]} {
		puts "<recv> got end-of-file on $chan"
		fileevent $chan readable {}
		clear_totem
		return
	}
	if { [string index $v 0] == "\{" } {
		interp_state $v
	} else {
		puts $v
		set ::ok 1
		set ::halt 0
		get_state -loop; # restarts the periodic status loop
	}
}

#==================================
#   startup
init
clear_totem
check_open_sock
