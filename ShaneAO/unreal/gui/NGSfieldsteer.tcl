#! /usr/bin/wish

#GUI to do basic field steers in NGS mode 

#Global Variables
global cumNS ; set cumNS 0
global cumEW ; set cumEW 0
global absNS ; set absNS 0
global absEW ; set absEW 0
global autoopen ; set autoopen 1
global movetel ; set movetel 1
global movemot ; set movemot 1 

proc moveTelescope {raOffset decOffset} {
    
#convert RA arcsec offset to hours:
set ra_hr [expr $raOffset/15./3600.]

#convert Dec arcsec offset to degrees:
set dec_deg [expr $decOffset/3600.]

# Send commands to move telescope
puts stdout "Moving telescope RA=$raOffset Dec=$decOffset."
exec modify -s poco POCDERE=$dec_deg
exec modify -s poco POCRARC=$ra_hr

puts stdout "Telescope move sent."

# For now not going to wait for Telescope Move to Complete:
set COMMENTED_OUT {
# Set command status to 5
exec modify -s poco POCOCMD = 5

# Wait for moves to complete
set test [lindex [exec show -s poco POCOCMD] 2]

while {$test > 0} {
        set test [lindex [exec show -s poco POCOCMD] 2]
        after 1000
}
puts stdout "Telescope move completed."
}

}

proc fieldsteer {fsoffset} {

    set positions [list Middle E2 E4 E6 E8 E10 W2 W4 W6 W8 S2 S4 S6 S8 N2 N4 N6 N8 N10 S4,E4 S4,E6 S6,E4 S6,E6 S4,W4 S4,W6 S6,W4 S6,W6 N4,W4 N4,W6 N6,W4 N6,E4 N2,E6 N3.5,E5.5 S3,E7]
    set Noffsets [list 0 0 0 0 0 0 0 0 0 0 -2 -4 -6 -8 2 4 6 8 10 -4 -4 -6 -6 -4 -4 -6 -6 4 4 6 6 2 3.5 -3]
    set Eoffsets [list 0 2 4 6 8 10 -2 -4 -6 -8 0 0 0 0 0 0 0 0 0 4 6 4 6 -4 -6 -4 -6 -4 -6 -4 4 6 5.5 7]

#determine absolute field steer position
    set datapos [lsearch -exact $positions $fsoffset]
    set ::absEW [lindex $Eoffsets $datapos]
    set ::absNS [lindex $Noffsets $datapos]

#determine relative telescope move
    set e [expr $::absEW - $::cumEW]
    set n [expr $::absNS - $::cumNS]

#open AO loops before moving anything
exec modify -s saocon loop=open

if {$::movetel} {
    moveTelescope $e $n
    #cum offsets should now equal absolute offsets
    set ::cumNS $::absNS
    set ::cumEW $::absEW
}

if {$::movemot} {
    exec fieldSteer mode=ngs pos=$fsoffset
}
}

# Make GUI

label .fsoffsets -text "Field Steering Motor Offsets"

label .cumNS_l -text "N-S:"
entry .cumNS_e -width 4 -textvariable cumNS -relief flat

label .cumEW_l -text "E-W:"
entry .cumEW_e -width 4 -textvariable cumEW -relief flat

checkbutton .moveTel -text "Move Telescope" -variable movetel
checkbutton .moveMot -text "Move Fieldsteer Motors" -variable movemot
checkbutton .autoOpen -text "Auto Open Loops" -variable autoopen

radiobutton .mid -text "Middle" -variable fsoffset -value "Middle"
radiobutton .n2 -text "N2" -variable fsoffset -value "N2"
radiobutton .n4 -text "N4" -variable fsoffset -value "N4"
radiobutton .n6 -text "N6" -variable fsoffset -value "N6"
radiobutton .n8 -text "N8" -variable fsoffset -value "N8"
radiobutton .n10 -text "N10" -variable fsoffset -value "N10"
radiobutton .s2 -text "S2" -variable fsoffset -value "S2"
radiobutton .s4 -text "S4" -variable fsoffset -value "S4"
radiobutton .s6 -text "S6" -variable fsoffset -value "S6"
radiobutton .s8 -text "S8" -variable fsoffset -value "S8"
radiobutton .e2 -text "E2" -variable fsoffset -value "E2"
radiobutton .e4 -text "E4" -variable fsoffset -value "E4"
radiobutton .e6 -text "E6" -variable fsoffset -value "E6"
radiobutton .e8 -text "E8" -variable fsoffset -value "E8"
radiobutton .e10 -text "E10" -variable fsoffset -value "E10"
radiobutton .w2 -text "W2" -variable fsoffset -value "W2"
radiobutton .w4 -text "W4" -variable fsoffset -value "W4"
radiobutton .w6 -text "W6" -variable fsoffset -value "W6"
radiobutton .w8 -text "W8" -variable fsoffset -value "W8"
radiobutton .s4e4 -text "S4,E4" -variable fsoffset -value "S4,E4"
radiobutton .s4e6 -text "S4,E6" -variable fsoffset -value "S4,E6"
radiobutton .s6e4 -text "S6,E4" -variable fsoffset -value "S6,E4"
radiobutton .s6e6 -text "S6,E6" -variable fsoffset -value "S6,E6"
radiobutton .s4w4 -text "S4,W4" -variable fsoffset -value "S4,W4"
radiobutton .s4w6 -text "S4,W6" -variable fsoffset -value "S4,W6"
radiobutton .s6w4 -text "S6,W4" -variable fsoffset -value "S6,W4"
radiobutton .s6w6 -text "S6,W6" -variable fsoffset -value "S6,W6"
radiobutton .n4w4 -text "N4,W4" -variable fsoffset -value "N4,W4"
radiobutton .n4w6 -text "N4,W6" -variable fsoffset -value "N4,W6"
radiobutton .n6w4 -text "N6,W4" -variable fsoffset -value "N6,W4"
radiobutton .n6w6 -text "N6,W6" -variable fsoffset -value "N6,W6"
radiobutton .n6e4 -text "N6,E4" -variable fsoffset -value "N6,E4"
radiobutton .n2e6 -text "N2,E6" -variable fsoffset -value "N2,E6"
radiobutton .n3e5 -text "N3.5,E5.5" -variable fsoffset -value "N3.5,E5.5"
radiobutton .s3e7 -text "S2,E7" -variable fsoffset -value "S3,E7"

button .onaxis -text "Back On-Axis" -command { fieldsteer Middle }

button .go -text "GO" -command { fieldsteer $fsoffset }

button .aoready -text "AOReady" -command {
catch {exec modify -s saocon loopstate=Settled} result
}

button .quit -text "QUIT" -command { exit }

grid .fsoffsets -row 3 -column 0 -columnspan 4 -sticky w
grid .cumNS_l -row 4 -column 0
grid .cumNS_e -row 4 -column 1
grid .cumEW_l -row 4 -column 2
grid .cumEW_e -row 4 -column 3

grid .moveTel -row 7 -column 0 -columnspan 3 -sticky w
grid .moveMot -row 8 -column 0 -columnspan 3 -sticky w
grid .autoOpen -row 9 -column 0 -columnspan 3 -sticky w

grid .onaxis -row 10 -column 0 -columnspan 2 -sticky w
grid .go -row 10 -column 2 
grid .aoready -row 10 -column 3 
grid .quit -row 10 -column 4 -sticky e

grid .mid .s2 .s4 .s6 .s8
grid .n2 .n4 .n6 .n8 .n10
grid .e2 .e4 .e6 .e8 .e10
grid .w2 .w4 .w6 .w8 
grid .s4e4 .s4e6 .s6e4 .s6e6
grid .s4w4 .s4w6 .s6w4 .s6w6
grid .n4w4 .n4w6 .n6w4 .n6w6
grid .n6e4 .n2e6 .n3e5 .s3e7

