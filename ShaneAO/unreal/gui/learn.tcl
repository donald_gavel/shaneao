#!/usr/bin/env wish

if { $::argc > 0 } {
    set i 1
    foreach arg $::argv {
        puts "argument $i is $arg"
        incr i
    }
} else {
    puts "no command line argument passed"
}

exit
