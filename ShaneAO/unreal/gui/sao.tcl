#!/usr/bin/wish

#make template GUI for SAO

set oc Open
set onoff On
set wfsrate 500
set ttrate 40
set wrate 200
set wfsgain 0.3
set ttgain 0.4
set uplgain 0.5
set centr CM
set wbleed 0.995
set tbleed 0.9
set refcents hs-20140519_16xLGS_onSky.fits
set flat dummy.fits
set offload Off

menu .mbar
. configure -menu .mbar

menu .mbar.fl -tearoff 0
.mbar add cascade -menu .mbar.fl -label File -underline 0
.mbar.fl add command -label "Load Refcent File..." -command {
	puts stdout "Browse for Refcents file name to load" }
.mbar.fl add command -label "Load Flat File..." -command {
	puts stdout "Browse for Flat file name to load" }
.mbar.fl add command -label Exit -command {exit}

menu .mbar.act -tearoff 0
.mbar add cascade -menu .mbar.act -label Actions -underline 0
.mbar.act add command -label "WFS Camera Start" -command { send $::sock "camstart()" }
.mbar.act add command -label "TT Camera Start" -command { send $::sock "ttcamstart()" }
.mbar.act add command -label "WFS Camera Reset" -command { send $::sock "rs()" }
.mbar.act add command -label "TT Camera Reset" -command { send $::sock "trs()" }
.mbar.act add command -label "Make Flat" -command { send $::sock "save_flat()" }

#Messages textbox
frame .textarea
label .message -text "Messages:"
text .txt -yscrollcommand ".srl_y set" -xscrollcommand ".srl_x set" \
-width 50 -height 10
scrollbar .srl_y -command ".txt yview" -orient v
scrollbar .srl_x -command ".txt xview" -orient h


#Loop Control Frame
frame .loop -relief groove

button .loop.wfs -text WFS -command { wfs }
button .loop.tt -text TT -command { tt }
button .loop.upl -text "Uplink TT" -command { uplink }
button .loop.open -text "Open" -command { open_loop }
button .loop.close -text "Close" -command { close_loop }

button .wfsbg -text "WFS Camera Background" -command { send $::sock "dark()" }
button .ttbg -text "TT Camera Background" -command { send $::sock "ttdark()" }

button .wfsreset -text "WFS Camera Reset" -command { send $::sock "rs()" }
button .ttreset -text "TT Camera Reset" -command { send $::sock "trs()" }

#shaneao command entry:
frame .sao
label .saocmd -text "ShaneAO Cmd:"
entry .saocmd_e -textvariable saocmd -width 35

#Configuration Frame
frame .config -relief sunken

#Camera Rates
label .wfsrate1 -text "WFS Rate (Hz):"
#tk_optionMenu .menu_wfsrates wfsrate 1500 1000 750 500 250 100 50
menubutton .menu_wfsrates -textvariable wfsrate -menu .menu_wfsrates.m
menu .menu_wfsrates.m
.menu_wfsrates.m add command -label "1500" -command { send $::sock "rate(1500)" }
.menu_wfsrates.m add command -label "1000" -command {send $::sock "rate(1000)" }
.menu_wfsrates.m add command -label "750" -command {send $::sock "rate(750)" }
.menu_wfsrates.m add command -label "500" -command {send $::sock "rate(500)" }
.menu_wfsrates.m add command -label "250" -command {send $::sock "rate(250)" }
.menu_wfsrates.m add command -label "100" -command {send $::sock "rate(100)" }
.menu_wfsrates.m add command -label "50" -command {send $::sock "rate(50)" }

label .ttrate1 -text "TT Rate (Hz):"
#tk_optionMenu .menu_ttrates ttrate 1000 500 250 125 40
menubutton .menu_ttrates -textvariable ttrate -menu .menu_ttrates.m
menu .menu_ttrates.m
.menu_ttrates.m add command -label "1000" -command {send $::sock "ttrate(1000)" }
.menu_ttrates.m add command -label "500" -command {send $::sock "ttrate(500)" }
.menu_ttrates.m add command -label "750" -command {send $::sock "ttrate(750)" }
.menu_ttrates.m add command -label "125" -command {send $::sock "ttrate(125)" }
.menu_ttrates.m add command -label "50" -command {send $::sock "ttrate(50)" }

#Subap Mode
label .subaps -text "SubAps:"
#tk_optionMenu .menu_subaps subaps 8x 16x
menubutton .menu_subaps -textvariable subaps -menu .menu_subaps.m
menu .menu_subaps.m
.menu_subaps.m add command -label "8x" -command {send $::sock "mode('8xLGS')"}
.menu_subaps.m add command -label "16x" -command {send $::sock "mode('16xLGS')"}

#Modes
label .mode1 -text "Operation Mode:"
#tk_optionMenu .menu_mode mode NGS LGS TT_Only WFS_Only
menubutton .menu_mode -textvariable mode -menu .menu_mode.m
menu .menu_mode.m
.menu_mode.m add command -label "NGS" -command {send $::sock "ngs()"}
.menu_mode.m add command -label "LGS" -command {send $::sock "lgs()"}

#Centroider
label .centr1 -text "Centroider:"
#tk_optionMenu .menu_centr centr CM Quad BinQuad
menubutton .menu_centr -textvariable centr -menu .menu_centr.m
menu .menu_centr.m
.menu_centr.m add command -label "CM" -command {send $::sock "cent('cog')"}
.menu_centr.m add command -label "Quad" -command {send $::sock "cent('quad')"}
.menu_centr.m add command -label "BinQuad" -command {send $::sock "cent('bin')"}

#Loop Gains
label .twgain -text "Tweeter Gain:"
entry .twgain_e -textvariable twgain -width 6
bind .twgain_e <Return> { send $::sock "gain(g=$twgain,wg=$woofgain)" }
label .savedgain -text "Saved Gain:"
entry .twsavedgain -textvariable twsavedgain -width 6 -relief flat

label .woofgain -text "Woofer Gain:"
entry .woofgain_e -textvariable woofgain -width 6
bind .woofgain_e <Return> { send $::sock "gain(g=$twgain,wg=$woofgain)" }
label .savedgain1 -text "Saved Gain:"
entry .woofsavedgain -textvariable woofsavedgain -width 6 -relief flat

label .uplgain1 -text "Uplink Gain:"
entry .uplgain1_e -textvariable uplgain -width 6
bind .uplgain1_e <Return> {
	if { $state(uplink enabled) == True } {
		send $::sock "uplink_gain($uplgain)"		
		}
	}
label .savedgain2 -text "Saved Gain:"
entry .uplsavedgain -textvariable uplsavedgain -width 6 -relief flat -state readonly

label .ttgain1 -text "TT Gain:"
entry .ttgain1_e -textvariable ttgain -width 6
bind .ttgain1_e <Return> { send $::sock "tt_gain($ttgain)" }
label .savedgain3 -text "Saved Gain:"
entry .ttsavedgain -textvariable ttsavedgain -width 6 -relief flat -state readonly

#Bleeds
label .twbleed -text "Tweeter Bleed:"
entry .twbleed_e -textvariable twbleed -width 6
bind .twbleed_e <Return> { send $::sock "bleed('tweeter',$twbleed)" }

label .woofbleed -text "Woofer Bleed:"
entry .woofbleed_e -textvariable woofbleed -width 6
bind .woofbleed_e <Return> { send $::sock "bleed('woofer',$woofbleed)" }

#Offloading
label .offloading -text "Offloading:"
radiobutton .offload_on -text "On" -variable offload -value "On" -command offload
radiobutton .offload_off -text "Off" -variable offload -value "Off" -command offload

#WFS tracking
label .wfstrack -text "WFS Tracking:"
radiobutton .wfstrack_on -text "On" -variable wfstrack -value "On"
radiobutton .wfstrack_off -text "Off" -variable wfstrack -value "Off"

#Calibration Source:
label .cal -text "Calibration Source:"
tk_optionMenu .menu_cal cal "Red Laser" "White Light" "Out"



#Status Frame:

frame .status -relief raised 
#groove ridge solid sunken flat raised

label .subap -text "SubAps:" 
entry .subaps_e -relief flat -textvariable subaps -state readonly

label .mode -text "Mode SubState:"
entry .mode_e -relief flat -textvariable mode -state readonly

label .wfsloop -text "WFS Loop:"
entry .wfsloop_e -relief flat -textvariable oc_wfs -state readonly

label .ttloop -text "TT Loop:"
entry .ttloop_e -relief flat -textvariable oc_tt -state readonly

label .uplloop -text "Uplink TT Loop:" 
entry .uplloop_e -relief flat -textvariable oc_ul -state readonly

label .offload -text "Offloading:"
entry .offload_e -relief flat -textvariable offload -state readonly

label .wfsrate -text "WFS Rate (Hz):"
entry .wfsrate_e -relief flat -textvariable wfsrate -state readonly

label .ttrate -text "TT Rate (Hz):"
entry .ttrate_e -relief flat -textvariable ttrate -state readonly

label .woofrate -text "Woofer Rate (Hz):"
entry .woofrate_e -relief flat -textvariable wrate -state readonly

label .wfsgain -text "WFS Gain:"
entry .wfsgain_e -relief flat -textvariable wfsgain -state readonly

label .ttgain -text "TT Gain:"
entry .ttgain_e -relief flat -textvariable ttgain -state readonly

label .uplgain -text "Uplink Gain:"
entry .uplgain_e -relief flat -textvariable uplgain -state readonly

label .centr -text "Centroider:"
entry .centr_e -relief flat -textvariable centr -state readonly

label .wbleed -text "Woofer Bleed:"
entry .wbleed_e -relief flat -textvariable wbleed -state readonly

label .tbleed -text "Tweeter Bleed:"
entry .tbleed_e -relief flat -textvariable tbleed -state readonly

label .refcent -text "Refcents:"
entry .refcent_e -relief flat -textvariable refcents -state readonly

label .flat -text "Flat file:"
entry .flat_e -relief flat -textvariable flat -state readonly

label .woof -text "Woofer:" 
entry .woof_e -relief flat -textvariable woofer_onoff -state readonly

label .tweet -text "Tweeter:"
entry .tweet_e -relief flat -textvariable tweeter_onoff -state readonly

grid .wfsloop -in .status -row 1 -column 1
grid .wfsloop_e -in .status -row 1 -column 2

grid .ttloop -in .status -row 2 -column 1
grid .ttloop_e -in .status -row 2 -column 2

grid .uplloop -in .status -row 3 -column 1
grid .uplloop_e -in .status -row 3 -column 2

grid .offload -in .status -row 4 -column 1
grid .offload_e -in .status -row 4 -column 2

grid .woof -in .status -row 5 -column 1
grid .woof_e -in .status -row 5 -column 2

grid .tweet -in .status -row 6 -column 1
grid .tweet_e -in .status -row 6 -column 2

grid .wfsrate -in .status -row 7 -column 1
grid .wfsrate_e -in .status -row 7 -column 2

grid .ttrate -in .status -row 8 -column 1
grid .ttrate_e -in .status -row 8 -column 2

grid .woofrate -in .status -row 9 -column 1
grid .woofrate_e -in .status -row 9 -column 2

grid .wfsgain -in .status -row 10 -column 1
grid .wfsgain_e -in .status -row 10 -column 2

grid .ttgain -in .status -row 11 -column 1
grid .ttgain_e -in .status -row 11 -column 2

grid .uplgain -in .status -row 12 -column 1
grid .uplgain_e -in .status -row 12 -column 2

grid .centr -in .status -row 13 -column 1
grid .centr_e -in .status -row 13 -column 2

grid .wbleed -in .status -row 14 -column 1
grid .wbleed_e -in .status -row 14 -column 2

grid .tbleed -in .status -row 15 -column 1
grid .tbleed_e -in .status -row 15 -column 2

grid .refcent -in .status -row 16 -column 1
grid .refcent_e -in .status -row 16 -column 2

grid .flat -in .status -row 17 -column 1
grid .flat_e -in .status -row 17 -column 2

grid .subap -in .status -row 18 -column 1
grid .subaps_e -in .status -row 18 -column 2

grid .mode -in .status -row 19 -column 1
grid .mode_e -in .status -row 19 -column 2

#Loop Control frame

grid .loop.wfs -row 1 -column 3 -columnspan 2 -sticky news
grid .loop.tt -row 1 -column 2 -columnspan 1 -sticky news
grid .loop.upl -row 1 -column 1 -columnspan 1 -sticky news
grid .loop.open -row 2 -column 1 -columnspan 2 -sticky news
grid .loop.close -row 2 -column 3 -columnspan 2 -sticky news

grid .wfsbg -in .loop -row 3 -column 1 -columnspan 2 -sticky news
grid .wfsreset -in .loop -row 3 -column 3 -columnspan 1 -sticky news
grid .ttbg -in .loop -row 4 -column 1 -columnspan 2 -sticky news 
grid .ttreset -in .loop -row 4 -column 3 -columnspan 1 -sticky news

#Config Frame

grid .subaps -in .config -row 1 -column 1
grid .menu_subaps -in .config -row 1 -column 2
grid .mode1 -in .config -row 2 -column 1
grid .menu_mode -in .config -row 2 -column 2

grid .centr1 -in .config -row 3 -column 1
grid .menu_centr -in .config -row 3 -column 2

grid .wfsrate1 -in .config -row 4 -column 1
grid .menu_wfsrates -in .config -row 4 -column 2
grid .ttrate1 -in .config -row 5 -column 1
grid .menu_ttrates -in .config -row 5 -column 2

grid .twgain -in .config -row 6 -column 1
grid .twgain_e -in .config -row 6 -column 2
grid .savedgain -in .config -row 6 -column 3
grid .twsavedgain -in .config -row 6 -column 4
grid .woofgain -in .config -row 7 -column 1
grid .woofgain_e -in .config -row 7 -column 2
grid .savedgain1 -in .config -row 7 -column 3
grid .woofsavedgain -in .config -row 7 -column 4
grid .uplgain1 -in .config -row 8 -column 1
grid .uplgain1_e -in .config -row 8 -column 2
grid .savedgain2 -in .config -row 8 -column 3
grid .uplsavedgain -in .config -row 8 -column 4
grid .ttgain1 -in .config -row 9 -column 1
grid .ttgain1_e -in .config -row 9 -column 2
grid .savedgain3 -in .config -row 9 -column 3
grid .ttsavedgain -in .config -row 9 -column 4

grid .twbleed -in .config -row 10 -column 1
grid .twbleed_e -in .config -row 10 -column 2
grid .woofbleed -in .config -row 11 -column 1
grid .woofbleed_e -in .config -row 11 -column 2

grid .offloading -in .config -row 12 -column 1
grid .offload_on -in .config -row 12 -column 2
grid .offload_off -in .config -row 12 -column 3

grid .wfstrack -in .config -row 13 -column 1
grid .wfstrack_on -in .config -row 13 -column 2
grid .wfstrack_off -in .config -row 13 -column 3

grid .cal -in .config -row 14 -column 1
grid .menu_cal -in .config -row 14 -column 2

#Message frame

grid .message -in .textarea -row 1 -column 1
grid .txt -in .textarea -row 2 -column 1
grid .srl_y -in .textarea -row 2 -column 2 -sticky ns
grid .srl_x -in .textarea -row 3 -column 1 -sticky ew

grid .saocmd -in .sao -row 1 -column 1
grid .saocmd_e -in .sao -row 1 -column 2

grid .config -row 1 -column 1 -rowspan 3
grid .loop -row 1 -column 2
grid .sao -row 2 -column 2
grid .textarea -row 3 -column 2 
grid .status -row 1 -column 3 -rowspan 3

#----------------------------------------------------------------------------------
set refcent_file_list [ list \
	hs-20140506-cog_16x.fits \
	hs-20140506_8x.fits \
	hs-20140506_16x.fits \
	hs-20140506_16x_onSky.fits \
	hs-20140515_16xLGS.fits \
	hs-20140515_16xLGS_onSky.fits \
	hs-2014-05-16_8xLGS.fits \
	hs-2014-05-16_8xLGS_onSky.fits \
	hs-2014-05-18_16xLGS.fits \
	hs-2014-05-18_16xLGS_onSky.fits \
	hs-2014-05-18_8xLGS.fits \
	hs-2014-05-18_8xLGS_onSky.fits \
	hs-2014-05-19_16xLGS.fits \
	hs-2014-05-19_16xLGS_onSky.fits \
	hs-2014-05-19_8xLGS.fits \
	hs-2014-05-19_8xLGS_onSky.fits \
	hs-2014-05-22_16xLGS.fits \
	hs-2014-05-22_16xLGS_onSky.fits \
	hs-2014-05-22_8xLGS.fits \
	hs-2014-05-22_8xLGS_onSky.fits \
	hs-2014-05-23_16xLGS.fits \
	hs-2014-05-23_16xLGS_onSky.fits \
	hs-2014-05-24_16xLGS.fits \
	hs-2014-05-24_16xLGS_onSky.fits \
	hs-2014-05-26_8xLGS.fits \
	hs-2014-05-26_8xLGS_onSky.fits \
]

#--------------------------------------------
#  respond to specific GUI button presses

proc mems {} {
	puts "<mems> you pressed the mems button"
	set s [.tp.mems cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "on" } {
		send $::sock "mems('off')"
	} elseif { $val == "off" } {
		send $::sock "mems('on')"
	}	
}

proc woofer {} {
	puts "<woofer> you pressed the woofer button"
	set s [.tp.woofer cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "on" } {
		send $::sock "woof('off')"
	} elseif { $val == "off" } {
		send $::sock "woof('on')"
	}	
}

proc camera {} {
	puts "<camera> you pressed the camera button"
	set s [.tp.camera cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "stopped" } {
		send $::sock "camstart()"
	} elseif { $val == "running" } {
		send $::sock "camstop()"
	}		
}

proc ttcamera {} {
	puts "<ttcamera> you pressed the ttcamera button"
	set s [.tp.ttcamera cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1] " '"]
	if { $val == "stopped" } {
		send $::sock "ttcamstart()"
	} elseif { $val == "running" } {
		send $::sock "ttcamstop()"
	}		
}

proc loop {} {
	set s [.tp.loop cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1]]
	if { $val == "open" } {
		send $::sock "close()"
	} elseif { $val == "closed" } {
		send $::sock "open()"
	}
}

proc offload {} {
	global offload
	if { $offload == "Off" } {
		send $::sock "offload('off')"
	} elseif { $offload == "On" } {
		send $::sock "offload('on')"
	}	
}

proc freeze {} {
	set s [.tp.frozen cget -text]
	set key_val [split $s ":"]
	set val [string trim [lindex $key_val 1]]
	if { $val == "True" } {
		send $::sock "resume()"
	} elseif { $val == "False" } {
		send $::sock "freeze()"
	}	
}

proc mon {call args} {
	puts "$call is done"
	if { $call == "dark" } {
		set path .tp.df.dk
	} elseif { $call == "ttdark" } {
		set path .tp.df.ttdk
	}
	$path configure -text $call -state enabled
	trace remove variable ::ok write "mon $call"
}

proc dark {} {
	send $::sock "dark()"
	.tp.df.dk configure -text waiting -state disabled
	trace add variable ::ok write {mon dark}
}

proc ttdark {} {
	send $::sock "ttdark()"
	.tp.df.ttdk configure -text waiting -state disabled
	trace add variable ::ok write {mon ttdark}
}

proc bias {} {
	send $::sock "bias0(); bias()"
}

proc flat {} {
	send $::sock "flat0(); flat()"
}

proc tt {} {
	global sock state
	puts "<tt> you pressed the tt button"
	if { $state(loop) == "closed" } {
		if { $state(subState) == "wfs only" } {
			send $sock "tt_close()"
		} elseif { $state(subState) == "tt only" } {
			send $sock "tt_open()"
		} elseif { $state(subState) == "lgs" } {
			send $sock "tt_open()"
		}
	} elseif { $state(loop) == "open" } {
		if { $state(subState) == "lgs" } {
			send $sock "tt_close()"
		}
	}
}

proc wfs {} {
	global sock state
	puts "<wfs> you pressed the wfs button"
	if { $state(loop) == "closed" } {
		if { $state(subState) == "tt only" } {
			send $sock "wfs_close()"
		} elseif { $state(subState) == "wfs only" } {
			send $sock "wfs_open()"
		} elseif { $state(subState) == "lgs" } {
			send $sock "wfs_open()"
		}
	} elseif { $state(loop) == "open" } {
		if { $state(subState) == "lgs" } {
			send $sock "wfs_close()"
		}
	}
}

proc open_loop {} {
	send $::sock "open()"
}

proc close_loop {} {
	send $::sock "close()"
}

proc uplink {} {
	global sock state
	if {$state(uplink enabled) == "True" } {
		if { $state(uplink loop) == "open" } {
			send $::sock "uplink_close()"			
		} else {
			send $::sock "uplink_open()"
		}
	} else {
		puts "<uplink> ERROR uplink must be enabled to do that"
	}
}

proc rate {} {
	global sock state
	set valid_rates [list ask 1500 1300 1000 700 500 250 100 50]
	
	if { ![winfo exists .tp.rate_select] } {
		ttk::combobox .tp.rate_select -values $valid_rates -width 16 -justify center -state readonly -textvariable lab_text(rate)
		grid .tp.rate_select -row 5
	}
	bind .tp.rate_select <Leave> { unselect }
	bind .tp.rate_select <ButtonPress> {
		bind .tp.rate_select <Leave> {}
		after 10 { bind .tp.rate_select <Leave> { unselect } }		
	}
	bind .tp.rate_select <<ComboboxSelected>> {
		set val [.tp.rate_select get]
		destroy .tp.rate_select
		if { $val == "ask" } {
			send $sock "rate(0)"
		} elseif { $val != $state(rate) } {
			send $sock "rate($val)"
		}
	}
}

proc ttrate {} {
	set valid_rates [list ask 40 125 500 1000]
	
	ttk::combobox .tp.tt_rate_select -values $valid_rates -width 16 -justify center -state readonly -textvariable lab_text(ttrate)
	grid .tp.tt_rate_select -row 7
	bind .tp.tt_rate_select <Leave> { unselect }
	bind .tp.tt_rate_select <ButtonPress> {
		bind .tp.tt_rate_select <Leave> {}
		after 10 { bind .tp.tt_rate_select <Leave> { unselect } }		
	}
	bind .tp.tt_rate_select <<ComboboxSelected>> {
		set val [.tp.tt_rate_select get]
		destroy .tp.tt_rate_select
		if { $val == "ask" } {
			send $sock "ttrate(0)"
		} elseif { $val != $state(ttrate) } {
			send $::sock "ttrate($val)"
		}
	}
}

proc cent {} {
	set valid_cents [list cog bin quad]
	
	ttk::combobox .tp.cent_select -values $valid_cents -width 16 -justify center -state readonly -textvariable lab_text(cent)
	grid .tp.cent_select -row 10
	bind .tp.cent_select <Leave> { unselect }
	bind .tp.cent_select <ButtonPress> {
		bind .tp.cent_select <Leave> {}
		after 10 { bind .tp.cent_select <Leave> { unselect } }		
	}
	bind .tp.cent_select <<ComboboxSelected>> {
		set val [.tp.cent_select get]
		destroy .tp.cent_select
		if { $val != $state(cent) } {
			send $::sock "cent('$val')"
		}
	}
}

proc mode {} {
	global sock state
	set valid_modes [list 16xLGS 8xLGS 16x 8x]
	ttk::combobox .tp.mode_select -values $valid_modes -width 16 -justify center -state readonly -textvariable lab_text(mode)
	grid .tp.mode_select -row 8
	bind .tp.mode_select <Leave> { unselect }
	bind .tp.mode_select <ButtonPress> {
		bind .tp.mode_select <Leave> {}
		after 10 { bind .tp.mode_select <Leave> { unselect } }		
	}
	bind .tp.mode_select <<ComboboxSelected>> {
		set val [.tp.mode_select get]
		destroy .tp.mode_select
		if { $val != $state(mode) } {
			send $sock "mode('$val')"
		}
	}
}

proc ngs {} {
	send $::sock "ngs()"
}

proc lgs {} {
	send $::sock "lgs()"
}

proc subState {} {
	global sock state
	puts "<rate> you pressed the subState button"
}

proc unselect {} {
	if { [winfo exists .tp.rate_select] } {
		destroy .tp.rate_select
	}
	if { [winfo exists .tp.tt_rate_select] } {
		destroy .tp.tt_rate_select
	}
	if { [winfo exists .tp.mode_select] } {
		destroy .tp.mode_select
	}	
	if { [winfo exists .tp.cent_select] } {
		destroy .tp.cent_select
	}	
}

#bind . <Key-Escape> { unselect }

#====================================
#  Maintain the socket connection

proc connect {} {
	# This is action in response to the 'connect' button push.
	# It also is invoked if a socket communication interchange times out.
	# 
    close_sock
    check_open_sock
}

proc open_sock {} {
	# create and configure the client-side socket
	#
    global verbose sock halt server
    set port 8889
    if { [catch {set sock [socket $server $port]} ] } {
        if $verbose {puts "<open_sock> cannot connect to $server"}
        #.tp.conn configure -text "not connected"
	puts "<open_sock> not connected"
        set halt 1
        return
    }
    fconfigure $sock -buffering none -blocking 0
    fileevent $sock readable [list recv $sock]
    #.tp.conn configure -text "connected: $server"
    puts "<open_sock> connected to $server"
    set halt 0
}

proc close_sock {} {
	# close the client-side socket connection
	#
    global sock halt get_state_timer
    after cancel $get_state_timer
    catch {
        puts $sock "bye";
        close $sock;  
    }
    set halt 1
    #clear_totem
    #.tp.conn configure -text "not connected"
    puts "<close_sock> socket dropped"
}

proc catch_timeout {} {
    puts "<totem.catch_timeout> socket timed out"
    close_sock
}

set get_state_timer 0
set server_ready_timer 0

proc check_open_sock {} {
	# Opens a socket to the shaneao commandServer and performs actions
	# to keep it connected and providing periodic status reports.
	#
	# If the socket drops, it restarts polling for server availability.
	# If the server connects, it starts polling for shaneao status reports.
	#
    global halt server server_ready_timer
    after cancel $server_ready_timer
    open_sock
    if { $halt } { set server_ready_timer [after 2000 check_open_sock]; return; }
    get_state -loop
}

#=========================================
#   interpret the state report

proc interp_state {state_text} {
	#
	# interpret the state report from the server
	#
	#  argument:
	#    text string from the server
	#  affects:
	#    1. creates 'state', a tcl array with key-value pairing
	#    2. adjusts the highlight colors on the GUI lables accordingly
	#
    global protected state lab_text
    global subaps mode oc_wfs oc_tt oc_ul offload wfsrate ttrate wrate wfsgain ttgain
    global uplgain centr wbleed tbleed refcents flat woofer_onoff tweeter_onoff
	global twgain woofgain twbleed woofbleed
	global twsavedgain woofsavedgain uplsavedgain ttsavedgain
	global offload wfstrack
	
    set state_text [string range $state_text 1 end-1]
    set s_list [split $state_text ","]
    foreach s $s_list {
        set key_val [split $s ":"]
        set key [string trim [lindex $key_val 0] " '\n"]
        set val [string trim [lindex $key_val 1] " '\n"]
		if { [string is double $val ]} {
			set val [expr $val + 0.]
		}
        set state($key) $val
		set lab_text($key) "$key: $val"
    }
    
    set subaps $state(mode)
    set mode $state(subState)
    set oc_ul $state(uplink loop)
    set wfsrate $state(rate)
    set ttrate $state(ttrate)
    set wrate $state(wrate)
    set wfsgain "??"
	switch $state(cent) {
		cog { set centr CM }
		quad { set centr Quad }
		bin { set centr BinQuad }
	}
    set wbleed $state(woofer_bleed)
    set tbleed $state(tweeter_bleed)
    set refcents $state(refCent_filename)
    set flat "??"
    set woofer_onoff $state(woofer)
    set tweeter_onoff $state(mems)
	
	set twsavedgain $state(savedGain)
	set woofsavedgain $state(saved_wGain)
	set uplsavedgain $state(uplink savedGain)
	set ttsavedgain $state(tt saved rGain)
	
	switch $state(offloading) {
		True { set offload "On" }
		False { set offload "Off" }
	}
	
	set wfstrack ""
	
	if { [string is double -strict $twgain] == 0 } { set twgain 0.0 }
	if { [string is double -strict $woofgain] == 0 } { set woofgain 0.0 }
	if { [string is double -strict $twbleed] == 0 } { set twbleed 0.0 }
	if { [string is double -strict $woofbleed] == 0 } { set woofbleed 0.0 }
	if { [string is double -strict $uplgain] == 0 } { set uplgain 0.0 }
	if { [string is double -strict $ttgain] == 0 } { set ttgain 0.0 }

	if { $state(subState) == "ngs" } {
		.loop.tt configure -state disabled
		.loop.wfs configure -state disabled
	} else {
		.loop.tt configure -state normal
		.loop.wfs configure -state normal
	}
	
	if { $state(uplink enabled) == True } {
		.loop.upl configure -state normal
	} else {
		.loop.upl configure -state disabled
	}
	
	switch $state(uplink loop) {
		open { .loop.upl configure -text "Uplink TT" }
		closed { .loop.upl configure -text "*Uplink TT*" }
	}
	
	if { $state(loop) == "closed" } {
		.loop.open configure -text Open
		.loop.close configure -text *Closed*
		if { $state(subState) == "lgs" } {
			.loop.tt configure -background green -text *TT*
			set oc_tt closed
			.loop.wfs configure -background green -text *WFS*
			set oc_wfs closed
		} elseif { $state(subState) == "tt only" } {
			.loop.tt configure -background green -text *TT*
			set oc_tt closed
			.loop.wfs configure -background white -text WFS
			set oc_wfs open
		} elseif { $state(subState) == "wfs only" } {
			.loop.tt configure -background white -text TT
			set oc_tt open
			.loop.wfs configure -background green -text *WFS*
			set oc_wfs closed
		}
	} elseif { $state(loop) == "open" } {
		.loop.open configure -text *Open*
		.loop.close configure -text Close
		.loop.tt configure -background white -text TT
		set oc_tt open
		.loop.wfs configure -background white -text WFS
		set oc_wfs open
	}
	
    return [array get state]
}

#==================================================================
#  Base level commands for sending and receiving data on the socket

proc get_state args {
	#
	#  Tells the server to send a state report
	#    The response is handled asynchronously by the recv routine
	#
    global verbose sock get_state_timer halt timeout_timer
    
    if { $halt == 1 } {
        return
    }
	set request "rval=status()"
    if $verbose {puts "status"; flush stdout}
    set timeout_timer [after 2000 catch_timeout]
    set err [catch {puts $sock "$request"}]
        if { $err } {
			puts "<get_state> failed to write socket"
			#clear_totem
			connect
			return
			}
    
	set halt 0
    after cancel $timeout_timer

    if { [lindex $args 0] == "-loop"} {
        after cancel $get_state_timer
        set get_state_timer [after 1000 { get_state -loop } ]        
    }

}

proc send {chan request} {
	#
	#  Tells the server (shaneao) to execute a command
	#  given by the text in the argument
	#
    set ::ok 0
    set ::halt 1 ; #halts the periodic status loop
    set timeout_timer [after 2000 catch_timeout]
	puts "<send> $request"
    set err [catch {puts $chan "$request"}]
    if { $err } {
		puts "<send> failed to write socket"
		#clear_totem
		connect
		return
	}
    after cancel $timeout_timer	
}

proc do {} {
	#
	#  Tells the server (shaneao) to execute a command
	#  given by the text in the entry box
	#
    global verbose sock timeout_timer halt
    	
    set request [.saocmd_e get]
    if $verbose {puts $request; flush stdout}
    set timeout_timer [after 2000 catch_timeout]
    set err [catch {puts $sock "$request"}]
        if { $err } {
			puts "<status_monitor.do> failed to write socket"
			#clear_totem
			connect
			return
			}

    after cancel $timeout_timer
	
}

bind .saocmd_e <Return> do 

proc recv {chan} {
	#
	#  Asyncronous receive from the command server socket
	#  This routine is triggered by a socket readable event (as configured by open_sock)
	#
	#     Responses beginning with '\{' are considered state reports and are
	#     sent on to the state interpreter.
	#     All other reponses are simply printed at the terminal since
	#     they are assumed to be just acknowledgements that a command was received.
	#     
	set v [read $chan]
	if {[eof $chan]} {
		puts "<recv> got end-of-file on $chan"
		fileevent $chan readable {}
		#clear_totem
		return
	}
	if { [string index $v 0] == "\{" } {
		interp_state $v
	} else {
		puts $v
		set ::ok 1
		set ::halt 0
		get_state -loop; # restarts the periodic status loop
	}
}

#==================================
#   startup
set verbose 0 ; # spew out messages of every attempt to communicate with the server
set server localhost
check_open_sock
