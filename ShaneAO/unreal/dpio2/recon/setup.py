# setup.py: Build and instgall the rtc2 extension.

from distutils.core import setup, Extension
import numpy as np

setup(
	name='dpio2',
	version='1.0',
	description = 'ShaneAO dpio2->tweeter DM recon rtc module',
	author='Don Gavel',
	author_email='gavel@ucolick.org',
	url='http://lao.ucolick.org/ShaneAO/',
	requires = ['numpy'],
	ext_modules = [
		Extension(
			"dpio2",
			sources = ["dpio2.c"],
			include_dirs = ['.','../../../dpio2/include','../../../dpio2/src/x86-linux-2.6.x/include',np.get_include()],
			library_dirs = ['/usr/local/lick/lib','/opt/kroot/lib'],
			extra_link_args = ['-Wl,-rpath,/usr/local/lick/lib','-Wl,-rpath,/opt/kroot/lib'],
			libraries = ['dpio2','pthread','rt']
			)
		],
	)
