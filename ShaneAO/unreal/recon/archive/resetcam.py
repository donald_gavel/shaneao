# resetcam.py
#  scripts to reset the cameras from scratch, regardless how RTC goofed them up

import os
import numpy as np
import readfits

def resetTT(d):
    os.system('/opt/EDTpdv/initcam -u 0 -f TipTilt_LittleJoe.cfg')
    os.system('/opt/EDTpdv/serial_cmd -u 0 "@RCL 1"')
    os.system('/opt/EDTpdv/serial_cmd -u 0 "@SEQ 1"')
    os.system('/opt/EDTpdv/take -u 0 -l 1 -f ttcam')
    os.system('/opt/EDTpdv/serial_cmd -u 0 "@SEQ 0"')
    u = np.fromfile('ttcam.raw',dtype=np.uint16)
    u = u.reshape((80,80))
    d.set_np2arr(u)

def resetWFS(d):
    map = readfits.readfits('/home/gavel/cvs/lroot/ShaneAO/unreal/recon/parameterFiles/i_map.fits')
    map = map.reshape((160*160))
    os.system('/opt/EDTpdv/initcam -u 1 -f WFS_BigJoe.cfg')
    os.system('/opt/EDTpdv/serial_cmd -u 1 "@RCL 4"')
    os.system('/opt/EDTpdv/serial_cmd -u 1 "@SEQ 1"')
    os.system('/opt/EDTpdv/take -u 1 -l 1 -f cam')
    os.system('/opt/EDTpdv/serial_cmd -u 1 "@SEQ 0"')
    u = np.fromfile('cam.raw',dtype=np.uint16)
    u = u[map].reshape((160,160))
    d.set_np2arr(u)
