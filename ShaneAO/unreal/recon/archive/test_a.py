import numpy as np
import readfits
import ds9

n = 80
nframes = 14

map = readfits.readfits('/home/gavel/cvs/lroot/ShaneAO/unreal/recon/parameterFiles/i_map.fits')
map = map.reshape((160*160))

for k in range(nframes):
    name = '/home/gavel/tts0_'+str(k).zfill(8)+'.raw'
    r = np.fromfile(name,dtype=np.uint16)
    if (n == 160):
        u = r[map].reshape((160,160))
    else:
        u = r.reshape((n,n))
    d.set_np2arr(u)
