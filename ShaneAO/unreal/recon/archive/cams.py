# cams.py

import edt
import ds9
import readfits

wfs = edt.Camera(1,'/home/gavel/WFS_BigJoe.cfg')
h = edt.Handler(wfs,'saveAsRaw').configure(['wfs0',2.0],'configSaveAsRaw')
wfs.handler(h)

tts = edt.Camera(0,'/home/gavel/TipTilt_LittleJoe.cfg')
h2 = edt.Handler(tts,'saveAsRaw').configure(['tts0',2.0],'configSaveAsRaw')
tts.handler(h2)

u = wfs.peek()
v = tts.peek()

map = readfits.readfits('/home/gavel/cvs/lroot/ShaneAO/unreal/recon/parameterFiles/i_map.fits')
map = map.reshape((160*160))

d = ds9.ds9()

def show(u,d,map=0):
    if (map == 0):
        d.set_np2arr(u)
    else:
        d.set_np2arr(u.reshape((160*160))[map].reshape((160,160)))

    