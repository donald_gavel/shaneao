# read and display a fits file
import os
from numpy import *
import pyfits

def readfits(filename):
    """Reads a FITS file from disk given the file name."""
    
    if (os.path.isfile(filename) == False):
        print "ERROR readfits cannot find file "+filename
        return array([0.])
    hdulist = pyfits.open(filename)
    data = hdulist[0].data
    hdulist.close()
    return data

def writefits(data,filename):
    """Writes a FITS file given the data."""

    if (os.path.isfile(filename) == True):
        print "ERROR writefits won't over write existing file "+filename
        return
    hdu = pyfits.PrimaryHDU(data)
    hdu.writeto(filename)
    