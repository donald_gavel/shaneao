import rtc
import rtc2
import numpy as np
from readfits import *
from dimg import *
import sys
import time

u = rtc.rtc('16x')

# scramble the simulated data
pdir = 'parameterFiles/'
wfs = readfits(pdir+'sim_wfs_image_16x.fits')
i_map = readfits(pdir+'i_map.fits')
wfs_s = np.zeros((160*160))
wfs_s[i_map] = wfs
wfs_s = wfs_s.reshape((160,160))

# load it into the reconstructor
d = wfs_s.astype(np.uint16)
rtc2.recon(d)
wfs2 = rtc2.peek('wfs')
s2 = rtc2.peek('s')
inten2 = rtc2.peek('inten')

# look at the result
#dimg(wfs2[0:3600].reshape((144,5,5)))

# take the reference centroids
refcent = rtc2.peek('s').copy()
rtc2.load('s_ref',refcent)

u.close_loop()
u.set_gain(50000.)
rtc2.recon(d)
da2 = rtc2.peek('da') # should be zero even though loop is closed

# turn on integrator bleeds and perturb the state vector
integrator_bleeds = np.ones((1090))*0.99
rtc2.load('integrator_bleeds',integrator_bleeds.astype(double))
#a = np.zeros((1090))
#rtc2.load('a',a.astype(double))
a2 = rtc2.peek('a')
a02 = rtc2.peek('a0')

# propagate for 10 steps
nt = 5000
ne = 10.
aset = []

print 'starting'
sys.stdout.flush()
t0 = time.time()

# test response to noise
aset.append(a2.copy())
for k in range(nt):
    n = np.random.normal(0.,ne,(160,160)).astype(int16)  # add noise
    if (k > nt/2): n = 0
    rtc2.recon((d+n).astype(uint16))
    aset.append(a2.copy())

## flat out speed test
#for k in range(nt):
#    rtc2.recon(d)


t1 = time.time()
print 'done'
sys.stdout.flush()
print 'it took ',t1-t0,'seconds for ',nt,' iterations'

if (len(aset) != 0):
    aset = np.array(aset)
    aset = aset[:,0:1024].reshape((nt+1,32,32))
    aset[:,0,0]=0.
    aset[:,31,31]=43000.
    plt.plot(aset[:,11,20]-aset[:,10,10])
    #plt.plot(np.ones((10))*a02[0])
    plt.show()
