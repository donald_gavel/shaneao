/*****************************************************************************

 FILE: mmult_tick.c
 DESCRIPTION:

 AUTHOR: Don Gavel
 LAST REVISED: 02/24/2013
 
   see https://computing.llnl.gov/tutorials/pthreads/#Mutexes
          http://http://pages.cs.wisc.edu/~travitch/pthreads_primer.html   (semaphores)

******************************************************************************/
#include <semaphore.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef __MACH__
#include "barrier.h"
#endif

typedef struct 
 {
   double      *A;  /* na x ns matrix */
   double      *b;  /* ns input vector */
   double      *c;  /* na output vector */
   int         ns;  /* length of b */
   int         na;  /* length of c */
   int        len;  /* the length of the piece of c that is to be done by each thread. Must divide na evenly */
   int      count;  /* countdown - 0 indicates all threads are done */
 } DATASTRUCT;

#define NUMTHRDS 16
#define NLOOP 1000

int attr_realTime(pthread_attr_t  *pattr, int policy, int priority);

DATASTRUCT data; 
double sec[NLOOP];
int sem_sig_count;

pthread_t terminal_inputThread;
pthread_t clockTickThread;
pthread_t threads[NUMTHRDS];
pthread_barrier_t barrier;
pthread_barrier_t barrier2;
sem_t clockTicked;

int state;
#define GO 1
#define STOP_AND_READ 2
#define EXIT 0

void *clockTicker(void *arg) /* and orchestrator of state */
{
    int i, j;
	struct timespec start, end;
	double total_time;
        double min, max, average; 
        const int histSize = 10;
        int histCount[histSize];
        double histMax[histSize];
        double histInterval;

	state = STOP_AND_READ;
	while ( state != EXIT ) {
            if (state == GO) {
                sem_sig_count = 0;
                total_time = 0.0;
                for ( i=0; i<NLOOP; i++) { /* runs the rtc in a rapid inner loop while only occassionally polling for a state change */
                    clock_gettime(0,&start);
                    sem_post(&clockTicked); /* triggers the mmult threads */
                    //		printf("<clockTicker> ------------- tick. i = %d --------------\n",i);
                    //mywait(1); /* wait one millisecond = 1 / 1kHz frame rate;  or wait for camera interrupts here */
                    pthread_barrier_wait(&barrier2);
                    clock_gettime(0,&end);
                    sec[i] = ((double)(end.tv_sec-start.tv_sec))+((double) (end.tv_nsec - start.tv_nsec)) * 1.0e-9;
                    total_time += sec[i];
                }
		//	state = EXIT;  // remove before flight
		
			
                printf("\n<clockticker> total_time = %g seconds  count=%d\n",total_time, sem_sig_count);
                average = total_time/(double)NLOOP;
                min = 100000000000.0;
                max = -100000000000.0;
                for (i=0; i<NLOOP; i++) {
                    if (min > sec[i]) min = sec[i];
                    if (max < sec[i]) max = sec[i];
                }
                printf("min=%f\n", min);
                printf("max=%f\n", max);

                
                histInterval = max/(double)histSize;
                for (j=0; j<histSize; j++) {
                    histCount[j] = 0;
                    histMax[j] = histInterval*(j + 1);
                }

                for (j=0; j < NLOOP; j++) {
                    double val =  sec[j];
                    for (i=0; i < histSize; i++) {
                        if (val <= histMax[i]) {
                            histCount[i]++;
                            i = histSize + 1;
                        }
                    }
                }
                
                printf("\n\n");
                printf("avg=%f\n", average);
                printf("min=%f\n", min);
                printf("max=%f\n", max);
                printf("\n");
                for (j=0; j<histSize; j++) {
                    printf("sec=%f  c=%d\n", histMax[j], histCount[j]);
                } 
                printf("\n\n");
                    
            } else {
                sleep(1); /* wait idle for one second, then poli the state that might be changed by user input */
            }
	}
	sem_post(&clockTicked); /* trigger all the mmult threads one final time so they complete */
	printf("<clockTicker> exiting\n");
	pthread_exit((void*) 0);

}

void *terminal_input(void *arg) {  /* separate thread that handles input from the terminal */

	ssize_t read;
	size_t len = 0;
	char * line = NULL;

	state = STOP_AND_READ;
	printf("<terminal_input> options are:\n    GO\n    STOP\n    EXIT\n    HELP\n");

	while ( state != EXIT ) {

		printf("<terminal_input> prompt: ");
		read = getline(&line, &len, stdin);

		switch (line[0]) {
			case 'G':
			case 'g':
				state = GO;
				printf("<terminal_input> GO\n");
				break;
			case 'S':
			case 's':
				state = STOP_AND_READ;
				printf("<terminal_input> STOP_AND_READ\n");
				break;
			case 'E':
			case 'e':
				state = EXIT;
				printf("<terminal_input> EXIT\n");
				break;
			default:
				printf("<terminal_input> options are:\n    GO\n    STOP\n    EXIT\n    HELP\n");    
				break;
		}
		if (line) {
			free(line);
			line = NULL;
		}
	}
	printf("<terminal_input> exiting\n");
	pthread_exit((void*) 0);
}

int mywait(int ms) /* wait for a given number of milliseconds */
{
  long i, j, n;
  n = 306000;  // timed on rtc to take 1 ms
  for ( j=0; j<ms; j++) {
    for ( i=0; i<n; i++) {}
  }
}

void *mmult(void *segment) /* c = Ab  computed in parallel */
{
/*
 mmult is the (partial) matrix multiply, assigned to this thread
 the argument tells which segment to compute
*/

  long offset, start, end;
/*  int n_cycles = NLOOP;  /* the test version of this code will exit after this many iterations */
  int i,j,k, na,ns,len;
  double *A, *b, *c;
  double checksum;
  
  na = data.na;
  ns = data.ns;
  len = data.len; /* number of rows assigned to this thread */
  offset = (long)segment * data.len; /* this is the offset into c, and the starting row index of A */
  start = offset;
  end = offset + data.len;
  
  A = data.A + data.ns*offset;
  b = data.b;
  c = data.c;// + offset;

//  for (k = 0; k<n_cycles; k++)
  while ( 1 ) 
  {
      //printf("seg=%d\n", segment);
    if ( segment == 0 ) {
	sem_wait(&clockTicked);
        sem_sig_count++;
	//printf("<mmult (%ld)> ----------- down-tick -------------- \n",segment);
    }
    pthread_barrier_wait(&barrier);
    if ( state == EXIT ) break;
//    printf("<mmult (%ld)> starting operation cycle %d\n",segment,k);
    for (j=start; j<end ; j++)   /* row number and result vector index */
    {
      c[j] = 0.;
      for (i=0; i<ns ; i++)   /* index through the sensor vector */
      {
          c[j] += (A[i] * b[i]);   /*  <------------ the computer will spend most of its life exectuting this line of code */
      }
    }
    pthread_barrier_wait(&barrier2);
  }
  printf("<mmult (%ld)> exiting\n",(long)segment);
  pthread_exit((void*) 0);
}

/*

============================ The main program ===========================

*/

int main (int argc, char *argv[])
{
  pthread_attr_t attr;
  int len,ns,na;
  long i,n;
  double *A, *b, *c;
  void *status;
  int root;

  root = 0;
  if (getuid() == 0){
      root = 1;
  }
  
  /* --- parameters --- */
  ns = 2*16*16; // number of sensors (b)
  na = 1024;    // number of actuators (c)
  len = na / NUMTHRDS ;  // evenly divide the output vector into pieces for each thread
  
  A = (double*) malloc ((long)ns*(long)na*sizeof(double));
  b = (double*) malloc (ns*sizeof(double));
  c = (double*) malloc (na*sizeof(double));
  
  printf("<Main> Initializing data\n");
  n = (long)ns*(long)na;
  for (i=0; i<n; i++) { A[i] = 1.; }
  for (i=0; i<ns; i++) { b[i] = 1.; }
  for (i=0; i<na; i++) { c[i] = 0.; }
  
  printf("<Main> b[0] = %g\n",b[0]);
  
  data.A = A; 
  data.b = b; 
  data.c = c;
  data.ns = ns;
  data.na = na;
  data.len = len;
  data.count = NUMTHRDS;
  
  printf("<Main> Creating Mutexs, Conditiions, and threads\n");

  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  if (root) {
      printf("Raising priority for root!\n");
      if (attr_realTime( &attr, SCHED_RR, 1)) {
          exit(-1);
      }
  }
  pthread_barrier_init(&barrier,NULL,NUMTHRDS);
  pthread_barrier_init(&barrier2,NULL,NUMTHRDS+1);
  sem_init(&clockTicked, 0, 0);

  pthread_create(&terminal_inputThread,&attr,terminal_input,NULL);
  pthread_create( &clockTickThread,&attr,clockTicker,NULL);
  for(i=0;i<NUMTHRDS;i++)
  {
    pthread_create(&threads[i], &attr, mmult, (void *)i); 
  }
  pthread_attr_destroy(&attr);
//  printf("<Main> now waiting for threads to join\n");   interfers with the prompt

  for(i=0;i<NUMTHRDS;i++)
  {
    pthread_join(threads[i], &status);
  }
  printf("<Main> mmult threads done \n");

  pthread_join(clockTickThread,&status);
  printf("<Main> clockTick thread done\n");
  pthread_join(terminal_inputThread,&status);
  printf("<Main> terminal_input thread done\n");
  free (A);
  free (b);
  free (c);

  pthread_barrier_destroy(&barrier);
  pthread_barrier_destroy(&barrier2);
  sem_destroy(&clockTicked);
  pthread_exit(NULL);
}


int attr_realTime(pthread_attr_t  *pattr, int policy, int priority)
{    
    struct sched_param schedParam;
    int status;
    int inheritsched;
    // set the values in m_Attr
    
    inheritsched = PTHREAD_EXPLICIT_SCHED;

    if (pthread_attr_setinheritsched(pattr, inheritsched)) {
        printf("failed to set priority, pthread_attr_setinheritsched\n");
        return -1;
    }
    
    if (pthread_attr_setschedpolicy(pattr, policy)) {
        printf("failed to set priority, pthread_attr_setschedpolicy\n");
        return -1;
    }

    if (pthread_attr_getschedparam(pattr, &schedParam)) {
        printf("failed to set priority, pthread_attr_getschedparam\n");
        return -1;
    }

    schedParam.sched_priority = priority;
    if (pthread_attr_setschedparam(pattr, &schedParam)) {
        printf("failed to set priority, pthread_attr_setschedparam\n");
        return -1;
    }

    return 0;
}
