# testCams.py
#  Start up and test the wavefront sensor and tip/tilt sensor cameras
#   Run this in a blank directory that has space for the dumped camera frames

import edt
from resetcam import *

if ('wfs' in globals()):
    del wfs
    del h
    del u

wfs = edt.Camera(1,'~/cvs/lroot/ShaneAO/unreal/config/WFS_BigJoe.cfg')
h = edt.Handler(wfs,'saveAsRaw')
h.configure(['wfs0',2.0],'configSaveAsRaw') # save frame every 2 seconds to wfs0_########.raw
wfs.handler(h)
u = wfs.peek() # u is a numpy array "peek" at the data

tts = edt.Camera(0,'~/cvs/lroot/ShaneAO/unreal/config/TipTilt_LittleJoe.cfg')
h2 = edt.Handler(tts,'saveAsRaw')
h2.configure(['tts0',2.0],'configSaveAsRaw') # save frame every 2 seconds to tts0_#######.raw
tts.handler(h2)
v = tts.peek() # v is a numpy array "peek" at the data

##==========
##  Here's the next generation script that will set up the cameras to call the real-time
##  reconstructor
#
## rtc2 will have to be compled as a handler, ostensibly by puting rtc2.c into the hander
## directory or somehow merging the edt.so and rtc2.so shared object libraries
#
#import rtc
#import rtc2
#
#u = rtc.rtc('16x') # loads all the parameters for the RTC
#
#configDir = '~/cvs/lroot/ShaneAO/unreal/config/'
#
#wfs = edt.Camera(1,configDir+'WFS_BigJoe.cfg')
#h = edt.Handler(wfs,'c_recon') # attaches the reconstructor in RTC2 to the wfs
## assume we don't need to configure this handler
## we don't need to move camera data back to wfs object, instead we peek at it in rtc2
#data = rtc2.peek('wfs')
