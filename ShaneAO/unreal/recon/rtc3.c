/*****************************************************************************

 FILE: rtc2.c
 DESCRIPTION:
  Real-Time-Control engine
 AUTHOR: Don Gavel
 LAST REVISED: 04/14/2014

   see https://computing.llnl.gov/tutorials/pthreads/#Mutexes
       http://http://pages.cs.wisc.edu/~travitch/pthreads_primer.html   (semaphores)
       http://nedbatchelder.com/text/whirlext.html (C interface to python)

 create with
  python setup.py install --home=$HOME/local

******************************************************************************/

#include "Python.h"
#include "numpy/arrayobject.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openblas/cblas.h>
#include <dlfcn.h>
#include <complex.h>
#include <fftw3.h>

void c_recon(void *,unsigned short *);
void c_ftr(void *,unsigned short *);
void c_hybrid(void *,unsigned short *);
void c_tipTiltRecon(void *, unsigned short *);

void *(*dpio2_output_function)(PyObject *,double *,long *); /* pointer to the dpio2.so function that will write to the tweeter DM */
PyObject *dpio2_instance;

fftw_plan p_sx;
fftw_plan p_sy;
fftw_plan p_est;
fftw_plan p_ph;

/* global data structure for rtc parameters and data */
typedef struct
{
  int		na, ns, npix, nsub;	/* na = number of states, ns = number of subapertures, npix = reduced width of wfs camera (40,80,160) depending on wfs mode, nsub = width of subaperture pixel region */
  int		nfilt, na_tw, na_woof;	/* breakdown of state vector: nfilt = number of woofer modes that are filtered, na_tw = #of tweeter actuators, na_woof = #of woofer actuators */
  double	*wfs_background;	/* background image for the wavefront sensor npix x npix */
  double	*wfs_flat;		/* flat field for the wavefront sensor npix x npix */
  double	*wfs_ma_alpha;	/* wavefront sensor moving average alpha */
  double	*centWts;		/* centroid weights  nsub x nsub x 3*/
  double    *centReg;       /* centroid regularization, one for wfs, one for tt */
  long		*i_map;			/* map from camera pixels to illuminated subaps */
  long		*g_map;			/* map from [mode] subaps to ng x ng array for fftw */
  double	*s_ref;			/* reference centroids, ns*2 vector */
  double	*integrator_bleeds;	/* integrator bleed coefficients, one per state variable */
  double	*aa_filt;		/* anti-aliasing filter coefficients, 2-vector = (alpha,beta) */
  double	*a0;			/* actuator default positions - state-vector zero points, na */
  double	*a_limit;		/* state-vector upper and lower limits */
  double	*controlMatrix;		/* (na+nfilt) x (2*ns+nfilt) reconstruction matrix */
  double	*controlMatrix_phase;		/* (na+nfilt) x (ns+nfilt) reconstruction matrix for use with phase */
  double *controlMatrix_slopes;  /* reconstruction matrix for use with slopes, to create phase. */
  long		*tweeter_map;		/* map from tweeter actuator index to dpio2 buffer */
  int		ntt;			/* ntt = number of pixels across tip/tilt camera (80) */
  int		ntts;			/* ntts = number of outputs from the tip/tilt sensor (2) */
  double	*tt_background;		/* background image for the tip/tilt sensor ntt x ntt (LGS mode) */
  double	*tt_flat;		/* flat field for the tip/tilt sensor ntt x ntt */
  double	*tt_centWts;		/* controid weight for tip/tilt ntt x ntt x 3 */
  double	*tts_ma_alpha;	/* tip/tilt sensor moving average alpha */
  fftw_complex *gx_ft; /* Filter for x-slopes */
  fftw_complex *gy_ft; /* Filter for y-slopes */
  fftw_complex *gd_ft; /* Filter for y-slopes */
  int ng; /* number of points along a grid of each side of the padded fft (64) */
} PARAMSTRUCT;

typedef struct
 {
  double	*wfs;			/* wfs camera data npix x npix */
  double	*wfs_ma;		/* wfs camera moving average */
  double	*wfs_subaps;		/* wfs camera data subap regions only */
  double	*s;			/* ns*2 vector of slopes (centroids) */
  double	*inten;			/* ns vector of subap intensities */
  double	*da;			/* na output vector of delta commands from reconstructor */
  double	*a;			/* na state vector = actuator commands */
  double	*tts;			/* tip/tilt sensor data ntt x ntt */
  double	*tts_ma;		/* tip/tilt sensor moving average ntt x ntt */
  double	*tt_inten;		/* tip/tilt intensity 1 */
  double	*dtt;			/* output vector of delta-tip/tilt commands (LGS mode) */
  double  *sg;    /* grid of slopes for fourier transform */
  fftw_complex  *x_ft; /* ns 2D array of slopes in X */
  fftw_complex  *y_ft; /* ns 2D array of slopes in Y */
  fftw_complex *e_ft; /* 2D complex array of phase */
  double  *e; /* 2D array of phase */
  double  *ei; /* 2D array of phase */
 } DATASTRUCT;

PARAMSTRUCT param;
DATASTRUCT data;

typedef struct {
  char *name;
  void **ptr;
  long usize; /* actually used size, in bytes */
  long size; /* in bytes */
} param_def;

param_def param_dict[100];
param_def size_params[20];
param_def data_dict[100];

double *telemetry_buffer;
int telem_index, telem_increment, telem_buffer_stride, max_telem_index;

int state;
#define GO 1
#define STOP_AND_READ 2
#define EXIT 0

int verbosity;


/*
 *	init()
 *		initialize the names, pointers, and  (maximum) sizes in the data and parameter dictionaries
 *		then allocate the memory for each
 *
 *		all data space is preallocated at this init step
 *		so provide room for the largest case
*/

void init() {
  char *pname;
  int k, l;
  param_def *p;
  void *q;
//  void *dlhandle;

  /* fixed (or default) parameters */
  param.npix = 160;
  param.nsub = 5;
  param.ntt = 80;
  param.na_tw = 1024;
  param.na_woof = 52;
  param.ntts = 0;
  param.ng = 64;

  p = size_params;
  p->name = "na";     p->ptr = (void *)&param.na;      p->size = 1L*sizeof(int); p++;
  p->name = "ns";     p->ptr = (void *)&param.ns;      p->size = 1L*sizeof(int); p++;
  p->name = "ng";     p->ptr = (void *)&param.ng;      p->size = 1L*sizeof(int); p++;
  p->name = "npix";   p->ptr = (void *)&param.npix;    p->size = 1L*sizeof(int); p++;
  p->name = "nsub";   p->ptr = (void *)&param.nsub;    p->size = 1L*sizeof(int); p++;
  p->name = "nfilt";  p->ptr = (void *)&param.nfilt;   p->size = 1L*sizeof(int); p++;
  p->name = "na_tw";  p->ptr = (void *)&param.na_tw;   p->size = 1L*sizeof(int); p++;
  p->name = "na_woof";p->ptr = (void *)&param.na_woof; p->size = 1L*sizeof(int); p++;
  p->name = "ntt";    p->ptr = (void *)&param.ntt;     p->size = 1L*sizeof(int); p++;
  p->name = "ntts";   p->ptr = (void *)&param.ntts;    p->size = 1L*sizeof(int); p++;
  p->name = "";

  p = param_dict;
  p->name = "controlMatrix"; 	p->ptr = (void *)&param.controlMatrix; 	p->size = 1000L*2000L*sizeof(double); 	p++;
  p->name = "controlMatrix_phase"; 	p->ptr = (void *)&param.controlMatrix_phase; 	p->size = 1000L*2000L*sizeof(double); 	p++;
  p->name = "controlMatrix_slopes"; p->ptr = (void *)&param.controlMatrix_slopes; p->size = 1000L*2000L*sizeof(double);   p++;
  p->name = "wfs_background"; 	p->ptr = (void *)&param.wfs_background; p->size = 160L*160L*sizeof(double); 	p++;
  p->name = "wfs_ma_alpha";	p->ptr = (void *)&param.wfs_ma_alpha; 	p->size = 1L*sizeof(double);		p++;
  p->name = "wfs_flat"; 	p->ptr = (void*)&param.wfs_flat; 	p->size = 160L*160L*sizeof(double); 	p++;
  p->name = "centWts";		p->ptr = (void*)&param.centWts; 	p->size = 5*5*3*sizeof(double); 	p++;
  p->name = "centReg";		p->ptr = (void*)&param.centReg; 	p->size = 2*sizeof(double); 		p++;
  p->name = "i_map";		p->ptr = (void*)&param.i_map;		p->size = 160L*160L*sizeof(long);	p++;
  p->name = "g_map";    p->ptr = (void*)&param.g_map;		p->size = 64L*64L*sizeof(long);	p++;
  p->name = "s_ref";		p->ptr = (void*)&param.s_ref;		p->size = 2*1000L*sizeof(double);	p++;
  p->name = "integrator_bleeds"; p->ptr = (void*)&param.integrator_bleeds; p->size = (1024+52+52)*sizeof(double); p++;
  p->name = "aa_filt"; 		p->ptr = (void*)&param.aa_filt; 	p->size = 2*sizeof(double); 		p++;
  p->name = "a0"; 		p->ptr = (void*)&param.a0; 		p->size = (1024+52+52)*sizeof(double); 	p++;
  p->name = "a_limit";		p->ptr = (void*)&param.a_limit;		p->size = (1024+52+52)*2*sizeof(double); p++;
  p->name = "tweeter_map";	p->ptr = (void*)&param.tweeter_map;	p->size = (1024)*sizeof(long);		p++;
  p->name = "tt_background"; 	p->ptr = (void*)&param.tt_background; 	p->size = 80L*80L*sizeof(double); 	p++;
  p->name = "tt_flat"; 		p->ptr = (void*)&param.tt_flat; 	p->size = 80L*80L*sizeof(double); 	p++;
  p->name = "tt_centWts"; 	p->ptr = (void*)&param.tt_centWts; 	p->size = 80L*80L*3*sizeof(double); 	p++;
  p->name = "tts_ma_alpha";	p->ptr = (void*)&param.tts_ma_alpha;	p->size = 1L*sizeof(double);		p++;
  p->name = "gx_ft"; p->ptr = (void*)&param.gx_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "gy_ft"; p->ptr = (void*)&param.gy_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "gd_ft"; p->ptr = (void*)&param.gd_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "";

  for (k=0; k<100; k++) {
    pname = param_dict[k].name;
    if (!strcmp(pname,"")) {break;}
    q = malloc(param_dict[k].size);
    if (q == NULL) {
      printf("<rtc2.c init> ERROR allocating memory for %s\n",pname);
      exit(EXIT_FAILURE);
    }
    *(param_dict[k].ptr) = q;
    param_dict[k].usize = 0;
  }

  p = data_dict;
  p->name = "wfs";		p->ptr = (void *)&data.wfs;		p->size = 160L*160L*sizeof(double);	p++;
  p->name = "wfs_ma";	p->ptr = (void *)&data.wfs_ma;	p->size = 160L*160L*sizeof(double); p++;
  p->name = "wfs_subaps";	p->ptr = (void *)&data.wfs_subaps;	p->size = 160L*160L*sizeof(double);	p++;
  p->name = "s";		p->ptr = (void *)&data.s;		p->size = 3*1000L*sizeof(double);	p++;
  p->name = "sg";		p->ptr = (void *)&data.sg;		p->size = 2*64*64L*sizeof(double);	p++;
  p->name = "x_ft"; p->ptr = (void *)&data.x_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "y_ft"; p->ptr = (void *)&data.y_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "e_ft"; p->ptr = (void *)&data.e_ft; p->size = 4096L*sizeof(fftw_complex); p++;
  p->name = "e"; p->ptr = (void *)&data.e; p->size = (4096L+52+52)*sizeof(double); p++;
  p->name = "ei"; p->ptr = (void *)&data.ei; p->size = 3*1000L*sizeof(double); p++;
  p->name = "inten";		p->ptr = (void *)&data.inten;		p->size = 2*1000L*sizeof(double);	p++;
  p->name = "da";		p->ptr = (void *)&data.da;		p->size = (1024+52+52+2)*sizeof(double); p++;
  p->name = "a";		p->ptr = (void *)&data.a;		p->size = (1024+52+52+2)*sizeof(double); p++;
  p->name = "tts";		p->ptr = (void *)&data.tts;		p->size = 80L*80L*sizeof(double);	p++;
  p->name = "tts_ma";	p->ptr = (void *)&data.tts_ma;	p->size = 80L*80L*sizeof(double);		p++;
  p->name = "tt_inten";		p->ptr = (void *)&data.tt_inten;	p->size = 1*sizeof(double);		p++;
  p->name = "dtt";		p->ptr = (void *)&data.dtt;		p->size = 2*sizeof(double);		p++;
  p->name = "";

  for (k=0; k<100; k++) {
    pname = data_dict[k].name;
    if (!strcmp(pname,"")) {break;}
    l = strlen(pname);
    if ((l >= 2) && strcmp(&pname[l-2], "ft")) {
      q = fftw_malloc(data_dict[k].size);
    } else {
      q = malloc(data_dict[k].size);
    }
    if (q == NULL) {
      printf("<rtc2.c init> ERROR allocating memory for %s\n",pname);
      exit(EXIT_FAILURE);
    }
    *(data_dict[k].ptr) = q;
    data_dict[k].usize = 0;
  }

  dpio2_output_function = NULL;
  dpio2_instance = (PyObject *)NULL;

  telem_index = 0;
  telem_increment = 0;
  max_telem_index = 4097; /* bit bucket plus 4096 chunks of data stream */
  telemetry_buffer = (double *)NULL;

  verbosity = 0;
}

/*
 *	param_entry(name)
 *		lookup the data or parameter entry given the name
 *		returns a pointer to the data entry in the data or parameter table (param_def)
 */

param_def *param_entry(char *name) {
  char *pname;
  int k;
  for (k=0; k<100; k++) {
    pname = size_params[k].name;
    if (!strcmp(pname,"")) { break; }
    if (!strcmp(pname,name)) { return &size_params[k]; }
  }
  for (k=0; k<100; k++) {
    pname = param_dict[k].name;
    if (!strcmp(pname,"")) { break; }
    if (!strcmp(pname,name)) { return &param_dict[k]; }
  }
  for (k=0; k<100; k++) {
    pname = data_dict[k].name;
    if (!strcmp(pname,"")) { break; }
    if (!strcmp(pname,name)) { return &data_dict[k]; }
  }
  return 0;
}

/*
 *	param_list()
 *	  list all the data and parameter names, pointers, and sizes
*/

static PyObject *
param_list() {
  char *pname;
  int k;

  for (k=0; k<20; k++) {
    pname = size_params[k].name;
    if (!strcmp(pname,"")) { break ; }
    printf("<rtc2.c param_list> parameter %s : %p %d\n",pname,*size_params[k].ptr,*(int *)size_params[k].ptr);
  }
  for (k=0; k<100; k++) {
    pname = param_dict[k].name;
    if (!strcmp(pname,"")) { break ; }
    printf("<rtc2.c param_list> parameter %s : %p %ld bytes\n",pname,*param_dict[k].ptr,param_dict[k].size);
  }
  for (k=0; k<100; k++) {
    pname = data_dict[k].name;
    if (!strcmp(pname,"")) { break ; }
    printf("<rtc2.c param_list> data %s : %p %ld bytes\n",pname,*data_dict[k].ptr,data_dict[k].size);
  }
  printf("<rtc2.c param_list> just to check, param.controlMatrix has the value %p\n",param.controlMatrix);
  printf("<rtc2.c param_list> just to check, param.wfs_background has the value %p\n",param.wfs_background);
  return Py_BuildValue("");
}

/*
 *	hello_world(...)
 *		"are you alive" check from python
 */

static PyObject *
hello_world(PyObject *self, PyObject *args)
{
    return Py_BuildValue("s", "rtc2 (LGS version) says hello, world!");
}

/*
 *	set_state(aState)
 *		set the rtc state
 */

static PyObject *
set_state(PyObject *self, PyObject *args)
{
    //PyTypeObject *PyInt_Type;
    int aState;

    if (!PyArg_ParseTuple(args, "i", &aState)) { return NULL; }

    if ((aState < 0) || (aState > 2)) {
        PyErr_SetString(PyExc_ValueError,"state must be 0, 1, or 2");
        return NULL;
    }

    state = aState;
    return Py_BuildValue("i",state);
}

/*
 *	get_state()
 *		query the rtc state
 */

static PyObject *
get_state(PyObject *self, PyObject *args)
{
    return Py_BuildValue("i",state);
}

static PyObject *
set_sizes(PyObject *self, PyObject *args)
{
  if (!PyArg_ParseTuple(args, "(iiiiiiii)",&param.na, &param.ns, &param.npix, &param.nsub, &param.nfilt, &param.na_tw, &param.na_woof, &param.ntt))
  {
    return NULL;
  }
  if (verbosity != 0) {
    printf("<rtc2.c set_sizes> set sizes:\n");
    printf("na = %d\n",param.na);
    printf("ns = %d\n",param.ns);
    printf("npix = %d\n",param.npix);
    printf("nsub = %d\n",param.nsub);
    printf("nfilt = %d\n",param.nfilt);
    printf("na_tw = %d\n",param.na_tw);
    printf("na_woof = %d\n",param.na_woof);
    printf("ntt = %d\n",param.ntt);
  }
  return Py_BuildValue("i",8);
}

static PyObject *
set_size(PyObject *self, PyObject *args)
{
  char *name, *pname;
  int val, k;

  if (!PyArg_ParseTuple(args, "si",&name,&val)) {
    PyErr_SetString(PyExc_ValueError,"args must be string and integer");
    return NULL;
  }

  for (k=0; k<20; k++) {
    pname = size_params[k].name;
    if (!strcmp(pname,"")) {
      PyErr_SetString(PyExc_ValueError,"argument not one of the size parameters.");
      /* PyErr_Format(PyExc_ValueError,"argument %s not one of the size parameters.",name); */
    }
    if (!strcmp(pname,name)) {
      *(int *)size_params[k].ptr = val;
      break;
    }
  }
  return Py_BuildValue("i",val);

}

static PyObject *
set_verbosity(PyObject *self, PyObject *args)
{
  if (!PyArg_ParseTuple(args,"i",&verbosity)) {
    return NULL;
  }
  return Py_BuildValue("");
}

/*
 *	load(name,array)
 *		load values from the numpy array into the named location in the parameter set
 */

static PyObject *
load(PyObject *self, PyObject *args)
{
    PyArrayObject *array;
    param_def *p_entry;
    char *name;
    char err_str[80];
    double *p0, *p, *q;

    long n,m;
    long nm,k,nmp;

    if (!PyArg_ParseTuple(args, "sO!", &name, &PyArray_Type, &array)) return NULL;
    if (array->nd > 2) {
      PyErr_SetString(PyExc_ValueError,"array must be one or two-dimensional");
      return NULL;
    }
    if (array->descr->type_num != PyArray_DOUBLE) {
	PyErr_SetString(PyExc_ValueError,"array must be of type float64");
	return NULL;
    }

    p_entry = param_entry(name);
    if (!p_entry) {
      snprintf(err_str,80,"data entry %s not found",name);
      PyErr_SetString(PyExc_ValueError,err_str);
      return NULL;
    }
    p0 = *(p_entry->ptr);
    p = p0;
    q = (double *)array->data;
    if (verbosity != 0) printf("<rtc2.c load> %s -> %p\n",name,p);

    n = array->dimensions[0];
    m = 1;
    if (array->nd == 2) {
      m = array->dimensions[1];
    }
    nm = (long)n*(long)m;
    p_entry->usize = nm*sizeof(double);
    if (p_entry->usize > p_entry->size) {
      snprintf(err_str,80,"array %s size %ld is larger than allocated space %ld",name,p_entry->usize,p_entry->size);
      PyErr_SetString(PyExc_ValueError,err_str);
      return NULL;
    }
    for (k=0; k<nm; k++) {
      *p++ = *q++;
    }

    if (verbosity != 0) {
      p = p0;
      nmp = nm > 3 ? 3 : nm;
      printf("<rtc2.c load> %ld elements (%ld bytes) loaded: [",nm,p_entry->usize);
      for (k = 0; k<nmp; k++) {
	printf("%g, ",*p++);
      }
      if (nm > nmp) printf("...");
      printf("]\n");
    }

    return Py_BuildValue("");
}

static PyObject *
cload(PyObject *self, PyObject *args)
{
  PyArrayObject *array;
  param_def *p_entry;
  char *name;
  char err_str[80];
  fftw_complex *p0, *p, *q;

  long n,m;
  long nm,k,nmp;

  if (!PyArg_ParseTuple(args, "sO!", &name, &PyArray_Type, &array)) return NULL;
  if (array->nd > 2) {
    PyErr_SetString(PyExc_ValueError,"array must be one or two-dimensional");
    return NULL;
  }
  if (array->descr->type_num != PyArray_CDOUBLE) {
PyErr_SetString(PyExc_ValueError,"array must be of type complex128");
return NULL;
  }

  p_entry = param_entry(name);
  if (!p_entry) {
    snprintf(err_str,80,"data entry %s not found",name);
    PyErr_SetString(PyExc_ValueError,err_str);
    return NULL;
  }
  p0 = *(p_entry->ptr);
  p = p0;
  q = (fftw_complex *)array->data;
  if (verbosity != 0) printf("<rtc2.c load> %s -> %p\n",name,p);

  n = array->dimensions[0];
  m = 1;
  if (array->nd == 2) {
    m = array->dimensions[1];
  }
  nm = (long)n*(long)m;
  p_entry->usize = nm*sizeof(fftw_complex);
  if (p_entry->usize > p_entry->size) {
    snprintf(err_str,80,"array %s size %ld is larger than allocated space %ld",name,p_entry->usize,p_entry->size);
    PyErr_SetString(PyExc_ValueError,err_str);
    return NULL;
  }
  for (k=0; k<nm; k++) {
    *p++ = *q++;
  }
  return Py_BuildValue("");
}

char allocate_fftw_doc[] = 
  
  "Allocate the plans used by FFTW for the Fourier Transfrom Reconstructor."
  "This must be done after the sizes are set by set_sizes and set_size.";

static PyObject *
allocate_fftw(PyObject *self, PyObject *args)
{
  printf("Preparing FFTW plans.");
  p_sx = fftw_plan_dft_r2c_2d(param.ng, param.ng, data.sg, data.x_ft, FFTW_MEASURE);
  p_sy = fftw_plan_dft_r2c_2d(param.ng, param.ng, data.sg + (param.ng * param.ng), data.y_ft, FFTW_MEASURE);
  p_est = fftw_plan_dft_c2r_2d(param.ng, param.ng, data.e_ft, data.e, FFTW_MEASURE);
  p_ph = fftw_plan_dft_r2c_2d(param.ng, param.ng, data.e, data.e_ft, FFTW_MEASURE);
  printf("Prepared FFTW plans.");
  return Py_BuildValue("");
}

char allocate_telemetry_doc[] =

  "Allocate memory for the telemetry buffer."
  "This can only be done **after** calls to set_sizes and set_size"
  "in order to define param.ns and param.ntts";

static PyObject *
allocate_telemetry(PyObject *self, PyObject *args)
{
    long n;

    telem_buffer_stride = (2*param.ns + param.ntts + param.na);
    n = (long)telem_buffer_stride*(long)max_telem_index*sizeof(double);
    if (telemetry_buffer != (double *)NULL) {
      free(telemetry_buffer);
    }
    telemetry_buffer = malloc(n);
    if (telemetry_buffer == NULL) {
      PyErr_Format(PyExc_MemoryError,"<rtc2.c load> ERROR allocating memory for telemetry buffer %ld bytes",n);
      return NULL;
    }
    
    printf("Allocated telemetry buffer at %p %ld bytes", telemetry_buffer, n);

    return Py_BuildValue("");
}

char telemetry_data_doc[] =

  "Start taking telemetry data.";

static PyObject*
telemetry_data(PyObject *self, PyObject *args)
{
  if (telem_index != 0) {
    PyErr_SetString(PyExc_RuntimeError,"<rtc2.telemetry_data> buffer not ready (possibly still taking data from the last request)");
    return NULL;
  }
  telem_increment = 1;
  return Py_BuildValue("i",telem_increment);
}

char telemetry_ready_doc[] =

  "Check if the telemetry buffer is ready to collect more data.";

static PyObject*
telemetry_ready(PyObject *self, PyObject *args)
{
  return Py_BuildValue("i",1-telem_increment);
}

char telemetry_peek_doc[] =

  "Create a python numpy array with a data pointer that points to the telemetry buffer."
  "This is so python can write the buffer to disk.";

static PyObject *
telemetry_peek(PyObject *self, PyObject *args)
{
  PyObject *peek_array;
  npy_intp dims[2];

  if (telemetry_buffer == NULL) {
    PyErr_SetString(PyExc_RuntimeError,"<rtc2.telemetry_peek> telemetry buffer not allocated yet.");
    return NULL;
  }
  dims[0] = max_telem_index;
  dims[1] = 2*param.ns + param.ntts + param.na;
  peek_array = PyArray_SimpleNewFromData(2,dims,NPY_DOUBLE,(void *)telemetry_buffer);
  return peek_array;
}

/*
 *	type(object)
 *		describe the type of the array object as seen at the c interface
 */
static PyObject *
type(PyObject *self, PyObject *args)
{
  PyArrayObject *array;
  long *p;

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;

  printf("<rtc2.c type> type_num = %d\n",array->descr->type_num);

  printf("<rtc2.c type> sizeof(int) = %ld\n",sizeof(int));
  printf("<rtc2.c type> sizeof(short) = %ld\n",sizeof(short));
  printf("<rtc2.c type> sizeof(long) = %ld\n",sizeof(long));

  printf("<rtc2.c type> PyArray_INT = %d\n",PyArray_INT);
  printf("<rtc2.c type> PyArray_SHORT = %d\n",PyArray_SHORT);
  printf("<rtc2.c type> PyArray_LONG = %d\n",PyArray_LONG);

  //p = (int*)array->data;
  p = (long *)array->data;
  printf("<rtc2.c type> data: %ld, %ld\n",*p, *(p+1));

  return Py_BuildValue("");
}
/*
 *	iload(name)
 *		load one of the integer map arrays
 */
static PyObject *
iload(PyObject *self, PyObject *args)
{
    PyArrayObject *array;
    param_def *p_entry;
    char *name;
    char err_str[80];
    long *p0, *p, *q;

    long n,m;
    long nm,k,nmp;

    if (!PyArg_ParseTuple(args, "sO!", &name, &PyArray_Type, &array)) return NULL;

    if (array->nd > 2) {
      PyErr_SetString(PyExc_ValueError,"array must be one or two-dimensional");
      return NULL;
    }
    if (array->descr->type_num != PyArray_LONG) {
	printf("<rtc2.c iload> type_num = %d, PyArray_LONG = %d\n",array->descr->type_num,PyArray_LONG);
	PyErr_SetString(PyExc_ValueError,"array must be of type python int = c long");
	return NULL;
    }

    p_entry = param_entry(name);
    if (!p_entry) {
      snprintf(err_str,80,"data entry %s not found",name);
      PyErr_SetString(PyExc_ValueError,err_str);
      return NULL;
    }
    p0 = (long *)(*(p_entry->ptr));
    p = p0;
    q = (long *)array->data;
    if (verbosity != 0) printf("<rtc2.c iload> %s -> %p\n",name,p);

    n = array->dimensions[0];
    m = 1;
    if (array->nd == 2) {
      m = array->dimensions[1];
    }
    nm = (long)n*(long)m;

    p_entry->usize = nm*sizeof(long);
    if (p_entry->usize > p_entry->size) {
      snprintf(err_str,80,"array %s size %ld is larger than allocated space %ld",name,p_entry->usize,p_entry->size);
      PyErr_SetString(PyExc_ValueError,err_str);
      return NULL;
    }
    for (k=0; k<nm; k++) {
      *p++ = *q++;
    }

    if (verbosity != 0) {
      p = p0;
      nmp = nm > 3 ? 3 : nm;
      printf("<rtc2.c iload> %ld elements (%ld bytes) loaded: [",nm,p_entry->usize);
      for (k = 0; k<nmp; k++) {
	printf("%ld, ",*p++);
      }
      if (nm > nmp) printf("...");
      printf("]\n");
    }

    return Py_BuildValue("");
}

/*
 *	peek(name)
 *		get values from the named data set
 *
 *		note: this puts the pointer to the data into the returned array
 *		thus "peek" also gives you "poke"
 *		peek can be used on (integer) maps so long as their name ends in "_map"
 */

static PyObject *
peek(PyObject *self, PyObject *args)
{
    PyObject *peek_array;
    npy_intp dims[1];
    param_def *p_entry;
    char *name;
    char err_str[80];
    int nd;
    double *p;

    if (!PyArg_ParseTuple(args, "s", &name)) return NULL;

    p_entry = param_entry(name);
    if (!p_entry) {
      snprintf(err_str,80,"data entry %s not found",name);
      PyErr_SetString(PyExc_ValueError,err_str);
      return NULL;
    }
    p = *(p_entry->ptr);

    if (verbosity != 0) printf("<rtc2.c peek> %s -> %p\n",name,p);

    nd = 1;
    if (strstr(name,"_map")) {
      dims[0] = p_entry->usize/sizeof(long);
      peek_array =  PyArray_SimpleNewFromData(nd,dims,NPY_LONG,(void *)p);
      return peek_array;
    }
    if (strstr(name, "_ft")) {
      dims[0] = p_entry->usize/sizeof(fftw_complex);
      peek_array =  PyArray_SimpleNewFromData(nd,dims,NPY_CDOUBLE,(void *)p);
      return peek_array;
    }
    dims[0] = p_entry->usize/sizeof(double);
    peek_array =  PyArray_SimpleNewFromData(nd,dims,NPY_DOUBLE,(void *)p);
    return peek_array;
}

static PyObject *
set_dpio2_output(PyObject *self, PyObject *args)
{
  char *error=NULL;

  if (!PyArg_ParseTuple(args, "O", &dpio2_instance)) return NULL;

  if (dpio2_instance != (PyObject *)Py_None) {
    dlerror();
    dpio2_output_function = dlsym(RTLD_DEFAULT, "c_output_write");
    printf("pointer to dpio2_output_function = %p",dpio2_output_function);
  } else {
    dpio2_output_function = NULL;
    printf("<rtc2.c set_dpio2_output> dpio2_output_function is set to NULL\n");
  }

  if (dpio2_output_function == NULL) {
    error = dlerror ();

    if (error != NULL) {
      PyErr_SetString (PyExc_ValueError, error);
    } else {
      PyErr_Format (PyExc_ValueError, "unknown error loading function c_output_write");
    }

  }

  return Py_BuildValue("");
}

static PyObject *
trace(PyObject *self, PyObject *args)
{
    PyArrayObject *array;
    double sum;
    long i, n;

    if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;
    if (array->nd != 2 || array->descr->type_num != PyArray_DOUBLE) {
	PyErr_SetString(PyExc_ValueError,
	"array must be two-dimensional and of type float");
	return NULL;
    }

    n = array->dimensions[0];
    if (n > array->dimensions[1]) n = array->dimensions[1];
    sum = 0.;
    for (i = 0; i < n; i++)
	sum += *(double *)(array->data + i*array->strides[0] + i*array->strides[1]);

    return PyFloat_FromDouble(sum);
}

/*
  recon is the python-callable reconstructor wrapper (wrapping a call to c_recon). Arguments are camera data from WFS
  passed as a numpy array. Result is a change to the state vector, which you'll have to
  look at with peek commands.
*/
static PyObject *
recon(PyObject *self, PyObject *args)
{
  char errStr[80];
  PyArrayObject *array;

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;
  if (array->nd != 2 || array->descr->type_num != NPY_UINT16) {
      PyErr_SetString(PyExc_ValueError,
      "array must be two-dimensional and of type NPY_UINT16");
      return NULL;
  }
  if ((array->dimensions[0] != param.npix) || (array->dimensions[1] != param.npix)) {
    snprintf(errStr,80,"array must be size of camera: %d x %d",param.npix,param.npix);
    PyErr_SetString(PyExc_ValueError,errStr);
    return NULL;
  }
  printf("Calling c_recon from python recon");
  c_recon((void *)NULL,(unsigned short *)array->data);

  return Py_BuildValue("");
}

/*
  TipTiltRecon is the python-callable reconstructor wrapper (wrapping a call to c_TipTiltRecon). Arguments are camera data from TT
  passed as a numpy array. Result is a change to the state vector, which you'll have to
  look at with peek commands.
*/
static PyObject *
tipTiltRecon(PyObject *self, PyObject *args)
{
  char errStr[80];
  PyArrayObject *array;

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;
  if (array->nd != 2 || array->descr->type_num != NPY_UINT16) {
      PyErr_SetString(PyExc_ValueError,
      "array must be two-dimensional and of type NPY_UINT16");
      return NULL;
  }
  if ((array->dimensions[0] != param.ntt) || (array->dimensions[1] != param.ntt)) {
    snprintf(errStr,80,"array must be size of camera: %d x %d",param.ntt,param.ntt);
    PyErr_SetString(PyExc_ValueError,errStr);
    return NULL;
  }
  printf("<rtc2.tipTiltRecon> called 1\n");
  c_tipTiltRecon((void *)NULL,(unsigned short *)array->data);

  return Py_BuildValue("");
}

static PyObject *
hybrid(PyObject *self, PyObject *args)
{
  char errStr[80];
  PyArrayObject *array;

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;
  if (array->nd != 2 || array->descr->type_num != NPY_UINT16) {
      PyErr_SetString(PyExc_ValueError,
      "array must be two-dimensional and of type NPY_UINT16");
      return NULL;
  }
  if ((array->dimensions[0] != param.npix) || (array->dimensions[1] != param.npix)) {
    snprintf(errStr,80,"array must be size of camera: %d x %d",param.npix,param.npix);
    PyErr_SetString(PyExc_ValueError,errStr);
    return NULL;
  }
  printf("Calling c_hybrid from python hybrid");
  c_hybrid((void *)NULL,(unsigned short *)array->data);

  return Py_BuildValue("");
}

/*
  recon is the python-callable reconstructor wrapper (wrapping a call to c_recon). Arguments are camera data from WFS
  passed as a numpy array. Result is a change to the state vector, which you'll have to
  look at with peek commands.
*/
static PyObject *
ftr(PyObject *self, PyObject *args)
{
  char errStr[80];
  PyArrayObject *array;

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;
  if (array->nd != 2 || array->descr->type_num != NPY_UINT16) {
      PyErr_SetString(PyExc_ValueError,
      "array must be two-dimensional and of type NPY_UINT16");
      return NULL;
  }
  if ((array->dimensions[0] != param.npix) || (array->dimensions[1] != param.npix)) {
    snprintf(errStr,80,"array must be size of camera: %d x %d",param.npix,param.npix);
    PyErr_SetString(PyExc_ValueError,errStr);
    return NULL;
  }
  printf("Calling c_ftr from python ftr");
  c_ftr((void *)NULL,(unsigned short *)array->data);

  return Py_BuildValue("");
}

void c_ftr(void *handler, unsigned short *d)
{
  int j, k, n, ncol, nrow, n_ft;
  double *sx, *sy, *inten, *s_ref_x, *s_ref_y, *sgx, *sgy;
  double *efilt, *afilt, *ett, *e, *ei;
  double *cx, *cy, *ci, *pdata;
  double *hi_limit, *low_limit;
  double sx_temp, sy_temp, inten_temp;
  double r;
  fftw_complex *e_ft, *gx_ft, *gy_ft, *sx_ft, *sy_ft, *denom;
  
  /* copy the camera data directly into a buffer that python can see */
  n = param.npix*param.npix;
  for (k = 0; k<n; k++) data.wfs[k] = (double)d[k]; /* or just set the pointer? data.wfs = d */
  /* keep a running average, which might be useful for centering on a dim guide star */
  for (k = 0; k<n; k++) data.wfs_ma[k] = param.wfs_ma_alpha[0] * data.wfs_ma[k] + (1. - param.wfs_ma_alpha[0]) * data.wfs[k];

  /* camera data background subtraction, de interlacing, and flat-fielding */
  //TODO: Why aren't darks and flats deinterlaced here? Do they come pre-deinterlaced?
  n = param.nsub*param.nsub*param.ns;
  for (k = 0; k<n; k++) data.wfs_subaps[k] = ((double)d[param.i_map[k]] - param.wfs_background[k])*param.wfs_flat[k];

  /* centroider */
  sx = data.s;
  sy = data.s+param.ns;
  sgx = data.sg;
  sgy = data.sg + (param.ng * param.ng);
  inten = data.inten;
  s_ref_x = param.s_ref;
  s_ref_y = param.s_ref+param.ns;
  n = param.nsub*param.nsub;
  cx = param.centWts;
  cy = param.centWts+n;
  ci = param.centWts+2*n;
  pdata = data.wfs_subaps;
  r = param.centReg[0];  
  for (k = 0; k<param.ns; k++) {
    sx_temp = 0.;
    sy_temp = 0.;
    inten_temp = 0.;
    for (j = 0; j<n; j++) {
      sx_temp += cx[j]*pdata[j];
      sy_temp += cy[j]*pdata[j];
      inten_temp += ci[j]*pdata[j];
    }
    pdata += n;
    inten[k] = (inten_temp > 1.0) ? inten_temp : 0.0;
    sx[k] = sx_temp/(inten[k]+r) - s_ref_x[k];
    sy[k] = sy_temp/(inten[k]+r) - s_ref_y[k];
        
    // Re-grid and effectively bin the slopes back onto the 160x160 camera...
    sgx[param.g_map[k]] = sx[k];
    sgy[param.g_map[k]] = sy[k];
  }
  
  // Forward FFT the slopes
  fftw_execute(p_sx);
  fftw_execute(p_sy);
  
  n_ft = (param.ng / 2) + 1;
  
  e_ft = data.e_ft;
  gx_ft = param.gx_ft;
  gy_ft = param.gy_ft;
  sx_ft = data.x_ft;
  sy_ft = data.y_ft;
  denom = param.gd_ft;
  // Perform the FTR reconstruction.
  for (k = 0; k < (param.ng * n_ft); k++) {
    e_ft[k] = (gx_ft[k] * sx_ft[k] + gy_ft[k] * sy_ft[k]) / denom[k];
  }
  //TODO: Fix FFT normalization here? Perhaps implicitly in denom?
  fftw_execute(p_est);
  
  ei = data.ei;
  e = data.e;
  for (k = 0; k <param.ns; k++) {
    ei[k] = e[param.g_map[k]];
  }
  
  
  /* copy filter states to appended e vector */
  efilt = data.ei+param.ns;
  afilt = data.a+param.na_tw+param.na_woof;
  for (k = 0; k<param.nfilt; k++) {
    efilt[k] = afilt[k];
  }

  /* copy tip/tilt sensor measurements to appended e vector */
  ett = efilt + param.nfilt;
  for (k = 0; k<param.ntts; k++) {
	  ett[k] = data.dtt[k];
  }
  
  // Now we have the phase, we can turn this into actuator commands using a control matrix.
  /* reconstruction */
  nrow = param.na; /* [tweeter, woofer, filter, uplinkTT] */
  ncol = param.ns+param.nfilt+param.ntts; /* [Phase, filter, TTS] */
  cblas_dgemv(CblasRowMajor, CblasNoTrans, nrow, ncol, 1.0, param.controlMatrix_phase, ncol, data.ei, 1, 0.0, data.da, 1);
  
  /* integrators */
  low_limit = param.a_limit;
  hi_limit = param.a_limit+param.na;
  for (k = 0; k<param.na; k++) {
    data.a[k] = (data.a[k] - param.a0[k])*param.integrator_bleeds[k] + data.da[k] + param.a0[k];
    data.a[k] = (data.a[k] > hi_limit[k]) ? hi_limit[k] : data.a[k];
    data.a[k] = (data.a[k] < low_limit[k]) ? low_limit[k] : data.a[k];
  }
  
  /* send the integrator values to the DM */
  if (dpio2_output_function) {
    dpio2_output_function (dpio2_instance,data.a,param.tweeter_map);
  }
  
}

void
c_hybrid(void *handler, unsigned short *d)
{
  int j, k, n, ncol, nrow, n_ft;
  double *sx, *sy, *inten, *s_ref_x, *s_ref_y, *sgx, *sgy;
  double *efilt, *afilt, *ett, *e, *ei;
  double *cx, *cy, *ci, *pdata;
  double *hi_limit, *low_limit;
  double sx_temp, sy_temp, inten_temp;
  double r;
  fftw_complex *e_ft, *gx_ft, *gy_ft, *sx_ft, *sy_ft, *denom;
  
  /* copy the camera data directly into a buffer that python can see */
  n = param.npix*param.npix;
  for (k = 0; k<n; k++) data.wfs[k] = (double)d[k]; /* or just set the pointer? data.wfs = d */
  /* keep a running average, which might be useful for centering on a dim guide star */
  for (k = 0; k<n; k++) data.wfs_ma[k] = param.wfs_ma_alpha[0] * data.wfs_ma[k] + (1. - param.wfs_ma_alpha[0]) * data.wfs[k];

  /* camera data background subtraction, de interlacing, and flat-fielding */
  //TODO: Why aren't darks and flats deinterlaced here? Do they come pre-deinterlaced?
  n = param.nsub*param.nsub*param.ns;
  for (k = 0; k<n; k++) data.wfs_subaps[k] = ((double)d[param.i_map[k]] - param.wfs_background[k])*param.wfs_flat[k];

  /* centroider */
  sx = data.s;
  sy = data.s+param.ns;
  sgx = data.sg;
  sgy = data.sg + (param.ng * param.ng);
  inten = data.inten;
  s_ref_x = param.s_ref;
  s_ref_y = param.s_ref+param.ns;
  n = param.nsub*param.nsub;
  cx = param.centWts;
  cy = param.centWts+n;
  ci = param.centWts+2*n;
  pdata = data.wfs_subaps;
  r = param.centReg[0];  
  for (k = 0; k<param.ns; k++) {
    sx_temp = 0.;
    sy_temp = 0.;
    inten_temp = 0.;
    for (j = 0; j<n; j++) {
      sx_temp += cx[j]*pdata[j];
      sy_temp += cy[j]*pdata[j];
      inten_temp += ci[j]*pdata[j];
    }
    pdata += n;
    inten[k] = (inten_temp > 1.0) ? inten_temp : 0.0;
    sx[k] = sx_temp/(inten[k]+r) - s_ref_x[k];
    sy[k] = sy_temp/(inten[k]+r) - s_ref_y[k];
  }
  
  // Once we have the slopes, we can reconstruct phase from a control matrix.
  nrow = param.ns; /* [phase, filter, uplinkTT] */
  ncol = param.ns*2; /* [slopes, filter, TTS] */
  cblas_dgemv(CblasRowMajor, CblasNoTrans, nrow, ncol, 1.0, param.controlMatrix_slopes, ncol, data.s, 1, 0.0, data.ei, 1);
  
  ei = data.ei;
  e = data.e;
  // Now we need to re-grid the phase.
  for (k = 0; k <param.ns; k++) {
    e[param.g_map[k]] = ei[k];
  }
  
  // Transform from e to e_ft
  fftw_execute(p_ph);
  
  // Do any hybrid work here...
  
  // Transform from e_ft back to e
  fftw_execute(p_est);
  
  ei = data.ei;
  e = data.e;
  for (k = 0; k <param.ns; k++) {
    ei[k] = e[param.g_map[k]];
  }
  
  
  /* copy filter states to appended e vector */
  efilt = data.ei+param.ns;
  afilt = data.a+param.na_tw+param.na_woof;
  for (k = 0; k<param.nfilt; k++) {
    efilt[k] = afilt[k];
  }

  /* copy tip/tilt sensor measurements to appended e vector */
  ett = efilt + param.nfilt;
  for (k = 0; k<param.ntts; k++) {
	  ett[k] = data.dtt[k];
  }
  
  // Now we have the phase, we can turn this into actuator commands using a control matrix.
  /* reconstruction */
  nrow = param.na; /* [tweeter, woofer, filter, uplinkTT] */
  ncol = param.ns+param.nfilt+param.ntts; /* [Phase, filter, TTS] */
  cblas_dgemv(CblasRowMajor, CblasNoTrans, nrow, ncol, 1.0, param.controlMatrix_phase, ncol, data.ei, 1, 0.0, data.da, 1);
  
  /* integrators */
  low_limit = param.a_limit;
  hi_limit = param.a_limit+param.na;
  for (k = 0; k<param.na; k++) {
    data.a[k] = (data.a[k] - param.a0[k])*param.integrator_bleeds[k] + data.da[k] + param.a0[k];
    data.a[k] = (data.a[k] > hi_limit[k]) ? hi_limit[k] : data.a[k];
    data.a[k] = (data.a[k] < low_limit[k]) ? low_limit[k] : data.a[k];
  }
  
  /* send the integrator values to the DM */
  if (dpio2_output_function) {
    dpio2_output_function (dpio2_instance,data.a,param.tweeter_map);
  }
  
  
}


/*
 c_recon is the reconstructor routine, which is intended to be called by the WFS camera handler.
    - the TTS camera handler has previously computed the tip/tilt and placed the results (scalars tip and tilt) into static memory
    - the results of the calculation here is to do the matrix multiplication and update the state vector
    - it follows with a call to the tweeter (dpio2) and woofer drivers*
       *it is yet to be determined if the woofer driver calls (a socket interface) need to be decimated in time
    - note that part of the state vector is anti-aliased versions of the WFS tip/tilt and/or
      state vectors of the uplink tip/tilt control and telescope offloading.

  arguments:
    d - pointer to the wavefront sensor camera raw data
*/

static int said_it_once_rtc2 = 0;

void
c_recon(void *handler,unsigned short *d)
{
  int j, k, n, ncol, nrow;
  double *sx, *sy, *inten, *s_ref_x, *s_ref_y;
  double *sfilt, *afilt, *stt;
  double *cx, *cy, *ci, *pdata;
  double *hi_limit, *low_limit;
  double sx_temp, sy_temp, inten_temp;
  double *p, *s, *a, r;
  
  /* copy the camera data directly into a buffer that python can see */
  n = param.npix*param.npix;
  for (k = 0; k<n; k++) data.wfs[k] = (double)d[k]; /* or just set the pointer? data.wfs = d */
  /* keep a running average, which might be useful for centering on a dim guide star */
  for (k = 0; k<n; k++) data.wfs_ma[k] = param.wfs_ma_alpha[0] * data.wfs_ma[k] + (1. - param.wfs_ma_alpha[0]) * data.wfs[k];

  /* camera data background subtraction, de interlacing, and flat-fielding */
  //TODO: Why aren't darks and flats deinterlaced here? Do they come pre-deinterlaced?
  n = param.nsub*param.nsub*param.ns;
  for (k = 0; k<n; k++) data.wfs_subaps[k] = ((double)d[param.i_map[k]] - param.wfs_background[k])*param.wfs_flat[k];

  /* centroider */
  sx = data.s;
  sy = data.s+param.ns;
  inten = data.inten;
  s_ref_x = param.s_ref;
  s_ref_y = param.s_ref+param.ns;
  n = param.nsub*param.nsub;
  cx = param.centWts;
  cy = param.centWts+n;
  ci = param.centWts+2*n;
  pdata = data.wfs_subaps;
  r = param.centReg[0];  
  for (k = 0; k<param.ns; k++) {
    sx_temp = 0.;
    sy_temp = 0.;
    inten_temp = 0.;
    for (j = 0; j<n; j++) {
      sx_temp += cx[j]*pdata[j];
      sy_temp += cy[j]*pdata[j];
      inten_temp += ci[j]*pdata[j];
    }
    pdata += n;
    inten[k] = (inten_temp > 1.0) ? inten_temp : 0.0;
    sx[k] = sx_temp/(inten[k]+r) - s_ref_x[k];
    sy[k] = sy_temp/(inten[k]+r) - s_ref_y[k];
  }

  /* copy filter states to appended s vector */
  sfilt = data.s+2*param.ns;
  afilt = data.a+param.na_tw+param.na_woof;
  for (k = 0; k<param.nfilt; k++) {
    sfilt[k] = afilt[k];
  }

  /* copy tip/tilt sensor measurements to appended s vector */
  stt = sfilt + param.nfilt;
  for (k = 0; k<param.ntts; k++) {
	stt[k] = data.dtt[k];
  }

  /* reconstruction */
  nrow = param.na; /* [tweeter, woofer, filter, uplinkTT] */
  ncol = param.ns*2+param.nfilt+param.ntts; /* [WFS, filter, TTS] */
  cblas_dgemv(CblasRowMajor, CblasNoTrans, nrow, ncol, 1.0, param.controlMatrix, ncol, data.s, 1, 0.0, data.da, 1);

  /* integrators */
  low_limit = param.a_limit;
  hi_limit = param.a_limit+param.na;
  for (k = 0; k<param.na; k++) {
    data.a[k] = (data.a[k] - param.a0[k])*param.integrator_bleeds[k] + data.da[k] + param.a0[k];
    data.a[k] = (data.a[k] > hi_limit[k]) ? hi_limit[k] : data.a[k];
    data.a[k] = (data.a[k] < low_limit[k]) ? low_limit[k] : data.a[k];
  }
  
  /* send the integrator values to the DM */
  if (dpio2_output_function) {
    dpio2_output_function (dpio2_instance,data.a,param.tweeter_map);
  }

  /* telemetry */
  s = data.s;  /* wavefront sensor */
  p = telemetry_buffer + telem_index*telem_buffer_stride;
  n = 2*param.ns;
  for (k = 0; k < n; k++) {
    p[k] = s[k];
  }
  p += n;
  n = param.ntts;
  s = s + param.nfilt; /* tipTilt sensor */
  for (k = 0; k < n; k++) {
    p[k] = s[k];
  }
  p += n;
  n = param.na;
  a = data.a; /* state vector */
  for (k = 0; k < n; k++) {
	p[k] = a[k];
  }
  telem_index += telem_increment;
  if (telem_index >= max_telem_index) { /* done, reset to bit bucket */
    telem_increment = 0;
    telem_index = 0;
  }
}

static int said_it_once_rtc2_tt = 0;

/*
 c_TipTiltRecon is the tip/tilt reconstructor routine, which is intended to be called by the TipTilt camera handler.
    - this computes the tip/tilt and placed the results (scalars tip and tilt) into static memory
    - it does not call DM drivers; instead the WFS camera handler does that, using the tip/tilt computed here

  arguments:
    d - pointer to the TipTilt sensor camera raw data
*/

void
c_tipTiltRecon(void *handler,unsigned short *d)
{
  int n, k;
  double ttx, tty, inten_temp, q;
  double *cx, *cy, *ci, *inten, r;

  /* copy the camera data directly into a buffer that python can see */
  n = param.ntt*param.ntt;
  for (k = 0; k<n; k++) data.tts[k] = (double)d[k]; /* or just set the pointer? data.wfs = d */
  /* keep a running average, which might be useful for finding a dim tip/tilt star */
  for (k = 0; k<n; k++) data.tts_ma[k] = param.tts_ma_alpha[0]*data.tts_ma[k] + (1. - param.tts_ma_alpha[0])*(data.tts[k] - param.tt_background[k]);

  /* camera data background subtraction, flat-field, and centroiding */
  ttx = 0.;
  tty = 0.;
  inten_temp = 0.;
  inten = data.tt_inten;
  cx = param.tt_centWts;
  cy = param.tt_centWts + n;
  ci = param.tt_centWts + 2*n;
  r = param.centReg[1];
  for (k = 0; k<n; k++) {
    q = data.tts[k] - param.tt_background[k];
    ttx += q*cx[k];
    tty += q*cy[k];
    inten_temp += q*ci[k];
  }
  inten[0] = (inten_temp > 1.0) ? inten_temp : 0.0;
  data.dtt[0] = ttx/(inten[0]+r);
  data.dtt[1] = tty/(inten[0]+r);

  if (said_it_once_rtc2_tt == 0) {
	printf("<rtc2.tipTiltRecon> called\n");
	said_it_once_rtc2_tt = 1;
  }
}

// Module functions table.

static PyMethodDef
module_functions[] = {
    { "hello_world", hello_world, METH_VARARGS, "Say hello." },
    { "param_list", param_list, METH_VARARGS, "List params and pointers."},
    { "set_state", set_state, METH_VARARGS, "Set the rtc state."},
    { "get_state", get_state, METH_VARARGS, "Get the rtc state."},
    { "set_sizes", set_sizes, METH_VARARGS, "Set all the size parameters (except ntts)."},
    { "set_size", set_size, METH_VARARGS, "Set a single size parameter (used for ntts in LGS mode)"},
    { "set_verbosity", set_verbosity, METH_VARARGS, "Set the level of the c-extension verboseness."},
    { "load", load, METH_VARARGS, "Set a parameter."},
    { "cload", cload, METH_VARARGS, "Load complex parameters."},
    { "type", type, METH_VARARGS, "Check the type of an array."},
    { "iload", iload, METH_VARARGS, "Set a map."},
    { "peek", peek, METH_VARARGS, "Peek at data."},
    { "set_dpio2_output", set_dpio2_output, METH_VARARGS, "Assign the DPIO2.Output object that is to receive actuator commands."},
    { "trace", trace, METH_VARARGS, "Calculate the trace of an array."},
    { "recon", recon, METH_VARARGS, "Perform wavefront reconstruction and control cycle."},
    { "ftr", ftr, METH_VARARGS, "Perform the FTR wavefront reconstruction."},
    { "hybrid", hybrid, METH_VARARGS, "Perform the Hybrid wavefront reconstruction."},
    { "tipTiltRecon", tipTiltRecon, METH_VARARGS, "Perform tip/tilt reconstruction using the TT Camera."},
    { "allocate_telemetry",allocate_telemetry,METH_VARARGS,allocate_telemetry_doc},
    { "telemetry_data",telemetry_data,METH_VARARGS,telemetry_data_doc},
    { "telemetry_ready",telemetry_ready,METH_VARARGS,telemetry_ready_doc},
    { "telemetry_peek",telemetry_peek,METH_VARARGS,telemetry_peek_doc},
    { "allocate_fftw", allocate_fftw, METH_VARARGS, allocate_fftw_doc},
    { NULL }
};

// This function is called to initialize the module.

void
initrtc3(void)
{
    
    Py_InitModule3("rtc3", module_functions, "rtc interface module.");
    /* The call to import_array() is required to enable the use of
       the functions defined in numpy/arrayobject.h. */
    import_array();
    state = STOP_AND_READ;
    init();
//    param_list();
}
