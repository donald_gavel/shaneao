;
;  p_matrix.pro
;   generate a matrix reconstructor for rtc
;
pro p_matrix,modes

  if (n_elements(modes) eq 0) then modes = [0,1]

  n_modes = (size(modes))[1]

  fitsbool = ['F','T']; FITS keyword values for booleans, indexed by secondary
  nostring = ['_nosec','']; FITS file substring, indexed by secondary
  yesstring = ['','_secondary']; FITS file substring, indexed by secondary

  for secondary = 0,1 do begin
    for imode = 0,n_modes-1 do begin

      mode = modes[imode]
      subapLocs = illums(mode,illum,secondary=secondary)
      n_subaps = (size(subapLocs))[1]
      modea = ['8x','16x','30x']
      n_across_select = [7, 14, 30]
      n_sec_select = [ 2, 4, 8 ]
      offset_select = [3, 7, 15]
      scale_select = [4, 2, 1]
      n_across = n_across_select[mode] + 1
      offset = offset_select[mode]
      scale = scale_select[mode]
      alocs = intarr(n_across,n_across,2)
      alocs[*,*,0] = (ones(n_across) ## indgen(n_across) - offset)*scale
      alocs[*,*,1] = transpose(alocs[*,*,0])
      alocs = reform(alocs,n_across*n_across,2)

      matrix = fltarr(n_across,n_across,n_subaps*2)
      for k = 0, n_subaps-1 do begin
	x = illum[k,0]
	y = illum[k,1]
	; -- x slopes --
	matrix[x,y,k] = 1.
	matrix[x+1,y,k] = -1.
	matrix[x,y+1,k] = 1.
	matrix[x+1,y+1,k] = -1.
	; -- y slopes --
	matrix[x,y,k+n_subaps] = 1.
	matrix[x+1,y,k+n_subaps] = 1.
	matrix[x,y+1,k+n_subaps] = -1.
	matrix[x+1,y+1,k+n_subaps] = -1.
      endfor
      matrix = transpose(reform(matrix,n_across*n_across,n_subaps*2))
      non_zero_rows = where(total(abs(matrix),1) ne 0)
      matrix2 = matrix[*,non_zero_rows]
      alocs = alocs[non_zero_rows,*]
      n_ps = (size(non_zero_rows))[1] ; number of phase sample points

      na = 32
      show = fltarr(na,na)
      for k = 0,n_ps -1 do begin
	x = alocs[k,0] + na/2
	y = alocs[k,1] + na/2
	show[x,y] = 1
      endfor
      disp,show,modea[mode]+' mode'

      ; invert the matrix, or read in the inverted matrix
      filename = 'reconMatrix_'+modea[mode]+yesstring[secondary]+'.fits'
      r = file_info(filename)
      if (r.exists) then begin
	reconMatrix = readfits(filename)
      endif else begin
	print,'<pMatrix> creating reconMatrix for ',modea[mode],' mode, this may take a minute...' & wait,.01
	reconMatrix = pseudoInverse(double(matrix2),0.01)
	print,'<pMatrix> done. Now writing it to disk for next time'
	mkhdr,hdr,reconMatrix

	sxaddpar,hdr,'WCSNAME','ShaneAO recon matrix'
	sxaddpar,hdr,'CNAME1','?phase sample points?'
	sxaddpar,hdr,'CNAME2','subap x cents @ subap y cents'
	sxaddpar,hdr,'CUNIT1','index'
	sxaddpar,hdr,'CUNIT2','index'
	sxaddpar,hdr,'BNAME','?phase for each sample and subap?'
	sxaddpar,hdr,'BUNIT','?',' unknown Fourier dimension'

	writefits,filename,reconMatrix,hdr
      endelse

      ; create the "K" matrix, which maps phase samples on the WFS grid to Fourier modes
      ;   K = [ cos(k x) | sin(k x) ]; k = [-16..15]*2*!pi / 32
      n_select = [8, 16, 32] ; these are the number of Fourier modes to be retained in each WFS mode
      n = n_select[mode]
      n2 = n*n
      kx = (findgen(n) - n/2 )*2*!pi / float(na)
      kx = ones(n) ## kx
      ky = transpose(kx)
      kx = reform(kx,n2)
      ky = reform(ky,n2)

      K = fltarr(n_ps,2*n2)
      for ix = 0,n_ps-1 do begin
	x = alocs[ix,0]
	y = alocs[ix,1]
	K[ix,0:n2-1] = cos(x*kx + y*ky)
	K[ix,n2:n2*2-1] = sin(x*kx + y*ky)
      endfor

      ; now invert K to calculate Fourier modes given phase samples
      print,'<pMatrix> starting K matrix inversion for ',modea[mode],' mode - this takes a while for 30x mode' & wait,.01
      R = pseudoInverse(double(K))
      print,'<pMatrix> done.

      ; finally, convert from Fourier modes to phase samples on the actuator grid
      na2 = na*na
      x = (findgen(na) - na/2)
      x = ones(na) ## x
      y = transpose(x)
      x = reform(x,na2)
      y = reform(y,na2)

      Kcos = cos( x # kx + y # ky )
      Ksin = sin( x # kx + y # ky )
      K2 = Kcos # R[0:n2-1,*] + Ksin # R[n2:n2*2-1,*]

      pM = K2 # reconMatrix

      ; check the result
      a = zernike(na,na,na/2,2,2,/noll)
      as = fltarr(na,na)
      ac = fltarr(n_ps)
      for i = 0,n_ps-1 do begin
	ix = alocs[i,0]+na/2
	iy = alocs[i,1]+na/2
	ac[i] = a[ix,iy]
	as[ix,iy] = a[ix,iy]
      endfor
      a_check = k2 # ac
      a_check = reform(a_check,na,na)
      disp,a_check

      ; append the woofer and woofer mode filter calculations
      nw = 52 ; number of woofer actuators
      nwm = 14 ; number of modes retained for woofer (up to spherical, minus piston)
      fs = 1000. ; sample rate (1kHz)
      dt = 1/fs
      tau = 1. / 200. ; modal time constant, in seconds. 200 Hz is the response time of the woofer

      M_ts = transpose(K2 # reconMatrix)

      ; *** these placeholders need to be replaced by real woofer mode calculations ***
      scale = max(M_ts)/10.
      M_tf = scale*randomn(seed,nwm,na2) ; this is the woofer modes as seen on the tweeter
      M_wf = scale*randomn(seed,nwm,nw)  ; this is the woofer modes as seen on the woofer
      ; ***

      M_fs = transpose(M_tf) ## M_ts
      M_ws = M_wf ## M_fs
      M_ff = -diagonal(ones(nwm))*(dt/tau)

      M = [[ M_ts, M_tf ], [ M_ws, fltarr(nwm,nw) ], [ M_fs, M_ff ] ]

      ; make the ShaneAO standard header and write to FITS file
      filename = 'controlMatrix_'+modea[mode]+nostring[secondary]+'.fits'
      print,'<pMatrix> writing '+filename
      mkhdr,hdr,M

      sxaddpar,hdr,'WCSNAME','ShaneAO step 1.3 Control matrix'
      sxaddpar,hdr,'CNAME1','subap x cents @ subap y cents @ woofer modes'
      sxaddpar,hdr,'CNAME2','tweeter DM index @ woofer DM index @ woofer modes'
      sxaddpar,hdr,'CUNIT1','index'
      sxaddpar,hdr,'CUNIT2','index'
      sxaddpar,hdr,'BNAME','gain/centroid for each AO input&output'
      sxaddpar,hdr,'BUNIT','DN for tweeter @ fraction for woofer @ gain for filter'

      sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
      sxaddpar,hdr,'ITYPE','RTCParam'
      sxaddpar,hdr,'DEVICE','DMs'
      sxaddpar,hdr,'FUNC','Control Matrix'
      sxaddpar,hdr,'SYMBOL','M'
      sxaddpar,hdr,'MODE',modea[mode]
      sxaddpar,hdr,'SEC',fitsbool[secondary],' Secondary obscuration'
      sxaddpar,hdr,'NS',n_subaps,' # of subapertures'
      sxaddpar,hdr,'NW',nw,' # of woofer actuators'
      sxaddpar,hdr,'NWM',nwm,' # of woofer modes controlled'
      sxaddpar,hdr,'ASTEP','1.3',' Algorithm step in RTC Software Definitions doc'
      writefits,filename,M,hdr
    endfor; loop over imode
  endfor; loop over secondary

end
