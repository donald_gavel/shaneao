;
;  deint_map.pro
;    de-interlace maps for the ShaneAO WFS camera
;
;  produces:
;    map from scrambled 160^2 data to a right-side up 160x160 display
;    map from scrambled 160^2 data to a vector of 25-element groups of pixels associated with subapertures
;
;


; function map25 produces a list of 1 dimensional indices into the camera image that denote subap pixels
;  The result can be viewed as an 25 x ns array
function map25,mode,secondary=secondary
  subapLocs = illums(mode,secondary=secondary)
  camPix = 160
  subap = 5
  ns = (size(subapLocs))[1]
  m25 = intarr(ns*subap*subap)
  for k = 0, ns-1 do begin
    p = k*subap*subap
    q = subapLocs[k,0] + subapLocs[k,1]*camPix
    for y = 0, subap-1 do begin
      for x = 0, subap-1 do begin
	m25[p + x + y*subap] = q + x + y*camPix
      endfor
    endfor
  endfor
  return,m25
end

function deint, sig

  dsig = uintarr(160, 160)
  forward_function deint_stage2

  topColsCntr = 0
  botColsCntr = 15
  topRowsCntr = 0
  botRowsCntr = 159

  for i = 0, (160 * 160 - 1), 20 do begin
    dsig[ 0 * 16 + topColsCntr, topRowsCntr ] = sig[i + 16]  ; tap  0
    dsig[ 1 * 16 + topColsCntr, topRowsCntr ] = sig[i + 12]  ; tap  1
    dsig[ 2 * 16 + topColsCntr, topRowsCntr ] = sig[i +  8]  ; tap  2
    dsig[ 3 * 16 + topColsCntr, topRowsCntr ] = sig[i +  4]  ; tap  3
    dsig[ 4 * 16 + topColsCntr, topRowsCntr ] = sig[i +  0]  ; tap  4
    dsig[ 5 * 16 + topColsCntr, topRowsCntr ] = sig[i +  2]  ; tap  5
    dsig[ 6 * 16 + topColsCntr, topRowsCntr ] = sig[i +  6]  ; tap  6
    dsig[ 7 * 16 + topColsCntr, topRowsCntr ] = sig[i + 10]  ; tap  7
    dsig[ 8 * 16 + topColsCntr, topRowsCntr ] = sig[i + 14]  ; tap  8
    dsig[ 9 * 16 + topColsCntr, topRowsCntr ] = sig[i + 18]  ; tap  9

    dsig[ 9 * 16 + botColsCntr, botRowsCntr ] = sig[i + 19]  ; tap 10
    dsig[ 8 * 16 + botColsCntr, botRowsCntr ] = sig[i + 15]  ; tap 11
    dsig[ 7 * 16 + botColsCntr, botRowsCntr ] = sig[i + 11]  ; tap 12
    dsig[ 6 * 16 + botColsCntr, botRowsCntr ] = sig[i +  7]  ; tap 13
    dsig[ 5 * 16 + botColsCntr, botRowsCntr ] = sig[i +  3]  ; tap 14
    dsig[ 4 * 16 + botColsCntr, botRowsCntr ] = sig[i +  1]  ; tap 15
    dsig[ 3 * 16 + botColsCntr, botRowsCntr ] = sig[i +  5]  ; tap 16
    dsig[ 2 * 16 + botColsCntr, botRowsCntr ] = sig[i +  9]  ; tap 17
    dsig[ 1 * 16 + botColsCntr, botRowsCntr ] = sig[i + 13]  ; tap 18
    dsig[ 0 * 16 + botColsCntr, botRowsCntr ] = sig[i + 17]  ; tap 19

    if (topColsCntr LT 15) then begin
      topColsCntr = topColsCntr + 1
      botColsCntr = botColsCntr - 1
    endif else begin
      topColsCntr = 0
      botColsCntr = 15
      topRowsCntr = topRowsCntr + 1
      botRowsCntr = botRowsCntr - 1
    endelse
  endfor
  dsig = deint_stage2(dsig)
  return,dsig
end

function deint_stage2,r2
  xtaps = indgen(10)*16
  ytaps = [0,80]
  ntaps = 20
  r3 = fltarr(16,80,ntaps)
  k = 0
  for y = 0,1 do begin
    for x = 0,9 do begin
      r3[*,*,k] = r2[xtaps[x]:xtaps[x]+15,ytaps[y]:ytaps[y]+79]
      k++
    endfor
  endfor

  r4 = fltarr(160,160)
  order = [16,12, 5, 8, 4, 9,15,18, 6, 2,$
	    3,10,14,19,13,20, 7, 1,17,11]-1

  flipx = [1, 1, 0, 0, 0, 0, 1, 1, 0, 0,$
	   1, 1, 0, 0, 0, 0, 1, 1, 0, 0]

  flipy = [1, 1, 0, 0, 0, 0, 1, 1, 0, 0,$
	   1, 1, 0, 0, 0, 0, 1, 1, 0, 0]
  k = 0
  for y = 0,1 do begin
    for x = 0,9 do begin
      s = r3[*,*,order[k]]
      if (flipx[k]) then s = reverse(s,1)
      if (flipy[k]) then s = reverse(s,2)
      r4[xtaps[x]:xtaps[x]+15,ytaps[y]:ytaps[y]+79] = s
      k++
    endfor
  endfor
  return,r4

end

; create a de interlace map for WFS cam. Use the result as follows: unscramData = scramData[map]
function deint_map
  r = uint( indgen(160*160) )
  map = deint(r)
  return,fix(map)
end

; create a de interlace map for WFS cam that maps from scrambled data to a 25 x ns vector containing the supab pixels
; use this map as follows: subap_vec_data = scramData[map]
function deint_map25,mode,secondary=secondary
  map = deint_map()
  map2 = map25(mode,secondary=secondary)
  r = map[map2]
  return,fix(r)
end

pro deint_map
  ; main code - generates the parameter data for ShaneAO
  map = deint_map()

  ; generate fits header
  mkhdr,hdr,map

  sxaddpar,hdr,'WCSNAME','descrambled ShaneAO WFS camera pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of WFS'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of WFS'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'BNAME','0-based index into array from pdv_wait_image read of WFS'
  sxaddpar,hdr,'BUNIT','index'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','De-interlace Map','Map from WFS pixels to pdv_wait_image array index'
  sxaddpar,hdr,'SYMBOL','i_map'
  sxaddpar,hdr,'ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'
  writefits,'i_map.fits',map,hdr

  fitsbool = ['F','T']; FITS keyword values for booleans, indexed by secondary
  nostring = ['_nosec','']; FITS file substring, indexed by secondary
  modetag = ['8','16','30']; tag as name of the subap, indexed by mode

  for secondary = 0,1 do begin

    for mode = 0,2 do begin

      ; scrambled maps
      map = deint_map25(mode,secondary = secondary)
      ; map is 1-d array, find out how many subap pixels in this mode
      samnp = (size(map))[1]
      ; for sake of easy interpretation and viewing of FITS file, reform
      map3 = reform(map,5,5,samnp/25)
      filename = 'i_map_subaps_'+modetag[mode]+'x'+nostring[secondary]+'.fits'
      mkhdr,hdr,map3

      sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera pixels in each subap'
      sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of subap'
      sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of subap'
      sxaddpar,hdr,'CNAME3','1-based index of subaps in this mode'
      sxaddpar,hdr,'CUNIT1','pixel'
      sxaddpar,hdr,'CUNIT2','pixel'
      sxaddpar,hdr,'CUNIT3','index'
      sxaddpar,hdr,'BNAME','0-based index into array from pdv_wait_image read of WFS'
      sxaddpar,hdr,'BUNIT','index'

      sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
      sxaddpar,hdr,'ITYPE','RTCParam'
      sxaddpar,hdr,'DEVICE','WFSCam'
      sxaddpar,hdr,'FUNC','De-interlace Map','Map from subap pixels to pdv_wait_image array index'
      sxaddpar,hdr,'SYMBOL','i_map_subaps'
      sxaddpar,hdr,'NS',fix((size(map))[1]/25),' Number of subaps'
      sxaddpar,hdr,'MODE',modetag[mode]+'x'
      sxaddpar,hdr,'SEC',fitsbool[secondary],' Secondary obscuration'
      sxaddpar,hdr,'ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'
      writefits,filename,map3,hdr

      ; unscrambled maps
      map = map25(mode,secondary = secondary)
      ; map is 1-d array, find out how many subap pixels in this mode
      samnp = (size(map))[1]
      ; for sake of easy interpretation and viewing of FITS file, reform
      map3 = reform(map,5,5,samnp/25)
      filename = 'u_map_subaps_'+modetag[mode]+'x'+nostring[secondary]+'.fits'
      mkhdr,hdr,map3

      sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera pixels in each subap'
      sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of subap'
      sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of subap'
      sxaddpar,hdr,'CNAME3','1-based index of subaps in this mode'
      sxaddpar,hdr,'CUNIT1','pixel'
      sxaddpar,hdr,'CUNIT2','pixel'
      sxaddpar,hdr,'CUNIT3','index'
      sxaddpar,hdr,'BNAME','0-based index into pixel array from WFS'
      sxaddpar,hdr,'BUNIT','index'

      sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
      sxaddpar,hdr,'ITYPE','RTCParam'
      sxaddpar,hdr,'DEVICE','WFSCam'
      sxaddpar,hdr,'FUNC','Subap Map',' wfs cam to subap blocks of 25 for centroider'
      sxaddpar,hdr,'SYMBOL','u_map_subaps'
      sxaddpar,hdr,'NS',fix((size(map))[1]/25),' Number of subaps'
      sxaddpar,hdr,'MODE',modetag[mode]+'x'
      sxaddpar,hdr,'SEC',fitsbool[secondary],' Secondary obscuration'
      sxaddpar,hdr,'ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'
      writefits,filename,map3,hdr

    endfor; loop over mode

  endfor; loop over secondary

end; deint

