;
;  param_gen.pro
;    parameter set generator for ShaneAO
;    (meant as an example script)
;

; WFS camera background
pro param_gen

  b = readfits('wfs_frames_2-22-13.fits')
  nf = (size(b))[3]
  bg = 0*b
  for k = 0,nf-1 do begin
    bg += b[*,*,k]
  endfor
  bg /= float(nf)

  mkhdr,hdr,bg

  sxaddpar,hdr,'WCSNAME','descrambled ShaneAO WFS camera pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of WFS'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of WFS'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'BNAME','WFS dark current'
  sxaddpar,hdr,'BUNIT','adu/cs',' counts per 0.01 s'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','camera background'
  sxaddpar,hdr,'SYMBOL','background'
  sxaddpar,hdr,'ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'
  sxaddpar,hdr,'XPOSURE',0.01,' Exposure time [s]'
  sxaddpar,hdr,'NAVE',nf,' Number of frames averaged'
  sxaddpar,hdr,'DINT','T',' Data is de-interlaced'
  writefits,'wfs_dark.fits',bg,hdr

  ; WFS camera flat field compensation
  flat = ones(160,160)

  mkhdr,hdr,flat

  sxaddpar,hdr,'WCSNAME','descrambled ShaneAO WFS camera pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of WFS'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of WFS'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'BNAME','WFS flat field value'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','camera flat field'
  sxaddpar,hdr,'SYMBOL','flat'
  sxaddpar,hdr,'ASTEP','1.1',' Algorithm step in RTC Software Definitions doc'
  sxaddpar,hdr,'DINT','T', ' Data is de-interlaced'
  writefits,'wfs_flat.fits',flat,hdr

  ; centroider weights
  w = fltarr(5,5,3)
  w[0:3,0:3,0] = [[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5],[-1.5,-0.5,0.5,1.5]]
  w[*,*,1] = transpose(w[*,*,0])
  w[0:3,0:3,2] = ones(4,4)

  mkhdr,hdr,w

  sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera subaperture pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of subaperture'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of subaperture'
  sxaddpar,hdr,'CNAME3','x @ y @ intensity'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'CUNIT3','index'
  sxaddpar,hdr,'BNAME','pixel weight for WFS centroider, COG'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','centroider weights'
  sxaddpar,hdr,'SYMBOL','w_[x,y,i]'
  sxaddpar,hdr,'ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'
  sxaddpar,hdr,'ALGO','COG',' Center of gravity'
  writefits,'cent_weights_cog.fits',w,hdr

  w = fltarr(5,5,3)
  w[0:3,0:3,0] = [[0,-1,1,0],[0,-1,1,0],[0,-1,1,0],[0,-1,1,0]]
  w[*,*,1] = transpose(w[*,*,0])
  w[0:3,0:3,2] = ones(4,4)

  mkhdr,hdr,w

  sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera subaperture pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of subaperture'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of subaperture'
  sxaddpar,hdr,'CNAME3','x @ y @ intensity'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'CUNIT3','index'
  sxaddpar,hdr,'BNAME','pixel weight for WFS centroider, 2x2 quad'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','centroider weights'
  sxaddpar,hdr,'SYMBOL','w_[x,y,i]'
  sxaddpar,hdr,'ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'
  sxaddpar,hdr,'ALGO','QUAD2',' 2x2 quad cell'
  writefits,'cent_weights_q2.fits',w,hdr

  w = fltarr(5,5,3)
  w[0:3,0:3,0] = [[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1],[-1,-1,1,1]]
  w[*,*,1] = transpose(w[*,*,0])
  w[0:3,0:3,2] = ones(4,4)

  mkhdr,hdr,w

  sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera subaperture pixels'
  sxaddpar,hdr,'CNAME1','1-based pixel along <X-like> axis of subaperture'
  sxaddpar,hdr,'CNAME2','1-based pixel along <Y-like> axis of subaperture'
  sxaddpar,hdr,'CNAME3','x @ y @ intensity'
  sxaddpar,hdr,'CUNIT1','pixel'
  sxaddpar,hdr,'CUNIT2','pixel'
  sxaddpar,hdr,'CUNIT3','index'
  sxaddpar,hdr,'BNAME','pixel weight for WFS centroider, 4x4 (binned) quad cell'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','WFSCam'
  sxaddpar,hdr,'FUNC','centroider weights'
  sxaddpar,hdr,'SYMBOL','w_[x,y,i]'
  sxaddpar,hdr,'ASTEP','1.2',' Algorithm step in RTC Software Definitions doc'
  sxaddpar,hdr,'ALGO','QUAD4',' 4x4 (binned) quad cell'
  writefits,'cent_weights_q4.fits',w,hdr

  ; reference centroids
  mode_set = ['8x','16x','30x']

  fitsbool = ['F','T']; FITS keyword values for booleans, indexed by secondary
  nostring = ['_nosec','']; FITS file substring, indexed by secondary

  for secondary = 0,1 do begin
    for mode = 0,2 do begin
      amode = mode_set[mode]
      subapLocs = illums(mode,secondary=secondary)
      ns = (size(subapLocs))[1]
      s_ref = fltarr(ns*2)

      filename = 'ref_cent_'+amode+nostring[secondary]+'.fits'
      mkhdr,hdr,s_ref

      sxaddpar,hdr,'WCSNAME','ShaneAO WFS camera subap indices x @ y'
      sxaddpar,hdr,'CNAME1','subap x cents @ subap y cents'
      sxaddpar,hdr,'CUNIT1','index'
      sxaddpar,hdr,'BNAME','reference centroid offset from subap center'
      sxaddpar,hdr,'BUNIT','pixel'

      sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
      sxaddpar,hdr,'ITYPE','RTCParam'
      sxaddpar,hdr,'DEVICE','WFSCam'
      sxaddpar,hdr,'FUNC',' reference centroids'
      sxaddpar,hdr,'SYMBOL','s_ref'
      sxaddpar,hdr,'MODE',amode
      sxaddpar,hdr,'SEC',fitsbool[secondary],' Secondary obscuration'
      sxaddpar,hdr,'NS',ns,' Number of subaps'
      sxaddpar,hdr,'ASTEP','1.3',' Algorithm step in RTC Software Definitions doc'
      writefits,filename,s_ref,hdr
    endfor; loop over mode
  endfor; loop over secondary

  ; DM default actuator postions and filter states
  nt = 1024 ; number of tweeter actuators
  abias = 43000/2 ; 100 volts in D/A units
  at0 = ones(nt)*abias
  nw = 52 ; number of woofer actuators
  wbias = 0.5 ; not sure of this - so for now assuming [0..1] is full range
  aw0 = ones(nw)*wbias
  nf = 14 ; number of woofer mode filters (up to zernike order 4, minus piston)
  af0 = fix(ones(nf))*0.

  a0 = [at0, aw0, af0] ; appended state vector
  n = nt+nw+nf

  mkhdr,hdr,a0

  sxaddpar,hdr,'WCSNAME','Shane AO steps 1.4 + 1.5'
  sxaddpar,hdr,'CNAME1','tweeter DM index @ woofer DM index @ woofer modes'
  sxaddpar,hdr,'CUNIT1','index'
  sxaddpar,hdr,'BNAME','default actuator or filter state'
  sxaddpar,hdr,'BUNIT','tweeterDN @ wooferFraction @ wooferModeValue'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','DMs'
  sxaddpar,hdr,'FUNC',' default actuator and filter states'
  sxaddpar,hdr,'SYMBOL','a0'
  sxaddpar,hdr,'NAT',nt,' Number of tweeter actuators'
  sxaddpar,hdr,'NAW',nw,' Number of woofer actuators'
  sxaddpar,hdr,'NF',nf,' Number of woofer mode filters'
  sxaddpar,hdr,'ASTEP','1.4, 1.5',' Algorithm step in RTC Software Definitions doc'
  writefits,'actuator_defaults.fits',a0,hdr

  ; integrator bleed coefficients
  nb = n ; total number of integrators
  b = ones(nb) ; bleed=1 implies pure integrator

  mkhdr,hdr,b

  sxaddpar,hdr,'WCSNAME','Shane AO step 1.5'
  sxaddpar,hdr,'CNAME1','tweeter DM index @ woofer DM index @ woofer modes'
  sxaddpar,hdr,'CUNIT1','index'
  sxaddpar,hdr,'BNAME','integrator bleed coefficient'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','DMs'
  sxaddpar,hdr,'FUNC',' integrator bleed coefficients'
  sxaddpar,hdr,'SYMBOL','b'
  sxaddpar,hdr,'NAT',nt,' Number of tweeter actuators'
  sxaddpar,hdr,'NAW',nw,' Number of woofer actuators'
  sxaddpar,hdr,'NF',nf,' Number of woofer mode filters'
  sxaddpar,hdr,'ASTEP','1.5',' Algorithm step in RTC Software Definitions doc'
  writefits,'integrator_bleeds.fits',b,hdr

  ; actuator limits
  a_limit = fltarr(n,2) ; stores both upper and lower limits of all integrators
  a_limit[0:nt-1,0] = 0. ; min on tweeter
  a_limit[0:nt-1,1] = 43000 ; max on tweeter
  a_limit[nt:nt+nw-1,0] = 0. ; min on woofer
  a_limit[nt:nt+nw-1,1] = 1. ; max on woofer
  a_limit[nt+nw:nt+nw+nf-1,0] = -2. ; need to experiment with this setting
  a_limit[nt+nw:nt+nw+nf-1,1] = 2.

  mkhdr,hdr,a_limit

  sxaddpar,hdr,'WCSNAME','Shane AO step 1.5'
  sxaddpar,hdr,'CNAME1','tweeter DM index @ woofer DM index @ woofer modes'
  sxaddpar,hdr,'CNAME2','min @ max'
  sxaddpar,hdr,'CUNIT1','index'
  sxaddpar,hdr,'CUNIT2','index'
  sxaddpar,hdr,'BNAME','actuator or filter limit'
  sxaddpar,hdr,'BUNIT','tweeterDN @ wooferFraction @ wooferModeGain'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','DMs'
  sxaddpar,hdr,'FUNC',' integrator limits'
  sxaddpar,hdr,'SYMBOL','a_[min,max]'
  sxaddpar,hdr,'NAT',nt,' Number of tweeter actuators'
  sxaddpar,hdr,'NAW',nw,' Number of woofer actuators'
  sxaddpar,hdr,'NF',nf,' Number of woofer mode filters'
  sxaddpar,hdr,'ASTEP','1.5',' Algorithm step in RTC Software Definitions doc'
  writefits,'actuator_limits.fits',a_limit,hdr

  ; tweeter mapping from actuator number to buffer location
  ;  use as: buffer[map] = a,  where a is the desired tweeter actuator values in raster-scan order
  tw_map_default = [ $
  [  386,  919,  534,  911,  742,  996,  886,  870,  518,  918,  902,  903, 1012,  745,  129,  257,  706,  707,  842,  609,  835,  714,  769,  547,  777,  834,  963,  617,  929,  723,  785,    12], $
  [  743,  583, 1004,  678,  838,  878,  646,  806,  910,  558,    3,  385,  590,  933,  662,  694,  979,   67,   65,  200,  449,  579,  328,  321,  625,  737,  193,  722,   72,  709,  585,   897], $
  [  998,  758, 1006,  812,  726,  974,  131,  879,  908,  805,  686,  940,  652,  654,  591,  684,  850,  202,   76,  905,  973,  330,  555,  623,  323,  937,  456,  195,  587,   74,  577,   970], $
  [  759,  559,  259,  750, 1007,  815,  854,  780,  524,  822,  660,  130,  598,  941,  655,  566,  770,   80,   75,  776,  715,  554,  460,  843,  329,  642,  717,  783,  586,  945,  943,   595], $
  [  909,   34,  751,  975,    2,  846,  532,  482,  814,  813,  416,  949,  781,  384, 1014,  847,  340,  480,  971,  459,  930,   73,   77,  898,  209,  744,  616,  725,  338,  802,  978,   457], $
  [  354,  982,  406,  288,  948,  685,  256,  916,  653,  773,  719,  687,  645,  983,  278,  517,  464,  649,  160,   82,   86,  128,  201,  205,   98,  853,  333,   32,  208,  584,    0,   336], $
  [ 1015,  557,   18,  820,  525,  400,  788,  527,  272,  693,  150,   50,  823,  431,  432,  855,  851,  791,  335,   81,  951,   99,  325,  210,  342,   69,  981,  461,  451,  631,   79,   226], $
  [  399,  304,  692,  355,  240,  695,  551, 1008,  564,  901,  112,  535,  274,  370,  438,  146,  563,  659,  214,  332,  691,  468,  391,  754,  912,  161,  882,  944,  227,  913,  212,   207], $
  [  178,  310,  498,  306,  884,  915,  711,  663,  116,  402, 1013,  947,  434,  885,  658,  871,  904,  533,  471,  216,  698,  752,  936, 1009,  592,  472,  661,  784,  204,  789,  624,   344], $
  [  565,  244,  148,  439,   17,   52,  311,  368,  182,  821,  372,  363,  275,   49,  276,  403,   94,   84,  816,  350,  331,  881,  604,  959,  407,  467,  596,  976,  469,  263,  849,   215], $
  [  786,  180,   21,  500,  756,  446,  305,  362,  147,  496,  404,  700,  118,  308,  540,  433,  689,  474,  656,  798,  476,  466,  151,  222,  203,  817,  478,  761,  958,   92,  470,   279], $
  [  757,  572,  914,  393,  414,  246,  144,  599,   51,  436,   19,   48,  183,  828,  374,  425,  343,  635,  632,  848,  405,  664,  657,  721,  824,  594,  634,  465,  597,   23,  211,   601], $
  [  668,  179,  176,  831,   16,  502,  671,  247,  318, 1021,  307,   22,  401, 1010,  242,  956,  113,  213,  149,  825,   87,  988,   83,  688,  920,  339,  529,  277,  602,  562,  638,   792], $
  [  375,  503,  796,  435,  286,  991, 1011,   54,  863,  309,  243,  703,   53,  437,  543,  787,  152,  593,  732,  985,  346,  341,  729,  570,  561,  568,  760,  347,  984,  600,  860,   569], $
  [  893,  526,  412,  628,  718,  181,  177,  606,  727,  158,  924,   30,  892, 1023,  887,  629,   88,  479,  349,  952,  539,   95,  977,   24,  345,   85,  605,  458,  858,  889,  337,   954], $
  [  957, 1016,  567,  829,  637,  765,  669,   28,   55,  541,  538,  145, 1022,  636,  764,  219,  845,  220,  922,  753,  730,  962,   90,  528,  794,  218,  537,  699,  965,  826,  672,   819], $
  [  127,  511,   56,  992,   26,  184,  410,  154,  312,  255,  282,  125,  284,  373,  253,  510,  536,  762,  507,  531,  408,  763,  917,  280,  505,  728,  250,  888,  696,  123,  249,   666], $
  [  501,  381,  159,  119,  509,  319,  156,  383,  287,   63,  665,  285,   31,  241,  445,  191,  379,  630,  861, 1017,  950,  626,  883,  755,  856,  857,  697, 1018,  121,  627,   43,  1019], $
  [  369,  413,   29,  497, 1020,  189,  115,  926,  157,  371,  273,  317,  499,  574,   62,  117,  281,  377,  633,  313,  560,  603,  185,  790,  986,   57,  980,  793,  955,  724,  852,   827], $
  [  734,  190,  245,  894,  766,  124,  607,  670,  252,  735,  175,  380,  575,  303,  508,  895,  477,  376,  799,  473,  795,  989,  217,  248,  327,   91,  667,   71,  733,  251,  639,   953], $
  [  990,  126,  927,  830,  254,  573,  143,  382,  862,  271,  155,  702,  447,  315,  542,  415,  553,   89,  880,  589,  859,  720,  223,  571,   25,  351,  731,  504,  521,  921,  923,   463], $
  [  283,  556,  620,  443,  701,  748,  411,  797,  876,   61,  430,  767,   60,    5,  621,  188,  705,  648,  334,  545,  619,   78,  581,  969,  206,  818,  809,  221,  690,  987,   93,   530], $
  [  387,  749,  316,  423,  877,  444,  549, 1005,  173,  677,   14,  301,  647,  142,  429,  807,  546, 1001,  716,  775,  841,  462,  199,  681,  618,  453,  515,  968,  197,  865,  808,   588], $
  [  270,   47,  967,  398,  397,  839,  174,   15,  999,  302,  366,  966,  167,  494,  582,  295,  736,  448,  746,  455,  192,  747,  942,  935,  523,  622,  615,  873,  844,  896,  713,   972], $
  [  997,  495,  932,  869,  367,  710,   46,  269,  804,   39,  238,  550,  741,  239,  679,  428,  833,  104,   66,  801, 1003,  578,  673, 1002,  194,  641,  875,  320,  513,  874,   64,   576], $
  [  141,  676,  421,  110,  900,  613,  111,  519,  396,  442,  548,  300,   45,  772,  293,   13,   68,  680,  233,  768,  993,  232,  196,  961,  651,  322,  782,  683,  928,  520,  105,   450], $
  [  868,  487,  492,  422,  268,  493,  644,  172,  314,  740,  165,  427,  294,  359,  395,  516,  907,  640,  712,  939,  198,  840,  361,  324,  643,  360,  608,  611,  779,  452,  552,   811], $
  [  140,  364,  612,   44,  365,  166,   37,  506,  390,  231,  187,  486,  394,  186,   38,  426,  771,  489,  708,  867,  488,  326,  872,   40,  800, 1000,  946,  454,  675,  261,   70,   739], $
  [  299,  262,  419,  267,  358,  103,  236,  420,  266,  237,  134,  298,  378,  230,  291,   27,  774,  778,  168,  836,  803,  114,  610,  995,  389,  964,  682,    9,  580,  650,   41,   960], $
  [  292,  485,   58,    6,  138,  171,  102,  170,  139,  164,  163,  108,  388,  357,  109,  484,  441,  934,  938,    7,  866,  906,  137,  544,  899,  169,  614,   97,  107,  738,  810,   106], $
  [   10,  348,   36,   42,  475,  260,   35,  409,  356,  229,   59,  418,  392,  440,  132,  424,    1,  235,   96,  353,  234,  864,  224,  136,  512,    8,  120,  994,  931,  122,  704,   225], $
  [   20,  228,  417,   11,  290,  101,  490,    4,  296,  491,  100,  289,  264,  162,  483,  891,  258,  133,  153,  890,   33,  522,  674,  481,  135,  832,  352,  265,  514,  837,  297,  925]]

  filename = '1k Mapping v2.0.csv'
  r = file_info(filename)
  if (r.exists) then begin
    u = read_csv('1k Mapping v2.0.csv',header=header)
    tw_map = u.filed01
    tw_map = reform(tw_map,32,32)
  endif else begin
    tw_map = tw_map_default
  endelse

  mkhdr,hdr,a_limit

  sxaddpar,hdr,'WCSNAME','ShaneAO tweeter DM actuators'
  sxaddpar,hdr,'CNAME1','1-based DM actuator along <X-like> axis of tweeter'
  sxaddpar,hdr,'CNAME2','1-based DM actuator along <Y-like> axis of tweeter'
  sxaddpar,hdr,'CUNIT1','index'
  sxaddpar,hdr,'CUNIT2','index'
  sxaddpar,hdr,'BNAME','0-based index into array for DPIO2 write to DM'
  sxaddpar,hdr,'BUNIT','index'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','MEMS DM'
  sxaddpar,hdr,'FUNC',' tweeter descramble map'
  sxaddpar,hdr,'SYMBOL','tweeter_map'
  sxaddpar,hdr,'ASTEP','1.6',' Algorithm step in RTC Software Definitions doc'
  writefits,'tweeter_map.fits',tw_map,hdr

  ; anti-aliasing filter time constant
  tau = 0.1 ; time constant, seconds
  fs = 1000. ; nominal frame rate, 1 kHz
  p = 0.5 ; 0.5 means 50% decay time, 0.1 means 90% decay time
  alpha = p^(1./(tau*fs))
  beta = 1 - alpha
  u = [alpha, beta]

  mkhdr,hdr,u

  sxaddpar,hdr,'WCSNAME','Shane AO step 5.1'
  sxaddpar,hdr,'CNAME1','alpha @ beta'
  sxaddpar,hdr,'CUNIT1','index'
  sxaddpar,hdr,'BNAME','anti-alias filter gain','fraction of old @ fraction of new'
  sxaddpar,hdr,'BUNIT','',' dimensionless'

  sxaddpar,hdr,'SYSTEM','ShaneAO',' UC Lick Observatory Shane LGS AO'
  sxaddpar,hdr,'ITYPE','RTCParam'
  sxaddpar,hdr,'DEVICE','DMs'
  sxaddpar,hdr,'FUNC','anti-alias filter'
  sxaddpar,hdr,'SYMBOL','aa_filt_alpha'
  sxaddpar,hdr,'ALPHA',alpha
  sxaddpar,hdr,'BETA',beta
  sxaddpar,hdr,'ASTEP','5.1',' Algorithm step in RTC Software Definitions doc'
  writefits,'aa_filt_alpha.fits',u,hdr

end

; generate all the parameters in one swell foop:

param_gen
deint_map
p_matrix,[0,1]

end
