;
; illums.pro
;    identify the illuminated subapertures for the ShaneAO system
;

; illums produces a list of x,y pairs pointing to lower-left corners of illuminated subaps
function illums,mode,illum,secondary=secondary,disp=disp
  camPix = 160
  subap = 5
  n_across_select = [7, 14, 30]
  n_sec_select = [ 2, 4, 8 ]

  ; --- determine illuminated subaps ---
  n = camPix / subap
  d = n_across_select[mode]
  dsec = n_sec_select[mode]
  sap = circle(n,n,n/2-0.5,n/2-0.5,d/2,1)
  if keyword_set(secondary) then begin
    sap -= circle(n,n,n/2-0.5,n/2-0.5,(dsec+1)/2,1)
  endif
  if (keyword_set(disp)) then disp,sap
  illum = where(sap eq 1)
  illum = [[illum mod n],[fix(illum/n)]]
  subapLocs = illum*subap
  for k=0,1 do illum[*,k] -= min(illum[*,k])
  return,subapLocs
end
