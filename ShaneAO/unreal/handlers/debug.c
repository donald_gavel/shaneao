#include "Camera.h"

void printSummary (Handler *instance, u_char *data) {

  static int counter=0;
  struct timespec starttime;

  Camera *camera;

  if (instance == NULL) {
    return;
  }

  camera = instance->camera;


  clock_gettime (CLOCK_REALTIME, &starttime);

  printf ("<printSummary> %ld.%09ld [%d]: %d*%d*%d (width*height*depth)\n",
            starttime.tv_sec, starttime.tv_nsec, counter,
            camera->width, camera->height, camera->depth);

  counter++;

  return;
}


/* Save the image as a .raw file. A raw file is straight binary unformated
 * storage of the unsigned 16-bit integers that form both the Big_Joe
 * and Little_Joe images.
 * Can be configured to save exactly
 * one file every n seconds, where n is specified as a floating point
 * number.
*/

typedef struct raw_config {

  char filename_base[128];
  double interval;
  struct timespec last;
  int counter;
<<<<<<< debug.c
  
} raw_config;
=======

} bitmap_config;
>>>>>>> 1.7

void saveAsRaw (Handler *instance, u_char *data) {

  Camera *camera;
  char filename[1024];
  char *config_base, *config_full;
  raw_config *configuration;
  unsigned short *adata,*bdata;
  int count,k;

  struct timespec now;
  double elapsed;


  clock_gettime (CLOCK_REALTIME, &now);

  if (instance == NULL) {
    return;
  }

  camera = instance->camera;


  if (instance->voidstar == NULL) {
    instance->voidstar = malloc (sizeof (raw_config));

    configuration = (raw_config *)instance->voidstar;

    configuration->counter = 0;
    configuration->last.tv_sec = 0;
    configuration->last.tv_nsec = 0;

    config_full = PyString_AsString (camera->config);
    config_base = basename (config_full);

    switch (config_base[0]) {
      case 'W':
        strcpy (configuration->filename_base, "BigJoe");
        break;
      case 'T':
        strcpy (configuration->filename_base, "LittleJoe");
        break;
      default:
        strcpy (configuration->filename_base, "unknown");
        break;
    }

  } else {
    configuration = (raw_config *)instance->voidstar;
  }


  if (configuration->interval != 0) {
    elapsed = now.tv_sec - configuration->last.tv_sec + (now.tv_nsec - configuration->last.tv_nsec) / 1000000000.0;

    if (elapsed < configuration->interval) {
      return;
    }
  }

  configuration->last.tv_sec  = now.tv_sec;
  configuration->last.tv_nsec = now.tv_nsec;


  snprintf (filename, sizeof (filename), "%s_%08d.raw", configuration->filename_base, configuration->counter);

  dvu_write_raw (camera->width*camera->height*2,data,filename);

  bdata = (unsigned short *)data;
  adata = (unsigned short *)camera->data;
  count = camera->width*camera->height;
  for (k=0; k<count; k++) adata[k] = bdata[k];
/*
  printf("<saveAsRaw.c> data = %u, %u, %u ...\n",bdata[0],bdata[1],bdata[2]);
*/

  configuration->counter++;

  return;
}



int configSaveAsRaw (Handler *instance, PyObject *config) {

  PyObject *element, *number;

  char *filename;
  double interval;
  int maximum_length;

  struct raw_config *parsed_config;


  if (instance == NULL) {
    PyErr_SetString (PyExc_RuntimeError, "Handler *instance is NULL");
    return -1;
  }

  if (config == NULL) {
    PyErr_SetString (PyExc_RuntimeError, "PyObject *config is NULL");
    return -1;
  }

  if (!PySequence_Check (config) || PySequence_Length (config) != 2) {
    PyErr_SetString (PyExc_TypeError, "expected a sequence with two values");
    return -1;
  }


  element = PySequence_GetItem (config, 0);

  if (element == NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  filename = PyString_AsString (element);
  Py_DECREF (element);
  element = NULL;

  if (filename == NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }


  element = PySequence_GetItem (config, 1);

  if (element == NULL) {
    /* An exception was raised. Let the caller handle it. */
    Py_DECREF (element);
    return -1;
  }

  if (!PyNumber_Check (element)) {
    PyErr_SetString (PyExc_TypeError, "interval must be specified as a number");
    Py_DECREF (element);
    return -1;
  }

  number = PyNumber_Float (element);
  Py_DECREF (element);
  element = NULL;

  if (PyErr_Occurred () != NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  interval = PyFloat_AsDouble (number);
  Py_DECREF (number);
  number = NULL;

  if (PyErr_Occurred () != NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  if (interval < 0) {
    PyErr_SetString (PyExc_ValueError, "requested interval is negative");
    return -1;
  }


  parsed_config = (raw_config *)malloc (sizeof (raw_config));

  if (parsed_config == NULL) {
    PyErr_SetString (PyExc_MemoryError, "unable to malloc a raw_config struct");
    return -1;
  }

  maximum_length = sizeof (parsed_config->filename_base) -1;

  if (strlen (filename) > maximum_length) {
    PyErr_Format (PyExc_ValueError, "filename too long, must not exceed %d characters", maximum_length);
    free (parsed_config);
    return -1;
  }


  strncpy (parsed_config->filename_base, filename, maximum_length);
  parsed_config->interval = interval;
  parsed_config->counter = 0;
  parsed_config->last.tv_sec = 0;
  parsed_config->last.tv_nsec = 0;

  if (instance->voidstar != NULL) {
    free (instance->voidstar);
  }

  instance->voidstar = parsed_config;

  return 0;
}

void doNothing (Handler *instance, u_char *data) {

  Camera *camera;
  char filename[1024];
  char *config_base, *config_full;
  raw_config *configuration;
  unsigned short *adata,*bdata;
  int count,k;

  struct timespec now;
  double elapsed;


  clock_gettime (CLOCK_REALTIME, &now);

  if (instance == NULL) {
    return;
  }

  camera = instance->camera;


  if (instance->voidstar == NULL) {
    instance->voidstar = malloc (sizeof (raw_config));

    configuration = (raw_config *)instance->voidstar;

    configuration->counter = 0;
    configuration->last.tv_sec = 0;
    configuration->last.tv_nsec = 0;

    config_full = PyString_AsString (camera->config);
    config_base = basename (config_full);

    switch (config_base[0]) {
      case 'W':
        strcpy (configuration->filename_base, "BigJoe");
        break;
      case 'T':
        strcpy (configuration->filename_base, "LittleJoe");
        break;
      default:
        strcpy (configuration->filename_base, "unknown");
        break;
    }

  } else {
    configuration = (raw_config *)instance->voidstar;
  }


  if (configuration->interval != 0) {
    elapsed = now.tv_sec - configuration->last.tv_sec + (now.tv_nsec - configuration->last.tv_nsec) / 1000000000.0;

    if (elapsed < configuration->interval) {
      return;
    }
  }

  configuration->last.tv_sec  = now.tv_sec;
  configuration->last.tv_nsec = now.tv_nsec;


//  snprintf (filename, sizeof (filename), "%s_%08d.raw", configuration->filename_base, configuration->counter);

//  dvu_write_raw (camera->width*camera->height*2,data,filename);

  /* save the data to the "peek" space */
  bdata = (unsigned short *)data;
  adata = (unsigned short *)camera->data;
  count = camera->width*camera->height;
  for (k=0; k<count; k++) adata[k] = bdata[k];
/*
  printf("<saveAsRaw.c> data = %u, %u, %u ...\n",bdata[0],bdata[1],bdata[2]);
*/

//  configuration->counter++;

  return;
}



int configDoNothing (Handler *instance, PyObject *config) {

  PyObject *element, *number;

  char *filename;
  double interval;
  int maximum_length;

  struct raw_config *parsed_config;


  if (instance == NULL) {
    PyErr_SetString (PyExc_RuntimeError, "Handler *instance is NULL");
    return -1;
  }

  if (config == NULL) {
    PyErr_SetString (PyExc_RuntimeError, "PyObject *config is NULL");
    return -1;
  }

  if (!PySequence_Check (config) || PySequence_Length (config) != 2) {
    PyErr_SetString (PyExc_TypeError, "expected a sequence with two values");
    return -1;
  }


  element = PySequence_GetItem (config, 0);

  if (element == NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  filename = PyString_AsString (element);
  Py_DECREF (element);
  element = NULL;

  if (filename == NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }


  element = PySequence_GetItem (config, 1);

  if (element == NULL) {
    /* An exception was raised. Let the caller handle it. */
    Py_DECREF (element);
    return -1;
  }

  if (!PyNumber_Check (element)) {
    PyErr_SetString (PyExc_TypeError, "interval must be specified as a number");
    Py_DECREF (element);
    return -1;
  }

  number = PyNumber_Float (element);
  Py_DECREF (element);
  element = NULL;

  if (PyErr_Occurred () != NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  interval = PyFloat_AsDouble (number);
  Py_DECREF (number);
  number = NULL;

  if (PyErr_Occurred () != NULL) {
    /* An exception was raised. Let the caller handle it. */
    return -1;
  }

  if (interval < 0) {
    PyErr_SetString (PyExc_ValueError, "requested interval is negative");
    return -1;
  }


  parsed_config = (raw_config *)malloc (sizeof (raw_config));

  if (parsed_config == NULL) {
    PyErr_SetString (PyExc_MemoryError, "unable to malloc a raw_config struct");
    return -1;
  }

  maximum_length = sizeof (parsed_config->filename_base) -1;

  if (strlen (filename) > maximum_length) {
    PyErr_Format (PyExc_ValueError, "filename too long, must not exceed %d characters", maximum_length);
    free (parsed_config);
    return -1;
  }


  strncpy (parsed_config->filename_base, filename, maximum_length);
  parsed_config->interval = interval;
  parsed_config->counter = 0;
  parsed_config->last.tv_sec = 0;
  parsed_config->last.tv_nsec = 0;

  if (instance->voidstar != NULL) {
    free (instance->voidstar);
  }

  instance->voidstar = parsed_config;

  return 0;
}

/* Hitting tab gets you two spaces. Actual tabs render as eight characters,
 * the way that any (sane) editor expects them to.
 *
 * vim: set expandtab tabstop=8 softtabstop=2 shiftwidth=2 autoindent:
 */

/* EOF */
