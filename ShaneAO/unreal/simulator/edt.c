char EDT_doc[] =
	"This module is a SIMULATOR of the edt to Python interface module that would run on rtc."
        "The edt Python module defines a generic object-oriented "
        "representation of a camera via an EDT interface. In the case of Shane "
        "AO, this means the wavefront sensor camera, and the tip/tilt camera.";


#include "Camera_simulator.h"  /* this is a change from the original file edt.c.sin */
#include <dlfcn.h>
#include <string.h>
#include "Python.h"
#include "numpy/arrayobject.h"   /* this is a change from the original file edt.c.sin */
#define PY_ARRAY_UNIQUE_SYMBOL EDT   /* this is a change from the original file edt.c.sin */

/* These references will be filled in at initialization time,
   allowing simpler checking and/or invocation of instance
   types. */

static PyTypeObject *camera_type=NULL;
static PyTypeObject *handler_type=NULL;



/******************************************************************************/
/* Method definitions for the Camera class. */

char Camera_doc[] =
	"An integer *unit* and configuration *filename* must be specified "
        "when creating a :class:`Camera` instance. A background pthread "
        "will be invoked to handle image acquisition; the handling of "
        "image data is determined dynamically via the :func:`handler` "
        "method.";


static PyObject *
Camera_new (PyTypeObject *type, PyObject *args, PyObject *kwds) {

  Camera *self = NULL;

  self = (Camera *)type->tp_alloc (type, 0);

  if (self == NULL) {
    return NULL;
  }

  Py_INCREF (Py_None);
  Py_INCREF (Py_None);

  self->config = NULL;
  self->handler = Py_None;
  self->notify = Py_None;
  self->unit = NULL;

  self->buffers = 4;
  self->depth = 0;
  self->height = 0;
  self->pause = TRUE;
  self->shutdown = FALSE;
  self->started = FALSE;
  self->width = 0;

  self->frame_interval = 0;
  self->frame_processing = 0;

  self->free_messages = NULL;
  self->last_message = NULL;
  self->messages = NULL;

  pthread_cond_init  (&self->condition, NULL);
  pthread_mutex_init (&self->message_mutex, NULL);
  pthread_mutex_init (&self->mutex, NULL);

  return (PyObject *)self;
}



static int
Camera_init (Camera *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"unit", "config", NULL};

  long unit=0;
  int channel=0, result=0;

  char default_config_dir[] = "/Users/donaldgavel/cvs/lroot/ShaneAO/unreal/config/";
  char *filename=NULL, *temp_string=NULL;

  Dependent *dependent = NULL;
  EdtDev *edt = NULL;
  Edtinfo edtinfo;
  pthread_attr_t attr;
  u_char waitchar;

  if (!PyArg_ParseTupleAndKeywords (args, kwds, "is", kwlist,
                                    &unit, &filename)) {
    PyErr_SetString (PyExc_TypeError, "Camera instances require an integer unit and string filename");
    return -1; 
  }

  if (unit < 0) {
    PyErr_SetString (PyExc_ValueError, "unit parameter must be positive");
    return -1;
  }

  if (unit > 100) {
    PyErr_SetString (PyExc_ValueError, "unit parameter is unreasonably large");
    return -1;
  }

  self->unit = PyInt_FromLong (unit);


  if (access (filename, R_OK) == -1 ) {
    /* Try prefixing the filename with $LROOT/data/$INSTRUMENT.
       Use alloca() here so that we don't have to track whether
       to free() temp_string. This assumes that the temp_string 
       will not exhaust the available stack space for allocations. */

    temp_string = (char *) alloca ((strlen (default_config_dir) + strlen (filename) + 1) * sizeof (char));
    stpcpy (stpcpy (temp_string, default_config_dir), filename);

    if (access (temp_string, R_OK) == -1 ) {
      PyErr_Format (PyExc_ValueError, "cannot read config file '%s'", filename);
      return -1;
    } else {
      filename = temp_string;
    }
  }

  self->config = PyString_FromString (filename);


  /* This next section of code is derived from the initcam.c example
     provided by EDT. */

  /* pdv_alloc_dependent() allocates a Dependent structure that must
     be freed. */

  dependent = pdv_alloc_dependent ();

  if (dependent == NULL) {
    PyErr_SetString (PyExc_MemoryError, "unable to allocate a PDV Dependent stuct");
    return -1;
  }

  result = pdv_readcfg (filename, dependent, &edtinfo);

  if (result != 0) {
    PyErr_SetString (PyExc_ValueError, "EDTpdv cannot parse the configuration file");
    free (dependent);
    return -1;
  }


  /* Resuming the code lifted from initcam.c... */

  /* Quote from the API manual: "The name of the device, which for all EDT
     Digital Video boards is "pdv". If dev_name is NULL, "pdv" will be
     assumed. EDT_INTERFACE can also be used (recommended); it’s defined
     as "pdv" in edtdef.h." */

  edt = edt_open_channel (EDT_INTERFACE, unit, channel);

  if (edt == NULL) {
    PyErr_SetString (PyExc_ValueError, "edt_open_channel() failed");
    free (dependent);
    return -1;
  }


  result = pdv_initcam (edt, dependent, unit, &edtinfo, filename, '\0', 0);
  free (dependent);

  if (result != 0) {
    PyErr_SetString (PyExc_ValueError, "pdv_initcam() failed");
    return -1;
  }

  edt_close (edt);


  /* This next section of code is derived from the simple_take.c example
     provided by EDT. */

  self->device = pdv_open_channel (EDT_INTERFACE, unit, channel);

  if (self->device == NULL) {
    PyErr_SetString (PyExc_RuntimeError, "unable to open EDT device");
    return -1;
  }

  /* Configure the number of ring buffers. */

  if (self->device->dd_p->force_single) {
    self->buffers = 1;
  }

  if (self->buffers > 1) {
    result = pdv_multibuf (self->device, self->buffers);

    if (result == -1) {
      PyErr_SetString (PyExc_RuntimeError, "pdv_multibuf() failed, try reducing the quantity of ring buffers");
      return -1;
    }
  }


  /* If not already set, establish a default serial wait character of '\n'.
     This parameter should be set via the 'serial_waitc' directive in the
     camera configuration file. */

  result = pdv_get_waitchar (self->device, &waitchar);

  if (result == 0 || waitchar == '\0') {
    pdv_set_waitchar (self->device, 1, '\n');
  }


  /* Determine other 'fixed' camera parameters. Note that the base dimensions
     of the camera image can change depending on any windowing requested via
     pdv_set_roi(). */

  self->width  = pdv_get_cam_width (self->device);
  self->height = pdv_get_cam_height (self->device);
  self->depth  = pdv_get_depth (self->device);

  if (self->width == 0 || self->height == 0 || self->depth == 0) {
    PyErr_SetString (PyExc_RuntimeError, "unable to determine camera parameters");
    return -1;
  }


  /* Create the background thread that will handle all camera frames. */

  pthread_attr_init (&attr);
  pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_JOINABLE);

  result = pthread_create (&self->thread, &attr, frameProcessing, self);

  pthread_attr_destroy (&attr);

  if (result != 0) {
    PyErr_SetString (PyExc_RuntimeError, "unable to create background thread");
    return -1;
  }

  return 0;
}



static void
Camera_del (Camera *self)
{
  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);
  Py_END_ALLOW_THREADS

  self->shutdown = TRUE;
  pthread_cond_signal (&self->condition);
  pthread_mutex_unlock (&self->mutex);

  if (self->started == TRUE)
  {
    pthread_join (self->thread, NULL);
  }

  pthread_mutex_destroy (&self->mutex);
  pthread_cond_destroy  (&self->condition);

  pdv_close (self->device);

  Py_DECREF (self->handler);
  Py_DECREF (self->notify);

  Py_XDECREF (self->config);
  Py_XDECREF (self->unit);

  freeMessages (self);
  pthread_mutex_destroy (&self->message_mutex);

  self->ob_type->tp_free ((PyObject*)self);
}



/******************************************************************************/
char Camera_handler_doc[] =

	"Assign a handler function for any/all camera frames acquired by "
        "the background processing thread. The sole argument can be None, "
        "which will remove all handlers; it can be a single :class:`Handler` "
        "instance; and it can be an iterable sequence of :class:`Handler` "
        "instances.";

static PyObject *
Camera_handler (Camera *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"handler", NULL};

  PyObject *element;
  PyObject *handler;
  PyObject *old_handler;

  int index;
  Py_ssize_t length;


  if (!PyArg_ParseTupleAndKeywords (args, kwds, "O:handler", kwlist, &handler)) {
    PyErr_SetString (PyExc_TypeError, "new handler not specified");
    return NULL;
  }

  old_handler = self->handler;



  if (handler == Py_None) {

    /* Need to increase the reference count for the borrowed reference
       returned by PyArg_ParseTupleAndKeywords(). */

    Py_INCREF (Py_None);
    self->handler = Py_None;


  } else if (PySequence_Check (handler) == 1) {

    /* Iterate through the sequence and populate a fresh tuple with the
       elements from the sequence. This is done in order to allow the caller
       to pass in any valid sequence type, as opposed to restricting them
       to a tuple. */

    length = PySequence_Length (handler);
    self->handler = PyTuple_New (length);

    if (self->handler == NULL) {
      PyErr_SetString (PyExc_MemoryError, "cannot create a tuple");
      Py_DECREF (old_handler);
      Py_INCREF (Py_None);
      self->handler = Py_None;
      return NULL;
    }

    for (index = 0; index < length; index++) {

      element = PySequence_GetItem (handler, index);

      if (element == NULL) {
        /* An exception was raised. Let the caller handle it. */
        Py_DECREF (old_handler);
        Py_DECREF (self->handler);
        Py_INCREF (Py_None);
        self->handler = Py_None;
        return NULL;
      }
      
      if (!PyObject_TypeCheck (element, handler_type)) {
        PyErr_SetString (PyExc_TypeError, "each element in the handler sequence must be a Handler instance");
        Py_DECREF (old_handler);
        Py_DECREF (self->handler);
        Py_DECREF (element);
        Py_INCREF (Py_None);
        self->handler = Py_None;
        return NULL;
      }

      /* Using PyTuple_SET_ITEM() instead of PyTuple_SetItem(), because we are
         populating a brand new tuple object, and the macro version is faster.*/

      PyTuple_SET_ITEM (self->handler, index, element);

      /* Do not invoke Py_DECREF() on the set elements because
         PyTuple_SET_ITEM() steals the new reference returned by
         PySequence_GetItem(). */
    }


  } else if (PyObject_TypeCheck (handler, handler_type)) {

    /* Need to increase the reference count for the borrowed reference
       returned by PyArg_ParseTupleAndKeywords(). */

    Py_INCREF (handler);

    self->handler = PyTuple_New (1);

    if (self->handler == NULL) {
      PyErr_SetString (PyExc_MemoryError, "cannot create a tuple");
      Py_DECREF (old_handler);
      Py_INCREF (Py_None);
      self->handler = Py_None;
      return NULL;
    }

    /* Using PyTuple_SET_ITEM() instead of PyTuple_SetItem(), because we are
       populating a brand new tuple object, and the macro version is faster. */

    PyTuple_SET_ITEM (self->handler, 0, handler);

    /* Do not invoke Py_DECREF() on the set elements because PyTuple_SET_ITEM()
       steals the new reference returned by PySequence_GetItem(). */


  } else {
      PyErr_SetString (PyExc_ValueError, "handler must be a sequence, a Handler, or None");
      return NULL;
  }


  /* The existing handler sequence, if any, is no longer required. */

  Py_DECREF (old_handler);

  Py_RETURN_NONE;
}



/******************************************************************************/
char Camera_messages_doc[] =

	"Retrieve all pending messages associated with this :class:`Camera` "
        "instance. If there are no messages, an empty tuple will be returned. "
        "Once retrieved, the internal queue of messages is emptied.";

static PyObject *
Camera_messages (Camera *self, PyObject *args) {

  PyObject *timestamp, *type, *text;
  PyObject *message, *messages;

  Message *head, *current, *next;

  int result;


  messages = PyList_New (0);

  if (messages == NULL) {
    PyErr_SetString (PyExc_MemoryError, "cannot create a list");
    return NULL;
  }

  pthread_mutex_lock (&self->message_mutex);

  head = self->messages;
  self->messages = NULL;
  self->last_message = NULL;

  pthread_mutex_unlock (&self->message_mutex);


  for (current  = head;
       current != NULL;
       current  = next) {

    next = current->next;
    message = PyTuple_New (3);

    if (message == NULL) {
      PyErr_SetString (PyExc_MemoryError, "cannot create a tuple");
      Py_DECREF (messages);
      releaseMessages (self, head);
      return NULL;
    }

    if (current->type == NULL || current->text == NULL) {
      PyErr_SetString (PyExc_RuntimeError, "message content is NULL!");
      Py_DECREF (messages);
      releaseMessages (self, head);
      return NULL;
    }

    timestamp = PyFloat_FromDouble (current->timestamp);
    type = PyString_FromString (current->type);
    text = PyString_FromString (current->text);

    if (timestamp == NULL || type == NULL || text == NULL) {
      PyErr_SetString (PyExc_MemoryError, "failed to convert message values");
      Py_DECREF (messages);
      releaseMessages (self, head);
      return NULL;
    }

    /* Using PyTuple_SET_ITEM() instead of PyTuple_SetItem(), because we are
       populating a brand new tuple object, and the macro version is faster. */

    PyTuple_SET_ITEM (message, 0, timestamp);
    PyTuple_SET_ITEM (message, 1, type);
    PyTuple_SET_ITEM (message, 2, text);

    /* Do not invoke Py_DECREF() on the set elements because PyTuple_SET_ITEM()
       steals the new reference returned by PyString_FromString(). */


    result = PyList_Append (messages, message);

    if (result < 0) {
      /* An exception was raised. Let the caller handle it. */
      Py_DECREF (messages);
      releaseMessages (self, head);
      return NULL;
    }
  }

  releaseMessages (self, head);
  return messages;
}



/******************************************************************************/
char Camera_notify_doc[] =
	"Different :class:`Camera` methods may generate messages that could "
        "be interpreted for the benefit of a single listening application, "
        "such as a KTL dispatcher. The *function* registered with "
        ":func:`notify` should accept no arguments, and is only invoked as an "
        "indication that events are available. To retrieve messages, invoke "
        ":func:`messages`. If *function* is set to None, notification will be "
        "disabled.";

static PyObject *
Camera_notify (Camera *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"function", NULL};

  PyObject *function;


  if (!PyArg_ParseTupleAndKeywords (args, kwds, "O:notify", kwlist, &function)) {
    PyErr_SetString (PyExc_TypeError, "sole argument is a function");
    return NULL;
  }

  if (function != Py_None && !PyCallable_Check (function)) {
    PyErr_SetString (PyExc_ValueError, "function argument is not callable");
    return NULL;
  }

  Py_INCREF (function);
  Py_DECREF (self->notify);
  self->notify = function;

  Py_RETURN_NONE;
}



/******************************************************************************/
char Camera_serial_doc[] =

	"Send a *command* string via the serial interface for this Camera "
        "instance. The response, if any, will be returned as a string; "
        "if there is an error with the command, a :class:`SerialException` "
        "will be raised. If *wait* is set to False, :func:`serial` will not "
        "wait for a response string, and will return None. In practice, all "
        "serial commands generate a response, even if it is just an ACK. \n\n"

        ".. warning:: If the serial command does generate a response, and "
        "   *wait* is set to False, this can permanently confuse the edt "
        "   kernel driver. At least, it does with version 4.2.1.7 of the "
        "   driver. Queries that expect a response will return nothing; "
        "   other queries may receive the response generated for a previous "
        "   query. Unloading and reloading the driver fixes the problem. "
        "   And yet, when this occurs, the serial_cmd binary continues to "
        "   work normally; perhaps this is because it doesn't care about the "
        "   return result from :func:`pdv_serial_wait`.";

static PyObject *
Camera_serial (Camera *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"command", "wait", NULL};

  PyObject *wait = NULL;
  char *command = NULL;
  char *response = NULL;
  int pending=0, received=0, result=0;
  bool unpause=FALSE;

  char flush[1024] = "\0";

  if (!PyArg_ParseTupleAndKeywords (args, kwds, "s|O:serial", kwlist, &command, &wait)) {
    PyErr_SetString (PyExc_TypeError, "command must be a string");
    return NULL;
  }

  if (wait == NULL || wait == Py_None || wait == Py_True) {
    wait = Py_True;

  } else if (wait != Py_False ) {
    PyErr_SetString (PyExc_TypeError, "if specified, wait must be a boolean");
  }


  /* Pause the background thread (if any) before issuing serial commands. */

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);
  Py_END_ALLOW_THREADS

  if (self->started == TRUE && self->pause == FALSE) {
    unpause = TRUE;
    self->pause = TRUE;
  }

  pthread_mutex_unlock (&self->mutex);

  /* This function is based largely on EDT's serial_cmd.c example. */

  /* Before issuing the command, flush out any unread responses
     or other junk in the serial read buffer. */

  result = -1;
  
  while (result != 0) {
    result = pdv_serial_read (self->device, flush, sizeof (flush));
  }


  result = pdv_serial_command (self->device, command);

  if (result != 0) {
    if (unpause == TRUE) {
      Py_BEGIN_ALLOW_THREADS
      pthread_mutex_lock (&self->mutex);
      Py_END_ALLOW_THREADS

      self->pause = FALSE;
      pthread_cond_signal  (&self->condition);
      pthread_mutex_unlock (&self->mutex);
    }

    PyErr_Format (PyExc_RuntimeError, "unknown error (%d) sending serial command '%s'", result, command);
    return NULL;
  }

  appendMessage (self, "serial_command", command);


  /* If the caller doesn't want the response, there's nothing left to do. */

  if (wait == Py_False) {
    Py_RETURN_NONE;
  }


  /* pdv_serial_wait() will wait for either a timeout to occur, or for
     64 bytes to arrive, whichever comes first-- unless a serial 'waitchar'
     is set via pdv_set_waitchar(), or via the serial_waitchar configuration
     file directive. If a waitchar is set, pdv_serial_wait() returns immediately
     when the 'waitchar' is found in the response stream.

     A larger target count could be specified, but all of the API examples use
     64 characters. The actual contents of the serial response are acquired via
     pdv_serial_read().

     Use alloca() here so that we don't have to track whether to free() the
     response. This assumes that the response will not exhaust the available
     stack space for allocations. */

  Py_BEGIN_ALLOW_THREADS
  pending  = pdv_serial_wait (self->device, self->device->dd_p->serial_timeout, 64);
  Py_END_ALLOW_THREADS

  response = (char *) alloca ((pending + 1) * sizeof (char));

  received = 0;

  /* The first character of the reponse is typically an ACK/NAK.
     EDT's serial_cmd.c handles the following magic values:

     \x06   ACK   command acknowledged
     \x15   NAK   command rejected
     \x02   STX   Pulnix STX
     \x03   ETX   Pulnix ETX
   */
 
  result = pdv_serial_read (self->device, &response[0], 1);

  if (result < 0) {
    if (unpause == TRUE) {
      Py_BEGIN_ALLOW_THREADS
      pthread_mutex_lock (&self->mutex);
      Py_END_ALLOW_THREADS

      self->pause = FALSE;
      pthread_cond_signal  (&self->condition);
      pthread_mutex_unlock (&self->mutex);
    }

    PyErr_Format (PyExc_RuntimeError, "unknown error (%d) receiving serial response", result);
    return NULL;
  }


  switch (response[0]) {
    case 0x06:
      /* Don't include the ACK character as part of the response. */
      pending -= 1;

      if (pending == 0) {
        response[0] = '\0';
      }

      break;

    case 0x15:
      if (unpause == TRUE) {
        Py_BEGIN_ALLOW_THREADS
        pthread_mutex_lock (&self->mutex);
        Py_END_ALLOW_THREADS

        self->pause = FALSE;
        pthread_cond_signal  (&self->condition);
        pthread_mutex_unlock (&self->mutex);
      }

      PyErr_SetString (PyExc_ValueError, "serial command rejected");
      return (NULL);
      break;

    default:
      /* Unhandled first character. Return it as part of the response. */
      received = 1;
      break;
  }

  while (received < pending) {
    result = pdv_serial_read (self->device, &response[received], pending - received);

    if (result < 0) {
      break;
    }

    received += result;
  }


  if (unpause == TRUE) {
    Py_BEGIN_ALLOW_THREADS
    pthread_mutex_lock (&self->mutex);
    Py_END_ALLOW_THREADS

    self->pause = FALSE;
    pthread_cond_signal  (&self->condition);
    pthread_mutex_unlock (&self->mutex);
  }

  if (result < 0) {
    PyErr_Format (PyExc_RuntimeError, "unknown error (%d) receiving serial response", result);
    return NULL;
  }


  /* Trim trailing whitespace; in the case of the *Joe cameras, that's
     usually a trailing '\r\n'. */

  if (response[received] == '\0' && received > 1) {
    received--;
  }

  while (isspace (response[received])) {
    response[received] = '\0';
    received--;
  }

  appendMessage (self, "serial_response", response);
  return PyString_FromString (response);
}



/******************************************************************************/
char Camera_start_doc[] =

	"Request that the camera begin exposures, which should in turn "
        "prompt the background processing thread to handle the resulting "
        "images.";

static PyObject *
Camera_start (Camera *self, PyObject *args) {

  PyObject *response = NULL;


  /* Use Camera.serial() to send a '@SEQ 1' command. */

  response = serialCommand (self, "@SEQ 1", Py_True);

  if (response == NULL) {
    /* Pass the exception upstream. */
    return NULL;
  }

  Py_DECREF (response);

  printf("<Camera_start> signaling GO mode\n"); /***DEBUG***/
  /* Signal the background thread that it is time to start processing. */

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock   (&self->mutex);
  Py_END_ALLOW_THREADS

  self->pause = FALSE;
  pthread_cond_signal  (&self->condition);
  pthread_mutex_unlock (&self->mutex);


  Py_RETURN_NONE;
}



/******************************************************************************/
char Camera_stop_doc[] =

	"Request that the camera stop exposures, and pause all activity in "
        "the background processing thread.";

static PyObject *
Camera_stop (Camera *self, PyObject *args) {

  PyObject *response = NULL;


  /* Use Camera.serial() to send a '@SEQ 0' command. */

  response = serialCommand (self, "@SEQ 0", Py_True);

  if (response == NULL) {
    /* Pass the exception upstream. */
    return NULL;
  }

  Py_DECREF (response);


  /* Signal the background thread that it is time to stop processing. */

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock   (&self->mutex);
  Py_END_ALLOW_THREADS

  self->pause = TRUE;
  pthread_cond_signal  (&self->condition);
  pthread_mutex_unlock (&self->mutex);


  Py_RETURN_NONE;
}



/******************************************************************************/
char Camera_timing_doc[] =

	"Return a dictionary containing the current timing information "
        "recorded in the :class:`Camera` instance by the background "
        "processing thread.";

static PyObject *
Camera_timing (Camera *self, PyObject *args) {

  PyObject *results = NULL;
  PyObject *key = NULL;
  PyObject *value = NULL;

  double data;

  results = PyDict_New ();

  if (results == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyDict_New () returned NULL");
    return NULL;
  }


  key = PyString_FromString ("interval");

  if (key == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyString_FromString () returned NULL");
    return NULL;
  }


  data = self->frame_interval / 10.0;

  value = PyFloat_FromDouble (data);

  if (value == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyFloat_FromDouble () returned NULL");
    return NULL;
  }


  PyDict_SetItem (results, key, value);

  Py_DECREF (key);
  Py_DECREF (value);


  key = PyString_FromString ("processing");

  if (key == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyString_FromString () returned NULL");
    return NULL;
  }


  data = self->frame_processing / 10.0;

  value = PyFloat_FromDouble (data);

  if (value == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyFloat_FromDouble () returned NULL");
    return NULL;
  }


  PyDict_SetItem (results, key, value);

  Py_DECREF (key);
  Py_DECREF (value);


  return results;
}

/******************************************************************************/
   /* this is a change from the original file edt.c.sin */
char Camera_load_doc[] =
	"Loads the camera with data. Argument must be a 160x160 "
	"uint16 array.";

static PyObject *
Camera_load(Camera *self, PyObject *args)
{
  PyArrayObject *array;
  unsigned short *d, *p;
  int k,n;
  char *errstr = "callback must provide a uint16 160x160 array";

  if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array)) return NULL;

  if (array->descr->type_num != NPY_UINT16) {
    PyErr_SetString(PyExc_ValueError,errstr);
    return NULL;
  }
  if (array->nd != 2) {
    PyErr_SetString(PyExc_ValueError,errstr);
    return NULL;
  }
  if (array->dimensions[0] != 160 || array->dimensions[1] != 160) {
    PyErr_SetString(PyExc_ValueError,errstr);
    return NULL;
  }
  
  d = array->data;
  p = self->device->dd_p->image_data;
  n = 160*160;
  for (k=0; k<n; k++) p[k] = d[k];
  
  self->device->exposing = 0;
  return Py_BuildValue("");
}
/******************************************************************************/
   /* this is a change from the original file edt.c.sin */
char Camera_exposing_doc[] =
      "Examine whether the camera is exposing "
      "that is, idling in pdv_wait_image.";

static PyObject *
Camera_exposing(Camera *self, PyObject *args)
{
  return Py_BuildValue("i",self->device->exposing);
}

/******************************************************************************/
   /* this is a change from the original file edt.c.sin */
char Camera_done_exposing_doc[] =
      "Tells the camera it is done exposing "
      "that is, pdv_wait_image is released from idle.";

static PyObject *
Camera_done_exposing(Camera *self, PyObject *args)
{
  self->device->exposing = 0;
  return Py_BuildValue("i",self->device->exposing);
}

/******************************************************************************/
   /* this is a change from the original file edt.c.sin */
char Camera_set_callback_doc[] =
	"Set the call-back routine from the edt simulator (in c)"
	"to the sauce simulator (in python).";

static PyObject *
Camera_set_callback(Camera *self, PyObject *args)
{
    PyObject *result = NULL;
    PyObject *temp;

    if (PyArg_ParseTuple(args, "O:set_callback", &temp)) {
        if (!PyCallable_Check(temp)) {
            PyErr_SetString(PyExc_TypeError, "parameter must be callable");
            return NULL;
        }
        Py_XINCREF(temp);         /* Add a reference to new callback */
        Py_XDECREF(self->device->edt_callback);  /* Dispose of previous callback */
        self->device->edt_callback = temp;       /* Remember new callback */
	printf("<Camera_set_callback> %p %p\n",self,self->device->edt_callback);
        /* Boilerplate to return "None" */
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
}

/******************************************************************************/
   /* this is a change from the original file edt.c.sin */
char Camera_callback_doc[] =
	"Call the call-back routine from the edt simulator (in c)"
	"to the sauce simulator (in python).";

static PyObject *
Camera_callback(Camera *self, PyObject *args)
{
  PyObject *arglist;
  PyArrayObject *result;
  char *errstr = "callback must provide a uint16 160x160 array";
  
  if (self->device->edt_callback == NULL) {
    PyErr_SetString(PyExc_ValueError,"callback is not set.");
    return NULL;
  }
  arglist = Py_BuildValue("()");
  printf("<Camera_callback> %p %p\n",self,self->device->edt_callback);
  result = PyEval_CallObject(self->device->edt_callback,arglist);
  Py_DECREF(arglist);
  if (result == NULL) {
    return NULL;
  }
  if (result->ob_type != &PyArray_Type) {
    PyErr_SetString(PyExc_ValueError,errstr);
    Py_DECREF(result);
    return NULL;    
  }
  if (result->descr->type_num != NPY_UINT16) {
    PyErr_SetString(PyExc_ValueError,errstr);
    Py_DECREF(result);
    return NULL;
  }
  if (result->nd != 2) {
    PyErr_SetString(PyExc_ValueError,errstr);
    Py_DECREF(result);
    return NULL;
  }
  if (result->dimensions[0] != 160 || result->dimensions[1] != 160) {
    PyErr_SetString(PyExc_ValueError,errstr);
    Py_DECREF(result);
    return NULL;
  }
  return result;
}

/******************************************************************************/
/* Method definitions for the Handler class. */

char Handler_doc[] =
	"The :class:`Handler` class encapsulates all the necessary metadata "
        "for a single image analysis routine. Individual :class:`Handler` "
        "instances are linked to a single :class:`Camera` instance.";


static PyObject *
Handler_new (PyTypeObject *type, PyObject *args, PyObject *kwds) {

  Handler *self = NULL;

  self = (Handler *)type->tp_alloc (type, 0);

  if (self == NULL) {
    return NULL;
  }

  self->camera = NULL;
  self->cleanup = NULL;
  self->pyobject = NULL;
  self->voidstar = NULL;
  self->function = NULL;
  self->name = NULL;

  return (PyObject *)self;
}



static int
Handler_init (Handler *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"camera", "function", NULL};
  char *error=NULL, *name=NULL;
  void *dlhandle;
  char fname[128];


  if (!PyArg_ParseTupleAndKeywords (args, kwds, "Os", kwlist, &self->camera, &name)) {
    PyErr_SetString (PyExc_TypeError, "Handler instances require a Camera instance and a string function name");
    return -1; 
  }

  /* Playing with fire here. The Handler class assumes that it will not be the
     sole significant reference to a Camera instance, and thus does not
     increment the borrowed reference given by PyArg_ParseTupleAndKeywords().

     Handler_del() does not invoke Py_DECREF() on self->camera for this reason.
     
     By engaging in this modest abuse of the garbage collector, we eliminate the
     possibility of a reference loop unnecessarily keeping the Camera instance
     alive. */

  if (!PyObject_TypeCheck (self->camera, camera_type)) {
    PyErr_SetString (PyExc_TypeError, "camera argument must be a Camera instance");
    return -1;
  }

  self->name = strdup (name);


  /* Invoke dlerror() to clear any lingering error messages
     before invoking dlsym(). */

  dlerror ();

#ifdef __MACH__
  strcpy(fname,getenv("HOME"));
  strcat(fname,"/local/lib/python/rtc2.so");
  dlhandle = dlopen(fname,RTLD_NOW);
#else
  dlhandle = dlopen("/home/gavel/local/lib/rtc2.so",RTLD_NOW);
#endif
  if (dlhandle == NULL) {
    self->function = NULL;
  } else {
    self->function = dlsym (dlhandle, self->name);
  }

  if (self->function == NULL) {
    error = dlerror ();

    if (error != NULL) {
      PyErr_SetString (PyExc_ValueError, error);
    } else {
      PyErr_Format (PyExc_ValueError, "unknown error loading function '%s'", self->name);
    }

    return -1;
  }

  pthread_mutex_init (&self->mutex, NULL);

  return 0;
}



static void
Handler_del (Handler *self)
{
  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);
  Py_END_ALLOW_THREADS

  pthread_mutex_unlock  (&self->mutex);
  pthread_mutex_destroy (&self->mutex);

  Py_XDECREF (self->pyobject);

  /* Do not invoke Py_DECREF() on self->camera; the reference counts are
     not incremented in Handler_init(). */

  if (self->cleanup == NULL && self->voidstar != NULL) {
    free (self->voidstar);
  } else if (self->cleanup != NULL) {
    self->cleanup ();
  }

  free (self->name);

  self->ob_type->tp_free ((PyObject*)self);
}



/******************************************************************************/
char Handler_configure_doc[] =
	"A :class:`Handler` instance may be configured before it is invoked, "
        "or reconfigured at any time while a sequence of camera frames are "
        "being processed. :func:`configure` will block if the current "
        ":class:`Handler` instance is being run by the processing thread. "
        "The two arguments for :func:`configure` are a Python *data* object, "
        "and a C *function* name that will be used to interpret the *data* "
        "argument; Handler->pyobject will remain unchanged, leaving it for "
        "the *function* to manipulate directly. If a *function* is not "
        "specified, *data* will be assigned directly to Handler->pyobject.";

static PyObject *
Handler_configure (Handler *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"data", "function", NULL};

  PyObject *data;
  char *error=NULL, *function=NULL;

  int (*interpreter)(Handler *, PyObject *);
  int result;


  if (!PyArg_ParseTupleAndKeywords (args, kwds, "O|s:configure", kwlist, &data, &function)) {
    PyErr_SetString (PyExc_TypeError, "configure requires a data object, and optionally a function name");
    return NULL;
  }


  if (function == NULL) {

    Py_INCREF (data);

    /* Acquire the mutex before manipulating the PyObject * reference,
       just in case this Handler is in active use by the processing thread. */

    Py_BEGIN_ALLOW_THREADS
    pthread_mutex_lock (&self->mutex);
    Py_END_ALLOW_THREADS

    Py_XDECREF (self->pyobject);
    self->pyobject = data;
    pthread_mutex_unlock (&self->mutex);

    Py_RETURN_NONE;
  }


  /* Invoke dlerror() to clear any lingering error messages
     before invoking dlsym(). */

  dlerror ();

  interpreter = dlsym (RTLD_DEFAULT, function);

  if (interpreter == NULL) {
    error = dlerror ();

    if (error != NULL) {
      PyErr_SetString (PyExc_ValueError, error);
    } else {
      PyErr_Format (PyExc_ValueError, "unknown error loading function '%s'", function);
    }
    return NULL;
  }

  /* Acquire the mutex before invoking the interpreter function, just in
     case this Handler is in active use by the processing thread. */

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);
  Py_END_ALLOW_THREADS

  result = interpreter (self, data);
  pthread_mutex_unlock (&self->mutex);

  if (result < 0) {
    /* The interpreter function should have set an exception string to
       go along with the error code. Let the caller handle the exception. */

    return NULL;
  }

  Py_RETURN_NONE;
}



/******************************************************************************/
/* Internal functions, not exposed via the Python interface. */


/******************************************************************************/
/* Append a Message to the queue of pending messages for this
   Camera instance. */

void
appendMessage (Camera *instance, const char *type, const char *message) {

  Message *new=NULL;
  struct timespec timestamp;
  PyGILState_STATE gstate;

  clock_gettime (CLOCK_REALTIME, &timestamp);

  if (instance == NULL || type == NULL || message == NULL) {
    return;
  }

  if (instance->notify == Py_None) {
    return;
  }


  /* Rather than lock/unlock the mutex multiple times to protect the
     different critical sections in this function, do a single lock/unlock
     cycle. */

  pthread_mutex_lock (&instance->message_mutex);

  if (instance->free_messages == NULL) {
    new = (Message *) malloc (sizeof (struct Message));

  } else {

    /* Critical section. */
    new = instance->free_messages;
    instance->free_messages = new->next;
  }

  new->next = NULL;
  new->text = strdup (message);
  new->type = strdup (type);
  new->timestamp = timestamp.tv_sec + timestamp.tv_nsec / 1000000000.0;

  if (instance->last_message == NULL) {

    /* Critical section. */
    instance->messages = new;
    instance->last_message = new;

  } else {

    /* Critical section. */
    instance->last_message->next = new;
    instance->last_message = new;
  }

  pthread_mutex_unlock (&instance->message_mutex);


  /* Invoke the notification function in a thread-safe manner. */
  gstate = PyGILState_Ensure ();

  PyObject_CallFunctionObjArgs (instance->notify, NULL);

  PyGILState_Release (gstate);
}



/******************************************************************************/
/* Free the contents of each Message in this chain, and prepend it to the
   queue of available Message structs.*/

void
releaseMessages (Camera *instance, Message *message) {

  Message *current, *next, *last;

  if (message == NULL) {
    return;
  }

  for (current  = message;
       current != NULL;
       current  = next) {

    last = current;
    next = current->next;

    free (current->text);
    free (current->type);

    current->text = NULL;
    current->type = NULL;
  }

  if (instance == NULL) {
    /* This should never happen. Do the right thing anyway. */

    for (current  = message;
         current != NULL;
         current  = next) {

      next = current->next;
      free (current);
    }

    return;
  }


  pthread_mutex_lock (&instance->message_mutex);

  last->next = instance->free_messages;
  instance->free_messages = message;

  pthread_mutex_unlock (&instance->message_mutex);

  return;
}



/******************************************************************************/
/* Free all Message structs associated with this Camera instance. */

void
freeMessages (Camera *instance) {

  Message *current, *next;

  if (instance == NULL) {
    return;
  }

  for (current  = instance->messages;
       current != NULL;
       current  = next) {

    next = current->next;
    free (current->text);
    free (current->type);
    free (current);
  }

  for (current  = instance->free_messages;
       current != NULL;
       current  = next) {

    next = current->next;
    free (current);
  }
}



/******************************************************************************/
/* Given a Camera instance and a simple string, perform the necessary
   machinations to execute the given serial command via the Camera
   instance's serial() method. */

PyObject *
serialCommand (Camera *instance, const char *request, PyObject *wait) {

  PyObject *arguments = NULL;
  PyObject *command = NULL;
  PyObject *response = NULL;


  command = PyString_FromString (request);

  if (command == NULL) {
    PyErr_SetString (PyExc_MemoryError, "cannot create a string instance");
    return NULL;
  }


  arguments = PyTuple_New (2);

  if (arguments == NULL) {
    PyErr_SetString (PyExc_MemoryError, "cannot create a tuple");
    Py_DECREF (command);
    return NULL;
  }


  /* Using PyTuple_SET_ITEM() instead of PyTuple_SetItem(), because we are
     populating a brand new tuple object, and the macro version is faster. */

  PyTuple_SET_ITEM (arguments, 0, command);
  PyTuple_SET_ITEM (arguments, 1, wait);

  /* Do not invoke Py_DECREF() on the set elements because PyTuple_SET_ITEM()
     steals the new reference returned by PyString_FromString(). */


  response = Camera_serial (instance, arguments, NULL);

  Py_DECREF (arguments);

  return response;
}




/******************************************************************************/
/* Tables of all the Python-accessible functions and attributes in this file,
   mapping Python names to the appropriate C function, and associating
   docstrings as necessary. See the Python documentation for further details:

   http://docs.python.org/2/c-api/structures.html#PyMethodDef
   http://docs.python.org/2/c-api/structures.html#PyMemberDef

   These boilerplate declarations are at the end of the file so that all
   the docstrings can be defined adjacent to the functions themselves,
   and included here. */

PyMemberDef Camera_members[] = {
  {"config", T_OBJECT_EX, offsetof (Camera, config), READONLY, "EDT configuration file"},
  {"depth",  T_INT, offsetof (Camera, depth),  READONLY, "camera color depth"},
  {"handlers", T_OBJECT_EX, offsetof (Camera, handler), READONLY, "sequence of image handling functions"},
  {"height", T_INT, offsetof (Camera, height), READONLY, "image height in pixels"},
  {"interval", T_INT, offsetof (Camera, frame_interval), READONLY, "frame interval in microtenths"},
  {"processing", T_INT, offsetof (Camera, frame_processing), READONLY, "frame processing in microtenths"},
  {"unit",   T_OBJECT_EX, offsetof (Camera, unit), READONLY, "EDT device unit number"},
  {"width",  T_INT, offsetof (Camera, width),  READONLY, "image width in pixels"},
  {NULL}  /* Sentinel */
};

PyMethodDef Camera_methods[] = {
  {"handler",  (PyCFunction)Camera_handler,  METH_KEYWORDS, Camera_handler_doc},
  {"messages", (PyCFunction)Camera_messages, METH_NOARGS,   Camera_messages_doc},
  {"notify",   (PyCFunction)Camera_notify,   METH_KEYWORDS, Camera_notify_doc},
  {"serial",   (PyCFunction)Camera_serial,   METH_KEYWORDS, Camera_serial_doc},
  {"start",    (PyCFunction)Camera_start,    METH_NOARGS,   Camera_start_doc},
  {"stop",     (PyCFunction)Camera_stop,     METH_NOARGS,   Camera_stop_doc},
  {"timing",   (PyCFunction)Camera_timing,   METH_NOARGS,   Camera_timing_doc},	
  {"load",     (PyCFunction)Camera_load,     METH_VARARGS,  Camera_load_doc},
  {"done_exposing", (PyCFunction)Camera_done_exposing, METH_NOARGS,  Camera_done_exposing_doc},
  {"exposing", (PyCFunction)Camera_exposing, METH_NOARGS,   Camera_exposing_doc},
  {"set_callback", (PyCFunction)Camera_set_callback,	METH_VARARGS, Camera_set_callback_doc},
  {"callback", (PyCFunction)Camera_callback, METH_NOARGS,   Camera_callback_doc},
  {NULL}  /* Sentinel */
};


PyMemberDef Handler_members[] = {
  {"name", T_STRING, offsetof (Handler, name), READONLY, "Handler function name"},
  {NULL}  /* Sentinel */
};

PyMethodDef Handler_methods[] = {
  {"configure", (PyCFunction)Handler_configure, METH_KEYWORDS, Handler_configure_doc},
  {NULL}  /* Sentinel */
};


static PyMethodDef EDT_module_methods[] = {
  {NULL} /* Sentinel */
};


static PyTypeObject CameraType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "edt.Camera",              /*tp_name*/
  sizeof (Camera),           /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)Camera_del,    /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  Camera_doc,                /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  Camera_methods,            /* tp_methods */
  Camera_members,            /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)Camera_init,     /* tp_init */
  0,                         /* tp_alloc */
  Camera_new,                /* tp_new */
};


static PyTypeObject HandlerType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "edt.Handler",             /*tp_name*/
  sizeof (Handler),          /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)Handler_del,   /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  Handler_doc,               /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  Handler_methods,           /* tp_methods */
  Handler_members,           /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)Handler_init,    /* tp_init */
  0,                         /* tp_alloc */
  Handler_new,               /* tp_new */
};


/* And last but not least, the function that Python will use to initialize
   the module, according to the above table. The function name has a
   mandatory structure: init<module_name>(), in our case, initedt().*/


PyMODINIT_FUNC
initedt (void) {
  PyObject* module=NULL;

  if (PyType_Ready (&CameraType) < 0) {
    return;
  }

  if (PyType_Ready (&HandlerType) < 0) {
    return;
  }

  camera_type = &CameraType;
  handler_type = &HandlerType;

  module = Py_InitModule3 ("edt", EDT_module_methods, EDT_doc);
  import_array();    /* this is a change from the original file edt.c.sin */
  PyEval_InitThreads();  /* this is a change from the original file edt.c.sin */
  
  if (module == NULL) {
    return;
  }

  Py_INCREF (&CameraType);
  Py_INCREF (&HandlerType);

  PyModule_AddObject (module, "Camera", (PyObject *)&CameraType);
  PyModule_AddObject (module, "Handler", (PyObject *)&HandlerType);
}


/* Hitting tab gets you two spaces. Actual tabs render as eight characters,
 * the way that any (sane) editor expects them to.
 *
 * vim: set expandtab tabstop=8 softtabstop=2 shiftwidth=2 autoindent: 
 */

/* EOF */
