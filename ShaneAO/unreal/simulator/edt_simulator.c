/* EDT frame-grabber simulator */
#include "edt_simulator.h"
# include <stdio.h>
# include <time.h>
# include <stdlib.h>

unsigned short *image_pointer[2]; /* for the two channels: WFS and TTS */
int width, height, depth; /* temporary storage for image size information */

/* functions */

EdtDev *
edt_open_channel(const char *device_name, int unit, int channel)
{
    EdtDev *dev;
    dev = (EdtDev *)malloc(sizeof(EdtDev));
    dev->unit_no = unit;
    dev->edt_callback = NULL;
    dev->exposing = 0;
    return dev;
}

uint_t
edt_reg_read(EdtDev *edt_p, uint_t desc)
{
    return 0;
}

int
edt_close(EdtDev *edt_p)
{
    if (edt_p) {
        free(edt_p);
    }
}

Dependent *
pdv_alloc_dependent(void)
{
    return (Dependent *)malloc(sizeof(Dependent));
}

int
pdv_readcfg(const char *cfgfile, Dependent *pm, Edtinfo *ei)
{
    pm->width = 160;
    pm->height = 160;
    pm->depth = 2;
    pm->force_single = 0;
    pm->serial_waitc = (u_int)'\n';
    pm->image_data = NULL;
    return 0;
}

int
pdv_initcam(EdtDev *edt_p, Dependent *dd_p, int unit, Edtinfo *edtinfo,
            const char *cfgfname, char *bitdir, int pdv_debug)
{
    int j,k,n,m;
    unsigned short *p;
    
    width = dd_p->width;
    height = dd_p->height;
    depth = dd_p->depth;
    image_pointer[unit] = malloc(width*height*sizeof(unsigned short));
    
    /* put a pattern on the simulated "data" */
    p = image_pointer[unit];
    n = dd_p->width;
    m = dd_p->height;
    for (k = 0; k< n; k++) {
        for (j = 0; j< m; j++) {
            *p = (unsigned short)(j*k);
            p++;
        }
    }

    pthread_cond_init (&edt_p->condition, NULL);
    pthread_mutex_init (&edt_p->mutex, NULL);

    return 0;
}

PdvDev *
pdv_open_channel(const char *edt_devname, int unit, int channel)
{
    PdvDev * pd;
    pd = (PdvDev *)edt_open_channel(edt_devname,unit, channel);
    pd->dd_p = pdv_alloc_dependent();
    pd->dd_p->width = width;
    pd->dd_p->height = height;
    pd->dd_p->depth = depth;
    pd->dd_p->image_data = image_pointer[unit];
    return pd;
}

int
pdv_multibuf(EdtDev *edt_p, int numbufs)
{
    edt_p->ring_buffer_numbufs = numbufs;
    return 0;
}

int
pdv_get_waitchar(PdvDev * pdv_p, u_char *wchar)
{
    *wchar = pdv_p->dd_p->serial_waitc;
    return 1;
}

int
pdv_set_waitchar(PdvDev *pdv_p, int enable, u_char wchar)
{
    pdv_p->dd_p->serial_waitc = wchar;
    return 1;
}

int
pdv_get_cam_width(PdvDev *pdv_p)
{
    return pdv_p->dd_p->width;
}

int
pdv_get_cam_height(PdvDev *pdv_p)
{
    return pdv_p->dd_p->height;
}

int
pdv_get_depth(PdvDev *pdv_p)
{
    return pdv_p->dd_p->depth;
}

int
pdv_serial_command(EdtDev *pd, const char *cmd)
{
    printf("<pdv_serial_command> %s\n",cmd);
    return 0;
}

int
pdv_serial_wait(EdtDev *pd, int msecs, int count)
{
    return 1; /* ACK  +' OK' */
}

int
pdv_serial_read(PdvDev *fd, char *buf, int size)
{
    if (size > 4) {
        return 0;
    }
    if (size == 1) {
        buf[0] = 0x06;
        return 1;
    }
    buf[0] = ' ';
    buf[1] = 'O';
    buf[2] = 'K';
    buf[3] = '\n';
    return 4;
}

void
pdv_start_image (PdvDev *pdv_p)
{
    return;
}

void
pdv_start_images (PdvDev *pdv_p, int count)
{
    return;
}

unsigned char  *
pdv_wait_image_old(PdvDev *pdv_p)
{
    struct timespec rqtp, rmtp;
    int ret;
    unsigned short d, *p;
    int k,n;
    float ns,us,ms,dt;
    
    ns = 1.0e-9;
    us = 1.0e-6;
    ms = 1.0e-3;
    dt = 1000.*us;
    //printf("<pdv_wait_image> click\n"); /***DEBUG***/
    rqtp.tv_sec = 0;
    rqtp.tv_nsec = (long)(dt/ns);
#ifdef __MACH__
    nanosleep(&rqtp,&rmtp);
#else
    mywait(1);
#endif
    n = 160*160;
    p = (unsigned short *)pdv_p->dd_p->image_data;
    for (k=0; k<n/2; k++) {
        d = (unsigned short)(drand48()*43000);
        *p++ = d;        
    }
    return pdv_p->dd_p->image_data;
}

unsigned char *
pdv_wait_image(PdvDev *pdv_p)
{
    PyObject *arglist;
    PyArrayObject *result;
    int k,n;
    unsigned short *p, *d;
    PyGILState_STATE gstate;
    
    n = 160*160;
    p = (unsigned short *)pdv_p->dd_p->image_data;

    pdv_p->exposing = 1;
    
    if (pdv_p->edt_callback == NULL) {
        return p;
    }
    
    arglist = Py_BuildValue("()");

    gstate = PyGILState_Ensure();

    result = PyEval_CallObject(pdv_p->edt_callback,arglist);
    Py_DECREF(arglist);

    d = result->data;
    
    for (k=0; k<n; k++) p[k] = d[k];
    Py_DECREF(result);

    PyGILState_Release(gstate);
    
    return p;
}

int
pdv_timeouts(PdvDev *pdv_p)
{
    return 0;
}

int
pdv_timeout_restart(PdvDev *pdv_p, int restart)
{
    return 0;
}

int
pdv_close(PdvDev *pdv_p)
{
    if (pdv_p) {
        if (pdv_p->dd_p) {
            if (pdv_p->dd_p->image_data) {
                free(pdv_p->dd_p->image_data);
            }
            free(pdv_p->dd_p);
        }
        free(pdv_p);
    }
}
