/* Test timing on mac */

#include <stdio.h>
#include <time.h>

#ifdef __MACH__
#include <mach/mach_time.h>

#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 0

#ifndef CLOCK_MONOTONIC_RAW     /* Not available on CentOS 5, kernel < 2.6.28 */
#define CLOCK_MONOTONIC_RAW CLOCK_MONOTONIC
#endif

#endif

int main() {
    struct timespec rqtp, rmtp;
    struct timespec handoff, completion;
    int ret,k,n;
    float ns,us,ms,dt;
    long result_sec, result_ns;
    float result;
    
    ns = 1.0e-9;
    us = 1.0e-6;
    ms = 1.0e-3;
    dt = 500*ms;
    n = 7;
    rqtp.tv_sec = 0;
    rqtp.tv_nsec = (long)(dt/ns);
    printf("<testtime> starting\n");
    printf("<testtime> rqtp: %ld sec, %ld ns\n",rqtp.tv_sec,rqtp.tv_nsec);
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &handoff);
    printf("<testtime> handoff: %u, %u \n",handoff.tv_sec,handoff.tv_nsec);
    
    for (k=0; k<n; k++) {
        printf("tick\n");
        ret = nanosleep(&rqtp,&rmtp);    
        //ret = nanosleep(&rqtp,&rmtp);    
    }
    
    clock_gettime(CLOCK_MONOTONIC_RAW, &completion);
    
    printf("<testtime> done\n");
    
    printf("<testtime> completion: %u, %u \n",completion.tv_sec,completion.tv_nsec);
    
    result_sec = completion.tv_sec - handoff.tv_sec;
    result_ns = completion.tv_nsec - handoff.tv_nsec;
    result = (float)result_sec + (float)(result_ns)*ns;
    printf("<testtime> results: %ld, %ld, %g\n",result_sec,result_ns,result);
}

#ifdef __MACH__
int clock_gettime(int clk_id, struct timespec *t /*struct mach_timespec *t*/){
    mach_timebase_info_data_t timebase;
    mach_timebase_info(&timebase);
    uint64_t time;
    time = mach_absolute_time();
     printf("<clock_gettime> timebase numer: %u, denom: %u\n",timebase.numer,timebase.denom);
     printf("<clock_gettime> time: %ld\n",time);

//    double nseconds = ((double)time * (double)timebase.numer)/((double)timebase.denom);
    double seconds = ((double)time * (double)timebase.numer)/((double)timebase.denom * 1e9);
     printf("<clock_gettime> seconds: %lg\n",seconds);

    double nseconds = ((double)time - (double)((uint64_t)seconds)*1e9);
     printf("<clock_gettime> nseconds: %lg\n",nseconds);

    t->tv_sec = seconds;
    t->tv_nsec = nseconds;
     printf("<clock_gettime> tv_sec: %u, tv_nsec: %u\n",t->tv_sec,t->tv_nsec);
    return 0;
}
#endif