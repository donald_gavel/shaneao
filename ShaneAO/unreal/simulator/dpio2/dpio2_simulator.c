/* dpio2 simulator */

/* Both VMOSA and VM_LINUX need to be defined before including
   the DPIO header files. */

#import <stdio.h>

#ifndef VMOSA
#define VMOSA 1
#endif

#ifndef VM_LINUX
#define VM_LINUX 1
#endif

#include "vmosa.h"
#include "dpio2.h"
#include "dpio2-linux.h"

#include <Python.h>
#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif

#ifndef bool
typedef int bool;
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

typedef struct DPIO {
  PyObject_HEAD
  PyObject *device;
  PyObject *frequency;
  PyObject *buffer_size;
  bool open;
  pthread_cond_t condition;
  pthread_mutex_t mutex;
  unsigned short *buffer;
  bool dma_complete;
  long processing_time;
  char *log_ident;
} DPIO;

static DPIO2_INTERRUPT_CALLBACK callback;
static PyObject *PyCallback = NULL;

static int said_it_once = 0;

void
dpio2_simulator_set_pycallback(PyObject *arg)
{
  Py_XINCREF(arg);
  Py_XDECREF(PyCallback);
  PyCallback = arg;
  return;
}

STATUS
dpio2Ioctl (int devno, int command, long argument)
{
    DPIO *self;
    PyObject *arglist, *result;
    PyGILState_STATE gstate;
    
    if (command == DPIO2_CMD_INTERRUPT_CALLBACK_ATTACH) {
        callback = *((DPIO2_INTERRUPT_CALLBACK *)argument);
    }
    if (command == DPIO2_CMD_DMA_START) {
        self = (DPIO *)callback.argument;
        if (said_it_once == 0) printf("<dpio2Ioctl> self = %p\n",self);
        /* tell the sauce simulator this happened */
        if (PyCallback != NULL) {
          
          arglist = Py_BuildValue("()");
          
          gstate = PyGILState_Ensure();
          
          result = PyEval_CallObject(PyCallback,arglist);
          Py_DECREF(arglist);
          
          Py_XDECREF(result);
          
          PyGILState_Release(gstate);
          
        }
        (*callback.pCallbackFunction)((long)self);
        said_it_once = 1;
    }
    return (STATUS)OK;
}

int
dpio2Scan  (DPIO2_INFO_T *pInfoArray)
{
  return 1;
}

STATUS
dpio2Open  (int devno, int mode)
{
    return (STATUS)OK;
}

STATUS
dpio2Close(int devno)
{
    return (STATUS)OK;
}

STATUS
dpio2DMALock (int devno, DPIO2_DMA *pDma)
{
    pDma->Page[0].pPhysicalAddr = 0;
    return (STATUS)OK;
}

void
dpio2DMAUnlock (int devno, DPIO2_DMA *pDma)
{
    return;
}
