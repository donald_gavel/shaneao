char DPIO2_doc[] =
        "The dpio2 Python module exposes the DPIO2 user-mode Linux API for use "
        "by Python applications.";


/* Python.h must be included before any standard header files, per the
   Python/C API documentation. */

#include <Python.h>
#include <structmember.h>   /* defines PyMemberDef and supporting symbols */
// #include <config.h> /*** in kroot ***/ /* this is a changed from the original dpio2.c */
#include "numpy/arrayobject.h" /* this is a changed from the original dpio2.c */

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#ifdef HAVE_TIME_H
#include <time.h>               /* struct timespec */
#endif

#include <stdarg.h>             /* variable arguments */
#include <syslog.h>             /* use syslog for logging */

#ifdef __MACH__
#include <mach/mach_time.h>
int clock_gettime(int clk_id, struct mach_timespec *t);
#define CLOCK_REALTIME 0
#define CLOCK_MONOTONIC 0
#else
#include <time.h>
#endif

#ifndef CLOCK_MONOTONIC_RAW     /* Not available on CentOS 5, kernel < 2.6.28 */
#define CLOCK_MONOTONIC_RAW CLOCK_MONOTONIC
#endif

/* Both VMOSA and VM_LINUX need to be defined before including
   the DPIO header files. */

#ifndef VMOSA
#define VMOSA 1
#endif

#ifndef VM_LINUX
#define VM_LINUX 1
#endif

#include "vmosa.h"
#include "dpio2.h"
#include "dpio2-linux.h"

/* Backwards compatibility with Python 2.4 for the Py_ssize_t type,
   which is a standard definition in Python 2.5 and later. */

#if PY_VERSION_HEX < 0x02050000 && !defined (PY_SSIZE_T_MIN)
typedef int Py_ssize_t;
#define PY_SSIZE_T_MAX INT_MAX
#define PY_SSIZE_T_MIN INT_MIN
#endif

#ifndef PyMODINIT_FUNC  /* boilerplate macro for DLL import/export */
#define PyMODINIT_FUNC void
#endif

#ifndef bool
typedef int bool;
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

char DPIO_doc[] =

  "A class representing a generic DPIO2 device. This class should never "
  "be invoked directly; use the :class:`Input` or class:`Output` subclasses "
  "instead.";

char Output_doc[] =

  "A class representing a DPIO2 device configured for output operations. "
  "The initialization of the device is not generic, it is done in the "
  "fashion expected for use with the Shane AO Boston Micromachines device.";


/* Global count of the number of available DPIO2 devices, as reported by
   dpio2Scan(). This gets filled in during module initialization. */

int device_count = -1;

/* Size of a memory page. This gets filled in during module initialization. */

int page_size = -1;


/* Fundamental struct for the DPIO class. */

typedef struct DPIO {
  PyObject_HEAD
  PyObject *device;
  PyObject *frequency;
  PyObject *buffer_size;
  bool open;
  pthread_cond_t condition;
  pthread_mutex_t mutex;
  unsigned short *buffer;
  bool dma_complete;
  long processing_time;
  char *log_ident;
} DPIO;


/* Prototypes for internal functions. */

static void dlog (DPIO *self, int priority, const char *format, ...);
static void dmaCallback (DWORD argument);
static long microtenths (const struct timespec minuend, const struct timespec subtrahend);
static STATUS parseValues (DPIO *self, PyObject *values);


/* Member functions for the DPIO and Output classes. */

char DPIO_current_doc[] =

  "Return a tuple of the current buffer contents.";

PyObject *
DPIO_current (DPIO *self, PyObject *args) {

  PyObject *number = NULL;
  PyObject *tuple = NULL;

  int count = 0;
  long buffer_size = -1;
  int elements = -1;
  
  
  buffer_size = PyInt_AsLong (self->buffer_size);

  if (buffer_size == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return NULL;
  }

  elements = buffer_size / 2;


  tuple = PyTuple_New (elements);

  if (tuple == NULL) {
    PyErr_SetString (PyExc_MemoryError, "PyTuple_New () returned a NULL tuple");
    return NULL;
  }

  while (count < elements) {

    number = PyInt_FromLong ((long)self->buffer[count]);

    if (number == NULL) {
      /* An exception is waiting. Let the caller handle it. */
      return NULL;
    }

    /* Using PyTuple_SET_ITEM () instead of PyTuple_SetItem (), because we are
       populating a brand new tuple object, and the macro version is faster. */

    PyTuple_SET_ITEM (tuple, count, number);

    /* Do not invoke Py_DECREF on 'descriptor' because PyTuple_SET_ITEM ()
       steals the reference. */

    count++;
  }

  return tuple;
}



static void
DPIO_del (DPIO *self) {

  long device = -1;
  
  if (self->device == NULL) {
    return;
  }

  device = PyInt_AsLong (self->device);

  if (device == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return;
  }

  if (self->log_ident != NULL) {
    self->log_ident = NULL;
    closelog ();
  }

  if (self->open == FALSE) {
    return;
  }

  /* The Boston Micromachines example code takes care to lower the
     PIO strobes and turn them off before closing the device. */

  dpio2Ioctl (device, DPIO2_CMD_PIO1_OUTPUT_VALUE_SET, FALSE);
  dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, FALSE);
  dpio2Ioctl (device, DPIO2_CMD_PIO1_DIRECTION_SELECT, FALSE);
  dpio2Ioctl (device, DPIO2_CMD_PIO2_DIRECTION_SELECT, FALSE);
  dpio2Close (device);

  pthread_mutex_destroy (&self->mutex);
  pthread_cond_destroy  (&self->condition);

  free (self->buffer);
  self->buffer = NULL;

  Py_XDECREF (self->buffer_size);
  Py_XDECREF (self->device);
  Py_XDECREF (self->frequency);

  return;
}



static int
DPIO_init (DPIO *self, PyObject *args, PyObject *kwds) {

  /* Do nothing. It would be nice to raise a NotImplementedError here,
     but that apparently causes problems with pickling. */

  return 0;
}



static int
Output_init (DPIO *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"device", "buffer", NULL};

  int device=-1, buffer_size=-1;
  long frequency = 50000000;
  STATUS status;
  DPIO2_INTERRUPT_CALLBACK callback;


  if (!PyArg_ParseTupleAndKeywords (args, kwds, "ii", kwlist, &device, &buffer_size)) {
    PyErr_SetString (PyExc_TypeError, "must specify a device number and buffer size");
    return -1;
  }


  /* The API documentation for dpio2Scan() states: "The maximum number of
     DPIO2 Modules is 8." */

  if (device < 1 || device > MAX_DPIO2_CARDS) {
    PyErr_Format (PyExc_ValueError, "device number must be between 1 and %d", MAX_DPIO2_CARDS);
    return -1;
  }

  if (device_count == -1) {
    PyErr_SetString (PyExc_RuntimeError, "device_count is -1; did dpio2Scan() fail?");
    return -1;
  }

  if (device > device_count) {
    PyErr_Format (PyExc_ValueError, "device number %d too high, only discovered %d device(s)", device, device_count);
    return -1;
  }

  self->device = PyInt_FromLong ((long)device);

  if (self->device == NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return -1;
  }

  /* Dave's code suggests that it must be aligned to a 4 byte boundary;
     the example code from Boston Micromachines enforces an 8 byte boundary. */

  if ((buffer_size & 0x7) != 0) {
    PyErr_Format (PyExc_RuntimeError, "buffer size (%d) must be a multiple of 8 bytes", buffer_size);
    return -1;
  }


  status = dpio2Open (device, DPIO2_OUTPUT);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to open DPIO2 device");
    return -1;
  }

  self->open = TRUE;


  /* The following sequence of dpio* commands is derived from the
     user-side DPIO interface layer from Dave Palmer's code base. */

  /* Configure PIO strobes, used for handshaking with the Cambridge
     Innovations hardware. */

  status = dpio2Ioctl (device, DPIO2_CMD_PIO1_DIRECTION_SELECT, TRUE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to set PIO1 direction");
    return -1;
  }

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_DIRECTION_SELECT, TRUE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to set PIO2 direction");
    return -1;
  }

  status = dpio2Ioctl (device, DPIO2_CMD_PIO1_OUTPUT_VALUE_SET, TRUE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to raise PIO1 strobe");
    return -1;
  }

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, TRUE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to raise PIO2 strobe");
    return -1;
  }


  /* Initialize our mutex and condition, and register DMA callback
     handlers. */

  pthread_cond_init  (&self->condition, NULL);
  pthread_mutex_init (&self->mutex, NULL);

  callback.condition = DPIO2_INT_COND_DMA_DONE_CHAIN;
  callback.pCallbackFunction = (FUNCPTR)dmaCallback;
  callback.argument = (long)self;

  status = dpio2Ioctl (device, DPIO2_CMD_INTERRUPT_CALLBACK_ATTACH, (long)&callback);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to register DMA callback");
    return -1;
  }

  status = dpio2Ioctl (device, DPIO2_CMD_INTERRUPT_ENABLE, DPIO2_INT_COND_DMA_DONE_CHAIN);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to enable interrupts");
    return -1;
  }


  /* Set the strobe frequency. */

  /* Use an arbitrary default of 50 MHz. If that ever needs to change,
     it should be set via an argument to this function. */

  status = dpio2Ioctl (device, DPIO2_CMD_STROBE_FREQUENCY_SET, (long)&frequency);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to set strobe frequency");
    return -1;
  }

  /* The 'frequency' argument is updated by the dpio2Ioctl() call to reflect
     the actual frequency used. */

  self->frequency = PyInt_FromLong (frequency);


  status = dpio2Ioctl (device, DPIO2_CMD_STROBE_GENERATION_ENABLE, DPIO2_PROGRAMMABLE_OSCILLATOR);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to enable strobe generation");
    return -1;
  }


  /* Contrary to the example code, enabling the following feature causes the
     Cambridge Innovations box to light its "frame error" indicator on every
     attempted DMA transfer. */

  status = dpio2Ioctl (device, DPIO2_CMD_CLOCKING_ON_BOTH_STROBE_EDGES_SELECT, FALSE);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to disable clocking on both edges");
    return -1;
  }


  status = dpio2Ioctl (device, DPIO2_CMD_FIFO_FLUSH, 0);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to flush command buffer");
    return -1;
  }


  status = dpio2Ioctl (device, DPIO2_CMD_DATA_SWAP_MODE_SELECT, DPIO2_NO_SWAP);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to disable endian swapping");
    return -1;
  }


  status = dpio2Ioctl (device, DPIO2_CMD_FPDP_ACTIVATION_SELECT, TRUE);

  if (status == ERROR ) {
    PyErr_SetString (PyExc_RuntimeError, "failed to activate FPDP interface");
    return -1;
  }


  /* Create and zero out the buffer that will be used for DMA transfer. */

  self->buffer_size = PyInt_FromLong ((long)buffer_size);

  status = posix_memalign ((void *)&self->buffer, 0x100, (size_t)buffer_size);

  if (status != 0) {
    PyErr_Format (PyExc_MemoryError, "failed to allocate buffer of size %d", buffer_size);
    return -1;
  }

  memset (self->buffer, 0, (size_t)buffer_size);


  return 0;
}



char DPIO_logging_doc[] =

  "Configure logging for this DPIO instance. If *ident* is set to None, "
  "logging will be disabled. Otherwise, *ident* should be set to an "
  "appropriate syslog ident string, and *facility* should be set to an integer "
  "syslog facility; using the :mod:`syslog` module, that might be specified as "
  "``syslog.LOG_LOCAL7``.";

static PyObject *
DPIO_logging (DPIO *self, PyObject *args, PyObject *kwds) {

  PyObject *ident = NULL;
  int facility = -1;

  static char *kwlist[] = {"ident", "facility", NULL};

  if (!PyArg_ParseTupleAndKeywords (args, kwds, "O|i", kwlist, &ident, &facility)) {
    PyErr_SetString (PyExc_TypeError, "arguments must be an ident and facility");
    return NULL;
  }


  /* Close the previous syslog connection, if any. */

  if (self->log_ident != NULL) {
    self->log_ident = NULL;
    closelog ();
  }


  /* If logging was disabled by passing None, no further actions
     are required. */

  if (ident == Py_None) {
    Py_RETURN_NONE;
  }


  if (facility == -1) {
    PyErr_SetString (PyExc_TypeError, "facility must be specified if ident is not None");
    return NULL;
  }


  self->log_ident = PyString_AsString (ident);

  if (self->log_ident == NULL ) {
    /* TypeError raised. Let the caller handle the exception. */
    return NULL;
  }

  openlog (self->log_ident, 0, facility);

  Py_RETURN_NONE;
}



static PyObject *
DPIO_new (PyTypeObject *type, PyObject *args, PyObject *kwds) {

  DPIO *self = NULL;

  self = (DPIO *)type->tp_alloc (type, 0);

  if (self == NULL) {
    return NULL;
  }

  self->buffer = NULL;
  self->buffer_size = NULL;
  self->device = NULL;
  self->dma_complete = FALSE;
  self->frequency = NULL;
  self->log_ident = NULL;
  self->open = FALSE;
  self->processing_time = 0;

  return (PyObject *)self;
}



char DPIO_print_doc[] =

  "Print the current value of the buffer to stdout. Only of any use "
  "for debugging.";

PyObject *
DPIO_print (DPIO *self, PyObject *args) {

  int count = 0;
  long buffer_size = PyInt_AsLong (self->buffer_size);
  int elements = buffer_size / 2;
  int row_length = sqrt (elements);

  while (count < elements) {

    printf ("%04x", (unsigned short)self->buffer[count]);
    count++;

    if (count % row_length == 0) {
      printf ("\n");
    } else {
      printf (" ");
    }
  }

  Py_RETURN_NONE;
}



char DPIO_timing_doc[] =

        "Return a dictionary containing the current timing information "
        "recorded for this :class:`DPIO` instance.";

PyObject *
DPIO_timing (DPIO *self, PyObject *args) {

  PyObject *results = NULL;
  PyObject *key = NULL;
  PyObject *value = NULL;

  double data;


  results = PyDict_New ();

  if (results == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyDict_New () returned NULL");
    return NULL;
  }


  key = PyString_FromString ("processing");

  if (key == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyString_FromString () returned NULL");
    return NULL;
  }


  /* The processing time is stored as tenths of microseconds. Provide results
     in fractional microseconds. */

  data = self->processing_time / 10.0;

  value = PyFloat_FromDouble (data);

  if (value == NULL)
  {
    PyErr_SetString (PyExc_MemoryError, "PyFloat_FromDouble () returned NULL");
    return NULL;
  }


  PyDict_SetItem (results, key, value);

  Py_DECREF (key);
  Py_DECREF (value);


  return results;
}

/*********************************************************/
/* this is a change from the original dpio2.c */
char Output_peek_doc[] =
  "Get a peek at the dpio2 buffer."
  "This returns a numpy array whose data pointer directly "
  "addresses the dpio2 buffer.";

PyObject *
Output_peek (DPIO *self, PyObject *args) {
  PyObject *peek_array;
  int nd;
  npy_intp dims[2];
  unsigned short *p;
  
  nd = 2;
  dims[0] = 32;
  dims[1] = 32;
  p = self->buffer;
  peek_array = PyArray_SimpleNewFromData(nd,dims,NPY_USHORT,(void *)p);
  return peek_array;
}

/* this is a change from the original dpio2.c */
char Output_set_callback_doc[] =
  "Set the dpio2 Output callback routine. "
  "This will be called (back) on Output_write. ";

void dpio2_simulator_set_pycallback(PyObject *arg);

static PyObject *
Output_set_callback(DPIO *self, PyObject *args)
{
    PyObject *result = NULL;
    PyObject *temp;

    if (PyArg_ParseTuple(args, "O:set_callback", &temp)) {
        if (!PyCallable_Check(temp)) {
            PyErr_SetString(PyExc_TypeError, "parameter must be callable");
            return NULL;
        }
        dpio2_simulator_set_pycallback(temp);
        /* Boilerplate to return "None" */
        Py_INCREF(Py_None);
        result = Py_None;
    }
    return result;
  
}

/*********************************************************/

char Output_write_doc[] =

  "Initiate a DMA transfer. If *values* is specified, it must be a sequence "
  "type whose length matches the current buffer size; if specified, *values* "
  "must be a sequence of integers, each 0 to 65535, and will be used to update "
  "the contents of the DMA buffer before initiating the transfer. If *values* "
  "is not specified or is set to None, the current buffer will be written "
  "without any updates. This function will block until the DMA transfer "
  "completes.";

PyObject *
Output_write (DPIO *self, PyObject *args, PyObject *kwds) {

  static char *kwlist[] = {"values", NULL};

  int device=-1, buffer_size=-1, page=0, pages=-1;
  long initial_id = 0;
  PyObject *values = NULL;
  struct timespec wake_time, start_time, end_time;

  DPIO2_DMA *page_info = NULL;
  DPIO2_DMA_DESC dma_descriptor;
  STATUS status;


  clock_gettime (CLOCK_MONOTONIC_RAW, &start_time);

  if (!PyArg_ParseTupleAndKeywords (args, kwds, "|O", kwlist, &values)) {
    PyErr_SetString (PyExc_TypeError, "sole optional argument is a set of values");
    return NULL;
  }

  if (values != NULL && values != Py_None) {
    status = parseValues (self, values);

    if (status == ERROR) {
      /* An exception is waiting. Let the caller handle it. */
      return NULL;
    }
  }

  device = PyInt_AsLong (self->device);

  if (device == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return NULL;
  }

  buffer_size = PyInt_AsLong (self->buffer_size);

  if (buffer_size == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return NULL;
  }

  /* Allocate and lock a buffer for the DMA request. */

  pages = 1 + (buffer_size + page_size - 1) / page_size;

  page_info = (DPIO2_DMA *)malloc (sizeof (DPIO2_DMA) + pages * sizeof (DPIO2_DMA_PAGE));

  if (page_info == NULL) {
    PyErr_SetString (PyExc_MemoryError, "can't allocate DMA page info struct");
    return NULL;
  }

  page_info->hDma = NULL;
  page_info->pUserAddr = self->buffer;
  page_info->dwBytes = buffer_size;
  page_info->dwOptions = 0;
  page_info->dwPages = pages;

  status = dpio2DMALock (device, page_info);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "DMA lock failed");
    free (page_info);
    return NULL;
  }

  /* Dave's code suggests that it must be aligned to a 4 byte boundary;
     the example code from Boston Micromachines enforces an 8 byte boundary. */

  if ((page_info->Page[0].pPhysicalAddr & 0x7) != 0) {
    PyErr_SetString (PyExc_RuntimeError, "PCI address must be aligned to an 8 byte boundary");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }


  /* Locking complete. Move on to configuring the DMA chain. */

  for (page = 0; page < page_info->dwPages - 1; page++) {
    dma_descriptor.descriptorId        = page + initial_id;
    dma_descriptor.nextDescriptorId    = page + initial_id + 1;
    dma_descriptor.pciAddress          = page_info->Page[page].pPhysicalAddr;
    dma_descriptor.blockSizeInBytes    = page_info->Page[page].dwBytes;
    dma_descriptor.lastBlockInChain    = FALSE;
    dma_descriptor.useD64              = TRUE;
    dma_descriptor.notEndOfFrame       = FALSE;
    dma_descriptor.endOfBlockInterrupt = FALSE;

    status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_DESC, (long)&dma_descriptor);

    if (status == ERROR) {
      PyErr_SetString (PyExc_RuntimeError, "failed to set DMA descriptor");
      dpio2DMAUnlock (device, page_info);
      free (page_info);
      return NULL;
    }
  }

  dma_descriptor.descriptorId        = page + initial_id;
  dma_descriptor.nextDescriptorId    = 0;
  dma_descriptor.pciAddress          = page_info->Page[page].pPhysicalAddr;
  dma_descriptor.blockSizeInBytes    = page_info->Page[page].dwBytes;
  dma_descriptor.lastBlockInChain    = TRUE;
  dma_descriptor.useD64              = TRUE;
  dma_descriptor.notEndOfFrame       = FALSE;
  dma_descriptor.endOfBlockInterrupt = TRUE;


  status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_DESC, (long)&dma_descriptor);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to set DMA descriptor");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }


  /* Configuration complete. Lower the PIO2 strobe, and begin
     the DMA transfer. */

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock   (&self->mutex);
  self->dma_complete = FALSE;
  pthread_mutex_unlock (&self->mutex);
  Py_END_ALLOW_THREADS

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, FALSE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to lower PIO2 strobe");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  /* The descriptor number is a unique identifier for the initial
     DPIO2_DMA_DESC struct that was written earlier. For the purposes
     of this ioctl, it's essentially the head of a linked list. */

  status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_START_DESCRIPTOR, initial_id);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to set start descriptor");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  printf("<Output_write> self = %p\n",self);
  status = dpio2Ioctl (device, DPIO2_CMD_DMA_START, 0);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to start DMA transfer");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }


  /* Wait for the transfer to complete. Even one second should be far
     too generous of a timeout. */

  clock_gettime (CLOCK_REALTIME, &wake_time);
  wake_time.tv_sec += 1;

  Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);

  if (self->dma_complete == FALSE) {
    pthread_cond_timedwait (&self->condition, &self->mutex, &wake_time);
  }

  pthread_mutex_unlock (&self->mutex);
  Py_END_ALLOW_THREADS


  /* Transfer complete. Raise the PIO2 strobe, and clean up. */

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, TRUE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to raise PIO2 strobe");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  dpio2DMAUnlock (device, page_info);
  free (page_info);

  if (self->dma_complete == FALSE) {
    PyErr_SetString (PyExc_RuntimeError, "DMA transfer timed out");
    return NULL;
  }


  /* Track the completion of this request in our weighted average of
     processing times. */

  clock_gettime (CLOCK_MONOTONIC_RAW, &end_time);

  if (self->processing_time != 0) {
    /* Weighted average, one hundred samples. */
    self->processing_time = (self->processing_time * 99 + microtenths (end_time, start_time)) / 100;

  } else {
    self->processing_time = microtenths (end_time, start_time);
  }

  Py_RETURN_NONE;
}

/*********************************************************/
/* this is a change from the original dpio2.c */

/*
 c_output_write is the c-callable version of Output_write.
 It is intended to be called by c_recon, the wfs camera
 callback routine in rtc2.c
*/
static int said_it_once = 0;

void *
c_output_write(DPIO *self, double *a, long *map) {

  int device=-1, buffer_size=-1, page=0, pages=-1;
  long initial_id = 0;
  struct timespec wake_time;
  int elements, index, mindex;

  DPIO2_DMA *page_info = NULL;
  DPIO2_DMA_DESC dma_descriptor;
  STATUS status;

  /***DEBUG***/
  if (said_it_once == 0) printf("<c_output_write> successfully called with DPIO2 object %p\n",self);
  //return NULL;
  /***DEBUG***/
  
  /* Transfer data from the reconstructor into the DPIO2 buffer */
  /* It's already been checked for range conformance in c_recon */

  device = PyInt_AsLong (self->device);

  if (device == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return NULL;
  }

  buffer_size = PyInt_AsLong(self->buffer_size); //2048;

  if (buffer_size == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return NULL;
  }

  /* this substitutes for the call to parseValues */
  elements = buffer_size / 2;
  for (index = 0; index < elements; index++ ) {
    if (said_it_once == 0) {
      if (index == 0) {
        printf("<dpio2.c_ouput_write> map = %p: %d  %d\n",map,map[0],map[1]);
      }
    }
    mindex = (int)map[index];
    self->buffer[mindex] = (unsigned short)a[index];
  }

  /* Allocate and lock a buffer for the DMA request. */

  pages = 1 + (buffer_size + page_size - 1) / page_size;

  page_info = (DPIO2_DMA *)malloc (sizeof (DPIO2_DMA) + pages * sizeof (DPIO2_DMA_PAGE));

  if (page_info == NULL) {
    printf("<c_output_write> can't allocate DMA page info struct\n");
    return NULL;
  }

  page_info->hDma = NULL;
  page_info->pUserAddr = self->buffer;
  page_info->dwBytes = buffer_size;
  page_info->dwOptions = 0;
  page_info->dwPages = pages;

  status = dpio2DMALock (device, page_info);

  if (status == ERROR) {
    printf("<c_output_write> DMA lock failed");
    free (page_info);
    return NULL;
  }

  /* Dave's code suggests that it must be aligned to a 4 byte boundary;
     the example code from Boston Micromachines enforces an 8 byte boundary. */

  if ((page_info->Page[0].pPhysicalAddr & 0x7) != 0) {
    printf("<c_output_write> PCI address must be aligned to an 8 byte boundary\n");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  /* Locking complete. Move on to configuring the DMA chain. */

  for (page = 0; page < page_info->dwPages - 1; page++) {
    dma_descriptor.descriptorId        = page + initial_id;
    dma_descriptor.nextDescriptorId    = page + initial_id + 1;
    dma_descriptor.pciAddress          = page_info->Page[page].pPhysicalAddr;
    dma_descriptor.blockSizeInBytes    = page_info->Page[page].dwBytes;
    dma_descriptor.lastBlockInChain    = FALSE;
    dma_descriptor.useD64              = TRUE;
    dma_descriptor.notEndOfFrame       = FALSE;
    dma_descriptor.endOfBlockInterrupt = FALSE;

    status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_DESC, (long)&dma_descriptor);

    if (status == ERROR) {
      printf("<c_output_write> failed to set DMA descriptor\n");
      dpio2DMAUnlock (device, page_info);
      free (page_info);
      return NULL;
    }
  }

  dma_descriptor.descriptorId        = page + initial_id;
  dma_descriptor.nextDescriptorId    = 0;
  dma_descriptor.pciAddress          = page_info->Page[page].pPhysicalAddr;
  dma_descriptor.blockSizeInBytes    = page_info->Page[page].dwBytes;
  dma_descriptor.lastBlockInChain    = TRUE;
  dma_descriptor.useD64              = TRUE;
  dma_descriptor.notEndOfFrame       = FALSE;
  dma_descriptor.endOfBlockInterrupt = TRUE;

  status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_DESC, (long)&dma_descriptor);

  if (status == ERROR) {
    printf("<c_output_write> failed to set DMA descriptor\n");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  /* Configuration complete. Lower the PIO2 strobe, and begin
     the DMA transfer. */

  //Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock   (&self->mutex);
  self->dma_complete = FALSE;
  pthread_mutex_unlock (&self->mutex);
  //Py_END_ALLOW_THREADS

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, FALSE);

  if (status == ERROR) {
    PyErr_SetString (PyExc_RuntimeError, "failed to lower PIO2 strobe");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  /* The descriptor number is a unique identifier for the initial
     DPIO2_DMA_DESC struct that was written earlier. For the purposes
     of this ioctl, it's essentially the head of a linked list. */

  status = dpio2Ioctl (device, DPIO2_CMD_DMA_SET_START_DESCRIPTOR, initial_id);

  if (status == ERROR) {
    printf("<c_output_write> failed to set start descriptor\n");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  status = dpio2Ioctl (device, DPIO2_CMD_DMA_START, 0);

  if (status == ERROR) {
    printf("<c_output_write> failed to start DMA transfer\n");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  /* Wait for the transfer to complete. Even one second should be far
     too generous of a timeout. */

  clock_gettime (CLOCK_REALTIME, &wake_time);
  wake_time.tv_sec += 1;

  //Py_BEGIN_ALLOW_THREADS
  pthread_mutex_lock (&self->mutex);

  if (self->dma_complete == FALSE) {
    pthread_cond_timedwait (&self->condition, &self->mutex, &wake_time);
  }

  pthread_mutex_unlock (&self->mutex);
  //Py_END_ALLOW_THREADS

  /* Transfer complete. Raise the PIO2 strobe, and clean up. */

  status = dpio2Ioctl (device, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, TRUE);

  if (status == ERROR) {
    printf("<c_output_write> failed to raise PIO2 strobe\n");
    dpio2DMAUnlock (device, page_info);
    free (page_info);
    return NULL;
  }

  dpio2DMAUnlock (device, page_info);
  free (page_info);

  if (self->dma_complete == FALSE) {
    printf("<c_output_write> DMA transfer timed out\n");
    return NULL;
  }

  said_it_once = 1; /***DEBUG***/
  return NULL;
}

/*********************************************************/

/* Internal functions. */


static void
dlog (DPIO *self, int priority, const char *format, ...) {

  va_list arguments;
  char buffer[1024];

  if (self == NULL || self->log_ident == NULL) {
    return;
  }

  va_start (arguments, format);


  vsnprintf (buffer, sizeof (buffer), format, arguments);
  syslog (priority, buffer);


  va_end (arguments);
}



static void
dmaCallback (DWORD argument) {

  DPIO *instance = (DPIO *)argument;

  if (instance == NULL) {
    return;
  }

  /* There is no need to invoke Py_BEGIN_ALLOW_THREADS here, since this
     callback is not being invoked via a Python context. */

  pthread_mutex_lock   (&instance->mutex);
  instance->dma_complete = TRUE;
  pthread_cond_signal  (&instance->condition);
  pthread_mutex_unlock (&instance->mutex);

  return;
}



static long
microtenths (const struct timespec minuend, const struct timespec subtrahend) {

  long result;

  result  = (minuend.tv_sec  - subtrahend.tv_sec)  * 10000000;
  result += (minuend.tv_nsec - subtrahend.tv_nsec) / 100;

  return result;
}



static STATUS
parseValues (DPIO *self, PyObject *values) {

  Py_ssize_t quantity = -1;
  long buffer_size=-1, elements=0, number, index;

  PyObject *element;


  buffer_size = PyInt_AsLong (self->buffer_size);

  if (buffer_size == -1 && PyErr_Occurred () != NULL) {
    /* An exception is waiting. Let the caller handle it. */
    return ERROR;
  }

  /* Each 'element' in the DMA buffer is an unsigned short. Thus, there
     are a total of buffer_size / 2 elements in the configured output. */

  elements = buffer_size / 2;

  quantity = PySequence_Check (values);

  if (quantity == -1) {
    PyErr_SetString (PyExc_TypeError, "values must be a sequence object");
    return ERROR;
  }

  quantity = PySequence_Length (values);

  if (quantity == -1) {
    PyErr_SetString (PyExc_TypeError, "values must be a sequence object");
    return ERROR;
  }

  if (elements != quantity) {
    PyErr_Format (PyExc_ValueError, "expected %ld values, found %ld instead", elements, (long)quantity);
    return ERROR;
  }


  for (index = 0; index < quantity; index++ ) {
    element = PySequence_GetItem (values, index);

    if (element == NULL) {
      /* An exception is waiting. Let the caller handle it. */
      return ERROR;
    }

    /* PySequence_GetItem returns a new reference. */

    number = PyInt_AsLong (element);
    Py_DECREF (element);

    if (number == -1 && PyErr_Occurred () != NULL) {
      /* An exception is waiting. Let the caller handle it. */
      return ERROR;
    }


    /* We finally have a number to inspect. The values in the buffer
       are unsigned shorts, with a range of 0 -> 65535. */

    if (number < 0 || number > 65535) {
      PyErr_Format (PyExc_ValueError, "value %ld is not in range 0 -> 65535", number);
      return ERROR;
    }


    /* Assign this new value to the current position in the buffer. */

    self->buffer[index] = (unsigned short)number;
  }

  return OK;
}






/* Tables of all the Python-accessible functions and attributes in this file,
   mapping Python names to the appropriate C function, and associating
   docstrings as necessary. See the Python documentation for further details:

   http://docs.python.org/2/c-api/structures.html#PyMethodDef
   http://docs.python.org/2/c-api/structures.html#PyMemberDef

   These boilerplate declarations are at the end of the file so that all
   the docstrings can be defined adjacent to the functions themselves,
   and included here. */

static PyMemberDef DPIO_members[] = {
  {"buffer_size", T_OBJECT_EX, offsetof (DPIO, buffer_size), READONLY, "DMA buffer size"},
  {"device",      T_OBJECT_EX, offsetof (DPIO, device),      READONLY, "DPIO2 device number"},
  {"frequency",   T_OBJECT_EX, offsetof (DPIO, frequency),   READONLY, "Strobe frequency"},
  {NULL}  /* Sentinel */
};

static PyMethodDef DPIO_methods[] = {
  {"current", (PyCFunction)DPIO_current, METH_NOARGS,   DPIO_current_doc},
  {"logging", (PyCFunction)DPIO_logging, METH_KEYWORDS, DPIO_logging_doc},
  {"stdout",  (PyCFunction)DPIO_print,   METH_NOARGS,   DPIO_print_doc},
  {"timing",  (PyCFunction)DPIO_timing,  METH_NOARGS,   DPIO_timing_doc},
  {NULL}  /* Sentinel */
};

static PyMethodDef Output_methods[] = {
  {"write", (PyCFunction)Output_write, METH_KEYWORDS, Output_write_doc},
  {"peek",  (PyCFunction)Output_peek,  METH_VARARGS,  Output_peek_doc},
  {"set_callback", (PyCFunction)Output_set_callback, METH_VARARGS, Output_set_callback_doc},
  {NULL}  /* Sentinel */
};

static PyMethodDef DPIO2_module_methods[] = {
  {NULL}  /* Sentinel */
};

static PyTypeObject DPIOType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "dpio2.DPIO",              /*tp_name*/
  sizeof (DPIO),             /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)DPIO_del,      /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  DPIO_doc,                  /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  DPIO_methods,              /* tp_methods */
  DPIO_members,              /* tp_members */
  0,                         /* tp_getset */
  0,                         /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)DPIO_init,       /* tp_init */
  0,                         /* tp_alloc */
  DPIO_new,                  /* tp_new */
};

static PyTypeObject OutputType = {
  PyObject_HEAD_INIT(NULL)
  0,                         /*ob_size*/
  "dpio2.Output",            /*tp_name*/
  sizeof (DPIO),             /*tp_basicsize*/
  0,                         /*tp_itemsize*/
  (destructor)DPIO_del,      /*tp_dealloc*/
  0,                         /*tp_print*/
  0,                         /*tp_getattr*/
  0,                         /*tp_setattr*/
  0,                         /*tp_compare*/
  0,                         /*tp_repr*/
  0,                         /*tp_as_number*/
  0,                         /*tp_as_sequence*/
  0,                         /*tp_as_mapping*/
  0,                         /*tp_hash */
  0,                         /*tp_call*/
  0,                         /*tp_str*/
  0,                         /*tp_getattro*/
  0,                         /*tp_setattro*/
  0,                         /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE, /*tp_flags*/
  Output_doc,                /* tp_doc */
  0,                         /* tp_traverse */
  0,                         /* tp_clear */
  0,                         /* tp_richcompare */
  0,                         /* tp_weaklistoffset */
  0,                         /* tp_iter */
  0,                         /* tp_iternext */
  Output_methods,            /* tp_methods */
  DPIO_members,              /* tp_members */
  0,                         /* tp_getset */
  &DPIOType,                 /* tp_base */
  0,                         /* tp_dict */
  0,                         /* tp_descr_get */
  0,                         /* tp_descr_set */
  0,                         /* tp_dictoffset */
  (initproc)Output_init,     /* tp_init */
  0,                         /* tp_alloc */
  DPIO_new,                  /* tp_new */
};

/* And last but not least, the function that Python will use to initialize
   the module, according to the above table. The function name has a
   mandatory structure: init<module_name>(), in our case, initdpio2().*/


PyMODINIT_FUNC
initdpio2 (void) {

  PyObject* module=NULL;

  DPIO2_INFO_T info;


  if (PyType_Ready (&DPIOType) < 0) {
    return;
  }

  if (PyType_Ready (&OutputType) < 0) {
    return;
  }

  module = Py_InitModule3 ("dpio2", DPIO2_module_methods, DPIO2_doc);
  import_array(); /* this is a change from the original dpio2.c file */
  
  if (module == NULL) {
    return;
  }

  Py_INCREF (&DPIOType);
  Py_INCREF (&OutputType);

  PyModule_AddObject (module, "DPIO", (PyObject *)&DPIOType);
  PyModule_AddObject (module, "Output", (PyObject *)&OutputType);


  /* Scan for available DPIO2 devices. This step is required before
     any attempts to invoke dpio2Open(). */

  device_count = dpio2Scan (&info);

  /* Determine the size of a memory page for future reference. */

  page_size = sysconf (_SC_PAGESIZE);
}


/* Hitting tab gets you two spaces. Actual tabs render as eight characters,
 * the way that any (sane) editor expects them to.
 *
 * vim: set expandtab tabstop=8 softtabstop=2 shiftwidth=2 autoindent: 
 */

/* EOF */
