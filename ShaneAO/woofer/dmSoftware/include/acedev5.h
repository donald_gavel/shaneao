#ifndef acedev5_H
#define acedev5_H
/*******************************************************************************
 * ALPAO project
 *
 * "@(#) $Id: acedev5.h,v 1.1 2013/03/22 18:44:56 jgates Exp $"
 *
 * History
 * -------
 * $Log: acedev5.h,v $
 * Revision 1.1  2013/03/22 18:44:56  jgates
 * Software from musher needed to run woofer deformable mirror.
 *
 * Revision 1.14.2.1.2.9  2011/05/05 11:56:31  cetres
 * Upgrade Drivers tio be compliant with v5Dr3 of DEV5 firmware
 *
 * Revision 1.14.2.1.2.8  2011/02/15 08:21:33  cetres
 * acedev5 dirvers is now fully compliant with PCI7300.
 * Fixed bug with DMA buffer for PCI7300.
 * Simplified the write data call
 * DLL now in version 2.0.1.10
 *
 * Revision 1.14.2.1.2.7  2011/02/07 08:58:29  cetres
 * Added async pattern and single actuator for PCI7200 and PCI7300
 *
 * Revision 1.14.2.1.2.6  2010/07/29 08:10:56  cetres
 * Fixed bug in DLL multiDM drive
 * Added Pattern generation funcitonnality
 *
 * Revision 1.14.2.1.2.5  2010/06/07 14:47:26  cetres
 * Major update. Added possibility to drive several mirror
 *
 * Revision 1.14.2.1.2.4  2010/05/20 17:39:01  cetres
 * 2 files of configuration
 *
 * Revision 1.14.2.1.2.3  2010/05/18 07:27:35  cetres
 * Added High level interface to retreive number of actuator
 *
 * Revision 1.14.2.1.2.2  2010/05/11 08:10:11  cetres
 * Fix Bug with PCI7300 and enable/disable trigger
 *
 * Revision 1.14.2.1.2.1  2010/04/21 08:55:25  cetres
 * Added Trigger enable disable functionnalities
 *
 * Revision 1.14.2.1  2009/12/17 13:38:35  cetres
 * Major change!
 * Removed All dedicated interface.
 * Every connection to a dev5 uses a configuration file
 *
 * Revision 1.11  2009/11/02 08:21:40  cetres
 * Added dm109 interface
 *
 * Revision 1.10  2009/09/04 08:58:09  cetres
 * Specific code for dm52, dm88, dm97 are now separated from acedev5.c
 * It helps to provides linux code
 *
 * Revision 1.9  2009/08/28 13:45:05  cetres
 * Added possibility to use sync and async mode
 * new dll 1.0.1.0
 *
 * Revision 1.8  2009/08/25 11:50:07  cetres
 * Added dm88 functionnalities
 *
 * Revision 1.7  2009/08/24 06:46:51  cetres
 * Added acedev5ReleaseWithoutReset function
 * DM176 (2x88) is now supported
 *
 * Revision 1.6  2009/05/06 09:16:13  cetres
 * Added Step function declaration in header file
 *
 * Revision 1.5  2009/04/06 07:25:18  cetres
 * Added ledbox 52
 *
 * Revision 1.4  2009/04/01 07:55:01  cetres
 * Added dm52 facilities
 *
 * Revision 1.3  2009/03/12 12:16:53  cetres
 * Move Define part in source file
 * Added High level functionnalities to send and load 97 and 69 intentities ineasily
 *
 * Revision 1.2  2009/02/05 09:53:05  cetres
 * Changed max and min value per actuator +or- 1A to +or- 0.3 A
 *
 * Revision 1.1  2008/09/18 13:06:56  cetres
 * Creation
 *
 *
 *
 ******************************************************************************/

/**
 * @file
 * acedev5 general header file.
 */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#include "windows.h"
#endif


#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT
#endif

/* New interface */
/* Init Release function */
DLL_EXPORT int acedev5Init(int nDm, int *dmIds, char * serialName1, ...);
DLL_EXPORT int acedev5Release(int nDm, int *dmId);
DLL_EXPORT int acedev5GetNbActuator(int nDm, int *dmId, int *nbActuator);
DLL_EXPORT int acedev5GetOffset(int nDm, int *dmId, double *offset);
/* Send Intensity */
DLL_EXPORT int acedev5Send(int nDm, int *dmId, double *value);
DLL_EXPORT int acedev5SendSingleActuator(int nDm, int *dmId, double *value, int actuatorId);
DLL_EXPORT int acedev5SoftwareDACReset(int nDm, int *dmId);
/* Trigger */
DLL_EXPORT int acedev5DisableTrig(int nDm, int *dmId);
DLL_EXPORT int acedev5EnableTrig(int nDm, int *dmId);
DLL_EXPORT int acedev5EnableTrigStrobo(int nDm, int *dmId);
/* Send Pattern */
DLL_EXPORT int acedev5StartPattern(int nDm, int *dmId, double *pattern, int nPattern, int nRepeat);
/*DLL_EXPORT int acedev5StartPatternSingleActuator(int nDm, int *dmId, double *pattern, int nPattern, int nRepeat);*/
DLL_EXPORT int acedev5StopPattern(int nDm, int *dmId);
DLL_EXPORT int acedev5QueryPattern(int nDm, int *dmId, int *status);

#ifdef __cplusplus
}
#endif

#endif /*!acedev5_H*/

/*___oOo___*/
