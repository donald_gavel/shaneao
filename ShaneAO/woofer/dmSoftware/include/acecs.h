#ifndef acecs_H
#define acecs_H
/*******************************************************************************
 * ALPAO project
 *
 * "@(#) $Id: acecs.h,v 1.1 2013/03/22 18:44:56 jgates Exp $"
 *
 * History
 * -------
 * $Log: acecs.h,v $
 * Revision 1.1  2013/03/22 18:44:56  jgates
 * Software from musher needed to run woofer deformable mirror.
 *
 * Revision 1.3  2009/10/26 09:47:42  cetres
 * Added ifdef for mexfile compilation
 * Changed of code -> DLL version is nox 1.0.0.1
 *
 * Revision 1.2  2009/08/31 11:03:51  cetres
 * Remove dead code
 *
 * Revision 1.1  2008/09/18 13:24:43  cetres
 * Creation
 *
 *
 ******************************************************************************/

/**
 * @file
 * Public definitions/declarations of Alpao Core Engine Common Software (acecs)
 */

/*
 * The following piece of code alternates the linkage type to C for all
 * functions declared within the braces, which is necessary to use the
 * functions in C++-code.
 */
#ifdef __cplusplus
extern "C" {
#endif

#ifdef WIN32
#include "windows.h"
#endif


#ifdef BUILD_DLL
    #define DLL_EXPORT __declspec(dllexport)
#else
    #define DLL_EXPORT
#endif

/*
 * System Headers
 */
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#ifndef WIN32
#include <termios.h>
#endif

/**
 * Macro to get file name and line number
 */
#ifndef DOXYGEN /* Code fragment ignored by Doxygen */
#ifndef __FILE_LINE__
#define acecsIToStr(a) #a
#define acecsIToStr2(a) acecsIToStr(a)
#define __FILE_LINE__ __FILE__ ":" acecsIToStr2(__LINE__)
#endif /*!__FILE_LINE__*/
#endif /*!DOXYGEN*/

/**
 * Macro to get min and max value
 */
#define acecsMax(a,b)  ((a)>=(b)?(a):(b))
#define acecsMin(a,b)  ((a)<=(b)?(a):(b))

/**
 * Completion status returned by subroutines
 */
typedef enum
{
    acecsFAILURE = -1,     /** Completion failure. */
    acecsSUCCESS           /** Successful completion */
} acecsCOMPL_STAT;

/**
 * Boolean
 */
typedef enum
{
    acecsFALSE = 0, /**< False */
    acecsTRUE       /**< True */
} acecsBOOLEAN;

/**
 * Logging level
 */
typedef enum
{
    acecsLOG_ERROR = -1, /**< Error messages */
    acecsLOG_QUIET,      /**< No echo */
    acecsLOG_WARNING,    /**< Abnormal events for application */
    acecsLOG_INFO,       /**< Major events (e.g when command is received).*/
    acecsLOG_TEST,       /**< Software test activities */
    acecsLOG_TRACE       /**< Use to trace subroutine calls */
} acecsLOG_LEVEL;

/**  Maximum size of the error message */
#define acecsERR_MSG_MAX_LEN 1024

/** Error message definition */
typedef char acecsERR_MSG[acecsERR_MSG_MAX_LEN];



/* Time stamp */
DLL_EXPORT void acecsGetTimeStamp(char timeStamp[32], unsigned int precision);

/**
 * Configuration parameters for logging service
 */
typedef struct
{
    acecsBOOLEAN   enabled;       /**< Flag indicating whether log is enabled
                                     or not */
    acecsLOG_LEVEL level;         /**< Lowest level of logged message */
    acecsBOOLEAN   printDate;     /**< Flag indicating whether date is
                                     printed out or not */
    acecsBOOLEAN   printFileLine; /**< Flag indicating whether file/line
                                     information is printed out or not */
} acecsLOG_CONFIG;

/* Log handling */
DLL_EXPORT void acecsLogSet(acecsBOOLEAN enabled, acecsLOG_LEVEL level,
                         acecsBOOLEAN printDetails);
DLL_EXPORT acecsBOOLEAN acecsLogIsEnabled(void);
DLL_EXPORT acecsLOG_LEVEL acecsLogGetLevel(void);
DLL_EXPORT acecsBOOLEAN acecsLogPrintDetails(void);
DLL_EXPORT void acecsLogPrint(acecsLOG_LEVEL level, const char *fileLine,
                        const char *format, ...);

/**
 * The following convenient macros are provided for logging information. They
 * automaticaly add log level and file/line information parameters.
 *
 * Log errors. The logging level is set to acecsLOG_ERROR.
 *
 * @sa acecsLogPrint
 */
#define acecsLogError(format, arg...) \
        acecsLogPrint(acecsLOG_ERROR, __FILE_LINE__, format, ##arg)
/**
 * Log information about abnormal events for application. The logging level is
 * set to acecsLOG_WARNING.
 *
 * @sa acecsLogPrint
 */
#define acecsLogWarning(format, arg...) \
        acecsLogPrint(acecsLOG_WARNING, __FILE_LINE__, format, ##arg)
/**
 * Log information about major events. For example, when command is received.
 * The logging level is fixed to acecsLOG_INFO.
 *
 * @sa acecsLogPrint
 */
#define acecsLogInfo(format, arg...) \
        acecsLogPrint(acecsLOG_INFO, __FILE_LINE__, format, ##arg)
/**
 * Log relevant information used for the software test activities. The
 * logging level is fixed to acecsLOG_TEST.
 *
 * @sa acecsLogPrint
 */
#define acecsLogTest(format, arg...) \
        acecsLogPrint(acecsLOG_TEST, __FILE_LINE__, format, ##arg)
/**
 * Log subroutine calls. The logging level is fixed to acecsLOG_TRACE.
 *
 * @sa acecsLogPrint
 */
#define acecsLogTrace(format, arg...) \
        acecsLogPrint(acecsLOG_TRACE, __FILE_LINE__, format, ##arg)

/* Error handling */


/** Error description */
typedef struct
{
    int      sequenceNumber;    /**< Number of the sequence in the stack  */
    char     timeStamp[32];     /**< The date when the error occured      */
    char     location[128];     /**< The location where the error occured */
                                /**< File, line, etc...                   */
    int      errorId;           /**< The error identifier                 */
    char     runTimePar[1024];  /**< Detailed information about the error */
} acecsERROR;

/* For the time being, the error stack size is set to 1. If an error occurs,
 * it is placed in this stack replacing previous one (if any). We will see
 * later if error history is needed.
 */
/** Maximum number of error in error stack */
#define acecsERR_STACK_SIZE 1
/** Error stack */
typedef struct
{
    int         size;                     /**< Number of error in stack */
    acecsERROR    stack[acecsERR_STACK_SIZE]; /**< Error stack              */
} acecsERROR_STACK;


/**
 * Add an error message into the error stack, by calling the
 * acecsErrAddInStack function. This macro automatically add file/line
 * informations.
 */
#define acecsErrAdd(errorId, format, arg...) \
    acecsErrAddInStack(__FILE_LINE__, errorId, format, ##arg)

DLL_EXPORT void acecsErrAddInStack  (const char    *fileLine,
                               int           errorId,
                               const char    *format, ... );
DLL_EXPORT void acecsErrReset(void);
DLL_EXPORT void acecsErrDisplay(void);
DLL_EXPORT acecsCOMPL_STAT acecsErrGetStatus(int *errorId, acecsERR_MSG errMsg);

#ifdef __cplusplus
}
#endif

#endif /*!acecs_H*/

/*___oOo___*/
