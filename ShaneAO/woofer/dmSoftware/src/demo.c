/**
 * @file
 * acedev5 dm Example program file.
 */

#include <stdio.h>
#include <stdlib.h>
#include "math.h"

#include "acedev5.h"
#include "acecs.h"

/*---------------------------------------------------------------------------
 *  main
 *---------------------------------------------------------------------------*/

int main(int argc, char *argv[])
{
    /* Set log level to quiet */
    acecsLogSet(acecsTRUE, acecsLOG_QUIET, acecsFALSE);

    /*
     * 1. Initialisation of the mirror
     */
    int nDm = 1;
    int dmIds[nDm];
    char serial[128];
    int i;
    printf("Enter the serial of your mirror\n");
    scanf("%s", serial);
    if (acedev5Init(nDm, dmIds, serial) == acecsFAILURE)
    {
        acecsErrDisplay();
        return -1;
    }
    printf("The mirror is now connected, the program will wait for 5 seconds...\n");
    /* Wait for 5 seconds to view the flatness of the mirror */
    sleep(5000);

    /* Retreive number of actuators */
    int nAct[1];
    acedev5GetNbActuator(nDm, dmIds, &nAct[0]);
    printf("Number of Actuators = %d\n", nAct[0]);

    /* Retreive offset */
    double offset[nAct[0]];
    acedev5GetOffset(nDm, dmIds, offset);
    printf("offset = ");
    for (i=0; i<nAct[0]; i++)
    {
        printf("%.4f ", offset[i]);
    }
    printf("\n");

    /* Enable DEV5 trigger signal */
    acedev5EnableTrig(nDm, dmIds);
    /* To disable it use acedev5DisableTrig(nDm, dmIds); */

    /*
     * 2. We will send on all actuator 0.12 for one second
     */
    /* Create vector where nAct cmdVector will be stored */
    double cmdVector[nAct[0]];
    /* Fill of zeros this cmdVector vector */
    for (i=0; i<nAct[0]; i++)
    {
        cmdVector[i] = 0;
    }

    /* Once the vector is build, change value one by one with 0.12
       0.12 is the value we want to apply */
    for (i=0; i<nAct[0]; i++)
    {
        printf("Send 0.12 on actuator %d\n", i+1);
        /* fill one value with 0.12 */
        cmdVector[i] = 0.12;

        if (acedev5Send(nDm, dmIds, cmdVector) == acecsFAILURE)
        {
            /* If send failed, view error message */
            acecsErrDisplay();
            /* Release the card */
            acedev5Release(nDm, dmIds);
            return -1;
        }
        /* Wait for 1000 ms (1 sec) to view the result on a wave front sensor for exemple */
        sleep(1000);
        /* replace the 0.05 value by zeros for next step. */
        cmdVector[i] = 0;
    }

    printf("\nReset Electronic. All actuators are set to 0 A\n");
    /*
     * 3. Send a software DAC reset to restore 0A on all actuators.
     */
    /* This function could be used as rescue method in case of non close loop for example. */
    if (acedev5SoftwareDACReset(nDm, dmIds) == acecsFAILURE)
	{
        acecsErrDisplay();
        if (acedev5Release(nDm, dmIds) == acecsFAILURE)
        {
            acecsErrDisplay();
            return -1;
        }
        return -1;
	}

    /*
     * 4. Release the mirror at the end of the program
     */
    printf("\nRelease the mirror card\n");
    if (acedev5Release(nDm, dmIds) == acecsFAILURE)
	{
        acecsErrDisplay();
        printf("\t Failed\n");
        return -1;
	}
    printf("\t Succeed\n");

    return 0;

}

