import SocketServer
import datetime
import os
import sys
import threading
import atexit
import signal

sys.path.append ('/opt/kroot/lib')
sys.path.append ('/opt/kroot/lib/python')

print sys.path

import DFW

from PyAcedev5 import *


print 'going'

ARRAY_LENGTH = 52

# KTL globals
SERVICE = 'wooferao'
DISPATCHER = 'DISP'



def total_seconds(days, seconds, microsecs):
    '''Return total seconds from day, seconds, and microseconds
    '''
    secs = float (days * 24 * 3600) + seconds
    tmicro = microsecs + (secs *10**6)
    tsec = tmicro/(10**6)
    return tsec


class AverageList():
    def __init__(self, length=1000):
        self.lst = []
        self.total = 0.0
        self.average = 0.0
        if length < 1 :
            length = 1
        self.length = length
        self.minimum = self.average
        self.maximum = self.average
        self.decay   = 0.0001

    def add(self, value):
        self.total += value
        self.lst.append(value)
        self.trim()
        self.setMinMax(value)

    def trim(self):
        while len(self.lst) > self.length :
            v = self.lst.pop(0)
            self.total -= v
        self.average = self.total / float (len(self.lst))

    def setMinMax(self, value):
        if len(self.lst) == 1:
            self.minimum = value
            self.maximum = value
        else:    
            self.minimum += self.decay
            self.maximum -= self.decay
        if self.minimum > self.average:
            self.minimum = self.average
        if self.maximum < self.average :        
            self.maximum = self.average
        if value < self.minimum :
            self.minimum = value
        if value > self.maximum :
            self.maximum = value
        

class EchoRequestHandler(SocketServer.BaseRequestHandler):
    def __init__(self, *args, **kwds):
        self.sockBuff = ''
        self.msgStart = 0
        self.loop = 1
        SocketServer.BaseRequestHandler.__init__(self, *args, **kwds)

    
    def setup(self):
        print self.client_address, 'connected!'
        self.request.send('hi ' + str(self.client_address) + '\n')

    def handle(self):
        dm = self.server.wooferDm
        print "dm=", dm
        while self.loop == 1:
            #data = self.request.recv(1024)
            #self.request.send(data)
            #data,status = self.getMessage()
            #if data.strip() == 'bye':
            #    return
            self.readSock()

    def finish(self):
        print self.client_address, 'disconnected!'
        self.request.send('bye ' + str(self.client_address) + '\n')
        

    def readSock(self):
        ''' Read in all the messages found and return a tupple of complete messages.
            Incomplete messages are left in self.sockBuff to be completed in subsequent calls
        '''    
        dbuff = self.request.recv(1024)
        #print "&&& readSock", dbuff
        messages = []
        self.findMsg(dbuff,messages)
        for msg in messages:
            self.handleMsg(msg)


    def findMsg(self, dbuff, messages):
        ''' Read all of dbuff and put all complete messages found in messages.
            Incomplete messages are left in self.sockBuff
            All messages start with '{' and end with '}'
        '''    
        length = len(dbuff)
        pos = 0
        msgFound = 0
        #print "&&& looking for start"
        if (self.msgStart == 1):
            pass
        else:
            # throw away characters until we find a '{'
            while (self.msgStart == 0) and (pos < length):
                c = dbuff[pos]
                #print "&&& ", c, pos
                pos += 1
                if (c == '{'):
                    self.msgStart = 1;

        #print "&&& looking for end ", pos            
        if (self.msgStart == 1):
            # keep reading characters until we  find '}'
            msgEnd = 0
            while (msgEnd == 0) and (pos < length):
                c = dbuff[pos]
                #print "&&& ", c,  pos
                pos += 1
                if (c != '}'):
                    self.sockBuff += c
                else:
                    msgEnd = 1
                    self.msgStart = 0
                    outMsg = (self.sockBuff)
                    #print "&&& out=", outMsg 
                    messages.append(outMsg)
                    self.sockBuff = ''
                    #print "&&& messages=", messages

        #print "&&& pos", pos, " length", length
        if (pos < length) :
            remainingBuff = dbuff[pos:]
            #print "recursing", remainingBuff, messages 
            self.findMsg(remainingBuff, messages)
    
        return



    def handleMsg(self, data):
        #print "&&& handleMsg", data
        dm      = self.server.wooferDm
        ktl     = self.server.ktl
        timeAvg = self.server.timeAvg
        arrLen = ARRAY_LENGTH
        token = data.split(',')
        #print data
        #print token
        msgType = token[0]
        if (msgType == 't') :
            try:
                #timing test
                dtNow = datetime.datetime.utcnow()
                dtStr = token[1]
                dt = datetime.datetime.strptime(dtStr, "%Y/%m/%d %H:%M:%S.%f")
                diff = dtNow - dt
                total_sec = total_seconds(diff.days, diff.seconds, diff.microseconds)
                dtNow = datetime.datetime.utcnow()
                nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")                 
                retStr = '{t,' + nowStr + ',' + str(total_sec) + '}'
                print retStr
                self.request.sendall(retStr)
            except Exception as err:
                print 'ERROR timing test', str(err)
                return data, -1
        elif (msgType == 'm'):
            # set 52 mirror positions
            try:
                dtStr = token[1]
                dt = datetime.datetime.strptime(dtStr, "%Y/%m/%d %H:%M:%S.%f")
                dtStart = datetime.datetime.utcnow()
                mirPos = [0.0] * arrLen
                for j in xrange(0, arrLen):
                    pos = float(token[j+2])
                    if pos < 0.0:
                        pos = 0.0
                    elif pos > 0.12:
                        pos = 0.12
                    mirPos[j] = str(pos)
                    dm.values[j] = pos
                dm.Send()
                dtNow = datetime.datetime.utcnow()
                nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")       
                retStr = '{m,' + nowStr + ',}'
                self.request.sendall(retStr)
                for j in xrange(0, arrLen) :
                    aKey = ktl.actuatorkeys[j]
                    v = mirPos[j]
                    #print aKey, v, dm.values[j]
                    ktl[aKey].set(v)
                ktl['COUNT'].incrementValue()
                ktl['TIMESTAMP'].set(nowStr)
                roundTripTm = dtNow - dt
                mirSetTm    = dtNow - dtStart
                seconds = float(roundTripTm.seconds)
                seconds += float(roundTripTm.microseconds) / 1000000.0
                timeAvg.add(seconds)
                ktl['TRTAVG'].set(str(timeAvg.average))
                ktl['TRTMIN'].set(str(timeAvg.minimum))
                ktl['TRTMAX'].set(str(timeAvg.maximum))
                #print "rt=", roundTripTm, " mir=", mirSetTm, "sec=", seconds
            except Exception as err:
                dtNow = datetime.datetime.utcnow()
                nowStr = dtNow.strftime("%m/%d %H:%M:%S.%f")
                estr = nowStr + ' ' + str(err)
                print 'ERROR mirror', estr
                ktl['ERR'].set(estr)
                return data, -1   
        elif msgType == 'bye' :
            return 'bye', -1
        return 'err', -1 


#
# #
# Keyword definitions.
# #
#

def setupKeywords (service):

    # Administr

    DFW.Keyword.Enumerated ('DISPSTA', service, 'initializing')

    ErrorKeyword ('ERR', service, '0')
    ErrorKeyword ('MSG', service, '')
    StopKeyword ('DISPSTOP' , service)

    print "service=", service
    service.actuatorkeys = []

    # Keywords
    for j in xrange (0, ARRAY_LENGTH) :
        kname = 'ACTUPOS' + str(j) 
        DFW.Keyword.Double (kname, service, '0')
        service.actuatorkeys.append(kname)

    DFW.Keyword.Integer('HEART',   service, '0')
    KeywordCount('COUNT', service, '0')
    DFW.Keyword.Double ('TRTAVG', service, '0')
    DFW.Keyword.Double ('TRTMIN', service, '0')
    DFW.Keyword.Double ('TRTMAX', service, '0')
    
    DFW.Keyword.String ('TIMESTAMP',service, '0000/00/00 00:00:00.000')



class ErrorKeyword (DFW.Keyword.Internal):

    def set (self, value):

        ''' Do not re-broadcast values set to ErrorKeyword objects,
            their values are strictly for internal reference.
        '''

        if self.value != value:
            self.value = value



class StopKeyword (DFW.Keyword.Internal):

    def __init__ (self, *args, **kwargs):
        kwargs['initial'] = '0'
        DFW.Keyword.Internal.__init__ (self, *args, **kwargs)


    def write (self, value):
        if value == '1':
            main.shutdown.set ()



class KeywordCount (DFW.Keyword.Integer):

    def __init__ (self, *args, **kwargs):
        DFW.Keyword.Integer.__init__ (self, *args, **kwargs)


    def incrementValue(self):        
        value = int (self.value) + 1
        value = '%d' % (value)
        self.set (value)


    def write(self, value) :
        self.set('0')


                
def main(args) :
    #Setup the socket and deformable mirror.
    serial_num = 'alpaoLEDBOX64'
    #serial_num = 'alpaoLSDM52-25-023'
    server = SocketServer.ThreadingTCPServer(('localhost', 5000), EchoRequestHandler)
    dm = PyAcedev5( serial_num )
    nbAct = dm.GetNbActuator()
    dm.values = [0] * nbAct
    dm.Send()
    print 'dm zeroed'
    server.wooferDm = dm
    server.timeAvg = AverageList()

    #Setup the KTL connection
    stdiosvc_config = '/usr/local/lick/data/wooferao/stdiosvc.conf'
    ktl = DFW.Service (SERVICE, stdiosvc_config, setupKeywords)
    status_keyword = '%sSTA' % (DISPATCHER)

    server.ktl = ktl

    def shutdown (*ignored):

        # Under normal exit conditions, the main.shutdown event will
        # already be set. It would not be set if we are exiting
        # under anomalous conditions, such as an unhandled
        # exception. It's harmless to set it multiple times.

        main.shutdown.set ()

        ktl[status_keyword].set ('shutting down')

        ktl.shutdown ()

    atexit.register (shutdown)
    signal.signal (signal.SIGINT,  shutdown)
    signal.signal (signal.SIGTERM, shutdown)

    ktl[status_keyword].set ('ready')


    # This will start the socket waiting for connections to accept.
    server.serve_forever()
    print 'fin!'


    def generateKeywordValues ():

        keywords = ('SEQUENCE_BOOLEAN',
                    'SEQUENCE_DOUBLE',
                    'SEQUENCE_DOUBLE_ARRAY',
                    'SEQUENCE_ENUMERATED',
                    'SEQUENCE_FLOAT',
                    'SEQUENCE_FLOAT_ARRAY',
                    'SEQUENCE_INTEGER',
                    'SEQUENCE_INTEGER_ARRAY',
                    'SEQUENCE_MASK',
                    'SEQUENCE_STRING',
                    'STUTTER')

        for keyword in keywords:
            try:
                service[keyword].nextInSequence ()
            except KeyError:
                pass



    while True:
        try:
            main.shutdown.wait (0.5)
        except KeyboardInterrupt, SystemExit:
            break

        if main.shutdown.isSet ():
            break

        # Queue the generation of keyword values.

        generateKeywordValues ()



main.shutdown = threading.Event ()


if __name__=="__main__":
    main(sys.argv)
