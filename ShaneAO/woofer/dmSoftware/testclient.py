import socket
import sys
import datetime

HOST, PORT = "localhost", 5000

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

arrLen = 52
mirPos = [0] * arrLen
mirPosStr = [''] * arrLen
for j in xrange(0, arrLen):
    mirPos[j] = float(j+1) * 0.001
    mirPosStr[j] = str(mirPos[j])

mirPosZerStr = [''] * arrLen
for j in xrange(0, arrLen):
    mirPosZerStr[j] = '0.0'

print "mirPosStr", mirPosStr
    
try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.settimeout(5)
    received = sock.recv(1024)
    print received
    wloop = 1
    while wloop == 1:
        # Connect to server and send data
        data = raw_input()
        print "input=", data
        if (data == 't'):
            print 'timestamp'
            dtNow = datetime.datetime.utcnow()
            nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")
            data = '{t,' + nowStr + '}'
            print "data=", data
            sock.sendall(data)
        elif data == 'm0':
            print data
            dtNow = datetime.datetime.utcnow()
            nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")
            data = '{m,' + nowStr + ',' + ','.join(mirPosZerStr) + '}'
            print "data=", data
            sock.sendall(data)    
        elif data == 'm1':
            print data
            dtNow = datetime.datetime.utcnow()
            nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")
            data = '{m,' + nowStr + ',' + ','.join(mirPosStr) + '}'
            print "data=", data
            sock.sendall(data)
        elif data == 'm2':
            print data
            dtNow = datetime.datetime.utcnow()
            nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")
            data = '{m,' + nowStr + ',' + ','.join(mirPosStr) + '}'
            data += '   dhwquiska' 
            data2 = '{m,' + nowStr + ',' + ','.join(mirPosZerStr) + '}'
            data += data2
            print "data=", data
            sock.sendall(data)
        elif data == 'm3':
            print data
            dtNow = datetime.datetime.utcnow()
            nowStr = dtNow.strftime("%Y/%m/%d %H:%M:%S.%f")
            data = '{m,' + nowStr
            sock.sendall(data)
            data2 = ',' + ','.join(mirPosStr) + '}'
            print "data=", data + data2
            sock.sendall(data2)
        elif data == 'bye' :
            data = '{bye}'
            print "data=", data
            sock.sendall(data)
        else:
            sock.sendall(data + "\n")

        # Receive data from the server and shut down
        try :
            received = sock.recv(1024)
            print "Sent:     {}", data    
            print "Received: {}", received
        except socket.timeout:
            print "socket timed out"
        if received.strip() == 'bye':
            wloop = 0
        
finally:
    sock.close()

 
print "Done!"
