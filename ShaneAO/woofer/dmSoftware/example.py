#!/usr/bin/python
# -*- coding: utf8 -*-

import sys
import time
from PyAcedev5 import *

def main(args):
    serial = raw_input("Please enter the S/N within the following format: alpaoHSDMXX-XX-XX: ")

    print("Connect the mirror")
    dm = PyAcedev5( serial )
    
    print("Retreive number of actuators")
    nbAct = dm.GetNbActuator()
    
    print("Send 0 on each actuators")
    dm.values = [0] * nbAct
    dm.Send()
    
    print("Poke each actuator")
    for i in range( nbAct ):
        dm.values[i] = 0.12
        # Send values vector
        dm.Send()
        print("Send 0.12 on actuator "+str(i))
        time.sleep(0.5) # Wait for 0.5 second
        dm.values[i] = 0
    
    print("Send 0 on each actuators")
    dm.values = [0] * nbAct
    dm.Send()
    
    print("Get offset")
    offset = dm.GetOffset()
    print offset
    
    print("Reset")
    dm.DACReset()
    
    print("Exit")

if __name__=="__main__":
    main(sys.argv)