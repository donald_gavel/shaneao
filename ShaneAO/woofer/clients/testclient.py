import socket
import sys
import datetime
import time

HOST, PORT = "musher.ucolick.org", 5000

# Create a socket (SOCK_STREAM means a TCP socket)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

arrLen = 52
mirPos = [0] * arrLen
mirPosStr = [''] * arrLen
for j in xrange(0, arrLen):
    mirPos[j] = float(j+1) * 0.001
    mirPosStr[j] = str(mirPos[j])

mirPosZerStr = [''] * arrLen
for j in xrange(0, arrLen):
    mirPosZerStr[j] = '0.0'

print "mirPosStr", mirPosStr
    
seq = 1

try:
    # Connect to server and send data
    sock.connect((HOST, PORT))
    sock.settimeout(5)
    received = sock.recv(1024)
    print received
    wloop = 1
    while wloop == 1:
        seq += 1
        # Connect to server and send data
        data = raw_input()
        print "input=", data
        if (data == 't'):
            print 'timestamp'
            dtNow = str(time.time())
            print "dtnow=", dtNow
            data = '{t,' +str(seq)+','+ dtNow + '}\n'
            print "data=", data
            sock.sendall(data)
        elif data == 'm0':
            print data
            nowStr = str(time.time())
            data = '{m,'  +str(seq)+','+ nowStr + ',' + ','.join(mirPosZerStr) + '}\n'
            print "data=", data
            sock.sendall(data)    
        elif data == 'm1':
            print data
            nowStr = str(time.time())
            data = '{m,' +str(seq)+',' + nowStr + ',' + ','.join(mirPosStr) + '}\n'
            print "data=", data
            sock.sendall(data)
        elif data == 'm2':
            # There is junk in this to make sure the server doesn't choke.
            print data
            nowStr = str(time.time())
            data = '{m,' +str(seq)+',' + nowStr + ',' + ','.join(mirPosStr) + '}\n'
            data += '   dhwquiska' 
            seq += 1
            data2 = '{m,' +str(seq)+',' + nowStr + ',' + ','.join(mirPosZerStr) + '}\n'
            data += data2
            print "data=", data
            sock.sendall(data)
        elif data == 'm3':
            print data
            nowStr = str(time.time())
            data = '{m,'+str(seq)+',' + nowStr
            sock.sendall(data)
            data2 = ',' + ','.join(mirPosStr) + '}\n'
            print "data=", data + data2
            sock.sendall(data2)
        elif data == 'bye' :
            data = '{bye}'
            print "data=", data
            sock.sendall(data)
        else:
            sock.sendall(data + "\n")

        # Receive data from the server and shut down
        try :
            received = sock.recv(1024)
            print "Sent:     {}", data    
            print "Received: {}", received
        except socket.timeout:
            print "socket timed out"
        if received.strip() == 'bye':
            wloop = 0
        
finally:
    sock.close()

 
print "Done!"
