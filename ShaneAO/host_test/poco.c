/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		poco

AUTHOR:	DWP			DATE: 5/13/05

COMPONENT DESCRIPTION:
This component is a standalone program whose purpose is to connect to the POCO system via a
socket and attach to the lickao shared memory.  Then, tip/tilt offloading data is read from 
shared memory 10 times per second and sent to POCO.

COMPONENT UNITS:
1.	main()			poco mainline

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"


// Configuration Variables

static const int pocoPort = 5700;
static const char pocoName[] = "128.114.176.5";  // "shard.ucolick.org";  // "134.9.244.95";  // 134.9.244.95 is lgs9 at LLNL
//XXX static const long secDelay = 0;			   // 0 secs
//XXX static const long nsecDelay = 500000000L;  // 500 msecs; changed from 50 msecs
static const long secDelay = 2;				// 1 secs
static const long nsecDelay = 500000000L;	// 500 msecs

// Global Variables


// Local Variables

static boolean quit = false;
static int pocoSock = -1;  							  // poco socket
static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static int totalSize;								  // total size of shared memory

static FileDataShmType *fileDataShmPtr;  // pointer to file data shared memory
static CamDataType *camDataPtr;		  // pointer to camera shared memory data
static LTTCamDataType *camLTTPtr;			  // pointer to Laser Tip Tilt camera shared memory data
static CentDataType *centDataPtr;		  // pointer to centroid shared memory data
static ReconDataType *reconDataPtr;	  // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the poco mainline.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
	boolean connectedFlag = false;
	int charsToSend, charsSent;
	static unsigned int xOrY = 0;
	const int bufSize = 32;
	char outBuf[bufSize];
	struct timespec next;

	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;

	struct sockaddr_in sockAddr;

//	set up signal handlers
	signal(SIGPIPE, SIG_IGN);  // send() will return an error if pipe is broken
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

	printf("poco7: starting\n");  //XXX

//	open the shared memory region, looping if an error occurs (presumably because it's not available yet)
//	get the pointer to the shared memory region, looping if an error occurs
	while (!quit && ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0))
	{
		printf("poco7: waiting for mmap open\n");  //XXX
		sleep(1);
	}
	if (!quit)
		printf("poco7: mmap opened\n");  //XXX
	while (!quit && ((startPtr = mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("poco7: waiting for mmap\n");  //XXX
		sleep(1);
	}
	if (!quit)
		printf("poco7: mmap completed\n");  //XXX

//	set up the pointers to the global variables
	fileDataShmPtr = (FileDataShmType *)startPtr;
	camDataPtr = (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr = (CentDataType *)((char *)camDataPtr + camDataSize);
	reconDataPtr = (ReconDataType *)((char *)centDataPtr + centDataSize);
	camLTTPtr = (LTTCamDataType *)((char *)reconDataPtr + reconDataSize);

//	create the POCO socket, displaying a message if an error is encountered
	if (!quit)
	{
//		enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while (!quit && (nanosleep(&next, NULL) == 0))
		{
//			if we're not connected to the POCO server, try to connect
			if (!connectedFlag)
			{
				if (pocoSock < 0)
				{
					if ((pocoSock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
						printf("poco7 Error: unable to create POCO socket\n");  //XXX
					else
						printf("poco7: socket created\n");  //XXX
				}
				printf("poco7: not connected\n");  //XXX
				sockAddr.sin_family = AF_INET;
				sockAddr.sin_port = htons(pocoPort);
				inet_pton(AF_INET, pocoName, &sockAddr.sin_addr);
				if (connect(pocoSock, (struct sockaddr *)&sockAddr, sizeof(sockAddr)) == 0)
				{
					sprintf(outBuf, "INSTRUMENT AO\n");
					charsToSend = strlen(outBuf);
					charsSent = send(pocoSock, outBuf, charsToSend, 0);

					printf("poco7: connect successful\n");  //XXX
					connectedFlag = true;
				}
			}

//			if we're connected to the POCO server, send the offload values to POCO
			if (connectedFlag)
			{
				if (xOrY == 0)
				{
					sprintf(outBuf, "Y %f\n", -reconDataPtr->xPocoOffload * 0.25);  // changed from 'X' and changed sign
					charsToSend = strlen(outBuf);
					charsSent = send(pocoSock, outBuf, charsToSend, 0);
//					printf("poco7: Y %f  %d  %d\n", -reconDataPtr->xPocoOffload * 0.25, charsToSend, charsSent);  //XXX
				}
				else
				{
					sprintf(outBuf, "X %f\n", -reconDataPtr->yPocoOffload * 0.25);  // changed from 'Y' and changed sign
					charsToSend = strlen(outBuf);
					charsSent = send(pocoSock, outBuf, charsToSend, 0);
//					printf("poco7: X %f  %d  %d\n", -reconDataPtr->yPocoOffload * 0.25, charsToSend, charsSent);  //XXX
				}

				if (charsToSend != charsSent)
				{
					printf("poco7: disconnected\n");  //XXX
					if (pocoSock > -1)
					{
						printf("poco7: shutting down socket 1\n");  //XXX
				//		shut the socket down and close it
						shutdown(pocoSock, SHUT_RDWR);
					    close(pocoSock);
						pocoSock = -1;
					}
					connectedFlag = false;
				}

				xOrY ^= 0x1;
			}

//			set up for next sleep
			next.tv_sec = secDelay;
			next.tv_nsec = nsecDelay;
		}
	}

	if (pocoSock > -1)
	{
		printf("poco7: shutting down socket 2\n");  //XXX
//		shut the socket down and close it
		shutdown(pocoSock, SHUT_RDWR);
	    close(pocoSock);
	}

	if (mmfd > -1)
	{
		printf("poco7: munmapping\n");  //XXX
//		clean up the shared memory and return
		if (startPtr > 0)
			munmap(startPtr, totalSize);
	  	close(mmfd);
	}

	printf("poco7: exiting, bye...\n");  //XXX
    return 0;
}


static void termHandler(int sigNum)
{
	printf("poco7: in termHandler\n");  //XXX
	quit = true;
}

