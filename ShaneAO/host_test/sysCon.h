/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			sysCon.h

AUTHOR:	DWP			DATE: 1/6/03

FILE DESCRIPTION:
This is the header file for the system controller component.

******************************************************************************/

#ifndef _SYSCON_H
#define _SYSCON_H		1

// Enums

enum  // states when running 'long' commands (i.e., commands that can take several seconds)
{
	eCmdStateInit,			 // initialize state
	eCmdStateSum,			 // sum data state
	eCmdStateAvg,			 // average data (and store to disk) state
	eCmdStateWaitForClosed,  // wait for closed loop configuration (used only for CamLTT_BackgroundProcess()) 
	eCmdStateSumClosed,		 // sum data in closed loop config (used only for CamLTT_BackgroundProcess())
	eCmdStateAvgClosed 		 // average data (and store to disk) in closed loop config (used only for CamLTT_BackgroundProcess())
};


// Defines

#define HRT_CPU 1  // CPU reserved for Hard Real-Time (HRT)


// Global Variables

extern boolean OkayToStartCamData;  // flag indicating that it is okay to start camera data
extern boolean OkayToFinishInit;  // flag indicating that it is okay for threads to complete their initializations

extern rtl_sem_t CamSem;	// semaphore for camera thread
extern rtl_sem_t LTTSem;	// semaphore for LTT camera thread
extern rtl_sem_t WFSSem;	// semaphore for WFS camera thread
extern rtl_sem_t CentSem;	// semaphore for centroid thread
extern rtl_sem_t ReconSem;	// semaphore for recon thread

extern int DiagFIFOfd;  // fd for FIFO used to send messages to the diagnostic data handler
extern int CmdFIFOfd;   // fd for FIFO used to receive command messages from the host interface

extern int CamSerialfd;  // fd for camera serial port

extern FileDataShmType *FileDataShmPtr;  // pointer to file data shared memory
extern CamDataType *CamDataPtr;			 // pointer to camera shared memory data
extern LTTCamDataType *CamLTTPtr;		 // pointer to Laser Tip Tilt camera shared memory data
extern CentDataType *CentDataPtr;		 // pointer to centroid shared memory data
extern ReconDataType *ReconDataPtr;		 // pointer to recon shared memory data

extern boolean TiptTiltAPDInitDone, CamInitDone, CamLTTInitDone, CamWFSInitDone, CentInitDone, ReconInitDone;  // initalization flags for threads

extern struct timespec TimeCamDrvr1, TimeCamDrvr2, TimeCamDrvr3, TimeCamDrvr4;  //TTT
extern struct timespec TimeCam1, TimeCam2, TimeCent1, TimeCent2, TimeRecon1, TimeRecon2;  //TTT


// Global Function Prototypes

void SysConSendStrngNoParm( char *strngPtr );
void SysConSendStrngIntParm( char *strngPtr, int16 intParam );
void SysConSendStrngFltParm( char *strngPtr, float32 floatParam );
uint32 SysConTimeDiff( struct rtl_timespec startTime, struct rtl_timespec endTime );

#endif
