/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			constant.h

AUTHOR:	DWP			DATE: 12/26/02

FILE DESCRIPTION:
This file contains constants, enums, and typedefs for the LickAO control system.

******************************************************************************/

#ifndef _CONSTANT_H
#define _CONSTANT_H		1

// Conditional compilation flags (for testing)

//XXXLLNLTest #define LLNL_TEST
#undef LLNL_TEST

//XXX20 #define TEST_60X60
#undef TEST_60X60

#define TEST_SHIFT
//XXXshift #undef TEST_SHIFT

// Defines

#define false			0
#define FALSE			0
#define true			1
#define TRUE			1

#define TEXT_BUF_LNGTH 128  // length of text buffers used primarily to set up messages for SysConSendStrngNoParm()

#define TEMP_BUF_LNGTH 32        // length of temporary buffers
#define BIG_TEMP_BUF_LNGTH 1024  // length of bigger temporary buffers

#define OUT_BUF_LNGTH_1 256    // length of output buffers, 1
#define OUT_BUF_LNGTH_2 2024   // length of output buffers, 1
#define OUT_BUF_LNGTH_3 65536  // length of output buffers, 1

#define SPR_BUF_LNGTH 4096  // length of .spr file buffers


// Enums

// error codes
enum
{
	eErrNoErr,				//  0. no error
	eErrRange,				//  1. an out-of-range error occurred
	eErrSaturate,			//  2. a saturation occurred
	eErrSqrtNeg,			//  3. an attempt was made to take the square root of a negative number
	eErrRowsOrder,			//  4. sub-ap row being processed by centroid is out of order
	eErrLostSocket,			//	5. lost the socket used for host communications
	eErrRestart,			//  6. a restart command has been received from the host
	eErrShutdown,			//  7. a shutdown command has been received from the host
	eErrQuit,			    //  8. a quit command has been received from the host
	eErrGettingDate,		//  9. an occurred while attempting to get the date/time
	eErrCreatingDir,		// 10. error creating directory
	eErrIncompleteFilename,	// 11. some part of a filename for a file that is being opened is not complete
	eErrTooManyFiles,		// 12. an attempt has been made to open too many diag files on a particular day (over 100)
	eErrFileAlreadyOpen,	// 13. a diagnostic data file is already open
	eErrOpeningFile			// 14. a system error occurred while attempting to open a file
};


#endif

