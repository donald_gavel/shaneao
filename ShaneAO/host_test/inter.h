#ifndef INTERFACET_AO_H
#define INTERFACET_AO_H

#include <stdio.h>
#include <iostream>
#include <list>
#include <map>
#include <sys/poll.h>
#include "interfacet.h"
#include "acceptthread.h"
#include "ktlkeyword.h"
#include "simple_timer.h"
#include "ktlfile.h"
#include "aoktlkeys.h"

// Temporary error, used as filled in the .C files until an explicit error
// is created.
#define ERR_TEMPORARY_ERR -1  

//
// CInterPoco controls communication between poco and other Processes. 
// It is unusual in that it is based on CEventThread but it will not be running 
// in a separate thread. It will be part of the Central thread.
// It is based on CEventThread so that it can take advantage of
// the CEventThread virtual functions in message passing.
//
class CInterAO : public CInterface
{
public:
    CInterAO();
    ~CInterAO();

    int  InterInit();

    virtual void vControlInfoUpdate();
    
    int  userMsg(string msg);
    int  userErr(string msg);


    int  updateC_VER(string *pstr);
    int  updateC_TELEM(int val);
    int  updateC_TRATE(int val); 
    int  updateC_DEBUG_C_SDEBUG(int val);
    int  updateC_GSMODE(int val);

    enum {
	BLAH_COMMAND = 10000,
	LAST_ENUM = 19999
    };

    virtual void vGetMsgRange(int *first, int *last){
	*first=INTR_TXT_COMMAND;*last=LAST_ENUM;};
    bool inMsgRange(int msg){
	return ((INTR_TXT_COMMAND <= msg) && (msg <= LAST_ENUM));};





protected:    
    // KTL action functions - actions to take when the keyword
    // value changes.
    // All must match type KTLChangeActionFunc
    static int afReadOnly       (CQData*, CEventThread*, ConnectionID);
    static int afAO             (CQData*, CEventThread*, ConnectionID);
    static int afAOTmp          (CQData*, CEventThread*, ConnectionID);
    static int afFile           (CQData*, CEventThread*, ConnectionID);
    
    // KTL delayed response functions
    static int drC_VER(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drInt(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drFile(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drAOTmp(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);

    virtual int vSetupKTLKeyWords();

    // keywords
    CKTLKeyString *m_pMSG;
    CKTLKeyString *m_pMSER;
    CKTLKeyInt    *m_pHEART;
    CKTLKeyString *m_pDWIM;
    
    CKTLKeyString *m_pC_VER;
    CKTLKeyInt    *m_pC_TELEM;
    CKTLKeyInt    *m_pC_TRATE;
    CKTLKeyInt    *m_pC_DEBUG;
    CKTLKeyInt    *m_pC_SDEBUG;
    CKTLKeyInt    *m_pC_GSMODE;

    CKTLInt    *m_pESTOP;
    CKTLInt    *m_pABORT;
    CKTLInt    *m_pWFSCAMINIT;
    CKTLFloat  *m_pWFSFRMRATE;
    CKTLInt    *m_pTTCINIT;
    CKTLFloat  *m_pTTFRMRATE;
    CKTLInt    *m_pAOMODE;
    CKTLInt    *m_pWFSTYPE;
    CKTLInt    *m_pWFSRES;
    CKTLInt    *m_pWFSTHRES;
    CKTLInt    *m_pTTCENTRMV;
    CKTLFloat  *m_pTTINTEG;
    CKTLInt    *m_pTTSIZE;
    CKTLInt    *m_pOFFLDENBL;
    CKTLFloat  *m_pUPLINKLPGAIN;
    CKTLFloat  *m_pUPLNKINTEG;
    CKTLInt    *m_pMTRXSYSCREATE;
    CKTLFloat  *m_pTWTINTEG;
    CKTLFloat  *m_pWOOINTEG;
    CKTLInt    *m_pVIBENBL;
    CKTLFloat  *m_pVIBFREQ;
    CKTLFloat  *m_pVIBALPHA;
    CKTLFloat  *m_pVIBINTEGGAIN;
    CKTLInt    *m_pWFSCAMDMYDUSE;
    CKTLInt    *m_pTTCAMDMYDUSE;
    CKTLInt    *m_pCENTDMYUSE;
    CKTLInt    *m_pTTDMYFOCUSE;
    CKTLInt    *m_pTTDMYUPLNKUSE;
    CKTLInt    *m_pTWTDMYSHPUSE;
    CKTLInt    *m_pWOODMYSHPUSE;
    CKTLInt    *m_pWOODMYVOLTUSE;
    CKTLInt    *m_pTWTDMYVOLTUSE;
    CKTLString *m_pC_HELPRTC;
    CKTLInt    *m_pTWTPOKE;
    CKTLInt    *m_pWOOPOKE;
    CKTLInt    *m_pDIAGFRMSV;
    CKTLInt    *m_pDIAGDATASEL;
    CKTLInt    *m_pDMSATCNTRESET;
    CKTLInt    *m_pDEBUGLVL;
    CKTLInt    *m_pTELEMSEL;
    CKTLInt    *m_pTELEMFILT;
    CKTLInt    *m_pRTCSTATEBC;
    CKTLInt    *m_pRTCSTATEREINIT;
    CKTLInt    *m_pAOLPOPNCLS;
    CKTLInt    *m_pTTLPOPNCLS;
    CKTLInt    *m_pAORESTART;
    CKTLInt    *m_pRTCRESTART;
    CKTLInt    *m_pUPLNKLPOPNCLS;
    CKTLInt    *m_pMTRXSYSCREAE;
    CKTLInt    *m_pFLATFLSV;
    CKTLInt    *m_pSHARPFLSV;
    CKTLInt    *m_pDMSFLAT;
    CKTLInt    *m_pDMSSHARP;
    CKTLInt    *m_pWFSCENTZR;
    CKTLInt    *m_pWFSCENTREFTK;
    CKTLInt    *m_pTTREFCENTTK;
    CKTLInt    *m_pWFSOFFSETZR;
    CKTLInt    *m_pWFSOFFSETSV;
    CKTLInt    *m_pTTOFFSETSV;
    CKTLFloat  *m_pTELMUPDRATE;
    CKTLFloat  *m_pTELMFILT;
    CKTLInt    *m_pTELMDATA;
    CKTLInt    *m_pDIAGFRMTOTK;
    CKTLInt    *m_pDIAGDATA;


    CKTLFile *m_pWFSCAMPGAIN;
    CKTLFile *m_pWFSCAMDARK;
    CKTLFile *m_pTTCPIXGAIN;
    CKTLFile *m_pTTCPDARK;
    CKTLFile *m_pWFSCENTWT;
    CKTLFile *m_pWFSCENTHRES;
    CKTLFile *m_pWFSCENTREF;
    CKTLFile *m_pWFSOFFSET;
    CKTLFile *m_pTTTHRES;
    CKTLFile *m_pTTLPGAIN;
    CKTLFile *m_pTTCENTREF;
    CKTLFile *m_pTTOFFSET;
    CKTLFile *m_pMTRXRECON;
    CKTLFile *m_pTWTBIAS;
    CKTLFile *m_pWOOBIAS;
    CKTLFile *m_pTWTLPGAIN;
    CKTLFile *m_pWOOLPGAIN;
    CKTLFile *m_pWFSCAMDMYD;
    CKTLFile *m_pTTCAMDMYD;
    CKTLFile *m_pCENTDMY;
    CKTLFile *m_pTTDMYFOC;
    CKTLFile *m_pTTDMYUPLNK;
    CKTLFile *m_pTWTDMYSHP;
    CKTLFile *m_pWOODMYSHP;
    CKTLFile *m_pWOODMYVOLT;
    CKTLFile *m_pTWTDMYVOLT;
    CKTLFile *m_pRECONSTPARM;
    CKTLFile *m_pWFSOFFSETFL;
    CKTLFile *m_pWFSOFFSETARY;
    CKTLFile *m_pTTOFFSETFL;
    CKTLFile *m_pTTOFFSETARY;
   




};

extern CInterAO *g_pInter;

extern ConnectionID gOurID;

#endif





















