/******************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:		cam_WFS.h

AUTHOR:			DHL (based on DWP's cam.h)		DATE: 10/7/04

FILE DESCRIPTION:
This is the header file for the SciMeasure Little Joe camera component.

******************************************************************************/

#ifndef _CAMERA_WFS_H
#define _CAMERA_WFS_H		1

// Defines

#define INIT_USE_AOA 0  // initial UseAOA flag; if 1, use AOA camera; if 0, use SciMeasure camera  
						// > IMPORTANT - without a decent amount of rework, the UseAOA flag has to 
						// > be know before the parms file is read; so, UseAOA is hard-coded to 
						// > this value regardless of the parms file value


// Global Variables
extern int16 UseAOA;
extern int WfsTotalSize;				// bytes in shared memory region
extern unsigned char *WfsBaseAddr;		// pointer to start of shared memory

extern int WFS_irq;
extern void *WFS_dev;

// Global Function Prototypes
void *CamWFS( void *arg );
void CamWFS_InitIntr( void );
void *CamWFS_DrvrWatch( void *arg );

#endif
