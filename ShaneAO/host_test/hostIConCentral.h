#ifndef HOST_ICON_CENTRAL_H
#define HOST_ICON_CENTRAL_H


#include "eventthread.h"
#include "hostIConListener.h"
#include "updateval.h"

#include "types.h"
#include "constant.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "inter.h"





class CCentral : public CEventThread
{
public:
    CCentral();
    ~CCentral();

    int Initialize();


    // Message ID that this event thread understands.
    enum {
	FIRST_CONTROL_MSG                      = 2000,
	CONTROL_INFO_UPDATE                    = 2001,
	HOSTCONNECTEDFLAG                      = 2002,
	HOSTICONPROCCMD                        = 2003,
	HOST_SEND_STRNG_NOPARM                 = 2004,
	HOST_SEND_STRNG_INT                    = 2005,
	HOST_SEND_STRNG_FLT                    = 2006,
	EXAM_DIAG_CMD_ERR_STRNG_NO_PARM        = 2007,
	EXAM_DIAG_CMD_ERR_STRNG_INT_PARM       = 2008,
	EXAM_DIAG_CMD_ERR_STRNG_FLT_PARM       = 2008,

	LAST_CONTROL_MSG  = 9999
    };

    static void* connector(void *thisPointer);

    virtual void vGetMsgRange(int *first, int *last){
	*first=FIRST_CONTROL_MSG;*last=LAST_CONTROL_MSG;};
    virtual bool vInMsgRange(int msg){
	return ((FIRST_CONTROL_MSG <= msg) && (msg <= LAST_CONTROL_MSG));};
    virtual int vStartupActions();

    // Local Variables
    int cmdFIFOfd;   // fd for FIFO used to send command messages to the HRT side
    pthread_t hostDiagThread;  // thread handles   // &&& change to CThread
    pthread_t DM_Thread;  // thread handles	// mrr   // &&& change to CThread
    
    // Global Function Prototype
    int16 HostIConProcCmd(const char msgBuf[]);
    int16 HostIConProcCmdBase(const char msgBuf[]);
    
    int queControlInfoUpdate();
    int queHostConnectedFlag(bool connected);
    int queHostIConProcCmd(const char *msgbuf);
    int queHostIConProcCmd(const char *msgbuf, int length);
    int queHostIConSendStrngNoParm(const char *str);
    int queHostIConSendStrngIntParm(const char *str, int n);
    int queHostIConSendStrngFltParm(const char *str, float32 f);

    int queExamine_eDiagCmdErrStrngNoParm(const char *diagString);
    int queExamine_eDiagCmdErrStrngIntParm(const char *diagString, int intParam);
    int queExamine_eDiagCmdErrStrngFltParm(const char *diagString, double fltParam);

    int setNEEDFUNC(CKTLKeyInt *pkey, int val);
    int setNEEDFUNC(CKTLKeyString *pkey, string val);
    int setNEEDFUNC(CKTLKeyFloat *pkey, double val);


    int setDWIM(string val);

    // Functions for text commands 
    // "ver" command
    int set_ver(string val); 
    boolean check_ver(); 

    int  set_telem(int val);
    void update_telem(int val);
    int  test_complete_telem(int respval);
    
    int  set_trate(int val);
    void update_trate(int val);
    int  test_complete_trate(int respval);
    
    int  set_debug(int val);
    int  set_sdebug();
    int  test_complete_debug(int val);
    int  test_complete_sdebug();
    void update_debug_sdebug(int val);

    int  set_gsmode(int val);
    void update_gsmode(int val);
    int  test_complete_gsmode(int respval);

protected:
    void CenterThread();      // the main function for this thread
    
    int vMsgHandler(CQMsg *pqmsg);

    int doControlInfoUpdate(CQMsg *pqmsg);
    int ControlInfoUpdate();

    int doHostConnectedFlag(CQMsg *pqmsg);
    int doHostIConProcCmd(CQMsg *pqmsg);
    int doHostIConSendStrngNoParm(CQMsg *pqmsg);
    int doHostIConSendStrngIntParm(CQMsg *pqmsg);
    int doHostIConSendStrngFltParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngNoParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngIntParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngFltParm(CQMsg *pqmsg);

    int16 HostIConProcMsgs(void);
    void HostIConSendStrngNoParm(const char *strngPtr );
    void HostIConSendStrngIntParm(const char *strngPtr, int16 intParam );
    void HostIConSendStrngFltParm(const char *strngPtr, float32 floatParam );
    void Change_Cam_Buf_Mapping (int16 idNum);


    CHostIConListener *m_pHostIConListener;

    // "ver" command variables
    CUpdateString m_verSRT;
    CUpdateString m_verHRT;

    CUpdateInt    m_telem;
    CUpdateInt    m_trate;
    CUpdateInt    m_debug;
    CUpdateInt    m_sdebug;
    CUpdateInt    m_gsmode;

    ////////////////////////////////////////////////
    //
    // This is a simple class used to trigger events
    // after short time intervals.
    //
    class CTriggerThread : public CThread
    {
    public:
	CTriggerThread();
	CTriggerThread(CCentral *pMaster);
	~CTriggerThread();
	
	int StopThread();
	
	static void* connector(void *thisPointer);
	
    protected:
	int Initialize();
	int TriggerLoop();
	CCentral *m_pMaster;
	CMutex m_LoopMutex;
	bool m_bLoop;
    };

    CTriggerThread *m_pTriggerThread;

};



extern CCondVarMutex    *g_pAllDone;
extern CCentral         *g_pCCentral;

#endif

