/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		centroid

AUTHOR:	DWP			DATE: 12/26/02

COMPONENT DESCRIPTION:
This component contains functions to perform centroiding for the LickAO control system.
At the moment, 3 centroiders are supported: quad (that uses 2 x 2 pixels), 4x4 binned
(that combines the four sets of 2 x 2 pixels in each corner of a 4 x 4 grid to produce
four bigger 'pixels'), and center-of-mass.

COMPONENT UNITS:
1.	Cent()
2.	CentInit()
3.	CentRestart()
4.	CentProcCmd()
5.	CentCentbin()
6.	CentCentcm()
7.	CentCentquad()
8.	CentCentcorr()
9.	CentCflatStart()
10.	CentCflatProcess()
11.	CentCflatAbort()
12.	CentRefcentStart()
13.	CentRefcentProcess()
14.	CentRefcentAbort()
15.	CentProcessBin()
16.	CentProcessCM()
17.	CentProcessQuad()
18.	CentDitherExtract()

REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_time.h>

#include "types.h"
#include "constant.h"
#include "cam.h"
#include "cam_WFS.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"


// Global Variables

int16 CrntCentroider;  // indicates which centroid algorithm is currently being used

int16 CentThresh;  // value centroider subtracts from raw data (then, if result is less than zero, 
				   // > centroider sets raw data to zero)

int16 CflatAvgN;  // number of frames to average when determining cflat
int16 CflatData[NUM_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current cflat data
int16 CflatUnusedTop[NUM_UNUSED_CAM_ROWS_TOP][NUM_PIX_IN_ROW];  // unused cflat data, top
int16 CflatUnusedBot[NUM_UNUSED_CAM_ROWS_BOT][NUM_PIX_IN_ROW];  // unused cflat data, bottom

int16 RefcentAvgN;  // number of frames to average when determining reference centroids
float32 RefcentsB[NUM_CENTROIDS];  // current reference centroids for 4x4 binned
float32 RefcentsC[NUM_CENTROIDS];  // current reference centroids for center-of-mass
float32 RefcentsQ[NUM_CENTROIDS];  // current reference centroids for quad-cell
float32 RefcentsO[NUM_CENTROIDS];  // current reference centroids for correlator

float32 Centoffs[NUM_CENTROIDS];  // current reference offsets (for image sharpening)

boolean TTDitherOn = false;	 // indicates whether TT mirror dithering is on or off
int16   TTDitherCosNdx;		 // indexes the TT cosine curve, TTDitherCosCurves[] for inserting the curve
float32 TTDitherCosCurves[DITHER_COS_LNGTH][DITHER_COS_LNGTH];  // cosine curves with which to dither TT mirror and extract curve

boolean CentGettingBehindFlag = false;


// Local Variables

static const float32 ttDitherCosUnity[DITHER_COS_LNGTH] = { 1.0, 0.707107, 0.0, -0.707107, -1.0, -0.707107, 0.0, 0.707107 };
static float32 ttDitherAmplitude;  // the amplitude of the TT sine curve
static float64 ttDitherConst;	   // constant used in TT dither extraction

static boolean ttDitherInitFlag = true;	 // flag indicating that TT dither should initialize (to stretch initialization over several cycles)
static int16 ttDitherInitNdx  = 0;	     // index used for TT initialization
static int16 ttDitherOldDataNdx = 0;					 // index into old TT dither data
static int16 ttDitherNewDataNdx = DITHER_COS_LNGTH - 1;  // index into new TT dither data

static int32 ttDitherOldData[NUM_CENTROIDS][DITHER_NUM_CYCLES][DITHER_COS_LNGTH];  // 'old' TT dither data used for box car averagers
static int32 ttDitherSums[NUM_CENTROIDS][DITHER_COS_LNGTH];					   	   // TT dither sums used for box car averagers
static float32 ttDitherNewData[NUM_CENTROIDS][DITHER_COS_LNGTH];				   // new TT dither data

static int16 runCmd = eCmdNone;  // the 'long' command that is currently being executed, if any

static int16 cflatCntr;  // counts frames being averaged when determining cflat
static int16 cflatState;  // current state of the process determining cflat
static int32 cflatSums[NUM_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // summed frame data for determining cflat
static int32 cflatUSumsTop[NUM_UNUSED_CAM_ROWS_TOP][NUM_PIX_IN_ROW];  // summed unused cflat data, top
static int32 cflatUSumsBot[NUM_UNUSED_CAM_ROWS_BOT][NUM_PIX_IN_ROW];  // summed unused cflat data, bottom

static int16 refcentCntr;  // counts sets of centroid data being averaged when determining refcent
static int16 refcentState;  // current state of the process determining refcent
static float32 refcentSums[NUM_CENTROIDS];  // summed centroid data for determining refcent

static boolean successfulPost;  // flag indicating a message has been successfully posted to the recon semaphore

static int16 centNdx;  // current index into centroid arrays

static boolean resyncFlag;  // flag indicating that an error has occurred somewhere in the centroiding process and
							// > that the remainder of the current frame should be ignored

static float32 xCentsSums;  // sum of X centroids used to compute mean
static float32 yCentsSums;  // sum of Y centroids used to compute mean

static float32 centCMWeightsX[4] = { -1.5, -0.5, 0.5, 1.5 };  // weights for computing X center of mass
static float32 centCMWeightsY[4] = { -1.5, -0.5, 0.5, 1.5 };  // weights for computing Y center of mass

// this structure contains the following data for each sub-aperture:
//	int16	subApRow	row of sub-apertures that this 4x4 cell is in (0 -> 7, -1 = end of list)
//	int16	pixRows[4]	pixel rows, within the subApRow, that the 16 pixels are in
//	int16	pixCols[4]	pixel columns that the 16 pixels are in
static BinnedDescType binnedDescs[NUM_SUBAPS + 1] =  // the '+ 1' is for the terminator row
{
											//                                 AABB    AABB
	{ 0, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                                 AABB    AABB
	{ 0, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                                 CCDD    CCDD
											//                                 CCDD    CCDD

	{ 1, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 1, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 1, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 1, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 1, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 1, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 2, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 2, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 2, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 2, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 2, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 2, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 3, {1, 2, 3, 4}, { 9, 10, 11, 12} },
	{ 3, {1, 2, 3, 4}, {15, 16, 17, 18} },  //         AABB    AABB    AABB                    AABB    AABB    AABB
	{ 3, {1, 2, 3, 4}, {21, 22, 23, 24} },  //         AABB    AABB    AABB                    AABB    AABB    AABB
	{ 3, {1, 2, 3, 4}, {39, 40, 41, 42} },  //         CCDD    CCDD    CCDD                    CCDD    CCDD    CCDD
	{ 3, {1, 2, 3, 4}, {45, 46, 47, 48} },  //         CCDD    CCDD    CCDD                    CCDD    CCDD    CCDD
	{ 3, {1, 2, 3, 4}, {51, 52, 53, 54} },

	{ 4, {1, 2, 3, 4}, { 9, 10, 11, 12} },
	{ 4, {1, 2, 3, 4}, {15, 16, 17, 18} },  //         AABB    AABB    AABB                    AABB    AABB    AABB
	{ 4, {1, 2, 3, 4}, {21, 22, 23, 24} },  //         AABB    AABB    AABB                    AABB    AABB    AABB
	{ 4, {1, 2, 3, 4}, {39, 40, 41, 42} },  //         CCDD    CCDD    CCDD                    CCDD    CCDD    CCDD
	{ 4, {1, 2, 3, 4}, {45, 46, 47, 48} },  //         CCDD    CCDD    CCDD                    CCDD    CCDD    CCDD
	{ 4, {1, 2, 3, 4}, {51, 52, 53, 54} },

	{ 5, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 5, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 5, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 5, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 5, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 5, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 6, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 6, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 6, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 AABB    AABB    AABB    AABB    AABB    AABB
	{ 6, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 6, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 CCDD    CCDD    CCDD    CCDD    CCDD    CCDD
	{ 6, {1, 2, 3, 4}, {45, 46, 47, 48} },

											//                                 AABB    AABB
	{ 7, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                                 AABB    AABB
	{ 7, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                                 CCDD    CCDD
											//                                 CCDD    CCDD

	{-1, {0, 0, 0, 0}, { 0,  0,  0,  0} }   // terminator row
};


// this structure contains the following data for each sub-aperture:
//	int16	subApRow	row of sub-apertures that this 4x4 cell is in (0 -> 7, -1 = end of list)
//	int16	pixRows[4]	pixel rows, within the subApRow, that the 16 pixels are in
//	int16	pixCols[4]	pixel columns that the 16 pixels are in
static CentMassDescType centMassDescs[NUM_SUBAPS + 1] =  // the '+ 1' is for the terminator row
{
											//                                 0123    0123
	{ 0, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                                 1xxx    1xxx
	{ 0, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                                 2xxx    2xxx
											//                                 3xxx    3xxx

	{ 1, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 1, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 0123    0123    0123    0123    0123    0123
	{ 1, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 1xxx    1xxx    1xxx    1xxx    1xxx    1xxx
	{ 1, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 2xxx    2xxx    2xxx    2xxx    2xxx    2xxx
	{ 1, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 3xxx    3xxx    3xxx    3xxx    3xxx    3xxx
	{ 1, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 2, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 2, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 0123    0123    0123    0123    0123    0123
	{ 2, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 1xxx    1xxx    1xxx    1xxx    1xxx    1xxx
	{ 2, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 2xxx    2xxx    2xxx    2xxx    2xxx    2xxx
	{ 2, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 3xxx    3xxx    3xxx    3xxx    3xxx    3xxx
	{ 2, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 3, {1, 2, 3, 4}, { 9, 10, 11, 12} },
	{ 3, {1, 2, 3, 4}, {15, 16, 17, 18} },  //         0123    0123    0123                    0123    0123    0123
	{ 3, {1, 2, 3, 4}, {21, 22, 23, 24} },  //         1xxx    1xxx    1xxx                    1xxx    1xxx    1xxx
	{ 3, {1, 2, 3, 4}, {39, 40, 41, 42} },  //         2xxx    2xxx    2xxx                    2xxx    2xxx    2xxx
	{ 3, {1, 2, 3, 4}, {45, 46, 47, 48} },  //         3xxx    3xxx    3xxx                    3xxx    3xxx    3xxx
	{ 3, {1, 2, 3, 4}, {51, 52, 53, 54} },

	{ 4, {1, 2, 3, 4}, { 9, 10, 11, 12} },
	{ 4, {1, 2, 3, 4}, {15, 16, 17, 18} },  //         0123    0123    0123                    0123    0123    0123
	{ 4, {1, 2, 3, 4}, {21, 22, 23, 24} },  //         1xxx    1xxx    1xxx                    1xxx    1xxx    1xxx
	{ 4, {1, 2, 3, 4}, {39, 40, 41, 42} },  //         2xxx    2xxx    2xxx                    2xxx    2xxx    2xxx
	{ 4, {1, 2, 3, 4}, {45, 46, 47, 48} },  //         3xxx    3xxx    3xxx                    3xxx    3xxx    3xxx
	{ 4, {1, 2, 3, 4}, {51, 52, 53, 54} },

	{ 5, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 5, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 0123    0123    0123    0123    0123    0123
	{ 5, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 1xxx    1xxx    1xxx    1xxx    1xxx    1xxx
	{ 5, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 2xxx    2xxx    2xxx    2xxx    2xxx    2xxx
	{ 5, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 3xxx    3xxx    3xxx    3xxx    3xxx    3xxx
	{ 5, {1, 2, 3, 4}, {45, 46, 47, 48} },

	{ 6, {1, 2, 3, 4}, {15, 16, 17, 18} },
	{ 6, {1, 2, 3, 4}, {21, 22, 23, 24} },  //                 0123    0123    0123    0123    0123    0123
	{ 6, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                 1xxx    1xxx    1xxx    1xxx    1xxx    1xxx
	{ 6, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                 2xxx    2xxx    2xxx    2xxx    2xxx    2xxx
	{ 6, {1, 2, 3, 4}, {39, 40, 41, 42} },  //                 3xxx    3xxx    3xxx    3xxx    3xxx    3xxx
	{ 6, {1, 2, 3, 4}, {45, 46, 47, 48} },

											//                                 0123    0123
	{ 7, {1, 2, 3, 4}, {27, 28, 29, 30} },  //                                 1xxx    1xxx
	{ 7, {1, 2, 3, 4}, {33, 34, 35, 36} },  //                                 2xxx    2xxx
											//                                 3xxx    3xxx

	{-1, {0, 0, 0, 0}, { 0,  0,  0,  0} }   // terminator row
};


// this structure contains the following data for each sub-aperture:
//	int16	subApRow	row of sub-apertures that this quad-cell is in (0 -> 7, -1 = end of list)
//	int16	pixRows[2]	pixel rows, within the subApRow, that the 4 pixels are in
//	int16	pixCols[2]	pixel columns that the 4 pixels are in
static QuadCellDescType quadCellDescs[NUM_SUBAPS + 1] =  // the '+ 1' is for the terminator row
{
	{ 0, {2, 3}, {28, 29} },  //                                  AB      AB
	{ 0, {2, 3}, {34, 35} },  //                                  CD      CD

	{ 1, {2, 3}, {16, 17} },
	{ 1, {2, 3}, {22, 23} },
	{ 1, {2, 3}, {28, 29} },  //                  AB      AB      AB      AB      AB      AB
	{ 1, {2, 3}, {34, 35} },  //                  CD      CD      CD      CD      CD      CD
	{ 1, {2, 3}, {40, 41} },
	{ 1, {2, 3}, {46, 47} },

	{ 2, {2, 3}, {16, 17} },
	{ 2, {2, 3}, {22, 23} },
	{ 2, {2, 3}, {28, 29} },  //                  AB      AB      AB      AB      AB      AB
	{ 2, {2, 3}, {34, 35} },  //                  CD      CD      CD      CD      CD      CD
	{ 2, {2, 3}, {40, 41} },
	{ 2, {2, 3}, {46, 47} },

	{ 3, {2, 3}, {10, 11} },
	{ 3, {2, 3}, {16, 17} },
	{ 3, {2, 3}, {22, 23} },  //          AB      AB      AB                      AB      AB      AB
	{ 3, {2, 3}, {40, 41} },  //          CD      CD      CD                      CD      CD      CD
	{ 3, {2, 3}, {46, 47} },
	{ 3, {2, 3}, {52, 53} },

	{ 4, {2, 3}, {10, 11} },
	{ 4, {2, 3}, {16, 17} },
	{ 4, {2, 3}, {22, 23} },  //          AB      AB      AB                      AB      AB      AB
	{ 4, {2, 3}, {40, 41} },  //          CD      CD      CD                      CD      CD      CD
	{ 4, {2, 3}, {46, 47} },
	{ 4, {2, 3}, {52, 53} },

	{ 5, {2, 3}, {16, 17} },
	{ 5, {2, 3}, {22, 23} },
	{ 5, {2, 3}, {28, 29} },  //                  AB      AB      AB      AB      AB      AB
	{ 5, {2, 3}, {34, 35} },  //                  CD      CD      CD      CD      CD      CD
	{ 5, {2, 3}, {40, 41} },
	{ 5, {2, 3}, {46, 47} },

	{ 6, {2, 3}, {16, 17} },
	{ 6, {2, 3}, {22, 23} },
	{ 6, {2, 3}, {28, 29} },  //                  AB      AB      AB      AB      AB      AB
	{ 6, {2, 3}, {34, 35} },  //                  CD      CD      CD      CD      CD      CD
	{ 6, {2, 3}, {40, 41} },
	{ 6, {2, 3}, {46, 47} },

	{ 7, {2, 3}, {28, 29} },  //                                  AB      AB
	{ 7, {2, 3}, {34, 35} },  //                                  CD      CD

	{-1, {0, 0}, { 0,  0} }   // terminator row
};


// Local Function Prototypes

static void CentInit( void );
static void CentProcCmd( int16 cmdNdx );
static void CentCentbin( void );
static void CentCentcm( void );
static void CentCentquad( void );
static void CentCentcorr( void );
static void CentCflatStart( void );
static void CentCflatProcess( int16 rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW], 
							  int16 rawUDataTop[][NUM_PIX_IN_ROW], int16 rawUDataBot[][NUM_PIX_IN_ROW] );
static void CentCflatAbort( void );
static void CentRefcentStart( void );
static void CentRefcentProcess( float32 centData[] );
static void CentRefcentAbort( void );
static int16 CentProcessBin( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							 float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							 float32 totalIntens[], float32 pixels[][4]  );
static int16 CentProcessCM( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							float32 totalIntens[] );
static int16 CentProcessQuad( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							  float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							  float32 totalIntens[], float32 pixels[][4] );
static int16 CentProcessCorr( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							  float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							  float32 totalIntens[] );
static void CentDitherExtract( float32 centroids[], float32 xResults[] );



/**********************************************************************************************

UNIT NAME:  Cent()

UNIT DESCRIPTION:
This unit is the centroid thread mainline.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *Cent( void *arg )
{
	static int16 crntCentroiderLocal;
	static int16 diagTelemCntr = 0;
	boolean sendTelemData;
	int16 tmpWriteNdx;
	int16 errVal, subApRow;
	int16 buffCntr, buffNdx, firstFree, firstFreeButWithDiag;
	diagFIFOMsgType diagFIFOMsg;
    CamBuffType *camBuffPtr;
    CentBuffType *centBuffPtr;

	int16 centNdxSav;  //CCCTTT

//	initialize variables used by the centroid thread
	CentInit();

//	enter an infinite loop
//		wait on the centroid semaphore (for camera data or a command) and report an error if one occurs in rtl_sem_wait()
	while (1)
	{
//        rtl_printf("c");  //III

		if (rtl_sem_wait(&CentSem) < 0)
			SysConSendStrngIntParm("Error waiting on CentSem, errno = ", rtl_errno);

//		if a command is pending, call CentProcCmd() to process it
		else if (CentDataPtr->cmdStart != eCmdNone)
		{
			CentProcCmd(CentDataPtr->cmdStart);
			CentDataPtr->cmdStart = eCmdNone;
		}

//		else
//			increment the camera sub-ap row read index and, if necessary, the camera buffer read index
		else
		{
//            rtl_printf("C");  //III
//            clock_gettime(CLOCK_REALTIME, &TimeCent1);  //TTT

            camBuffPtr = &CamDataPtr->camBuffs[CamDataPtr->camBuffNdxs[CamDataPtr->readNdx]];
			if (++camBuffPtr->subapRowReadNdx >= NUM_SUBAP_ROWS_PER_FRAME)
			{
				if (++CamDataPtr->readNdx >= NUM_CAM_BUFFS)
					CamDataPtr->readNdx = 0;
                camBuffPtr = &CamDataPtr->camBuffs[CamDataPtr->camBuffNdxs[CamDataPtr->readNdx]];
				camBuffPtr->subapRowReadNdx = 0;
			}
			subApRow = camBuffPtr->subapRowReadNdx;

//			if we're using the AOA camera, make sure that the sub-ap row read index is no more than five less 
//			> than the write index (if it is, we're getting behind in the centroiding)
//			otherwise, make sure the read index is no more than one less than the write index (this is different because
//			> the AOA data is sent a few centroid rows at a time, whereas all centroid rows for a frame are sent at once 
//			> for the SciMeasure camera)
			if (UseAOA != 0)  // using AOA camera
			{
				tmpWriteNdx = camBuffPtr->subapRowWriteNdx;
				if (CamDataPtr->readNdx != CamDataPtr->writeNdx)
					tmpWriteNdx += NUM_SUBAP_ROWS_PER_FRAME;
				if (subApRow < (tmpWriteNdx - 5))
				{
					SysConSendStrngNoParm("Warning: centroiding getting behind");
					CentGettingBehindFlag = true;
				}
			}
			else  // using SciMeasure camera
			{
				tmpWriteNdx = CamDataPtr->writeNdx;
				if (CamDataPtr->readNdx > CamDataPtr->writeNdx)
					tmpWriteNdx += NUM_CAM_BUFFS;
				if (CamDataPtr->readNdx < (tmpWriteNdx - 1))
					SysConSendStrngNoParm("Warning: centroiding getting behind");
			}

//			if we're at the beginning of a new frame
			if (subApRow == 0)
			{
				crntCentroiderLocal = CrntCentroider;

//				reset the resync flag to false and centNdx to 0
				resyncFlag = false;
				centNdx = 0;

//              if we successfully posted last time
//					find the next available camera data buffer
//					if there isn't an available camera data buffer
//						report an error
//					if the buffer we're about to use has unread diag data in it
//						initiate a diag data resync
                if (successfulPost)
                {
                    buffCntr = 0;
                    buffNdx = CentDataPtr->centroidBuffNdxs[CentDataPtr->writeNdx];
                    firstFree = -1;
                    firstFreeButWithDiag = -1;
                    while ((buffCntr < NUM_CENT_BUFFS) && (firstFree < 0))
                    {
                        if (++buffNdx >= NUM_CENT_BUFFS)
                            buffNdx = 0;
                        if (!CentDataPtr->centroidBuffs[buffNdx].inUseFlag)
                        {
                            if ((CentDataPtr->diagStatFlag != eDiagStatOperating) ||
                                (CentDataPtr->centroidBuffs[buffNdx].diagIndicator == eDiagIndNotInUse))
                                firstFree = buffNdx;
                            else if (firstFreeButWithDiag < 0)
                                firstFreeButWithDiag = buffNdx;
                        }
                        buffCntr++;
                    }

                    if ((firstFree < 0) && (firstFreeButWithDiag < 0))
                        SysConSendStrngNoParm("Error: centroid buffers all full");
                    else
                    {
                        if (++CentDataPtr->writeNdx >= NUM_CENT_BUFFS)
                            CentDataPtr->writeNdx = 0;

                        if (firstFree < 0)
                        {
                            firstFree = firstFreeButWithDiag;

                            CentDataPtr->diagStatFlag = eDiagStatResyncing;
                            diagFIFOMsg.diagID = eDiagCentData;
                            diagFIFOMsg.diagStat = eDiagStatResyncing;
                            diagFIFOMsg.diagReadNdx = firstFree;
                            if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
                                SysConSendStrngIntParm("Error writing centroid resync to diag FIFO, errno = ", rtl_errno);
                            SysConSendStrngNoParm("Warning: centroid diagnostic data is resyncing");
                        }
                        CentDataPtr->centroidBuffNdxs[CentDataPtr->writeNdx] = firstFree;
                    }
					successfulPost = false;
                }
            }
            centBuffPtr = &CentDataPtr->centroidBuffs[CentDataPtr->centroidBuffNdxs[CentDataPtr->writeNdx]];
            centBuffPtr->inUseFlag = true;

//			if we're not re-syncing the frame
//				call one of the centroid processors, depending on CrntCentroider, to compute centroids for the current sub-ap row
//				if an error occurred, set the resync flag
//				else
			if (!resyncFlag)
			{
				errVal = eErrNoErr;
				switch (crntCentroiderLocal)
				{
					case eCentBin:
						errVal = CentProcessBin(subApRow, camBuffPtr->data[subApRow], &centNdx,
												&centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
												&centBuffPtr->xTTError, &centBuffPtr->yTTError,
												centBuffPtr->intens, centBuffPtr->pixels);
						break;

					case eCentCM:
						errVal = CentProcessCM(subApRow, camBuffPtr->data[subApRow], &centNdx,
											   &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
											   &centBuffPtr->xTTError, &centBuffPtr->yTTError,
											   centBuffPtr->intens);
						break;

					case eCentQuad:
						errVal = CentProcessQuad(subApRow, camBuffPtr->data[subApRow], &centNdx,
												 &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
												 &centBuffPtr->xTTError, &centBuffPtr->yTTError,
												 centBuffPtr->intens, centBuffPtr->pixels);
						break;

					case eCentCorr:
						errVal = CentProcessCorr(subApRow, camBuffPtr->data[subApRow], &centNdx,
											     &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
											     &centBuffPtr->xTTError, &centBuffPtr->yTTError,
											     centBuffPtr->intens);
						break;

					case eCentTest:
						centNdxSav = centNdx;
						errVal = CentProcessBin(subApRow, camBuffPtr->data[subApRow], &centNdx,
											    &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
											    &centBuffPtr->xTTError, &centBuffPtr->yTTError,
											    centBuffPtr->intens, centBuffPtr->pixels);
						centNdx = centNdxSav;
						errVal = CentProcessCorr(subApRow, camBuffPtr->data[subApRow], &centNdx,
											     &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[NUM_SUBAPS],
											     &centBuffPtr->xTTError, &centBuffPtr->yTTError,
											     centBuffPtr->intens);
						break;

					default:
						errVal = eErrRange;
						SysConSendStrngNoParm("Error: unexpected CrntCentroider value");
						break;
				}

				if (errVal != eErrNoErr)
					resyncFlag = true;
				else
				{
//					if we're at the end of the frame
					if (subApRow == (NUM_SUBAP_ROWS_PER_FRAME - 1))
					{
//						if we're running the cflat command
//							call CentCflatProcess() to process the raw data
//						else if we're running the refcent command
//							call CentRefcentProcess() to process the centroid data
						if (runCmd == eCmdCentFlat)
							CentCflatProcess(camBuffPtr->data, camBuffPtr->unusedDataTop, camBuffPtr->unusedDataBot);
						else if (runCmd == eCmdCentRef)
							CentRefcentProcess(centBuffPtr->xyCents);

//						if TT dithering is on
//							call CentDitherExtract() to extract the cosine waves; note that, for now, for test purposes, when 
//							> dithering is enabled, the centroid intensity values are over-written with the extracted cosine 
//							> curves for the X centroids
						if (TTDitherOn)
							CentDitherExtract(centBuffPtr->xyCents, centBuffPtr->intens);

//						send the centroids off to recon
 						if (rtl_sem_post(&ReconSem) < 0)
							SysConSendStrngIntParm("Error posting to ReconSem, errno = ", rtl_errno);
                        else
                            successfulPost = true;

//						if diag data transfer is to be restarted and we're not operating already
//							indicate that we're operating
//						else if diag data transfer is to enter standby and we're not in standby already
//							indicate that we're operating in standby
						if ((CentDataPtr->diagActFlag == eDiagActRestart) && (CentDataPtr->diagStatFlag != eDiagStatOperating))
                        {
							CentDataPtr->diagStatFlag = eDiagStatOperating;
                            CentDataPtr->diagActFlag = eDiagActNoAction;
                        }
						else if ((CentDataPtr->diagActFlag == eDiagActEnterStandby) && (CentDataPtr->diagStatFlag != eDiagStatStandby))
                        {
							CentDataPtr->diagStatFlag = eDiagStatStandby;
                            CentDataPtr->diagActFlag = eDiagActNoAction;
                        }

//						if diagnostic data transfer (to the SRT) is operating and a message should be sent
//							send the centroids off to diag
						if ((CentDataPtr->diagTelemRate > 0) && 
							(++diagTelemCntr >= (CamFrameRate / CentDataPtr->diagTelemRate)))
						{
							diagTelemCntr = 0;
							sendTelemData = true;
						}
						else
							sendTelemData = false;

						if ((CentDataPtr->diagStatFlag == eDiagStatOperating) && 
							((CentDataPtr->diagDiagCntr > 0) || sendTelemData  || CentDataPtr->diagPSFFlag))
						{
							if (sendTelemData)
								centBuffPtr->diagIndicator = eDiagIndTelem;
							else
								centBuffPtr->diagIndicator = eDiagIndInUse;

							diagFIFOMsg.diagID = eDiagCentData;
							diagFIFOMsg.diagStat = eDiagStatOperating;
							diagFIFOMsg.diagReadNdx = CentDataPtr->centroidBuffNdxs[CentDataPtr->writeNdx];

							if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
							{
								centBuffPtr->diagIndicator = eDiagIndNotInUse;
								SysConSendStrngIntParm("Error writing centroid data to diag FIFO, errno = ", rtl_errno);
							}
                            else
                            {
                                if (CentDataPtr->diagDiagCntr > 0)
                                    CentDataPtr->diagDiagCntr--;
                            }
						}
					}
				}
			}
            camBuffPtr->inUseFlag = false;  // it appears that this is getting reset prematurely; but, it won't get checked again
											// > until the frame is complete or there's an error; so, it's okay

//            clock_gettime(CLOCK_REALTIME, &TimeCent2);  //TTT
		}
	}

	return 0;
}


/**********************************************************************************************

UNIT NAME:  CentInit()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the centroid thread.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentInit( void )
{
    int16 i, j;
	int retVal;
	struct rtl_timespec next;

//	initialize variables
	CentDataPtr->cmdStart = eCmdNone;
	CentDataPtr->cmdCrnt = eCmdNone;

	CentDataPtr->writeNdx = NUM_CENT_BUFFS - 1;
	CentDataPtr->readNdx = NUM_CENT_BUFFS - 1;

	CentDataPtr->diagActFlag = eDiagActNoAction;
	CentDataPtr->diagStatFlag = eDiagStatStandby;
	CentDataPtr->diagDiagRate = 0;
	CentDataPtr->diagDiagCntr = 0;
	CentDataPtr->diagTelemRate = 0;
	CentDataPtr->diagPSFFlag = false;

	for (i = 0; i < NUM_CENT_BUFFS; i++)
    {
        CentDataPtr->centroidBuffNdxs[i] = NUM_CENT_BUFFS - 1;
        CentDataPtr->centroidBuffs[i].inUseFlag = false;
        CentDataPtr->centroidBuffs[i].diagIndicator = eDiagIndNotInUse;
    }

	centNdx = 0;
	resyncFlag = false;
	xCentsSums = 0.0;
	yCentsSums = 0.0;
    successfulPost = true;

	TTDitherOn = false;
	TTDitherCosNdx = 0;
	ttDitherAmplitude = 0.0;
	ttDitherConst = 0.0;
	for (i = 0; i < DITHER_COS_LNGTH; i++)
	{
		for (j = 0; j < DITHER_COS_LNGTH; j++)
			TTDitherCosCurves[i][j] = 0.0;
	}
	ttDitherInitFlag = true;
	ttDitherInitNdx  = 0;
	ttDitherOldDataNdx = 0;
	ttDitherNewDataNdx = DITHER_COS_LNGTH - 1;

	CentGettingBehindFlag = false;

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
	while (!OkayToFinishInit)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from rtl_nanosleep() in CentInit(), retVal = ", retVal);
    }

//	call the initialization function for the current centroider, as indicated by CrntCentroider
	switch (CrntCentroider)
	{
		case eCentBin:
			CentCentbin();
			break;
		case eCentCM:
			CentCentcm();
			break;
		case eCentQuad:
			CentCentquad();
			break;
		case eCentCorr:
			CentCentcorr();
			break;
		case eCentTest:
			CentCentbin();
			CentCentcorr();
			break;
	}

    CentInitDone = true;
}


/**********************************************************************************************

UNIT NAME:  CentRestart()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the centroid thread when processing
is restarted after the camera has been turned off then back on.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void CentRestart( void )
{
    int16 i;

//	initialize variables
	CentDataPtr->writeNdx = NUM_CENT_BUFFS - 1;
	CentDataPtr->readNdx = NUM_CENT_BUFFS - 1;

	for (i = 0; i < NUM_CENT_BUFFS; i++)
    {
        CentDataPtr->centroidBuffNdxs[i] = NUM_CENT_BUFFS - 1;
        CentDataPtr->centroidBuffs[i].inUseFlag = false;
        CentDataPtr->centroidBuffs[i].diagIndicator = eDiagIndNotInUse;
    }

	centNdx = 0;
	resyncFlag = false;
	xCentsSums = 0.0;
	yCentsSums = 0.0;
    successfulPost = true;
}


/**********************************************************************************************

UNIT NAME:  CentProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process centroid commands.

UNIT INPUTS:
	cmdNdx - index into CmdTable[] for the command to be processed

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentProcCmd( int16 cmdNdx )
{
    int16 i, j, k;
	float32 cosVal;

//	switch on command ID corresponding to command index
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		  // abort long commands
			switch (runCmd)
			{
				case eCmdCentFlat:
					CentCflatAbort();
					break;

				case eCmdCentRef:
					CentRefcentAbort();
					break;

				default:
					SysConSendStrngNoParm("Error: unexpected command abort received by cent");
					break;
			}
			break;

		case eCmdCentBin:     // selects 4x4 binned as centroid algorithm
			CentCentbin();
			CrntCentroider = eCentBin;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: centbin complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentCM:      // selects center-of-mass as centroid algorithm
			CentCentcm();
			CrntCentroider = eCentCM;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: centcm complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentQuad:    // selects quad-cell as centroid algorithm
			CentCentquad();
			CrntCentroider = eCentQuad;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: centquad complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentCorr:    // selects correlator as centroid algorithm
			CentCentcorr();
			CrntCentroider = eCentCorr;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: centcorr complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentTest:    // selects centroid test mode
			CentCentbin();
			CentCentcorr();
			CrntCentroider = eCentTest;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: centtest complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentFlat:    // determines and stores new flat field file (dark levels) (long command)
			CentCflatStart();
			break;

		case eCmdCentRef:     // determines and stores new ref cents (long command)
			CentRefcentStart();
			break;

		case eCmdCentSParms:  // sets ref cents to zeros (but doesn't store to disk!)
			switch (CrntCentroider)
			{
				case eCentBin:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsB[i] = 0.0;
					break;
				case eCentCM:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsC[i] = 0.0;
					break;
				case eCentQuad:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsQ[i] = 0.0;
					break;
				case eCentCorr:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsO[i] = 0.0;
					break;
				case eCentTest:
					for (i = 0; i < NUM_CENTROIDS; i++)
					{
						RefcentsB[i] = 0.0;
						RefcentsO[i] = 0.0;
					}
					break;
			}
			SysConSendStrngNoParm("Notification: sparms complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentThresh:  // sets threshold used by centroider on raw data
			CentThresh = CentDataPtr->cmdIntParam;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: thresh complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentCentoffs:  // sets centroid offsets sent by host for image sharpening
			for (i = 0; i < NUM_CENTROIDS; i++)
				Centoffs[i] = FileDataShmPtr->newCentoffs[i];
			SysConSendStrngNoParm("Notification: centoffs complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentSaveoffs:  // saves centroid offsets
			FileDataPut(eFileTypeCentOff);
			SysConSendStrngNoParm("Notification: saveoffs complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentDon:  // turns TT mirror dithering on
			TTDitherCosNdx = 0;
			ttDitherAmplitude = CentDataPtr->cmdFloatParam;

			for (i = 0; i < DITHER_COS_LNGTH; i++)
			{
				cosVal = ttDitherCosUnity[i] * ttDitherAmplitude;
				j = i;
				for (k = 0; k < DITHER_COS_LNGTH; k++)
				{
					TTDitherCosCurves[k][j] = cosVal;
					if (++j >= DITHER_COS_LNGTH)
						j = 0;
				}
			}

			if (ttDitherAmplitude == 0.0)
				ttDitherConst = 0.0;
			else
				ttDitherConst = 1.414213 / 															    // 1.414213 brings the extracted unity cos curve to 1.0
								(0.707107 * ttDitherAmplitude * DITHER_COS_LNGTH * DITHER_NUM_CYCLES);  // 0.755929 is the biased stdev of a unit cos curve

			ttDitherInitFlag = true;
			ttDitherInitNdx  = 0;
			ttDitherOldDataNdx = 0;
			ttDitherNewDataNdx = DITHER_COS_LNGTH - 1;
			TTDitherOn = true;

			SysConSendStrngNoParm("Notification: don complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCentDoff:  // turns TT mirror dithering off
			TTDitherOn = false;
			SysConSendStrngNoParm("Notification: doff complete");
			CentDataPtr->cmdCrnt = eCmdNone;
			break;

		default:
			SysConSendStrngIntParm("Error: unexpected command received by cent, cmdNdx = ", cmdNdx);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CentCentbin()

UNIT DESCRIPTION:
The purpose of this unit is to execute the 'centbin' command and, in so doing, initialize the
4x4 binned algorithm.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCentbin( void )
{
//	initialize variables
}


/**********************************************************************************************

UNIT NAME:  CentCentcm()

UNIT DESCRIPTION:
The purpose of this unit is to execute the 'centcm' command and, in so doing, initialize the
center of mass algorithm.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCentcm( void )
{
//	initialize variables
}


/**********************************************************************************************

UNIT NAME:  CentCentquad()

UNIT DESCRIPTION:
The purpose of this unit is to execute the 'centquad' command and, in so doing, initialize the
quad-cell algorithm.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCentquad( void )
{
//	initialize variables
}


/**********************************************************************************************

UNIT NAME:  CentCentcorr()

UNIT DESCRIPTION:
The purpose of this unit is to execute the 'centcorr' command and, in so doing, initialize the
correlator algorithm.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCentcorr( void )
{
//	initialize variables
}


/**********************************************************************************************

UNIT NAME:  CentCflatStart()

UNIT DESCRIPTION:
The purpose of this unit is to start the processing of the cflat command by initializing the
cflat sums (to be used to compute averages) and setting counters and flags necessary to run 
the command.

Note that, for historical reasons, the data being determined and stored here is referred to as
'centroid flat-field' data (or, 'cflat' data).  In reality, it is simply dark data (used to try
to subtract out electronic and other background noise).

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCflatStart( void )
{
	int16 i, j, k;

//	initialize cflat sums to zeroes
	for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
	{
		for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
		{
			for (k = 0; k < NUM_PIX_IN_ROW; k++)
				cflatSums[i][j][k] = 0;
		}
	}
	for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
	{
		for (k = 0; k < NUM_PIX_IN_ROW; k++)
			cflatUSumsTop[j][k] = 0;
	}
	for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
	{
		for (k = 0; k < NUM_PIX_IN_ROW; k++)
			cflatUSumsBot[j][k] = 0;
	}

//	initialize sum counter and cflat state and set flag to indicate that the cflat command is running
	cflatCntr = 0;
	cflatState = eCmdStateSum;
	runCmd = eCmdCentFlat;
}


/**********************************************************************************************

UNIT NAME:  CentCflatProcess()

UNIT DESCRIPTION:
The purpose of this unit is to process the cflat command by receiving CflatAvgN frames worth of
raw camera data, averaging those frames, and writing the result to a file when done.

Note that this will execute 1 frame longer than CflatAvgN in order to spread out the averaging
and storing of the data.

UNIT INPUTS:
1.	rawUDataTop[][] - raw unused camera data for current frame, top
2.	rawData[][][] - raw camera data for current frame
3.	rawUDataBot[][] - raw unused camera data for current frame, bottom

UNIT OUTPUTS:
1.	updates CflatData[][][], CflatUnusedTop[][], and CflatUnusedBot[][]


UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCflatProcess( int16 rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
							  int16 rawUDataTop[][NUM_PIX_IN_ROW], int16 rawUDataBot[][NUM_PIX_IN_ROW] )
{
	int16 i, j, k;

//	sum or average and store cflat data, depending on cflatState
	switch (cflatState)
	{
		case eCmdStateSum:
//			add current raw frame data to cflat sums
			for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
			{
				for (k = 0; k < NUM_PIX_IN_ROW; k++)
					cflatUSumsTop[j][k] += rawUDataTop[j][k];
			}
			for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
			{
				for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						cflatSums[i][j][k] += rawData[i][j][k];
				}
			}
			for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
			{
				for (k = 0; k < NUM_PIX_IN_ROW; k++)
					cflatUSumsBot[j][k] += rawUDataBot[j][k];
			}

//			increment sum counter and change state to average data if we have enough data
			if (++cflatCntr >= CflatAvgN)
				cflatState = eCmdStateAvg;
			break;

		case eCmdStateAvg:
//			average the cflat sums
			for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
			{
				for (k = 0; k < NUM_PIX_IN_ROW; k++)
					CflatUnusedTop[j][k] = (int16)(cflatUSumsTop[j][k] / cflatCntr);
			}
			for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
			{
				for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						CflatData[i][j][k] = (int16)(cflatSums[i][j][k] / cflatCntr);
				}
			}
			for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
			{
				for (k = 0; k < NUM_PIX_IN_ROW; k++)
					CflatUnusedBot[j][k] = (int16)(cflatUSumsBot[j][k] / cflatCntr);
			}

//			command the SRT to store the cflat data to disk
			FileDataPut(eFileTypeCflat);

//			set flags to indicate that command is not running and notify the host
			runCmd = eCmdNone;
			CentDataPtr->cmdCrnt = eCmdNone;
			SysConSendStrngNoParm("Notification: cflat complete");
			break;

		default:
			SysConSendStrngNoParm("Error: unexpected cflatState");
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CentCflatAbort()

UNIT DESCRIPTION:
The purpose of this unit is to abort the cflat command by simply setting the command flags
to indicate that no command is executing (since the new cflat data is stored in a temporary
buffer until the command completes, there is no additional cleanup to be done here).

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentCflatAbort( void )
{
	SysConSendStrngNoParm("Notification: cflat aborted");
	runCmd = eCmdNone;
	CentDataPtr->cmdCrnt = eCmdNone;
}


/**********************************************************************************************

UNIT NAME:  CentRefcentStart()

UNIT DESCRIPTION:
The purpose of this unit is to start the processing of the refcent command by initializing the
refcent sums (to be used to compute averages) and setting counters and flags necessary to run 
the command.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentRefcentStart( void )
{
	int16 i;

//	make sure we're not in centtest mode and report an error and return if we are
	if (CrntCentroider == eCentTest)
	{
		CentDataPtr->cmdCrnt = eCmdNone;
		SysConSendStrngNoParm("Error: can't run refcent in centtest mode");
		return;
	}

//	initialize refcent sums to zeroes
	for (i = 0; i < NUM_CENTROIDS; i++)
		refcentSums[i] = 0.0;

//	initialize sum counter and refcent state and set flag to indicate that the refcent command is running
	refcentCntr = 0;
	refcentState = eCmdStateInit;
	runCmd = eCmdCentRef;
}


/**********************************************************************************************

UNIT NAME:  CentRefcentProcess()

UNIT DESCRIPTION:
The purpose of this unit is to process the refcent command by receiving RefcentAvgN frames worth of
centroid data, averaging that data, and writing the result to a file when done.

Note that this will execute 2 frames longer than RefcentAvgN in order to initialize and to spread 
out the averaging and storing of the data.

UNIT INPUTS:
1.	centData[] - current centroid data

UNIT OUTPUTS:
1.	updates RefcentsB[], RefcentsC[], RefcentsQ[], or RefcentsO[] depending on CrntCentroider

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentRefcentProcess( float32 centData[] )
{
	int16 i;

//	initialize, sum, or average and store refcent data, depending on refcentState
	switch (refcentState)
	{
		case eCmdStateInit:
//			zero out the current reference centroids (so they won't influence determination 
//			> of the new reference centroids)
			switch (CrntCentroider)
			{
				case eCentBin:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsB[i] = 0.0;
					break;
				case eCentCM:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsC[i] = 0.0;
					break;
				case eCentQuad:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsQ[i] = 0.0;
					break;
				case eCentCorr:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsO[i] = 0.0;
					break;
			}
//			change state to start summing data (next time)
			refcentState = eCmdStateSum;
			break;

		case eCmdStateSum:
//			add current centroid data to refcent sums
			for (i = 0; i < NUM_CENTROIDS; i++)
				refcentSums[i] += centData[i];

//			increment sum counter and change state to average data if we have enough data
			if (++refcentCntr >= RefcentAvgN)
				refcentState = eCmdStateAvg;
			break;

		case eCmdStateAvg:
//			average the refcent sums
//			command the SRT to store the refcent data to disk
			switch (CrntCentroider)
			{
				case eCentBin:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsB[i] = refcentSums[i] / (float32)refcentCntr;
					FileDataPut(eFileTypeRefCentB);
					break;
				case eCentCM:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsC[i] = refcentSums[i] / (float32)refcentCntr;
					FileDataPut(eFileTypeRefCentC);
					break;
				case eCentQuad:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsQ[i] = refcentSums[i] / (float32)refcentCntr;
					FileDataPut(eFileTypeRefCentQ);
					break;
				case eCentCorr:
					for (i = 0; i < NUM_CENTROIDS; i++)
						RefcentsO[i] = refcentSums[i] / (float32)refcentCntr;
					FileDataPut(eFileTypeRefCentO);
					break;
			}

//			set flags to indicate that command is not running and notify the host
			runCmd = eCmdNone;
			CentDataPtr->cmdCrnt = eCmdNone;
			SysConSendStrngNoParm("Notification: refcent complete");
			break;

		default:
			SysConSendStrngNoParm("Error: unexpected refcentState");
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CentRefcentAbort()

UNIT DESCRIPTION:
The purpose of this unit is to abort the refcent command by simply setting the command flags
to indicate that no command is executing.  The new refcent data is stored in a temporary
buffer until the command completes.  However, the current reference centroids are zeroed out
when this command begins.  Therefore, if the refcent command is aborted, a cparms command
should be issued.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentRefcentAbort( void )
{
	SysConSendStrngNoParm("Notification: refcent aborted (issue a cparms command)");
	runCmd = eCmdNone;
	CentDataPtr->cmdCrnt = eCmdNone;
}


/**********************************************************************************************

UNIT NAME:  CentProcessBin()

UNIT DESCRIPTION:
The purpose of this unit is to:
1.	compute centroids, using the 4x4 binned approach, and 
2.	compute Tip/Tilt errors.

The 4x4 binned approach simply combines 4 pixels in each corner to produce a larger 'single'
pixel in each corner and then uses the quad-cell approach with the larger pixels (see the
definition of binnedDescs[] for a picture).

UNIT INPUTS:
1.	subApRow		- current row of sub-aps
2.	pixData[][]		- pixel data (i.e., raw data)
3.	centNdx			- current index into the centroid arrays

UNIT OUTPUTS:
1.	centNdx			- current index into the centroid arrays; if this is zero when returned, all sub-aps for
					  > current frame have been received and processed
2.	xCents[]		- current X centroids
3.	yCents[]		- current Y centroids
4.	xTTError		- X tip/tilt error (computed when all sub-ap pixels for current frame have been received)
5.	yTTError		- Y tip/tilt error (computed when all sub-ap pixels for current frame have been received)
6.	totalIntens[]	- total sub-ap intensities
7.	pixels[][]		- pixels used for centroid computations (needed for PSF)
7.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrRowsOrder	- sub-ap row being processed by centroid is out of order
2.	eErrNoErr		- no error

**********************************************************************************************/

static int16 CentProcessBin( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							 float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							 float32 totalIntens[], float32 pixels[][4] )
{
	int16 i, j, pixRow0, pixRow1, pixCol0, pixCol1;
	float32 a, b, c, d, totalIntensity;

//	make sure the sub-ap row is the one we're expecting and report an error if it's not
	if (binnedDescs[*centNdx].subApRow != subApRow)
	{
		SysConSendStrngIntParm("Error: sub-ap rows out of order in CentProcessBin(), new row = ", subApRow);
		return eErrRowsOrder;
	}

//	for each sub-ap in this row
//		compute a, b, c, d, and the total intensity for this sub-ap
//		compute the X and Y centroids for this sub-ap
	while (binnedDescs[*centNdx].subApRow == subApRow)
	{
		a = (float32)(-CentThresh);
		b = (float32)(-CentThresh);
		c = (float32)(-CentThresh);
		d = (float32)(-CentThresh);
		for (i = 0; i < 2; i++)
		{
			for (j = 0; j < 2; j++)
			{
				pixRow0 = binnedDescs[*centNdx].pixRows[i];
				pixRow1 = binnedDescs[*centNdx].pixRows[2 + i];
				pixCol0 = binnedDescs[*centNdx].pixCols[j];
				pixCol1 = binnedDescs[*centNdx].pixCols[2 + j];

				a += (float32)(pixData[pixRow0][pixCol0] - CflatData[subApRow][pixRow0][pixCol0]);
				b += (float32)(pixData[pixRow0][pixCol1] - CflatData[subApRow][pixRow0][pixCol1]);
				c += (float32)(pixData[pixRow1][pixCol0] - CflatData[subApRow][pixRow1][pixCol0]);
				d += (float32)(pixData[pixRow1][pixCol1] - CflatData[subApRow][pixRow1][pixCol1]);
			}
		}

		totalIntens[*centNdx] = a + b + c + d;  // for diagnostics

		if (a < 0.0)
			a = 0.0;
		if (b < 0.0)
			b = 0.0;
		if (c < 0.0)
			c = 0.0;
		if (d < 0.0)
			d = 0.0;

		pixels[*centNdx][0] = a;  // for PSF
		pixels[*centNdx][1] = b;  // for PSF
		pixels[*centNdx][2] = c;  // for PSF
		pixels[*centNdx][3] = d;  // for PSF

		totalIntensity = a + b + c + d;

		if (totalIntensity <= 0.0)
		{
			xCents[*centNdx] = 0.0;
			yCents[*centNdx] = 0.0;
		}
		else
		{
			xCents[*centNdx] = ((b + d) - (a + c)) / totalIntensity - 
                                RefcentsB[*centNdx] - Centoffs[*centNdx];
			yCents[*centNdx] = ((c + d) - (a + b)) / totalIntensity -
                                RefcentsB[NUM_SUBAPS + *centNdx] - Centoffs[NUM_SUBAPS + *centNdx];

			xCentsSums += xCents[*centNdx];
			yCentsSums += yCents[*centNdx];
		}

		(*centNdx)++;
	}

//	if all the sub-aps for the current frame have been received
//		compute X and Y tip/tilt errors
//		subract X and Y tip/tilt errors from X and Y centroids
	if (*centNdx == NUM_SUBAPS)
	{
		*centNdx = 0;

		*xTTError = xCentsSums / NUM_SUBAPS;
		*yTTError = yCentsSums / NUM_SUBAPS;

		xCentsSums = 0.0;
		yCentsSums = 0.0;

		if (CrntCentroider != eCentTest)  //CCCTTT
		{  //CCCTTT
			for (i = 0; i < NUM_SUBAPS; i++)
			{
				xCents[i] -= *xTTError;
				yCents[i] -= *yTTError;
			}
		}  //CCCTTT
	}

	return eErrNoErr;
}


/**********************************************************************************************

UNIT NAME:  CentProcessCM()

UNIT DESCRIPTION:
The purpose of this unit is to:
1.	compute centroids, using the center of mass approach, and 
2.	compute Tip/Tilt errors.

IMPORTANT: Note that CentProcessCM() does not support PSF estimation at present.

UNIT INPUTS:
1.	subApRow		- current row of sub-aps
2.	pixData[][]		- pixel data (i.e., raw data)
3.	centNdx			- current index into the centroid arrays

UNIT OUTPUTS:
1.	centNdx			- current index into the centroid arrays; if this is zero when returned, all sub-aps for
					  > current frame have been received and processed
2.	xCents[]		- current X centroids
3.	yCents[]		- current Y centroids
4.	xTTError		- X tip/tilt error (computed when all sub-ap pixels for current frame have been received)
5.	yTTError		- Y tip/tilt error (computed when all sub-ap pixels for current frame have been received)
6.	totalIntens[]	- total sub-ap intensities
7.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrRowsOrder	- sub-ap row being processed by centroid is out of order
2.	eErrNoErr		- no error

**********************************************************************************************/

static int16 CentProcessCM( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							float32 totalIntens[] )
{
	int16 i, j, pixRow, pixCol;
	float32 tmpPix[4][4], rows[4], cols[4];

//	make sure the sub-ap row is the one we're expecting and report an error if it's not
	if (centMassDescs[*centNdx].subApRow != subApRow)
	{
		SysConSendStrngIntParm("Error: sub-ap rows out of order in CentProcessCM(), new row = ", subApRow);
		return eErrRowsOrder;
	}

//	for each sub-ap in this row
//		compute the rows and columns and the total intensity for this sub-ap
//		compute the X and Y center of mass for this sub-ap
	while (centMassDescs[*centNdx].subApRow == subApRow)
	{
		totalIntens[*centNdx] = 0.0;
		for (i = 0; i < 4; i++)
		{
			cols[i] = 0.0;
			rows[i] = 0.0;

			pixRow = centMassDescs[*centNdx].pixRows[i];
			for (j = 0; j < 4; j++)
			{
				pixCol = centMassDescs[*centNdx].pixCols[j];
				tmpPix[i][j] = (float32)(pixData[pixRow][pixCol] - CflatData[subApRow][pixRow][pixCol]) - CentThresh;
				totalIntens[*centNdx] += tmpPix[i][j];
			}
		}

		for (i = 0; i < 4; i++)
		{
			for (j = 0; j < 4; j++)
			{
				cols[i] += tmpPix[j][i];
				rows[i] += tmpPix[i][j];
			}
		}

		xCents[*centNdx] = 0.0;
		yCents[*centNdx] = 0.0;
		for (i = 0; i < 4; i++)
		{
			if (cols[i] > 0.0)
				xCents[*centNdx] += cols[i] * centCMWeightsX[i];
			if (rows[i] > 0.0)
				yCents[*centNdx] += rows[i] * centCMWeightsY[i];
		}

		if (totalIntens[*centNdx] <= 0.0)
		{
			xCents[*centNdx] = 0.0;
			yCents[*centNdx] = 0.0;
		}
		else
		{
			xCents[*centNdx] = xCents[*centNdx] / totalIntens[*centNdx] / centCMWeightsX[3] - 
								   RefcentsC[*centNdx] - Centoffs[*centNdx];
			yCents[*centNdx] = yCents[*centNdx] / totalIntens[*centNdx] / centCMWeightsY[3] - 
								   RefcentsC[NUM_SUBAPS + *centNdx] - Centoffs[NUM_SUBAPS + *centNdx];

			xCentsSums += xCents[*centNdx];
			yCentsSums += yCents[*centNdx];
		}

		(*centNdx)++;
	}

//	if all the sub-aps for the current frame have been received
//		compute X and Y tip/tilt errors
//		subract X and Y tip/tilt errors from X and Y centroids
	if (*centNdx == NUM_SUBAPS)
	{
		*centNdx = 0;

		*xTTError = xCentsSums / NUM_SUBAPS;
		*yTTError = yCentsSums / NUM_SUBAPS;

		xCentsSums = 0.0;
		yCentsSums = 0.0;

		if (CrntCentroider != eCentTest)  //CCCTTT
		{  //CCCTTT
			for (i = 0; i < NUM_SUBAPS; i++)
			{
				xCents[i] -= *xTTError;
				yCents[i] -= *yTTError;
			}
		}  //CCCTTT
	}

	return eErrNoErr;
}


/**********************************************************************************************

UNIT NAME:  CentProcessQuad()

UNIT DESCRIPTION:
The purpose of this unit is to:
1.	compute centroids, using the quad-cell approach, and 
2.	compute Tip/Tilt errors.

UNIT INPUTS:
1.	subApRow		- current row of sub-aps
2.	pixData[][]		- pixel data (i.e., raw data)
3.	centNdx			- current index into the centroid arrays

UNIT OUTPUTS:
1.	centNdx			- current index into the centroid arrays; if this is zero when returned, all sub-aps for
					  > current frame have been received and processed
2.	xCents[]		- current X centroids
3.	yCents[]		- current Y centroids
4.	xTTError		- X tip/tilt error (computed when all sub-ap pixels for current frame have been received)
5.	yTTError		- Y tip/tilt error (computed when all sub-ap pixels for current frame have been received)
6.	totalIntens[]	- total sub-ap intensities
7.	pixels[][]		- pixels used for centroid computations (needed for PSF)
7.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrRowsOrder	- sub-ap row being processed by centroid is out of order
2.	eErrNoErr		- no error

**********************************************************************************************/

static int16 CentProcessQuad( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							  float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							  float32 totalIntens[], float32 pixels[][4] )
{
	int16 i, pixRow0, pixRow1, pixCol0, pixCol1;
	float32 a, b, c, d, totalIntensity;
	
//	make sure the sub-ap row is the one we're expecting and report an error if it's not
	if (quadCellDescs[*centNdx].subApRow != subApRow)
	{
		SysConSendStrngIntParm("Error: sub-ap rows out of order in CentProcessQuad(), new row = ", subApRow);
		return eErrRowsOrder;
	}

//	for each sub-ap in this row
//		compute a, b, c, d, and the total intensity for this sub-ap
//		compute the X and Y centroids for this sub-ap
	while (quadCellDescs[*centNdx].subApRow == subApRow)
	{
		pixRow0 = quadCellDescs[*centNdx].pixRows[0];
		pixRow1 = quadCellDescs[*centNdx].pixRows[1];
		pixCol0 = quadCellDescs[*centNdx].pixCols[0];
		pixCol1 = quadCellDescs[*centNdx].pixCols[1];

		a = (float32)(pixData[pixRow0][pixCol0] - CflatData[subApRow][pixRow0][pixCol0] - CentThresh);
		b = (float32)(pixData[pixRow0][pixCol1] - CflatData[subApRow][pixRow0][pixCol1] - CentThresh);
		c = (float32)(pixData[pixRow1][pixCol0] - CflatData[subApRow][pixRow1][pixCol0] - CentThresh);
		d = (float32)(pixData[pixRow1][pixCol1] - CflatData[subApRow][pixRow1][pixCol1] - CentThresh);

		totalIntens[*centNdx] = a + b + c + d;  // for diagnostics

		if (a < 0.0) 
			a = 0.0;
		if (b < 0.0)
			b = 0.0;
		if (c < 0.0)
			c = 0.0;
		if (d < 0.0)
			d = 0.0;

		pixels[*centNdx][0] = a;  // for PSF
		pixels[*centNdx][1] = b;  // for PSF
		pixels[*centNdx][2] = c;  // for PSF
		pixels[*centNdx][3] = d;  // for PSF

		totalIntensity = a + b + c + d;

		if (totalIntensity <= 0.0)
		{
			xCents[*centNdx] = 0.0;
			yCents[*centNdx] = 0.0;
		}
		else
		{
			xCents[*centNdx] = ((b + d) - (a + c)) / totalIntensity - 
                                RefcentsQ[*centNdx] - Centoffs[*centNdx];
			yCents[*centNdx] = ((c + d) - (a + b)) / totalIntensity -
                                RefcentsQ[NUM_SUBAPS + *centNdx] - Centoffs[NUM_SUBAPS + *centNdx];

			xCentsSums += xCents[*centNdx];
			yCentsSums += yCents[*centNdx];
		}

		(*centNdx)++;
	}

//	if all the sub-aps for the current frame have been received
//		compute X and Y tip/tilt errors
//		subract X and Y tip/tilt errors from X and Y centroids
	if (*centNdx == NUM_SUBAPS)
	{
		*centNdx = 0;

		*xTTError = xCentsSums / NUM_SUBAPS;
		*yTTError = yCentsSums / NUM_SUBAPS;

		xCentsSums = 0.0;
		yCentsSums = 0.0;

		if (CrntCentroider != eCentTest)  //CCCTTT
		{  //CCCTTT
			for (i = 0; i < NUM_SUBAPS; i++)
			{
				xCents[i] -= *xTTError;
				yCents[i] -= *yTTError;
			}
		}  //CCCTTT
	}
	
	return eErrNoErr;
}


/**********************************************************************************************

UNIT NAME:  CentProcessCorr()

UNIT DESCRIPTION:
The purpose of this unit is to:
1.	compute centroids, using the correlation algorithm, and 
2.	compute Tip/Tilt errors.

Note:	centtest mode (marked by either centtest or CCCTTT) is a hybrid mode that causes the centbin Y centroids
		to be put in the Y data slots and the Y centcorr centroids to be put in the X data slots.  Also, tip/tilt
		data is not taken out of either set of centoid data.  Needless to say, loops cannot be closed in this mode.

IMPORTANT: Note that CentProcessCorr() does not support PSF estimation at present.

UNIT INPUTS:
1.	subApRow		- current row of sub-aps
2.	pixData[][]		- pixel data (i.e., raw data)
3.	centNdx			- current index into the centroid arrays

UNIT OUTPUTS:
1.	centNdx			- current index into the centroid arrays; if this is zero when returned, all sub-aps for
					  > current frame have been received and processed
2.	xCents[]		- current X centroids
3.	yCents[]		- current Y centroids
4.	xTTError		- X tip/tilt error (computed when all sub-ap pixels for current frame have been received)
5.	yTTError		- Y tip/tilt error (computed when all sub-ap pixels for current frame have been received)
6.	totalIntens[]	- total sub-ap intensities
7.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrRowsOrder	- sub-ap row being processed by centroid is out of order
2.	eErrNoErr		- no error

**********************************************************************************************/

static int16 CentProcessCorr( int16 subApRow, int16 pixData[][NUM_PIX_IN_ROW], int16 *centNdx, 
							  float32 xCents[], float32 yCents[], float32 *xTTError, float32 *yTTError, 
							  float32 totalIntens[] )
{
	int16 i, j, pixRow, pixCol;
	float32 denom;

	float32 corr00, corr0p, corr0m, corrp0, corrm0;  // p and m stand for plus one and minus one

	float32 s[4][4];  // pixels corresponding to s[][] are arranged as follows:
					  // >		s00  s01  s02  s03
					  // >		s10  s11  s12  s13
					  // >		s20  s21  s22  s23
					  // >		s30  s31  s32  s33

	static const float32 corrX = 1.76;  // these coefficients are applied to pixels as follows:
	static const float32 corrY = 0.91;  // >	Z Y Y Z
	static const float32 corrZ = 0.27;  // >	Y X X Y
										// >	Y X X Y
										// >	Z Y Y Z

//	make sure the sub-ap row is the one we're expecting and report an error if it's not
	if (centMassDescs[*centNdx].subApRow != subApRow)
	{
		SysConSendStrngIntParm("Error: sub-ap rows out of order in CentProcessCM(), new row = ", subApRow);
		return eErrRowsOrder;
	}

//	for each sub-ap in this row
	while (centMassDescs[*centNdx].subApRow == subApRow)
	{
//		move pixels into a more easily accessible array, subtracting cflat and thresh along the way
		totalIntens[*centNdx] = 0.0;  // for diagnostics only, in this case
		for (i = 0; i < 4; i++)
		{
			pixRow = centMassDescs[*centNdx].pixRows[i];
			for (j = 0; j < 4; j++)
			{
				pixCol = centMassDescs[*centNdx].pixCols[j];
				s[i][j] = (float32)(pixData[pixRow][pixCol] - CflatData[subApRow][pixRow][pixCol]) - CentThresh;
				totalIntens[*centNdx] += s[i][j];
			}
		}

//		perform the correlations
		corr00 = corrX * (s[1][1] + s[1][2] + s[2][1] + s[2][2]) +
				 corrY * (s[0][1] + s[0][2] + s[1][0] + s[1][3] + s[2][0] + s[2][3] + s[3][1] + s[3][2]) + 
				 corrZ * (s[0][0] + s[0][3] + s[3][0] + s[3][3]);

		corr0p = corrX * (s[1][2] + s[1][3] + s[2][2] + s[2][3]) +
				 corrY * (s[0][2] + s[0][3] + s[1][1] + s[2][1] + s[3][2] + s[3][3]) + 
				 corrZ * (s[0][1] + s[3][1]);

		corr0m = corrX * (s[1][0] + s[1][1] + s[2][0] + s[2][1]) +
				 corrY * (s[0][0] + s[0][1] + s[1][2] + s[2][2] + s[3][0] + s[3][1]) + 
				 corrZ * (s[0][2] + s[3][2]);

		corrp0 = corrX * (s[2][1] + s[2][2] + s[3][1] + s[3][2]) +
				 corrY * (s[1][1] + s[1][2] + s[2][0] + s[2][3] + s[3][0] + s[3][3]) + 
				 corrZ * (s[1][0] + s[1][3]);

		corrm0 = corrX * (s[0][1] + s[0][2] + s[1][1] + s[1][2]) +
				 corrY * (s[0][0] + s[0][3] + s[1][0] + s[1][3] + s[2][1] + s[2][2]) + 
				 corrZ * (s[2][0] + s[2][3]);

//		perform interpolations and make sure we haven't railed out
//CCCTTT start
		if (CrntCentroider == eCentTest)
		{
			denom = corr00 + corr00 - (corrp0 + corrm0);
			if (denom == 0.0)
				xCents[*centNdx] = 0.0;
			else
			{
				xCents[*centNdx] = (corrp0 - corrm0) / denom - 
								    RefcentsO[NUM_SUBAPS + *centNdx] - Centoffs[NUM_SUBAPS + *centNdx];

				if (xCents[*centNdx] < -1.0)
					xCents[*centNdx] = -1.0;
				else if (xCents[*centNdx] > 1.0)
					xCents[*centNdx] = 1.0;

				xCentsSums += xCents[*centNdx];
			}
		}
		else
		{
//CCCTTT end
			denom = corr00 + corr00 - (corr0p + corr0m);
			if (denom == 0.0)
				xCents[*centNdx] = 0.0;
			else
			{
				xCents[*centNdx] = (corr0p - corr0m) / denom - 
								    RefcentsO[*centNdx] - Centoffs[*centNdx];

				if (xCents[*centNdx] < -1.0)
					xCents[*centNdx] = -1.0;
				else if (xCents[*centNdx] > 1.0)
					xCents[*centNdx] = 1.0;

				xCentsSums += xCents[*centNdx];
			}

			denom = corr00 + corr00 - (corrp0 + corrm0);
			if (denom == 0.0)
				yCents[*centNdx] = 0.0;
			else
			{
				yCents[*centNdx] = (corrp0 - corrm0) / denom - 
								    RefcentsO[NUM_SUBAPS + *centNdx] - Centoffs[NUM_SUBAPS + *centNdx];

				if (yCents[*centNdx] < -1.0)
					yCents[*centNdx] = -1.0;
				else if (yCents[*centNdx] > 1.0)
					yCents[*centNdx] = 1.0;

				yCentsSums += yCents[*centNdx];
			}
		}  //CCCTTT

		(*centNdx)++;
	}

//	if all the sub-aps for the current frame have been received
//		compute X and Y tip/tilt errors
//		subract X and Y tip/tilt errors from X and Y centroids
	if (*centNdx == NUM_SUBAPS)
	{
		*centNdx = 0;

		*xTTError = xCentsSums / NUM_SUBAPS;
		*yTTError = yCentsSums / NUM_SUBAPS;

		xCentsSums = 0.0;
		yCentsSums = 0.0;

		if (CrntCentroider != eCentTest)  //CCCTTT
		{  //CCCTTT
			for (i = 0; i < NUM_SUBAPS; i++)
			{
				xCents[i] -= *xTTError;
				yCents[i] -= *yTTError;
			}
		}  //CCCTTT
	}

	return eErrNoErr;
}


/**********************************************************************************************

UNIT NAME:  CentDitherExtract()

UNIT DESCRIPTION:
The purpose of this unit is to extract the cosine wave used to dither the tip/tilt mirror.  This
is done for each centroid, using a matched filter.  At present, 512 cycles of an 8 element cosine
curve are being used.

Note that, for now, for test purposes, when dithering is enabled, the centroid intensity values 
are over-written with the extracted cosine curves for the X centroids.

UNIT INPUTS:
1.	centroids[] - current X and Y centroids

UNIT OUTPUTS:
1.	xResults[] - extracted cosine curves for the X centroids (for now, for testing)

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CentDitherExtract( float32 centroids[], float32 xResults[] )
{
	int16 i, j;
	int32 newDataProductInt32;
	float32 newDataProduct;
	volatile float32 dummy;  // for testing

//	spend several cycles initializing data, if we're in the mode (so as not to init ~1.3 megs at one time)
	if (ttDitherInitFlag)
	{
		for (i = 0; i < DITHER_NUM_CYCLES; i++)
		{
			for (j = 0; j < DITHER_COS_LNGTH; j++)
				ttDitherOldData[ttDitherInitNdx][i][j] = 0;
		}
		for (j = 0; j < DITHER_COS_LNGTH; j++)
		{
			ttDitherNewData[ttDitherInitNdx][j] = 0.0;
			ttDitherSums[ttDitherInitNdx][j] = 0;
		}
		if (++ttDitherInitNdx >= NUM_CENTROIDS)
			ttDitherInitFlag = false;

		return;
	}

//	for each centroid
	for (i = 0; i < NUM_CENTROIDS; i++)
	{
//		put new data element into new data array
		ttDitherNewData[i][ttDitherNewDataNdx] = centroids[i];

//		multiply new data array by appropriate cosine curve
		newDataProduct = 0.0;
		for (j = 0; j < DITHER_COS_LNGTH; j++)
			newDataProduct += ttDitherNewData[i][j] * TTDitherCosCurves[ttDitherNewDataNdx][j];
		newDataProductInt32 = (int32)(newDataProduct * 100000.0);

//		subtract old data from sum
		ttDitherSums[i][ttDitherNewDataNdx] -= ttDitherOldData[i][ttDitherOldDataNdx][ttDitherNewDataNdx];

//		put new data array multiplied by cosine curve into old data and add it to sum
		ttDitherOldData[i][ttDitherOldDataNdx][ttDitherNewDataNdx] = newDataProductInt32;
		ttDitherSums[i][ttDitherNewDataNdx] += newDataProductInt32;

//		compute the filter output for this point in time
		if (i < NUM_SUBAPS)  // for testing
			xResults[i] = (float32)((float64)ttDitherSums[i][ttDitherNewDataNdx] / 100000.0 * ttDitherConst);
		else
			dummy = (float32)((float64)ttDitherSums[i][ttDitherNewDataNdx] / 100000.0 * ttDitherConst);
	}

//	update indexes
	if (--ttDitherNewDataNdx < 0)
	{
		ttDitherNewDataNdx = DITHER_COS_LNGTH - 1;

		if (++ttDitherOldDataNdx >= DITHER_NUM_CYCLES)
			ttDitherOldDataNdx = 0;
	}
}

