/******************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			cam_WFS_Mngr.h

AUTHOR:	DHL			DATE: 10/7/04

FILE DESCRIPTION:
This is the header file for the SciMeasure WFS camera manager component.

******************************************************************************/

#ifndef _CAMERA_WFS_MNGR_H
#define _CAMERA_WFS_MNGR_H		1


// Defines
#define WFS_UNIT_NUM 1	// the unit number used by the EDT driver  //XXXWFSTEST
//XXXWFSTEST #define WFS_UNIT_NUM 0	// the unit number used by the EDT driver


// Defines specific to the DMA ring buffers.  
#define WFSnumbufs	8					   // number of DMA ring buffers
/*XXX48x48*/ #define WFSbufsize (48 * 48 * 2 * 2)	   // don't know why the second multiply by 2 but this was the way it was done in the example code rt_take
#define WFSbufsize64x64 (64 * 64 * 2 * 2)  // IMPORTANT: pdv_multibuf_block() can't seem to handle it when memory
										   // > is allocated for 12k buffers; so, we're allocating for 16k
//*XXX64x64*/ #define WFSbufsize (64 * 64 * 2 * 2)	// don't know why the second multiply by 2 but this was the way it was done in the example code rt_take
#define MAXNUMBUFS 1024					   // size of the array that holds the DMA buffer addresses


// Camera control commands from HRT camWFS
enum
{
	eWFSInitialize1,	// initialize the EDT camera interface, 1
	eWFSInitialize2,	// initialize the EDT camera interface, 2
	eWFSSetRep,			// set the Little Joe repetition count
	eWFSSetAtten,		// set the Little Joe attenuation
	eWFSReadRep,		// read and display the REP as a frame rate
	eWFSReadAtten		// read and display the attenuation as a gain
};


// Typedefs

// Global Function Prototypes

void CamWFSMngr_ProcCmd (int16 cmd, int16 param);
void CamWFSMngr_StartCam(void);
void CamWFSMngr_StopCam(void);

#endif
