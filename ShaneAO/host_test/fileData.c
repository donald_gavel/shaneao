/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		fileData

AUTHOR:	DWP			DATE: 3/12/03

COMPONENT DESCRIPTION:
This component contains functions to put and get data to and from shared memory and to command
the SRT side to read and write files as necessary.

IMPORTANT: Although these functions are in a separate file, they are called from and, therefore,
		   run as part of other threads on the HRT side.

COMPONENT UNITS:
1.	FileDataInit()
2.	FileDataCmd()
3.	FileDataGet()
4.	FileDataPut()

REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_time.h>

#include "types.h"
#include "constant.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"


// Global Variables


// Local Variables


// Local Function Prototypes


/**********************************************************************************************

UNIT NAME:  FileDataInit()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the HRT-side file data handler.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void FileDataInit( void )
{
	int16 i, j, k;

//	initialize shared memory control variables
	FileDataShmPtr->allDataAvailFromFile = false;
	FileDataShmPtr->someDataAvailFromFile = false;

	for (i = 0; i < eFileTypeNum; i++)
	{
		FileDataShmPtr->dataAvailFromFile[i] = false;
		FileDataShmPtr->dataShmLockHRT[i] = false;
		FileDataShmPtr->dataShmLockSRT[i] = false;
	}

//	initialize shared memory data variables (almost all of these should be overwritten by file data)

//	parms data
	for (i = 0; i < eFileTypeNum; i++)
		FileDataShmPtr->crntFilenames[i][0] = '\0';

	FileDataShmPtr->cflatAvgN = INIT_CFLAT_AVGN;
	CflatAvgN = INIT_CFLAT_AVGN;
	FileDataShmPtr->refcentAvgN = INIT_REFCENT_AVGN;
	RefcentAvgN = INIT_REFCENT_AVGN;
	FileDataShmPtr->cmAvgN = INIT_CM_AVGN;
	CMAvgN = INIT_CM_AVGN;
	FileDataShmPtr->flatSharpAvgN = INIT_FLAT_SHARP_AVGN;
	FlatSharpAvgN = INIT_FLAT_SHARP_AVGN;

	FileDataShmPtr->gsMode = INIT_GS_MODE;
	GSMode = INIT_GS_MODE;

	FileDataShmPtr->camFrameRate = INIT_FRAME_RATE;
	CamFrameRate = INIT_FRAME_RATE;
	CamFrameRateNdx = 0;
	while ((CamFrameRate > LegalFrameRates[CamFrameRateNdx]) && (LegalFrameRates[CamFrameRateNdx + 1] != -1))
		CamFrameRateNdx ++;
	CamFrameRate = LegalFrameRates[CamFrameRateNdx];
	SysConSendStrngIntParm("Notification: camera being set to rate: ", CamFrameRate);
	FileDataShmPtr->camGain = INIT_CAM_GAIN;
	CamGain = INIT_CAM_GAIN;
	FileDataShmPtr->camCRTGain = INIT_CRT_GAIN;
	CamCRTGain = INIT_CRT_GAIN;
	CamDataPtr->RS170crtGain = CamCRTGain;  //XXXRS170

	FileDataShmPtr->crntCentroider = INIT_CRNT_CENTROIDER;
	CrntCentroider = INIT_CRNT_CENTROIDER;
	FileDataShmPtr->centThresh = INIT_CENT_THRESH;
	CentThresh = INIT_CENT_THRESH;

	FileDataShmPtr->reconGain = INIT_RECON_GAIN;
	ReconGain = INIT_RECON_GAIN;
	FileDataShmPtr->reconInt = INIT_RECON_INT;
	ReconInt = INIT_RECON_INT;
	FileDataShmPtr->reconCwt1 = INIT_RECON_CWT1;
	ReconCwt1 = INIT_RECON_CWT1;
	FileDataShmPtr->reconCwt2 = INIT_RECON_CWT2;
	ReconCwt2 = INIT_RECON_CWT2;

	FileDataShmPtr->ttGain = INIT_TT_GAIN;
	TTGain = INIT_TT_GAIN;

	FileDataShmPtr->apdRate = INIT_APD_RATE;
	APDRate = INIT_APD_RATE;
	FileDataShmPtr->apdGain = INIT_APD_GAIN;
	APDGain = INIT_APD_GAIN;
	for (i = 0; i < NUM_APDS; i++)
	{
		FileDataShmPtr->apdBack[i] = INIT_APD_BACK;
		APDBack[i] = INIT_APD_BACK;
	}

	for (i = 0; i < NUM_DAC_BOARDS; i++)
		FileDataShmPtr->dacSNs[i] = -1;

	FileDataShmPtr->LttFrameRate = 50;
	LttFrameRate = 50;
	FileDataShmPtr->LttCamGain = 1;
	LttCamGain = 1;
	FileDataShmPtr->LttFov = 2;
	LttFov =2;
	FileDataShmPtr->UseAPD = 0;
	UseAPD = 0;
	FileDataShmPtr->UseAOA = INIT_USE_AOA;
	UseAOA = INIT_USE_AOA;

//	cflat data
	for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
	{
		for (k = 0; k < NUM_PIX_IN_ROW; k++)
		{
			FileDataShmPtr->cflatUnusedTop[j][k] = 0;
			CflatUnusedTop[j][k] = 0;
		}
	}
	for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
	{
		for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
		{
			for (k = 0; k < NUM_PIX_IN_ROW; k++)
			{
				FileDataShmPtr->cflatData[i][j][k] = 0;
				CflatData[i][j][k] = 0;
			}
		}
	}
	for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
	{
		for (k = 0; k < NUM_PIX_IN_ROW; k++)
		{
			FileDataShmPtr->cflatUnusedBot[j][k] = 0;
			CflatUnusedBot[j][k] = 0;
		}
	}

//	LTT open loop background data
	for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
	{
		for (j = 0; j < OPEN_PIX_IN_ROW; j++)
		{
			FileDataShmPtr->LTTOpenBackground[i][j] = 0;
		}
	}

//	LTT closed loop background data
	for (i = 0; i < (CLOSED_PIX_IN_ROW * CLOSED_ROWS_IN_CAM); i++)
		LTTClosedBackground[i] = 0;

//	refcent data
//	centoff data
	for (i = 0; i < NUM_CENTROIDS; i++)
	{
		FileDataShmPtr->refcentsB[i] = 0.0;
		RefcentsB[i] = 0.0;
		FileDataShmPtr->refcentsC[i] = 0.0;
		RefcentsC[i] = 0.0;
		FileDataShmPtr->refcentsQ[i] = 0.0;
		RefcentsQ[i] = 0.0;
		FileDataShmPtr->refcentsO[i] = 0.0;
		RefcentsO[i] = 0.0;
		FileDataShmPtr->centoffs[i] = 0.0;
		Centoffs[i] = 0.0;
	}

//	cm data
	FileDataShmPtr->cmDataFileError = false;
	for (i = 0; i < NUM_ACTV_ACTS; i++)
	{
		for (j = 0; j < NUM_CENTROIDS; j++)
		{
			FileDataShmPtr->controlMatrix[i][j] = 1.0;
			ControlMatrix[i][j] = 1.0;
		}
	}

//	flat data
//	sharp data
	for (i = 0; i < NUM_ACTV_ACTS; i++)
	{
		FileDataShmPtr->flat[i] = 0.0;
		Flat[i] = 0.0;
		FileDataShmPtr->sharp[i] = 0.0;
		Sharp[i] = 0.0;
	}
}


/**********************************************************************************************

UNIT NAME:  FileDataCmd()

UNIT DESCRIPTION:
The purpose of this unit is to send commands to the SRT to read and write files into and from 
shared memory.  Note that this is the only function in this code that must be re-entrant.

UNIT INPUTS:
1.	readCmd		indicates to the SRT side which files should be read

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void FileDataCmd( int16 cmd )
{
	diagFIFOMsgType diagFIFOMsg;

//	set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagCmdFile;
	diagFIFOMsg.cmdNdx = cmd;
    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm("Error writing file command to diag FIFO, errno = ", rtl_errno);
}


/**********************************************************************************************

UNIT NAME: FileDataGet()

UNIT DESCRIPTION:
The purpose of this unit is to check whether there is new parameter file data in shared memory
and, if there is, move it to program variables.

UNIT INPUTS:
1.	fileType	indicates file type (eFileTypeParms, eFileTypeCflat, etc.)

UNIT OUTPUTS:
	boolean return value - indicates whether new data has been moved to shared memory

UNIT ERROR CODES:
	None

**********************************************************************************************/

boolean FileDataGet( int16 fileType )
{
	int16 i, j, k;

//	if there is new file data in shared memory
	if (FileDataShmPtr->dataAvailFromFile[fileType])
	{
//		move it to program variables
		switch (fileType)
		{
			case eFileTypeParms:    // parameter data
				CflatAvgN = FileDataShmPtr->cflatAvgN;
				RefcentAvgN = FileDataShmPtr->refcentAvgN;
				CMAvgN = FileDataShmPtr->cmAvgN;
				FlatSharpAvgN = FileDataShmPtr->flatSharpAvgN;

				GSMode = FileDataShmPtr->gsMode;

				CamFrameRate = FileDataShmPtr->camFrameRate;
				CamFrameRateNdx = 0;
				while ((CamFrameRate > LegalFrameRates[CamFrameRateNdx]) && (LegalFrameRates[CamFrameRateNdx + 1] != -1))
					CamFrameRateNdx ++;
				CamFrameRate = LegalFrameRates[CamFrameRateNdx];
				SysConSendStrngIntParm("Notification: camera being set to rate: ", CamFrameRate);
				CamGain = FileDataShmPtr->camGain;
				CamCRTGain = FileDataShmPtr->camCRTGain;
				CamDataPtr->RS170crtGain = CamCRTGain;  //XXXRS170

				CrntCentroider = FileDataShmPtr->crntCentroider;
				CentThresh = FileDataShmPtr->centThresh;

				ReconGain = FileDataShmPtr->reconGain;
				ReconInt = FileDataShmPtr->reconInt;
				ReconCwt1 = FileDataShmPtr->reconCwt1;
				ReconCwt2 = FileDataShmPtr->reconCwt2;

				TTGain = FileDataShmPtr->ttGain;

				APDRate = FileDataShmPtr->apdRate;
				APDGain = FileDataShmPtr->apdGain;
				for (i = 0; i < NUM_APDS; i++)
					APDBack[i] = FileDataShmPtr->apdBack[i];

				LttFrameRate = FileDataShmPtr->LttFrameRate;
				LttCamGain = FileDataShmPtr->LttCamGain;
				LttFov = FileDataShmPtr->LttFov;
				UseAPD = FileDataShmPtr->UseAPD;
				UseAOA = FileDataShmPtr->UseAOA;

				SysConSendStrngNoParm("Notification: done reading parms file");
				break;

			case eFileTypeCflat:    // cflat data
				for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						CflatUnusedTop[j][k] = FileDataShmPtr->cflatUnusedTop[j][k];
				}
				for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
				{
					for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
					{
						for (k = 0; k < NUM_PIX_IN_ROW; k++)
							CflatData[i][j][k] = FileDataShmPtr->cflatData[i][j][k];
					}
				}
				for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						CflatUnusedBot[j][k] = FileDataShmPtr->cflatUnusedBot[j][k];
				}
				SysConSendStrngNoParm("Notification: done reading cflat file");
				break;

			case eFileTypeRefCentB:  // reference centroid, 4x4 binned
				for (i = 0; i < NUM_CENTROIDS; i++)
					RefcentsB[i] = FileDataShmPtr->refcentsB[i];
				SysConSendStrngNoParm("Notification: done reading refcentB file");
				break;

			case eFileTypeRefCentC:  // reference centroid, center-of-mass
				for (i = 0; i < NUM_CENTROIDS; i++)
					RefcentsC[i] = FileDataShmPtr->refcentsC[i];
				SysConSendStrngNoParm("Notification: done reading refcentC file");
				break;

			case eFileTypeRefCentQ:  // reference centroid, quad-cell
				for (i = 0; i < NUM_CENTROIDS; i++)
					RefcentsQ[i] = FileDataShmPtr->refcentsQ[i];
				SysConSendStrngNoParm("Notification: done reading refcentQ file");
				break;

			case eFileTypeRefCentO:  // reference centroid, correlator
				for (i = 0; i < NUM_CENTROIDS; i++)
					RefcentsO[i] = FileDataShmPtr->refcentsO[i];
				SysConSendStrngNoParm("Notification: done reading refcentO file");
				break;

			case eFileTypeCentOff:  // centroid offset data
				for (i = 0; i < NUM_CENTROIDS; i++)
					Centoffs[i] = FileDataShmPtr->centoffs[i];
				SysConSendStrngNoParm("Notification: done reading centoff file");
				break;

			case eFileTypeCM:       // control matrix data
				for (i = 0; i < NUM_ACTV_ACTS; i++)
				{
					for (j = 0; j < NUM_CENTROIDS; j++)
						ControlMatrix[i][j] = FileDataShmPtr->controlMatrix[i][j];
				}
				SysConSendStrngNoParm("Notification: done reading cm file");
				break;

			case eFileTypeFlat:     // DM flat voltage data
				for (i = 0; i < NUM_ACTV_ACTS; i++)
					Flat[i] = FileDataShmPtr->flat[i];
				SysConSendStrngNoParm("Notification: done reading flat file");
				break;

			case eFileTypeSharp:    // DM sharp (image sharpened) voltage data
				for (i = 0; i < NUM_ACTV_ACTS; i++)
					Sharp[i] = FileDataShmPtr->sharp[i];
				SysConSendStrngNoParm("Notification: done reading sharp file");
				break;

			case eFileTypeLTTOpen:    // Laser tilt/tip background data for open loop
				for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
				{
					for (j = 0; j < OPEN_PIX_IN_ROW; j++)
						LTTOpenBackground[i][j] = FileDataShmPtr->LTTOpenBackground[i][j];
				}
				SysConSendStrngNoParm("Notification: done reading LTT open background file");
				break;

			case eFileTypeLTTClosed:    // Laser tilt/tip background data for closed loop
				for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
					LTTClosedBackground[i] = FileDataShmPtr->LTTClosedBackground[i];
				SysConSendStrngNoParm("Notification: done reading LTT closed background file");
				break;

			default:
				SysConSendStrngIntParm("Error: unexpected file type in FileDataGet(), ", fileType);
				break;
		}

		FileDataShmPtr->dataAvailFromFile[fileType] = false;
		return true;
	}

	return false;
}


/**********************************************************************************************

UNIT NAME: FileDataPut()

UNIT DESCRIPTION:
The purpose of this unit is to move data from program variables to shared memory and command 
the SRT to write the data to file

UNIT INPUTS:
1.	fileType	indicates file type (eFileTypeParms, eFileTypeCflat, etc.)

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void FileDataPut( int16 fileType )
{
	int16 i, j, k;

//	try to lock shared memory and report an error if it's already locked
	FileDataShmPtr->dataShmLockHRT[fileType] = true;
	if (FileDataShmPtr->dataShmLockSRT[fileType])
        SysConSendStrngIntParm("Error: HRT attempting to write to shared memory while locked, ", fileType);
//	else
	else
	{
//		move the data to be written to disk into shared memory
//		call FileDataCmd() to command the SRT to write this data to file
		switch (fileType)
		{
			case eFileTypeParms:    // parameter data
				FileDataShmPtr->cflatAvgN = CflatAvgN;
				FileDataShmPtr->refcentAvgN = RefcentAvgN;
				FileDataShmPtr->cmAvgN = CMAvgN;
				FileDataShmPtr->flatSharpAvgN = FlatSharpAvgN;

				FileDataShmPtr->gsMode = GSMode;

				FileDataShmPtr->camFrameRate = CamFrameRate;
				FileDataShmPtr->camGain = CamGain;
				FileDataShmPtr->camCRTGain = CamCRTGain;

				FileDataShmPtr->crntCentroider = CrntCentroider;
				FileDataShmPtr->centThresh = CentThresh;

				FileDataShmPtr->reconGain = ReconGain;
				FileDataShmPtr->reconInt = ReconInt;
				FileDataShmPtr->reconCwt1 = ReconCwt1;
				FileDataShmPtr->reconCwt2 = ReconCwt2;

				FileDataShmPtr->ttGain = TTGain;

				FileDataShmPtr->apdRate = APDRate;
				FileDataShmPtr->apdGain = APDGain;
				for (i = 0; i < NUM_APDS; i++)
					FileDataShmPtr->apdBack[i] = APDBack[i];

				FileDataShmPtr->LttFrameRate = LttFrameRate;
			    FileDataShmPtr->LttCamGain = LttCamGain;
				FileDataShmPtr->LttFov = LttFov;
				FileDataShmPtr->UseAPD = UseAPD;
				FileDataShmPtr->UseAOA = UseAOA;

				FileDataCmd(eFileWriteParmsData);
				break;

			case eFileTypeCflat:    // cflat data
				for (j = 0; j < NUM_UNUSED_CAM_ROWS_TOP; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						FileDataShmPtr->cflatUnusedTop[j][k] = CflatUnusedTop[j][k];
				}
				for (i = 0; i < NUM_SUBAP_ROWS_PER_FRAME; i++)
				{
					for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
					{
						for (k = 0; k < NUM_PIX_IN_ROW; k++)
							FileDataShmPtr->cflatData[i][j][k] = CflatData[i][j][k];
					}
				}
				for (j = 0; j < NUM_UNUSED_CAM_ROWS_BOT; j++)
				{
					for (k = 0; k < NUM_PIX_IN_ROW; k++)
						FileDataShmPtr->cflatUnusedBot[j][k] = CflatUnusedBot[j][k];
				}

				FileDataCmd(eFileWriteCflatData);
				break;

			case eFileTypeRefCentB:  // reference centroid, 4x4 binned
				for (i = 0; i < NUM_CENTROIDS; i++)
					FileDataShmPtr->refcentsB[i] = RefcentsB[i];

				FileDataCmd(eFileWriteRefCentBData);
				break;

			case eFileTypeRefCentC:  // reference centroid, center-of-mass
				for (i = 0; i < NUM_CENTROIDS; i++)
					FileDataShmPtr->refcentsC[i] = RefcentsC[i];

				FileDataCmd(eFileWriteRefCentCData);
				break;

			case eFileTypeRefCentQ:  // reference centroid, quad-cell
				for (i = 0; i < NUM_CENTROIDS; i++)
					FileDataShmPtr->refcentsQ[i] = RefcentsQ[i];

				FileDataCmd(eFileWriteRefCentQData);
				break;

			case eFileTypeRefCentO:  // reference centroid, correlator
				for (i = 0; i < NUM_CENTROIDS; i++)
					FileDataShmPtr->refcentsO[i] = RefcentsO[i];

				FileDataCmd(eFileWriteRefCentOData);
				break;

			case eFileTypeCentOff:  // centroid offset data
				for (i = 0; i < NUM_CENTROIDS; i++)
					FileDataShmPtr->centoffs[i] = Centoffs[i];

				FileDataCmd(eFileWriteCentOffData);
				break;

			case eFileTypeSM:		// system matrix data
				FileDataCmd(eFileWriteSMData);
				break;

			case eFileTypeFlat:     // DM flat voltage data
				for (i = 0; i < NUM_ACTV_ACTS; i++)
					FileDataShmPtr->flat[i] = Flat[i];

				FileDataCmd(eFileWriteFlatData);
				break;

			case eFileTypeSharp:    // DM sharp (image sharpened) voltage data
				for (i = 0; i < NUM_ACTV_ACTS; i++)
					FileDataShmPtr->sharp[i] = Sharp[i];

				FileDataCmd(eFileWriteSharpData);
				break;

			case eFileTypeLTTOpen:    // Laser tilt/tip background data for open loop
				for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
				{
					for (j = 0; j < OPEN_PIX_IN_ROW; j++)
						FileDataShmPtr->LTTOpenBackground[i][j] = LTTOpenBackground[i][j];
				}

				FileDataCmd(eFileWriteOpenBackData);
				break;

			case eFileTypeLTTClosed:    // Laser tilt/tip background data for closed loop
				for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
					FileDataShmPtr->LTTClosedBackground[i] = LTTClosedBackground[i];

				FileDataCmd(eFileWriteClosedBackData);
				break;

			default:
				SysConSendStrngIntParm("Error: unexpected file type in FileDataPut(), ", fileType);
				break;
		}
	}

//	unlock shared memory
	FileDataShmPtr->dataShmLockHRT[fileType] = false;
}
