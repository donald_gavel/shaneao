/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			hostICon.h

AUTHOR:	DWP			DATE: 1/8/03

FILE DESCRIPTION:
This is the header file for the host interface controller component.

******************************************************************************/

#ifndef _HOSTICON_H
#define _HOSTICON_H		1

#include "types.h"
#include "fileMngr.h"
#include "fileData.h"

// Enums

enum 
{
	eCmdNone = -1,	   // no command; MUST be less than zero!

	eCmdQuit,	       // quit command
	eCmdRestart,	   // restart command
	eCmdShutdown,	   // shutdown command

	eCmdDiag,		   // diagnostic
	eCmdImages,		   // collect raw images (same as diag 0x01)
	eCmdData,		   // collect diagnostic data (same as diag 0x0E)
	eCmdAPDData,	   // collect ADP (actually, TT) data (same as diag 0x10)
	eCmdTelem,		   // select telemetry data
	eCmdTRate,		   // telemetry rate of update (per second)
	eCmdPOn,		   // turns PSF estimation on
	eCmdPOff,		   // turns PSF estimation off

	eCmdCamWFSInit,	   // initialize SciMeasure WFS camera
	eCmdCamRate,	   // frame rate of the camera (per second)
	eCmdCamGain,       // set camera gain (1 or 2)
	eCmdCamCRTGain,    // set camera CRT gain (0 to 4)
	eCmdCamSRate,	   // read frame rate from camera
	eCmdCamSGain,      // read gain from camera
	eCmdCamSCRTGain,   // read CRT gain from camera

	eCmdCentBin,	   // select 4x4 binned as centroid algorithm
	eCmdCentCM,		   // select center-of-mass as centroid algorithm
	eCmdCentQuad,	   // select quad-cell as centroid algorithm
	eCmdCentCorr,	   // select correlator as centroid algorithm
	eCmdCentTest,	   // select centroid test mode
	eCmdCentFlat,	   // determines and stores new flat file (dark levels)
	eCmdCentRef,	   // determines and stores new ref cents
	eCmdCentSParms,	   // sets ref cents to all zeroes
	eCmdCentThresh,	   // sets threshold used by centroider on raw data
	eCmdCentCentoffs,  // sets centroid offsets sent by host for image sharpening
	eCmdCentSaveoffs,  // saves centroid offsets
	eCmdCentDon,	   // turns TT mirror dithering on
	eCmdCentDoff,	   // turns TT mirror dithering off

	eCmdReconCM,	   // determine System Matrix (in preparation for computing CM offline)
	eCmdReconClose,	   // closes the AO loop
	eCmdReconOpen,	   // opens the AO loop
	eCmdReconEstop,	   // opens the AO loop and the TT loop
	eCmdReconGain,	   // sets AO loop gain (proportional gain)
	eCmdReconInt,	   // sets AO loop integrator gain
	eCmdReconCwt1,	   // sets AO loop compensator weight 1
	eCmdReconCwt2,	   // sets AO loop compensator weight 2
	eCmdReconMflat,    // averages and saves actuator voltages as flat mirror values
	eCmdReconFlatten,  // loads the latest stored flat mirror values
	eCmdReconMsharp,   // averages and saves actuator voltages as image-sharpened mirror values
	eCmdReconSharpen,  // loads the latest stored image-sharpened mirror values
	eCmdReconReg,	   // starts moving actuator x up and down at 1 Hz
	eCmdReconRegok,	   // stops moving all actuators that were started with reg

	eCmdTTGSMode,	   // 0 sets to ngs mode, 1 sets to lgs mode

	eCmdTTClose,	   // close tip/tilt loop
	eCmdTTOpen,		   // open tip/tilt loop
	eCmdTTGain,		   // set tip/tilt loop gain
	eCmdTTXMov,		   // set tip/tilt mirror X actuator (in Volts) (** these are temporary settings, they will be overwritten **)
	eCmdTTYMov,		   // set tip/tilt mirror Y actuator (in Volts) (** > by the reg command or when the tt loop is closed     **)
	eCmdTTLXMov,	   // set laser mirror X actuator (in Volts) (** these are temporary settings, they will be overwritten **)
	eCmdTTLYMov,	   // set laser mirror Y actuator (in Volts) (** > by the reg command or when the apd loop is closed    **)

	eCmdTTAPDClose,	   // close APD tip/tilt loop
	eCmdTTAPDOpen,	   // open APD tip/tilt loop
	eCmdTTAPDRate,	   // set APD tip/tilt loop rate
	eCmdTTAPDGain,	   // set APD tip/tilt loop gain
	eCmdTTAPDBack,	   // determine APD background
	eCmdTTAPDZBack,	   // zero APD background

	eCmdTTOlOn,		   // turn POCO offloading on  //XXXpoco3
	eCmdTTOlOff,	   // turn POCO offloading off  //XXXpoco3

	eCmdLTTCamInit,	   // initialize SciMeasure LTT camera
	eCmdLTTClose,	   // close LTT loop
	eCmdLTTOpen,	   // open LTT loop
	eCmdLTTRate,	   // set LTT loop rate
	eCmdLTTGain,	   // set LTT loop gain
	eCmdLTTBack,	   // determine LTT background
	eCmdLTTZBack,	   // zero LTT background
	eCmdLTTFov,		   // set closed-loop field of view to 2,4, or 6 arc seconds

	eCmdAbort,		   // abort command
	eCmdSysConParms,   // reads the centroid parameter files
	eCmdSysConCParms,  // reads the centroid parameter files (for backward compat.)
    eCmdSysConFillcm,  // reads the specified cm file
	eCmdTestN,		   // test, no param
	eCmdTestI,		   // test, int param
	eCmdTestF,		   // test, float param

	eCmdNumCmds		   // number of commands
};

enum
{
	eCmdActNone,		// no action required
	eCmdActQuit,	    // quit the AO control program
	eCmdActRestart,		// restart the AO system 
	eCmdActShutdown,	// shutdown the AO system
	eCmdActSendToDiag,  // send the command to the diagnostic thread
	eCmdActSendToHRT,	// send the command to the HRT processor
	eCmdActSendToCam,	// send the command to the HRT camera thread
	eCmdActSendToLTT,	// send the command to the HRT LTT camera thread
	eCmdActSendToCent,	// send the command to the HRT centroid thread
	eCmdActSendToRecon	// send the command to the HRT recon thread
};

enum
{
	eCmdNoRestrict = 0x00,  // all 3 loops can be open or closed for command to execute
	eCmdAOOpenBit  = 0x01,  // the ao loop must be open (this is actually a bit mask)
	eCmdTTOpenBit  = 0x02,  // the tt loop must be open (this is actually a bit mask)
	eCmdBothSHOpen = 0x03,  // both ao and tt loops must be open (both use Shack-Hartmann WFS) (must be eCmdAOOpenBit ORed with eCmdTTOpenBit)

	eCmdAPDOpenBit = 0x04,  // the apd tt loop must be open (this is actually a bit mask)
	eCmdLTTOpenBit = 0x08,  // the LTT loop must be open (this is actually a bit mask)
	eCmdBothTTOpen = 0x0E,  // both tt loops must be open (must be eCmdTTOpenBit ORed with eCmdAPDOpenBit ORed with eCmdAPDOpenBit)

	eCmdAllOpen	   = 0x0F   // all loops must be open (must be eCmdAOOpenBit ORed with eCmdTTOpenBit ORed with eCmdAPDOpenBit ORed with eCmdLTTOpenBit)
};

enum
{
	eCmdParamNone,	  // no parameter
	eCmdParamInt,	  // integer parameter
	eCmdParamFloat,	  // float parameter
	eCmdParamStrng,	  // string parameter
	eCmdParamSpecial  // 'special' parameters, 80 float values to accomodate centoffs
};


// Typedefs

// structure for the command table
typedef struct
{
	const char *strng;			// pointer to ASCII command string constant
	int16   idNum;			// command ID number (may or may not be the same as the index into the table!)
	int16   action;			// action to be taken when command is received
	int16	loopRestrict;	// indicates whether the ao and/or tt loops must be open for a command to execute
	int16   paramIndicator;	// indicates what kind of parameter the command has (none, int, or float)
	int16   paramLimLowI;	// lower limit for integer parameters (inclusive)
	int16   paramLimHighI;	// upper limit for integer parameters (inclusive); if lower and upper are equal, there is no limit
	float32 paramLimLowF;	// lower limit for float parameters (inclusive)
	float32 paramLimHighF;	// upper limit for float parameters (inclusive); if lower and upper are equal, there is no limit
} cmdTableType;

// structure for FIFO command messages being sent from hostICon to sysCon
typedef struct
{
	int16	cmdNdx;
	int16   intParam;
	float32 floatParam;
	char	strngParam[MAX_FILENAME_LENGTH];
} cmdFIFOMsgType;


// Global Variables

extern FileDataShmType *g_pFileDataShmPtr;  // pointer to file data shared memory
extern CamDataType *g_pCamDataPtr;			 // pointer to camera data
extern LTTCamDataType *g_pCamLTTPtr;		 // pointer to Laser Tip Tilt camera data
extern CentDataType *g_pCentDataPtr;		 // pointer to centroid data
extern ReconDataType *g_pReconDataPtr;		 // pointer to centroid data

extern int g_HostSocket;  // ethernet socket for host communications

extern int g_DiagFIFOfd;  // fd for FIFO used to receive diagnostic data to the diag thread

extern boolean g_HostConnectedFlag;  // indicates whether the host socket is connected

extern const cmdTableType g_CmdTable[eCmdNumCmds];  // command table

int16 HostIConProcCmd(char msgBuf[]);

#endif
