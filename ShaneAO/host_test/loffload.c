/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		loffload

AUTHOR:	DWP			DATE: 7/22/03

COMPONENT DESCRIPTION:
This component is a standalone program whose sole purpose is to attach to the lickao shared
memory and read and display averaged laser steering mirror voltages periodically.  The laser
operator uses this program by logging on remotely and running it so as to to see the laser 
steering mirror voltages in order to apply laser alignment corrections accordingly (this is 
called laser offloading).

COMPONENT UNITS:
1.	main()						loffload mainline

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;			 // mmap file descriptor
static int totalSize;			 // total size of shared memory
static unsigned char *startPtr;  // pointer to start of shared memory

static FileDataShmType *fileDataShmPtr;  // pointer to file data shared memory
static CamDataType *camDataPtr;		  // pointer to camera shared memory data
static LTTCamDataType *camLTTPtr;			  // pointer to Laser Tip Tilt camera shared memory data
static CentDataType *centDataPtr;		  // pointer to centroid shared memory data
static ReconDataType *reconDataPtr;	  // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the loffload mainline.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;
	float32 tmpX, tmpY;

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

//	open the shared memory region, exiting if an error occurs
	if ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0)
	{
		printf("Error creating shared memory, errno = %d\n", errno);
		exit(-1);
	}

//	get the pointer to the shared memory region, exiting if an error occurs
	if ((startPtr = mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED)
	{
		printf("Error getting pointer to shared memory, errno = %d\n", errno);
		exit(-1);
	}

//	set up the pointers to the global variables
	fileDataShmPtr = (FileDataShmType *)startPtr;
	camDataPtr = (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr = (CentDataType *)((char *)camDataPtr + camDataSize);
	reconDataPtr = (ReconDataType *)((char *)centDataPtr + centDataSize);
	camLTTPtr = (LTTCamDataType *)((char *)reconDataPtr + reconDataSize);

//	enter an infinite loop
	while (!quit)
	{
//		sleep for a second
        sleep(1);

//		print the offload values (it's possible that the X value could come from one loop and the Y value from the next; but,
//		> these values are averages; so, it really doesn't matter)
		tmpX = reconDataPtr->xLaserOffload;
		tmpY = reconDataPtr->yLaserOffload;
		printf("Avg TT Volts X: %f   Avg TT Volts Y: %f\n", tmpX, tmpY);
	}

	if (mmfd > -1)
	{
		printf("loffload: munmapping\n");  //XXX
//		clean up the shared memory
		if (startPtr > 0)
			munmap(startPtr, totalSize);
	  	close(mmfd);
	}

	printf("loffload: exiting, bye...\n");  //XXX
    return 0;
}


static void termHandler(int sigNum)
{
	quit = true;
}
