#ifndef HOST_ICON_LISTENER_H
#define HOST_ICON_LISTENER_H

#include "thread.h"

#include "types.h"

class CHostIConListener : public CThread
{
public:
    enum {
        IN_BUFF_LENGTH = 128,
        MSG_BUFF_LENGTH = 2048
    };

    CHostIConListener(int serverSocket);

    void Setup();

    void HaltListener();

protected:
    static void* connector(void *thisPointer);

    void Listener();
    int16 HostIConProcMsgs(void);

    //int HostSocket;  // ethernet socket for host communications

    int  m_serverSocket;
    bool m_loop;
    CMutex m_mutex;
};




#endif
