/**********************************************************************************************

COPYRIGHT (C) 2005
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		dispRS170

AUTHOR:				DATE: 11/14/05

COMPONENT DESCRIPTION:
This component is a standalone program whose sole purpose is to attach to the lickao shared
memory, periodically read RS170Buffs[][][] data, and write that data to the Camber 8510 RS-170
video board.  

COMPONENT UNITS:
1.	main()						dispRS170 mainline

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"

#include <Rdr8510Display.h>
#include <Rdr8510VideoConfig.h>

// Configuration Variables

static const int borderWidth = 5;

static const long secDelay = 0;			  // 0 secs
static const long nsecDelay = 40000000L;  // 40 msecs, for a 25 Hz update rate


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static int totalSize;								  // total size of shared memory

static FileDataShmType *fileDataShmPtr;  // pointer to file data shared memory
static CamDataType *camDataPtr;			 // pointer to camera shared memory data
static LTTCamDataType *camLTTPtr;		 // pointer to Laser Tip Tilt camera shared memory data
static CentDataType *centDataPtr;		 // pointer to centroid shared memory data
static ReconDataType *reconDataPtr;		 // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the dispRS170 mainline.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
	int i, j, k, iCam, jCam;
	int crntReadBuff;

	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;

	struct timespec next;

	int numRowsInDisp, numColsInDisp, offsetInRow, offsetInCol, offsetInMem;
	int numRowsToDisp, numColsToDisp;
	int numDispPixPerCamRow, numDispPixPerCamCol;
	int topEnd, bottomBegin, leftEnd, rightBegin;  // used to prevent drawing over crosshairs
	int tmpBuff[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW];

	Rdr8510Display *disp;

/*XXX64x64
	const int quadRowStartNdxs[4] = { 0,                   0,                   NUM_ROWS_IN_CAM / 2, NUM_ROWS_IN_CAM / 2 };
	const int quadRowStopNdxs[4] =  { NUM_ROWS_IN_CAM / 2, NUM_ROWS_IN_CAM / 2, NUM_ROWS_IN_CAM    , NUM_ROWS_IN_CAM     };
	const int quadColStartNdxs[4] = { 0,                   NUM_PIX_IN_ROW / 2,  0,                   NUM_PIX_IN_ROW / 2  };
	const int quadColStopNdxs[4] =  { NUM_PIX_IN_ROW / 2,  NUM_PIX_IN_ROW,      NUM_PIX_IN_ROW / 2,  NUM_PIX_IN_ROW      };
XXX64x64*/
	const int quadRowStartNdxs[4] = { FIRST_USED_CAM_ROW,  FIRST_USED_CAM_ROW,  NUM_ROWS_IN_CAM / 2, NUM_ROWS_IN_CAM / 2 };
	const int quadRowStopNdxs[4] =  { NUM_ROWS_IN_CAM / 2, NUM_ROWS_IN_CAM / 2, LAST_USED_CAM_ROW,   LAST_USED_CAM_ROW   };
	const int quadColStartNdxs[4] = { FIRST_USED_CAM_COL,  NUM_PIX_IN_ROW / 2,  FIRST_USED_CAM_COL,  NUM_PIX_IN_ROW / 2  };
	const int quadColStopNdxs[4] =  { NUM_PIX_IN_ROW / 2,  LAST_USED_CAM_COL,   NUM_PIX_IN_ROW / 2,  LAST_USED_CAM_COL   };

	int lowVal[4], highVal[4];
	float scaleFactor;

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

//	open the shared memory region
	while (!quit && ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0))
	{
		printf("dispRS170: waiting for mmap open\n");  //XXX
		sleep(1);
	}

//	get the pointer to the shared memory region
	while (!quit && ((startPtr = (unsigned char *)mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("dispRS170: waiting for mmap\n");  //XXX
		sleep(1);
	}

//	set up the pointers to the global variables
	fileDataShmPtr = (FileDataShmType *)startPtr;
	camDataPtr = (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr = (CentDataType *)((char *)camDataPtr + camDataSize);
	reconDataPtr = (ReconDataType *)((char *)centDataPtr + centDataSize);
	camLTTPtr = (LTTCamDataType *)((char *)reconDataPtr + reconDataSize);

//	instantiate display object
	disp = new Rdr8510Display("linuxTest", 640, 486, Rdr8510VideoConfig::RS170_RGB);	// not sure why this has to be done first
	sleep(1);
	disp = new Rdr8510Display("dispRS170", 640, 486, Rdr8510VideoConfig::RS170_NTSC);
	sleep(2);

//	get number of rows and columns and compute number of display pixels per camera pixel
	numRowsInDisp = disp->GetNumRows();
	numColsInDisp = disp->GetNumCols();
	if (numRowsInDisp < numColsInDisp)
	{
		numDispPixPerCamRow = numRowsInDisp / NUM_ROWS_IN_CAM;
		numDispPixPerCamCol	= numRowsInDisp / NUM_PIX_IN_ROW;
	}
	else
	{
		numDispPixPerCamRow = numColsInDisp / NUM_ROWS_IN_CAM;
		numDispPixPerCamCol	= numColsInDisp / NUM_PIX_IN_ROW;
	}
	numRowsToDisp = numDispPixPerCamRow * NUM_ROWS_IN_CAM;
	numColsToDisp = numDispPixPerCamCol * NUM_PIX_IN_ROW;

	offsetInRow = (numColsInDisp - numColsToDisp) / 2;
	offsetInCol = (numRowsInDisp - numRowsToDisp) / 2;

//	clear the temporary buffer
	for (i = 0; i < NUM_ROWS_IN_CAM; i++)
	{
		for (j = 0; j < NUM_PIX_IN_ROW; j++)
			tmpBuff[i][j] = 0;
	}

//	clear the screen
	for (i = 0; i < numRowsInDisp; i++)
	{
		for (j = 0; j < numColsInDisp; j++)
			disp->unpackedVideoMem[i * numColsInDisp + j] = 0;
	}

//	make the border and everything within it white (including the crosshairs)
	for (i = offsetInCol - borderWidth; i < offsetInCol + numRowsToDisp + borderWidth; i++)
	{
		for (j = offsetInRow - borderWidth; j < offsetInRow + numColsToDisp + borderWidth; j++)
			disp->unpackedVideoMem[i * numColsInDisp + j] = 0xFF00;
	}

// UseAOA -- for AOA	topEnd = numRowsToDisp / 2 - 1 - 58;  // make 2 pixel cross hairs  XXX the 58 is temporary
	topEnd = numRowsToDisp / 2 - 1;  // make 2 pixel cross hairs  XXX the 58 is temporary
	bottomBegin = topEnd + 2;
	leftEnd = numColsToDisp / 2 - 1;
	rightBegin = leftEnd + 2;

/*XXX test start
//	set up lots of shades of gray
	unsigned int gray[640];
//	for (i =   0; i < 160; i++)
//		gray[i] = 0x3F00;
//	for (i = 160; i < 320; i++)
//		gray[i] = 0x7F00;
//	for (i = 320; i < 480; i++)
//		gray[i] = 0xBF00;
//	for (i = 480; i < 640; i++)
//		gray[i] = 0xFF00;
	for (i = 0; i < 640; i++)
		gray[i] = (i % 64) << 9;
//XXX test end */

//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	next.tv_sec = secDelay;
	next.tv_nsec = nsecDelay;
	while (!quit && (nanosleep(&next, NULL) == 0))
	{
//		if (!camLTTPtr->intrIgnore && !camDataPtr->intrIgnore)  //dispRS170 seems to have problems when doing serial comms with camera (not sure why)  XXXINTIG
		{
/*XXX test start
			for (i = 0; i < numRowsInDisp; i++)
			{
				for (j = 0; j < numColsInDisp; j++)
					disp->unpackedVideoMem[i * numColsInDisp + j] = gray[j];
			}
//XXX test end */
//*XXX
//			move data out of the RS170Buffs[][][] quickly, finding the lowest and highest values for each quadrant as we go
			crntReadBuff = camDataPtr->crntRS170ReadBuff;
			for (k = 0; k < 4; k++)
			{
				lowVal[k] = 0xFFFF;
				highVal[k] = 0;

				for (i = quadRowStartNdxs[k]; i < quadRowStopNdxs[k]; i++)
				{
					for (j = quadColStartNdxs[k]; j < quadColStopNdxs[k]; j++)
					{
						tmpBuff[i][j] = (int)camDataPtr->RS170Buffs[crntReadBuff][i][j];
						if (tmpBuff[i][j] < lowVal[k])
							lowVal[k] = tmpBuff[i][j];
						if (tmpBuff[i][j] > highVal[k])
							highVal[k] = tmpBuff[i][j];
					}
				}
			}

//			scale the data
//			if we're not auto-scaling, set up a fixed scale factor
			if (camDataPtr->RS170crtGain > 0)
				scaleFactor = 255.0 / (16383.0 / (float)camDataPtr->RS170crtGain);
			for (k = 0; k < 4; k++)
			{
				if (camDataPtr->RS170crtGain <= 0)
				{
//					auto-scale by quadrant
					scaleFactor = (float)(highVal[k] - lowVal[k]);
					if (scaleFactor > 0.0)
						scaleFactor = 255.0 / scaleFactor;
				}

				for (i = quadRowStartNdxs[k]; i < quadRowStopNdxs[k]; i++)
				{
					for (j = quadColStartNdxs[k]; j < quadColStopNdxs[k]; j++)
					{
						tmpBuff[i][j] = (int)((float)(tmpBuff[i][j] - lowVal[k]) * scaleFactor);
						tmpBuff[i][j] = (tmpBuff[i][j] << 8) & 0xFF00;
					}
				}
			}

//			write the data to the video memory, expanding the pixels and leaving the crosshairs alone
			for (i = 0; i < topEnd; i++)
			{
				offsetInMem = (i + offsetInCol) * numColsInDisp + offsetInRow;
				iCam = i / numDispPixPerCamRow;
				for (j = 0; j < leftEnd; j++)
				{
					jCam = j / numDispPixPerCamCol;
					disp->unpackedVideoMem[offsetInMem + j] = tmpBuff[iCam][jCam];
				}
				for (j = rightBegin; j < numColsToDisp; j++)
				{
					jCam = j / numDispPixPerCamCol;
					disp->unpackedVideoMem[offsetInMem + j] = tmpBuff[iCam][jCam];
				}
			}
			for (i = bottomBegin; i < numRowsToDisp; i++)
			{
				offsetInMem = (i + offsetInCol) * numColsInDisp + offsetInRow;
				iCam = i / numDispPixPerCamRow;
				for (j = 0; j < leftEnd; j++)
				{
					jCam = j / numDispPixPerCamCol;
					disp->unpackedVideoMem[offsetInMem + j] = tmpBuff[iCam][jCam];
				}
				for (j = rightBegin; j < numColsToDisp; j++)
				{
					jCam = j / numDispPixPerCamCol;
					disp->unpackedVideoMem[offsetInMem + j] = tmpBuff[iCam][jCam];
				}
			}
//XXX*/
		}

//		set up for next sleep
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
	}

	if (mmfd > -1)
	{
		printf("dispRS170: munmapping\n");  //XXX
//		clean up the shared memory
		if (startPtr > 0)
			munmap(startPtr, totalSize);
	  	close(mmfd);
	}

	printf("dispRS170: exiting, bye...\n");  //XXX
    return 0;
}


static void termHandler(int sigNum)
{
	quit = true;
}
