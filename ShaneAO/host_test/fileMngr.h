/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			fileMngr.h

AUTHOR:	DWP			DATE: 3/12/03

FILE DESCRIPTION:
This is the header file for the file manager on the SRT side.

******************************************************************************/

#ifndef _FILEMNGR_H
#define _FILEMNGR_H		1


// Enums

enum
{
	eFileTypeParms,      // parameter data
	eFileTypeCflat,      // cflat data
	eFileTypeRefCentB,   // reference centroids, 4x4 binned
	eFileTypeRefCentC,   // reference centroids, center-of-mass
	eFileTypeRefCentQ,   // reference centroids, quad-cell
	eFileTypeRefCentO,   // reference centroids, correlator
	eFileTypeCentOff,    // centroid offset data
	eFileTypeCM,         // control matrix data
	eFileTypeSM,         // system matrix data
	eFileTypeFlat,       // DM flat voltage data
	eFileTypeSharp,      // DM sharp (image sharpened) voltage data
	eFileTypeLTTOpen,	 // Laser tilt/tip background data for open loop
	eFileTypeLTTClosed,	 // Laser tilt/tip background data for closed loop
	eFileTypeNum
};

enum
{
	eFileReadAllData,        // 'all' parameter data should be read from file into shared memory
	eFileReadCMData,         // control matrix data should be read from file into shared memory

	eFileWriteParmsData,      // parameter data should be written to file from shared memory
	eFileWriteCflatData,      // cflat data should be written to file from shared memory
	eFileWriteRefCentBData,   // 4x4 binned reference centroid data should be written to file from shared memory
	eFileWriteRefCentCData,   // center-of-mass reference centroid data should be written to file from shared memory
	eFileWriteRefCentQData,   // quad-cell reference centroid data should be written to file from shared memory
	eFileWriteRefCentOData,   // correlator reference centroid data should be written to file from shared memory
	eFileWriteCentOffData,    // centroid offset data should be written to file from shared memory
	eFileWriteSMData,		  // system matrix data should be written to file from shared memory
	eFileWriteFlatData,       // DM flat voltage data should be written to file from shared memory
	eFileWriteSharpData,      // DM sharp (image sharpened) voltage data should be written to file from shared memory
	eFileWriteOpenBackData,	  // WFS background data for open loop program
	eFileWriteClosedBackData  // WFS background data for open loop program
};


// Defines

#define MAX_PATHNAME_LENGTH 128  // this can include the filename
#define MAX_FILENAME_LENGTH 32   // this can include the date directory name (mmyydd), if there is one

#define TEXT_BUF_LNGTH 128  // length of text buffers used primarily to set up messages for HostDiagSendStrngNoParm()


// Typedefs



// Global Variables



// Global Function Prototypes

void FileMngr( int16 fileCmd );
void FileMngrInit( void );
void FileMngrWriteParms( void );

#endif
