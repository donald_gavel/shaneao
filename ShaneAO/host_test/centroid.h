/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			centroid.h

AUTHOR:	DWP			DATE: 12/26/02

FILE DESCRIPTION:
This is the header file for the centroid component.

******************************************************************************/

#ifndef _CENTROID_H
#define _CENTROID_H		1

#include "cam.h"

// Enums

enum
{
	eCentBin,   // 4x4 binned
	eCentCM,    // center of mass
	eCentQuad,  // quad-cell
	eCentCorr,  // correlator
	eCentTest,  // test mode
};


// Defines

#define INIT_CFLAT_AVGN    100  // initial number of frames to average when determining cflat
#define INIT_REFCENT_AVGN  100  // initial number of frames to average when determining reference centroids

#define INIT_CRNT_CENTROIDER eCentQuad  // indicates which centroid algorithm is currently being used
#define INIT_CENT_THRESH     0          // value centroider subtracts from raw data (then, if result is less than zero, 
				                        // > centroider sets raw data to zero)

#define NUM_SUBAPS		   40				 // number of wavefront sensor (WFS) sub-apertures
#define NUM_CENTROIDS	   (2 * NUM_SUBAPS)	 // number of centroids

#define NUM_CENT_BUFFS	   4196			// number of centroid buffers (there are lots of these 
						   				// > so the diagnostics data handler can get behind)

#define DITHER_COS_LNGTH    8    // length of TT dither cosine curve
#define DITHER_NUM_CYCLES   512  // number of cosine cycles to use TT dither matched filter


// Typedefs

// 4x4 binned descriptor type
typedef struct 
{
	int16	subApRow;	 // row of sub-apertures that this binned cell is in (0 -> 7, -1 = end of list)
	int16	pixRows[4];	 // pixel rows, within the subApRow, that the 16 pixels are in
	int16	pixCols[4];	 // pixel columns that the 16 pixels are in
} BinnedDescType;

// center of mass descriptor type
typedef struct 
{
	int16	subApRow;	 // row of sub-apertures that this center of mass cell is in (0 -> 7, -1 = end of list)
	int16	pixRows[4];	 // pixel rows, within the subApRow, that the 16 pixels are in
	int16	pixCols[4];	 // pixel columns that the 16 pixels are in
} CentMassDescType;

// quad-cell descriptor type
typedef struct 
{
	int16	subApRow;	 // row of sub-apertures that this quad-cell is in (0 -> 7, -1 = end of list)
	int16	pixRows[2];	 // pixel rows, within the subApRow, that the 4 pixels are in
	int16	pixCols[2];	 // pixel columns that the 4 pixels are in
} QuadCellDescType;

// centroid buffer type
typedef struct
{
    boolean inUseFlag;               // indicates whether this buffer is in use for non-diag data
	int16	diagIndicator;			 // indicates eDiagIndNotInUse, eDiagIndInUse, or eDiagIndTelem

	float32 xyCents[NUM_CENTROIDS];	 // X centroids followed by Y centroids
	float32 xTTError;				 // X Tip/Tilt error
	float32 yTTError;				 // Y Tip/Tilt error

	float32 intens[NUM_SUBAPS];		 // total sub-ap intensities
	float32 pixels[NUM_SUBAPS][4];	 // pixels used in centroid computations (needed for PSF) 
} CentBuffType;

// centroid data type
typedef struct
{
	int16			cmdStart;		// index (into CmdTable[]) of command to be started or eCmdNone
	int16			cmdCrnt;		// index (into CmdTable[]) of command currently running or eCmdNone
	int16			cmdIntParam;	// integer parameter for command, if it has one
	float32			cmdFloatParam;  // float parameter for command, if it has one

	int16			diagActFlag;    // diagnostic action flag (set only on SRT side (except at initialization))
	int16			diagStatFlag;   // diagnostic status flag (set only on HRT side)
	int16			diagDiagRate;   // indicates rate at which diagnostic data should be sent to SRT (always 0 or 1)
	int16			diagDiagCntr;	// counter for diagnostic data being sent to SRT
	int16			diagTelemRate;  // indicates rate at which telemetry data should be sent to SRT (same as diag data, but at different rate)
	boolean			diagPSFFlag;	// indicates that PSF estimation is on, so data should be sent

	int16			writeNdx;						   // index into camBuffNdxs[] for buffer being written to
	int16			readNdx;						   // index into camBuffNdxs[] for buffer being read from
	int16			centroidBuffNdxs[NUM_CENT_BUFFS];  // circular list of indexes into centroidBuffs[]

	CentBuffType	centroidBuffs[NUM_CENT_BUFFS];	   // the centroid buffers
} CentDataType;


// Global Variables

extern int16 CrntCentroider;  // indicates which centroid algorithm is currently being used

extern int16 CentThresh;  // value centroider subtracts from raw data (then, if result is less than zero, 
	   				      // > centroider sets raw data to zero)

extern int16 CflatAvgN;  // number of frames to average when determining cflat
extern int16 CflatData[NUM_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current cflat data
extern int16 CflatUnusedTop[NUM_UNUSED_CAM_ROWS_TOP][NUM_PIX_IN_ROW];  // unused cflat data, top
extern int16 CflatUnusedBot[NUM_UNUSED_CAM_ROWS_BOT][NUM_PIX_IN_ROW];  // unused cflat data, bottom

extern int16 RefcentAvgN;  // number of frames to average when determining reference centroids
extern float32 RefcentsB[NUM_CENTROIDS];  // current reference centroids for 4x4 binned
extern float32 RefcentsC[NUM_CENTROIDS];  // current reference centroids for center-of-mass
extern float32 RefcentsQ[NUM_CENTROIDS];  // current reference centroids for quad-cell
extern float32 RefcentsO[NUM_CENTROIDS];  // current reference centroids for correlator

extern float32 Centoffs[NUM_CENTROIDS];  // current reference offsets (for image sharpening)

extern boolean TTDitherOn;		// indicates whether TT mirror dithering is on or off
extern int16   TTDitherCosNdx;  // indexes the TT cosine curve, TTDitherCosCurves[] for inserting the curve
extern float32 TTDitherCosCurves[DITHER_COS_LNGTH][DITHER_COS_LNGTH];  // cosine curves with which to dither TT mirror and extract curve

extern boolean CentGettingBehindFlag;


// Global Function Prototypes

void *Cent( void *arg );
void CentRestart( void );


#endif
