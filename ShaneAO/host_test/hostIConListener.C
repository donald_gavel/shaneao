#include <errno.h>

#include "hostIConListener.h"
#include "hostIConCentral.h"
#include "socket.h"

#include "hostICon.h"

//
// Connector (see CThread::connector
//
void* CHostIConListener::connector(void *thisPointer)
{
    ((CThread*)thisPointer)->UpdateThreadId();
    ((CHostIConListener*)thisPointer)->Listener();
    CThread::SelfDelete((CHostIConListener*)thisPointer);
    pthread_exit(NULL);
}



CHostIConListener::CHostIConListener(int serverSocket) 
{
    m_coreProcConnector = connector;
    m_loop = true;
    m_serverSocket = serverSocket;
}


void CHostIConListener::HaltListener() 
{
    LogNotice("HaltListener");
    m_mutex.Lock();
    m_loop = false;
    m_mutex.Unlock();
}


void CHostIConListener::Listener() 
{
    //	enter an almost-infinite loop (exit if a system error occurs or drop out if a restart or 
    //		shutdown command is received)

    m_mutex.Lock();
    bool loop = m_loop = true;
    m_mutex.Unlock();
    while (loop) {	
        //	listen on the server socket and accept when a connection is initiated, exiting if an error occurs
        LogNotice("Listener Waiting to connect to host...\n");
        if (listen(m_serverSocket, 0) < 0) {
            LogErr("Listener Error listening on server socket, errno = %d\n", errno);
            exit(-1); 
        }
        
        if ((g_HostSocket = accept(m_serverSocket, NULL, NULL)) < 0) {
            LogErr("Listener Error accepting host socket, errno = %d\n", errno);
            exit(-1);
        }
        
        g_pCCentral->queHostConnectedFlag(true);        
        g_pCCentral->queHostIConSendStrngNoParm("   Host connected!"); 
        LogNotice("Listener    connected");
	
        g_pCCentral->queHostIConProcCmd( "ver" );

        // Call HostIConProcMsgs() to process host messages; HostIConProcMsgs() will not return unless
        //	the socket is lost or a quit, restart, or shutdown message is received
        HostIConProcMsgs(); // <--- this is the important part

        m_mutex.Lock(); 
        loop = m_loop;
        m_mutex.Unlock();
    }
    
    g_pCCentral->queHostConnectedFlag(false); 
    
    //	shut the socket down and close it  
    shutdown(g_HostSocket, SHUT_RDWR);
    close(g_HostSocket);
    
}






/**********************************************************************************************
UNIT NAME:  HostIConProcMsgs()

UNIT DESCRIPTION:
The purpose of this unit is to process messages from the host, by receiving them, parsing them,
and calling HostIConProcCmd() to process commands.

UNIT INPUTS:
	None

UNIT OUTPUTS:
1.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrLostSocket	- lost the socket used for host communications
2.	eErrRestart		- a restart command has been received from the host
3.	eErrShutdown	- a shutdown command has been received from the host
4.	eErrQuit	    - a quit command has been received from the host
**********************************************************************************************/
int16 CHostIConListener::HostIConProcMsgs(void)
{
    //static int16   msgNdx = 0;
    int16   msgNdx = 0;
    char	inBuf[IN_BUFF_LENGTH], msgBuf[MSG_BUFF_LENGTH];
    int16   numBytes, i;
    //	enter a nearly infinite loop (return if the socket is lost or a restart command is received)
    while (m_loop) {	
        // Receive a message from the host via the host socket
        numBytes = recv(g_HostSocket, inBuf, IN_BUFF_LENGTH, 0);
        LogNotice("Listener expecting %d bytes", numBytes);
        //	if an error occurred in the receive, report it
        if (numBytes < 0) {
            g_pCCentral->queHostIConSendStrngIntParm("Socket receive error, errno: ", errno);
            //	else if zero bytes have been received (other side has shut down), 
            //	 return indicating that socket has been lost
        } else if (numBytes == 0) {
            return eErrLostSocket;
        } else {	// Parse the message (with a stream socket, there's no guarantee that messages will come in tidy 
	        //	packets), calling HostIConProcCmd() as necessary; return if a restart command has been received
            i = 0;
            while (i < numBytes) {
                while ((i < numBytes) && (inBuf[i] != '\0') && (inBuf[i] != '\n') && (msgNdx < MSG_BUFF_LENGTH)) {	
                    // Fill the message buffer
                    msgBuf[msgNdx] = inBuf[i];
                    msgNdx++;
                    i++;
                }
                
                if (msgNdx >= MSG_BUFF_LENGTH) {	
                    // Oops! too big
                    msgNdx = 0;
                    i = numBytes;  // so outer loop will fall through
                    LogWarn("Listener message buffer overflow, reseting");
                    g_pCCentral->queHostIConSendStrngNoParm("Error: message buffer overflow");
                } else if (i < numBytes) {	
                    // Complete rsg recv'd, null terminate it and send command to be processed
                    msgBuf[msgNdx] = '\0';
                    LogNotice("Listener got msgBuf=%s", msgBuf);
                    g_pCCentral->queHostIConProcCmd(msgBuf);
                    msgNdx = 0;
                    i++;
                }
            }
        }
    }
}


