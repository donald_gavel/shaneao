/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		poco

AUTHOR:	DWP			DATE: 5/13/05

COMPONENT DESCRIPTION:
This component is a standalone program whose purpose is to connect to the POCO system via a
socket and attach to the lickao shared memory.  Then, tip/tilt offloading data is read from 
shared memory 10 times per second and sent to POCO.

COMPONENT UNITS:
1.	main()			poco mainline

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"


// Configuration Variables

static const int pocoPort = 5700;
static const char pocoName[] = "128.114.176.36";  // "shanepc.ucolick.org";  // "134.9.244.95";  // 134.9.244.95 is lgs9 at LLNL
//XXX static const long secDelay = 0;			   // 0 secs
//XXX static const long nsecDelay = 500000000L;  // 500 msecs; changed from 50 msecs
static const long secDelay = 2;				// 1 secs
static const long nsecDelay = 500000000L;	// 500 msecs

// Global Variables


// Local Variables

FileDataShmType *fileDataShmPtr;  // pointer to file data shared memory
CamDataType *camDataPtr;		  // pointer to camera shared memory data
LTTCamDataType *camLTTPtr;			  // pointer to Laser Tip Tilt camera shared memory data
CentDataType *centDataPtr;		  // pointer to centroid shared memory data
ReconDataType *reconDataPtr;	  // pointer to recon shared memory data


// Local Function Prototypes


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the poco mainline.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
	boolean connectedFlag = false;
	static unsigned int xOrY = 0;
	const int bufSize = 32;
	char outBuf[bufSize];
	struct timespec next;

	int mmfd;					    // mmap file descriptor
	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;
	int totalSize;				    // total size of shared memory
	unsigned char *startPtr;	    // pointer to start of shared memory

	int pocoSock;
	struct sockaddr_in sockAddr;

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

//	open the shared memory region, looping if an error occurs (presumably because it's not available yet)
//	get the pointer to the shared memory region, looping if an error occurs
	while ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0)
		sleep(1);
	while ((startPtr = mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED)
		sleep(1);

//	set up the pointers to the global variables
	fileDataShmPtr = (FileDataShmType *)startPtr;
	camDataPtr = (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr = (CentDataType *)((char *)camDataPtr + camDataSize);
	reconDataPtr = (ReconDataType *)((char *)centDataPtr + centDataSize);
	camLTTPtr = (LTTCamDataType *)((char *)reconDataPtr + reconDataSize);

//	create the POCO socket, displaying a message if an error is encountered
	if ((pocoSock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		printf("Error: unable to create POCO socket\n");
	else
	{
		printf("T1\n");  //XXX
//		enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while (nanosleep(&next, NULL) == 0)
		{
//			printf("T2\n");  //XXX
//			if we're not connected to the POCO server, try to connect
			if (!connectedFlag)
			{
				sockAddr.sin_family = AF_INET;
				sockAddr.sin_port = htons(pocoPort);
				inet_pton(AF_INET, pocoName, &sockAddr.sin_addr);
				if (connect(pocoSock, (struct sockaddr *)&sockAddr, sizeof(sockAddr)) == 0)
					connectedFlag = true;
			}
//			printf("T3\n");  //XXX

//			if we're connected to the POCO server, send the offload values to POCO
			if (connectedFlag)
			{
				if (xOrY == 0)
				{
					sprintf(outBuf, "Y %f\n", -reconDataPtr->xPocoOffload * 0.25);  // changed from 'X' and changed sign
					send(pocoSock, outBuf, strlen(outBuf), 0);
//					printf("S1: %d\n", send(pocoSock, outBuf, strlen(outBuf) + 1, 0));  //XXXtest
//					printf("%s\n", outBuf);  //XXXtest
				}
				else
				{
					sprintf(outBuf, "X %f\n", -reconDataPtr->yPocoOffload * 0.25);  // changed from 'Y' and changed sign
					send(pocoSock, outBuf, strlen(outBuf), 0);
//					printf("S2: %d\n", send(pocoSock, outBuf, strlen(outBuf) + 1, 0));  //XXXtest
//					printf("%s\n", outBuf);  //XXXtest
				}
				xOrY ^= 0x1;
			}

//			set up for next sleep
			next.tv_sec = secDelay;
			next.tv_nsec = nsecDelay;
		}

		printf("T4\n");  //XXX
//		shut the socket down and close it
		shutdown(pocoSock, SHUT_RDWR);
	    close(pocoSock);
	}

//	clean up the shared memory and return
	munmap(startPtr, totalSize);
  	close(mmfd);

    return 0;
}
