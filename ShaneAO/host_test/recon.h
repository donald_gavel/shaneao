/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			recon.h

AUTHOR:	DWP			DATE: 1/22/03

FILE DESCRIPTION:
This is the header file for the reconstruction component.

******************************************************************************/

#ifndef _RECON_H
#define _RECON_H		1


// Enums

// ao loop status values
enum
{
	eAOLoopOpen,
	eAOLoopOpening,
	eAOLoopClosed
};

enum
{
	eCMStateSetMin,
	eCMStateAvgMin,
	eCMStateSetMax,
	eCMStateAvgMax
};

enum
{
	eCMStateStart,
	eCMStateTakeZero,
	eCMStateTakeData,
	eCMStateTurn,
	eCMStateDone
};


// Defines

#define INIT_CM_AVGN			100  // number of DM voltages to average when determining sharp mirror values
#define INIT_FLAT_SHARP_AVGN	100	 // number of DM voltages to average when determining flat and sharp mirror values

#define INIT_RECON_GAIN 0.35  // AO loop gain (proportional gain)
#define INIT_RECON_INT  0.95  // AO loop integrator gain
#define INIT_RECON_CWT1 1.0   // AO loop compensator weight 1 (IMPORTANT: > 0.0 means use the default from defaultCwt1s[])
#define INIT_RECON_CWT2 1.0   // AO loop compensator weight 2 (IMPORTANT: > 0.0 means use the default from defaultCwt2s[])

#define DAC_VOLT_MAX   5.0   	// maximum value for DAC_VOLT_LIMIT
#define DAC_VOLT_LIMIT 5.0   	// voltage limit (plus or minus) for DACs, NEVER set this above 5.0
//#define TT_VOLT_LIMIT 4.0  		// voltage limit (plus or minus) for the laser tip/tilt mirror, NEVER set this above 5.0  tt2inch
#define TT_VOLT_LIMIT 5.0  		// voltage limit (plus or minus) for the laser tip/tilt mirror, NEVER set this above 5.0  tt2inch tt3inch
#define TTLASER_VOLT_LIMIT 2.0  // voltage limit (plus or minus) for the laser tip/tilt mirror, NEVER set this above 5.0
#define DAC_FULL_VOLTS 20.0  	// DAC full range in volts (-10.0 to +10.0)

#define DAC_MIN_VAL  0x4000  // minimum value for AO hardware (-5V), NEVER set this below 0x4000
#define DAC_MID_VAL  0x7FFF  // DAC middle value (0V)
#define DAC_MAX_VAL  0xBFFF  // maximum value for AO hardware (+5V), NEVER set this above 0xBFFF
#define DAC_FULL_VAL 0xFFFF  // DAC full-scale value (+10V)

#define DAC_TELCO_ENABLE_PORT 135  // DAC telco enable port (in dacDACMap[])

#define TELCO_ENABLE   0.0  // telco enable (active low, 0 Volts)
#define TELCO_DISABLE -5.0  // telco disable (-5 Volts; actually comes out to the telco as +5 Volts)

#define NUM_DAC_BOARDS			5	 // number of DAC boards
#define NUM_DAC_CHANS_PER_BOARD 32   // number of DAC channels per board
#define NUM_DAC_PORTS			160	 // total number of DAC ports (five 32-channel boards)

#define NUM_ACTUATORS			127	 // number of Deformable Mirror (DM) actuators
#define NUM_ACTV_ACTS			61	 // number of active Deformable Mirror (DM) actuators

#define NUM_RECON_BUFFS			1124  // number of recon buffers (there are lots of these 
									  // > so the diagnostics data handler can get behind)

#define CM_NUM_DATA_SETS 14  // number of data sets to collect when determining slopes for each actuator for cm


// Typedefs

// recon buffer type
typedef struct
{
	int16	diagIndicator;			   // indicates eDiagIndNotInUse, eDiagIndInUse, or eDiagIndTelem

	float32 reconData[NUM_ACTV_ACTS];  // recon data

	float32 ttData[eTTDiagNumData];	   // tip/tilt data (primarily for diagnostics)
} ReconBuffType;

// recon data type
typedef struct
{
	int16			cmdStart;		// index (into CmdTable[]) of command to be started or eCmdNone
	int16			cmdCrnt;		// index (into CmdTable[]) of command currently running or eCmdNone
	int16			cmdIntParam;	// integer parameter for command, if it has one
	float32			cmdFloatParam;  // float parameter for command, if it has one

	int16			diagActFlag;	// diagnostic action flag (set only on SRT side (except at initialization))
	int16			diagStatFlag;	// diagnostic status flag (set only on HRT side)
	int16			diagDiagRate;   // indicates rate at which diagnostic data should be sent to SRT (always 0 or 1)
	int16			diagDiagCntr;	// counter for diagnostic data being sent to SRT
	int16			diagTelemRate;	// indicates rate at which telemetry data should be sent to SRT (same as diag data, but at different rate)
	boolean			diagPSFFlag;	// indicates that PSF estimation is on, so data should be sent

	float32			xPocoOffload;  // X POCO offload value (averaged. scaled TT mirror voltage, X axis)  //XXXpoco
	float32			yPocoOffload;  // Y POCO offload value (averaged, scaled TT mirror voltage, Y axis)  //XXXpoco

	float32			xLaserOffload;  // X laser offload value (averaged laser steering mirror voltage, X axis)
	float32			yLaserOffload;  // Y laser offload value (averaged laser steering mirror voltage, Y axis)

	boolean 		lttCamCloseDone;  // used by cam_LTT_Mngr (on the SRT side) to let cam_LTT (on the HRT side) know when 
									  // > the SciMeasure LTT camera is in closed loop mode

	boolean 		newAPDDataFlag;  		 // indicates whether there is new APD data pending
	int32 			newAPDCounts[NUM_APDS];  // new APD counts
	float32 		newAPDErrX;  			 // new X error from APD centroider, in APD units (analogous to pixel units)
	float32 		newAPDErrY;  			 // new Y error from APD centroider, in APD units (analogous to pixel units)
	boolean			newAPDUpdateDACsFlag;	 // indicates whether DACs should be updated
	float32 		newAPDVoltsX;  			 // new X DAC voltage setting resulting from the APDs
	float32 		newAPDVoltsY;  			 // new Y DAC voltage setting resulting from the APDs
	float32 		newAPDTelcoX;			 // new X telco voltage resulting from the APDs
	float32 		newAPDTelcoY;			 // new Y telco voltage resulting from the APDs
	float32 		newAPDTelcoEnable;		 // new X telco enable resulting from the APDs
	float32			newAPDOverLimit;		 // new APD over limit flag
	float32			newAPDCountsPerSec;		 //	new total APD counts per second

	int16			writeNdx;					     // index into reconBuffNdxs[] for buffer being written to
	int16			reconBuffNdxs[NUM_RECON_BUFFS];  // circular list of indexes into reconBuffs[]

	ReconBuffType	reconBuffs[NUM_RECON_BUFFS];     // the recon buffers
} ReconDataType;

// DAC map data type
typedef struct
{
	int board;  // DAC board (0 to 4)
	int chan;	// DAC channel on DAC board (0 to 31)
} DACMapType;

// data type for the table that drives the ReconCMProcess() function (to create a system matrix)
typedef struct
{
	float32 volts;  // volts to set for current step
	int16 state;	// state that voltage corresponds to
} CMVoltsType;


// Global Variables

extern DACMapType dacTTMap[eTTNumVolts];

extern int16 ReconRunCmd;  // the 'long' command that is currently being executed, if any

extern int16 AOLoopStat;  // indicates the status of the ao loop (open, opening, or closed)

extern float32 ReconGain;  // AO loop gain (proportional gain)
extern float32 ReconInt;   // AO loop integrator gain
extern float32 ReconCwt1;  // AO loop compensator weight 1
extern float32 ReconCwt2;  // AO loop compensator weight 2

extern float32 CrntReconCwt1;  // ReconCwt1 if ReconCwt1 is <= 0.0, else defaultCwt1s[] corresponding to current frame rate
extern float32 CrntReconCwt2;  // ReconCwt2 if ReconCwt2 is <= 0.0, else defaultCwt2s[] corresponding to current frame rate

extern float32 ControlMatrix[NUM_ACTV_ACTS][NUM_CENTROIDS];  // control matrix used for VMM reconstruction (AKA, cm)

extern int16 CMAvgN;  // number of centroids to average when determining a system matrix

extern int16 FlatSharpAvgN;           // number of DM voltages to average when determining flat and sharp mirror values
extern float32 Flat[NUM_ACTV_ACTS];   // current flat mirror values (i.e., DM voltages corresponding to a 'flattened' mirror)
extern float32 Sharp[NUM_ACTV_ACTS];  // current sharp mirror values (i.e., DM voltages corresponding to a 'image sharpened' mirror)

extern boolean RegActive;  // flag indicating whether the reg command is active or not


// Global Function Prototypes

void *Recon( void *arg );
void ReconRestart( void );
void ReconUpdateDACs( DACMapType map[], float32 volts[], int16 numVals );


#endif
