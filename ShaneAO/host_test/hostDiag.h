/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			diag.h

AUTHOR:	DWP			DATE: 1/8/03

FILE DESCRIPTION:
This is the header file for the diagnostic data handler component.

******************************************************************************/

#ifndef _HOSTDIAG_H
#define _HOSTDIAG_H		1


// Enums

enum
{
	eDiagActNoAction,      // no action
	eDiagActRestart,	   // diag packet transfer from HRT to SRT should restart
	eDiagActEnterStandby,  // diag packet transfer from HRT to SRT should enter standby
	eDiagStatOperating,	   // diag packet transfer from HRT to SRT is operating and okay
	eDiagStatResyncing,	   // diag packet transfer from HRT to SRT is operating and resyncing is in progress (after an overrun)
	eDiagStatStandby	   // diag packet transfer from HRT to SRT is in standby (not operating)
};

enum
{
	eDiagIndNotInUse,  // indicates that buffer is not in use for diag data
	eDiagIndInUse,	   // indicates that buffer is in use for diag data
	eDiagIndTelem	   // indicates that buffer is in use for diag data and has a telemetry frame
};

enum
{
	eDiagCamData,	  // diag data from the camera thread
	eDiagCentData,	  // diag data from the centroid thread
	eDiagReconData,	  // diag data from the recon thread
	eDiagLTTData,	  // diag data from the Laser Tip Tilt thread
	eDiagNumThreads,  // number of threads that can provide diag data

	eDiagLTTCmd,			  // command from the HRT to change a Tilt/Tip camera parameter
	eDiagWFSCmd,			  // command from the HRT to change a SciMeasure camera parameter
	eDiagCmdFile,			  // command from the HRT to read or write a file
	eDiagCmdHost,			  // command from the host, via hostICon
	eDiagCmdErrStrngNoParm,	  // error message to be sent to the host, no parameter
	eDiagCmdErrStrngIntParm,  // error message to be sent to the host, integer parameter
	eDiagCmdErrStrngFltParm	  // error message to be sent to the host, float parameter
};

enum
{
	eDiagRawBit		= 0x01,  // bit set in diag and telem commands to indicate raw camera data (from camera thread)
	eDiagCentBit	= 0x02,  // bit set in diag and telem commands to indicate centroid data (from centroid thread)
	eDiagIntenBit	= 0x04,  // bit set in diag and telem commands to indicate intensity data (from centroid thread)
	eDiagDMVoltsBit	= 0x08,  // bit set in diag and telem commands to indicate DM Volts data (from recon thread)
	eDiagTTBit		= 0x10,  // bit set in diag and telem commands to indicate tip/tilt data (from recon thread)
	eDiagLttBit		= 0x20,  // bit set in diag and telem commands to indicate ltt data (from LTT camera thread)
	eDiagNumBits	=    6	 // number of possible diag data types
};

enum
{
	eDiagRawNdx,	  // index corresponding to eDiagRawBit
	eDiagLttNdx,	  // index corresponding to eDiagLttBit
	eDiagCentNdx,	  // index corresponding to eDiagCentBit
	eDiagIntenNdx,	  // index corresponding to eDiagIntenBit
	eDiagDMVoltsNdx,  // index corresponding to eDiagDMVoltsBit
	eDiagTTNdx		  // index corresponding to eDiagTTBit
};

enum
{
	eDiagOutStrng,		 // null terminated string
	eDiagOutRaw,		 // raw image data
	eDiagOutLtt,		 // LTT image data
	eDiagOutCent,		 // centroid data
	eDiagOutInten,		 // intensity data
	eDiagOutDMVolts,	 // DM Volts data
	eDiagOutTT			 // tip/tilt data
};


// Defines

#define DIAG_STRING_MAX_LENGTH 128

#define NO_FILE -1


// Typedefs

// structure for FIFO diagnostic messages being sent to diag (not all of these fields are used for each diagID)
typedef struct
{
	int16	diagID;
	int16	diagStat;
	int16	diagReadNdx;

	int16	stringLength;

	int16	cmdNdx;
	int16	intParam;
	float32	floatParam;
} diagFIFOMsgType;

typedef struct
{
	int16 diagBit;				// bit set in diag and telem commands indicating diag data type
	char  *dirName;				// name of directory for diag data storage
	char  *fileName;			// file name for diag data storage
	int   fileDesc;				// current file descriptor for file to which data is being stored (or NO_FILE)
    int16 sourceNdx;			// index of data source thread that supplies this diag data type
} DiagDataDescType;

typedef struct
{
	int16 diagBits;				// bits for diag data types that this data source thread provides
	int16 saveToFileCntr;		// current count for data that is being saved to a file
	int16 saveToFileMaxCount;	// maximum count for data that is being saved to a file
	int16 *diagActFlag;			// diagnostic action flag (set only on SRT side (except at initialization))
	int16 *diagStatFlag;		// diagnostic status flag (set only on HRT side)
	int16 *diagDiagRate;		// indicates rate at which diagnostic data should be sent to SRT (always 0 or 1)
	int16 *diagDiagCntr;		// counter for diagnostic data being sent to SRT
	int16 *diagTelemRate;		// indicates rate at which telemetry data should be sent to SRT (same as diag data, but at different rate)
	boolean *diagPSFFlag;		// indicates whether data should be sent for PSF estimation
} DiagSourceDescType;


// Global Variables

extern int16 DiagFileSeqNum;  // sequence number, used in file names, for diagnostic data files

extern char CrntDateStr[MAX_FILENAME_LENGTH];  // current date string


// Global Function Prototypes

void *HostDiag( void *arg );
void HostDiagSendStrngNoParm( const char *strngPtr );
void HostDiagSendStrngIntParm( const char *strngPtr, int16 intParam );
void HostDiagSendStrngFltParm( const char *strngPtr, float32 floatParam );

#endif
