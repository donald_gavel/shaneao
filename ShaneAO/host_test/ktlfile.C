#include "ktlfile.h"
#include "hostIConCentral.h"

CKTLFile::CKTLFile(CInterface *pInter, string baseName,  
                   KTLChangeActionFunc af, KTLDelayedResponseFunc dr)
{
    m_pInter   = pInter;
    m_BaseName = baseName;
    m_afFile   = af;
    m_drFile   = dr;

    int WR = CKTLKeyWord::KTLREAD | CKTLKeyWord::KTLWRITE;
    string str = m_BaseName + "FN"; 
    m_pInter->KTLSetup(m_pName   = new CKTLFileString(str.c_str(), -1, "", WR, m_afFile, m_drFile, this));
    str = m_BaseName + "FS";
    m_pInter->KTLSetup(m_pStatus = new CKTLFileInt   (str.c_str(), -1, 0,  WR, m_afFile, m_drFile, this));
}


CKTLFile::~CKTLFile() {
    delete m_pName;
    delete m_pStatus;
    m_pName = NULL;
    m_pStatus = NULL;
    m_pInter = NULL;
}


int CKTLFile::SetFileName(string newval) 
{
    LogWarn("CKTLFile::SetFileName");
    return g_pCCentral->setNEEDFUNC(m_pName, newval);
}


int CKTLFile::SetFileStatus(int newval)
{
    LogWarn("CKTLFile::SetFileStatus");
    return g_pCCentral->setNEEDFUNC(m_pStatus, newval);
}


CKTLFileString::CKTLFileString(const char *name, int key, const char *val, int readWrite, 
                               KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                               CKTLFile *pKTLFile) :
    CKTLString(name, key, val, readWrite, actFunc, drFunc)
{
    LogDebug("CKTLFileString::CKTLFileString");
    m_pKTLFile = pKTLFile;
}

CKTLFileInt::CKTLFileInt(const char *name, int key, int val, int readWrite, 
                               KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                               CKTLFile *pKTLFile) :
    CKTLInt(name, key, val, readWrite, actFunc, drFunc)
{
    LogDebug("CKTLFileInt::CKTLFileInt");
    m_pKTLFile = pKTLFile;
}
