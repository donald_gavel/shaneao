/******************************************************************************

COPYRIGHT (C) 2003
GENERAL STANDARDS CORPORATION and LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			camDrvr.h

AUTHOR:				DATE: 3/17/03

COMPONENT DESCRIPTION:
This is the header file for camDrvr.c.

******************************************************************************/

#ifndef CAMDRVR_H
#define CAMDRVR_H


// Defines

#define NUM_DMA_BUFFS				8		// number of camera DMA buffers
#define NUM_DMA_WORDS				2048	// size of camera DMA buffers in words
#define NUM_DMA_BUFFS_PER_FRAME     2       // number of camera DMA buffers per full camera frame

#define NUM_DMA_CYCLES_TO_IGNORE    2000    // number of camera DMA cycles to ignore when starting up
#define DMA_CYCLE_TO_RESET_FIFO     1000    // when to reset the Rx FIFO when starting up


// Global Variables

extern int16   DMABuffWriteNdx;		 // DMA buffer write index
extern int16   DMABuffReadNdx;		 // DMA buffer read index
extern uint16 *DMABuffPtrs[];		 // DMA buffer pointers
extern int16   DMABuffWriteCntrs[];  // incremented when buff is written to, decremented 
									 // > when read (so cam can check for overruns)

extern int16   CamIntrWatchCntr;  // watchdog counter for camera interrupt

extern int16   CamFirstTimeCntr;    // counts DMA cycles being ignored when starting up
extern boolean CamRxFIFOResetFlag;  // indicates whether Rx FIFO has been reset at startup


// Global Function Prototypes

void CamDrvrSetBits(int regID, uint32 bits);
void CamDrvrClrBits(int regID, uint32 bits);
void CamDrvrFlpBits(int regID, uint32 bits);
void CamDrvrReadRegs(void);
void CamDrvrTickData(void);
void CamDrvrStartData(void);
void CamDrvrStopData(void);
int CamDrvrInit(void);
void CamDrvrCleanup(void);
void *CamDrvrWatch( void *arg );


#endif

