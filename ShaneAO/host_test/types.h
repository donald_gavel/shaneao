/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			types.h

AUTHOR:	DWP			DATE: 12/26/02

FILE DESCRIPTION:
This file contains data type definitions for the LickAO control system.

******************************************************************************/

#ifndef _TYPES_H
#define _TYPES_H		1

typedef short			int16;
typedef unsigned short	uint16;

/*
 *typedef long			int32;
 *typedef unsigned long	uint32;
 */

typedef int			int32;
typedef unsigned int	uint32;


typedef float			float32;
typedef double			float64;

typedef signed short	boolean;

#endif

