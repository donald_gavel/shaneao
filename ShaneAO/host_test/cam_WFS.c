/**********************************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		cam_WFS

AUTHOR:	DHL & DWP	DATE: 10/7/04

COMPONENT DESCRIPTION:
This component contains functions to interface to the SciMeasure camera, which replaces the AOA 
camera, for the LickAO control system.  This SciMeasure camera is designated 'WFS', to distinguish
it from the old AOA camera and from the SciMeasure LTT camera.  

COMPONENT UNITS:
1.	CamWFS()
2.	CamWFS_Init()
3.	CamWFS_InitIntr()
4.	CamWFS_IntrHandler()
5.	CamWFS_ProcCmd()
6.	CamWFS_SendCmd()
7.	CamWFS_LoadDataBuffer()
8.	CamWFS_deIntlvQuad16()
9.	CamWFS_DrvrWatch()

REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_fcntl.h>
#include <rtl_time.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_posixio.h>
#include <rtl_fifo.h>
#include <rtl_math.h>
#include <sys/rtl_mman.h>

#include "types.h"
#include "constant.h"
#include "camDrvr.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"

#include "edt_lnx_kernel.h"

// Defines


// Global Variables
int16 UseAOA = INIT_USE_AOA;  // flag to select between AOA and WFS camera (see IMPORTANT note in cam_WFS.h)
int WfsTotalSize;			  // num bytes in shared memory region
u_char *WfsBaseAddr;  		  // pointer to start of shared memory

int WFS_irq = -1;
void *WFS_dev;


// Local Variables
static uint16 *buf_array[MAXNUMBUFS];

static const int16 ValidFrameRates[] = { 50, 100, 200, 250, 350, 500, 750, 1000, -1 };  // -1 is the terminator

static int16 CamWFSIntrWatchCntr = -1;  // watchdog counter for camera interrupt
static unsigned long oldAffinity;
static int16 diagTelemCntr = 0;

static int BadCntr = 0, SkipCntr = 0, SyncErrCntr = 0, BehindCntr = 0;  //XXX

static int LastDoneProc = 0;

static int16 deIntlvdData[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW];  // deinterleaved camera data (this is out here so as not to 
															 // > make the stack too big)

// static int openCntr = 0, closeCntr = 0;  //XXXTRACER
// static uint32 WFS_TimeKeeper = 0;  //XXXTRACER


// Local Function Prototypes
static void CamWFS_Init( void );
static unsigned int CamWFS_IntrHandler(unsigned int irq, struct rtl_frame *regs);

static void CamWFS_ProcCmd( int16 cmdNdx );
static void CamWFS_SendCmd( int16 cmd, int16 param );

static void CamWFS_LoadDataBuffer( int bufNdx );

static void CamWFS_deIntlvQuad16(uint16 *src, int width, int rows, int16 *dest);  //XXX060404 
// static void CamWFS_deIntlvQuad16(uint16 *src, int width, int rows, int16 dest[][NUM_PIX_IN_ROW]);  //XXX060404

/**********************************************************************************************

UNIT NAME:  CamWFS()

UNIT DESCRIPTION:
This unit is the camera thread mainline.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamWFS( void *arg )
{
	int curbuf;
//	struct rtl_timespec next;

	static int doneCount = 0;
//	static int doneCount = 0, goodCntr = 0, localSyncErrCntr = 0, localSyncErrCntrCntr = 0, missedCntr = 0;  //XXXTRACER
//	static int secsCntr = 0;  //XXXTRACER
//	static int prevOpenCntr = 0, prevCloseCntr = 0;  //XXXTRACER
//	static int prevSecsCntr = 0;  //XXXTRACER

	CamDataPtr->intrIgnore = false;  //XXXINTIG
	CamDataPtr->initDone = false;

//	initialize variables used by the camera thread (note that this is in cam.c)
	CamInit();

//	perform WFS camera initializations (including initializing the camera interrupt)
	CamWFS_Init();

//	send commands to initialize the EDT camera interface in user space
	CamWFS_SendCmd (eWFSInitialize2, 0);
	CamWFS_SendCmd (eWFSSetRep, CamFrameRate);
	CamWFS_SendCmd (eWFSSetAtten, (3 - CamGain));  // IMPORTANT: this will set CamDataPtr->initDone

	CamWFSIntrWatchCntr = 0;

//	enter infinite loop
	while (1)
	{
//		this semaphore is currently activated in two places:
//		> in sysCon in response to WFS Host commands and in intr_handler in response to incomming frame data
		if (rtl_sem_wait(&WFSSem) < 0)
			SysConSendStrngIntParm("Error waiting on WFSSem, errno = ", rtl_errno);

//		if a command is pending, call CamWFS_ProcCmd() to process it
		else if (CamDataPtr->cmdStart != eCmdNone)
		{
			CamWFS_ProcCmd(CamDataPtr->cmdStart);
			CamDataPtr->cmdStart = eCmdNone;
		}
		else // process the interrupt
		{
			doneCount = edt_get_done(WFS_dev);
			if (doneCount < 10)  // doneCount will go to zero when the mode is changed; skip the first few (see doneCount in CamWFS_IntrHandler())
				SkipCntr++;
			else
			{
				if ((doneCount - LastDoneProc) != 1)  // we've missed one or more interrupts
				{
//XXX					++localSyncErrCntr;
//XXX					missedCntr += doneCount - LastDoneProc - 1;
				}
				else
				{
					curbuf = (doneCount - 1) % WFSnumbufs;  // the last DMA buffer is one less than DoneCount
					if (UseAOA == 0) 
						CamWFS_LoadDataBuffer(curbuf);
				}
			}
			LastDoneProc = doneCount;

/*/XXXTRACER start
			if (++goodCntr >= CamFrameRate)  //XXX
			{
				if (openCntr != prevOpenCntr)
				{
					prevOpenCntr = openCntr;
					prevSecsCntr = secsCntr;
				}
				if (closeCntr != prevCloseCntr)
				{
					prevCloseCntr = closeCntr;
					prevSecsCntr = secsCntr;
				}

				if ((BadCntr > 0) || (localSyncErrCntr > 0) || (BehindCntr > 0)) //XXX
				{
					if (localSyncErrCntr > 0)
						localSyncErrCntrCntr++;

					rtl_printf ("<0>wg: %d, b: %d, skp: %d, sp: %d, spc: %d, bh: %d, o: %d, c: %d, secs: %d\n", 
								goodCntr, BadCntr, SkipCntr, localSyncErrCntr, localSyncErrCntrCntr, BehindCntr, 
								openCntr, closeCntr, secsCntr);  //XXX
				}
				++secsCntr;
				goodCntr = 0;  //XXX
				BadCntr = 0;  //XXX
				SkipCntr = 0;  //XXX
				SyncErrCntr = 0;  //XXX
				localSyncErrCntr = 0;  //XXX
				BehindCntr = 0;  //XXX
				missedCntr = 0;
				WFS_TimeKeeper = 0;
			} 
//XXXTRACER end */
		}
	}
	return 0;
}


/**********************************************************************************************

UNIT NAME:  CamWFS_Init()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the camera thread.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamWFS_Init( void )
{
	int i;
	int retVal;
	int bufPages;
	struct rtl_timespec next;

//	map local arrays to shared memory
	bufPages = WFSbufsize / PAGE_SIZE;
	if (WFSbufsize % PAGE_SIZE)
		bufPages++;
	buf_array[0] = (uint16 *)WfsBaseAddr;
	for (i = 1; i < WFSnumbufs; i++)
	    buf_array[i] = buf_array[i-1] + (bufPages * PAGE_SIZE / 2);

//	initialize variables
	CamDataPtr->cmdStart = eCmdNone;
	CamDataPtr->cmdCrnt = eCmdNone;

	CamDataPtr->writeNdx = NUM_CAM_BUFFS - 1;
	CamDataPtr->readNdx = NUM_CAM_BUFFS - 1;

	CamDataPtr->diagActFlag = eDiagActNoAction;
	CamDataPtr->diagStatFlag = eDiagStatStandby;
	CamDataPtr->diagDiagRate = 0;
    CamDataPtr->diagDiagCntr = 0;
	CamDataPtr->diagTelemRate = 0;

	CamDataPtr->intrIgnore = false;  //XXXINTIG

	for (i = 0; i < NUM_CAM_BUFFS; i++)
    {
        CamDataPtr->camBuffNdxs[i] = NUM_CAM_BUFFS - 1;
        CamDataPtr->camBuffs[i].inUseFlag = false;
        CamDataPtr->camBuffs[i].diagIndicator = eDiagIndNotInUse;
        CamDataPtr->camBuffs[i].subapRowWriteNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
        CamDataPtr->camBuffs[i].subapRowReadNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
    }

	CamDataPtr->crntRS170WriteBuff = 0;  //XXXRS170
	CamDataPtr->crntRS170ReadBuff = 1;  //XXXRS170
	CamDataPtr->RS170crtGain = 0;  //XXXRS170

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
	while (!OkayToFinishInit)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in CamWFS_Init(), retVal = ", retVal);
    }

//	send command to do the first step of EDT camera interface initialization in user space 
//	> and wait a second for that to happen
	CamWFS_SendCmd (eWFSInitialize1, 0);
	next.tv_sec = 1;
	next.tv_nsec = 0;
	if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
		SysConSendStrngIntParm("Error from nanosleep() in CamWFS(), retVal = ", retVal);

    CamWFSInitDone = true;
	rtl_printf ("<0>CamWFSInit done\n");
}


/**********************************************************************************************

UNIT NAME:  CamWFS_InitIntr()

UNIT DESCRIPTION:
The purpose of this unit is to initialize the interrupt for the WFS camera.  

IMPORTANT: This has to be called within the scope of init_module().  That's why it's 
		   a separate function.  

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void CamWFS_InitIntr( void )
{
	int retVal;
	unsigned long affinity = 1 << HRT_CPU;

//	get the IRQ# for WFS from the EDT driver and install a handler for it
	if (UseAOA == 0)
	{
		WFS_irq = edt_get_irq_value(WFS_UNIT_NUM);
		rtl_printf ("<0>WFS camera using irg %d\n", WFS_irq);  //XXX
		if (WFS_irq >= 0)
		{
			WFS_dev = edt_get_dev_from_irq(WFS_irq);
		    edt_take_interrupt(WFS_irq, WFS_dev);
			if ((retVal = rtl_request_irq( WFS_irq, CamWFS_IntrHandler )) != 0)
				rtl_printf("<0>WFS failed to get irq %d\n", WFS_irq);
		    rtl_irq_set_affinity(WFS_irq, &affinity, &oldAffinity);
		}
	}
}


/**********************************************************************************************

UNIT NAME:  CamWFS_IntrHandler()

UNIT DESCRIPTION:
This routine is called in response to an interrupt generated by the EDT driver upon receipt of a
complete frame of camera data.  It's sole purpose is to activate a semaphore which triggers the
collection of the camera frame data.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static unsigned int CamWFS_IntrHandler(unsigned int irq, struct rtl_frame *regs)
{
	static int doneCount = 0, lastDoneIntr = 0;  //XXX051011

//	rtl_printf ("I");  //XXX060711
//	bump up WFS camera watchdog counter
	CamWFSIntrWatchCntr++;

//	let EDT do its interrupt processing
	edt_intr_process(WFS_dev);

//	ignore this interrupt if we're doing serial communications
	if (CamDataPtr->intrIgnore || CamLTTPtr->intrIgnore)  //XXXINTIG
	{
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
		return 0;
	}

//	get the current done counter
	doneCount = edt_get_done(WFS_dev);
	if (doneCount < 7)  // doneCount goes to zero when switching modes, let it settle (see doneCount in CamWFs())
	{
		lastDoneIntr = doneCount;
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
	}
	else if (lastDoneIntr == doneCount)  // interrupt could be a serial interrupt or sg ready (not sure what "sg ready" is)
	{
		++BadCntr;
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
	}
	else if ((doneCount - lastDoneIntr) != 1)  // we've missed one or more interrupts (could send this along; but don't for now)
	{
		++SyncErrCntr;
		lastDoneIntr = doneCount;
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
	}
	else if (((doneCount - LastDoneProc) > 1) && ((doneCount - LastDoneProc) < 10))  // camWFS() is behind, so don't send
	{
		++BehindCntr;
		lastDoneIntr = doneCount;
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
	}
	else  // everything's okay, so wake up CamWFS()
	{
//		rtl_printf ("D");  //XXX060711
		lastDoneIntr = doneCount;
		rtl_hard_enable_irq(irq);  // re-enable the rtl irq
		rtl_sem_post(&WFSSem);
	}

	return 0;
}


/**********************************************************************************************

UNIT NAME:  CamWFS_ProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process camera commands.

UNIT INPUTS:
	cmdNdx - index into CmdTable[] for the command to be processed

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamWFS_ProcCmd( int16 cmdNdx )
{
	int    retVal;  //XXX060501
	struct rtl_timespec next;  //XXX060501

//	switch on command ID corresponding to command index
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		   // abort long commands
			SysConSendStrngNoParm("Error: unexpected command abort received by cam");
			break;

		case eCmdCamWFSInit:   // initialize SciMeasure WFS camera
			CamWFS_SendCmd(eWFSInitialize1, 0);
			next.tv_sec = 1;  //XXX060501
			next.tv_nsec = 0;  //XXX060501
			if ((retVal = rtl_nanosleep(&next, NULL)) < 0)  //XXX060501
				SysConSendStrngIntParm("Error from nanosleep() in CamWFS(), retVal = ", retVal);  //XXX060501

			CamWFS_SendCmd(eWFSInitialize2, 0);
			next.tv_sec = 7;  //XXX060630
			next.tv_nsec = 0;  //XXX060630
			if ((retVal = rtl_nanosleep(&next, NULL)) < 0)  //XXX060630
				SysConSendStrngIntParm("Error from nanosleep() in CamWFS(), retVal = ", retVal);  //XXX060630

/*/XXX060501*/			CamWFS_SendCmd(eWFSSetRep, CamFrameRate);
/*/XXX060501*/			CamWFS_SendCmd(eWFSSetAtten, (3 - CamGain));
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamRate:      // set frame rate of the camera (per second)
			CamFrameRate = CamDataPtr->cmdIntParam;
			CamFrameRateNdx = 0;  // find a legal camera rate
			while ((CamFrameRate > ValidFrameRates[CamFrameRateNdx]) && (ValidFrameRates[CamFrameRateNdx + 1] != -1))
				CamFrameRateNdx ++;
			CamFrameRate = ValidFrameRates[CamFrameRateNdx];
			CamWFS_SendCmd (eWFSSetRep, CamFrameRate);
			FileDataPut(eFileTypeParms);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamGain:      // set camera gain (0, 1, 2, or 3)
			CamGain = CamDataPtr->cmdIntParam;
			CamWFS_SendCmd (eWFSSetAtten, (3 - CamGain));
			FileDataPut(eFileTypeParms);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamCRTGain:   // set camera CRT gain (0 to 4)
//XXXRS170			SysConSendStrngNoParm("Error: CRT Gain not supported by SciMeasure camera.");
			CamCRTGain = CamDataPtr->cmdIntParam;	//XXXRS170
			CamDataPtr->RS170crtGain = CamCRTGain;  //XXXRS170
			FileDataPut(eFileTypeParms);			//XXXRS170
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSRate:	   // read frame rate from camera
			CamWFS_SendCmd (eWFSReadRep, 0);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSGain:     // read gain from camera
			CamWFS_SendCmd (eWFSReadAtten, 0);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSCRTGain:  // read CRT gain from camera
			SysConSendStrngNoParm("Error: CRT Gain not supported by SciMeasure camera.");
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		default:
			SysConSendStrngIntParm("Error: unexpected command received by cam, cmdNdx = ", cmdNdx);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CamWFS_SendCmd()

UNIT DESCRIPTION:
The purpose of this unit is to send commands to the SRT side (by using the FIFO diagnostic 
message process) to write messages to the camera and/or to change the DMA mapping (to accommodate 
changes in frame size).

UNIT INPUTS:
1.	cmd		command to be sent the the SRT side
2.	param	parameter to go along with the command

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamWFS_SendCmd( int16 cmd, int16 param )
{
	static diagFIFOMsgType diagFIFOMsg;

//	set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagWFSCmd;
	diagFIFOMsg.cmdNdx = cmd;
	diagFIFOMsg.intParam = param;

    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm("Error writing CamWFS command to diag FIFO, errno = ", rtl_errno);
}


/**********************************************************************************************

UNIT NAME:  CamWFS_CamWFS_LoadDataBuffer

UNIT DESCRIPTION:
The purpose of this unit is to process frames from the SciMeasure WFS camera.

NOTE: for testing and ease-of-implementation reasons, this function has been written to be 
100% compatible with the AOA version of Cent().  For that reason, it is quite inefficient.
When the AOA camera is history, it would make much more sense to send frames to Cent() in 
their entirety.  

UNIT INPUTS:
1.	bufNdx - index of the SciMeasure camera buffer to process

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamWFS_LoadDataBuffer(int bufNdx)
{
	boolean sendTelemData;
	int16 i, j;
	int16 buffCntr, buffNdx;
	int16 firstFree, firstFreeButWithDiag;
	int16 subapNdx, subapRowNdx, unusedRowNdx, subapDataNdx;
	static diagFIFOMsgType diagFIFOMsg;
	int16 *DataPtr;
    CamBuffType *camBuffPtr;
	static int16 RS170WriteCntr = 1000;  //XXXRS170

//	find the next available WFS camera data buffer
//	if there isn't an available camera data buffer, report an error
//	if the buffer we're about to use has unread diag data in it, initiate a diag data resync
    camBuffPtr = &CamDataPtr->camBuffs[CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx]];
    buffCntr = 0;
    buffNdx = CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx];
    firstFree = -1;
    firstFreeButWithDiag = -1;
    while ((buffCntr < NUM_CAM_BUFFS) && (firstFree < 0))
    {
        if (++buffNdx >= NUM_CAM_BUFFS)
            buffNdx = 0;
        if (!CamDataPtr->camBuffs[buffNdx].inUseFlag)
        {
            if ((CamDataPtr->diagStatFlag != eDiagStatOperating) ||
                (CamDataPtr->camBuffs[buffNdx].diagIndicator == eDiagIndNotInUse))
                firstFree = buffNdx;
            else if (firstFreeButWithDiag < 0)
                firstFreeButWithDiag = buffNdx;
        }
        buffCntr++;
    }

    if ((firstFree < 0) && (firstFreeButWithDiag < 0))
	{
        SysConSendStrngNoParm("Error: camWFS buffers all full");
        camBuffPtr = &CamDataPtr->camBuffs[0];  //XXX XXX
	}
    else
    {
        if (++CamDataPtr->writeNdx >= NUM_CAM_BUFFS)
            CamDataPtr->writeNdx = 0;

        if (firstFree < 0)
        {
            firstFree = firstFreeButWithDiag;

            CamDataPtr->diagStatFlag = eDiagStatResyncing;
            diagFIFOMsg.diagID = eDiagCamData;
            diagFIFOMsg.diagStat = eDiagStatResyncing;
            diagFIFOMsg.diagReadNdx = firstFree;
            if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
                SysConSendStrngIntParm("Error writing camera resync to diag FIFO, errno = ", rtl_errno);
            SysConSendStrngNoParm("Warning: WFS camera diagnostic data is resyncing");
        }
        CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx] = firstFree;
        camBuffPtr = &CamDataPtr->camBuffs[firstFree];
        camBuffPtr->inUseFlag = true;
    }

//	if diag data transfer is to be restarted and we're not operating already
//		indicate that we're operating
//	else if diag data transfer is to enter standby and we're not in standby already
//		indicate that we're operating in standby
	if ((CamDataPtr->diagActFlag == eDiagActRestart) && (CamDataPtr->diagStatFlag != eDiagStatOperating))
    {
		CamDataPtr->diagStatFlag = eDiagStatOperating;
        CamDataPtr->diagActFlag = eDiagActNoAction;
    }
	else if ((CamDataPtr->diagActFlag == eDiagActEnterStandby) && (CamDataPtr->diagStatFlag != eDiagStatStandby))
    {
		CamDataPtr->diagStatFlag = eDiagStatStandby;
        CamDataPtr->diagActFlag = eDiagActNoAction;
    }

	sendTelemData = false;
	// SysConSendStrngIntParm("diagTelemRate ", CamDataPtr->diagTelemRate);
	if ((CamDataPtr->diagTelemRate > 0) && 
		(++diagTelemCntr >= (CamFrameRate / CamDataPtr->diagTelemRate)))
	{
		diagTelemCntr = 0;
		sendTelemData = true;
		// SysConSendStrngIntParm("	LoadDataBuffer ", bufNdx);
		// SysConSendStrngIntParm("	diagStatFlag ", CamDataPtr->diagStatFlag);
		// SysConSendStrngIntParm("	eDiagStatOperating ", eDiagStatOperating);
		// SysConSendStrngIntParm("	diagDiagCntr ", CamDataPtr->diagDiagCntr);
	}

//	deinterleave the data, move it into a camera data buffer, and post to let the centroider know that it's available
/*XXX48x48*/	CamWFS_deIntlvQuad16(buf_array[bufNdx], NUM_PIX_IN_ROW_48, NUM_ROWS_IN_CAM_48, &deIntlvdData[0][0]);
//*XXX64x64*/	CamWFS_deIntlvQuad16(buf_array[bufNdx], NUM_PIX_IN_ROW, NUM_ROWS_IN_CAM, &deIntlvdData[0][0]);

//	move the unused data first so that it'll be in the right place if we're computing a cflat
/*XXX64x64* /
	DataPtr = deIntlvdData[0] + FIRST_UNUSED_CAM_ROW_TOP * NUM_PIX_IN_ROW;  // unused data, top (empty for AOA)
	for (unusedRowNdx = 0; unusedRowNdx < NUM_UNUSED_CAM_ROWS_TOP; ++unusedRowNdx)
	{
		for (subapDataNdx = 0; subapDataNdx < NUM_PIX_IN_ROW; ++subapDataNdx)
			camBuffPtr->unusedDataTop[unusedRowNdx][subapDataNdx] = *DataPtr++;
	}
	DataPtr = deIntlvdData[0] + FIRST_UNUSED_CAM_ROW_BOT * NUM_PIX_IN_ROW;  // unused data, bottom
	for (unusedRowNdx = 0; unusedRowNdx < NUM_UNUSED_CAM_ROWS_BOT; ++unusedRowNdx)
	{
		for (subapDataNdx = 0; subapDataNdx < NUM_PIX_IN_ROW; ++subapDataNdx)
			camBuffPtr->unusedDataBot[unusedRowNdx][subapDataNdx] = *DataPtr++;
	}

	DataPtr = deIntlvdData[0] + FIRST_USED_CAM_ROW * NUM_PIX_IN_ROW;  // used data
//XXX64x64*/
/*XXX48x48*/	DataPtr = deIntlvdData[0];  // used data
	for (subapNdx = 0; subapNdx < NUM_SUBAP_ROWS_PER_FRAME; ++subapNdx)
	{
		for (subapRowNdx = 0; subapRowNdx < NUM_CAM_ROWS_PER_SUBAP; ++subapRowNdx)
		{
//*XXX64x64*/			for (subapDataNdx = 0; subapDataNdx < NUM_PIX_IN_ROW; ++subapDataNdx)
/*XXX48x48*/			for (subapDataNdx = FIRST_USED_CAM_COL; subapDataNdx < LAST_USED_CAM_COL; ++subapDataNdx)
				camBuffPtr->data[subapNdx][subapRowNdx][subapDataNdx] = *DataPtr++;
		}
        camBuffPtr->subapRowWriteNdx = subapNdx;
		if (rtl_sem_post(&CentSem) < 0)
		{
			SysConSendStrngIntParm("Error posting to CentSem in cam_WFS, errno = ", rtl_errno);
            return;  // terminate, we've got an error (very unlikely!)
		}
	}

//XXXRS170 start
	if (++RS170WriteCntr >= (CamFrameRate / 25))  //XXX when frame rate is changed, set RS170SwitchCount, or something
	{											  //XXX > like that, to avoid doing this division every time
		RS170WriteCntr = 0;

		CamDataPtr->crntRS170WriteBuff ^= 0x1;
		DataPtr = deIntlvdData[0];  // used data
//*XXX64x64*/		for (i = 0; i < NUM_ROWS_IN_CAM; i++)
/*XXX48x48*/		for (i = FIRST_USED_CAM_ROW; i < LAST_USED_CAM_ROW; i++)
		{
//*XXX64x64*/			for (j = 0; j < NUM_PIX_IN_ROW; j++)
/*XXX48x48*/			for (j = FIRST_USED_CAM_COL; j < LAST_USED_CAM_COL; j++)
				CamDataPtr->RS170Buffs[CamDataPtr->crntRS170WriteBuff][i][j] = *DataPtr++;
		}
		CamDataPtr->crntRS170ReadBuff = CamDataPtr->crntRS170WriteBuff;
	}
//XXXRS170 end

//	if diagnostic data transfer (to the SRT) is operating and a message should be sent
//		send the camera data off to diag
	if ((CamDataPtr->diagStatFlag == eDiagStatOperating) && 
		((CamDataPtr->diagDiagCntr > 0) || sendTelemData))
	{
		if (sendTelemData)
			camBuffPtr->diagIndicator = eDiagIndTelem;
		else
			camBuffPtr->diagIndicator = eDiagIndInUse;

		diagFIFOMsg.diagID = eDiagCamData;
		diagFIFOMsg.diagStat = eDiagStatOperating;
		diagFIFOMsg.diagReadNdx = CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx];

		if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
		{
			camBuffPtr->diagIndicator = eDiagIndNotInUse;
			SysConSendStrngIntParm("Error writing camera data to diag FIFO, errno = ", rtl_errno);
		}
        else
        {
            if (CamDataPtr->diagDiagCntr > 0)
                CamDataPtr->diagDiagCntr--;
        }
	}
}


/**********************************************************************************************

UNIT NAME:  CamWFS_deIntlvQuad16()

UNIT DESCRIPTION:
The purpose of this unit is to de-interleave the SciMeasure camera data.  

UNIT INPUTS:
1.	src   - pointer to the ring buffer
2.	width - width of the frame
3.	rows  - number of rows in the frame

UNIT OUTPUTS:
1.	dest - pointer to the output array


UNIT ERROR CODES:
	None

**********************************************************************************************/

/*/XXX060404 start -- XXX060623 this code is to rotate the SMC wfs image 90 degrees; once the camera is tested in its
//								> proper orientation, this can be removed
static void CamWFS_deIntlvQuad16(uint16 *src, int width, int rows, int16 dest[][NUM_PIX_IN_ROW])
{
    int i, j, k;
    int nRows, nCols, nHalfRows, nHalfCols;
	int ul, ur, ll, lr;  // indexes for quadrants on the CCD (upper left, upper right, lower left, lower right)

	int16 iFlipped;
	int16 tmpDest[NUM_ROWS_IN_CAM * NUM_PIX_IN_ROW];

    nRows = rows;
    nCols = width;
    nHalfRows = nRows >> 1;
    nHalfCols = nCols >> 1;
	i = 0;
	for (j = 0; j < nHalfRows; j++)
	{
		ul = j * nCols;
		ur = ul + nCols - 1;
		ll = (nRows - 1 - j) * nCols;
		lr = ll + nCols - 1;

		for (k = 0; k < nHalfCols; k++)
		{
		    tmpDest[ul++] = src[i++];
		    tmpDest[ur--] = src[i++];
		    tmpDest[ll++] = src[i++];
		    tmpDest[lr--] = src[i++];
		}
	}

	for (i = 0; i < nRows; i++)
	{
		k = i * nCols;
		iFlipped = nRows - 1 - i;
		for (j = 0; j < nCols; j++)
//			dest[i][j] = tmpDest[k + j];
//			dest[j][i] = tmpDest[k + j];
//			dest[nCols - 1 - j][i] = tmpDest[k + j];
			dest[j][iFlipped] = tmpDest[k + j];
	}
}
//XXX060404 end -- XXX060623 end */
//XXX060404 start
static void CamWFS_deIntlvQuad16(uint16 *src, int width, int rows, int16 *dest)
{
    int i, j, k;
    int nRows, nCols, nHalfRows, nHalfCols;
	int ul, ur, ll, lr;  // indexes for quadrants on the CCD (upper left, upper right, lower left, lower right)

    nRows = rows;
    nCols = width;
    nHalfRows = nRows >> 1;
    nHalfCols = nCols >> 1;
	i = 0;
	for (j = 0; j < nHalfRows; j++)
	{
		ul = j * nCols;
		ur = ul + nCols - 1;
		ll = (nRows - 1 - j) * nCols;
		lr = ll + nCols - 1;

		for (k = 0; k < nHalfCols; k++)
		{
		    dest[ul++] = src[i++];
		    dest[ur--] = src[i++];
		    dest[ll++] = src[i++];
		    dest[lr--] = src[i++];
		}
	}
}
//XXX060404 end */

/**********************************************************************************************

UNIT NAME:  CamWFS_DrvrWatch()

UNIT DESCRIPTION:
This is a thread used as a camera watchdog.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamWFS_DrvrWatch( void *arg )
{
	int retVal;
	struct rtl_timespec next;

//  spin on the InitDone flags for the other threads
    while (!TiptTiltAPDInitDone || !CamInitDone || !CentInitDone || !ReconInitDone || !CamLTTInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm("Error from nanosleep() in CamWatch(), retVal = ", retVal);
    }

//	enter infinite test loop
	while (1)
	{
//		wait for 5 seconds (increased from 1 sec to accommodate longer delay when switching camera programs)
		next.tv_sec = 5;
		next.tv_nsec = 0;
   		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep(), retVal = ", retVal);

//		make sure we've received camera frames and, if not
//			notify the user
        if (CamWFSIntrWatchCntr == 0)
        {
            CamWFSIntrWatchCntr = -1;
			SysConSendStrngNoParm("Notification: SMC WFS camera watchdog timeout");
		}
        else if (CamWFSIntrWatchCntr > 0)
            CamWFSIntrWatchCntr = 0;
	}
}
