/**********************************************************************************************

COPYRIGHT (C) 2003
GENERAL STANDARDS CORPORATION and LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		camDrvr

AUTHOR:				DATE: 3/17/03

COMPONENT DESCRIPTION:
This component is the driver for the General Standards PMC64-HPDI32ALT DMA board that receives
data from the AOA camera.  It began as hpdi32.c, provided by General Standards (Revision: 8, 
7/29/02 4:40p) and has been heavily modified.  The original code was used with permission.

EXTERNALLY ACCESSIBLE UNITS:
1.	CamDrvrSetBits()
2.	CamDrvrClrBits()
3.	CamDrvrFlpBits()
4.	CamDrvrReadRegs()
5.	CamDrvrTickData()
6.	CamDrvrStartData()
7.	CamDrvrStopData()
8.	CamDrvrInit()
9.	CamDrvrCleanup()
10. CamDrvrWatch()

REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_time.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
//#include <rtl_errno.h>

#include <asm/system.h>
#include <asm/types.h>
#include <asm/uaccess.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/page.h>

#include <linux/delay.h>
#include <linux/ioport.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/pci.h>
#include <linux/proc_fs.h>
#include <linux/sched.h>
#include <linux/wait.h>
#include <linux/wrapper.h>
#include <linux/slab.h>
#include <linux/ioctl.h>
#include <linux/types.h>
#include <linux/config.h>
#include <linux/mm.h>

#include "types.h"
#include "constant.h"
#include "camDrvrL.h"
#include "camDrvr.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"


/* #defines	**************************************************************/

#define	MODULE_NAME			"AOControlHard"

#define	MAX_DEVICES			1  // IMPORTANT: this must be changed if adding more PMC64 boards (other changes needed too!)
							   // IMPORTANT: this code has not been tested with more than 1 PMC64 board

#define	GSC_REG_BLOCK_SIZE	512	/* Register block size. */

/* GSC IRQ items. */
#define	IRQ_GSC_FAIL		(-1)	/* Request failed. */
#define	IRQ_GSC_IRQ			1		/* Got the interrupt. */

/* DMA items */
#define	_DMA_READ			0x1
#define	_DMA_WRITE			0x2

#define	DMA_CSR_ENABLE		HPDI32_ENCODE(1,0,0)
#define	DMA_CSR_START		HPDI32_ENCODE(1,1,1)
#define	DMA_CSR_ABORT		HPDI32_ENCODE(1,2,2)
#define	DMA_CSR_CLEAR		HPDI32_ENCODE(1,3,3)
#define	DMA_CSR_DONE		HPDI32_ENCODE(1,4,4)

#define	DMA_DPR_END_OF_CHAIN		HPDI32_ENCODE(1,1,1)
#define	DMA_DPR_TERMINAL_COUNT_IRQ	HPDI32_ENCODE(1,2,2)
#define	DMA_DPR_HOST_TO_BOARD		HPDI32_ENCODE(0,3,3)
#define	DMA_DPR_BOARD_TO_HOST		HPDI32_ENCODE(1,3,3)

#define	DMA_MODE_SIZE_32_BITS			HPDI32_ENCODE(3,1,0)
#define	DMA_MODE_INPUT_ENABLE			HPDI32_ENCODE(1,6,6)
#define	DMA_MODE_BURSTING_LOCAL			HPDI32_ENCODE(1,8,8)
#define	DMA_MODE_INTERRUPT_WHEN_DONE	HPDI32_ENCODE(1,10,10)
#define	DMA_MODE_LOCAL_ADRESS_CONSTANT	HPDI32_ENCODE(1,11,11)
#define	DMA_MODE_STANDARD_DMA			HPDI32_ENCODE(0,12,12)
#define	DMA_MODE_DEMAND_MODE_DMA		HPDI32_ENCODE(1,12,12)
#define	DMA_MODE_PCI_INTERRUPT_ENABLE	HPDI32_ENCODE(1,17,17)

/* PLX EEPROM/Control register bits. */
#define	CNTRL_EEPROM_RELOAD		HPDI32_ENCODE(1,29,29)
#define	CNTRL_SOFTWARE_RESET	HPDI32_ENCODE(1,30,30)
#define	CNTRL_RESET_DONE		HPDI32_ENCODE(1,31,31)

/* PLX Interrupt Control/Status Register bits. */
#define	INTCSR_PCI_INT_ENABLE			HPDI32_ENCODE(1,8,8)
#define	INTCSR_LOCAL_INT_ENABLE			HPDI32_ENCODE(1,11,11)
#define	INTCSR_LOCAL_INT_OUTPUT_ENABLE	HPDI32_ENCODE(1,16,16)
#define	INTCSR_DMA_0_IRQ_ENABLE			HPDI32_ENCODE(1,18,18)
#define	INTCSR_DMA_1_IRQ_ENABLE			HPDI32_ENCODE(1,19,19)
#define	INTCSR_DMA_0_IRQ_ACTIVE			HPDI32_ENCODE(1,21,21)
#define	INTCSR_DMA_1_IRQ_ACTIVE			HPDI32_ENCODE(1,22,22)

/* Interrupt configuration macros. */
#define	_INT_INDEX_FRAME_VALID_S	0
#define	_INT_INDEX_FRAME_VALID_E	1
#define	_INT_INDEX_D1				2
#define	_INT_INDEX_D2				3
#define	_INT_INDEX_D3				4
#define	_INT_INDEX_D4				5
#define	_INT_INDEX_D5				6
#define	_INT_INDEX_D6				7
#define	_INT_INDEX_TX_FIFO_EMPTY	8
#define	_INT_INDEX_TX_FIFO_AE		9
#define	_INT_INDEX_TX_FIFO_AF		10
#define	_INT_INDEX_TX_FIFO_FULL		11
#define	_INT_INDEX_RX_FIFO_EMPTY	12
#define	_INT_INDEX_RX_FIFO_AE		13
#define	_INT_INDEX_RX_FIFO_AF		14
#define	_INT_INDEX_RX_FIFO_FULL		15

#define	_INT_EL_EDGE	0x0
#define	_INT_EL_LEVEL	0x1

#define	_INT_HL_HIGH	0x0
#define	_INT_HL_LOW		0x1


/* typedefs	**************************************************************/

typedef struct
{
	unsigned long	offset;
	unsigned long	flags;	/* The flags must match. */
	long		    size;	/* The size must be the same. */
} dev_bar_dat_t;

typedef struct
{
	__u32	reg;
	__u32	value;
	__u32	mask;	/* Only for MOD requests. */
} hpdi32_reg_t;

typedef struct
{
	unsigned int		flags;
	int					int_enable;

	struct
	{
		unsigned long	mode_32;	/* DMAMODEx */
		unsigned long	padr_32;	/* DMAPADRx */
		unsigned long	ladr_32;	/* DMALADRx */
		unsigned long	siz_32;		/* DMASIZx */
		unsigned long	dpr_32;		/* DMADPRx */
		unsigned long	csr_8;		/* DMACSRx */
	} vaddr;
} dev_dma_t;

typedef struct
{
	unsigned long	reg;
	unsigned long	value;
} dev_reg_id_t;

typedef struct
{
	u32	address;	/* The base address. */
	void* vaddr;	/* The virtual address (memory only). */
	u32	flags;		/* The BAR address flags (i.e. TYPE, ...). */
	u32	size;		/* The size of the region. */
	u32	requested;	/* Have we reserved the region? */
} dev_region_t;

typedef struct
{
	unsigned long		reg;
	unsigned long		type;
	unsigned long		size;	/* 1, 2 or 4 bytes  */
	unsigned long		offset;	/* Align per size.  */
	unsigned long		vaddr;	/* Virtual address. */
	const dev_region_t*	region;
} dev_reg_t;

typedef enum
{
	HPDI32_32_BIT,
	HPDI32_64_BIT
} dev_type_t;

typedef struct
{
	struct pci_dev*		pci;		/* For PCI access. */
	dev_type_t			type;		/* HPDI32 board type. */
	dev_region_t		plx_mem;	/* PLX features I/O memory. */
	dev_region_t		plx_io;		/* PLX features I/O ports. */
	dev_region_t		gsc_mem;	/* GSC features I/O memory. */

	struct
	{
		dev_dma_t	channel[2];
	} dma;

	struct
	{
		unsigned char	number;			/* IRQ number. */
		u32				driver_gsc;		/* Driver used. */
		u32				user_gsc;		/* User used. */
		u32				user_other;		/* User used. */
		u32				status_gsc;		/* Logged IRQs. */
		u32				status_other;	/* Logged IRQs. */
	} irq;

	struct
	{
		unsigned long	plx_cntrl_32;	/* Serial EEPROM Control */
		unsigned long	plx_intcsr_32;	/* Interrupt Control/Status */

		unsigned long	plx_dmaarb_32;	/* DMA Mode Arbitration */
		unsigned long	plx_dmacsr0_8;	/* DMA 0 Command/Status */
		unsigned long	plx_dmacsr1_8;	/* DMA 1 Command/Status */
		unsigned long	plx_dmathr_32;	/* DMA Threshold */

		unsigned long	gsc_frr_32;		/* Firmware Revision    FRR */
		unsigned long	gsc_bcr_32;		/* Board Control        BCR */
		unsigned long	gsc_bsr_32;		/* Board Status         BSR */
		unsigned long	gsc_tar_32;		/* Tx Almost Register   TAR */
		unsigned long	gsc_rar_32;		/* Rx Almost Register   RAR */
		unsigned long	gsc_fsr_32;		/* Features Register    FSR */
		unsigned long	gsc_fdr_32;		/* FIFO Data            FDR */

		unsigned long	gsc_rscr_32;	/* Rx Status Counter    RSCR */
		unsigned long	gsc_rlcr_32;	/* Rx Line Counter      RLCR */
		unsigned long	gsc_icr_32;		/* Interrupt Control    ICR */
		unsigned long	gsc_isr_32;		/* Interrupt Status     ISR */
		unsigned long	gsc_tcdr_32;	/* Tx Clock Divider     TCDR */

		unsigned long	gsc_tfwr_32;    /* Tx FIFO Words        TFWR */
		unsigned long	gsc_rfwr_32;    /* Rx FIFO Words        RFWR */
		unsigned long	gsc_ielr_32;	/* Interrupt Edge/Level IELR */
		unsigned long	gsc_ihlr_32;	/* Interrupt Hi/Low     IHLR */
	} vaddr;

	struct
	{
		u32	gsc_frr;	/* Firmware Revision */
		u32	gsc_fsr;	/* Feature Set */
		u32	gsc_rfsr;	/* Rx FIFO Size */
		u32	gsc_tfsr;	/* Tx FIFO Size */
	} cache;
} dev_data_t;


/* variables	**************************************************************/

int16   DMABuffWriteNdx;				   // DMA buffer write index
int16   DMABuffReadNdx;					   // DMA buffer read index
uint16 *DMABuffPtrs[NUM_DMA_BUFFS];		   // DMA buffer pointers
int16   DMABuffWriteCntrs[NUM_DMA_BUFFS];  // incremented when buff is written to, decremented 
										   // > when read (so cam can check for overruns)

int16 CamIntrWatchCntr = -1;  // watchdog counter for camera interrupt

int16 CamFirstTimeCntr = NUM_DMA_CYCLES_TO_IGNORE;  // counts DMA cycles being ignored when starting up
boolean CamRxFIFOResetFlag = false;                 // indicates whether Rx FIFO has been reset at startup

static unsigned long oldAffinity;

static dev_data_t* _dev_list[MAX_DEVICES];
static dev_data_t  _dev_lists[MAX_DEVICES];  // this is to avoid dynamic memory allocation, but, leave other things the same
static int		   _dev_qty = 0;

static boolean keepGoingFlag = false;

static const dev_region_t	_pci_region	=
{
	/* address		*/	0,		/* Required for PCI registers. */
	/* vaddr		*/	0,		/* Ignored for PCI registers. */
	/* flags		*/	0,		/* Ignored for PCI registers. */
	/* size			*/	256,	/* Required for PCI registers. */
	/* requested	*/	0		/* Ignored for PCI registers. */
};

/* prototypes	**************************************************************/

static void _dma_start(dev_data_t* dev, void* buff, int nBytes);
//static unsigned int _dev_isr(unsigned int irq, struct pt_regs* regs);
static unsigned int _dev_isr(unsigned int irq, struct rtl_frame* regs);
static int _id_hpdi32_32(dev_data_t* dev);
static int _id_hpdi32_64(dev_data_t* dev);
static int _id_verify(struct pci_dev* pci, const dev_reg_id_t* list, size_t size);
static int _bar_verify(struct pci_dev* pci, const dev_bar_dat_t* bar, dev_region_t** region, size_t size);
static int _regions_verify(dev_region_t** list, size_t size);
static int _regions_request(dev_region_t** list, size_t size);
static void _regions_release(dev_region_t** list, size_t size);
static void _dev_data_t_init(dev_data_t* dev);
static unsigned long _reg_vaddr(dev_data_t* dev, unsigned long reg);
static void _reg_decode(const dev_region_t*	region, unsigned long reg, dev_reg_t* dreg);
static void _dev_reset(dev_data_t* dev);
static void _dev_reset_2(dev_data_t* dev);


/******************************************************************************
*
*	Function:	CamDrvrSetBits
*				CamDrvrClrBits
*				CamDrvrFlpBits
*				CamDrvrReadRegs
*               CamDrvrTickData
*
*	Purpose:
*		Functions to set, clear, and flip bits in the GSC control register.
*		Function to read and display registers.
*       Function to output a clock and data using the DMA board GPIO bits
*       All primarily for test purposes.
*
*	Arguments:
*		regID	identifies register that bits are in (at the moment, only gsc_bcr_32
*				> is supported)
*		bits	any bit equal to 1 will be set, the others will remain unchanged
*
*	Returned:
*		None.
*
******************************************************************************/

void CamDrvrSetBits(int regID, uint32 bits)
{
	uint32 ul;

	/*	Set bits. */
	ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
	ul |= bits;
	writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
}


void CamDrvrClrBits(int regID, uint32 bits)
{
	uint32 ul;

	/*	Clear bits. */
	ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
	ul &= ~bits;
	writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
}


void CamDrvrFlpBits(int regID, uint32 bits)
{
	uint32 ul;

	/*	Flip bits. */
	ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
	ul ^= bits;
	writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
}


void CamDrvrReadRegs(void)
{
    char textBuff[TEXT_BUF_LNGTH];
    uint32 bcr, bsr, rfr, tfr, tar, rar, fsr, isr, icr, tcd, iel, ihl, rsc, rlc;
    uint32 dmam, dmasz, pcic, pcii, dmaa, dmas0, dmas1, dmat;

	/*	Read and display registers. */
    bcr = readl(_dev_list[0]->vaddr.gsc_bcr_32);
	bsr = readl(_dev_list[0]->vaddr.gsc_bsr_32);
	rfr = readl(_dev_list[0]->vaddr.gsc_rfwr_32);
	tfr = readl(_dev_list[0]->vaddr.gsc_tfwr_32);
    tar = readl(_dev_list[0]->vaddr.gsc_tar_32);
    rar = readl(_dev_list[0]->vaddr.gsc_rar_32);
    fsr = readl(_dev_list[0]->vaddr.gsc_fsr_32);

	isr = readl(_dev_list[0]->vaddr.gsc_isr_32);
    icr = readl(_dev_list[0]->vaddr.gsc_icr_32);
    tcd = readl(_dev_list[0]->vaddr.gsc_tcdr_32);
    iel = readl(_dev_list[0]->vaddr.gsc_ielr_32);
    ihl = readl(_dev_list[0]->vaddr.gsc_ihlr_32);
    rsc = readl(_dev_list[0]->vaddr.gsc_rscr_32);
    rlc = readl(_dev_list[0]->vaddr.gsc_rlcr_32);

    pcic  = readl(_dev_list[0]->vaddr.plx_cntrl_32);
    pcii  = readl(_dev_list[0]->vaddr.plx_intcsr_32);
	dmam  = readl(_dev_list[0]->dma.channel[0].vaddr.mode_32);
	dmasz = readl(_dev_list[0]->dma.channel[0].vaddr.siz_32);
	dmaa  = readl(_dev_list[0]->vaddr.plx_dmaarb_32);
    dmas0 = (uint32)readb(_dev_list[0]->vaddr.plx_dmacsr0_8);
    dmas1 = (uint32)readb(_dev_list[0]->vaddr.plx_dmacsr1_8);
    dmat  = readl(_dev_list[0]->vaddr.plx_dmathr_32);

    sprintf(textBuff, "bcr: %lx  bsr: %lx  rfr: %lx  wfr: %lx  tar: %lx  rar: %lx  fsr: %lx", 
                     bcr, bsr, rfr, tfr,  tar, rar, fsr);
    SysConSendStrngNoParm(textBuff);

    sprintf(textBuff, "isr: %lx  icr: %lx  tcd: %lx  iel: %lx  ihl: %lx  rsc: %lx  rlc: %lx", 
                     isr, icr, tcd, iel, ihl, rsc, rlc);
    SysConSendStrngNoParm(textBuff);

    sprintf(textBuff, "pcic: %lx  pcii: %lx  dmam: %lx  dmasz: %lx  dmaa: %lx  dmas0: %lx  dmas1: %lx  dmat: %lx", 
                     pcic, pcii, dmam, dmasz, dmaa, dmas0, dmas1, dmat);
    SysConSendStrngNoParm(textBuff);
}


void CamDrvrTickData(void)
{
    static int16 startUp = 0, state = 0;
    static int16 outDataNdx = 0;
    static const uint16 outData[16] = { 0x00, 0x01, 0x02, 0x03,  // CMD D4 seems to be flakey on the DMA board
                                        0x08, 0x09, 0x0A, 0x0B,
                                        0x10, 0x11, 0x12, 0x13,
                                        0x18, 0x19, 0x1A, 0x1B };
	uint32 ul;

    if (OkayToStartCamData)
    {
        if (startUp < 4000)
            startUp++;
        else
        {
            switch (state)
            {
                case 0:  // clock low to high, using CMD D1
                    ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
                    ul |= 0x02000000;
                    writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
                    break;

                case 1:  // change data, using CMD D6 to CMD D2
                    ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
                    ul = (ul & ~0x7C000000) | ((outData[outDataNdx] << 26) & 0x7C000000);
                    writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
                    if (++outDataNdx >= 15)
                        outDataNdx = 0;
                    break;

                case 2:  // clock high to low, using CMD D1
                    ul = readl(_dev_list[0]->vaddr.gsc_bcr_32);
                    ul &= ~0x02000000;
                    writel(ul, _dev_list[0]->vaddr.gsc_bcr_32);
                    break;

                case 3:
                    break;
            }
            if (++state >= 4)
                state = 0;
        }
    }
}


/******************************************************************************
*
*	Function:	CamDrvrStartData
*
*	Purpose:
*		Start continuous data reads using PMC64 board 0 and DMA channel 0.
*
*	Arguments:
*		None.
*
*	Returned:
*		None.
*
******************************************************************************/

void CamDrvrStartData(void)
{
	int16 i;
	uint32 ul;
	dev_data_t*	dev = _dev_list[0];  // IMPORTANT: this will only work with a single PMC64 board for now!
	dev_dma_t*	dma	= &dev->dma.channel[0];  // always use DMA channel 0

//	if we're not already going
//		initialize variables
//		set flag indicating that DMA should keep going
//      do some board initialization
//		enable interrupts so we can receive an Rx FIFO Almost Empty interrupt
	if (!keepGoingFlag)
	{
		DMABuffReadNdx = NUM_DMA_BUFFS - 1;
		DMABuffWriteNdx = NUM_DMA_BUFFS - 1;
		for (i = 0; i < NUM_DMA_BUFFS; i++)
			DMABuffWriteCntrs[i] = 0;

		keepGoingFlag = true;

        _dev_reset_2(dev);  // initialize the board 

		CamDrvrSetBits(0, HPDI32_GSC_BCR_RX_FIFO_RESET |  // 0000 0004
						  HPDI32_GSC_BCR_RX_ENABLE	   |  // 0000 0020
						  HPDI32_GSC_BCR_D1_IN		   |  // 0200 0000
						  HPDI32_GSC_BCR_D2_IN		   |  // 0400 0000
						  HPDI32_GSC_BCR_D3_IN		   |  // 0800 0000
						  HPDI32_GSC_BCR_D4_IN		   |  // 1000 0000
						  HPDI32_GSC_BCR_D5_IN		   |  // 2000 0000
						  HPDI32_GSC_BCR_D6_IN);		  // 4000 0000

		/* Clear the DMA command/status register and clear the interrupt. */
		writeb(0, dma->vaddr.csr_8);
		writeb(DMA_CSR_CLEAR, dma->vaddr.csr_8);

//      Using Rx Almost Full, set to the value as follows, didn't work (every few hundred or few thousand times, the
//      > Rx Almost Full interrupt would not occur (even though everything looked okay)).  So, Rx Almost Empty is being
//      > used.  Even Rx Almost Empty has problems in that it generates interrupts before its terminal value.  But, that's
//      > handled in the interrupt handler (for now).
//      > writel(0x1DC1000F, _dev_list[0]->vaddr.gsc_rar_32);  // set Rx Almost Full value (0x1DC1 = 8192 - (512 + 64) + 1)
        writel(0x00100000 + (NUM_DMA_WORDS / 2 + 0x40), _dev_list[0]->vaddr.gsc_rar_32);  // set Rx Almost Empty value
        writel(HPDI32_INT_GSC_RX_FIFO_AE, _dev_list[0]->vaddr.gsc_icr_32);  // enable the Rx FIFO Almost Empty interrupt

        ul = readl(dev->vaddr.plx_intcsr_32);  // enable interrupts
        ul |= INTCSR_PCI_INT_ENABLE   |
		      INTCSR_LOCAL_INT_ENABLE |
		      INTCSR_LOCAL_INT_OUTPUT_ENABLE;
        writel(ul, dev->vaddr.plx_intcsr_32);

        rtl_hard_enable_irq(dev->pci->irq);  // enable the rtl irq
	}
}


/******************************************************************************
*
*	Function:	CamDrvrStopData
*
*	Purpose:
*		Stop continuous data reads.
*
*	Arguments:
*		None.
*
*	Returned:
*		None.
*
******************************************************************************/

void CamDrvrStopData(void)
{
//	set flag indicating that interrupts should not be handled and 
//  > reset the board
	keepGoingFlag = false;
    _dev_reset(_dev_list[0]);  // reset the board 
}


/******************************************************************************
*
*	Function:	_dma_start
*
*	Purpose:
*		Start a DMA transfer per the given arguments.
*
*	Arguments:
*		dev			The device data structure.
*		buff		The data transfer buffer.
*		nBytes		The number of bytes to transfer.
*
*	Returned:
*		None.
*
******************************************************************************/

static void _dma_start(dev_data_t* dev, void* buff, int nBytes)
{
	u32			ul;
	dev_dma_t*	dma	= &dev->dma.channel[0];  // always use DMA channel 0

    static const u32 MODE_32 = DMA_MODE_SIZE_32_BITS          |  // 0000 0003
                               DMA_MODE_INPUT_ENABLE          |  // 0000 0040
                               DMA_MODE_INTERRUPT_WHEN_DONE   |  // 0000 0400
                               DMA_MODE_LOCAL_ADRESS_CONSTANT |  // 0000 0800
                               DMA_MODE_PCI_INTERRUPT_ENABLE;    // 0002 0000

    static const u32 DPR_32 = DMA_DPR_TERMINAL_COUNT_IRQ |  // 0004
                              DMA_DPR_BOARD_TO_HOST;        // 0008

    static const u32 INTCSR_32 = INTCSR_PCI_INT_ENABLE | 
                                 INTCSR_LOCAL_INT_ENABLE | 
                                 INTCSR_LOCAL_INT_OUTPUT_ENABLE;

//  set up the DMA controller	
	writel(MODE_32, dma->vaddr.mode_32);     /* DMAMODEx */

	ul = virt_to_bus(buff);                  /* DMAPADRx */
	writel(ul, dma->vaddr.padr_32);

	ul = HPDI32_REG_OFFSET(HPDI32_GSC_FDR);  /* DMALADRx */
	writel(ul, dma->vaddr.ladr_32);

	writel((u32)nBytes, dma->vaddr.siz_32);  /* DMASIZx */

	writel(DPR_32, dma->vaddr.dpr_32);       /* DMADPRx */

	writel(0, dev->vaddr.plx_dmaarb_32);     /* DMAARB */

	writel(0, dev->vaddr.plx_dmathr_32);     /* DMATR */

//  clear the DMA interrupt, enable, and start
	writeb(0, dma->vaddr.csr_8);                               // clear the control/status register
	writeb(DMA_CSR_CLEAR, dma->vaddr.csr_8);                   // clear the DMA interrupt
	writeb(DMA_CSR_ENABLE, dma->vaddr.csr_8);                  // enable the DMA

	ul = readl(dev->vaddr.plx_intcsr_32);                      // enable interrupts
	ul |= dma->int_enable | INTCSR_32;
	writel(ul, dev->vaddr.plx_intcsr_32);

	rtl_hard_enable_irq(dev->pci->irq);                        // enable the rtl irq

	writeb(DMA_CSR_ENABLE | DMA_CSR_START, dma->vaddr.csr_8);  // start the DMA
}


/******************************************************************************
*
*	Function:	_dev_isr
*
*	Purpose:
*		Service an interrupt.
*
*	Arguments:
*		irq		The interrupt number.
*		regs	Unused.
*
*	Returned:
*		None.
*
******************************************************************************/

//static unsigned int _dev_isr(unsigned int irq, struct pt_regs* regs)
static unsigned int _dev_isr(unsigned int irq, struct rtl_frame* regs)
{
	u32				intcsr, bsr;		/* PLX Interrupt Control/Status and board status register */
	unsigned long	ul;
	dev_data_t*	    dev = _dev_list[0];			 // IMPORTANT: this will only work with a single PMC64 board for now!
	dev_dma_t*	    dma = &dev->dma.channel[0];  // always use DMA channel 0;

    boolean postIt;
//    int16 i;            //III
//    int16 intType = 0;  //III
//    char textBuff[TEXT_BUF_LNGTH];             //FFF
//    static int timeCntr = 0;                   //FFF
//    static uint32 timeLength;                  //FFF
//    static struct rtl_timespec timeKeep = {0, 0};  //FFF

    static const u32 INTCSR_32 = INTCSR_PCI_INT_ENABLE | 
                                 INTCSR_LOCAL_INT_ENABLE | 
                                 INTCSR_LOCAL_INT_OUTPUT_ENABLE;

//    rtl_printf("I");    //III
//    CamDrvrReadRegs();  //III

//	if we're not to keep going, simply return
    if (!keepGoingFlag)
        return 0;

    CamIntrWatchCntr++;

    postIt = false;

//  if we've received a DMA interrupt
    intcsr = readl(dev->vaddr.plx_intcsr_32);
	if ((intcsr & INTCSR_DMA_0_IRQ_ENABLE) && (intcsr & INTCSR_DMA_0_IRQ_ACTIVE))
	{
//        rtl_printf("1");  //III
//        intType = 1;      //III

		/* Clear the DMA command/status register and clear the interrupt. */
		writeb(0, dma->vaddr.csr_8);
		writeb(DMA_CSR_CLEAR, dma->vaddr.csr_8);

		/* Disable the DONE interrupt. */
		ul = readl(dma->vaddr.mode_32);
		ul &= ~DMA_MODE_INTERRUPT_WHEN_DONE;
		writel(ul, dma->vaddr.mode_32);

		/* Disable the DMA 0 interrupt. */
		intcsr &= ~dma->int_enable;
		writel(intcsr, dev->vaddr.plx_intcsr_32);

//        clock_gettime(CLOCK_REALTIME, &TimeCamDrvr3);  //TTT

/*/FFF start TEST2
        if (++timeCntr >= 1000)
        {
            timeCntr = 0;

            timeLength = SysConTimeDiff(timeKeep, TimeCamDrvr3);
            if (timeLength == 0)
                SysConSendStrngNoParm("data kHz: div by zero");
            else
            {
                sprintf(textBuff, "data kHz: %ld", 1024000000 / timeLength);  // data frequency in kHz
                SysConSendStrngNoParm(textBuff);
            }

//            clock_gettime(CLOCK_REALTIME, &timeKeep);  //TTT
        }
//FFF end*/

        if (CamFirstTimeCntr == 0)
            postIt = true;
		else
		{
			if ((CamFirstTimeCntr < DMA_CYCLE_TO_RESET_FIFO) && !CamRxFIFOResetFlag)
			{
				CamDrvrSetBits(0, HPDI32_GSC_BCR_RX_FIFO_RESET);  // 0000 0004
				CamRxFIFOResetFlag = true;
			}
		}

//        clock_gettime(CLOCK_REALTIME, &TimeCamDrvr4);  //TTT
	}

//  if we received an Rx FIFO Almost Empty interrupt, reset it
	if ((readl(_dev_list[0]->vaddr.gsc_isr_32) & HPDI32_INT_GSC_RX_FIFO_AE) != 0)
        writel(HPDI32_INT_GSC_RX_FIFO_AE, _dev_list[0]->vaddr.gsc_isr_32);

//  report an error if the Rx FIFO is full
    if (((bsr = readl(_dev_list[0]->vaddr.gsc_bsr_32)) & HPDI32_INT_GSC_RX_FIFO_FULL) == 0)
        SysConSendStrngNoParm("Error: DMA FIFO full");

//  if the Rx FIFO Almost Empty flag is not set (i.e., we're on the empty side of Almost Empty)
	if ((bsr & HPDI32_INT_GSC_RX_FIFO_AE) == 0)
    {
//      simply enable interrupts to be able to receive the next Rx FIFO Almost Empty interrupt
//        if (intType != 1)       //III
//            CamDrvrReadRegs();  //III
//        rtl_printf("2");  //III
//        intType += 2;     //III

        ul = readl(dev->vaddr.plx_intcsr_32) | INTCSR_32;  // enable the interrupts
        writel(ul, dev->vaddr.plx_intcsr_32);

        rtl_hard_enable_irq(dev->pci->irq);  // enable the rtl irq
    }
//  else (the Rx FIFO Almost Empty flag is set (i.e., we're on the full side of Almost Empty))
    else
    {
//      if a DMA is currently in progress
//          simply re-enable interrupts (we got an invalid Rx FIFO Almost Empty interrupt)
//      else
//          increment the write index and the corresponding write counter
//			call _dma_start() to start the next DMA sequence
        if ((readb(dma->vaddr.csr_8) & DMA_CSR_ENABLE) != 0)
        {
//            rtl_printf("3");  //III
//            intType += 4;     //III

            ul = readl(dev->vaddr.plx_intcsr_32) | INTCSR_32;  // enable the interrupts
            writel(ul, dev->vaddr.plx_intcsr_32);

            rtl_hard_enable_irq(dev->pci->irq);  // enable the rtl irq
        }
        else
        {
//            clock_gettime(CLOCK_REALTIME, &TimeCamDrvr1);  //TTT
//            rtl_printf("4");  //III
//            intType += 8;     //III

            if (CamFirstTimeCntr > 0)  // ignore the first DMA cycles (to let the camera stabilize)
				CamFirstTimeCntr--;
            if (CamFirstTimeCntr == 0)
            {
                if (++DMABuffWriteNdx >= NUM_DMA_BUFFS)
                    DMABuffWriteNdx = 0;
                DMABuffWriteCntrs[DMABuffWriteNdx]++;
            }

            _dma_start(_dev_list[0], DMABuffPtrs[DMABuffWriteNdx], NUM_DMA_WORDS * 2);

//            clock_gettime(CLOCK_REALTIME, &TimeCamDrvr2);  //TTT
        }
    }

//     SysConSendStrngIntParm("intType: ", intType);  //III

//  if the camera semaphore is to be posted, post it
    if (postIt)  // postIt has to be used because of the way RTLinux assigns interrupt priorities
    {
//        clock_gettime(CLOCK_REALTIME, &TimeCamDrvr3);  //TTT
        if (rtl_sem_post(&CamSem) < 0)
            SysConSendStrngIntParm("Error posting command to CamSem, errno = ", rtl_errno);
//        clock_gettime(CLOCK_REALTIME, &TimeCamDrvr4);  //TTT
//        SysConSendStrngIntParm("Around post: ", SysConTimeDiff(TimeCamDrvr3, TimeCamDrvr4));  //TTT
    }

//    rtl_printf("i");  //III

    return 0;
}


/******************************************************************************
*
*	Function:	CamDrvrInit
*
*	Purpose:
*		Initialize the camera driver.
*
*	Arguments:
*		None.
*
*	Returned:
*		  0		All went well.
*		< 0		There was an error.
*
******************************************************************************/

int CamDrvrInit(void)
{
	boolean error;
	int	i;
	int status = -ENODEV;
	unsigned long affinity = 1 << HRT_CPU;

	dev_data_t		dev;
	dev_region_t	dummy;

	dev_region_t* regions[] =
	{
		&dev.plx_mem,
		&dev.plx_io,
		&dev.gsc_mem,
		&dummy,
		&dummy,
		&dummy
	};

	struct pci_dev*	pci;

	/*
	*	Initialize DMA variables.
	*/
    for (i = 0; i < NUM_DMA_BUFFS; i++)
        DMABuffWriteCntrs[i] = 0;
	DMABuffWriteNdx = NUM_DMA_BUFFS - 1;
	DMABuffReadNdx = NUM_DMA_BUFFS - 1;

	/*
	*	Acquire memory for DMA buffers.
	*/
	i = 0;
	error = false;
	while ((i < NUM_DMA_BUFFS) && !error)
	{
		DMABuffPtrs[i] = kmalloc(NUM_DMA_WORDS * 2, GFP_KERNEL | __GFP_DMA);
		if (DMABuffPtrs[i] == NULL)
		{
	        SysConSendStrngIntParm("Error: unable to acquire DMA memory, index = ", i);
			CamDrvrCleanup();
			error = true;
		}
		i++;
	}

	if (!error)
	{
		memset(&_dev_list, 0, sizeof(_dev_list));

		/*
		*	Locate any and all HPDI32 boards (LickAO only has 1 for now).
		*/
		PCI_DEVICE_LOOP(pci)
		{
			memset(&dummy, 0, sizeof(dummy));
			memset(&dev, 0, sizeof(dev_data_t));
			dev.pci	= pci;

			i = _id_hpdi32_32(&dev);
			if (i)
				i = _id_hpdi32_64(&dev);
			if (i)
				continue;

			i = _regions_verify(regions, ARRAY_ELEMENTS(regions));
			if (i)
				continue;

			/*
			*	We've got an HPDI32. Get the required resources.
			*/
			i = _regions_request(regions, ARRAY_ELEMENTS(regions));
			if (i)
			{
				_regions_release(regions, ARRAY_ELEMENTS(regions));
				continue;
			}

			/* Initialize the structure.  */
			_dev_data_t_init(&dev);

			/* Reset the device and add it to our list. */
			_dev_reset(&dev);
			if (_dev_qty >= MAX_DEVICES)
			{
				_regions_release(regions, ARRAY_ELEMENTS(regions));
				SysConSendStrngNoParm("Error: too many PMC64 devices");
				continue;
			}
			_dev_list[_dev_qty] = &_dev_lists[_dev_qty];  // this is to avoid dynamic memory allocation, but, leave other things the same
			memcpy(_dev_list[_dev_qty], &dev, sizeof(dev_data_t));

			/* Request the hardware interrupt for the device. */
			if ((status = rtl_request_irq(dev.pci->irq, _dev_isr)) != 0)
			{
				_regions_release(regions, ARRAY_ELEMENTS(regions));
				SysConSendStrngIntParm("Error: can't open camera interrupt, status = ", status);
				continue;
			}
            rtl_irq_set_affinity(dev.pci->irq, &affinity, &oldAffinity);

			_dev_qty++;
		}
	}

	if (_dev_qty)
		status = 0;

	return(status);
}


/******************************************************************************
*
*	Function:	_id_hpdi32_32
*
*	Purpose:
*		See if the referenced board is a 32-bit PCI interface version.
*		This test is independent of the board's form factor.
*
*	Arguments:
*		dev		The device structure of interest.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _id_hpdi32_32(dev_data_t* dev)
{
	static const dev_bar_dat_t	bar[6]	=
	{
		/* offset				flags	size			     */
		{ PCI_BASE_ADDRESS_0,	0,		256 /* PLX memory    */	},
		{ PCI_BASE_ADDRESS_1,	0x1,	256 /* PLX I/O ports */	},
		{ PCI_BASE_ADDRESS_2,	0,		GSC_REG_BLOCK_SIZE		},
		{ PCI_BASE_ADDRESS_3,	0,		0 /* Unused. */			},
		{ PCI_BASE_ADDRESS_4,	0,		0 /* Unused. */			},
		{ PCI_BASE_ADDRESS_5,	0,		0 /* Unused. */			}
	};

	static const dev_reg_id_t	id[4]	=
	{
		/* reg						value			*/
		{ PCI_VENDOR_ID,			HPDI32_VENDOR_ID	},
		{ PCI_DEVICE_ID,			HPDI32_DEVICE_ID_32	},
		{ PCI_SUBSYSTEM_VENDOR_ID,	HPDI32_SUBVENDOR_ID	},
		{ PCI_SUBSYSTEM_ID,			HPDI32_SUBDEVICE_ID_32	}
	};

	dev_region_t	dummy;
	int				i;

	dev_region_t* regions[6] =
	{
		&dev->plx_mem,
		&dev->plx_io,
		&dev->gsc_mem,
		&dummy,
		&dummy,
		&dummy
	};

	dev->type	= HPDI32_32_BIT;
	i			= _id_verify(dev->pci, id, 4);

	if (i == 0)
		i = _bar_verify(dev->pci, bar, regions, 6);

	return(i);
}


/******************************************************************************
*
*	Function:	_id_hpdi32_64
*
*	Purpose:
*		See if the referenced board is a 64-bit PCIX interface version.
*		This test is independent of the board's form factor.
*
*	Arguments:
*		dev		The device structure of interest.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _id_hpdi32_64(dev_data_t* dev)
{
	static const dev_bar_dat_t bar[6] =
	{
		/* offset				flags	size			     */
		{ PCI_BASE_ADDRESS_0,	0,		512 /* PLX memory    */	},
		{ PCI_BASE_ADDRESS_1,	0x1,	256 /* PLX I/O ports */	},
		{ PCI_BASE_ADDRESS_2,	0,		GSC_REG_BLOCK_SIZE		},
		{ PCI_BASE_ADDRESS_3,	0,		0   /* Unused. */		},
		{ PCI_BASE_ADDRESS_4,	0,		0   /* Unused. */		},
		{ PCI_BASE_ADDRESS_5,	0,		0   /* Unused. */		}
	};

	static const dev_reg_id_t id[4] =
	{
		/* reg						value			*/
		{ PCI_VENDOR_ID,			HPDI32_VENDOR_ID	},
		{ PCI_DEVICE_ID,			HPDI32_DEVICE_ID_64	},
		{ PCI_SUBSYSTEM_VENDOR_ID,	HPDI32_SUBVENDOR_ID	},
		{ PCI_SUBSYSTEM_ID,			HPDI32_SUBDEVICE_ID_64	}
	};

	dev_region_t	dummy;
	int				i;

	dev_region_t* regions[6] =
	{
		&dev->plx_mem,
		&dev->plx_io,
		&dev->gsc_mem,
		&dummy,
		&dummy,
		&dummy
	};

	dev->type	= HPDI32_64_BIT;
	i			= _id_verify(dev->pci, id, 4);

	if (i == 0)
		i = _bar_verify(dev->pci, bar, regions, 6);

	return(i);
}


/******************************************************************************
*
*	Function:	_id_verify
*
*	Purpose:
*		Verify the identification of the given PCI device to see if it
*		complies with the requirements of the given list. is a 32-bit
*
*	Arguments:
*		pci		The structure referencing the PCI device of interest.
*		list	The id register list to validate.
*		size	The size of the above list.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _id_verify(struct pci_dev* pci, const dev_reg_id_t* list, size_t size)
{
	int	i;
	u16	word;

	for (i = 0; i < (int) size; i++)
	{
		pci_read_config_word(pci, list[i].reg, &word);

		if (word != list[i].value)
			break;
	}

	i = (i == size) ? 0 : 1;
	return(i);
}


/******************************************************************************
*
*	Function:	_bar_verify
*
*	Purpose:
*		Verify that the Base Address Registers are valid for the
*		the desired device.
*
*	Arguments:
*		pci		The structure referencing the PCI device of interest.
*		bar		This list give the BAR requirements.
*		region	The data describing the regions get put in this list.
*		size	The size of the above lists.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _bar_verify(struct pci_dev* pci, const dev_bar_dat_t* bar, dev_region_t** region, size_t size)
{
	int	i;
	u32	address;
	u32	r_size;

	for (i = 0; i < size; i++)
	{
		pci_read_config_dword(pci, bar[i].offset, &address);

		/* Use a PCI feature to get the address mask. */
		pci_write_config_dword(pci, bar[i].offset, ~0);
		pci_read_config_dword(pci, bar[i].offset, &r_size);

		/* Restore the register. */
		pci_write_config_dword(pci, bar[i].offset, address);
		r_size	= 1 + ~(r_size & ~0xF);

		if ((address & 0xF) != bar[i].flags)
			break;

		if (r_size != bar[i].size)
			break;

		region[i]->address	= address & 0xFFFFFFF0;
		region[i]->flags	= bar[i].flags;
		region[i]->size		= r_size;
	}

	i = (i == size) ? 0 : 1;
	return(i);
}


/******************************************************************************
*
*	Function:	_regions_verify
*
*	Purpose:
*		Verify that the resource regions are available for use.
*
*	Arguments:
*		list	The list of resource regions is recorded here.
*		size	The size of the list.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _regions_verify(dev_region_t** list, size_t size)
{
	int	i;
	int	test;

	for (i = 0; i < size; i++)
	{
		if (list[i]->size == 0)
			continue;

		if (list[i]->flags & PCI_BASE_ADDRESS_SPACE_IO)
			test = REGION_IO_CHECK(list[i]->address, list[i]->size);
		else
			test = REGION_MEM_CHECK(list[i]->address, list[i]->size);

		if (test < 0)
			break;
	}

	i = (i == size) ? 0 : 1;
	return(i);
}


/******************************************************************************
*
*	Function:	_regions_request
*
*	Purpose:
*		Request that the specified set of memory regions be reserved
*		for our use.
*
*	Arguments:
*		list	The list of resource regions is recorded here.
*		size	The size of the list.
*
*	Returned:
*		0	All went well.
*		1	There was an error.
*
******************************************************************************/

static int _regions_request(dev_region_t** list, size_t size)
{
	int	i;
	int	test;

	for (i = 0; i < size; i++)
	{
		if (list[i]->size == 0)
			continue;

		if (list[i]->flags & PCI_BASE_ADDRESS_SPACE_IO)
		{
			test = REGION_IO_REQUEST(list[i]->address, list[i]->size, MODULE_NAME);

			if (test == 0)
				break;

			list[i]->requested	= 1;
			list[i]->vaddr		= (void*) list[i]->address;
		}
		else
		{
			test = REGION_MEM_REQUEST(list[i]->address, list[i]->size, MODULE_NAME);

			if (test == 0)
				break;

			list[i]->requested	= 1;
			list[i]->vaddr		= ioremap(list[i]->address, list[i]->size);
		}
	}

	i = (i == size) ? 0 : 1;
	return(i);
}


/******************************************************************************
*
*	Function:	_regions_release
*
*	Purpose:
*		Release any of the given regions that were successfully
*		requested.
*
*	Arguments:
*		list	The list of resource regions is recorded here.
*		size	The size of the list.
*
*	Returned:
*		None.
*
******************************************************************************/

static void _regions_release(dev_region_t** list, size_t size)
{
	int	i;

	for (i = 0; i < size; i++)
	{
		if (list[i]->requested == 0)
		{
		}
		else if (list[i]->flags & PCI_BASE_ADDRESS_SPACE_IO)
			REGION_IO_RELEASE(list[i]->address, list[i]->size);
		else
		{
			iounmap(list[i]->vaddr);
			REGION_MEM_RELEASE(list[i]->address, list[i]->size);
		}
	}
}


/******************************************************************************
*
*	Function:	_dev_data_t_init
*
*	Purpose:
*		Initialize the idevice data structure. The initialization
*		done here must not assume that the device is open or closed.
*		Those related fields are untouched as we don't want to
*		interfere with the current state.
*
*	Arguments:
*		dev		The structure for the device of interest.
*
*	Returned:
*		None.
*
******************************************************************************/

static void _dev_data_t_init(dev_data_t* dev)
{
	dev_dma_t*		dma_ch;
	unsigned long	vaddr;

	dev->vaddr.plx_cntrl_32		= _reg_vaddr(dev, HPDI32_PLX_CNTRL);
	dev->vaddr.plx_dmaarb_32	= _reg_vaddr(dev, HPDI32_PLX_DMAARB);
	dev->vaddr.plx_dmacsr0_8	= _reg_vaddr(dev, HPDI32_PLX_DMACSR0);
	dev->vaddr.plx_dmacsr1_8	= _reg_vaddr(dev, HPDI32_PLX_DMACSR1);
	dev->vaddr.plx_dmathr_32	= _reg_vaddr(dev, HPDI32_PLX_DMATHR);
	dev->vaddr.plx_intcsr_32	= _reg_vaddr(dev, HPDI32_PLX_INTCSR);

	dev->vaddr.gsc_frr_32		= _reg_vaddr(dev, HPDI32_GSC_FRR);   // firmware revision
	dev->vaddr.gsc_bcr_32		= _reg_vaddr(dev, HPDI32_GSC_BCR);   // board control
	dev->vaddr.gsc_bsr_32		= _reg_vaddr(dev, HPDI32_GSC_BSR);   // board status
	dev->vaddr.gsc_tar_32		= _reg_vaddr(dev, HPDI32_GSC_TAR);   // Tx Programmable Almost
	dev->vaddr.gsc_rar_32		= _reg_vaddr(dev, HPDI32_GSC_RAR);   // Rx Programmable Almost
	dev->vaddr.gsc_fsr_32		= _reg_vaddr(dev, HPDI32_GSC_FSR);   // Features
	dev->vaddr.gsc_fdr_32		= _reg_vaddr(dev, HPDI32_GSC_FDR);   // Rx/Tx FIFOs
	dev->vaddr.gsc_rscr_32      = _reg_vaddr(dev, HPDI32_GSC_RSCR);	 // Rx Status Counter
	dev->vaddr.gsc_rlcr_32      = _reg_vaddr(dev, HPDI32_GSC_RLCR);	 // Rx Line Counter
	dev->vaddr.gsc_icr_32		= _reg_vaddr(dev, HPDI32_GSC_ICR);   // interrupt control
	dev->vaddr.gsc_isr_32		= _reg_vaddr(dev, HPDI32_GSC_ISR);   // interrupt status
    dev->vaddr.gsc_tcdr_32		= _reg_vaddr(dev, HPDI32_GSC_TCDR);  // Tx clock divider
    dev->vaddr.gsc_tfwr_32		= _reg_vaddr(dev, HPDI32_GSC_TFWR);  // Tx FIFO words
    dev->vaddr.gsc_rfwr_32		= _reg_vaddr(dev, HPDI32_GSC_RFWR);  // Rx FIFO words
	dev->vaddr.gsc_ielr_32		= _reg_vaddr(dev, HPDI32_GSC_IELR);  // interrupt edge/level
	dev->vaddr.gsc_ihlr_32		= _reg_vaddr(dev, HPDI32_GSC_IHLR);  // interrupt high/low

	dev->cache.gsc_frr = readl(dev->vaddr.gsc_frr_32);

	vaddr = _reg_vaddr(dev, HPDI32_GSC_FSR);
	dev->cache.gsc_fsr = readl(vaddr);

	vaddr = _reg_vaddr(dev, HPDI32_GSC_RFSR);
	dev->cache.gsc_rfsr	= readl(vaddr);

	vaddr = _reg_vaddr(dev, HPDI32_GSC_TFSR);
	dev->cache.gsc_tfsr	= readl(vaddr);

	if ((dev->cache.gsc_frr & HPDI32_GSC_FRR_FSR) == 0)
		dev->cache.gsc_fsr	= 0;

	if ((dev->cache.gsc_fsr & HPDI32_GSC_FSR_TFSR_RFSR) == 0)
	{
		dev->cache.gsc_rfsr	= 0;
		dev->cache.gsc_tfsr	= 0;
	}

	dma_ch					= &dev->dma.channel[0];
	dma_ch->flags			= _DMA_READ | _DMA_WRITE;
	dma_ch->int_enable		= INTCSR_DMA_0_IRQ_ENABLE;
	dma_ch->vaddr.mode_32	= _reg_vaddr(dev, HPDI32_PLX_DMAMODE0);
	dma_ch->vaddr.padr_32	= _reg_vaddr(dev, HPDI32_PLX_DMAPADR0);
	dma_ch->vaddr.ladr_32	= _reg_vaddr(dev, HPDI32_PLX_DMALADR0);
	dma_ch->vaddr.siz_32	= _reg_vaddr(dev, HPDI32_PLX_DMASIZ0);
	dma_ch->vaddr.dpr_32	= _reg_vaddr(dev, HPDI32_PLX_DMADPR0);
	dma_ch->vaddr.csr_8		= _reg_vaddr(dev, HPDI32_PLX_DMACSR0);

	dma_ch					= &dev->dma.channel[1];
	dma_ch->int_enable		= INTCSR_DMA_1_IRQ_ENABLE;
	dma_ch->vaddr.mode_32	= _reg_vaddr(dev, HPDI32_PLX_DMAMODE1);
	dma_ch->vaddr.padr_32	= _reg_vaddr(dev, HPDI32_PLX_DMAPADR1);
	dma_ch->vaddr.ladr_32	= _reg_vaddr(dev, HPDI32_PLX_DMALADR1);
	dma_ch->vaddr.siz_32	= _reg_vaddr(dev, HPDI32_PLX_DMASIZ1);
	dma_ch->vaddr.dpr_32	= _reg_vaddr(dev, HPDI32_PLX_DMADPR1);
	dma_ch->vaddr.csr_8		= _reg_vaddr(dev, HPDI32_PLX_DMACSR1);

	if (dev->cache.gsc_fsr & HPDI32_GSC_FSR_DMA_CH1)
		dma_ch->flags	= _DMA_WRITE;
	else
		dma_ch->flags	= 0;
}


/******************************************************************************
*
*	Function:	_reg_vaddr
*
*	Purpose:
*		Get the virtual address of the specified register.
*
*	Arguments:
*		dev		The respective device of interest.
*		reg		The register of interest.
*
*	Returned:
*		unsigned long	The register's virtual address.
*
******************************************************************************/

static unsigned long _reg_vaddr(dev_data_t* dev, unsigned long reg)
{
	dev_reg_t		dreg;
	unsigned long	type = HPDI32_REG_TYPE(reg);

	switch (type)
	{
		default:
		case HPDI32_PCI_REGISTER:
			_reg_decode(&_pci_region, reg, &dreg);
			break;

		case HPDI32_PLX_REGISTER:
			_reg_decode(&dev->plx_mem, reg, &dreg);
			break;

		case HPDI32_GSC_REGISTER:
			_reg_decode(&dev->gsc_mem, reg, &dreg);
			break;
	}

	return(dreg.vaddr);
}


/******************************************************************************
*
*	Function:	_reg_decode
*
*	Purpose:
*		Decode a register id.
*
*	Arguments:
*		region	This is the resource region where the register resides.
*		reg		This is the register of interest.
*		dreg	The decoded data is put here.
*
*	Returned:
*		None.
*
******************************************************************************/

static void _reg_decode(const dev_region_t*	region, unsigned long reg, dev_reg_t* dreg)
{
	dreg->reg	 = reg;
	dreg->region = region;
	dreg->type	 = HPDI32_REG_TYPE(reg);
	dreg->offset = HPDI32_REG_OFFSET(reg);
	dreg->size	 = HPDI32_REG_SIZE(reg);
	dreg->vaddr	 = dreg->offset;
	dreg->vaddr	 += (unsigned long) region->vaddr;
}


/******************************************************************************
*
*	Function:	_dev_reset
*
*	Purpose:
*		Reset the device to a default inactive state.
*
*	Arguments:
*		dev	The data for the device of interest.
*
*	Returned:
*		None.
*
******************************************************************************/

static void _dev_reset(dev_data_t* dev)
{
	writel(0, dev->vaddr.plx_intcsr_32);  /* Interrupts off. */

	writel(HPDI32_GSC_BCR_BOARD_RESET, dev->vaddr.gsc_bcr_32);
	udelay(20);  /* Wait for reset. */  // IMPORTANT: this can't run in kernel mode!
}

static void _dev_reset_2(dev_data_t* dev)
{
	int retVal;
	struct rtl_timespec next;

	writel(0, dev->vaddr.plx_intcsr_32);  /* Interrupts off. */
	writel(HPDI32_GSC_BCR_BOARD_RESET, dev->vaddr.gsc_bcr_32);

	next.tv_sec = 0;
	next.tv_nsec = 20000;
	if ((retVal = rtl_nanosleep(&next, NULL)) < 0)  /* Wait for reset. */
		SysConSendStrngIntParm("Error from nanosleep() in _dev_reset_2(), retVal = ", retVal);
}


/******************************************************************************
*
*	Function:	CamDrvrCleanup
*
*	Purpose:
*		Clean things up when the kernel is about to unload the module.
*
*	Arguments:
*		None.
*
*	Returned:
*		None.
*
******************************************************************************/

void CamDrvrCleanup(void)
{
	int i;
	dev_region_t* list[3];

	for (i = 0; i < NUM_DMA_BUFFS; i++)
	{
		if (DMABuffPtrs[i] != NULL)
			kfree(DMABuffPtrs[i]);
	}

	for (i = 0; i < MAX_DEVICES; i++)
	{
		if (_dev_list[i])
		{
			_dev_reset(_dev_list[i]);
			list[0]	= &_dev_list[i]->plx_mem;
			list[1]	= &_dev_list[i]->plx_io;
			list[2]	= &_dev_list[i]->gsc_mem;
			_regions_release(list, ARRAY_ELEMENTS(list));
			rtl_hard_disable_irq(_dev_list[i]->pci->irq);
            rtl_irq_set_affinity (_dev_list[i]->pci->irq, &oldAffinity, NULL);
			rtl_free_irq(_dev_list[i]->pci->irq);
			_dev_list[i] = NULL;
		}
	}

	_dev_qty = 0;
}


/**********************************************************************************************

UNIT NAME:  CamDrvrWatch()

UNIT DESCRIPTION:
This is a thread used as a camera watchdog.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamDrvrWatch( void *arg )
{
	int retVal;
	struct rtl_timespec next;

//  spin on the InitDone flags for the other threads
    while (!TiptTiltAPDInitDone || !CamInitDone || !CentInitDone || !ReconInitDone  || !CamLTTInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm("Error from nanosleep() in CamWatch(), retVal = ", retVal);
    }

//	enter infinite test loop
	while (1)
	{
//		wait for a second
		next.tv_sec = 0;
		next.tv_nsec = 1000000000;
   		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep(), retVal = ", retVal);

//		make sure we've received camera frames and, if not
        if ((CamIntrWatchCntr == 0) || CentGettingBehindFlag)
        {
			CentGettingBehindFlag = false;

            CamIntrWatchCntr = -1;

//			set flag indicating that interrupts should not be handled and reset the board (this is the equivalent 
//			> of CamDrvrStopData except that _dev_reset_2() is called)
			keepGoingFlag = false;
		    _dev_reset_2(_dev_list[0]);  // reset the board 

//			initialize variables
			CamRxFIFOResetFlag = false;
			CamFirstTimeCntr = NUM_DMA_CYCLES_TO_IGNORE;
			CamRestart();
			CentRestart();
			ReconRestart();

//      	re-start the camera data (this will begin normal operation when the camera goes back on)
			CamDrvrStartData();

//			notify the user
			SysConSendStrngNoParm("Notification: camera watchdog timeout");
		}
        else if (CamIntrWatchCntr > 0)
            CamIntrWatchCntr = 0;
	}
}
