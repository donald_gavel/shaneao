/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		cam

AUTHOR:	DWP			DATE: 1/7/03

COMPONENT DESCRIPTION:
This component contains functions to interface to the AOA camera for the LickAO control system.
The AOA camera uses an obsolete Mercury I/O protocol called IOTTL/80.  In order to support this
interface, the system uses a General Standards PMC64-HPDI32ALT DMA board (one of the few that 
handles TTL signals) and receives all of the data (at ~3.7 MHz), with the protocol control lines 
brought in as data bits.  The software then parses the valid data by looking at the control 
bits.  

COMPONENT UNITS:
1.	Cam()
2.	CamInit()
3.	CamRestart()
4.  CamProcCmd()
5.  CamSerialWrite()
6.  CamSerial()
  
REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_time.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_serial.h>
#include <rtl_posixio.h>

#include "types.h"
#include "constant.h"
#include "camDrvr.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"


// Defines

#define SERIAL_BUF_LNGTH 64  // length of serial data buffers
#define CAM_SER_OUT_BUF_LEN 128


// Global Variables

int16 CamFrameRate;  // camera frame rate in frames per second
int16 CamGain;		 // camera gain (1 or 2)
int16 CamCRTGain;	 // camera CRT gain (0 to 4)  //XXXRS170 CRT gain (0 to 64)

int16 CamFrameRateNdx = 0;  // index for current camera rate into LegalFrameRates[]
//XXX UseAOA const int16 LegalFrameRates[] = { 55, 100, 200, 250, 350, 500, 750, 1000, -1 };  // -1 is the terminator
const int16 LegalFrameRates[] = { 50, 100, 200, 250, 350, 500, 750, 1000, -1 };  // -1 is the terminator

// Local Variables

static boolean successfulPost = true;  // indicates when a message has been successfully posted to the centroid sem

static int16 noSyncCntr = 0;  // counts the number of DMA cycles (i.e., filled DMA buffers) without a sync pulse
static int16 dmaBuffDataNdx;  // index for the current data element in the current DMA buffer
static int16 subapNdx = -1;	  // index for the sub-aperture that we're currently receiving (-1 means we're looking for a sync pulse)
static int16 subapRowNdx;	  // index for the current row in the current subaperture
static int16 unusedRowNdx;	  // index for the unused camera rows
static int16 subapDataNdx;	  // index for the current data element (pixel) fof the current row for the current subaperture

static boolean fullSerialMsgFlag;         // indicates whether a full serial message has been received from the camera
static int16 serialMsgNdx;                // index for serial message from camera
static char serialMsg[SERIAL_BUF_LNGTH];  // serial message from camera

static int16 serialTimeoutCntr = -1;  // counter to test for camera serial timeout

static char camSerialOutBuf[CAM_SER_OUT_BUF_LEN];                   // camera serial circular output buffer
static int16 camSerialOutBufPutNdx = 0, camSerialOutBufGetNdx = 0;  // put and get indexes into camSerialOutBuf[]

static const char *camStrs[3] = { "Frame Ra",  // the beginning of ASCII strings received from the camera used to
								  "GAIN set",  // > filter messages to be sent to the host
								  "CRT Gain" };

// the data comes out from the camera on 4 taps, each 16 contiguous pixels long, in the order tap 1, tap 3, tap 2, tap 4
// > the following table maps the data into its correct order:
static const int16 camDataMap[NUM_PIX_IN_ROW] = 
{
    0, 32, 16, 48,
    1, 33, 17, 49,
    2, 34, 18, 50,
    3, 35, 19, 51,
    4, 36, 20, 52,
    5, 37, 21, 53,
    6, 38, 22, 54,
    7, 39, 23, 55,
    8, 40, 24, 56,
    9, 41, 25, 57,
   10, 42, 26, 58,
   11, 43, 27, 59,
   12, 44, 28, 60,
   13, 45, 29, 61,
   14, 46, 30, 62,
   15, 47, 31, 63
};


// Local Function Prototypes

static void CamProcCmd( int16 cmdNdx );
static void CamSerialWrite( char serialString[] );


/**********************************************************************************************

UNIT NAME:  Cam()

UNIT DESCRIPTION:
This unit is the camera thread mainline.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *Cam( void *arg )
{
	static int16 diagTelemCntr = 0;
	boolean diagDataAvail, sendTelemData;
	uint16 dataEl1, dataEl2;
	int16 buffCntr, buffNdx, firstFree, firstFreeButWithDiag;
	diagFIFOMsgType diagFIFOMsg;
    CamBuffType *camBuffPtr;

    static boolean validSync = false;

#ifdef LLNL_TEST
	static int16 testCntr = -1;  //LLNLTest
	static uint16 testData = 0;  //LLNLTest
#endif

//    char textBuff[TEXT_BUF_LNGTH];             //FFF
//    static int timeCntr = 0;                   //FFF
//    static uint32 timeLength;                  //FFF
//    static struct timespec timeKeep = {0, 0};  //FFF

//	int retVal;  //III
//	struct rtl_timespec next;  //III

//	initialize variables used by the camera thread
	CamInit();

//	enter infinite loop
	while (1)
	{
//        rtl_printf("a");  //III

		if (rtl_sem_wait(&CamSem) < 0)
			SysConSendStrngIntParm("Error waiting on CamSem, errno = ", rtl_errno);

//		if a command is pending, call CamProcCmd() to process it
		else if (CamDataPtr->cmdStart != eCmdNone)
		{
			CamProcCmd(CamDataPtr->cmdStart);
			CamDataPtr->cmdStart = eCmdNone;
		}

//		else
		else
		{
//            rtl_printf("A");  //III
//            clock_gettime(CLOCK_REALTIME, &TimeCam1);  //TTT

//			increment the DMA buffer read index
			if (++DMABuffReadNdx >= NUM_DMA_BUFFS)
				DMABuffReadNdx = 0;

//			if the write counter corresponding to the DMA read index is greater than 1 (i.e., the buffer has been written
//			> to (via the camera's DMA) more than once since the last time it was read)
//				report an error and set subapNdx to go back to looking for a the camera sync pulse (to ignore the rest of this frame)
			if (DMABuffWriteCntrs[DMABuffReadNdx] > 1)
			{
				subapNdx = -1;
				SysConSendStrngIntParm("Error: DMA buffer has been overwritten, counter = ", DMABuffWriteCntrs[DMABuffReadNdx]);
				DMABuffWriteCntrs[DMABuffReadNdx] = 0;
			}
//			else
			else
			{
                camBuffPtr = &CamDataPtr->camBuffs[CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx]];
				dmaBuffDataNdx = 0;
				while (dmaBuffDataNdx < NUM_DMA_WORDS)
				{
#ifdef LLNL_TEST
					if (++testCntr >= 7400)  //XXXLLNLTest
						testCntr = 0;  //XXXLLNLTest
#endif

//					if subapNdx indicates that we're looking for the camera sync pulse
//						loop through the DMA data looking for the camera sync pulse (active low, on even words)
					if (subapNdx < 0)
					{
#ifdef LLNL_TEST
/*XXXLLNLTest start*/
						while ((dmaBuffDataNdx < NUM_DMA_WORDS) && (testCntr != 0))
						{
							if (++testCntr >= 7400)
								testCntr = 0;
							dmaBuffDataNdx += 2;
						}
/*XXXLLNLTestXXX middle*/
#else
						while ((dmaBuffDataNdx < NUM_DMA_WORDS) && 
                               ((DMABuffPtrs[DMABuffReadNdx][dmaBuffDataNdx] & CAM_SYNC_BIT) != 0))
							dmaBuffDataNdx += 2;
/*XXXLLNLTest end*/
#endif

//						if a sync pulse was not found
//							if we've cycled through more than ~1 second's worth of data, report an error
						if (dmaBuffDataNdx >= NUM_DMA_WORDS)
						{
							if (++noSyncCntr > 7500)  // 7500 is a little more than a second's worth of data
							{
								noSyncCntr = 0;
								SysConSendStrngNoParm("Error: can't find camera sync pulse");
							}
						}

//						else
//							initialize variables to be able to move data into camera buffers
//                          if we successfully posted last time
//							    find the next available camera data buffer
//							    if there isn't an available camera data buffer
//								    report an error
//							    if the buffer we're about to use has unread diag data in it
//							        initiate a diag data resync
						else
						{
/*/FFF start TEST2
                            if (++timeCntr >= 100)
                            {
                                timeCntr = 0;

                                timeLength = SysConTimeDiff(timeKeep, TimeCamDrvr3);
                                sprintf(textBuff, "frame Hz: %ld", 100000000 / timeLength);  // frame frequency in Hz
                                SysConSendStrngNoParm(textBuff);

                                timeKeep = TimeCamDrvr3;
//                                clock_gettime(CLOCK_REALTIME, &timeKeep);  //TTT
                            }
//FFF end*/
							noSyncCntr = 0;
                            validSync = true;
							subapNdx = 0;
							subapRowNdx = 0;
							unusedRowNdx = 0;
							subapDataNdx = 0;
#ifdef LLNL_TEST
							testData += 1;  //XXXLLNLTest
#endif

							if (successfulPost)
							{
                                buffCntr = 0;
                                buffNdx = CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx];
                                firstFree = -1;
                                firstFreeButWithDiag = -1;
                                while ((buffCntr < NUM_CAM_BUFFS) && (firstFree < 0))
                                {
                                    if (++buffNdx >= NUM_CAM_BUFFS)
                                        buffNdx = 0;
                                    if (!CamDataPtr->camBuffs[buffNdx].inUseFlag)
                                    {
                                        if ((CamDataPtr->diagStatFlag != eDiagStatOperating) ||
                                            (CamDataPtr->camBuffs[buffNdx].diagIndicator == eDiagIndNotInUse))
                                            firstFree = buffNdx;
                                        else if (firstFreeButWithDiag < 0)
                                            firstFreeButWithDiag = buffNdx;
                                    }
                                    buffCntr++;
                                }

                                if ((firstFree < 0) && (firstFreeButWithDiag < 0))
                                    SysConSendStrngNoParm("Error: camera buffers all full");
                                else
                                {
                                    if (++CamDataPtr->writeNdx >= NUM_CAM_BUFFS)
                                        CamDataPtr->writeNdx = 0;

                                    if (firstFree < 0)
                                    {
                                        firstFree = firstFreeButWithDiag;

                                        CamDataPtr->diagStatFlag = eDiagStatResyncing;
                                        diagFIFOMsg.diagID = eDiagCamData;
                                        diagFIFOMsg.diagStat = eDiagStatResyncing;
                                        diagFIFOMsg.diagReadNdx = firstFree;
                                        if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
                                            SysConSendStrngIntParm("Error writing camera resync to diag FIFO, errno = ", rtl_errno);
                                        SysConSendStrngNoParm("Warning: camera diagnostic data is resyncing");
                                    }
                                    CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx] = firstFree;
                                    camBuffPtr = &CamDataPtr->camBuffs[firstFree];
                                    camBuffPtr->inUseFlag = true;
                                }
								successfulPost = false;
							}
						}
					}

//					if subapNdx indicates that we've got usable frame data (i.e, we've found a sync pulse and everything's okay)
//						move DMA data to cam data buffer, checking for unexpected sync pulses and unscrambling the data
//						> as we go
					if (subapNdx >= 0)
					{
						diagDataAvail = false;
						while (dmaBuffDataNdx < NUM_DMA_WORDS)
						{
							dataEl1 = DMABuffPtrs[DMABuffReadNdx][dmaBuffDataNdx];
							dataEl2 = DMABuffPtrs[DMABuffReadNdx][dmaBuffDataNdx + 1];
                            dmaBuffDataNdx += 2;
#ifdef LLNL_TEST
/*XXXLLNLTest start*/
							{
								dataEl1 = testData;
								testData += 1;
								dataEl2 = testData;
								testData += 1;
/*XXXLLNLTestXXX middle*/
#else
							if (((dataEl1 & CAM_SYNC_BIT) == 0) && !validSync)  // unexpected sync pulse
							{
								subapNdx = -1;
								SysConSendStrngIntParm("Error: unexpected sync pulse in DMA data, index = ", dmaBuffDataNdx - 2);
								break;  // terminate the loop, we've got an error
							}
							else if ((dataEl1 & CAM_DATA_VALID_BIT) == 0)
							{
                                validSync = false;
/*XXXLLNLTest end*/
#endif

								if (subapNdx < NUM_SUBAP_ROWS_PER_FRAME)  // we're in a subap row (as opposed to the unused camera rows)
								{
									camBuffPtr->data[subapNdx][subapRowNdx]
													[camDataMap[subapDataNdx]] = (int16)(dataEl1 & CAM_DATA_MASK);
									camBuffPtr->data[subapNdx][subapRowNdx]
													[camDataMap[subapDataNdx + 1]] = (int16)(dataEl2 & CAM_DATA_MASK);

									if ((subapDataNdx += 2) >= NUM_PIX_IN_ROW)
									{
										subapDataNdx = 0;
										if (++subapRowNdx >= NUM_CAM_ROWS_PER_SUBAP)
										{
											subapRowNdx = 0;
                                            camBuffPtr->subapRowWriteNdx = subapNdx;
											if (rtl_sem_post(&CentSem) < 0)
											{
												SysConSendStrngIntParm("Error posting to CentSem, errno = ", rtl_errno);
                                                break;  // terminate the loop, we've got an error
											}
											successfulPost = true;
											++subapNdx;
										}
									}
								}
								else  // we're in the unused camera rows
								{
									camBuffPtr->unusedDataBot[unusedRowNdx]
                                                          	 [camDataMap[subapDataNdx]] = (int16)(dataEl1 & CAM_DATA_MASK);
									camBuffPtr->unusedDataBot[unusedRowNdx]
                                                          	 [camDataMap[subapDataNdx + 1]] = (int16)(dataEl2 & CAM_DATA_MASK);

									if ((subapDataNdx += 2) >= NUM_PIX_IN_ROW)
									{
										subapDataNdx = 0;
										if (++unusedRowNdx >= NUM_UNUSED_CAM_ROWS_BOT)
										{
											subapNdx = -1;
											diagDataAvail = true;
											break;  // terminate the loop, we're done with the frame
										}
									}
								}
							}
						}

//						if diagnostic data is available to be sent (i.e., we've received a full frame)
						if (diagDataAvail)
						{
//							if diag data transfer is to be restarted and we're not operating already
//								indicate that we're operating
//							else if diag data transfer is to enter standby and we're not in standby already
//								indicate that we're operating in standby
							if ((CamDataPtr->diagActFlag == eDiagActRestart) && (CamDataPtr->diagStatFlag != eDiagStatOperating))
                            {
								CamDataPtr->diagStatFlag = eDiagStatOperating;
                                CamDataPtr->diagActFlag = eDiagActNoAction;
                            }
							else if ((CamDataPtr->diagActFlag == eDiagActEnterStandby) && (CamDataPtr->diagStatFlag != eDiagStatStandby))
                            {
								CamDataPtr->diagStatFlag = eDiagStatStandby;
                                CamDataPtr->diagActFlag = eDiagActNoAction;
                            }

//							if diagnostic data transfer (to the SRT) is operating and a message should be sent
//								send the camera data off to diag
							if ((CamDataPtr->diagTelemRate > 0) && 
								(++diagTelemCntr >= (CamFrameRate / CamDataPtr->diagTelemRate)))
							{
								diagTelemCntr = 0;
								sendTelemData = true;
							}
							else
								sendTelemData = false;
							if ((CamDataPtr->diagStatFlag == eDiagStatOperating) && 
								((CamDataPtr->diagDiagCntr > 0) || sendTelemData))
							{
								if (sendTelemData)
									camBuffPtr->diagIndicator = eDiagIndTelem;
								else
									camBuffPtr->diagIndicator = eDiagIndInUse;

								diagFIFOMsg.diagID = eDiagCamData;
								diagFIFOMsg.diagStat = eDiagStatOperating;
								diagFIFOMsg.diagReadNdx = CamDataPtr->camBuffNdxs[CamDataPtr->writeNdx];

								if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
								{
									camBuffPtr->diagIndicator = eDiagIndNotInUse;
									SysConSendStrngIntParm("Error writing camera data to diag FIFO, errno = ", rtl_errno);
								}
                                else
                                {
                                    if (CamDataPtr->diagDiagCntr > 0)
                                        CamDataPtr->diagDiagCntr--;
                                }
							}
						}
					}
				}
				DMABuffWriteCntrs[DMABuffReadNdx] = 0;
			}

//            clock_gettime(CLOCK_REALTIME, &TimeCam2);  //TTT
		}
	}

	return 0;
}


/**********************************************************************************************

UNIT NAME:  CamInit()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the camera thread.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamInit( void )
{
	int i, retVal;
	struct rtl_timespec next;

//	initialize variables
	noSyncCntr = 0;
	subapNdx = -1;

	CamDataPtr->cmdStart = eCmdNone;
	CamDataPtr->cmdCrnt = eCmdNone;

	CamDataPtr->writeNdx = NUM_CAM_BUFFS - 1;
	CamDataPtr->readNdx = NUM_CAM_BUFFS - 1;

	CamDataPtr->diagActFlag = eDiagActNoAction;
	CamDataPtr->diagStatFlag = eDiagStatStandby;
	CamDataPtr->diagDiagRate = 0;
    CamDataPtr->diagDiagCntr = 0;
	CamDataPtr->diagTelemRate = 0;
	CamDataPtr->diagPSFFlag = false;

	for (i = 0; i < NUM_CAM_BUFFS; i++)
    {
        CamDataPtr->camBuffNdxs[i] = NUM_CAM_BUFFS - 1;
        CamDataPtr->camBuffs[i].inUseFlag = false;
        CamDataPtr->camBuffs[i].diagIndicator = eDiagIndNotInUse;
        CamDataPtr->camBuffs[i].subapRowWriteNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
        CamDataPtr->camBuffs[i].subapRowReadNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
    }

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
	while (!OkayToFinishInit)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in CamInit(), retVal = ", retVal);
    }

    CamInitDone = true;
}


/**********************************************************************************************

UNIT NAME:  CamRestart()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the camera thread when processing
is restarted after the camera has been turned off then back on.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void CamRestart( void )
{
	int i;

//	initialize variables
	noSyncCntr = 0;
	subapNdx = -1;

	CamDataPtr->writeNdx = NUM_CAM_BUFFS - 1;
	CamDataPtr->readNdx = NUM_CAM_BUFFS - 1;

	for (i = 0; i < NUM_CAM_BUFFS; i++)
    {
        CamDataPtr->camBuffNdxs[i] = NUM_CAM_BUFFS - 1;
        CamDataPtr->camBuffs[i].inUseFlag = false;
        CamDataPtr->camBuffs[i].diagIndicator = eDiagIndNotInUse;
        CamDataPtr->camBuffs[i].subapRowWriteNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
        CamDataPtr->camBuffs[i].subapRowReadNdx = NUM_SUBAP_ROWS_PER_FRAME - 1;
    }
}


/**********************************************************************************************

UNIT NAME:  CamProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process camera commands.

UNIT INPUTS:
	cmdNdx - index into CmdTable[] for the command to be processed

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamProcCmd( int16 cmdNdx )
{
	char camMsg[SERIAL_BUF_LNGTH];

//	switch on command ID corresponding to command index
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		   // abort long commands
			SysConSendStrngNoParm("Error: unexpected command abort received by cam");
			break;

		case eCmdCamWFSInit:   // initialize SciMeasure WFS camera
			SysConSendStrngNoParm("Error: camera init command not supported by AOA");
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamRate:      // set frame rate of the camera (per second)
			CamFrameRate = CamDataPtr->cmdIntParam;
			CamFrameRateNdx = 0;  // find a legal camera rate
			while ((CamFrameRate > LegalFrameRates[CamFrameRateNdx]) && (LegalFrameRates[CamFrameRateNdx + 1] != -1))
				CamFrameRateNdx ++;
			CamFrameRate = LegalFrameRates[CamFrameRateNdx];
			sprintf(camMsg, "RATE %d\r", CamFrameRate);
            serialTimeoutCntr = 0;
			CamSerialWrite(camMsg);
			FileDataPut(eFileTypeParms);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamGain:      // set camera gain (1 or 2)
			CamGain = CamDataPtr->cmdIntParam;
			if (CamGain < 1)       //XXX gain can now 
				CamGain = 1;       //XXX > be 0, 1, 2, or 3
			else if (CamGain > 2)  //XXX > for SMC camera 
				CamGain = 2;
			sprintf(camMsg, "GAIN %d\r", CamGain);
            serialTimeoutCntr = 0;
			CamSerialWrite(camMsg);
			FileDataPut(eFileTypeParms);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamCRTGain:   // set camera CRT gain (0 to 4)
			if (CamDataPtr->cmdIntParam > 4)  //XXXRS170
				CamDataPtr->cmdIntParam = 4;  //XXXRS170
			CamCRTGain = CamDataPtr->cmdIntParam;
			sprintf(camMsg, "CRTGAIN %d\r", CamCRTGain);
            serialTimeoutCntr = 0;
			CamSerialWrite(camMsg);
			FileDataPut(eFileTypeParms);
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSRate:	   // read frame rate from camera
            serialTimeoutCntr = 0;
			CamSerialWrite("RATE\r");
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSGain:     // read gain from camera
            serialTimeoutCntr = 0;
			CamSerialWrite("GAIN\r");
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdCamSCRTGain:  // read CRT gain from camera
            serialTimeoutCntr = 0;
			CamSerialWrite("CRTGAIN\r");
			CamDataPtr->cmdCrnt = eCmdNone;
			break;

		default:
			SysConSendStrngIntParm("Error: unexpected command received by cam, cmdNdx = ", cmdNdx);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CamSerialWrite()

UNIT DESCRIPTION:
This unit simply sends a character string in the camera's serial port output buffer (to be 
handled by the CamSerial() thread).

UNIT INPUTS:
1.	serialString[]	- null-terminated character string to be sent to the camera's serial port

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamSerialWrite( char serialString[] )
{
    int16 i, strLength;

//  write character string to the camera's serial output buffer
    strLength = strlen(serialString);
    for (i = 0; i < strLength; i++)
    {
        camSerialOutBuf[camSerialOutBufPutNdx] = serialString[i];
        if (++camSerialOutBufPutNdx >= CAM_SER_OUT_BUF_LEN)
            camSerialOutBufPutNdx = 0;
    }
}


/**********************************************************************************************

UNIT NAME:  CamSerial()

UNIT DESCRIPTION:
This is a thread that handles the serial I/O with the camera.  It periodically checks for serial 
input from the camera and, when a full message has been received (as delimited by a CR), the 
message is sent off to the host.  Also, if there is serial data to go out, the data is sent.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamSerial( void *arg )
{
	int i, retVal;
	struct rtl_timespec next;
	char inBuf[SERIAL_BUF_LNGTH];
    char textBuff[TEXT_BUF_LNGTH];

//  spin on the InitDone flags for the other threads
    while (!TiptTiltAPDInitDone || !CamInitDone || !CentInitDone || !ReconInitDone || !CamLTTInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm("Error from nanosleep() in CamSerial(), retVal = ", retVal);
    }

    serialMsgNdx = 0;
    fullSerialMsgFlag = false;

//	enter infinite loop
	while (1)
	{
//		wait for half a second
		next.tv_sec = 0;
		next.tv_nsec = 500000000;
   		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep(), retVal = ", retVal);

//		try to read camera serial data and if there is new data
//          parse the data and, if a full message has been received, send it off to the host
		if ((retVal = rtl_read(CamSerialfd, inBuf, sizeof(inBuf))) < 0)
            SysConSendStrngIntParm("Error from camera serial read, errno = ", rtl_errno);
		else if (retVal > 0)
        {
            i = 0;
            while (i < retVal)
            {
                while ((i < retVal) && (serialMsgNdx < SERIAL_BUF_LNGTH) && !fullSerialMsgFlag)
                {
                    if (inBuf[i] == '\n')
                    {
                        fullSerialMsgFlag = true;
                        serialMsg[serialMsgNdx] = '\0';
                    }
                    else
                    {
                        if (rtl_isalnum(inBuf[i]) || (inBuf[i] == ' '))
                        {
                            serialMsg[serialMsgNdx] = inBuf[i];
                            serialMsgNdx++;
                        }
                    }
                    i++;
                }

                if (fullSerialMsgFlag)
                {
                    if ((serialMsgNdx > 0) && ((strncmp(camStrs[0], serialMsg, strlen(camStrs[0])) == 0) ||  // frame rate
											   (strncmp(camStrs[1], serialMsg, strlen(camStrs[1])) == 0) ||  // camera gain
											   (strncmp(camStrs[2], serialMsg, strlen(camStrs[2])) == 0)))	  // CRT gain
                    {
                        sprintf(textBuff, "Notification from camera: %s", serialMsg);
                        SysConSendStrngNoParm(textBuff);
                        serialTimeoutCntr = -1;
                    }
                    serialMsgNdx = 0;
                    fullSerialMsgFlag = false;
                }
                else if (serialMsgNdx >= SERIAL_BUF_LNGTH)
                {
                    SysConSendStrngNoParm("Error: camera serial message too long");
                    serialMsgNdx = 0;
                }
            }
        }

//      check to see if the camera's serial channel has timed out (i.e., a command has been issued, 
//      > but no response has been received)
        if (serialTimeoutCntr > -1)
        {
            if (++serialTimeoutCntr >= 6)  // ~3 seconds
            {
                serialTimeoutCntr = -1;
                SysConSendStrngNoParm("Error: camera serial timeout");
            }
        }

//      if there are characters to go out, send them out one at a time and slowly (so as not to disable interrupts 
//      > for too long)
        while (camSerialOutBufPutNdx != camSerialOutBufGetNdx)
        {
            if (rtl_write(CamSerialfd, &camSerialOutBuf[camSerialOutBufGetNdx], 1) < 0)
                SysConSendStrngIntParm("Error from camera serial write, errno = ", rtl_errno);
            if (++camSerialOutBufGetNdx >= CAM_SER_OUT_BUF_LEN)
                camSerialOutBufGetNdx = 0;

            next.tv_sec = 0;
            next.tv_nsec = 1000000;  // wait a millisecond (which corresponds to 1000 characters/sec. or ~9000 baud)
            if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
                SysConSendStrngIntParm("Error from nanosleep(), retVal = ", retVal);
        }
	}
}
