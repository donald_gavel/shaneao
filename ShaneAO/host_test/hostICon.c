/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		hostICon

AUTHOR:	DWP			DATE: 1/30/03

COMPONENT DESCRIPTION:
This component is the host interface controller.  To begin with, its main purpose is to perform
initialization for the host interface (establish the host socket, create the hostDiag thread,
etc.).  Then, it's primary responsibility is to sit on the host input socket, parse messages when
received, and send the result off to the HRT (Hard Real Time) processor via an RTLinux FIFO.
So, after initialization, it could be cosidered the 'host input controller'.

COMPONENT UNITS:
1.	main()
2.	HostIConProcMsgs()
3.	HostIConProcCmd()
4.	HostIConSendStrngNoParm()
5.	HostIConSendStrngIntParm()
6.	HostIConSendStrngFltParm()

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
//#include <rtl_fifo.h>

#include "types.h"
#include "constant.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"



// Global Variables

FileDataShmType *FileDataShmPtr;  // pointer to file data shared memory
CamDataType *CamDataPtr;		  // pointer to camera shared memory data
LTTCamDataType *CamLTTPtr;		  // pointer to LTT camera shared memory data
CentDataType *CentDataPtr;		  // pointer to centroid shared memory data
ReconDataType *ReconDataPtr;	  // pointer to recon shared memory data

int HostSocket;  // ethernet socket for host communications

int DiagFIFOfd;  // fd for FIFO used to receive diagnostic data to the diag thread

boolean HostConnectedFlag = false;  // indicates whether the host socket is connected

#include "cmdTable.h"  // the command table is in a separate file so that both the HRT and SRT programs can include it


// Local Variables

static int cmdFIFOfd;   // fd for FIFO used to send command messages to the HRT side

static pthread_t hostDiagThread;  // thread handles

// Global Function Prototype
int16 HostIConProcCmd(char msgBuf[]);

// Local Function Prototypes

static int16 HostIConProcMsgs(void);
static void HostIConSendStrngNoParm( char *strngPtr );
static void HostIConSendStrngIntParm( char *strngPtr, int16 intParam );
static void HostIConSendStrngFltParm( char *strngPtr, float32 floatParam );
static void Change_Cam_Buf_Mapping (int16 idNum);

/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the host interface mainline.  Its purpose is to open FIFOs and shared memory for 
communication with the HRT processor and an ethernet socket for communication with the host.
It then calls HostIConProcMsgs() to process host messages (HostIConProcMsgs() will not return 
unless the socket is lost or a restart message is received).

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
    static struct sockaddr_in address;

	boolean restartFlag, shutdownFlag, quitFlag;

	int mmfd;					    // mmap file descriptor
	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;
	int totalSize;				    // total size of shared memory
	unsigned char *startPtr;	    // pointer to start of shared memory

	int retVal;
	pthread_attr_t attr;
    struct sched_param param;

    int serverSocket, port;

//	spin on the FIFO for sending command messages to the HRT side until it is opened
	printf("Waiting to connect to HRT...\n");
	while ((cmdFIFOfd = open("/dev/rtf1", O_NONBLOCK | O_CREAT | O_WRONLY)) < 0)
        ;
	printf("   HRT connected!\n");

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

	printf("trace1\n");  //dwp100405
//	open the shared memory region, exiting if an error occurs
	if ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0)
	{
		printf("Error creating shared mem on Linux side, errno = %d\n", errno);
		exit(-1);
	}

	printf("trace2\n");  //dwp100405
//	get the pointer to the shared memory region, exiting if an error occurs
	if ((startPtr = mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED)
	{
		printf("Error getting pointer to shared mem on SRT side, errno = %d\n", errno);
		exit(-1);
	}

//	set up the pointers to the global variables
	FileDataShmPtr = (FileDataShmType *)startPtr;
	CamDataPtr = (CamDataType *)((char *)FileDataShmPtr + shmDataSize);
	CentDataPtr = (CentDataType *)((char *)CamDataPtr + camDataSize);
	ReconDataPtr = (ReconDataType *)((char *)CentDataPtr + centDataSize);
	CamLTTPtr = (LTTCamDataType *)((char *)ReconDataPtr + reconDataSize);

	printf("trace3\n");  //dwp100405
//	initialize the two SciMeasure cameras
	CamLTTMngr_StartCam();
	if (FileDataShmPtr->UseAOA == 0)
		CamWFSMngr_StartCam();

	printf("trace4\n");  //dwp100405
//	open the FIFO for sending diagnostic data notification messages to the diag thread, exiting if an error occurs
	if ((DiagFIFOfd = open("/dev/rtf0", O_CREAT | O_RDWR)) < 0)
	{
		printf("Error creating diagnostic FIFO from hostICon, errno = %d\n", errno);
		exit(-1);
	}

	printf("trace5\n");  //dwp100405
//  create and bind the server socket to establish communication with the host, exiting if an error occurs
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		printf("Error creating server socket, errno = %d\n", errno);
		exit(-1);
	}

    port = 15000;
    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    address.sin_addr.s_addr = INADDR_ANY;
    while ((bind(serverSocket, (struct sockaddr *)&address, sizeof(address)) < 0) && (port < 15010))
    {
        port++;
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        address.sin_addr.s_addr = INADDR_ANY;
    }
    if (port < 15010)
		printf("Server socket bound as port %d\n", port);
    else
	{
		printf("Error binding server socket, errno = %d\n", errno);
		exit(-1);
	}

	printf("trace6\n");  //dwp100405
//	create hostDiag thread, exiting if an error occurs
	pthread_attr_init(&attr);
	pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 15;
	pthread_attr_setschedparam(&attr, &param);
	if ((retVal = pthread_create(&hostDiagThread, &attr, HostDiag, 0)) != 0)
	{
		printf("Error creating host diag thread, retVal = %d\n", retVal);
		exit(-1);
	}

	printf("trace7\n");  //dwp100405
//	set the srtIsUpFlag in shared memory to let the HRT know that the SRT is up (the HRT will be spinning on this flag)
	FileDataShmPtr->srtIsUpFlag = true;

//	enter an almost-infinite loop (exit if a system error occurs or drop out if a restart or shutdown command is received)
	restartFlag = false;
	shutdownFlag = false;
	quitFlag = false;

	while (!restartFlag && !shutdownFlag && !quitFlag)
	{

//		listen on the server socket and accept when a connection is initiated, exiting if an error occurs
		printf("Waiting to connect to host...\n");
		if (listen(serverSocket, 0) < 0)
		{
			printf("Error listening on server socket, errno = %d\n", errno);
			exit(-1);
		}

		if ((HostSocket = accept(serverSocket, NULL, NULL)) < 0)
		{
			printf("Error accepting host socket, errno = %d\n", errno);
			exit(-1);
		}

        HostConnectedFlag = true;
		HostIConSendStrngNoParm("   Host connected!");
		printf("   Host connected!\n");

//		call HostIConProcMsgs() to process host messages; HostIConProcMsgs() will not return unless
//		> the socket is lost or a quit, restart, or shutdown message is received
		retVal = HostIConProcMsgs();
		if (retVal == eErrQuit)
        {
            HostIConSendStrngNoParm("Notification: quitting control program...");
            sleep(2);  // let the message be sent
			quitFlag = true;
        }
		else if (retVal == eErrRestart)
        {
            HostIConSendStrngNoParm("Notification: restarting system...");
            sleep(2);  // let the message be sent
			restartFlag = true;
        }
		else if (retVal == eErrShutdown)
        {
            HostIConSendStrngNoParm("Notification: shutting down system...");
            sleep(2);  // let the message be sent
			shutdownFlag = true;
        }

        HostConnectedFlag = false;

//		shut the socket down and close it
		shutdown(HostSocket, SHUT_RDWR);
	    close(HostSocket);
	}

//	shut everything down
	CamLTTMngr_StopCam();
	if (FileDataShmPtr->UseAOA == 0)
		CamWFSMngr_StopCam();

	pthread_cancel(hostDiagThread);
	pthread_join(hostDiagThread, NULL);

	munmap(startPtr, totalSize);
  	close(mmfd);

    shutdown(serverSocket, SHUT_RDWR);
    close(serverSocket);

	close(cmdFIFOfd);
	close(DiagFIFOfd);

//	return to quit the control program, restart (reboot) the system, or shutdown the system
	if (quitFlag)
        return 250;
	else if (restartFlag)
        return 251;
	else if (shutdownFlag)
        return 252;
    else
        return -1;
}


/**********************************************************************************************

UNIT NAME:  HostIConProcMsgs()

UNIT DESCRIPTION:
The purpose of this unit is to process messages from the host, by receiving them, parsing them,
and calling HostIConProcCmd() to process commands.

UNIT INPUTS:
	None

UNIT OUTPUTS:
1.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrLostSocket	- lost the socket used for host communications
2.	eErrRestart		- a restart command has been received from the host
3.	eErrShutdown	- a shutdown command has been received from the host
4.	eErrQuit	    - a quit command has been received from the host

**********************************************************************************************/

int16 HostIConProcMsgs(void)
{
	const int inBuffLngth = 128, msgBuffLngth = 2048;
    static int16 msgNdx = 0, retVal;
	char inBuf[inBuffLngth], msgBuf[msgBuffLngth];
	int16 numBytes, i;

//	enter a nearly infinite loop (return if the socket is lost or a restart command is received)
	while (1)
	{
//		receive a message from the host via the host socket
		numBytes = recv(HostSocket, inBuf, inBuffLngth, 0);

//		if an error occurred in the receive, report it
		if (numBytes < 0)
			HostIConSendStrngIntParm("Socket receive error, errno: ", errno);
//		else if zero bytes have been received (other side has shut down), return indicating that socket has been lost
		else if (numBytes == 0)
			return eErrLostSocket;
//		else
		else
		{
//			parse the message (with a stream socket, there's no guarantee that messages will come in tidy packets),
//			> calling HostIConProcCmd() as necessary; return if a restart command has been received
			i = 0;
			while (i < numBytes)
			{
				while ((i < numBytes) && (inBuf[i] != '\0') && (inBuf[i] != '\n') && (msgNdx < msgBuffLngth))
				{
					msgBuf[msgNdx] = inBuf[i];
					msgNdx++;
					i++;
				}
				if (msgNdx >= msgBuffLngth)
				{
					msgNdx = 0;
					i = numBytes;  // so outer loop will fall through
					HostIConSendStrngNoParm("Error: message buffer overflow");
				}
				else if (i < numBytes)
				{
					msgBuf[msgNdx] = '\0';
					retVal = HostIConProcCmd(msgBuf);
					if ((retVal == eErrQuit) || (retVal == eErrRestart) || (retVal == eErrShutdown))
						return retVal;
					msgNdx = 0;
					i++;
				}
			}
		}
	}
}


/**********************************************************************************************

UNIT NAME:  HostIConProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process commands received from the host.  Most valid messages on
are passed to the System Controller on the HRT processor.  Some messages (restart and diagnostic
messages) are handled on this processor (the Soft Real Time (SRT) processor).

UNIT INPUTS:
1.	msgBuf			- null-terminated buffer containing ASCII message from the host

UNIT OUTPUTS:
1.	return value	- see error codes below

UNIT ERROR CODES:
1.	eErrNoErr		- no error (no unhandled error, at least)
2.	eErrRestart		- a restart command has been received from the host
3.	eErrShutdown	- a shutdown command has been received from the host
4.	eErrQuit	    - a quit command has been received from the host

**********************************************************************************************/

int16 HostIConProcCmd(char msgBuf[])
{
	boolean nullEncountered;
    int16 i;
	int16 msgNdx, cmdNdx;
	int16 intParam = 0;
	float32 floatParam = 0.0;
	char strngParam[MAX_FILENAME_LENGTH] = "";
	cmdFIFOMsgType cmdFIFOMsg;
	diagFIFOMsgType diagFIFOMsg;

    char *testBufPtr;

//	find the end of the first word in the message (presumably the command) and null-terminate it
	msgNdx = 0;
	while ((msgBuf[msgNdx] != ' ') && (msgBuf[msgNdx] != '\0'))
    {
        printf("%x ", msgBuf[msgNdx]);
		msgNdx++;
    }
    printf("\n");
	if (msgBuf[msgNdx] == '\0')
		nullEncountered = true;
	else
	{
		nullEncountered = false;
		msgBuf[msgNdx] = '\0';
	}

//	look for the command in the command table and report an error if it's not there
	cmdNdx = 0;
	while ((cmdNdx < eCmdNumCmds) && (strcmp(msgBuf, CmdTable[cmdNdx].strng) != 0))
		cmdNdx++;
	if (cmdNdx == eCmdNumCmds)
	{
		HostIConSendStrngNoParm("Error: invalid command from host");
		return eErrNoErr;
	}

//	if there is a command parameter, convert it to binary, reporting an error if there is a problem
	if (CmdTable[cmdNdx].paramIndicator != eCmdParamNone)
	{
		if (nullEncountered)
		{
			HostIConSendStrngNoParm("Error: parameter expected with command from host");
			return eErrNoErr;
		}

		msgNdx++;  // get past the null

		switch (CmdTable[cmdNdx].paramIndicator)
		{
			case eCmdParamInt:
	            intParam = strtol(&msgBuf[msgNdx], &testBufPtr, 10);
	            if ((testBufPtr[0] != '\0') && (testBufPtr[0] != ' '))
				{
					HostIConSendStrngNoParm("Error converting integer command parameter");
					return eErrNoErr;
				}
				if ((CmdTable[cmdNdx].paramLimLowI != CmdTable[cmdNdx].paramLimHighI) &&
					((intParam < CmdTable[cmdNdx].paramLimLowI) || (intParam > CmdTable[cmdNdx].paramLimHighI)))
				{
					HostIConSendStrngNoParm("Integer command parameter out of range");
					return eErrNoErr;
				}
				break;

			case eCmdParamFloat:
	            floatParam = strtod(&msgBuf[msgNdx], &testBufPtr);
	            if ((testBufPtr[0] != '\0') && (testBufPtr[0] != ' '))
				{
					HostIConSendStrngNoParm("Error converting float command parameter");
					return eErrNoErr;
				}
				if ((CmdTable[cmdNdx].paramLimLowF != CmdTable[cmdNdx].paramLimHighF) &&
					((floatParam < CmdTable[cmdNdx].paramLimLowF) || (floatParam > CmdTable[cmdNdx].paramLimHighF)))
				{
					HostIConSendStrngNoParm("Float command parameter out of range");
					return eErrNoErr;
				}
				break;

			case eCmdParamStrng:
				while ((msgBuf[msgNdx] == ' ') && (msgBuf[msgNdx] != '\0'))  // get past any additional blanks
					msgNdx++;
				if (msgBuf[msgNdx] == '\0')
				{
					HostIConSendStrngNoParm("Error: parameter expected with command from host");
					return eErrNoErr;
				}
				if (strlen(&msgBuf[msgNdx]) >= MAX_FILENAME_LENGTH)
				{
					HostIConSendStrngNoParm("Error: parameter expected with command from host");
					return eErrNoErr;
				}
				strcpy(strngParam, &msgBuf[msgNdx]);
				break;

			case eCmdParamSpecial:  // 'special' parameters, 80 float values to accomodate centoffs
				testBufPtr = &msgBuf[msgNdx];
				for (i = 0; i < NUM_CENTROIDS; i++)
				{
		            if (testBufPtr[0] == '\0')
					{
						HostIConSendStrngNoParm("Error: too few parameters in centoff command");
						return eErrNoErr;
					}

					FileDataShmPtr->newCentoffs[i] = strtod(testBufPtr, &testBufPtr);

		            if ((testBufPtr[0] != '\0') && (testBufPtr[0] != ' '))
					{
						HostIConSendStrngIntParm("Error converting centoff value: ", i);
						return eErrNoErr;
					}
					if ((CmdTable[cmdNdx].paramLimLowF != CmdTable[cmdNdx].paramLimHighF) &&
						((FileDataShmPtr->newCentoffs[i] < CmdTable[cmdNdx].paramLimLowF) || 
						 (FileDataShmPtr->newCentoffs[i] > CmdTable[cmdNdx].paramLimHighF)))
					{
						HostIConSendStrngFltParm("Error: centoff value out of range: ", FileDataShmPtr->newCentoffs[i]);
						return eErrNoErr;
					}
				}
				break;
		}
	}

//	if we've gotten this far, we have a validly formatted command, so send it off
	switch (CmdTable[cmdNdx].action)
	{
		case eCmdActQuit:	    // quit AO system (return to mainline to quit)
			return eErrQuit;
			break;

		case eCmdActRestart:	// restart AO system (return to mainline to perform reset -r)
			return eErrRestart;
			break;

		case eCmdActShutdown:	// shutdown AO system (return to mainline to perform reset -h)
			return eErrShutdown;
			break;

		case eCmdActSendToDiag:	// send to diagnostic thread
			diagFIFOMsg.diagID = eDiagCmdHost;
			diagFIFOMsg.cmdNdx = cmdNdx;
			diagFIFOMsg.intParam = intParam;
			diagFIFOMsg.floatParam = floatParam;

			if (write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
				printf("Error writing to diag FIFO, SRT side, errno: %d\n", errno);
			break;

		case eCmdActSendToHRT:	  // send to HRT processor
		case eCmdActSendToCam:    // send to HRT camera thread
		case eCmdActSendToLTT:    // send to HRT LTT camera thread
        case eCmdActSendToCent:	  // send to HRT centroid thread
        case eCmdActSendToRecon:  // send to HRT recon thread
			cmdFIFOMsg.cmdNdx = cmdNdx;
			if (CmdTable[cmdNdx].paramIndicator == eCmdParamInt)
				cmdFIFOMsg.intParam = intParam;
			else if (CmdTable[cmdNdx].paramIndicator == eCmdParamFloat)
				cmdFIFOMsg.floatParam = floatParam;
			else if (CmdTable[cmdNdx].paramIndicator == eCmdParamStrng)
				strcpy(cmdFIFOMsg.strngParam, strngParam);

			if (write(cmdFIFOfd, &cmdFIFOMsg, sizeof(cmdFIFOMsgType)) < 0)
				HostIConSendStrngIntParm("Error writing to command FIFO, SRT side, errno: ", errno);
			break;

		default:
			HostIConSendStrngIntParm("Error: invalid command table action = ", CmdTable[cmdNdx].action);
			break;
	}

	return eErrNoErr;
}

/**********************************************************************************************

UNIT NAME:  HostIConSendStrngNoParm()
UNIT NAME:  HostIConSendStrngIntParm()
UNIT NAME:  HostIConSendStrngFltParm()

UNIT DESCRIPTION:
These units sends a null-terminated string (generally an error message) to hostDiag to be sent
to the host.

UNIT INPUTS:
1.	strngPtr    - pointer to null-terminated character string
2.	intParam	- integer parameter
3.	floatParam	- float parameter

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void HostIConSendStrngNoParm( char *strngPtr )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
		printf("Error writing noParm message to diag FIFO, SRT side, errno = %d\n", errno);
		printf("*** %s\n", strngPtr);
    }
}


static void HostIConSendStrngIntParm( char *strngPtr, int16 intParam )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngIntParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	diagMsg.diagFIFOMsg.intParam = intParam;

	if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
		printf("Error writing intParm message to diag FIFO, SRT side, errno = %d\n", errno);
		printf("*** %s %d\n", strngPtr, intParam);
    }
}


static void HostIConSendStrngFltParm( char *strngPtr, float32 floatParam )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngFltParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	diagMsg.diagFIFOMsg.floatParam = floatParam;

	if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
		printf("Error writing floatParm message to diag FIFO, SRT side, errno = %d\n", errno);
		printf("*** %s %g\n", strngPtr, floatParam);
    }
}
