/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			cam.h

AUTHOR:	DWP			DATE: 1/7/03

FILE DESCRIPTION:
This is the header file for the camera component.

******************************************************************************/

#ifndef _CAMERA_H
#define _CAMERA_H		1


// Defines

#define INIT_FRAME_RATE				100  	// initial camera rate in frames per second
#define INIT_CAM_GAIN				1		// initial camera gain (1 or 2)
#define INIT_CRT_GAIN				0		// initial camera CRT gain (0 to 4)

#define CAM_SYNC_BIT				0x8000  // sync pulse bit in camera DMA data
#define CAM_DATA_VALID_BIT			0x4000  // data valid bit in camera DMA data
#define CAM_DATA_MASK				0x0FFF  // mask for camera DMA data

#define NUM_ROWS_IN_CAM				64		// number of rows in the AOA camera
#define NUM_ROWS_IN_CAM_48			48		// number of rows in the SMC camera with 48x48 configuration
#define NUM_CAM_ROWS_PER_SUBAP		6		// number of AOA camera rows per sub-aperture row
#define NUM_SUBAP_ROWS_PER_FRAME	8		// number of sub-aperture rows per frame

//#define FIRST_USED_CAM_ROW			0		// first used camera row			  // XXX UseAOA -- for AOA
//#define FIRST_UNUSED_CAM_ROW_TOP	0		// first unused camera row, top		  // XXX UseAOA -- for AOA
//#define NUM_UNUSED_CAM_ROWS_TOP		0		// number of unused camera rows, top  // XXX UseAOA -- for AOA
//#define FIRST_UNUSED_CAM_ROW_BOT	48		// first unused camera row, bottom    // XXX UseAOA -- for AOA
//#define NUM_UNUSED_CAM_ROWS_BOT		16		// number of unused camera rows       // XXX UseAOA -- for AOA
#define FIRST_USED_CAM_ROW			8		// first used camera row			  // XXX UseAOA -- for SMC
#define NUM_USED_CAM_ROWS			48		// number of used camera rows		  // XXX UseAOA -- for SMC
#define LAST_USED_CAM_ROW			(FIRST_USED_CAM_ROW + NUM_USED_CAM_ROWS)
#define FIRST_USED_CAM_COL			8		// first used camera column			  // XXX UseAOA -- for SMC
#define NUM_USED_CAM_COLS			48		// number of used camera columns	  // XXX UseAOA -- for SMC
#define LAST_USED_CAM_COL			(FIRST_USED_CAM_COL + NUM_USED_CAM_COLS)
#define FIRST_UNUSED_CAM_ROW_TOP	0		// first unused camera row, top		  // XXX UseAOA -- for SMC
#define NUM_UNUSED_CAM_ROWS_TOP		8		// number of unused camera rows, top  // XXX UseAOA -- for SMC
#define FIRST_UNUSED_CAM_ROW_BOT	56		// first unused camera row, bottom    // XXX UseAOA -- for SMC
#define NUM_UNUSED_CAM_ROWS_BOT		8		// number of unused camera rows       // XXX UseAOA -- for SMC

#define NUM_PIX_IN_ROW				64		// number of pixels in each row of the AOA camera
#define NUM_PIX_IN_ROW_48			48		// number of pixels in each row of the SMC camera with 48x48 configuration

#define NUM_CAM_BUFFS				200		// number of camera data buffers (there are lots of these 
											// > so the diagnostics data handler can get behind)


// Typedefs

// camera buffer type
typedef struct
{
    boolean inUseFlag;                       // indicates whether this buffer is in use for non-diag data
	int16	diagIndicator;					 // indicates eDiagIndNotInUse, eDiagIndInUse, or eDiagIndTelem

	int16	subapRowWriteNdx;				 // index of sub-ap row being written to
	int16	subapRowReadNdx;				 // index of sub-ap row being read from

	int16	unusedDataTop[NUM_UNUSED_CAM_ROWS_TOP]	 // the unused camera data, top  (first 8 camera rows for SMC)
					     [NUM_PIX_IN_ROW];
	int16	data[NUM_SUBAP_ROWS_PER_FRAME]   // the camera data with subapertures (first 48 camera rows)
				[NUM_CAM_ROWS_PER_SUBAP]
				[NUM_PIX_IN_ROW];
	int16	unusedDataBot[NUM_UNUSED_CAM_ROWS_BOT]	 // the unused camera data, bottom (last 8 camera rows)
					     [NUM_PIX_IN_ROW];
} CamBuffType;

// camera data type
typedef struct
{
	int16			cmdStart;		// index (into CmdTable[]) of command to be started or eCmdNone
	int16			cmdCrnt;		// index (into CmdTable[]) of command currently running or eCmdNone
	int16			cmdIntParam;	// integer parameter for command, if it has one
	float32			cmdFloatParam;  // float parameter for command, if it has one

	int16			diagActFlag;	// diagnostic action flag (set only on SRT side (except at initialization))
	int16			diagStatFlag;	// diagnostic status flag (set only on HRT side)
	int16			diagDiagRate;   // indicates rate at which diagnostic data should be sent to SRT (always 0 or 1)
	int16			diagDiagCntr;	// counter for diagnostic data being sent to SRT
	int16			diagTelemRate;  // rate at which telemetry data should be sent to SRT (same as diag data, but at different rate)
	boolean			diagPSFFlag;	// indicates that PSF estimation is on, so data should be sent

	boolean			intrIgnore;	// indicates SMC camera interrrupt should be ignored (because we're doing serial comms)  XXXINTIG
	boolean			initDone;	// indicates SMC WFS camera initialization is done

	int16			writeNdx;				     // index into camBuffNdxs[] for buffer being written to
	int16			readNdx;				     // index into camBuffNdxs[] for buffer being read from
	int16			camBuffNdxs[NUM_CAM_BUFFS];  // circular list of indexes into camBuffs[]

	CamBuffType		camBuffs[NUM_CAM_BUFFS];     // the camera frame buffers

	int16			crntRS170WriteBuff;  // the RS-170 buffer that CamLTT_LoadDataBuffer() is currently writing to //XXXRS170
	int16			crntRS170ReadBuff;   // the RS-170 buffer that dispRS170 is currently reading from //XXXRS170 
	int16			RS170crtGain;		 // 0 means auto-scale, 1 to 64 applies fixed gains (actually attenuations) //XXXRS170 
	int16			RS170Buffs[2][NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW];  // the RS-170 buffers //XXXRS170 
} CamDataType;


// Global Variables

extern int16 CamFrameRate;  // camera frame rate in frames per second
extern int16 CamGain;		// camera gain (1 or 2)
extern int16 CamCRTGain;	// camera CRT gain (0 to 4)

extern int16 CamFrameRateNdx;  // index for current camera rate into LegalFrameRates[]
extern const int16 LegalFrameRates[];

// Global Function Prototypes

void *Cam( void *arg );
void CamInit( void );
void CamRestart( void );
void *CamSerial( void *arg );

#endif
