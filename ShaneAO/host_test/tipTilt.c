/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		tipTilt

AUTHOR:	DWP			DATE: 6/4/03

COMPONENT DESCRIPTION:
This component contains functions to apply control laws for the tip/tilt mirror (on the optical 
bench) and laser fast-steering mirror (FSM) (at the top of the telescope) and to output the resulting 
values to the mirror actuator DACs.

The system can operate in 2 guidestar modes, natural guidestar (NGS) or laser guidestar (LGS).  When
in NGS mode, tip/tilt information is derived from the same wavefront sensor that the DM corrections are
derived from and the tip/tilt mirror actuators are manipulated appropriately (when in NGS, the APDs and
laser FSM are not used).  When in LGS mode, tip/tilt information is derived from quad APDs (aimed at a
faint 'anchor' star) and the tip/tilt mirror actuators are manipulated using that data.  The tip/tilt data 
derived from the wavefront sensor is used to steer the laser beam using the laser FSM.  In both modes,
the telescope offload (telco) signals are derived from the tip/tilt mirror settings.

IMPORTANT: Some of the functions in this module are invoked from recon and are, therefore, part of the
recon thread.  TipTiltAPD() and its corresponding functions make up a thread that executes periocdically, 
gathering APD data, and processing that data.

The APD pulses are counted using an IOtech DaqBoard 2001 with a custom pulse stretcher board (the old APDs
put out pulses about 10 nanosecs long).

COMPONENT UNITS:
1.	TipTiltInit()
2.	TipTiltProcCmd()
3.	TipTiltCloseLoop()
4.	TipTiltProcess()
5.	TipTiltOpenLoop()
6.	TipTiltAPD()
7.	TipTiltAPDInit()
8.	TipTiltAPDCloseLoop()
9.	TipTiltAPDProcess()
10.	TipTiltAPDOpenLoop()
11.	TipTiltAPDBackStart()
12.	TipTiltAPDBackProcess()
13.	TipTiltAPDBackAbort()
  
REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_math.h>
#include <rtl_time.h>

/* XXXUseAPD / for db2k start (db2k is the DaqBoard 2001 used for pulse counting for the APDs)
#include <asm/io.h>
#include <linux/delay.h>
#define DB2K_MAJOR   62  // 61
#define DB2K_ROOT    0
#define DB2K_NUMDEVS 4
#define DLINUX       1
#define ADC_HZ (HZ/10)
#include "/opt/db2kdriver/iottypes.h"
#include "/opt/db2kdriver/w32ioctl.h"
#ifndef DAQDD
#define DAQDD 1
#endif
#include "/opt/db2kdriver/daqx.h"
#include "/opt/db2kdriver/ddapi.h"
#define ADCFILE
#include "/opt/db2kdriver/device.h"
#include "/opt/db2kdriver/daqio.h"
// for db2k end */

#include "types.h"
#include "constant.h"
#include "camDrvr.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"


// Defines

// the following scale factor (TT_SCALE_FACTOR) was used in the original code (it looks like someone was trying to 
// > convert to 12-bit DAC counts, but goofed because the factor is applied to ttErrors in pixel units not in Volts); 
// > it's being used here, for now, so as not to have to rescale all of the other factors in the code
#define TT_SCALE_FACTOR   409.4   // = 2047 / 5

#define TT_TELCO_FACTOR   0.1	  // scale factor applied to telco output (so, apparently, it expects between -0.5 and 0.5 Volts)
#define TT_MAX_OLD_CODE   2047.0  // the maximum control value that the old code would have produced, correspnding to 5 Volts

#define TELCO_AVG_N 300  // N for Telco rotating average

// Global Variables
int16 GSMode;  // 0 sets to ngs mode, 1 sets to lgs mode

int16 TTLoopStat;   // indicates the status of the tip/tilt loop (open, opening, or closed)
int16 APDLoopStat;  // indicates the status of the APD tip/tilt loop (open, opening, or closed)

float32 TTGain;  // tip/tilt loop gain

float32 CrntTTVolts[eTTNumVolts];  // current tip/tilt Volts
float32 CrntTTMovs[eTTNumVolts];  // current tip/tilt voltage settings as per ttxmov and ttymov

float32 axisRotationNGS = 0.0;    // tip/tilt axis rotation for NGS mode (i.e., for the tip/tilt mirror) (in degrees)
float32 axisRotationLGS = 135.0;  // tip/tilt axis rotation for LGS mode (i.e., for laser steering) (in degrees)
// float32 axisRotationTT = -45;  // tip/tilt axis rotation for the TT mirror on the optical table (in degrees)  tt2inch
float32 axisRotationTT = 0;  // tip/tilt axis rotation for the TT mirror on the optical table (in degrees)  tt3inch

float32 r1x, r2x, r1y, r2y;  // tip/tilt rotation coefficients
float32 rtt2inch1x, rtt2inch2x, rtt2inch1y, rtt2inch2y;

int16   APDRate;     // rate at which APD tt loop is updated, per sec (between 50 and 500)
float32 APDGain;     // gain for the APD tt loop

int32   APDBack[4];  // APD background counts for APDs 0 through 3 respectively

int32   CrntAPDCounts[NUM_APDS];  // current APD counts (from the TipTiltAPD() thread)
float32 CrntAPDErrX;  // current X error from APD centroider, in APD units (analogous to pixel units)
float32 CrntAPDErrY;  // current Y error from APD centroider, in APD units (analogous to pixel units)

float32 CrntAPDOverLim;  // current APD over limit flag
float32 CrntAPDCountsPerSec;  // current total APD counts per second

// Local Variables

static boolean pocoOffloadOnFlag = false;  // indicates whether POCO offloading data should be sent or not //XXXpoco3

static boolean goingToLGS = false;

static float32 ttSignX, ttSignY, axisRotation;  // tip/tilt signs and axis rotation

static int16 lttCamCloseSettleCntr;  // counts number of SciMeasure LTT camera frames to wait after it has entered 
									 // > closed loop mode

static const float32 ttSignXNGS = -1.0;  // sign of the X axis for NGS mode (1.0 or -1.0)
static const float32 ttSignYNGS = -1.0;  // sign of the Y axis for NGS mode (1.0 or -1.0)

static const float32 ttSignXLGS = -1.0;  // sign of the X axis for LGS mode (1.0 or -1.0)
static const float32 ttSignYLGS = -1.0;  // sign of the Y axis for LGS mode (1.0 or -1.0)

static const float32 ttSignXAPD = -1.0;  // sign of the X axis for the APDs (1.0 or -1.0)  //XXXdwp120112
static const float32 ttSignYAPD = -1.0;  // sign of the Y axis for the APDs (1.0 or -1.0)  //XXXdwp120112

//XXXUseAPD static PDEVICE_EXTENSION pde;  // PDE (pointer to device-extension) for db2k board

/*
The qs in the loopQs[] table below have been computed, offline, using the following equations:

// this structure contains the following data for frame rate:
//	  float32 framePeriod   frame period in seconds (approximate)
//	  float32 ttGainAdjust  frame rate dependent adjustment of tip/tilt gain
//	  float32 z				always 0.9, at present
//	  float32 wn			frame rate dependent multiples of pi
//	  float32 a				always 90 * TWO_PI, at present
//	  float32 b				always 150 * TWO_PI, at present
//	  float32 alpha			frame rate dependent multiples of pi
static const TTGainParmsType ttGainParms[] = 
{
	{ 0.02,    0.5, 0.9,  5.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 0.5 * TWO_PI },  //   55 frames / sec
	{ 0.01,    0.5, 0.9, 10.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 0.5 * TWO_PI },  //  100 frames / sec
	{ 0.004,   0.2, 0.9, 35.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 2.0 * TWO_PI },  //  250 frames / sec
	{ 0.002,   0.2, 0.9, 50.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 3.0 * TWO_PI },  //  500 frames / sec
	{ 0.00133, 0.3, 0.9, 60.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 4.0 * TWO_PI },  //  750 frames / sec (apparently not tested)
	{ 0.001,   0.3, 0.9, 60.0 * TWO_PI, 90.0 * TWO_PI, 150.0 * TWO_PI, 4.0 * TWO_PI },  // 1000 frames / sec (apparently not tested)
};
//	set up sign and rotation values, to be used below
	if (GSMode == eGSModeNGS)
	{
		ttSignX = ttSignXNGS;
		ttSignY = ttSignYNGS;
		axisRotation = axisRotationNGS;
	}
	else  // (GSMode == eGSModeLGS)
	{
		ttSignX = ttSignXLGS;
		ttSignY = ttSignYLGS;
		axisRotation = axisRotationLGS;
	}

//	put parameters into local variables to make the equations that use them easier to read
	Ts    = ttGainParms[CamFrameRateNdx].framePeriod;
	k     = TTGain * ttGainParms[CamFrameRateNdx].ttGainAdjust;
	z     = ttGainParms[CamFrameRateNdx].z;
	wn    = ttGainParms[CamFrameRateNdx].wn;
	a     = ttGainParms[CamFrameRateNdx].a;
	b     = ttGainParms[CamFrameRateNdx].b;
	alpha = ttGainParms[CamFrameRateNdx].alpha;

	wd	= wn * sqrt(1.0 - z * z);

//	compute gain coefficients and apply ttSignX and ttSignY
	q0 = -(2.0 * exp(-alpha * Ts) + exp(-a * Ts));
	q1 =  exp(-2.0 * alpha * Ts) + 2.0 * exp(-((a + alpha) * Ts));
	q2 = -exp(-((a + 2.0 * alpha) * Ts));

	q3 =  k;
	q4 = -k * (2.0 * exp(-z * wn * Ts) * cos(wd * Ts) + exp(-b * Ts));
	q5 =  k * (exp(-2.0 * z * wn * Ts) + 2.0 * exp(-b * Ts) * exp(-z * wn * Ts) * cos(wd * Ts));
	q6 = -k *  exp(-2.0 * z * wn * Ts) * exp(-b * Ts);

	q3x = ttSignX * q3;  q4x = ttSignX * q4;  q5x = ttSignX * q5;  q6x = ttSignX * q6;
	q3y = ttSignY * q3;  q4y = ttSignY * q4;  q5y = ttSignY * q5;  q6y = ttSignY * q6;
*/
static const LoopQsType loopQs[] =
{
//		q0				q1				q2			q3			q4				q5				q6
	{-1.878214988,	0.881934392,	-1.08061E-05,	0.5,	-0.546910763,	0.161359495,	-1.05084E-09},  // 55 frames per second
	{-1.941645292,	0.945885726,	-0.003287267,	0.5,	-0.54695111,	0.161403627,	-1.30216E-05},  // 100
	{-1.967102918,	1.022939484,	-0.053843092,	0.2,	-0.187625053,	0.050316863,	-0.000437015},  // 200
	{-2.006101389,	1.102440927,	-0.09418657,	0.2,	-0.172683968,	0.044931503,	-0.000946526},  // 250
	{-2.09126977,	1.259842191,	-0.166842035,	0.2,	-0.203905396,	0.062841608,	-0.003045655},  // 350
	{-2.24872429,	1.548932585,	-0.29928123,	0.2,	-0.249131464,	0.09776005,		-0.009800059},  // 500
	{-2.405628801,	1.847094272,	-0.440894186,	0.3,	-0.458657962,	0.228159547,	-0.03473578},   // 750
	{-2.518444519,	2.058944984,	-0.5402344,		0.3,	-0.538505857,	0.316484523,	-0.059306604}   // 1000
};

static float32 q0,  q1,  q2,		// tip/tilt gain coefficients
			   q3x, q4x, q5x, q6x, 
			   q3y, q4y, q5y, q6y;

static float32 xControl, xCntrl1, xCntrl2, xCntrl3;  // xControl from this time, last time, 2 times ago, and 3 times ago
static float32 xErr0,    xErr1,   xErr2,   xErr3;	 // xErr from this time, last time, 2 times ago, and 3 times ago

static float32 yControl, yCntrl1, yCntrl2, yCntrl3;  // yControl from this time, last time, 2 times ago, and 3 times ago
static float32 yErr0,    yErr1,   yErr2,   yErr3;	 // yErr from this time, last time, 2 times ago, and 3 times ago

static boolean ttFirstTime;  // flag indicating first time through the TT loop (since closing the loop)

static int16 telcoVoltsNdx;  // index into telcoVoltsX_I32[] and telcoVoltsY_I32[]
static int32 crntTelcoSumX_I32, crntTelcoSumY_I32;  // current sums for Telco rotating average
static int32 telcoVoltsX_I32[TELCO_AVG_N], telcoVoltsY_I32[TELCO_AVG_N];  // history of voltages for Telco rotating average

static float32 xAvg = 0.0, yAvg = 0.0;  // rolling averages for laser offload values

//	APD loop gains
static float32 apdQ0x, apdQ0y;     // loop gains - use to scale system
static const float32 apdQ1 = 1.0;  // adaptive gain - nominally 1
static const float32 apdQ2 = 0.8;  // 'fixed' gain - 0.8 typ for best convergence in adapt mode
static const float32 apdQ3 = 1.0;  // controls adapt rate -- nominally 1
static const float32 adapt = 30.0;

static const float32 apdMinIntensityThresh = 50.0;  // minimum threshold for APD total intensity
static boolean lowIntensMsgPosted = false;  // indicates whether intensity-too-low error message has been posted

static float32 axisRotationAPD = 0.0;			// axis rotation for APDs (in degrees)
static float32 apdR1x, apdR2x, apdR1y, apdR2y;  // APD rotation coefficients

static float32 apdX_yk1, apdY_yk1, apdX_fk1, apdY_fk1;
static float32 apdXControl1, apdYControl1;

static int16 perSecCntr = 0;  // counts APD loops per second for computing totalAPDCountsPerSec
static int32 totalAPDCountsPerSec = 0;  // total APD counts per second
static float32 totalAPDCountsPerSecKeep = 0.0;
static float32 apdOverLimitFlag;  // APD over limit flag (overLimitFlag is a float so it can be part of ttData[])

static boolean apdFirstTime;  // flag indicating first time through the apd loop (since closing the loop)

static int16 apdSumCntr;  		 // counts values in APD background sums
static int32 apdSums[NUM_APDS];  // sums for determining APD background


// Local Function Prototypes

static void TipTiltCloseLoop( void );
static void TipTiltAPDBackStart( void );
static void TipTiltAPDBackProcess( int32 apdCounts[NUM_APDS] );

/**********************************************************************************************

UNIT NAME:  TipTiltInit()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by tip/tilt.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltInit( void )
{
	int16 i;

//	initialize variables
	goingToLGS = false;

	TTLoopStat = eTTLoopOpen;
	FileDataShmPtr->ttLoopStat = eTTLoopOpen;

	APDLoopStat = eAPDLoopOpen;
	FileDataShmPtr->apdLoopStat = eAPDLoopOpen;

	perSecCntr = 0;
	totalAPDCountsPerSec = 0;
	totalAPDCountsPerSecKeep = 0.0;

	for (i = 0; i < eTTNumVolts; i++)
	{
		CrntTTVolts[i] = 0.0;
		CrntTTMovs[i] = 0.0;
	}
	CrntTTVolts[eTTTelcoEnable] = TELCO_DISABLE;

	pocoOffloadOnFlag = false;  //XXXpoco3

//	set up sign and rotation values (in case reg 82, 83, 84, or 85 is executed)
	if (GSMode == eGSModeNGS)
		axisRotation = axisRotationNGS;
	else  // (GSMode == eGSModeLGS)
		axisRotation = axisRotationLGS;

	r1x = cos(axisRotation * RAD);
	r2x = cos((axisRotation + 90) * RAD);
	r1y = sin(axisRotation * RAD);
	r2y = sin((axisRotation + 90) * RAD);

	rtt2inch1x = cos(axisRotationTT * RAD);
	rtt2inch2x = cos((axisRotationTT + 90) * RAD);
	rtt2inch1y = sin(axisRotationTT * RAD);
	rtt2inch2y = sin((axisRotationTT + 90) * RAD);
}


/**********************************************************************************************

UNIT NAME:  TipTiltProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process tip/tilt commands.

UNIT INPUTS:
	cmdNdx - index into CmdTable[] for the command to be processed

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltProcCmd( int16 cmdNdx )
{
	int16 i;

//	switch on command ID corresponding to command index
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		  // abort long commands
			SysConSendStrngNoParm("Error: unexpected command abort received by tipTilt");
			break;

		case eCmdTTGSMode:  // 0 sets to ngs mode, 1 sets to lgs mode
			GSMode = ReconDataPtr->cmdIntParam;
			FileDataPut(eFileTypeParms);
			if (GSMode == 0)
				SysConSendStrngNoParm("Notification: switched to ngs mode");
			else
			{
				goingToLGS = true;
				SysConSendStrngNoParm("Notification: switched to lgs mode");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTClose:  // close tip/tilt loop
			TipTiltCloseLoop();
			SysConSendStrngNoParm("Notification: ttclose complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTOpen:  // open tip/tilt loop
			TipTiltOpenLoop();
			SysConSendStrngNoParm("Notification: ttopen complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTGain:  // set tip/tilt loop gain
			TTGain = ReconDataPtr->cmdFloatParam;
			FileDataPut(eFileTypeParms);
			SysConSendStrngNoParm("Notification: ttgain complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTXMov:	  // set tip/tilt mirror X actuator (in Volts) (** this is a temporary setting **)
			CrntTTVolts[eTTMirrorX] = ReconDataPtr->cmdFloatParam;
			CrntTTMovs[eTTMirrorX] = ReconDataPtr->cmdFloatParam;
	        ReconUpdateDACs(dacTTMap, CrntTTVolts, eTTNumVolts);
			SysConSendStrngNoParm("Notification: ttxmov complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTYMov:	  // set tip/tilt mirror Y actuator (in Volts) (** this is a temporary setting **)
			CrntTTVolts[eTTMirrorY] = ReconDataPtr->cmdFloatParam;
			CrntTTMovs[eTTMirrorY] = ReconDataPtr->cmdFloatParam;
	        ReconUpdateDACs(dacTTMap, CrntTTVolts, eTTNumVolts);
			SysConSendStrngNoParm("Notification: ttymov complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTLXMov:	  // set laser mirror X actuator (in Volts) (** this is a temporary setting **)
			CrntTTVolts[eTTLaserX] = ReconDataPtr->cmdFloatParam;
	        ReconUpdateDACs(dacTTMap, CrntTTVolts, eTTNumVolts);
			SysConSendStrngNoParm("Notification: lxmov complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTLYMov:	  // set laser mirror Y actuator (in Volts) (** this is a temporary setting **)
			CrntTTVolts[eTTLaserY] = ReconDataPtr->cmdFloatParam;
	        ReconUpdateDACs(dacTTMap, CrntTTVolts, eTTNumVolts);
			SysConSendStrngNoParm("Notification: lymov complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTAPDClose:  // close APD tip/tilt loop
			if (UseAPD != 1)
				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
			else
			{
				TipTiltAPDCloseLoop();
				SysConSendStrngNoParm("Notification: apdclose complete");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTAPDOpen:	  // open APD tip/tilt loop
			if (UseAPD != 1)
				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
			else
			{
				TipTiltAPDOpenLoop();
				SysConSendStrngNoParm("Notification: apdopen complete");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTAPDRate:	  // set APD tip/tilt loop rate
			if (UseAPD != 1)
				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
			else
			{
				APDRate = ReconDataPtr->cmdIntParam;
				FileDataPut(eFileTypeParms);
				SysConSendStrngNoParm("Notification: apdrate complete");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTAPDGain:	  // set APD tip/tilt loop gain
//XXX			if (UseAPD != 1)
//XXX				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
//XXX			else
			{
				APDGain = ReconDataPtr->cmdFloatParam;
				FileDataPut(eFileTypeParms);
				SysConSendStrngNoParm("Notification: apdgain complete");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTAPDBack:	  // determine APD background
			if (UseAPD != 1)
			{
				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
				ReconDataPtr->cmdCrnt = eCmdNone;
			}
			else
				TipTiltAPDBackStart();
			break;

		case eCmdTTAPDZBack:  // zero APD background
			if (UseAPD != 1)
				SysConSendStrngNoParm("Error: currently using SciMeasure LTT");
			else
			{
				for (i = 0; i < NUM_APDS; i++)
					APDBack[i] = 0;
				SysConSendStrngNoParm("Notification: apdzback complete");
			}
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTOlOn:  // turn POCO offloading on  //XXXpoco3
			pocoOffloadOnFlag = true;
			SysConSendStrngNoParm("Notification: olon complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdTTOlOff:  // turn POCO offloading off  //XXXpoco3
			pocoOffloadOnFlag = false;
			ReconDataPtr->xPocoOffload = 0.0;  // do this here in case the  XXXpoco3
			ReconDataPtr->yPocoOffload = 0.0;  // > camera has gone off     XXXpoco3
			SysConSendStrngNoParm("Notification: oloff complete");
			ReconDataPtr->cmdCrnt = eCmdNone;
			break;

		default:
			SysConSendStrngIntParm("Error: unexpected command received by tipTilt, cmdNdx = ", cmdNdx);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  TipTiltCloseLoop()

UNIT DESCRIPTION:
The purpose of this unit is to close the tip/tilt loop.  This is done by initializing variables 
and setting the TTLoopStat flag to indicate that the loop is closed.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void TipTiltCloseLoop( void )
{
//	make sure we're not in centtest mode and report an error and return if we are
	if (CrntCentroider == eCentTest)
	{
		SysConSendStrngNoParm("Error: can't close tip/tilt loop in centtest mode");
		return;
	}

//	initialize and set up variables as follows:
//	set flag to indicate first time through the TT loop
	ttFirstTime = true;

//	initialize control and error values
	xCntrl1 = 0.0;  // xControl from last time
	xCntrl2 = 0.0;  // xControl from 2 times ago
	xCntrl3 = 0.0;  // xControl from 3 times ago
	xErr1 = 0.0;    // xErr from last time
	xErr2 = 0.0;    // xErr from 2 times ago
	xErr3 = 0.0;    // xErr from 3 times ago

	yCntrl1 = 0.0;  // yControl from last time
	yCntrl2 = 0.0;  // yControl from 2 times ago
	yCntrl3 = 0.0;  // yControl from 3 times ago
	yErr1 = 0.0;    // yErr from last time
	yErr2 = 0.0;    // yErr from 2 times ago
	yErr3 = 0.0;    // yErr from 3 times ago

//	set up sign and rotation values, to be used below
	if (GSMode == eGSModeNGS)
	{
		axisRotation = axisRotationNGS;

		ttSignX = ttSignXNGS;
		ttSignY = ttSignYNGS;
	}
	else  // (GSMode == eGSModeLGS)
	{
		axisRotation = axisRotationLGS;

		ttSignX = ttSignXLGS;
		ttSignY = ttSignYLGS;
	}

//	compute rotation coefficients
	r1x = cos(axisRotation * RAD);
	r2x = cos((axisRotation + 90) * RAD);

	r1y = sin(axisRotation * RAD);
	r2y = sin((axisRotation + 90) * RAD);

//	set up gain coefficients
	q0 = loopQs[CamFrameRateNdx].q0;
	q1 = loopQs[CamFrameRateNdx].q1;
	q2 = loopQs[CamFrameRateNdx].q2;

//	if we're in NGS mode
//		set CrntTTVolts[eTTTelcoEnable] to indicate that telco is enabled
	if (GSMode == eGSModeNGS)
		CrntTTVolts[eTTTelcoEnable] = TELCO_ENABLE;

	xAvg = 0.0;
	yAvg = 0.0;

//	set the TTLoopStat flag to indicate that the loop is closed
	TTLoopStat = eTTLoopClosed;
	FileDataShmPtr->ttLoopStat = eTTLoopClosed;
}


/**********************************************************************************************

UNIT NAME:  TipTiltProcess()

UNIT DESCRIPTION:
The purpose of this unit is to apply the control law for the tip/tilt mirror actuators 
and output the resulting values to the actuator DACs.

Note that the logic for the closed loop portion of this code comes from the original code 
(re-written for readability and with at least one bug removed).  Although the intent of the 
original code appears to be to apply a two-pole low-pass filter, the specific characteristics
of that filter are not obvious.  Although this approach is considerably more complicated than
is necessary for this application, it has the positive attribute of appearing to work (on the
old system).  Therefore, it is being used here, at least for now.

UNIT INPUTS:
1.	xTTError	- current X tip/tilt error (in pixel units)
2.	yTTError	- current Y tip/tilt error (in pixel units)
3.  

UNIT OUTPUTS:
1.	

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltProcess( float32 xTTError, float32 yTTError, float32 ttData[], 
					 float32 *xLaserOffload, float32 *yLaserOffload)
{
	boolean updateDACsFlag;
	int16 i;
	int32 newX_I32, newY_I32;  // newX and newY in int32, truncated to tenths of millivolts
	float32 newX, newY;
	float32 limit, newErrorX, newErrorY;
	float32 newTelcoX, newTelcoY;
	float32 tmpTTVoltsX, tmpTTVoltsY;  //tt2inch

//	set up gain coefficients and apply ttSignX and ttSignY
	q3x = ttSignX * TTGain * loopQs[CamFrameRateNdx].q3;
	q4x = ttSignX * TTGain * loopQs[CamFrameRateNdx].q4;
	q5x = ttSignX * TTGain * loopQs[CamFrameRateNdx].q5;
	q6x = ttSignX * TTGain * loopQs[CamFrameRateNdx].q6;

	q3y = ttSignY * TTGain * loopQs[CamFrameRateNdx].q3;
	q4y = ttSignY * TTGain * loopQs[CamFrameRateNdx].q4;
	q5y = ttSignY * TTGain * loopQs[CamFrameRateNdx].q5;
	q6y = ttSignY * TTGain * loopQs[CamFrameRateNdx].q6;

//	apply rotation
	newErrorX = r1x * xTTError + r1y * yTTError;
	newErrorY = r2x * xTTError + r2y * yTTError;

//	compute X and Y errors, applying scale factor to make data consistent with the old code
	xErr0 = newErrorX * TT_SCALE_FACTOR;
	yErr0 = newErrorY * TT_SCALE_FACTOR;
	
//	compute new control values
	xControl = - q0  * xCntrl1  // xControl from last time
			   - q1  * xCntrl2  // xControl from 2 times ago 
			   - q2  * xCntrl3  // xControl from 3 times ago 
			   + q3x * xErr0 	// xErr from this time
			   + q4x * xErr1 	// xErr from last time
			   + q5x * xErr2 	// xErr from 2 times ago
			   + q6x * xErr3;	// xErr from 3 times ago

	yControl = - q0  * yCntrl1  // yControl from last time
			   - q1  * yCntrl2  // yControl from 2 times ago
			   - q2  * yCntrl3  // yControl from 3 times ago
			   + q3y * yErr0    // yErr from this time
			   + q4y * yErr1    // yErr from last time
			   + q5y * yErr2    // yErr from 2 times ago
			   + q6y * yErr3;   // yErr from 3 times ago

//	clip the control values to the max that the old code should have produced (should correspond to -5 to +5 Volts)
//	> scaled down to DAC_VOLT_LIMIT
	limit = TT_MAX_OLD_CODE * DAC_VOLT_LIMIT / DAC_VOLT_MAX;

	if (xControl < -limit)
		xControl = -limit;
	else if (xControl > limit)
		xControl = limit;

	if (yControl < -limit)
		yControl = -limit;
	else if (yControl > limit)
		yControl = limit;

//	save control and error values for next time
	xCntrl3 = xCntrl2;
	xCntrl2 = xCntrl1;
	xCntrl1 = xControl;
	xErr3 = xErr2;
	xErr2 = xErr1;
	xErr1 = xErr0;

	yCntrl3 = yCntrl2;
	yCntrl2 = yCntrl1;
	yCntrl1 = yControl;
	yErr3 = yErr2;
	yErr2 = yErr1;
	yErr1 = yErr0;

//	convert control values to volts
	newX = xControl / TT_SCALE_FACTOR;
	newY = yControl / TT_SCALE_FACTOR;

//	if we're in NGS mode
//		if this is the first time through the loop (since closing the loop)
//			set up values for Telco boxcar average
	if (GSMode == eGSModeNGS)
	{
		if (ttFirstTime)
		{
			ttFirstTime = false;

			telcoVoltsNdx = 0;

			newX_I32 = (int32)(newX * 10000.0);  // truncate newX and newY to tenths of millivolts to avoid the 
			newY_I32 = (int32)(newY * 10000.0);	 // > potential cumulative error of a boxcar averager

			crntTelcoSumX_I32 = newX_I32 * TELCO_AVG_N;
			crntTelcoSumY_I32 = newY_I32 * TELCO_AVG_N;
			for(i = 0; i < TELCO_AVG_N; i++)
			{
				telcoVoltsX_I32[i] = newX_I32;
				telcoVoltsY_I32[i] = newY_I32;
			}

			newTelcoX = newX;
			newTelcoY = newY;
		}
//		else
//			update values for Telco boxcar averages and compute the averages
		else
		{
			newX_I32 = (int32)(newX * 10000.0);  // truncate newX and newY to tenths of millivolts to avoid the 
			newY_I32 = (int32)(newY * 10000.0);	 // > potential cumulative error of a boxcar averager

			crntTelcoSumX_I32 -= telcoVoltsX_I32[telcoVoltsNdx];
			crntTelcoSumY_I32 -= telcoVoltsY_I32[telcoVoltsNdx];
			crntTelcoSumX_I32 += newX_I32;
			crntTelcoSumY_I32 += newY_I32;
			telcoVoltsX_I32[telcoVoltsNdx] = newX_I32;
			telcoVoltsY_I32[telcoVoltsNdx] = newY_I32;

			if (++telcoVoltsNdx >= TELCO_AVG_N)
				telcoVoltsNdx = 0;

			newTelcoX = (float32)(crntTelcoSumX_I32 / TELCO_AVG_N) / 10000.0;
			newTelcoY = (float32)(crntTelcoSumY_I32 / TELCO_AVG_N) / 10000.0;
		}

//		scale the new telco data and put it into CrntTTVolts[]
		CrntTTVolts[eTTTelcoX] = newTelcoX * TT_TELCO_FACTOR;
		CrntTTVolts[eTTTelcoY] = newTelcoY * TT_TELCO_FACTOR;
	}

//	update the tip/tilt data running averages used for laser offloading (actually maintains averages whether in lgs or ngs)
	xAvg = (xAvg * (CamFrameRate - 1) + newX) / CamFrameRate;
	yAvg = (yAvg * (CamFrameRate - 1) + newY) / CamFrameRate;
	*xLaserOffload = xAvg;
	*yLaserOffload = yAvg;

//	if the tip/tilt loop is closed
//		if we're in NGS mode, update the current TT mirror settings
//		else, update the current laser steering settings
//	else if the loop is opening
//		set the X and Y TT values to the tip/tilt mover values and range check
//		set telco offloading to disable
//		update the DACs with the new voltages
	updateDACsFlag = false;
	if (TTLoopStat == eTTLoopClosed)
	{
		if (GSMode == eGSModeNGS)
		{
			CrntTTVolts[eTTMirrorX] = newX;
			CrntTTVolts[eTTMirrorY] = newY;

			if (!pocoOffloadOnFlag)  //XXXpoco3
			{
				ReconDataPtr->xPocoOffload = 0.0;  //XXXpoco3
				ReconDataPtr->yPocoOffload = 0.0;  //XXXpoco3
			}
			else
			{
				ReconDataPtr->xPocoOffload = CrntTTVolts[eTTTelcoX];  //XXXpoco
				ReconDataPtr->yPocoOffload = CrntTTVolts[eTTTelcoY];  //XXXpoco
			}
		}
		else  // (GSMode == eGSModeLGS)
		{
			CrntTTVolts[eTTLaserX] = newX;
			CrntTTVolts[eTTLaserY] = newY;
		}
		updateDACsFlag = true;
	}
	else if (TTLoopStat == eTTLoopOpening)
	{
		if (GSMode == eGSModeNGS)
		{
			CrntTTVolts[eTTMirrorX] = 0.0;
			CrntTTVolts[eTTMirrorY] = 0.0;
			CrntTTMovs[eTTMirrorX] = 0.0;
			CrntTTMovs[eTTMirrorY] = 0.0;
			CrntTTVolts[eTTTelcoEnable] = TELCO_DISABLE;

			ReconDataPtr->xPocoOffload = 0.0;  //XXXpoco
			ReconDataPtr->yPocoOffload = 0.0;  //XXXpoco
		}
		else  // (GSMode == eGSModeLGS)
		{
			CrntTTVolts[eTTLaserX] = 0.0;
			CrntTTVolts[eTTLaserY] = 0.0;
		}
		updateDACsFlag = true;

		TTLoopStat = eTTLoopOpen;
		FileDataShmPtr->ttLoopStat = eTTLoopOpen;
	}
	else  // (TTLoopStat == eTTLoopOpen)
	{
		if ((GSMode == eGSModeNGS) && !RegActive)  // dithering only works in NGS mode
		{
			CrntTTVolts[eTTMirrorX] = CrntTTMovs[eTTMirrorX];
			CrntTTVolts[eTTMirrorY] = CrntTTMovs[eTTMirrorY];
			updateDACsFlag = true;
		}
	}

	if (goingToLGS)  // this is in case we're going into LGS mode with a dither going
	{
		goingToLGS = false;
		if (!RegActive)
		{
			CrntTTVolts[eTTMirrorX] = CrntTTMovs[eTTMirrorX];
			CrntTTVolts[eTTMirrorY] = CrntTTMovs[eTTMirrorY];
			updateDACsFlag = true;
		}
	}

	if (updateDACsFlag)
	{
		if (TTDitherOn && (GSMode == eGSModeNGS))
		{
//			tmpTTVoltsX = -(CrntTTVolts[eTTMirrorX] + TTDitherCosCurves[0][TTDitherCosNdx]);  // tt2inch
			tmpTTVoltsX = CrntTTVolts[eTTMirrorX] + TTDitherCosCurves[0][TTDitherCosNdx];  // tt3inch
			tmpTTVoltsY = CrntTTVolts[eTTMirrorY] + TTDitherCosCurves[0][TTDitherCosNdx];  // tt2inch

			CrntTTVolts[eTTMirrorX] = rtt2inch1x * tmpTTVoltsX + rtt2inch1y * tmpTTVoltsY;  // tt2inch
			CrntTTVolts[eTTMirrorY] = rtt2inch2x * tmpTTVoltsX + rtt2inch2y * tmpTTVoltsY;  // tt2inch

			if (CrntTTVolts[eTTMirrorX] < -TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorX] = -TT_VOLT_LIMIT;  // tt2inch
			else if (CrntTTVolts[eTTMirrorX] > TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorX] = TT_VOLT_LIMIT;  // tt2inch

			if (CrntTTVolts[eTTMirrorY] < -TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorY] = -TT_VOLT_LIMIT;  // tt2inch
			else if (CrntTTVolts[eTTMirrorY] > TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorY] = TT_VOLT_LIMIT;  // tt2inch

			if (++TTDitherCosNdx >= DITHER_COS_LNGTH)
				TTDitherCosNdx = 0;
		}
		else if (GSMode == eGSModeNGS)
		{
//			tmpTTVoltsX = -CrntTTVolts[eTTMirrorX];  // tt2inch
			tmpTTVoltsX = CrntTTVolts[eTTMirrorX];  // tt3inch
			tmpTTVoltsY = CrntTTVolts[eTTMirrorY];  // tt2inch

			CrntTTVolts[eTTMirrorX] = rtt2inch1x * tmpTTVoltsX + rtt2inch1y * tmpTTVoltsY;  // tt2inch
			CrntTTVolts[eTTMirrorY] = rtt2inch2x * tmpTTVoltsX + rtt2inch2y * tmpTTVoltsY;  // tt2inch

			if (CrntTTVolts[eTTMirrorX] < -TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorX] = -TT_VOLT_LIMIT;  // tt2inch
			else if (CrntTTVolts[eTTMirrorX] > TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorX] = TT_VOLT_LIMIT;  // tt2inch

			if (CrntTTVolts[eTTMirrorY] < -TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorY] = -TT_VOLT_LIMIT;  // tt2inch
			else if (CrntTTVolts[eTTMirrorY] > TT_VOLT_LIMIT)  // tt2inch
				CrntTTVolts[eTTMirrorY] = TT_VOLT_LIMIT;  // tt2inch
		}
		else if (GSMode == eGSModeLGS)
		{
			if (CrntTTVolts[eTTLaserX] < -TTLASER_VOLT_LIMIT)
				CrntTTVolts[eTTLaserX] = -TTLASER_VOLT_LIMIT;
			else if (CrntTTVolts[eTTLaserX] > TTLASER_VOLT_LIMIT)
				CrntTTVolts[eTTLaserX] = TTLASER_VOLT_LIMIT;

			if (CrntTTVolts[eTTLaserY] < -TTLASER_VOLT_LIMIT)
				CrntTTVolts[eTTLaserY] = -TTLASER_VOLT_LIMIT;
			else if (CrntTTVolts[eTTLaserY] > TTLASER_VOLT_LIMIT)
				CrntTTVolts[eTTLaserY] = TTLASER_VOLT_LIMIT;
		}

        ReconUpdateDACs(dacTTMap, CrntTTVolts, eTTNumVolts);
	}

//	move tip/tilt data to ttData[] (primarily for diagnostics)
	ttData[eTTDiagErrorX] = xTTError;
	ttData[eTTDiagErrorY] = yTTError;
	for (i = 0; i < eTTNumVolts; i++)
		ttData[eTTDiagMirrorX + i] = CrntTTVolts[i];
	for (i = 0; i < NUM_APDS; i++)
		ttData[eTTDiagAPDA + i] = (float32)CrntAPDCounts[i];
	ttData[eTTDiagAPDErrX] = CrntAPDErrX;
	ttData[eTTDiagAPDErrY] = CrntAPDErrY;
	ttData[eTTDiagLVDTX] = 0.0;
	ttData[eTTDiagLVDTY] = 0.0;
	ttData[eTTDiagAPDOverLim] = CrntAPDOverLim;
	ttData[eTTDiagAPDCountsPerSec] = CrntAPDCountsPerSec;
}


/**********************************************************************************************

UNIT NAME:  TipTiltOpenLoop()

UNIT DESCRIPTION:
The purpose of this unit is to open the tip/tilt loop.  This is done by simply setting the
TTLoopStat flag to indicate that the loop is opening (things will be set as necessry when 
TipTiltProcess() runs with the next frame).

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltOpenLoop( void )
{
//	set flag indicating that the TT loop is opening
	if (TTLoopStat == eTTLoopClosed)
	{
		TTLoopStat = eTTLoopOpening;
		FileDataShmPtr->ttLoopStat = eTTLoopOpening;
	}
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPD()

UNIT DESCRIPTION:
This unit is a thread that collects APD data, computes tip and tilt errors, applies a control
law, and outputs voltages to the tip/tilt mirror actuators and telco offloading.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *TipTiltAPD( void *arg )
{
//XXXUseAPD	int16 i;
	int retVal;
//XXXUseAPD	int32 apdCounts[NUM_APDS];
//XXXUseAPD	CTR_READ_T ctrReadStruct;
	struct rtl_timespec next;

//	perform initialization for the APD tip/tilt process
	TipTiltAPDInit();

//	enter infinite loop
    while (1)
    {
/*XXXUseAPD start
//		read the 4 APD counters
		for (i = 0; i < NUM_APDS; i++)
		{
 			ctrReadStruct.errorCode      = DerrNoError;
			ctrReadStruct.ctrDevType     = CtrLocal9513;
			ctrReadStruct.ctrPortOffset2 = i;
			ctrReadStruct.ctrValue2		 = -1;

			ctrRead(pde, (daqIOTP)&ctrReadStruct);

			apdCounts[i] = ctrReadStruct.ctrValue2;

 			if ((ctrReadStruct.errorCode != DerrNoError) || (apdCounts[i] < 0))
			{
				SysConSendStrngIntParm("Error reading APD counter: ", i);
				apdCounts[i] = 0;
			}
		}

//		call TipTiltAPDProcess() to process the data
		TipTiltAPDProcess( apdCounts );

//		if we're determining the APD background
//			call TipTiltAPDBackProcess() to process the data
		if (ReconRunCmd == eCmdTTAPDBack)
			TipTiltAPDBackProcess( apdCounts );
XXXUseAPD end*/

//		wait for the next APD cycle
		next.tv_sec = 0;
		next.tv_nsec = 1000000000L / APDRate;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			rtl_printf("Error from nanosleep(), retVal = %d\n", retVal);
	} 
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDInit()

UNIT DESCRIPTION:
The purpose of this unit is to perform initialization for the APD tip/tilt thread.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltAPDInit( void )
{
	int16 i;
	int retVal;
	struct rtl_timespec next;

//	initialize the DaqBoard 2001 (db2k) board used for counting pulses and configure its 4 counters
//XXXUseAPD	pde = dboard2000_getPDE(0);  // get the board's PDE (pointer to device-extension, in db2k terminology)
//XXXUseAPD	initializeCtrs(pde);  // this should already be done by initializeAdc(); but, we're doing it again anyway
//XXXUseAPD	for (i = 0; i < NUM_APDS; i++)
//XXXUseAPD		configureCtr(pde, i, CounterClear);

//	initialize variables
	apdOverLimitFlag = 0.0;  // false

	for (i = 0; i < NUM_APDS; i++)
		CrntAPDCounts[i] = 0;
	CrntAPDErrX = 0.0;
	CrntAPDErrY = 0.0;
	CrntAPDOverLim = 0.0;
	CrntAPDCountsPerSec = 0;

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT and
//	> on ReconInitDone to let recon finish initing (so it'll be ready to receive APD data)
	while (!OkayToFinishInit || !ReconInitDone)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in TipTiltAPDInit() 2, retVal = ", retVal);
    }

    TiptTiltAPDInitDone = true;
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDCloseLoop()

UNIT DESCRIPTION:
The purpose of this unit is to close the APD tip/tilt loop.  This is done by initializing variables 
and setting the APDLoopStat flag to indicate that the loop is closed.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltAPDCloseLoop( void )
{
//	make sure we're in lgs mode and report an error if we're not
	if (GSMode != eGSModeLGS)
		SysConSendStrngNoParm("Error: system must be in lgs mode");

//	otherwise
	else
	{
//		initialize and set up variables as follows:
		lowIntensMsgPosted = false;

		apdR1x = cos(axisRotationAPD * RAD);
		apdR2x = cos((axisRotationAPD + 90) * RAD);
		apdR1y = sin(axisRotationAPD * RAD);
		apdR2y = sin((axisRotationAPD + 90) * RAD);

		apdX_yk1 = 0.0;
		apdY_yk1 = 0.0;
		apdX_fk1 = 0.0;
		apdY_fk1 = 0.0;
		apdXControl1 = 0.0;
		apdYControl1 = 0.0;

		apdFirstTime = true;

//		if we're using the APDs
//			set the APDLoopStat flag to indicate that the loop is closed
//		else
//			set the APDLoopStat flag to indicate that the loop is closing (because we have to wait for the camera to 
//			> get into closed loop mode)
		if (UseAPD == 1)
		{
			APDLoopStat = eAPDLoopClosed;
			FileDataShmPtr->apdLoopStat = eAPDLoopClosed;
		}
		else
		{
			ReconDataPtr->lttCamCloseDone = false;
			lttCamCloseSettleCntr = 0;

			APDLoopStat = eAPDLoopClosing;
			FileDataShmPtr->apdLoopStat = eAPDLoopClosing;
		}
	}
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDProcess()

UNIT DESCRIPTION:
The purpose of this unit is to process APD data by computing an APD centroid, determining new
tip/tilt mirror settings if the APD tip/tilt loop is closed, and sending processed data off
to the recon thread (so that only one thread is writing to the DACs and to keep diagnostic data 
synchronized).

Note that the logic for the closed loop portion of this code comes from the original code.  
Although the approach used is more complicated than is necessary for this application, it works
on the old system.  So, it is being used here, at least for now.

UNIT INPUTS:
1.	apdCounts[] - current APD counts

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltAPDProcess( int32 apdCounts[NUM_APDS] )
{
	boolean updateDACsFlag;
	int16 i;
	int16 fovFactor;
	int32 overLimit;
	float32 a, b, c, d, totalIntensity;
	float32 apdErrX, apdErrY;
	float32 x_yk0, y_yk0, x_fk0, y_fk0;
	float32 xControl, yControl;
	float32 tmpXVolts, tmpYVolts;
	float32 xVolts = 0.0, yVolts = 0.0, xTelco = 0.0, yTelco = 0.0, telcoEnable = TELCO_DISABLE;
	int32 xVolts_I32, yVolts_I32;  // xVolts and yVolts in int32, truncated to tenths of millivolts
	float32 tmpTTVoltsX, tmpTTVoltsY;  //tt2inch

	float32 aveN;  //XXXdwp120112

//	check to make sure APDs aren't being over-illuminated
	if (UseAPD == 1)
	{
		overLimit = MAX_APD_COUNT_LIMIT / APDRate;
		if (perSecCntr == 0)
			apdOverLimitFlag = 0.0;  // false
		if ((apdCounts[0] > overLimit) || (apdCounts[1] > overLimit) || 
			(apdCounts[2] > overLimit) || (apdCounts[3] > overLimit))
			apdOverLimitFlag = 1.0;  // true
	}

	totalAPDCountsPerSec += apdCounts[0] + apdCounts[1] + apdCounts[2] + apdCounts[3];
	if (++perSecCntr >= APDRate)
	{
		perSecCntr = 0;
		totalAPDCountsPerSecKeep = (float32)totalAPDCountsPerSec;
		totalAPDCountsPerSec = 0;
	}

//	subtract APD backgrounds (computed when using APDs, entered into parms file by user when using LTT)
//XXX050916	if (UseAPD == 1)
		fovFactor = 1;
//XXX050916	else
//XXX050916		fovFactor = LttFov * LttFov / 4;

//XXXdwp120112	apdCounts[0] -= APDBack[0] * fovFactor;
//XXXdwp120112	apdCounts[1] -= APDBack[1] * fovFactor;
//XXXdwp120112	apdCounts[2] -= APDBack[2] * fovFactor;
//XXXdwp120112	apdCounts[3] -= APDBack[3] * fovFactor;

//	compute the APD centroid
	a = (float32)apdCounts[0];
	b = (float32)apdCounts[1];
	c = (float32)apdCounts[2];
	d = (float32)apdCounts[3];

	if (a < 0.0) a = 0.0;
	if (b < 0.0) b = 0.0;
	if (c < 0.0) c = 0.0;
	if (d < 0.0) d = 0.0;

	totalIntensity = a + b + c + d;

	if (totalIntensity <= 0.0)
	{
		apdErrX = 0.0;
		apdErrY = 0.0;
	}
	else
	{
		apdErrX = ((b + d) - (a + c)) / totalIntensity;
		apdErrY = ((c + d) - (a + b)) / totalIntensity;
	}

	updateDACsFlag = false;

//	if we're in LGS mode
	if (GSMode == eGSModeLGS)
	{
//		if the APD loop is opening
		if (APDLoopStat == eAPDLoopOpening)
		{
			xVolts = 0.0;
			yVolts = 0.0;
			telcoEnable = TELCO_DISABLE;
			updateDACsFlag = true;

			ReconDataPtr->xPocoOffload = 0.0;  //XXXpoco
			ReconDataPtr->yPocoOffload = 0.0;  //XXXpoco

			APDLoopStat = eAPDLoopOpen;
			FileDataShmPtr->apdLoopStat = eAPDLoopOpen;
		}

//		else if the APD loop is closing (we'll only be in the state if the SciMeasure LTT camera is in use)
		else if (APDLoopStat == eAPDLoopClosing)
		{
			if (ReconDataPtr->lttCamCloseDone)
			{
				if (++lttCamCloseSettleCntr >= LTT_CAM_CLOSE_SETTLE_WAIT)
				{
					APDLoopStat = eAPDLoopClosed;
					SysConSendStrngNoParm("Notification: lttclose complete");
				}
			}
		}

//		else if the APD loop is closed
		else if (APDLoopStat == eAPDLoopClosed)
		{
//			if we're below the allowable overall intensity, alert the host (but, leave things where they are)
			if (totalIntensity < apdMinIntensityThresh)
			{
				if (!lowIntensMsgPosted)
				{
					SysConSendStrngFltParm("Error: APD intensity too low ", totalIntensity);
					lowIntensMsgPosted = true;
				}
			}
//			otherwise
			else
			{
				lowIntensMsgPosted = false;

//XXXdwp120112 -- start -- change to simple inegrator with low pass filter
//				apply proportional gain and integrate
			    xControl = apdXControl1 + ttSignXAPD * APDGain * apdErrX;
			    yControl = apdYControl1 + ttSignYAPD * APDGain * apdErrY;

				aveN = (float)APDBack[0];  // APDBack[0] is serving as n for this rolling average low pass filter
				if (aveN > 1.0)
				{
					xControl = (xControl + apdXControl1 * (aveN - 1.0)) / aveN;
					yControl = (yControl + apdYControl1 * (aveN - 1.0)) / aveN;
				}

//				save values for next time
			    apdXControl1 = xControl;
			    apdYControl1 = yControl;

/*XXXdwp120112 -- middle
//				determine and apply proportional gain
				apdQ0x = ttSignXAPD * APDGain;
				apdQ0y = ttSignYAPD * APDGain;

				x_yk0 = apdQ0x * apdErrX;
				y_yk0 = apdQ0y * apdErrY;

//				apply adaptive gradient projection algorithm
			    x_fk0 = apdX_fk1 - (adapt * apdQ1 * x_yk0 * apdX_yk1) / (apdQ3 + apdX_yk1 * apdX_yk1);
			    y_fk0 = apdY_fk1 - (adapt * apdQ1 * y_yk0 * apdY_yk1) / (apdQ3 + apdY_yk1 * apdY_yk1);

//				apply remainder of control law
			    xControl = apdXControl1 + x_fk0 * x_yk0 - apdQ2 * x_yk0;
			    yControl = apdYControl1 + y_fk0 * y_yk0 - apdQ2 * y_yk0;

//				save values for next time
			    apdXControl1 = xControl;  // integrator values
			    apdYControl1 = yControl;
			    apdX_yk1 = x_yk0;  // discrete difference values for projection algorithm
			    apdY_yk1 = y_yk0;
			    apdX_fk1 = x_fk0;
			    apdY_fk1 = y_fk0;
XXXdwp120112 -- end*/
 
//				convert control values to volts, apply rotation, and range check
				tmpXVolts = xControl * DAC_VOLT_MAX;
				tmpYVolts = yControl * DAC_VOLT_MAX;

				xVolts = apdR1x * tmpXVolts + apdR1y * tmpYVolts;
				yVolts = apdR2x * tmpXVolts + apdR2y * tmpYVolts;

				if (xVolts < -DAC_VOLT_LIMIT)
					xVolts = -DAC_VOLT_LIMIT;
				else if (xVolts > DAC_VOLT_LIMIT)
					xVolts = DAC_VOLT_LIMIT;

				if (yVolts < -DAC_VOLT_LIMIT)
					yVolts = -DAC_VOLT_LIMIT;
				else if (yVolts > DAC_VOLT_LIMIT)
					yVolts = DAC_VOLT_LIMIT;

//				if this is the first time through the loop (since closing the loop)
//					set up values for telco boxcar average
				if (apdFirstTime)
				{
					apdFirstTime = false;

					telcoVoltsNdx = 0;

					xVolts_I32 = (int32)(xVolts * 10000.0);  // truncate xVolts and yVolts to tenths of millivolts to avoid the 
					yVolts_I32 = (int32)(yVolts * 10000.0);	 // > potential cumulative error of a boxcar averager

					crntTelcoSumX_I32 = xVolts_I32 * TELCO_AVG_N;
					crntTelcoSumY_I32 = yVolts_I32 * TELCO_AVG_N;
					for(i = 0; i < TELCO_AVG_N; i++)
					{
						telcoVoltsX_I32[i] = xVolts_I32;
						telcoVoltsY_I32[i] = yVolts_I32;
					}

					xTelco = xVolts;
					yTelco = yVolts;
				}

//				else
//					update values for telco boxcar averages and compute the averages
				else
				{
					xVolts_I32 = (int32)(xVolts * 10000.0);  // truncate xVolts and yVolts to tenths of millivolts to avoid the 
					yVolts_I32 = (int32)(yVolts * 10000.0);	 // > potential cumulative error of a boxcar averager

					crntTelcoSumX_I32 -= telcoVoltsX_I32[telcoVoltsNdx];
					crntTelcoSumY_I32 -= telcoVoltsY_I32[telcoVoltsNdx];
					crntTelcoSumX_I32 += xVolts_I32;
					crntTelcoSumY_I32 += yVolts_I32;
					telcoVoltsX_I32[telcoVoltsNdx] = xVolts_I32;
					telcoVoltsY_I32[telcoVoltsNdx] = yVolts_I32;

					if (++telcoVoltsNdx >= TELCO_AVG_N)
						telcoVoltsNdx = 0;

					xTelco = (float32)(crntTelcoSumX_I32 / TELCO_AVG_N) / 10000.0;
					yTelco = (float32)(crntTelcoSumY_I32 / TELCO_AVG_N) / 10000.0;
				}

//				scale the new telco data
				xTelco *= TT_TELCO_FACTOR;
				yTelco *= TT_TELCO_FACTOR;

				if (!pocoOffloadOnFlag)  //XXXpoco3
				{
					ReconDataPtr->xPocoOffload = 0.0;  //XXXpoco3
					ReconDataPtr->yPocoOffload = 0.0;  //XXXpoco3
				}
				else
				{
					ReconDataPtr->xPocoOffload = xTelco;  //XXXpoco2; minus sign removed
					ReconDataPtr->yPocoOffload = yTelco;  //XXXpoco2; minus sign removed
				}

//				indicate that telco is enabled and that DACs should be updated
				telcoEnable = TELCO_ENABLE;
				updateDACsFlag = true;
			}
		}
	}

//	finally, send the new APD data off to recon (so that only one thread is writing to the DACs)
	for (i = 0; i < NUM_APDS; i++)
		ReconDataPtr->newAPDCounts[i] = apdCounts[i];
	ReconDataPtr->newAPDErrX = apdErrX;
	ReconDataPtr->newAPDErrY = apdErrY;

	if (updateDACsFlag)
	{
//		tmpTTVoltsX = -xVolts;  // tt2inch
		tmpTTVoltsX = xVolts;  // tt3inch
		tmpTTVoltsY = yVolts;  // tt2inch

		xVolts = rtt2inch1x * tmpTTVoltsX + rtt2inch1y * tmpTTVoltsY;  // tt2inch
		yVolts = rtt2inch2x * tmpTTVoltsX + rtt2inch2y * tmpTTVoltsY;  // tt2inch

		if (xVolts < -TT_VOLT_LIMIT)  // tt2inch
			xVolts = -TT_VOLT_LIMIT;  // tt2inch
		else if (xVolts > TT_VOLT_LIMIT)  // tt2inch
			xVolts = TT_VOLT_LIMIT;  // tt2inch

		if (yVolts < -TT_VOLT_LIMIT)  // tt2inch
			yVolts = -TT_VOLT_LIMIT;  // tt2inch
		else if (yVolts > TT_VOLT_LIMIT)  // tt2inch
			yVolts = TT_VOLT_LIMIT;  // tt2inch

		ReconDataPtr->newAPDVoltsX = xVolts;
		ReconDataPtr->newAPDVoltsY = yVolts;
		ReconDataPtr->newAPDTelcoX = -xTelco;
		ReconDataPtr->newAPDTelcoY = -yTelco;
		ReconDataPtr->newAPDTelcoEnable = telcoEnable;
	}
	ReconDataPtr->newAPDUpdateDACsFlag = updateDACsFlag;

	ReconDataPtr->newAPDOverLimit = apdOverLimitFlag;
	ReconDataPtr->newAPDCountsPerSec = totalAPDCountsPerSecKeep;

	ReconDataPtr->newAPDDataFlag = true;

	if (rtl_sem_post(&ReconSem) < 0)
		SysConSendStrngIntParm("Error posting to ReconSem, errno = ", rtl_errno);
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDOpenLoop()

UNIT DESCRIPTION:
The purpose of this unit is to open the APD tip/tilt loop.  This is done by simply setting the
APDLoopStat flag to indicate that the loop is opening (things will be set as necessry when 
TipTiltAPD() runs next time).

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltAPDOpenLoop( void )
{
//	set flag indicating that the APD loop is opening
	if ((APDLoopStat == eAPDLoopClosing) || (APDLoopStat == eAPDLoopClosed))
	{
		APDLoopStat = eAPDLoopOpening;
		FileDataShmPtr->apdLoopStat = eAPDLoopOpening;
	}
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDBackStart()

UNIT DESCRIPTION:
The purpose of this unit is to start the APD background determination.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void TipTiltAPDBackStart( void )
{
	int i;

//	initialize variables and indicate that command is running
	apdSumCntr = 0;
	for (i = 0; i < NUM_APDS; i++)
	{
		APDBack[i] = 0;  // there's a slight risk, here: if the background process is aborted, these will stay zeros;
						 // > but, I've never seen a command aborted (and the APDs are going away, anyway)
		apdSums[i] = 0;
	}

	ReconRunCmd = eCmdTTAPDBack;
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDBackProcess()

UNIT DESCRIPTION:
The purpose of this unit is to process APD data when the APD background is being determined.

UNIT INPUTS:
1.	apdCounts[] - current APD counts

UNIT OUTPUTS:
1.	APDBack[] - global variable containing APD background counts

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void TipTiltAPDBackProcess( int32 apdCounts[NUM_APDS] )
{
	int i;

//	add the APD counts to the current sums
	for (i = 0; i < NUM_APDS; i++)
		apdSums[i] += apdCounts[i];

//	increment sum counter and if we have enough data:
//		compute the averages
//		command the SRT to store the computed background data in the parms file
//		set flags to indicate that command is not running and notify the host
	if (++apdSumCntr >= CflatAvgN)
	{
		for (i = 0; i < NUM_APDS; i++)
			APDBack[i] = apdSums[i] / apdSumCntr;

		FileDataPut(eFileTypeParms);

		ReconRunCmd = eCmdNone;
		ReconDataPtr->cmdCrnt = eCmdNone;
		SysConSendStrngNoParm("Notification: apdback complete");
	}
}


/**********************************************************************************************

UNIT NAME:  TipTiltAPDBackAbort()

UNIT DESCRIPTION:
The purpose of this unit is to abort the APD background determination.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void TipTiltAPDBackAbort( void )
{
	SysConSendStrngNoParm("Notification: apdback aborted");

	ReconRunCmd = eCmdNone;
	ReconDataPtr->cmdCrnt = eCmdNone;
}



