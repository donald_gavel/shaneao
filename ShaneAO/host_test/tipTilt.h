/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			tipTilt.h

AUTHOR:	DWP			DATE: 6/4/03

FILE DESCRIPTION:
This is the header file for the tip/tilt component.

******************************************************************************/

#ifndef _TIPTILT_H
#define _TIPTILT_H		1


// Enums

enum
{
	eGSModeNGS,  // natural guidestar mode
	eGSModeLGS,  // laser guidestar mode
};

// tip/tilt loop status values
enum
{
	eTTLoopOpening,
	eTTLoopOpen,
	eTTLoopClosed
};

// APD tip/tilt loop status values
enum
{
	eAPDLoopOpening,
	eAPDLoopOpen,
	eAPDLoopClosing,
	eAPDLoopClosed
};

// indexes into dacTTMap[] and CrntTTVolts[]
enum
{
	eTTMirrorX,
	eTTMirrorY,
	eTTTelcoX,
	eTTTelcoY,
	eTTLaserX,
	eTTLaserY,
	eTTNotUsed,
	eTTTelcoEnable,
	eTTNumVolts
};

// indexes for tip/tilt related diagnostic data
enum
{
	eTTDiagErrorX,
	eTTDiagErrorY,

	eTTDiagMirrorX,
	eTTDiagMirrorY,
	eTTDiagTelcoX,
	eTTDiagTelcoY,
	eTTDiagLaserX,
	eTTDiagLaserY,
	eTTDiagNotUsed,
	eTTDiagTelcoEnable,

	eTTDiagAPDA,
	eTTDiagAPDB,
	eTTDiagAPDC,
	eTTDiagAPDD,

	eTTDiagAPDErrX,
	eTTDiagAPDErrY,

	eTTDiagLVDTX,
	eTTDiagLVDTY,

	eTTDiagAPDOverLim,
	eTTDiagAPDCountsPerSec,

	eTTDiagNumData
};

// rotation modes for the laser FSM part of the reg command
enum
{
	eRegTTNotActive,
	eRegTTNoRot,
	eRegTTRot,
	eRegTTRot0To4
};


// Defines

#define PI     3.141593
#define	TWO_PI (2.0 * PI)
#define	RAD    (TWO_PI / 360.0)

#define NUM_APDS 4  // number of APDs
#define MAX_APD_COUNT_LIMIT 1000000L  // max APD counts (per APD, per second) before flagging an error

#define INIT_GS_MODE eGSModeNGS  // 0 sets to ngs mode, 1 sets to lgs mode

#define INIT_TT_GAIN  1.0  // gain for tip/tilt loop

#define INIT_APD_RATE 100    // rate at which APD tt loop is updated, per sec (between 50 and 500)
#define INIT_APD_GAIN 0.003  // gain for the APD tt loop
#define INIT_APD_BACK 0	     // APD background counts


// Typedefs

// gain coefficients for tip/tilt loop
typedef struct
{
	float32 q0;
	float32 q1;
	float32 q2;
	float32 q3;
	float32 q4;
	float32 q5;
	float32 q6;
} LoopQsType;


// Global Variables

extern int16 GSMode;  // 0 sets to ngs mode, 1 sets to lgs mode

extern int16 TTLoopStat;   // indicates the status of the tip/tilt loop (open, opening, or closed)
extern int16 APDLoopStat;  // indicates the status of the APD tip/tilt loop (open, opening, or closed)

extern float32 TTGain;  // tip/tilt loop gain

extern int16   APDRate;     // rate at which APD tt loop is updated, per sec (between 50 and 500)
extern float32 APDGain;     // gain for the APD tt loop
extern int32   APDBack[4];  // APD background counts for APDs 0 through 3 respectively

extern float32 CrntTTVolts[eTTNumVolts];  // current tip/tilt Volts

extern float32 axisRotationNGS;  // tip/tilt axis rotation for NGS mode (i.e., for the tip/tilt mirror) (in degrees)
extern float32 axisRotationLGS;  // tip/tilt axis rotation for LGS mode (i.e., for laser steering) (in degrees)
extern float32 axisRotationTT;  // tip/tilt axis rotation for the TT mirror on the optical table (in degrees)

extern float32 r1x, r2x, r1y, r2y;  // tip/tilt rotation coefficients
extern float32 rtt2inch1x, rtt2inch2x, rtt2inch1y, rtt2inch2y;

extern int32   CrntAPDCounts[NUM_APDS];  // current APD counts (from the TipTiltAPD() thread)
extern float32 CrntAPDErrX;  // current X error from APD centroider, in APD units (analogous to pixel units)
extern float32 CrntAPDErrY;  // current Y error from APD centroider, in APD units (analogous to pixel units)
extern float32 CrntAPDOverLim;  // current APD over limit flag
extern float32 CrntAPDCountsPerSec;  //	current total APD counts per second


// Global Function Prototypes

void TipTiltInit( void );
void TipTiltProcCmd( int16 cmdNdx );
void TipTiltProcess( float32 xTTError, float32 yTTError, float32 ttData[], 
					 float32 *xLaserOffload, float32 *yLaserOffload);
void TipTiltOpenLoop( void );
void *TipTiltAPD( void *arg );
void TipTiltAPDInit( void );
void TipTiltAPDCloseLoop( void );
void TipTiltAPDProcess( int32 apdCounts[NUM_APDS] );
void TipTiltAPDOpenLoop( void );
void TipTiltAPDBackAbort( void );

#endif
