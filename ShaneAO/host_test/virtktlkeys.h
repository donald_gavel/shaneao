#ifndef VIRTKTLKEYS_H_564002
#define VIRTKTLKEYS_H_564002

#include "interfacet.h"

class CKTLInt : public CKTLKeyInt
{
 public:
    CKTLInt(const char *name, int key, int val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);
    // Use the pure virtual functions when most keywords are
    // working to get compilation errors about which keywords
    // are not finished.
    //virtual int setValue() = 0;
    //virtual int TestComplete(int resp) = 0;
    virtual int SetValue(int val) {
        LogWarn("CAOKeyInt %s has no setValue", GetName());
        return -1;
    };
    virtual int TestComplete(int resp) {
        LogWarn("CAOKeyInt %s has no TestComplete", GetName());
        return 0; // say it's complete so we don't test forever.
    };
};


class CKTLFloat : public CKTLKeyFloat
{
 public:
    CKTLFloat(const char *name, int key, double val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);
    // Use the pure virtual functions when most keywords are
    // working to get compilation errors about which keywords
    // are not finished.
    //virtual int setValue(double val) = 0;
    //virtual int TestComplete(double resp) = 0;
    virtual int SetValue(double val) {
        LogWarn("CAOKeyFloat %s has no setValue", GetName());
        return -1;
    };
    virtual int TestComplete(double resp) {
        LogWarn("CAOKeyFloat %s has no TestComplete", GetName());
        return 0; // say it's complete so we don't test forever.
    };

};


class CKTLString : public CKTLKeyString
{
 public:
    CKTLString(const char *name, int key, const char *val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);
    // Use the pure virtual functions when most keywords are
    // working to get compilation errors about which keywords
    // are not finished.
    //virtual int setValue(string val) = 0;
    //virtual int TestComplete(double resp) = 0;
    virtual int SetValue(string val) {
        LogWarn("CAOKeyFloat %s has no setValue", GetName());
        return -1;
    };
    virtual int TestComplete(string resp) {
        LogWarn("CAOKeyFloat %s has no TestComplete", GetName());
        return 0; // say it's complete so we don't test forever.
    };

};














#endif
