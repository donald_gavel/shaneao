/**********************************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		cam_WFS_Mngr

AUTHOR:	DHL & DWP	DATE: 10/7/04

COMPONENT DESCRIPTION:
This component contains functions which interface to the WFS Little Joe Camera camera 
for the LickAO control system.  Control originates on the HRT in cam_WFS but all communication
with the EDT driver happens here.

COMPONENT UNITS:
1.	CamWFSMngr_ProcCmd()
2.	CamWFSMngr_StartCam()
3.	CamWFSMngr_StopCam()
4.	CamWFSMngr_RepCount()
5.	CamWFSMngr_TotalBufferSize()
6.	CamWFSMngr_SendToCam()

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <syslog.h>  //XXXTRACER2
//#include <rtl_fifo.h>
//#include <rtl_time.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"
#include "psfest.h"
#include "edtinc.h"


// Defines

#define ACK 6  // acknowledge from the SciMeasure Camera

// Local Variables

static int fd;					  // mmap file descriptor for the WFS shared memory
static unsigned char *wfsCamBuf;  // pointer for WFS shared memory buffer
static PdvDev *wfs_pdv_p;		  // pointer for the WFS PDV/EDT device

static u_int totalBufSize;	// the amount of shared memory allocated

static Dependent *dd_p;
static Edtinfo edtinfo;

//XXX static const WFSCamOffsets[4] = { 0, 0, 0, 0 };  //XXXNOISE
static const WFSCamOffsets[4] = { 350, 350, 350, 350 };  //XXXNOISE


// Local Function Prototypes

static int CamWFSMngr_RepCount (int Frame_Rate);
static u_int CamWFSMngr_TotalBufferSize (int16 DataSize);
static void CamWFSMngr_SendToCam (char outString[], char inString[]);


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_ProcCmd()

UNIT DESCRIPTION:
This unit processes commands originating in cam_WFS.

UNIT INPUTS:
1. cmd - the command ID
2. param - an integer parameter

UNIT OUTPUTS:
	None

**********************************************************************************************/

void CamWFSMngr_ProcCmd (int16 cmd, int16 param)
{
	int16 Gain;
	int retVal, Count, Atten;
	float FrameRate;
	char textBuf[32], inString[64];

//	switch on command ID corresponding to command index
	switch (cmd)
	{
		case eWFSInitialize1:
//			syslog(LOG_ERR | LOG_USER, "I0\n");  //XXXTRACER2
			if (wfs_pdv_p != NULL)
			{
//				pdv_start_images(wfs_pdv_p, 1);		  // stop the free-run acquistion XXX060501
//				syslog(LOG_ERR | LOG_USER, "I1\n");  //XXXTRACER2

				edt_disable_ring_buffers(wfs_pdv_p);										   // disable ring buffers
				pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", dd_p, &edtinfo);  // read the config file
				syslog(LOG_ERR | LOG_USER, "I1\n");  //XXXTRACER2
				pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", NULL, 0);  // init the cam
				syslog(LOG_ERR | LOG_USER, "I2\n");  //XXXTRACER2
			}
			break;

		case eWFSInitialize2:
//			syslog(LOG_ERR | LOG_USER, "I3\n");  //XXXTRACER2
			if (wfs_pdv_p != NULL)
			{
//				syslog(LOG_ERR | LOG_USER, "prePMB 1\n");  //XXX060501
				pdv_multibuf_block(wfs_pdv_p, WFSnumbufs, wfsCamBuf, totalBufSize);			   // set the DMA ring buffer sizes
//				syslog(LOG_ERR | LOG_USER, "postPMB 1\n");  //XXX060501

				syslog(LOG_ERR | LOG_USER, "I4\n");  //XXXTRACER2

				sprintf (textBuf, "@PRG %d", wfsLoopProg);
				CamWFSMngr_SendToCam (textBuf, NULL);   // set the camera program
				syslog(LOG_ERR | LOG_USER, "I5\n");  //XXXTRACER2

				sprintf (textBuf, "@FAM %d", wfsLoopFilt);
				CamWFSMngr_SendToCam (textBuf, NULL);
				syslog(LOG_ERR | LOG_USER, "I6\n");  //XXXTRACER2

				sprintf (textBuf, "@OIC #0:%d", WFSCamOffsets[0]);  //XXXNOISE
				CamWFSMngr_SendToCam (textBuf, NULL);				//XXXNOISE
				syslog(LOG_ERR | LOG_USER, "I7\n");  //XXXTRACER2

				sprintf (textBuf, "@OIC #1:%d", WFSCamOffsets[1]);  //XXXNOISE
				CamWFSMngr_SendToCam (textBuf, NULL);				//XXXNOISE
				syslog(LOG_ERR | LOG_USER, "I8\n");  //XXXTRACER2

				sprintf (textBuf, "@OIC #2:%d", WFSCamOffsets[2]);  //XXXNOISE
				CamWFSMngr_SendToCam (textBuf, NULL);				//XXXNOISE
				syslog(LOG_ERR | LOG_USER, "I9\n");  //XXXTRACER2

				sprintf (textBuf, "@OIC #3:%d", WFSCamOffsets[3]);  //XXXNOISE
				CamWFSMngr_SendToCam (textBuf, NULL);				//XXXNOISE
				syslog(LOG_ERR | LOG_USER, "I10\n");  //XXXTRACER2

//				syslog(LOG_ERR | LOG_USER, "prePSI 1\n");  //XXX060501
				pdv_start_images(wfs_pdv_p, 0);			// start the free-run acquistion
//				syslog(LOG_ERR | LOG_USER, "postPSI 1\n");  //XXX060501

				CamWFSMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
//				syslog(LOG_ERR | LOG_USER, "I11\n");  //XXXTRACER

				HostDiagSendStrngNoParm("Notification: wfscami complete");  //XXX060630
			}
			break;

		case eWFSSetRep:
			if (wfs_pdv_p != NULL)
			{
				CamWFSMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera

//				syslog(LOG_ERR | LOG_USER, "prePSI 2\n");  //XXX060501
				pdv_start_images(wfs_pdv_p, 1);			// stop the free-run acquistion
//				syslog(LOG_ERR | LOG_USER, "postPSI 2\n");  //XXX060501

				sprintf (textBuf, "@REP %d", CamWFSMngr_RepCount(param));  // param is the desired frame rate which is converted into a repetition count
				CamWFSMngr_SendToCam (textBuf, NULL);

//				syslog(LOG_ERR | LOG_USER, "prePMB 2\n");  //XXX060501
				pdv_multibuf_block(wfs_pdv_p, WFSnumbufs, wfsCamBuf, totalBufSize);  // set the DMA ring buffer sizes
//				syslog(LOG_ERR | LOG_USER, "postPMB 2\n");  //XXX060501

  //XXX				CamWFSMngr_SendToCam ("@OAC?", NULL);  // query offsets  //XXXNOISE

//				syslog(LOG_ERR | LOG_USER, "prePSI 3\n");  //XXX060501
				pdv_start_images(wfs_pdv_p, 0);			// and then re-start the free-run acquistion
//				syslog(LOG_ERR | LOG_USER, "postPSI 3\n");  //XXX060501

				CamWFSMngr_SendToCam ("@SEQ 1", NULL);  // restart the camera data

				HostDiagSendStrngNoParm("Notification: wfsrate complete");
			}
			break;

		case eWFSSetAtten:
			if (wfs_pdv_p != NULL)
			{
				CamWFSMngr_SendToCam ("@SEQ 0", NULL);	// stop sending data from the camera

//				syslog(LOG_ERR | LOG_USER, "prePSI 4\n");  //XXX060501
				pdv_start_images(wfs_pdv_p, 1);			// stop the free-run acquistion
//				syslog(LOG_ERR | LOG_USER, "postPSI 4\n");  //XXX060501

				sprintf (textBuf, "@AAM %d", param);	// param is the attenuation for all modules
				CamWFSMngr_SendToCam (textBuf, NULL);

//				syslog(LOG_ERR | LOG_USER, "prePMB 3\n");  //XXX060501
				pdv_multibuf_block(wfs_pdv_p, WFSnumbufs, wfsCamBuf, totalBufSize);  // set the DMA ring buffer sizes
//				syslog(LOG_ERR | LOG_USER, "postPMB 3\n");  //XXX060501

//				syslog(LOG_ERR | LOG_USER, "prePSI 5\n");  //XXX060501
				pdv_start_images(wfs_pdv_p, 0);			// and then re-start the free-run acquistion
//				syslog(LOG_ERR | LOG_USER, "postPSI 5\n");  //XXX060501

				CamWFSMngr_SendToCam ("@SEQ 1", NULL);	// restart the camera data

				HostDiagSendStrngNoParm("Notification: wfsgain complete");
			}
			CamDataPtr->initDone = true;
			break;

		case eWFSReadRep:  // read and report the REP as a frame rate
			if (wfs_pdv_p != NULL)
			{
				CamWFSMngr_SendToCam ("@REP?", inString);

				retVal = sscanf (&inString[10], "%x", &Count);  // reply format: "ACK@REP! $00hhhh" , where ACK is 0x6 and hhhh is the hex rep count
				FrameRate = 1.0 / ((0.00002 * (Count + 1)) + 0.001);

				HostDiagSendStrngFltParm("Notification: WFS camera rate: ", FrameRate);
			}
			break;

		case eWFSReadAtten:		// read and report the attenuation as a gain
			if (wfs_pdv_p != NULL)
			{
				CamWFSMngr_SendToCam ("@AIM? #0", inString);

				retVal = sscanf (&inString[10], "%d", &Atten);  // reply format: "ACK@AIM! #0:V", where ACK is 0x6 and V is the atten value
				Gain = 3 - Atten;

				HostDiagSendStrngIntParm("Notification: WFS camera gain: ", Gain);
			}
			break;

		default:
			HostDiagSendStrngIntParm("Error: unexpected command in cam_WFS_Mngr: ", cmd);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_StartCam()

UNIT DESCRIPTION:
The purpose of this unit is to initialize the EDT driver and set up the shared memory for the 
DMA ring buffers.  The memory size is provided from the HRT in cam_WFS.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

**********************************************************************************************/

void CamWFSMngr_StartCam(void)
{
	char devname[128];

	totalBufSize = CamWFSMngr_TotalBufferSize (WFSbufsize);

	if ((fd = open("/dev/WFS_rt_edt_mem", O_RDWR)) < 0)
	{
		printf("Error: couldn't open shared memory in CamWFS\n");
		return;
	}
	wfsCamBuf = mmap(0, totalBufSize, PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	if (wfsCamBuf == MAP_FAILED)
	{
		printf ("Error: couldn't map shared memory in CamWFS_Mngr\n");
		return;
	}

	memset(wfsCamBuf, 0x11, totalBufSize);
	printf("In camWFS, allocated %d bytes for %d buffers\n", totalBufSize, WFSnumbufs);

//	open the edt device (EDT_INTERFACE is defined in edtdef.h (included via edtinc.h))
	strcpy(devname, EDT_INTERFACE);
	printf ("Opening camWFS EDTpdv device: %s, unit: %d\n", devname, WFS_UNIT_NUM);
    if ((wfs_pdv_p = pdv_open(devname, WFS_UNIT_NUM)) == NULL)
    {
		printf("Error: pdv_open(%s,%d) failed \n", devname, WFS_UNIT_NUM);
		return;
    }

	dd_p = pdv_alloc_dependent();
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_StopCam()

UNIT DESCRIPTION:
The purpose of this unit is to close the EDT driver and unmap the shared memory for the 
DMA ring buffers.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

**********************************************************************************************/

void CamWFSMngr_StopCam(void)
{
	if (wfs_pdv_p != NULL)
	{
		CamWFSMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera

//		syslog(LOG_ERR | LOG_USER, "prePSI 6\n");  //XXX060501
		pdv_start_images(wfs_pdv_p, 1);			// stop the free-run acquistion
//		syslog(LOG_ERR | LOG_USER, "postPSI 6\n");  //XXX060501

	    pdv_close(wfs_pdv_p);
	}

    munmap(wfsCamBuf, totalBufSize);
    close(fd);
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_RepCount(int Frame_Rate)

UNIT DESCRIPTION:
The purpose of this unit is to compute a repetition count that yields the desired frame rate.
The values in the case statement assume a base frame rate of 1000 fps where each additional
repetition adds 20 microseconds to the frame time.  Currently program 3 has a 1000 fps base
rate but if a different program is selected in the future, these values may have to be adjusted.
The rate verses rep count for any program can be determined empirically by running the Little Joe 
and PdvShow utilities.  Adjust the rep count in Little Joe and read the frame rate in PdvShow.

UNIT INPUTS:
1.	Frame_Rate - in frames per second

UNIT OUTPUTS:
1.	the repetition count

UNIT ERROR CODES:
	None

**********************************************************************************************/
static int CamWFSMngr_RepCount (int Frame_Rate)
{
	int Count = 0;

	switch (Frame_Rate)
	{
		case 50:
			Count = 950;
			break;
		case 100:
			Count = 450;
			break;
		case 200:
			Count = 200;
			break;
		case 250:
			Count = 150;
			break;
		case 350:
			Count = 93;	// actual rate is 349.65
			break;
		case 500:
			Count = 50;
			break;
		case 750:
			Count = 17;	// actual rate is 746.27
			break;
		case 1000:
			Count = 0;
			break;
		default:
			// this should never happen since the frame rate is constrained in the GUI
			HostDiagSendStrngIntParm("Error: illegal Frame Rate in cam_WFS_Mngr = ", Frame_Rate);
			break;
	}
	return Count;
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_TotalBufferSize()

UNIT DESCRIPTION:
The purpose of this unit is to calculate the total DMA buffer size - rounding up to page size and 
making sure there's room for deinterleave if needed

UNIT INPUTS:
	Data size

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static u_int CamWFSMngr_TotalBufferSize (int16 DataSize)
{
	int npages;
	u_int bufsize = DataSize;
	u_int totalsize;

	npages = DataSize / PAGE_SIZE;

	if (DataSize % PAGE_SIZE)
	{
		npages++;
		bufsize = npages * PAGE_SIZE;
	}

	totalsize = bufsize * WFSnumbufs;
	return totalsize;
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_SendToCam()

UNIT DESCRIPTION:
The purpose of this unit is to send an ASCII command to the camera and read the response.

UNIT INPUTS:
1.	outString - the command string
2.	inString - a pointer to an array to put the camera response into, or NULL

UNIT OUTPUTS:
1.	inString - the camera response 

**********************************************************************************************/

static void CamWFSMngr_SendToCam (char outString[], char inString[])
{
    int  retVal, inCount;
	char localInString[64], errMsg[64];

	struct timespec next;

//	don't bother doing anything, if there's no camera
	if (wfs_pdv_p == NULL)
		return;

//	printf ("Sending to WFS camera: %s\n", outString);  //XXX

//  indicate that interrrupt should be ignored (because we're doing serial comms)  XXXINTIG
	CamDataPtr->intrIgnore = true;  //XXXINTIG

//	make sure the string to be returned is null terminated
	if (inString != NULL)
		inString[0] = '\0';

	next.tv_sec = 0;
	next.tv_nsec = 1000000;  // was 1000, then 100000
	nanosleep(&next, NULL);

//	flush any junk from the read buffer and issue command to camera
	pdv_serial_read(wfs_pdv_p, localInString, 32);
	next.tv_sec = 0;
	next.tv_nsec = 10000;  // was 1000
	nanosleep(&next, NULL);

	retVal = pdv_serial_command(wfs_pdv_p, outString);
	next.tv_sec = 0;
	next.tv_nsec = 10000;  // was 1000
	nanosleep(&next, NULL);

//	if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
	if (retVal != 0)
	{
		HostDiagSendStrngIntParm("Error from pdv_serial_command() in CamWFS: ", retVal);
		printf ("Error from pdv_serial_command() in CamWFS: %d\n", retVal);  //XXX
	}
	else
	{
		if (inString == NULL)
		{
			inCount = pdv_serial_wait(wfs_pdv_p, 500, 1);
			next.tv_sec = 0;
			next.tv_nsec = 500000000;  //XXX new; was 300000000
			nanosleep(&next, NULL);
		}
		else
		{
			inCount = pdv_serial_wait(wfs_pdv_p, 500, sizeof(localInString) - 1);
			next.tv_sec = 0;
			next.tv_nsec = 10000;  // was 1000
			nanosleep(&next, NULL);
		}

		if (inCount == 0)
		{
			HostDiagSendStrngNoParm("Error: no response from CamWFS 1");
			printf ("Error: no response from CamWFS 1\n");  //XXX
		}
		else
		{
			retVal = pdv_serial_read(wfs_pdv_p, localInString, inCount);

			if (retVal == 0)
			{
				HostDiagSendStrngNoParm("Error: no response from CamWFS 2");
				printf ("Error: no response from CamWFS 2\n");  //XXX
			}
			else if (retVal != inCount)
			{
				HostDiagSendStrngIntParm("Error from pdv_serial_read() in CamWFS: ", retVal);
				printf ("Error from pdv_serial_read() in CamWFS: %d\n", retVal);  //XXX
			}
			else if (localInString[0] != ACK)
			{
				HostDiagSendStrngNoParm("Error: no ACK from CamWFS");
				printf ("Error: no ACK from CamWFS\n");  //XXX
			}
			else if (retVal > 2)
			{
				localInString[retVal - 2] = '\0';
				sprintf(errMsg, "Notification: message from CamWFS: %s", &localInString[1]);
				HostDiagSendStrngNoParm(errMsg);
				printf ("%s\n", errMsg);  //XXX

				if (inString != NULL)
					strcpy(inString, localInString);
			}
		}
	}

//	indicate that interrupts should no longer be ignored
	CamDataPtr->intrIgnore = false;  // XXXINTIG
}
