/* psfest.h
 *
 * Header for library of routines for implementation of J.-P. Veran's
 * PSF estimation algorithm.
 *
 * Michael Fitzgerald (fitz@astron.berkeley.edu)
 * 
 */

#include "fitsio.h"

/*****  A few definitions *****/

#define N_SUBAP NUM_SUBAPS      // number of Shack-Hartmann subapertures  dwp
#define N_ACT   NUM_ACTV_ACTS   // number of deformable mirror actuators  dwp

/* number of pixels on a side of a WFS cell (assume square) */
#define N_CELL_PIX_SIDE 2 
/* number of pixels in a WFS cell */
#define N_CELL_PIX (N_CELL_PIX_SIDE*N_CELL_PIX_SIDE) 
/* number of pixels in WFS */
#define N_WFS_PIX (N_SUBAP*N_CELL_PIX) 



// pixel on the WFS
typedef double pixel_t;	
#define BITPIX_PIXEL DOUBLE_IMG
#define TYPE_PIXEL TDOUBLE
// a centroid position in the WFS (output of centering)
typedef double centr_t;	
#define BITPIX_CENTR DOUBLE_IMG
#define TYPE_CENTR TDOUBLE
// voltage (?) for an actuator on the DM 
typedef double dm_t;	
#define BITPIX_DM DOUBLE_IMG
#define TYPE_DM TDOUBLE



/*****  Some globals  *****/


/*  to be implemented
// modal gains
#define BITPIX_GAINS DOUBLE_IMG
#define TYPE_GAINS TDOUBLE
double pGains[N_ACT];

// number of modes
uint nModes = N_ACT;
*/



/****  Main Routines  ****/

void clear_accumulators(void);
void single_cycle(pixel_t* pWFS_pix, centr_t* pWFS_cent, dm_t* pDM_comm);
void write_out(char *filename);
