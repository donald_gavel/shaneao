/**********************************************************************************************

COPYRIGHT (C) 2005
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:		dispRS170

AUTHOR:				DATE: 11/14/05

COMPONENT DESCRIPTION:
This component is a standalone program whose sole purpose is to attach to the lickao shared
memory, periodically read RS170Buffs[][][] data, and write that data to the Camber 8510 RS-170
video board.  

COMPONENT UNITS:
1.	main()						dispRS170 mainline

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"

#include <Rdr8510Display.h>
#include <Rdr8510VideoConfig.h>

// Configuration Variables

static const long secDelay = 0;			  // 0 secs
static const long nsecDelay = 40000000L;  // 40 msecs, for a 25 Hz update rate


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static int totalSize;								  // total size of shared memory

static FileDataShmType *fileDataShmPtr;  // pointer to file data shared memory
static CamDataType *camDataPtr;			 // pointer to camera shared memory data
static LTTCamDataType *camLTTPtr;		 // pointer to Laser Tip Tilt camera shared memory data
static CentDataType *centDataPtr;		 // pointer to centroid shared memory data
static ReconDataType *reconDataPtr;		 // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
This unit is the dispRS170 mainline.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

int main(void)
{
	int i, j, iCam, jCam;
	int crntReadBuff;

	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;

	struct timespec next;

	int numRowsInDisp, numColsInDisp;
	int numRowsToDisp, numColsToDisp, offsetInRow;
	int numDispPixPerCamRow, numDispPixPerCamCol;
	int tmpBuffs[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW];

	Rdr8510Display *disp;

	int lowVal, highVal;
	float scaleFactor;

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

//	open the shared memory region
	while (!quit && ((mmfd = open("/dev/LickAO_mmap", O_RDWR)) < 0))
	{
		printf("dispRS170: waiting for mmap open\n");  //XXX
		sleep(1);
	}

//	get the pointer to the shared memory region
	while (!quit && ((startPtr = (unsigned char *)mmap(0, totalSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("dispRS170: waiting for mmap\n");  //XXX
		sleep(1);
	}

//	set up the pointers to the global variables
	fileDataShmPtr = (FileDataShmType *)startPtr;
	camDataPtr = (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr = (CentDataType *)((char *)camDataPtr + camDataSize);
	reconDataPtr = (ReconDataType *)((char *)centDataPtr + centDataSize);
	camLTTPtr = (LTTCamDataType *)((char *)reconDataPtr + reconDataSize);

//	instantiate display object
//XXX	disp = new Rdr8510Display("LinuxTest", 640, 486, Rdr8510VideoConfig::RS170_RGB);	// not sure why this has to be done first
//XXX	sleep(1);
	disp = new Rdr8510Display("dispRS170", 640, 486, Rdr8510VideoConfig::RS170_NTSC);
	sleep(2);

//	get number of rows and columns and compute number of display pixels per camera pixel
	numRowsInDisp = disp->GetNumRows();
	numColsInDisp = disp->GetNumCols();
	if (numRowsInDisp < numColsInDisp)
	{
		numDispPixPerCamRow = numRowsInDisp / NUM_ROWS_IN_CAM;
		numDispPixPerCamCol	= numRowsInDisp / NUM_PIX_IN_ROW;
	}
	else
	{
		numDispPixPerCamRow = numColsInDisp / NUM_ROWS_IN_CAM;
		numDispPixPerCamCol	= numColsInDisp / NUM_PIX_IN_ROW;
	}
	numRowsToDisp = numDispPixPerCamRow * NUM_ROWS_IN_CAM;
	numColsToDisp = numDispPixPerCamCol * NUM_PIX_IN_ROW;
	offsetInRow = (numColsInDisp - numColsToDisp) / 2;

//	clear the screen
	for (i = 0; i < numRowsInDisp; i++)
	{
		for (j = 0; j < numColsInDisp; j++)
			disp->unpackedVideoMem[i * numColsInDisp + j] = 0;
	}

/*XXX test start
//	set up lots of shades of gray
	unsigned int gray[640];
//	for (i =   0; i < 160; i++)
//		gray[i] = 0x3F00;
//	for (i = 160; i < 320; i++)
//		gray[i] = 0x7F00;
//	for (i = 320; i < 480; i++)
//		gray[i] = 0xBF00;
//	for (i = 480; i < 640; i++)
//		gray[i] = 0xFF00;
	for (i = 0; i < 640; i++)
		gray[i] = (i % 64) << 9;
//XXX test end */

//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	next.tv_sec = secDelay;
	next.tv_nsec = nsecDelay;
	while (!quit && (nanosleep(&next, NULL) == 0))
	{
//		if (!camLTTPtr->intrIgnore && !camDataPtr->intrIgnore)  //dispRS170 seems to have problems when doing serial comms with camera (not sure why)  XXXINTIG
		{
/*XXX test start
			for (i = 0; i < numRowsInDisp; i++)
			{
				for (j = 0; j < numColsInDisp; j++)
					disp->unpackedVideoMem[i * numColsInDisp + j] = gray[j];
			}
//XXX test end */
//*XXX
			lowVal = 0xFFFF;
			highVal = 0;
			crntReadBuff = camDataPtr->crntRS170ReadBuff;
			if ((crntReadBuff < 0) || (crntReadBuff > 1))					//XXX
			{																//XXX
//				crntReadBuff = 0;											//XXX
				printf("Error: invalid crntReadBuff: %d\n", crntReadBuff);  //XXX
			}																//XXX
			for (i = 0; i < NUM_ROWS_IN_CAM; i++)
			{
				for (j = 0; j < NUM_PIX_IN_ROW; j++)
				{
					tmpBuffs[i][j] = (int)camDataPtr->RS170Buffs[crntReadBuff][i][j];
					if (tmpBuffs[i][j] < lowVal)
						lowVal = tmpBuffs[i][j];
					if (tmpBuffs[i][j] > highVal)
						highVal = tmpBuffs[i][j];
				}
			}

			scaleFactor = (float)(highVal - lowVal);
			if (scaleFactor > 0.0)
				scaleFactor = 255.0 / scaleFactor;

			for (i = 0; i < NUM_ROWS_IN_CAM; i++)
			{
				for (j = 0; j < NUM_PIX_IN_ROW; j++)
				{
					tmpBuffs[i][j] = (int)((float)(tmpBuffs[i][j] - lowVal) * scaleFactor);
					tmpBuffs[i][j] = (tmpBuffs[i][j] << 8) & 0xFF00;
				}
			}

			for (i = 0; i < numRowsToDisp; i++)
			{
				iCam = i / numDispPixPerCamRow;
				for (j = 0; j < numColsToDisp; j++)
				{
					jCam = j / numDispPixPerCamCol;
					disp->unpackedVideoMem[i * numColsInDisp + j + offsetInRow] = tmpBuffs[iCam][jCam];
				}
			}
//XXX*/
		}

//		set up for next sleep
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
	}

	if (mmfd > -1)
	{
		printf("dispRS170: munmapping\n");  //XXX
//		clean up the shared memory
		if (startPtr > 0)
			munmap(startPtr, totalSize);
	  	close(mmfd);
	}

	printf("dispRS170: exiting, bye...\n");  //XXX
    return 0;
}


static void termHandler(int sigNum)
{
	quit = true;
}
