/**********************************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		cam_LTT

AUTHOR:	DHL & DWP	DATE: 10/7/04

COMPONENT DESCRIPTION:
This component contains functions to interface to the SciMeasure Laser tip/tilt camera for the 
LickAO control system.

COMPONENT UNITS:
1.	CamLTT()
2.	CamLTT_Init()
3.	CamLTT_IntrHandler()
4.	CamLTT_Restart()
5.	CamLTT_ProcCmd()
6.	CamLTT_SendCmd()
7.	CamLTT_LoadDataBuffer()
8.	CamLTT_deIntlvQuad()
9.	CamLTT_BackgroundStart()
10.	CamLTT_BackgroundProcess()
11.	CamLTT_DrvrWatch()
  
REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_fcntl.h>
#include <rtl_time.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_unistd.h>
#include <rtl_serial.h>
#include <rtl_posixio.h>
#include <rtl_fifo.h>
#include <rtl_math.h>
#include <sys/rtl_mman.h>

#include "types.h"
#include "constant.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_LTT_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"

#include "edt_lnx_kernel.h"

// Defines


// Global Variables
boolean LTTOpenLoopMode = true;

int16 LttFrameRate;  // LTT camera frame rate in frames per second
int16 LttCamGain;	 // LTT camera gain
int16 LttFov;	     // closed-loop field of view to 2,4, or 6 arc seconds
int16 UseAPD;        // if 0 use the SciMeasure Camera, else if 1 use the APD tilt/tip system

int16 LTTOpenBackground[OPEN_ROWS_IN_CAM][OPEN_PIX_IN_ROW];			// open loop background data
int16 LTTClosedBackground[CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW];  // closed loop background data

int LttTotalSize;	  // bytes in shared memory regionT
u_char *LttBaseAddr;  // pointer to start of shared memory

int LTT_irq = -1;
void *LTT_dev;


// Local Variables
static uint16 *open_buf_array[MAXNUMBUFS];
static uint16 *close_buf_array[MAXNUMBUFS];
static int16 runCmd = eCmdNone;  // the 'long' command that is currently being executed, if any

static int16 BackgroundCntr;   // counts frames being averaged when determining Background
static int16 BackgroundState;  // current state of the process determining Background
static int32 OpenBackSums[OPEN_ROWS_IN_CAM][OPEN_PIX_IN_ROW];		  // summed data for determining open backgrounds
static int32 ClosedBackSums[CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW];  // summed data for determining closed backgrounds

static int16 CamLTTIntrWatchCntr = -1;  // watchdog counter for camera interrupt
static unsigned long oldAffinity;
static int16 diagTelemCntr = 0;

static int DoneCount = 0, LastDone = 0, LttPostCntr = 0;  // variables shared by CamLTT() and CamLTT_IntrHandler()
static int BadCntr = 0;  //XXX

static int16 lttCamCloseSettleCntr;  // counts number of SciMeasure LTT camera frames to wait after it has entered 
									 // > closed loop mode

// static const int16 lttToAPD_Map[NUM_APDS] = { 0, 1, 2, 3 };  // maps the LTT camera quad-cell values to the APD positions
static const int16 lttToAPD_Map[NUM_APDS] = { 3, 2, 1, 0 };  // maps the LTT camera quad-cell values to the APD positions

// the following are for calculating quad-cell (APD) data from open loop data; they correspond to 2, 4, and 6 arc seconds
/*XXX20 start*/
#ifdef TEST_60X60
static const int16 openNumPixRowsInQuads[3] = { 3, 6, 9 };  // number of open loop pixel rows in closed loop quad-cell values
static const int16 openNumPixColsInQuads[3] = { 3, 6, 9 };  // number of open loop pixel columns in closed loop quad-cell values
static const int16 openStartRowForQuads[3][NUM_APDS] = { { 27, 27, 30, 30 },   // starting pixel rows for upper-left,
														 { 24, 24, 30, 30 },   // > upper-right, lower-left, and lower-right
														 { 21, 21, 30, 30 } };
static const int16 openStartColForQuads[3][NUM_APDS] = { { 27, 30, 27, 30 },   // starting pixel columns for upper-left,
														 { 24, 30, 24, 30 },   // > upper-right, lower-left, and lower-right
														 { 21, 30, 21, 30 } };
//XXX20 middle */
#else
static const int16 openNumPixRowsInQuads[3] = { 1, 2, 3 };  // number of open loop pixel rows in closed loop quad-cell values
static const int16 openNumPixColsInQuads[3] = { 1, 2, 3 };  // number of open loop pixel columns in closed loop quad-cell values
static const int16 openStartRowForQuads[3][NUM_APDS] = { { 9, 9, 10, 10 },   // starting pixel rows for upper-left,
														 { 8, 8, 10, 10 },   // > upper-right, lower-left, and lower-right
														 { 7, 7, 10, 10 } };
static const int16 openStartColForQuads[3][NUM_APDS] = { { 9, 10, 9, 10 },   // starting pixel columns for upper-left,
														 { 8, 10, 8, 10 },   // > upper-right, lower-left, and lower-right
														 { 7, 10, 7, 10 } };
#endif
//XXX20 end */

static const int16 closedNumPixRowsInQuads[3] = { 1, 2, 3 };  // number of closed loop pixel rows in closed loop quad-cell values
static const int16 closedNumPixColsInQuads[3] = { 1, 2, 3 };  // number of closed loop pixel columns in closed loop quad-cell values
static const int16 closedStartRowForQuads[3][NUM_APDS] = { { 2, 2, 3, 3 },   // starting pixel rows for upper-left,
														   { 1, 1, 3, 3 },   // > upper-right, lower-left, and lower-right
														   { 0, 0, 3, 3 } };
static const int16 closedStartColForQuads[3][NUM_APDS] = { { 2, 3, 2, 3 },   // starting pixel columns for upper-left,
														   { 1, 3, 1, 3 },   // > upper-right, lower-left, and lower-right
														   { 0, 3, 0, 3 } };

// Local Function Prototypes
static void CamLTT_Init(void);
static unsigned int CamLTT_IntrHandler(unsigned int irq, struct rtl_frame *regs);
static void CamLTT_Restart( void );

static void CamLTT_ProcCmd( int16 cmdNdx );
static void CamLTT_SendCmd( int16 cmd, int16 param );

static void CamLTT_LoadDataBuffer( int bufNdx );
static void CamLTT_deIntlvQuad(uint16 *src, int width, int rows, int16 *dest);

static void CamLTT_BackgroundStart(void);
static void CamLTT_BackgroundProcess(LTTCamBuffType *camBuffPtr, int16 closedData[]);


/**********************************************************************************************

UNIT NAME:  CamLTT()

UNIT DESCRIPTION:
This unit is the LTT camera thread mainline.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
1.	return value	- always 0 (NULL); should never be returned

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamLTT( void *arg )
{
	int curbuf;
	int diff;
	static int diff_count = 0;

	static int goodCntr = 0;  //XXX

//	perform LTT camera initializations
	CamLTT_Init();

//	send commands to initialize the EDT camera interface in user space
	CamLTT_SendCmd (eLTTInitialize, 0);
	CamLTT_SendCmd (eLTTSetRep, LttFrameRate);
	CamLTT_SendCmd (eLTTSetAtten, (3 - LttCamGain));

	CamLTTIntrWatchCntr = 0;

//	enter infinite loop
	while (1)
	{
//		this semaphore is currently activated in two places:
//		> in sysCon in response to LTT Host commands and in intr_handler in response to incomming frame data
		if (rtl_sem_wait(&LTTSem) < 0)
			SysConSendStrngIntParm("Error waiting on LTTSem, errno = ", rtl_errno);

//		if a command is pending, call CamLTT_ProcCmd() to process it
		else if (CamLTTPtr->cmdStart != eCmdNone)
		{
			CamLTT_ProcCmd(CamLTTPtr->cmdStart);
			CamLTTPtr->cmdStart = eCmdNone;
		}
		else // process the interrupt
		{
			if (LttPostCntr != 1)
			{
				SysConSendStrngIntParm("Error: CamLTT behind, 1: ", LttPostCntr);
				LttPostCntr = 0;
			}
			else
			{			
				LttPostCntr = 0;

//XXX050916				if (++goodCntr >= LttFrameRate)  //XXX
//XXX050916				{
//XXX050916					if (BadCntr > 10)  //XXX
//XXX050916						rtl_printf ("<0>L G: %d, B: %d\n", goodCntr, BadCntr);  //XXX
//XXX050916					goodCntr = 0;  //XXX
//XXX050916					BadCntr = 0;  //XXX
//XXX050916				} 

				diff = abs(LastDone - DoneCount);
				if (diff > 1)
				{
					diff_count++;
					rtl_printf ("<0>LTT sync problem last= %d current= %d errors= %d\n", LastDone, DoneCount, diff_count);
				}

				if (DoneCount <= 0)									//XXX051007
					rtl_printf ("<0>DoneCount = %d\n", DoneCount);  //XXX051007
				else												//XXX051007
				{													//XXX051007
					curbuf = (DoneCount - 1) % LTTnumbufs;  // the last DMA buffer is one less than DoneCount
					if (UseAPD == 0) 
						CamLTT_LoadDataBuffer (curbuf);
				}													//XXX051007

				LastDone = DoneCount;

				if (LttPostCntr != 0)
				{
					SysConSendStrngIntParm("Error: CamLTT behind, 2: ", LttPostCntr);
					LttPostCntr = 0;
				}
			}
		}
	}
	return 0;
}


/**********************************************************************************************

UNIT NAME:  CamLTT_Init()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the camera thread and to enable the
interrupt handler.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_Init(void)
{
	int i;
	int ret;
	int OpenBufPages, CloseBufPages;
	unsigned long affinity = 1 << HRT_CPU;
	struct rtl_timespec next;

//	map local arrays to shared memory
	OpenBufPages = LTTOpenbufsize / PAGE_SIZE;
	if (LTTOpenbufsize % PAGE_SIZE) OpenBufPages ++;

	CloseBufPages = LTTClosebufsize / PAGE_SIZE;
	if (LTTClosebufsize % PAGE_SIZE) CloseBufPages ++;

	open_buf_array[0] = (uint16 *)LttBaseAddr;
	close_buf_array[0] = (uint16 *)LttBaseAddr;
	for (i=1; i < LTTnumbufs; i++)
	{
	    open_buf_array[i] = open_buf_array[i-1] + (OpenBufPages * PAGE_SIZE / 2);
	    close_buf_array[i] = close_buf_array[i-1] + (CloseBufPages * PAGE_SIZE / 2);
	}

//	get the IRQ# from the EDT driver and install a handler for it
	LTT_irq = edt_get_irq_value(LTT_UNIT_NUM);
	rtl_printf ("<0>LTT camera using irg %d\n", LTT_irq);  //XXX
	if (LTT_irq >= 0)
	{
		LTT_dev = edt_get_dev_from_irq(LTT_irq);
	    edt_take_interrupt(LTT_irq, LTT_dev);
		if ((ret = rtl_request_irq( LTT_irq, CamLTT_IntrHandler )) != 0)
			rtl_printf("<0>LTT failed to get irq %d\n", LTT_irq);
	    rtl_irq_set_affinity(LTT_irq, &affinity, &oldAffinity);
	}

//	initialize variables
	CamLTTPtr->cmdStart = eCmdNone;
	CamLTTPtr->cmdCrnt = eCmdNone;

	CamLTTPtr->writeNdx = NUM_CAM_BUFFS - 1;
	CamLTTPtr->readNdx = NUM_CAM_BUFFS - 1;

	CamLTTPtr->diagActFlag = eDiagActNoAction;
	CamLTTPtr->diagStatFlag = eDiagStatStandby;
	CamLTTPtr->diagDiagRate = 0;
    CamLTTPtr->diagDiagCntr = 0;
	CamLTTPtr->diagTelemRate = 0;
	CamLTTPtr->diagPSFFlag = false;

	for (i = 0; i < NUM_CAM_BUFFS; i++)
    {
        CamLTTPtr->camBuffNdxs[i] = NUM_CAM_BUFFS - 1;
        CamLTTPtr->camBuffs[i].inUseFlag = false;
        CamLTTPtr->camBuffs[i].diagIndicator = eDiagIndNotInUse;
    }

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
	while (!OkayToFinishInit)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((ret = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in camLTT_Init(), retVal = ", ret);
    }

    CamLTTInitDone = true;
	rtl_printf ("<0>CamLTTInit done\n");
}


/**********************************************************************************************

UNIT NAME:  CamLTT_IntrHandler()

UNIT DESCRIPTION:
This routine is called in response to an interrupt generated by the EDT driver upon receipt of a
complete frame of camera data.  It's sole purpose is to activate a semaphore which triggers the
collection of the camera frame data.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static unsigned int CamLTT_IntrHandler(unsigned int irq, struct rtl_frame *regs)
{
//	rtl_printf("n");  //XXXTRACER
//	bump up LTT camera watchdog counter
	CamLTTIntrWatchCntr++;

//	let EDT do its interrupt processing
	edt_intr_process(LTT_dev);

//	get the current done counter
	DoneCount = edt_get_done(LTT_dev);
	if (DoneCount == 0)  //	DoneCount goes to zero whenever we switch between modes
	{
//		rtl_printf("o");  //XXXTRACER
		LastDone = 0;
	}

//	re-enable the rtl irq
	rtl_hard_enable_irq(irq);

//	wake up the camLTT thread only if there's a new buffer finished; otherwise, interrupt could be a 
//	> serial interrupt or sg ready (not sure what "sg ready" is)
	if (LastDone == DoneCount)
	{
//		rtl_printf("b");  //XXXTRACER
		;  //XXX050916  ++BadCntr;  //XXX
	}
	else
	{
		LttPostCntr++;  // used to make sure we're not getting interrupted faster than we're processing
						// > (since we're manipulating variables shared with CamLTT())
		rtl_sem_post(&LTTSem);
	}
//	rtl_printf("X");  //XXXTRACER

	return 0;
}


/**********************************************************************************************

UNIT NAME:  CamLTT_Restart()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the camera thread when processing
is restarted after the camera has been turned off then back on.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_Restart( void )
{
}


/**********************************************************************************************

UNIT NAME:  CamLTT_ProcCmd()

UNIT DESCRIPTION:
The purpose of this unit is to process camera commands.

UNIT INPUTS:
	cmdNdx - index into CmdTable[] for the command to be processed

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_ProcCmd( int16 cmdNdx )
{
	int i, j;

//	switch on command ID corresponding to command index
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		  // abort long commands
			if (runCmd == eCmdLTTBack)
			{
				SysConSendStrngNoParm("Notification: lttback aborted");
				runCmd = eCmdNone;
				CamLTTPtr->cmdCrnt = eCmdNone;
			}
			break;

		case eCmdLTTCamInit:  // initialize SciMeasure LTT camera
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				CamLTT_SendCmd (eLTTInitialize, 0);
				CamLTT_SendCmd (eLTTSetRep, LttFrameRate);
				CamLTT_SendCmd (eLTTSetAtten, (3 - LttCamGain));
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTClose:    // close LTT loop
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
//				make sure we're in lgs mode and report an error if we're not
				if (GSMode != eGSModeLGS)
					SysConSendStrngNoParm("Error: system must be in lgs mode");

//				otherwise, close the loop
				else
				{
					if (LTTOpenLoopMode)  // make sure we're actually changing modes
					{
						CamLTT_SendCmd (eLTTMapClose, 0);
//XXX we're not using						CamLTT_SendCmd (eLTTSetRep, LttFrameRate);   // have to set these again because the camera's
//XXX > @RCL any more						CamLTT_SendCmd (eLTTSetAtten, (3 - LttCamGain));  // > @RCL command resets them 
						TipTiltAPDCloseLoop();  // have to do the APD close loop stuff, too
						LTTOpenLoopMode = false;
					}
				}
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTOpen:     // open LTT loop
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				if (!LTTOpenLoopMode)  // make sure we're actually changing modes
				{
					TipTiltAPDOpenLoop();  // have to do the APD open loop stuff, too
					CamLTT_SendCmd (eLTTMapOpen, 0);
					LTTOpenLoopMode = true;
				}
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTRate:     // set LTT camera frame rate
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				LttFrameRate = CamLTTPtr->cmdIntParam;
				APDRate = LttFrameRate;  // change APD rate value, too
				CamLTT_SendCmd (eLTTSetRep, LttFrameRate);
				FileDataPut(eFileTypeParms);
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTGain:	  // set LTT camera gain
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				LttCamGain = CamLTTPtr->cmdIntParam;
				CamLTT_SendCmd (eLTTSetAtten, (3 - LttCamGain));
				FileDataPut(eFileTypeParms);
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTBack:     // determine LTT background
			if (UseAPD == 1)
			{
				SysConSendStrngNoParm("Error: currently using APDs");
				CamLTTPtr->cmdCrnt = eCmdNone;
			}
			else
				CamLTT_BackgroundStart ();
			break;

		case eCmdLTTZBack:    // zero the LTT background data
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
				{
					for (j = 0; j < OPEN_PIX_IN_ROW; j++)
						LTTOpenBackground[i][j] = 0;
				}

				for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
					LTTClosedBackground[i] = 0;

//XXX050916				for (i = 0; i < NUM_APDS; i++)
//XXX050916					APDBack[i] = 0;

				SysConSendStrngNoParm("Notification: lttzback complete");
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdLTTFov:      // set closed-loop field of view to 2,4, or 6 arc seconds
			if (UseAPD == 1)
				SysConSendStrngNoParm("Error: currently using APDs");
			else
			{
				LttFov = CamLTTPtr->cmdIntParam;	// this is used in tipTilt to determine software binning
				FileDataPut(eFileTypeParms);
				SysConSendStrngNoParm("Notification: lttfov complete");
			}
			CamLTTPtr->cmdCrnt = eCmdNone;
			break;

		default:
			SysConSendStrngIntParm("Error: unexpected command received by cam, cmdNdx = ", cmdNdx);
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CamLTT_SendCmd()

UNIT DESCRIPTION:
The purpose of this unit is to send commands to the SRT to write messages (buy subverting the FIFO 
diagnostic message process) to the camera and to change the DMA mapping to accommodate changes in 
frame size into shared memory.

UNIT INPUTS:
1.	Cmd		tells the SRT side to either send an ASCII message or change the DMA mapping.

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_SendCmd( int16 cmd, int16 param )
{
	diagFIFOMsgType diagFIFOMsg;

//	set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagLTTCmd;
	diagFIFOMsg.cmdNdx = cmd;
	diagFIFOMsg.intParam = param;

    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm("Error writing CamLTT command to diag FIFO, errno = ", rtl_errno);
}


/**********************************************************************************************

UNIT NAME:  CamLTT_LoadDataBuffer

UNIT DESCRIPTION:
The purpose of this unit is to 

UNIT INPUTS:
1.	

UNIT OUTPUTS:
1.	


UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_LoadDataBuffer(int bufNdx)
{
	boolean sendTelemData;
	int16 i, j, fovNdx;
	int16 buffCntr, buffNdx;
	int16 firstFree, firstFreeButWithDiag;
	diagFIFOMsgType diagFIFOMsg;
	int16 closedData[CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW];
    LTTCamBuffType *camBuffPtr;
	uint16 *DataPtr;
	int16 (*data6x6)[CLOSED_PIX_IN_ROW];
	int16 *data1D;

//	find the next available LTT camera data buffer
//	if there isn't an available camera data buffer, report an error
//	if the buffer we're about to use has unread diag data in it, initiate a diag data resync
    camBuffPtr = &CamLTTPtr->camBuffs[CamLTTPtr->camBuffNdxs[CamLTTPtr->writeNdx]];
    buffCntr = 0;
    buffNdx = CamLTTPtr->camBuffNdxs[CamLTTPtr->writeNdx];
    firstFree = -1;
    firstFreeButWithDiag = -1;
    while ((buffCntr < NUM_CAM_BUFFS) && (firstFree < 0))
    {
        if (++buffNdx >= NUM_CAM_BUFFS)
            buffNdx = 0;
        if (!CamLTTPtr->camBuffs[buffNdx].inUseFlag)
        {
            if ((CamLTTPtr->diagStatFlag != eDiagStatOperating) ||
                (CamLTTPtr->camBuffs[buffNdx].diagIndicator == eDiagIndNotInUse))
                firstFree = buffNdx;
            else if (firstFreeButWithDiag < 0)
                firstFreeButWithDiag = buffNdx;
        }
        buffCntr++;
    }

	if ((firstFree < 0) && (firstFreeButWithDiag < 0))
	{
		SysConSendStrngNoParm("Error: camLTT buffers all full");
        camBuffPtr = &CamLTTPtr->camBuffs[0];  //XXX XXX
	}
    else
    {
        if (++CamLTTPtr->writeNdx >= NUM_CAM_BUFFS)
            CamLTTPtr->writeNdx = 0;

        if (firstFree < 0)
        {
            firstFree = firstFreeButWithDiag;

            CamLTTPtr->diagStatFlag = eDiagStatResyncing;
            diagFIFOMsg.diagID = eDiagLTTData;
            diagFIFOMsg.diagStat = eDiagStatResyncing;
            diagFIFOMsg.diagReadNdx = firstFree;
            if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
                SysConSendStrngIntParm("Error writing camera resync to diag FIFO, errno = ", rtl_errno);
            SysConSendStrngNoParm("Warning: LTT camera diagnostic data is resyncing");
        }
        CamLTTPtr->camBuffNdxs[CamLTTPtr->writeNdx] = firstFree;
        camBuffPtr = &CamLTTPtr->camBuffs[firstFree];
        camBuffPtr->inUseFlag = true;
    }

//	if diag data transfer is to be restarted and we're not operating already
//		indicate that we're operating
//	else if diag data transfer is to enter standby and we're not in standby already
//		indicate that we're operating in standby
	if ((CamLTTPtr->diagActFlag == eDiagActRestart) && (CamLTTPtr->diagStatFlag != eDiagStatOperating))
    {
		CamLTTPtr->diagStatFlag = eDiagStatOperating;
        CamLTTPtr->diagActFlag = eDiagActNoAction;
    }
	else if ((CamLTTPtr->diagActFlag == eDiagActEnterStandby) && (CamLTTPtr->diagStatFlag != eDiagStatStandby))
    {
		CamLTTPtr->diagStatFlag = eDiagStatStandby;
        CamLTTPtr->diagActFlag = eDiagActNoAction;
    }

	sendTelemData = false;
	if ((CamLTTPtr->diagTelemRate > 0) && 
		(++diagTelemCntr >= (LttFrameRate / CamLTTPtr->diagTelemRate)))
	{
		diagTelemCntr = 0;
		sendTelemData = true;
	}

    camBuffPtr->ignoreDataFlag = true;
//	if we're in open loop mode (both LTT and APD)
	if ((LTTOpenLoopMode) && (APDLoopStat == eAPDLoopOpen))
	{
		if (((CamLTTPtr->diagStatFlag == eDiagStatOperating) && 
			 ((CamLTTPtr->diagDiagCntr > 0) || sendTelemData)) || 
			(runCmd == eCmdLTTBack))
		{
//			move DMA data to LTT cam data buffer
			DataPtr = open_buf_array[bufNdx];
			CamLTT_deIntlvQuad(DataPtr, OPEN_PIX_IN_ROW, OPEN_ROWS_IN_CAM, &camBuffPtr->data[0][0]);

//			if we're running the background command, process the data
			if (runCmd == eCmdLTTBack)
			{
				CamLTT_deIntlvQuad(DataPtr, CLOSED_PIX_IN_ROW, CLOSED_ROWS_IN_CAM, closedData);  // get data in closed loop format, too
				CamLTT_BackgroundProcess(camBuffPtr, closedData);
			}

//			subtract the open loop backgrounds
			for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
			{
				for (j = 0; j < OPEN_PIX_IN_ROW; j++)
					camBuffPtr->data[i][j] -= LTTOpenBackground[i][j];
			}

//			if we're to send telemetry data, compute the open loop quad-cells to update them in TipTiltAPD()
			if (sendTelemData)
			{
				fovNdx = LttFov / 2 - 1;
				if ((fovNdx >= 0) && (fovNdx <= 2))
				{
					for (i = 0; i < NUM_APDS; i++)
						camBuffPtr->quadCounts[i] = 0;
					for (i = 0; i < openNumPixRowsInQuads[fovNdx]; i++)
					{
						for (j = 0; j < openNumPixColsInQuads[fovNdx]; j++)
						{
							camBuffPtr->quadCounts[lttToAPD_Map[0]] += camBuffPtr->data[openStartRowForQuads[fovNdx][0] + i]
																					   [openStartColForQuads[fovNdx][0] + j];
							camBuffPtr->quadCounts[lttToAPD_Map[1]] += camBuffPtr->data[openStartRowForQuads[fovNdx][1] + i]
																					   [openStartColForQuads[fovNdx][1] + j];
							camBuffPtr->quadCounts[lttToAPD_Map[2]] += camBuffPtr->data[openStartRowForQuads[fovNdx][2] + i]
																					   [openStartColForQuads[fovNdx][2] + j];
							camBuffPtr->quadCounts[lttToAPD_Map[3]] += camBuffPtr->data[openStartRowForQuads[fovNdx][3] + i]
																					   [openStartColForQuads[fovNdx][3] + j];
						}
					}
				    camBuffPtr->ignoreDataFlag = false;
				}
			}
		}
	}
	else  // closed loop
	{
		fovNdx = LttFov / 2 - 1;
		if ((fovNdx >= 0) && (fovNdx <= 2))
		{
//			move DMA data to LTT cam data buffer
			DataPtr = close_buf_array[bufNdx];
			CamLTT_deIntlvQuad(DataPtr, CLOSED_PIX_IN_ROW, CLOSED_ROWS_IN_CAM, &camBuffPtr->data[0][0]);

//			subtract out backgrounds
			data1D = (int16 *)camBuffPtr->data;  // re-cast the data as a 1-dimensional array
			for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
				data1D[i] -= LTTClosedBackground[i];

//			compute the closed loop quad-cells
			for (i = 0; i < NUM_APDS; i++)
				camBuffPtr->quadCounts[i] = 0;
			data6x6 = (int16(*)[CLOSED_PIX_IN_ROW])camBuffPtr->data;  // re-cast the data as a 6x6 array
			for (i = 0; i < closedNumPixRowsInQuads[fovNdx]; i++)
			{
				for (j = 0; j < closedNumPixColsInQuads[fovNdx]; j++)
				{
					camBuffPtr->quadCounts[lttToAPD_Map[0]] += data6x6[closedStartRowForQuads[fovNdx][0] + i]
																	  [closedStartColForQuads[fovNdx][0] + j];
					camBuffPtr->quadCounts[lttToAPD_Map[1]] += data6x6[closedStartRowForQuads[fovNdx][1] + i]
																	  [closedStartColForQuads[fovNdx][1] + j];
					camBuffPtr->quadCounts[lttToAPD_Map[2]] += data6x6[closedStartRowForQuads[fovNdx][2] + i]
																	  [closedStartColForQuads[fovNdx][2] + j];
					camBuffPtr->quadCounts[lttToAPD_Map[3]] += data6x6[closedStartRowForQuads[fovNdx][3] + i]
																	  [closedStartColForQuads[fovNdx][3] + j];
				}
			}

			camBuffPtr->ignoreDataFlag = false;
		}
	}

//	post whether we're in open or closed loop (and whether there's actually new data or not) to keep the read index in
//	> TipTiltAPD() up to date
	if (rtl_sem_post(&LTT_Data_Sem) < 0)
		SysConSendStrngIntParm("Error posting command to LTT_Data_Sem, errno = ", rtl_errno);

//	if diagnostic data transfer (to the SRT) is operating and a message should be sent
//		send the camera data off to diag
	if ((CamLTTPtr->diagStatFlag == eDiagStatOperating) && 
		((CamLTTPtr->diagDiagCntr > 0) || sendTelemData))
	{
		if (sendTelemData)
			camBuffPtr->diagIndicator = eDiagIndTelem;
		else
			camBuffPtr->diagIndicator = eDiagIndInUse;

		diagFIFOMsg.diagID = eDiagLTTData;
		diagFIFOMsg.diagStat = eDiagStatOperating;
		diagFIFOMsg.diagReadNdx = CamLTTPtr->camBuffNdxs[CamLTTPtr->writeNdx];

		if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
		{
			camBuffPtr->diagIndicator = eDiagIndNotInUse;
			SysConSendStrngIntParm("Error writing camera data to diag FIFO, errno = ", rtl_errno);
		}
        else
        {
            if (CamLTTPtr->diagDiagCntr > 0)
                CamLTTPtr->diagDiagCntr--;
        }
	}
}


/**********************************************************************************************

UNIT NAME:  CamLTT_deIntlvQuad()

UNIT DESCRIPTION:
The purpose of this unit is to de-interleave the SciMeasure camera data.  

UNIT INPUTS:
1.	src   - pointer to the ring buffer
2.	width - width of the frame
3.	rows  - number of rows in the frame

UNIT OUTPUTS:
1.	dest - pointer to the output array


UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_deIntlvQuad(uint16 *src, int width, int rows, int16 *dest)
{
    int i, j, k;
    int nRows, nCols, nHalfRows, nHalfCols;
	int ul, ur, ll, lr;  // indexes for quadrants on the CCD (upper left, upper right, lower left, lower right)

    nRows = rows;
    nCols = width;
    nHalfRows = nRows >> 1;
    nHalfCols = nCols >> 1;
	i = 0;
	for (j = 0; j < nHalfRows; j++)
	{
		ul = j * nCols;
		ur = ul + nCols - 1;
		ll = (nRows - 1 - j) * nCols;
		lr = ll + nCols - 1;

		for (k = 0; k < nHalfCols; k++)
		{
		    dest[ul++] = src[i++];
		    dest[ur--] = src[i++];
		    dest[ll++] = src[i++];
		    dest[lr--] = src[i++];
		}
	}
}


/**********************************************************************************************

UNIT NAME:  CamLTT_BackgroundStart()

UNIT DESCRIPTION:
The purpose of this unit is to start the processing of the background command by initializing the
background sums (to be used to compute averages) and setting counters and flags necessary to run 
the command.


UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_BackgroundStart( void )
{
	int16 i, j;

//	initialize background sums to zeroes
	for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
	{
		for (j = 0; j < OPEN_PIX_IN_ROW; j++)
			OpenBackSums[i][j] = 0;
	}

	for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
		ClosedBackSums[i] = 0;

//	initialize sum counter and state and set flag to indicate that the background command is running
	BackgroundCntr = 0;
	BackgroundState = eCmdStateSum;
	runCmd = eCmdLTTBack;
}


/**********************************************************************************************

UNIT NAME:  CamLTT_BackgroundProcess(LTTCamBuffType *camBuffPtr)

UNIT DESCRIPTION:
The purpose of this unit is to process the Background command by receiving CflatAvgN frames worth of
raw camera data, averaging those frames, and writing the result to a file when done.

Note that this will execute 1 frame longer than CflatAvgN in order to spread out the averaging
and storing of the data.

UNIT INPUTS:
1.	camBuffPtr - pointer to the current data buffer
2.	closedData[] - data in closed loop format, needed for processing closed loop backgrounds

UNIT OUTPUTS:
1.	updates open loop background data

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void CamLTT_BackgroundProcess(LTTCamBuffType *camBuffPtr, int16 closedData[])
{
	int16 i, j;

//	sum or average and store Background data, depending on BackgroundState
	switch (BackgroundState)
	{
		case eCmdStateSum:
//			add current raw frame data to Background sums
			for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
			{
				for (j = 0; j < OPEN_PIX_IN_ROW; j++)
					OpenBackSums[i][j] += camBuffPtr->data[i][j];
			}

//			increment sum counter and change state to average data if we have enough data
			if (++BackgroundCntr >= CflatAvgN)
				BackgroundState = eCmdStateAvg;
			break;

		case eCmdStateAvg:
//			average the background sums to compute the new backgrounds and command the SRT to store them to disk
			for (i = 0; i < OPEN_ROWS_IN_CAM; i++)
			{
				for (j = 0; j < OPEN_PIX_IN_ROW; j++)
					LTTOpenBackground[i][j] = (int16)(OpenBackSums[i][j] / BackgroundCntr);
			}
			FileDataPut(eFileTypeLTTOpen);

//			initialize sum counter, switch to closed loop configuration (6x6 as opposed to 20x20),
//			> and change state to wait for that switch to complete
			BackgroundCntr = 0;
			ReconDataPtr->lttCamCloseDone = false;
			lttCamCloseSettleCntr = 0;
			CamLTT_SendCmd (eLTTMapClose, 0);

			BackgroundState = eCmdStateWaitForClosed;
			break;

		case eCmdStateWaitForClosed:
//			wait until switch to closed loop has completed
			if (ReconDataPtr->lttCamCloseDone)
			{
				if (++lttCamCloseSettleCntr >= LTT_CAM_CLOSE_SETTLE_WAIT)
					BackgroundState = eCmdStateSumClosed;
			}
			break;

		case eCmdStateSumClosed:
//			add current raw frame data to closed loop Background sums
			for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
				ClosedBackSums[i] += closedData[i];

//			increment sum counter and change state to average data if we have enough data
			if (++BackgroundCntr >= CflatAvgN)
				BackgroundState = eCmdStateAvgClosed;
			break;

		case eCmdStateAvgClosed:
//			average the closed loop background sums to compute the new backgrounds and command the SRT to store them to disk
			for (i = 0; i < (CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW); i++)
				LTTClosedBackground[i] = (int16)(ClosedBackSums[i] / BackgroundCntr);
			FileDataPut(eFileTypeLTTClosed);

//			switch back to the open loop configuration (20x20)
			CamLTT_SendCmd (eLTTMapOpen, 0);

//			set flags to indicate that command is not running and notify the host
			runCmd = eCmdNone;
			CamLTTPtr->cmdCrnt = eCmdNone;
			SysConSendStrngNoParm("Notification: LTT background complete");
			break;

		default:
			SysConSendStrngNoParm("Error: unexpected LTT Background State");
			break;
	}
}


/**********************************************************************************************

UNIT NAME:  CamLTT_DrvrWatch()

UNIT DESCRIPTION:
This is a thread used as a camera watchdog.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void *CamLTT_DrvrWatch( void *arg )
{
	int retVal;
	struct rtl_timespec next;

//  spin on the InitDone flags for the other threads
    while (!TiptTiltAPDInitDone || !CamInitDone || !CentInitDone || !ReconInitDone || !CamLTTInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm("Error from nanosleep() in CamWatch(), retVal = ", retVal);
    }

//	enter infinite test loop
	while (1)
	{
//		wait for 2 seconds (increased from 1 sec to accommodate longer delay when switching camera programs)
		next.tv_sec = 2;
		next.tv_nsec = 0;
   		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep(), retVal = ", retVal);

//		make sure we've received camera frames and, if not
//			notify the user
        if (CamLTTIntrWatchCntr == 0)
        {
            CamLTTIntrWatchCntr = -1;
			SysConSendStrngNoParm("Notification: LTT camera watchdog timeout");
		}
        else if (CamLTTIntrWatchCntr > 0)
            CamLTTIntrWatchCntr = 0;
	}
}
