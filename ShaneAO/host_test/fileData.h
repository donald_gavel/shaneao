/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			fileData.h

AUTHOR:	DWP			DATE: 3/12/03

FILE DESCRIPTION:
This is the header file for the file data handler on the HRT side.

******************************************************************************/

#ifndef _FILEDATA_H
#define _FILEDATA_H		1

#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "recon.h"

// Enums


// Defines


// Typedefs

// file data shared memory type
typedef struct 
{
	boolean srtIsUpFlag;  // used to indicate to the HRT that the SRT has come up

	boolean allDataAvailFromFile;					// indicates that new data has been read from all files and is available
	boolean someDataAvailFromFile;					// indicates that new data has been read from one or more files and is available
	boolean dataAvailFromFile[eFileTypeNum];		// indicates that new data has been read from a file and is available
	volatile boolean dataShmLockHRT[eFileTypeNum];  // used to lock shared memory for writes
	volatile boolean dataShmLockSRT[eFileTypeNum];  // used to lock shared memory for writes

//	'non-parms' data to be shared with SRT (not stored to disk, NOT protected by dataShmLocks)
	int16 aoLoopStat;   // indicates the status of the ao loop (open, opening, or closed)
	int16 ttLoopStat;   // indicates the status of the tip/tilt loop (open, opening, or closed)
	int16 apdLoopStat;  // indicates the status of the APD tip/tilt loop (open, opening, or closed)

	float32 crntReconCwt1;  // ReconCwt1 if ReconCwt1 is <= 0.0, else defaultCwt1s[] corresponding to current frame rate
	float32 crntReconCwt2;  // ReconCwt2 if ReconCwt2 is <= 0.0, else defaultCwt2s[] corresponding to current frame rate

//	parms data
	char crntFilenames[eFileTypeNum][MAX_FILENAME_LENGTH];  // current filenames

	int16 cflatAvgN;      // number of frames to average when determining cflat
	int16 refcentAvgN;    // number of frames to average when determining reference centroids
	int16 cmAvgN;         // number of centroids to average when determining a system matrix
	int16 flatSharpAvgN;  // number of DM voltages to average when determining flat and sharp mirror values

	int16 gsMode;  // 0 sets to ngs mode, 1 sets to lgs mode

	int16 camFrameRate;  // camera frame rate in frames per second
	int16 camGain;		 // camera gain (1 or 2)
	int16 camCRTGain;	 // camera CRT gain (0 to 4)

	int16 crntCentroider;  // indicates which centroid algorithm is currently being used
	int16 centThresh;      // value centroider subtracts from raw data (then, if result is less than zero,
				           // > centroider sets raw data to zero)

	float32 reconGain;  // AO loop gain (proportional gain)
	float32 reconInt;   // AO loop integrator gain
	float32 reconCwt1;  // AO loop compensator weight 1
	float32 reconCwt2;  // AO loop compensator weight 2

	float32 ttGain;  // tip/tilt loop gain

	int16   apdRate;			// rate at which APD tt loop is updated, per sec (between 50 and 500)
	float32 apdGain;			// gain for the APD tt loop
	int32   apdBack[NUM_APDS];  // APD background counts for APDs 0 through 3 respectively

	uint32 dacSNs[NUM_DAC_BOARDS];  // DAC board serial numbers in the order that they are installed in the chassis and should 
									// > be hooked up to the hardware (the order is from left-to-right, looking at the front 
									// > of the chassis)
    int16 LttFrameRate;     // camera frame rate in frames per second
    int16 LttCamGain;	    // set LTT loop gain
    int16 LttFov;	        // closed-loop field of view to 2,4, or 6 arc seconds
    int16 UseAPD;           // if 0 use the LTT SciMeasure Camera, else if 1 use the APD array
    int16 UseAOA;           // if 0 use the LTT SciMeasure Camera, else if 1 use the AOA camera


//	cflat data
	int16 cflatUnusedTop[NUM_UNUSED_CAM_ROWS_TOP][NUM_PIX_IN_ROW];  // unused cflat data
	int16 cflatData[NUM_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current cflat data
	int16 cflatUnusedBot[NUM_UNUSED_CAM_ROWS_BOT][NUM_PIX_IN_ROW];  // unused cflat data

//	refcent data
	float32 refcentsB[NUM_CENTROIDS];  // current reference centroids, 4x4 binned
	float32 refcentsC[NUM_CENTROIDS];  // current reference centroids, center-of-mass
	float32 refcentsQ[NUM_CENTROIDS];  // current reference centroids, quad-cell
	float32 refcentsO[NUM_CENTROIDS];  // current reference centroids, correlator

//	centoff data
	float32 centoffs[NUM_CENTROIDS];     // current reference offsets (for image sharpening)
	float32 newCentoffs[NUM_CENTROIDS];  // new reference offsets (used to transfer these values when a centoffs command is executed)

//	cm data
	boolean cmDataFileError;			                  // indicates cm data file read error occurred
	float32 controlMatrix[NUM_ACTV_ACTS][NUM_CENTROIDS];  // control matrix used for VMM reconstruction (AKA, cm)

//	sm data
	float32 systemMatrix[NUM_CENTROIDS][NUM_ACTV_ACTS];  // system matrix (used by host to create a cm)

//	flat data
	float32 flat[NUM_ACTV_ACTS];  // current flat mirror values (i.e., DM voltages corresponding to a 'flattened' mirror)

//	sharp data
	float32 sharp[NUM_ACTV_ACTS];  // current sharp mirror values (i.e., DM voltages corresponding to a 'image sharpened' mirror)

//	Laser tilt/tip background data
	int16 LTTOpenBackground[OPEN_ROWS_IN_CAM][OPEN_PIX_IN_ROW];			// open loop
	int16 LTTClosedBackground[CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW];  // closed loop
} FileDataShmType;


// Global Variables


// Global Function Prototypes

void FileDataInit( void );
void FileDataCmd( int16 readCmd );

boolean FileDataGet( int16 fileType );
void FileDataPut( int16 fileType );


#endif
