/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			cmdTable.h

AUTHOR:	DWP			DATE: 2/10/03

FILE DESCRIPTION:
This file simply defines the command table.  It is in a separate file so that
it can be included by both the HRT and SRT programs.

******************************************************************************/


// the fields for g_CmdTable[] are:
//		char	*strng;			// pointer to ASCII command string constant
//		int16	idNum;			// command ID number (may or may not be the same as the index into the table!)
//		int16	action;			// action to be taken when command is received
//		int16	loopRestrict	// indicates whether the ao and/or tt loops must be open for a command to execute
//		int16	paramIndicator;	// indicates what kind of parameter the command has (none, int, or float)
//		int16   paramLimLowI;	// lower limit for integer parameters (inclusive)
//		int16   paramLimHighI;	// upper limit for integer parameters (inclusive); if lower and upper are equal, there is no limit
//		float32 paramLimLowF;	// lower limit for float parameters (inclusive)
//		float32 paramLimHighF;	// upper limit for float parameters (inclusive); if lower and upper are equal, there is no limit
const cmdTableType g_CmdTable[eCmdNumCmds] =
{
  {"quit",     eCmdQuit,         eCmdActQuit,        eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // quit command
  {"restart",  eCmdRestart,      eCmdActRestart,     eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // restart command
  {"shutdown", eCmdShutdown,     eCmdActShutdown,    eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // shutdown command

  {"diag",     eCmdDiag,         eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamInt,     0, 0,                 0.0, 0.0},  // diagnostic command
  {"images",   eCmdImages,       eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // collect raw images (same as diag 0x01)
  {"data",     eCmdData,         eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // collect diagnostic data (same as diag 0x0E)
  {"apddata",  eCmdAPDData,      eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // collect ADP (actually, TT) data (same as diag 0x10)
  {"telem",    eCmdTelem,        eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamInt,     0, 0,                 0.0, 0.0},  // select telemetry data
  {"trate",    eCmdTRate,        eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamInt,     1, 50,                0.0, 0.0},  // telemetry rate of update (per second)
  {"pon",      eCmdPOn,          eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // turns PSF estimation on
  {"poff",     eCmdPOff,         eCmdActSendToDiag,  eCmdNoRestrict, eCmdParamInt,     0, 9999,              0.0, 0.0},  // turns PSF estimation off

  {"wfscami",  eCmdCamWFSInit,   eCmdActSendToCam,   eCmdAllOpen,	 eCmdParamNone,    0, 0,                 0.0, 0.0},  // initialize SciMeasure WFS camera
  {"rate",     eCmdCamRate,      eCmdActSendToCam,   eCmdBothSHOpen, eCmdParamInt,    50, 1000,              0.0, 0.0},  // set frame rate of the camera (per second)  //XXX changed from 55 to 50 for SMC
  {"cgain",    eCmdCamGain,      eCmdActSendToCam,   eCmdBothSHOpen, eCmdParamInt,     0, 3,                 0.0, 0.0},  // set camera gain (1 or 2)  //XXX change to 0, 1, 2, or 3 for SMC
  {"crtgain",  eCmdCamCRTGain,   eCmdActSendToCam,   eCmdNoRestrict, eCmdParamInt,     0, 64,                0.0, 0.0},  // set camera CRT gain (0 to 4)  //XXXRS170  CRT gain (0 to 64)
  {"srate",    eCmdCamSRate,     eCmdActSendToCam,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // read frame rate from camera
  {"scgain",   eCmdCamSGain,     eCmdActSendToCam,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // read gain from camera
  {"scrtgain", eCmdCamSCRTGain,  eCmdActSendToCam,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // read CRT gain from camera

  {"centbin",  eCmdCentBin,      eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // select 4x4 binned as centroid algorithm
  {"centcm",   eCmdCentCM,       eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // select center-of-mass as centroid algorithm
  {"centquad", eCmdCentQuad,     eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // select quad-cell as centroid algorithm
  {"centcorr", eCmdCentCorr,     eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // select correlator as centroid algorithm
  {"centtest", eCmdCentTest,     eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // select centroid test mode
  {"cflat",    eCmdCentFlat,     eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // determines and stores new flat file (dark levels)
  {"refcent",  eCmdCentRef,      eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamNone,    0, 0,                 0.0, 0.0},  // determines and stores new ref cents
  {"sparms",   eCmdCentSParms,   eCmdActSendToCent,  eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // sets ref cents from scratch params file
  {"thresh",   eCmdCentThresh,   eCmdActSendToCent,  eCmdBothSHOpen, eCmdParamInt,     0, 4095,              0.0, 0.0},  // sets threshold used by centroider on raw data
  {"centoffs", eCmdCentCentoffs, eCmdActSendToCent,  eCmdNoRestrict, eCmdParamSpecial, 0, 0,                -1.0, 1.0},  // sets centroid offsets (for image sharpening)
  {"saveoffs", eCmdCentSaveoffs, eCmdActSendToCent,  eCmdNoRestrict, eCmdParamNone,	   0, 0,                 0.0, 0.0},  // saves centroid offsets to file
  {"don",      eCmdCentDon,      eCmdActSendToCent,  eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, DAC_VOLT_LIMIT},  // turns TT mirror dithering on
  {"doff",     eCmdCentDoff,     eCmdActSendToCent,  eCmdNoRestrict, eCmdParamNone,	   0, 0,                 0.0, 0.0},  // turns TT mirror dithering off

  {"cm",	   eCmdReconCM,      eCmdActSendToRecon, eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // determine System Matrix (in preparation for computing CM offline)
  {"close",    eCmdReconClose,   eCmdActSendToRecon, eCmdAOOpenBit,  eCmdParamNone,    0, 0,                 0.0, 0.0},  // closes the AO loop
  {"open",     eCmdReconOpen,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // opens the AO loop
  {"estop",    eCmdReconEstop,   eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // opens the AO loop and the TT loop
  {"gain",     eCmdReconGain,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, 1.5},  // sets AO loop gain (proportional gain)  //XXXGAINTEST - set 1.0 to 10.0
  {"int",      eCmdReconInt,     eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, 1.0},  // sets AO loop integrator gain  PIPI
  {"cwt1",     eCmdReconCwt1,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                -1.5, 1.0},  // sets AO loop compensator weight 1 (> 0.0 means use default)
  {"cwt2",     eCmdReconCwt2,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                -1.5, 1.0},  // sets AO loop compensator weight 2 (> 0.0 means use default)
  {"mflat",    eCmdReconMflat,   eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // averages and saves actuator voltages as flat mirror values
  {"flatten",  eCmdReconFlatten, eCmdActSendToRecon, eCmdAOOpenBit,  eCmdParamNone,    0, 0,                 0.0, 0.0},  // loads the latest stored flat mirror values
  {"msharp",   eCmdReconMsharp,  eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // averages and saves actuator voltages as image-sharpened mirror values
  {"sharpen",  eCmdReconSharpen, eCmdActSendToRecon, eCmdAOOpenBit,  eCmdParamNone,    0, 0,                 0.0, 0.0},  // loads the latest stored image-sharpened mirror values
  {"reg",      eCmdReconReg,     eCmdActSendToRecon, eCmdAllOpen,    eCmdParamInt,    -1, 460,				 0.0, 0.0},  // starts moving actuator x up and down at 1 Hz
  {"regok",    eCmdReconRegok,   eCmdActSendToRecon, eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // stops moving all actuators that were started with reg

  {"gsmode",   eCmdTTGSMode,	 eCmdActSendToRecon, eCmdAllOpen,    eCmdParamInt,     0, 1,                 0.0, 0.0},  // 0 sets to ngs mode, 1 sets to lgs mode

  {"ttclose",  eCmdTTClose,      eCmdActSendToRecon, eCmdTTOpenBit,  eCmdParamNone,    0, 0,                 0.0, 0.0},  // close tip/tilt loop
  {"ttopen",   eCmdTTOpen,       eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // open tip/tilt loop
  {"ttgain",   eCmdTTGain,       eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, 1.5},  // set tip/tilt loop gain  //XXXGAINTEST - set 1.0 to 10.0
  {"ttxmov",   eCmdTTXMov,       eCmdActSendToRecon, eCmdAllOpen,	 eCmdParamFloat,   0, 0,      -TT_VOLT_LIMIT, TT_VOLT_LIMIT},  // set tip/tilt mirror X actuator (in Volts)  tt2inch
  {"ttymov",   eCmdTTYMov,       eCmdActSendToRecon, eCmdAllOpen,	 eCmdParamFloat,   0, 0,      -TT_VOLT_LIMIT, TT_VOLT_LIMIT},  // set tip/tilt mirror Y actuator (in Volts)  tt2inch
  {"lxmov",    eCmdTTLXMov,      eCmdActSendToRecon, eCmdAllOpen,	 eCmdParamFloat,   0, 0, -TTLASER_VOLT_LIMIT, TTLASER_VOLT_LIMIT},  // set laser mirror X actuator (in Volts)
  {"lymov",    eCmdTTLYMov,      eCmdActSendToRecon, eCmdAllOpen,	 eCmdParamFloat,   0, 0, -TTLASER_VOLT_LIMIT, TTLASER_VOLT_LIMIT},  // set laser mirror Y actuator (in Volts)

  {"apdclose", eCmdTTAPDClose,   eCmdActSendToRecon, eCmdAPDOpenBit, eCmdParamNone,    0, 0,                 0.0, 0.0},  // close APD tip/tilt loop
  {"apdopen",  eCmdTTAPDOpen,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // open APD tip/tilt loop
  {"apdrate",  eCmdTTAPDRate,    eCmdActSendToRecon, eCmdAllOpen,	 eCmdParamInt,    50, 500,               0.0, 0.0},  // set APD tip/tilt loop rate
  {"apdgain",  eCmdTTAPDGain,    eCmdActSendToRecon, eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, 1.5},  // set APD tip/tilt loop gain  //XXXGAINTEST - set 1.0 to 10.0
  {"apdback",  eCmdTTAPDBack,    eCmdActSendToRecon, eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // determine APD background
  {"apdzback", eCmdTTAPDZBack,   eCmdActSendToRecon, eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // zero APD background

  {"olon", 	   eCmdTTOlOn,   	 eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // turn POCO offloading on  //XXXpoco3
  {"oloff",    eCmdTTOlOff,   	 eCmdActSendToRecon, eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // turn POCO offloading off  //XXXpoco3

  {"lttcami",  eCmdLTTCamInit,   eCmdActSendToLTT,   eCmdAllOpen,	 eCmdParamNone,    0, 0,                 0.0, 0.0},  // initialize SciMeasure LTT camera
  {"lttclose", eCmdLTTClose,     eCmdActSendToLTT,   eCmdLTTOpenBit, eCmdParamNone,    0, 0,                 0.0, 0.0},  // close LTT loop
  {"lttopen",  eCmdLTTOpen,      eCmdActSendToLTT,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // open LTT loop
  {"lttrate",  eCmdLTTRate,      eCmdActSendToLTT,   eCmdLTTOpenBit, eCmdParamInt,    50, 500,               0.0, 0.0},  // set LTT frame rate
  {"lttgain",  eCmdLTTGain,      eCmdActSendToLTT,   eCmdLTTOpenBit, eCmdParamInt,     0, 3,                 0.0, 0.0},  // set LTT camera gain
  {"lttback",  eCmdLTTBack,      eCmdActSendToLTT,   eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // determine LTT background
  {"lttzback", eCmdLTTZBack,     eCmdActSendToLTT,   eCmdAllOpen,    eCmdParamNone,    0, 0,                 0.0, 0.0},  // zero LTT background
  {"lttfov",   eCmdLTTFov,       eCmdActSendToLTT,   eCmdNoRestrict, eCmdParamInt,     2, 6,                 0.0, 0.0},  // set closed-loop field of view to 2,4, or 6 arc seconds

  {"abort",    eCmdAbort,        eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // abort currently running HRT command
  {"parms",    eCmdSysConParms,  eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // reads the centroid parameter files
  {"cparms",   eCmdSysConCParms, eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // reads the centroid parameter files (for backward compat.)
  {"fillcm",   eCmdSysConFillcm, eCmdActSendToHRT,   eCmdBothSHOpen, eCmdParamStrng,   0, 0,                 0.0, 0.0},  // reads the specified cm file
  {"testn",    eCmdTestN,        eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamNone,    0, 0,                 0.0, 0.0},  // test, no param
  {"testi",    eCmdTestI,        eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamInt,     0, 0,                 0.0, 0.0},  // test, int param
  {"testf",    eCmdTestF,        eCmdActSendToHRT,   eCmdNoRestrict, eCmdParamFloat,   0, 0,                 0.0, 0.0}   // test, float param
};
