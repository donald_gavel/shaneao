/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
FILE NAME:			dacMaps.h

AUTHOR:	DWP			DATE: 5/16/03

FILE DESCRIPTION:
This file defines DAC maps that map 'logical' DAC ports (i.e., indexes into 
these tables) into physical DAC boards and channels.  This is a separate file 
simply because of the length of the tables.

******************************************************************************/

// DAC board handles used to map logical board addresses (as in the DACMapType structures below) to physical DAC boards
// > (these are modified when the DAC boards are initialized)
static int16 dacBoardHandles[NUM_DAC_BOARDS] = { -1, -1, -1, -1, -1 };

// IMPORTANT: due to a cabling SNAFU, the odd and even port addresses are swapped

// the fields for the DAC maps are:
//		int board;  // logical DAC board (0 to 4)
//		int chan;	// DAC channel on DAC board (0 to 31)

// dacDACMap[] is a 'straight' mapping from the map index to channels 0 through 31 of boards 0 through 4.
// In the following, the 'a' numbers are the designators for the active actuators, followed by the active actuator
// > row (starting from the bottom) and position in that row.
static DACMapType dacDACMap[NUM_DAC_PORTS] =
{
	{ 0,	 1 },	//   0		
	{ 0,	 0 },	//   1		
	{ 0,	 3 },	//   2		
	{ 0,	 2 },	//   3		
	{ 0,	 5 },	//   4		
	{ 0,	 4 },	//   5		
	{ 0,	 7 },	//   6		
	{ 0,	 6 },	//   7		
	{ 0,	 9 },	//   8		
	{ 0,	 8 },	//   9		
	{ 0,	11 },	//  10		
	{ 0,	10 },	//  11		
	{ 0,	13 },	//  12	a26	4, 0
	{ 0,	12 },	//  13		
	{ 0,	15 },	//  14		
	{ 0,	14 },	//  15		
	{ 0,	17 },	//  16		
	{ 0,	16 },	//  17	a35	5, 0
	{ 0,	19 },	//  18	a18	3, 0
	{ 0,	18 },	//  19		
	{ 0,	21 },	//  20		
	{ 0,	20 },	//  21		
	{ 0,	23 },	//  22		
	{ 0,	22 },	//  23	a43	6, 0
	{ 0,	25 },	//  24	a27	4, 1
	{ 0,	24 },	//  25	a11	2, 0
	{ 0,	27 },	//  26		
	{ 0,	26 },	//  27		
	{ 0,	29 },	//  28		
	{ 0,	28 },	//  29	a50	7, 0
	{ 0,	31 },	//  30	a36	5, 1
	{ 0,	30 },	//  31	a19	3, 1

	{ 1,	 1 },	//  32	a05	1, 0
	{ 1,	 0 },	//  33		
	{ 1,	 3 },	//  34		
	{ 1,	 2 },	//  35	a56	8, 0
	{ 1,	 5 },	//  36	a44	6, 1
	{ 1,	 4 },	//  37	a28	4, 2
	{ 1,	 7 },	//  38	a12	2, 1
	{ 1,	 6 },	//  39	a00	0, 0
	{ 1,	 9 },	//  40		
	{ 1,	 8 },	//  41		
	{ 1,	11 },	//  42	a51	7, 1
	{ 1,	10 },	//  43	a37	5, 2
	{ 1,	13 },	//  44	a20	3, 2
	{ 1,	12 },	//  45	a06	1, 1
	{ 1,	15 },	//  46		
	{ 1,	14 },	//  47		
	{ 1,	17 },	//  48	a57	8, 1
	{ 1,	16 },	//  49	a45	6, 2
	{ 1,	19 },	//  50	a29	4, 3
	{ 1,	18 },	//  51	a13	2, 2
	{ 1,	21 },	//  52	a01	0, 1
	{ 1,	20 },	//  53		
	{ 1,	23 },	//  54		
	{ 1,	22 },	//  55	a52	7, 2
	{ 1,	25 },	//  56	a38	5, 3
	{ 1,	24 },	//  57	a21	3, 3
	{ 1,	27 },	//  58	a07	1, 2
	{ 1,	26 },	//  59		
	{ 1,	29 },	//  60		
	{ 1,	28 },	//  61	a58	8, 2
	{ 1,	31 },	//  62	a46	6, 3
	{ 1,	30 },	//  63	a30	4, 4

	{ 2,	 1 },	//  64	a14	2, 3
	{ 2,	 0 },	//  65	a02	0, 2
	{ 2,	 3 },	//  66		
	{ 2,	 2 },	//  67		
	{ 2,	 5 },	//  68	a53	7, 3
	{ 2,	 4 },	//  69	a39	5, 4
	{ 2,	 7 },	//  70	a22	3, 4
	{ 2,	 6 },	//  71	a08	1, 3
	{ 2,	 9 },	//  72		
	{ 2,	 8 },	//  73		
	{ 2,	11 },	//  74	a59	8, 3
	{ 2,	10 },	//  75	a47	6, 4
	{ 2,	13 },	//  76	a31	4, 5
	{ 2,	12 },	//  77	a15	2, 4
	{ 2,	15 },	//  78	a03	0, 3
	{ 2,	14 },	//  79		
	{ 2,	17 },	//  80		
	{ 2,	16 },	//  81	a54	7, 4
	{ 2,	19 },	//  82	a40	5, 5
	{ 2,	18 },	//  83	a23	3, 5
	{ 2,	21 },	//  84	a09	1, 4
	{ 2,	20 },	//  85		
	{ 2,	23 },	//  86		
	{ 2,	22 },	//  87	a60	8, 4
	{ 2,	25 },	//  88	a48	6, 5
	{ 2,	24 },	//  89	a32	4, 6
	{ 2,	27 },	//  90	a16	2, 5
	{ 2,	26 },	//  91	a04	0, 4
	{ 2,	29 },	//  92		
	{ 2,	28 },	//  93		
	{ 2,	31 },	//  94	a55	7, 5
	{ 2,	30 },	//  95	a41	5, 6

	{ 3,	 1 },	//  96	a24	3, 6
	{ 3,	 0 },	//  97	a10	1, 5
	{ 3,	 3 },	//  98		
	{ 3,	 2 },	//  99		
	{ 3,	 5 },	// 100		
	{ 3,	 4 },	// 101	a49	6, 6
	{ 3,	 7 },	// 102	a33	4, 7
	{ 3,	 6 },	// 103	a17	2, 6
	{ 3,	 9 },	// 104		
	{ 3,	 8 },	// 105		
	{ 3,	11 },	// 106		
	{ 3,	10 },	// 107		
	{ 3,	13 },	// 108	a42	5, 7
	{ 3,	12 },	// 109	a25	3, 7
	{ 3,	15 },	// 110		
	{ 3,	14 },	// 111		
	{ 3,	17 },	// 112		
	{ 3,	16 },	// 113		
	{ 3,	19 },	// 114	a34	4, 8
	{ 3,	18 },	// 115		
	{ 3,	21 },	// 116		
	{ 3,	20 },	// 117		
	{ 3,	23 },	// 118		
	{ 3,	22 },	// 119		
	{ 3,	25 },	// 120		
	{ 3,	24 },	// 121		
	{ 3,	27 },	// 122		
	{ 3,	26 },	// 123		
	{ 3,	29 },	// 124		
	{ 3,	28 },	// 125		
	{ 3,	31 },	// 126		
	{ 3,	30 },	// 127		

	{ 4,	 1 },	// 128	TT mirror, X
	{ 4,	 0 },	// 129	TT mirror, Y
	{ 4,	 3 },	// 130	Telco offload, X
	{ 4,	 2 },	// 131	Telco offload, Y
	{ 4,	 5 },	// 132	Laser steering, X
	{ 4,	 4 },	// 133	Laser steering, Y
	{ 4,	 7 },	// 134		
	{ 4,	 6 },	// 135	Telco offload enable
	{ 4,	 9 },	// 136		
	{ 4,	 8 },	// 137		
	{ 4,	11 },	// 138		
	{ 4,	10 },	// 139		
	{ 4,	13 },	// 140		
	{ 4,	12 },	// 141		
	{ 4,	15 },	// 142		
	{ 4,	14 },	// 143		
	{ 4,	17 },	// 144		
	{ 4,	16 },	// 145		
	{ 4,	19 },	// 146		
	{ 4,	18 },	// 147		
	{ 4,	21 },	// 148		
	{ 4,	20 },	// 149		
	{ 4,	23 },	// 150		
	{ 4,	22 },	// 151		
	{ 4,	25 },	// 152		
	{ 4,	24 },	// 153		
	{ 4,	27 },	// 154		
	{ 4,	26 },	// 155		
	{ 4,	29 },	// 156		
	{ 4,	28 },	// 157		
	{ 4,	31 },	// 158		
	{ 4,	30 }	// 159		
};


// dacDMMap[] is a mapping for the deformable mirror.
// In the following, the 'a' numbers are the designators for the active actuators, followed by the active actuator
// > row (starting from the bottom) and position in that row.
static DACMapType dacDMMap[NUM_ACTV_ACTS] =
{
	{ 1,	 6 },	//  39	a00	0, 0
	{ 1,	21 },	//  52	a01	0, 1
	{ 2,	 0 },	//  65	a02	0, 2
	{ 2,	15 },	//  78	a03	0, 3
	{ 2,	26 },	//  91	a04	0, 4

	{ 1,	 1 },	//  32	a05	1, 0
	{ 1,	12 },	//  45	a06	1, 1
	{ 1,	27 },	//  58	a07	1, 2
	{ 2,	 6 },	//  71	a08	1, 3
	{ 2,	21 },	//  84	a09	1, 4
	{ 3,	 0 },	//  97	a10	1, 5

	{ 0,	24 },	//  25	a11	2, 0
	{ 1,	 7 },	//  38	a12	2, 1
	{ 1,	18 },	//  51	a13	2, 2
	{ 2,	 1 },	//  64	a14	2, 3
	{ 2,	12 },	//  77	a15	2, 4
	{ 2,	27 },	//  90	a16	2, 5
	{ 3,	 6 },	// 103	a17	2, 6

	{ 0,	19 },	//  18	a18	3, 0
	{ 0,	30 },	//  31	a19	3, 1
	{ 1,	13 },	//  44	a20	3, 2
	{ 1,	24 },	//  57	a21	3, 3
	{ 2,	 7 },	//  70	a22	3, 4
	{ 2,	18 },	//  83	a23	3, 5
	{ 3,	 1 },	//  96	a24	3, 6
	{ 3,	12 },	// 109	a25	3, 7

	{ 0,	13 },	//  12	a26	4, 0
	{ 0,	25 },	//  24	a27	4, 1
	{ 1,	 4 },	//  37	a28	4, 2
	{ 1,	19 },	//  50	a29	4, 3
	{ 1,	30 },	//  63	a30	4, 4
	{ 2,	13 },	//  76	a31	4, 5
	{ 2,	24 },	//  89	a32	4, 6
	{ 3,	 7 },	// 102	a33	4, 7
	{ 3,	19 },	// 114	a34	4, 8

	{ 0,	16 },	//  17	a35	5, 0
	{ 0,	31 },	//  30	a36	5, 1
	{ 1,	10 },	//  43	a37	5, 2
	{ 1,	25 },	//  56	a38	5, 3
	{ 2,	 4 },	//  69	a39	5, 4
	{ 2,	19 },	//  82	a40	5, 5
	{ 2,	30 },	//  95	a41	5, 6
	{ 3,	13 },	// 108	a42	5, 7

	{ 0,	22 },	//  23	a43	6, 0
	{ 1,	 5 },	//  36	a44	6, 1
	{ 1,	16 },	//  49	a45	6, 2
	{ 1,	31 },	//  62	a46	6, 3
	{ 2,	10 },	//  75	a47	6, 4
	{ 2,	25 },	//  88	a48	6, 5
	{ 3,	 4 },	// 101	a49	6, 6

	{ 0,	28 },	//  29	a50	7, 0
	{ 1,	11 },	//  42	a51	7, 1
	{ 1,	22 },	//  55	a52	7, 2
	{ 2,	 5 },	//  68	a53	7, 3
	{ 2,	16 },	//  81	a54	7, 4
	{ 2,	31 },	//  94	a55	7, 5

	{ 1,	 2 },	//  35	a56	8, 0
	{ 1,	17 },	//  48	a57	8, 1
	{ 1,	28 },	//  61	a58	8, 2
	{ 2,	11 },	//  74	a59	8, 3
	{ 2,	22 }	//  87	a60	8, 4
};


// dacTTMap[] is a mapping for the tip/tilt system.
// IMPORTANT: the X's and Y's in the following, and in the rest of the code, correspond to the left-right, up-down
// > directions, as viewd on the video monitor, and to the cable labeling.  However, the X's and Y's, as designated
// > for the optics, are rotated 90 degrees (X is perpendicular to the optical table (up-down as it's mounted on the 
// > telescope) and Y is into and out of the optical table (left-right as it's mounted on the telescope)).
DACMapType dacTTMap[eTTNumVolts] =
{
	{ 4,	 1 },	// 128	TT mirror, X
	{ 4,	 0 },	// 129	TT mirror, Y
	{ 4,	 3 },	// 130	Telco offload, X
	{ 4,	 2 },	// 131	Telco offload, Y
	{ 4,	 5 },	// 132	Laser steering, X
	{ 4,	 4 },	// 133	Laser steering, Y
	{ 4,	 7 },	// 134		
	{ 4,	 6 }	// 135	Telco offload enable
};
