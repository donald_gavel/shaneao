/**********************************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		initcamTest

AUTHOR:	DHL & DWP	DATE: 10/7/04

COMPONENT DESCRIPTION:
This temporary component tests the EDT initcam() function.

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <syslog.h>  //XXXTRACER2
//#include <rtl_fifo.h>
//#include <rtl_time.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"
#include "psfest.h"
#include "edtinc.h"


// Defines

#define ACK 6  // acknowledge from the SciMeasure Camera

// Local Variables

static int fd;					  // mmap file descriptor for the WFS shared memory
static unsigned char *wfsCamBuf;  // pointer for WFS shared memory buffer
static PdvDev *wfs_pdv_p;		  // pointer for the WFS PDV/EDT device

//XXX static u_int totalBufSize;	// the amount of shared memory allocated
static off_t totalBufSize;	// the amount of shared memory allocated

static Dependent *dd_p;
static Edtinfo edtinfo;

//XXX static const WFSCamOffsets[4] = { 0, 0, 0, 0 };  //XXXNOISE
static const WFSCamOffsets[4] = { 350, 350, 350, 350 };  //XXXNOISE


// Local Function Prototypes

static int CamWFSMngr_RepCount (int Frame_Rate);
static u_int CamWFSMngr_TotalBufferSize (int16 DataSize);
static void CamWFSMngr_SendToCam (char outString[], char inString[]);


/**********************************************************************************************

UNIT NAME:  main()

UNIT DESCRIPTION:
Mainline for the initcam() test.  

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

**********************************************************************************************/

int main(void)
{
	int16 Gain;
	int retVal, Count, Atten;
	float FrameRate;
	char textBuf[32], inString[64];

	syslog(LOG_ERR | LOG_USER, "Init0\n");  //XXXTRACER2

	CamWFSMngr_StartCam();
	syslog(LOG_ERR | LOG_USER, "Init1\n");  //XXXTRACER2

	if (wfs_pdv_p != NULL)
	{
		pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", dd_p, &edtinfo);  // read the config file
		syslog(LOG_ERR | LOG_USER, "Init2\n");  //XXXTRACER2

		pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", NULL, 0);  // init the cam
		syslog(LOG_ERR | LOG_USER, "Init3\n");  //XXXTRACER2

		syslog(LOG_ERR | LOG_USER, "prePMB 1\n");  //XXX060501
		pdv_multibuf_block(wfs_pdv_p, WFSnumbufs, wfsCamBuf, totalBufSize);			   // set the DMA ring buffer sizes
		syslog(LOG_ERR | LOG_USER, "postPMB 1\n");  //XXX060501

		pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", dd_p, &edtinfo);  // read the config file
		syslog(LOG_ERR | LOG_USER, "Init4\n");  //XXXTRACER2

		pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", NULL, 0);  // init the cam
		syslog(LOG_ERR | LOG_USER, "Init5\n");  //XXXTRACER2

		pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", dd_p, &edtinfo);  // read the config file
		syslog(LOG_ERR | LOG_USER, "Init6\n");  //XXXTRACER2

		pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-64x64CL.cfg", NULL, 0);  // init the cam
		syslog(LOG_ERR | LOG_USER, "Init7\n");  //XXXTRACER2
	}

	CamWFSMngr_StopCam();
	syslog(LOG_ERR | LOG_USER, "Init8\n");  //XXXTRACER2
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_StartCam()

UNIT DESCRIPTION:
The purpose of this unit is to initialize the EDT driver and set up the shared memory for the 
DMA ring buffers.  The memory size is provided from the HRT in cam_WFS.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

**********************************************************************************************/

void CamWFSMngr_StartCam(void)
{
	char devname[128];

	totalBufSize = CamWFSMngr_TotalBufferSize (WFSbufsize);

/*/XXX	if ((fd = open("/dev/WFS_rt_edt_mem", O_RDWR)) < 0)
	if ((fd = open("/dev/WFS_rt_edt_mem", O_CREAT | O_RDWR)) < 0)
	{
		printf("Error: couldn't open shared memory in CamWFS: %d\n", fd);
		return;
	} */

/*
//	if ((fd = shm_open("/dev/WFS_rt_edt_mem", O_CREAT | O_RDWR, 0600)) < 0)
	if ((fd = shm_open("/WFS_rt_edt_mem", O_CREAT | O_RDWR, 0600)) < 0)
	{
		printf("Error: couldn't open shared memory in CamWFS: %d\n", errno);
		return;
	}

	if (ftruncate(fd, totalBufSize) < 0)
	{
		printf("Error sizing WFS shared mem, errno = %d, %d, %d\n", errno, fd, totalBufSize);
		return;
	}

	wfsCamBuf = mmap(0, totalBufSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
	if (wfsCamBuf == MAP_FAILED)
	{
		printf ("Error: couldn't map shared memory in CamWFS_Mngr: %d\n", errno);
		return;
	}
*/

	wfsCamBuf = malloc(totalBufSize);

	memset(wfsCamBuf, 0x11, totalBufSize);
	printf("In camWFS, allocated %d bytes for %d buffers\n", totalBufSize, WFSnumbufs);

//	open the edt device (EDT_INTERFACE is defined in edtdef.h (included via edtinc.h))
	strcpy(devname, EDT_INTERFACE);
	printf ("Opening camWFS EDTpdv device: %s, unit: %d\n", devname, WFS_UNIT_NUM);
    if ((wfs_pdv_p = pdv_open(devname, WFS_UNIT_NUM)) == NULL)
    {
		printf("Error: pdv_open(%s,%d) failed \n", devname, WFS_UNIT_NUM);
		return;
    }

	dd_p = pdv_alloc_dependent();
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_StopCam()

UNIT DESCRIPTION:
The purpose of this unit is to close the EDT driver and unmap the shared memory for the 
DMA ring buffers.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

**********************************************************************************************/

void CamWFSMngr_StopCam(void)
{
	if (wfs_pdv_p != NULL)
	{
		CamWFSMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
//XXX		pdv_start_images(wfs_pdv_p, 1);			// stop the free-run acquistion
	    pdv_close(wfs_pdv_p);
	}

    free(wfsCamBuf);

/*
    munmap(wfsCamBuf, totalBufSize);
	shm_unlink("/WFS_rt_edt_mem");
    close(fd);
*/
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_RepCount(int Frame_Rate)

UNIT DESCRIPTION:
The purpose of this unit is to compute a repetition count that yields the desired frame rate.
The values in the case statement assume a base frame rate of 1000 fps where each additional
repetition adds 20 microseconds to the frame time.  Currently program 3 has a 1000 fps base
rate but if a different program is selected in the future, these values may have to be adjusted.
The rate verses rep count for any program can be determined empirically by running the Little Joe 
and PdvShow utilities.  Adjust the rep count in Little Joe and read the frame rate in PdvShow.

UNIT INPUTS:
1.	Frame_Rate - in frames per second

UNIT OUTPUTS:
1.	the repetition count

UNIT ERROR CODES:
	None

**********************************************************************************************/
static int CamWFSMngr_RepCount (int Frame_Rate)
{
	int Count = 0;

	switch (Frame_Rate)
	{
		case 50:
			Count = 950;
			break;
		case 100:
			Count = 450;
			break;
		case 200:
			Count = 200;
			break;
		case 250:
			Count = 150;
			break;
		case 350:
			Count = 93;	// actual rate is 349.65
			break;
		case 500:
			Count = 50;
			break;
		case 750:
			Count = 17;	// actual rate is 746.27
			break;
		case 1000:
			Count = 0;
			break;
		default:
			// this should never happen since the frame rate is constrained in the GUI
			HostDiagSendStrngIntParm("Error: illegal Frame Rate in cam_WFS_Mngr = ", Frame_Rate);
			break;
	}
	return Count;
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_TotalBufferSize()

UNIT DESCRIPTION:
The purpose of this unit is to calculate the total DMA buffer size - rounding up to page size and 
making sure there's room for deinterleave if needed

UNIT INPUTS:
	Data size

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static u_int CamWFSMngr_TotalBufferSize (int16 DataSize)
{
	int npages;
	u_int bufsize = DataSize;
	u_int totalsize;

	npages = DataSize / PAGE_SIZE;

	if (DataSize % PAGE_SIZE)
	{
		npages++;
		bufsize = npages * PAGE_SIZE;
	}

	totalsize = bufsize * WFSnumbufs;
	return totalsize;
}


/**********************************************************************************************

UNIT NAME:  CamWFSMngr_SendToCam()

UNIT DESCRIPTION:
The purpose of this unit is to send an ASCII command to the camera and read the response.

UNIT INPUTS:
1.	outString - the command string
2.	inString - a pointer to an array to put the camera response into, or NULL

UNIT OUTPUTS:
1.	inString - the camera response 

**********************************************************************************************/

static void CamWFSMngr_SendToCam (char outString[], char inString[])
{
/*    int  retVal, inCount;
	char localInString[64], errMsg[64];

	struct timespec next;

//	don't bother doing anything, if there's no camera
	if (wfs_pdv_p == NULL)
		return;

//	printf ("Sending to WFS camera: %s\n", outString);  //XXX

//  indicate that interrrupt should be ignored (because we're doing serial comms)  XXXINTIG
	CamDataPtr->intrIgnore = true;  //XXXINTIG

//	make sure the string to be returned is null terminated
	if (inString != NULL)
		inString[0] = '\0';

	next.tv_sec = 0;
	next.tv_nsec = 100000;  // was 1000
	nanosleep(&next, NULL);

//	flush any junk from the read buffer and issue command to camera
	pdv_serial_read(wfs_pdv_p, localInString, 32);
	next.tv_sec = 0;
	next.tv_nsec = 1000;
	nanosleep(&next, NULL);

	retVal = pdv_serial_command(wfs_pdv_p, outString);
	next.tv_sec = 0;
	next.tv_nsec = 1000;
	nanosleep(&next, NULL);

//	if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
	if (retVal != 0)
	{
		HostDiagSendStrngIntParm("Error from pdv_serial_command() in CamWFS: ", retVal);
		printf ("Error from pdv_serial_command() in CamWFS: %d\n", retVal);  //XXX
	}
	else
	{
		if (inString == NULL)
		{
			inCount = pdv_serial_wait(wfs_pdv_p, 500, 1);
			next.tv_sec = 0;
			next.tv_nsec = 300000000;  //XXX new
			nanosleep(&next, NULL);
		}
		else
		{
			inCount = pdv_serial_wait(wfs_pdv_p, 500, sizeof(localInString) - 1);
			next.tv_sec = 0;
			next.tv_nsec = 1000;
			nanosleep(&next, NULL);
		}

		if (inCount == 0)
		{
			HostDiagSendStrngNoParm("Error: no response from CamWFS 1");
			printf ("Error: no response from CamWFS 1\n");  //XXX
		}
		else
		{
			retVal = pdv_serial_read(wfs_pdv_p, localInString, inCount);

			if (retVal == 0)
			{
				HostDiagSendStrngNoParm("Error: no response from CamWFS 2");
				printf ("Error: no response from CamWFS 2\n");  //XXX
			}
			else if (retVal != inCount)
			{
				HostDiagSendStrngIntParm("Error from pdv_serial_read() in CamWFS: ", retVal);
				printf ("Error from pdv_serial_read() in CamWFS: %d\n", retVal);  //XXX
			}
			else if (localInString[0] != ACK)
			{
				HostDiagSendStrngNoParm("Error: no ACK from CamWFS");
				printf ("Error: no ACK from CamWFS\n");  //XXX
			}
			else if (retVal > 2)
			{
				localInString[retVal - 2] = '\0';
				sprintf(errMsg, "Notification: message from CamWFS: %s", &localInString[1]);
				HostDiagSendStrngNoParm(errMsg);
				printf ("%s\n", errMsg);  //XXX

				if (inString != NULL)
					strcpy(inString, localInString);
			}
		}
	}

//	indicate that interrupts should no longer be ignored
	CamDataPtr->intrIgnore = false;  // XXXINTIG
*/
}
