/**********************************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****
  
MODULE NAME:		sysCon

AUTHOR:	DWP			DATE: 1/6/03

COMPONENT DESCRIPTION:
This component is the system controller for the hard realtime processor for the LickAO control 
system.  It performs the RTLinux module initialization and cleanup, semaphore initialization, 
and provides the task that waits for commands from the soft realtime processor (that communicates 
with the host) and dispatches them to other components as appropriate.  Virtually all system 
initialization is done here to simplify cleanup.

The SysCon thread is also in this component.  Its purpose is to wait on the command FIFO from 
the Soft Real Time (SRT) side and dispatch commands as they are received.

COMPONENT UNITS:
1.	init_module()
2.	cleanup_module()
3.	SysConGlobalsInit()
4.	SysConGlobalsCleanup()
5.	SysCon()
6.	SysConSendStrngNoParm()
7.	SysConSendStrngIntParm()
8.	SysConSendStrngFltParm()
9.	SysConTimeDiff()

REVISIONS:

**********************************************************************************************/

#include <rtl.h>
#include <rtl_fifo.h>
#include <rtl_serial.h>
#include <rtl_fcntl.h>
#include <rtl_posixio.h>
#include <rtl_unistd.h>
#include <rtl_pthread.h>
#include <rtl_semaphore.h>
#include <rtl_time.h>
#include <rtl_math.h>
#include <sys/rtl_mman.h>
#include <linux/shm.h>
#include <rtl_termios.h>

#include "types.h"
#include "constant.h"
#include "camDrvrL.h"
#include "camDrvr.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "sysCon.h"
#include "hostICon.h"
#include "hostDiag.h"

#include "edt_lnx_kernel.h"


RTLINUX_MODULE(AOControlHard);
MODULE_AUTHOR("Lawrence Livermore National Laboratory");
MODULE_DESCRIPTION("LickAO HRT, Rev. 1.00");
MODULE_LICENSE("Proprietary: Send bug reports to palmer25@llnl.gov");
MODULE_SUPPORTED_DEVICE("LickAO");


// Global Variables

boolean OkayToStartCamData = false;  // flag indicating that it is okay to start camera data
boolean OkayToFinishInit = false;  // flag indicating that it is okay for threads to complete their initializations

rtl_sem_t CamSem;		// semaphore for centroid thread
rtl_sem_t WFSSem;		// semaphore for SciMeasure camera thread
rtl_sem_t LTTSem;		// semaphore for LTT camera thread
rtl_sem_t CentSem;		// semaphore for centroid thread
rtl_sem_t ReconSem;		// semaphore for recon thread

int DiagFIFOfd;  // fd for FIFO used to send messages to the diagnostic data handler
int CmdFIFOfd;   // fd for FIFO used to receive command messages from the host interface

int CamSerialfd;  // fd for camera serial port

FileDataShmType *FileDataShmPtr;  // pointer to file data shared memory
CamDataType *CamDataPtr;		  // pointer to camera shared memory data
LTTCamDataType *CamLTTPtr;		  // pointer to Laser Tip Tilt camera shared memory data
CentDataType *CentDataPtr;		  // pointer to centroid shared memory data
ReconDataType *ReconDataPtr;	  // pointer to recon shared memory data

boolean TiptTiltAPDInitDone = false;
boolean CamInitDone = false;
boolean CamLTTInitDone = false;
boolean CamWFSInitDone = false;
boolean CentInitDone = false;
boolean ReconInitDone = false;  // initalization flags for threads

// struct timespec TimeCamDrvr1, TimeCamDrvr2, TimeCamDrvr3, TimeCamDrvr4;  //TTT
// struct timespec TimeCam1, TimeCam2, TimeCent1, TimeCent2, TimeRecon1, TimeRecon2;  //TTT

#include "cmdTable.h"  // the command table is in a separate file so that both the HRT and SRT programs can include it


// Local Variables

static rtl_pthread_t camLTTWatchThread;
static rtl_pthread_t camWFSWatchThread;
static rtl_pthread_t camWatchThread;
static rtl_pthread_t camSerialThread;
static rtl_pthread_t tipTiltAPDThread;
static rtl_pthread_t cameraThread;
static rtl_pthread_t camera_WFS_Thread;
static rtl_pthread_t camera_LTT_Thread;
static rtl_pthread_t centroidThread;
static rtl_pthread_t reconThread;
static rtl_pthread_t sysConThread;  // thread handles

static int mmfd;					// mmap file descriptor
static int totalSize;				// total size of shared memory
static unsigned char *startPtr;		// pointer to start of shared memory
static int LTT_fd;					// Laser Tip-Tilt camera shared memory file descriptor
static int WFS_fd;					// WFS centroid camera shared memory file descriptor

static char cmFilenameSave[MAX_FILENAME_LENGTH] = "";

// Local Function Prototypes

static int SysConGlobalsInit(void);
static void SysConGlobalsCleanup(void);
static void *SysCon( void *arg );


/**********************************************************************************************

UNIT NAME:  init_module()

UNIT DESCRIPTION:
The purpose of this unit is to perform RTLinux initialization for the AOControlHard module.  
Threads and semaphores are created.

UNIT INPUTS:
	None

UNIT OUTPUTS:
1.	return value	- see error codes below

UNIT ERROR CODES:
1.	values as set by system calls

**********************************************************************************************/

int init_module(void)
{
	int retVal;
	rtl_pthread_attr_t attr;
    struct rtl_sched_param param;
	struct rtl_termios term;

	rtl_printf("<0>Starting hard real-time process\n");  // can't send this message to the host!

//	initialize pointers to global/shared variables
	if ((retVal = SysConGlobalsInit()) < 0)
		return retVal;  // error message has already been displayed

//	initalize the file shared memory variables
	FileDataInit();

//	init the srtIsUpFlag in shared memory in preparation for spinning on it
	FileDataShmPtr->srtIsUpFlag = false;

//	open FIFOs for HRT/SRT communication, returning errno if an error occurs (the SRT will spin on the command FIFO
//	> until it is opened)
	if ((DiagFIFOfd = rtl_open("/dev/rtf0", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_WRONLY)) < 0)
	{
		rtl_printf("Error creating diagnostic output FIFO, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}
	if ((CmdFIFOfd = rtl_open("/dev/rtf1", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY)) < 0)
	{
		SysConSendStrngIntParm("Error creating command input FIFO, errno = ", rtl_errno);
		return rtl_errno;
	}

//	create semaphores, returning errno if an error occurs
	if ((rtl_sem_init(&CamSem, 0, 0) < 0) ||
		(rtl_sem_init(&WFSSem, 0, 0) < 0) ||
		(rtl_sem_init(&LTTSem, 0, 0) < 0) ||
		(rtl_sem_init(&CentSem, 0, 0) < 0) ||
		(rtl_sem_init(&ReconSem, 0, 0) < 0))
	{
		SysConSendStrngIntParm("Error creating semaphores, errno = ", rtl_errno);
		return rtl_errno;
	}

//  open and set up camera serial port
	if (UseAOA != 0)
	{
		if ((CamSerialfd = rtl_open("/dev/ttyS0", RTL_O_NONBLOCK)) < 0)
		{
			rtl_printf ("<0>Error opening camera serial port, errno = %d\n", rtl_errno);
			SysConSendStrngIntParm("Error opening camera serial port, errno = ", rtl_errno);
			return rtl_errno;
	    }
	}

//	(the serial port initialization that used to have to be done here doesn't have to be done any more;
//	> see code 050511 and before for the initialization)

//	create threads, returning the value returned by pthread_create() if an error occurs
//	> (note that, in the following, higher numbers mean higher priorities)
	if (UseAOA != 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
//		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 4;  //XXX060228; was 30
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camWatchThread, &attr, CamDrvrWatch, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating camWatch thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("CamWatch thread priority is: ", param.sched_priority);
	}
	else
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
//		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 3;  //XXX060228; was 30
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camWFSWatchThread, &attr, CamWFS_DrvrWatch, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating camWFSWatch thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("CamWFSWatch thread priority is: ", param.sched_priority);
	}


	if (UseAPD == 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
//		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 2;  //XXX060228; was 29
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camLTTWatchThread, &attr, CamLTT_DrvrWatch, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating camLTTWatch thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("CamLTTWatch thread priority is: ", param.sched_priority);
	}


	if (UseAOA != 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 25;
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camSerialThread, &attr, CamSerial, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating camSerRead thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("CamSerRead thread priority is: ", param.sched_priority);
	}


	if (UseAPD != 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
		rtl_pthread_attr_setfp_np(&attr, 1);
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 13;  //XXX060228; was 20
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&tipTiltAPDThread, &attr, TipTiltAPD, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating tipTiltAPD thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("TipTiltAPD thread priority is: ", param.sched_priority);
	}


	if (UseAOA != 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 14;  // changed from 16
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&cameraThread, &attr, Cam, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating camera thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("Camera thread priority is: ", param.sched_priority);
	}
	else
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 16;  //060314, changed from 15
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camera_WFS_Thread, &attr, CamWFS, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating WFS camera thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("WFS camera thread priority is: ", param.sched_priority);
	}


	if (UseAPD == 0)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
		rtl_pthread_attr_setfp_np(&attr, 1);  //XXXnew
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 15;  //050328, changed from 15, then 14; 060314, changed from 16
		rtl_pthread_attr_setschedparam(&attr, &param);
		if ((retVal = rtl_pthread_create(&camera_LTT_Thread, &attr, CamLTT, 0)) != 0)
		{
			SysConSendStrngIntParm("Error creating LTT camera thread, retVal = ", retVal);
			return retVal;
		}
		rtl_pthread_attr_getschedparam(&attr, &param);
		SysConSendStrngIntParm("LTT camera thread priority is: ", param.sched_priority);
	}


	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
	rtl_pthread_attr_setfp_np(&attr, 1);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 10;
	rtl_pthread_attr_setschedparam(&attr, &param);
	if ((retVal = rtl_pthread_create(&centroidThread, &attr, Cent, 0)) != 0)
	{
		SysConSendStrngIntParm("Error creating centroid thread, retVal = ", retVal);
		return retVal;
	}
	rtl_pthread_attr_getschedparam(&attr, &param);
	SysConSendStrngIntParm("Centroid thread priority is: ", param.sched_priority);


	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
	rtl_pthread_attr_setfp_np(&attr, 1);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 5;
	rtl_pthread_attr_setschedparam(&attr, &param);
	if ((retVal = rtl_pthread_create(&reconThread, &attr, Recon, 0)) != 0)
	{
		SysConSendStrngIntParm("Error creating recon thread, retVal = ", retVal);
		return retVal;
	}
	rtl_pthread_attr_getschedparam(&attr, &param);
	SysConSendStrngIntParm("Recon thread priority is: ", param.sched_priority);


	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_setcpu_np(&attr, HRT_CPU);
	rtl_pthread_attr_setfp_np(&attr, 1);
	rtl_pthread_attr_setreserve_np(&attr, 1);  // only one task can reserve, so, this should be applied last
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 1;
	rtl_pthread_attr_setschedparam(&attr, &param);
	if ((retVal = rtl_pthread_create(&sysConThread, &attr, SysCon, 0)) != 0)
	{
		SysConSendStrngIntParm("Error creating HRT system controller thread, retVal = ", retVal);
		return retVal;
	}
	rtl_pthread_attr_getschedparam(&attr, &param);
	SysConSendStrngIntParm("SysCon thread priority is: ", param.sched_priority);

//	initialize the camera driver and start data
	OkayToStartCamData = false;
	if (UseAOA != 0)
	{
		if ((retVal = CamDrvrInit()) < 0)
		{
			SysConSendStrngIntParm("Error initializing camera driver, retVal = ", retVal);
			return retVal;
		}
		else
			OkayToStartCamData = true;
	}

//	get the IRQ# for WFS from the EDT driver and install a handler for it
	if (UseAOA == 0)
		CamWFS_InitIntr();

//	get the IRQ# for LTT from the EDT driver and install a handler for it
	if (UseAPD == 0)
		CamLTT_InitIntr();

	return 0;
}


/**********************************************************************************************

UNIT NAME:  cleanup_module()

UNIT DESCRIPTION:
The purpose of this unit is to perform RTLinux cleanup for the AOControlHard module.  
Threads and semaphores are destroyed.

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void cleanup_module(void)
{
	int tmpInt;

	rtl_printf("Cleaning up hard real-time process\n");  // probably can't send this message to the host!

//	stop camera data and clean up the driver

	if (UseAOA != 0)
	{
		rtl_close(CamSerialfd);
		CamDrvrStopData();
		CamDrvrCleanup();
	}
	
//	cancel and join threads
	if (UseAOA != 0)
	{
		rtl_pthread_cancel(camWatchThread);
		rtl_pthread_join(camWatchThread, NULL);
	}
	else
	{
		rtl_pthread_cancel(camWFSWatchThread);
		rtl_pthread_join(camWFSWatchThread, NULL);
	}

	rtl_pthread_cancel(camLTTWatchThread);
	rtl_pthread_join(camLTTWatchThread, NULL);

	if (UseAOA != 0)
	{
		rtl_pthread_cancel(camSerialThread);
		rtl_pthread_join(camSerialThread, NULL);
	}

	rtl_pthread_cancel(tipTiltAPDThread);
	rtl_pthread_join(tipTiltAPDThread, NULL);

	if (UseAOA != 0)
	{
		rtl_pthread_cancel(cameraThread);
		rtl_pthread_join(cameraThread, NULL);
	}
	else
	{
		rtl_pthread_cancel(camera_WFS_Thread);
		rtl_pthread_join(camera_WFS_Thread, NULL);
	}

	rtl_pthread_cancel(camera_LTT_Thread);
	rtl_pthread_join(camera_LTT_Thread, NULL);

	rtl_pthread_cancel(centroidThread);
	rtl_pthread_join(centroidThread, NULL);

	rtl_pthread_cancel(reconThread);
	rtl_pthread_join(reconThread, NULL);

	rtl_pthread_cancel(sysConThread);
	rtl_pthread_join(sysConThread, NULL);

//	destroy semaphores
	rtl_sem_destroy(&CamSem);
	rtl_sem_destroy(&WFSSem);
	rtl_sem_destroy(&LTTSem);
	rtl_sem_destroy(&CentSem);
	rtl_sem_destroy(&ReconSem);

//	close FIFOs
	rtl_close(DiagFIFOfd);
	rtl_close(CmdFIFOfd);

//	free up the two EDT irqs
	if (WFS_irq >= 0)
	{
//XXX060501	    tmpInt = edt_restore_os_interrupt(WFS_irq, WFS_dev);
		rtl_free_irq (WFS_irq);
	}

	if (LTT_irq >= 0)
	{
//XXX060501	    tmpInt = edt_restore_os_interrupt(LTT_irq, LTT_dev);
		rtl_free_irq (LTT_irq);
	}

//	clean up shared memory
	SysConGlobalsCleanup();
}


/**********************************************************************************************

UNIT NAME:  SysConGlobalsInit()

UNIT DESCRIPTION:
This unit sets up the mmap for and initializes pointers to global/shared variables.  

Pointers to virtually all global memory variables are set up here so that they can all be 
part of the same mmap region to facilitate sharing the variables between several threads on 
RTLinux and Linux on multiple processors.  Global memory is used in this application so that 
large chunks of data will not have to be moved around (in most cases, data ia put in memory 
once and never moved again).

IMPORTANT: pointers to variables are set up here, but the variables are NOT initialized.  That
is up to the code that is responsible for the data.  GlobalsInit() must be called before any
tasks are initialized.

UNIT INPUTS:
	None

UNIT OUTPUTS:
1.	return value	- see error codes below

UNIT ERROR CODES:
1.	values as set by system calls

**********************************************************************************************/

static int SysConGlobalsInit(void)
{
	int shmDataSize, camDataSize, centDataSize, reconDataSize, lttDataSize;
	int npages;
	int bufsize;

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	lttDataSize = sizeof(LTTCamDataType);
	centDataSize = sizeof(CentDataType);
	reconDataSize = sizeof(ReconDataType);
	totalSize = shmDataSize + camDataSize + centDataSize + reconDataSize + lttDataSize;

//	create the shared memory region, returning errno if an error occurs (passing a non-zero 
//	> value for the mode means a node in Linux is also created (as opposed to just RTLinux))
	if ((mmfd = rtl_shm_open("/dev/LickAO_mmap", RTL_O_CREAT, 0600)) < 0)
	{
		rtl_printf("Error creating shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	size the shared memory region, returning errno if an error occurs
	if (rtl_ftruncate(mmfd, totalSize) < 0)
	{
		rtl_printf("Error sizing shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	get the pointer to the shared memory region, returning errno if an error occurs
	if ((startPtr = rtl_mmap(0, totalSize, RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, mmfd, 0)) == RTL_MAP_FAILED)
	{
		rtl_printf("Error getting pointer to shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	create the shared memory regions for two SciMeasure Little Joe cameras
//	
//	The shared memory region for the Laser Tip Tilt (LTT) Camera has to accomodate two different data sizes so we're going to 
//	allocate for the larger size and use the region for both cases.
	npages = LTTOpenbufsize / PAGE_SIZE;
	if (LTTOpenbufsize % PAGE_SIZE)
		npages++;
	bufsize = npages * PAGE_SIZE;
	LttTotalSize = bufsize * LTTnumbufs;

	rtl_printf ("<0>sysCon LTT shared memory size %d\n", LttTotalSize);  //XXX
//XXX	rtl_printf ("<0> %d  %d  %d  %d  %d\n", npages, LTTOpenbufsize, PAGE_SIZE, bufsize, LTTnumbufs);  //XXX

	if ((LTT_fd = rtl_shm_open("/dev/LTT_rt_edt_mem", RTL_O_CREAT, 0600)) < 0)
	{
		rtl_printf("Error creating LTT shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	size the shared memory region, returning errno if an error occurs
	if (rtl_ftruncate(LTT_fd, LttTotalSize) < 0)
	{
		rtl_printf("Error sizing LTT shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	get the pointer to the shared memory region, returning errno if an error occurs
	if ((LttBaseAddr = rtl_mmap(0, LttTotalSize, RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, LTT_fd, 0)) == RTL_MAP_FAILED)
	{
		rtl_printf("Error getting pointer to LTT shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	create the shared memory region for the SciMeasure WFS camera
	npages = WFSbufsize64x64 / PAGE_SIZE;  // IMPORTANT: pdv_multibuf_block() can't seem to handle 12k buffers; so, we're allocating for 16k
	if (WFSbufsize64x64 % PAGE_SIZE)
		npages++;
	bufsize = npages * PAGE_SIZE;
	WfsTotalSize = bufsize * WFSnumbufs;

	rtl_printf ("<0>sysCon WFS shared memory size %d\n", WfsTotalSize);  //XXX
//XXX	rtl_printf ("<0> %d  %d  %d  %d  %d\n", npages, WFSbufsize, PAGE_SIZE, bufsize, WFSnumbufs);  //XXX

	if ((WFS_fd = rtl_shm_open("/dev/WFS_rt_edt_mem", RTL_O_CREAT, 0600)) < 0)
	{
		rtl_printf("Error creating WFS shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	size the shared memory region, returning errno if an error occurs
	if (rtl_ftruncate(WFS_fd, WfsTotalSize) < 0)
	{
		rtl_printf("Error sizing WFS shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	get the pointer to the shared memory region, returning errno if an error occurs
	if ((WfsBaseAddr = rtl_mmap(0, WfsTotalSize, RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, WFS_fd, 0)) == RTL_MAP_FAILED)
	{
		rtl_printf("Error getting pointer to WFS shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

//	set up the pointers to the global variables
	FileDataShmPtr = (FileDataShmType *)startPtr;
	CamDataPtr = (CamDataType *)((char *)FileDataShmPtr + shmDataSize);
	CentDataPtr = (CentDataType *)((char *)CamDataPtr + camDataSize);
	ReconDataPtr = (ReconDataType *)((char *)CentDataPtr + centDataSize);
	CamLTTPtr = (LTTCamDataType *)((char *)ReconDataPtr + reconDataSize);
	return 0;
}


/**********************************************************************************************

UNIT NAME:  SysConGlobalsCleanup()

UNIT DESCRIPTION:
This unit cleans up the mmap for the global/shared variables

UNIT INPUTS:
	None

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void SysConGlobalsCleanup(void)
{
//	unmap, close, and destroy the global memory regions
	rtl_munmap(startPtr, totalSize);
	rtl_close(mmfd);
	rtl_shm_unlink("/dev/LickAO_mmap");

	rtl_munmap(LttBaseAddr, LttTotalSize);
	rtl_close(LTT_fd);
	rtl_shm_unlink("/dev/LTT_rt_edt_mem");

	rtl_munmap(WfsBaseAddr, WfsTotalSize);
	rtl_close(WFS_fd);
	rtl_shm_unlink("/dev/WFS_rt_edt_mem");
}


/**********************************************************************************************

UNIT NAME:  SysCon()

UNIT DESCRIPTION:
This unit is the system controller thread mainline (on the Hard Real Time (HRT) side).  Its
purpose is to wait on the command FIFO from the Soft Real Time (SRT) side and dispatch commands
as they are received.

IMPORTANT:  RTLinux does not support blocking FIFOs.  So, at least for now, the FIFO is polled
            twice times per second.

UNIT INPUTS:
1.	arg				- needed for pthread_create(), not currently used

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

static void *SysCon( void *arg )
{
	int retVal;
    int16 i;
    int16 cmdAction;
	struct rtl_timespec next;
	cmdFIFOMsgType cmdFIFOMsg;

	static int16 sysConCmdCrnt = eCmdNone;

//	spin on the srtIsUpFlag in shared memory until the SRT is up
	while (!FileDataShmPtr->srtIsUpFlag)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in SysCon(), retVal = ", retVal);
    }

//	command the SRT to read all file data, spin until it's received into shared memory, and then move it 
//	> into program variables
	FileDataCmd(eFileReadAllData);
	while (!FileDataShmPtr->allDataAvailFromFile)
    {
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in SysCon() 1, retVal = ", retVal);
    }
	if (!FileDataGet(eFileTypeParms))
		SysConSendStrngNoParm("Error reading parms data at startup, all defaults being used");
    else
    {
        if (!FileDataGet(eFileTypeCflat))
            SysConSendStrngNoParm("Error reading cflat data at startup, defaults being used");
        if (!FileDataGet(eFileTypeRefCentB))
            SysConSendStrngNoParm("Error reading refcentB data at startup, defaults being used");
        if (!FileDataGet(eFileTypeRefCentC))
            SysConSendStrngNoParm("Error reading refcentC data at startup, defaults being used");
        if (!FileDataGet(eFileTypeRefCentQ))
            SysConSendStrngNoParm("Error reading refcentQ data at startup, defaults being used");
        if (!FileDataGet(eFileTypeRefCentO))
            SysConSendStrngNoParm("Error reading refcentO data at startup, defaults being used");
        if (!FileDataGet(eFileTypeCentOff))
            SysConSendStrngNoParm("Error reading centoff data at startup, defaults being used");
        if (!FileDataGet(eFileTypeCM))
            SysConSendStrngNoParm("Error reading cm data at startup, defaults being used");
        if (!FileDataGet(eFileTypeFlat))
            SysConSendStrngNoParm("Error reading flat data at startup, defaults being used");
        if (!FileDataGet(eFileTypeSharp))
            SysConSendStrngNoParm("Error reading sharp data at startup, defaults being used");
        if (!FileDataGet(eFileTypeLTTOpen))
            SysConSendStrngNoParm("Error reading LTT open background data at startup, defaults being used");
        if (!FileDataGet(eFileTypeLTTClosed))
            SysConSendStrngNoParm("Error reading LTT closed background data at startup, defaults being used");
    }
    FileDataShmPtr->cmDataFileError = false;
	FileDataShmPtr->someDataAvailFromFile = false;
	FileDataShmPtr->allDataAvailFromFile = false;

//	alert other threads that it's okay to finish their initializations and wait for them to do it
	OkayToFinishInit = true;
    while (!TiptTiltAPDInitDone || !CamInitDone || !CentInitDone || !ReconInitDone || !CamLTTInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm("Error from nanosleep() in SysCon() 2, retVal = ", retVal);
    }

//  if it's okay to start the camera (i.e., it initialized properly)
//      start the camera data (this begins normal operation)
	if (OkayToStartCamData)
		CamDrvrStartData();  // CamSimTst

//	enter an infinite loop
	while (1)
	{
//      pause for half a second
		next.tv_sec = 0;
		next.tv_nsec = 500000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm("Error from nanosleep() in SysCon() 3, retVal = ", retVal);
    
//		check whether file data is available and, if it is, move it into program variables
		if ((sysConCmdCrnt == eCmdSysConParms) || (sysConCmdCrnt == eCmdSysConCParms))
		{
			if (FileDataShmPtr->allDataAvailFromFile)
			{
                if (!FileDataGet(eFileTypeParms))
                    SysConSendStrngNoParm("Error reading parms data, all previous values being used");
                else
                {
                    if (!FileDataGet(eFileTypeCflat))
                        SysConSendStrngNoParm("Error reading cflat data, previous values being used");
                    if (!FileDataGet(eFileTypeRefCentB))
                        SysConSendStrngNoParm("Error reading refcentB data, previous values being used");
                    if (!FileDataGet(eFileTypeRefCentC))
                        SysConSendStrngNoParm("Error reading refcentC data, previous values being used");
                    if (!FileDataGet(eFileTypeRefCentQ))
                        SysConSendStrngNoParm("Error reading refcentQ data, previous values being used");
                    if (!FileDataGet(eFileTypeRefCentO))
                        SysConSendStrngNoParm("Error reading refcentO data, previous values being used");
                    if (!FileDataGet(eFileTypeCentOff))
                        SysConSendStrngNoParm("Error reading centoff data, previous values being used");
                    if (!FileDataGet(eFileTypeCM))
                        SysConSendStrngNoParm("Error reading cm data, previous values being used");
                    if (!FileDataGet(eFileTypeFlat))
                        SysConSendStrngNoParm("Error reading flat data, previous values being used");
                    if (!FileDataGet(eFileTypeSharp))
                        SysConSendStrngNoParm("Error reading sharp data, previous values being used");
                    if (!FileDataGet(eFileTypeLTTOpen))
                        SysConSendStrngNoParm("Error reading LTT open data, previous values being used");
                    if (!FileDataGet(eFileTypeLTTClosed))
                        SysConSendStrngNoParm("Error reading LTT closed data, previous values being used");
                }
				FileDataShmPtr->cmDataFileError = false;
				FileDataShmPtr->allDataAvailFromFile = false;
				FileDataShmPtr->someDataAvailFromFile = false;
				sysConCmdCrnt = eCmdNone;
			}
		}
		else if (sysConCmdCrnt == eCmdSysConFillcm)
		{
			if (FileDataShmPtr->cmDataFileError)
			{
				SysConSendStrngNoParm("Error reading cm file data on HRT side");
				strcpy(FileDataShmPtr->crntFilenames[eFileTypeCM], cmFilenameSave);  // restore original cm filename
				FileDataShmPtr->cmDataFileError = false;
				sysConCmdCrnt = eCmdNone;
			}
			else if (FileDataShmPtr->dataAvailFromFile[eFileTypeCM])
			{
                if (!FileDataGet(eFileTypeCM))
                    SysConSendStrngNoParm("Error reading cm data, previous values being used");
				FileDataPut(eFileTypeParms);  // store the new cm filename
				FileDataShmPtr->someDataAvailFromFile = false;
				FileDataShmPtr->dataAvailFromFile[eFileTypeCM] = false;
				sysConCmdCrnt = eCmdNone;
			}
		}
		else if ((FileDataShmPtr->allDataAvailFromFile) || (FileDataShmPtr->someDataAvailFromFile))
		{
			SysConSendStrngNoParm("Error: unexpected file data on HRT side");
			FileDataShmPtr->allDataAvailFromFile = false;
			FileDataShmPtr->someDataAvailFromFile = false;
			for (i = 0; i < eFileTypeNum; i++)
				FileDataShmPtr->dataAvailFromFile[i] = false;
		}
					
//		check command FIFO for message
        retVal = rtl_read(CmdFIFOfd, &cmdFIFOMsg, sizeof(cmdFIFOMsgType));
		if (retVal < 0)
			SysConSendStrngIntParm("Error reading from command FIFO, HRT side, errno = ", rtl_errno);

//		if there is a message, send the command off to the appropriate thread
		else if (retVal > 0)
		{
			if (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdAbort)
			{
				cmdAction = eCmdActNone;
				if (CamDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActSendToCam;
				else if (CamLTTPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActSendToLTT;
				else if (CentDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActSendToCent;
				else if (ReconDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActSendToRecon;
			}
			else
			{
				if ((CamDataPtr->cmdCrnt == eCmdNone) &&
					(CamLTTPtr->cmdCrnt == eCmdNone) &&
					(CentDataPtr->cmdCrnt == eCmdNone) &&
					(ReconDataPtr->cmdCrnt == eCmdNone) &&
					(sysConCmdCrnt == eCmdNone))
					cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
				else if (((CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdReconReg) ||     // special cases allowing multiple reg
                          (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdReconRegok)) &&  // > commands or the regok command
						  (ReconDataPtr->cmdCrnt == eCmdReconReg))
					cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
				else
				{
					cmdAction = eCmdActNone;
					SysConSendStrngNoParm("Error: command not allowed, another command is executing");
				}

				if (cmdAction != eCmdActNone)
				{
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdAOOpenBit) > 0) && (AOLoopStat != eAOLoopOpen))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm("Error: command not allowed, AO loop must be open");
					}
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTTOpenBit) > 0) && (TTLoopStat != eTTLoopOpen))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm("Error: command not allowed, TT loop must be open");
					}
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdAPDOpenBit) > 0) && (APDLoopStat != eAPDLoopOpen))
					{
						cmdAction = eCmdActNone;
						if (UseAPD == 1)
							SysConSendStrngNoParm("Error: command not allowed, APD loop must be open");
					}
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdLTTOpenBit) > 0) && (!LTTOpenLoopMode))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm("Error: command not allowed, LTT loop must be open");
					}
				}
			}

			switch (cmdAction)
			{
				case eCmdActNone:
					break;

				case eCmdActSendToCam:
					CamDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					CamDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamInt)
						CamDataPtr->cmdIntParam = cmdFIFOMsg.intParam;
					else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamFloat)
						CamDataPtr->cmdFloatParam = cmdFIFOMsg.floatParam;
					if (UseAOA != 0)
					{
						if (rtl_sem_post(&CamSem) < 0)
							SysConSendStrngIntParm("Error posting command to CamSem, errno = ", rtl_errno);
					}
					else	// we are using the SciMeasure Camera
					{
						if (rtl_sem_post(&WFSSem) < 0)
							SysConSendStrngIntParm("Error posting command to WFSSem, errno = ", rtl_errno);
					}
					break;

				case eCmdActSendToLTT:
					CamLTTPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					CamLTTPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamInt)
						CamLTTPtr->cmdIntParam = cmdFIFOMsg.intParam;
					else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamFloat)
						CamLTTPtr->cmdFloatParam = cmdFIFOMsg.floatParam;
					if (rtl_sem_post(&LTTSem) < 0)
						SysConSendStrngIntParm("Error posting command to LTTSem, errno = ", rtl_errno);
					break;

				case eCmdActSendToCent:
					CentDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					CentDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamInt)
						CentDataPtr->cmdIntParam = cmdFIFOMsg.intParam;
					else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamFloat)
						CentDataPtr->cmdFloatParam = cmdFIFOMsg.floatParam;
					if (rtl_sem_post(&CentSem) < 0)
						SysConSendStrngIntParm("Error posting command to CentSem, errno = ", rtl_errno);
					break;

				case eCmdActSendToRecon:
					ReconDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					ReconDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamInt)
						ReconDataPtr->cmdIntParam = cmdFIFOMsg.intParam;
					else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator == eCmdParamFloat)
						ReconDataPtr->cmdFloatParam = cmdFIFOMsg.floatParam;
					if (rtl_sem_post(&ReconSem) < 0)
						SysConSendStrngIntParm("Error posting command to ReconSem, errno = ", rtl_errno);
					break;

				case eCmdActSendToHRT:
					switch (CmdTable[cmdFIFOMsg.cmdNdx].idNum)
					{
						case eCmdSysConParms:   // reads all parameter data files
						case eCmdSysConCParms:  // reads all parameter data files
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							FileDataCmd(eFileReadAllData);
							SysConSendStrngNoParm("Notification: cparms received");
							break;

						case eCmdSysConFillcm:  // reads the specified cm file
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy(cmFilenameSave, FileDataShmPtr->crntFilenames[eFileTypeCM]);  // save cmFilename in case of error
							strcpy(FileDataShmPtr->crntFilenames[eFileTypeCM], cmdFIFOMsg.strngParam);
							strcat(FileDataShmPtr->crntFilenames[eFileTypeCM], ".sdt");
							FileDataCmd(eFileReadCMData);
							SysConSendStrngNoParm("Notification: fillcm complete");
							break;

						case eCmdTestN:
							SysConSendStrngNoParm("Command received (no param)");
							break;

						case eCmdTestI:
							SysConSendStrngIntParm("Command received (int param): ", cmdFIFOMsg.intParam);
							break;

						case eCmdTestF:
							SysConSendStrngFltParm("Command received (float param): ", cmdFIFOMsg.floatParam);
							break;

						default:
							SysConSendStrngIntParm("Error: unexpected command received by syscon, cmdNdx = ", cmdFIFOMsg.cmdNdx);
							break;
					}
					break;
			}
		}
	}
}


/**********************************************************************************************

UNIT NAME:  SysConSendStrngNoParm()
UNIT NAME:  SysConSendStrngIntParm()
UNIT NAME:  SysConSendStrngFltParm()

UNIT DESCRIPTION:
These units sends a null-terminated string (generally an error message) to hostDiag to be sent
to the host.

UNIT INPUTS:
1.	strngPtr    - pointer to null-terminated character string
2.	intParam	- integer parameter
3.	floatParam	- float parameter

UNIT OUTPUTS:
	None

UNIT ERROR CODES:
	None

**********************************************************************************************/

void SysConSendStrngNoParm( char *strngPtr )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
//		rtl_printf("Error writing noParm message to diag FIFO, HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
//		rtl_printf("*** %s\n", strngPtr);
    }
}


void SysConSendStrngIntParm( char *strngPtr, int16 intParam )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngIntParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	diagMsg.diagFIFOMsg.intParam = intParam;

	if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
//		rtl_printf("Error writing intParm message to diag FIFO, HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
//		rtl_printf("*** %s %d\n", strngPtr, intParam);
    }
}


void SysConSendStrngFltParm( char *strngPtr, float32 floatParam )
{
	int16 stringLength;
	struct
	{
		diagFIFOMsgType diagFIFOMsg;
		char diagString[DIAG_STRING_MAX_LENGTH];
	} diagMsg;

	diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngFltParm;

	stringLength = strlen(strngPtr) + 1;
	if (stringLength > DIAG_STRING_MAX_LENGTH)
	{
		stringLength = DIAG_STRING_MAX_LENGTH;
		strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
		diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
	}
	else
		strcpy(diagMsg.diagString, strngPtr);

	diagMsg.diagFIFOMsg.stringLength = stringLength;

	diagMsg.diagFIFOMsg.floatParam = floatParam;

	if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
//		rtl_printf("Error writing floatParm message to diag FIFO, HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
//		rtl_printf("*** %s %g\n", strngPtr, floatParam);
    }
}


//TTT start
/**********************************************************************************************

UNIT NAME:  SysConTimeDiff()

UNIT DESCRIPTION:
The purpose of this unit is to compute compute and return time differences.

UNIT INPUTS:
    startTime - start time (struct timespec)
    endTime   - end time (struct timespec)

UNIT OUTPUTS:
	return value - difference between start time and end time, in microseconds (uint32)

UNIT ERROR CODES:
	None

**********************************************************************************************/

uint32 SysConTimeDiff( struct rtl_timespec startTime, struct rtl_timespec endTime )
{
    uint32 secs, nSecs;

    if (endTime.tv_sec == startTime.tv_sec)
    {
        secs = 0;
        nSecs = endTime.tv_nsec - startTime.tv_nsec;
    }
    else
    {
        secs = endTime.tv_sec - startTime.tv_sec - 1;
        nSecs = endTime.tv_nsec + (1000000000 - startTime.tv_nsec);
    }

    return secs * 1000000 + nSecs / 1000;
}
//TTT end
