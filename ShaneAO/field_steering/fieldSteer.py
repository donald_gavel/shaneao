"""
fieldSteer.py
  Wavefront sensor field steering
"""
file = '/Users/donaldgavel/Dropbox/py/modules/fieldSteer.py'
import numpy as np
np.set_printoptions(linewidth = 120)
import scipy.linalg

# units & conventions
mm = 0.001
mr = 0.001
I = np.eye(3)
i = 1j
pi = np.pi
cos = np.cos
sin = np.sin
degrees = pi/180.

def vector(l):
    """create a column vector given a list
    """
    return np.matrix(l).transpose()

x = vector([1,0,0])
y = vector([0,1,0])
z = vector([0,0,1])
origin = vector([0,0,0])

def rotate(dir,alpha):
    """return the active rotation matrix about an axis
    dir = 'x', 'y', or 'z'
    alpha = angle, in radians
    """
    if (dir == 'x'):
        return np.matrix([
            [1.,0.,0.],
            [0., cos(alpha),-sin(alpha)],
            [0., sin(alpha), cos(alpha)]
            ])
    elif (dir == 'y'):
        return np.matrix([
            [cos(alpha), 0., sin(alpha)],
            [0.,1.,0.],
            [-sin(alpha), 0., cos(alpha)]
            ])
    elif (dir == 'z'):
        return np.matrix([
            [cos(alpha),-sin(alpha), 0.],
            [sin(alpha), cos(alpha), 0.],
            [0.,0.,1.]])
    return None

def normalized(v):
    """return the normalized version of a vector
    """
    return v / np.linalg.norm(v)

def cross(v,w=None):
    """cross product of 2 3-vectors
    """
    if (w == None):
        vx,vy,vz = v
        c = [[ 0., -vz,  vy ],
            [  vz,  0., -vx ],
            [ -vy,  vx,  0. ]]
        return np.matrix(c)
    else:
        return np.matrix(np.cross(v.transpose(),w.transpose()).transpose())

def euler_rotate(v,w,theta=1.):
    """general rotation of the vector(s) v about an arbitrary vector w, by an amount theta
    """
    c = cross(w)
    T = scipy.linalg.expm(c*theta)
    return T*v

def intersect(ray,plane):
    """Intersect a ray with a plane mirror
    ray = tuple (position, direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
          in which case the 3ed column is treated as the ray direction
    plane = tuple (position, normal) both 3-vectors
    """
    p,z = ray
    if (z.shape[1] > 1):
        z3 = z[:,2]
    else:
        z3 = z
    q,n = plane
    n = n[:,0]
    M = ( z3 * n.transpose() ) / ( z3.transpose() * n )
    p = p + M * (q - p)
    return (p,z)

def reflect(ray,mirror):
    """Reflect a ray off a mirror
    ray = ray tuple (position,direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
        The vectors are all treated as directions. The position is unchanged
    mirror = mirror tuple (position, normal) both 3-vectors, or a normalized 3-vector
    
    It is implicitly assumed that the ray position is a point on the mirror plane, but this is not checked.
    """
    p,v = ray
    q,n = mirror
    n = n[:,0]
    H = I - 2 * n * n.transpose()
    v = H*v
    return (p,v)

def propagate(ray,distance):
    """Propagate a ray for a distance
    ray = tuple (position, direction) both 3-vectors, or direction can be a matrix whose columns are 3-vectors
          in which case the 3ed column is treated as the ray direction
    distance = scalar distance
    """
    p, z = ray
    if (z.shape[1] > 1):
        z3 = z[:,2]
    else:
        z3 = z
    p = p + distance*z3
    return(p,z)

## parameters
mirror_pivot_radius = 13.82*mm
alpha = 0.*degrees # this is the departure from right angle of the WFS path
d01 = 100.*mm # origin -> m1
d12 = 30.*mm # m1 -> m2
d23 = 100.*mm # m2 -> focus
plate_scale = 0.29 # arcseconds per millimeter
fov_arcsec = 20 # arcseconds
fov_mm = fov_arcsec*plate_scale*mm
f_number = 20.
pdiam = 1./f_number

def calibrate():
    """define the mirrors and nominal angles of the system.
    """
    global s0, mirror1, mirror2, pivot1, pivot2, focalPlane
    
    p1 = origin + z*d01
    pivot1 = p1 + vector((-mirror_pivot_radius,0,0))
    rot1 = rotate('y',135*degrees)
    nab = rot1*np.hstack([x,y,z])
    mirror1 = (p1, nab)
    pivot1 = rot1*(pivot1 - p1) + p1
    
    p2 = mirror1[0] + d12*(-x)
    pivot2 = p2 + vector((-mirror_pivot_radius,0,0))
    rot2 = rotate('x',alpha)*rotate('z',-45*degrees)
    nab = rot2*np.hstack([x,y,z])
    mirror2 = (p2, nab)
    pivot2 = rot2*(pivot2 - p2) + p2
    
    # find the focal plane position and normal
    r0 = (origin,z)
    r0 = intersect(r0,mirror1)
    r0 = reflect(r0,mirror1)
    r0 = intersect(r0,mirror2)
    r0 = reflect(r0,mirror2)
    r0 = propagate(r0,d23)
    focalPlane = r0
    
    # define a coordinate system at the focal plane -
    #  x - perpundicular to table; y - parallel to table = z cross x; z - along ray;
    s0 = np.hstack((x,y,z))
    s0 = rotate('x',90*degrees + alpha)*s0
    
    # propagate a gut ray and a full axis-set (3 orthonormal unit vectors)
    s = np.hstack((x,y,z))
    ray = (origin, s)
    ray = intersect(ray,mirror1)
    ray = reflect(ray,mirror1)
    ray = intersect(ray,mirror2)
    ray = reflect(ray,mirror2)
    ray = intersect(ray,focalPlane)
    
    # figure rotation of original x,y on focal plane coordinates
    r = s0.transpose() # reference coordinates on focal plane
    v = ray[1] # original x and y vectors after reflections
    a = r*v # 3x3 transformation matrix; row&col 3 should be (0,0,1). 2x2 upper block should be the rotation
    theta = np.arctan2(a[1,0],a[0,0]) # this is the rotation about z of the original axis set

def shoot(r,m1=(0.,0.),m2=(0.,0.)):
    """Shoots a ray starting at point r (and telecentric direction z) to the focal plane.
    argument r is a 3-vector
    
    m1 and m2 are optional commands that move the two steering mirrors. the command is given as two
    angles, (ra, rb) in radians. The ra and rb axes are afixed to each mirror and orthogonal
    to the mirror's normal, as defined in the calibration.
    
    returns (position,rotation matrix) = tuple(3-vector,3x3 matrix) denoting the ray as it hit focal plane,
        expressed in focal plane coordinates
    
    The intercept point is the first element of the returned tuple
    The intercept angle is encoded in the z-vector (3ed column) of the second element of the returned tuple
    """
    v1 = mirror1[1] * np.vstack((0,vector(m1)))
    p1 = euler_rotate(mirror1[0] - pivot1,v1) + pivot1
    v2 = mirror2[1] * np.vstack((0,vector(m2)))
    p2 = euler_rotate(mirror2[0] - pivot2,v2) + pivot2
    mirror1_moved = (p1, euler_rotate(mirror1[1],v1))
    mirror2_moved = (p2, euler_rotate(mirror2[1],v2))
    
    ray = (r,np.hstack((x,y,z)))
    ray = intersect(ray,mirror1_moved)
    ray = reflect(ray,mirror1_moved)
    ray = intersect(ray,mirror2_moved)
    ray = reflect(ray,mirror2_moved)
    ray = intersect(ray,focalPlane)
    p = s0.transpose() * (ray[0] - focalPlane[0])
    ax = s0.transpose() * ray[1]
    
    return (p,ax)

def func(m,px,py):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    px, py is the desized position, in mm on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (px - p.item(0), py - p.item(1),ax[:,2].item(0),ax[:,2].item(1))
    return v

def cfunc(m,cx,cy):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    cx, cy is the desired centering, in radians incident on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (p.item(0),p.item(1), cx - ax[:,2].item(0), cy - ax[:,2].item(1))
    return v

def pcfunc(m,px,py,cx,cy):
    """This is the function for the solver code
    m is the 4-tuple of mirror commands
    px, py is the desized position, in mm on focal plane
    cx, cy is the desired centering, in radians incident on focal plane
    """
    m1 = (m[0],m[1])
    m2 = (m[2],m[3])
    p,ax = shoot(origin,m1,m2)
    v = (px - p.item(0),py - p.item(1), cx - ax[:,2].item(0), cy - ax[:,2].item(1))
    return v

def Rmatrix():
    """form a 4x4 matrix of focal plane spot position and angle of arrival sensitivity to mirror moves
    """
    r_set = []
    delta = 0.001
    spot0 = shoot(origin)
    
    spot = shoot(origin,(delta,0),(0.,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((spot[0][0:2],spot[1][0:2,2])))
    
    spot = shoot(origin,(0,delta),(0.,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    
    spot = shoot(origin,(0,0),(delta,0.))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    
    spot = shoot(origin,(0,0),(0.,delta))
    dspot = (spot[0] - spot0[0], spot[1] - spot0[1])
    r_set.append(np.vstack((dspot[0][0:2],dspot[1][0:2,2])))
    r = np.hstack(r_set)/delta
    
    return r,r_set

def pcSolve(p,c):
    pc = np.vstack((vector(p),vector(c)))
    R, r_set = Rmatrix()
    Rinv = np.linalg.inv(R)
    m = Rinv * pc
    m1 = (m.item(0),m.item(1))
    m2 = (m.item(2),m.item(3))
    return (m1,m2)

def show(ray,n=4):
    """display the ray point and its orientation triplet in an easy to read form
    """
    print np.round(np.hstack(ray),n)

def scan(m):
    """scan the p & c pair along +/- 3 x the unit motor commands, m
    m is a 2-tuple of 2-tuples describing mirror1 and mirror 2 motor commands
    """
    ma = np.array(m)
    amp_set = np.arange(-3,3,.5)
    x_set = []
    y_set = []
    cx_set = []
    cy_set = []
    for amp in amp_set:
        m = amp*ma
        m1 = m[0]
        m2 = m[1]
        spot = shoot(origin,m1,m2)
        px = spot[0].item(0)
        py = spot[0].item(1)
        cx = spot[1][:,2].item(0)
        cy = spot[1][:,2].item(1)
        x_set.append(px)
        y_set.append(py)
        cx_set.append(cx)
        cy_set.append(cy)
        
    return x_set,y_set,cx_set,cy_set

calibrate()
import matplotlib.pyplot as plt
import scipy.optimize
plt.ion()

def drawPlot(kind):
    """plot the various motion results.
    
    Pointing is shown on the focal plane, in focal plane coordinates.
    Units are meters, and generally shown on a +/- 10mm scale
    
    Centering is shown on the focal plane as an incident angle at the focal plane.
    Units are radians, generally shown on a +/- 10mr scale
    The pupil diameter is ~50mr (=1/f#)
    
    Raw Motion:
    +/- 30 milliradian of motion of each mirror channel in turn.
    
    Linear Solved:
    Computes the sensitivity of p and c to mirror motions
    (see Rmatrix), using deltas of 1 mr mirror commands
    Then 'solves' for the mirror commands using the inverse
    of this matrix as desired position is scanned from
    -3 to +3 mm
    
    Experimental Motion:
    A 'hand waving' argument that non-decentering pointing moves
    are coordinated motions of the mirror motors related by
    a factor of sqrt(2) and axes are independent.
    The mirror1 is scanned +/- 60 mr in each axis, with
    mirror2 following according to the scale factor.
    
    Optimizer:
    A least-squares solution to the motor moves that produce
    non-decentering position moves. The desired positions are
    scanned +/- 5mm. Two additional plots show the mirror
    commands that do this.
    """
    kinds = ['Raw Motion',
             'Linear Solved',
             'Experimental Motions',
             'Optimizer']
    if (not (kind in kinds)):
        print '<drawPlots> ERROR'+kind+' not in '+str(kinds)
        return None
    
    c_title = 'centering ('+str(int(pdiam*1000))+'mr = full pupil)'
    calibrate()
    if (kind == 'Optimizer'):
        plt.figure(kind,figsize=(12,12))
    
        plt.subplot(2,2,1)
        plt.title('position')
        plt.xlabel('focal plane position x, meters')
        plt.ylabel('focal plane position y, meters')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
    
        plt.subplot(2,2,2)
        plt.title(c_title)
        plt.xlabel('centering x, radians')
        plt.ylabel('centering y, radians')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
        
        plt.subplot(2,2,3)
        plt.title('mirror 1')
        plt.xlabel('a command, radians')
        plt.ylabel('b command, radians')
        plt.grid('on')
        
        plt.subplot(2,2,4)
        plt.title('mirror 2')
        plt.xlabel('a command, radians')
        plt.ylabel('b command, radians')
        plt.grid('on')
    
        plt.subplots_adjust(wspace = 0.6)
    
    else:
        plt.figure(kind,figsize=(12,6))
    
        plt.subplot(1,2,1)
        plt.title('position')
        plt.xlabel('focal plane position x, meters')
        plt.ylabel('focal plane position y, meters')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
    
        plt.subplot(1,2,2)
        plt.title(c_title)
        plt.xlabel('centering x, radians')
        plt.ylabel('centering y, radians')
        plt.axis((-.01,.01,-.01,.01))
        plt.grid('on')
        
        plt.subplots_adjust(wspace = 0.4)
                
    if (kind == 'Linear Solved'):
        delta = 1*mm
        p = (delta, 0)
        c = (0, 0)
        m1,m2 = pcSolve(p,c)
        px = np.matrix((m1,m2))
        
        p = (0, delta)
        c = (0, 0)
        m1,m2 = pcSolve(p,c)
        py = np.matrix((m1,m2))
        
        x_set,y_set,cx_set,cy_set = scan(px)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(px+py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(px-py)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Raw Motion'):
        d = 0.01
        m1 = ((d,0),(0,0))
        m2 = ((0,d),(0,0))
        m3 = ((0,0),(d,0))
        m4 = ((0,0),(0,d))
        
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m2)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m3)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        x_set,y_set,cx_set,cy_set = scan(m4)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Experimental Motions'):
        d = 0.02
        
        m1 = ((d,0),(1.414*d,0))
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
        
        m1 = ((0,d),(0,-d*.707))
        x_set,y_set,cx_set,cy_set = scan(m1)
        plt.subplot(1,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(1,2,2); plt.plot(cx_set,cy_set,'x-')
    
    if (kind == 'Optimizer'):
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,0))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')

        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for py in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(0,py))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            p,ax = shoot(origin,m1,m2)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')
        
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,px))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')
        
        m_set =[]
        x_set =[]
        y_set =[]
        cx_set =[]
        cy_set = []
        for px in np.arange(-.005,.005,.001):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,-px))
            #print str(nr)+' motors: '+str(np.round(u,3))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            m_set.append((m1,m2))
            p,ax = shoot(origin,m1,m2)
            items = ((p.item(0),p.item(1)),(ax[:,2].item(0),ax[:,2].item(1)))
            #print 'p, c = '+str(items)
            x_set.append(p.item(0))
            y_set.append(p.item(1))
            cx_set.append(ax[:,2].item(0))
            cy_set.append(ax[:,2].item(1))
        
        plt.subplot(2,2,1); plt.plot(x_set,y_set,'x-')
        plt.subplot(2,2,2); plt.plot(cx_set,cy_set,'x-')
        m = np.array(m_set)
        m1a = m[:,0,0]; m1b = m[:,0,1]; m2a = m[:,1,0]; m2b = m[:,1,1]
        plt.subplot(2,2,3); plt.plot(m1a,m1b,'x-')
        plt.subplot(2,2,4); plt.plot(m2a,m2b,'x-')

kinds = ['Raw Motion',
         'Linear Solved',
         'Experimental Motions',
         'Optimizer']

doit = 0
if (doit):
    for kind in kinds:
        drawPlot(kind)

def genPTable(limit=5*mm,delta=None):
    """generate a lookup table for pointing motions
    """
    if (delta == None):
        delta = limit*2./10.
    plt.figure('Lookup Table - Pointing',figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    m_set = []
    p_set = []
    for px in np.arange(-limit,limit+.01*mm,delta):
        for py in np.arange(-limit,limit+.01*mm,delta):
            u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,py))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            p_set.append((px,py))
            m_set.append((m1,m2))
            
    p = np.array(p_set)
    m = np.array(m_set)
    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
        
    return p,m

def genCTable(limit=10*mr,delta=None):
    """generate a lookup table for centering motions
    """
    if (delta == None):
        delta = 2.*limit/10.
    plt.figure('Lookup Table - Centering',figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('a command, radians')
    plt.ylabel('b command, radians')
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    m_set = []
    c_set = []
    for cx in np.arange(-limit,limit+.01*mr,delta):
        for cy in np.arange(-limit,limit+.01*mr,delta):
            u,nr = scipy.optimize.leastsq(cfunc,(0,0,0,0),(cx,cy))
            m1 = (u.item(0),u.item(1))
            m2 = (u.item(2),u.item(3))
            c_set.append((cx,cy))
            m_set.append((m1,m2))
            
    c = np.array(c_set)
    m = np.array(m_set)
    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
        
    return c,m

# Motor encoder count data - from data taken Dec 3, 2013
motorA = {'name':'WFS2RY','mid':-38766,'scale':12766/(2.94*degrees),'limits':[13466,-110346]}
motorB = {'name':'WFS2RX','mid':-32792,'scale':-15000/(3.25*degrees),'limits':[18194,-115753]}
motorC = {'name':'WFS1RX','mid':-40696,'scale':15000/(3.35*degrees),'limits':[7786,-124997]}
motorD = {'name':'WFS1RY','mid':-23125,'scale':15000/(3.37*degrees),'limits':[22551,-94530]}

def anglesToCommands(a):
    """Convert the angles in the list a to motor encoder counts.
    m is a n x 2 x 2 array, so it is a list of m motor commands, each
    with the order [[mirror1rot+y, mirror1rot-z], [mirror2rot+y, mirror2rot+z]]
    """
    if (len(a.shape) == 2):
        al = [a]
    else:
        al = list(a)
    cl = []
    motorList = [motorC,motorD,motorA,motorB]
    for u in al:
        u = u.flatten()
        v = np.zeros((4))
        for k in range(4):
            motor = motorList[k]
            v[k] = int(motor['mid'] + motor['scale']*u[k])
        v = v.reshape((2,2))
        cl.append(v)
    return np.array(cl)
    
def plotTable(m,title='name me',units='radians'):
    """plot the already generated lookup table for pointing motions
    """
    plt.figure(title,figsize=(12,6))
    
    plt.subplot(1,2,1)
    plt.title('mirror 1')
    plt.xlabel('C command, '+units)
    plt.ylabel('D command, '+units)
    plt.grid('on')
    
    plt.subplot(1,2,2)
    plt.title('mirror 2')
    plt.xlabel('A command, '+units)
    plt.ylabel('B command, '+units)
    plt.grid('on')

    plt.subplots_adjust(wspace = 0.4)

    plt.subplot(1,2,1); plt.plot(m[:,0,0],m[:,0,1],'x')
    plt.subplot(1,2,2); plt.plot(m[:,1,0],m[:,1,1],'x')
    
    if (units == 'counts'):
        plt.subplot(1,2,1)
        c = np.array(motorC['limits'])
        d = np.array(motorD['limits'])
        x = [ c.min(), c.max() ]
        y = [ d.min(), d.min() ]
        plt.plot(x,y,'k')
        y = [ d.max(), d.max() ]
        plt.plot(x,y,'k')
        y = [ d.min(), d.max() ]
        x = [ c.min(), c.min() ]
        plt.plot(x,y,'k')
        x = [ c.max(), c.max() ]
        plt.plot(x,y,'k')
        
        plt.subplot(1,2,2)
        c = np.array(motorA['limits'])
        d = np.array(motorB['limits'])
        x = [ c.min(), c.max() ]
        y = [ d.min(), d.min() ]
        plt.plot(x,y,'k')
        y = [ d.max(), d.max() ]
        plt.plot(x,y,'k')
        y = [ d.min(), d.max() ]
        x = [ c.min(), c.min() ]
        plt.plot(x,y,'k')
        x = [ c.max(), c.max() ]
        plt.plot(x,y,'k')

def point(pxa,pya,verbose=False):
    """calculate the motor positions, A,B,C,D in motor encoder counts,
    required to point to a given field position, pxa, pya, given in arcseconds.
    """
    global state
    px = pxa*plate_scale*mm
    py = pya*plate_scale*mm
    u,nr = scipy.optimize.leastsq(func,(0,0,0,0),(px,py))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(pxa,pya),'c':(0,0),'motors':c}
    if (verbose):
        print('A: '+motorA['name']+' = '+str(int(c[1,0]))+' ( '+str(m2[0])+' radians)')
        print('B: '+motorB['name']+' = '+str(int(c[1,1]))+' ( '+str(m2[1])+' radians)')
        print('C: '+motorC['name']+' = '+str(int(c[0,0]))+' ( '+str(m1[0])+' radians)')
        print('D: '+motorD['name']+' = '+str(int(c[0,1]))+' ( '+str(m1[1])+' radians)')
    else:
        return c
    
def center(cxa,cya,verbose=False):
    """calculate the motor positions, A,B,C,D in motor encoder counts,
    required to center to a given pupil position, cxa, cya, given in fractions
    of a pupil diameter
    """
    global state
    cx = cxa*pdiam
    cy = cya*pdiam
    u,nr = scipy.optimize.leastsq(cfunc,(0,0,0,0),(cx,cy))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(0,0),'c':(cxa,cya),'motors':c}
    if (verbose):
        print('A: '+motorA['name']+' = '+str(int(c[1,0]))+' ( '+str(m2[0])+' radians)')
        print('B: '+motorB['name']+' = '+str(int(c[1,1]))+' ( '+str(m2[1])+' radians)')
        print('C: '+motorC['name']+' = '+str(int(c[0,0]))+' ( '+str(m1[0])+' radians)')
        print('D: '+motorD['name']+' = '+str(int(c[0,1]))+' ( '+str(m1[1])+' radians)')
    else:
        return c

def pnc(pxa,pya,cxa,cya,verbose=False):
    """calculate the motor positions, A,B,C,D in motor encoder counts,
    required to both point and center to a given
    position, pxa, pya, in arcsec on the focal plane and
    centring, cxa, cya, in fractions of pupil
    """
    global state
    px = pxa*plate_scale*mm
    py = pya*plate_scale*mm
    cx = cxa*pdiam
    cy = cya*pdiam
    u,nr = scipy.optimize.leastsq(pcfunc,(0,0,0,0),(px,py,cx,cy))
    m1 = (u.item(0),u.item(1))
    m2 = (u.item(2),u.item(3))
    m = np.array([m1,m2])
    c = anglesToCommands(m).reshape((2,2))
    state = {'p':(pxa,pya),'c':(cxa,cya),'motors':c}
    if (verbose):
        print('A: '+motorA['name']+' = '+str(int(c[1,0]))+' ( '+str(m2[0])+' radians)')
        print('B: '+motorB['name']+' = '+str(int(c[1,1]))+' ( '+str(m2[1])+' radians)')
        print('C: '+motorC['name']+' = '+str(int(c[0,0]))+' ( '+str(m1[0])+' radians)')
        print('D: '+motorD['name']+' = '+str(int(c[0,1]))+' ( '+str(m1[1])+' radians)')
    else:
        return c

state = {'p':(0,0),'c':(0,0),'motors':pnc(0,0,0,0)}

def deltaP(dpx,dpy,verbose=False):
    """adjust the pointing by a delta amount, dpx, dpy,
    given in arcseconds
    """
    global state
    px = state['p'][0] + dpx
    py = state['p'][1] + dpy
    cx = state['c'][0]
    cy = state['c'][1]
    c = pnc(px,py,cx,cy,verbose=verbose)
    return c

def deltaC(dcx,dcy,verbose=False):
    """adjust the centering by a delta amount, dcx, dxy,
    given in fractions of a pupil
    """
    global state
    px = state['p'][0]
    py = state['p'][1]
    cx = state['c'][0] + dcx
    cy = state['c'][1] + dcy
    c = pnc(px,py,cx,cy,verbose=verbose)
    return c

