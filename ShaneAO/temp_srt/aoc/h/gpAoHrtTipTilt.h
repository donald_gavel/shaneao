/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtTipTilt.h
 *
 * 	@version  $Id: gpAoHrtTipTilt.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the tip/tilt component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the tip/tilt component.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/26/2008
 *
 *
 *	09/06/2007: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _TIPTILT_H
#define _TIPTILT_H		1


// Defines, Enums, and Typedefs

#define INIT_TT_GAIN  0.1  // gain for tip/tilt loop

//******************************************
// LQG defines, enums, and typedefs -- start

// indexes into fits file for filter parameters and Kalman gains
// > IMPORTANT -- if the following changes, it is likely that NG, NA, and NB will need to change!
enum {	eAtmWind_r_and_g1, eAtmWind_theta_and_g2,  // AtmWind means atmosphere and windshake induced TT
		eAtmWind_unused1, eAtmWind_unused2, eAtmWind_unused3,
		eCpVib_alpha0R, eCpVib_alpha0I, eCpVib_alpha1R, eCpVib_alpha1I, eCpVib_alpha2R,  eCpVib_alpha2I,  // CpVib means common path vibration induced TT
		eCpVib_unused1, eCpVib_unused2, eCpVib_unused3, eCpVib_unused4,
		ePhi,
		eNcpVib_beta0R, eNcpVib_beta0I,  // NcpVib means non common path vibration induced TT
		eNcpVib_unused1, eNcpVib_unused2, eNcpVib_unused3, eNcpVib_unused4, eNcpVib_unused5, eNcpVib_unused6, eNcpVib_unused7, eNcpVib_unused8,
		eRho,
		eKalmanGainsOffset };  // the Kalman gains come after the filter params, in the same order -- all reals, followed by all imaginaries

#define N_ALLPARAMS				(3 * eKalmanGainsOffset)  // there are 2 sets of parameters -- the filter parameters and the 
#define KALMAN_GAIN_IMAG_OFFSET (2 * eKalmanGainsOffset)  // > Kalman gains; but, the Kalman gains are complex

#define NG 2  // number of g's
#define NA 3  // number of alphas and a's
#define NB 1  // number of betas and b's

// environmental parameter defines and enums
#define PARM_DIM 3	 // size of parameter lookup table dimensions
#define NS PARM_DIM  // size of parameter lookup table dimension for seeing
#define NW PARM_DIM  // size of parameter lookup table dimension for wind power
#define NR PARM_DIM  // size of parameter lookup table dimension for star brightness

enum { eTtEnvSeeing, eTtEnvWindPower, eTtEnvStarBrightness };

// LQG defines, enums, and typedefs -- end
//******************************************


// Global Variables

extern float   hrt_TTGainStage;		  // tip/tilt loop gain, stage
extern float   hrt_TTGainWfrSurface;  // tip/tilt loop gain, woofer surface

extern int	   hrt_ttSplitSwitch;
extern boolean hrt_ttLQGSwitch;
extern float   hrt_ttWfr9ActMicronsPerMASX;
extern float   hrt_ttWfr9ActMicronsPerMASY;

extern float   hrt_ttCompDelay_microSecs;  // tip/tilt computational delay in microseconds

extern double  hrt_ttSeeing, hrt_ttWindPower, hrt_ttStarBrightness;


// Global Function Prototypes

void  gpAoHrtTipTilt_Init( void );
void  gpAoHrtTipTilt_Process( float xTTCentError, float yTTCentError, 
							  ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr );
void  gpAoHrtTipTilt_CloseOpenTTLoop( int newState );
short gpAoHrtTipTilt_ProcCmd( short cmdNdx );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
