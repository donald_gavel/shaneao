/*******************************************************************************
 *
 *	COPYRIGHT (C) 2010
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoGmb.h
 *
 * 	@version  $Id: gpAoGmb.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the GMB interfaces to the AOC system - it includes general defines
 *	and system specific defines for the GMB interface components.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the GMB system - it includes general defines
 *	and system specific defines for the GMB interfaces components.
 *
 *	@b AUTHOR:
 *	SMJ	DATE: 3/30/2010
 *
 *	@b REVISIONS:
 *	
 *	03/30/2010: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _AOC_GMB_H
	#define _AOC_GMB_H			1


//=====================================================================================================================
// General Defines
//

#define AOC_GMB_ERR				-1

extern	int errGmb;

// IMPORTANT: AOC_CHECK_GMB_ERR() cannot be used in HostICon, because it uses HostDiagSendStrngNoParm()
#if (HRT != 1)
#define AOC_CHECK_GMB_ERR( gE, strErr, strFunc, rsp )													\
{																										\
    if (gE)																								\
    {																									\
		char tmpBuf[512];																				\
		char *eS;																						\
		eS = gpUtGmbErr2Str(gE);																		\
		sprintf(tmpBuf, "ERROR(%s): GMB Error: %s, ErrNum:<%x> Task:<%s>\n", strFunc, eS, gE, strErr );	\
		HostDiagSendStrngNoParm(eDebugLevelErr, tmpBuf);												\
		rsp;																							\
	}																									\
}
#endif

#define AOC_GMB_WRITE_VALUE( outPtr, newValue, valueType )									\
{																							\
	*((valueType*)outPtr) = newValue;														\
	errGmb = gpUtGmbBroadcastArup( gmb, (void*)outPtr, sizeof( valueType ) );				\
}	

#define AOC_GMB_WRITE_ARRAY( outPtr, inPtr, arrayType, xferCount )							\
{																							\
	int xyz;																				\
	for( xyz=0; xyz<count; xyz++ )															\
		((arrayType*)outPtr)[ xyz ] = ((arrayType*)newValue)[ xyz ];						\
	errGmb = gpUtGmbBroadcastArup( gmb, (void*)outPtr, count * sizeof( arrayType ) );		\
}	

#define AOC_HRT_GMB_WRITE_CMD_STATUS( cmd, errCode, newState, errStr )								\
{																									\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].state = newState;										\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrCode = errCode;									\
	strncpy( hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg, errStr, GPI_MAX_STRING_LEN-1 );	\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg[ GPI_MAX_STRING_LEN-1 ] = '\0';				\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].modified = 1;											\
	gpAoHrtFileData_Put( eFileTypeCmdStatus );														\
}

#define AOC_SRT_GMB_WRITE_CMD_STATUS( cmd, errCode, newState, errStr )								\
{																									\
	srtFileDataShmPtr->cmdStatusList[ cmd ].state = newState;										\
	srtFileDataShmPtr->cmdStatusList[ cmd ].stateErrCode = errCode;									\
	strncpy( srtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg, errStr, GPI_MAX_STRING_LEN-1 );	\
	srtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg[ GPI_MAX_STRING_LEN-1 ] = '\0';				\
	srtFileDataShmPtr->cmdStatusList[ cmd ].modified = 1;											\
	gpAoSrtFileMngr_SetCmdStatus( );																\
}


#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
