/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoSrtHostICon.h
 *
 * 	@version  $Id: gpAoSrtHostICon.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the host interface controller component (on the SRT side).
 *
 *	@b DESCRIPTION:
 *	This is the header file for the host interface controller component (on the SRT side).
 *
 *	@b AUTHOR:
 *	SMJ & DWP	DATE: 12/17/2008
 *
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _HOSTICON_H
#define _HOSTICON_H		1


#define SRT_SW_VERSION		"1.3"
#define SRT_SW_DATE			"July 20, 2012"

// Max length of message buffer including trailing '\0'
#define	MSG_BUFF_LEN		2048

#define	GP_GMBBLOCK_AOC		1			; Indicate that AOC will be writing the GMB AOC block - reading others


//-----------------------------------------------------------------------------
// Typedefs
//

// structure for the command table
typedef struct
{
	const char*				strng;									// pointer to ASCII command string constant
	short					idNum;									// command ID number (may or may not be the same as the index into the table!)
	short					action;									// action to be taken when command is received
	short					loopRestrict;							// indicates whether the ao and/or tt loops must be open for a command to execute
	short					paramIndicator[MAX_NUM_CMD_PARAMS];		// parameter type (none, int, or float, strng, special)

} cmdTableType;

// structure for FIFO command messages being sent from hostICon to sysCon
typedef struct
{
	short		cmdNdx;
	int			intParam[ MAX_NUM_CMD_PARAMS ];
	float		floatParam[ MAX_NUM_CMD_PARAMS ];
	char		strngParam[ MAX_NUM_CMD_PARAMS ][MAX_FILENAME_LENGTH];
} cmdFIFOMsgType;


//-----------------------------------------------------------------------------
// Global Variables
//
extern FileDataShmType*		   g_pFileDataShmPtr;	   // pointer to file data shared memory
extern CamDataType*			   g_pCamDataPtr;		   // pointer to camera data
extern CentDataType*		   g_pCentDataPtr;		   // pointer to centroid data
extern ReconDataType*		   g_pReconDataPtr;		   // pointer to centroid data
extern HrtDataFlowControlType* g_pDataFlowControlPtr;  // pointer to Data Flow Control variables

extern sem_t				   gmbSemaphore;  // semaphore to protect multi-threaded gmb accesses

extern int					   srtSystemDebugLevel;  // Current System Debug level

extern int					   prevTlcErrStatArray[eTlcErrNum];  // previous TLC error/status flags
extern int					   prevClipCounts[3];				 // previous clip counter values
extern float				   prevCCDTempKelvins;				 // previous CCD temperature value
extern int					   prevDarkCornerVal;				 // previous dark corner correct value

extern int					   g_HostSocket;		   // ethernet socket for host communications
extern int					   g_DiagFIFOfd;		   // fd for FIFO used to receive diagnostic data to the diag thread
extern char					   g_HostConnectedFlag;  // indicates whether the host socket is connected
extern const cmdTableType	   CmdTable[ ];		   // command table

extern int					   prevTlcErrStatArray[eTlcErrNum];  // previous TLC error/status flags

short gpAoSrtHostICon_ProcCmd( char msgBuf[], int subCmdFlag );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
