
/*********************************************
 * 
 * Edt Driver Wrapper Code 
 * Copyright EDT 2004
 *
 * Note - all functions called from the edt library must be declared asmlinkage, 
 * as must the functions in edt_lnx_kernel.c
 *
 *********************************************/

#ifndef _EDT_LNX_KERNEL_H
#define _EDT_LNX_KERNEL_H


// autoconf.h MUST be included first
#include <linux/autoconf.h>
//#include <linux/types.h>
//#include <linux/ioctl.h>
//#include <linux/errno.h>
//#include <linux/time.h>

/* for asmlinkage */
#include <linux/linkage.h>

#include "edt_mem_mapping.h"

/**************************************************/
/* Edt wrapper functions around kernel calls */
/**************************************************/


/* typedefs for kernel structures - all code except edt_lnx_kernel doesn't  
   know about these */

typedef void *EdtWaitHandle;
typedef void *EdtKiobuf;
typedef void *EdtPciDev;

typedef volatile unsigned long EdtSpinLock_t;

typedef void *EdtPage;


asmlinkage void    edt_printk (char *fmt, ...);

asmlinkage void    edt_free_irq (unsigned int irq, void *dev_id);

asmlinkage void   *edt_memcpy (void *dest, void *src, size_t length);
asmlinkage void   *edt_memset (void *dest, int c, size_t length);

asmlinkage int     edt_verify_area_read (const void *addr, unsigned long size);
asmlinkage int     edt_verify_area_write (const void *addr, unsigned long size);

asmlinkage unsigned long edt_copy_to_user (void *to, const void *from, unsigned long n);

asmlinkage unsigned long edt_copy_from_user (void *to, const void *from,
				  unsigned long n);

asmlinkage unsigned long
edt_copy_pages_to_user (void *to, EdtPage * from, unsigned long n);

asmlinkage unsigned long
edt_copy_pages_from_user (EdtPage * sg, void *from, unsigned long n);

asmlinkage void    edt_wake_up (EdtWaitHandle p);

asmlinkage void    edt_wake_up_interruptible (EdtWaitHandle p);

asmlinkage void    edt_interruptible_sleep_on (EdtWaitHandle p);

asmlinkage long    edt_interruptible_sleep_on_timeout (EdtWaitHandle p,
					    signed long timeout);

asmlinkage void    edt_spin_lock (EdtSpinLock_t * lock);

asmlinkage void    edt_spin_unlock (EdtSpinLock_t * lock);

asmlinkage void    edt_start_intr_critical (EdtSpinLock_t * lock, unsigned long *flags);

asmlinkage void    edt_end_intr_critical (EdtSpinLock_t * lock, unsigned long flags);

asmlinkage pid_t   edt_get_current_pid (void);
asmlinkage void *   edt_get_current(void);

asmlinkage unsigned long edt_virt_to_bus (volatile void *adr);
asmlinkage unsigned long edt_virt_to_phys (volatile void *adr);
asmlinkage unsigned long edt_phys_to_virt (unsigned long physaddr);

asmlinkage int     edt_register_interrupt (int irq, void *dev_id, char *name);

asmlinkage void     edt_delay(int msecs);
asmlinkage void     edt_udelay(int usecs);

asmlinkage int    user_function(void *dev, void *data, u_int length);

/*********************************/
/* structure used to initialize each board */
/********************************/

typedef struct _board_values
{
    int     boardtype;
    EdtPciDev dev;
} BoardValues;

asmlinkage int
edt_unmap_pci_memory(caddr_t pMem);

asmlinkage int
edt_map_pci_board (void * bvp,
		   unsigned char *irq_p,
		   caddr_t * pMem,
		   off_t * pMem2Size,
		   caddr_t *pMemPhys,
		   caddr_t *pMemPhys2,
		   int unit);

asmlinkage int     edt_init_module_type (int subid, BoardValues * bvp);

/********************************************/
/* Memory access functions                  */
/********************************************/

asmlinkage void   *edt_kmalloc (size_t, char *tag);
asmlinkage void   *edt_kmalloc_dma (size_t, char *tag);
asmlinkage void    edt_kfree (const void *ptr, char *tag);
asmlinkage EdtPage edt_get_free_kernel_page (char *tag);
asmlinkage EdtPage edt_get_free_dma_page (char *tag);
asmlinkage EdtPage edt_get_free_page (unsigned int mask, char *tag);
asmlinkage void    edt_free_page (EdtPage page, char *tag);

asmlinkage void * edt_get_free_pages (int size, char *tag);
asmlinkage void * edt_get_free_dma_pages (int size, char *tag);

asmlinkage void    edt_free_pages (void * addr, int size, char *tag);

asmlinkage int edt_mem_map_alloc_kernel(EdtMemMapping *map, int size);

asmlinkage int edt_has_swiotlb(void);


asmlinkage void    edt_vfree (void *addr, char *tag);

asmlinkage void   *edt_vmalloc (unsigned int size, char *tag);

asmlinkage unsigned long edt_vmalloc_to_bus (unsigned long adr);



asmlinkage unsigned long edt_kvirt_to_phys (unsigned long adr);

asmlinkage unsigned long edt_kvirt_to_bus (void * adr);

asmlinkage unsigned long edt_uvirt_to_bus (unsigned long adr);

asmlinkage unsigned long edt_page_to_bus ( EdtPage page);

asmlinkage unsigned long edt_kernel_to_phys(void *p);

asmlinkage void    edt_open_wait_handle (EdtWaitHandle * h);

asmlinkage void    edt_close_wait_handle (EdtWaitHandle * h);

asmlinkage void    edt_init_spinlock (EdtSpinLock_t * lockp);

asmlinkage EdtSpinLock_t * edt_create_spinlock(void);

asmlinkage unsigned long
edt_pci_map_region(void *dev, void *addr, int size, int write);
asmlinkage void
edt_pci_unmap_region(void *dev, void *addr, int size, int write);

asmlinkage void
edt_pci_dma_sync_single_for_device(void *dev, caddr_t addr, int size, int write);

asmlinkage void
edt_pci_dma_sync_single_for_cpu(void *dev, caddr_t addr, int size, int write);

asmlinkage int
edt_pci_map_sg(void *dev, EdtMemMapping *map);

asmlinkage int
edt_pci_unmap_sg(void *dev, EdtMemMapping *map);

asmlinkage void
edt_pci_sync_sg(void *dev, EdtMemMapping *map);

asmlinkage bus_addr32_t
edt_pci_map_get_sg_val(EdtMemMapping *map, int index, int *lenp);

asmlinkage void edt_destroy_spinlock(EdtSpinLock_t *lockp);

asmlinkage int     edt_map_user_sglist (EdtMemMapping * map, unsigned int *Sglist,
			     int size);

asmlinkage void    edt_unmap_user_sglist (EdtMemMapping * map);

asmlinkage void   *edt_mem_map_page_virtual (EdtMemMapping * map, int pageno);

asmlinkage void
edt_unmap_sglist_page(EdtMemRange *mrp, int entry, void *pci_dev, 
		       int own_sg_mem, int writeflag);
asmlinkage int
edt_map_sglist_page(EdtMemRange *mrp,  EdtPCISg *mlp, void *pci_dev,
		    int entry, int blocklength);

/*
 * function to avoid race conditions while sleeping per
 * Linux Device Drivers 2nd Edition.
 */

asmlinkage int     edt_wait_event_interruptible_timeout (EdtWaitHandle * handle,
					      volatile unsigned short *arg1,
					      unsigned short arg2,
					      signed long timeout);
asmlinkage int     edt_wait_event_interruptible (EdtWaitHandle * handle,
				      volatile unsigned short *arg1,
				      unsigned short arg2);


asmlinkage int     edt_wait_event_timeout (EdtWaitHandle * handle,
					      volatile unsigned short *arg1,
					      unsigned short arg2,
					      signed long timeout);
asmlinkage int     edt_wait_event (EdtWaitHandle * handle,
				      volatile unsigned short *arg1,
				      unsigned short arg2);


asmlinkage int     edt_mem_map_open (EdtMemMapping * map, unsigned int maxsize);

asmlinkage void    edt_mem_map_release (EdtMemMapping * map);

asmlinkage int     edt_map_user_mem_map (int rw,
			      EdtMemMapping * kbhandlep,
			      user_addr_t useraddr, size_t len);

asmlinkage int    edt_mem_map_setup_os_sg(EdtMemMapping *map);
asmlinkage int    edt_mem_map_clear_sg(EdtMemMapping *map);

asmlinkage unsigned long edt_mem_map_page_to_bus (EdtMemMapping * map, int pagen);

asmlinkage unsigned long edt_mem_map_byte_offset (EdtMemMapping * kbhandle);

asmlinkage unsigned long edt_mem_map_bus_address (EdtMemMapping * map, int pagen);

asmlinkage void    edt_unmap_mem_map (EdtMemMapping * map);

asmlinkage int     edt_mmap_vmap_address (void *dev,
					  void *vma,
			       caddr_t  MemBasePhys,
			       caddr_t  Mem2BasePhys);

asmlinkage void *  edt_virt_to_page(void *vaddr);
asmlinkage void *  edt_get_dmamem_page(void *dev, u_int offset);
asmlinkage void *  edt_get_sglistmem_page (void *devp, u_int pagen);

asmlinkage void    edt_do_gettimeofday (struct timeval *);
asmlinkage void    edt_pci_write_config (EdtPciDev dev, unsigned int addr, unsigned int value);
asmlinkage unsigned int edt_pci_read_config (EdtPciDev dev, unsigned int addr);

asmlinkage int     edt_HZ (void);		/* returns the kernels HZ value */

asmlinkage int edt_sprintf(char * buf, const char * fmt, ...);

asmlinkage int edt_needs_kernel_buffers(int npages);

/* Functions referring to the use of pages
   reserved at module load time, for use with
   swiotlb systems */

asmlinkage int edt_use_reserved_pages(void);
asmlinkage int edt_send_sig(pid_t pid, int signal);


asmlinkage int edt_reserved_pages_open(void);

asmlinkage void lock_setup_dma(void);

asmlinkage void unlock_setup_dma(void);


#ifdef P53B

#define MODULE_NAME "p53b"

#else

#define MODULE_NAME "edt"

#endif

/* private fops functions called from edt_lnx_kernel.c */

asmlinkage int     edt_private_open (void *dev_ptr);
asmlinkage int     edt_private_release (void *dev_ptr);
asmlinkage ssize_t     edt_private_read (void *dev_ptr,
			  char *user_buf, size_t count, loff_t * seek);
asmlinkage ssize_t     edt_private_read_nodma (void *dev_ptr,
			  char *user_buf, size_t count, loff_t * seek);
asmlinkage ssize_t     edt_private_write (void *dev_ptr,
			   const char *user_buf, size_t count, loff_t * seek);
asmlinkage int     edt_private_ioctl (void *dev_ptr, unsigned int cmd,
				      unsigned long arg, int compat32_flag);
asmlinkage int     edt_private_mmap (void *dev_ptr, void *vma);

asmlinkage int     edt_private_init_module (BoardValues bv[], int nboards,
				 unsigned int memsize);

asmlinkage int     edt_intr(void *dev_id);
asmlinkage int     edt_intr_process(void *dev_id);

asmlinkage void    edt_private_cleanup_module (void);

asmlinkage void   *edt_get_dev_from_minor (int minor);

asmlinkage caddr_t edt_private_get_membase(void *dev_ptr, int range);

/* this routine returns the interrupt status(es) - flags ored together */

#define INT_TYPE_PGDONE    1  /* page done interrupt */
#define INT_TYPE_DEVINT    2  /* interrupt from interface */
#define INT_TYPE_RDY       4  /* scatter-gather empty */
   
asmlinkage int     edt_check_interrupt(void *dev_id);

/* Routines to retrieve the IRQs used by EDT boards for use by 
   other kernel routines (i.e. RTLinux)
*/

#define MAX_IRQS  16

typedef struct _irqdata {
    int irq;
    void *dev;
    char *name;
} EdtIrqData;

asmlinkage int edt_get_n_irqs(void);
asmlinkage int edt_get_irq_value( const int index);
asmlinkage void * edt_get_dev_from_irq(int irq);

asmlinkage int edt_take_interrupt(int irq, void *dev_id);
asmlinkage int edt_restore_os_interrupt(int irq, void *dev_id);
/* these routines give access in the kernel to the most 
   recent buffer finished */

/* Note that the done buffer pointer will normally be user-memory 
   unless kernel_buffers is true */

asmlinkage void * edt_get_done_buffer(void *dev);
asmlinkage int edt_get_done(void *dev);
asmlinkage int edt_get_curbuf(void *dev);
asmlinkage int edt_get_use_kernel_buffers(void *dev);
asmlinkage void edt_set_ignore_intr(void *dev, int state);
asmlinkage int edt_get_ignore_intr(void *dev);
asmlinkage void edt_trace(void *dev, int value);

asmlinkage void edt_enable_linux_spinlocks(void *dev);
asmlinkage void edt_disable_linux_spinlocks(void *dev);

asmlinkage void edt_take_over_lock(void *devid, void * newlock);
asmlinkage void edt_restore_lock(void *devid);

typedef void (*Lock_f) (void *lock) ;
asmlinkage void edt_replace_lock_unlock(Lock_f lock, Lock_f unlock);
asmlinkage void edt_restore_lock_unlock(void);

asmlinkage
void get_and_clear_reg_stats(u_int *sets, u_int *gets);
 
                                                                                      
asmlinkage void **
edt_get_buf_kbuffer_pages(void *devid, int buf, int *npages);
                                                                                      

asmlinkage int
edt_set_debuglevel(void *devid, int level);
/* register a user function with untyped data transfer */

asmlinkage void *  edt_register_user_data(void *dev, void *u_data);

asmlinkage void    edt_register_user_function(void *dev, edt_user_function f);

asmlinkage void    edt_register_dma_callback(void *dev, int (*dma_cb)(void *, void **pages, u_int npages, u_int size));

                                                                                

/*******************************************
 * Exported serial routines 
 ******************************************/

asmlinkage void edt_serial_write(void* devid, char *buf, int size, int unit, 
				 int flags);

asmlinkage u_int edt_serial_read(void* devid, char *buf, int size, int unit);

asmlinkage int edt_serial_wait(void * devid, int mwait, int count);

asmlinkage int  edt_serial_init(void * devid, int unit);

#endif
