/*******************************************************************************
 *	COPYRIGHT (C) 2012
 *	UNIVERSITY OF CALIFORNIA
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *	@file gpAoSrtHostWoofer.h
 *
 * 	@version  $Id: gpAoSrtHostWoofer.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the host woofer interface controller component (on the SRT side).
 *
 *	@b DESCRIPTION:
 *	This is the header file for the host woofer interface controller component (on the SRT side).
 *
 *	@b AUTHOR:
 *	MRR		DATE: 8/22/2012
 *
 *	@b REVISIONS:
 *
 ********************************************************************************/

#ifndef _HOSTICON_H
#define _HOSTICON_H		1

static boolean	WooferquitThread = false;

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
