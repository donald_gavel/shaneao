/*******************************************************************************
 *
 *	COPYRIGHT (C) 2010
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpUtGmbHrt.h
 *
 *	@brief 
 *	This header file contains TLC/Gmb-related constants and typedefs for the HRT.  
 *
 *	@b DESCRIPTION:
 *	This header file contains TLC/Gmb-related constants and typedefs for the HRT.  
 *	@par
 *	IMPORTANT: GPI_MAX_STRING_LEN, GPI_STATE, and AOC_HRT_GMB_WRITE_CMD_STATUS
 *	were taken from gp_UtGeneral.h and gpAoGmb.h because including those filea
 *	was causing major incompatibility problems with RTLinux
 *	@par
 *
 *	@b AUTHOR:
 *	DWP		DATE: 07/30/2008
 *
 *	@b FUNCTION NAMES:
 *	
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _GP_UT_GMB_HRT_H
#define _GP_UT_GMB_HRT_H		1

#define GPI_MAX_STRING_LEN 128   

/*!
 *  State Values: These are the possible statue values of the command
 *  WARNING: if the number of states change, 
 *	then change gpStatusStrings
 */
typedef enum
{
	GPI_STATE_IDLE,
	GPI_STATE_BUSY,
	GPI_STATE_ERROR
} GPI_STATE;

#define AOC_HRT_GMB_WRITE_CMD_STATUS( cmd, errCode, newState, errStr )								\
{																									\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].state = newState;										\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrCode = errCode;									\
	strncpy( hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg, errStr, GPI_MAX_STRING_LEN-1 );	\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].stateErrMsg[ GPI_MAX_STRING_LEN-1 ] = '\0';				\
	hrtFileDataShmPtr->cmdStatusList[ cmd ].modified = 1;											\
	gpAoHrtFileData_Put( eFileTypeCmdStatus );														\
}

#endif

