/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtFileData.h
 *
 * 	@version  $Id: gpAoHrtFileData.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the file data handler on the HRT side.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the file data handler on the HRT side.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/26/2008
 *
 *
 *	@b REVISIONS:
 *	
 *	09/06/2007: Initial Critical Design Version
 *
 *******************************************************************************
 */


#ifndef _FILEDATA_H
#define _FILEDATA_H		1

//=====================================================================================================================
// Enums

enum { eTlcErrWfsCCD, eTlcErrDarkCorner, eTlcErrAoSettled, eTlcErrAoTooLittleLight, eTlcErrAoTooMuchLight, 
	   eTlcErrPupTooLittleLight, eTlcErrPupTooMuchLight, eTlcErrCalSnr, eTlcErrClipTT, eTlcErrClipWfr, 
	   eTlcErrClipTwt, eTlcErrNum };
enum { eTlcErrNone, eTlcErrError, eTlcErrLowLow, eTlcErrLow, eTlcErrHigh, eTlcErrHighHigh, eTlcErrSettled, eTlcErrPersistentClip };


// Defines

//=====================================================================================================================
// Typedefs


typedef struct
{
	int			modified;
	int			subCmdFlag;
	GPI_STATE	state;								// State of the command: GPI_STATE_IDLE, GPI_STATE_ERROR, GPI_STATE_BUSY
	int			stateErrCode;						// Probably not being used....
													//  > Error code associated with the state.
	char		stateErrMsg[GPI_MAX_STRING_LEN];	// Reflects errors due to exe a cmd
} CmdStatus;


// file data shared memory type
typedef struct 
{
	int				systemDebug;						// Overall system debug level - initially read from parms.txt file
	int				tlcOnFlag;							// Indicates whether TLC is on or off - this is NOT read from parms and
														// > defaults to TLC on - to go to host mode (for more messages over the 
														// > socket), use setDebug

	int				watchDogBits;						// Individual bits for watchdogs - see gpAoConstant.h for bit details

	boolean			srtIsUpFlag;						// used to indicate to the HRT that the SRT has come up
	boolean			hrtQuitFlag;						// used to indicate to the HRT that it should quit

	float			r0_estimate;						// Currently not used - set by 'setObservationConfiguration' command
	float			star_I_magnitude;					// Currently not used - set by 'setObservationConfiguration' command
	
	char			allDataAvailFromFile;				// indicates that new data has been read from all files and is available
	char			someDataAvailFromFile;				// indicates that new data has been read from one or more files and is available
	char			dataAvailFromFile[ eFileTypeNum ];	// indicates that new data has been read from a file and is available
	char			dataFileError[ eFileTypeNum ];		// indicates file error during new new data read					

	volatile char	dataShmLockHRT[ eFileTypeNum ];		// used to lock shared memory for writes
	volatile char	dataShmLockSRT[ eFileTypeNum ];		// used to lock shared memory for writes

// Cmd Parameters
    CmdStatus		cmdStatusList[ eCmdCount ];	 		// Command completion status - this is accessible to both HRT and SRT - but SRT sets final status from this

//	parms data
	char		crntFilenames[ eFileTypeNum ][ MAX_FILENAME_LENGTH ];  // current filenames
	char		wfs_ref_and_apertureFilename[ MAX_FILENAME_LENGTH ];   // file name for the aperture and wfs CAL data

//
//	'non-parms' data to be shared with SRT (not stored to disk, NOT protected by dataShmLocks)
//
	short		aoTwtLoopStat;				// (hrt-w srt-r) indicates the status of the tweeter ao loop (open, opening, or closed)
	short		aoWfrLoopStat;				// (hrt-w srt-r)indicates the status of the woofer ao loop (open, opening, or closed)
	short		aoTTLoopStat;				// (hrt-w srt-r)indicates the status of the tip/tilt loop (open, opening, or closed)

	short		BacksAvgN;					// (srt-w hrt-r) # frames to average when determining WFS dark, backgrnd values
	short		FlatsAvgN;					// (srt-w hrt-r) # frames to average when determining WFS flat (gain) values
	short		centAvgN;					// (srt-w hrt-r) # frames to average when determining reference centroids
	short		smAvgN;						// (srt-w hrt-r) # centroids to average when determining a system matrix
	short		MirrorShapeAvgN;			// (srt-w hrt-r) # DM voltages to average when determining flat and sharp mirror values

	short		camFrameRate;				// (srt-w hrt-r) index for camera frame rate in frames per second

	short		camGain;					// (srt-w hrt-r) camera gain (0, 1, 2, or 3)

	float		calSNR_Gain;				// multiplier applied to cal SNR and then multiplied by the ref cents calculated from Cal data

	short		centThresh;					// value centroider subtracts from raw WFS data (then, if result is less than zero,
											// > centroider sets raw data to zero)

	float 		centroidGainX;				// gain to apply to X cetroids (primarily to correct for gain difference between X and Y)
	float 		centroidGainY;				// gain to apply to Y cetroids (primarily to correct for gain difference between X and Y)

	float 		darkCornerCorrectSwitch;	// turns dark level correction (using unilluminated corner pixels) on and off

	float		ttGainStage;				// tip/tilt loop gain, stage
	float		ttGainWfrSurface;			// tip/tilt loop gain, woofer surface

	int			ttSplitSwitch;				// determines how to split TT -- 0 = TT/woofer, 1 = all to TT, 2 = all to woofer
	boolean		ttLQGSwitch;				// turns TT LQG filter on and off -- false = off, true = on
	float		ttWfr9ActMicronsPerMASX;	// P/V microns across 9 illuminated woofer actuators to give 1 MAS tip on sky
	float		ttWfr9ActMicronsPerMASY;	// P/V microns across 9 illuminated woofer actuators to give 1 MAS tilt on sky

	float		ttCompDelay_microSecs;		// tip/tilt computational delay in microseconds

	double		ttSeeing;					// seeing as reported by host (or parms file for testing)
	double		ttWindPower;				// wind power as reported by host (or parms file for testing)
	double		ttStarBrightness;			// star brightness as reported by host (or parms file for testing)

	double		ttLQGAllParams[NS][NW][NR][N_ALLPARAMS];  // tables of all LQG filter parameters and Kalman gains
// 	the LQG filter parameters lookup table selection ranges are used as follows:
// 	> if (							 (TLC_value < range[1])) set index to 0		  -- NOTE: a test for (TLC_value < range[0]) is NOT done
// 	> if ((TLC_value >= range[1]) && (TLC_value < range[2])) set index to 1
// 	> ...
// 	> if ((TLC_value >= range[N - 1])) 						 set index to (N - 1) -- NOTE: a test for (TLC_value > range[N]) is NOT done
	double		ttLQGAllRanges[3][PARM_DIM + 1];  // LQG filter parameters lookup table selection ranges

	double 		focusZernikeVector[MAX_WOOFER_ACTS];			// Zernike offload vector for focus, for M2
	double 		m1ZernikeVectors[eM1MaxVals][MAX_WOOFER_ACTS];  // Zernike offload vectors for M1
	int    		m1ReadBuffNdx;				// read index into triply buffered M1 offload buffer
	int    		m2ReadBuffNdx;				// read index into triply buffered M2 offload buffer
	float  		crntM1Vals[3][eM1MaxVals];  // M1 offload data, triply buffered for asynchronous use by the SRT
	float  		crntM2Vals[3][eM2MaxVals];  // M2 offload data, triply buffered for asynchronous use by the SRT

// 	this structure contains the following data for each sub-aperture:
//		short	subApRow	row of sub-apertures that this quad-cell is in (-1 = end of list)
//		short	pixRows[2]	pixel rows, within the subApRow, that the 4 pixels are in
//		short	pixCols[2]	pixel columns that the 4 pixels are in
	QuadCellDescType quadCellDescs[MAX_NUM_SUBAPS];
	CMDescType CMDescs[MAX_NUM_SUBAPS];	// mrr DM

	// Aperture data and results read in from file './config/config/aperture.fits'
	float		aperturePattern[NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];
	int			subApMap[ MAX_NUM_SUBAPS ];  // maps index into full array position

	short		numSubaps;			// number of wavefront sensor (WFS) sub-apertures
	short		numCentroids;		// number of centroids
	
	char		doneLongDiskWrite;  // indicate that SRT is writing data, as requested by hrt

//	wfs bkGrnd data
	short		applyBackData;																						   // apply back data ( 0: no, 1: yes )
	short		wfsBkGrndData[MAX_NUM_FRAME_RATES][MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current back data

//	wfs dark data
	short		applyDarkData;																						 // apply dark data ( 0: no, 1: yes )	
	short		wfsDarkData[MAX_NUM_FRAME_RATES][MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];	 // current dark data

//	wfs flat data
	short		applyFlatData;																						 // apply flat data ( 0: no, 1: yes )
	float		wfsFlatData[MAX_NUM_FRAME_RATES][MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];	 // current flat data
	float		flatTestStepsData[FLATTER_NUM_STEPS * 4][FLATTER_NUM_STEPS * 4];  //XXXdwp110621 -- for flat field testing

//	refcent data - at this level - the values are all in 'mas'
	float		shm_refCentsQ[MAX_NUM_CENTROIDS];			   // base reference centroids -- used to set hrt_currRefCentsQ
	float		shm_offCentsQ_offsets[MAX_NUM_CENTROIDS];	   // offset centroids, being passed in from host
	float		shm_offCentsQ_Cal_RePhase[MAX_NUM_CENTROIDS];  // re-phase centroids, read in from file from host
	float		shm_offCentsQ_Cal[MAX_NUM_CENTROIDS];		   // offset centroids, being passed in from CAL subsystem
	float		shm_offCentsQ_Apodizer[9][MAX_NUM_CENTROIDS];  // offset centroids, for apodizers - read in from files
															   // >  1-8 are used - skip '0'

//	NOTE! The following items are stored in their disk files in 'phase' format, unless specified as volts
//
//	twt flat/shape data
	float		twtLabFlat[MAX_TWEETER_ACTS];											// current twt lab flat voltage values
	float		twtSysFlat[MAX_TWEETER_ACTS];											// current twt sys flat voltage values
	float		twtLabFlat_volts[MAX_TWEETER_ACTS];										// Note - these are filled in before data is written out to file
	float		twtSysFlat_volts[MAX_TWEETER_ACTS];										// Note - these are filled in before data is written out to file
	float		twtDmShape[MAX_TWEETER_ACTS];											// current twt dm shape voltage values
	float		twtDmShape_volts[MAX_TWEETER_ACTS];										// Note - these are ONLY filled in before data is written out to file

	float		twtHostDmOffset[MAX_TWEETER_ACTS];										// current twt host offset phase values (from file)

//	NOTE! The following items are stored in their files in 'phase' format, unless specified as volts
//
//	wfr flat/shape data
	float		wfrLabFlat[MAX_WOOFER_ACTS];											// current wfr lab flat voltage values
	float		wfrSysFlat[MAX_WOOFER_ACTS];											// current wfr sys flat voltage values
	float		wfrLabFlat_volts[MAX_WOOFER_ACTS];										// Note - these are ONLY filled in before data is written out to file
	float		wfrSysFlat_volts[MAX_WOOFER_ACTS];										// Note - these are ONLY filled in before data is written out to file
	float		wfrDmShape[MAX_WOOFER_ACTS];											// current wfr dm shape voltage values
	float		wfrDmShape_volts[MAX_WOOFER_ACTS];										// Note - these are ONLY filled in before data is written out to file

	float		wfrHostDmOffset[MAX_WOOFER_ACTS];										// current twt host offest voltage values (from file)

//	the following are used to transfer in the Coeff values for the DMs 
	float		twtPhaseToVoltsCoeff[ MAX_NUM_DM_COEFF ][ MAX_TWEETER_ACTS ];			// Coeff data for translation of phaseto volts Wfr (plus min/max voltage)
	float		twtMinVolts[ MAX_TWEETER_ACTS ];										// Min voltage data  Twt
	float		twtMaxVolts[ MAX_TWEETER_ACTS ];										// Max voltage data  Twt
	float		twtMinMicrons[ MAX_TWEETER_ACTS ];										// Min micron range  Twt
	float		twtMaxMicrons[ MAX_TWEETER_ACTS ];										// Max micron range  Twt

	float		wfrPhaseToVoltsCoeff[ MAX_NUM_DM_COEFF ][ MAX_WOOFER_ACTS ];			// Coeff data for translation of phaseto volts Twt (plus min/max voltage)
	float		wfrMinVolts[ MAX_WOOFER_ACTS ];											// Min  voltage data  Wfr
	float		wfrMaxVolts[ MAX_WOOFER_ACTS ];											// Max  voltage data  Wfr
	float		wfrMinMicrons[ MAX_WOOFER_ACTS ];										// Min  micron range  Wfr
	float		wfrMaxMicrons[ MAX_WOOFER_ACTS ];										// Max  micron range  Wfr

	float		ttPhaseToVoltsCoeff[ MAX_NUM_DM_COEFF ][ MAX_TT_ACTS ];					// Coeff data for translation of phaseto volts TT (plus min/max voltage)
	float		ttMinVolts[ MAX_TT_ACTS ];												// Min  voltage data  TT
	float		ttMaxVolts[ MAX_TT_ACTS ];												// Max  voltage data  TT
	float		ttMinMilliSec[ MAX_TT_ACTS ];											// Min  milliSecRange TT
	float		ttMaxMilliSec[ MAX_TT_ACTS ];											// Max  milliSecRange TT

	short		numTwt_Phase2VoltsCoeff;												// These are set from the input file ( num coeff )
	short		numWfr_Phase2VoltsCoeff;
	short		numTT_Phase2VoltsCoeff;

	// Following flag indicates that the 'cycle actuator' is active	
	char		cycleActActive;															// flag indicating whether the "cycle Act"  command is active or not

	float		FTR_reconFilter[5][ APERTURE_GRID_AREA ];								// index 0: Real X, 1: real Y, 2: img X, 3: img Y, 4: Denom

	int			FTR_wfrVecSize;															// Size of the FTR Woofer Vector (num elements) - read from PARMS FILE

	float		FTR_twtCleanup[MAX_TWEETER_ACTS][MAX_TWEETER_CLEAN_MODES];				// tweeter mode cleanup matrix -- indexes backwards due to IDL
	float		FTR_wfrControlMatrix[ MAX_WOOFER_ACTS ][ MAX_WFR_VEC_SIZE * 2 ];		// control matrix used for woofer FTR reconstruction
	float		FTR_wfrCleanup[MAX_WOOFER_ACTS][MAX_WOOFER_CLEAN_MODES];				// woofer mode cleanup matrix -- indexes backwards due to IDL
	long		FTR_wfrVectorList[ MAX_WOOFER_ACTS ];									// modes to extract for woofer cm multiply
	long		FTR_twtZeroList[ MAX_WOOFER_ACTS * 2 ];									// tweeter modes to zero after woofer extraction ( hermisian ) [ Vector format ]
	float		FTR_TwtInfFuncMask[APERTURE_GRID_SIZEROW][APERTURE_GRID_SIZECOL];
	int			FTR_wfrActMap[MAX_WOOFER_ACTS ];

// modal gains	
	int			selectModalGains[ APERTURE_GRID_AREA ];									// apply optimizer modal gain data ( 0: apply 1.0, 1: apply OFC values 4: apply host values )

	short		offloadM1M2;															// offload M1/M2 mirror data ( 0: niether M1 or M2, 0x1: m1 on, 0x2: M2 on - bit encoded )
	int			offloadZernRotationAngle;												// angle to rotate Zernikes for M1/M2 offload, in degrees
	int			offloadZernFlip;														// indicates whether Zernikes should be flipped in X or not

	float		hostModalGains[ APERTURE_GRID_AREA ];									// Modal gains set by 'Host File'
	float		initialModalGains[ APERTURE_GRID_AREA ];								// Modal gains set by 'Initial File'
	float		ofcOutputGains[ APERTURE_GRID_AREA ];									// Recon opt modal gains that are internally generated - not necessarily applied

	float		param_mode_cleanup_thresh;												// mode cleanup threshold in microns
	float		param_mode_cleanup_gain;												// mode cleanup gain

	float		param_modal_loss_dm;													// modal loss - twt - from param file		
	float		param_modal_loss_tt;													// modal loss - wfr - from param file

// T/T offsets
	float		param_tt_x_offset;														// X offsets applied to TT stage
	float		param_tt_y_offset;														// Y offsets applied to TT stage

// 	Misc 
	short		useWfsSimData;															// use simulated wfs data rather than actual from the wfs camera
	short		applyCalData;															// apply CAL offset Data ( 0: no, 1: yes )
	float		inFuncMinGain;															// Min Gain that will be allowed to be applied
	short		selectorForInFuncMultiplier;											// Set by the selectInFuncGains command - either host defined or all '1's
	int			dmSlavingSwitch;														// turns woofer and tweeter slaving on and off
	float		twtNeighborLimit;														// tweeter inter-actuator difference limit -- see XXXdwpDifferentialActPokeLimit
	float		localWaffleGain;														// gain applied to local waffle suppressor  //XXXdwp110816
	float		overThreshFraction;														// fraction of sub-aps whose intensity must be over the centThresh for frame to be used  //XXXdwp110816

//	Influence Function 
	short		makeInFunc_xMode;														// xPos for mode to test
	short		makeInFunc_yMode;														// xPos for mode to test
	float		makeInFunc_amplitude;
	short		makeInFunc_numAvg;														// number of frames to average when collecting the InFunc Data

	// NOTE!!! - These are written out as a block - DO NOT Seperate!!!!!!!!!!!!!!!!!
	float		makeInFuncTweeterCentAverage[2][ MAX_TWEETER_ACTS ];					// Storage location for 'makeInFuncMode' shape - read from FTR (sine-cosine)
	float		makeInFuncTweeterCalcPhaseAverage[2][ MAX_TWEETER_ACTS ];				// Storage location for 'makeInFuncMode' shape - read from FTR (sine-cosine)
	float		makeInFuncTweeterAppliedPhase[2][ MAX_TWEETER_ACTS ];					// Storage location for 'makeInFuncMode' shape - read from FTR (sine-cosine)

//	TLC error checking range data, read in from ranges.txt on SRT side
	float		rangesWfsCcdTemp[4];  // lowFail, lowWarn, highWarn, and highFail in Kelvins -- HRT doesn't actually need these
	int			rangesCornerCrct[2];  // dark corner correction low, high in DNs
	float		rangesAoSettled;	  // seconds to wait to claim AO loops are settled
	int			rangesAoLight;		  // AO loop high in DNs -- don't need low, because hrt_overThreshFraction is used
	int			rangesPupLight[2];	  // pupil track low, high in DNs
	float		rangesCalSnr;		  // Cal SNR low -- don't need high -- HRT doesn't actually need this
	int			rangesClipPersist;    // number of consecutive counts for DM/TT clipping to persist
//	if a range value gets added or removed, update NUM_RANGES !!

//	TLC error status	// mrr what are these?
	int			tlcErrStatArraySticky[eTlcErrNum];  // array indicating status of TLC errors -- an error sticks if it occurs
	int			tlcErrStatArray[eTlcErrNum];		// array indicating status of TLC errors -- an error doesn't stick
	float		tlcErrCCDTempKelvins;
	int			tlcErrDarkCornerVal;
	int			tlcErrAoLight;
	int			tlcErrPupilLight;
	float		tlcErrCalSnr;
	int			tlcErrClipCounts[3];  // TT, wfr, twt

//	tweeter System Matrix
	short		twtSM_data[NUM_TWT_SM_POKE_LEVELS][MAX_TWEETER_ACTS][NUM_ROWS_IN_CAM * NUM_PIX_IN_ROW];	 // tweeter System Matrix

} FileDataShmType;


// Global Variables


// Global Function Prototypes

	void		gpAoHrtFileData_Init( void );
	void		gpAoHrtFileData_Cmd( short readCmd );

	char		gpAoHrtFileData_Get( short fileType );
	int			gpAoHrtFileData_Put( int fileType );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
