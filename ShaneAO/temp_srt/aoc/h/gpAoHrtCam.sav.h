/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtCam.h
 *
 * 	@version  $Id: gpAoHrtCam.sav.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the WFS camera component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the WFS camera component.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/25/2008
 *
 *	@b REVISIONS:
 *	
 *	
 *	01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _CAMERA_WFS_H
#define _CAMERA_WFS_H		1

//=====================================================================================================================
// Defines

//XXXdwp100427 -- put a flag in config.deps to choose 86 or 128 for NUM_ROWS_IN_CAM_ACTUAL XXX
//XXXdwp101207 #define NUM_ROWS_IN_CAM_ACTUAL		86	// actual number of rows in the WFS camera
#define NUM_ROWS_IN_CAM_ACTUAL		128	// actual number of rows in the WFS camera
#define NUM_PIX_IN_ROW_ACTUAL		128 // actual number of pixels in each row of the WFS camera

#define NUM_SEGS_IN_CAM				16	// number of segments in the camera (CCID66 has 20, but we ROI to 16)
#define NUM_PIX_IN_SEG_ROW			16	// number of pixels in each row of the WFS camera segment

// the actual 128x86 (or 128x128) camera frames are de-interleaved into a 96x96 array to correspond to 48x48 subaps
#define NUM_ROWS_IN_CAM				96	// number of rows in the WFS camera -- for calculations
#define NUM_PIX_IN_ROW				96	// number of pixels in each row of the WFS camera -- for calculations
#define NUM_ROWS_IN_CAM_FOR_SUBAPS	86	// number of rows in the WFS camera -- corresponding to actual sub-aps
#define NUM_PIX_IN_ROW_FOR_SUBAPS	86	// number of pixels in each row of the WFS camera -- corresponding to actual sub-aps
#define ROWS_OFFSET					4	// rows to offset to get NUM_ROWS_IN_CAM_FOR_SUBAPS into NUM_ROWS_IN_CAM
#define PIX_IN_ROW_OFFSET			4	// pixels to offset to get NUM_PIX_IN_ROW_FOR_SUBAPS into NUM_PIX_IN_ROW

#define NUM_CAM_ROWS_PER_SUBAP		2	// number of WFS camera rows per subap row
#define NUM_CAM_COLS_PER_SUBAP		2	// number of WFS camera columns per subap row

#define NUM_SUBAPS_ROWS_PER_FRAME	48	// number of sub-aperture rows per frame
#define FIRST_USED_CAMERA_ROW		0	// first used camera row
#define NUM_UNUSED_CAM_ROWS_TOP		0	// number of unused camera rows, top
#define FIRST_UNUSED_CAM_ROW_BOT	96  // first unused camera row, bottom
#define NUM_UNUSED_CAM_ROWS_BOT		0	// number of unused camera rows, bottom

#define FIRST_CAM_ROW_IN_SUBAP		0	// first camera row containing sub-ap data in each sub-ap (starting at 0)
#define FIRST_CAM_COL_IN_SUBAP		0	// first camera column containing sub-ap data in each sub-ap (starting at 0)

// max number of sub-aperture rows per frame
#define MAX_SUBAP_ROWS_PER_FRAME	(NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP)

// max number of unused camera rows, top
#define MAX_UNUSED_CAM_ROWS_TOP		(NUM_ROWS_IN_CAM / 2)

// max number of unused camera rows
#define MAX_UNUSED_CAM_ROWS_BOT		(NUM_ROWS_IN_CAM / 2)

#define INIT_FRAME_RATE_INDEX		1	// initial camera rate in frames per second ( INDEX! into LEGAL_FRAMES )

#define INIT_CAM_GAIN				1	// initial camera gain (1 or 2)

#define	INIT_TIMEOUT_ERROR			300	// Number of loops for timeout - waiting for INitializers to complete.

// Defines specific to the DMA ring buffers.  
#define WFSnumbufs 4  // number of DMA ring buffers


//=====================================================================================================================
// Enums 

enum 
{
	eEdtBufStatNotInitted,
	eEdtBufStatReadNew,
	eEdtBufStatOkay
};


//=====================================================================================================================
// Typedefs

typedef struct
{
	int	dummy1;
	int	dummy2;

} OptimalParams;


// camera buffer type
typedef struct
{
	// the unused camera data, top
	unsigned short	unusedDataTop[ MAX_UNUSED_CAM_ROWS_TOP ][ NUM_PIX_IN_ROW ];
	// the camera data with subapertures (middle [MAX_SUBAP_ROWS_PER_FRAME * NUM_CAM_ROWS_PER_SUBAP] camera rows)
	unsigned short	usedCamData[ MAX_SUBAP_ROWS_PER_FRAME ][ NUM_CAM_ROWS_PER_SUBAP ] [ NUM_PIX_IN_ROW ];
	// the unused camera data, bottom
	unsigned short	unusedDataBot[ MAX_UNUSED_CAM_ROWS_BOT ][ NUM_PIX_IN_ROW ];
} CamBuffType;

// camera data type
typedef struct
{
	short			cmdStart;							 // index (into CmdTable[]) of command to be started or eCmdNone
	short			cmdCrnt;							 // index (into CmdTable[]) of command currently running or eCmdNone

	short			cmdIntParam[ MAX_NUM_CMD_PARAMS];	 // integer parameter for command, if it has one
	float			cmdFloatParam[ MAX_NUM_CMD_PARAMS];	 // float parameter for command, if it has one

	boolean			edtBufStat;							 // indicates the status of the EDT buffers in RTL mode
	boolean			intrIgnore;							 // indicates SMC camera interrrupt should be ignored 
														 // > (because we're doing serial comms)

	short			camWriteNdx;						 // index into camBuffNdxs[] for buffer being written to
	CamBuffType		camBuffs[NUM_HRT_DATA_BUFFS];		 // the camera frame buffers

	unsigned short  simDataBuffs[WFSnumbufs][NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW];  // simulated wfs data
	boolean			simFileRead;						 // indicates whether a simulated wfs data file has been read in

	int				rawDispWriteBuffNdx;				 // the raw display buffer currently being written
	int				rawDispReadBuffNdx;					 // the raw display buffer currently being read
	unsigned short	rawDispBuffs[2][NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL];  // the raw display buffers
} CamDataType;


//=====================================================================================================================
// Global Variables

#if ( TIMER_TEST != 0 )
extern long camFrameID;  // a sequential counter incremented every time a valid frame is sent to the centroider
#endif

extern int numSubapRowsPerFrame;		// number of sub-aperture rows per frame
extern int firstUsedCamRow;				// first used camera row

extern int firstUnusedCamRowTop;		// first unused camera row, top
extern int numUnusedCamRowsTop;			// number of unused camera rows, top
extern int firstUnusedCamRowBot;		// first unused camera row, bottom
extern int numUnusedCamRowsBot;			// number of unused camera rows, bottom

extern int WFS_fd;						// WFS centroid camera shared memory file descriptor

extern short hrt_useWfsSimData;			// flag indicating whether to use simulated wfs data ( 0: no, 1: yes )

extern short hrt_camFrameRate;			// camera frame rate in frames per second
extern short hrt_camGain;				// camera gain (0, 1, 2, or 3)

extern short CamFrameRateNdx;			// index for current camera rate into LegalFrameRates[]
// moved to gpAoSrtCamMngr.h  extern const short LegalFrameRates[];

extern int WFS_irq;
extern void* WFS_dev;

#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
extern int slowComputerCatchUp;
#endif

//=====================================================================================================================
// Global Function Prototypes
//
void* gpAoHrtCam( void *arg );
void  gpAoHrtCam_IntrInit( void );
void* gpAoHrtCam_DrvrWatch( void *arg );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
