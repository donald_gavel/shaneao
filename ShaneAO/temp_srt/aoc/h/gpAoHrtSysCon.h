/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtSysCon.h
 *
 * 	@version  $Id: gpAoHrtSysCon.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the system controller (sysCon) component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the system controller (sysCon) component.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 12/17/2008
 *
 *
 *	@b REVISIONS:
 *
 * *******************************************************************************
 */

#ifndef _SYSCON_H
#define _SYSCON_H		1

#define HRT_SW_VERSION		"1.3"
#define HRT_SW_DATE			"July 20, 2012"

// Enums

enum  // states when running 'long' commands (i.e., commands that can take several seconds)
{
	eCmdStateSumDarkBack,	// sum wfs dark/back data state
	eCmdStateSumFlat,		// sum wfs flat data state
	eCmdStateFlatWait,		// wait for TT settle after move	
	eCmdStateSumCent,		// sum cent data state
	eCmdStateSumTwt,		// sum Twt data state
	eCmdStateSumWfr,		// sum Wfr data state
	eCmdStateAvg,			// average data (and store to disk) state
	eCmdStateFlatCalcGain,	// calc gain over subaperture
	eCmdStateWaitWrite,		// waiting for SRT write to complete
	eCmdState_None
};


// Global Variables

extern char		 OkayToFinishInit;	   // flag indicating that it is okay for threads to complete their initializations
extern int		 hrtSystemDebugLevel;

#if ((RTL_SYSTEM == 0) || (HRT == 1))
extern rtl_sem_t WFSSem;			   // semaphore for WFS camera thread
extern rtl_sem_t CentSem;			   // semaphore for centroid thread
extern rtl_sem_t ReconSem;			   // semaphore for recon thread
extern rtl_sem_t FftwPlanSem;		   // semaphore for fftw plan mutual exclusion
#endif

extern long	CentDataAvailFlag;	 // used to alert cent thread that data is available
extern long	ReconDataAvailFlag;  // used to alert recon thread that data is available

extern int DiagFIFOfd;	// fd for FIFO used to send messages to the diagnostic data handler
extern int CmdFIFOfd;	// fd for FIFO used to receive command messages from the host interface

extern FileDataShmType*			hrtFileDataShmPtr; 		// pointer to file data shared memory
extern CamDataType*				hrtCamDataPtr;			// pointer to camera shared memory data
extern CentDataType*			hrtCentDataPtr;			// pointer to centroid shared memory data
extern ReconDataType*			hrtReconDataPtr;		// pointer to recon shared memory data
extern HrtDataFlowControlType*	hrtDataFlowControlPtr;	// pointer to buffer status for HRT data

extern char CamWFSInitDone, CentInitDone, ReconInitDone;  // initalization flags for threads

// thread quit flags
extern boolean QuitCamWFSWatchThread;
extern boolean QuitCamera_WFS_Thread;
extern boolean QuitCentroidThread;
extern boolean QuitReconThread;
extern boolean QuitSysConThread;

// Global Function Prototypes

void SysConSendStrngNoParm(  int msgDebugLevel, char *strngPtr );
void SysConSendStrngStrParm( int msgDebugLevel, char *strngPtr, char* strParam );
void SysConSendStrngIntParm( int msgDebugLevel, char *strngPtr, int intParam );
void SysConSendStrngFltParm( int msgDebugLevel, char *strngPtr, float floatParam );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
