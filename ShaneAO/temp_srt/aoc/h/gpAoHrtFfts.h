/*!
 *
 *	@file gpAoHrtFfts.h
 *
 *	@brief 
 *	This is the header file for the standalone fft units.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the standalone fft units.
 *
 *	@b AUTHOR:
 *	various	DATE: 1/26/2010
 *
 *	@b STATIC (LOCAL) FUNCTION NAMES:
 *
 *	@b EXTERNAL FUNCTION NAMES:
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _FFTS_H
#define _FFTS_H		1

#include "standalone.h"
#include "spiral_fft.h"

/*!
 *	Global Function Prototypes
 */

void fft_r2cf_48( R * R0, R * R1, R * Cr, R * Ci, INT v );
void fft_c2c_48( const R * ri, const R * ii, R * ro, R * io, INT v );

#endif
//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
