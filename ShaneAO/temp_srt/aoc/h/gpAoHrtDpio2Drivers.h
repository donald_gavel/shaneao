/*******************************************************************************
 *
 *	COPYRIGHT (C) 2010
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtDpio2Drivers.h
 *
 * 	@version
 * 	
 *	@brief 
 *	This is the header file for the RTL dpio2 drivers interface component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the RTL dpio2 drivers interface component.
 *
 *	@b AUTHOR:
 *	DWP	DATE: 06/18/2010
 *
 *	@b REVISIONS:
 *	
 *******************************************************************************
 */

#ifndef _DPIO2_RTL_H
#define _DPIO2_RTL_H		1

// Defines

// #define dpio2DevnoWFR	0  // these assignments for the woofer and tweeter are arbitrary; but, of course, they	// mrr DM
// #define dpio2DevnoTWT	1  // > must match the boards the DMs are plugged into (determined by bus order)	// mrr DM

#define dpio2DevnoTWT	0  // > must match the boards the DMs are plugged into (determined by bus order)	// mrr DM

#define dpio2StrobeFreqWFR	(20 * 1000 * 1000)  // the woofer can't handle 50 MHz
#define dpio2StrobeFreqTWT	(50 * 1000 * 1000)  // 50 MHz is the fastest allowable strobe frequency	// mrr DM ?

#define sizeInBytesWFR 	 208

// #define sizeInBytesTWT	8192	// mrr DM
#define sizeInBytesTWT	2048	// mrr DM


// Global Variables

extern unsigned short *dpio2BufferWFR[2];
extern unsigned short *dpio2BufferTWT[2];


// Global Function Prototypes

int  gpAoHrtDpio2Init ( void );
int  gpAoHrtDpio2StartDmaTransfer (int dpio2DeviceNumber, int desc_num, int buffNdx);
void gpAoHrtDpio2Cleanup ( void );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
