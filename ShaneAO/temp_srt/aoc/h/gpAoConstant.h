/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoConstant.h
 *
 * 	@version  $Id: gpAoConstant.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the overal system - it includes general defines
 *	and system specific defines for the AOC component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the overal system - it includes general defines
 *	and system specific defines for the AOC component.
 *	There are also definitions for the different hardware configurations. 
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/25/2008
 *
 *	@b REVISIONS:
 *	
 *	01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _CONSTANT_H
#define _CONSTANT_H			1

#include <stdio.h>
#include <string.h>

#define TEST_INTERVAL 1

//=====================================================================================================================
// Data and Config directory paths and file names
#define DATA_DIR     "/home/palmer25/gpi_root/aoc/data/"
#define CONFIG_DIR   "/home/palmer25/gpi_root/aoc/config/"
#define PARMS_FNAME  "/home/palmer25/gpi_root/aoc/config/parms.txt"
#define RANGES_FNAME "/home/palmer25/gpi_root/aoc/config/ranges.txt"
#define RTF0_FNAME   "/home/palmer25/gpi_root/aoc/config/rtf0"
#define RTF1_FNAME   "/home/palmer25/gpi_root/aoc/config/rtf1"


//=====================================================================================================================
// General Defines ( not system specific )
typedef	signed short boolean;
#define false		 0
#ifndef FALSE
	#define FALSE	 0
#endif
#define true		 1
#ifndef TRUE
	#define TRUE	 1
#endif

#define	TICKS_PER_MICROSEC		2394.0	// CPU clock ticks per microsecond

#define TEXT_BUF_LNGTH			128		// length of text buffers used primarily to set up messages for SysConSendStrngNoParm()

#define TEMP_BUF_LNGTH			128		// length of temporary buffers
#define BIG_TEMP_BUF_LNGTH		1024	// length of bigger temporary buffers

#define OUT_BUF_LNGTH_1			256		// length of output buffers, 1
#define OUT_BUF_LNGTH_2			2024	// length of output buffers, 2
#define OUT_BUF_LNGTH_3			256536L	// length of output buffers, 3

//=====================================================================================================================
// Unit conversions
//

// the native units for the FTR is slope (delta-y over delta-x, where delta-x is a subap measured in microns)
#define MAS__PER__CENT_UNIT						( 890.8416 )	// mas (on sky) per centroid unit
#define	MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT	( 0.78041822 )	// micronSlopePerSubAp per centroid unit
#define MAS__PER__MICRON_SLOPE_PER_SUBAP		( MAS__PER__CENT_UNIT / MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT )  // mas per micronSlopePerSubAp


//=====================================================================================================================
// CPU allocations

#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
#define LINUX_CPU                        0
#define SRT_CPU_MAIN                     1  // SRT Main (including AOC_SERVER) -- constant not currently used
#define SRT_CPU_DIAG                     1  // SRT Diag (including map2slope)  -- constant not currently used
#define SRT_CPU_TIMERS                   1  // SRT Timers
#define HRT_CPU_CAM						 1  // Cam
#define HRT_CPU_CENT					 1  // Cent
#define HRT_CPU_RECON					 2  // Recon
#define HRT_CPU_SYSCON					 1	// SysCon
#define HRT_CPU_RECON_THREADS_START		 4	// recon worker threads -- 8 to 13 for recon; 14 and 15 for recon opt
#define HRT_CPU_RECON_THREADS_NUM        2  // number of recon worker threads (also see FTR_NUM_THREADS in gpAoHrtRecon.h)
#define HRT_CPU_RECONOPT_THREADS_NUM     2  // number of recon optimizer worker threads (also see NUM_OPT_THREADS in gpAoHrtRecon.h)
#else
/*/dwp121018 -- start
#define LINUX_CPU                        0
#define SRT_CPU_MAIN                     1  // SRT Main (including AOC_SERVER) -- constant not currently used
#define SRT_CPU_DIAG                     2  // SRT Diag (including map2slope)  -- constant not currently used
#define SRT_CPU_TIMERS                   3  // SRT Timers
#define HRT_CPU_CAM						 4  // Cam
#define HRT_CPU_CENT					 5  // Cent
#define HRT_CPU_RECON					 6  // Recon
#define HRT_CPU_SYSCON					 7	// SysCon
#define HRT_CPU_RECON_THREADS_START		 8	// recon worker threads -- 8 to 13 for recon; 14 and 15 for recon opt
#define HRT_CPU_RECON_THREADS_NUM        6  // number of recon worker threads (also see FTR_NUM_THREADS in gpAoHrtRecon.h)
#define HRT_CPU_RECONOPT_THREADS_NUM     2  // number of recon optimizer worker threads (also see NUM_OPT_THREADS in gpAoHrtRecon.h)
/ * dwp121018 -- middle -- surprisingly, the following made things way worse * /
#define LINUX_CPU                        0
#define SRT_CPU_MAIN                     1  // SRT Main (including AOC_SERVER) -- constant not currently used
#define SRT_CPU_DIAG                     2  // SRT Diag (including map2slope)  -- constant not currently used
#define SRT_CPU_TIMERS                   3  // SRT Timers
											// skip 4 & 5
#define HRT_CPU_CAM						 6  // Cam
#define HRT_CPU_CENT					 7  // Cent
#define HRT_CPU_RECON					 8  // Recon
#define HRT_CPU_SYSCON					 9	// SysCon
											// skip 10 & 11
#define HRT_CPU_RECON_THREADS_START		12	// recon worker threads -- 12 to 17 for recon; 18 and 19 for recon opt
#define HRT_CPU_RECON_THREADS_NUM        6  // number of recon worker threads (also see FTR_NUM_THREADS in gpAoHrtRecon.h)
#define HRT_CPU_RECONOPT_THREADS_NUM     2  // number of recon optimizer worker threads (also see NUM_OPT_THREADS in gpAoHrtRecon.h)
//dwp121018 -- end;  dwp121020 -- start */
#define LINUX_CPU                        0
#define SRT_CPU_MAIN                     4  // SRT Main (including AOC_SERVER) -- constant not currently used
#define SRT_CPU_DIAG                     8  // SRT Diag (including map2slope)  -- constant not currently used
#define SRT_CPU_TIMERS                  12  // SRT Timers
#define HRT_CPU_CAM						 1  // Cam
#define HRT_CPU_CENT					 5  // Cent
#define HRT_CPU_RECON					 9  // Recon
#define HRT_CPU_SYSCON					13	// SysCon
//dwp121020 IMPORTANT: note the differences in the following -- if you go back to a previous config, add HRT_CPU_STRIDE and HRT_CPU_RECONOPT_THREADS_START or delete 
//dwp121020 IMPORTANT: > them from the code (grep on dwp121020); also, if you go back to a previous configuration, change the following 2 lines at the beginning 
//dwp121020 IMPORTANT: > of /etc/rc.d/rc.sysinit (which is also linked from /etc/rc.sysinit) back to:
//dwp121020 IMPORTANT: > o	/bin/bindProc 1 0xf
//dwp121020 IMPORTANT: > o	/bin/bindProc $$ 0xf
#define HRT_CPU_RECON_THREADS_START		 2	// recon worker threads -- 12 to 17 for recon; 18 and 19 for recon opt
#define HRT_CPU_RECONOPT_THREADS_START	 3	// recon worker threads -- 12 to 17 for recon; 18 and 19 for recon opt
#define HRT_CPU_STRIDE					 4	// separation that puts logical CPU numbers on the same chip -- used only for assigning processors
#define HRT_CPU_RECON_THREADS_NUM        6  // number of recon worker threads (also see FTR_NUM_THREADS in gpAoHrtRecon.h)
#define HRT_CPU_RECONOPT_THREADS_NUM     2  // number of recon optimizer worker threads (also see NUM_OPT_THREADS in gpAoHrtRecon.h)
//dwp121020 -- end */
#endif

// ----------------------------------------------------------------------
//   TMP-SRT ( these are on timers and recheck as indicated below - they do not run continuously )
// ----------------------------------------------------------------------
// 	Notes:
//  DUMPDATA (gpAoSrtHostDiag_DumpData)     Wakes up once per millisec, to test if diag dump is required.
//  map2Slope                               Wakes up once per 100usec, to test if mapping is required.
//  	(Note this thread is used by both CAL map->slopes (cal update) and TLC map to offsets command.)
// ---------------------------------------------------------------------- 

#define	DEFAULT_SOCKET_PORT			15000

//=====================================================================================================================
// Platform Characteristics ( Set these into the '$AOC_ROOT/config/config.deps' file as well if you create any!!! )
// Platfom type and characteristics`
//			 Bit encode!
//
//#define tick					0	// mrr

#define WFS_REAL_GPI		 	0x00000002
#define WFS_SIM_48X48		 	0x00000008

#define	WFR_GPI_121			 	0x00000400
#define	WFR_SIM_121			 	0x00000800

#define	TWT_SIM_4K			 	0x00002000	// mrr 4K
#define	TWT_BMC_4K			 	0x00004000	// mrr 4K
#define TWT_TYPE				0x00004000	// mrr	// done so Visual Studio will show the right stuff

#define MAX_NUM_DM_COEFF	 	5

#define NUM_SRT_DISPLAY_BUFS 	3	// Number of availble SRT bufs for display data transfer

// This defines the number of data buffers for ALL data save types except PSD  (Camera, centroid, recon, volts, etc)
#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
#define NUM_HRT_DATA_BUFFS	 	1600
#else
#define NUM_HRT_DATA_BUFFS	 	16000
#endif

// This defines the number of data buffers for PSD types
#define NUM_HRT_PSD_BUFFS	 	10		

#define PI					 	3.141592654
#define	TWO_PI				 	(2.0 * PI)
#define	RAD					 	(TWO_PI / 360.0)

// Default 'display' update rate
#define DEFAULT_DISP_RATE	 	1

// Max number of parameters that can be passed to a command
#define MAX_NUM_CMD_PARAMS	 	3

// Max number of fits file dimensions that are currently supported
#define MAX_NUM_FITS_FILE_DIM	10

#define	M2_UPDATE_TIME			10    // 100 msecs, so 10 times per sec
#define TLC_ERRSTAT_TIME		10    // 100 msecs, so 10 times per sec
#define WATCH_DOG_TIME		 	2000  // every 20 seconds

#define WatchDog_ReconBit	 	0x1
#define WatchDog_CamBit		 	0x2
#define WatchDog_CentBit	 	0x4

// Based on bits available above
#define WatchDog_AllBits	 	0x07

//=====================================================================================================================
// printout handling
//

#if (RTL_SYSTEM == 0)
// these _PRINTF_ macros are only used when one or more debug compiler flags are set, 
// > which isn't possible in RTL mode
#define HRT_PRINTF_MAX1	  if ( hrtSystemDebugLevel >= 2 ) (void)printf( "\n" ),			(void)printf
#define HRT_PRINTF_MIN	  if ( hrtSystemDebugLevel >= 1 ) (void)printf( "\n" ),			(void)printf
#define HRT_PRINTF		  								  (void)printf( "\n" ),			(void)printf		
#define HRT_PRINTF_NOCRLF if ( hrtSystemDebugLevel >= 0 ) 								(void)printf		
#define HRT_PRINTF_ERR	  								  (void)printf( "\nERROR " ),	(void)printf
#define HRT_PRINTF_WARN	  if ( hrtSystemDebugLevel >= 0 ) (void)printf( "\nWARNING " ),	(void)printf
#define SRT_PRINTF_MAX1	  if ( srtSystemDebugLevel >= 2 ) (void)printf( "\n" ),			(void)printf
#define SRT_PRINTF_MIN	  if ( srtSystemDebugLevel >= 1 ) (void)printf( "\n" ),			(void)printf
#define SRT_PRINTF		  								  (void)printf( "\n" ),			(void)printf
#define SRT_PRINTF_NOCRLF if ( srtSystemDebugLevel >= 0 )								(void)printf		
#define SRT_PRINTF_ERR	  								  (void)printf( "\nERROR " ),	(void)printf
#define SRT_PRINTF_WARN	  if ( srtSystemDebugLevel >= 0 ) (void)printf( "\nWARNING " ),	(void)printf
#endif

enum
{
	eDebugLevelErr = -2,
	eDebugLevelWarn,
	eDebugLevelNone, 
	eDebugLevelMin, 
	eDebugLevelMax 
};

//=====================================================================================================================
// Defines
//

#define	EXEC_SUB_CMD( cmdStr, cmdNdx, timeOut )							\
{																		\
	int waitFlag = 0;													\
	gpAoSrtHostICon_ProcCmd( cmdStr, 1);								\
	while( srtFileDataShmPtr->cmdStatusList[ cmdNdx ].modified == 0)	\
	{																	\
		if( waitFlag++ > timeOut)										\
			break;														\
		sleep( 1 );														\
	}																	\
	srtFileDataShmPtr->cmdStatusList[ cmdNdx ].modified = 0;			\
}


//=====================================================================================================================
// Enums

enum 
{
	eCmdNone = -1,	   				// no command; MUST be less than zero!

//0
	eCmdQuit,		   				// quit command
	eCmdRestart,	   				// restart command
	eCmdShutdown,	   				// shutdown command
	eCmdSetDebug,					// setDebug Command

	eCmdDumpData,	   				// diagnostic
	
	eCmdGblInit,					// GLOBAL_INIT command
	eCmdGblIndex,  					// GLOBAL_INDEX command
	eCmdGblPark,					// GLOBAL_PARK command
	eCmdGblReboot,					// GLOBAL_REBOOT command

	eCmdImages,		   				// collect raw images (same as diag 0x01)
	eCmdData,		   				// collect diagnostic data (same as diag 0x0E)
	eCmdTelem,		   				// select telemetry data
	eCmdTRate,		   				// telemetry rate of update (per second)

//13	
	eCmdCamWFSInit,	   				// initialize SciMeasure WFS camera

	eCmdCamRateAuto,				// do 'autoCamRate' command
	eCmdCamRate,	   				// frame rate of the camera (per second)
	eCmdCamGain,	   				// set camera gain (0, 1, 2, or 3)
	eCmdCamSRate,	   				// read frame rate from camera
	eCmdCamSGain,	  				// read gain from camera

//19
	eCmdRefOffs,			 		// sets centroid offsets, sent by host in MAS
	eCmdRefRePhaseOffs,				// sets centroid 'Re-Phase' offsets, in MAS, and saves to file
	eCmdSetSimCalData,				// sets a phase map to GMB like CAL does - for testing only
	eCmdSetRefCents,				// sets current reference Centroid configuration ( ref and offset combos )

	eCmdOffsetRefCents,				// offsets current reference Centroid configuration ( ref and offset combos )

	eCmdRcvApoRefOffs,				// gets apodizer offsets, from file, in nM slope, reads from file (8)
	eCmdStoreApoOffs,				// saves apodizer offsets, to file, in nM slope, write to one of 8 files
	eCmdSaveRefOffs,  				// saves centroid offsets, nM slope (diagnostic)
	eCmdSaveRefCents,	  			// determines and stores new ref cents in nM slope

	eCmdTakeBacks,					// determines and stores new wfs bkGrnd levels for each frame rate
	eCmdTakeDarks,					// determines and stores new wfs darks file for each frame rate
	eCmdTakeFlats,					// determines and stores new wfs flat (gain) file for each frame rate

//31
	eCmdSetDrkBck,					// select backs, darks or niether	
	eCmdSetFlats,                   // select flat application or not

	eCmdThresh,						// sets threshold used by centroider on raw data

	eCmdMakeTwtSM,					// make a tweeter System Matrix (a.k.a., poke matrix)

	eCmdCalOnOff,					// Turn On/Off Cal Data
	eCmdClsOpn,						// close / Open the AO loops ( Wfr, Twt, TT )
	eCmdClsOpnOFC,					// close / Open the OFC loop
	eCmdOffload,					// starts or stops, independently, the m1/m2 offload

	eCmdTGain,		 				// sets AO Twt loop gain (proportional gain) -- not currently used
	eCmdTInt,						// sets AO Twt loop integrator gain -- not currently used
	eCmdStatsLog,					// turns stats/error logging on and off
	eCmdReserved3,					//

	eCmdWGain,						// sets AO Wfr loop gain (proportional gain) -- not currently used
	eCmdWInt,						// sets AO Wfr loop integrator gain -- not currently used
	eCmdTTOffset,					// set tip/tilt offsets
	eCmdReserved5,					//

//47	
	eCmdSetTwt,						// Sets the tweeter from the requested existing file
	eCmdSetWfr,						// Sets the woofer from the requested existing file
	eCmdTwtOff,						// Offsets the tweeter using the specified file
	eCmdWfrOff,						// Offsets the woofer using the specified file

	eCmdStorMdlGains,				// Store current optimizer gains to file
	eCmdSetHstMdlGains,				// Set optimizer gains from host specified file
	eCmdSelectGainSrc,				// select which optimizer to use
	eCmdSetGainSrcs,				// select which optimizer to use at each mode ( file input )

	eCmdSetShape,					// Sets the shape on the woofer or tweeter (bit encoded) to existing 'lab flat, sys flat or dm shape file'
	eCmdStoreShape,					// averages and saves a 'pre-stored' Dm data  ( which one is specified in argument) 

	eCmdMakeInFunc,					// Set mirror to sin/cos for one mode of FTR
	eCmdSelectInFunc,				// Select either host defined or all '1's for InFluence Func gain

	eCmdCycleAct,					// starts moving actuator x up and down at 1 Hz
	eCmdCycleEnd,					// stops moving all actuators that were started with reg

//61	
	eCmdTTGain,		   				// set tip/tilt loop gains
	eCmdTT_Mov,		   				// set tip/tilt mirror X,Y actuators (in volts or angle) (** these are temporary settings, they will be overwritten **)

	eCmdAbort,		   				// abort command
	
	eCmdSysConParms,   				// reads the centroid parameter files
	eCmdEngConfig,
	eCmdObsConfig,

	eCmdFillFtrWfrCm,  				// reads the Wfr cm ftr file -- no corresponding entry in CmdTable

	eCmdEndFlag,		   			// end of list
	eCmdCount = eCmdEndFlag
};

enum
{
	eCmdActNone,					// no action required
	eCmdActQuit,					// quit the AO control program
	eCmdActRestart,					// restart the AO system 
	eCmdActShtdwn,					// shutdown the AO system

	eCmdActEngCfg,
	eCmdActObsCfg,

	eCmdActGblInit,					// Handle GLOBAL_INIT 
	eCmdActGblNdx,					// Handle GLOBAL_INDEX 
	eCmdActGblPark,                 // Handle GLOBAL_PARK 
	eCmdActGblRbt,					// Handle GLOBAL_REBOOT 
	
	eCmdActToDiag,					// send the command to the diagnostic thread
	eCmdActToHRT,					// send the command to the HRT processor
	eCmdActToCam,					// send the command to the HRT camera thread
	eCmdActToCent,					// send the command to the HRT centroid thread
	eCmdActToRecon					// send the command to the HRT recon thread
};

enum		// Bit masks for command restrictions
{
	eCmdNoCare			= 0x00,		// all 3 loops can be open or closed for command to execute

	eCmdTwtOpenBit		= 0x01,		// the Twt Ao loop must be open
	eCmdWfrOpenBit		= 0x02,		// the Wfr Ao loop must be open
	eCmdTTOpenBit		= 0x04,		// the tt loop must be open
	eCmdCalOpenBit		= 0x08,
	eCmdSHOpen			= 0x07,		// both ao and tt loops must be open (both use Shack-Hartmann WFS) (must be eCmdTwtOpenBit | eCmdWfrOpenBit | eCmdTTOpenBit)
	eCmdAllOpen			= 0x0F,		// all loops must be open ( includes CAL )

	eCmdTwtClosedBit	= 0x10,		// the Twt Ao loop must be closed
	eCmdWfrClosedBit	= 0x20,		// the Wfr Ao loop must be closed
	eCmdTTClosedBit		= 0x40,		// the tt loop must be closed
	eCmdCalClosedBit	= 0x80,		// the CAL loop must be closed
	eCmdSHClosed		= 0x70,		// both ao and tt loops must be closed (both use Shack-Hartmann WFS) (must be eCmdTwtOpenBit | eCmdWfrOpenBit | eCmdTTOpenBit)
	eCmdAllClsd			= 0xF0		// all loops must be closed ( includes CAL )


};

enum
{
	eCmdParmNone,					// no parameter
	eCmdParmInt,					// integer parameter
	eCmdParmFlt,					// float parameter
	eCmdParmStr						// string parameter
};


//=========================================================
// Error codes
enum
{
	eErrNoErr,				//  0. no error
	eErrRange,				//  1. an out-of-range error occurred
	eErrLostSocket,			//	2. lost the socket used for host communications
	eErrRestart,			//  3. a restart command has been received from the host
	eErrShutdown,			//  4. a shutdown command has been received from the host
	eErrQuit,			    //  5. a quit command has been received from the host
	eErrFileAlreadyOpen,	//  6. a diagnostic data file is already open
	eErrOpeningFile,		//  7. a system error occurred while attempting to open a file
	eErrWritingHeader,		//  8. a system error occurred while attempting to write the header to the file
	eErrWritingData			//  9. a system error occurred while attempting to write data to file
};

#endif	// _CONSTANT_H

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
