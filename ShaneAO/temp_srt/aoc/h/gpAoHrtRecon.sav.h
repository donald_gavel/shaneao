/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtRecon.h
 *
 * 	@version  $Id: gpAoHrtRecon.sav.h,v 1.1 2012/10/26 16:31:40 jgates Exp $
 * 	
 *	@brief 
 *	This is the header file for the reconstructor component.
 *
 *	@b DESCRIPTION:
 *	This is the header file for the reconstructor component.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/25/2008
 *
 *	@b REVISIONS:
 *	
 *	
 *	01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _RECON_H
#define _RECON_H		1

#if ( STANDALONE_FFT == 0 )
#include <fftw3.h>
#endif


//=====================================================================================================================
// Enums

//=========================================================
// ao loop status values
enum
{
	eAOLoopOpen,
	eAOLoopOpening,
	eAOLoopClosing,
	eAOLoopAlmostClosed,
	eAOLoopClosed
};

enum
{
	eCMStateStart,
	eCMStateTakeZero,
	eCMStateTakeData,
	eCMStateTurn,
	eCMStateDone
};

#if ( TIMER_TEST != 0 )
enum
{
	eTimerLastIntr,				//  0
	eTimerThisIntr,				//	1
	eTimerAfterWFSSem,			//	2
	eTimerAfterEDTIntrProc,		//	3
	eTimerAfterRTLHardEnable,	//	4

	eTimerAfterCentroid,		//	5
	eTimerAfterCentMoveUnpack,  //	6

	eTimerAfterReconSem,		//	7

	eTimerFTRStart,				//  8
	eTimerFTRAfterSums,			//  9
	eTimerAfterTT,				// 10
	eTimerFTRAfterExtend,		// 11
	eTimerFTRAfterRowFFT,		// 12
	eTimerFTRAfterColFFT,		// 13
	eTimerFTRAfterFilter,		// 14
	eTimerFTRAfterIntegrate,	// 15
	eTimerAfterFTR,				// 16

	eTimerAfterParse,			// 17
	eTimerAfterProcTWT,			// 18
	eTimerAfterProcWFR,			// 19
	eTimerAfterLongCmd,			// 20

	eTimerAfterCentPupTrack,	// 21

	eTimerOPTStartGetData,		// 22
	eTimerOPTAfterCrntAvg,		// 23
	eTimerOPTAfterMoveToOpt,	// 24
	eTimerOPTAfterA1,			// 25
	eTimerOPTAfterA1B2,			// 26
	eTimerOPTAfterA2B1,			// 27
	eTimerFTRAfterOpt,			// 28
	eTimerFTRAfterGainCopy,		// 29

	eTimerAfterProcStates,		// 30

	eTimerProbe1,				// 31

	eTimerMaxTimes
};
#endif

// tweeter slaving fields
enum
{
	eTwtSlaveDestRow,	// a -1 in this field terminates the list
	eTwtSlaveDestCol,
	eTwtSlaveSrc1Row,
	eTwtSlaveSrc1Col,
	eTwtSlaveSrc2Row,	// a -1 in this field indicates that there is no slave source 2
	eTwtSlaveSrc2Col,
	eTwtSlaveSrc3Row,	// a -1 in this field indicates that there is no slave source 3  //XXXdwpBadActSlaves
	eTwtSlaveSrc3Col,																	 //XXXdwpBadActSlaves
	eTwtSlaveMaxFields
};

// indexes for tip/tilt related diagnostic data
enum
{
	eTTxErrorCentUnits,			//  0
	eTTyErrorCentUnits,			//	1

	eTTxErrorAnglePreLQG,		//	2
	eTTyErrorAnglePreLQG,		//	3

	eTTxErrorAngle,				//	4
	eTTyErrorAngle,				//	5

	eTTxErrorAngleStage,		//	6
	eTTyErrorAngleStage,		//	7

	eTTxErrorAngleWoofer,		//	8
	eTTyErrorAngleWoofer,		//	9

	eTTxSettingAngleUnclipped,  // 10
	eTTySettingAngleUnclipped,	// 11

	eTTxSettingAngle,			// 12
	eTTySettingAngle,			// 13

	eTTxSettingVolts,			// 14
	eTTySettingVolts,			// 15

	eTTxSettingAngleWoofer,		// 16 -- this is the integrated woofer setting for X, used only for r0 calculation
	eTTySettingAngleWoofer,		// 17 -- this is the integrated woofer setting for Y, used only for r0 calculation

	eTTPistonWoofer,			// 18

	eTTNumData
};

// indexes for clipping and cleanup related diagnostic data
enum
{
	eClipCleanupTwtClipCount,  // the Twt, Wfr, and TT clip counts need to be the 
	eClipCleanupWfrClipCount,  // > first 3 indexes, starting with 0, to move into 
	eClipCleanupTTClipCount,   // > the corresponding 3 display data fields
	eClipCleanupTTWfrClipCount,

	eClipCleanupTwtPiston,
	eClipCleanupTwtCleanupMode,
	eClipCleanupTwtCleanupValue,

	eClipCleanupWfrPiston,
	eClipCleanupWfrCleanupMode,
	eClipCleanupWfrCleanupValue,

	eClipCleanupNumData
};

// indexes for M1 and M2 offloading
enum 
{ 
	eM1AstigCos,	  // M1 Zernike modes
	eM1AstigSin,
	eM1ComaX,
	eM1ComaY,
	eM1Spherical,
	eM1TrefoilCos,
	eM1TrefoilSin,
	eM1SecAstigCos,
	eM1SecAstigSin,
	eM1SecComaX,
	eM1SecComaY,
	eM1SecSpherical,
	eM1TetrafoilX,
	eM1TetrafoilY,
	eM1SecTrefX,
	eM1SecTrefY,
	eM1MaxVals 
};

enum 
{ 
	eM2Tip, 	// M2 Zernike modes
	eM2Tilt, 
	eM2Focus, 
	eM2MaxVals 
};

enum 
{						  // indexes for M1/M2 offloading related diagnostic data
	eOffloadMode,		  // 1 = tip, 2 = tilt, 3 = focus, 4 through 19 = the 16 M1 high order modes
	eOffloadOp,			  // 0 = the mode was just projected, 1 = the mode was just filtered
	eOffloadRawVal,		  // the newly projected raw mode value
	eOffloadFilteredVal,  // filtered value:
						  // > if the mode was just projected, this is the previously filtered value
						  // > if the mode was just filtered, this is the newly filtered value
	eOffloadRotatedVal,   // the rotated value for the last full set of data that was completed (the 
						  // > rotations are done all at once)
	eOffloadDiagNumData 
};

// indexes for statistics related diagnostic data
enum
{
	eStatsSNR,						 //  0
	eStatsRmsWfsError,				 //  1
	eStatsR0,						 //  2

	eStatsSNR_Intensities_0,		 //  3
	eStatsSNR_Intensities_1,		 //  4
	eStatsSNR_Intensities_2,		 //  5
	eStatsSNR_Intensities_3,		 //  6

	eStatsRmsWfsError_sumOfSquares,	 //  7

	eStatsR0_tip,					 //  8
	eStatsR0_tilt,					 //  9
	eStatsR0_tipTilt,				 // 10
	eStatsR0_crntN,					 // 11
	eStatsR0_smoothedTT,			 // 12
	eStatsR0_variance,				 // 13

	eStatsNumData
};


//=========================================================
typedef enum  // FTR algorithm sub-thread commands and status
{
	eReconAlgFTRTh_Wait,

	// Recon States
	eReconAlgFTRTh_StartTT_Sums,
	eReconAlgFTRTh_StartFFTRows,
	eReconAlgFTRTh_StartFFTCols,
	eReconAlgFTRTh_StartReconFilter,
	eReconAlgFTRTh_StartReconApplyOptGains,
	eReconAlgFTRTh_SendToOptimizer,
	eReconAlgFTRTh_StartOptimizerGain,
	eReconAlgFTRTh_StartWooferExtract,		// Not threaded at this point
	eReconAlgFTRTh_StartWooferVmm,
	eReconAlgFTRTh_StartTwtInflFuncMask,
	eReconAlgFTRTh_StartIFFTCols,
	eReconAlgFTRTh_StartIFFTRows,

	// Optimizer
	eReconAlgFTRTh_Opt_Start,
	eReconAlgFTRTh_PreProcess_A1,				// collect data			
	eReconAlgFTRTh_PreProcess_A2B1_FFTA,
	eReconAlgFTRTh_PreProcess_A1B2_FFTB,
	eReconAlgFTRTh_PostProcess_1,					// optimal new gains
	eReconAlgFTRTh_PostProcess_2,
	eReconAlgFTRTh_PostProcess_3,
	eReconAlgFTRTh_OptGains,	

	// General States 
	eReconAlgFTRTh_InProg,
	eReconAlgFTRTh_Quit

} ReconOpt_Thread_States;

//=====================================================================================================================
// Defines
//
//=============================================
//  APERTURE SIZE STUFF
//
#define APERTURE_GRID_SIZEROW		(NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP)  //Aperture row grid size
#define APERTURE_GRID_SIZECOL		(NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP)   //Aperture column grid size

#define APERTURE_GRID_AREA			(APERTURE_GRID_SIZEROW * APERTURE_GRID_SIZECOL)

#define	GPI_PUP_DIA					APERTURE_GRID_SIZEROW						// (n=48): diameter of extended pupil in GPI pixels

//=============================================
// Optimizer Stuff
#define MAX_MODES					APERTURE_GRID_AREA							// (numModes=48*48=2304=one Frame Data): # of Spatial Fourier (SF) modes 

#define PER_LEN						2048										// (perLen):length of periodogram
#define HALF_PER_LEN				(PER_LEN / 2)

//=============================================
//  NUM_THREAD STUFF
//		Note that FTR worker threads must come first.  
//		Also, note that these are different than the CPU assignments (see gpAoConstant.h).

#define FTR_START_THREAD		0
#define FTR_NUM_THREADS			HRT_CPU_RECON_THREADS_NUM  // Has to be even and > 1
#define FTR_LIMIT_THREAD		(FTR_START_THREAD + FTR_NUM_THREADS)

#define OPT_START_THREAD		FTR_LIMIT_THREAD
#define OPT_NUM_THREADS			HRT_CPU_RECONOPT_THREADS_NUM  // Has to be > 0
#define OPT_LIMIT_THREAD		(OPT_START_THREAD + OPT_NUM_THREADS)

#define NUM_TOTAL_ALG_THREADS	OPT_LIMIT_THREAD

#define MAX_TWEETER_ACTS		((NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP)*(NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP))  // maximum number of twt DM actuators
#define TWEETER_LOOKUP_BINS		16192  // number of tweeter lookup bins

#define MAX_WOOFER_ACTS			81
#define WOOFER_ROW_SIZE			9
#define WOOFER_COL_SIZE			9
#define WOOFER_LOOKUP_BINS		4096   // number of woofer lookup bins
#define NUM_ACTIVE_WFR_ACTS		69	   // Number of Active actuators on the woofer  - NOTE! this should be the same as one side of ParseAlgFTR_ControlMatrix matrix
#define MAX_WFR_VEC_SIZE		30	   // Max FTR Vector Size

#define MAX_TWEETER_CLEAN_MODES (NUM_ACTIVE_WFR_ACTS + 1)  // maximum tweeter cleanup modes (actual number is waffle plus controlled woofer modes)
#define MAX_WOOFER_CLEAN_MODES  NUM_ACTIVE_WFR_ACTS		   // maximum woofer cleanup modes (actual number is uncontrolled woofer modes)

#define INIT_CM_AVGN			5	   // number of CM values to average when determining sharp mirror values
#define INIT_MIRRORSHAPE_AVGN	10	   // number of DM values to average when determining flat and sharp mirror values

#define MAX_TT_ACTS				2

#define TT_LOOKUP_BINS			4096   // number of tt lookup bins

#define MAX_NUM_CM_DATA_SETS	15	   // number of data sets to collect when determining slopes for each actuator for cm

#if ( TIMER_TEST != 0 )
#define NUM_TIME_SETS 8
#endif

#define MAX_OFFLOAD_FILTER_VALS	150
// from Jean-Pierre, 10/14/10: M2 expects tip and tilt in units of mm of spot displacement in the f/16 
// > focal plane. 1 arcsec on the sky corresponds to 0.621 mm
#define M2_TT_MM_PER_MAS		0.000621  // millimeters per MAS for M2 T/T


//=====================================================================================================================
// Typedefs
#if ( STANDALONE_FFT == 0 )
	typedef	float			fft_data_type;
	typedef fftwf_complex	fft_complex_type;
#else
	typedef	float			fft_data_type;
	typedef float			fft_complex_type[2];
#endif

typedef enum
{
	StateInFunc_Start,

	StateInFunc_SetMode,
	StateInFunc_ReadShape,
	StateInFunc_SetShape,
	StateInFunc_Average,
	StateInFunc_SaveData,

	StateInFunc_Wait_FTR,
	StateInFunc_Wait_Shape,

	StateInFunc_Cleanup,
	StateInFunc_Idle,

	StateInFunc_Invalid

} MakeInFuncState;


//=========================================================
// data type for the table that drives the makeInFuncProcess() function (to create Influnce Funcion Data)
typedef struct
{
	MakeInFuncState		state;					// phase to set for current step
	char			sine_select;			// true: sine, false co-sine (or not used)
	
} MakeInFuncStateDriver;


//=========================================================
// data type for the various status to be dumped
typedef struct
{
	float		cent_rms;			// updated each frame - raw data
	float		dummy[14];			// filler for later updates

} LoopStatus;


//=========================================================
// recon buffer type
typedef struct
{
	short		diagIndicator;									// indicates eDiagIndNotInUse, eDiagIndInUse, or eDiagIndTelem

	float		reconOptGains[APERTURE_GRID_AREA];				// This is a copy of the current Optimizer Modal gains - for display

	float		ftrFFTModesData[APERTURE_GRID_AREA][2];			// raw FTR DATA for diagnostics (complex) (includes both tweeter and woofer coef. - prior to Optimizer )
	float		ftrFFTModesDataOptimized[APERTURE_GRID_AREA][2];// raw FTR DATA for diagnostics (complex) (includes both tweeter and woofer coef. - after Optimizer AND integrator )

	float		ftrFFTModesTwtData[APERTURE_GRID_AREA][2];		// raw FTR Twt DATA for diagnostics (complex) (Does NOT include woofer coef.)

	float		reconRawTwtPhase[MAX_TWEETER_ACTS];				// Twt Raw data (after parse and before integration)
	float		reconTwtPhase[MAX_TWEETER_ACTS];				// Twt data - microns - clipped twt phase
	float		reconTwtUnclippedPhase[MAX_TWEETER_ACTS];		// Twt data - microns - unclipped twt phase
	float		reconTwtVolts[APERTURE_GRID_AREA];				// Twt data - volts to write to twt

	float		reconRawWfrPhase[MAX_WOOFER_ACTS];				// Wfr Raw data (after parse and before integration)

	float		reconTTWfrPhase[MAX_WOOFER_ACTS];				// Wfr data - microns - clipped wfr phase, with new TT
	float		reconTTWfrUnclippedPhase[MAX_WOOFER_ACTS];		// Wfr data - microns - unclipped wfr phase, with new TT
	float		reconTTWfrVolts[MAX_WOOFER_ACTS];				// Wfr data - volts to write to wfr, with new TT

	float		reconWfrPhase[MAX_WOOFER_ACTS];					// Wfr data - microns - clipped wfr phase, with new high order
	float		reconWfrUnclippedPhase[MAX_WOOFER_ACTS];		// Wfr data - microns - unclipped wfr phase, with new high order
	float		reconWfrVolts[MAX_WOOFER_ACTS];					// Wfr data - volts to write to wfr, with new high order

	float		reconTTData[eTTNumData];						// TT data
	float		reconClipCleanupData[eClipCleanupNumData];		// clipping and cleanup data
	float  		reconOffloadDiagData[eOffloadDiagNumData];  	// M1, M2 offload diagnostic data

#if ( TIMER_TEST == 0 )
	long long	rdtscFrameDoneStamp;							// Time when this frame was completed ( includes write to DM but NOT dm throughput)
#else
	long long	timerTimesToPrint[4];							// a sampling of timer times to print
#endif

	float		statistics[eStatsNumData];  // statistics along with some interim calculations

	float		twt_state;		   // Current twt state - as applied through this frame
	float		wfr_state;		   // Current wfr state - as applied through this frame
	float		tt_state;		   // Current t/t state - as applied through this frame
	float		twt_shape_update;  // twt shape update - updated when shape is written else 0
	float		wfr_shape_update;  // wfr shape update - updated when shape is written else 0
	float		ofc_state;		   // Current OFC state (open or closed)
	float		ofc_step;		   // Current OFC step (step in the OFC state machine)

//	variables used in dark-corner background / dark level correction (using unilluminated corner pixels)
//	> IMPORTANT -- The values that go in these variables are actually determined in the centroider thread!  
//	> IMPORTANT -- This is simply to save yet another dumpDiag data packet while making sure the dark-corner 
//	> IMPORTANT -- correction works.  
	float		hrt_darkCornerCorrectSwitch;  // turns dark level correction on and off
	int			darkCornerPrevFrameRateNdx;
	int			darkCornerBoxcarIndex, darkCornerBoxcarN;
	int			darkCornerBoxcarSums[DARK_CORNER_NUM_PIXELS];
	float		darkCornerBoxcarAves[DARK_CORNER_NUM_PIXELS];
	float		darkCornerCorrection;
} ReconBuffType;

//=========================================================
// optimizer modal buffer type
typedef struct
{
	float		modalPsds[ MAX_MODES ][ PER_LEN ];
	float		modalPsdsJointInputs[ MAX_MODES ][ PER_LEN ];

} PsdBuffType;

//=========================================================
// recon data type
typedef struct
{
	int				cmdStart;								// index (into CmdTable[]) of command to be started or eCmdNone
	int				cmdCrnt;								// index (into CmdTable[]) of command currently running or eCmdNone
	int				cmdIntParam[MAX_NUM_CMD_PARAMS];		// integer parameter for command, if it has one
	float			cmdFloatParam[MAX_NUM_CMD_PARAMS];		// float parameter for command, if it has one

	int				reconBufNdx;							// index into reconBuffs[] for buffer being used by recon

	int				psdWriteNdx;							// index into reconBuffs[] for buffer being written to

	ReconBuffType	reconBuffs[NUM_HRT_DATA_BUFFS];			// the recon buffers (defined above )

	PsdBuffType		psdBuffs[NUM_HRT_PSD_BUFFS];			// PSD buffers - filled about 1/sec
	
} ReconDataType;


//=====================================================================================================================
// Global Variables

#if ( TIMER_TEST != 0 )
extern long reconFrameID;
extern struct timespec timerTimes[NUM_TIME_SETS][eTimerMaxTimes];
#endif

extern int	 	tweeterSlaves[APERTURE_GRID_AREA][eTwtSlaveMaxFields];		// tweeter slaving variables

//XXXdwpBadActSlaves -- start -- for testing bad actuator slaving
#define BAD_ACTS_SLVS 54
extern int	twtSlavesBadActs[BAD_ACTS_SLVS][eTwtSlaveMaxFields];  // tweeter slaving variables for bad actuators
//XXXdwpBadActSlaves -- end -- for testing bad actuator slaving

extern short	hrt_ReconRunCmd;											// the 'long' command that is currently being executed, if any

extern short	hrt_AoTwtLoopStat;											// indicates the status of the ao loop (open, opening, or closed)
extern short	hrt_AoWfrLoopStat;											// indicates the status of the ao loop (open, opening, or closed)
extern short	hrt_AoTTLoopStat;											// indicates the status of the ao loop (open, opening, or closed)

extern short	hrt_optimizer_OFC_Stat;										// indicates the status of the OFC Optimizer (open, opening, closing, or closed)

extern short 	hrt_CMAvgN;													// number of centroids to average when determining a system matrix
extern short	hrt_MirrorShapeAvgN;										// number of DM values to average when determining flat and sharp mirror values

extern float	hrt_twtLabFlat[MAX_TWEETER_ACTS];							// current twt Lab flat mirror values
extern float	hrt_twtSysFlat[MAX_TWEETER_ACTS];							// current twt Sys flat mirror values
extern float	hrt_twtDmShape[MAX_TWEETER_ACTS];							// current twt Dm Shape mirror values

extern float	hrt_twtHostOffset[MAX_TWEETER_ACTS];						// current twt mirror 'offset' values (for the Host Specified offset)

extern float	hrt_tt_x_offset;
extern float	hrt_tt_y_offset;

extern float	hrt_wfrLabFlat[MAX_WOOFER_ACTS];							// current wfr Lab flat mirror values
extern float	hrt_wfrSysFlat[MAX_WOOFER_ACTS];							// current wfr Sys flat mirror values
extern float	hrt_wfrDmShape[MAX_WOOFER_ACTS];							// current wfr Dm Shape mirror values

extern float	hrt_wfrHostOffset[MAX_WOOFER_ACTS];							// current wfr mirror 'offset' values (for the Host Specified offset)

extern short	hrt_twtZeroArray[APERTURE_GRID_SIZECOL];					// Array indicating Tweeter modes that are zeroed (used in integrator)
extern int		hrt_twtCoeffUsed_Array[ APERTURE_GRID_AREA ];				// tweeter modes that are used for FTR [ Array Format for integrator ]

// Data array for 'hrt' clipping deconstruction 
extern float	hrt_twtCleanup[MAX_TWEETER_CLEAN_MODES][MAX_TWEETER_ACTS];	// tweeter mode cleanup matrix
extern float	hrt_wfrCleanup[MAX_WOOFER_CLEAN_MODES][MAX_WOOFER_ACTS];	// woofer mode cleanup matrix

// Modal gains
//
extern short	hrt_selectModalGains[ APERTURE_GRID_AREA ];					// selector for which type gain to apply when loops are closed
																			//			( 1:OFC optimizer set, 4: Host File Set, 8: Initial File Set )

extern float	hrt_workingModalGains[ APERTURE_GRID_AREA ];				// Working gains applied when loop is closed ( source determined by hrt_selectModalGains )

extern float	hrt_hostModalGains[ APERTURE_GRID_AREA ];					// Modal gains set by 'Host File'
extern float	hrt_initialModalGains[ APERTURE_GRID_AREA ];				// Modal gains set by 'Initial File'
extern float	hrt_ofcOutputGains[ APERTURE_GRID_AREA ];					// Recon opt modal gains that are internally generated - not necessarily applied


extern float	hrt_mode_cleanup_thresh;									// mode cleanup threshold
extern float	hrt_mode_cleanup_gain;										// mode cleanup gain

extern float	hrt_modal_loss_dm;											// modal loss - DMs - from param file
extern float	hrt_modal_loss_tt;											// modal loss - TT - from param file

// Influence Function data
//
extern int		makeInFuncModes[ APERTURE_GRID_SIZECOL ][ APERTURE_GRID_SIZECOL ][2];	// Storage location for 'makeInFuncMode' to drive the FRT while processing InFunc

extern int		makeInFuncState;											// Current makeInFunc State (enum) when running 'makeInFunc'
extern int		makeInFunc_UpdateModes;										// Flag to FTR to update modes so that tweeter shape can be read ( 0: no update, 1: update frame)
extern int		makeInFunc_numAverage;										// Number of frames to average
extern float	makeInFunc_amplitude;										// amplitude for mode to test


//=====================================================================================================================
// (O)FC Global variables
extern volatile char	ReconAlgFTRThreadStat[NUM_TOTAL_ALG_THREADS];	// sub-thread status variables. these are chars to make sure they're atomic

extern const short		ReconAlgFTR_RowsPerThread;						//IMPORTANT: APERTURE_GRID_SIZEROW MUST be evenly divisible by NUM_ALGFTRTHREADS
extern const short		ReconAlgFTR_ColsPerThread;						//IMPORTANT: APERTURE_GRID_SIZECOL MUST be evenly divisible by NUM_ALGFTRTHREADS

//reconstruction Fourier filter values
extern fft_complex_type	ReconAlgFTR_filterX[ APERTURE_GRID_AREA ];
extern fft_complex_type	ReconAlgFTR_filterY[ APERTURE_GRID_AREA ];
extern fft_data_type		ReconAlgFTR_filterDenom[ APERTURE_GRID_AREA ];

//=====================================================================================================================
// Global Function Prototypes

void*	gpAoHrtRecon( void *arg );
void	gpAoHrtRecon_InitThreading(void);
void	gpAoHrtRecon_CleanThreading(void);
void	gpAoHrtRecon_InitMemory(void);
void	gpAoHrtRecon_CleanMemory(void);


#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
