/**********************************************************************************************
*
* COPYRIGHT (C) 2008
*
* LAWRENCE LIVERMORE NATIONAL LABORATORY
* ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
*
***********************************************************************************************/
/*!
 *
 *	@file gpAoHrtParse.c
 *
 * 	@version $Id: gpAoHrtParse.c,v 1.1 2012/10/26 16:31:41 jgates Exp $
 * 	
 *	@brief 
 *	Perform wavefront reconstruction parsing and related tasks.  
 *
 *	@b DESCRIPTION:
 *	This component contains functions to perform the computations necessary for wavefront
 *	reconstruction parsing,
 *
 *	This component implements parsing steps in the recon component worker threads.
 *
 *	@b AUTHOR:
 *	DWP & SMJ & VJR	DATE: 05/01/2009
 *
 *	@b STATIC (LOCAL) FUNCTION NAMES:
 *
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoHrtParse_ParseFTR()
 *	-# @c gpAoHrtParse_ParseVMM()
 *	-# @c gpAoHrtParse_FTRAlgoProcess()
 *	-# @c gpAoHrtParse_FTRAlgoInit()
 *	-# @c gpAoHrtParse_FTRAlgoInitMemory()
 *	-# @c gpAoHrtParse_FTRAlgoCleanMemory()
 *
 *******************************************************************************
 */

#if (RTL_SYSTEM != 0)
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <math.h>
#else
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#endif

#if (RTL_SYSTEM == 0)
// mrr #include <gpUtGeneral.h>
// mrr #include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr
#else
#include "gpUtGmbHrt.h"
#endif

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoHrtParse.h" 
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"
#include "gpAoHrtFfts.h"


//*******************************************************************************
//** General Global Variables
//*******************************************************************************


//*******************************************************************************
//** Reconstructor-specific Global Variables
//*******************************************************************************


//*******************************************************************************
//** General Local Variables
//*******************************************************************************

static boolean wfrVector_firstTimeFlag = true;
static int 	   wfrVector_ndxTranspose[APERTURE_GRID_AREA];  // APERTURE_GRID_AREA is quite a bit too big, but safe
static int 	   twtVector_ndxTranspose[APERTURE_GRID_AREA];  // APERTURE_GRID_AREA is quite a bit too big, but safe


//*******************************************************************************
//** Reconstructor-specific local variables/define/constant
//*******************************************************************************

//Pointer to recon buffer pointer (used by the parser commands of worker threads only)
static ReconBuffType *ReconAlgFTR_parseBufPtrCurrent;


//*******************************************************************************
//** General Local Function Prototypes
//*******************************************************************************
#if (PARSE_DEBUG != 0) 
	static int gpAoHrtParse_dumpCornerOfMatrix( void *pIn, int rowSize, int complexFlag, int dType );
#endif


//*******************************************************************************
//** Reconstructor-specific Local function prototypes
//*******************************************************************************

// Woofer CM (real)
float ParseAlgFTR_WfrControlMatrix[ NUM_ACTIVE_WFR_ACTS ][ MAX_WFR_VEC_SIZE * 2 ];		// Real valued filter 			

// Tweeter filter values (real)
float ParseAlgFTR_TwtInfFuncMask[APERTURE_GRID_SIZEROW][APERTURE_GRID_SIZECOL];			// Real valued filter 			

int			 ParseAlgFTR_wfrVecSize;								// Actual size of wfr vector (determined when read in)

// Woofer Vector (r,ii pairs) and the WooferVectorList
short		 ParseAlgFTR_WfrVectorList[ MAX_WFR_VEC_SIZE];			// List of modes to extract for woofer FTR CM ( complex postiion )
short		 ParseAlgFTR_TwtZeroList[ MAX_WFR_VEC_SIZE*2];			// List of Tweeter modes to zero after woofer extraction

int			 ParseAlgFTR_wfrActMap[NUM_ACTIVE_WFR_ACTS];			// Map of active woofer actuators in current DM

// Location to keep packed array, until it is unpacked
static float tmp_wfrPackedVector[ NUM_ACTIVE_WFR_ACTS ];

static short ParseAlgFTR_WfrVectorList_times2[MAX_WFR_VEC_SIZE*2];	// Precalculated list of modes ( real and imag positions )
static float ParseAlgFTR_WfrVector[ MAX_WFR_VEC_SIZE*2];			// Extracted FTR Woofer vector for FTR-CM

//parser constants
static short ParseAlgFTR_rowStartsWooferVMM[FTR_NUM_THREADS];
static short ParseAlgFTR_rowsPerVMMThread[FTR_NUM_THREADS];	

//*******************************************************************************
//** fft-related variables -- start

// fft configuration parameters
static const int fft_n_row = APERTURE_GRID_SIZEROW;
static const int fft_n_col = APERTURE_GRID_SIZECOL;

// pointers to split parser data
static fft_complex_type* ParseAlgFTR_rowStartsC[FTR_NUM_THREADS],   *ParseAlgFTR_colStartsC[FTR_NUM_THREADS];
static fft_complex_type* ParseAlgFTR_rowStartsCT[FTR_NUM_THREADS],  *ParseAlgFTR_colStartsCT[FTR_NUM_THREADS];

#if ( STANDALONE_FFT == 0 )
// pointer to phase results from reconstruction/parsing
static fft_complex_type* ParseAlgFTR_pPhaseC;
static fft_complex_type* ParseAlgFTR_pPhaseCT;

// fftw plans for the inverse FFT (2d)
static fftwf_plan ParseAlgFTR_ifftPlanRows[FTR_NUM_THREADS];
static fftwf_plan ParseAlgFTR_ifftPlanCols[FTR_NUM_THREADS];

#else
// arrays of phase results from reconstruction/parsing
static fft_complex_type ParseAlgFTR_pPhaseC [APERTURE_GRID_AREA * sizeof(fft_complex_type)];
static fft_complex_type ParseAlgFTR_pPhaseCT[APERTURE_GRID_AREA * sizeof(fft_complex_type)];
#endif

//** fft-related variables -- end
//*******************************************************************************


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtParse_ParseFTR( ReconBuffType* reconBufPtr )
 *
 *	@brief
 *	The purpose of this function is to perform the FTR reconstruction parsing.
 *
 *	@b DESCRIPTION:
 *	This unit does the setup for the 'threaded' parsing for the tweeter/woofer parse portion of the FTR reconstructor.
 *	This unit defines the portion of work to be performed by the switch/case int the function 'gpAoHrtParse_FTRAlgoProcess'
 *	
 *	Basic tasks:
 *		1) Extract Woofer modes out of the 'ParseAlgFTR_pPhaseF', leaving only tweeter modes there
 *		2) Perform 'Woofer VMM' on woofer modes
 *		3) Filter Tweeter modes with 'read-in' filter
 *		4) Perform IFFT on filtered Tweeter
 *
 *	These tasks are 'set' in this routine - but the actual processing is done in 'gpAoHrtParse_FTRAlgoProcess'
 *	@par
 *
 *	@b Usage: gpAoHrtParse_ParseFTR( )( ReconBuffType*  reconBufPtr );
 * 
 *	@param [out] reconBufPtr (ReconBuffType*):	Current recon data 
 * 
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@see gpAoHrtParse_FTRAlgoProcess
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtParse_ParseFTR( ReconBuffType*  reconBufPtr )
{
	short	ii;
	int		ndx;

//	calculate transpose indexes, if they haven't been calculated already
	if (wfrVector_firstTimeFlag)
	{
		wfrVector_firstTimeFlag = false;

		for(ii = 0; ii < ParseAlgFTR_wfrVecSize; ii++)
		{
			ndx = ParseAlgFTR_WfrVectorList[ ii ];
			wfrVector_ndxTranspose[ii] = ((ndx % APERTURE_GRID_SIZEROW) * APERTURE_GRID_SIZEROW * 2 + 
										  (int)(ndx / APERTURE_GRID_SIZEROW ) * 2 ) ;
		}

		for(ii = 0; ii < (ParseAlgFTR_wfrVecSize * 2); ii++)
		{
			ndx = ParseAlgFTR_TwtZeroList[ ii ];
			twtVector_ndxTranspose[ii] = ((ndx % APERTURE_GRID_SIZEROW) * APERTURE_GRID_SIZEROW * 2 + 
										  (int)(ndx / APERTURE_GRID_SIZEROW ) * 2 ) ;
		}
	}

//	Cache the current input data pointer for access by the worker threads
	ReconAlgFTR_parseBufPtrCurrent = reconBufPtr;

	#if (PARSE_DEBUG != 0) 
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Starting Woofer Extraction");
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR):         Note the indexes are into a transposed matrix");
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR):         Woofer Vector ::" );
	#endif			

	// Copy out all modes and then zero out the woofer modes
	for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
	{
		reconBufPtr->ftrFFTModesTwtData[ ii ][ 0 ] = reconBufPtr->ftrFFTModesDataOptimized[ ii ][ 0 ];
		reconBufPtr->ftrFFTModesTwtData[ ii ][ 1 ] = reconBufPtr->ftrFFTModesDataOptimized[ ii ][ 1 ];
	}

	//	extract out the woofer vector - as it will be overwritten during the tweeter Filtering.
	for(ii = 0; ii < ParseAlgFTR_wfrVecSize; ii++)
	{
		ParseAlgFTR_WfrVector[ ii * 2     ] = 
			( ((float*)(reconBufPtr->ftrFFTModesTwtData))[ wfrVector_ndxTranspose[ii]     ] ) * 2.0;
		ParseAlgFTR_WfrVector[ ii * 2 + 1 ] = 
			( ((float*)(reconBufPtr->ftrFFTModesTwtData))[ wfrVector_ndxTranspose[ii] + 1 ] ) * 2.0;

		#if (PARSE_DEBUG != 0) 
			HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): %d: index[ %d / transpose:%d ] ( %7.3f, %7.3f)", ii, ndx, wfrVector_ndxTranspose[ii], ParseAlgFTR_WfrVector[ ii*2 ], ParseAlgFTR_WfrVector[ ii*2 + 1 ] );
		#endif	
	}

	#if (PARSE_DEBUG != 0) 
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Starting Twt zero after Extraction");
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR):			Note the indexes are into a transposed matrix");
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR):          Twt zeros indicies and transposed indicies : " );
	#endif

	// Do the zero (Note - it is being done on a transposed matrix)
	for(ii = 0; ii < (ParseAlgFTR_wfrVecSize * 2); ii++)
	{
		((float*)(reconBufPtr->ftrFFTModesTwtData))[ twtVector_ndxTranspose[ii]     ] = 0.0;
		((float*)(reconBufPtr->ftrFFTModesTwtData))[ twtVector_ndxTranspose[ii] + 1 ] = 0.0;

		#if (PARSE_DEBUG != 0) 
			HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): %d: index[ %d / transpose:%d ] ", ii, ndx, twtVector_ndxTranspose[ii] );
		#endif	
	}

//	Perform Woofer VMM
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartWooferVmm;
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
	{
		while (ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
	}

	// Move Wfr packed array into full matrix
	for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
		ReconAlgFTR_parseBufPtrCurrent->reconRawWfrPhase[ ii ] = 0.0f;
	for( ii=0; ii<NUM_ACTIVE_WFR_ACTS; ii++)
		ReconAlgFTR_parseBufPtrCurrent->reconRawWfrPhase[ ParseAlgFTR_wfrActMap[ ii ] ] = tmp_wfrPackedVector[ ii ];

	#if (PARSE_DEBUG != 0) 
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of Woofer Control matrix ");
		gpAoHrtParse_dumpCornerOfMatrix( ParseAlgFTR_WfrControlMatrix, (MAX_WFR_VEC_SIZE*2), 0, TFLOAT );
	
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of parsed Woofer Actuator commands ");
		gpAoHrtParse_dumpCornerOfMatrix( (void*)(ReconAlgFTR_parseBufPtrCurrent->reconRawWfrPhase), 12, 0, TFLOAT );
	#endif

//	Perform Tweeter Filter
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartTwtInflFuncMask;
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
	{
		while (ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
	}

	#if (PARSE_DEBUG != 0)
	{
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of tweeter filter");
		gpAoHrtParse_dumpCornerOfMatrix( ParseAlgFTR_TwtInfFuncMask, APERTURE_GRID_SIZEROW , 0, TFLOAT );

		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of tweeter coeff (not phase) after filter - before ifft");
		gpAoHrtParse_dumpCornerOfMatrix( (void*)(ParseAlgFTR_pPhaseC), APERTURE_GRID_SIZEROW*2, 1, TDOUBLE );

		{
			int ii;
			float	tmpSum[2] = {0.0f, 0.0f};
			for( ii=336; ii<384; ii++)
			{
				HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Filter Mult Output (ii=%d): ", ii); 
				tmpSum[0] += ParseAlgFTR_pPhaseC[ ii ][0];
				tmpSum[1] += ParseAlgFTR_pPhaseC[ ii ][1];
				HRT_PRINTF("       out: (%g,%g)", ParseAlgFTR_pPhaseC[ ii ][0], ParseAlgFTR_pPhaseC[ ii][1] );
			}
			HRT_PRINTF( "\n Sum of tweeterFilter out = %9.5f, %9.5f", tmpSum[0], tmpSum[1] );
		}
	}
	#endif

	// Hey!! This is for the MakeInFuncgains!!!  bottom line is that it replaces the calculated modes with predefined modes and THEN does the tweeter phase calculation!
	//
	
	if( makeInFunc_UpdateModes )
	{
		fft_complex_type* pInData = (fft_complex_type*) &(makeInFuncModes[ 0 ][ 0 ][ 0 ]);		// Ptr to complex value phase
		for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
		{
			ParseAlgFTR_pPhaseC[ ii ][0] = pInData[ ii ][0];
			ParseAlgFTR_pPhaseC[ ii ][1] = pInData[ ii ][1];
		}
	}
	
//	perform 1d column iffts in parallel (on the phase data)
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartIFFTCols;
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
	{
		while (ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
	}

	#if (PARSE_DEBUG != 0) 
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of column ifft Col output (before Transpose) ");
		gpAoHrtParse_dumpCornerOfMatrix( ParseAlgFTR_pPhaseC, APERTURE_GRID_SIZEROW*2, 1, TDOUBLE );

		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of column ifft Col output ( after Transpose) ");
		gpAoHrtParse_dumpCornerOfMatrix( ParseAlgFTR_pPhaseCT, APERTURE_GRID_SIZEROW*2, 1, TDOUBLE );
	#endif

//	perform 1d row iffts in parallel to complete the 2d iffts (on the phase data)
//	this is where the reconBufPtr is updated
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartIFFTRows;
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
	{
		while (ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
	}

	#if (PARSE_DEBUG != 0) 
		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of row ifft Row output (PhaseCT)");
		gpAoHrtParse_dumpCornerOfMatrix( ParseAlgFTR_pPhaseCT, APERTURE_GRID_SIZECOL*2, 1, TDOUBLE );

		HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Dump of tweeter Actuator commands (buf->reconRawTwtPhase) ");
		gpAoHrtParse_dumpCornerOfMatrix( (void*)(ReconAlgFTR_parseBufPtrCurrent->reconRawTwtPhase), 1, 0, TFLOAT );
	#endif

	return;
}


/*!
 *******************************************************************************
 *
 *	@fn short gpAoHrtParse_FTRAlgoProcess(short thNdx, char threadCmnd )
 *
 *	@brief
 *  FTR Parse worker thread commands
 *
 *	@b DESCRIPTION:
 *	This module does actual work for the 'threaded' woofer parse and the tweeter IFF / woofer VMM  portion of the FTR reconstructor.
 *	This module is called directly by the recon thread and therefore is part of that thread [gpAoHrtRecon_FTRAlgoThread()]
 *	This set of switches (cases) is	called prior to the FTR reconstructor itself.  This unit does not invoke new threads!
. *	@par
 *	
 *
 *	@b Usage: short gpAoHrtParse_FTRAlgoProcess( short thNdx, char threadCmnd )
 * 
 *	@param[in]  thNdx		(short):	Calling working thread number
 *	@param[in]  threadCmnd	(short):		Parse command
  *
 *	@return 
 *	return a flag indicating whether the command was processed
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@see gpAoHrtParse_ParseFTR
 *	
 *	@callgraph
 *******************************************************************************
 */

short gpAoHrtParse_FTRAlgoProcess( short thNdx, char threadCmnd )
{
	int				CmdProcessed = 1;
	float*			pCM;
	float			fTmp;
	fft_data_type*	pInPhaseCoeff;
	fft_data_type*	pOutPhaseCoeff;
	float*			pTwtInfFuncMask;
#if ( STANDALONE_FFT != 0 )
	int 			dataStartNdx;
#endif

//	execute parse command
	switch (threadCmnd)
	{
//      ********************************************
//      **  Perform Woofer VMM extraction         **
//      ********************************************
		case eReconAlgFTRTh_StartWooferVmm:
			{
				int ii, jj, ndx;

//				This is a parse command
				CmdProcessed = 1;

//				get the woofer parse control matrix
				for( jj=0; jj< ParseAlgFTR_rowsPerVMMThread[ thNdx ]; jj++ )
				{
//					Using the tmp, instead of the result pointer saved quite a bit of time
					fTmp = 0.0;
					pCM = ParseAlgFTR_WfrControlMatrix[ ParseAlgFTR_rowStartsWooferVMM[thNdx] + jj ];
					for( ii=0; ii<(ParseAlgFTR_wfrVecSize*2); ii++ )
						fTmp += pCM[ii] * ParseAlgFTR_WfrVector[ii];
					ndx = ParseAlgFTR_rowStartsWooferVMM[thNdx] + jj;
					tmp_wfrPackedVector[ ndx ] = fTmp;
				}
			}
			return CmdProcessed;

//      ********************************************
//      ** Perform Tweeter masking                **
//      ********************************************
		case eReconAlgFTRTh_StartTwtInflFuncMask:
			{
				int ii;
//				This is a parse command
				CmdProcessed = 1;

//				This will multiply the complex data (taken from the ReconBuf->fftRawData area) by the real filter value 
//				> (or not, as the case may be)
				pInPhaseCoeff = (fft_data_type*)ReconAlgFTR_parseBufPtrCurrent->ftrFFTModesTwtData + thNdx * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZEROW * 2;
				pOutPhaseCoeff = (fft_data_type*) ParseAlgFTR_pPhaseC + thNdx * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZEROW * 2;

				pTwtInfFuncMask = ParseAlgFTR_TwtInfFuncMask[ thNdx * ReconAlgFTR_RowsPerThread ];

				#if (PARSE_DEBUG != 0) 
					float tmpSum[2] = {0.0f, 0.0f};
				#endif

				if( hrtFileDataShmPtr->selectorForInFuncMultiplier == 2 )
				{
					for( ii=0; ii<APERTURE_GRID_SIZECOL*ReconAlgFTR_RowsPerThread ; ii++)
					{
						#if (PARSE_DEBUG != 0) 
							if( thNdx == 0 && (ii >= 7*APERTURE_GRID_SIZECOL) && (ii < 8*APERTURE_GRID_SIZECOL) )
								HRT_PRINTF( "\nFilter Mult (ii=%d): in: (%9.5f,%9.5f), filt: %9.5f", 
													ii, pInPhaseCoeff[ ii*2 ], pInPhaseCoeff[ ii*2 + 1 ], pTwtInfFuncMask[ii] );
						#endif

						// Each complex value is multiplied by the real filter Coeff
						pOutPhaseCoeff[ ii*2     ]  = pInPhaseCoeff[ ii*2     ] * pTwtInfFuncMask[ii];		// real part
						pOutPhaseCoeff[ ii*2 + 1 ]  = pInPhaseCoeff[ ii*2 + 1 ] * pTwtInfFuncMask[ii];		// imaginary part

						#if (PARSE_DEBUG != 0) 
							if( thNdx == 0 && (ii >= 7*APERTURE_GRID_SIZECOL) && (ii < 8*APERTURE_GRID_SIZECOL) )
							{
								tmpSum[0] += pOutPhaseCoeff[ ii*2 ] ;
								tmpSum[1] += pOutPhaseCoeff[ ii*2 + 1 ];
								HRT_PRINTF( " out: (%9.5f,%9.5f)", pOutPhaseCoeff[ ii*2 ], pOutPhaseCoeff[ ii*2 + 1 ] );
							}
						#endif
					}
				}
				else
				{
					for( ii=0; ii<APERTURE_GRID_SIZECOL*ReconAlgFTR_RowsPerThread ; ii++)
					{
						#if (PARSE_DEBUG != 0) 
							if( thNdx == 0 && (ii >= 7*APERTURE_GRID_SIZECOL) && (ii < 8*APERTURE_GRID_SIZECOL) )
								HRT_PRINTF( "\nFilter Mult (ii=%d): in: (%9.5f,%9.5f), filt: %9.5f", 
													ii, pInPhaseCoeff[ ii*2 ], pInPhaseCoeff[ ii*2 + 1 ], pTwtInfFuncMask[ii] );
						#endif

						// Each complex value is not multiplied by the real filter Coeff
						pOutPhaseCoeff[ ii*2     ]  = pInPhaseCoeff[ ii*2     ];		// real part
						pOutPhaseCoeff[ ii*2 + 1 ]  = pInPhaseCoeff[ ii*2 + 1 ];		// imaginary part

						#if (PARSE_DEBUG != 0) 
							if( thNdx == 0 && (ii >= 7*APERTURE_GRID_SIZECOL) && (ii < 8*APERTURE_GRID_SIZECOL) )
							{
								tmpSum[0] += pOutPhaseCoeff[ ii*2 ] ;
								tmpSum[1] += pOutPhaseCoeff[ ii*2 + 1 ];
								HRT_PRINTF( " out: (%9.5f,%9.5f)", pOutPhaseCoeff[ ii*2 ], pOutPhaseCoeff[ ii*2 + 1 ] );
							}
						#endif
					}
				}

				#if (PARSE_DEBUG != 0) 
					if( thNdx == 0)
						HRT_PRINTF("DEBUG (gpAoHrtParse_ParseFTR): Sum of tweeterFilter out = %9.5f, %9.5f", tmpSum[0], tmpSum[1] );
				#endif
			}
			return CmdProcessed;

//		********************************************
//		**        Inverse FFT on Columns          **
//		********************************************
		case eReconAlgFTRTh_StartIFFTCols:
			{
				int ii, jj;
				fft_complex_type		*rowsC, *colsCT;
				
//				This is a parse command
				CmdProcessed = 1;

//				columns, complex to complex
#if ( STANDALONE_FFT == 0 )
				fftwf_execute( ParseAlgFTR_ifftPlanCols[thNdx] );		// This is operating on the phaseC array
#else
//				note that real and imaginary are swapped for inverse fft
				dataStartNdx = thNdx * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL;
				fft_c2c_48(&ParseAlgFTR_pPhaseC[dataStartNdx][1],  // first imaginary element in complex input array
						   &ParseAlgFTR_pPhaseC[dataStartNdx][0],  // first real element in complex input array
						   &ParseAlgFTR_pPhaseC[dataStartNdx][1],  // first imaginary element in complex output array
						   &ParseAlgFTR_pPhaseC[dataStartNdx][0],  // first real element in complex output array
						   ReconAlgFTR_ColsPerThread);
#endif

//				moved to transpose matrix ( C -> CT ) so that original data (for other threads) is not affected
//				AND fftw is much faster on the rows
				for( jj=0; jj<ReconAlgFTR_RowsPerThread; jj++)  // transpose matrices back
				{
					rowsC = ParseAlgFTR_pPhaseC   + ( ReconAlgFTR_RowsPerThread * thNdx + jj ) * APERTURE_GRID_SIZEROW ;
					colsCT = ParseAlgFTR_pPhaseCT + ( ReconAlgFTR_RowsPerThread * thNdx + jj );

					for( ii = 0; ii<APERTURE_GRID_SIZEROW; ii++)
					{
						colsCT[ ii * APERTURE_GRID_SIZEROW ][0] = rowsC[ ii ][0];
						colsCT[ ii * APERTURE_GRID_SIZEROW ][1] = rowsC[ ii ][1];
					}
				}
			}
			return CmdProcessed;

//		********************************************
//		**         Inverse FFT On rows            **
//		********************************************
		case eReconAlgFTRTh_StartIFFTRows:
			{
				int ii, jj;
				volatile float	*pF_Out;

//				This is a parse command
				CmdProcessed = 1;

//				rows, complex to complex
#if ( STANDALONE_FFT == 0 )
				fftwf_execute( ParseAlgFTR_ifftPlanRows[thNdx] );  // This is operating on the phaseCT array
#else
//				note that real and imaginary are swapped for inverse fft
				dataStartNdx = thNdx * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL;
				fft_c2c_48(&ParseAlgFTR_pPhaseCT[dataStartNdx][1],  // first imaginary element in complex input array
						   &ParseAlgFTR_pPhaseCT[dataStartNdx][0],  // first real element in complex input array
						   &ParseAlgFTR_pPhaseCT[dataStartNdx][1],  // first imaginary element in complex output array
						   &ParseAlgFTR_pPhaseCT[dataStartNdx][0],  // first real element in complex output array
						   ReconAlgFTR_RowsPerThread);
#endif
		
//				extract the real portion of pPhaseC[] (this is the final phase BEFORE multiply by influence functions)
				for( ii=0; ii<ReconAlgFTR_RowsPerThread; ii++)  
				{
					// pfOut - here - points to the head of each column to receive data
					pF_Out = ReconAlgFTR_parseBufPtrCurrent->reconRawTwtPhase + ( ii + ReconAlgFTR_RowsPerThread * thNdx )  * APERTURE_GRID_SIZEROW;

					// Remember we are extracting from transposed matrix so ii and jj are reversed
					for( jj=0; jj<APERTURE_GRID_SIZEROW; jj++)
						pF_Out[ jj ] = ParseAlgFTR_pPhaseCT[ jj + ( ii +  thNdx * ReconAlgFTR_RowsPerThread )  * APERTURE_GRID_SIZEROW ][0];
				}
			}
			return CmdProcessed;
	}
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtParse_FTRAlgoInit( void )
 *
 *	@brief
 *  Initialize variables used by the Parse FTR threads.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the FTR parser algorithm.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtParse_FTRAlgoInit( );
 * 
 *	\@param[in] None
 *
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtParse_FTRAlgoInit( void )
{
	short ii;
	short rowsPerVmmMainThread = 0;

	//	Make the wooferVectorList_times2 array - for speed!
	for( ii=0; ii<ParseAlgFTR_wfrVecSize; ii++ )
	{
	   ParseAlgFTR_WfrVectorList_times2[ii*2]		= ParseAlgFTR_WfrVectorList[ ii ] * 2;
	   ParseAlgFTR_WfrVectorList_times2[ii*2 + 1]	= ParseAlgFTR_WfrVectorList[ ii ] * 2 + 1;
	}

	rowsPerVmmMainThread = ( NUM_ACTIVE_WFR_ACTS / FTR_NUM_THREADS );

	//	Take care of situation where number of rows is NOT evenly divisable by NUM_THREADS
	if( NUM_ACTIVE_WFR_ACTS % FTR_NUM_THREADS != 0 )
	   rowsPerVmmMainThread++;

	//	initialize the worker thread data pointers for the parser data
	for( ii=0; ii<FTR_NUM_THREADS; ii++ )
	{
		ParseAlgFTR_rowStartsC[ ii ] = ParseAlgFTR_pPhaseC + ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZEROW;
		ParseAlgFTR_colStartsC[ ii ] = ParseAlgFTR_pPhaseC + ii * ReconAlgFTR_ColsPerThread;

		ParseAlgFTR_rowStartsCT[ ii ] = ParseAlgFTR_pPhaseCT + ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZEROW;
		ParseAlgFTR_colStartsCT[ ii ] = ParseAlgFTR_pPhaseCT + ii * ReconAlgFTR_ColsPerThread;

		ParseAlgFTR_rowStartsWooferVMM[ii] = ii * rowsPerVmmMainThread;

		// Here the actuators are even spaced across all except the last thread 
		ParseAlgFTR_rowsPerVMMThread[ii] = rowsPerVmmMainThread;				
		
		// If the last thread, it get the remainder
		if( ii == (FTR_NUM_THREADS-1) ) 
			ParseAlgFTR_rowsPerVMMThread[ii] = NUM_ACTIVE_WFR_ACTS - ii * rowsPerVmmMainThread ;
	}
}


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtParse_FTRAlgoInitMemory( void )
 *
 *	@brief
 *  Allocate memory used by the Parse FTR algorithm.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to allocate memory used by the FTR parser algorithm.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtParse_FTRAlgoInitMemory( void );
 * 
 *	\@param[in] None
 *
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtParse_FTRAlgoInitMemory( void )
{
#if ( STANDALONE_FFT == 0 )
	short	ii;

	if (rtl_sem_wait(&FftwPlanSem) < 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR waiting on FftwPlanSem, errno = ", rtl_errno);

//	allocate fftw memory and measure plans for the ifft that has to be performed
	ParseAlgFTR_pPhaseC		= (fft_complex_type*)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type) );
	ParseAlgFTR_pPhaseCT	= (fft_complex_type*)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type) );

//	initialize the FFT plans for the parser
	for( ii=0; ii<FTR_NUM_THREADS; ii++)
	{
	    ParseAlgFTR_ifftPlanCols[ii] = fftwf_plan_many_dft( 1, &fft_n_row, ReconAlgFTR_ColsPerThread, 
		  									&ParseAlgFTR_pPhaseC[ ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL ],	NULL, 1, APERTURE_GRID_SIZECOL, 
		  									&ParseAlgFTR_pPhaseC[ ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL ],	NULL, 1, APERTURE_GRID_SIZECOL, 
		  									FFTW_BACKWARD, FFTW_PATIENT );

	    ParseAlgFTR_ifftPlanRows[ii] = fftwf_plan_many_dft( 1, &fft_n_col, ReconAlgFTR_RowsPerThread, 
		  									&ParseAlgFTR_pPhaseCT[ ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL ], NULL, 1, APERTURE_GRID_SIZEROW, 
		  									&ParseAlgFTR_pPhaseCT[ ii * ReconAlgFTR_RowsPerThread * APERTURE_GRID_SIZECOL ], NULL, 1, APERTURE_GRID_SIZEROW, 
		  									FFTW_BACKWARD, FFTW_PATIENT );
	}

	rtl_sem_post(&FftwPlanSem);
#endif
}

/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtParse_FTRAlgoCleanMemory( void )
 *
 *	@brief
 *  Clean up the FTR parser memory.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to de-allocate FTR parser memory.
 *
 *	@par
 *	
 *	@b Usage: gpAoHrtParse_FTRAlgoCleanMemory( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtParse_FTRAlgoCleanMemory( void )
{
#if ( STANDALONE_FFT == 0 )
	short	ii;

//	destroy fftw plans
	for (ii = 0; ii < FTR_NUM_THREADS; ii++)
	{
		fftwf_destroy_plan(ParseAlgFTR_ifftPlanRows[ii]);
		fftwf_destroy_plan(ParseAlgFTR_ifftPlanCols[ii]);
	}

//	free fftw memory and measure plans for the 3 ffts that have to be performed
	fftwf_free(ParseAlgFTR_pPhaseC);
	fftwf_free(ParseAlgFTR_pPhaseCT);
#endif
}


#if (PARSE_DEBUG != 0) 
/*!
 *******************************************************************************
 *	@fn static int gpAoHrtParse_dumpCornerOfMatrix( )
 *
 *	@brief
 *  Dump corner of a 2D matrix
 *
 *	@b DESCRIPTION:
 * 		This currently only supports 2D type matrices ( will work on 2D if step size is set to 6 )
 *		Only supports FLOATS and LONGS  ( may or may not be complex - based on input flag )
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtParse_dumpCornerOfMatrix( void *pIn, int rowSize, int complexFlag, int dType );
 * 
 *	\@param[in]
 *		None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int gpAoHrtParse_dumpCornerOfMatrix( void *pIn, int rowSize, int complexFlag, int dType )
{
	int ii, jj;

	if( dType == TFLOAT )
	{
		if( complexFlag != 0)
		{
			HRT_PRINTF( "\n INFO - complex matrix corner dump at pointer: %p\n", pIn );
			for( jj=0; jj<9; jj++)
			{
				for( ii=0; (ii<10) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " (%9.5f, ii%9.5f)  ", *((float*)pIn + jj*rowSize + ii*2), *((float*)pIn + jj*rowSize + ii*2 + 1) );

				float tmpSum_r = 0.0;
				float tmpSum_i = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum_r +=  *((float*)pIn + jj*rowSize + ii*2);
					tmpSum_i +=  *((float*)pIn + jj*rowSize + ii*2 + 1);
				}

				HRT_PRINTF(" :  Sum real/img = ( %9.5f, %9.5f )\n", tmpSum_r, tmpSum_i );
			}
			HRT_PRINTF("\n");
		}
		else
		{
			HRT_PRINTF( "\n INFO - matrix corner dump at pointer: %p\n", pIn );
			for( jj=0; jj<9; jj++)
			{
				for( ii=0; (ii<10) && (ii<rowSize); ii++)
					HRT_PRINTF( "  %9.5f,  ", *( (float*)pIn+ jj*rowSize + ii ) );

				float tmpSum = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum +=  *((float*)pIn + jj*rowSize + ii );
				}
	
				HRT_PRINTF(" Sum=%9.5f\n", tmpSum );
			}
			HRT_PRINTF("\n");
		}
	}

	if( dType == TLONG )
	{
		if( complexFlag != 0)
		{
			HRT_PRINTF( "\n INFO - complex (long) matrix corner dump at pointer: %p\n", pIn );
			for( jj=0; jj<9; jj++)
			{
				for( ii=0; (ii<10) && (ii<rowSize); ii++)
					HRT_PRINTF( " ( %ld, ii%ld  ", *((long*)pIn + jj*rowSize + ii * 2 ) , *((long*)pIn + jj*rowSize + ii * 2 + 1 )  );
				HRT_PRINTF("\n");
			}
			HRT_PRINTF("\n");
		}
		else
		{
			HRT_PRINTF( "\n INFO - matrix (long) corner dump at pointer: %p\n", pIn );
			for( jj=0; jj< 11; jj++)
			{
				for( ii=0; (ii<11) && (ii<rowSize); ii++)
					HRT_PRINTF( "  %ld,  ", *( (long*)pIn+ jj*rowSize + ii ) );
				HRT_PRINTF("\n");
			}
			HRT_PRINTF("\n");
		}
	}
	return 0;
}
#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
