/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoSrtDispCentInt.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to display centroid intensity frames. 
 *
 *	@b DESCRIPTION:
 *	This component contains code to display centroid intensity frames to the monitor.  
 *	It is similar to dispraw (that was based on the graywin.c example code in the 
 *	SDL/test directory).  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 4/26/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDispCentIntCreateScreen()
 *	-# @c gpAoSrtDispCentIntDraw()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

//#include <gpUtGeneral.h>	// mrr
//#include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "SDL.h"


// Configuration Variables

#define NUM_COLORS	256
#define PIX_SIZE    10

static const long secDelay = 0;			   // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static long long totalSizeExceptRecon;				  // total size of shared memory

static FileDataShmType*			fileDataShmPtr;		  // pointer to file data shared memory
static CamDataType*		 		camDataPtr;			  // pointer to camera shared memory data
static CentDataType*			centDataPtr;		  // pointer to centroid shared memory data
static HrtDataFlowControlType*	dataFlowControlPtr;	  // pointer to Data Flow Control variables


// Local Function Prototypes

static void termHandler(int sigNum);


static SDL_Surface *gpAoSrtDispCentIntCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)
{
	SDL_Surface *screen;
	int i;
	SDL_Color palette[NUM_COLORS];

// 	set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

//	set a gray colormap from black to white
	for ( i = 0; i < NUM_COLORS; i++ )
	{
		palette[i].r = i * (256 / NUM_COLORS);
		palette[i].g = i * (256 / NUM_COLORS);
		palette[i].b = i * (256 / NUM_COLORS);
	}
	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}


int main(void)
{
	int   i, j, m, n;
	Uint8 tmpData;
	Uint8 *buffer;

	int crntReadBuffNdx;

	size_t shmDataSize, camDataSize, centDataSize, dataControlSize;

	struct timespec next, remaining;

	const int	 width		= NUM_SUBAPS_PER_ROW * PIX_SIZE;
	const int	 height		= NUM_SUBAPS_PER_COL * PIX_SIZE;
	const int	 bpp 		= 8;  // bits per pixel -- any more than 8 makes this really slow
	const Uint32 videoflags = SDL_SWSURFACE;

	static int satFlashCntr = 0, satFlashVal = 0;
	static SDL_Surface *screen = NULL; 
	static SDL_Event event;

	float tmpInten[NUM_SUBAPS_PER_COL][NUM_SUBAPS_PER_ROW];

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	centDataSize = sizeof(CentDataType);
	dataControlSize = sizeof(HrtDataFlowControlType);

	totalSizeExceptRecon = (long long)shmDataSize + camDataSize + centDataSize + dataControlSize;
	printf("spAoDispIntCam: Attempting to open shared memory:: Size = %lld / 0x%llx\n", totalSizeExceptRecon, totalSizeExceptRecon );
	printf("spAoDispIntCam: shmDataSize: %lx, camDataSize: %lx, centDataSize: %lx, dataControlSize: %lx\n",
			shmDataSize, camDataSize, centDataSize, dataControlSize);

//	open the shared memory region
//	IMPORTANT: reconData is no longer included in AOC_mmap -- dispInt doesn't need reconData, so the pointer to
//	> it is not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap open: /dev/AOC_mmap\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap open: ./AOC_mmap\n");
		sleep(1);
	}
#endif

//	get the pointer to the shared memory region
	while (!quit && ((startPtr = mmap(0, totalSizeExceptRecon, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap: %llx\n", totalSizeExceptRecon);
		sleep(1);
	}
	printf( "spAoDispIntCam: Shm start Ptr = %p, totalSizeExceptRecon = 0x%llx\n", startPtr, totalSizeExceptRecon );

//	set up the pointers to the global variables
	fileDataShmPtr		= (FileDataShmType *)startPtr;
	camDataPtr			= (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr			= (CentDataType *)((char *)camDataPtr + camDataSize);
	dataFlowControlPtr	= (HrtDataFlowControlType *)((char *)centDataPtr + centDataSize);

//	initialize SDL and exit if we can't
//	set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispCentIntDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispCentIntCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispCentIntDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

	float minVal, maxVal, scaleFactor;

//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

		minVal =  100000.0;
		maxVal = -100000.0;

//		move data out of the centDispIntens[][] quickly, finding mins and maxes to auto-scale by frame
		crntReadBuffNdx = centDataPtr->centDispReadBuffNdx;
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
			{
				tmpInten[i][j] = centDataPtr->centDispIntens[crntReadBuffNdx][i * NUM_SUBAPS_PER_ROW + j];
				if ((tmpInten[i][j] > 0) && (tmpInten[i][j] < minVal))
					minVal = tmpInten[i][j];
				if (tmpInten[i][j] > maxVal)
					maxVal = tmpInten[i][j];
			}
		}

//		calculate scale factor
		if (minVal == 100000.0)
			minVal = 0.0;
		if (maxVal == -100000.0)
			maxVal = 0.0;
		if (maxVal == minVal)
			scaleFactor = 0.0;
		else
			scaleFactor = 255.0 / ((float)maxVal - (float)minVal);

//		this is to flash saturated sub-aps
		if (++satFlashCntr >= 5)
		{
			satFlashCntr = 0;
			if (satFlashVal == 0)
				satFlashVal = 255;
			else
				satFlashVal = 0;

//			we're also using satFlashCntr to print out min and max, instead of having another counter
			printf("III Frame min: %8.1f, max: %8.1f\n", minVal, maxVal);
		}

//		auto-scale
		Uint8 tmpBuffer[NUM_SUBAPS_PER_COL][NUM_SUBAPS_PER_ROW];
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
			{
				if (tmpInten[i][j] >= 65535.0)
					tmpBuffer[i][j] = satFlashVal;
				else if (tmpInten[i][j] == 0)
					tmpBuffer[i][j] = 0;
				else
					tmpBuffer[i][j] = (Uint8)((tmpInten[i][j] - minVal) * scaleFactor);
			}
		}

//		spread the data over the display pixels
		buffer = (Uint8 *)screen->pixels;
		for (i = 0; i < NUM_SUBAPS_PER_COL; i++)
		{
			for (j = 0; j < NUM_SUBAPS_PER_ROW; j++)
			{
				tmpData = tmpBuffer[i][j];
				for (m = 0; m < PIX_SIZE; m++)
				{
					for (n = 0; n < PIX_SIZE; n++)
						buffer[((i * PIX_SIZE) + m) * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + (j * PIX_SIZE) + n] = tmpData;
				}
			}
		}

//		draw in sub-ap grid lines
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i += PIX_SIZE)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j++)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = 255;
		}
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i++)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j += PIX_SIZE)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = 255;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

//		check for quit
		while(!quit && SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				printf("dispint quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(startPtr, totalSizeExceptRecon);
	  	close(mmfd);
	}
}


static void termHandler(int sigNum)
{
	printf("dispint quitting due to OS term: %d\n", sigNum);
	quit = true;
}
