/**********************************************************************************************
*
* COPYRIGHT (C) 2007-8
*
* LAWRENCE LIVERMORE NATIONAL LABORATORY
* ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
*
***********************************************************************************************/
/*!
 *
 *	@file gpAoHrtTipTilt.c
 *
 * 	@version $Id: gpAoHrtTipTilt.c,v 1.1 2012/10/26 16:31:41 jgates Exp $
 * 	
 *	@brief 
 *	Perform tip/tilt tasks.  
 *
 *	@b DESCRIPTION:
 *	This component contains units to extract tip and tilt from the wavefront as read by the WFS,
 *	temporally filter the tip/tilt data, and finally pass the tip/tilt data to be converted to volts
 *	and output to the tip/tilt platform by the gpAoHrtVolts module.  
 *
 *	The remaining wavefront, with tip and tilt removed, is passed on to the recon component.  
 *	The tip/tilt filter coefficients are also optimized in this component.
 *
 *	@b IMPORTANT: Several of the functions in this module are invoked from recon and are, therefore,
 *	actually part of the recon thread.  
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 11/26/2008
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoHrtTipTilt_Init()
 *	-# @c gpAoHrtTipTilt_ProcCmd()
 *	-# @c gpAoHrtTipTilt_CloseLoop()
 *	-# @c gpAoHrtTipTilt_OpenLoop()
 *	-# @c gpAoHrtTipTilt_Process()
 *	-# @c gpAoHrtTipTilt_LQGFilt()
 *	-# @c ttCfuncs()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#if (RTL_SYSTEM != 0)
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <math.h>
#else
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#endif

#if (RTL_SYSTEM == 0)
// mrr #include <gpUtGeneral.h>
// mrr #include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr
#else
#include "gpUtGmbHrt.h"
#endif

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoHrtParse.h" 
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"


// Defines, Enums, and Typedefs

#define SPLIT_FILTER_ORDER 4  // filter order for the Butterworth filter used to temporally split the TT data
//XXXdwp101006 -- probably need a SPLIT_FILTER_LAG for each frame rate
#define SPLIT_FILTER_LAG   9  // filter lag for the Butterworth filter used to temporally split the TT data

typedef double ttComplex[2];

//******************************************
// LQG defines, enums, and typedefs -- start

static const int GS_IN_FITS[NG]   = { eAtmWind_r_and_g1, eAtmWind_theta_and_g2 }; 
static const int AS_R_IN_FITS[NA] = { eCpVib_alpha0R, eCpVib_alpha1R, eCpVib_alpha2R }; 
static const int AS_I_IN_FITS[NA] = { eCpVib_alpha0I, eCpVib_alpha1I, eCpVib_alpha2I };
static const int BS_R_IN_FITS[NB] = { eNcpVib_beta0R }; 
static const int BS_I_IN_FITS[NB] = { eNcpVib_beta0I };

// LQG defines, enums, and typedefs -- end
//******************************************


// Global Variables

float   hrt_TTGainStage;	   // tip/tilt loop gain, stage
float   hrt_TTGainWfrSurface;  // tip/tilt loop gain, woofer surface

int		hrt_ttSplitSwitch = 0;
boolean hrt_ttLQGSwitch = true;
float	hrt_ttWfr9ActMicronsPerMASX = 0.01125;
float	hrt_ttWfr9ActMicronsPerMASY = 0.01125;

//******************************
// LQG global variables -- start

float hrt_ttCompDelay_microSecs = 0.0;  // tip/tilt computational delay in microseconds

// current seeing, wind power, and star brightness, as reported by the TLC
// XXX these need to get updated from GMB by the SRT side (currently in parms file for testing) XXX
double hrt_ttSeeing = 0.0, hrt_ttWindPower = 0.0, hrt_ttStarBrightness = 0.0;

// LQG global variables -- end
//******************************


// Local Variables

static const float ttSignX = -1.0;  // sign of the X axis (1.0 or -1.0)  //XXXdwp110531 -- made negative
static const float ttSignY = -1.0;  // sign of the Y axis (1.0 or -1.0)  //XXXdwp110531 -- made negative

static float xGainStage, yGainStage, xGainWfrSurface, yGainWfrSurface;

static float axisRotationTT = 0.0;  // tip/tilt axis rotation for the TT mirror on the optical table (in degrees)
static float r1x, r2x, r1y, r2y;    // tip/tilt rotation coefficients

static float xSettingAngle = 0.0, ySettingAngle = 0.0;  // in milli-arcseconds (MAS) -- these are the integrators
static float xSettingAngleWoofer = 0.0, ySettingAngleWoofer = 0.0;  // used only for r0 calculations

static int   newMoveTT = -1;  // -1 = no new move TT, 0 = move in angle, 1 = move in volts
static float xNewSetting = 0.0, yNewSetting = 0.0;

// in all of the following "previous value" arrays, used for the temporal split filter, lower indeces have more recent data 
// > (so, [0] has the most recent data)
static double prevInputsX[SPLIT_FILTER_LAG];     // previous inputs, X
static double prevOutputsX[SPLIT_FILTER_ORDER];	 // previous outputs, X
static double prevInputsY[SPLIT_FILTER_LAG];     // previous inputs, Y
static double prevOutputsY[SPLIT_FILTER_ORDER];	 // previous outputs, Y

// Butterworth temporal split filter coefficients from matlab: [b, a] = butter(n, omega)
// where n is the order (e.g. n = 4 is fourth order, four poles) and omega is the frequency 
// relative to nyquist -- e.g. 750 Hz Nyquist (1500 Hz sample freq) with a 50 Hz cutoff for 
// the lowpass has omega = 0.067
#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
static const double b[][SPLIT_FILTER_ORDER + 1] = 
{
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    }   // 250 fps,  [b, a] = butter(4, 50/(250/2))
};
static const double a[][SPLIT_FILTER_ORDER + 1] = 
{
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 }   // 250 fps,  [b, a] = butter(4, 50/(250/2))
};
#else
static const double b[][SPLIT_FILTER_ORDER + 1] = 
{
{ 0.046582906636444,     0.186331626545775,     0.279497439818662,     0.186331626545775,     0.046582906636444    },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 0.004824343357716,     0.019297373430865,     0.028946060146297,     0.019297373430865,     0.004824343357716    },  // 500 fps,  [b, a] = butter(4, 50/(500/2))
{ 0.000416599204407,     0.001666396817627,     0.002499595226440,     0.001666396817627,     0.000416599204407    },  // 1000 fps, [b, a] = butter(4, 50/(1000/2))
{ 0.000092764620292285,  0.000371058481169140,  0.000556587721753710,  0.000371058481169140,  0.000092764620292285 },  // 1500 fps, [b, a] = butter(4, 50/(1500/2))
{ 0.000092764620292285,  0.000371058481169140,  0.000556587721753710,  0.000371058481169140,  0.000092764620292285 }   // 2000 fps, same as 1500 for now
};
static const double a[][SPLIT_FILTER_ORDER + 1] = 
{
{ 1.000000000000000, -0.782095198023338,  0.679978526916299, -0.182675697753033,  0.030118875043169 },  // 250 fps,  [b, a] = butter(4, 50/(250/2))
{ 1.000000000000000, -2.369513007182038,  2.313988414415880, -1.054665405878568,  0.187379492368185 },  // 500 fps,  [b, a] = butter(4, 50/(500/2))
{ 1.000000000000000, -3.180638548874721,  3.861194348994219, -2.112155355110972,  0.438265142261981 },  // 1000 fps, [b, a] = butter(4, 50/(1000/2))
{ 1.000000000000000, -3.453185137586611,  4.504139091633959, -2.627303618228233,  0.577833898105561 },  // 1500 fps, [b, a] = butter(4, 50/(1500/2))
{ 1.000000000000000, -3.453185137586611,  4.504139091633959, -2.627303618228233,  0.577833898105561 }   // 2000 fps, same as 1500 for now
};
#endif

// factors to calculate TT woofer vectors -- the full width (and height) of the illuminated woofer is 45 mm, 
// > which is nine 5 mm actuators
static const float ttWoofer1dFactorsX[WOOFER_ROW_SIZE] = 
	{ -20.0 / 45.0, -15.0 / 45.0, -10.0 / 45.0, -05.0 / 45.0,    // left side
		0.0, 													 // middle
	   05.0 / 45.0,  10.0 / 45.0,  15.0 / 45.0,  20.0 / 45.0 };  // right side
static const float ttWoofer1dFactorsY[WOOFER_COL_SIZE] = 
	{ -20.0 / 45.0, -15.0 / 45.0, -10.0 / 45.0, -05.0 / 45.0,    // top
		0.0, 													 // middle
	   05.0 / 45.0,  10.0 / 45.0,  15.0 / 45.0,  20.0 / 45.0 };  // bottom

//*****************************
// LQG local variables -- start

static const int tPlusOne = 0, tPlusTwo = 1;
static const int xNdx = 0, yNdx = 1;
static		 int oldNdx = 0, newNdx = 1;

static int seeingNdx = 0, windPowerNdx = 0, starBrightnessNdx = 0;  // indeces into ttLQGAllParams[][][]
static double oldSeeing = -1.0, oldWindPower = -1.0, oldStarBrightness = -1.0;

static double result_tX = 0.0, result_tMinusOneX = 0.0, result_tMinusTwoX = 0.0;
static double result_tY = 0.0, result_tMinusOneY = 0.0, result_tMinusTwoY = 0.0;

static double delta = 0;  // TT computational delay in time steps

// filter parameters currently in use (depending on seeing, wind-power, and star brightness, as reported by the TLC)
static double crnt_r;
static double crnt_theta;
static double crnt_alphasR[NA];
static double crnt_alphasI[NA];
static double crnt_betasR[NB];
static double crnt_betasI[NB];

// Kalman gains currently in use (depending on seeing, wind-power, and star brightness, as reported by the TLC)
static ttComplex crnt_ks_for_gs[NG];
static double	 crnt_ks_for_asR[NA];
static double	 crnt_ks_for_asI[NA];
static double	 crnt_k_for_phi;
static double	 crnt_ks_for_bsR[NB];
static double	 crnt_ks_for_bsI[NB];
static double	 crnt_k_for_rho;

// state vector components -- x[t] = ( g[t], a[t], b[t], phi[t-1], rho[t-1] )
static ttComplex g[2][2][NG];   // g	   -- first index for old/new -- second index for x/y -- third index for g1, g2
static double	 aR[2][2][NA];  // a, real -- first index for old/new -- second index for x/y -- third index for a1, a2, a3
static double	 aI[2][2][NA];  // a, imag -- first index for old/new -- second index for x/y -- third index for a1, a2, a3
static double	 phi[2][2];	    // phi	   -- first index for old/new -- second index for x/y
static double	 bR[2][2][NB];  // b, real -- first index for old/new -- second index for x/y -- third index for b1
static double	 bI[2][2][NB];  // b, imag -- first index for old/new -- second index for x/y -- third index for b1
static double	 rho[2][2];	    // phi	   -- first index for old/new -- second index for x/y

ttComplex tmpC1, tmpC2;  // temporary complex variables

// LQG local variables -- end
//*****************************


// Local Function Prototypes

static void gpAoHrtTipTilt_MoveTT( float value[], int flagVolts );

static void gpAoHrtTipTilt_LQGFilt( double *xTTError, double *yTTError );

static inline void ttCadd( ttComplex opC1, ttComplex opC2, ttComplex resC );
static inline void ttCsub( ttComplex opC1, ttComplex opC2, ttComplex resC );
static inline void ttCmul( ttComplex opC1, ttComplex opC2, ttComplex resC );
static inline void ttCdiv( ttComplex opC1, ttComplex opC2, ttComplex resC );
static inline void ttCabs( ttComplex opC1, double *resF );
static inline void ttCexp( ttComplex opC1, ttComplex resC );


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtTipTilt_Init( void )
 *
 *	@brief
 *	Initialize variables used by tip/tilt.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to initialize variables used by tip/tilt.
 *	@par
 *
 *	@b Usage: gpAoHrtTipTilt_Init( );
 * 
 *	\@param[in] None
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *
 *******************************************************************************
 */

void gpAoHrtTipTilt_Init( void )
{
	int i, j, k;

//	initialize misc. variables
	hrt_AoTTLoopStat = eAOLoopOpen;
	hrtFileDataShmPtr->aoTTLoopStat = eAOLoopOpen;

//	initialize the state variables for the tt LQG filter
	result_tMinusOneX = 0.0;
	result_tMinusTwoX = 0.0;
	result_tMinusOneY = 0.0;
	result_tMinusTwoY = 0.0;
	for (i = 0; i < 2; i++)
	{
		for (j = 0; j < 2; j++)
		{
			for (k = 0; k < NG; k++)
			{
				g[i][j][k][0] = 0.0;
				g[i][j][k][1] = 0.0;
			}
			for (k = 0; k < NA; k++)
			{
				aR[i][j][k] = 0.0;
				aI[i][j][k] = 0.0;
			}
			phi[i][j] = 0.0;
			for (k = 0; k < NB; k++)
			{
				bR[i][j][k] = 0.0;
				bI[i][j][k] = 0.0;
			}
			rho[i][j] = 0.0;
		}
	}

//	set up rotation values
	r1x = cos(axisRotationTT * RAD);
	r2x = cos((axisRotationTT + 90) * RAD);
	r1y = sin(axisRotationTT * RAD);
	r2y = sin((axisRotationTT + 90) * RAD);

//	initialize the previous value arrays, used for the temporal split filter
	for (i = 0; i < SPLIT_FILTER_LAG; i++)
	{
		prevInputsX[i] = 0.0;
		prevInputsY[i] = 0.0;
	}
	for (i = 0; i < SPLIT_FILTER_ORDER; i++)
	{
		prevOutputsX[i] = 0.0;
		prevOutputsY[i] = 0.0;
	}

//	set up gains and apply ttSignX and ttSignY
	xGainStage		= ttSignX * hrt_TTGainStage;
	yGainStage		= ttSignY * hrt_TTGainStage;
	xGainWfrSurface = ttSignX * hrt_TTGainWfrSurface;
	yGainWfrSurface = ttSignY * hrt_TTGainWfrSurface;

	return;
}


/*!
 *******************************************************************************
 *
 *	@fn int gpAoHrtTipTilt_ProcCmd( short cmdNdx )
 *
 *	@brief
 *	Process TipTilt commands
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to process TipTilt commands from the host (via the SRT side).
 *	This unit is called from the gpAoHrtRecon_ProcCmd() routine
 *	@par
 *	
 *
 *	@b Usage: cmdNdx = gpAoHrtTipTilt_ProcCmd( (short) cmdNdx );
 * 
 *	@param[in]  cmdNdx (short):  index into CmdTable[] for the command to be processed
 *
 *	@return 
 *		 <0: Command Ndx handled in this function
 *		>=0: Command Ndx not handled in this function (recon needs to handle it)
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

short gpAoHrtTipTilt_ProcCmd( short cmdNdx )
{
	switch( cmdNdx )
	{
		case eCmdTTGain:  // set tip/tilt loop gains
			hrt_TTGainStage = hrtReconDataPtr->cmdFloatParam[0];
			hrt_TTGainWfrSurface = hrtReconDataPtr->cmdFloatParam[1];
			gpAoHrtFileData_Put(eFileTypeParms);
			SysConSendStrngNoParm(eDebugLevelMax, "INFO 'setTTGain' complete");
// mrr DM			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
			hrtReconDataPtr->cmdCrnt = eCmdNone;
			return -1;

		case eCmdTTOffset:  // set tip/tilt loop offsets
			hrt_tt_x_offset = hrtReconDataPtr->cmdFloatParam[0];
			hrt_tt_y_offset = hrtReconDataPtr->cmdFloatParam[1];
			gpAoHrtFileData_Put(eFileTypeParms);
			SysConSendStrngNoParm(eDebugLevelMax, "INFO 'setTTOff' complete");
// mrr DM			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
			hrtReconDataPtr->cmdCrnt = eCmdNone;
			return -1;

		case eCmdTT_Mov:  // set tip/tilt mirror
			{
				float tmpTTValues[ MAX_TT_ACTS ];

				tmpTTValues[0] = hrtReconDataPtr->cmdFloatParam[0];
				tmpTTValues[1] = hrtReconDataPtr->cmdFloatParam[1];
				gpAoHrtTipTilt_MoveTT( tmpTTValues, hrtReconDataPtr->cmdIntParam[2] );
// mrr DM				AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}	
			return -1;
	}
	return cmdNdx;
}


/*!
 *******************************************************************************
 *
 *	@fn  gpAoHrtTipTilt_MoveTT( float xPos, float yPos, int flagVolts )
 *
 *	@brief
 *	Initialize variables used by tip/tilt.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to initialize variables used by tip/tilt.
 *	@par
 *
 *	@b Usage: gpAoHrtTipTilt_MoveTT( xPos, yPos, flagVolts )
 * 
 *	@param [in] xPos (float): position for TT X mirror ( if flagVolts = 1: then volts, else angle )
 *	@param [in] yPos (float): position for TT Y mirror ( if flagVolts = 1: then volts, else angle )
 *	@param [in] flagVolts (int): if flagVolts = 1: then volts, else angle
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *
 *******************************************************************************
 */

void gpAoHrtTipTilt_MoveTT( float values[], int flagVolts )
{
	xNewSetting = values[0];
	yNewSetting = values[1];
	newMoveTT   = flagVolts;  // -1 = no new move TT, 0 = move in angle, 1 = move in volts

	SysConSendStrngNoParm(eDebugLevelMax, "INFO 'tt_mov' requested");
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtTipTilt_CloseOpenTTLoop( int newState )
 *
 *	@brief
 *  Open / Close the tip/tilt loop.  
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to close the tip/tilt loop.  This is done by initializing
 *  variables and setting the AoTTLoopStat flag to indicate that the loop is closed.
 *	@par
 *
 *	@b Usage: gpAoHrtTipTilt_CloseOpenTTLoop( newState );
 * 
 *	@param[in] newState (int): set the TT loop open (0), or closed (1)
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtTipTilt_CloseOpenTTLoop( int newState  )
{
	int i;

	switch( newState )
	{
	case 0:  // open the loop
		//	set flag indicating that the TT loop is opening
		if( hrt_AoTTLoopStat == eAOLoopClosed)
		{
			hrt_AoTTLoopStat = eAOLoopOpening;
			hrtFileDataShmPtr->aoTTLoopStat = eAOLoopOpening;
		}
		break;

	case 1:  // close the loop
//		initialize and set up variables as follows:

//		compute rotation coefficients
		r1x = cos(axisRotationTT * RAD);
		r2x = cos((axisRotationTT + 90) * RAD);
		r1y = sin(axisRotationTT * RAD);
		r2y = sin((axisRotationTT + 90) * RAD);

//		initialize the previous value arrays, used for the temporal split filter -- this is done each time the
//		> loops are closed to try to avoid cumulative truncation error
		for (i = 0; i < SPLIT_FILTER_LAG; i++)
		{
			prevInputsX[i] = 0.0;
			prevInputsY[i] = 0.0;
		}
		for (i = 0; i < SPLIT_FILTER_ORDER; i++)
		{
			prevOutputsX[i] = 0.0;
			prevOutputsY[i] = 0.0;
		}

//		set up gains and apply ttSignX and ttSignY
		xGainStage		= ttSignX * hrt_TTGainStage;
		yGainStage		= ttSignY * hrt_TTGainStage;
		xGainWfrSurface = ttSignX * hrt_TTGainWfrSurface;
		yGainWfrSurface = ttSignY * hrt_TTGainWfrSurface;

//		set the AoTTLoopStat flag to indicate that the loop is closing
		hrt_AoTTLoopStat = eAOLoopClosing;
		hrtFileDataShmPtr->aoTTLoopStat = eAOLoopClosing;
		break;

	default:			// Typically "do nothing case"
		break;
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtTipTilt_Process( float xTTCentError, float yTTCentError, 
 *								ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr )
 *
 *	@brief
 *	Process tip/tilt data.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to apply the tip/tilt control law and, if we're 
 *	in closed loop, output voltages to the TT stage and the woofer DM.  Tip/tilt 
 *	is split between those 2 devices because the TT stage is quite slow to respond.  
 *
 *	@b Usage: gpAoHrtTipTilt_Process( xTTCentError, yTTCentError, 
 *									  reconBufPtr, lastReconBufPtr );
 * 
 *	@param[in]  xTTCentError	(float):			current X tip/tilt error (in pixel units)
 *	@param[in]  yTTCentError 	(float):			current Y tip/tilt error (in pixel units)
 *	@param[out] reconBufPtr		(ReconBuffType*):	current recon data pointer
 *	@param[in]  lastReconBufPtr	(ReconBuffType*):	previous recon data pointer
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtTipTilt_Process( float xTTCentError, float yTTCentError, 
							 ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr )
{
	int    i, j, yy, xxIn, xxOut, tmpNdx;
	int    respFromConvTT, respFromConvWfr;
	double inX_t0, outLowX_t0, inY_t0, outLowY_t0;

	float		 tmpXData, tmpYData;
	float		 ttWoofer1dX[WOOFER_ROW_SIZE], ttWoofer1dY[WOOFER_COL_SIZE];
	float		 ttWooferShape[MAX_WOOFER_ACTS];
	float		 ttWooferPiston = 0.0;
	static float hrt_ttWfr9ActMicronsPerMASX_Prev = -1.0, hrt_ttWfr9ActMicronsPerMASY_Prev = -1.0;
	static float ttWooferVectorX[MAX_WOOFER_ACTS], ttWooferVectorY[MAX_WOOFER_ACTS];

	double		  error_t_X = 0.0,   error_t_Y = 0.0;
	static double error_tm1_X = 0.0, error_tm1_Y = 0.0;
	double		  tmpTtDataX = 0.0,  tmpTtDataY = 0.0;
	static double prevOutputX = 0.0, prevOutputY = 0.0;

	float *ttData = reconBufPtr->reconTTData;

	float tmpTTWfrPhase[MAX_WOOFER_ACTS];  //XXXdwp110516 -- for testing only

	reconBufPtr->reconClipCleanupData[eClipCleanupTTClipCount] = 0;
	reconBufPtr->reconClipCleanupData[eClipCleanupTTWfrClipCount] = 0;

//	apply rotation
//	> the rotation corrections were introducing a very small amount of cross-talk -- so,
//	> leave out, unless they become necessary
//	ttData[eTTxErrorCentUnits] = r1x * xTTCentError + r1y * yTTCentError;
//	ttData[eTTyErrorCentUnits] = r2x * xTTCentError + r2y * yTTCentError;
	ttData[eTTxErrorCentUnits] = xTTCentError;
	ttData[eTTyErrorCentUnits] = yTTCentError;

//XXXdwp110512 -- start -- put this back
//	convert to angles (in milli-arcseconds (MAS)) and apply offset
	ttData[eTTxErrorAnglePreLQG] = ttData[eTTxErrorCentUnits] * MAS__PER__MICRON_SLOPE_PER_SUBAP - hrt_tt_x_offset;
	ttData[eTTyErrorAnglePreLQG] = ttData[eTTyErrorCentUnits] * MAS__PER__MICRON_SLOPE_PER_SUBAP - hrt_tt_y_offset;
//XXXdwp110512 -- end -- put this back */
/*/ *XXXdwp110512 -- start -- T/T response test -- take this out
//	convert to angles (in milli-arcseconds (MAS)) and apply offset
	{
		static const int switchFreq = 2;
		static const float switchOffsetMAS = 100.0;
		static int switchCntr = 0;
		static float switchCrntOffset = -switchOffsetMAS;
		if (++switchCntr >= (LegalFrameRates[hrt_camFrameRate] / (switchFreq * 2)))
		{
			switchCntr = 0;
			switchCrntOffset = -switchCrntOffset;
		}
		ttData[eTTxErrorAnglePreLQG] = ttData[eTTxErrorCentUnits] * MAS__PER__MICRON_SLOPE_PER_SUBAP - hrt_tt_x_offset + switchCrntOffset;
		ttData[eTTyErrorAnglePreLQG] = ttData[eTTyErrorCentUnits] * MAS__PER__MICRON_SLOPE_PER_SUBAP - hrt_tt_y_offset + switchCrntOffset;
	}
// XXXdwp110512 -- end -- T/T response test -- take this out */
	
//	apply the LQG filter
	ttData[eTTxErrorAngle] = ttData[eTTxErrorAnglePreLQG];
	ttData[eTTyErrorAngle] = ttData[eTTyErrorAnglePreLQG];
	if (hrt_ttLQGSwitch)
	{
		error_t_X = (double)ttData[eTTxErrorAngle] - prevOutputX;
		error_t_Y = (double)ttData[eTTyErrorAngle] - prevOutputY;

		tmpTtDataX = error_tm1_X;
		tmpTtDataY = error_tm1_Y;
		gpAoHrtTipTilt_LQGFilt( &tmpTtDataX, &tmpTtDataY );
		ttData[eTTxErrorAngle] = (float)tmpTtDataX;
		ttData[eTTyErrorAngle] = (float)tmpTtDataY;

		prevOutputX = tmpTtDataX;
		prevOutputY = tmpTtDataY;
		error_tm1_X = error_t_X;
		error_tm1_Y = error_t_Y;
	}

//	apply the Butterworth temporal split filter to determine what goes onto the TT stage (low pass) and what
//	> goes onto the woofer (high pass)
	inX_t0 = (double)ttData[eTTxErrorAngle];
	inY_t0 = (double)ttData[eTTyErrorAngle];

	outLowX_t0 = b[hrt_camFrameRate][0] * inX_t0;
	outLowY_t0 = b[hrt_camFrameRate][0] * inY_t0;
	for (j = 0; j < SPLIT_FILTER_ORDER; j++)
	{
		outLowX_t0 += b[hrt_camFrameRate][j + 1] * prevInputsX[j] - a[hrt_camFrameRate][j + 1] * prevOutputsX[j];
		outLowY_t0 += b[hrt_camFrameRate][j + 1] * prevInputsY[j] - a[hrt_camFrameRate][j + 1] * prevOutputsY[j];
	}

//	split depending on hrt_ttSplitSwitch (for testing purposes)
//XXXDWP110914 -- start
	if (reconBufPtr->skipFrameFlag)
	{
		ttData[eTTxErrorAngleStage]  = 0.0;
		ttData[eTTxErrorAngleWoofer] = 0.0;
		ttData[eTTyErrorAngleStage]  = 0.0;
		ttData[eTTyErrorAngleWoofer] = 0.0;
	}
	else
//XXXDWP110914 -- end
	{
		switch (hrt_ttSplitSwitch)
		{
			case 0:  // do the normal filter split
			default:
				ttData[eTTxErrorAngleStage]  = (float) outLowX_t0;			  // low pass, X
				ttData[eTTxErrorAngleWoofer] = (float)(inX_t0 - outLowX_t0);  // high pass, X
				ttData[eTTyErrorAngleStage]  = (float) outLowY_t0;			  // low pass, Y
				ttData[eTTyErrorAngleWoofer] = (float)(inY_t0 - outLowY_t0);  // high pass, Y
				break;

			case 1:  // send all to the TT stage
				ttData[eTTxErrorAngleStage]  = ttData[eTTxErrorAngle];
				ttData[eTTxErrorAngleWoofer] = 0.0;
				ttData[eTTyErrorAngleStage]  = ttData[eTTyErrorAngle];
				ttData[eTTyErrorAngleWoofer] = 0.0;
				break;

			case 2:  // send all to the woofer
				ttData[eTTxErrorAngleStage]  = 0.0;
				ttData[eTTxErrorAngleWoofer] = ttData[eTTxErrorAngle];
				ttData[eTTyErrorAngleStage]  = 0.0;
				ttData[eTTyErrorAngleWoofer] = ttData[eTTyErrorAngle];
				break;
		}
	}

//	save filter settings for next time
	for (j = SPLIT_FILTER_LAG - 1; j > 0; j--)
	{
		prevInputsX[j] = prevInputsX[j - 1];
		prevInputsY[j] = prevInputsY[j - 1];
	}
	prevInputsX[0] = inX_t0;
	prevInputsY[0] = inY_t0;

	for (j = SPLIT_FILTER_ORDER - 1; j > 0; j--)
	{
		prevOutputsX[j] = prevOutputsX[j - 1];
		prevOutputsY[j] = prevOutputsY[j - 1];
	}
	prevOutputsX[0] = outLowX_t0;
	prevOutputsY[0] = outLowY_t0;

//	integrate the TT stage settings and convert to volts
	if ( hrt_AoTTLoopStat == eAOLoopClosing )  // zero the integrators
	{
		aoLoopSettleCntr = 0;
//		hrt_AoTTLoopStat gets set to eAOLoopClosed down below
//XXXdwp110803		xSettingAngle = 0.0;
//XXXdwp110803		ySettingAngle = 0.0;
//XXXdwp110803		xSettingAngleWoofer = 0.0;  // used only for r0 calculations
//XXXdwp110803		ySettingAngleWoofer = 0.0;  // used only for r0 calculations

	}
	else if (( hrt_AoTTLoopStat == eAOLoopClosed ) || ( hrt_AoTTLoopStat == eAOLoopAlmostClosed ))
	{
		xSettingAngle = xSettingAngle * hrt_modal_loss_tt + ttData[eTTxErrorAngleStage] * xGainStage;
		ySettingAngle = ySettingAngle * hrt_modal_loss_tt + ttData[eTTyErrorAngleStage] * yGainStage;
		xSettingAngleWoofer = xSettingAngleWoofer + ttData[eTTxErrorAngleWoofer] * xGainWfrSurface;  // used only for r0 calculation
		ySettingAngleWoofer = ySettingAngleWoofer + ttData[eTTyErrorAngleWoofer] * yGainWfrSurface;  // used only for r0 calculation
	}
	else  // don't integrate if loops aren't closed or closing
	{
		xSettingAngle = ttData[eTTxErrorAngleStage] * xGainStage;
		ySettingAngle = ttData[eTTyErrorAngleStage] * yGainStage;
		xSettingAngleWoofer = ttData[eTTxErrorAngleWoofer] * xGainWfrSurface;  // used only for r0 calculation
		ySettingAngleWoofer = ttData[eTTyErrorAngleWoofer] * yGainWfrSurface;  // used only for r0 calculation
	}
	ttData[eTTxSettingAngle] = xSettingAngle;
	ttData[eTTySettingAngle] = ySettingAngle;
	respFromConvTT = gpAoHrtVolt_ConvTT( &ttData[eTTxSettingAngle], 
										 &ttData[eTTxSettingVolts],
										 &ttData[eTTxSettingAngleUnclipped] );
	ttData[eTTxSettingAngleWoofer] = xSettingAngleWoofer;
	ttData[eTTySettingAngleWoofer] = ySettingAngleWoofer;

//	scalar multiply the woofer portions of the tip and tilt temporal splits by the woofer tip and tilt 
//	> vectors and add the results to each other to give this time step's residual tip/tilt woofer shape
	tmpXData = ttData[eTTxErrorAngleWoofer] * xGainWfrSurface;
	tmpYData = ttData[eTTyErrorAngleWoofer] * yGainWfrSurface;
	for (i = 0; i < MAX_WOOFER_ACTS; i++)
		ttWooferShape[i] = tmpXData * ttWooferVectorX[i] + tmpYData * ttWooferVectorY[i];

//	flip data in x for woofer, integrate (if necessary), and gather piston data
	if ( lastReconBufPtr == NULL )  // if there's no previous frame, don't integrate
	{
		for( yy=0; yy<WOOFER_COL_SIZE; yy++)
		{
			for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
			{
				tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
				reconBufPtr->reconTTWfrPhase[ tmpNdx ] = ttWooferShape[ xxIn + yy * WOOFER_ROW_SIZE ];
				ttWooferPiston += reconBufPtr->reconTTWfrPhase[ tmpNdx ];
			}
		}
	}
	else if (( hrt_AoTTLoopStat != eAOLoopClosed ) && ( hrt_AoTTLoopStat != eAOLoopAlmostClosed ))
	{
		// If loop is closing, zero the integrators
		// IMPORTANT -- this implies that the woofer should be commanded to its bias before closing the loop
		if ( hrt_AoTTLoopStat == eAOLoopClosing )
		{
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
//XXXdwp110803					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
//XXXdwp110803					reconBufPtr->reconTTWfrPhase[ tmpNdx ] = 0.0;  // if the wfr is closed, this will get re-written immediately below
				}
			}
		}
		if (( hrt_AoWfrLoopStat == eAOLoopClosed ) || ( hrt_AoWfrLoopStat == eAOLoopAlmostClosed )) // if TT is open but wfr is closed or almost closed, just pass along the prev woofer phase
		{
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					reconBufPtr->reconTTWfrPhase[ tmpNdx ] = lastReconBufPtr->reconWfrPhase[ tmpNdx ];
					ttWooferPiston += reconBufPtr->reconTTWfrPhase[tmpNdx];
				}
			}
		}
		else  // if neither TT nor wfr are closed, don't integrate
		{
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					reconBufPtr->reconTTWfrPhase[ tmpNdx ] = ttWooferShape[ xxIn + yy * WOOFER_ROW_SIZE ];
					ttWooferPiston += reconBufPtr->reconTTWfrPhase[tmpNdx];
				}
			}
		}
	}
	else  // TT is almost closed or closed, so integrate
	{
		if ( hrt_AoWfrLoopStat != eAOLoopClosed )  // if TT is closed but wfr is open, integrate without the woofer
		{
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					reconBufPtr->reconTTWfrPhase[ tmpNdx ] = 
						lastReconBufPtr->reconTTWfrPhase[ tmpNdx ] /* * hrt_modal_loss_dm */ +
							ttWooferShape[ xxIn + yy * WOOFER_ROW_SIZE ];
					ttWooferPiston += reconBufPtr->reconTTWfrPhase[tmpNdx];
					reconBufPtr->reconWfrPhase[tmpNdx] = reconBufPtr->reconTTWfrPhase[tmpNdx];  //XXXdwp110803 -- for dispwfr, primarily
				}
			}
		}
		else  // if both TT and wfr are closed, integrate with the woofer
		{
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					reconBufPtr->reconTTWfrPhase[ tmpNdx ] = 
						lastReconBufPtr->reconWfrPhase[ tmpNdx ] /* * hrt_modal_loss_dm -- don't leak in TT */ +
							ttWooferShape[ xxIn + yy * WOOFER_ROW_SIZE ];
					ttWooferPiston += reconBufPtr->reconTTWfrPhase[tmpNdx];
				}
			}
		}
	}

//	take out woofer piston
	ttWooferPiston /= MAX_WOOFER_ACTS;
	for (i = 0; i < MAX_WOOFER_ACTS; i++)
		reconBufPtr->reconTTWfrPhase[i] -= ttWooferPiston;
	ttData[eTTPistonWoofer] = ttWooferPiston;

//XXXdwp110516 -- for testing only -- start
	for( yy=0; yy<WOOFER_COL_SIZE; yy++)
	{
		for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
		{
			tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
			tmpTTWfrPhase[tmpNdx] = reconBufPtr->reconTTWfrPhase[tmpNdx] + hrt_wfrLabFlat[xxIn + yy * WOOFER_ROW_SIZE];
		}
	}
//XXXdwp110516 -- for testing only -- end
//	convert woofer phase to volts
	respFromConvWfr = gpAoHrtVolt_ConvWoofer( tmpTTWfrPhase,  //XXXdwp110516 -- for testing only -- remove this
//XXXdwp110516 -- for testing only -- remove this	respFromConvWfr = gpAoHrtVolt_ConvWoofer( reconBufPtr->reconTTWfrPhase, 
											  reconBufPtr->reconTTWfrVolts, 
											  reconBufPtr->reconTTWfrUnclippedPhase );

//	output to TT stage and woofer mirror, if the loop is closed
	if ( respFromConvTT > 0)
		SysConSendStrngNoParm(eDebugLevelErr, "(gpAoHrtTipTilt_Process): 'gpAoHrtVolt_ConvTT' failed - no write");
	else if ( respFromConvWfr > 0)
		SysConSendStrngNoParm(eDebugLevelErr, "(gpAoHrtTipTilt_Process): 'gpAoHrtVolt_ConvWoofer' failed - no write");
	else if (( hrt_AoTTLoopStat == eAOLoopClosed ) || ( hrt_AoTTLoopStat == eAOLoopAlmostClosed ))
	{
		gpAoHrtVolt_WriteTTVolts( &ttData[eTTxSettingVolts], false );  // 'false' says not to physically write, because we're about to do that anyway
		gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconTTWfrVolts, false );
		reconBufPtr->reconClipCleanupData[eClipCleanupTTClipCount] = -respFromConvTT;  // respFromConv will be negative if clipping occurred
		reconBufPtr->reconClipCleanupData[eClipCleanupTTWfrClipCount] = -respFromConvWfr;  // respFromConv will be negative if clipping occurred
	}

//	update loop status
	if ( hrt_AoTTLoopStat == eAOLoopClosing )
	{
		hrt_AoTTLoopStat = eAOLoopAlmostClosed;
		hrtFileDataShmPtr->aoTTLoopStat = eAOLoopAlmostClosed;
	}
	else if ( hrt_AoTTLoopStat == eAOLoopAlmostClosed )
	{
		hrt_AoTTLoopStat = eAOLoopClosed;
		hrtFileDataShmPtr->aoTTLoopStat = eAOLoopClosed;
	}
	else if ( hrt_AoTTLoopStat != eAOLoopClosed )
	{
		if( hrt_AoTTLoopStat == eAOLoopOpening )
		{
			hrt_AoTTLoopStat = eAOLoopOpen;
			hrtFileDataShmPtr->aoTTLoopStat = eAOLoopOpen;
		}

//		propagate the current tt settings, primarily for r0 calculations
		if( lastReconBufPtr == NULL )
		{
			ttData[eTTxSettingAngle]	 = 0.0;
			ttData[eTTySettingAngle]	 = 0.0;
			ttData[eTTxErrorAngleWoofer] = 0.0;
			ttData[eTTyErrorAngleWoofer] = 0.0;
		}
		else
		{
			ttData[eTTxSettingAngle]	 = lastReconBufPtr->reconTTData[eTTxSettingAngle];
			ttData[eTTySettingAngle]	 = lastReconBufPtr->reconTTData[eTTySettingAngle];
			ttData[eTTxErrorAngleWoofer] = lastReconBufPtr->reconTTData[eTTxErrorAngleWoofer];
			ttData[eTTyErrorAngleWoofer] = lastReconBufPtr->reconTTData[eTTyErrorAngleWoofer];
		}

//		only recalculate TT woofer vectors when the loop is open (and a MicronsPerMAS has changed)
		if (hrt_ttWfr9ActMicronsPerMASX != hrt_ttWfr9ActMicronsPerMASX_Prev)
		{
			hrt_ttWfr9ActMicronsPerMASX_Prev = hrt_ttWfr9ActMicronsPerMASX;
//			calculate the 1d TT woofer vector values for X and then spread them over the X vector
			for (i = 0; i < WOOFER_ROW_SIZE; i++)
				ttWoofer1dX[i] = hrt_ttWfr9ActMicronsPerMASX * ttWoofer1dFactorsX[i];
			for (j = 0; j < WOOFER_COL_SIZE; j++)
			{
				for (i = 0; i < WOOFER_ROW_SIZE; i++)
					ttWooferVectorX[j * WOOFER_ROW_SIZE + i] = ttWoofer1dX[i];
			}
		}
		if (hrt_ttWfr9ActMicronsPerMASY != hrt_ttWfr9ActMicronsPerMASY_Prev)
		{
			hrt_ttWfr9ActMicronsPerMASY_Prev = hrt_ttWfr9ActMicronsPerMASY;
//			calculate the 1d TT woofer vector values for Y and then spread them over the Y vector
			for (i = 0; i < WOOFER_COL_SIZE; i++)
				ttWoofer1dY[i] = hrt_ttWfr9ActMicronsPerMASY * ttWoofer1dFactorsY[i];
			for (j = 0; j < WOOFER_ROW_SIZE; j++)
			{
				for (i = 0; i < WOOFER_COL_SIZE; i++)
					ttWooferVectorY[i * WOOFER_COL_SIZE + j] = ttWoofer1dY[i];
			}
		}

//		move the TT stage, if we need to
		if( newMoveTT >= 0 )
		{
			if( newMoveTT != 0 )  // volts
			{
				ttData[eTTxSettingAngle] = 0.0;
				ttData[eTTySettingAngle] = 0.0;
				ttData[eTTxSettingVolts] = xNewSetting;
				ttData[eTTySettingVolts] = yNewSetting;
				ttData[eTTxSettingAngleWoofer] = 0.0;  // this assumes that a flat shape is being put on the woofer
				ttData[eTTySettingAngleWoofer] = 0.0;  // > tip/tilt should really be projected from the new shape
			}
			else  // angle
			{
				ttData[eTTxSettingAngle] = xNewSetting;
				ttData[eTTySettingAngle] = yNewSetting;
				gpAoHrtVolt_ConvTT( &ttData[eTTxSettingAngle], 
									&ttData[eTTxSettingVolts],
									&ttData[eTTxSettingAngleUnclipped] );
				ttData[eTTxSettingAngleWoofer] = 0.0;  // this assumes that a flat shape is being put on the woofer
				ttData[eTTySettingAngleWoofer] = 0.0;  // > tip/tilt should really be projected from the new shape
			}
			gpAoHrtVolt_WriteTTVolts( &ttData[eTTxSettingVolts], true );

			newMoveTT = -1;
		}
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtTipTilt_LQGFilt( double *xTTError, double *yTTError );
 *
 *	@brief
 *  This unit performs tip/tilt LQG filter calculations.
 *
 *	@b DESCRIPTION:
 *	This unit performs calculations for the tip/tilt (TT) LQG filter described 
 *	in TTControlRecipe4.pdf by J-P Veran and Lisa Poyneer.  This filter is a 
 *	Kalman-like filter in that it is constructed in a Kalman filter framework.  
 *	But, it does not self-optimize (i.e., the gains are not adjusted in real-time).  
 *	Rather, the filter parameters are read in depending on three external conditions:
 *	wind power, seeing, and star brightness.  
 *	@par
 *
 *	@b Usage: gpAoHrtTipTilt_LQGFilt( double *xTTError, double *yTTError )
 * 
 *	@param[in/out] xTTError	(double*):	X TT error, unfiltered on the way in, 
 *											filtered on the way out
 *	@param[in/out] yTTError (double*):	Y TT error, unfiltered on the way in, 
 *											filtered on the way out
 *
 *	@return 
 *	None.
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtTipTilt_LQGFilt( double *xTTError, double *yTTError )
{
	int i;

	double zX, zY, rX, rY;

	ttComplex gT[2][2][NG];   // g	     -- first index for tPlusOne/tPlusTwo -- second index for x/y -- third index for g1, g2
	double	  aRT[2][2][NA];  // a, real -- first index for tPlusOne/tPlusTwo -- second index for x/y -- third index for a1, a2, a3
	double	  aIT[2][2][NA];  // a, imag -- first index for tPlusOne/tPlusTwo -- second index for x/y -- third index for a1, a2, a3
	double	  phiT[2][2];	  // phi	 -- first index for tPlusOne/tPlusTwo -- second index for x/y
	double	  result[2][2];

//	if any of the TLC values have changed, load corresponding filter parameters and Kalman gains
	boolean updateParams = false;
	if (oldSeeing != hrt_ttSeeing)  // IMPORTANT: please note that we are NOT using seeing in the LQG -- the param can 
	{								//			  > still be changed in the parms file
		oldSeeing = hrt_ttSeeing;
		updateParams = true;
		seeingNdx = 0;
		while ((hrt_ttSeeing >= hrtFileDataShmPtr->ttLQGAllRanges[eTtEnvSeeing][seeingNdx + 1]) && 
			   (seeingNdx < (NS - 1)))
			seeingNdx++;
	}
	if (oldWindPower != hrt_ttWindPower)  // IMPORTANT: please note that we are NOT using wind power in the LQG -- the param can 
	{									  //			> still be changed in the parms file
		oldWindPower = hrt_ttWindPower;
		updateParams = true;
		windPowerNdx = 0;
		while ((hrt_ttWindPower >= hrtFileDataShmPtr->ttLQGAllRanges[eTtEnvWindPower][windPowerNdx + 1]) && 
			   (windPowerNdx < (NW - 1)))
			windPowerNdx++;
	}
	if (oldStarBrightness != hrtFileDataShmPtr->ttStarBrightness)  // the shared memory version needs to be checked, here, 
	{															   // > because eCmdActObsCfg can change it
		hrt_ttStarBrightness = hrtFileDataShmPtr->ttStarBrightness;
		oldStarBrightness = hrt_ttStarBrightness;
		updateParams = true;
		starBrightnessNdx = 0;
		while ((hrt_ttStarBrightness >= hrtFileDataShmPtr->ttLQGAllRanges[eTtEnvStarBrightness][starBrightnessNdx + 1]) && 
			   (starBrightnessNdx < (NR - 1)))
			starBrightnessNdx++;
	}

	if (updateParams)
	{
		crnt_r	   = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eAtmWind_r_and_g1];
		crnt_theta = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eAtmWind_theta_and_g2];

		for ( i = 0; i < NA; i++ )
		{
			crnt_alphasR[i]	   = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][AS_R_IN_FITS[i]];
			crnt_alphasI[i]	   = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][AS_I_IN_FITS[i]];
			crnt_ks_for_asR[i] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + AS_R_IN_FITS[i]];
			crnt_ks_for_asI[i] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + AS_I_IN_FITS[i]];
		}

		for ( i = 0; i < NB; i++ )
		{
			crnt_betasR[i]	   = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][BS_R_IN_FITS[i]];
			crnt_betasI[i]	   = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][BS_I_IN_FITS[i]];
			crnt_ks_for_bsR[i] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + BS_R_IN_FITS[i]];
			crnt_ks_for_bsI[i] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + BS_I_IN_FITS[i]];
		}

		for ( i = 0; i < NG; i++ )  // IMPORTANT - the Ks for gs are the only complex Ks
		{
			crnt_ks_for_gs[i][0] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + GS_IN_FITS[i]];
			crnt_ks_for_gs[i][1] = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][KALMAN_GAIN_IMAG_OFFSET + GS_IN_FITS[i]];
		}

		crnt_k_for_phi = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + ePhi];
		crnt_k_for_rho = hrtFileDataShmPtr->ttLQGAllParams[seeingNdx][windPowerNdx][starBrightnessNdx][eKalmanGainsOffset + eRho];
	}

//	stage 1: Update controller state
//	step 1.1: Compute the atm+wind model states
//	g[newNdx][xNdx][1] = crnt_r * cexp(I * crnt_theta) * g[oldNdx][xNdx][1];  // g2 for X
//	g[newNdx][yNdx][1] = crnt_r * cexp(I * crnt_theta) * g[oldNdx][yNdx][1];  // g2 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = crnt_theta;
	ttCexp(tmpC1, tmpC2);		  							// tmpC2 = cexp(I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);							// tmpC1 = crnt_r * cexp(I * crnt_theta)
	ttCmul(tmpC1, g[oldNdx][xNdx][1], g[newNdx][xNdx][1]);  // g[newNdx][xNdx][1] = crnt_r * cexp(I * crnt_theta) * g[oldNdx][xNdx][1];  // g2 for X
	ttCmul(tmpC1, g[oldNdx][yNdx][1], g[newNdx][yNdx][1]);  // g[newNdx][yNdx][1] = crnt_r * cexp(I * crnt_theta) * g[oldNdx][yNdx][1];  // g2 for X

//	g[newNdx][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][xNdx][0] + g[oldNdx][xNdx][1];  // g1 for X
//	g[newNdx][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][yNdx][0] + g[oldNdx][yNdx][1];  // g1 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = -crnt_theta;
	ttCexp(tmpC1, tmpC2);		  							// tmpC2 = cexp(-I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);							// tmpC1 = crnt_r * cexp(-I * crnt_theta)
	ttCmul(tmpC1, g[oldNdx][xNdx][0], tmpC2);				// tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][xNdx][0]
	ttCadd(tmpC2, g[oldNdx][xNdx][1], g[newNdx][xNdx][0]);  // g[newNdx][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][xNdx][0] + g[oldNdx][xNdx][1];  // g1 for X
	ttCmul(tmpC1, g[oldNdx][yNdx][0], tmpC2);				// tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][yNdx][0]
	ttCadd(tmpC2, g[oldNdx][yNdx][1], g[newNdx][yNdx][0]);  // g[newNdx][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[oldNdx][yNdx][0] + g[oldNdx][yNdx][1];  // g1 for Y

//	step 1.2: Compute CP vibration states
//	step 1.3: Compute total phase state (phi)
	phi[newNdx][xNdx] = g[oldNdx][xNdx][0][0];	// phi for X, start
	phi[newNdx][yNdx] = g[oldNdx][yNdx][0][0];  // phi for Y, start
	for ( i = 0; i < NA; i++ )
	{
		aR[newNdx][xNdx][i] = crnt_alphasR[i] * aR[oldNdx][xNdx][i] - crnt_alphasI[i] * aI[oldNdx][xNdx][i];  // a, Real for X
		aR[newNdx][yNdx][i] = crnt_alphasR[i] * aR[oldNdx][yNdx][i] - crnt_alphasI[i] * aI[oldNdx][yNdx][i];  // a, Real for Y

		aI[newNdx][xNdx][i] = crnt_alphasI[i] * aR[oldNdx][xNdx][i] + crnt_alphasR[i] * aI[oldNdx][xNdx][i];  // a, Imag for X
		aI[newNdx][yNdx][i] = crnt_alphasI[i] * aR[oldNdx][yNdx][i] + crnt_alphasR[i] * aI[oldNdx][yNdx][i];  // a, Imag for Y

		phi[newNdx][xNdx] += aR[oldNdx][xNdx][i];  // phi for X, finish
		phi[newNdx][yNdx] += aR[oldNdx][yNdx][i];  // phi for Y, finish
	}

//	step 1.4: Compute NCP vibration states
//	step 1.5: Compute total NCP state (rho)
	rho[newNdx][xNdx] = 0.0;  // rho for X, start
	rho[newNdx][yNdx] = 0.0;  // rho for Y, start
	for ( i = 0; i < NB; i++ )
	{
		bR[newNdx][xNdx][i] = crnt_betasR[i] * bR[oldNdx][xNdx][i] - crnt_betasI[i] * bI[oldNdx][xNdx][i];  // b, Real for X
		bR[newNdx][yNdx][i] = crnt_betasR[i] * bR[oldNdx][yNdx][i] - crnt_betasI[i] * bI[oldNdx][yNdx][i];  // b, Real for Y

		bI[newNdx][xNdx][i] = crnt_betasI[i] * bR[oldNdx][xNdx][i] + crnt_betasR[i] * bI[oldNdx][xNdx][i];  // b, Imag for X
		bI[newNdx][yNdx][i] = crnt_betasI[i] * bR[oldNdx][yNdx][i] + crnt_betasR[i] * bI[oldNdx][yNdx][i];  // b, Imag for Y

		rho[newNdx][xNdx] += bR[oldNdx][xNdx][i];  // rho for X, finish
		rho[newNdx][yNdx] += bR[oldNdx][yNdx][i];  // rho for Y, finish
	}
			
//	step 2: Compute pseudo-open loop measurement
	delta = hrt_ttCompDelay_microSecs / 1000000.0 * (float)(LegalFrameRates[hrt_camFrameRate]);
	zX = *xTTError - ((delta - 1.0) * result_tMinusOneX - delta * result_tMinusTwoX);  // z for X
	zY = *yTTError - ((delta - 1.0) * result_tMinusOneY - delta * result_tMinusTwoY);  // z for Y

//	step 3: Compute the scaled, estimated measurement
	rX = phi[newNdx][xNdx] + rho[newNdx][xNdx];
	rY = phi[newNdx][yNdx] + rho[newNdx][yNdx];
	for ( i = 0; i < NG; i++ )
	{
//		g[newNdx][xNdx][i] -= crnt_ks_for_gs[i] * rX;  // g for X
		tmpC1[0] = rX;
		tmpC1[1] = 0.0;
		ttCmul(tmpC1, crnt_ks_for_gs[i], tmpC2);				// tmpC2 = crnt_ks_for_gs[i] * zX
		ttCsub(g[newNdx][xNdx][i], tmpC2, g[newNdx][xNdx][i]);  // g[newNdx][xNdx][i] -= crnt_ks_for_gs[i] * zX;  // g for X
//		g[newNdx][yNdx][i] -= crnt_ks_for_gs[i] * rY;  // g for Y
		tmpC1[0] = rY;
		tmpC1[1] = 0.0;
		ttCmul(tmpC1, crnt_ks_for_gs[i], tmpC2);				// tmpC2 = crnt_ks_for_gs[i] * zY
		ttCsub(g[newNdx][yNdx][i], tmpC2, g[newNdx][yNdx][i]);  // g[newNdx][yNdx][i] -= crnt_ks_for_gs[i] * zY;  // g for Y
	}
	for ( i = 0; i < NA; i++ )
	{
		aR[newNdx][xNdx][i] -= crnt_ks_for_asR[i] * rX;  // a, Real for X
		aR[newNdx][yNdx][i] -= crnt_ks_for_asR[i] * rY;  // a, Real for Y
		aI[newNdx][xNdx][i] -= crnt_ks_for_asI[i] * rX;  // a, Imag for X
		aI[newNdx][yNdx][i] -= crnt_ks_for_asI[i] * rY;  // a, Imag for Y
	}
	phi[newNdx][xNdx] -= crnt_k_for_phi * rX;  // phi for X
	phi[newNdx][yNdx] -= crnt_k_for_phi * rY;  // phi for Y
	for ( i = 0; i < NB; i++ )
	{
		bR[newNdx][xNdx][i] -= crnt_ks_for_bsR[i] * rX;  // b, Real for X
		bR[newNdx][yNdx][i] -= crnt_ks_for_bsR[i] * rY;  // b, Real for Y
		bI[newNdx][xNdx][i] -= crnt_ks_for_bsI[i] * rX;  // b, Imag for X
		bI[newNdx][yNdx][i] -= crnt_ks_for_bsI[i] * rY;  // b, Imag for Y
	}
	rho[newNdx][xNdx] -= crnt_k_for_rho * rX;  // rho for X, start
	rho[newNdx][yNdx] -= crnt_k_for_rho * rY;  // rho for Y, start

//	step 4: Compute the new estimated state
	for ( i = 0; i < NG; i++ )
	{
//		g[newNdx][xNdx][i] += crnt_ks_for_gs[i] * zX;  // g for X
		tmpC1[0] = zX;
		tmpC1[1] = 0.0;
		ttCmul(tmpC1, crnt_ks_for_gs[i], tmpC2);				// tmpC2 = crnt_ks_for_gs[i] * zX
		ttCadd(g[newNdx][xNdx][i], tmpC2, g[newNdx][xNdx][i]);  // g[newNdx][xNdx][i] += crnt_ks_for_gs[i] * zX;  // g for X
//		g[newNdx][yNdx][i] += crnt_ks_for_gs[i] * zY;  // g for Y
		tmpC1[0] = zY;
		tmpC1[1] = 0.0;
		ttCmul(tmpC1, crnt_ks_for_gs[i], tmpC2);				// tmpC2 = crnt_ks_for_gs[i] * zY
		ttCadd(g[newNdx][yNdx][i], tmpC2, g[newNdx][yNdx][i]);  // g[newNdx][yNdx][i] += crnt_ks_for_gs[i] * zY;  // g for Y
	}
	for ( i = 0; i < NA; i++ )
	{
		aR[newNdx][xNdx][i] += crnt_ks_for_asR[i] * zX;  // a, Real for X
		aR[newNdx][yNdx][i] += crnt_ks_for_asR[i] * zY;  // a, Real for Y
		aI[newNdx][xNdx][i] += crnt_ks_for_asI[i] * zX;  // a, Imag for X
		aI[newNdx][yNdx][i] += crnt_ks_for_asI[i] * zY;  // a, Imag for Y
	}
	phi[newNdx][xNdx] += crnt_k_for_phi * zX;  // phi for X
	phi[newNdx][yNdx] += crnt_k_for_phi * zY;  // phi for Y
	for ( i = 0; i < NB; i++ )
	{
		bR[newNdx][xNdx][i] += crnt_ks_for_bsR[i] * zX;  // b, Real for X
		bR[newNdx][yNdx][i] += crnt_ks_for_bsR[i] * zY;  // b, Real for Y
		bI[newNdx][xNdx][i] += crnt_ks_for_bsI[i] * zX;  // b, Imag for X
		bI[newNdx][yNdx][i] += crnt_ks_for_bsI[i] * zY;  // b, Imag for Y
	}
	rho[newNdx][xNdx] += crnt_k_for_rho * zX;  // rho for X, start
	rho[newNdx][yNdx] += crnt_k_for_rho * zY;  // rho for Y, start

//	stage 2: Estimate TTM command
//	step 1: Compute one-step ahead prediction
//	gT[tPlusOne][xNdx][1] = crnt_r * cexp(I * crnt_theta) * g[newNdx][xNdx][1];  // g2 for X
//	gT[tPlusOne][yNdx][1] = crnt_r * cexp(I * crnt_theta) * g[newNdx][yNdx][1];  // g2 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = crnt_theta;
	ttCexp(tmpC1, tmpC2);		  							   // tmpC2 = cexp(I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);							   // tmpC1 = crnt_r * cexp(I * crnt_theta)
	ttCmul(tmpC1, g[newNdx][xNdx][1], gT[tPlusOne][xNdx][1]);  // gT[tPlusOne][xNdx][1] = crnt_r * cexp(I * crnt_theta) * g[newNdx][xNdx][1];  // g2 for X
	ttCmul(tmpC1, g[newNdx][yNdx][1], gT[tPlusOne][yNdx][1]);  // gT[tPlusOne][yNdx][1] = crnt_r * cexp(I * crnt_theta) * g[newNdx][yNdx][1];  // g2 for Y

//	gT[tPlusOne][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[newNdx][xNdx][0] + g[newNdx][xNdx][1];  // g1 for X
//	gT[tPlusOne][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[newNdx][yNdx][0] + g[newNdx][yNdx][1];  // g1 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = -crnt_theta;
	ttCexp(tmpC1, tmpC2);		  							   // tmpC2 = cexp(-I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);							   // tmpC1 = crnt_r * cexp(-I * crnt_theta)
	ttCmul(tmpC1, g[newNdx][xNdx][0], tmpC2);				   // tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[newNdx][xNdx][0]
	ttCadd(tmpC2, g[newNdx][xNdx][1], gT[tPlusOne][xNdx][0]);  // gT[tPlusOne][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[newNdx][xNdx][0] + g[newNdx][xNdx][1];  // g1 for X
	ttCmul(tmpC1, g[newNdx][yNdx][0], tmpC2);				   // tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[newNdx][yNdx][0]
	ttCadd(tmpC2, g[newNdx][yNdx][1], gT[tPlusOne][yNdx][0]);  // gT[tPlusOne][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[newNdx][yNdx][0] + g[newNdx][yNdx][1];  // g1 for Y

	phiT[tPlusOne][xNdx] = g[newNdx][xNdx][0][0];  // phi for X, start
	phiT[tPlusOne][yNdx] = g[newNdx][yNdx][0][0];  // phi for Y, start
	result[tPlusOne][xNdx] = gT[tPlusOne][xNdx][0][0];  // result for X, start
	result[tPlusOne][yNdx] = gT[tPlusOne][yNdx][0][0];  // result for Y, start
	for ( i = 0; i < NA; i++ )
	{
		aRT[tPlusOne][xNdx][i] = crnt_alphasR[i] * aR[newNdx][xNdx][i] - crnt_alphasI[i] * aI[newNdx][xNdx][i];  // a, Real for X
		aRT[tPlusOne][yNdx][i] = crnt_alphasR[i] * aR[newNdx][yNdx][i] - crnt_alphasI[i] * aI[newNdx][yNdx][i];  // a, Real for Y
		aIT[tPlusOne][xNdx][i] = crnt_alphasI[i] * aR[newNdx][xNdx][i] + crnt_alphasR[i] * aI[newNdx][xNdx][i];  // a, Imag for X
		aIT[tPlusOne][yNdx][i] = crnt_alphasI[i] * aR[newNdx][yNdx][i] + crnt_alphasR[i] * aI[newNdx][yNdx][i];  // a, Imag for Y
		phiT[tPlusOne][xNdx] += aR[newNdx][xNdx][i];  // phi for X, finish
		phiT[tPlusOne][yNdx] += aR[newNdx][yNdx][i];  // phi for Y, finish
		result[tPlusOne][xNdx] += aRT[tPlusOne][xNdx][i];  // result for X, finish
		result[tPlusOne][yNdx] += aRT[tPlusOne][yNdx][i];  // result for Y, finish
	}

//	step 2: Compute two-step ahead prediction
//	gT[tPlusTwo][xNdx][1] = crnt_r * cexp(I * crnt_theta) * gT[tPlusOne][xNdx][1];  // g2 for X
//	gT[tPlusTwo][yNdx][1] = crnt_r * cexp(I * crnt_theta) * gT[tPlusOne][yNdx][1];  // g2 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = crnt_theta;
	ttCexp(tmpC1, tmpC2);		  								  // tmpC2 = cexp(I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);								  // tmpC1 = crnt_r * cexp(I * crnt_theta)
	ttCmul(tmpC1, gT[tPlusOne][xNdx][1], gT[tPlusTwo][xNdx][1]);  // gT[tPlusTwo][xNdx][1] = crnt_r * cexp(I * crnt_theta) * g[tPlusOne][xNdx][1];  // g2 for X
	ttCmul(tmpC1, gT[tPlusOne][yNdx][1], gT[tPlusTwo][yNdx][1]);  // gT[tPlusTwo][yNdx][1] = crnt_r * cexp(I * crnt_theta) * g[tPlusOne][yNdx][1];  // g2 for Y

//	gT[tPlusTwo][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * gT[tPlusOne][xNdx][0] + gT[tPlusOne][xNdx][1];  // g1 for X
//	gT[tPlusTwo][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * gT[tPlusOne][yNdx][0] + gT[tPlusOne][yNdx][1];  // g1 for Y
	tmpC1[0] = 0.0;
	tmpC1[1] = -crnt_theta;
	ttCexp(tmpC1, tmpC2);		  								  // tmpC2 = cexp(-I * crnt_theta)
	tmpC1[0] = crnt_r;
	tmpC1[1] = 0.0;
	ttCmul(tmpC1, tmpC2, tmpC1);								  // tmpC1 = crnt_r * cexp(-I * crnt_theta)
	ttCmul(tmpC1, gT[tPlusOne][xNdx][0], tmpC2);				  // tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[tPlusOne][xNdx][0]
	ttCadd(tmpC2, gT[tPlusOne][xNdx][1], gT[tPlusTwo][xNdx][0]);  // gT[tPlusTwo][xNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[tPlusOne][xNdx][0] + g[tPlusOne][xNdx][1];  // g1 for X
	ttCmul(tmpC1, gT[tPlusOne][yNdx][0], tmpC2);				  // tmpC2 = crnt_r * cexp(-I * crnt_theta) * g[tPlusOne][yNdx][0]
	ttCadd(tmpC2, gT[tPlusOne][yNdx][1], gT[tPlusTwo][yNdx][0]);  // gT[tPlusTwo][yNdx][0] = crnt_r * cexp(-I * crnt_theta) * g[tPlusOne][yNdx][0] + g[tPlusOne][yNdx][1];  // g1 for Y

	phiT[tPlusTwo][xNdx] = gT[tPlusOne][xNdx][0][0];  // phi for X, start
	phiT[tPlusTwo][yNdx] = gT[tPlusOne][yNdx][0][0];  // phi for Y, start
	result[tPlusTwo][xNdx] = gT[tPlusOne][xNdx][0][0];  // result for X, start
	result[tPlusTwo][yNdx] = gT[tPlusOne][yNdx][0][0];  // result for Y, start
	for ( i = 0; i < NA; i++ )
	{
		aRT[tPlusTwo][xNdx][i] = crnt_alphasR[i] * aRT[tPlusOne][xNdx][i] - crnt_alphasI[i] * aIT[tPlusOne][xNdx][i];  // a, Real for X
		aRT[tPlusTwo][yNdx][i] = crnt_alphasR[i] * aRT[tPlusOne][yNdx][i] - crnt_alphasI[i] * aIT[tPlusOne][yNdx][i];  // a, Real for Y
		aIT[tPlusTwo][xNdx][i] = crnt_alphasI[i] * aRT[tPlusOne][xNdx][i] + crnt_alphasR[i] * aIT[tPlusOne][xNdx][i];  // a, Imag for X
		aIT[tPlusTwo][yNdx][i] = crnt_alphasI[i] * aRT[tPlusOne][yNdx][i] + crnt_alphasR[i] * aIT[tPlusOne][yNdx][i];  // a, Imag for Y
		phiT[tPlusTwo][xNdx] += aRT[tPlusOne][xNdx][i];  // phi for X, finish
		phiT[tPlusTwo][yNdx] += aRT[tPlusOne][yNdx][i];  // phi for Y, finish
		result[tPlusTwo][xNdx] += aRT[tPlusTwo][xNdx][i];  // result for X, finish
		result[tPlusTwo][yNdx] += aRT[tPlusTwo][yNdx][i];  // result for Y, finish
	}

//	step 3: Compute d[t + 1]
	*xTTError = (1.0 - delta) * result[tPlusOne][xNdx] + delta * result[tPlusTwo][xNdx];  // result for X
	*yTTError = (1.0 - delta) * result[tPlusOne][yNdx] + delta * result[tPlusTwo][yNdx];  // result for Y

//	save old results for next time
	result_tMinusTwoX = result_tMinusOneX;
	result_tMinusOneX = result_tX;
	result_tX		  = result[tPlusOne][xNdx];  // the result for (t + 1) now becomes the result for t next time, etc.
	result_tMinusTwoY = result_tMinusOneY;
	result_tMinusOneY = result_tY;
	result_tY		  = result[tPlusOne][yNdx];  // the result for (t + 1) now becomes the result for t next time, etc.

//	flip the old and new indeces for next time
	oldNdx ^= 1;
	newNdx ^= 1;
}


/*!
 *******************************************************************************
 *
 *	@fn	static void ttCfuncs( )
 *
 *	@brief
 *	This is a collection of functions to perform double complex math.  
 *
 *	@b DESCRIPTION: 
 *	This is a collection of functions to perform double complex math because the 
 *	C complex library is not available in the kernel.  Given x = a, ib and y = c, id, 
 *	the functions included are:
 *	@par
 *	ttCadd (addition): x + y = (a + c), i(b + d)
 *	@par
 *	ttCsub (subtraction): x - y = (a - c), i(b - d)
 *	@par
 *	ttCmul (multiplication): x * y = (ac - bd), i(ad + bc) 
 *	@par
 *	ttCdiv (division): x / y = (ac + bd) / (c*c + d*d), i(bc - ad) / (c*c + d*d)
 *	@par
 *	ttCabs (absolute value, aka, modulus): cabsf(x) = sqrt( a * a + b * b )
 *	@par
 *	ttCexp (exponentiation): cexpf(x) = (pow(e, a) * cos(b)), i(pow(e, a) * sin(b))
 *	@par
 *	
 *	@b Usage: optCfuncs( opC1, opC2, resC )
 * 
 *	@param[ in ] 	opC1	( ttComplex ):	operand 1 (x in the above)
 *	@param[ in ] 	opC2	( ttComplex ):	operand 2 (y in the above, for binary operators)
 *	@param[ out ]	resC	( ttComplex ):	result (optCabsf's result is double)
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static inline void ttCadd( ttComplex opC1, ttComplex opC2, ttComplex resC )
{
//	given x = a, ib and y = c, id
//	ttCaddf (addition): x + y = (a + c), i(b + d)
	resC[0] = opC1[0] + opC2[0];
	resC[1] = opC1[1] + opC2[1];
}

static inline void ttCsub( ttComplex opC1, ttComplex opC2, ttComplex resC )
{
//	given x = a, ib and y = c, id
//	ttCsubf (subtraction): x - y = (a - c), i(b - d)
	resC[0] = opC1[0] - opC2[0];
	resC[1] = opC1[1] - opC2[1];
}

static inline void ttCmul( ttComplex opC1, ttComplex opC2, ttComplex resC )
{
//	given x = a, ib and y = c, id
//	ttCmulf (multiplication): x * y = (ac - bd), i(ad + bc) 
	double a = opC1[0], b = opC1[1], c = opC2[0], d = opC2[1];
	resC[0] = a * c - b * d;
	resC[1] = a * d + b * c;
}

static inline void ttCdiv( ttComplex opC1, ttComplex opC2, ttComplex resC )
{
//	given x = a, ib and y = c, id
//	ttCdivf (division): x / y = (ac + bd) / (c*c + d*d), i(bc - ad) / (c*c + d*d)
	double a = opC1[0], b = opC1[1], c = opC2[0], d = opC2[1];
	double denom = c * c + d * d;
	if (denom == 0.0)
	{
		resC[0] = 0.0;  //XXX may want to make this NAN
		resC[1] = 0.0;	//XXX may want to make this NAN
	}
	else
	{
		resC[0] = (a * c + b * d) / denom;
		resC[1] = (b * c - a * d) / denom;
	}
}

static inline void ttCabs( ttComplex opC1, double *resF )
{
//	given x = a, ib and y = c, id
//	ttCabsf (absolute value, aka, modulus): cabsf(x) = sqrt( a * a + b * b )
	double a = opC1[0], b = opC1[1];
	*resF = sqrt( a * a + b * b );
}

static inline void ttCexp( ttComplex opC1, ttComplex resC )
{
//	given x = a, ib and y = c, id
//	ttCexpf (exponentiation): cexpf(x) = (pow(e, a) * cos(b)), i(pow(e, a) * sin(b))
	const double e = 2.71828182845905;
	double a = opC1[0], b = opC1[1], powEA;
	powEA   = pow(e, a);
	resC[0] = powEA * cos(b);
	resC[1] = powEA * sin(b);
}


//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
