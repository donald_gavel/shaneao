/*******************************************************************************
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 ******************************************************************************/
/*!
 *	@file gpAoSrtDispCentInt.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to display centroid intensity frames. 
 *
 *	@b DESCRIPTION:
 *	This component contains code to display centroid intensity frames to the monitor.  
 *	It is similar to dispraw (that was based on the graywin.c example code in the 
 *	SDL/test directory).  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 4/26/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDispCentIntCreateScreen()
 *	-# @c gpAoSrtDispCentIntDraw()
 *
 *	@b REVISIONS:
 *
 ********************************************************************************/

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

//#include <gpUtGeneral.h>
//#include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "SDL.h"

// Configuration Variables
#define NUM_COLORS	256
//#define PIX_SIZE    20  // should be even
#define PIX_SIZE    50  // Size of the box on screen in pixels, should be even
#define HALF_PIX	(PIX_SIZE / 2)

#define BACKGROUND_NDX 0
#define GRID_NDX 	   1
#define GOOD_SPOT_NDX  2
#define SAT_SPOT_NDX   3  // saturated spot

static const long secDelay = 0;			   // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate

// Global Variables

// Local Variables
static boolean quit = false;
static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static long long totalSizeExceptRecon;				  // total size of shared memory

static FileDataShmType*			fileDataShmPtr;		  // pointer to file data shared memory
static CamDataType*		 		camDataPtr;			  // pointer to camera shared memory data
static CentDataType*			centDataPtr;		  // pointer to centroid shared memory data
static HrtDataFlowControlType*	dataFlowControlPtr;	  // pointer to Data Flow Control variables

// Local Function Prototypes
static void termHandler(int sigNum);

/*====================================================================================================================

====================================================================================================================*/
static SDL_Surface 
*gpAoSrtDispCentIntCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)
{
SDL_Surface *screen;
int i;
SDL_Color palette[NUM_COLORS];

	// 	set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

	/*/	set a gray colormap from black to white
	for ( i = 0; i < NUM_COLORS; i++ )
	{
		palette[i].r = i * (256 / NUM_COLORS);
		palette[i].g = i * (256 / NUM_COLORS);
		palette[i].b = i * (256 / NUM_COLORS);
	} */

	//	set up colors
	palette[BACKGROUND_NDX].r = 0;
	palette[BACKGROUND_NDX].g = 0;
	palette[BACKGROUND_NDX].b = 0;

	palette[GRID_NDX].r = 255;
	palette[GRID_NDX].g = 255;
	palette[GRID_NDX].b = 255;

	palette[GOOD_SPOT_NDX].r = 255;
	palette[GOOD_SPOT_NDX].g = 255;
	palette[GOOD_SPOT_NDX].b = 0;

	palette[SAT_SPOT_NDX].r = 255;
	palette[SAT_SPOT_NDX].g = 0;
	palette[SAT_SPOT_NDX].b = 0;

	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}

/*====================================================================================================================

====================================================================================================================*/
int 
main(int argc, char *argv[])
{
int   i, j, m, n;
int   boxCenter, xPos, yPos;
Uint8 spotColor;
Uint8 *buffer;
float gain;
int crntReadBuffNdx;
size_t shmDataSize, camDataSize, centDataSize, dataControlSize;
struct timespec next, remaining;
const int	 width		= NUM_SUBAPS_PER_ROW * PIX_SIZE;
const int	 height		= NUM_SUBAPS_PER_COL * PIX_SIZE;
const int	 bpp 		= 8;  // bits per pixel -- any more than 8 makes this really slow
const Uint32 videoflags = SDL_SWSURFACE;
static int printCntr = 0;
static SDL_Surface *screen = NULL; 
static SDL_Event event;
float tmpCentX[NUM_SUBAPS_PER_COL][NUM_SUBAPS_PER_ROW];
float tmpCentY[NUM_SUBAPS_PER_COL][NUM_SUBAPS_PER_ROW];

	//	set up signal handlers
	signal(SIGTERM, termHandler);

	//	set defaults and then parse command line
	gain = 1.0;
    --argc;
    ++argv;
    while (argc && (argv[0][0] == '-'))
    {
		switch (argv[0][1])
		{
			case 'g':		// gain
			    ++argv;
			    --argc;
			    gain = atof(argv[0]);
			    break;

			case '?':
			case 'h':
			    printf("\nDisplay 3m centroids\n");
			    printf("usage:\n");
			    printf("  -g   gain    centroid gain\n");
			    printf("  -h   print   this help message\n");
			    exit(0);
			    break;

			default:
			    printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			    break;
		}
		argc--;
		argv++;
    }

	//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	centDataSize = sizeof(CentDataType);
	dataControlSize = sizeof(HrtDataFlowControlType);

	totalSizeExceptRecon = (long long)shmDataSize + camDataSize + centDataSize + dataControlSize;
	printf("spAoDispIntCam: Attempting to open shared memory:: Size = %lld / 0x%llx\n", totalSizeExceptRecon, totalSizeExceptRecon );
	printf("spAoDispIntCam: shmDataSize: %lx, camDataSize: %lx, centDataSize: %lx, dataControlSize: %lx\n",
			shmDataSize, camDataSize, centDataSize, dataControlSize);

	//	open the shared memory region
	//	IMPORTANT: reconData is no longer included in AOC_mmap -- dispInt doesn't need reconData, so the pointer to
	//	> it is not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap open: /dev/AOC_mmap\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap open: ./AOC_mmap\n");
		sleep(1);
	}
#endif

	//	get the pointer to the shared memory region
	while (!quit && ((startPtr = mmap(0, totalSizeExceptRecon, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispCentIntDraw: waiting for mmap: %llx\n", totalSizeExceptRecon);
		sleep(1);
	}
	printf( "spAoDispIntCam: Shm start Ptr = %p, totalSizeExceptRecon = 0x%llx\n", startPtr, totalSizeExceptRecon );

	//	set up the pointers to the global variables
	fileDataShmPtr		= (FileDataShmType *)startPtr;
	camDataPtr			= (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr			= (CentDataType *)((char *)camDataPtr + camDataSize);
	dataFlowControlPtr	= (HrtDataFlowControlType *)((char *)centDataPtr + centDataSize);

	//	initialize SDL and exit if we can't
	//	set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispCentIntDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispCentIntCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispCentIntDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

	float minXVal, maxXVal, minYVal, maxYVal;

	//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

		minXVal = minYVal =  100000.0;
		maxXVal = maxYVal = -100000.0;

	//	move data out of the centDispCents[][] quickly, finding mins and maxes to display (centroids are not auto-scaled)
		crntReadBuffNdx = centDataPtr->centDispReadBuffNdx;
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
			{
				tmpCentX[i][j] = centDataPtr->centDispCents[crntReadBuffNdx][i * NUM_SUBAPS_PER_ROW + j] / MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
				if (tmpCentX[i][j] < minXVal)
					minXVal = tmpCentX[i][j];
				if (tmpCentX[i][j] > maxXVal)
					maxXVal = tmpCentX[i][j];

				tmpCentY[i][j] = centDataPtr->centDispCents[crntReadBuffNdx][MAX_NUM_SUBAPS + i * NUM_SUBAPS_PER_ROW + j] / MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
				if (tmpCentY[i][j] < minYVal)
					minYVal = tmpCentY[i][j];
				if (tmpCentY[i][j] > maxYVal)
					maxYVal = tmpCentY[i][j];
				printf( " %f %f\n", tmpCentY[i][j], tmpCentY[i][j]);
			}
			printf("\n");
		}

		//	print mins and maxes
		if (++printCntr >= 5)
		{
			printCntr = 0;
			printf("CCC Frame minX: %8.4f, maxX: %8.4f minY: %8.4f, maxY: %8.4f\n", minXVal, maxXVal, minYVal, maxYVal);
		}

		//	zero the display pixels
		buffer = (Uint8 *)screen->pixels;
		memset(buffer, BACKGROUND_NDX, (size_t)(NUM_SUBAPS_PER_COL * PIX_SIZE * NUM_SUBAPS_PER_ROW * PIX_SIZE));

		//	draw little '+' signs to indicate the centroid locations and little dots in the center of each display box
		//	> (note that there are not active centroids along the edges, so we don't need to worry about going outside the
		//	> buffer -- otherwise, we would)
		for (i = HALF_PIX * NUM_SUBAPS_PER_ROW * PIX_SIZE + HALF_PIX, m = 0; 
			 i < (NUM_SUBAPS_PER_COL * PIX_SIZE * NUM_SUBAPS_PER_ROW * PIX_SIZE); 
			 i += NUM_SUBAPS_PER_ROW * PIX_SIZE * PIX_SIZE, m++)
		{
			n = 0;	// mrr redundant with below??
			for (j = 0, n = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j += PIX_SIZE, n++)
			{
				boxCenter = i + j;

				spotColor = GOOD_SPOT_NDX;
				xPos = (int)(tmpCentX[m][n] * gain * HALF_PIX);
				if (xPos < (-HALF_PIX + 1))
				{
					xPos = -HALF_PIX + 2;
					spotColor = SAT_SPOT_NDX;
				}
				else if (xPos > (HALF_PIX - 1))
				{
					xPos = HALF_PIX - 2;
					spotColor = SAT_SPOT_NDX;
				}
				yPos = (int)(tmpCentY[m][n] * gain * HALF_PIX);
				if (yPos < (-HALF_PIX + 1))
				{
					yPos = -HALF_PIX + 2;
					spotColor = SAT_SPOT_NDX;
				}
				else if (yPos > (HALF_PIX - 1))
				{
					yPos = HALF_PIX - 2;
					spotColor = SAT_SPOT_NDX;
				}

				if ((tmpCentX[m][n] != 0.0) || (tmpCentY[m][n] != 0.0))	// Will not print if the centroid is 0 
				{
					buffer[boxCenter + (xPos    ) + (yPos    ) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;  // centroid '+' center

					buffer[boxCenter + (xPos    ) + (yPos + 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;  // centroid '+' bottom
					buffer[boxCenter + (xPos + 1) + (yPos    ) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;  // centroid '+' left
					buffer[boxCenter + (xPos    ) + (yPos - 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;  // centroid '+' top
					buffer[boxCenter + (xPos - 1) + (yPos    ) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;  // centroid '+' right

					buffer[boxCenter + (xPos + 1) + (yPos + 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;
					buffer[boxCenter + (xPos + 1) + (yPos - 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;
					buffer[boxCenter + (xPos - 1) + (yPos - 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;
					buffer[boxCenter + (xPos - 1) + (yPos + 1) * NUM_SUBAPS_PER_ROW * PIX_SIZE] = spotColor;

					buffer[boxCenter] = GRID_NDX;  // center spot
				}
			}
		}

		//	draw in sub-ap grid lines
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i += PIX_SIZE)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j++)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = GRID_NDX;
		}
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i++)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j += PIX_SIZE)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = GRID_NDX;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

		//	check for quit
		while(!quit && SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				printf("dispcent quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(startPtr, totalSizeExceptRecon);
	  	close(mmfd);
	}
}

/*====================================================================================================================

====================================================================================================================*/
static void 
termHandler(int sigNum)
{
	printf("dispcent quitting due to OS term: %d\n", sigNum);
	quit = true;
}
