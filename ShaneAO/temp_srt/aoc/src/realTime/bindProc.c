/* 
 * bindProc - simple command-line tool to set CPU affinity of a given task
 *
 *	The reason that we need this for the gpiaoc is to affine Linux and the SRT to the first 4 processors as startup time,
 *	leaving the other 12 processors for the HRT.  And the reason that we need to do that is because calling 
 *	rtl_pthread_attr_setreserve_np() without it was causing the network daemon to timeout as the system was truing to move 
 *	it off a reserved processor ("NETDEV WATCHDOG: eth0 (bnx2): transmit timed out").  I think this is a bnx2 driver problem.
 *	If that gets fixed, this bindProc step will no longer be necessary.  
 *
 * To use the code:
 *	1. to compile:		gcc bindProc.c -o bindProc
 *	2. then (as root):	cp bindProc /bin
 *	3. then, add the following 2 lines to the beginning of /etc/rc.d/rc.sysinit (which is also linked from /etc/rc.sysinit)
 *	/bin/bindProc 1 0xf
 *	/bin/bindProc $$ 0xf
 */

#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <sched.h>

int main(int argc, char *argv[])
{
	unsigned long new_mask;  //XXX note, this should really be cpu_set_t, but that wasn't working
	unsigned long cur_mask;	 //XXX note, this should really be cpu_set_t, but that wasn't working
	unsigned int len = sizeof(new_mask);
	pid_t pid;

	if (argc != 3) 
	{
		fprintf(stderr, "usage: %s [pid] [cpu_mask]\n", argv[0]);
		return -1;
	}

	pid = atol(argv[1]);
	sscanf(argv[2], "%08lx", &new_mask);

	if (sched_getaffinity(pid, len, &cur_mask) < 0)
	{
		perror("1 sched_getaffinity");
		return -1;
	}
	printf("pid %d's old affinity: %08lx\n", pid, cur_mask);

	if (sched_setaffinity(pid, len, &new_mask))
	{
		perror("2 sched_setaffinity");
		printf(" pid: %d, len: %d, new_mask: %08lx\n", pid, len, new_mask);
		return -1;
	}

	if (sched_getaffinity(pid, len, &cur_mask) < 0)
	{
		perror("3 sched_getaffinity");
		return -1;
	}
	printf(" pid %d's new affinity: %08lx\n", pid, cur_mask);

	return 0;
}

