/*******************************************************************************
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 ******************************************************************************/
/*!
 *	@file gpAoSrtHostICon.c
 *
 * 	@version  $Id: gpAoSrtHostICon.c,v 1.1 2012/10/26 16:31:42 jgates Exp $
 * 	
 *	@brief 
 *	System Controller for the Hard-Real-Time (HRT) components.  Provides: Startup-init,
 *	Closedown-Cleanup, Command-Status interface to the SRT system.
 *
 *	@b DESCRIPTION:
 *  This component is the host interface controller.  To begin with, its main purpose is to perform
 *  initialization for the host interface (establish the host socket, create the hostDiag thread,
 *  etc.).  Then, it's primary responsibility is to sit on the host input socket, parse messages when
 *  received, and send the result off to the HRT (Hard Real Time) processor via an RTLinux FIFO.
 *  So, after initialization, it could be cosidered the 'host input controller'.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 12/17/2008
 *
 *	@b FUNCTION NAMES:
 *	-# @c main()
 *	-# @c gpAoSrtHostICon_RunSckt()
 *	-# @c gpAoSrtHostICon_ProcMsgs()
 *	-# @c gpAoSrtHostICon_ProcCmd()
 *	-# @c gpAoSrtHostICon_GetCommandIndex()
 *	-# @c gpAoSrtHostICon_ReadAperture()
 *	-# @c gpAoSrtHostICon_readFits2D()
 *	-# @c HostIConSendStrngNoParm()
 *	-# @c HostIConSendStrngIntParm()
 *	-# @c HostIConSendStrngFltParm()
 *
 *	@b REVISIONS:
 *	$$
 *	mrr	12/17/2008	Added "Socket Connected! message
 *
 ********************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>		 // added for 'domain' socket interface to AOC server
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

#include "gpUtGmbHrt.h"	// mrr

#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "fitsio.h"

#if ( RTL_SYSTEM == 0 )
int initHRT(void);		// these are in gpAoHrtSysCon.c
void cleanupHRT(void);  // these are in gpAoHrtSysCon.c
#endif

#define PRINTF_SEND_STRNGS_HI 0  // use this to turn off printfs for strings being sent to HostDiag (for normal operation)	// mrr
// #define PRINTF_SEND_STRNGS_HI 1  // use this to turn on printfs for strings being sent to HostDiag (for testing)

#define NUM_RANGES	12

// Global Variables
FileDataShmType*			srtFileDataShmPtr;			// pointer to file data shared memory
CamDataType*				srtCamDataPtr;				// pointer to camera shared memory data
CentDataType*				srtCentDataPtr;				// pointer to centroid shared memory data
ReconDataType*				srtReconDataPtr;			// pointer to recon shared memory data
HrtDataFlowControlType*		srtDataFlowControlPtr;		// pointer to Data Flow Control variables

int							HostSocket;					// ethernet socket for host communications
int							DiagFIFOfd;					// fd for FIFO used to receive diagnostic data to the diag thread
char						HostConnectedFlag = false;	// indicates whether the host socket is connected

int							srtSystemDebugLevel = 0;	// Starting point
int							prevClipCounts[3];
float						prevCCDTempKelvins = 0.0;
int							prevDarkCornerVal = 0;

#include "gpAoCmdTable.h"  // the command table is in a separate file so that both the HRT and SRT programs can include it

// Local Variables
static const char *configDir = CONFIG_DIR;

static char		 restartFlag, shutdownFlag, quitFlag;
static int		 CmdFIFOfd;		  // fd for FIFO used to send command messages to the HRT side
static pthread_t hostDiagThread;  // thread handle for the HostDiag Thread

static int junksubapmap[48] = 
{
13,
14,
15,
16,
22,
23,
24,
25,
26,
27,
31,
32,
33,
34,
35,
36,
37,
38,
41,
42,
43,
46,
47,
48,
51,
52,
53,
56,
57,
58,
61,
62,
63,
64,
65,
66,
67,
68,
72,
73,
74,
75,
76,
77,
83,
84,
85,
86
};

// Local Function Prototypes
static void	 gpAoSrtHostICon_RunSckt( char* restartFlag, char* shutdownFlag, char* quitFlag );
static short gpAoSrtHostICon_ProcMsgs(void);
static int	 gpAoSrtHostICon_GetCommandIndex( char* cmdStr );
	
static void	 gpAoSrtHostICon_ReadAperture( float* _currAp, short* pSubApCnt, short* pSkippedRowsTop, short* pSkippedRowsBot );
static void	 gpAoSrtHostICon_readFits2D(char *filename, int height, int width, int dataType, void *data);

static void	 HostIConSendStrngNoParm ( int msgDebugLevel, char* strngPtr );
static void	 HostIConSendStrngIntParm( int msgDebugLevel, char* strngPtr, int intParam );
static void	 HostIConSendStrngFltParm( int msgDebugLevel, char* strngPtr, float floatParam );

static int isblank( int cc )
{
	return ( (cc==' ') || (cc=='\011') || (cc=='\033') );
}

/*!
 *******************************************************************************
 *	@fn main( void )
 *
 *	@brief
 *	Cleanup and dispose of variables for HRT system control.
 *
 *	@b DESCRIPTION:
 *	This unit is the host interface mainline.  Its purpose is to open FIFOs and shared memory for 
 *	communication with the HRT processor and an ethernet socket for communication with the host.
 *	It then calls gpAoSrtHostICon_ProcMsgs() to process host messages (gpAoSrtHostICon_ProcMsgs() will not return 
 *	unless the socket is lost or a restart message is received).
 *
 *	@b Usage: main()
 * 
 *	@b Param None.
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
int 
main( void )
{
char			fileName[ MAX_PATHNAME_LENGTH ];
int				mmfd, mmfd2;							 // mmap file descriptors
//	size_t			shmDataSize, camDataSize, centDataSize, srtDataControlSize;
size_t			shmDataSize, camDataSize, centDataSize, srtDataControlSize, srtTTCamDataSize, srtTTDataSize;	// mrr TT
long long		totalSizeExceptRecon, reconDataSize;     // total size of shared memory and recon size
unsigned char	*startPtr, *startPtr2;					 // pointers to start of shared memory
int				tmpNdx;
int				exitCode = 0;
int				retVal, ii, jj;
pthread_attr_t	hostDiagAttr;
short			subApCnt = 0, skippedRowsTop = 0, skippedRowsBot = 0;
double			*tmpDoublePtr;
diagFIFOMsgType diagFIFOMsg;
FILE*			fp;

	//	can't send this message to HostDiag because the FIFO isn't set up yet
	printf("INFO (main) aperture window size is: %d x %d\n", (NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP), (NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP) );

#if ( RTL_SYSTEM == 0 )
 	if( initHRT() != 0 )
 		printf("ERR (main) error during initHRT()\n");  // can't send this message to HostDiag because the FIFO isn't set up yet
#endif

	//	spin on the FIFO for sending command messages to the HRT side until it is opened
	printf("INFO (main) waiting to connect to HRT...\n");  // can't send this message to HostDiag because the FIFO isn't set up yet
#if ( RTL_SYSTEM != 0 )
	while (( CmdFIFOfd = open("/dev/rtf1", O_NONBLOCK | O_CREAT | O_WRONLY)) < 0)
        ;
#else
	while (( CmdFIFOfd = open(RTF1_FNAME, O_NONBLOCK | O_WRONLY, S_IRUSR | S_IWUSR)) < 0)
        ;
#endif
	printf("INFO (main) HRT connected!\n");  // can't send this message to HostDiag because the FIFO isn't set up yet
	//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	centDataSize = sizeof(CentDataType);
	srtDataControlSize = sizeof(HrtDataFlowControlType);
	reconDataSize = sizeof(ReconDataType);

	//	open the shared memory region
	totalSizeExceptRecon = (long long)shmDataSize + camDataSize + centDataSize + srtDataControlSize;
	//	can't send these two messages to HostDiag because the FIFO isn't set up yet
	printf("INFO (main) attempting to open shared memory: Size = %lld / 0x%llx\n", totalSizeExceptRecon, totalSizeExceptRecon );
	printf("INFO (main) attempting to open shared memory 2: Size = %lld / 0x%llx\n", reconDataSize, reconDataSize );
#if ( RTL_SYSTEM != 0 )
	if ((mmfd = open("/dev/AOC_mmap", O_RDWR)) < 0)
	{
		//	can't send this message to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) opening shared mem on SRT side, errno = %d, Description: %s\n", errno, strerror( errno) );
	}
	else if ((mmfd2 = open("/dev/AOC_mmap2", O_RDWR)) < 0)
	{
	//	can't send this message to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) opening shared mem 2 on SRT side, errno = %d, Description: %s\n", errno, strerror( errno) );
	}
#else
	if ((mmfd = shm_open("./AOC_mmap", O_RDWR, S_IRUSR | S_IWUSR)) < 0)
	{
		//		can't send this message to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) opening shared mem on SRT side, errno = %d, Description: %s\n", errno, strerror( errno) );
	}
	else if ((mmfd2 = shm_open("./AOC_mmap2", O_RDWR, S_IRUSR | S_IWUSR)) < 0)
	{
		//		can't send this message to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) opening shared mem 2 on SRT side, errno = %d, Description: %s\n", errno, strerror( errno) );
	}
#endif

	//	get the pointer to the shared memory region
	if ((startPtr = mmap(0, totalSizeExceptRecon, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED)
	{
		//	can't send these two messages to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) getting pointer to shared mem on SRT side, errno = %d, Error message: %s\n", errno, strerror(errno));
		printf("ERR (main) shmDataSize = %ld  camDataSize = %ld   centDataSize = %ld  totalSizeExceptRecon = %lld\n", 
			   shmDataSize, camDataSize, centDataSize, totalSizeExceptRecon);
	}
	else if ((startPtr2 = mmap(0, reconDataSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd2, 0)) == MAP_FAILED)
	{
		//	can't send these two messages to HostDiag because the FIFO isn't set up yet
		printf("ERR (main) getting pointer to shared mem 2 on SRT side, errno = %d, Error message: %s\n", errno, strerror(errno));
		printf("ERR (main) reconDataSize = %lld\n", reconDataSize);
	}
	else
		printf("INFO (main) shared memory allocation for SRT is okay\n");  //	can't send this message to HostDiag because the FIFO isn't set up yet

	//	set up the pointers to the global variables
	srtFileDataShmPtr = (FileDataShmType *)startPtr;
	srtCamDataPtr = (CamDataType *)((char *)srtFileDataShmPtr + shmDataSize);
	srtCentDataPtr = (CentDataType *)((char *)srtCamDataPtr + camDataSize);
	srtDataFlowControlPtr = (HrtDataFlowControlType *)((char *)srtCentDataPtr + centDataSize);
	srtReconDataPtr = (ReconDataType *)startPtr2;

	//	can't send these five messages to HostDiag because the FIFO isn't set up yet
	printf("INFO (main) Start Ptr = %p, totalSizeExceptRecon = 0x%llx\n", startPtr, totalSizeExceptRecon );
	printf("INFO (main) Size of FileData = 0x%lx, ptr = %p\n", sizeof(FileDataShmType), srtFileDataShmPtr);
	printf("INFO (main) Size of CamData = 0x%lx, ptr = %p\n", sizeof(CamDataType), srtCamDataPtr);
	printf("INFO (main) Size of CentData = 0x%lx, ptr = %p\n", sizeof(CentDataType), srtCentDataPtr);
	printf("INFO (main) Size of ReconData = 0x%lx, ptr = %p\n", sizeof(ReconDataType), srtReconDataPtr);

	//	now that shared memory is created, read in several files that won't change during normal operation
	//	read in the aperture bitmap and make a sub-ap map from it
	gpAoSrtHostICon_ReadAperture( &(srtFileDataShmPtr->aperturePattern[0][0]), &subApCnt, &skippedRowsTop, &skippedRowsBot );
	tmpNdx = 0;

	printf("\nsubApMap\n");	// mrr DM
	for( ii=0; ii<(NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP) ; ii++)	 // number of rows
	{
		for( jj=0; jj<(NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP); jj++ )	 // number of columns
		{
			int offset = ii * NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP + jj;
			if ( srtFileDataShmPtr->aperturePattern[ii][jj] != 0)
			{
				srtFileDataShmPtr->subApMap[tmpNdx] = offset;  // this is the sub-ap map for moving between packed and full arrays
				printf("%d %d\n", tmpNdx, srtFileDataShmPtr->subApMap[tmpNdx] );
				tmpNdx++;
			}
		}
	}
	subApCnt = 48;
	tmpNdx = subApCnt;
	printf("\nNew subApMap\n");	// mrr DM
	for (ii=0; ii<subApCnt;ii++)
	{
		srtFileDataShmPtr->subApMap[ii] = junksubapmap[ii];  // this is the sub-ap map for moving between packed and full arrays
		printf("%d %d\n", ii, srtFileDataShmPtr->subApMap[ii] );
	}
	if( tmpNdx != subApCnt )
	{
		//	can't send this message to HostDiag because the FIFO isn't set up yet
    	printf("ERR (main) aperture mapping is incorrect, got: %d, expected: %d\n", tmpNdx, subApCnt );
	}
	srtFileDataShmPtr->numCentroids = subApCnt * 2;				// numCentroids is the x, y combinations - hence "* 2"
	srtFileDataShmPtr->numSubaps = subApCnt;					// numSubAp moved to shared memeory
	
	//	read in ranges for environmental parameters for the tip/tilt LQG filter
	for (ii = 0; ii <= PARM_DIM; ii++)  // set for test purposes (in case there isn's a file)
	{
		srtFileDataShmPtr->ttLQGAllRanges[eTtEnvSeeing][ii]			= (double)ii;  // ranges for seeing
		srtFileDataShmPtr->ttLQGAllRanges[eTtEnvWindPower][ii]		= (double)ii;  // ranges for wind power
		srtFileDataShmPtr->ttLQGAllRanges[eTtEnvStarBrightness][ii] = (double)ii;  // ranges for star brightness
	}
	sprintf( fileName, "%sconfig/envLQGParmRanges.fits", configDir );
	gpAoSrtHostICon_readFits2D( fileName, 3, PARM_DIM + 1, TDOUBLE, &(srtFileDataShmPtr->ttLQGAllRanges[0][0]) );

	//	read in filter parameters and Kalman gains for the tip/tilt LQG filter
	tmpDoublePtr = &(srtFileDataShmPtr->ttLQGAllParams[0][0][0][0]);
	for( ii = 0; ii < NS * NW * NR * N_ALLPARAMS; ii++)  // set to all zeros in case there isn't a file
		tmpDoublePtr[ii] = 0.0;
	sprintf( fileName, "%sconfig/mergedLQGParams.fits", configDir );
	gpAoSrtHostICon_readFits2D( fileName, NS * NW * NR, N_ALLPARAMS, TDOUBLE, &(srtFileDataShmPtr->ttLQGAllParams[0][0][0][0]) );

	//	read in Zernike mode vectors for offloading to M1 and M2
	double tmpZernikeVectors[MAX_WOOFER_ACTS][eM1MaxVals + 1];  // the "+ 1" is for the focus vector
	for( ii = 0; ii < (eM1MaxVals + 1); ii++ )  // set to all zeros in case there isn't a file
	{
		for( jj = 0; jj < MAX_WOOFER_ACTS; jj++ )
			tmpZernikeVectors[jj][ii] = 0.0;
	}
	sprintf( fileName, "%sconfig/offloadZernikes.fits", configDir );
	gpAoSrtHostICon_readFits2D( fileName, eM1MaxVals + 1, MAX_WOOFER_ACTS, TDOUBLE, &tmpZernikeVectors[0][0] );
	for( jj = 0; jj < MAX_WOOFER_ACTS; jj++ )
		srtFileDataShmPtr->focusZernikeVector[jj] = tmpZernikeVectors[jj][0];  // focus is the first vector in the file
	for( ii = 0; ii < eM1MaxVals; ii++ )
	{
		for( jj = 0; jj < MAX_WOOFER_ACTS; jj++ )
			srtFileDataShmPtr->m1ZernikeVectors[ii][jj] = tmpZernikeVectors[jj][ii + 1];  // indeces are swapped due to IDL
	}

	//	init tlc error array and read in ranges for checking operational values (wfs light levels, etc.)
	//for (ii = 0; ii < eTlcErrNum; ii++)	// mrr
	//{
	//	prevTlcErrStatArray[ii] = eTlcErrNone;
	//	srtFileDataShmPtr->tlcErrStatArraySticky[ii] = eTlcErrNone;
	//	srtFileDataShmPtr->tlcErrStatArray[ii] = eTlcErrNone;
	//}
	srtFileDataShmPtr->tlcErrCCDTempKelvins = 0.0;
	prevCCDTempKelvins = 0.0;
	srtFileDataShmPtr->tlcErrDarkCornerVal = 0;
	prevDarkCornerVal = 0;
	srtFileDataShmPtr->tlcErrAoLight = 0;
	srtFileDataShmPtr->tlcErrPupilLight = 0;
	srtFileDataShmPtr->tlcErrCalSnr = 0.0;
	srtFileDataShmPtr->tlcErrClipCounts[0] = 0;
	srtFileDataShmPtr->tlcErrClipCounts[1] = 0;
	srtFileDataShmPtr->tlcErrClipCounts[2] = 0;
	prevClipCounts[0] = 0;
	prevClipCounts[1] = 0;
	prevClipCounts[2] = 0;
	srtFileDataShmPtr->rangesWfsCcdTemp[0] = 0.0;
	srtFileDataShmPtr->rangesWfsCcdTemp[1] = 0.0;
	srtFileDataShmPtr->rangesWfsCcdTemp[2] = 0.0;
	srtFileDataShmPtr->rangesWfsCcdTemp[3] = 0.0;
	srtFileDataShmPtr->rangesCornerCrct[0] = 0;
	srtFileDataShmPtr->rangesCornerCrct[1] = 0;
	srtFileDataShmPtr->rangesAoSettled 	   = 0.0;
	srtFileDataShmPtr->rangesAoLight 	   = 0;
	srtFileDataShmPtr->rangesPupLight[0]   = 0;
	srtFileDataShmPtr->rangesPupLight[1]   = 0;
	srtFileDataShmPtr->rangesCalSnr		   = 0.0;
	srtFileDataShmPtr->rangesClipPersist   = 0;

	retVal = EOF;
	if ( ( fp = fopen( RANGES_FNAME, "r" ) ) == NULL )
		HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (SRT main()) opening ranges file for reading");
	else
	{
		retVal = fscanf 
		( fp, "\
 WFS_CCD_K_LOW_FAIL:     %g\
 WFS_CCD_K_LOW_WARN:	 %g\
 WFS_CCD_K_HIGH_WARN:	 %g\
 WFS_CCD_K_HIGH_FAIL:	 %g\
 CORNER_CRCT_DN_LOW:     %d\
 CORNER_CRCT_DN_HIGH:    %d\
 AO_SETTLED_SECS:		 %g\
 AO_TOO_HIGH_LIGHT_DN:   %d\
 PUP_TOO_LOW_LIGHT_DN:   %d\
 PUP_TOO_HIGH_LIGHT_DN:  %d\
 CAL_SNR_TOO_LOW:        %g\
 CLIP_PERSIST_COUNT:     %d\
 ",
			&srtFileDataShmPtr->rangesWfsCcdTemp[0],
			&srtFileDataShmPtr->rangesWfsCcdTemp[1],
			&srtFileDataShmPtr->rangesWfsCcdTemp[2],
			&srtFileDataShmPtr->rangesWfsCcdTemp[3],
			&srtFileDataShmPtr->rangesCornerCrct[0],
			&srtFileDataShmPtr->rangesCornerCrct[1],
			&srtFileDataShmPtr->rangesAoSettled,
			&srtFileDataShmPtr->rangesAoLight,
			&srtFileDataShmPtr->rangesPupLight[0],
			&srtFileDataShmPtr->rangesPupLight[1],
			&srtFileDataShmPtr->rangesCalSnr,
			&srtFileDataShmPtr->rangesClipPersist 
			//	if a range value gets added or removed, update NUM_RANGES !!
		);
		if ( retVal != NUM_RANGES )
		{
			HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (SRT main()) bad ranges count, got:      ", retVal);
			HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (SRT main()) bad ranges count, expected: ", NUM_RANGES);
		}
		if( retVal == EOF )
			HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (SRT main()) reading from ranges file" );
		if( fclose( fp ) == EOF )
			HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (SRT main()) error closing ranges file after reading" );
	}

	//	read in the simulated wfs data, if available, for use in sim mode
	sprintf( fileName, "%sconfig/simulation/simImage.fits", configDir );
	printf("(main) Reading fits simulation file from:  %s\n", fileName );
	gpAoSrtCamMngr_ReadSimWfsData( fileName );

	//	initialize command handling status area before calling camera stuff (so ending status is not returned to TLC)
	for( ii=0; ii<eCmdCount; ii++)
	{
		srtFileDataShmPtr->cmdStatusList[ ii ].modified = 0;
		srtFileDataShmPtr->cmdStatusList[ ii ].state = 0;
		srtFileDataShmPtr->cmdStatusList[ ii ].subCmdFlag = 1;		// preset to sub command format so the internal commands are not set out to TLC
		srtFileDataShmPtr->cmdStatusList[ ii ].stateErrCode = 0;	// No Error
		strcpy( srtFileDataShmPtr->cmdStatusList[ ii ].stateErrMsg, "");
	}
	
	//	initialize the SciMeasure camera
	gpAoSrtCamMngr_StartCam();

	//	open the FIFO for sending diagnostic data update messages to the diag thread
#if ( RTL_SYSTEM != 0 )
	if ((DiagFIFOfd = open("/dev/rtf0", O_CREAT | O_RDWR)) < 0)
#else
	if ((DiagFIFOfd = open(RTF0_FNAME, O_RDWR, S_IRUSR | S_IWUSR)) < 0)
#endif
	{
	//		can't send this message to HostDiag because there's an error setting up the FIFO
		printf("ERR (main) creating diag FIFO (rtf0), errno = %d, Error Description: %s\n", errno, strerror( errno ) );
	}

	//	create hostDiag thread
	pthread_attr_init(&hostDiagAttr);
	if( (retVal = pthread_create(&hostDiagThread, &hostDiagAttr, gpAoSrtHostDiag, 0)) != 0)
	{
	//		can't send this message to HostDiag because there's an error creating hostDiag
		printf("ERR (main) creating host diag thread, retVal = %d\n", retVal);
	}

	//	set the srtIsUpFlag in shared memory to let the HRT know that the SRT is up (the HRT will be spinning on this flag)
	srtFileDataShmPtr->srtIsUpFlag = true;

	// Setup interface to GMB 

	// ignore SIGPIPE signals (Note - this does not seem to work in GDB - you still seem to get these signals) 
	signal( SIGPIPE, SIG_IGN); 

	//	enter an almost-infinite loop (exit if a system error occurs or drop out if a restart or shutdown command is received)
	restartFlag = false;
	shutdownFlag = false;
	quitFlag = false;
	while(  !shutdownFlag && !quitFlag )
		gpAoSrtHostICon_RunSckt( &restartFlag, &shutdownFlag, &quitFlag );

	//	stop the wfs camera
	gpAoSrtCamMngr_StopCam();

	// If we have gotten here the  "gpAoSrtHostICon_RunSckt" has returned, so system is done,
	// > so shut everything down
	gpAoSrtFileMngr_abortMapToSlopesThread();
	usleep(20000);  // 20 msecs
	gpAoSrtFileMngr_abortTimerThread();
	usleep(20000);  // 20 msecs
	gpAoSrtHostDiag_KillDumpDataThread();
	usleep(10000);  // 10 msecs

    diagFIFOMsg.diagID = eDiagQuit;  //	set up and send a diag FIFO message to tell hostDiag to quit
	diagFIFOMsg.cmdNdx = 0;
	if (write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
	{
		//	can't send this message to HostDiag because there's an error with the FIFO
		printf("ERR (main) writing to diag FIFO, SRT side, errno = %d\n", errno);
	}
	usleep(20000);  // 20 msecs

#if ( RTL_SYSTEM == 0 )
	cleanupHRT();
#else
	srtFileDataShmPtr->hrtQuitFlag = true;  // tell the HRT side to shut down (if we're in RTL mode)
#endif

	munmap(startPtr, totalSizeExceptRecon);
  	close(mmfd);
	munmap(startPtr2, reconDataSize);
  	close(mmfd2);

	close( CmdFIFOfd );
	close( DiagFIFOfd );

	//	return to quit the control program, restart (reboot) the system, or shutdown the system
	if (quitFlag)
        exitCode = 250;
	else if (restartFlag)
        exitCode = 251;
	else if (shutdownFlag)
        exitCode = 252;
    else
        exitCode = -1;

	printf("INFO (main) exiting main program, exit code = %d\n", exitCode );  // can't send this message to HostDiag because the FIFO is closed
	return exitCode;
}

/*!
 *******************************************************************************
 *	@fn gpAoSrtHostICon_RunSckt( char* pRestartFlag, char* pShutdownFlag, char* pQuitFlag)
 *
 *	@brief
 *	Socket for host interface
 *
 *	@b DESCRIPTION:
 *	This unit provides the socket based interface to the AOC RPC Server module.
 *
 *	@par
 *	IMPORTANT: This routine does NOT return until it gets a RESET, RESTART, Socket error, etc.
 *	 
 *	@par
 *
 *	@b Usage: gpAoSrtHostICon_RunSckt( char* pRestartFlag, char* pShutdownFlag, char* pQuitFlag )
 * 
 *	@b Param None.
 *
 *	@return 
 *		set in the input flags (true/false).
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
static void 
gpAoSrtHostICon_RunSckt( char* pRestartFlag, char* pShutdownFlag, char* pQuitFlag )
{	
int 						reuse;
int							serverSocket, port, tryCntr;
int							retVal;
static struct sockaddr_in	address;

	//  create and bind the server socket to establish communication with the host
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		printf("ERR (gpAoSrtHostICon_RunSckt) creating server socket, errno = %d (%s)\n", errno, strerror(errno));  // can't send this to HostDiag

	signal( SIGPIPE, SIG_IGN );

	reuse = 1;
	if (setsockopt(serverSocket, SOL_SOCKET, SO_REUSEADDR, (const void *)&reuse, sizeof(int)) < 0)
		printf("ERR (gpAoSrtHostICon_RunSckt) setting socket re-use option, errno = %d (%s)\n", errno, strerror(errno));  // can't send this to HostDiag
	else
	{
		tryCntr = 0;
		port = DEFAULT_SOCKET_PORT;
	    address.sin_family = AF_INET;
	    address.sin_port = htons(port);
	    address.sin_addr.s_addr = INADDR_ANY;
	    while ((bind(serverSocket, (struct sockaddr *)&address, sizeof(address)) < 0) && (tryCntr < 10))
	    {
			printf("ERR (gpAoSrtHostICon_RunSckt) binding server socket, errno = %d (%s)\n", errno, strerror(errno));  // can't send this to HostDiag
			sleep(1);
	        tryCntr++;
			port = DEFAULT_SOCKET_PORT;
		    address.sin_family = AF_INET;
		    address.sin_port = htons(port);
		    address.sin_addr.s_addr = INADDR_ANY;
	    }
	    if (tryCntr < 10)
			printf("INFO (gpAoSrtHostICon_RunSckt) server socket bound as port %d\n", port);  // can't send this to HostDiag
	}

	while( !(*pRestartFlag) && !(*pShutdownFlag) && !(*pQuitFlag) )
	{
		//	listen on the server socket and accept when a connection is initiated
		printf("INFO (gpAoSrtHostICon_RunSckt) waiting to connect to host...\n");  // can't send this to HostDiag
		if (listen( serverSocket, 0) < 0)
			printf("ERR (gpAoSrtHostICon_RunSckt) listening on server socket, errno = %d (%s)\n", errno, strerror(errno));  // can't send this to HostDiag
		if ((HostSocket = accept( serverSocket, NULL, NULL)) < 0)
			printf("ERR (gpAoSrtHostICon_RunSckt) accepting host socket, errno = %d (%s)\n", errno, strerror(errno));  // can't send this to HostDiag

        HostConnectedFlag = true;
		sleep (1);

#if (SKIP_RECON == 0)
		printf("INFO (main) \n===================\n     Socket Connected! \n===================\n");  // mrr
		HostIConSendStrngNoParm(eDebugLevelNone, "===================  Socket Connected! ===================");
#else
		printf("INFO (main) \n\n===================\n     Socket Connected! \n===================\n === Skipping Recon ===\n\n");  // mrr
		HostIConSendStrngNoParm(eDebugLevelNone, "\t\t===================  Socket Connected! ===================");
		HostIConSendStrngNoParm(eDebugLevelNone, "\t\t===================    Skipping Recon  ===================");
		HostIConSendStrngNoParm(eDebugLevelNone, "\t\t==========================================================");		
#endif
		sleep (5);

		//	call gpAoSrtHostICon_ProcMsgs() to process host messages; gpAoSrtHostICon_ProcMsgs() will not return unless
		//	> the socket is lost or a quit, restart, or shutdown message is received
		retVal = gpAoSrtHostICon_ProcMsgs();
		switch( retVal )
		{
			case eErrQuit:
	            HostIConSendStrngNoParm(eDebugLevelNone, "\t\tINFO quitting control program...");
				// Attempt to kill the socket client nicely
				HostIConSendStrngNoParm(eDebugLevelNone, "\t\tBYE");
				HostIConSendStrngNoParm(eDebugLevelNone, " ");
				HostIConSendStrngNoParm(eDebugLevelNone, "\t\tDon't forget to run '~/gpi_root/aoc/src/realTime/aocstop'");
				sleep(5);
				*pQuitFlag = true;
				break;

			case eErrRestart:
	            HostIConSendStrngNoParm(eDebugLevelNone, "\t\tINFO restarting system...");
				// Attempt to kill the socket client nicely
				HostIConSendStrngNoParm(eDebugLevelNone, "\t\tBYE");
				sleep(1);
				*pRestartFlag = true;
				break;

			case eErrShutdown:
	            HostIConSendStrngNoParm(eDebugLevelNone, "\t\tINFO shutting down system...");
				// Attempt to kill the socket client nicely
				HostIConSendStrngNoParm(eDebugLevelNone, "\t\tBYE");
				sleep(1);
				*pShutdownFlag = true;
				break;

			case eErrLostSocket:
				printf( "WARN (gpAoSrtHostICon_RunSckt) lost socket\n");  // can't send this to HostDiag
				continue;

			default:
				HostIConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoSrtHostICon_RunSckt) RetValue from socket closing is: ", retVal );
				continue;
		}
        HostConnectedFlag = false;

		// shut the socket down and close it
		shutdown(HostSocket, SHUT_RDWR);
	    close(HostSocket);
	}

    shutdown( serverSocket, SHUT_RDWR);
    close( serverSocket );
	return;
}

/*!
 *******************************************************************************
 *	@fn gpAoSrtHostICon_ProcMsgs( void )
 *
 *	@brief
 *	collect 'command packet' from host - insure it is complete.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to process messages from the host/TLC, by receiving them, parsing them,
 *	and calling gpAoSrtHostICon_ProcCmd() to process commands.
 *
 *	@par
 *	IMPORTANT: This routine does NOT return until it gets a RESET, RESTART, Socket error, etc.
 *	 
 *	@par
 *
 *	@b Usage: gpAoSrtHostICon_ProcMsgs( )
 * 
 *	@b Param None.
 *
 *	@return 
 *	1.	eErrLostSocket	- lost the socket used for host communications
 *	2.	eErrRestart		- a restart command has been received from the host
 *	3.	eErrShutdown	- a shutdown command has been received from the host
 *	4.	eErrQuit	    - a quit command has been received from the host
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
short 
gpAoSrtHostICon_ProcMsgs( void )
{
const int		inBuffLngth = 128;
static short	msgNdx = 0, retVal;
char			inBuf[inBuffLngth], msgBuf[MSG_BUFF_LEN];
short			numBytes, ii;

	//	enter a nearly infinite loop (return if the socket is lost or a restart command is received)
	while( HostConnectedFlag )
	{
		//	receive a message from the host via the host socket
		numBytes = recv(HostSocket, inBuf, inBuffLngth, 0);

		//	if an error occurred in the receive, report it
		if (numBytes < 0)
		{
			HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcMsgs) socket receive error, errno = ", errno );
			return eErrLostSocket;
		}
		//	else if zero bytes have been received (other side has shut down), return indicating that socket has been lost
		else if (numBytes == 0)
			return eErrLostSocket;
		else
		{
			//	parse the message (with a stream socket, there's no guarantee that messages will come in tidy packets),
			//	> calling gpAoSrtHostICon_ProcCmd() as necessary; return if a restart command has been received
			ii = 0;
			HostIConSendStrngIntParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcMsgs) num bytes in message = ", numBytes);
			while( ii < numBytes )
			{
				while ((ii < numBytes) && (inBuf[ii] != '\0') && (inBuf[ii] != '\n') && (msgNdx < MSG_BUFF_LEN))
				{
					msgBuf[msgNdx] = inBuf[ii];
					msgNdx++;
					ii++;
				}
				if (msgNdx >= MSG_BUFF_LEN)
				{
					msgNdx = 0;
					ii = numBytes;  // so outer loop will fall through
					HostIConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcMsgs) message buffer overflow");
				}
				else if (ii < numBytes)
				{
					msgBuf[msgNdx] = '\0';
					HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcMsgs) message okay");
					retVal = gpAoSrtHostICon_ProcCmd( msgBuf, 0 );	// indicate that this is NOT a subCmnd!
					if ((retVal == eErrQuit) || (retVal == eErrRestart) || (retVal == eErrShutdown))
						return retVal;
					msgNdx = 0;
					ii++;
				}
			}
		}
	}
	return 0;
}

/*!
 *******************************************************************************
 *	@fn  gpAoSrtHostICon_ProcCmd( char msgBuf[], int subCmdFlag )
 *
 *	@brief
 *	Parse commands and send them off for processing
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to process commands received from the host.  Most valid messages on
 *	are passed to the System Controller on the HRT processor.  Some messages (restart and diagnostic
 *	messages) are handled on this processor (the Soft Real Time (SRT) processor).
 *
 *	Currently you can NOT run both - that may be rectified in the future.
 *	 
 *	@par
 *
 *	@b Usage:  gpAoSrtHostICon_ProcCmd( char msgBuf[], flag )
 * 
 *	@b Param [in] msgBuf		(char[]):	NULL terminated buffer containing ASCII message from the host
 *	@b Param [in] subCmdFlag	(int):		subcommand (!=0) or not (-==0) - indcates that Gmb should be updated
 *
 *	@return 
 *	1.	eErrLostSocket	- lost the socket used for host communications
 *	2.	eErrRestart		- a restart command has been received from the host
 *	3.	eErrShutdown	- a shutdown command has been received from the host
 *	4.	eErrQuit	    - a quit command has been received from the host
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
short 
gpAoSrtHostICon_ProcCmd( char msgBuf[], int subCmdFlag )
{
boolean errFlag = false;

int	  ii;
int	  numParams;
int	  cmdNdx, bufNdxStart[MAX_NUM_CMD_PARAMS + 1], bufNdxEnd[MAX_NUM_CMD_PARAMS + 1];  // the "+ 1" is to include the command itself
char  msgSegment[MAX_NUM_CMD_PARAMS + 1][MSG_BUFF_LEN];  // the "+ 1" is to include the command itself

int	  intParam[MAX_NUM_CMD_PARAMS];
float floatParam[MAX_NUM_CMD_PARAMS];
char  strngParam[MAX_NUM_CMD_PARAMS][MAX_FILENAME_LENGTH];

char  ackNakString[TEXT_BUF_LNGTH];

cmdFIFOMsgType	cmdFIFOMsg;
diagFIFOMsgType	diagFIFOMsg;

char *testBufPtr;

	//	parse input line into its string segments and null-terminate them
	for( ii = 0; ii < (MAX_NUM_CMD_PARAMS + 1); ii++ )
		msgSegment[ii][0] = '\0';  // make sure all the message segments start NULL terminated
	for( numParams = 0; numParams < (MAX_NUM_CMD_PARAMS + 1); numParams++ )
	{
		if (numParams == 0)
			bufNdxStart[0] = 0;
		else
			bufNdxStart[numParams] = bufNdxEnd[numParams - 1];

		//	skip white space
		while ((msgBuf[bufNdxStart[numParams]] != '\0') && isblank(msgBuf[bufNdxStart[numParams]]))
			(bufNdxStart[numParams])++;

		//	get the command or the next parameter 
		bufNdxEnd[numParams] = bufNdxStart[numParams];
		while ((msgBuf[bufNdxEnd[numParams]] != '\0') && !isblank(msgBuf[bufNdxEnd[numParams]]))
			(bufNdxEnd[numParams])++;
		if (bufNdxEnd[numParams] != bufNdxStart[numParams])
		{
			strncpy (msgSegment[numParams], &msgBuf[bufNdxStart[numParams]], bufNdxEnd[numParams] - bufNdxStart[numParams]);
			msgSegment[numParams][bufNdxEnd[numParams] - bufNdxStart[numParams]] = '\0';
		}

		if( msgBuf[bufNdxEnd[numParams]] == '\0')
			break;
	}

	//	look for the command in the command table and report an error if it's not there
	cmdNdx = gpAoSrtHostICon_GetCommandIndex( msgSegment[0] );
	if ( cmdNdx == -1 )
	{
		HostIConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) command not found in cmd list");
		errFlag = true;
	}

	//	save command parameters, if there are any, reporting an error if there is a problem
	if (!errFlag)
	{
		HostIConSendStrngIntParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) valid cmd, index = ", cmdNdx);

		srtFileDataShmPtr->cmdStatusList[cmdNdx].subCmdFlag = subCmdFlag; 

		for (ii = 0; ii < MAX_NUM_CMD_PARAMS; ii++)
		{
			intParam[ii] = 0;
			floatParam[ii] = 0.0;
			strngParam[ii][0] = '\0';

			if ((CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmNone) || errFlag)
				break;  // we are done looking

			switch (CmdTable[cmdNdx].paramIndicator[ii])
			{
				case eCmdParmInt:
					intParam[ii] = strtol(msgSegment[ii+1], &testBufPtr, 0);
					if ( (intParam[ii] == 0) && ((void*)testBufPtr == (void*)(&msgSegment[ii+1])) )
				   	{
				   		HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) converting int cmd param #", ii);
						errFlag = true;
				   	}
					else
						HostIConSendStrngIntParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) int param: ", intParam[ii]);
				   	break;

				case eCmdParmFlt:
				    floatParam[ii] = strtod(msgSegment[ii+1], &testBufPtr);
				    if ( (floatParam[ii] == 0.0) && ((void*)testBufPtr == (void*)(&msgSegment[ii+1])) )
				   	{
				   		HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) converting float cmd param #", ii);
						errFlag = true;
				   	}
					else
						HostIConSendStrngFltParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) float param: ", floatParam[ii]);
				   	break;

				case eCmdParmStr:
				   	if ( strlen( msgSegment[ii]) >= MAX_FILENAME_LENGTH )
				   	{
				   		HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) converting string cmd param #", ii);
						errFlag = true;
				   	}
					else
					   	strcpy( strngParam[ii], msgSegment[ii + 1] );
				   	break;

				default:
				   	HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) invalid param type:", CmdTable[cmdNdx].paramIndicator[ii]);
				   	break;
			}  // end of Switch
		}
	}

	//	send a NAK or ACK to the TLC
	if (errFlag)
	{
//		sprintf(ackNakString, "NAK:\t '%s'", msgSegment[0]);  // send a NAK to the TLC
		sprintf(ackNakString, "NAK:\t '%s'", msgBuf);  // send a NAK to the TLC
		HostIConSendStrngNoParm(eDebugLevelNone, ackNakString);
		return eErrNoErr;
	}
	else
	{
//		sprintf(ackNakString, "ACK:\t '%s'", msgSegment[0]);  // send an ACK to the TLC
		sprintf(ackNakString, "ACK:\t '%s'", msgBuf);  // send an ACK to the TLC
		HostIConSendStrngNoParm(eDebugLevelNone, ackNakString);
	}

	//	if we've gotten this far, we have a validly formatted command, so send it off
	switch (CmdTable[cmdNdx].action)
	{
		case eCmdActQuit:		  // quit AO system (return to mainline to quit)
			return eErrQuit;

		case eCmdActRestart:	  // restart AO system (return to mainline to perform reset -r)
			return eErrRestart;

		case eCmdActShtdwn:		  // shutdown AO system (return to mainline to perform reset -h)
			return eErrShutdown;

		case eCmdActEngCfg:
			{
				char	tmpCmd[128];
				int		ii;

			}
			gpAoSrtFileMngr_WriteParms( );
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Eng_Config' ");
			break;

		case eCmdActObsCfg:
			{
				char tmpCmd[128];

			}
			gpAoSrtFileMngr_WriteParms( );
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Obs_Config' ");
			break;

		case eCmdActGblInit:					// Handle GLOBAL_INIT 
			if (intParam[0] != 1)  // globalInit now has a single int param indicating whether only error flags should be reset or not
			{
				EXEC_SUB_CMD( "stopCycling", eCmdCycleEnd, 2 );
				EXEC_SUB_CMD( "offloadOnOff 0 0", eCmdOffload, 1);
				EXEC_SUB_CMD( "calDataOnOff 0", eCmdCalOnOff, 1 );
				EXEC_SUB_CMD( "closeOpenLoops 0 0 0", eCmdClsOpn, 1 );
				EXEC_SUB_CMD( "readConfig", eCmdSysConParms, 5);
				EXEC_SUB_CMD( "selectDarksBacks 0x1", eCmdSetDrkBck, 1);
				EXEC_SUB_CMD( "setTT 1 0.0 0.0", eCmdTT_Mov, 1);
				EXEC_SUB_CMD( "setShapes 0x44", eCmdSetShape, 1);
			}
			for (ii = 0; ii < eTlcErrNum; ii++)
			{
				srtFileDataShmPtr->tlcErrStatArraySticky[ii] = eTlcErrNone;
				srtFileDataShmPtr->tlcErrStatArray[ii] = eTlcErrNone;
			}
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Global_Init' ");
			break;

		case eCmdActGblNdx:					// Handle GLOBAL_INDEX 
			EXEC_SUB_CMD( "stopCycling", eCmdCycleEnd, 2 );
			EXEC_SUB_CMD( "offloadOnOff 0 0", eCmdOffload, 1);
			EXEC_SUB_CMD( "calDataOnOff 0", eCmdCalOnOff, 1 );
			EXEC_SUB_CMD( "closeOpenLoops 0 0 0", eCmdClsOpn, 1 );
			EXEC_SUB_CMD( "setTT 1 0.0 0.0", eCmdTT_Mov, 1);
			EXEC_SUB_CMD( "setShapes 0x44", eCmdSetShape, 1);
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Global_Index' ");
			break;

		case eCmdActGblPark:					// Handle GLOBAL_PARK 
			EXEC_SUB_CMD( "stopCycling", eCmdCycleEnd, 2 );
			EXEC_SUB_CMD( "offloadOnOff 0 0", eCmdOffload, 1);
			EXEC_SUB_CMD( "calDataOnOff 0", eCmdCalOnOff, 1 );
			EXEC_SUB_CMD( "closeOpenLoops 0 0 0", eCmdClsOpn, 1 );
			EXEC_SUB_CMD( "setTT 1 0.0 0.0", eCmdTT_Mov, 1);
			EXEC_SUB_CMD( "setShapes 0x44", eCmdSetShape, 1);
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Global_Park' ");
			break;

		case eCmdActGblRbt:						// Handle GLOBAL_REBOOT 
			EXEC_SUB_CMD( "stopCycling", eCmdCycleEnd, 2 );
			EXEC_SUB_CMD( "offloadOnOff 0 0", eCmdOffload, 1);
			EXEC_SUB_CMD( "calDataOnOff 0", eCmdCalOnOff, 1 );
			EXEC_SUB_CMD( "closeOpenLoops 0 0 0", eCmdClsOpn, 1 );
			EXEC_SUB_CMD( "setTT 1 0.0 0.0", eCmdTT_Mov, 1);
			EXEC_SUB_CMD( "setShapes 0x44", eCmdSetShape, 1);
			HostIConSendStrngNoParm(eDebugLevelMin, "INFO (gpAoSrtHostICon_ProcCmd) finished 'Global_Reboot' ");
			break;
			
		case eCmdActToDiag:					// send to diagnostic thread
			diagFIFOMsg.diagID = eDiagCmdHost;
			diagFIFOMsg.cmdNdx = cmdNdx;
			for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
			{
				if (CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmInt)
					diagFIFOMsg.intParam[ii] = intParam[ii];
				else if (CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmFlt)
					diagFIFOMsg.floatParam[ii] = floatParam[ii];
			}
			if (write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
				HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) writing to diag FIFO, SRT side, errno = ", errno);
			break;

		case eCmdActToHRT:					// send to HRT processor
		case eCmdActToCam:					// send to HRT camera thread
        case eCmdActToCent:					// send to HRT centroid thread
        case eCmdActToRecon:				// send to HRT recon thread
			cmdFIFOMsg.cmdNdx = cmdNdx;
			for( ii = 0; ii < MAX_NUM_CMD_PARAMS; ii++)
			{
				if (CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmInt)
					cmdFIFOMsg.intParam[ii] = intParam[ii];
				else if (CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmFlt)
					cmdFIFOMsg.floatParam[ii] = floatParam[ii];
				else if (CmdTable[cmdNdx].paramIndicator[ii] == eCmdParmStr)
					strcpy( cmdFIFOMsg.strngParam[ii], strngParam[ii]);
			}
			if (write( CmdFIFOfd, &cmdFIFOMsg, sizeof(cmdFIFOMsgType)) < 0)
				HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) writing to command FIFO, SRT side, errno = ", errno );
			break;

		default:
				HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ProcCmd) invalid command table action = ", CmdTable[cmdNdx].action);
			break;
	}
	return eErrNoErr;
}

/*!
 *******************************************************************************
 *	@fn  gpAoSrtHostICon_GetCommandIndex( char* _cmdStr )
 *
 *	@brief
 *	Searches command table for command string - if found - returns the index, else returns -1
 *
 *	@b DESCRIPTION:
 *	Searches command table for command string - if found - returns the index, else returns -1
 *	 
 *	@par
 *
 *	@b Usage:  gpAoSrtHostICon_GetCommandIndex( char* _cmdStr )
 *
 *	@b Param [in] msgBuf (char[]): NULL terminated buffer containing ASCII message from the host
 *
 *	@return 
 *		-1		: Command not found
 *		>= 0	: Command Index
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
static int 
gpAoSrtHostICon_GetCommandIndex( char* _cmdStr )
{
int locCmdNdx = 0;

	while( ( CmdTable[ locCmdNdx ].idNum != eCmdEndFlag) && (strcmp( _cmdStr, CmdTable[ locCmdNdx ].strng) != 0))
		locCmdNdx++;

	if ( CmdTable[ locCmdNdx ].idNum == eCmdEndFlag)
		return -1;

	return locCmdNdx;
}

/*!
 *******************************************************************************
 *	@fn  gpAoSrtHostICon_ReadAperture( char* _cmdStr )
 *
 *	@brief
 *	Read in the bitmap file defining the clear aperture.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to read in the bitmap file defining the clear 
 *	aperture and set up the sub-aps accordingly.  
 *	@par
 *
 *	@b Usage:  gpAoSrtHostICon_ReadAperture( )
 *
 *	@b Param 
 *	None.
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/

static float tmpData[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];  // make this static so it gets inited to 0.0
static float tmpDatag[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];  // make this static so it gets inited to 0.0

static void 
gpAoSrtHostICon_ReadAperture( float* pCurrAp, short* pSubApCnt, short* pSkippedRowsTop, short* pSkippedRowsBot )
{
char	  fileName[ MAX_PATHNAME_LENGTH ];
fitsfile* fptr;
fitsfile* fptrg;
int		  status = 0;
long	  fpixel[10];  // will handle 2 dimensions
long	  gpixel[10];
int		  ii, jj;
int		  actCntr = 0;
char	  topFlagForSkip = true;
char	  botFlagForSkip = false;
int		  numElem = (int)( ( (long)NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP) * (NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP) * 3  );

	// open the aperture file, read it, and close it
	sprintf( fileName, "%sconfig/WFS_CALa.fits", configDir );
	//	sprintf( fileName, "%sconfig/aperture.fits", configDir );	// mrr
	if( fits_open_file( &fptr, fileName, READONLY, &status) != 0 )
		HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ReadAperture) 'WFS_CALa.fits' open failed, status = ", status);
	else
	{	
		fpixel[0] = 1;
		fpixel[1] = 1;
		fpixel[2] = 1;
		status = 0;
		if(	fits_read_pix( fptr, TFLOAT, fpixel, numElem, NULL, (void*)(&tmpData[0][0][0]), NULL, &status ) !=  0)
		{
			char tmpBuf[512];
			sprintf(tmpBuf, "ERR (gpAoSrtHostICon_ReadAperture) file error: 'WFS_CALa'; fits msg on next line:");
			HostIConSendStrngNoParm(eDebugLevelErr, tmpBuf );
			fits_get_errstatus( status, tmpBuf);
			HostIConSendStrngNoParm(eDebugLevelErr, tmpBuf );
		}
		else if( fits_close_file(fptr, &status) != 0 )
	    	HostIConSendStrngIntParm(eDebugLevelErr, "ERR (HostICon_ReadAper) unable to close file: ", status);
	}

	//sprintf( fileName, "%sconfig/aperture.fits", configDir );
	//if( fits_open_file( &fptrg, fileName, READONLY, &status) != 0 )
	//	HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_ReadAperture) 'aperture.fits' open failed, status = ", status);
	//else
	//{	
	//	status = 0;
	//	if(	fits_read_pix( fptrg, TFLOAT, gpixel, numElem, NULL, (void*)(&tmpDatag[0][0][0]), NULL, &status ) !=  0)
	//	{
	//		char tmpBuf[512];
	//		sprintf(tmpBuf, "ERR (gpAoSrtHostICon_ReadAperture) file error: 'aperture.fits'; fits msg on next line:");
	//		HostIConSendStrngNoParm(eDebugLevelErr, tmpBuf );
	//		fits_get_errstatus( status, tmpBuf);
	//		HostIConSendStrngNoParm(eDebugLevelErr, tmpBuf );
	//	}
	//	else if( fits_close_file(fptrg, &status) != 0 )
	//    	HostIConSendStrngIntParm(eDebugLevelErr, "ERR (HostICon_ReadAper) unable to close file: ", status);
	//}

	// transfer the aperture data into the 'pCurrAp' location
	printf("\ntmpData");
	for ( ii=0; ii<NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP; ii++)
	{
		printf("\n%d ", ii);
		for( jj=0; jj<NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP; jj++)
		{
			*( pCurrAp + ii * NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP + jj) = tmpData[2][ ii ][ jj ];
			printf("%f ", tmpData[2][ ii ][ jj ]);
		}
	}

	*pSkippedRowsTop = 0;		// This will count how many rows were skipped in the UPPER 'aperture area'
	*pSkippedRowsBot = 0;		// This will count how many rows were skipped in the LOWER 'aperture area'

	short	cntNumSubaps = 0;

	for ( ii=0; ii<NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP; ii++)
	{	// pre-check to see if all subaps in row are invalid
		if( !botFlagForSkip )
		{
			botFlagForSkip = true;
			for( jj=0; jj<NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP; jj++)
			{
				if ( *( pCurrAp + ii * NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP + jj) > 0)
				{
					topFlagForSkip = false;
					botFlagForSkip = false;
					break;
				}
			}			
		}

		if( topFlagForSkip )
			(*pSkippedRowsTop)++;	// count number of upper rows skipped in real aperture
		
		if( botFlagForSkip )
			(*pSkippedRowsBot)++;	// count number of lower rows skipped in real aperture

		for( jj=0; jj<NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP; jj++)
		{
			if ( *( pCurrAp + ii * NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP + jj) > 0)
			{
				srtFileDataShmPtr->quadCellDescs[cntNumSubaps].subApRow   = ii;

				srtFileDataShmPtr->quadCellDescs[cntNumSubaps].pixRows[0] = FIRST_CAM_ROW_IN_SUBAP;
				srtFileDataShmPtr->quadCellDescs[cntNumSubaps].pixRows[1] = FIRST_CAM_ROW_IN_SUBAP + 1;

				srtFileDataShmPtr->quadCellDescs[cntNumSubaps].pixCols[0] = jj * NUM_CAM_COLS_PER_SUBAP + FIRST_CAM_COL_IN_SUBAP;
				srtFileDataShmPtr->quadCellDescs[cntNumSubaps].pixCols[1] = jj * NUM_CAM_COLS_PER_SUBAP + FIRST_CAM_COL_IN_SUBAP + 1;

				cntNumSubaps++;
			}
			actCntr++;
		}
	}

	*pSubApCnt = cntNumSubaps;
	printf("\nSubApCnt = %d\nSkippedRowsTop = %d\nSkippedRowsBot = %d\n\n", *pSubApCnt, *pSkippedRowsTop, *pSkippedRowsBot);
}

/*!
 *******************************************************************************
 *	@fn  gpAoSrtHostICon_readFits2D(char *filename, int height, int width, int dataType, void *data)
 *
 *	@brief
 *	This unit reads a fits file from disk into a 2D array.  
 *
 *	@b DESCRIPTION:
 *	This unit reads a fits file from disk into a 2D array.  
 *	@par
 *
 *	@b Usage:  gpAoSrtHostICon_readFits2D(&filename, height, width, dataType, &data)
 *
 *	@b Param [in]  filename	(char[]):	name of fits file to read
 *	@b Param [in]  height	(int):		the length of one dimensions of the array
 *	@b Param [in]  width	(int):		the length of the other dimensions of the array
 *	@b Param [in]  dataType	(int):		data type, using cfitsio constants (TBYTE, TFLOAT, etc.)
 *	@b Param [out] data		(void *):	array into which data should be read (must be height times width)
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void 
gpAoSrtHostICon_readFits2D(char *filename, int height, int width, int dataType, void *data)
{
fitsfile *fptr;
int  status = 0;
long fpixel[4];

	//	open the file
	if (fits_open_file(&fptr, filename, READONLY, &status) != 0)
		HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_readFits2D) in fits_open_file(): ", status);
	else
	{
		//	read the data
		fpixel[0] = 1;
		fpixel[1] = 1;
		if (fits_read_pix(fptr, dataType, fpixel, height * width, NULL, data, NULL, &status) != 0)
			HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_readFits2D) in fits_read_pix(): ", status);
		else
		{
			//	close the file
			if (fits_close_file(fptr, &status) != 0)
				HostIConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtHostICon_readFits2D) in fits_close_file(): ", status);
		}
	}
}

/*!
 *******************************************************************************
 *	@fn  HostIConSendStrngNoParm( int msgDebugLevel, char *strngPtr )
 *	@fn  HostIConSendStrngIntParm( int msgDebugLevel, char *strngPtr, int intParam )
 *	@fn  HostIConSendStrngFltParm( int msgDebugLevel, char *strngPtr, float floatParam )
 *
 *	@brief
 *	These units send null-terminated strings to hostDiag.  
 *
 *	@b DESCRIPTION:
 *	These units send null-terminated strings to hostDiag. The strings are 
 *	generally status or error messages to be logged, output to the console, or sent
 *	to the bit bucket.  
 *	@par
 *
 *	@b Usage: HostIConSendStrngNoParm(  msgDebugLevel, strngPtr )
 *	@b Usage: HostIConSendStrngIntParm( msgDebugLevel, strngPtr, intParam )
 *	@b Usage: HostIConSendStrngFltParm( msgDebugLevel, strngPtr, floatParam )
 *	@b Usage: HostIConSendStrngStrParm( msgDebugLevel, strngPtr, strParamPtr )
 * 
 *	@param[in] msgDebugLevel (int):		message debug level
 *	@param[in] strngPtr		 (char*):	pointer to null-terminated character string
 *	@param[in] intParam		 (int):		integer parameter
 *	@param[in] floatParam	 (float):	float parameter
 *	@param[in] strParam		 (char*):	string parameter
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
static void 
HostIConSendStrngNoParm( int msgDebugLevel, char *strngPtr )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= srtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy(diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;

		if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
	    {
			//	can't send this message to hostDiag because the FIFO seems to have failed
			printf( "ERR (HostIConSendStrngNoParm) writing to diag FIFO, errno = %d, %s\n", errno, diagMsg.diagString );
	    }

		if (PRINTF_SEND_STRNGS_HI != 0)
			printf( "SRT HI - %s\n", diagMsg.diagString);
	}
}

static void 
HostIConSendStrngIntParm( int msgDebugLevel, char *strngPtr, int intParam )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= srtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngIntParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy(diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;
		diagMsg.diagFIFOMsg.intParam[0] = intParam;

		if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
	    {
			//	can't send this message to hostDiag because the FIFO seems to have failed
			printf( "ERR (HostIConSendStrngIntParm) writing to diag FIFO, errno = %d, %s\n", errno, diagMsg.diagString );
	    }

		if (PRINTF_SEND_STRNGS_HI != 0)
			printf( "SRT HI - %s\n", diagMsg.diagString);
	}
}

static void 
HostIConSendStrngFltParm( int msgDebugLevel, char *strngPtr, float floatParam )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= srtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngFltParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy(diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;
		diagMsg.diagFIFOMsg.floatParam[0] = floatParam;

		if (write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
	    {
			//	can't send this message to hostDiag because the FIFO seems to have failed
			printf( "ERR (HostIConSendStrngFltParm) writing to diag FIFO, errno = %d, %s\n", errno, diagMsg.diagString );
	    }

		if (PRINTF_SEND_STRNGS_HI != 0)
			printf( "SRT HI - %s\n", diagMsg.diagString);
	}
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
