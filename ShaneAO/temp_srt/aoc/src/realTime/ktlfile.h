#ifndef KTLFILE_H_4829237
#define KTLFILE_H_4829237

#include "virtktlkeys.h"


class CKTLFile;


class CKTLFileString : public CKTLString
{
 public:
    CKTLFileString(const char *name, int key, const char *val, int readWrite, 
                   KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                   CKTLFile *pKTLFile);
    CKTLFile *m_pKTLFile;
};

class CKTLFileInt : public CKTLInt
{
 public:
    CKTLFileInt(const char *name, int key, int val, int readWrite, 
                KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                CKTLFile *pKTLFile);
    CKTLFile *m_pKTLFile;
};


class CKTLFile
{
 public:

    CKTLFile(CInterface *pInter, string baseName,  
             KTLChangeActionFunc    af,
             KTLDelayedResponseFunc dr); 
    virtual ~CKTLFile();

    CKTLFileString *m_pName;
    CKTLFileInt    *m_pStatus;
    string          m_BaseName;
    CInterface     *m_pInter;

    KTLChangeActionFunc m_afFile;
    KTLDelayedResponseFunc m_drFile;

    virtual int SetFileName(string newVal);
    virtual int SetFileStatus(int newVal);

    virtual int TestComplete(string targetVal);
    virtual int TestComplete(int targetVal);

};




#endif
