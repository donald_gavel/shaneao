/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoDispReconFtr.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to display Fourier transform 
 *	reconstructor (ftr) modes.  
 *
 *	@b DESCRIPTION:
 *	This component contains code to display Fourier transform reconstructor (ftr) 
 *	modes to the monitor.  It is similar to dispraw (that was based on the 
 *	graywin.c example code in the SDL/test directory).  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 6/3/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDispReconFtrCreateScreen()
 *	-# @c gpAoSrtDispReconFtrDraw()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

//#include <gpUtGeneral.h>
//#include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "SDL.h"


// Configuration Variables

#define NUM_COLORS	256
#define PIX_SIZE    10

static const long secDelay = 0;			   // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;								   // mmap file descriptor
static unsigned char *startPtr2 = (unsigned char *)0;  // pointer to start of shared memory
static size_t reconDataSize;						   // size of shared memory

static ReconDataType *reconDataPtr;  // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


static SDL_Surface *gpAoSrtDispReconFtrCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)
{
	SDL_Surface *screen;
	int i;
	SDL_Color palette[NUM_COLORS];

// 	set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

//	set a gray colormap from black to white
	for ( i = 0; i < NUM_COLORS; i++ )
	{
		palette[i].r = i * (256 / NUM_COLORS);
		palette[i].g = i * (256 / NUM_COLORS);
		palette[i].b = i * (256 / NUM_COLORS);
	}
	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}


static float tmpTmpFtr[APERTURE_GRID_SIZEROW][APERTURE_GRID_SIZECOL][2];
static float tmpFtr[APERTURE_GRID_SIZEROW][APERTURE_GRID_SIZECOL];

int main(void)
{
	int   i, j, m, n;
	Uint8 tmpData;
	Uint8 *buffer;

	int crntReadBuffNdx;

	struct timespec next, remaining;

	const int	 width		= NUM_SUBAPS_PER_ROW * PIX_SIZE;
	const int	 height		= NUM_SUBAPS_PER_COL * PIX_SIZE;
	const int	 bpp 		= 8;  // bits per pixel -- any more than 8 makes this really slow
	const Uint32 videoflags = SDL_SWSURFACE;

	static int satFlashCntr = 0;
	static SDL_Surface *screen = NULL; 
	static SDL_Event event;

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	determine sizes of global variables
	reconDataSize = sizeof(ReconDataType);
	printf("spAoDispReconFtr: Attempting to open shared memory:: Size = %lld / 0x%llx\n", reconDataSize, reconDataSize );

//	open the shared memory region
//	IMPORTANT: reconData is now in its own AOC_mmap2 -- dispftr doesn't need the others, so the pointers to
//	> them are not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap2", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispReconFtrDraw: waiting for mmap open: /dev/AOC_mmap2\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap2", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispReconFtrDraw: waiting for mmap open: ./AOC_mmap2\n");
		sleep(1);
	}
#endif

//	get the pointer to the shared memory region
	while (!quit && ((startPtr2 = mmap(0, reconDataSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispReconFtrDraw: waiting for mmap: %llx\n", reconDataSize);
		sleep(1);
	}
	printf( "gpAoSrtDispReconFtrDraw: Shm start Ptr = %p, reconDataSize = 0x%llx\n", startPtr2, reconDataSize );

//	set up the pointers to the global variables
	reconDataPtr = (ReconDataType *)startPtr2;

//	initialize SDL and exit if we can't set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispReconFtrDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispReconFtrCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispReconFtrDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

	float minVal, maxVal, scaleFactor;

//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

//		move data out of the reconDispFtrModes[][][] quickly
		crntReadBuffNdx = reconDataPtr->reconDispFtrModesReadBuffNdx;
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
			{
				tmpTmpFtr[i][j][0] = reconDataPtr->reconDispFtrModes[crntReadBuffNdx][i * NUM_SUBAPS_PER_ROW + j][0];  // real
				tmpTmpFtr[i][j][1] = reconDataPtr->reconDispFtrModes[crntReadBuffNdx][i * NUM_SUBAPS_PER_ROW + j][1];  // imag
			}
		}

		minVal =  100000.0;
		maxVal = -100000.0;

//		calculate the modulus, finding mins and maxes to auto-scale by frame
		float tmpR, tmpI;
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
			{
				tmpR = tmpTmpFtr[i][j][0];  // real
				tmpI = tmpTmpFtr[i][j][1];  // imag
				tmpFtr[i][j] = sqrt(tmpR * tmpR + tmpI * tmpI);
				if (tmpFtr[i][j] > 0.0)
					tmpFtr[i][j] = log10(tmpFtr[i][j]);  // put this on a log scale
				if (tmpFtr[i][j] < minVal)
					minVal = tmpFtr[i][j];
				if ((tmpFtr[i][j] > maxVal) && (tmpFtr[i][j] != 0.0))
					maxVal = tmpFtr[i][j];
			}
		}

//		calculate scale factor
		if (minVal == 100000.0)
			minVal = 0.0;
		if (maxVal == -100000.0)
			maxVal = 0.0;
		if (maxVal == minVal)
			scaleFactor = 0.0;
		else
			scaleFactor = 255.0 / ((float)maxVal - (float)minVal);

//		this is to flash saturated sub-aps in other disp programs; here, we're just using it to print out min and max
		if (++satFlashCntr >= 5)
			printf("FFF Frame min: %8.4f, max: %8.4f\n", minVal, maxVal);

//		auto-scale
		Uint8 tmpBuffer[NUM_SUBAPS_PER_COL][NUM_SUBAPS_PER_ROW];
		for ( i = 0; i < NUM_SUBAPS_PER_COL; i++ )
		{
			for ( j = 0; j < NUM_SUBAPS_PER_ROW; j++ )
				tmpBuffer[i][j] = (Uint8)((tmpFtr[i][j] - minVal) * scaleFactor);
		}

//		spread the data over the display pixels
		buffer = (Uint8 *)screen->pixels;
		for (i = 0; i < NUM_SUBAPS_PER_COL; i++)
		{
			for (j = 0; j < NUM_SUBAPS_PER_ROW; j++)
			{
				tmpData = tmpBuffer[i][j];
				for (m = 0; m < PIX_SIZE; m++)
				{
					for (n = 0; n < PIX_SIZE; n++)
						buffer[((i * PIX_SIZE) + m) * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + (j * PIX_SIZE) + n] = tmpData;
				}
			}
		}

//		draw in sub-ap grid lines
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i += PIX_SIZE)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j++)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = 255;
		}
		for (i = 0; i < (NUM_SUBAPS_PER_COL * PIX_SIZE); i++)
		{
			for (j = 0; j < (NUM_SUBAPS_PER_ROW * PIX_SIZE); j += PIX_SIZE)
				buffer[i * (NUM_SUBAPS_PER_ROW * PIX_SIZE) + j] = 255;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

//		check for quit
		while (!quit && SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				printf("dispftr quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(reconDataPtr, reconDataSize);
	  	close(mmfd);
	}
}


static void termHandler(int sigNum)
{
	printf("dispftr quitting due to OS term: %d\n", sigNum);
	quit = true;
}
