/*******************************************************************************
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 ******************************************************************************/
/*!
 *	@file WooferThread.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to display woofer phase.  
 *
 *	@b DESCRIPTION:
 *	This component contains code to display display woofer phase to the monitor.  
 *	It is similar to dispraw (that was based on the graywin.c example code in the 
 *	SDL/test directory).  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 6/7/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDispReconWfrCreateScreen()
 *	-# @c gpAoSrtDispReconWfrDraw()
 *
 *	@b REVISIONS:
 *
 ********************************************************************************/

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

#include "gpUtGmbHrt.h"	// mrr

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "SDL.h"

// Configuration Variables
#define USING_DISPLAY	1	// mrr

#if ( USING_DISPLAY )	// mrr
	#define NUM_COLORS	256
	#define PIX_SIZE    60

	#define SAT_MINUS_SPOT_NDX 254
	#define SAT_PLUS_SPOT_NDX  255
#endif

static const float satMinusLimit = -3.8;  // microns
static const float satPlusLimit  =  3.8;  // microns

static const long secDelay = 0;			   // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate

// Global Variables

// Local Variables
static boolean quit = false;
static int mmfd = -1;								   // mmap file descriptor
static unsigned char *startPtr2 = (unsigned char *)0;  // pointer to start of shared memory
static size_t reconDataSize;						   // size of shared memory

static ReconDataType *reconDataPtr;  // pointer to recon shared memory data

// Local Function Prototypes
static void termHandler(int sigNum);


#if ( USING_DISPLAY )	// mrr
/**********************************************************************************************/
//	Eventually we won't need to use the display, but for now it will be useful	// mrr
/**********************************************************************************************/
static SDL_Surface *
gpAoSrtDispReconWfrCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)
{
SDL_Surface *screen;
int i;
SDL_Color palette[NUM_COLORS];

	// 	set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

	//	set a gray colormap from black to white
	for ( i = 0; i < (NUM_COLORS - 2); i++ )
	{
		palette[i].r = i * (256 / (NUM_COLORS - 2));
		palette[i].g = i * (256 / (NUM_COLORS - 2));
		palette[i].b = i * (256 / (NUM_COLORS - 2));
	}

	palette[SAT_MINUS_SPOT_NDX].r = 255;  // red
	palette[SAT_MINUS_SPOT_NDX].g = 0;
	palette[SAT_MINUS_SPOT_NDX].b = 0;

	palette[SAT_PLUS_SPOT_NDX].r = 255;  // yellow
	palette[SAT_PLUS_SPOT_NDX].g = 255;
	palette[SAT_PLUS_SPOT_NDX].b = 0;

	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}
#endif

/**********************************************************************************************/

/**********************************************************************************************/
static float tmpWfr[WOOFER_COL_SIZE][WOOFER_ROW_SIZE];

int 
main(void)
{
int   i, j, m, n;
int	  xxIn, xxOut, yy;
Uint8 tmpData;
Uint8 *buffer;

int crntReadBuffNdx;
float minVal, maxVal; 
struct timespec next, remaining;

#if ( USING_DISPLAY )	// mrr
	const int	 width		= WOOFER_ROW_SIZE * PIX_SIZE;
	const int	 height		= WOOFER_COL_SIZE * PIX_SIZE;
	float scaleFactor;
#endif

const int	 bpp 		= 8;  // bits per pixel -- any more than 8 makes this really slow
const Uint32 videoflags = SDL_SWSURFACE;

static int satFlashCntr = 0;
static SDL_Surface *screen = NULL; 
static SDL_Event event;

	//	set up signal handlers
	signal(SIGTERM, termHandler);

	//	determine sizes of global variables
	reconDataSize = sizeof(ReconDataType);
	printf("spAoDispReconWfr: Attempting to open shared memory:: Size = %lld / 0x%llx\n", reconDataSize, reconDataSize );

	//	open the shared memory region
	//	IMPORTANT: reconData is now in its own AOC_mmap2 -- dispftr doesn't need the others, so the pointers to
	//	> them are not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap2", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispReconWfrDraw: waiting for mmap open: /dev/AOC_mmap2\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap2", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispReconWfrDraw: waiting for mmap open: ./AOC_mmap2\n");
		sleep(1);
	}
#endif

	//	get the pointer to the shared memory region
	while (!quit && ((startPtr2 = mmap(0, reconDataSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispReconWfrDraw: waiting for mmap: %llx\n", reconDataSize);
		sleep(1);
	}
	printf( "gpAoSrtDispReconWfrDraw: Shm start Ptr = %p, reconDataSize = 0x%llx\n", startPtr2, reconDataSize );

	//	set up the pointers to the global variables
	reconDataPtr = (ReconDataType *)startPtr2;

#if ( USING_DISPLAY )	// mrr
	//	initialize SDL and exit if we can't set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispReconWfrDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispReconWfrCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispReconWfrDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

#endif

	//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

		minVal =  100000.0;
		maxVal = -100000.0;

		//	move data out of the reconDispWfrPhase[][] quickly, finding mins and maxes to display (centroids are not auto-scaled)
		//	> the woofer data is flipped in X, here, for display purposes
		crntReadBuffNdx = reconDataPtr->reconDispWfrPhaseReadBuffNdx;
		for( yy = 0; yy < WOOFER_COL_SIZE; yy++)
		{
			for( xxIn = 0, xxOut = WOOFER_ROW_SIZE - 1; xxIn < WOOFER_ROW_SIZE; xxIn++, xxOut-- )
			{
				tmpWfr[yy][xxOut] = reconDataPtr->reconDispWfrPhase[crntReadBuffNdx][yy * WOOFER_ROW_SIZE + xxIn];
				if (tmpWfr[yy][xxOut] < minVal)
					minVal = tmpWfr[yy][xxOut];
				if (tmpWfr[yy][xxOut] > maxVal)
					maxVal = tmpWfr[yy][xxOut];
			}
		}

		//	At this point we send it out the socket to the Woofer Driver PC
		//	We need to clip it for min max and scale it for the correct current











#if ( USING_DISPLAY )	// mrr
		//	calculate scale factor
		if (minVal == 100000.0)
			minVal = 0.0;
		if (maxVal == -100000.0)
			maxVal = 0.0;
		if (maxVal == minVal)
			scaleFactor = 0.0;
		else
			scaleFactor = 253.0 / ((float)maxVal - (float)minVal);

		//	this is to flash saturated sub-aps in other disp programs; here, we're just using it to print out min and max
		if (++satFlashCntr >= 5)
			printf("WWW Frame min: %8.4f, max: %8.4f\n", minVal, maxVal);

		//	auto-scale
		Uint8 tmpBuffer[WOOFER_COL_SIZE][WOOFER_ROW_SIZE];
		for ( i = 0; i < WOOFER_COL_SIZE; i++ )
		{
			for ( j = 0; j < WOOFER_ROW_SIZE; j++ )
			{
				if (tmpWfr[i][j] < satMinusLimit)
					tmpBuffer[i][j] = SAT_MINUS_SPOT_NDX;
				else if (tmpWfr[i][j] > satPlusLimit)
					tmpBuffer[i][j] = SAT_PLUS_SPOT_NDX;
				else
					tmpBuffer[i][j] = (Uint8)((tmpWfr[i][j] - minVal) * scaleFactor);
			}
		}

		//	spread the data over the display pixels
		buffer = (Uint8 *)screen->pixels;
		for (i = 0; i < WOOFER_COL_SIZE; i++)
		{
			for (j = 0; j < WOOFER_ROW_SIZE; j++)
			{
				tmpData = tmpBuffer[i][j];
				for (m = 0; m < PIX_SIZE; m++)
				{
					for (n = 0; n < PIX_SIZE; n++)
						buffer[((i * PIX_SIZE) + m) * (WOOFER_ROW_SIZE * PIX_SIZE) + (j * PIX_SIZE) + n] = tmpData;
				}
			}
		}

		//	draw in grid lines
		for (i = 0; i < (WOOFER_COL_SIZE * PIX_SIZE); i += PIX_SIZE)
		{
			for (j = 0; j < (WOOFER_ROW_SIZE * PIX_SIZE); j++)
				buffer[i * (WOOFER_ROW_SIZE * PIX_SIZE) + j] = 253;
		}
		for (i = 0; i < (WOOFER_COL_SIZE * PIX_SIZE); i++)
		{
			for (j = 0; j < (WOOFER_ROW_SIZE * PIX_SIZE); j += PIX_SIZE)
				buffer[i * (WOOFER_ROW_SIZE * PIX_SIZE) + j] = 253;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
#endif
		//	check for quit
		while (!quit && SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				printf("dispwfr quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(reconDataPtr, reconDataSize);
	  	close(mmfd);
	}
}
////////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////
static void 
termHandler(int sigNum)
{
	printf("WooferThread quiting due to OS term: %d\n", sigNum);
	quit = true;
}
