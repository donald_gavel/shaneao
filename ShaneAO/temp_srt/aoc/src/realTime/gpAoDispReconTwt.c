/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoDispReconTwt.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to display tweeter phase.  
 *
 *	@b DESCRIPTION:
 *	This component contains code to display display tweeter phase to the monitor.  
 *	It is similar to dispraw (that was based on the graywin.c example code in the 
 *	SDL/test directory).  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 6/7/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDispReconTwtCreateScreen()
 *	-# @c gpAoSrtDispReconTwtDraw()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

//#include <gpUtGeneral.h>
//#include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "SDL.h"


// Configuration Variables

#define NUM_COLORS	256
#define PIX_SIZE    10

#define NEIGH_LOW_SPOT_NDX   252
#define NEIGH_HIGH_SPOT_NDX  253
#define SAT_MINUS_SPOT_NDX   254
#define SAT_PLUS_SPOT_NDX    255

static const float satMinusLimit    = -1.2;  // microns
static const float satPlusLimit     =  1.2;  // microns

static const long secDelay = 0;			   // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate

#define NUM_TWEETER_ROWS 32	// mrr	DM
#define NUM_TWEETER_COLS 32	// mrr	DM


// Global Variables


// Local Variables

static float twtNeighborLimit;  // microns

static boolean quit = false;
static int mmfd = -1;								   // mmap file descriptor
static unsigned char *startPtr2 = (unsigned char *)0;  // pointer to start of shared memory
static size_t reconDataSize;						   // size of shared memory

static ReconDataType *reconDataPtr;  // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);


static SDL_Surface *gpAoSrtDispReconTwtCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)
{
	SDL_Surface *screen;
	int i;
	SDL_Color palette[NUM_COLORS];

// 	set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

//	set a gray colormap from black to white
	for ( i = 0; i < (NUM_COLORS - 4); i++ )
	{
		palette[i].r = i * (256 / (NUM_COLORS - 4));
		palette[i].g = i * (256 / (NUM_COLORS - 4));
		palette[i].b = i * (256 / (NUM_COLORS - 4));
	}

	palette[NEIGH_LOW_SPOT_NDX].r = 0;     // blue
	palette[NEIGH_LOW_SPOT_NDX].g = 0;
	palette[NEIGH_LOW_SPOT_NDX].b = 255;

	palette[NEIGH_HIGH_SPOT_NDX].r = 255;  // purple
	palette[NEIGH_HIGH_SPOT_NDX].g = 0;
	palette[NEIGH_HIGH_SPOT_NDX].b = 255;

	palette[SAT_MINUS_SPOT_NDX].r = 255;   // red
	palette[SAT_MINUS_SPOT_NDX].g = 0;
	palette[SAT_MINUS_SPOT_NDX].b = 0;

	palette[SAT_PLUS_SPOT_NDX].r = 255;    // yellow
	palette[SAT_PLUS_SPOT_NDX].g = 255;
	palette[SAT_PLUS_SPOT_NDX].b = 0;

	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}


static float tmpTwt[NUM_TWEETER_ROWS][NUM_TWEETER_COLS];

int main(int argc, char *argv[])
{
	int   i, j, m, n;
	int	  xx, yyIn, yyOut;
	Uint8 tmpData;
	Uint8 *buffer;

	int crntReadBuffNdx;

	struct timespec next, remaining;

	const int	 width		= NUM_TWEETER_COLS * PIX_SIZE;
	const int	 height		= NUM_TWEETER_ROWS * PIX_SIZE;
	const int	 bpp 		= 8;  // bits per pixel -- any more than 8 makes this really slow
	const Uint32 videoflags = SDL_SWSURFACE;

	static int satFlashCntr = 0;
	static SDL_Surface *screen = NULL; 
	static SDL_Event event;

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	set defaults and then parse command line
	twtNeighborLimit = 0.4;  // microns
    --argc;
    ++argv;
    while (argc && (argv[0][0] == '-'))
    {
		switch (argv[0][1])
		{
			case 'n':		// neighbor limit
			    ++argv;
			    --argc;
			    twtNeighborLimit = atof(argv[0]);
			    break;

			case '?':
			case 'h':
			    printf("\nDump raw wfs camera data for GPI wfs\n");
			    printf("usage:\n");
			    printf("  -n limit  actuator neighbor limit\n");
			    printf("  -h        print this help message\n");
			    exit(0);
			    break;

			default:
			    printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			    break;
		}
		argc--;
		argv++;
    }

//	determine sizes of global variables
	reconDataSize = sizeof(ReconDataType);
	printf("spAoDispReconTwt: Attempting to open shared memory:: Size = %lld / 0x%llx\n", reconDataSize, reconDataSize );

//	open the shared memory region
//	IMPORTANT: reconData is now in its own AOC_mmap2 -- disptwt doesn't need the others, so the pointers to
//	> them are not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap2", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispReconTwtDraw: waiting for mmap open: /dev/AOC_mmap2\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap2", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispReconTwtDraw: waiting for mmap open: ./AOC_mmap2\n");
		sleep(1);
	}
#endif

//	get the pointer to the shared memory region
	while (!quit && ((startPtr2 = mmap(0, reconDataSize, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispReconTwtDraw: waiting for mmap: %llx\n", reconDataSize);
		sleep(1);
	}
	printf( "gpAoSrtDispReconTwtDraw: Shm start Ptr = %p, reconDataSize = 0x%llx\n", startPtr2, reconDataSize );

//	set up the pointers to the global variables
	reconDataPtr = (ReconDataType *)startPtr2;

//	initialize SDL and exit if we can't set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispReconTwtDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispReconTwtCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispReconTwtDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

	float minVal, maxVal, scaleFactor;

//	enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

		minVal =  100000.0;
		maxVal = -100000.0;

//		move data out of the reconDispTwtPhase[][] quickly, finding mins and maxes to display (centroids are not auto-scaled)
//		> the tweeter data is flipped in Y, here, for display purposes
		crntReadBuffNdx = reconDataPtr->reconDispTwtPhaseReadBuffNdx;
		for( yyIn = 0, yyOut = NUM_TWEETER_ROWS - 1; yyIn < NUM_TWEETER_ROWS; yyIn++, yyOut--)
		{
			for( xx = 0; xx < NUM_TWEETER_ROWS; xx++ )
			{
				tmpTwt[yyIn][xx] = reconDataPtr->reconDispTwtPhase[crntReadBuffNdx][yyOut * NUM_TWEETER_ROWS + xx];
				if (tmpTwt[yyIn][xx] < minVal)
					minVal = tmpTwt[yyIn][xx];
				if (tmpTwt[yyIn][xx] > maxVal)
					maxVal = tmpTwt[yyIn][xx];
			}
		}

//		calculate scale factor
		if (minVal == 100000.0)
			minVal = 0.0;
		if (maxVal == -100000.0)
			maxVal = 0.0;
		if (maxVal == minVal)
			scaleFactor = 0.0;
		else
			scaleFactor = 251.0 / ((float)maxVal - (float)minVal);

//		this is to flash saturated sub-aps in other disp programs; here, we're just using it to print out min and max
		if (++satFlashCntr >= 5)
			printf("TTT Frame min: %8.4f, max: %8.4f\n", minVal, maxVal);

//		auto-scale
		Uint8 tmpBuffer[NUM_TWEETER_ROWS][NUM_TWEETER_COLS];
		for ( i = 0; i < NUM_TWEETER_ROWS; i++ )
		{
			for ( j = 0; j < NUM_TWEETER_COLS; j++ )
			{
//				if (tmpTwt[i][j] < satMinusLimit)
//					tmpBuffer[i][j] = SAT_MINUS_SPOT_NDX;
//				else if (tmpTwt[i][j] > satPlusLimit)
//					tmpBuffer[i][j] = SAT_PLUS_SPOT_NDX;
//				else
					tmpBuffer[i][j] = (Uint8)((tmpTwt[i][j] - minVal) * scaleFactor);
			}
		}

//XXXdwpDifferentialActPokeLimit //XXXdwp110803 -- start
//		now, to test the actuator neighbor limit, check against the limits and set the pixel color accordingly -- NOTE 
//		> that, for this, the sub-aps are not masked and slaves are NOT omitted
		float neighborAve, neighborDiff;
		for ( i = 1; i < (NUM_TWEETER_ROWS - 1); i++ )
		{
			for ( j = 1; j < (NUM_TWEETER_COLS - 1); j++ )
			{
				neighborAve = (tmpTwt[i][j - 1] + tmpTwt[i][j + 1] + tmpTwt[i - 1][j] + tmpTwt[i + 1][j]) / 4.0;
				neighborDiff = tmpTwt[i][j] - neighborAve;
				if (neighborDiff > twtNeighborLimit)  // the actuator is too 'big' with respect to its neighbors
					tmpBuffer[i][j] = NEIGH_HIGH_SPOT_NDX;
				else if (neighborDiff < -twtNeighborLimit)  // the actuator is too 'little' with respect to its neighbors
					tmpBuffer[i][j] = NEIGH_LOW_SPOT_NDX;

				if (tmpTwt[i][j] < satMinusLimit)		   // for now, put the saturated on top of the high and low limits
					tmpBuffer[i][j] = SAT_MINUS_SPOT_NDX;
				else if (tmpTwt[i][j] > satPlusLimit)
					tmpBuffer[i][j] = SAT_PLUS_SPOT_NDX;
			}
		}
//XXXdwpDifferentialActPokeLimit -- end

//		spread the data over the display pixels
		buffer = (Uint8 *)screen->pixels;
		for (i = 0; i < NUM_TWEETER_ROWS; i++)
		{
			for (j = 0; j < NUM_TWEETER_COLS; j++)
			{
				tmpData = tmpBuffer[i][j];
				for (m = 0; m < PIX_SIZE; m++)
				{
					for (n = 0; n < PIX_SIZE; n++)
						buffer[((i * PIX_SIZE) + m) * (NUM_TWEETER_COLS * PIX_SIZE) + (j * PIX_SIZE) + n] = tmpData;
				}
			}
		}

//		draw in grid lines
		for (i = 0; i < (NUM_TWEETER_ROWS * PIX_SIZE); i += PIX_SIZE)
		{
			for (j = 0; j < (NUM_TWEETER_COLS * PIX_SIZE); j++)
				buffer[i * (NUM_TWEETER_COLS * PIX_SIZE) + j] = 251;
		}
		for (i = 0; i < (NUM_TWEETER_ROWS * PIX_SIZE); i++)
		{
			for (j = 0; j < (NUM_TWEETER_COLS * PIX_SIZE); j += PIX_SIZE)
				buffer[i * (NUM_TWEETER_COLS * PIX_SIZE) + j] = 251;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

//		check for quit
		while (!quit && SDL_PollEvent(&event))
		{
			if (event.type == SDL_QUIT)
			{
				printf("disptwt quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(reconDataPtr, reconDataSize);
	  	close(mmfd);
	}
}


static void termHandler(int sigNum)
{
	printf("disptwt quitting due to OS term: %d\n", sigNum);
	quit = true;
}
