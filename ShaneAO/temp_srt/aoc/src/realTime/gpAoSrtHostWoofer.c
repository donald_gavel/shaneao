/*******************************************************************************
 *
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoSrtHostWoofer.c
 *
 *	@brief 
 *	Host Woofer interface.
 *
 *	@b DESCRIPTION:
 *	This component is the host Woofer interface.  It is a thread running on the 
 *	Soft Real-Time (SRT) processor.  Its main purpose is to send shapes to the Woofer 
 *  Processor.
 *
 *	The HRT processors store the Woofer data in circular buffers that go back 
 *	in time for 10 seconds.  So, when running at 1.5 kHz, a minimum of 15,000 data sets are
 *	required.  Additionally there must be some TBD number of buffers allocated to give the
 *	SRT time to process the data. These circular buffers are stored in shared memory, to
 *	allow the SRT side access to it.  
 *
 *	@b AUTHOR:
 *	MRR, SMJ & DWP	DATE: 8/22/2012
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtHostWoofer()
 *	-# @c gpAoSrtHostDiag_ProcCmd()
 *	-# @c gpAoSrtHostDiag_OpenFitsFile()
 *	-# @c gpAoSrtHostDiag_SetupHeader()
 *	-# @c gpAoSrtHostDiag_DumpData()
 *	-# @c gpAoSrtHostDiag_SetupFitsHeader()
 *	-# @c gpAoSrtHostDiag_WriteOneFrameData()
 *	-# @c gpAoSrtHostDiag_KillDumpDataThread()
 *	-# @c gpAoSrtHostDiag_MoveDisplayData()
 *	-# @c HostDiagSendCamDisplayData()
 *	-# @c HostDiagSendCentDisplayData()
 *	-# @c HostDiagSendTwtDisplayData()
 *	-# @c HostDiagSendWfrDisplayData()
 *	-# @c HostDiagSendTTDisplayData()
 *	-# @c HostDiagSendStrngIntParm()
 *	-# @c HostDiagSendStrngFltParm()
 *	-# @c HostDiagSendStrngIntParm()
 *	-# @c HostDiagSendSocketMsg()
 *	-# @c HostDiagSendSocketData()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>

#include "gpUtGmbHrt.h"	// mrr

#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtReconOpt.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoSrtHostWoofer.h"
 
#include "fitsio.h"

#define PRINTF_SEND_STRNGS_HD 0  // use this to turn off printfs for strings being sent to the host (for normal operation) // mrr
//#define PRINTF_SEND_STRNGS_HD 1  // use this to turn on printfs for strings being sent to the host (for testing)

/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtHostWoofer( void *arg )
 *
 *	@brief
 *	Woofer data handler mainline.
 *
 *	@b DESCRIPTION:
 *	This unit is the data handler for the Woofer.  It performs initializations
 *	for the gpAoSrtHostWoofer thread and then sits on a FIFO waiting for commands 
 *  (processing them as they are received) or a timeout to send data over the 
 *  socket to the Woofer controller hardware.  
 *	@par
 *
 *	@b Usage: 
 *	Invoked as a result of pthread_create().
 * 
 *	@param[in] arg (void *): Needed for pthread_create(), not currently used.
 *
 *	@return 
 *	(void *) Always 0 (NULL).
 * 
 *	@exception None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
void *
gpAoSrtHostWoofer( void *arg )
{
	short			cmdNdx, diagStringLength, ii, retVal;
	int				intParam[MAX_NUM_CMD_PARAMS] = { 0, 0, 0 };
	float			floatParam[MAX_NUM_CMD_PARAMS] = { 0.0, 0.0, 0.0};
	char			diagString[DIAG_STRING_MAX_LENGTH];
	diagFIFOMsgType	diagFIFOMsg;
	static int		retryFifo = 0;

//	enter an almost-infinite loop
	while( !WooferquitThread )
	{

	}


	return NULL;  // should only execute on shutdown
}
