#ifndef HOST_ICON_CENTRAL_H
#define HOST_ICON_CENTRAL_H

#include <semaphore.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#include "gpUtGmbHrt.h"	// mrr

#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "fitsio.h"

#include "gpAoStubs.h"
#include "gpAoHrtSysCon.h"


#include "eventthread.h"
#include "hostIConListener.h"
#include "updateval.h"
#include "virtktlkeys.h"
#include "inter.h"

#include "missingdefs.h"



class CCentral : public CEventThread
{
public:
    CCentral();
    ~CCentral();

    int Initialize();


    // Message ID that this event thread understands.
    enum {
	FIRST_CONTROL_MSG                      = 2000,
	CONTROL_INFO_UPDATE                    = 2001,
	HOSTCONNECTEDFLAG                      = 2002,
	HOSTICONPROCCMD                        = 2003,
	HOST_SEND_STRNG_NOPARM                 = 2004,
	HOST_SEND_STRNG_INT                    = 2005,
	HOST_SEND_STRNG_FLT                    = 2006,
	EXAM_DIAG_CMD_ERR_STRNG_NO_PARM        = 2007,
	EXAM_DIAG_CMD_ERR_STRNG_INT_PARM       = 2008,
	EXAM_DIAG_CMD_ERR_STRNG_FLT_PARM       = 2008,

	LAST_CONTROL_MSG  = 9999
    };

    static void* connector(void *thisPointer);

    virtual void vGetMsgRange(int *first, int *last){
	*first=FIRST_CONTROL_MSG;*last=LAST_CONTROL_MSG;};
    virtual bool vInMsgRange(int msg){
	return ((FIRST_CONTROL_MSG <= msg) && (msg <= LAST_CONTROL_MSG));};
    virtual int vStartupActions();

    // Local Variables
    int cmdFIFOfd;   // fd for FIFO used to send command messages to the HRT side
    pthread_t hostDiagThread;  // thread handles   // &&& change to CThread
    pthread_t DM_Thread;  // thread handles	// mrr   // &&& change to CThread
    
    // Global Function Prototype 
    int queControlInfoUpdate();
    int queHostConnectedFlag(bool connected);
    int queHostIConProcCmd(const char *msgbuf, int subCmdFlag);
    int queHostIConProcCmd(const char *msgbuf, int subCmdFlag, int length);
    int queHostIConSendStrngNoParm(int debugLevel, const char *str);
    int queHostIConSendStrngIntParm(int debugLevel, const char *str, int n);
    int queHostIConSendStrngFltParm(int debugLevel, const char *str, float f);

    int queExamine_eDiagCmdErrStrngNoParm(const char *diagString);
    int queExamine_eDiagCmdErrStrngIntParm(const char *diagString, int intParam);
    int queExamine_eDiagCmdErrStrngFltParm(const char *diagString, double fltParam);

    int setNEEDFUNC(CKTLKeyInt *pkey, int val);
    int setNEEDFUNC(CKTLKeyString *pkey, string val);
    int setNEEDFUNC(CKTLKeyFloat *pkey, double val);


    int setDWIM(string val);

    // Functions for text commands 
    // "ver" command
    int set_ver(string val); 
    bool check_ver(); 

    int  set_telem(int val);
    void update_telem(int val);
    int  test_complete_telem(int respval);
    
    int  set_trate(int val);
    void update_trate(int val);
    int  test_complete_trate(int respval);
    
    int  set_debug(int val);
    int  set_sdebug();
    int  test_complete_debug(int val);
    int  test_complete_sdebug();
    void update_debug_sdebug(int val);

    int  set_gsmode(int val);
    void update_gsmode(int val);
    int  test_complete_gsmode(int respval);


    int GetCommandIndex(const char* cmdStr);
    void ReadAperture( float *pCurrAp, short *pSubApCnt, short *pSkippedRowsTop, short *pSkippedRowsBot );

protected:
    void CenterThread();      // the main function for this thread
    
    int vMsgHandler(CQMsg *pqmsg);

    int doControlInfoUpdate(CQMsg *pqmsg);
    int ControlInfoUpdate();

    int doHostConnectedFlag(CQMsg *pqmsg);
    int doHostIConProcCmd(CQMsg *pqmsg);
    int doHostIConSendStrngNoParm(CQMsg *pqmsg);
    int doHostIConSendStrngIntParm(CQMsg *pqmsg);
    int doHostIConSendStrngFltParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngNoParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngIntParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngFltParm(CQMsg *pqmsg);

    short HostIConProcCmd(const char msgBuf[], int subCmdFlag);
    short HostIConProcCmdBase(const char msgBuf[], int subCmdFlag);
    void  HostIConSendStrngNoParm(int debugLevel, const char *strngPtr );
    void  HostIConSendStrngIntParm(int debugLevel, const char *strngPtr, int intParam );
    void  HostIConSendStrngFltParm(int debugLevel, const char *strngPtr, float floatParam );

    void ReadFits2D(const char *filename, int height, int width, int dataType, void *data);

    CHostIConListener *m_pHostIConListener;
    struct sockaddr_in  m_address;

    // Make certain the following get initialized to 0.0.  TODO, looks like m_tmpDatag is no longer used, delete
    float m_tmpData[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP]; 
    float m_tmpDatag[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];  

    // "ver" command variables
    CUpdateString m_verSRT;
    CUpdateString m_verHRT;

    CUpdateInt    m_telem;
    CUpdateInt    m_trate;
    CUpdateInt    m_debug;
    CUpdateInt    m_sdebug;
    CUpdateInt    m_gsmode;

    ////////////////////////////////////////////////
    //
    // This is a simple class used to trigger events
    // after short time intervals.
    //
    class CTriggerThread : public CThread
    {
    public:
	CTriggerThread();
	CTriggerThread(CCentral *pMaster);
	~CTriggerThread();
	
	int StopThread();
	
	static void* connector(void *thisPointer);
	
    protected:
	int Initialize();
	int TriggerLoop();
	CCentral *m_pMaster;
	CMutex m_LoopMutex;
	bool m_bLoop;
    };

    CTriggerThread *m_pTriggerThread;

};



extern CCondVarMutex    *g_pAllDone;
extern CCentral         *g_pCCentral;

extern int g_rtl_system; // variable version of RTL_SYSTEM compilation flag so all code can be compiled.

#endif

