/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoSrtDumpRawCam.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to dump raw camera frames. 
 *
 *	@b DESCRIPTION:
 *	This component contains code to dump raw camera frames to disk.  The advantage
 *	of this standalone program, over the AOC's dumpData command, is that it dumps 
 *	all 128x128 pixels of the AO system's wfs ROI (including guardbands), whereas the 
 *	dumpData command doesn't dump guardbands.  The disadvantage is that is is limited
 *	to 10 frames per second.  
 *	@par
 *	Note that this program does not do any pre-processing of the data.  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 3/28/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c dumpWriteFits()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

// mrr
// #include <gpUtGeneral.h>
// #include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr


#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#include "fitsio.h"


// Configuration Variables

#define FRAMES_PER_SECOND  10   // 10 frames per second
#define DATA_SAMPLE_FREQ   100  // sample for new data at 100 Hz
#define MAX_FRAMES_TO_DUMP 100  // 100 frames is 10 seconds

static const long secDelay = 0;			   					   // 0 secs
static const long nsecDelay = 1000000000L / DATA_SAMPLE_FREQ;  // 10 msecs

static const char *dataDir = DATA_DIR;


// Global Variables


// Local Variables

static boolean quit = false;  // quit flag

static int mmfd = -1;								  // mmap file descriptor
static unsigned char *startPtr = (unsigned char *)0;  // pointer to start of shared memory
static long long totalSizeExceptRecon;				  // total size of shared memory

static FileDataShmType*			fileDataShmPtr;		 // pointer to file data shared memory
static CamDataType*		 		camDataPtr;			 // pointer to camera shared memory data
static CentDataType*			centDataPtr;		 // pointer to centroid shared memory data
static HrtDataFlowControlType*	dataFlowControlPtr;	 // pointer to Data Flow Control variables


// Local Function Prototypes

static void dumpWriteFits(char *filename, int nData, 
						  unsigned short data[][NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL], boolean overwriteFile);
static void termHandler(int sigNum);


int main(int argc, char *argv[])
{
	boolean 	   overwriteFile, newData;
	char		   fname[128];
	int			   i, j;
	int			   crntReadBuffNdx = -1, watchdogTimer = 0;
	int			   nFrames, frameNdx;
	size_t  	   shmDataSize, camDataSize, centDataSize, dataControlSize;
	struct		   timespec next, remaining;
	unsigned short tmpRaw[MAX_FRAMES_TO_DUMP][NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL];

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	set defaults and then parse command line
	nFrames = FRAMES_PER_SECOND;
	strcpy(fname, "dumprawData");
	overwriteFile = false;
    --argc;
    ++argv;
    while (argc && (argv[0][0] == '-'))
    {
		switch (argv[0][1])
		{
			case 'n':		// number of frames
			    ++argv;
			    --argc;
			    nFrames = atoi(argv[0]);
			    break;

			case 'f':		// save filename
			    ++argv;
			    --argc;
			    strcpy(fname, argv[0]);
			    break;

			case 'o':		// overwrite file
			    overwriteFile = true;
			    break;

			case '?':
			case 'h':
			    printf("\nDump raw wfs camera data for GPI wfs\n");
			    printf("usage:\n");
			    printf("  -n numFrames  dump numFrames frames\n");
			    printf("  -f fileName   output to fileName file (don't include .fits extension)\n");
			    printf("  -o            overwrite fileName, if it exists\n");
			    printf("  -h            print this help message\n");
			    exit(0);
			    break;

			default:
			    printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			    break;
		}
		argc--;
		argv++;
    }
	if (nFrames > MAX_FRAMES_TO_DUMP)
	{
		nFrames = MAX_FRAMES_TO_DUMP;
		printf("gpAoDumpRawCam: limiting number of frames to: %d\n", MAX_FRAMES_TO_DUMP);
	}

//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	centDataSize = sizeof(CentDataType);
	dataControlSize = sizeof(HrtDataFlowControlType);

	totalSizeExceptRecon = (long long)shmDataSize + camDataSize + centDataSize + dataControlSize;
	printf("gpAoDumpRawCam: Attempting to open shared memory:: Size = %lld / 0x%llx\n", totalSizeExceptRecon, totalSizeExceptRecon );
	printf("gpAoDumpRawCam: shmDataSize: %lx, camDataSize: %lx, centDataSize: %lx, dataControlSize: %lx\n",
			shmDataSize, camDataSize, centDataSize, dataControlSize);

//	open the shared memory region
//	IMPORTANT: reconData is no longer included in AOC_mmap -- dispRaw doesn't need reconData, so the pointer to
//	> it is not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap", O_RDWR)) < 0))
	{
		printf("gpAoDumpRawCam: waiting for mmap open: /dev/AOC_mmap\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoDumpRawCam: waiting for mmap open: ./AOC_mmap\n");
		sleep(1);
	}
#endif

//	get the pointer to the shared memory region
	while (!quit && ((startPtr = mmap(0, totalSizeExceptRecon, PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoDumpRawCam: waiting for mmap: %llx\n", totalSizeExceptRecon);
		sleep(1);
	}
	printf( "gpAoDumpRawCam: Shm start Ptr = %p, totalSizeExceptRecon = 0x%llx\n", startPtr, totalSizeExceptRecon );

//	set up the pointers to the global variables
	fileDataShmPtr		= (FileDataShmType *)startPtr;
	camDataPtr			= (CamDataType *)((char *)fileDataShmPtr + shmDataSize);
	centDataPtr			= (CentDataType *)((char *)camDataPtr + camDataSize);
	dataFlowControlPtr	= (HrtDataFlowControlType *)((char *)centDataPtr + centDataSize);

//	enter a loop, sleeping for nsecDelay each time, until we've processed all frames or a signal 
//	> is received (presumably a terminate)
	frameNdx = -1;  // skip the first frame, in case crntReadBuffNdx is about to switch
	while ((frameNdx < nFrames) && !quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

//		see if we have new data
		newData = false;
		if (crntReadBuffNdx != camDataPtr->rawDispReadBuffNdx)
		{
			crntReadBuffNdx = camDataPtr->rawDispReadBuffNdx;
			if (frameNdx < 0)  // skip the first frame, in case crntReadBuffNdx is about to switch
				frameNdx++;
			else
				newData = true;
			watchdogTimer = 0;
		}
		else if (++watchdogTimer > DATA_SAMPLE_FREQ)
		{
			printf("gpAoDumpRawCam: watchdog timed out -- no new data for > a second\n");
			quit = true;
		}

//		if we have new data, move it out of the rawDispBuffs[][][] quickly
		if (newData)
		{
			for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
			{
				for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
						tmpRaw[frameNdx][i][j] = camDataPtr->rawDispBuffs[crntReadBuffNdx][i][j];
			}
			if ((frameNdx % 5) == 0)  // indicate that something is happening
			{
				printf(".");
				fflush(stdout);
			}
			frameNdx++;
		}
	}
	printf("\n");

//	if we're not quitting, store the data that's been collected
	if (!quit)
		dumpWriteFits(fname, nFrames, tmpRaw, overwriteFile);

//	finally, unmap and close the shared memory, if it's been opened
	if (mmfd > 0)
	{
		munmap(startPtr, totalSizeExceptRecon);
	  	close(mmfd);
	}

	printf("gpAoDumpRawCam: dumpraw done\n");
}


static void dumpWriteFits(char *filename, int nData, 
						  unsigned short data[][NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL], boolean overwriteFile)
{
	fitsfile  *fptr = NULL;
	char	   fullFilename[128], fitsFilename[128];
	char	   crntDateStr[32] = "nodate";  // current date string
	int		   status = 0;
	long	   dataAxes[3], fpixel[3];
	struct tm *tmPtr;
	time_t	   now, nowMinus10Hours;

	if (filename[0] == '/')
	{
		strcpy(fullFilename, filename);  // if the filename starts with a '/', assume it's a full path name
		strcat(fullFilename, ".fits");   // except, without fits extension
	}
	else
	{
//		get the date/time, if possible
		now = time(NULL);
		if (now == (time_t)-1)
			printf("dumpWriteFits: error getting date/time 1\n");
		else
		{
			nowMinus10Hours = now - (time_t)36000;  // change the date at 10:00 AM (not midnight)
			if( (tmPtr = localtime( &nowMinus10Hours ) ) == NULL)
				printf("dumpWriteFits: error getting date/time 2\n");
			else
				sprintf( crntDateStr, "%02d%02d%02d", tmPtr->tm_year - 100, tmPtr->tm_mon + 1, tmPtr->tm_mday);
		}

//		construct the date directory path and then the 'full' filename
		sprintf(fullFilename, "%s%s/", dataDir, crntDateStr);
		mkdir(fullFilename, (mode_t)0777);
		strcat(fullFilename, filename);
		strcat(fullFilename, ".fits");
	}
	printf("dumpWriteFits: saving file: %s\n", fullFilename);

//	open the file, overwriting it if so requested
	if (overwriteFile)
	{
		strcpy(fitsFilename, "!");  // make sure the filename begins with '!' so fitsio overwrite the existing file
		strcat(fitsFilename, fullFilename);
		if (fits_create_file(&fptr, fitsFilename, &status))
			printf("dumpWriteFits: error (%d) opening file: %s\n", status, fullFilename);
	}
	else
	{
		if (fits_create_file(&fptr, fullFilename, &status))
			printf("dumpWriteFits: error (%d) opening file -- may already exist (use -o): %s\n", status, fullFilename);
	}

//	create an image, write header keywords, write out the data, and close the file
	if (fptr != NULL)
	{
		dataAxes[0] = NUM_PIX_IN_ROW_ACTUAL;
		dataAxes[1] = NUM_ROWS_IN_CAM_ACTUAL;
		dataAxes[2] = nData;
		fpixel[0] = 1;
		fpixel[1] = 1;
		fpixel[2] = 1;
		if (fits_create_img(fptr, SHORT_IMG, 3, dataAxes, &status))
			printf("dumpWriteFits: error (%d) creating image: %s\n", status, fullFilename);
		else if (fits_write_date(fptr, &status))
			printf("dumpWriteFits: error (%d) writing date: %s\n", status, fullFilename);
		else if (fits_write_comment(fptr, "GPI dumpraw data", &status))
			printf("dumpWriteFits: error (%d) writing comment: %s\n", status, fullFilename);
		else if (fits_write_pix(fptr, TSHORT, fpixel, nData * NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL, data, &status))
			printf("dumpWriteFits: error (%d) writing pixel data: %s\n", status, fullFilename);
		if (fits_close_file(fptr, &status))
			printf("dumpWriteFits: error (%d) closing file: %s\n", status, fullFilename);
	}
}


static void termHandler(int sigNum)
{
	printf("dumpraw quitting due to OS term: %d\n", sigNum);
	quit = true;
}
