/*******************************************************************************
 *	COPYRIGHT (C) 2008
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 ******************************************************************************/
/*!
 *	@file gpAoHrtSysCon.c
 *
 * 	@version  $Id: gpAoHrtSysCon.c,v 1.1 2012/10/26 16:31:41 jgates Exp $
 * 	
 *	@brief 
 *	System Controller for the Hard-Real-Time (HRT) components.  Provides: Startup-init,
 *	Closedown-Cleanup, Command-Status interface to the SRT system.
 *
 *	@b DESCRIPTION:
 *	This component is the Hard Real Time (Hrt) system controller for the AOC AO control system.
 *	It performs the RTLinux module initialization and cleanup, semaphore initialization, 
 *	and provides the task that waits for commands from the soft realtime processor (that communicates
 *	and translates command and status to and from the host) and dispatches them to other components as
 *	appropriate.  Virtually all HRT system initialization is done here to simplify cleanup.
 *
 *	The gpAoHrtSysCon thread is also included in this component.  Its purpose is to wait on the command FIFO 
 *	from the Soft Real Time (SRT) side and dispatch commands as they are received.
 *
 *	@b AUTHOR:
 *	DWP & SMJ	DATE: 12/17/2008
 *
 *	@b FUNCTION NAMES:
 *	-# @c initHRT() or main()
 *	-# @c cleanupHRT()
 *	-# @c gpAoHrtSysCon_GlobalsInit()
 *	-# @c gpAoHrtSysCon_GlobalsCleanup()
 *	-# @c gpAoHrtSysCon()
 *	-# @c SysConSendStrngNoParm()
 *	-# @c SysConSendStrngIntParm()
 *	-# @c SysConSendStrngFltParm()
 *	-# @c SysConSendStrngStrParm()
 *
 *	@b REVISIONS:
 *
 ********************************************************************************/

#if (RTL_SYSTEM != 0)
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <math.h>
#else
#define _GNU_SOURCE
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#include <math.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <error.h>
#include <sys/mman.h>
#include <unistd.h>
#include <limits.h>
#include <sys/time.h>
#include <linux/unistd.h> 
#endif

#if (RTL_SYSTEM == 0)
// mrr #include <gpUtGeneral.h>
// mrr #include "gpAoGmb.h"
#include "gpUtGmbHrt.h"	// mrr
#else
#include "gpUtGmbHrt.h"
#endif

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"
#if ( RTL_SYSTEM == 0 )
#include "gpAoSrtDpio2Drivers.h"
#else
#include "gpAoHrtDpio2Drivers.h"
#endif

#if (CAM_TYPE == WFS_REAL_GPI)
#if (RTL_SYSTEM != 0)
typedef unsigned char		u_char;
typedef unsigned short		u_short;
typedef unsigned int		u_int;
typedef long long			__kernel_loff_t;
typedef __kernel_loff_t		loff_t;
#include "edt_lnx_kernel_Local.h"
#endif
#endif

#define PRINTF_SEND_STRNGS_SC 0  // use this to turn off printfs for strings being sent to the SRT (for normal operation)	// mrr
// #define PRINTF_SEND_STRNGS_SC 1  // use this to turn on printfs for strings being sent to the SRT (for testing)

// Global Variables
char					OkayToFinishInit = false;		// flag indicating that it is okay for threads to complete their initializations

rtl_sem_t				WFSSem;							// semaphore for SciMeasure camera thread
rtl_sem_t				CentSem;						// semaphore for centroid thread
rtl_sem_t				ReconSem;						// semaphore for recon thread

long					CentDataAvailFlag = 0;			// used to alert cent and recon threads that data is available because
long					ReconDataAvailFlag = 0;			// > CentSem and ReconSem are too slow and non-deterministic

rtl_sem_t				FftwPlanSem;					// semaphore for fftw plan mutual exclusion

int						DiagFIFOfd;						// fd for FIFO used to send messages to the diagnostic data handler
int						CmdFIFOfd;						// fd for FIFO used to receive command messages from the host interface

int						hrtSystemDebugLevel = 0;

FileDataShmType*		hrtFileDataShmPtr;				// pointer to file data shared memory
CamDataType*			hrtCamDataPtr;					// pointer to camera shared memory data
CentDataType*			hrtCentDataPtr;					// pointer to centroid shared memory data
ReconDataType*			hrtReconDataPtr;				// pointer to recon shared memory data
HrtDataFlowControlType*	hrtDataFlowControlPtr;			// pointer to Data Flow Control variables

// initalization flags for threads
char					CamWFSInitDone = false;
char					CentInitDone = false;
char					ReconInitDone = false;

// thread quit flags
boolean 				QuitCamWFSWatchThread = false;
boolean 				QuitCamera_WFS_Thread = false;
boolean 				QuitCentroidThread = false;
boolean 				QuitReconThread = false;
boolean 				QuitSysConThread = false;

#if ( RTL_SYSTEM != 0 )
#include "gpAoCmdTable.h"  // the command table is in a separate file so that both the HRT and SRT programs can include it
#endif

// Local Variables
static rtl_pthread_t	camWFSWatchThread;
static rtl_pthread_t	camera_WFS_Thread;
static rtl_pthread_t	centroidThread;
static rtl_pthread_t	reconThread;
static rtl_pthread_t	sysConThread;

static int				mmfd, mmfd2;		   				  // mmap file descriptors
static long long		totalSizeExceptRecon, reconDataSize;  // total size of shared memory and recon size
static unsigned char	*startPtr, *startPtr2;				  // pointer to start of shared memory

static char FTR_wfrCmFilenameSave[MAX_FILENAME_LENGTH] = "";

// Local Function Prototypes
	   void		cleanupHRT(void);
static int		gpAoHrtSysCon_GlobalsInit(void);
static void		gpAoHrtSysCon_GlobalsCleanup(void);
static void*	gpAoHrtSysCon( void *arg );

/*!
 *******************************************************************************
 *	@fn main( void )
 *
 *	@brief
 *	Mainline for AOC Hard Real Time (HRT) module, when in RTLinux mode.  Performs
 *	initialization for the HRT when in user-side mode.  
 *
 *	@b DESCRIPTION:
 *	This unit is the mainline for AOC Hard Real Time (HRT) module, when in RTLinux 
 *	mode.  It performs initialization for the HRT when in user-side mode.  Shared 
 *	memory is allocated, threads and semaphores are created, etc.  
 *	@par
 *
 *	IMPORTANT: this unit needs to be called main() when in RTLinux mode.  In user-side
 *	mode, it is called initHRT().  
 *	@par
 *
 *	@b Usage: 
 *	retVal = initHRT();  (when in user-side mode)
 * 
 *	@b Param None.
 *
 *	@return 
 *	(int) 0 = success
 *	(int) !0 = error codes as set by system calls
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
#if ( RTL_SYSTEM == 0 )
int 
initHRT(void)
#else
int 
main(void)
#endif
{
int retVal;
rtl_pthread_attr_t attr;
#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))	// mrr 4K
int dpio2Errno;
#endif
struct rtl_sched_param param;

	// can't send this message to the host because the FIFO isn't set up yet
	printf ( "\n================\nStarting hard real-time process ==== Ver 1.1 ====\n================\n" );  
	
	rtl_printf ( "<0>\n");
	rtl_printf ( "<0>\n* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n" );
	rtl_printf ( "<0>\t 3m HRT SW Version \t%s\t%s\n", HRT_SW_VERSION, HRT_SW_DATE );
	rtl_printf ( "<0>\t 3m SRT SW Version \t%s\t%s\n", SRT_SW_VERSION, SRT_SW_DATE );
	rtl_printf ( "<0>* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *\n\n" );

	printf( "\n  Syscon: Starting Globals Init\n");	// mrr DM

	//	initialize pointers to global/shared variables
	if ((retVal = gpAoHrtSysCon_GlobalsInit()) !=  0)
	{
		printf( " Syscon: **** Global Init ERROR\n");	// mrr DM
		return retVal;  // error message has already been displayed
	}	

	printf( "    Syscon: Ending Globals Init\n");	// mrr DM

	//	if either dpio2 DM is to be used, init them both (which means that both dpio2 boards need to be in the
	//	> AOC; but both DMs don't necessarily need to be connected to the dpio2 boards)
	//	> IMPORTANT: because memory is allocated in the dpio2 init functions, they MUST be executed in this context
#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))	// mrr 4K
	//	initialize the dpio2 drivers to control the GPI woofer (including the T/T stage) and the GPI
	//	> tweeter -- there are 2 dpio2 boards (one per DM); but, as would be expected, both boards use 
	//	> with the same driver software

	printf( "  Syscon: Calling dpio2 Init\n");	// mrr DM

#if ( RTL_SYSTEM == 0 )
	dpio2Errno = gpAoSrtDpio2Init();
#else
	dpio2Errno = gpAoHrtDpio2Init();
#endif
	if (dpio2Errno < 0)
	{
		// 		can't send this message to the host because the FIFO isn't set up yet
		printf( "Syscon: ERR (initHRT): dpio2 driver init failed - return = %d\n", dpio2Errno );
	}
#endif

	printf( "  *Syscon: Returning from dpio2 Init\n");	// mrr DM


	printf( "  Syscon: Starting 	gpAoHrtFileData_Init()\n");	// mrr DM

	//	initalize the file shared memory variables
	gpAoHrtFileData_Init();

	printf( "  *Syscon: ending 	gpAoHrtFileData_Init()\n");	// mrr DM

	//	init the hrtQuitFlag in shared memory and the srtIsUpFlag in preparation for spinning on it
	hrtFileDataShmPtr->hrtQuitFlag = false;
	hrtFileDataShmPtr->srtIsUpFlag = false;

	printf( "  Syscon: Opening FIFO's\n");	// mrr DM
	//	open FIFOs for HRT/SRT communication, returning errno if an error occurs (the SRT will spin on the command FIFO
	//	> until it is opened)
#if ( RTL_SYSTEM != 0 )
	if ((DiagFIFOfd = rtl_open("/dev/rtf0", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_WRONLY)) < 0)
#else
	if ((DiagFIFOfd = rtl_open(RTF0_FNAME, RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDWR, S_IRUSR | S_IWUSR)) < 0)
#endif
	{
	// 	can't send this message to the host because the FIFO setup failed
		printf( "Syscon: ERR (initHRT) creating diagnostic output FIFO, errno = %d\n", rtl_errno);
		return rtl_errno;
	}
#if ( RTL_SYSTEM != 0 )
	if ((CmdFIFOfd = rtl_open("/dev/rtf1", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY)) < 0)
#else
	if ((CmdFIFOfd = rtl_open(RTF1_FNAME, RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY, S_IRUSR | S_IWUSR)) < 0)
#endif
	{
	// 	can't send this message to the host because the FIFO setup failed
		printf( "Syscon: ERR (initHRT) creating diagnostic output FIFO, errno = %d\n", rtl_errno);
		return rtl_errno;
	}
	printf( "  *Syscon: Ending FIFO's\n");	// mrr DM

	printf( "  Syscon: Creating Semaphores's\n");	// mrr DM

	//	create semaphores, returning errno if an error occurs
	if (rtl_sem_init(&WFSSem, 1, 0) < 0)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR creating WFSSem, errno = ", rtl_errno);
		return rtl_errno;
	}
	if (rtl_sem_init(&CentSem, 1, 0) < 0)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR creating CentSem, errno = ", rtl_errno);
		return rtl_errno;
	}
	if (rtl_sem_init(&ReconSem, 1, 0) < 0)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR creating ReconSem, errno = ", rtl_errno);
		return rtl_errno;
	}
	if(rtl_sem_init(&FftwPlanSem, 1, 1) < 0)  // create this one unlocked (third parameter set to 1)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR creating FftwPlanSem semaphore, errno = ", rtl_errno);
		return rtl_errno;
	}
	printf( "  *Syscon: Ending Semaphores's\n");	// mrr DM

	printf( "  Syscon: Starting to create threads\n");	// mrr DM

	//	create threads, returning the value returned by pthread_create() if an error occurs
	//	> (note that, in the following, higher numbers mean higher priorities)
	// camWFSWatchThread
	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 5;
	rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
	rtl_pthread_attr_setfp_np(&attr, 1);
	if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_CAM) != 0 )
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Assignment failed: ", HRT_CPU_CAM );
	//	rtl_pthread_attr_setreserve_np(&attr, 1);  // only the last thread using a processor can reserve it -- so, gpAoHrtCam does it
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
	if ((retVal = rtl_pthread_create(&camWFSWatchThread, &attr, gpAoHrtCam_DrvrWatch, 0)) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT) creating camWFSWatch thread, retVal = ", retVal);
 	rtl_pthread_attr_destroy(&attr);

	//	camera_WFS_Thread
	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 10;  // 3;
	rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
	rtl_pthread_attr_setfp_np(&attr, 1);
	if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_CAM) != 0 )
	{  												//dwp121020
		printf("WARN (initHRT) CPU Assignment failed for CPU #%d", HRT_CPU_CAM );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Assignment failed: ", HRT_CPU_CAM );
	}
	if( rtl_pthread_attr_setreserve_np(&attr, 1) != 0 )  //dwp121019 -- put this back in -- the regression test for it seems to work now -- but no help
	{  												//dwp121020
		printf("WARN (initHRT) CPU Reserve failed for CPU #%d", HRT_CPU_CAM );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Reserve failed: ", HRT_CPU_CAM );
	}
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
	if ((retVal = rtl_pthread_create(&camera_WFS_Thread, &attr, gpAoHrtCam, 0)) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT) creating WFS camera thread, retVal = ", retVal);
 	rtl_pthread_attr_destroy(&attr);

	// centroidThread
	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 13;  // 4;
	rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
	rtl_pthread_attr_setfp_np(&attr, 1);
	if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_CENT) != 0 )
	{  												//dwp121020
		printf("WARN (initHRT) CPU Assignment failed for CPU #%d", HRT_CPU_CENT );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Assignment failed: ", HRT_CPU_CENT );
	}
	if( rtl_pthread_attr_setreserve_np(&attr, 1) != 0 )  //dwp121019 -- put this back in -- the regression test for it seems to work now -- but no help
	{  												//dwp121020
		printf("WARN (initHRT) CPU Reserve failed for CPU #%d", HRT_CPU_CENT );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Reserve failed: ", HRT_CPU_CENT );
	}
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
	if ((retVal = rtl_pthread_create(&centroidThread, &attr, gpAoHrtCent, 0)) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT) creating centroid thread, retVal = ", retVal);
 	rtl_pthread_attr_destroy(&attr);

	//	Reconstruction initialization
	//	Memory initialization
	gpAoHrtRecon_InitMemory();

	//	Init main reconstruction thread
	// reconThread
	printf("    Syscon: starting Recon thread on CPU %d\n", HRT_CPU_RECON);	// mrr DM  //dwp121020
	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 15;  // 2;
	rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
	rtl_pthread_attr_setfp_np(&attr, 1);
	if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_RECON) != 0 )
	{  												//dwp121020
		printf("WARN (initHRT) CPU Assignment failed for CPU #%d", HRT_CPU_RECON );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Assignment failed: ", HRT_CPU_CENT );
	}
	if( rtl_pthread_attr_setreserve_np(&attr, 1) != 0 )  //dwp121019 -- put this back in -- the regression test for it seems to work now -- but no help
	{  												//dwp121020
		printf("WARN (initHRT) CPU Reserve failed for CPU #%d", HRT_CPU_RECON );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Reserve failed: ", HRT_CPU_CENT );
	}
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
	if ((retVal = rtl_pthread_create(&reconThread, &attr, gpAoHrtRecon, 0)) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT) creating recon thread, retVal = ", retVal);
 	rtl_pthread_attr_destroy(&attr);

	//	Init reconstruction worker threads
	// worker Threads
	gpAoHrtRecon_InitThreading();

	// sysConThread
	printf("    Syscon: starting Syscon thread on CPU %d\n", HRT_CPU_SYSCON);	// mrr DM  //dwp121020
	rtl_pthread_attr_init(&attr);
	rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 1;
	rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
	rtl_pthread_attr_setfp_np(&attr, 1);
	if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_SYSCON) != 0 )
	{  												//dwp121020
		printf("WARN (initHRT) CPU Assignment failed for CPU #%d", HRT_CPU_SYSCON );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Assignment failed: ", HRT_CPU_CENT );
	}
	if( rtl_pthread_attr_setreserve_np(&attr, 1) != 0 )  //dwp121019 -- put this back in -- the regression test for it seems to work now -- but no help
	{  												//dwp121020
		printf("WARN (initHRT) CPU Reserve failed for CPU #%d", HRT_CPU_SYSCON );  //dwp121020
		SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT) CPU Reserve failed: ", HRT_CPU_CENT );
	}
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
	if ((retVal = rtl_pthread_create(&sysConThread, &attr, gpAoHrtSysCon, 0)) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT) creating sysCon thread, retVal = ", retVal);
 	rtl_pthread_attr_destroy(&attr);

	printf( "  *Syscon: Ending create threads\n");	// mrr DM

	printf( "  Syscon: Starting to spin on hrtFileDataShmPtr->srtIsUpFlag\n");	// mrr DM

	//	initialize the wfs camera interrupt (must be done from this context -- so 
	//	> spin on the srtIsUpFlag first)
#if ( RTL_SYSTEM != 0 )
	while (!hrtFileDataShmPtr->srtIsUpFlag)
		usleep(100000);  // can't use rtl_nanosleep() in user-space
#endif
	gpAoHrtCam_IntrInit();

	printf( "  *Syscon: Ending spinning on hrtFileDataShmPtr->srtIsUpFlag, waiting to quit\n");	// mrr DM


#if ( RTL_SYSTEM != 0 )
	while (!hrtFileDataShmPtr->hrtQuitFlag)
		usleep(100000);  // can't use rtl_nanosleep() in user-space

	printf( "\n    ****  Syscon Main has quit\n");	// mrr DM

		cleanupHRT();
	rtl_printf ( "\n================\n     syscon Main has quit and finished Cleanup     \n================\n" );  
#endif

	printf ( "\n================\n     syscon Main has quit and finished Cleanup     \n================\n" );
	
	return 0;
}

/*!
 *******************************************************************************
 *	@fn cleanupHRT( void )
 *
 *	@brief
 *	Cleanup and dispose of variables for HRT system control.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to perform RTLinux cleanup for the AOC Hard Real- 
 *	Time (HRT) module ~ Threads and semaphores are closed and deallocated.
 *	@par
 *
 *	@b Usage: cleanupHRT()
 * 
 *	@b Param None.
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
void 
cleanupHRT(void)
{
	SysConSendStrngNoParm(eDebugLevelMin, "INFO (cleanupHRT): Cleaning up hard real-time process");

	//	clean up the dpio2 DM drivers
#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))	// mrr 4K
#if ( RTL_SYSTEM == 0 )
	gpAoSrtDpio2Cleanup();
#else
	gpAoHrtDpio2Cleanup();
#endif
#endif

	//	free up the EDT irq
#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
	if (WFS_irq >= 0)
	{
		edt_restore_os_interrupt(WFS_irq, WFS_dev);
		rtl_free_irq (WFS_irq);
	}
#endif

	//	tell threads to quit
	usleep(1000);  // wait a millisec -- can't use rtl_nanosleep() in user-space
	QuitCamWFSWatchThread = true;
	usleep(1000);  // wait a millisec -- can't use rtl_nanosleep() in user-space
	QuitCamera_WFS_Thread = true;
	usleep(1000);  // wait a millisec -- can't use rtl_nanosleep() in user-space
	QuitCentroidThread = true;
	usleep(1000);  // wait a millisec -- can't use rtl_nanosleep() in user-space
	QuitReconThread = true;
	usleep(1000);  // wait a millisec -- can't use rtl_nanosleep() in user-space
	QuitSysConThread = true;
	usleep(600000);  // wait 0.6 seconds (SysCon has a long delay) -- can't use rtl_nanosleep() in user-space

	gpAoHrtRecon_CleanThreading();

	//	destroy semaphores
	rtl_sem_destroy(&WFSSem);
	rtl_sem_destroy(&CentSem);
	rtl_sem_destroy(&ReconSem);
	rtl_sem_destroy(&FftwPlanSem);

	//	close FIFOs
	rtl_close( DiagFIFOfd );
	rtl_close( CmdFIFOfd );

	//	clean recon memory
	gpAoHrtRecon_CleanMemory();

	//	clean up shared memory
	gpAoHrtSysCon_GlobalsCleanup();
}

/*!
 *******************************************************************************
 *	@fn gpAoHrtSysCon_GlobalsInit( void )
 *
 *	@brief
 *	This unit sets up the mmap for and initializes pointers to global/shared variables  
 *	Initialize global variables for HRT system control.
 *
 *	@b DESCRIPTION:
 *	Pointers to virtually all global memory variables are set up here so that they can all be 
 *	part of the same mmap region to facilitate sharing the variables between several threads on 
 *	RTLinux and Linux on multiple processors.  Global memory is used in this application so that 
 *	large chunks of data will not have to be moved around (in most cases, data ia put in memory 
 *	once and never moved again).
 *	
 *	IMPORTANT: pointers to variables are set up here, but the variables are NOT initialized.  That
 *	is up to the code that is responsible for the data.  GlobalsInit() must be called before any
 *	tasks are initialized.
 *
 *	@par
 *
 *	@b Usage: 
 *	retVal = gpAoHrtSysCon_GlobalsInit();
 * 
 *	@b Param None.
 *
 *	@return 
 *	(int) 0 = success
 *	(int) !0 = failure - system error codes are returned.
 * 
 *	@exception 
 *	None.
 *
 *	@see
 *	gpAoHrtSysCon_GlobalsCleanup
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************/
static int 
gpAoHrtSysCon_GlobalsInit(void)
{
size_t shmDataSize, camDataSize, centDataSize, hrtDataControlSize;

	//	determine sizes of global variables
	shmDataSize = sizeof(FileDataShmType);
	camDataSize = sizeof(CamDataType);
	centDataSize = sizeof(CentDataType);
	hrtDataControlSize = sizeof(HrtDataFlowControlType);
	reconDataSize = sizeof(ReconDataType);

	totalSizeExceptRecon = shmDataSize + camDataSize + centDataSize + hrtDataControlSize;

	printf("INFO (gpAoHrtSysCon_GlobalsInit) Attempting to open shared memory; size: %lld\n", totalSizeExceptRecon );  // can't send this error to the host!
	printf("INFO (gpAoHrtSysCon_GlobalsInit) Attempting to open shared memory 2; size: %lld\n", reconDataSize );  // can't send this error to the host!

	//	create the shared memory region, returning errno if an error occurs (passing a non-zero 
	//	> value for the mode means a node in Linux is also created (as opposed to just RTLinux))
#if ( RTL_SYSTEM != 0 )
	if ((mmfd = rtl_shm_open( "/dev/AOC_mmap", RTL_O_CREAT, 0666)) < 0)
		printf("ERR Creating shared mem on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
	else if ((mmfd2 = rtl_shm_open( "/dev/AOC_mmap2", RTL_O_CREAT, 0666)) < 0)
		printf("ERR Creating shared mem 2 on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
#else
	if ((mmfd = rtl_shm_open( "./AOC_mmap", O_RDWR | RTL_O_CREAT, S_IRUSR | S_IWUSR)) < 0)
		printf("ERR Creating shared mem on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
	else if ((mmfd2 = rtl_shm_open( "./AOC_mmap2", O_RDWR | RTL_O_CREAT, S_IRUSR | S_IWUSR)) < 0)
		printf("ERR Creating shared mem 2 on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
#endif

	//	size the shared memory region, returning errno if an error occurs
	if (rtl_ftruncate(mmfd, totalSizeExceptRecon) < 0)
	{
		printf("ERR (gpAoHrtSysCon_GlobalsInit): Error sizing shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}
	else if (rtl_ftruncate(mmfd2, reconDataSize) < 0)
	{
		printf("ERR (gpAoHrtSysCon_GlobalsInit): Error sizing shared mem 2, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}

	//	get the pointer to the shared memory region, returning errno if an error occurs
	if ((startPtr = rtl_mmap(0, totalSizeExceptRecon, RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, mmfd, 0)) == RTL_MAP_FAILED)
	{
		printf("ERR (gpAoHrtSysCon_GlobalsInit): Error getting pointer to shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}
	if ((startPtr2 = rtl_mmap(0, reconDataSize, RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, mmfd2, 0)) == RTL_MAP_FAILED)
	{
		printf("ERR (gpAoHrtSysCon_GlobalsInit): Error getting pointer to shared mem 2, errno = %d\n", rtl_errno);  // can't send this error to the host!
		return rtl_errno;
	}
	printf("INFO (gpAoHrtSysCon_GlobalsInit) shared memory allocation for HRT is okay\n");  // can't send this error to the host!

	//	set up the pointers to the global variables
	hrtFileDataShmPtr		= (FileDataShmType *)startPtr;
	hrtCamDataPtr			= (CamDataType *)((char *)hrtFileDataShmPtr + shmDataSize);
	hrtCentDataPtr			= (CentDataType *)((char *)hrtCamDataPtr + camDataSize);
	hrtDataFlowControlPtr	= (HrtDataFlowControlType *)((char *)hrtCentDataPtr + centDataSize);
	hrtReconDataPtr			= (ReconDataType *)startPtr2;

	printf( "INFO (gpAoHrtSysCon_GlobalsInit): Start Ptr = %p, totalSizeExceptRecon = 0x%llx\n", startPtr, totalSizeExceptRecon );  // can't send this error to the host!
	printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of FileData = 0x%lx, ptr = %p\n", sizeof(FileDataShmType), hrtFileDataShmPtr);  // can't send this error to the host!
	printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of CamData = 0x%lx, ptr = %p\n", sizeof(CamDataType), hrtCamDataPtr);  // can't send this error to the host!
	printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of CentData = 0x%lx, ptr = %p\n", sizeof(CentDataType), hrtCentDataPtr);  // can't send this error to the host!
	printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of ReconData = 0x%lx, ptr = %p\n", sizeof(ReconDataType), hrtReconDataPtr);  // can't send this error to the host!
	
	return 0;
}

/*!
 *******************************************************************************
 *	@fn gpAoHrtSysCon_GlobalsCleanup( void )
 *
 *	@brief
 *	This unit cleans up the mmap for the initialized pointers to global/shared variables
 *	for HRT system control.
 *
 *	@b DESCRIPTION:
 *	Pointers to virtually all global memory variables will be deallocated and removed here. This
 *	is a reversal of the process done in 'gpAoHrtSysCon_GlobalsInit' HOWEVER, the associated
 *	memory is still allocated when this is called, it will be deallocated, if at all possible.
 *	@par
 *	
 *	IMPORTANT: This code WILL deallocate memory
 *	@par
 *
 *	@b Usage: 
 *	gpAoHrtSysCon_GlobalsCleanup();
 * 
 *	@b Param None.
 *
 *	@return 
 *	None.
 * 
 *	@exception 
 *	None.
 *
 *	@see
 *	gpAoHrtSysCon_GlobalsInit
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	gpAoHrtSysCon_GlobalsInit() must have been called.
 *
 *	@callgraph
 ********************************************************************************/
static void 
gpAoHrtSysCon_GlobalsCleanup(void)
{
//	unmap, close, and destroy the global memory regions
	rtl_munmap(startPtr, totalSizeExceptRecon);
	rtl_close(mmfd);
	rtl_munmap(startPtr2, reconDataSize);
	rtl_close(mmfd2);

#if ( RTL_SYSTEM != 0 )
	rtl_shm_unlink( "/dev/AOC_mmap" );
	rtl_shm_unlink( "/dev/AOC_mmap2" );
#else
	rtl_shm_unlink( "./AOC_mmap" );
	rtl_shm_unlink( "./AOC_mmap2" );
#endif
}

/*!
 *******************************************************************************
 *	@fn gpAoHrtSysCon( void* dummy )
 *
 *	@brief
 *	System controller thread for the Hard Real Time (Hrt) side.  
 *
 *	@b DESCRIPTION:
 *	This unit is the system controller thread for the Hard Real Time (Hrt) side.  Its
 *	purpose is to wait on the command FIFO from the Soft Real Time (Srt) side and 
 *	dispatch commands as they are received.
 *	@par
 *	IMPORTANT:  RTLinux does not support blocking FIFOs.  So, the FIFO is polled.  
 *	@par
 *
 *	@b Usage: gpAoHrtSysCon( NULL );
 * 
 *	@param[in] dummy (void *):  Needed for pthread_create(), not currently used.
 *
 *	@return 
 *	0 :  always 0 (NULL); should never return
 * 
 *	@exception 
 *	None.
 *
 *	@see
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
static void *
gpAoHrtSysCon( void *arg )
{
int					retVal;
short				i, ii;
short				cmdAction;
struct rtl_timespec next;
cmdFIFOMsgType		cmdFIFOMsg;
static short		sysConCmdCrnt = eCmdNone;  // This is used to handle multi-step comamnds that require SRT side support

#if (RTL_SYSTEM == 0)
	cpu_set_t mask;
	int		  cpuNum = HRT_CPU_SYSCON;

	CPU_ZERO( &mask );
	CPU_SET( cpuNum, &mask);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) - CPU #",  cpuNum );
	if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) executing sched_setaffinity() for CPU #",  cpuNum );
	sched_getaffinity( 0, sizeof( mask ), &mask);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else
	printf ("      Syscon: Starting SysCon thread id: %d\n", rtl_pthread_self());	// mrr DM
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) SysCon thread id: ", rtl_pthread_self());
#endif

//	spin on the srtIsUpFlag in shared memory until the SRT is up
	while (!hrtFileDataShmPtr->srtIsUpFlag)
	{
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			printf("WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = %d\n", retVal);  // can't send this to the host!
	}
	printf("      Syscon: SRT is Up\n");	// mrr DM    

	//	command the SRT to read all file data, spin until it's received into shared memory
	//	> (this does NOT load it into HRT local memory) 
	printf("      Syscon: Starting to read data\n");	// mrr DM    
	gpAoHrtFileData_Cmd(eFileReadAllData);
	printf("      *Syscon: Ending reading data\n");	// mrr DM    
	printf("      Syscon: Starting to wait for hrtFileDataShmPtr->allDataAvailFromFile\n");	// mrr DM    
	while (!hrtFileDataShmPtr->allDataAvailFromFile)
	{
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);
	}
	printf("      *Syscon: Finished waiting for hrtFileDataShmPtr->allDataAvailFromFile\n");	// mrr DM    

	//	Now test if data was read successfully and if so, then move it into program variables
	if (!gpAoHrtFileData_Get(eFileTypeParms))
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading parms data at startup, defaults being used");
	else
	{
		if (!gpAoHrtFileData_Get(eFileTypeWfsDark_0))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_0 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsDark_1))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_1 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsDark_2))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_2 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsDark_3))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_3 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsDark_4))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_4 data at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_0))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_0 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_1))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_1 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_2))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_2 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_3))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_3 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_4))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_4 data at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_0))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_0 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_1))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_1 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_2))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_2 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_3))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_3 data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_4))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_4 data at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeRefCentQ))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcentQ data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeRefOffs))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcent offset data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeRefRePhaseOffs))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Cal RePhase data at startup, defaults being used");
		//	if (!gpAoHrtFileData_Get(eFileTypeReserve1))
		//		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Reserve1 file at startup, defaults being used");
		//	if (!gpAoHrtFileData_Get(eFileTypeReserve2))
		//		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Reserve2 file at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeTwtDmCoeff))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for TWT phase to volts, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrDmCoeff))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for WFR phase to volts, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTTDmCoeff))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for TT to volts, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeTwtLabFlat))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt lab flat data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTwtSysFlat))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt sys flat data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTwtDmShape))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt dm shape data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTwtHostOffset))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt host offset data at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeWfrLabFlat))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr lab flat data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrSysFlat))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr sys flat data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrDmShape))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr dm shape data at startup, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrHostOffset))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr host offset data at startup, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeTwtCleanup))				// tweeter cleanup matrix
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Cleanup matrix, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTwtReconFilter))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Recon Filter, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeWfrCM_FTR))				// woofer control matrix for FTR calculations data
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr CM (FTR), defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrCleanup))				// woofer cleanup matrix
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr SM (FTR), defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrVecList))				// wfr extraction vector selection list
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Vector List, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeTwtZeroList))				// twt zeros after wfr extraction
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Zero List, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypetwtInfFuncMask))			// twt inflence functions / twt selection mask
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading MEMS Infl. Func., defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeWfrFtrActMap))			// twt inflence functions / twt selection mask
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR FTR Act Map, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeInitialModalGains))				// FTR optimizer initial gains
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Inital Gain Values, defaults being used");
		if (!gpAoHrtFileData_Get(eFileTypeHostModalGains))			// FTR optimizer host specified  gains
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Gain Values, defaults being used");

		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_1))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #1, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_2))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #2, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_3))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #3, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_4))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #4, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_5))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #5, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_6))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #6, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_7))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #7, previous values being used");
		if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_8))
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #8, previous values being used");
	}
		printf("      *Syscon: Ending SRT Data tested\n");	// mrr DM    

	for( ii=0; ii<eFileTypeNum; ii++)
		hrtFileDataShmPtr->dataFileError[ ii ] = false;

	hrtFileDataShmPtr->someDataAvailFromFile = false;
	hrtFileDataShmPtr->allDataAvailFromFile = false;

	printf("      Syscon: Alert Other threads it is ok to finish init\n");	// mrr DM    

	//	alert other HRT threads that it's okay to finish their initializations and wait for them to do it
	OkayToFinishInit = true;
	while (!CentInitDone || !ReconInitDone || !CamWFSInitDone)
	{
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);
	}
	printf("      *Syscon: All threads have finished init'ing\n");	// mrr DM    

	//	enter an infinite loop
	while (!QuitSysConThread)
	{
		//	pause for half a second
		//	Note! The following delay will also allow HRT->SRT commands for file data to execute.  If this timer
		// 		is removed or replaced with a 'wait' that wakes up on each command, the file reads may not have
		// 		been completed. 
		next.tv_sec = 0;
		next.tv_nsec = 500000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);
	
		//	check whether file data is available and, if it is, move it into program variables
		switch( sysConCmdCrnt )
		{
			case eCmdSysConParms:

				if (hrtFileDataShmPtr->allDataAvailFromFile)
				{
					if (!gpAoHrtFileData_Get(eFileTypeParms))
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading parms data, all previous values being used");
					else
					{
						if (!gpAoHrtFileData_Get(eFileTypeWfsDark_0))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_0 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsDark_1))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_1 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsDark_2))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_2 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsDark_3))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_3 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsDark_4))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_4 data, previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_0))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_0 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_1))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_1 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_2))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_2 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_3))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_3 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_4))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_4 data, previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_0))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_0 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_1))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_1 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_2))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_2 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_3))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_3 data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_4))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_4 data, previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeRefCentQ))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcentQ data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeRefOffs))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcent offset data at startup, defaults being used");
						if (!gpAoHrtFileData_Get(eFileTypeRefRePhaseOffs))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading RePhase data, previous values being used");
					
						if (!gpAoHrtFileData_Get(eFileTypeTwtDmCoeff))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Coeff for 'phs to Vlt' table, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrDmCoeff))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Coeff for 'phs to Vlt' table, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTTDmCoeff))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading TT Coeff for 'phs to Vlt' table, previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeTwtLabFlat))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt flat Lab data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTwtSysFlat))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt flat Sys data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTwtDmShape))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt dm shape data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTwtHostOffset))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt host offset data at startup, defaults being used");
					
						if (!gpAoHrtFileData_Get(eFileTypeWfrLabFlat))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Lab flat data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrSysFlat))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Sys flat data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrDmShape))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr dm shape data, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrHostOffset))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr host offset data at startup, defaults being used");

						if (!gpAoHrtFileData_Get(eFileTypeTwtCleanup))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Cleanup matrix, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTwtReconFilter))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt ReconFilter (FTR), previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeWfrCM_FTR))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR CM (FTR), previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrCleanup))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR SM (FTR), previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrVecList))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR Vect List (FTR), previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeTwtZeroList))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Zero List, defaults being used");
						if (!gpAoHrtFileData_Get(eFileTypetwtInfFuncMask))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Tweeter InfFuncMask (FTR), previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeWfrFtrActMap))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Act Map (FTR), previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeInitialModalGains))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Init Gain, 1.0 values being set");
						if (!gpAoHrtFileData_Get(eFileTypeHostModalGains))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Host Gains, previous values being used");

						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_1))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #1, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_2))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #2, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_3))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #3, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_4))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #4, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_5))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #5, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_6))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #6, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_7))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #7, previous values being used");
						if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_8))
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #8, previous values being used");
					}

					for( ii=0; ii<eFileTypeNum;ii++)
						hrtFileDataShmPtr->dataFileError[ ii ] = false;
					hrtFileDataShmPtr->allDataAvailFromFile = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					SysConSendStrngNoParm(eDebugLevelMin, "INFO 'readConfig' complete");
					sysConCmdCrnt = eCmdNone;
				}
				break;

			case eCmdFillFtrWfrCm:
				if ( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrCM_FTR ])
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr CM file on HRT side");
					strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR], FTR_wfrCmFilenameSave);  // restore original Wfr CM filename
					hrtFileDataShmPtr->dataFileError[ eFileTypeWfrCM_FTR ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				if (hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR])
				{
					if (!gpAoHrtFileData_Get( eFileTypeWfrCM_FTR ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading FTR Wfr CM file, previous values being used");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr CM filename
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				break;

			case eCmdRefOffs:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefOffs ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offsets file");
					hrtFileDataShmPtr->dataFileError[ eFileTypeRefOffs ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				// We have new ref Offset FileName
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeRefOffs ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offsets file");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new Ref Off filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdRefRePhaseOffs:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefRePhaseOffs ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref CAL re-Phase Offs");
					hrtFileDataShmPtr->dataFileError[ eFileTypeRefRePhaseOffs ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				// We have new ref Re-Phase Offsets
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeRefRePhaseOffs ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref CAL re-Phase Offs file");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the ref Re-Phase Offsets filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdSetSimCalData:  // this is basically a no-op -- it's run in response to a setSimCalData command that's handled on the SRT side
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefMapOffs ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offset Map file");
					hrtFileDataShmPtr->dataFileError[ eFileTypeRefMapOffs ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				// We have new Cal Ref Offset Map 
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeRefMapOffs ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offset Map file");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;
							
			case eCmdSetTwt:
				printf ( " HRT eCmdSetTwt-1\n" );	// mrr twt
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeTwtDmShape ] )
				{
					printf ( " HRT eCmdSetTwt-1: Error in data file\n" );	// mrr twt
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host shape file for Tweeter on HRT side");
					hrtFileDataShmPtr->dataFileError[ eFileTypeTwtDmShape ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}

				// We have new twtShapeFileName
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ])
				{
					printf ( " HRT eCmdSetTwt: new file name\n" );	// mrr twt
					if (!gpAoHrtFileData_Get( eFileTypeTwtDmShape ))
					{	// oops! error
						printf ( " HRT eCmdSetTwt: error in new file name\n" );	// mrr twt
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host DmShape file for tweeter");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{	// Got the data
						printf ( " HRT eCmdSetTwt: new file name data avail == false\n" );	// mrr twt
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new Twt DM Shape filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdTwtOff:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeTwtHostOffset] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host 'offset' file for Tweeter on HRT side");
					hrtFileDataShmPtr->dataFileError[ eFileTypeTwtHostOffset ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeTwtHostOffset ))
					{	
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host offset file for tweeter");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new Ref Off filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdSetWfr:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrDmShape] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host shape file for Woofer");
					hrtFileDataShmPtr->dataFileError[ eFileTypeWfrDmShape] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				// We have new wfrShapeFileName
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeWfrDmShape ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host DmShape file for woofer");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr dm sahpe filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdWfrOff:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrHostOffset ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Offset file for Woofer on HRT side");
					hrtFileDataShmPtr->dataFileError[ eFileTypeWfrHostOffset ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeWfrHostOffset ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host Offset file for woofer");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr Off filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdSetHstMdlGains:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeHostModalGains ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Initial Opt Gains (HRT)");
					hrtFileDataShmPtr->dataFileError[ eFileTypeHostModalGains ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeHostModalGains ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Opt Gains" );
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
						sysConCmdCrnt = eCmdNone;
						break;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new modal gains filename
						sysConCmdCrnt = eCmdNone;
						break;
					}
				}
				break;

			case eCmdSetGainSrcs:
				if( hrtFileDataShmPtr->dataFileError[ eFileTypeModalGainSelectors ] )
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt modal selection choices (HRT)");
					hrtFileDataShmPtr->dataFileError[ eFileTypeModalGainSelectors ] = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
					sysConCmdCrnt = eCmdNone;
					break;
				}
				if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ])
				{
					if (!gpAoHrtFileData_Get( eFileTypeModalGainSelectors ))
					{
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Modal Selector File");
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
						sysConCmdCrnt = eCmdNone;
					}
					else
					{
						hrtFileDataShmPtr->someDataAvailFromFile = false;
						hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
						gpAoHrtFileData_Put(eFileTypeParms);  // store the new modal gain choices filename
						sysConCmdCrnt = eCmdNone;
					}
				}
				break;

			default:
				if ((hrtFileDataShmPtr->allDataAvailFromFile) || (hrtFileDataShmPtr->someDataAvailFromFile))
				{
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) unexpected file data on HRT side");
					hrtFileDataShmPtr->allDataAvailFromFile = false;
					hrtFileDataShmPtr->someDataAvailFromFile = false;
					for (i = 0; i < eFileTypeNum; i++)
						hrtFileDataShmPtr->dataAvailFromFile[i] = false;
					sysConCmdCrnt = eCmdNone;
				}
				break;
		}

		//	check command FIFO for message
		retVal = rtl_read(CmdFIFOfd, &cmdFIFOMsg, sizeof(cmdFIFOMsgType));
		if( retVal < 0 && rtl_errno != EAGAIN )
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading from command FIFO, HRT side, errno = ", rtl_errno);

		//	if there is a message, send the command off to the appropriate thread
		else if (retVal > 0)
		{
			if (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdAbort)
			{
				SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) command is ABORT command");
				// Abort is handled special at this point - it is NOT handled below.
				cmdAction = eCmdActNone;	// no action will be taken in this routine but send to appropriate thread
				if (hrtCamDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActToCam;
				else if (hrtCentDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActToCent;
				else if (hrtReconDataPtr->cmdCrnt != eCmdNone)
					cmdAction = eCmdActToRecon;
				else if (sysConCmdCrnt != eCmdNone ) 
					sysConCmdCrnt = eCmdNone;
			}
			else
			{
				// Check validity of command at this point
				if ((hrtCamDataPtr->cmdCrnt == eCmdNone)   && (hrtCentDataPtr->cmdCrnt == eCmdNone) && 
					(hrtReconDataPtr->cmdCrnt == eCmdNone) && (sysConCmdCrnt == eCmdNone) && !currentlyCyclingActuator)
				{
					// If nothing going on then allow command to proceed
					cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
				}
				else if (((CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdCycleAct) ||   // allow multiple cycleActuator commands
						  (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdCycleEnd)) &&  // > or the stopCycling command
						  currentlyCyclingActuator)
				{
					// if special case (cycleActuators or stopCycling), then allow command to proceed
					cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
				}
				else
				{
					// Another command is in progress - flag error and kill the new command
					cmdAction = eCmdActNone;
					if( hrtCamDataPtr->cmdCrnt != eCmdNone )
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Cam cmnd running = ", (int)(hrtCamDataPtr->cmdCrnt) );
					if( hrtCentDataPtr->cmdCrnt != eCmdNone )
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Cent cmnd running = ", (int)(hrtCentDataPtr->cmdCrnt) );
					if( hrtReconDataPtr->cmdCrnt != eCmdNone )
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Recon cmnd running = ", (int)(hrtReconDataPtr->cmdCrnt) );
					if( sysConCmdCrnt != eCmdNone )
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) SysCon cmnd running = ", sysConCmdCrnt );
					if( currentlyCyclingActuator )
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Actuator cycling" );
				}

				// If we still have a command, check the state of the AO loop and verify command is okay
				if (cmdAction != eCmdActNone)
				{
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTwtOpenBit) > 0) && (hrt_AoTwtLoopStat != eAOLoopOpen))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Twt AO loop must be open");
					}
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdWfrOpenBit) > 0) && (hrt_AoWfrLoopStat != eAOLoopOpen))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Wfr AO loop must be open");
					}
					if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTTOpenBit) > 0) && (hrt_AoTTLoopStat != eAOLoopOpen))
					{
						cmdAction = eCmdActNone;
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, TT AO loop must be open");
					}
					if (cmdAction == eCmdActNone)
					{
					}
					else
					{
						if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTwtClosedBit) > 0) && (hrt_AoTwtLoopStat != eAOLoopClosed))
						{
							cmdAction = eCmdActNone;
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Twt AO loop must be closed");
						}
						if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdWfrClosedBit) > 0) && (hrt_AoWfrLoopStat != eAOLoopClosed))
						{
							cmdAction = eCmdActNone;
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Wfr AO loop must be closed");
						}
						if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTTClosedBit) > 0) && (hrt_AoTTLoopStat != eAOLoopClosed))
						{
							cmdAction = eCmdActNone;
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, TT AO loop must be closed");
						}
						if (cmdAction == eCmdActNone)
							printf("    ***** GPI ERRORAO loop must be closed\n" );
					}
				}
			}

			SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) valid command ready to execute: ", cmdFIFOMsg.cmdNdx);
			// Okay - if here - execute the appropriate command 
			switch (cmdAction)
			{
				case eCmdActNone:
					break;

				case eCmdActToCam:
					SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to camera");
					hrtCamDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					hrtCamDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
					{
						if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
							hrtCamDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
						else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
							hrtCamDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
					}
					if (rtl_sem_post(&WFSSem) < 0)
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to WFSSem, errno = ", rtl_errno);
					break;

				case eCmdActToCent:
					SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to centroider");
					hrtCentDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					hrtCentDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
					{
						if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
							hrtCentDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
						else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
							hrtCentDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
					}						
					if (rtl_sem_post(&CentSem) < 0)
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to CentSem, errno = ", rtl_errno);
					break;

				case eCmdActToRecon:
					SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to recon");
					hrtReconDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
					hrtReconDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
					for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
					{
						if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
							hrtReconDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
						else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
							hrtReconDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
					}
					if (rtl_sem_post(&ReconSem) < 0)
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to ReconSem, errno = ", rtl_errno);
					break;

				case eCmdActToHRT:
					SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to HRT");
					switch( CmdTable[cmdFIFOMsg.cmdNdx].idNum )
					{
						case eCmdSysConParms:   // reads all parameter data files
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							gpAoHrtFileData_Cmd(eFileReadAllData);
							break;

						case eCmdSetDebug:
							if      ( !strcmp( "none", cmdFIFOMsg.strngParam[0]) || !strcmp( "0", cmdFIFOMsg.strngParam[0]) )
								hrtSystemDebugLevel = 0;
							else if ( !strcmp( "min",  cmdFIFOMsg.strngParam[0]) || !strcmp( "1", cmdFIFOMsg.strngParam[0]) )
								hrtSystemDebugLevel = 1;
							else if ( !strcmp( "max",  cmdFIFOMsg.strngParam[0]) || !strcmp( "2", cmdFIFOMsg.strngParam[0]) )
								hrtSystemDebugLevel = 2;
							else if ( !strcmp( "tlc",  cmdFIFOMsg.strngParam[0]) || !strcmp( "-1", cmdFIFOMsg.strngParam[0]) )
								hrtFileDataShmPtr->tlcOnFlag = 1;  // TLC on
							else if ( !strcmp( "host", cmdFIFOMsg.strngParam[0]) || !strcmp( "-2", cmdFIFOMsg.strngParam[0]) )
								hrtFileDataShmPtr->tlcOnFlag = 0;  // TLC off -- don't send once per second messages
							else if ( !strcmp( "time", cmdFIFOMsg.strngParam[0]) || !strcmp( "-3", cmdFIFOMsg.strngParam[0]) )
								hrtFileDataShmPtr->tlcOnFlag = -1;  // TLC off -- send once per second messages
							else
								SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) invalid setDebug level");
							hrtFileDataShmPtr->systemDebug = hrtSystemDebugLevel;
							SysConSendStrngNoParm(eDebugLevelMin, "INFO 'setDebug' complete");
							gpAoHrtFileData_Put( eFileTypeParms );  // store the new debug level in parms
							break;

						case eCmdRefOffs:
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefOffs ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdRefOffs );
							break;

						case eCmdRefRePhaseOffs:
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefRePhaseOffs ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdRefRePhaseOffs );
							break;

						case eCmdSetSimCalData:  // this is run in response to a setSimCalData command that's handled on the SRT side
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefMapOffs ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdRefMapOffs );
							break;
							
						case eCmdSetTwt:  // reads the tweeter Shape file from the host
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtDmShape ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd(eFileRdTwtDmShapeData);
							break;

						case eCmdTwtOff:  // reads the tweeter Offset file from the host
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtHostOffset ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd(eFileRdTwtHostOffsetData);
							break;

						case eCmdSetWfr:  // reads the woofer Shape file from the host
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrDmShape], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd(eFileRdWfrDmShapeData);
							break;

						case eCmdRcvApoRefOffs:
							{
								int fileType = -1;
								int rdType  = -1;
								int	apoNum = -1;
								
								sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
								apoNum = cmdFIFOMsg.intParam[0];

								switch( apoNum )
								{
									case 1:
										fileType = eFileTypeApoRefCentOff_1;
										rdType = eFileRdApoRefCentOff_1;
										break;
									case 2:
										fileType = eFileTypeApoRefCentOff_2;
										rdType = eFileRdApoRefCentOff_2;
										break;
									case 3:
										fileType = eFileTypeApoRefCentOff_3;
										rdType = eFileRdApoRefCentOff_3;
										break;
									case 4:
										fileType = eFileTypeApoRefCentOff_4;
										rdType = eFileRdApoRefCentOff_4;
										break;
									case 5:
										fileType = eFileTypeApoRefCentOff_5;
										rdType = eFileRdApoRefCentOff_5;
										break;
									case 6:
										fileType = eFileTypeApoRefCentOff_6;
										rdType = eFileRdApoRefCentOff_6;
										break;
									case 7:
										fileType = eFileTypeApoRefCentOff_7;
										rdType = eFileRdApoRefCentOff_7;
										break;
									case 8:
										fileType = eFileTypeApoRefCentOff_8;
										rdType = eFileRdApoRefCentOff_8;
										break;
								}
								strcpy( hrtFileDataShmPtr->crntFilenames[ fileType ], cmdFIFOMsg.strngParam[1]);
								gpAoHrtFileData_Cmd( rdType );
							}
							break;

						case eCmdWfrOff:  // reads the tweeter Offset file from the host
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrHostOffset], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdWfrHostOffsetData);
							break;

						case eCmdSetHstMdlGains:
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeHostModalGains ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdHostModalGains );
							break;

						case eCmdSetGainSrcs:
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeModalGainSelectors ], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd( eFileRdModalGainSelectors );
							break;

						case eCmdFillFtrWfrCm:  // reads the specified ftr wfr cm file
							sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
							strcpy( FTR_wfrCmFilenameSave, hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR]);  // save cmFilename in case of error
							strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR], cmdFIFOMsg.strngParam[0]);
							gpAoHrtFileData_Cmd(eFileReadFtrWfrCMData);
							break;

						default:
							SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) unexpected command received by syscon, cmdNdx = ", cmdFIFOMsg.cmdNdx);
							break;
					}
					break;
			}
		}
	}
	return NULL;
}

/*!
 *******************************************************************************
 *	@fn SysConSendStrngNoParm()
 *	@fn SysConSendStrngIntParm()
 *	@fn SysConSendStrngFltParm()
 *	@fn SysConSendStrngStrParm()
 *
 *	@brief
 *	These units send null-terminated strings to hostDiag on the SRT side.  
 *
 *	@b DESCRIPTION:
 *	These units send null-terminated strings to hostDiag on the SRT side. The strings
 *	are generally status or error messages to be logged, output to the console, or sent
 *	to the bit bucket.  
 *	@par
 *
 *	@b Usage: SysConSendStrngNoParm(  msgDebugLevel, strngPtr )
 *	@b Usage: SysConSendStrngIntParm( msgDebugLevel, strngPtr, intParam )
 *	@b Usage: SysConSendStrngFltParm( msgDebugLevel, strngPtr, floatParam )
 *	@b Usage: SysConSendStrngStrParm( msgDebugLevel, strngPtr, strParamPtr )
 * 
 *	@param[in] msgDebugLevel (int):		message debug level
 *	@param[in] strngPtr		 (char*):	pointer to null-terminated character string
 *	@param[in] intParam		 (int):		integer parameter
 *	@param[in] floatParam	 (float):	float parameter
 *	@param[in] strParam		 (char*):	string parameter
 *
 *	@exception 
 *	None.
 *
 *	@see
 *	None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 ********************************************************************************/
void 
SysConSendStrngNoParm( int msgDebugLevel, char *strngPtr )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy( diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;

		if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
		{
			//	can't send this message to the host because the FIFO seems to have failed
			printf( "ERR (SysConSendStrngNoParm) writing noParm message to diag FIFO, HRT side, errno = %d, %s\n", rtl_errno, strngPtr );
		}

		if (PRINTF_SEND_STRNGS_SC != 0)
			printf( "HRT    - %s\n", diagMsg.diagString);
	}
}

void 
SysConSendStrngIntParm( int msgDebugLevel, char *strngPtr, int intParam )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngIntParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy( diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;
		diagMsg.diagFIFOMsg.intParam[0] = intParam;

		if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
		{
			// 	can't send this message to the host because the FIFO seems to have failed
			printf( "ERR (SysConSendStrngIntParm) writing intParm message to diag FIFO, HRT side, errno = %d, %s %d\n", rtl_errno, strngPtr, intParam );
		}

		if (PRINTF_SEND_STRNGS_SC != 0)
			printf( "HRT    - %s %d\n", diagMsg.diagString, intParam);
	}
}

void 
SysConSendStrngFltParm( int msgDebugLevel, char *strngPtr, float floatParam )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngFltParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
			strcpy( diagMsg.diagString, strngPtr);

		diagMsg.diagFIFOMsg.stringLength = stringLength;
		diagMsg.diagFIFOMsg.floatParam[0] = floatParam;

		if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
		{
			//	can't send this message to the host because the FIFO seems to have failed
			printf( "ERR (SysConSendStrngFltParm) writing floatParm message to diag FIFO, HRT side, errno = %d, %s %g\n", rtl_errno, strngPtr, floatParam );
		}

		if (PRINTF_SEND_STRNGS_SC != 0)
			printf( "HRT    - %s %f\n", diagMsg.diagString, floatParam);
	}
}

void 
SysConSendStrngStrParm( int msgDebugLevel, char* strngPtr, char* strParam )
{
short stringLength;
struct
{
	diagFIFOMsgType diagFIFOMsg;
	char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

	if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
	{
		diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;
		diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

		stringLength = strlen(strngPtr) + strlen(strParam) + 1;
		if (stringLength > DIAG_STRING_MAX_LENGTH)
		{
			stringLength = DIAG_STRING_MAX_LENGTH;
			strncpy( diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
			if( (stringLength - strlen( strngPtr) ) > 1 )
				strncat( diagMsg.diagString, strParam, (stringLength - strlen( strngPtr)));
			diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
		}
		else
		{
			strcpy(diagMsg.diagString, strngPtr);
			strcat(diagMsg.diagString, strParam);
		}

		diagMsg.diagFIFOMsg.stringLength = stringLength;

		if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
		{
			//	can't send this message to the host because the FIFO seems to have failed
			printf( "ERR (SysConSendStrngStrParm) writing strParm message to diag FIFO, HRT side, errno = %d, %s %s\n", rtl_errno, strngPtr, strParam );
		}

		if (PRINTF_SEND_STRNGS_SC != 0)
			printf( "HRT    - %s\n", diagMsg.diagString);
	}
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
