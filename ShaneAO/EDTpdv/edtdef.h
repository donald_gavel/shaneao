#ifndef _EDTDEF_H
#define _EDTDEF_H

#define EDT_INTERFACE "pdv"
#define PDV 1
#define EDT_COPIES 1

#ifndef BUILD
#define BUILD "0001"
#endif
#define FULLBUILD BUILD " " __DATE__ " " __TIME__

#define EDT_LIBRARY_VERSION "5.3.8.1"

#endif
