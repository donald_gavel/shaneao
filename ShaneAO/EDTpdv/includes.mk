LIBOBJS+= \
    clsim_lib.o \
    cl_logic_lib.o \
    edt_bitload.o \
    edt_error.o \
    edt_trace.o \
    edt_vco.o \
    edt_xilinx.o \
    libdvu.o \
    libedt.o \
    libpdv.o \
    pciload_fn.o \
    pdv_bayer_filter.o \
    pdv_dmy_image.o \
    pdv_initcam.o \
    pdv_interlace.o \
    pdv_recplay.o \
    readcfg.o \
    libedt_timing.o \
    pdv_irig.o \
    edt_si570.o \
    lib_two_wire.o \
    chkbuf.o \
    edt_os_lnx.o

TARGETS+= \
    bitload \
    checkcam \
    check_gap_cl \
    cl_logic \
    clsiminit \
    cl_speed \
    clink_tester \
    countbits \
    dumpit \
    dvinfo \
    edt_sysinfo \
    initcam \
    irigdump \
    new_strobe \
    pdb \
    pdvplayback \
    rcxload \
    rcxmake \
    serial_cmd \
    setdebug \
    setperiod \
    send_tiffs \
    simple_clsend \
    simple_event \
    simple_etrig \
    simple_irig2 \
    simplemem \
    simple_sequence \
    simplest_take \
    simple_take \
    speedtest \
    strobe \
    take \
    taketwo \
    trace \
    usps_test \
    waitevent \
    watchstat \
    xtest \
    timing_test \
    msp430_load \
    clink_counters \
    pdvrecord \
    bigread \
    clsimvar \
    pdvterm \
    sample

CPP_LIBOBJS+=

CPP_TARGETS+=

