/*
* FPGA header read and other utilities
* 
* Originally written it was only Xilinx (hence the name), now encompasses Alterra, may
* include in FPGA types in the future; nevertheless Xilinx will likely be found here
* and there for a long time used generically for all types of FPGAs.   
*/

#include "edtdef.h"

#ifdef _KERNEL

#include "edtdrv.h"

#define EdtDev Edt_Dev

#ifdef _NT_DRIVER_ 

#define edt_strtol strtol

#endif

#include "pciload.h"

#else

#include "edtinc.h"

#include "pciload.h"

#define edt_set edt_reg_write
#define edt_get edt_reg_read

#define edt_delay edt_msleep

#define EDTPRINTF(a,b,c) if (edt_get_verbosity() > b) { printf c ; }

#define edt_strtol strtol

#endif


#ifdef DOXYGEN_SHOW_UNDOC
/**
* @weakgroup edt_undoc
* @{
*/
#endif

/* always use ida_indexed() for new stuff. All the rest should be obsoleted by that
 * one but... to change them would mean a regression testing nightmare
 */
u_int ida_4013e(void *dmy, int sector);
u_int ida_4013xla(void *dmy, int sector);
u_int ida_4028xla(void *dmy, int sector); /* and xc2s150 */
u_int ida_xc2s100(void *dmy, int sector); /* and 200 and 300e */
u_int ida_xc3s1200e(void *dmy, int sector);
u_int ida_xc2s1200e(void *dmy, int sector);
u_int ida_xc5vlx30t(void * edt_p, int dmy);
u_int ida_indexed(void * edt_p, int dmy); /* was ida_xc5vlx30t -- use this for all from now on */
u_int edt_get_fpga_info_index(EdtDev *edt_p);

void edt_read_prom_data(EdtDev *edt_p, int promcode, int segment, EdtPromData *pdata);
void edt_x_block_program(EdtDev *edt_p, u_int addr, u_char *data, int nbytes, int xtype);
void edt_get_sns(EdtDev *edt_p, char *esn, char *osn);
int edt_x_prom_detect(EdtDev * edt_p, u_char *stat);
Edt_prominfo ep;

/* get from edt_get_prominfo */
Edt_prominfo EPinfo[32] =
{
/* comment                   name            prom                 bus     type       magic        sectorsize        scts/seg dfltseg id_addr(),     s0, s1 */
/* DUMMY              0 */  {"unknown [0]",  "na",                "na",   0,         0,           0,                0,       0,      NULL,          -1, -1},
/* AMD_4013E          1 */  {"4013e",        "AMD 29F010",        "PCI",  XTYPE_X,   XilinxMagic, E_SECTOR_SIZE,    8,       0,      ida_4013e,      1, -1},
/* AMD_4013XLA        2 */  {"4013xla",      "AMD 29F010",        "PCI",  XTYPE_X,   XilinxMagic, AMD_SECTOR_SIZE,  1,       1,      ida_4013xla,    1, 3},
/* AMD_4028XLA        3 */  {"4028xla",      "AMD 29F010",        "PCI",  XTYPE_BT,  XilinxMagic, AMD_SECTOR_SIZE,  2,       2,      ida_4028xla,    2, 3},
/* AMD_XC2S150        4 */  {"xc2s150",      "AMD 29LV040B",      "PCI",  XTYPE_BT,  XilinxMagic, AMD_SECTOR_SIZE,  2,       2,      ida_4028xla,    2, 3},
/* AMD_XC2S200_4M     5 */  {"xc2s200",      "AMD 29LV040B 4MB",  "PCI",  XTYPE_BT,  XilinxMagic, AMD_SECTOR_SIZE,  4,       2,      ida_xc2s100,    0, 1},
/* AMD_XC2S200_8M     6 */  {"xc2s200",      "AMD 29LV081B 8MB",  "PCI",  XTYPE_BT,  XilinxMagic, AMD_SECTOR_SIZE,  4,       2,      ida_xc2s100,    2, 3},
/* AMD_XC2S100_8M     7 */  {"xc2s100",      "AMD 29LV081B 8MB",  "PCI",  XTYPE_BT,  XilinxMagic, AMD_SECTOR_SIZE,  4,       2,      ida_xc2s100,    2, 3},
/* AMD_XC2S300E       8 */  {"xc2s300e",     "AMD 29LV081B 8MB",  "PCI",  XTYPE_LTX, XilinxMagic, AMD_SECTOR_SIZE,  4,       2,      ida_xc2s100,   -1, -1},
/* SPI_XC3S1200E      9 */  {"xc3s1200e",    "SPI W25P16",        "PCIe", XTYPE_SPI, XilinxMagic, SPI_SECTOR_SIZE,  32,      0,      ida_xc3s1200e,  0, -1},
/* AMD_XC5VLX30T     10 */  {"xc5vlx30t",    "AMD S29GL064N",     "PCIe", XTYPE_BT2, XilinxMagic, AMD_SECTOR_SIZE,  32,      3,      ida_indexed,    3, -1},
/* AMD_XC5VLX50T     11 */  {"xc5vlx50t",    "AMD S29GL064N",     "PCIe", XTYPE_BT2, XilinxMagic, AMD_SECTOR_SIZE,  32,      3,      ida_indexed,    3, -1},
/* AMD_EP2SGX30D     12 */  {"ep2sgx30d",    "AMD S29GL128N",     "PCIe", XTYPE_BT2, AlteraMagic, AMD_SECTOR_SIZE,  32,      3,      ida_indexed,    3, -1},
/* AMD_XC5VLX70T     13 */  {"xc5vlx70t",    "AMD S29GL064N",     "PCIe", XTYPE_BT2, XilinxMagic, AMD_SECTOR_SIZE,  64,      0,      ida_indexed,    0, -1},
/* AMD_XC5VLX30T_A   14 */  {"xc5vlx30t",    "AMD S29GL064N (A)", "PCIe", XTYPE_BT2, XilinxMagic, AMD_SECTOR_SIZE,  32,      3,      ida_indexed,    3, -1},
/* AMD_XC6SLX45      15 */  {"xc6slx45",     "AMD S29GL064N",     "PC104",XTYPE_BT2, XilinxMagic, AMD_SECTOR_SIZE,  32,      3,      ida_indexed,    3, -1},
/* AMD_EP2SGX30D_A   16 */  {"ep2sgx30d",    "AMD S29GL256P",     "PCIe", XTYPE_BT2, AlteraMagic, AMD_SECTOR_SIZE2, 32,      3,      ida_indexed,    3, -1},
/* AMD_EP2AGX45D     17 */  {"ep2agx45d",    "AMD S29GL256S",     "PCIe", XTYPE_BT3, AlteraMagic, AMD_SECTOR_SIZE2, 64,      3,      ida_indexed,    3, -1},
/* DUMMY             18 */  {"unknown [18]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             19 */  {"unknown [19]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             20 */  {"unknown [20]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             21 */  {"unknown [21]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             22 */  {"unknown [22]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             23 */  {"unknown [23]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             24 */  {"unknown [24]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             25 */  {"unknown [25]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             26 */  {"unknown [26]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             27 */  {"unknown [27]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             28 */  {"unknown [28]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             29 */  {"unknown [29]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             30 */  {"unknown [30]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1},
/* DUMMY             31 */  {"unknown [31]", "na",                "na",   0,         0,           0,                 0,      0,       NULL,         -1, -1}
};

#define EDT_MAX_PROMCODE 16

static struct
{
    u_int   mid;
    u_int   devid;
    u_int   prom;
}       older_devices[] =

{
    {
	0x1, 0x20, AMD_4013E
    },
    {
	0x1, 0x4f, AMD_4013XLA
    },
	{
	    0x20, 0xe3, AMD_4013XLA
	},
	{
	    0x00, 0x00, 0
	}
};

/* shorthand debug level */
#define DEBUG1 EDTLIB_MSG_INFO_1
#define DEBUG2 EDTLIB_MSG_INFO_2

/*
* command bits for the 4028xla boot controller
*/
#define BT_EN_READ	0x8000

/*
* bit to set in all prom addresses to enable the upper 2 bits of prom
* address after we have read the status (jumper and 5/3.3v)
*/
#define	EN_HIGH_ADD	0x00800000

static int needswap = 0;

static void	spi_read(EdtDev *edt_p, int addr, u_char *rbuf, int rcnt);
static int	spi_reset(EdtDev *edt_p);
static void	spiprim(EdtDev *edt_p, int opcode, int wbytes, int rbytes, unsigned char* wbuf, unsigned char* rbuf);
static void	spi_xwr(EdtDev *edt_p, int val);
static int	spi_xrd(EdtDev *edt_p);
static void	spi_xw(EdtDev *edt_p, u_int addr, u_int data);
static u_int	spi_xr(EdtDev *edt_p, u_int addr);

static void   bt_write(EdtDev *edt_p,u_int addr, u_char val);
static u_char bt_read(EdtDev *edt_p, u_int addr);
static void   bt_reset(EdtDev *edt_p);
static u_char bt_readstat(EdtDev *edt_p);

static void   bt2_reset(EdtDev *edt_p);

static void   xwrite(EdtDev *edt_p, u_int addr, u_char val);
static u_char xread(EdtDev *edt_p, u_int addr);
static void   xreset(EdtDev *edt_p);
static u_char xreadstat(EdtDev *edt_p);


static void   tst_write(EdtDev * edt_p, uint_t desc, uint_t val);
static uint_t tst_read(EdtDev * edt_p, uint_t desc);

/* on some sun platforms memset isn't in the kernel */
static void
edt_zero(void *s, size_t n)
{
#ifdef __sun
    bzero(s, n);
#else
    memset(s, 0, n);
#endif
}

void tst_pflash(EdtDev *edt_p, uint_t addr, uint_t val);

static int 
spi_block_program(EdtDev *edt_p, int addr0, int size, u_char *spibuf, int verify_only);

char *
read_x_block(EdtDev *edt_p, u_int addr, u_char *buf, int size, int xtype);

/*
 * get prom info
 */
Edt_prominfo *
edt_get_prominfo(int promcode)
{
    return &EPinfo[promcode];
}

/*
 * get max promcode
 */
int
edt_get_max_promcode()
{
    return EDT_MAX_PROMCODE;
}

/* return true if machine is little_endian */

#ifdef _KERNEL

static int
edt_little_endian()
{
    u_short test;
    u_char *byte_p;

    byte_p = (u_char *) & test;
    *byte_p++ = 0x11;
    *byte_p = 0x22;
    if (test == 0x1122)
    {
        return (0);
    }
    else
    {
        return (1);
    }
}

#endif

#define DQ7 0x80
#define DQ5 0x20
#define DQ3 0x08
static int maxloops = 0;


static int debug_fast = 0;
static int do_fast = 0;
static int force_slow = 0;

int
edt_get_debug_fast()
{
    return (debug_fast) ;
}
int
edt_set_debug_fast(int val)
{
    (debug_fast = val) ;
    return val;
}
int
edt_get_do_fast()
{
    return (do_fast) ;
}
void
edt_set_do_fast(int val)
{
    do_fast = val ;
}

int
edt_get_force_slow()
{
    return (force_slow) ;
}
void
edt_set_force_slow(int val)
{
    force_slow = val ;
}



static char *device_name[NUM_DEVICE_TYPES] = {"pcd", "pdv", "p11w", "p16d", "p53b"};
char *read_x_string(EdtDev *edt_p, u_int addr, char *buf, int size, int xtype);



/* what are these? */

volatile caddr_t dmyaddr = 0;
volatile u_int *cfgaddr ;
volatile u_int *tstaddr ;


u_int
swap32(u_int val)
{
    /* already set hardware swap if LTX hub */
    return (
	((val & 0x000000ff) << 24)
	| ((val & 0x0000ff00) << 8)
	| ((val & 0x00ff0000) >> 8)
	| ((val & 0xff000000) >> 24));
}


/*
* print 16 bytes at the address passed read from 4028xla boot controller
*/
static void
bt_print16(EdtDev * edt_p, u_int addr)
{
    int     i;

    EDTPRINTF(edt_p,2,("%08x ", addr));
    for (i = 1; i < 16; i++)
    {
	EDTPRINTF(edt_p,2,(" %02x", bt_read(edt_p, addr)));
	addr++;
    }
    EDTPRINTF(edt_p,2,("\n"));
}

/*
* print 16 bytes at the address passed
*/
static void
xprint16(EdtDev * edt_p, u_int addr)
{
    int     i;

    EDTPRINTF(edt_p,2,("%08x ", addr));
    for (i = 1; i < 16; i++)
    {
	EDTPRINTF(edt_p,2,(" %02x", xread(edt_p, addr)));
	addr++;
    }
    EDTPRINTF(edt_p,2,("\n"));
}

/*
* print 16 bytes at the address passed
*/
static void
spi_print16(EdtDev * edt_p, u_int addr)
{
    int     i;
    u_char val;

    EDTPRINTF(edt_p,2,("%08x ", addr));
    for (i = 1; i < 16; i++)
    {
	spi_read(edt_p, (u_int)addr, &val, 1);
	EDTPRINTF(edt_p,2,(" %02x", val));
	addr++;
    }
    EDTPRINTF(edt_p,2,("\n"));
}



void
edt_x_print16(EdtDev * edt_p, u_int addr, int xtype)
{
    switch(xtype)
    {
    case XTYPE_SPI:
	spi_print16(edt_p, (u_int)addr);
	break;
    case XTYPE_BT:
    case XTYPE_BT2:
	bt_print16(edt_p, addr);
	break;
    case XTYPE_X:
	xprint16(edt_p, addr);
	break;
    }
}

u_char
edt_x_read(EdtDev * edt_p, u_int addr, int xtype)
{
    u_char  val;

    switch(xtype)
    {
    case XTYPE_SPI:
	spi_read(edt_p, addr, &val, 1);
        return val;
    case XTYPE_BT:
    case XTYPE_BT2:
	return bt_read(edt_p, addr);
    case XTYPE_X:
	return xread(edt_p, addr);
    }

    return 0;
}

/*
* sector erase for AMD 29LV0XXX
*/
static int
bt_sector_erase(EdtDev * edt_p, u_int sector, u_int sec_size)
{
    u_int   addr;
    int     done = 0;
    int     loops = 0;
    u_char  val;

    addr = sector * sec_size;
    bt_write(edt_p, 0x5555, 0xaa);
    bt_write(edt_p, 0x2aaa, 0x55);
    bt_write(edt_p, 0x5555, 0x80);
    bt_write(edt_p, 0x5555, 0xaa);
    bt_write(edt_p, 0x2aaa, 0x55);
    bt_write(edt_p, addr, 0x30);
    done = 0;
    while (!done)
    {
	val = bt_read(edt_p, addr);
	loops++;
	if (val & DQ7)
	{
	    done = 1;
	}
    }
    return 0;
}

/*
 * sector erase for AMD S29GL64N, S29GL128N
 */


static int
bt2_sector_erase(EdtDev * edt_p, u_int sector, u_int sec_size)
{
    u_int   addr;
    int     done = 0;
    int     result = 0;
    u_char  val;
#ifndef _KERNEL
    double t= edt_timestamp();
    double elapsed;

    addr = sector * sec_size;
    bt_write(edt_p, 0xaaa, 0xaa);
    bt_write(edt_p, 0x555, 0x55);
    bt_write(edt_p, 0xaaa, 0x80);
    bt_write(edt_p, 0xaaa, 0xaa);
    bt_write(edt_p, 0x555, 0x55);
    bt_write(edt_p, addr, 0x30);
    done = 0;
    while (!done)
    {
	edt_msleep(1);
	val = bt_read(edt_p, addr);
	elapsed = edt_timestamp() - t;

	if (val & DQ7)
	{
	    done = 1;
	}
	if (elapsed > 3.0)
	{
            done = 1;
            result = 1;
	}
    }
#endif

    return(result);
}

static int
xsector_erase(EdtDev * edt_p, u_int sector, u_int sec_size)
{
    u_int   addr;
    int     done = 0;
    int     loops = 0;
    u_char  val;

    addr = sector * sec_size;
    xwrite(edt_p, 0x5555, 0xaa);
    xwrite(edt_p, 0x2aaa, 0x55);
    xwrite(edt_p, 0x5555, 0x80);
    xwrite(edt_p, 0x5555, 0xaa);
    xwrite(edt_p, 0x2aaa, 0x55);
    xwrite(edt_p, addr, 0x30);
    done = 0;
    while (!done)
    {
	val = xread(edt_p, addr);
	loops++;
	if (val & DQ7)
	{
	    done = 1;
	    break;
	}
    }
    return 0;
}



/*
* for 4028xla, xc2s100, xc2s200
*/
static int
bt_byte_program(EdtDev * edt_p, int xtype, u_int addr, u_char data)
{
    u_char  val;
    int     done = 0;
    int     loops = 0;

    bt_write(edt_p, 0x5555, 0xaa);
    bt_write(edt_p, 0x2aaa, 0x55);
    bt_write(edt_p, 0x5555, 0xa0);
    bt_write(edt_p, addr, data);
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_RD_ROM);

    while (!done)
    {
	val = (u_char) tst_read(edt_p, EDT_FLASHROM_DATA);

	if (val == data)
	    done = 1;
	if (loops > 1000)
	{
	    EDTPRINTF(edt_p,1,("bt_byte_program: failed on %x data %x val %x\n",
		addr, data, val));
	    bt_reset(edt_p );
	    bt_write(edt_p, 0x555, 0xaa);
	    bt_write(edt_p, 0x2aa, 0x55);
	    bt_write(edt_p, 0x555, 0xa0);
	    bt_write(edt_p, addr, data);
	    tst_write(edt_p, EDT_FLASHROM_DATA, BT_RD_ROM);
	    loops = 0;
	}
	loops++;
    }
    if (loops > maxloops)
    {
	maxloops = loops;
    }
    if (done)
	return (0);
    else
	return (addr);
}


/*
 * for xc5vlx30t, xc5vlx50t, ep2sgx30d
 */
static int
bt2_byte_program(EdtDev * edt_p, int xtype, u_int addr, u_char data)
{
    u_char  val;
    int     done = 0;
    int     loops = 0;
    int     failures = 0;

    bt_write(edt_p, 0xaaa, 0xaa);
    bt_write(edt_p, 0x555, 0x55);
    bt_write(edt_p, 0xaaa, 0xa0);
    bt_write(edt_p, addr, data);
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_RD_ROM);

    while (!done)
    {
	val = (u_char) tst_read(edt_p, EDT_FLASHROM_DATA);

	if (val == data)
	    done = 1;
	if (loops > 1000)
	{
	    bt2_reset(edt_p);
	    bt_write(edt_p, 0xaaa, 0xaa);
	    bt_write(edt_p, 0x555, 0x55);
	    bt_write(edt_p, 0xaaa, 0xa0);
	    bt_write(edt_p, addr, data);
	    tst_write(edt_p, EDT_FLASHROM_DATA, BT_RD_ROM);
	    loops = 0;
	    failures++;
	}
	loops++;

        if (failures > 5)
	{
	    EDTPRINTF(edt_p,0,("bt2_byte_program: failed on %x data %x val %x\n", addr, data, val));
	    return(1);
	}
    }
    if (loops > maxloops)
    {
	maxloops = loops;
    }
    if (done)
	return (0);
    else
	return (addr);
}


/*
* for 4013e and others
*/

/* return 0 for success */
static int
xbyte_program(EdtDev * edt_p, u_int addr, u_char data)
{
    u_char  val;
    int     done = 0;
    int     loops = 0;

	tst_pflash(edt_p, addr, data) ;
    while (!done)
    {
	loops++;
	val = xread(edt_p, addr);
	if (val == data)
	    done = 1;
	if (loops > 100)
	{
	    EDTPRINTF(edt_p,1,("xbyte_program: failed on %x data %x val %x\n",
		addr, data, val));
	    loops = 0;
	}
    }
    if (loops > maxloops)
    {
	if (loops > 10)
	    EDTPRINTF(edt_p,1,("maxloops %x %d\n", addr, loops));
	maxloops = loops;
    }
    if (done)
	return (0);
    else
	return (addr);
}


/*
 * Program xilinx: 1 byte
 */
void
edt_x_byte_program(EdtDev *edt_p, u_int addr, u_char data, int xtype)
{
    u_char buf[2];

    switch(xtype)
    {

    case XTYPE_SPI:
        buf[0] = data;
	edt_x_block_program(edt_p, addr, buf, 1, xtype);

        break;
    case XTYPE_BT:
	bt_byte_program(edt_p, xtype, addr, data);
	break;
    case XTYPE_BT2:
	bt2_byte_program(edt_p, xtype, addr, data);
	break;
    case XTYPE_X:
	xbyte_program(edt_p, addr, data); 
	break;
    }
}

/*
 * Program Xilinx: N bytes starting at addr
 */
void
edt_x_block_program(EdtDev *edt_p, u_int addr, u_char *data, int nbytes, int xtype)
{
    int i;
    u_char *dp = data;

    switch(xtype)
    {

    case XTYPE_SPI:
	spi_block_program(edt_p, addr, nbytes, data, 0);
	break;

    case XTYPE_BT:
	for (i=0; i<nbytes; i++)
	    bt_byte_program(edt_p, xtype, addr++, *(dp++));
	break;
    case XTYPE_BT2:
	for (i=0; i<nbytes; i++)
	    bt2_byte_program(edt_p, xtype, addr++, *(dp++));
	break;
    case XTYPE_X:
	for (i=0; i<nbytes; i++)
	    xbyte_program(edt_p, addr++, *(dp++)); 
	break;
    
    }
}

/*
 * Verify Xilinx: N bytes starting at addr
 */
void
edt_x_verify(EdtDev *edt_p, u_int addr, u_char *data, int nbytes, int xtype)
{
    switch(xtype)
    {
    case XTYPE_SPI:
	spi_block_program(edt_p, addr, nbytes, data, 1);
	break;
    default:
    	EDTPRINTF(edt_p,1,("Oops! Can't verify type %d xilinx with edt_x_verify (pciload internal)\n", xtype));
	break;
    }
}



/** Retrieve the board's embedded information string from the PCI xilinx information header.
* 
* The EDT manufacturer's part numbers is embedded in an unused area of the Xilinx
* FPGA PROM, and is preserved across reloads (via pciload, hubload, etc.) unless
* options to overwrite are invoked in one of those utilities. This subroutine
* retrieves the EDT serial number portion of that information.
*
* The data is an ASCII string, with the following colon-separated fields:
*
* serial number:part number:clock speed:options:revision:interface xilinx:
*
* (To see the information string, run \e pciload with no arguments.)
*
* @note Information embedding was implemented in Sept. 2004; boards shipped before
* that time will yield a string with all NULLS unless the board's FPGA
* has since been updated with the embedded information.
*
* @param edt_p pointer to edt device structure returned by #edt_open 
* @param esn the EDT part number without dashes.
* @see edt_get_sns, edt_get_osn, edt_parse_esn, edt_fmt_pn
*/
void
edt_get_esn(EdtDev *edt_p, char *esn)
{
    char dmy[128];
    edt_get_sns(edt_p, esn, dmy);
}

/** Retrieve the board OEM's embedded information string from the PCI xilinx information header.
* 
* Some OEMs embed part number or other information about the board in an unused area of the
* Xilinx FPGA PROM. This information is preserved across reloads (via pciload, hubload, etc.)
* unless options to overwrite are invoked in one of those utilities. This subroutine
* retrieves the OEM serial number portion of that information.
*
* @note Information embedding was implemented in Sept. 2004; boards shipped before
* that time will yield a string with all NULLS unless the board's FPGA
* has since been updated with the embedded information.
*
* @param edt_p pointer to edt device structure returned by #edt_open 
* @param osn the OEM's part number, if present.
* @see edt_get_sns, edt_get_esn, edt_fmt_pn
*/
void
edt_get_osn(EdtDev *edt_p, char *osn)
{
    char dmy[128];
    edt_get_sns(edt_p, dmy, osn);
}

static int
isdigit_str(char *s)
{
    unsigned int i;

    for (i=0; i<strlen(s); i++)
	if ((s[i]) < '0' || s[i] > '9')
	    return 0;
    return 1;
}


/**
 * Extract the name of the on-board firmware in the device's FPGA PROM, minus
 * the extension.
 *
 * Fills in the name string field with the name of the device's onboard PROM,
 * minus the voltage descriptor if any, and extension (.ncd). 
 * For devices with two voltage sectors to the firmware, two files with
 * _3v.ncd and _5v.ncd extensions will be present; in such cases the _3v and _5v
 * will be stripped off as well.  For example if the device is loaded with
 * "pcidev-10_3v.ncd" and "pcidev-10_5v.ncd", the subroutine will return "pcidev-10".
 *
 * @param edt_p pointer to edt device structure returned by #edt_open 
 * @param name FPGA filename
 * @see edt_x_get_fname_auto
 */
int
edt_x_get_fname(EdtDev *edt_p, char *name)
{
    int promcode;
    char *p;
    char *idstr;
    EdtPromData pdata;
    Edt_prominfo *ep;
    u_char stat;
	
    promcode = edt_x_prom_detect(edt_p, &stat);
    
    ep = edt_get_prominfo(promcode);
	
    edt_read_prom_data(edt_p, promcode, ep->defaultseg,
					   &pdata);    
	
    idstr = pdata.id;

#if defined ( __APPLE__) && defined(_KERNEL) 	
    if ((p = edt_strrchr(idstr, '.')) != NULL)
		*p = '\0' ;
	
    if ((p = edt_strrchr(idstr, '_')) != NULL)
    {
		if (( *(p + 1) == '3' || *(p + 1) == '5' )  && ( *(p + 2) == 'v' ))
			*p = '\0' ;
    }
	
    if ((p = edt_strrchr(idstr, '-')) != NULL)
    {
		*p = '\0' ;
    }
#else
    if ((p = strrchr(idstr, '.')) != NULL)
		*p = '\0' ;
	
    if ((p = strrchr(idstr, '_')) != NULL)
    {
		if (( *(p + 1) == '3' || *(p + 1) == '5' )  && ( *(p + 2) == 'v' ))
			*p = '\0' ;
    }
	
    if ((p = strrchr(idstr, '-')) != NULL)
    {
		*p = '\0' ;
    }

#endif	
    strcpy(name, idstr);
	
    return 0;
}

/**
 * Extract the name of the on-board firmware in the device's FPGA PROM, minus
 * the version and extension.
 *
 * Fills in the name string field with the name of the device's onboard PROM,
 * minus the version, voltage descriptor if any, and extension (.ncd). 
 * For devices with two voltage sectors to the firmware, two files with
 * _3v.ncd and _5v.ncd extensions will be present; in such cases _3v and _5v
 * will be stripped off as well.  For example if the device is loaded with
 * "pcidev-10_3v.ncd" and "pcidev-10_5v.ncd", the subroutine will return "pcidev".
 *
 *
 * @param edt_p pointer to edt device structure returned by #edt_open 
 * @param name FPGA filename
 * @see edt_x_get_fname
 */
int
edt_x_get_fname_auto(EdtDev *edt_p, char *name)
{
    Edt_prominfo *ep;
    char *p;
    char *idstr;
    EdtPromData pdata;
    int promcode;
    u_char stat;

    promcode = edt_x_prom_detect(edt_p, &stat);

    ep = edt_get_prominfo(promcode);
    edt_read_prom_data(edt_p, promcode, ep->defaultseg,
                   &pdata);    

    idstr = pdata.id;

    /* strip off trailing .ncd (etc) */
#if defined( __APPLE__)  && defined(_KERNEL)
    if ((p = edt_strrchr(idstr, '.')) != NULL)
#else
    if ((p = strrchr(idstr, '.')) != NULL)
#endif
    {

	*p = '\0' ;

	/* strip off trailing _[35]v */
	if ((strlen(idstr) > 3) && ((strcmp(p-3, "_3v") == 0) || (strcmp(p-3, "_5v") == 0)))
	    *(p-3) = '\0' ;

    }

    /* if present, strip off trailing -xxx (version number) */
#if defined(__APPLE__) && defined(_KERNEL)
    if ((p =edt_strrchr(idstr, '-')) != NULL)
#else
    if ((p = strrchr(idstr, '-')) != NULL)
#endif
	*p = '\0';


    strcpy(name, idstr);

    return 0;
}


/** Retrieve the board's manufacturer and OEM embedded information strings strings from the PCI xilinx
* information header.
* 
* Certain information about the board, including manufacturer's part number, serial number, clock speed,
* Xilinx FPGA, and options, is embedded in an unused area of the Xilinx FPGA PROM at the time of
* manufacture. This information is preserved across reloads (via pciload, hubload, etc.) unless options
* overwrite are invoked in the utility. This subroutine retrieves EDT and OEM (if present) information.
* The data is an ASCII string, with the following colon-separated fields:
*
* serial number:part number:clock speed:options:revision:interface xilinx:
*
* (To see the information string, run \e pciload with no arguments.)
* 
* @note Information embedding was implemented in Sept. 2004; boards shipped before
* that time will yield a string with all NULLS unless the board's FPGA
* has since been updated with the embedded information.
*
* @param edt_p pointer to edt device structure returned by #edt_open 
* @param esn the EDT part number without dashes.
* @param osn the OEM's part number, if present.
* @see edt_get_esn, edt_get_osn, edt_parse_esn, edt_fmt_pn
*/


void
edt_get_sns_sector(EdtDev *edt_p, char *esn, char *osn, int sector)
{
    int promcode;
    u_char stat;
    EdtPromData pdata;

    promcode = edt_x_prom_detect(edt_p, &stat);

    if ((promcode == PROM_UNKN) || (promcode > edt_get_max_promcode()))
    {
	esn[0] = 0;
	osn[0] = 0;
    }
    else
    {
	edt_read_prom_data(edt_p, promcode, sector, &pdata);
        strcpy(esn, pdata.esn);
        strcpy(osn, pdata.osn);
    }
}

void
edt_get_sns(EdtDev *edt_p, char *esn, char *osn)
{
    u_char stat; 
    int promcode = edt_x_prom_detect(edt_p, &stat);
    Edt_prominfo *ep = edt_get_prominfo(promcode);
    

    edt_get_sns_sector(edt_p, esn, osn, ep->defaultseg);
}

/***************************************************************************/
/* Instruction set for ST M25P20 SPI Flash Memory used on EDT TLKRCI */
#define SPI_WREN  0x06	/* Write Enable,  0 params                   */
#define SPI_WRDI  0x04	/* Write Disable, 0 params                   */
#define SPI_RDSR  0x05	/* Read Status,   0 wbytes,         1 rbyte  */
#define SPI_WRSR  0x01	/* Write Status,  1 wbyte                    */
#define SPI_READ  0x03	/* Read data,     3 addr,           n rbytes */
#define SPI_FREAD 0x0B	/* Fast Read,     3 addr + 1 dummy, n rbytes */
#define SPI_PP    0x02	/* Page Program,  3 addr + 1-256 wbytes      */
#define SPI_SE    0xD8	/* Sector Erase,  3 addr                     */
#define SPI_BE    0xC7	/* Bulk Erase,    0 params                   */
#define SPI_DP    0xB9	/* Power Down,    0 params                   */
#define SPI_RES   0xAB	/* Read Sig,      3 wdummy,         1 rbyte  (DP off)*/

#define SPI_REG   0x02000084	/* PCI Register for flash writes */
#define SPI_EN    0x8000	/* Enable access to SPI reads and writes */
#define SPI_S     0x2000	/* SEL, low true */
#define SPI_D     0x1000	/* DATA out to serial flash */
#define SPI_Q     0x0200	/* Data from flash spi_q is in bit 9 */


/* SPI WORKING VARIABLES */
unsigned char  Spi_wbufa[260];		/* Need 3 addr + 256 data for PP */
unsigned char* Spi_wbuf=Spi_wbufa+3;		/* Just the 256 bytes of data */
unsigned char  Spi_rbuf[260];		/* Read buffer, could use 256 Kbytes? */
volatile caddr_t Spi_mapaddr = 0;


/* for addr=0x040100C0     04:byte_count 01:interface_Xilinx, C0:PCI_address */
static void
spi_xw(EdtDev *edt_p, u_int addr, u_int data)
{
    u_int	dmy;
    edt_set(edt_p, addr, data) ;
    dmy = edt_get(edt_p, 0x04000080) ;	/*flush chipset write buffers*/
}

static u_int
spi_xr(EdtDev *edt_p, u_int addr)
{
    return(edt_get(edt_p, addr)) ;
}


static void
spi_xwr(EdtDev *edt_p, int val)
{
    int d;
    d = SPI_EN | val;				/* Set SEREN bit */
    spi_xw(edt_p, SPI_REG, d);
}

static int
spi_xrd(EdtDev *edt_p)
{
    int v;
    v = spi_xr(edt_p, 0x02000084);
    if (v & SPI_Q)  return(1);
    else return(0);
}


static void 
spiprim(EdtDev *edt_p, int opcode, int wbytes, int rbytes, unsigned char* wbuf, unsigned char* rbuf) 
{
    int c, b, cdat;

    /* spi_xwr(edt_p, SPI_S); */	/* SEL hi, goes low with first bit*/

    cdat = opcode;
    for (b=0; b<8; b++)  {		/* for each bit in the char */
        if (cdat & 0x80) {
	    spi_xwr(edt_p, SPI_D);
        } else {
	    spi_xwr(edt_p, 0);
        }
        cdat <<= 1;
    }

    for (c=0; c<wbytes; c++) {		/* for each char in wbuf */
	cdat = wbuf[c];
	for (b=0; b<8; b++)  {		/* for each bit in the char */
	    if (cdat & 0x80) {
    		spi_xwr(edt_p, SPI_D);
	    } else {
    		spi_xwr(edt_p, 0);
	    }
	    cdat <<= 1;
	}
    }

    for (c=0; c<rbytes; c++) {		/* for each char in rbuf */
	cdat = 0;
	for (b=0; b<8; b++)  {		/* for each bit in the char */
	    cdat <<= 1;
	    if (spi_xrd(edt_p) & 1)  
		cdat |= 1;
	    if ((b!=7) || (c!=(rbytes-1)))  {
    	        spi_xwr(edt_p, 0);
	    }
	}
	rbuf[c] = (char) cdat;
    }

    spi_xwr(edt_p, SPI_S);		/* Raise the Select line (to deselect) */
}

/*
 * Read electronic signature
 */
static int
spi_reset(EdtDev *edt_p)
{
    unsigned char rval;
    char *magic = "555";   		/* For M25P20 should be 0x11/0x13 */

    spiprim(edt_p, SPI_RES, 3, 1, (unsigned char *)magic, &rval);

    return(rval&0xff);                      /* Also out of Power Down mode  */
}



/* Read rcnt bytes starting from address addr, store in rbuf[] */
static void
spi_read(EdtDev *edt_p, int addr, u_char *rbuf, int rcnt)
{
    if ((rcnt<1) || (rcnt > 256))   {
	EDTPRINTF(edt_p,1,("Error, bad spi_read() rcnt of %d\n", rcnt));
	return;
    }
    Spi_wbufa[0]=addr>>16; 
    Spi_wbufa[1]=addr>>8; 
    Spi_wbufa[2]=(char) addr;     /* 24 bit address */
    Spi_wbufa[3]=0;  				            /* dummy byte */

    spiprim(edt_p, SPI_FREAD, 4, rcnt, Spi_wbufa, rbuf);    
}

static int
spi_write(EdtDev *edt_p, u_int addr, int wcnt)
{
    int statcnt;

    if ((wcnt<1) || (wcnt>256))   {
	EDTPRINTF(edt_p,1,("Error, bad spi_write() wcnt of %d\n", wcnt));
	return -1;
    }
    spiprim(edt_p, SPI_WREN, 0, 0, NULL, NULL);		    /* write-enable */

    Spi_wbufa[0]=addr>>16; 
    Spi_wbufa[1]=addr>>8; 
    Spi_wbufa[2]=(char) addr;     /* 24 bit address */

    spiprim(edt_p, SPI_PP,  3+wcnt, 0, Spi_wbufa, NULL);		    /* page program */

    statcnt=0;
    do {
        spiprim(edt_p, SPI_RDSR, 0, 1, NULL, Spi_rbuf);    	/* Read Status reg */
	statcnt++;
    } while ((Spi_rbuf[0] & 1) == 1);		/* Could hang here forever? */

	return 0;
}

#define TOO_MANY 100000




static int
spi_sector_erase(EdtDev *edt_p, u_int addr)
{
    int statcnt;

    spiprim(edt_p, SPI_WREN, 0, 0, NULL, NULL);		    /* write-enable */
    Spi_wbufa[0]=addr>>16; 
    Spi_wbufa[1]=addr>>8; 
    Spi_wbufa[2]=(u_char) addr;     /* 24 bit address */
    spiprim(edt_p, SPI_SE,  3, 0, Spi_wbufa, NULL);    

    statcnt=0;
    do {
        spiprim(edt_p, SPI_RDSR, 0, 1, NULL, Spi_rbuf);    	/* Read Status reg */
	if (statcnt++ > TOO_MANY)
	{
	    EDTPRINTF(edt_p,1,("erase spun for too long, bailing out\n"));
	    return(-1);
	}
    } while ((Spi_rbuf[0] & 1) == 1);		/* Could hang here forever? */
		    /* ALERT also should check all 8 status bits says JG */ 

    return(0);
}

/* 
 * check a string for forward or back slashes
 */
int
has_slashes(char *name)
{
    char   *p = name;

    while (*p)
    {
	if ((*p == '/') || (*p == '\\'))
	    return 1;
	++p;
    }
    return 0;
}


/*
 * from spiw in pdbx.c
 */
static int 
spi_block_program(EdtDev *edt_p, int addr0, int size, u_char *spibuf, int verify_only)
{
    int i, addr, n, bcnt, errorcnt, val, first, last;

    /* reset HACK -- sometimes it doesn't work the first time (?) */
    for (i=0; i<5; i++)
    {
	if ((val = spi_reset(edt_p)) != 0)
	    break;
	(void)edt_delay(100);
    }

    if ((val != 0x11) && (val != 0x13) && (val != 0x14))
    {
	EDTPRINTF(edt_p,0,("    Error, did not detect SPI flash device, \n"));
	EDTPRINTF(edt_p,0,("      spi_reset() returned 0x%02x, not 0x11/0x13/0x14\n", val));
	return -1;
    }

    if (!verify_only)
    {
	first = (addr0&0xff0000)>>16;
	last  = ((addr0+size)&0xff0000)>>16;
	EDTPRINTF(edt_p,0,("  First sector is %d, last is %d, erasing", first, last));

	for (n=first; n<=last; n++)
	{
	    spi_sector_erase(edt_p, n<<16);
	    EDTPRINTF(edt_p,0,(".")); 
	}
	EDTPRINTF(edt_p,0,("\n"));

	EDTPRINTF(edt_p,0,("  Writing 0x%x bytes to spiflash at 0x%x.",size,addr0));
	bcnt=0;  addr=addr0;
	while (bcnt<size)
	{
	    n=0;
	    while (bcnt<size)
	    {
		if ((n!=0) && (((addr+n)&0xff)==0)) break;  /* page end */
#ifndef _KERNEL                
                if (!(bcnt % 10000)) {printf("."); fflush(stdout); }
#endif
                Spi_wbuf[n] = spibuf[bcnt]; 
		n = n+1;
		bcnt = bcnt+1;
	    }
	    if (n>0) spi_write(edt_p, addr, n);	/* Write 1-256 bytes from Spi_wbuf*/
	    addr = addr + n;
	}
    }

    EDTPRINTF(edt_p,0,("\n"));
    EDTPRINTF(edt_p,0,("  Verifying."));
    bcnt=0;  addr=addr0; errorcnt=0;
    while (bcnt<size)
    {
	spi_read(edt_p, addr, Spi_rbuf, 256);
	n=0;
	while (bcnt<size)  {
	    if ((n!=0) && (((addr+n)&0xff)==0)) break;  /* page end */
#ifndef _KERNEL                
            if (!(bcnt % 10000)) {printf("."); fflush(stdout); }
#endif
            if ((Spi_rbuf[n]&0xff) != (spibuf[bcnt]&0xff)) {
		if (errorcnt++ <= 10) {
		    EDTPRINTF(edt_p,0,("Error at 0x%x: wrote:%02x, read:%02x\n", 
				addr0+bcnt, spibuf[bcnt], Spi_rbuf[n]));
		}
	    }
	    n = n+1;
	    bcnt = bcnt+1;
	}
	addr = addr + n;
    }
    if (errorcnt)
	EDTPRINTF(edt_p,0,("    Saw %d errors\n", errorcnt));

    if (!errorcnt)
        EDTPRINTF(edt_p,0,("\nno errors\n"));

    return 0;
}



/* write a byte to the PCI interface prom (only 4013E and 4013XLA) */
static void
xwrite(EdtDev * edt_p, u_int addr, u_char val)
{

	(void)tst_read(edt_p, EDT_FLASHROM_ADDR);
	tst_write(edt_p, EDT_FLASHROM_DATA, val) ;
	(void)tst_read(edt_p, EDT_FLASHROM_ADDR);
	tst_write(edt_p, EDT_FLASHROM_ADDR, EN_HIGH_ADD | addr | EDT_WRITECMD) ;
}

/* read a byte from the PCI interface prom (only 4013E and 4013XLA) */
static u_char
xread(EdtDev * edt_p, u_int addr)
{

	(void)tst_read(edt_p, EDT_FLASHROM_ADDR);
	tst_write(edt_p, EDT_FLASHROM_ADDR, EN_HIGH_ADD | addr) ;
	(void)tst_read(edt_p, EDT_FLASHROM_ADDR);
	return (u_char) (tst_read(edt_p, EDT_FLASHROM_DATA));
 
}


/* ?reset? 4013e or 4013xla */
static void 
xreset(EdtDev * edt_p)
{
    xwrite(edt_p, 0x5555, 0xaa);
    xwrite(edt_p, 0x2aaa, 0x55);
    xwrite(edt_p, 0x5555, 0xf0);
}


/*
* write a byte to the PROM thru the boot controller on the 4028xla
*/
static void
bt_write(EdtDev * edt_p, u_int addr, u_char val)
{
    /* write the address out in three chunks to the boot controller */
    /* first all commands are off */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    /* first byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_LO | (addr & 0xff));
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    /* second byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_MID | ((addr >> 8) & 0xff));
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);

    /* third byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_HI | ((addr >> 16) & 0xff));
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);

    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_ROM | (val & 0xff));
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
}



/* ?reset? the 4028xla */
static void 
bt_reset(EdtDev * edt_p)
{
    bt_write(edt_p, 0x555, 0xaa);
    bt_write(edt_p, 0x2aa, 0x55);
    bt_write(edt_p, 0x555, 0xf0);
}

/* ?reset? for xc5vlx30t, xc5vlx50t, ep2sgx30d */
static void 
bt2_reset(EdtDev * edt_p)
{
    bt_write(edt_p, 0x0, 0xf0);
}


static u_char
xreadstat(EdtDev * edt_p)
{
    u_char  stat;
    uint_t  rdval;

    /*
    * disable the high address drive by writing 0 to bit24 of FPROM address
    * register.
    */
    tst_write(edt_p, EDT_FLASHROM_ADDR, 0);
    rdval = tst_read(edt_p, EDT_FLASHROM_DATA);
    stat = (u_char) (rdval >> 8);
    return (stat);
}

/*
* read a byte from the 4028xla prom through the boot controller.
*/
static u_char
bt_read(EdtDev * edt_p, u_int addr)
{
    u_char  stat;

    /* write the address out in three chunks to the boot controller */
    /* first all commands are off */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    /* first byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_LO | (addr & 0xff));
    /* clear BT_LD_LO */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    /* second byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_MID | ((addr >> 8) & 0xff));
    /* clear BT_LD_MID */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    /* third byte of address */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_LD_HI | ((addr >> 16) & 0xff));
    /* clear BT_LD_HI , set rd command and disable output buffer */
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_RD_ROM);

    /* read the byte */
    stat = (u_char) tst_read(edt_p, EDT_FLASHROM_DATA);
    /* all commands are off */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    return (stat);
}

/*
* read the jumper position and bus voltage from the boot controller
*/
static u_char
bt_readstat(EdtDev * edt_p)
{
    u_char  stat;
    uint_t rdval;

    /*
    * read the boot controller status A0 and A1 low with read asserted -
    * enables boot controller on bus
    */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    tst_write(edt_p, EDT_FLASHROM_DATA, BT_EN_READ | BT_READ);
    rdval = tst_read(edt_p, EDT_FLASHROM_DATA);
    stat = (u_char) rdval;
    /* reset all boot command */
    tst_write(edt_p, EDT_FLASHROM_DATA, 0);
    return (stat);
}



void
edt_x_reset(EdtDev *edt_p, int xtype)
{
    switch(xtype)
    {
    case XTYPE_SPI:
	spi_reset(edt_p);
	break;
    case XTYPE_BT:
	bt_reset(edt_p);
	break;
    case XTYPE_BT2:
	bt2_reset(edt_p);
	break;
    case XTYPE_X:
	xreset(edt_p) ;
	break;
    }
}


/*
 * get the sector size -- from array for older (pre PCIe), 1st 4 bytes of PROM
 * for newer (PCIe8, etc.) 
 */
u_int
edt_get_fpga_sectorsize(EdtDev *edt_p, Edt_prominfo *ep)
{
    u_int tmpval;

    if (ep->xtype == XTYPE_BT)
    {
	tmpval = bt_read(edt_p, 0x0)        |
		(bt_read(edt_p, 0x1) << 8)  | 
		(bt_read(edt_p, 0x2) << 16) | 
		(bt_read(edt_p, 0x3) << 24) ;
	return tmpval;
    }
    else return ep->sectorsize;
}

/*
 * new fpgas (pcie8, etc) info addr is indexed from 2nd 4 bytes of prom
 */
u_int
edt_get_fpga_info_index(EdtDev *edt_p)
{
    u_int tmpval;

    tmpval = bt_read(edt_p, 0x4)        |
	    (bt_read(edt_p, 0x5) << 8)  | 
	    (bt_read(edt_p, 0x6) << 16) | 
	    (bt_read(edt_p, 0x7) << 24) ;
    return tmpval;
}


/*
 * get the onboard PROM device info from the string at the end of the segment
 *  NOTE: don't call directly -- instead use edt_get_promaddrs
 */
u_int
edt_get_id_addr(int promcode, int segment)
{
    u_int id_addr;
    Edt_prominfo *ep = edt_get_prominfo(promcode);

    if (ep->sectorsize == 0)
	return 1;

    switch(promcode)
    {

	case AMD_4013E:
	    id_addr = (ep->sectsperseg * ep->sectorsize) - PCI_ID_SIZE;
	    break;

	case SPI_XC3S1200E:
	    id_addr = segment * ep->sectorsize;
	    break;

	default: 
	    id_addr = ((segment + 1) * ep->sectsperseg * ep->sectorsize) - PCI_ID_SIZE;
	    break;

    }
    return id_addr;
}


/*
 * get id addresses, ==>including serial numbers<== (since they're not all the same 
 * any more -- specifically with newer PCIE (XC3S1200E, XCVLX30/50T) we put the ID
 * at the beginning not the end, and have up to 1024 bytes (though the sizes,
 * particularly OSN, are still the same so far) (OSN will stay the same I think too)
 */
u_int
edt_get_promaddrs(EdtDev *edt_p, int promcode, int segment, 
                 EdtPromIdAddresses *paddr)
            
{
    Edt_prominfo *ep = edt_get_prominfo(promcode);
    char tst[4];
    /* u_int info_addr = ep->id_addr((u_long)edt_p, segment); */
	 /* STORE MORE STUFF HERE ?? */

    paddr->id_addr = edt_get_id_addr(promcode, segment);

    /*
     * SPI devices: 
     *   PROM ID 128
     *   OSN 32
     *   ESN 64 (or more up to total of 1024)
     *   data
     */
    if (ep->xtype == XTYPE_SPI)
    {
	paddr->osn_addr = paddr->id_addr + PCI_ID_SIZE;
	paddr->esn_addr = paddr->osn_addr + OSN_SIZE;
        paddr->extra_tag_addr = paddr->esn_addr + ESN_SIZE;
        paddr->extra_size_addr = paddr->extra_tag_addr + 4;
        paddr->extra_data_addr = paddr->extra_size_addr + 4;

        
    }
    /*
     * All other devices:
     *   data (0 up to segment size minus 128 [PCI_ID_SIZE])
     *   .
     *   .
     *   .
     *   OSN 32
     *   ESN 64 
     *   PROM ID 128
     */
    else
    {
	paddr->osn_addr = paddr->id_addr - OSN_SIZE;
	paddr->esn_addr = paddr->osn_addr - ESN_SIZE;
        paddr->extra_tag_addr = paddr->esn_addr - 4;
        paddr->extra_size_addr = paddr->extra_tag_addr - 4;
        /* we don't know extra_data_addr yet */

    }

    read_x_block(edt_p, paddr->extra_tag_addr, (u_char *)tst, 4, ep->xtype);

    if (strncmp(tst,"XTR:",4) == 0)
    {
    
        read_x_block(edt_p, paddr->extra_size_addr, (u_char *)&paddr->extra_size, 4, ep->xtype);

        if (needswap)
        {
            paddr->extra_size = swap32(paddr->extra_size);
        }
        if (ep->xtype != XTYPE_SPI)
            paddr->extra_data_addr = paddr->extra_size_addr - (4 + paddr->extra_size);

    }
    else
        paddr->extra_size = 0;

    return paddr->id_addr;
}


/*
 * addresses for id information, per xilinx type
 */
u_int
ida_4013e(void *dmy, int segment)
{
    return (8 * E_SECTOR_SIZE) - PCI_ID_SIZE;
}

u_int
ida_4013xla(void *dmy, int segment)
{
    return (segment * AMD_SECTOR_SIZE) + AMD_SECTOR_SIZE - PCI_ID_SIZE;
}

u_int
ida_4028xla(void *dmy, int segment)
{
    return ((segment + 1) * 2 * AMD_SECTOR_SIZE) - PCI_ID_SIZE;
}

u_int
ida_xc2s100(void *dmy, int segment)
{
    return ((segment + 1) * 4 * AMD_SECTOR_SIZE) - PCI_ID_SIZE;
}

u_int
ida_xc3s1200e(void *dmy, int segment)
{
    return segment * SPI_SECTOR_SIZE;
}


u_int
ida_indexed(void * edt_p , int dmy)
{
    return edt_get_fpga_info_index((EdtDev *)edt_p);
}

void tst_init(EdtDev *edt_p)
{

#ifndef _KERNEL

    if (!edt_p->mapaddr)
        edt_p->mapaddr = (caddr_t)edt_mapmem(edt_p, 0, 256) ;

#endif

    needswap = !edt_little_endian();
}

static void
tst_write(EdtDev * edt_p, uint_t desc, uint_t val)
{

#ifndef _KERNEL
    u_int dmy;

    if (edt_p->mapaddr)
    {

	tstaddr = (volatile u_int *)(edt_p->mapaddr + (desc & 0xff)) ;

	if (needswap)
	{
	    *tstaddr = swap32(val) ;
	}
	else
	{
	    *tstaddr = val ;
	}

	dmy = *tstaddr;
    }
    else
#endif
        edt_set(edt_p, desc, val) ;

}

static uint_t
tst_read(EdtDev * edt_p, uint_t desc)
{

#ifndef _KERNEL
    
    if (edt_p->mapaddr)
    {

	tstaddr = (volatile u_int *)(edt_p->mapaddr + (desc & 0xff)) ;

        if (needswap)
	{
	    return (swap32(*tstaddr)) ;
	}
	else 
            return  (*tstaddr) ;

    }
    else
#endif
        return(edt_get(edt_p, desc)) ;
}

void tst_pflash(EdtDev *edt_p, uint_t addr, uint_t val)
{

    tst_write(edt_p, EDT_FLASHROM_DATA, (u_char) 0xaa);
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_ADDR, (u_int) (0x5555 | EDT_WRITECMD));
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_DATA, (u_char) 0x55);
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_ADDR, (u_int) (0x2aaa | EDT_WRITECMD));
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_DATA, (u_char) 0xa0);
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_ADDR, (u_int) (0x5555 | EDT_WRITECMD));
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_DATA, (u_char) val);
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
    tst_write(edt_p, EDT_FLASHROM_ADDR, 
	(u_int) (EN_HIGH_ADD | addr | EDT_WRITECMD));
    (void) tst_read(edt_p, EDT_FLASHROM_ADDR);
}

/*
* read a string out of the xilinx, of a given size, from a given address.
* used for Xilinx ID id string and serial number strings
*
* ARGUMENTS
*   edt_p     pointer to already opened edt device
*   addr      address to start reading the string
*   buf       buffer to read the string into
*   size      number of bytes to read
*
* RETURNS
*   pointer to magic first valid ASCII character in string (should be
*   0th but may not be, esp. if blank)
*
*/
char *
read_x_string(EdtDev *edt_p, u_int addr, char *buf, int size, int xtype)
{
    int  i;
    char *ret = 0;
    u_char  val;

    /* EDTPRINTF(edt_p,1,("full string = ")); */
    for (i = 0; i < size; i++)
    {
	switch(xtype)
	{
	case XTYPE_SPI:
	    spi_reset(edt_p);
	    spi_read(edt_p, (u_int)addr, &val, 1);
	    break;
	case XTYPE_BT:
	case XTYPE_BT2:
	    val = bt_read(edt_p, (u_int)addr);
	    break;
	case XTYPE_X:
	    val = xread(edt_p, (u_int)addr);
	    break;
	}
	buf[i] = val;
	addr++;
	/* EDTPRINTF(edt_p,1,("%x ", val)); */
	/* record start of string */
	if (!ret && val >= 0x20 && val <= 0x7e)
	    ret = &buf[i];
    }
    buf[i] = 0;
    return ret;
}

/*
* read a string out of the xilinx, of a given size, from a given address.
* used for Xilinx ID id string and serial number strings
*
* ARGUMENTS
*   edt_p     pointer to already opened edt device
*   addr      address to start reading the string
*   buf       buffer to read the string into
*   size      number of bytes to read
*
* RETURNS
*
*/

char *
read_x_block(EdtDev *edt_p, u_int addr, u_char *buf, int size, int xtype)
{
    int  i;
    char *ret = 0;
    u_char  val;

    /* EDTPRINTF(edt_p,1,("full string = ")); */
    for (i = 0; i < size; i++)
    {
	switch(xtype)
	{
	case XTYPE_SPI:
	    spi_reset(edt_p);
	    spi_read(edt_p, (u_int)addr, &val, 1);
	    break;
	case XTYPE_BT:
	case XTYPE_BT2:
	    val = bt_read(edt_p, (u_int)addr);
	    break;
	case XTYPE_X:
	    val = xread(edt_p, (u_int)addr);
	    break;
	}
	buf[i] = val;
	addr++;

    }
    
    return (char *) buf;
}

int
edt_sector_erase(EdtDev * edt_p, u_int sector, u_int sec_size, int xtype)
{
    int result = 0;

    switch(xtype)
    {
    case XTYPE_SPI:
	spi_sector_erase(edt_p, sector << 16);
	break;
    case XTYPE_BT:
	bt_sector_erase(edt_p, sector, sec_size);
	break;
    case XTYPE_BT2:
	result = bt2_sector_erase(edt_p, sector, sec_size);
	break;
    case XTYPE_X:
	xsector_erase(edt_p, sector, sec_size);
	break;
    }
    return(result);
}


int
edt_x_prom_detect(EdtDev * edt_p, u_char *stat)
{
    u_int   desc = EDT_FLASHROM_DATA;
    u_int   flashrom_bits, idbits;
    u_char  mid, devid;
    int     prom;

    EDTPRINTF(edt_p, 2, ("edt_x_prom_detect\n"));

    /* set up structures for direct peek/poke */
    tst_init(edt_p) ;

    /*
    * if the BT_A0 bit in the PROM_DATA can be written high
    * this must be a new board with a 4028xla or XC2S___
    */

    tst_write(edt_p, desc, BT_A0);
    flashrom_bits = tst_read(edt_p, desc) & BT_A0;

    if (flashrom_bits == BT_A0)
    {
	EDTPRINTF(edt_p,2,("  FLASHROM command BT_A0 can be set board has boot controller\n"));

	*stat = bt_readstat(edt_p);
	idbits = (u_int)((*stat & STAT_IDMASK) >> STAT_IDSHFT);
	EDTPRINTF(edt_p,2,("\t\tRead code %02x id bits %x\n", *stat, idbits));


	    switch (idbits)
	    {
        case STAT_EP2SGX30D_A:
		    prom = AMD_EP2SGX30D_A;
		    break;
        case STAT_EP2SGX30D:
		    prom = AMD_EP2SGX30D;
		    break;
		case STAT_XC3S1200E:
		    prom = SPI_XC3S1200E;
		    break;
		case STAT_XC5VLX30T:
		    prom = AMD_XC5VLX30T;
		    break;
		case STAT_XC5VLX50T:
		    prom = AMD_XC5VLX50T;
		    break;
		case STAT_XC5VLX70T:
		    prom = AMD_XC5VLX70T;
		    break;
		case STAT_XC4028XLA:
		    prom = AMD_4028XLA;
		    break;
		case STAT_XC2S150:
		    prom = AMD_XC2S150;
		    break;
		case STAT_XC2S200_NP:
		    prom = AMD_XC2S200_4M;
		    break;
		case STAT_XC2S200:
		    prom = AMD_XC2S200_8M;
		    break;
		case STAT_XC2S100:
		    prom = AMD_XC2S100_8M;
		    break;
		case STAT_XC5VLX30T_A:
		    prom = AMD_XC5VLX30T_A;
		    break;
		case STAT_XC6SLX45:
		    prom = AMD_XC6SLX45;
		    break;
		default:
		    prom = PROM_UNKN;
		    break;
	    }


    }
    else
    {
	EDTPRINTF(edt_p,2,("  BT_A0 wrote, read back %02x, must not have boot controller\n", flashrom_bits));
	prom = PROM_UNKN;
    }
    /*
    * if prom code is not set yet test for other prom types by reading 
    * manufacturer code in flash prom.
    */
    if (prom == PROM_UNKN)  
    {
	int i;

	edt_x_reset(edt_p, 0);

	/* this code borrowed from xautoselect() */
	xwrite(edt_p, 0x5555, 0xaa);
	xwrite(edt_p, 0x2aaa, 0x55);
	xwrite(edt_p, 0x5555, 0x90);
	mid = xread(edt_p, 0x0);
	devid = xread(edt_p, 0x1);

	for (i = 0; older_devices[i].prom != 0; i++)
	{
	    if (mid == older_devices[i].mid && devid == older_devices[i].devid)
	    {
		prom = older_devices[i].prom;
		break;
	    }
	}

	    EDTPRINTF(edt_p,2,("  edt_x_prom_detect: autoselect manuf id %02x devid %02x code %d\n",
	    mid, devid, prom));
	edt_x_reset(edt_p, 0);

	*stat = xreadstat(edt_p);
    }
    return (prom);
}


EdtPromParmBlock *
edt_get_parms_block(EdtPromData *pdata, char *id)

{
    EdtPromParmBlock *p;

    p = (EdtPromParmBlock *) pdata->extra_buf;

    if (p->size > (u_int) pdata->extra_size)
        return BAD_PARMS_BLOCK;

    while (p->size && ((u_char *) p - pdata->extra_buf) < pdata->extra_size)
    {
        if (strncmp(id, p->type,4) == 0)
            return p;
        p = (EdtPromParmBlock *) ((u_char *) p + p->size);
    }

    return NULL;

}
EdtPromParmBlock *edt_add_parmblock(EdtPromData *pdata, char *type,  int datasize)

{
    
    EdtPromParmBlock *p;

    p = edt_get_parms_block(pdata, type);

    if (p)
        return NULL;

    if (pdata->extra_size + datasize + 8 > PROM_EXTRA_SIZE)
        return NULL;

    p = (EdtPromParmBlock *) (pdata->extra_buf + pdata->extra_size);
    strncpy(p->type,type,4);
    p->size = datasize;
    
    pdata->extra_size += 8 + datasize;

    return p;
}




void
edt_x_program_extra(EdtDev *edt_p, int promcode, EdtPromData *pdata, int sect)
{
    u_int   id_addr;
    Edt_prominfo *ep = edt_get_prominfo(promcode);
    int sector;
    EdtPromIdAddresses addr;
    char *key_str = "XTR:";

    sector = (sect == IS_DEFAULT_SECTOR)? ep->defaultseg:sect;

    if ((id_addr = edt_get_promaddrs(edt_p, promcode, sector, &addr)) == 1)
    {
	EDTPRINTF(edt_p, 0, ("invalid device; no ID info\n"));
	return;
    }

    edt_x_reset(edt_p, ep->xtype);

    if (pdata->extra_size)
    {
        u_int size;

        size = pdata->extra_size;
        if (!edt_little_endian())
            size = swap32(size);

        if (ep->xtype != XTYPE_SPI)
            addr.extra_data_addr = addr.extra_size_addr - (4 + pdata->extra_size);
        
        edt_x_block_program(edt_p, addr.extra_tag_addr, (u_char *) key_str, 4, ep->xtype);
        edt_x_block_program(edt_p, addr.extra_size_addr, (u_char *) &size, 4, ep->xtype);
        edt_x_block_program(edt_p, addr.extra_data_addr, pdata->extra_buf, pdata->extra_size, ep->xtype);
    }
   
    edt_x_reset(edt_p, ep->xtype);
    
}

static void
edt_program_sn_str(EdtDev *edt_p, u_int addr, char *str, char *tag, int size, int xtype)

{
    char    sn_str[ESN_SIZE];
    if (str[0])
    {
	edt_x_reset(edt_p, xtype);
	edt_zero(sn_str, size);
	if (strncmp(str, "erase", 5) != 0)
	{
	    strcpy(sn_str, tag);
	    strcat(sn_str, str);
	}
	
	edt_x_block_program(edt_p, addr, (u_char *)sn_str, size, xtype);
    }
}


void
edt_program_promdata(EdtDev *edt_p, 
                int promcode, 
                int sector, 
                EdtPromData *pdata)
{

    Edt_prominfo *ep = edt_get_prominfo(promcode);

    EdtPromIdAddresses addr;

    /* get addresses */
    addr.id_addr = edt_get_promaddrs(edt_p, promcode, sector, &addr);

    EDTPRINTF(edt_p, 2, 
        ("\nedt_program_promdata id_addr %x oemsn addr %x edtsn addr %x\n", 
        addr.id_addr, addr.osn_addr, addr.esn_addr));

    edt_program_sn_str(edt_p, addr.osn_addr,pdata->osn, "OSN:", OSN_SIZE, ep->xtype);

    edt_program_sn_str(edt_p, addr.esn_addr,pdata->esn, "ESN:", ESN_SIZE, ep->xtype);

    edt_x_program_extra(edt_p, promcode, pdata, sector);
}

/*
 * return a string containing the Xilinx type; mainly for debug output
 */
char *
edt_x_type_string(int xtype)
{
    switch (xtype)
    {
    	case XTYPE_X: return("XTYPE_X");
    	case XTYPE_BT: return("XTYPE_BT");
    	case XTYPE_LTX: return( "XTYPE_LTX");
    	case XTYPE_SPI: return( "XTYPE_SPI");
    	case XTYPE_BT2: return( "XTYPE_BT2");
	default: return("UNKNOWN");
    }	
}

/*
* get the onboard PROM device info from the string at the end of the segment
* TODO: maybe implement larger PCIE_ID_SIZE
*/
void 
edt_read_prom_data(EdtDev *edt_p, int promcode, int segment, 
             EdtPromData *pdata)

{
    Edt_prominfo *ep = edt_get_prominfo(promcode);
    char    idbuf[PCI_ID_SIZE + 1];
    char    oemsnbuf[OSN_SIZE + 1];
    char    edtsnbuf[ESN_SIZE + 1];

    char    *ret;

    EdtPromIdAddresses addr;

    edt_zero(pdata,  sizeof(EdtPromData));

    /* get addresses */
    addr.id_addr = edt_get_promaddrs(edt_p, promcode, segment, &addr);

    EDTPRINTF(edt_p, 2, 
        ("\nedt_read_prom_data id_addr %x oemsn addr %x edtsn addr %x\n", 
        addr.id_addr, addr.osn_addr, addr.esn_addr));

    /* BITFILE ID */
    if ((ret = read_x_string(edt_p, addr.id_addr, idbuf, PCI_ID_SIZE, ep->xtype)) != NULL) 
	strncpy(pdata->id, ret, PCI_ID_SIZE);

    /* OSN */
    if ((ret = read_x_string(edt_p, addr.osn_addr, oemsnbuf, OSN_SIZE, ep->xtype)) != NULL)
    {
        if (strncmp(ret, "OSN:", 4) == 0)
        {
	    strncpy(pdata->osn, &ret[4], OSN_SIZE);
	    if (pdata->osn[strlen(pdata->osn)-1] == ':')
	        pdata->osn[strlen(pdata->osn)-1] = '\0';
        }
    }

    /* ESN */
    if ((ret = read_x_string(edt_p, addr.esn_addr, edtsnbuf, ESN_SIZE, ep->xtype)) != NULL)
	if (strncmp(ret, "ESN:", 4) == 0)
	    strncpy(pdata->esn, &ret[4], ESN_SIZE);

    /* EXTRA  */

    if (addr.extra_size)
    {
        if ((ret = read_x_block(edt_p, addr.extra_data_addr, pdata->extra_buf, addr.extra_size, ep->xtype)) != NULL)
        {
            pdata->extra_size = addr.extra_size;
        }
    }

#ifndef _KERNEL
    if (pdata->esn)
        edt_parse_esn(pdata->esn, &pdata->ei);
#endif

    edt_x_reset(edt_p, ep->xtype);
}

#ifndef _KERNEL

/** Parse the board's embedded information string.
* 
* During manufacturing programming, EDT embeds selected information is embedded into an unused
* area of the Xilinx FPGA PROM. This information is preserved across reloads
* (via pciload, hubload, etc.) unless options to overwrite are invoked in one
* of those utilities. This subroutine takes as an argument the full information
* string, as retrieved from #edt_get_esn, #edt_get_osn or #edt_get_sns, into the
* fields indicated by the #Edt_embinfo structure.
*    
* (To see the information string, run \e pciload with no arguments.)
* 
* @note Information embedding was implemented in Sept. 2004; boards shipped before
* that time will yield a string with all NULLS unless the board's FPGA
* has since been updated with the embedded information.
*
* @param str embedded inforamtion string, with information from one of
* the serial number retrieval subroutines (edt_get_esn, etc.)
* @param ei #Edt_embinfo structure into which the the parsed information will be put
* @see edt_get_esn, edt_fmt_pn
*/
int
edt_parse_esn(char *str, Edt_embinfo *ei)
{
    unsigned int i,j=0;
    int n;
    int nfields=0;
    char sn[128], pn[128], ifx[128], rev[128], clock[128], opt[128];
    char tmpstr[128];

    edt_zero(ei,sizeof(*ei));

    sn[0] = pn[0] = clock[0] = opt[0] = ifx[0] = '\0';

    for (i=0; i<strlen(str); i++)
    {
	tmpstr[j++] = str[i];
	if (str[i] == ':')
	{
	    ++nfields;
	    if (str[i+1] == ':') /* insert space if empty field */
		tmpstr[j++] = ' ';
	}
    }
    tmpstr[j] = '\0';

    if (nfields < 4)
	return -1;


    n = sscanf(tmpstr, "%[^:]:%[^:]:%[^:]:%[^:]:%[^:]:%[^:]:", sn, pn, clock, opt, rev, ifx);

    /* first pass we had 10-digit p/ns; still may be some around that have that */
    if (strlen(sn) > 10)
	sn[10] = '\0';

    if ((strlen(pn) > 10) || (strlen(opt) > 10) || !isdigit_str(clock))
	return -1;

    if (n < 4)
	return -1;

    if (n < 5)
	rev[0] = '\0';

    if (n < 6)
	ifx[0] = '\0';

    strcpy(ei->sn, sn);
    strcpy(ei->pn, pn);
    strcpy(ei->opt, opt);
    strcpy(ei->ifx, ifx);
    ei->clock = edt_strtol(clock,0,0);
    ei->rev = edt_strtol(rev,0,0);

    return 0;
}

#endif
