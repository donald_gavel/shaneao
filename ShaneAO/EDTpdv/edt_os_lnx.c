
#include "edt_utils.h"

#include "edt_error.h"

#include <time.h>


HANDLE edt_open_datafile(const char *path,
			   const char *szFileName,
			   u_char writing,
			   u_char direct,
			   u_char truncate)

{

    int ok = 0;

    HANDLE pFile;

    int rwmode;

    int createmode;

    int flags = 0;

    char name[256];

    if (path)
	sprintf(name, "%s/%s", path, szFileName);
    else
	sprintf(name, "%s",szFileName);
    

    if (direct)
	flags = O_DIRECT;
    
    if (writing)
    {
        if (truncate)
	    flags |= O_TRUNC;
        pFile = open(name, flags | (O_CREAT | O_RDWR), 0777);
    }
    else
        pFile = open(name, flags | O_RDONLY);

    if (pFile == -1)
    {
	perror("Open");
	edt_msg(EDT_MSG_FATAL,
		"Failed to open %s for %s\n", name, (writing)?"write":"read");
   }
    return pFile;
}

void edt_close_datafile(HANDLE f)

{


    close(f);



}

int edt_write_datafile(HANDLE f, void *p, int bufsize)

{

int rc;


    rc = write(f,p,bufsize);

    if (rc == -1)
		{
			printf("Write Error");
			printf("Error on file %d\n", f);
		}

    return rc;

}

int edt_read_datafile(HANDLE f, void *p, int bufsize)

{

int rc;


    rc = read(f,p,bufsize);

    return rc;

}

void *edt_alloc_aligned(int size)

{
    void *p;
    volatile int throwaway_int = posix_memalign(&p,512,size);
    return p;
}

void edt_free_aligned(void *p)

{
    free(p);
}

/**
* Causes the process to sleep for the specified number of milliseconds.
*
* @param msecs  The number of milliseconds for the process to sleep.
*
* @return 0 on success, -1 on failure. If an error occurs, call
* #edt_perror to get the system error message. 
*/

extern int dumping_sleep;

#include <semaphore.h>

int
edt_wait_event_timeout(sem_t *p, int timeout)

{

    int t = 0;

    while (t < timeout && sem_trywait(p))
    {
	edt_msleep(10);
	t += 10;
    }

    if (t < timeout)
	return 0;
    else
	return 1;

}

#include <asm/ioctls.h>
#include <sys/ioctl.h>

static int kbhitedt()
{
    int numchars;
        /* int n, ch ; */
    ioctl(0, FIONREAD, &numchars) ;

    /* for (n=0; n<numchars; n++)  ch=getchar() ;*/
    return(numchars);
}

#include <string.h>

u_char edt_wait_for_console_input(char *line,
								const int timeout,  
								const int maxlen)

{


    int t = 0;

    while (t < timeout && !kbhitedt())
    {
	edt_msleep(10);
	t += 10;
    }

    if (t < timeout)
    {
	volatile char *throwaway_charp = fgets(line, maxlen, stdin);
	if (strlen(line))
	    if (line[strlen(line)-1] == '\n')
		line[strlen(line)-1] = 0;

	return TRUE;
    }
    else
	return FALSE;


}
#include <sys/timeb.h>

#include <time.h>

int edt_get_datestr(char *s, int maxlen)

{
    time_t tm;

    time(&tm);

    strncpy(s,ctime(&tm),maxlen);

    if (s[strlen(s)-1] == '\n')
        s[strlen(s)-1] = 0;

    return 0;
}

uint64_t
edt_get_file_position(HANDLE f)

{
	return lseek(f, 0, SEEK_CUR);

}

uint64_t
edt_get_file_size(HANDLE f)

{


	struct stat buf;



	fstat(f, &buf);

	return buf.st_size;

}

int edt_file_seek(HANDLE f, uint64_t to)

{
    uint64_t rc;

    rc = lseek(f, to, SEEK_SET);

	return (rc != to);

}

/**
* Convenience routine to allocate memory in a system-independent way.
* The buffer returned is page aligned. Uses VirtualAlloc on Windows NT
* systems, valloc on UNIX-based systems.
*
* @param size:	number of bytes of memory to allocate.
*
* @return The address of the allocated memory, or NULL on error. If
* NULL, use #edt_perror to print the error.
*/

unsigned char *
edt_alloc(int size)
{
    unsigned char *ret;

    ret = (unsigned char *) valloc(size);
    if (ret)
    {
	u_char *tmp_p;

	for (tmp_p = ret; tmp_p < ret + size; tmp_p += 4096)
	{
	    /* edt_msg(EDTDEBUG, "touching %x\n",tmp_p) ; */
	    *tmp_p = 0xa5;
	}
    }
    return (ret);
}

/**
* Convenience routine to free the memory allocated with #edt_alloc.
*
* @param ptr   Address of memory buffer to free.
*
* @return 0 if sucessful, -1 if unsuccessful.
*/

void
edt_free(uchar_t *ptr)
{
    free(ptr);
}


/**
* Gets the seconds + fractional seconds since the Epoch, OS independent.
* Accuracy is OS dependent.
*
* @return the timestamp in seconds; a double value with tenths/hundredths/etc on the right of the decimal point.
*/
double
edt_timestamp()
{
    double  dtime;

    struct timeval endtime;

    gettimeofday(&endtime, (struct timezone *) NULL);

    dtime = (double) endtime.tv_sec
	+ (((double) endtime.tv_usec) / 1000000.0);

    return (dtime);	

}

/** return time in seconds since last call */
double
edt_dtime()
{
    double  dtime;

    static struct timeval starttime;
    struct timeval endtime;
    double  start;
    double  end;

    gettimeofday(&endtime, (struct timezone *) NULL);

    start = (double) starttime.tv_sec
	+ (((double) starttime.tv_usec) / 1000000.0);
    end = (double) endtime.tv_sec
	+ (((double) endtime.tv_usec) / 1000000.0);
    dtime = end - start;


    starttime = endtime;
    return (dtime);
}

static double elapsed_start = 0.0;

/** 
* Like #edt_dtime, but doesn't reset start time unless requested.
* @param reset true if start time should be reset. 
*/

double 
edt_elapsed(u_char reset)
{

    if (reset || elapsed_start == 0.0)
    {
	elapsed_start = edt_timestamp();
    }

    return edt_timestamp() - elapsed_start;
}

/**
* Causes the process to sleep for the specified number of milliseconds.
*
* @param msecs  The number of milliseconds for the process to sleep.
*
* @return 0 on success, -1 on failure. If an error occurs, call
* #edt_perror to get the system error message. 
*/
void
edt_msleep(int msecs)
{


	    struct timespec ts;
	    double farg = (double)msecs;

	    ts.tv_sec = (time_t) ((farg * 1000000.0) / 1000000000.0);
	    ts.tv_nsec = (long) (msecs % 1000) * 1000000;

	    (void)nanosleep(&ts, NULL);
}


/**
* causes process to sleep for specified number of microseconds
*
* @param usecs: number of microseconds for process to sleep
*
* @return 0 on success; -1 on error
*/
void
edt_usleep(int usecs)
{
	    struct timespec ts;

	    ts.tv_sec = usecs / 1000000;
	    ts.tv_nsec = (long) (usecs % 1000000) * 1000;

	    (void)nanosleep(&ts, NULL);
}

void
edt_usec_busywait(u_int usec)
{
    double t = 0.0, f_usec = usec / 1000000;

    edt_dtime();    	   /* edt_dtime() returns a double containing the delta time since last called. */
    while (t < f_usec)
	t += edt_dtime();  /* spin until the sum of the deltas equals the input. */
}

/*
* below are utility routines, intended primarily for EDT library
* internal use but available for application use if desired
*/

/**
* fwd_to_back: for use by #edt_correct_slashes
*/
void
edt_fwd_to_back(char *str)
{
    char   *p = str;

    while (*p != '\0')
    {
	if (*p == '/')
	    *p = '\\';
	++p;
    }
}

/**
* back_to_fwd: for use by #edt_correct_slashes
*/
void
edt_back_to_fwd(char *str)
{
    char   *p = str;

    while (*p != '\0')
    {
	if (*p == '\\')
	    *p = '/';
	++p;
    }
}



/**
* change the slashes from forward to back or vice versa depending
* on whether it's Wind*ws or Un*x
*/
void
edt_correct_slashes(char *str)
{
    edt_back_to_fwd(str);
}

/**
* Determines file access, independent of operating system. 
*
* This a convenience routine that maps to access() on Unix/Linux
* systems and _access() on Windows systems.
*
* @param fname     path name of file to check access permissions
* @param perm      permission flag(s) to test for. See documentation
* for access() (Unix/Linux) or _access() (Windows) for valid values.
*
* \Example
* @code
* if(edt_access("file.ras", F_OK))
*     printf("Warning: overwriting file %s\n", "file.ras");
* @endcode
*
* @return 0 on success, -1 on failure
*/

int
edt_access(char *fname, int perm)
{
    int     ret;

    edt_correct_slashes(fname);
    ret = access(fname, perm);
    return ret;
}

/**
* @fn void edt_closedir(DIRHANDLE dirphandle)
*
* Cross platform function to close a directory handle. 
*/

/**
* @fn DIRHANDLE edt_opendir(char *dirname)
*
* Cross platform function to open a directory for reading.
* @param dirname the name of the directory to open.
*/

/**
* @fn int edt_readdir(DIRHANDLE dirphandle, char *name)
*
* Cross platform function to read the next directory entry. 
*
* @param dirphandle a handle for the directory, returned by #edt_opendir
*
* @param name a pointer to a string into which the name of the next
* file in the directory will be written.  The string must be allocated
* by the caller to at least 256 bytes.
*
*/


#include <dirent.h>

DIRHANDLE
edt_opendir(const char *dirname)
{
    return (DIRHANDLE) opendir(dirname);
}

int
edt_readdir(DIRHANDLE dirphandle, char *name)
{
    DIR *dirp = (DIR *) dirphandle;

    struct dirent *dp;

    if ((dp = readdir(dirp)) == NULL)
	return 0;
    strcpy(name, dp->d_name);
    return 1;
}

void
edt_closedir(DIRHANDLE dirphandle)
{
    DIR *dirp = (DIR *) dirphandle;
    closedir(dirp);
}

#include <sys/statvfs.h>

int64_t
edt_disk_free(const char *name)

{
	int rc;
	struct statvfs stat;

	rc = statvfs(name, &stat);

	if (rc == 0)
	{
	    return stat.f_bsize * stat.f_bavail;
	}
	else
	    return 0;
}

