/****************************************************************************
Copyright Notice:
  This computer software is proprietary to VMETRO. The use of this software
  is governed by a licensing agreement. VMETRO retains all rights under
  the copyright laws of the United States of America and other countries.
  This software may not be furnished or disclosed to any third party and
  may not be copied or reproduced by any means, electronic, mechanical, or
  otherwise, in whole or in part, without specific authorization in writing
  from VMETRO.
 
    Copyright (c) 1996-2005 by VMETRO, ASA.  All Rights Reserved.


Module      : vmosa-linux.h

Description : Header file declaring linux in vmosa

Copyright(c): 2004 VMETRO asa. All Rights Reserved.

Revision History:
  01a,06jul2004,meh  Created.

*****************************************************************************/

#ifndef VMOSA_LINUX_INCLUDED
#define VMOSA_LINUX_INCLUDED

#ifndef __KERNEL__
#include <inttypes.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif


#define OK      (0)
#define ERROR   (-1)

#define TRUE    (1)
#define FALSE   (0)

#ifndef NULL
#define   NULL  (0)
#endif


/* Typedefs
 */
#ifndef BOOL
typedef  int             BOOL;
#endif
#ifndef STATUS
typedef int             STATUS;
#endif
#ifndef PWOID
typedef void*           PWOID;
#endif

#ifndef UINT8
#ifndef uint8_t
typedef unsigned char    UINT8;
#else
typedef uint8_t          UINT8;
#endif
#endif

#ifndef UINT16
#ifndef uint16_t
typedef unsigned short   UINT16;
#else
typedef uint16_t         UINT16;
#endif
#endif

/* Despite the careful typing, later code assumes that a UINT32 is
   precisely four bytes; for example, see the register writes in
   vmosaCDpio2DmaController.cpp. */

#ifndef UINT32
#ifndef uint32_t
typedef unsigned int     UINT32;
#else
typedef uint32_t         UINT32;
#endif
#endif

#ifndef UINT
typedef unsigned int     UINT;
#endif

#ifndef DWORD
#ifndef uint64_t
typedef unsigned long    DWORD;
#else
typedef uint64_t         DWORD;
#endif
#endif


#ifdef __cplusplus 

typedef  void (*FUNCPTR)(...);

#else

typedef  void (*FUNCPTR)(DWORD);

#endif



#ifdef __cplusplus
}
#endif



#endif /* VMOSA_LINUX_INCLUDED */


