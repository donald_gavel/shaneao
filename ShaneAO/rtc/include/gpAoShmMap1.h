/*!
 *
 *  @file gpAoShmMap1.h
 *
 *  @brief
 *  This is the header file for the primary shared memory data structure
 *
 *  @b DESCRIPTION:
 *  This is the header file for the primary shared memory data structure
 *  that describes the content and layout of the shared memory
 *  segment containing
 *  FileDataShm, CamData, CentData, DataFlowControl
 *
 *  @b AUTHOR:
 *  SLA             DATE: 2012-11-20
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _SHMMAP1_H
#define _SHMMAP1_H              1

#include "gpAoHrtFileData.h"
#include "WFSCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtHostDiag.h"
#include "TTcam.h"

// Typedefs

// structure for ensuring that all modules know the same sizes and offsets in shared memory
// and for allowing the compiler to align the contents of all the structures
typedef struct
{
    FileDataShmType         FileDataShm;
    WFScamDataType          WFScamDataShm;
    CentDataType            CentDataShm;
    HrtDataFlowControlType  DataFlowControlShm;
#   if ADD_TIP_TILT
    TTcamDataType           TTcamDataShm;
    HrtDataFlowControlType  TTDataFlowControlShm;
#   endif /* ADD_TIP_TILT */
} ShmMap1Type;

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
