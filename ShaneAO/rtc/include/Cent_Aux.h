//  Cent_Aux.h
//  Misc cent stuff

// center of mass descriptor type
typedef struct 
{
    short       subApRow;    // row of sub-apertures that this center of mass cell is in (0 -> 7, -1 = end of list)
    short       pixRows[4];  // pixel rows, within the subApRow, that the 16 pixels are in
    short       pixCols[4];  // pixel columns that the 16 pixels are in
} CentMassDescType;

// this structure contains the following data for each sub-aperture:
//  int16   subApRow    row of sub-apertures that this 4x4 cell is in (0 -> 7, -1 = end of list)
//  int16   pixRows[4]  pixel rows, within the subApRow, that the 16 pixels are in
//  int16   pixCols[4]  pixel columns that the 16 pixels are in
static CentMassDescType 
centMassDescs_0 [ 48 + 1 ] =  // the '+ 1' is for the terminator row
{
    { 1, {0, 1, 2, 3}, {12, 13, 14, 15} },  //               0123   0123   0123   0123
    { 1, {0, 1, 2, 3}, {16, 17, 18, 19} },  //               1xxx   1xxx   1xxx   1xxx
    { 1, {0, 1, 2, 3}, {20, 21, 22, 23} },  //               2xxx   2xxx   2xxx   2xxx
    { 1, {0, 1, 2, 3}, {24, 25, 26, 27} },  //               3xxx   3xxx   3xxx   3xxx

    { 2, {0, 1, 2, 3}, { 8,  9, 10, 11} },
    { 2, {0, 1, 2, 3}, {12, 13, 14, 15} },  //         0123   0123   0123   0123   0123   0123
    { 2, {0, 1, 2, 3}, {16, 17, 18, 19} },  //         1xxx   1xxx   1xxx   1xxx   1xxx   1xxx
    { 2, {0, 1, 2, 3}, {20, 21, 22, 23} },  //         2xxx   2xxx   2xxx   2xxx   2xxx   2xxx
    { 2, {0, 1, 2, 3}, {24, 25, 26, 27} },  //         3xxx   3xxx   3xxx   3xxx   3xxx   3xxx
    { 2, {0, 1, 2, 3}, {28, 29, 30, 31} }, 

    { 3, {0, 1, 2, 3}, { 4,  5,  6,  7} },  
    { 3, {0, 1, 2, 3}, { 8,  9, 10, 11} },  
    { 3, {0, 1, 2, 3}, {12, 13, 14, 15} },  //  0123   0123   0123   0123   0123   0123   0123   0123
    { 3, {0, 1, 2, 3}, {16, 17, 18, 19} },  //  1xxx   1xxx   1xxx   1xxx   1xxx   1xxx   1xxx   1xxx
    { 3, {0, 1, 2, 3}, {20, 21, 22, 23} },  //  2xxx   2xxx   2xxx   2xxx   2xxx   2xxx   2xxx   2xxx
    { 3, {0, 1, 2, 3}, {24, 25, 26, 27} },  //  3xxx   3xxx   3xxx   3xxx   3xxx   3xxx   3xxx   3xxx
    { 3, {0, 1, 2, 3}, {28, 29, 30, 31} },
    { 3, {0, 1, 2, 3}, {32, 33, 34, 35} },

    { 4, {0, 1, 2, 3}, { 4,  5,  6,  7} },
    { 4, {0, 1, 2, 3}, { 8,  9, 10, 11} },  //  0123   0123   0123                 0123   0123   0123
    { 4, {0, 1, 2, 3}, {12, 13, 14, 15} },  //  1xxx   1xxx   1xxx                 1xxx   1xxx   1xxx
    { 4, {0, 1, 2, 3}, {24, 25, 26, 27} },  //  2xxx   2xxx   2xxx                 2xxx   2xxx   2xxx
    { 4, {0, 1, 2, 3}, {28, 29, 30, 31} },  //  3xxx   3xxx   3xxx                 3xxx   3xxx   3xxx
    { 4, {0, 1, 2, 3}, {32, 33, 34, 35} },

    { 5, {0, 1, 2, 3}, { 4,  5,  6,  7} },
    { 5, {0, 1, 2, 3}, { 8,  9, 10, 11} },  //  0123   0123   0123                 0123   0123   0123
    { 5, {0, 1, 2, 3}, {12, 13, 14, 15} },  //  1xxx   1xxx   1xxx                 1xxx   1xxx   1xxx
    { 5, {0, 1, 2, 3}, {24, 25, 26, 27} },  //  2xxx   2xxx   2xxx                 2xxx   2xxx   2xxx
    { 5, {0, 1, 2, 3}, {28, 29, 30, 31} },  //  3xxx   3xxx   3xxx                 3xxx   3xxx   3xxx
    { 5, {0, 1, 2, 3}, {32, 33, 34, 35} },

    { 6, {0, 1, 2, 3}, { 4,  5,  6,  7} },  
    { 6, {0, 1, 2, 3}, { 8,  9, 10, 11} },  
    { 6, {0, 1, 2, 3}, {12, 13, 14, 15} },  //  0123   0123   0123   0123   0123   0123   0123   0123
    { 6, {0, 1, 2, 3}, {16, 17, 18, 19} },  //  1xxx   1xxx   1xxx   1xxx   1xxx   1xxx   1xxx   1xxx
    { 6, {0, 1, 2, 3}, {20, 21, 22, 23} },  //  2xxx   2xxx   2xxx   2xxx   2xxx   2xxx   2xxx   2xxx
    { 6, {0, 1, 2, 3}, {24, 25, 26, 27} },  //  3xxx   3xxx   3xxx   3xxx   3xxx   3xxx   3xxx   3xxx
    { 6, {0, 1, 2, 3}, {28, 29, 30, 31} },
    { 6, {0, 1, 2, 3}, {32, 33, 34, 35} },

    { 7, {0, 1, 2, 3}, { 8,  9, 10, 11} },
    { 7, {0, 1, 2, 3}, {12, 13, 14, 15} },  //         0123   0123   0123   0123   0123   0123
    { 7, {0, 1, 2, 3}, {16, 17, 18, 19} },  //         1xxx   1xxx   1xxx   1xxx   1xxx   1xxx
    { 7, {0, 1, 2, 3}, {20, 21, 22, 23} },  //         2xxx   2xxx   2xxx   2xxx   2xxx   2xxx
    { 7, {0, 1, 2, 3}, {24, 25, 26, 27} },  //         3xxx   3xxx   3xxx   3xxx   3xxx   3xxx
    { 7, {0, 1, 2, 3}, {28, 29, 30, 31} }, 

    { 8, {0, 1, 2, 3}, {12, 13, 14, 15} },  //               0123   0123   0123   0123
    { 8, {0, 1, 2, 3}, {16, 17, 18, 19} },  //               1xxx   1xxx   1xxx   1xxx
    { 8, {0, 1, 2, 3}, {20, 21, 22, 23} },  //               2xxx   2xxx   2xxx   2xxx
    { 8, {0, 1, 2, 3}, {24, 25, 26, 27} },  //               3xxx   3xxx   3xxx   3xxx

    {-1, {0, 0, 0, 0}, { 0,  0,  0,  0} }   // terminator row
};
