/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtVolts.h
 *
 *  @brief
 *  This is the header file for the Hrt Voltage Conversion  component.
 *
 *  @b DESCRIPTION:
 *  This is the header file for the Hrt Voltage Conversion  component.
 *
 *  @b AUTHOR:
 *  SMJ DATE: 01/07/2009
 *
 *  @b REVISIONS:
 *
 *
 *  01/07/2009: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _VOLTS_H
#define _VOLTS_H        1

// This header file uses #defines from other local header files.

#include "gpAoHrtSysCon.h"

//==============================================================================
// Defines

#define MAX_NUM_PHASE_VOLT_POLY_COEFF   5

#define DM_SLAVE_SWITCH_WFR_BIT 0x2  // bit to turn woofer slaving on and off (0x0 = off, 0x2 = on)
#define DM_SLAVE_SWITCH_TWT_BIT 0x1  // bit to turn tweeter slaving on and off (0x0 = off, 0x1 = on)

enum
{
    eDmShape_None = 0x000,          // MirrorShape - None
    eDmShape_TwtLabFlat = 0x001,    // MirrorShape - Tweeter Lab Flat
    eDmShape_TwtSysFlat = 0x002,    // MirrorShape - Tweeter Sys Flat
    eDmShape_TwtDmShape = 0x004,    // MirrorShape - Tweeter Dm Shape

    eDmShape_WfrLabFlat = 0x010,    // MirrorShape - Tweeter Lab Flat
    eDmShape_WfrSysFlat = 0x020,    // MirrorShape - Tweeter Sys Flat
    eDmShape_WfrDmShape = 0x040,    // MirrorShape - Tweeter Dm Shape

    eDmShape_MaxBit =   0x040       // highest bit value
};


//==============================================================================
// Typedefs



//==============================================================================
// Global Variables

extern  int     hrt_dmSlavingSwitch;  // turns woofer and tweeter slaving on and off

// ************************************
// DM phase (nm) to volts - polynomial
//

extern  float   hrt_twtPhaseToVoltsLookup[ MAX_TWEETER_ACTS ][ TWEETER_LOOKUP_BINS ];       // Calculated from the SRT COEFF
extern  float   hrt_wfrPhaseToVoltsLookup[ MAX_WOOFER_ACTS ] [ WOOFER_LOOKUP_BINS ];        // Calculated from the SRT COEFF
extern  float   hrt_ttMasToVoltsLookup [ MAX_TT_ACTS ]     [ TT_LOOKUP_BINS ];              // Calculated from the SRT COEFF

extern  float   hrt_twtMinMicrons[ MAX_TWEETER_ACTS ];                                      // Calculated from the SRT COEFF
extern  float   hrt_twtMaxMicrons[ MAX_TWEETER_ACTS ];                                      // Calculated from the SRT COEFF
extern  float   hrt_wfrMinMicrons[ MAX_WOOFER_ACTS ];                                       // Calculated from the SRT COEFF
extern  float   hrt_wfrMaxMicrons[ MAX_WOOFER_ACTS ];                                       // Calculated from the SRT COEFF
extern  float   hrt_ttMinMAS[ MAX_TT_ACTS ];                                                // Calculated from the SRT COEFF
extern  float   hrt_ttMaxMAS[ MAX_TT_ACTS ];                                                // Calculated from the SRT COEFF

extern  float   hrt_twtMinVolts[ MAX_TWEETER_ACTS ];                                        // Calculated from the SRT COEFF
extern  float   hrt_twtMaxVolts[ MAX_TWEETER_ACTS ];                                        // Calculated from the SRT COEFF
extern  float   hrt_wfrMinVolts[ MAX_WOOFER_ACTS ];                                         // Calculated from the SRT COEFF
extern  float   hrt_wfrMaxVolts[ MAX_WOOFER_ACTS ];                                         // Calculated from the SRT COEFF
extern  float   hrt_ttMinVolts[ MAX_TT_ACTS ];                                              // Calculated from the SRT COEFF
extern  float   hrt_ttMaxVolts[ MAX_TT_ACTS ];                                              // Calculated from the SRT COEFF

//==============================================================================
// Global Function Prototypes
//

int gpAoHrtVolt_Init( void );

int gpAoHrtVolt_ConvTweeter( float twtPhase[], float twtVolts[], float twtPhaseUnclipped[] );
int gpAoHrtVolt_ConvWoofer(  float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] );
int gpAoHrtVolt_ConvTT(      float ttAngle[],  float ttVolts[],  float ttAngleUnclipped[] );

int gpAoHrtVolt_ConvWooferTst( float tmpWfrPhase[], float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] );  //XXXdwp110516 -- for testing only

int     gpAoHrtVolt_WriteTweeterVolts( float ttVolts[] );
boolean gpAoHrtVolt_WriteWooferVolts( float wfrVolts[], boolean ttOnlyFlag );
int     gpAoHrtVolt_WriteTTVolts( float twtVolts[], boolean physicallyWriteFlag );

int gpAoHrtVolt_OffsetShapesFromFile( void );
int gpAoHrtVolt_SetShapesFromFile( void );

int gpAoHrtVolt_MirrorShapeStart( short cmdID );
int gpAoHrtVolt_MirrorShapeProcess( ReconBuffType* reconBufPtr );
int gpAoHrtVolt_MirrorShapeAbort( void );
int gpAoHrtVolt_StartReg( short actNum, ReconBuffType* reconBufPtr );
int gpAoHrtVolt_ProcessReg( ReconBuffType* reconBufPtr );
int gpAoHrtVolt_StopReg(  ReconBuffType* reconBufPtr );

#endif

//******************************************************************************
// * End of file
//******************************************************************************
