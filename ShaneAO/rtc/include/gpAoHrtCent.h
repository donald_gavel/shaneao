/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtCent.h
 *
 *  @brief
 *  This is the header file for the centroid component.
 *
 *  @b DESCRIPTION:
 *  This is the header file for the centroid component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ   DATE: 11/26/2008
 *
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _CENTROID_H
#define _CENTROID_H     1

// This header file uses #defines from other local header files.

#include "WFSCam.h"

//==============================================================================
// Enums

// Centroid Dark/back/flat modes
enum
{
    eDarkBack_Back,
    eDarkBack_Dark,
    eDarkBack_Flat,
    eDarkBack_None
};

//==============================================================================
// Defines and enums
//
#define INIT_WFSDARKBACK_AVGN 100   // initial number of frames to avg when determining dark or Back calculations
#define INIT_WFSFLAT_AVGN     100   // initial number of frames to avg when determining flat calculation
#define INIT_REFCENT_AVGN     200   // initial number of frames to avg when determining ref centroids

#define INIT_WFS_BACK_APPLY   0  // apply the background subtraction (0: no, 1: yes)
#define INIT_WFS_DARK_APPLY   0  // apply the dark subtraction (0: no, 1: yes)
#define INIT_WFS_FLAT_APPLY   0  // apply the flat adjustment (0: no, 1: yes)

#define INIT_CENT_THRESH      0  // value centroider subtracts from raw data (then,
                                 // > if result < 0, centroider sets raw data to zero)

#define NUM_SUBAPS_PER_ROW (NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP)  // number of subaps in row   // 10
#define NUM_SUBAPS_PER_COL (NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP)   // number of subaps in column    // 10
#define MAX_NUM_SUBAPS     (NUM_SUBAPS_PER_ROW * NUM_SUBAPS_PER_COL)   // number of subaps total    // 100

#define MAX_NUM_CENTROIDS (2 * MAX_NUM_SUBAPS)  // max number of centroids  // 200

#define FLAT_PROCESS_FRATE_WAIT_DIVISOR 10  // # frames to wait after TT move = FRate / FLAT_PROCESS_FRATE_WAIT_DIVISOR

#define NUM_REFCENT_STORE_BUFS 40  // number of reference centroid stored -- 40 is somewhat arbitrary

#define DARK_CORNER_NUM_PIXELS 4  // number of dark-corner pixels used in background / dark level correction

enum
{
    eCalDiagSNRGain,
    eCalDiagSNR,
    eCalDiagGain,
    eCalDiagRMS,
    eCalDiagNumData
};

enum
{
    ePupTrackDataLeft,
    ePupTrackDataRight,
    ePupTrackDataTop,
    ePupTrackDataBottom,
    ePupTrackDataUnfiltX,
    ePupTrackDataUnfiltY,
    ePupTrackDataFiltX,
    ePupTrackDataFiltY,
    ePupTrackLightLevelFlag,
    ePupTrackNumData
};

//==============================================================================
// Typedefs

// CM descriptor type
typedef struct
{
    short   subApRow;    // row of sub-apertures that this quad-cell is in
    short   pixRows[4];  // pixel rows, within the subApRow, that the 4 pixels are in
    short   pixCols[4];  // pixel columns that the 4 pixels are in
} CMDescType;

// quad-cell descriptor type
typedef struct
{
    short   subApRow;    // row of sub-apertures that this quad-cell is in
    short   pixRows[2];  // pixel rows, within the subApRow, that the 4 pixels are in
    short   pixCols[2];  // pixel columns that the 4 pixels are in
} QuadCellDescType;

// centroid buffer type
typedef struct
{
    float     xyCents[MAX_NUM_CENTROIDS];  // X centroids followed by Y centroids
    float     intens[MAX_NUM_SUBAPS];      // total sub-ap intensities

    float     pupTrackData[ePupTrackNumData];  // pupil alignment data

    float     averageIntensity;  // average sub-ap intensity

    short     refCentStoreBufNdx;        // index into refCentStoreBuffers[][] for this frame
    short     refCentCalOffStoreBufNdx;  // index into refCentCalOffStoreBuffers[][] for this frame

    CamBuffType *camBuffPtr;  // this is the camera buffer that the centroids were calced from, simply
                              // > to pass on to the tweeter System Matrix calculator (for now)

    boolean   skipFrameFlag;  // flag indicating that frame should be skipped XXXDWP110914

#if ( TIMER_TEST == 0 )
    long long rdtscFrameStartStamp;  // Time when this frame got to centroider
#endif
} CentBuffType;

// centroid data type
typedef struct
{
    int          cmdStart;                           // index (into CmdTable[]) of command to be started or eCmdNone
    int          cmdCrnt;                            // index (into CmdTable[]) of command currently running or eCmdNone
    int          cmdIntParam[ MAX_NUM_CMD_PARAMS ];  // integer parameter for command, if it has one

    float        cmdFloatParam[ MAX_NUM_CMD_PARAMS ];  // float parameter for command, if it has one

    short        centBufNdx;  // index into centroidBuffs[] for buffer being used by centroider

    short        crntRefCentStoreBufNdx;        // current index into refCentStoreBuffers[][]
    short        crntRefCentCalOffStoreBufNdx;  // current index into refCentCalOffStoreBuffers[][]

//  the following 2 buffers are for storing reference centroids and Cal offsets for diag purposes (instead of
//  > moving them into centroidBuffs[] every frame) and to store the current Cal offsets for the centroider
    float        refCentStoreBuffers[NUM_REFCENT_STORE_BUFS][MAX_NUM_CENTROIDS];
    float        refCentCalOffStoreBuffers[NUM_REFCENT_STORE_BUFS][MAX_NUM_CENTROIDS];

    float        calDiagStoreBuffers[NUM_REFCENT_STORE_BUFS][eCalDiagNumData];  // Cal diagnostic data

    CentBuffType centroidBuffs[NUM_HRT_DATA_BUFFS];  // the centroid buffers

//  the following are used by gpAoDispCentInt.c and gpAoDispCentroid.c (standalone programs on the user side)
    int          centDispWriteBuffNdx;                 // the centroid display buffers currently being written
    int          centDispReadBuffNdx;                  // the centroid display buffers currently being read
    float        centDispCents[2][MAX_NUM_CENTROIDS];  // the centroid display buffers -- X centroids followed by Y centroids
    float        centDispIntens[2][MAX_NUM_SUBAPS];    // the centroid intensity display buffers
} CentDataType;


//==============================================================================
// Global Variables

#if ( TIMER_TEST != 0 )
extern long centFrameID;
#endif

extern int hrt_numSubaps;       // number of wavefront sensor (WFS) sub-apertures
extern int hrt_numCentroids;    // number of centroids

extern int hrt_CentThresh;  // value centroider subtracts from raw data (then, if result is less than zero,
                              // > centroider sets raw data to zero)

extern int hrt_BacksAvgN;    // number of frames to average when determining WFS darkBack values
extern int hrt_FlatsAvgN;    // number of frames to average when determining WFS 'Flat(gain)' values

extern int hrt_applyBackData;    // apply back data ( 0: no, 1: yes )
extern int hrt_wfsBkGrndData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current Back data

extern int hrt_applyDarkData;    // apply dark data ( 0: no, 1: yes )
extern int hrt_wfsDarkData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];    // current dark data

extern int hrt_applyFlatData;    // apply flat data ( 0: no, 1: yes )
extern float hrt_wfsFlatData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current flat data( only valid in pupil area, outside is 1s)

extern int hrt_centAvgN;                            // number of frames to average when determining reference centroids
extern float hrt_currRefCentsQ[MAX_NUM_CENTROIDS];  // current reference centroids for quad-cell ( includes offsets etc. )

extern int hrt_applyCalData;  // apply CAL offset Data ( 0: no, 1: yes )

extern float hrt_centroidGainX;  // gain to apply to X cetroids (primarily to correct for gain difference between X and Y)
extern float hrt_centroidGainY;  // gain to apply to Y cetroids (primarily to correct for gain difference between X and Y)

extern float hrt_overThreshFraction;  // number of sub-aps whose intensity must be over the centThresh for frame to be used  //XXXdwp110816

// variables used in dark-corner background / dark level correction (using unilluminated corner pixels)
extern float hrt_darkCornerCorrectSwitch;  // turns dark level correction on and off
extern int   darkCornerPrevFrameRateNdx;
extern int   darkCornerBoxcarIndex, darkCornerBoxcarN;
extern int   darkCornerBoxcarSums[DARK_CORNER_NUM_PIXELS];
extern float darkCornerBoxcarAves[DARK_CORNER_NUM_PIXELS];
extern float darkCornerCorrection;

extern char hrt_CentGettingBehindFlag;

#define FLATTER_NUM_STEPS 100  //XXXdwp110621 -- for flat field testing -- put this back in gpAoCent.c
extern float flatTestStepsData[FLATTER_NUM_STEPS * 4][FLATTER_NUM_STEPS * 4];  //XXXdwp110621 -- for flat field testing


//==============================================================================
// Global Function Prototypes

void *gpAoHrtCent( void *arg );
void gpAoHrtCent_Restart( void );
void gpAoHrtCent_GetCrntDarkBackFlat( int crntCamFrameRateNdx );

#endif

//******************************************************************************
// * End of file
//******************************************************************************
