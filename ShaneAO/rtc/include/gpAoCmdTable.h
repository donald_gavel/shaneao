/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file cmdTable.h
 *
 *  @brief
 *  This defines the current command list into the RT AOC unit
 *
 *  @b DESCRIPTION:
 *  This file simply defines the command table.  It is in a separate file so that
 *  it can be included by both the HRT and SRT programs.
 *
 *  The fields for CmdTable[] are:
 *          char*           strng;          // pointer to ASCII command string constant
 *          short           idNum;          // command ID number (may or may not be the same as the index into the table!)
 *          short           action;         // action to be taken when command is received
 *          short           loopRestrict    // indicates whether the ao and/or tt loops must be open for a command to execute
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 11/26/2008
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

const cmdTableType CmdTable[ ] =
{
 // 0
 { "quit",                        eCmdQuit,             eCmdActQuit,    eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // quit command
 { "restart",                     eCmdRestart,          eCmdActRestart, eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // restart command
 { "shutdown",                    eCmdShutdown,         eCmdActShtdwn,  eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // shutdown command

 { "setDebug",                    eCmdSetDebug,         eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // set Debug Level ("none", "min", "max")
 { "dumpData",                    eCmdDumpData,         eCmdActToDiag,  eCmdNoCare, {eCmdParmInt,  eCmdParmFlt,   eCmdParmInt } }, // diag command

 { "globalInit",                  eCmdGblInit,          eCmdActGblInit, eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // global_Init command
 { "globalIndex",                 eCmdGblIndex,         eCmdActGblNdx,  eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // global_Index command
 { "globalPark",                  eCmdGblPark,          eCmdActGblPark, eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // global_Park command
 { "globalReboot",                eCmdGblReboot,        eCmdActGblRbt,  eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // global_Reboot command

 { "images",                      eCmdImages,           eCmdActToDiag,  eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // collect raw images (same as dumpData 0x01)
 { "data",                        eCmdData,             eCmdActToDiag,  eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // collect diagnostic data (same as diag 0x0E)
 { "telem",                       eCmdTelem,            eCmdActToDiag,  eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // select telemetry data
 { "trate",                       eCmdTRate,            eCmdActToDiag,  eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // telemetry rate of update (per second)

 { "initWfsCam",                  eCmdWFScamInit,       eCmdActToCam,   eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // initialize SciMeasure WFS camera

 //14
 { "autoCamRate",                 eCmdCamRateAuto,      eCmdActToCam,   eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // auto set camera frame rate (frms per sec)
 { "setCamRate",                  eCmdCamRate,          eCmdActToCam,   eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // set camera frame rate (frms per sec)
 { "camGain",                     eCmdCamGain,          eCmdActToCam,   eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // set camera gain (0, 1, 2, or 3)
 { "getCamRate",                  eCmdCamSRate,         eCmdActToCam,   eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // read frame rate from camera
 { "getCamGain",                  eCmdCamSGain,         eCmdActToCam,   eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // read gain from camera

 // the following command updates offsets, NOT the reference centroids (confusing name)
 { "receiveRefCents",             eCmdRefOffs,          eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // store host 'mas' offsets into Shm
 { "receiveRefCentsCal",          eCmdRefRePhaseOffs,   eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // store host 'mas' Cal RePhase Offs to shm & file
 { "setSimCalData",               eCmdSetSimCalData,    eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // load host phase map into GMB like CAL sub-system
 { "setRefCents",                 eCmdSetRefCents,      eCmdActToCent,  eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // sets cent ref configuration

 //23
 { "offsetRefCents",              eCmdOffsetRefCents,   eCmdActToCent,  eCmdAllOpen,{eCmdParmInt,  eCmdParmInt,   eCmdParmNone} }, // store APO Ref Offs into SHM
 { "receiveApoRCentOffs",         eCmdRcvApoRefOffs,    eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // store host phase map offsets to shm
 { "storeApoRCentOffs",           eCmdStoreApoOffs,     eCmdActToCent,  eCmdAllClsd,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // saves offsets as specified apodizer file
 { "measureRefOffsets",           eCmdSaveRefOffs,      eCmdActToCent,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // saves 'avg' centroid offsets to next default file
 { "measureRefCents",             eCmdSaveRefCents,     eCmdActToCent,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // saves 'avg' ref offset to next default file

 //28
 { "takePixelBacks",              eCmdTakeBacks,        eCmdActToCent,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // determines and stores new background levels
 { "takePixelDarks",              eCmdTakeDarks,        eCmdActToCent,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // determines and stores new dark background levels
 { "takePixelFlats",              eCmdTakeFlats,        eCmdActToCent,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // determines and stores new flat values for WFS

//31
 { "selectDarksBacks",            eCmdSetDrkBck,        eCmdActToCent,  eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // select backs, darks or niether
 { "selectFlatField",             eCmdSetFlats,         eCmdActToCent,  eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // select flat application or not

 { "setCentThresh",               eCmdThresh,           eCmdActToCent,  eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // sets threshold used by centroider on raw data

 { "makeTwtSM",                   eCmdMakeTwtSM,        eCmdActToRecon, eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // make a tweeter System Matrix (a.k.a., poke matrix)

 { "calDataOnOff",                eCmdCalOnOff,         eCmdActToCent,  eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // starts or stops applying Cal data
 { "closeOpenLoops",              eCmdClsOpn,           eCmdActToRecon, eCmdNoCare, {eCmdParmInt,  eCmdParmInt,   eCmdParmInt } }, // closes or opens one or more of the 3 AO loops
 { "closeOpenOFC",                eCmdClsOpnOFC,        eCmdActToRecon, eCmdAllClsd,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // closes or opens the OFC loop
 { "offloadOnOff",                eCmdOffload,          eCmdActToRecon, eCmdNoCare, {eCmdParmInt,  eCmdParmInt,   eCmdParmInt} },  // starts or stops, independently, the m1/m2 offload

//39
 { "tGain",                       eCmdTGain,            eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, // sets AO loop gain (proportional gain)
 { "tInt",                        eCmdTInt,             eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, // sets AO loop integrator gain
 { "statsLog",                    eCmdStatsLog,         eCmdActToDiag,  eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // turns stats/error logging on and off
 { "reserved3",                   eCmdReserved3,        eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, //

 { "wGain",                       eCmdWGain,            eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, // sets AO loop gain (proportional gain)
 { "wInt",                        eCmdWInt,             eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, // sets AO loop integrator gain
 { "setTTOff",                    eCmdTTOffset,         eCmdActToRecon, eCmdAllOpen,{eCmdParmFlt,  eCmdParmFlt,   eCmdParmNone} }, // set tip/tilt offsets
 { "reserved5",                   eCmdReserved5,        eCmdActToRecon, eCmdNoCare, {eCmdParmFlt,  eCmdParmNone,  eCmdParmNone} }, //

 { "setTweeterShape",             eCmdSetTwt,           eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // loads the Twt shape from a host specified file
 { "setWooferShape",              eCmdSetWfr,           eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // loads the Wfr shape from a host specified file
 { "setTweeterOffset",            eCmdTwtOff,           eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // offset twt shape using host named file data
 { "setWooferOffset",             eCmdWfrOff,           eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // offset wfr shape using host named file data

 //51
 { "storeModalGains",             eCmdStorMdlGains,     eCmdActToRecon, eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // stores working modal gains to named file
 { "setGainValues",               eCmdSetHstMdlGains,   eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // load 'host' Modal gains from named file
 { "selectGainSrc",               eCmdSelectGainSrc,    eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // sets overall Optimizer input sources
 { "setGainSrcChoices",           eCmdSetGainSrcs,      eCmdActToHRT,   eCmdNoCare, {eCmdParmStr,  eCmdParmNone,  eCmdParmNone} }, // sets individual Modal Gain selector choices based on file

 { "setShapes",                   eCmdSetShape,         eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // loads the specified pre-stored flat mirror voltage
 { "storeShapes",                 eCmdStoreShape,       eCmdActToRecon, eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // avg  and stores specified actuator voltage & phase

 { "makeInFuncGains",             eCmdMakeInFunc,       eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmInt,   eCmdParmFlt } },  // Stores infuence function data for one mode (sin-cos pair)
 { "selectInFuncGains",           eCmdSelectInFunc,     eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} },  // Selects host defined or all ones for InFuncGain

 { "cycleActuator",               eCmdCycleAct,         eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // starts moving actuator x up and down at 1 Hz
 { "stopCycling",                 eCmdCycleEnd,         eCmdActToRecon, eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // stops moving all actuators that were started with reg

 //61
 { "setTTGain",                   eCmdTTGain,           eCmdActToRecon, eCmdAllOpen,{eCmdParmFlt,  eCmdParmFlt,   eCmdParmNone} }, // set tip/tilt loop gains
 { "setTT",                       eCmdTT_Mov,           eCmdActToRecon, eCmdAllOpen,{eCmdParmFlt,  eCmdParmFlt,   eCmdParmInt } }, // set tip/tilt mirror X actuator (in Volts)

 { "abort",                       eCmdAbort,            eCmdActToHRT,   eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // abort currently running HRT command

 { "readConfig",                  eCmdSysConParms,      eCmdActToHRT,   eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // reads all parameter files
 { "setEngConfiguration",         eCmdEngConfig,        eCmdActEngCfg,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // sets Eng Config values
 { "setObservationConfiguration", eCmdObsConfig,        eCmdActObsCfg,  eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // sets Observation Config Values

 { "initTTcam",                   eCmdTTcamInit,        eCmdActToTTcam, eCmdAllOpen,{eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // initialize TT camera

 { "autoTTcamRate",               eCmdTTcamRateAuto,    eCmdActToTTcam, eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // auto set TT camera frame rate (frms per sec)
 { "setTTcamRate",                eCmdTTcamRate,        eCmdActToTTcam, eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // set TT camera frame rate (frms per sec)
 { "TTcamGain",                   eCmdTTcamGain,        eCmdActToTTcam, eCmdSHOpen, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // set TT camera gain (0, 1, 2, or 3)
 { "getTTcamRate",                eCmdTTcamSRate,       eCmdActToTTcam, eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // read frame rate from TT camera
 { "getTTcamGain",                eCmdTTcamSGain,       eCmdActToTTcam, eCmdNoCare, {eCmdParmNone, eCmdParmNone,  eCmdParmNone} }, // read gain from TT camera

 //67
 { "loopTweet",                   eCmdLoopTweet,        eCmdActToRecon, eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // closes or opens tweeter loop
 { "loopWoof",                    eCmdLoopWoof,         eCmdActToRecon, eCmdNoCare, {eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // closes or opens woofer loop
 { "setShapeTweet",               eCmdSetShapeTweet,    eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // loads the specified pre-stored tweeter shape
 { "setShapeWoof",                eCmdSetShapeWoof,     eCmdActToRecon, eCmdAllOpen,{eCmdParmInt,  eCmdParmNone,  eCmdParmNone} }, // loads the specified pre-stored woofer shape
 { "",                            eCmdEndFlag,              -1,            0,       {    0,             0,            0,      } }   // End of list flag

};

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
