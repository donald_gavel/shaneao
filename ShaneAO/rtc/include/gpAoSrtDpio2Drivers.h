/*******************************************************************************
 *
 *  COPYRIGHT (C) 2010
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoSrtDpio2Drivers.h
 *
 *  @brief 
 *  This is the header file for the user-side dpio2 drivers interface component.
 *
 *  @b DESCRIPTION:
 *  This is the header file for the user-side dpio2 drivers interface component.
 *
 *  @b AUTHOR:
 *  DWP DATE: 03/03/2010
 *
 *  @b REVISIONS:
 *  
 *******************************************************************************
 */

#ifndef _DPIO2_USER_H
#define _DPIO2_USER_H       1


// Defines

// these assignments for the woofer and tweeter are arbitrary; but, of
// course, they must match the boards the DMs are plugged into (determined
// by bus order)
#define dpio2DevnoWFR 1 
#define dpio2DevnoTWT 0

#define dpio2StrobeFreq (50 * 1000 * 1000)  // 50 MHz is the fastest allowable
// #define dpio2StrobeFreq (20 * 1000 * 1000)  // strobe frequency for testing

#define sizeInBytesWFR   208
#define sizeInBytesTWT  2048	// 1024 actuators * 2 bytes per short


// Global Variables

extern unsigned short *dpio2BufferWFR[2];
extern unsigned short *dpio2BufferTWT[2];


// Global Function Prototypes

int  gpAoSrtDpio2Init ( void );
int  gpAoSrtDpio2StartDmaTransfer (int dpio2DeviceNumber, int desc_num, int buffNdx);
void gpAoSrtDpio2Cleanup ( void );

#endif

//******************************************************************************
// * End of file
//******************************************************************************
