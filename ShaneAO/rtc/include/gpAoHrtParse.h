/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtParse.h
 *
 *  @brief
 *  This is the header file for the parse component.
 *
 *  @b DESCRIPTION:
 *  This is the header file for the parse component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ & VJR DATE: 05/01/2008
 *
 *  @b REVISIONS:
 *
 *
 *  01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _PARSE_H
#define _PARSE_H        1

// This header file uses #defines from other local header files.

#include "gpAoHrtRecon.h"

//==============================================================================
// Global Variables


//==============================================================================
// Global Function Prototypes

//(O)FC global functions
void            gpAoHrtParse_ParseFTR( ReconBuffType*  reconBufPtr );
short           gpAoHrtParse_FTRAlgoProcess( short thNdx, char threadCmnd );
void            gpAoHrtParse_FTRAlgoInit( void );
void            gpAoHrtParse_FTRAlgoInitMemory( void );
void            gpAoHrtParse_FTRAlgoCleanMemory( void );

// Woofer Vector (r,i pairs) and the WooferVectorList
extern short    ParseAlgFTR_WfrVectorList[ MAX_WFR_VEC_SIZE ];
extern short    ParseAlgFTR_TwtZeroList[ MAX_WFR_VEC_SIZE * 2 ];
extern float    ParseAlgFTR_TwtInfFuncMask[ APERTURE_GRID_SIZEROW ][ APERTURE_GRID_SIZECOL ];   // Real valued filter
extern int      ParseAlgFTR_wfrActMap[ NUM_ACTIVE_WFR_ACTS ];                                   // Map of active woofer actuators in current DM
extern float    ParseAlgFTR_WfrControlMatrix[ NUM_ACTIVE_WFR_ACTS ][ MAX_WFR_VEC_SIZE * 2 ];    // Complex conv. from Wfr Coeff to Act values

extern int      ParseAlgFTR_wfrVecSize;                                                         // read in from file

#endif

//******************************************************************************
// * End of file
//******************************************************************************
