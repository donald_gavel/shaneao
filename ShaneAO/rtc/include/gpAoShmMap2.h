/*!
 *
 *  @file gpAoShmMap1.h
 *
 *  @brief
 *  This is the header file for the secondary shared memory data structure
 *
 *  @b DESCRIPTION:
 *  This is the header file for the secondary shared memory data structure
 *  that describes the content and layout of the shared memory
 *  segment containing
 *  FileDataShm, CamData, CentData, DataFlowControl
 *
 *  @b AUTHOR:
 *  SLA             DATE: 2012-11-20
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _SHMMAP2_H
#define _SHMMAP2_H              1

#include "gpAoHrtRecon.h"

// Typedefs

// structure for ensuring that all modules know the same sizes and offsets in shared memory
// and for allowing the compiler to align the contents of all the structures
typedef struct
{
    ReconDataType           ReconDataShm;
} ShmMap2Type;

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
