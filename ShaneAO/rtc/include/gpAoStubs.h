/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoStubs.h
 *
 *  @brief
 *  This is the header file for stubs to be able to build the hrt side in user space.
 *
 *  @b DESCRIPTION:
 *  This is the header file for stubs to be able to build the hrt side in user space.
 *
 *  @b AUTHOR:
 *  DWP     DATE: 07/30/2008
 *
 *  @b FUNCTION NAMES:
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _STUBS_H
#define _STUBS_H        1

#include "gpAoConstant.h"
#include <errno.h>

// Enums


// Defines

#if (RTL_SYSTEM == 0)
#define rtl_sem_t	sem_t
#define rtl_sem_init	sem_init
#define rtl_sem_wait	sem_wait
#define rtl_sem_trywait	sem_trywait
#define rtl_sem_post	sem_post
#define rtl_sem_destroy sem_destroy

#define rtl_shm_open	shm_open
#define rtl_shm_unlink	shm_unlink
#define rtl_ftruncate	ftruncate
#define rtl_mmap	mmap
#define rtl_munmap	munmap

#define rtl_timespec	timespec
#define rtl_clock_gettime	clock_gettime
#define rtl_timespec_sub	timespec_sub
#define rtl_timespec_to_ns	timespec_to_ns
#define rtl_errno	errno

#define rtl_printf	printf
#define rtl_nanosleep	nanosleep
#define rtl_usleep	usleep
#define rtl_open	open
#define rtl_write	write
#define rtl_read	read
#define rtl_close	close

#define rtl_pthread_t			pthread_t
#define rtl_pthread_attr_t		pthread_attr_t
#define rtl_pthread_cancel		pthread_cancel
#define rtl_pthread_join		pthread_join
#define rtl_pthread_create		pthread_create
#define rtl_sched_param			sched_param
#define rtl_pthread_attr_init		pthread_attr_init
#define rtl_pthread_attr_destroy	pthread_attr_destroy
#define rtl_pthread_attr_setdetachstate pthread_attr_setdetachstate
#define rtl_pthread_attr_getschedparam	pthread_attr_getschedparam
#define rtl_pthread_attr_setschedparam	pthread_attr_setschedparam

#define RTL_CLOCK_REALTIME  CLOCK_REALTIME

#define RTL_O_NONBLOCK	O_NONBLOCK
#define RTL_O_CREAT	O_CREAT
#define RTL_O_RDONLY	O_RDONLY
#define RTL_O_WRONLY	O_WRONLY
#define RTL_O_RDWR	O_RDWR
#define RTL_PROT_READ	PROT_READ
#define RTL_PROT_WRITE	PROT_WRITE
#define RTL_MAP_SHARED	MAP_SHARED
#define RTL_MAP_FAILED	MAP_FAILED

// Structure Declarations
struct rtl_frame
{
    int dumInt1;
    int dumInt2;
};

#if ( TIMER_TEST != 0 )
#define NSEC_PER_SEC 1000000000L

/*
 * set_normalized_timespec - set timespec sec and nsec parts and normalize
 *
 * @ts:         pointer to timespec variable to be set
 * @sec:        seconds to set
 * @nsec:       nanoseconds to set
 *
 * Set seconds and nanoseconds field of a timespec variable and
 * normalize to the timespec storage format
 *
 * Note: The tv_nsec part is always in the range of
 *       0 <= tv_nsec < NSEC_PER_SEC
 * For negative values only the tv_sec field is negative !
 */
static void set_normalized_timespec(struct timespec *ts, time_t sec, long nsec)
{
    while (nsec >= NSEC_PER_SEC)
    {
        nsec -= NSEC_PER_SEC;
        ++sec;
    }
    while (nsec < 0)
    {
        nsec += NSEC_PER_SEC;
        --sec;
    }
    ts->tv_sec = sec;
    ts->tv_nsec = nsec;
}

/*
 * sub = lhs - rhs, in normalized form
 */
static inline struct timespec timespec_sub(struct timespec *lhs, struct timespec *rhs)
{
    struct timespec ts_delta;
    set_normalized_timespec(&ts_delta, lhs->tv_sec - rhs->tv_sec, lhs->tv_nsec - rhs->tv_nsec);
    return ts_delta;
}

/*
 * timespec_to_ns - Convert timespec to nanoseconds
 *
 * @ts:         pointer to the timespec variable to be converted
 *
 * Returns the scalar nanosecond representation of the timespec parameter.
 */
static inline long long timespec_to_ns(const struct timespec *ts)
{
    return ((long long)ts->tv_sec * NSEC_PER_SEC) + ts->tv_nsec;
}
#endif	/* TIMER_TEST enabled */
#endif	/* RTL_SYSTEM disabled */

#if (CAM_TYPE != WFS_REAL_GPI)
// defines
#define PdvDev int
#if (RTL_SYSTEM == 0)
#define PAGE_SIZE 4096  // this is in the edt and/or RTL includes, when they are being used
#endif
#endif

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
