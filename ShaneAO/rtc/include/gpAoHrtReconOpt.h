/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtReconOpt.h
 *
 *  @brief
 *  Header for wavefront reconstruction optimization and related tasks.
 *
 *  @b DESCRIPTION:
 *  Header for wavefront reconstruction optimization and related tasks.
 *
 *  @b AUTHOR:
 *  DWP & SMJ & VJR DATE: 05/28/2009
 *
 *  @b REVISIONS:
 *
 *
 *  01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */
#ifndef _RECONOPT_H
#define _RECONOPT_H     1

#if (RTL_SYSTEM == 0)
#include <bits/posix1_lim.h>            // 0907xx for _POSIX_PATH_MAX (256) max bytes for a file path
#else
#define _POSIX_PATH_MAX 256
#endif

//=========================================================
// defines
//
#define NUM_OPT_THREADS         2                                  // ReconOpt threads for ALGFTR

#define GAIN_1PT5K_FPS          0.20  // 0.30                      // Max Gain for 1.5k fps
#define GAIN_OTHER_FPS          0.30  // 0.36                      // Max Gain for all other fps
#define NUM_GAIN_STEPS          128

#define OPT_TOTAL_LEN           (4 * PER_LEN)                      // ( 8192) test temporal data sets in Fourier modes
#define OPT_TOTAL_LEN_INVERSE   (1.0f / OPT_TOTAL_LEN)             // Inverse to avoid divides in code

#define NUM_FFTS                (OPT_TOTAL_LEN / PER_LEN * 2 - 1)  // Currently '7'

#define OPT_AVG_LEN             4096                               // XXX: Guess-timate! Defines length of the running average for the optimizer
#define OPT_AVG_LEN_INVERSE     (1.0f / OPT_AVG_LEN)


//=========================================================
enum        //optimization cmd if(ReconAlgFTRThreadStat[thNdx] != eReconAlgFTRTh_Quit)
{
    eReconOptAlgFTRCmd_None,
    eReconOptAlgFTRCmd_Start,
    eReconOptAlgFTRCmd_Stop,
    eReconOptAlgFTRCmd_Update
};

// States used in the Optimizer code Foreground ( NOT threads! - those are defined in the Recon.h)
typedef enum
{
    eReconOpt_State_Init,
    eReconOpt_State_Wait,
    eReconOpt_State_Start,
    eReconOpt_State_Collect_Group_A1,
    eReconOpt_State_Collect_Group_A2B1,
    eReconOpt_State_Collect_Group_A1B2,
    eReconOpt_State_Wait_PreProcess_Done,
    eReconOpt_State_Wait_PostProcess_1_Done,
    eReconOpt_State_Wait_PostProcess_2_Done,
    eReconOpt_State_Wait_PostProcess_3_Done,
    eReconOpt_State_WaitPostProcessPeriod

} ReconOpt_State_t;

//=====================================================================================================================
// Global Variables

// Optimization processing status (step)
extern ReconOpt_State_t ReconOpt_State;


///=====================================================================================================================
// Global Function Prototypes

void    gpAoHrtReconOpt_FTRAlgoInit( void );
void    gpAoHrtReconOpt_FTRAlgoInitThreading( void );
void    gpAoHrtReconOpt_FTRAlgoCleanThreading( void );
void    gpAoHrtReconOpt_FTRAlgoInitMemory( void );
void    gpAoHrtReconOpt_FTRAlgoInitFftPlans( void );
void    gpAoHrtReconOpt_FTRAlgoCleanMemory( void );
void    gpAoHrtReconOpt_FTRAlgoStart( void );
void    gpAoHrtReconOpt_FTRAlgoStop( void );
void    gpAoHrtReconOpt_ReconOptFTR( float* pFtrFFTModesData, float* pOptModeGains );
short   gpAoHrtReconOpt_FTRAlgoProcess( short thNdx, char threadCmnd );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
