/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file TTcam.h
 *
 *  @brief
 *  This is the header file for the TT camera component.
 *
 *  @b DESCRIPTION:
 *  This is the header file for the TT camera component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 11/25/2008
 *
 *  @b REVISIONS:
 *
 *
 *  01/15/2008: Initial Critical Design Version
 *
 *******************************************************************************
 */

#ifndef _CAMERA_TT_H
#define _CAMERA_TT_H           1

// This header file uses #defines from other local header files.

#include "gpAoConstant.h"

//==============================================================================
// Defines


#define TTNUM_ROWS_IN_CAM_ACTUAL        64      // 128  // actual number of rows in the TT camera
#define TTNUM_PIX_IN_ROW_ACTUAL         64  // 128      // actual number of pixels in each row of the TT camera
#define TTNUM_ROWS_IN_CAM_ACT_160       160      // actual number of rows in the TT camera for 160x160
#define TTNUM_PIX_IN_ROW_ACT_160        160  // actual number of pixels in each row of the TT camera for 160x160

#define TTNUM_SEGS_IN_CAM               8       // 16// number of segments in the camera (CCID66 has 20, but we ROI to 16)
#define TTNUM_SEGS_IN_CAM_160           20      // number of segments in the camera for 160x160
#define TTNUM_PIX_IN_SEG_ROW            16      // number of pixels in each row of the TT camera segment

// the actual 128x86 (or 128x128) camera frames are de-interleaved into a 40x40 array to correspond to 48x48 subaps
#define TTNUM_ROWS_IN_CAM               40      // 96   // number of rows in the TT camera -- for calculations // mrr DM
#define TTNUM_PIX_IN_ROW                40      // 96   // number of pixels in each row of the TT camera -- for calculations   // mrr DM
#define TTNUM_ROWS_IN_CAM_FOR_SUBAPS    32      // 86   // number of rows in the TT camera -- corresponding to actual sub-aps  // mrr DM
#define TTNUM_PIX_IN_ROW_FOR_SUBAPS     32      // 86   // number of pixels in each row of the TT camera -- corresponding to actual sub-aps    // mrr DM
#define TTROWS_OFFSET                   4       // 0    // 4    // rows to offset to get NUM_ROWS_IN_CAM_FOR_SUBAPS into NUM_ROWS_IN_CAM        // mrr DM
#define TTPIX_IN_ROW_OFFSET             4       // 0    // 4    // pixels to offset to get NUM_PIX_IN_ROW_FOR_SUBAPS into NUM_PIX_IN_ROW        // mrr DM
#define TTROWS_OFFSET_160               48      // 60  // rows to offset to get from 160x160 to 40x40
#define TTPIX_IN_ROW_OFFSET_160         48      // 60  // pixels to offset to get from 160x160 to 40x40

#define TTNUM_CAM_ROWS_PER_SUBAP        4       // number of TT camera rows per subap row      // mrr DM
#define TTNUM_CAM_COLS_PER_SUBAP        4       // number of TT camera columns per subap row   // mrr DM

#define TTNUM_SUBAPS_ROWS_PER_FRAME     8       // 48   // number of sub-aperture rows per frame        // mrr DM
#define TTFIRST_USED_CAMERA_ROW         0       // first used camera row

#define TTFIRST_CAM_ROW_IN_SUBAP        0       // first camera row containing sub-ap data in each sub-ap (starting at 0)
#define TTFIRST_CAM_COL_IN_SUBAP        0       // first camera column containing sub-ap data in each sub-ap (starting at 0)

// max number of sub-aperture rows per frame
#define TTMAX_SUBAP_ROWS_PER_FRAME      (TTNUM_ROWS_IN_CAM / TTNUM_CAM_ROWS_PER_SUBAP)

#define TTINIT_FRAME_RATE_INDEX         1       // initial TT camera rate in frames per second ( INDEX! into LEGAL_FRAMES )

#define TTINIT_CAM_GAIN                 1       // initial TT camera gain

#define TTINIT_TIMEOUT_ERROR            300     // Number of loops for timeout - waiting for INitializers to complete.

// Defines specific to the DMA ring buffers.
#define TTnumbufs 4  // number of DMA ring buffers

// camera buffer type
typedef struct
{
//  'used' camera data -- that is, camera data with active sub-aps (there used to be 'unused' data, that's no longer needed)
    unsigned short  usedCamData[TTMAX_SUBAP_ROWS_PER_FRAME][TTNUM_CAM_ROWS_PER_SUBAP][TTNUM_PIX_IN_ROW];  // 10x4x40
} TTcamBuffType;

// camera data type
typedef struct
{
    short           cmdStart;                           // index (into CmdTable[]) of command to be started or eCmdNone
    short           cmdCrnt;                            // index (into CmdTable[]) of command currently running or eCmdNone

    short           cmdIntParam[ MAX_NUM_CMD_PARAMS];   // integer parameter for command, if it has one
    float           cmdFloatParam[ MAX_NUM_CMD_PARAMS]; // float parameter for command, if it has one

    boolean         edtBufStat;                         // indicates the status of the EDT buffers in RTL mode
    boolean         intrIgnore;                         // indicates SMC camera interrrupt should be ignored
                                                        // > (because we're doing serial comms)

    short           camWriteNdx;                        // index into camBuffNdxs[] for buffer being written to
    TTcamBuffType   camBuffs[TTNUM_HRT_DATA_BUFFS];     // the camera frame buffers

    unsigned short  simDataBuffs[TTnumbufs][TTNUM_ROWS_IN_CAM_ACTUAL][TTNUM_PIX_IN_ROW_ACTUAL];  // simulated tt data
    boolean         simFileRead;                        // indicates whether a simulated tt data file has been read in

    int             rawDispWriteBuffNdx;                // the raw display buffer currently being written
    int             rawDispReadBuffNdx;                 // the raw display buffer currently being read
    unsigned short  rawDispBuffs[2][TTNUM_ROWS_IN_CAM_ACTUAL][TTNUM_PIX_IN_ROW_ACTUAL]; // the raw display buffers

    short           stateSRT;
    short           stateHRT;

    int             StartCamDrinkMe;                    // will be given value to indicate TTcamMngr_StartCam() is done
    int             Init1DrinkMe;                       // will be given value to indicate TT Initialize2 is done
    int             Init2DrinkMe;                       // will be given value to indicate TT Initialize2 is done
    int             IntrInitDrinkMe;                    // will be given value to indicate TT_IntrInit() is done
    int             SetRepDrinkMe;                      // will be given value to indicate TT SetRep is done
    int             SetAttenDrinkMe;                    // will be given value to indicate TT SetAtten is done
} TTcamDataType;


//==============================================================================
// Global Variables

#if ( TIMER_TEST != 0 )
#define TTNUM_TIME_SETS           8
extern long TTcamFrameID;  // a sequential counter incremented every time a valid frame is sent to the centroider
#endif  /* TIMER_TEST */

extern short hrt_useTtSimData;          // flag indicating whether to use simulated tt data ( 0: no, 1: yes )

extern short hrt_TTcamFrameRate;        // camera frame rate INDEX (NOT the actual frame rate!)
extern short hrt_TTcamGain;             // camera gain (0, 1, 2, or 3)

extern short TTcamFrameRateNdx;         // index for current camera rate into LegalFrameRatesTT[]
// moved to TTcamMngr.h  extern const short LegalFrameRatesTT[];

extern int TT_irq;
extern void* TT_dev;

#if ( TIMER_TEST != 0 )
//
extern long TTreconFrameID;
extern struct timespec TTtimerTimes[TTNUM_TIME_SETS][eTimerMaxTimes];
#endif

//==============================================================================
// Global Function Prototypes
//
void* TTcam( void *arg );
void  TTcam_IntrInit( void );
void* TTcam_DrvrWatch( void *arg );

#endif  /* _CAMERA_TT_H */

//******************************************************************************
// * End of file
//******************************************************************************
