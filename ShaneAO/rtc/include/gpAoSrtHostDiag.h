/*******************************************************************************
 *
 *      COPYRIGHT (C) 2008
 *      LAWRENCE LIVERMORE NATIONAL LABORATORY
 *      ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *      @file gpAoSrtHostDiag.h
 *
 *      @brief
 *      This is the header file for the diagnostic data handler component (on the SRT side).
 *
 *      @b DESCRIPTION:
 *      This is the header file for the diagnostic data handler component (on the SRT side).
 *
 *      @b AUTHOR:
 *      SMJ & DWP       DATE: 12/17/2008
 *
 *      @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _HOSTDIAG_H
#define _HOSTDIAG_H             1

// This header file uses #defines from other local header files.

#include "gpAoHrtFileData.h"

// Enums

enum
{
    // hrtBufState flags
    eBufIdle,                       // (state-0) idle indicates that buffer is not actively being used
    eBufCamProc,                    // (state-1) cam indicates that buffer is in Cam processing
    eBufReqCent,                    // (state-2) cam indicates that cam done - awaiting cent processing
    eBufCentProc,                   // (state-3) cent indicates that buffer is in Cent processing
    eBufReqRecon,                   // (state-4) cent indicates that cent done - awaiting recon processing
    eBufReconProc,                  // (state-5) recon indicates that buffer is in Recon processing
};

enum
{
    eDisplayData,                   // display data ready - currently from the recon thread

    eDiagQuit,                      // quit the thread (used at shutdown)
    eDiagWFSCmd,                    // command from the HRT to change a SciMeasure camera parameter
    eDiagCmdFile,                   // command from the HRT to read or write a file
    eDiagCmdHost,                   // command from the host, via hostICon
    eDiagCmdErrStrngNoParm,         // error message to be sent to the host, no parameter
    eDiagCmdErrStrngIntParm,        // error message to be sent to the host, integer parameter
    eDiagCmdErrStrngFltParm,        // error message to be sent to the host, float parameter
    eDiagCmdResponse,               // response message to a command sent from the SRT indicating succes or failure and optional data.
    eDiagTTCmd                      // command from the HRT to change a TT camera parameter
};

enum
{
    // dumpDataProcess Status types
    eDumpStart,                     // Flag from HostDiag to start a new dump - only valid if status = 'eDumpStopped'
    eDumpInit,
    eDumpInit_PSD,
    eDumpOpeningFiles,
    eDumpAbortReq,
    eDumpClosingFiles,
    eDumpStopped,
    eDumpSavingFiles,
    eDumpSavingFiles_PSD
};

enum
{
    eDumpRawImageNdx,
    eDumpCentNdx,
    eDumpIntenNdx,
    eDumpFtrFFTModesNdx,

    eDumpFtrTwtNdx,
    eDumpFtrWfrNdx,

    eDumpClsdLpPwrSpctNdx,
    eDumpCalcOpnLpPwrSpctNdx,
    eDumpModalGainsNdx,
    eDumpSettingsTwtNdx,

    eDumpSettingsWfrNdx,
    eDumpSettingsTTNdx,
    eDumpSettingsM1M2Ndx,
    eDumpStatsNdx,

    eDumpSettingsTTWfrNdx,
    eDumpPupilAlignErrNdx,
    eDumpCalDiagNdx,
    eDumpCalRefOffsetsNdx,

    eDumpCurrRefCentNdx,
    eDumpCurrLoopStatusNdx,
    eDumpOptimizedFtrModesNdx,

    eDumpUnclippedTwtPhaseNdx,
    eDumpUnclippedWfrPhaseNdx,
    eDumpClipCleanupNdx,
    eDumpUnclippedTTWfrNdx,

    eDumpNumTypes  // number of possible dump data types supported at this point
};

// Enum values for dumpDiag ONLY bits (telem defined seperately)
enum
{
    eDumpRawImageBit                = 0x00000001,   // bit set to dump raw camera data (from camera thread)
    eDumpCentBit                    = 0x00000002,   // bit set to dump centroid data (from centroid thread)
    eDumpIntenBit                   = 0x00000004,   // bit set to dump intensity data (from centroid thread)
    eDumpFtrRawBit                  = 0x00000008,   // bit set to dump FTR Output should be dumped

    eDumpFtrTwtBit                  = 0x00000020,   // bit set to dump FTR Tweeter Data
    eDumpFtrWfrBit                  = 0x00000040,   // bit set to dump FTR Woofer Data

    eDumpClsdLpPwrSpctBit           = 0x00000100,   // bit set to dump Closed loop power Spectrum
    eDumpCalcOpnLpPwrSpctBit        = 0x00000200,   // bit set to dump Derived Open Loop Power Spectrum
    eDumpModalGainsBit              = 0x00000400,   // bit set to dump Modal Gains
    eDumpSettingsTwtBit             = 0x00000800,   // bit set to dump clipped twt phase

    eDumpSettingsWfrBit             = 0x00001000,   // bit set to dump clipped wfr phase
    eDumpSettingsTTBit              = 0x00002000,   // bit set to dump Settings for TT
    eDumpSettingsM1M2Bit            = 0x00004000,   // bit set to dump Settings for M1/M2
    eDumpStatsBit                   = 0x00008000,   // bit set to dump statistics

    eDumpSettingsTTWfrBit           = 0x00010000,   // bit set to dump setting for woofer with new TT
    eDumpPupilAlignErrBit           = 0x00020000,   // bit set to dump the pupil misalignment error
    eDumpCalDiagBit                 = 0x00040000,   // bit set to dump the Cal diag data
    eDumpCalRefOffsetsBit           = 0x00080000,   // bit set to dump the Cal Ref Centroid offsets

    eDumpCurrRefBit                 = 0x00100000,   // bit set to dump the current Ref Centroids
    eDumpCurrLoopStatusBit          = 0x00200000,   // bit set to dump the current loop Status
    eDumpOptimizedFtrModesBit       = 0x00400000,   // bit set to dump the ftr output with optimizer gain applied, but not yet integrated

    eDumpUnclippedTwtPhaseBit       = 0x01000000,   // bit set to dump the unclipped twt phase
    eDumpUnclippedWfrPhaseBit       = 0x02000000,   // bit set to dump the unclipped wfr phase
    eDumpClipCleanupBit             = 0x04000000,   // bit set to dump clipping and cleanup data
    eDumpUnclippedTTWfrBit          = 0x08000000,   // bit set to dump unclipped wfr phase with new TT

    eDumpUnusedBits                 = 0x10000000,   // First unused bits - this can be used for error checking or end of list

    eDumpInvalid                    = eDumpUnusedBits,      // another definition for end of list check
};

// Enum values for Telem-Display ONLY bits (dumpDiag defined seperately)
enum
{
    eDisplayRawBit          = 0x0001,       // bit set in diag and telem commands to indicate raw camera data (from camera thread)
    eDisplayCentBit         = 0x0002,       // bit set in diag and telem commands to indicate centroid data (from centroid thread)
    eDisplayIntenBit        = 0x0004,       // bit set in diag and telem commands to indicate intensity data (from centroid thread)
    eDisplayTwtBit          = 0x0008,       // bit set in diag and telem commands to indicate Tweeter DM data (from recon thread)
    eDisplayWfrBit          = 0x0010,       // bit set in diag and telem commands to indicate Tweeter DM data (from recon thread)
    eDisplayRmsModal        = 0x0020,
    eDisplayTTBit           = 0x0040,       // bit set in diag and telem commands to indicate tip/tilt data (from recon thread)
    eDisplayM1M2Offload     = 0x0080,       // bit set in diag and telem commands to indicate M1/M2 offload data
    eDisplayPupCenter       = 0x0100,       // bit set in diag and telem commands to indicate coarse pupil-centering data
    eDisplayStats           = 0x0200,       // bit set in diag and telem commands to indicate statistics

    eDisplayNumTypes        = 10,           // number of possible display data types supported at this point

    eDisplayNone            = 0x0000,       // No Data marker
    eDisplayNotNew          = 0x8000
};

enum
{
    eDiagOutStrng,          // null terminated string
    eDiagOutRaw,            // raw image data
    eDiagOutCent,           // centroid data
    eDiagOutInten,          // intensity data
    eDiagOutTwt,            // Tweeter data
    eDiagOutTT              // tip/tilt data
};


// Defines

#define DIAG_STRING_MAX_LENGTH 256

#define NO_FILE ((fitsfile*)(-1))


// Typedefs

// structure for FIFO diagnostic messages being sent to diag (not all of these fields are used for each diagID)
typedef struct
{
    short   diagID;
    short   diagStat;
    short   diagReadNdx;

    short   debugLevel;     // indicates the message debug level when diagID is one of the eDiagErrStrng types
    short   stringLength;

    short   cmdNdx;
    int     intParam[ MAX_NUM_CMD_PARAMS ];
    float   floatParam[ MAX_NUM_CMD_PARAMS ];
    char    strngParam[ MAX_NUM_CMD_PARAMS ][ MAX_FILENAME_LENGTH ];
} diagFIFOMsgType;

typedef struct
{
    int     diagBit;                // bit set in diag and telem commands indicating diag data type (-1 flags end of list)
    char*   dirName;                // name of directory for diag data storage
    char*   fileName;               // file name for diag data storage
    void*   fileDesc;               // current file descriptor for file to which data is being stored (or NO_FILE if not active)
} DiagDataDescType;

typedef struct
{
    int     diagBits;               // bits for diag data types that this data source thread provides
} DiagSourceDescType;

// Buffer Status
typedef struct
{
    char    inDisplayProcess;       // indicates that the current frame has been sent to SRT for display
    char    inSaveProcess;          // indicates that the current frame is now being saved by SRT
                    //      - flip/flop with next buffer to avoid overwrite
    char    dispDoUpdate;           // indicates time for display update
                    //      - Set by Camera based on frame rate and display rate
                    //      - FIFO message sent by last HRT process (currently recon)
    short   hrtBufState;            // indicates the current hrt state of this buffer
} DataBufferElemStatusType;

// structure to hold the diag and display interface between the HRT and SRT
typedef struct
{
    short   dispCntr;               // Number of frames since last display update ( R/W: HRT )
    short   dispRdyBuff;            // Buffer that is next for display (Ndx WRT: HRT, RD: SRT)
    short   dispRate;               // frame display updates per second ( WRT: SRT, RD: HRT )

    short   nextDispBuf;            // Next buffer to receive display data (currently 3 buffers, 1..3)      (WRT/RD HRT only)

    short   decimateRate;           // Number of frames for each frame saved
    short   decimateCnt;            // Number of frames since last decimate frame saved

    char    srtSaveInProg;          // SRT is saving data (R/W: SRT)
    char    srtTelemInProg;         // SRT is sending 'socket' display data (R/W: SRT)

    // Buffer status (display, save) is kept here - since common to all HRT processes
    DataBufferElemStatusType        bufStat[ NUM_HRT_DATA_BUFFS ];          // Can be moved to new data block for HRT only
} HrtDataFlowControlType;


// Global Variables

extern short DiagFileSeqNum;  // sequence number, used in file names, for diagnostic data files

extern char CrntDateStr[MAX_FILENAME_LENGTH];  // current date string


// Global Function Prototypes

void *gpAoSrtHostDiag( void *arg );
void HostDiagSendStrngNoParm( int msgDebugLevel, const char *strngPtr );
void HostDiagSendStrngIntParm( int msgDebugLevel, const char *strngPtr, int intParam );
void HostDiagSendStrngFltParm( int msgDebugLevel, const char *strngPtr, float floatParam );
void gpAoSrtHostDiag_KillDumpDataThread( void );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
