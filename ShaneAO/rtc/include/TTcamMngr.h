/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file TTcamMngr.h
 *
 *  @brief
 *  This is the header file for the TT camera manager component (on the SRT side).
 *
 *  @b DESCRIPTION:
 *  This is the header file for the TT camera manager component (on the SRT side).
 *
 *  @b AUTHOR:
 *  SMJ & DWP       DATE: 12/17/2008
 *
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _CAMERA_TT_MNGR_H
#define _CAMERA_TT_MNGR_H               1


// Defines
#define TT_UNIT_NUM 0                   // the unit number used by the EDT driver for Little Joe
                                        //dwp121019 -- had Little Joe EDT board pulled out because I suspect
                                        //dwp121019 -- > it's affecting timing (it's not currently being inited)

#define NUM_FRAME_RATES_TT 6            /* now used and important */
#define MAX_FRAME_RATE_TT          2000


// Camera control commands from HRT camTT
enum eTTcmds
{
    eTTInitialize1,         // initialize the EDT camera interface, 1
    eTTInitialize2,         // initialize the EDT camera interface, 2
    eTTMapOpen,             //
    eTTMapClose,            //
    eTTSetRep,              // set the Little Joe repetition count
    eTTSetAtten,            // set the Little Joe attenuation
    eTTReadRep,             // read and display the REP as a frame rate
    eTTReadAtten            // read and display the attenuation as a gain
};

// Typedefs

#if 0 /* same as in WFScamMngr.h so we omit until we have to dup */
// Local Variables

// this LUT is taken from TempLUT-Tiger66.xls, provided by SciMeasure
static const float ccdTemperatureLUT[] =
{
925.3, 120.6,  95.7,  82.4,  73.5,  66.9,  61.7,  57.4,  53.8,  50.6,
 47.8,  45.4,  43.2,  41.1,  39.3,  37.6,  36,    34.5,  33.2,  31.9,
 30.6,  29.5,  28.4,  27.4,  26.4,  25.5,  24.6,  23.7,  22.9,  22.1,
 21.3,  20.6,  19.9,  19.2,  18.6,  17.9,  17.3,  16.7,  16.2,  15.6,
 15.1,  14.5,  14,    13.5,  13,    12.5,  12.1,  11.6,  11.2,  10.8,
 10.3,   9.9,   9.5,   9.1,   8.7,   8.4,   8,     7.6,   7.3,   6.9,
  6.6,   6.2,   5.9,   5.6,   5.3,   5,     4.7,   4.4,   4.1,   3.8,
  3.5,   3.2,   2.9,   2.6,   2.4,   2.1,   1.8,   1.6,   1.3,   1.1,
  0.8,   0.6,   0.3,   0.1,  -0.1,  -0.4,  -0.6,  -0.8,  -1,    -1.2,
 -1.5,  -1.7,  -1.9,  -2.1,  -2.3,  -2.5,  -2.7,  -2.9,  -3.1,  -3.3,
 -3.5,  -3.7,  -3.9,  -4,    -4.2,  -4.4,  -4.6,  -4.8,  -4.9,  -5.1,
 -5.3,  -5.5,  -5.6,  -5.8,  -6,    -6.1,  -6.3,  -6.5,  -6.6,  -6.8,
 -6.9,  -7.1,  -7.2,  -7.4,  -7.5,  -7.7,  -7.8,  -8,    -8.1,  -8.3,
 -8.4,  -8.6,  -8.7,  -8.8,  -9,    -9.1,  -9.2,  -9.4,  -9.5,  -9.6,
 -9.8,  -9.9, -10,   -10.2, -10.3, -10.4, -10.5, -10.7, -10.8, -10.9,
-11,   -11.2, -11.3, -11.4, -11.5, -11.6, -11.7, -11.9, -12,   -12.1,
-12.2, -12.3, -12.4, -12.5, -12.6, -12.8, -12.9, -13,   -13.1, -13.2,
-13.3, -13.4, -13.5, -13.6, -13.7, -13.8, -13.9, -14,   -14.1, -14.2,
-14.3, -14.4, -14.5, -14.6, -14.7, -14.8, -14.9, -15,   -15.1, -15.2,
-15.3, -15.4, -15.5, -15.5, -15.6, -15.7, -15.8, -15.9, -16,   -16.1,
-16.2, -16.3, -16.3, -16.4, -16.5, -16.6, -16.7, -16.8, -16.9, -16.9,
-17,   -17.1, -17.2, -17.3, -17.4, -17.4, -17.5, -17.6, -17.7, -17.8,
-17.8, -17.9, -18,   -18.1, -18.1, -18.2, -18.3, -18.4, -18.5, -18.5,
-18.6, -18.7, -18.8, -18.8, -18.9, -19,   -19,   -19.1, -19.2, -19.3,
-19.3, -19.4, -19.5, -19.6, -19.6, -19.7, -19.8, -19.8, -19.9, -20,
-20,   -20.1, -20.2, -20.2, -20.3, -20.4
};
#endif /* 0 */

// Global Variables

// WARNING: LegalFrameRatesTT[] is defined in both gpAoHrtCamTT.c (for the HRT) and TTcamMngr.c (for the SRT), for now
extern const short LegalFrameRatesTT[];


// Global Function Prototypes

void TTcamMngr_ProcCmd ( short cmd, int param );
int  TTcamMngr_StartCam( void );
void TTcamMngr_StopCam( void );

int TTcamMngr_ReadSimTtData( char *filename );  // read simulated tt data into shared mem for use in sim mode

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
