/*******************************************************************************
 *  COPYRIGHT (C) 2012
 *  UNIVERSITY OF CALIFORNIA
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *  @file gpAoSrtHostWoofer.h
 *
 *  @brief
 *  This is the header file for the host woofer interface controller component (on the SRT side).
 *
 *  @b DESCRIPTION:
 *  This is the header file for the host woofer interface controller component (on the SRT side).
 *
 *  @b AUTHOR:
 *  MRR     DATE: 8/22/2012
 *
 *  @b REVISIONS:
 *
 ********************************************************************************/

#ifndef _HOSTICON_H
#define _HOSTICON_H     1

static boolean  WooferquitThread = false;

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
