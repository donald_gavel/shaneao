/******************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

FILE NAME:            cam_LTT_Mngr.h

AUTHOR:    DHL            DATE: 10/7/04

FILE DESCRIPTION:
This is the header file for the SciMeasure LTT camera manager component.

******************************************************************************/

#ifndef _CAMERA_LTT_MNGR_H
#define _CAMERA_LTT_MNGR_H        1


// Defines
#define LTT_UNIT_NUM 0        // the unit number used by the EDT driver  //XXXWFSTEST
//XXXWFSTEST #define LTT_UNIT_NUM 1        // the unit number used by the EDT driver

#ifdef TEST_60X60
#define OpenLoopProg    0   // Little Joe program for 60x60 frame size with binning of 1  //XXX20
#define OpenLoopFilt    1   //XXXNOISE
#else
#define OpenLoopProg    1   // Little Joe program for 20x20 frame size with binning of 3  //XXX20
#define OpenLoopFilt    3   //XXXNOISE
#endif

#define CloseLoopProg   2   // Little Joe program for 6x6 frame size with binning 3
#define CloseLoopFilt   3   //XXXNOISE


// Defines specific to the DMA ring buffers.
#define LTTnumbufs      8   // number of DMA ring buffers
#define LTTOpenbufsize (OPEN_ROWS_IN_CAM * OPEN_PIX_IN_ROW * 2 * 2)  // don't know why the second multiply by 2; but, this
                                                                     // > was the way it was done in the example code rt_take
#define LTTClosebufsize (6 * 6 * 2 * 2) // frame size is reduced to 6x6 in the program used by closed-loop mode
#define MAXNUMBUFS 1024                 // size of the array that holds the DMA buffer addresses

// Camera control commands from HRT camLTT
enum
{
    eLTTInitialize1,    // initialize the EDT camera interface, step 1
    eLTTInitialize2,    // initialize the EDT camera interface, step 2
    eLTTMapOpen,        // change the size of the mapped DMA for open-loop data
    eLTTMapClose,       // change the size of the mapped DMA for close-loop data
    eLTTSetRep,         // set the Little Joe repetition count
    eLTTSetAtten        // set the Little Joe attenuation
};


// Typedefs

// Global Function Prototypes

void CamLTTMngr_ProcCmd(int16 cmd, int16 param);
void CamLTTMngr_StartCam(void);
void CamLTTMngr_StopCam(void);

#endif
