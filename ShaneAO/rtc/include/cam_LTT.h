/******************************************************************************

COPYRIGHT (C) 2003
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

FILE NAME:        cam_LTT.h

AUTHOR:            DHL (based on DWP's cam.h)        DATE: 10/7/04

FILE DESCRIPTION:
This is the header file for the camera component.

******************************************************************************/

#ifndef _CAMERA_LTT_H
#define _CAMERA_LTT_H        1


// Defines

#ifdef TEST_60X60
#define OPEN_ROWS_IN_CAM        60    // number of rows in the LTT camera in open-loop mode  //XXX20
#define OPEN_PIX_IN_ROW         60    // number of pixels in each row of the LTT camera in open-loop mode  //XXX20
#else
#define OPEN_ROWS_IN_CAM        20    // number of rows in the LTT camera in open-loop mode  //XXX20
#define OPEN_PIX_IN_ROW         20    // number of pixels in each row of the LTT camera in open-loop mode  //XXX20
#endif

#define CLOSED_ROWS_IN_CAM        6    // number of rows in the LTT camera in closed-loop mode
#define CLOSED_PIX_IN_ROW         6    // number of pixels in each row of the LTT camera in closed-loop mode
#define NUM_CAM_BUFFS           200    // number of camera data buffers (there are lots of these
                                       // > so the diagnostics data handler can get behind)

#define LTT_CAM_CLOSE_SETTLE_WAIT 10  // number of SMC LTT camera frames to wait after it has entered closed loop mode


// Typedefs

// LTT camera buffer type
typedef struct
{
    boolean     inUseFlag;      // indicates whether this buffer is in use for non-diag data
    int16       diagIndicator;  // indicates eDiagIndNotInUse, eDiagIndInUse, or eDiagIndTelem

    boolean     ignoreDataFlag;         // this is used to tell TipTiltAPD() to ignore the data being sent (because it isn't
                                        // > actually new data); the semaphore needs to be posted to TipTiltAPD() to keep
                                        // > the buffer indexes up to date
    int32       quadCounts[NUM_APDS];   // these are the quad-cell values, computed from the SciMeasure
                                        // > camera data, that TipTiltAPD() will use instead of APD readings

    int16       data[OPEN_ROWS_IN_CAM]  // dimension the camera data for the larger size
                    [OPEN_PIX_IN_ROW];
} LTTCamBuffType;

// LTT camera data type
typedef struct
{
    int16           cmdStart;       // index (into CmdTable[]) of command to be started or eCmdNone
    int16           cmdCrnt;        // index (into CmdTable[]) of command currently running or eCmdNone
    int16           cmdIntParam;    // integer parameter for command, if it has one
    float32         cmdFloatParam;  // float parameter for command, if it has one

    int16           diagActFlag;    // diagnostic action flag (set only on SRT side (except at initialization))
    int16           diagStatFlag;   // diagnostic status flag (set only on HRT side)
    int16           diagDiagRate;   // indicates rate at which diagnostic data should be sent to SRT (always 0 or 1)
    int16           diagDiagCntr;   // counter for diagnostic data being sent to SRT
    int16           diagTelemRate;  // rate at which telemetry data should be sent to SRT (same as diag data, but at different rate)
    boolean         diagPSFFlag;    // indicates that PSF estimation is on, so data should be sent

    boolean         intrIgnore;     // indicates LTT camera interrrupt should be ignored (because we're doing serial comms)  XXXINTIG

    int16           writeNdx;                   // index into camBuffNdxs[] for buffer being written to
    int16           readNdx;                    // index into camBuffNdxs[] for buffer being read from
    int16           camBuffNdxs[NUM_CAM_BUFFS]; // circular list of indexes into camBuffs[]
    LTTCamBuffType  camBuffs[NUM_CAM_BUFFS];    // the camera frame buffers
} LTTCamDataType;


// Global Variables
extern boolean LTTOpenLoopMode;

extern int16 LttFrameRate;  // camera frame rate in frames per second
extern int16 LttCamGain;    // set LTT loop gain
extern int16 LttFov;        // closed-loop field of view to 2,4, or 6 arc seconds
extern int16 UseAPD;        // if 0 use the SciMeasure Camera, else if 1 use the APD tilt/tip system

extern int16 LTTOpenBackground[OPEN_ROWS_IN_CAM][OPEN_PIX_IN_ROW];           // open loop data for determining Background
extern int16 LTTClosedBackground[CLOSED_ROWS_IN_CAM * CLOSED_PIX_IN_ROW];  // closed loop background data

extern int LttTotalSize;            // bytes in shared memory region
extern unsigned char *LttBaseAddr;  // pointer to start of shared memory

extern int LTT_irq;
extern void *LTT_dev;


// Global Function Prototypes
void *CamLTT( void *arg );
void CamLTT_InitIntr( void );
void *CamLTT_DrvrWatch( void *arg );

#endif
