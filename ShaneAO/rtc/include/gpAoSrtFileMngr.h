/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoSrtFileMngr.h
 *
 *  @brief
 *  This is the header file for the file manager component (on the SRT side).
 *
 *  @b DESCRIPTION:
 *  This is the header file for the file manager component (on the SRT side).
 *
 *  @b AUTHOR:
 *  SMJ & DWP   DATE: 12/17/2008
 *
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _FILEMNGR_H
#define _FILEMNGR_H     1


// Enums
enum
{
// 0
    eFileTypeParms,             // parameter data

// 1
    // Order for eFileTypeWfsDark_0 to eFileTypeWfsDark_nn is necessary ( used in code to access )
    eFileTypeWfsDark_0,         // Wfs Dark data
    eFileTypeWfsDark_1,         // Wfs Dark data
    eFileTypeWfsDark_2,         // Wfs Dark data
    eFileTypeWfsDark_3,         // Wfs Dark data
    eFileTypeWfsDark_4,         // Wfs Dark data

    // Order for eFileTypeWfsBkGrnd_0 to eFileTypeWfsBkGrnd_nn is necessary ( used in code to access )
    eFileTypeWfsBkGrnd_0,       // Wfs BkGrnd data
    eFileTypeWfsBkGrnd_1,       // Wfs BkGrnd data
    eFileTypeWfsBkGrnd_2,       // Wfs BkGrnd data
    eFileTypeWfsBkGrnd_3,       // Wfs BkGrnd data
    eFileTypeWfsBkGrnd_4,       // Wfs BkGrnd data

// 11
    // Order for eFileTypeWfsFlat_0 to eFileTypeWfsFlat_nn is necessary ( used in code to access )
    eFileTypeWfsFlat_0,         // Wfs Flat data
    eFileTypeWfsFlat_1,         // Wfs Flat data
    eFileTypeWfsFlat_2,         // Wfs Flat data
    eFileTypeWfsFlat_3,         // Wfs Flat data
    eFileTypeWfsFlat_4,         // Wfs Flat data

// 16
    eFileTypeRefCentQ,          // ref centroids, quad-cell
    eFileTypeRefOffs,           // Cent Offs
    eFileTypeRefRePhaseOffs,    // Cal Re-phase offsets
    eFileTypeRefMapOffs,

// 20
    eFileTypeReserve1,          // was eFileTypeTwtCM_VMM
    eFileTypeReserve2,          // was eFileTypeWfrCM_VMM
    eFileTypeTwtSM,             // tweeter System Matrix (only written, never read)
    eFileTypeReserve4,          // was eFileTypeWfrSM

    eFileTypeInitialModalGains, // FTR optimizer gains (read into working area)
    eFileTypeHostModalGains,    // FTR optimizer Host gains (set by host)
    eFileTypeModalGainSelectors,// FTR optimizer Modal selectors (sent through file)

// 27
    eFileTypeTwtLabFlat,        // Tweeter lab flat data
    eFileTypeTwtSysFlat,        // Tweeter sys flat data
    eFileTypeTwtDmShape,        // Tweeter dm shape data

// 30
    eFileTypeTwtHostOffset,     // Tweeter offset from host specified file

    eFileTypeWfrLabFlat,        // Woofer lab flat data
    eFileTypeWfrSysFlat,        // Woofer sys flat data
    eFileTypeWfrDmShape,        // Woofer dm shape data

    eFileTypeWfrHostOffset,     // Woofer offset from host specified file

    eFileTypeTwtDmCoeff,        // coeffs to generate lookup tables for the TWT
    eFileTypeWfrDmCoeff,        // coeffs to generate lookup tables for the WFR
    eFileTypeTTDmCoeff,         // coeffs to generate lookup tables for the TT

// 38
    eFileTypeTwtCleanup,        // tweeter modes cleanup matrix
    eFileTypeTwtReconFilter,    // twt filter data x,y,Denom
    eFileTypeWfrCM_FTR,         // woofer control matrix for FTR calculations data
    eFileTypeWfrCleanup,        // woofer modes cleanup matrix
    eFileTypeWfrVecList,        // wfr extraction vector selection list
    eFileTypeTwtZeroList,       // twt zero list after woofer extraction
    eFileTypetwtInfFuncMask,    // twt coeff selection mask
    eFileTypeWfrFtrActMap,      // wfr actuator map after the FTR CM is applied

// 46
    eFileTypeApoRefCentOff_1,   // Offsets for apodizer #1
    eFileTypeApoRefCentOff_2,   // Offsets for apodizer #2
    eFileTypeApoRefCentOff_3,   // Offsets for apodizer #3
    eFileTypeApoRefCentOff_4,   // Offsets for apodizer #4
    eFileTypeApoRefCentOff_5,   // Offsets for apodizer #5
    eFileTypeApoRefCentOff_6,   // Offsets for apodizer #6
    eFileTypeApoRefCentOff_7,   // Offsets for apodizer #7
    eFileTypeApoRefCentOff_8,   // Offsets for apodizer #8

// 54
    eFileTypeMakeInFunc,        // Woofer dm shape data

// Special Purpose flags for HRT->SRT ( HRT passing param updates and cmd status updates to SRT )
    eFileTypeCmdStatus,

    eFileTypeNum
};

enum
{
    eFileReadAllData,           // 'all' parameter data should be read from file into shared memory

    // These reads have to be available
    eFileReadReserve1,          // was eFileReadTwtCMData
    eFileReadReserve2,          // was eFileReadWfrCMData
    eFileReadReserve3,          // was eFileReadWfrSMData
    eFileReadFtrWfrCMData,      // FTR woofer control matrix
    eFileReadReserve4,          // was eFileReadFtrWfrSMData

    eFileRdRefCentQ,            // ref centroids, quad-cell
    eFileRdRefOffs,             // Cent Offs
    eFileRdRefRePhaseOffs,      // Cal Re-phase offsets
    eFileRdRefMapOffs,          // Mapped offset

    eFileRdApoRefCentOff_1,     // Offsets for apodizer #1
    eFileRdApoRefCentOff_2,     // Offsets for apodizer #2
    eFileRdApoRefCentOff_3,     // Offsets for apodizer #3
    eFileRdApoRefCentOff_4,     // Offsets for apodizer #4
    eFileRdApoRefCentOff_5,     // Offsets for apodizer #5
    eFileRdApoRefCentOff_6,     // Offsets for apodizer #6
    eFileRdApoRefCentOff_7,     // Offsets for apodizer #7
    eFileRdApoRefCentOff_8,     // Offsets for apodizer #8

    // These reads have to be available, to service commands from TLC to read them
    eFileRdTwtDmShapeData,      // Twt Host Shape data should be read from file into shared memory
    eFileRdTwtHostOffsetData,   // Twt Host Offset data should be read from file into shared memory
    eFileRdWfrDmShapeData,      // Wfr Host Shape data should be read from file into shared memory
    eFileRdWfrHostOffsetData,   // Wfr Host Offset data should be read from file into shared memory

    eFileRdInitialModalGains,   // Read the optimizer gains (initial)
    eFileRdHostModalGains,      // Read the optimizer gains as specified by the host
    eFileRdModalGainSelectors,  // Read the optimizer individual modal selector file

    eFileWriteParmsData,        // parameter data should be written to file from shared memory

    // Order for eFileWriteWfsDarkData_0 to eFileWriteWfsDarkData_nn is necessary ( used in code to access )
    eFileWriteWfsDarkData_0,    // Wfs Dark data should be written to file from shared memory
    eFileWriteWfsDarkData_1,    // Wfs Dark data should be written to file from shared memory
    eFileWriteWfsDarkData_2,    // Wfs Dark data should be written to file from shared memory
    eFileWriteWfsDarkData_3,    // Wfs Dark data should be written to file from shared memory
    eFileWriteWfsDarkData_4,    // Wfs Dark data should be written to file from shared memory

    // Order for eFileWriteWfsBkGrndData_0 to eFileWriteWfsBkGrndData_nn is necessary ( used in code to access )
    eFileWriteWfsBkGrndData_0,  // Wfs BkGrnd data should be written to file from shared memory
    eFileWriteWfsBkGrndData_1,  // Wfs BkGrnd data should be written to file from shared memory
    eFileWriteWfsBkGrndData_2,  // Wfs BkGrnd data should be written to file from shared memory
    eFileWriteWfsBkGrndData_3,  // Wfs BkGrnd data should be written to file from shared memory
    eFileWriteWfsBkGrndData_4,  // Wfs BkGrnd data should be written to file from shared memory

    // Order for eFileWriteWfsFlatData_0 to eFileWriteWfsFlatData_nn is necessary ( used in code to access )
    eFileWriteWfsFlatData_0,    // Wfs Flat data should be written to file from shared memory
    eFileWriteWfsFlatData_1,    // Wfs Flat data should be written to file from shared memory
    eFileWriteWfsFlatData_2,    // Wfs Flat data should be written to file from shared memory
    eFileWriteWfsFlatData_3,    // Wfs Flat data should be written to file from shared memory
    eFileWriteWfsFlatData_4,    // Wfs Flat data should be written to file from shared memory

    eFileWriteRefCentQData,     // quad-cell reference centroid data should be written to file from shared memory
    eFileWriteRefOffsQData,     // quad-cell reference offset data should be written to file from shared memory
    eFileWriteReserve1,         //
    eFileWriteReserve2,         //
    eFileWriteTwtSM,            // Write out the tweeter System Matrix
    eFileWriteOptGains,         // Write out current optimizer gains

    eFileWriteTwtLabFlat,       // Tweeter lab flat phase data should be written to file from shared memory
    eFileWriteTwtSysFlat,       // Tweeter sys flat phase data should be written to file from shared memory
    eFileWriteTwtDmShape,       // Tweeter dm shape phase data should be written to file from shared memory

    eFileWriteWfrLabFlat,       // Woofer lab flat phase data should be written to file from shared memory
    eFileWriteWfrSysFlat,       // Woofer sys flat phase data should be written to file from shared memory
    eFileWriteWfrDmShape,       // Woofer dm shape phase data should be written to file from shared memory

    eFileWriteMakeInFunc,

    eFileWriteOpenBackData,     // WFS background data for open loop program
    eFileWriteClosedBackData,   // WFS background data for open loop program

    // Special Purpose
    eFileWriteCmdStatus
};


enum
{
    // map2slopeProcess Status types
    eThreadStart,                   // Flag from HostDiag to start thread - only valid if status = 'eThreadStopped'
    eThreadInit,
    eThreadProcess,
    eThreadAbortReq,
    eThreadStopped
};


// Defines

#define MAX_PATHNAME_LENGTH 128     // this can include the filename
#define MAX_FILENAME_LENGTH 64      // this can include the date directory name (mmyydd), if there is one

#define TEXT_BUF_LNGTH 128          // length of text buffers used primarily to set up messages for HostDiagSendStrngNoParm()


// Typedefs


// Global Variables


// Global Function Prototypes
void gpAoSrtFileMngr( short fileCmd );
void gpAoSrtFileMngr_Init( void );
void gpAoSrtFileMngr_WriteParms( void );
void gpAoSrtFileMngr_ProcCmd( short fileCmd );
void gpAoSrtFileMngr_SetCmdStatus( void );
void gpAoSrtFileMngr_abortMapToSlopesThread( void );
void gpAoSrtFileMngr_abortTimerThread( void );

#endif

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
