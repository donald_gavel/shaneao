/* 
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2006                         (c) 2006
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME
 *  src/gpStatTesterClient.C
 *
 * PURPOSE:
 *  Provide a hard test program for STATUS SERVER client calls.
 *
 * FUNCTION NAME(S)
 *
 *INDENT-OFF*
 * $Log: gpAocStatMonitor.cpp,v $
 * Revision 1.1  2012/09/12 19:54:18  will
 * *** empty log message ***
 *
 * Revision 1.1  2009/06/04 16:03:01  sjones
 * Initial checkin - These do not work at this time - need to be modified for AOC.
 *
  *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/

#ifdef __cplusplus
extern "C" {
#endif

/*!
 *  Defines
 */
#define GPI_LOCAL_DEBUG_LEVEL statTesterClientLocalDebugLevel // Debug level

#define IP_MAX_STR_LEN 16

/*!
 * Includes
 */
#include <stdio.h>
#include <string.h>
#include <sys/time.h>

#include <gpStatClient.h>
#include <gpStatTimer.h>

#include <gpUtGeneral.h>
#include <gpUtCLArg.h>
#include <gpUtDebug.h>

/*!
 *  Typedefs
 */
typedef struct
{
	char name[GPI_MAX_NAME_LEN];
	double period;
	int comp;
} statSub_t;

typedef struct
{
	unsigned int maxSize;
	unsigned int currentSize;
	statSub_t ** subArray;
} statSubTable_t;


/*!
 *  Variables
 */
int  statTesterClientLocalDebugLevel = GPI_DEBUG_MIN;       // Start level

static char ipStr[IP_MAX_STR_LEN];
static unsigned int portNum = 50000;
static char configFile[GPI_MAX_NAME_LEN];
static statSubTable_t * subTable = NULL;

/*!
 *  Prototypes
 */

static int printUpdateTest( char ** nameArray, int arraySize );

static int connectTest( void );

static int noSubscriptionTest( void );

static int invalidSubscribeTest( void );

static int multipalSubscribeTest( char * name );

static int multipalSubscribeTimerTest( char * name );

static int updateReceivedTest( char ** nameArray, int arraySize );

static int updateReceivedTimerTest( char ** nameArray, int arraySize );

static int periodSubscribeTest( void );

static int configSubscribeTest( void );

static statSubTable_t * readConfig( const char * configFile );

/*
 ************************************************************************
 *+
 * FUNCTION: main
 *
 * RETURNS: int
 *
 * DESCRIPTION:
 *   main to call gpStatClientTimer
 *
 *
 *-
 ************************************************************************
 */
int main
(
    int argc,		/* Number of command line args */
    const char *argv[]	/* List of command line args */
)
{
	int err;
	int rv = 0;
	int rvCase;

	int arraySize = 166;
	char * nameArray[] = {
	  "ifs.observation.cameraReady", 
	  "ifs.observation.currentIntegrationTime", 
	  "ifs.observation.takeExposureIntegrationTime", 
	  "ifs.observation.observeIntegrationTime", 
	  "ifs.observation.currentSamplingMode", 
	  "ifs.observation.takeExposureSamplingMode", 
	  "ifs.observation.observeSamplingMode", 
	  "ifs.observation.currentSamplingModeAsString", 
	  "ifs.observation.takeExposureSamplingModeAsString", 
	  "ifs.observation.observeSamplingModeAsString", 
	  "ifs.observation.currentNumberOfReads", 
	  "ifs.observation.takeExposureNumberOfReads", 
	  "ifs.observation.observeNumberOfReads", 
	  "ifs.observation.currentReadNumber", 
	  "ifs.observation.currentNumberOfCoadds", 
	  "ifs.observation.takeExposureNumberOfCoadds", 
	  "ifs.observation.observeNumberOfCoadds", 
	  "ifs.observation.currentCoaddNumber", 
	  "ifs.observation.observeSubregionStartx", 
	  "ifs.observation.observeSubregionStarty", 
	  "ifs.observation.observeSubregionEndx", 
	  "ifs.observation.observeSubregionEndy", 
	  "ifs.observation.takeExposureSubregionStartx", 
	  "ifs.observation.takeExposureSubregionStarty", 
	  "ifs.observation.takeExposureSubregionEndx", 
	  "ifs.observation.takeExposureSubregionEndy", 
	  "ifs.observation.exposureInProgress", 
	  "ifs.observation.acquisitionInProgress", 
	  "ifs.observation.readoutInProgress", 
	  "ifs.observation.dataWritingInProgress", 
	  "ifs.observation.exposureAbortInProgress", 
	  "ifs.observation.exposureAbortLevel", 
	  "ifs.observation.exposureStatus", 
	  "ifs.observation.lastExposureFilename", 
	  "ifs.observation.currentExposureFilename", 
	  "ifs.observation.exposureFileDirectory", 
	  "ifs.observation.testStatus", 
	  "ifs.observation.simulate", 
	  "ifs.observation.debug", 

	  "ifs.pressure.isHardwareInitialized",
	  "ifs.pressure.pressureInmTorr01", 
	  "ifs.pressure.pressureInmTorr02",
	  "ifs.pressure.filter1", 
	  "ifs.pressure.filter2", 
	  "ifs.pressure.gasCorrectionFactor1", 
	  "ifs.pressure.gasCorrectionFactor2",
	  "ifs.pressure.gaugeActive1", 
	  "ifs.pressure.gaugeActive2", 
	  "ifs.pressure.isLogging", 
	  "ifs.pressure.isPolling", 
	  "ifs.pressure.pollingRateInSeconds", 
	  "ifs.pressure.logFilename", 
	  "ifs.pressure.pressureUpperLimit", 
	  "ifs.pressure.pressureLowerLimit", 
	  "ifs.pressure.pressureOutOfRange01", 
	  "ifs.pressure.pressureOutOfRange02", 
	  "ifs.pressure.pressureReadingMismatch", 
	  "ifs.pressure.allowablePressureMismatchAbsolute", 
	  "ifs.pressure.allowablePressureMismatchPercent", 
/*	  "ifs.pressure.pressureAlarm", */
	  "ifs.pressure.testStatus", 
	  "ifs.pressure.simulate", 
	  "ifs.pressure.debug", 

	  "ifs.temperature.isHardwareInitialized", 
	  "ifs.temperature.temperatureInKelvinSensor01", 
	  "ifs.temperature.temperatureInKelvinSensor02", 
	  "ifs.temperature.temperatureInKelvinSensor03", 
	  "ifs.temperature.temperatureInKelvinSensor04", 
	  "ifs.temperature.temperatureInKelvinSensor05", 
	  "ifs.temperature.temperatureInKelvinSensor06", 
	  "ifs.temperature.temperatureInKelvinSensor07", 
	  "ifs.temperature.temperatureInKelvinSensor08", 
	  "ifs.temperature.temperatureInKelvinSensor09", 
	  "ifs.temperature.temperatureInKelvinSensor10", 
	  "ifs.temperature.sensorLocation01", 
	  "ifs.temperature.sensorLocation02", 
	  "ifs.temperature.sensorLocation03", 
	  "ifs.temperature.sensorLocation04", 
	  "ifs.temperature.sensorLocation05", 
	  "ifs.temperature.sensorLocation06", 
	  "ifs.temperature.sensorLocation07", 
	  "ifs.temperature.sensorLocation08", 
	  "ifs.temperature.sensorLocation09", 
	  "ifs.temperature.sensorLocation10", 
	  "ifs.temperature.outputPercentageChannel1", 
	  "ifs.temperature.outputPercentageChannel2", 
	  "ifs.temperature.isTemperatureRegulatedChannel1", 
	  "ifs.temperature.isTemperatureRegulatedChannel2", 
	  "ifs.temperature.targetTemperatureChannel1", 
	  "ifs.temperature.targetTemperatureChannel2", 
	  "ifs.temperature.pidPChannel1", 
	  "ifs.temperature.pidIChannel1", 
	  "ifs.temperature.pidDChannel1", 
	  "ifs.temperature.pidPChannel2", 
	  "ifs.temperature.pidIChannel2", 
	  "ifs.temperature.pidDChannel2", 
	  "ifs.temperature.maximumPositiveHeatingOutputChangeChannel1", 
	  "ifs.temperature.maximumPositiveHeatingOutputChangeChannel2", 
	  "ifs.temperature.maximumNegativeHeatingOutputChangeChannel1", 
	  "ifs.temperature.maximumNegativeHeatingOutputChangeChannel2", 
	  "ifs.temperature.maximumTemperatureChangeChannel1", 
	  "ifs.temperature.maximumTemperatureChangeChannel2", 
	  "ifs.temperature.outputRangeChannel1", 
	  "ifs.temperature.maximumOutputRangeChannel1", 
	  "ifs.temperature.maximumCurrentNumberChannel1", 
	  "ifs.temperature.maximumCurrentInAmpsChannel1", 
	  "ifs.temperature.isLogging", 
	  "ifs.temperature.isPolling", 
	  "ifs.temperature.pollingRateInSeconds", 
	  "ifs.temperature.logFilename", 
/*	  "ifs.temperature.temperatureAlarm01", 
	  "ifs.temperature.temperatureAlarm02", 
	  "ifs.temperature.temperatureAlarm03", 
	  "ifs.temperature.temperatureAlarm04", 
	  "ifs.temperature.temperatureAlarm05", 
	  "ifs.temperature.temperatureAlarm06", 
	  "ifs.temperature.temperatureAlarm07", 
	  "ifs.temperature.temperatureAlarm08", 
	  "ifs.temperature.temperatureAlarm09", 
	  "ifs.temperature.temperatureAlarm10", */
	  "ifs.temperature.temperatureLowerLimit01", 
	  "ifs.temperature.temperatureLowerLimit02", 
	  "ifs.temperature.temperatureLowerLimit03", 
	  "ifs.temperature.temperatureLowerLimit04", 
	  "ifs.temperature.temperatureLowerLimit05", 
	  "ifs.temperature.temperatureLowerLimit06", 
	  "ifs.temperature.temperatureLowerLimit07", 
	  "ifs.temperature.temperatureLowerLimit08", 
	  "ifs.temperature.temperatureLowerLimit09", 
	  "ifs.temperature.temperatureLowerLimit10", 
	  "ifs.temperature.temperatureUpperLimit01", 
	  "ifs.temperature.temperatureUpperLimit02", 
	  "ifs.temperature.temperatureUpperLimit03", 
	  "ifs.temperature.temperatureUpperLimit04", 
	  "ifs.temperature.temperatureUpperLimit05", 
	  "ifs.temperature.temperatureUpperLimit06", 
	  "ifs.temperature.temperatureUpperLimit07", 
	  "ifs.temperature.temperatureUpperLimit08", 
	  "ifs.temperature.temperatureUpperLimit09", 
	  "ifs.temperature.temperatureUpperLimit10", 
	  "ifs.temperature.temperatureMaxChange01", 
	  "ifs.temperature.temperatureMaxChange02", 
	  "ifs.temperature.temperatureMaxAllowedDeviationFromSetpoint01", 
	  "ifs.temperature.temperatureMaxAllowedDeviationFromSetpoint02", 
	  "ifs.temperature.temperatureOutOfRange01", 
	  "ifs.temperature.temperatureOutOfRange02", 
	  "ifs.temperature.temperatureOutOfRange03", 
	  "ifs.temperature.temperatureOutOfRange04", 
	  "ifs.temperature.temperatureOutOfRange05", 
	  "ifs.temperature.temperatureOutOfRange06", 
	  "ifs.temperature.temperatureOutOfRange07", 
	  "ifs.temperature.temperatureOutOfRange08", 
	  "ifs.temperature.temperatureOutOfRange09", 
	  "ifs.temperature.temperatureOutOfRange10", 
	  "ifs.temperature.temperatureChangingTooFast01", 
	  "ifs.temperature.temperatureChangingTooFast02", 
	  "ifs.temperature.temperatureSetpointDeviation01", 
	  "ifs.temperature.temperatureSetpointDeviation02", 
	  "ifs.temperature.testStatus", 
	  "ifs.temperature.simulate", 
	  "ifs.temperature.debug",

	  "ifs.pupilviewing.cameraReady", 
	  "ifs.pupilviewing.integrationTime", 
	  "ifs.pupilviewing.numberOfCoadds", 
	  "ifs.pupilviewing.currentCoaddNumber", 
	  "ifs.pupilviewing.exposureInProgress", 
	  "ifs.pupilviewing.readoutInProgress", 
	  "ifs.pupilviewing.dataWritingInProgress", 
	  "ifs.pupilviewing.exposureAbortInProgress", 
	  "ifs.pupilviewing.exposureAbortLevel", 
	  "ifs.pupilviewing.exposureStatus", 
	  "ifs.pupilviewing.lastExposureFilename", 
	  "ifs.pupilviewing.currentExposureFilename", 
	  "ifs.pupilviewing.exposureFileDirectory", 
	  "ifs.pupilviewing.testStatus", 
	  "ifs.pupilviewing.simulate", 
	  "ifs.pupilviewing.debug" 
				};

	int helpFlag = 0;

	int allFlag = 0;
	int printUpdateFlag = 0;
	int connectFlag = 0;
	int noSub = 0;
	int invalidSub = 0;
	int multiSubFlag = 0;
	int multiSubTimedFlag = 0;
	int receivedFlag = 0;
	int receivedTimedFlag = 0;
	int periodSubFlag = 0;
	int configFlag = 0;

	/* set defualt IP address to local host */
	strcpy( ipStr, "127.0.0.1" );

	static GP_UT_CLARG_STRUCT info[] =
    {    
		{ "-help", "Displays command line help.",
			0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &helpFlag },
		
		{ "-all", "Runs all test cases", 0u, 0u, GP_UT_CLARG_TYPE_NONE, 
			NULL, 0, &allFlag },

		{ "-update", "Subscribes to all elements and prints updates to "
			"stdout", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, 
			&printUpdateFlag },

		{ "-connect", "Checks if client can connect/disconnect to Status "
			"Server", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &connectFlag },

		{ "-noSub", "Checks that no updates are sent within 1 second, if "
			"nothing is subscribed to.", 0u, 0u, GP_UT_CLARG_TYPE_NONE, 
			NULL, 0, &noSub },

		{ "-invalidSun", "Checks that an invalid subscription is handled "
			"properly.", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, 
			&invalidSub },

		{ "-multiSub", "Checks that multipal subscriptions to the same "
			"item are handled properly.", 0u, 0u, GP_UT_CLARG_TYPE_NONE, 
			NULL, 0, &multiSubFlag },

		{ "-multiSubTimed", "Checks that multipal subscriptions to the "
			"same item are handled properly, with timing results.", 0u, 0u, 
			GP_UT_CLARG_TYPE_NONE, NULL, 0, &multiSubTimedFlag },

		{ "-received", "Checks the updates are received for subscribed "
			"items.", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, 
			&receivedFlag },

		{ "-receivedTimed", "Checks the updates are received for "
			"subscribed items, with timing results.", 0u, 0u, 
			GP_UT_CLARG_TYPE_NONE, NULL, 0, &receivedTimedFlag },

		{ "-periodSub", "Checks the items can be subscribed to at a "
			"variaty of periods", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, 
			&periodSubFlag },

		{ "-ip", "Sets the IP of the Status Server.",
           1u, 1u, GP_UT_CLARG_TYPE_STRING, ipStr, IP_MAX_STR_LEN, NULL },

         { "-port", "Sets the port number with the client will connect to.", 
		 	1u, 1u, GP_UT_CLARG_TYPE_INT, &portNum, 
			sizeof(portNum), NULL },

		{ "-config", "Sets the config file to be used, if not set the "
			"whole GMB area will be updated.", 1u, 1u,
			GP_UT_CLARG_TYPE_STRING, configFile, GPI_MAX_STRING_LEN, 
			&configFlag },

         { NULL, NULL, 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, NULL }
    };

	if ( gpUtCLArgAutoProcess(info, sizeof(info), argc, argv) < 0)
    {
        return EXIT_FAILURE;
    }

    if (helpFlag != 0)
    {
        gpUtCLArgHelp(info);
        return EXIT_SUCCESS;
    }

	if ( configFlag != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_NONE," : using Config file <%s>\n", 
			configFile ); 

		subTable = readConfig( configFile );
	}

	GPI_DEBUG( GPI_DEBUG_NONE," : main : connecting to IP <%s> on port "
		"<%d>\n", ipStr, portNum );
	
	/* all expect:
	 *		printUpdateTest - b/c it never ends 
	 *		multipalSubscribeTest - b/c it's a subset of 
	 *			multiSubTimedFlag
	 *		updateReceivedTest - b/c it's a subset of receivedFlag
	 */
	if ( allFlag != 0 )
	{
		connectFlag = 1;
		noSub = 1;
		invalidSub = 1;
		multiSubTimedFlag = 1;
		receivedTimedFlag = 1;
		periodSubFlag = 1;
	}

	/* the test funtions are not || directly with rv like:
	 *		rv = rv || testFunction(); 	
	 * b/c if rv is set then the test function is not ran.
	 * Could use a | (bitwise or), but this is more clear */

	/* This tests does not need the connection to already be open and then 
	 * closed */
	if ( connectFlag != 0 )
	{
		rvCase = connectTest();
		rv = rv || rvCase;
	}

	// connect to Status Server
	err = gpStatClientConnect( ipStr, portNum );  
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : main : ERROR : Could not connect "
			"to SS <%d>\n", err);
			
		GPI_DEBUG( GPI_DEBUG_NONE, " : main : *****FAIL**** : <%d>\n", 
			err );

		return err;	
	}
	
	/* go through all test case flags */
	if ( printUpdateFlag != 0 )
	{
		rvCase = printUpdateTest( nameArray, arraySize );
		rv = rv || rvCase;
	}

	if ( noSub != 0 )
	{
		rvCase = noSubscriptionTest();
		rv = rv || rvCase;
	}
	
	if ( invalidSub != 0 )
	{
		rvCase = invalidSubscribeTest();
		rv = rv || rvCase;
	}	
	
	if ( multiSubFlag != 0 )
	{
		rvCase = multipalSubscribeTest( nameArray[0] );
		rv = rv || rvCase;
	}	
	
	if ( multiSubTimedFlag != 0 )
	{
		rvCase = multipalSubscribeTest( nameArray[0] );
		rvCase = rvCase || multipalSubscribeTimerTest( nameArray[0] );
		rv = rv || rvCase;
	}	
	
	if ( receivedFlag != 0 )
	{
		rvCase = updateReceivedTest( nameArray, arraySize );
		rv = rv || rvCase;
	}	

	if ( receivedTimedFlag != 0 )
	{
		rvCase = updateReceivedTest( nameArray, arraySize );
		rvCase = rvCase || updateReceivedTimerTest( nameArray, arraySize );
		rv = rv || rvCase;
	}
	
	if ( periodSubFlag != 0 )
	{
		rvCase = periodSubscribeTest();
		rv = rv || rvCase;
	}	


	if ( configFlag != 0 )
	{
		rvCase = configSubscribeTest();
		rv = rv || rvCase;
	}
	
	// close Status Server connection
	err = gpStatClientClose();
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : main : ERROR : Could not "
			"close connection to SS <%d>\n", err);
	
		GPI_DEBUG( GPI_DEBUG_NONE, " : main : *****FAIL**** : <%d>\n", 
			err );

		return err;	

	}

	if ( rv == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_NONE, " : main : *****PASS**** : <%d>\n", rv );
	}
	else
	{
		GPI_DEBUG( GPI_DEBUG_NONE, " : main : *****FAIL**** : <%d>\n", rv );
	}
	
	return rv;
}	


/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int printUpdateTest
( 
	char ** nameArray,
	int arraySize
)
{
	int ndx;
	int err;
	stat_element_t * statElem;
	
	const double period = 0.01;
	const double staleTime = 60; // sec
	const int comp = (int) (staleTime/period); 

	for( ndx = 0; ndx < arraySize; ndx ++ )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : printUpdateTest : "
			"subscribing to <%s>\n", nameArray[ndx] );
		
		err = gpStatClientSubscribe( nameArray[ndx], period, 0, comp );
		
		if ( err != 0 )
		{
			GPI_DEBUG( GPI_DEBUG_MIN, " : printUpdateTest : ERROR "
				": subscription <%s>, error = <%d>\n", nameArray[ndx], err );

			return err;
		}
		
		GPI_DEBUG( GPI_DEBUG_MIN, " : printUpdateTest : "
			"subscription <%s> successful with period of <%lf> [msec]\n", 
			nameArray[ndx], 1.0e3 * period );
		
	}	

	GPI_DEBUG( GPI_DEBUG_MIN, " : printUpdateTest : display subscribed "
		"updates when changed\n\tShould also display an update every minute "
		"if data has not changed...\n" );

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();	

	// loop
	while( 1 )
	{
		statElem = gpStatClientListenUpdate();
		
		// if valid statElem and net time is greater than calcStartTime,
		// then proccess element
		if ( statElem != NULL  )
		{
		  switch (statElem->dt) {
		  case GPI_DATA_TYPE_CHAR:
		    /* currently, GMB strings have type GPI_DATA_TYPE_CHAR
		     * instead of GPI_DATA_TYPE_STRING. therefore, we will
		     * print the whole string instead of just a char
		     * since the IFS doesn't have any char GMB vars.
		     */

		    /* this is for a string */
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (char/string): <%s> = <%s>\n",
		      statElem->name, ( (char *) statElem->addr ) );

		    /*  this is for a single char
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (char): <%s> = <%c>\n",
		      statElem->name, *( (char *)statElem->addr ) ); 
		    */
		    break;
		  case GPI_DATA_TYPE_UCHAR:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (uchar): <%s> = <%c> <0x%X>\n",
		      statElem->name, *( (unsigned char *)statElem->addr ), *( (unsigned char *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_SCHAR:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (schar): <%s> = <%c>\n",
		      statElem->name, *( (signed char *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_SHORT:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (short): <%s> = <%d>\n",
		      statElem->name, *( (short *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_USHORT:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (ushort): <%s> = <%u> <0x%X>\n",
		      statElem->name, *( (unsigned short *)statElem->addr ), *( (unsigned short *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_INT:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (int): <%s> = <%d>\n",
		      statElem->name, *( (int *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_UINT:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (uint): <%s> = <%u> <0x%X>\n",
		      statElem->name, *( (unsigned int *)statElem->addr ), *( (unsigned int *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_LONG:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (long): <%s> = <%ld>\n",
		      statElem->name, *( (long *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_ULONG:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (ulong): <%s> = <%lu> <0x%lX>\n",
		      statElem->name, *( (unsigned long *)statElem->addr ), *( (unsigned long *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_FLOAT:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (float): <%s> = <%f>\n",
		      statElem->name, *( (float *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_DOUBLE:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (double): <%s> = <%f>\n",
		      statElem->name, *( (double *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_LDOUBLE:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (ldouble): <%s> = <%Lg>\n",
		      statElem->name, *( (long double *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_STRING:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (string): <%s> = <%s>\n",
		      statElem->name, ( (char *)statElem->addr ) );
		    break;
		  case GPI_DATA_TYPE_SIZE_T:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (size_t): <%s> = <%lu> <0x%lX>\n",
		      statElem->name, *( (size_t *)statElem->addr ), *( (size_t *)statElem->addr ) );
		    break;
		  default:
		    GPI_DEBUG( GPI_DEBUG_NONE, " : printUpdateTest (default) : <%s> = <%s>\n",
		      statElem->name, ( (char *)statElem->addr ) );
		    break;

		  }
		}
	}


	GPI_DEBUG( GPI_DEBUG_MIN, " : printUpdateTest : PASS\n" );

	return 0;
}

/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int connectTest
(
	void
)
{
	int err;

	// connect to Status Server
	err = gpStatClientConnect( ipStr, portNum );  
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : connectTest : ERROR : Could not "
			"connect to SS\n");
		return err;
	}

	// check connection state
	err = gpStatClientCheckConnection();
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : connectTest : ERROR : check "
			"connection to SS FAIL\n" );
		return err;
	}

	// close Status Server connection
	err = gpStatClientClose();
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : connectTest : ERROR : Could not "
			"close connection to SS\n");
		return err;
	}

	// check connection state
	err = gpStatClientCheckConnection();
	if ( err == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : connectTest : ERROR : check "
			"connection reports open after connection has been closed\n" );
		return -1;
	}

	GPI_DEBUG( GPI_DEBUG_MIN, " : connectTest : PASS\n" );
	
	return 0;
}

/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int noSubscriptionTest
(
	void
)
{
	int rv = 0;
	stat_element_t * statElem;
	
	struct timeval startTime;
	struct timeval nowTime;
	const double endTime = 1;  // seconds
	double elapsed;

	GPI_DEBUG( GPI_DEBUG_MIN, " : noSubscriptionTest : no "
		"subscriptions\n" );

	gettimeofday( &startTime, NULL );

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// listen for updates
	while( 1 )
	{
		gettimeofday( &nowTime, NULL );

		elapsed = (nowTime.tv_sec + 
			1.0e-6 * nowTime.tv_usec) -
			(startTime.tv_sec + 
			1.0e-6 * startTime.tv_usec); 
		
		// if time for start to now is greater than end, then break
		if ( elapsed > endTime )
		{	
			break;
		}
		
		statElem = gpStatClientListenUpdate();

		if ( statElem != NULL )
		{
			rv = -2;
			break;	
		}
	}	

	// check return value
	if ( rv == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : noSubscriptionTest : PASS\n" );
	}
	else 
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : noSubscriptionTest : FAIL : "
			" Unexpected update received <%s>\n", statElem->name );
	}

	return rv;	
}



/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int invalidSubscribeTest
(
	void
)
{
	int ndx;
	int err;
	int rv = 0;
	char name[32] = "INVALID_SUBSCRIPTION_NAME";

	GPI_DEBUG( GPI_DEBUG_MIN, " : invalidSubscribeTest : subscribing to "
		"<%s>\n", name );

	// try to subscribe to invalid name 10 times
	for( ndx = 0; ndx < 10; ndx ++ )
	{
		err = gpStatClientSubscribe( name, 1, 0, 1 );
		if ( err == 0 )
		{
			rv = -1;
		}
	}	

	if ( rv != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : invalidSubscribeTest : FAIL : "
			" successful subscription to invalid name <%s>\n", name );

		return rv;	
	}

	rv = noSubscriptionTest();
	
	// check return value
	if ( rv == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : invalidSubscribeTest : PASS\n" );
	}

	return rv;	
}


/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int multipalSubscribeTest
( 
	char * name 
)
{
	int ndx;
	int err;
	int rv = 0;
	const int subscribeCount = 5;
	const double period = 0.1;

	GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : subscribing "
		"to <%s> <%d> times\n", name, subscribeCount );

	// subscribe to variable first time, this should work
	err = gpStatClientSubscribe( name, period, 0, 1 );
	
	if ( err != 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : "
			"ERROR : unable to subscribe to <%s>, error = <%d>\n", 
			name, err );
				
		return err;
	}

	GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : "
		"first subscription to <%s> successful with period of <%lf> "
		"[msec]\n", name, 1.0e3 * period );

	// subscribe to the same variable multipal times
	for( ndx = 1; ndx < subscribeCount; ndx ++ )
	{
		err = gpStatClientSubscribe( name, period, 0, 1 );
		
		if ( err != 0 )
		{
			GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : "
				"<%d> : subscription <%s>, error = <%d>\n", ndx+1, name, 
				err );
		}
		else
		{
			rv++;

			GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : ERROR "
				"<%d> subscription <%s> successful\n",  ndx+1, name );
		}
	}

	if ( rv == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : PASS : "
			"only allowed to subscribe to <%s> once\n", name );
	}
	else
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTest : FAIL "
			"allowed to subscribe to <%s> <%d> times\n", name, rv+1 );

	}
	
	return rv;	
}


/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int multipalSubscribeTimerTest
( 
	char * name
)
{
	int rv;
	stat_element_t * statElem;
	int count;

	struct timeval lastUpdateTime;
	struct timeval nextUpdateTime;

	struct timeval startTime;
	struct timeval nowTime;

	const double calcStartTime = 1; // seconds
	const double endTime = 1 + calcStartTime; // seconds

	double elapsed;

	double meanTime;
	double maxTime;
	double minTime;

	meanTime = 0;
	maxTime = 0;
	minTime = endTime;
	count = 0;

	gettimeofday( &lastUpdateTime, NULL );
	gettimeofday( &startTime, NULL );
	
	GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTimerTest : check for "
		"updates, please wait <%lf> seconds\n", endTime );

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// loop
	while( 1 )
	{
		gettimeofday( &nowTime, NULL );

		elapsed = (nowTime.tv_sec + 
			1.0e-6 * nowTime.tv_usec) -
			(startTime.tv_sec + 
			1.0e-6 * startTime.tv_usec); 
		
		// if time for start to now is greater than end, then break
		if ( elapsed > endTime )
		{	
			break;
		}
		
		statElem = gpStatClientListenUpdate();
		
		// if valid statElem and net time is greater than calcStartTime,
		// then proccess element
		if ( statElem != NULL && elapsed > calcStartTime )
		{
			count++;

			gettimeofday( &nextUpdateTime, NULL );
			
			// calc time between updates
			elapsed = (nextUpdateTime.tv_sec + 
					1.0e-6 * nextUpdateTime.tv_usec) -
			  		(lastUpdateTime.tv_sec + 
					1.0e-6 * lastUpdateTime.tv_usec);  	

			lastUpdateTime = nextUpdateTime;

			// calc statistics 
			meanTime += elapsed;

			if ( elapsed > maxTime ) { maxTime = elapsed; }
			else if ( elapsed < minTime ) { minTime = elapsed; }

		}
		else if ( statElem != NULL )
		{
			// required so initial calc time is valid
			gettimeofday( &lastUpdateTime, NULL );
		}
	}

	meanTime = meanTime / count;

	GPI_DEBUG( GPI_DEBUG_MIN, " : multipalSubscribeTimerTest : update "
		"receaved for <%s>\n\t at delay time of [msec] : mean <%lf>, "
		"min <%lf>, max <%lf>, # samples = <%d> over a <%lf> second "
		"interval\n", name, 1.0e3 * meanTime, 1.0e3 * minTime, 
		1.0e3 * maxTime, count, endTime - calcStartTime );

	return rv;	
}

/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int updateReceivedTest
( 
	char ** nameArray,
	int arraySize
)
{
	int ndx;
	int err;
	int rv = 0;
	stat_element_t * statElem;
	int count[arraySize];
	
	const double period = 0.1;

	double elapsed;

	struct timeval startTime;
	struct timeval nowTime;
	const double endTime = 10 * period; // seconds

	for( ndx = 0; ndx < arraySize; ndx ++ )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : "
			"subscribing to <%s>\n", nameArray[ndx] );
		
		err = gpStatClientSubscribe( nameArray[ndx], period, 0, 1 );
		
		if ( err != 0 )
		{
			GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : ERROR "
				": subscription <%s>, error = <%d>\n", nameArray[ndx], err );

			return err;
		}
		
		GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : "
			"subscription <%s> successful with period of <%lf> [msec]\n", 
			nameArray[ndx], 1.0e3 * period );
		
	}	

	gettimeofday( &startTime, NULL );
	
	GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : check for "
		"updates, please wait <%lf> seconds\n", endTime );

	// init variables
	for( ndx = 0; ndx < arraySize; ndx ++ )
	{
		count[ndx] = 0;
	}
	
	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// loop
	while( 1 )
	{
		gettimeofday( &nowTime, NULL );

		elapsed = (nowTime.tv_sec + 
			1.0e-6 * nowTime.tv_usec) -
			(startTime.tv_sec + 
			1.0e-6 * startTime.tv_usec); 
		
		// if time for start to now is greater than end, then break
		if ( elapsed > endTime )
		{	
			break;
		}
		
		statElem = gpStatClientListenUpdate();
		
		// if valid statElem and net time is greater than calcStartTime,
		// then proccess element
		if ( statElem != NULL  )
		{
			// look for name ndx
			for( ndx = 0; ndx < arraySize; ndx++ )
			{
				if( strcmp( statElem->name, nameArray[ndx] ) == 0 )
				{
					count[ndx]++;
				}
			}
		}
		else
		{
			if ( gpStatClientCheckConnection() != 0 )
			{
				GPI_DEBUG( GPI_DEBUG_NONE, " : updateReceivedTest : "
					"ERROR : sockets unexpectedly closed\n");
					
				/* break while loop, since connect is closed */
				break;
			}

		}
	}

	for ( ndx = 0; ndx < arraySize; ndx++ )
	{
		if ( count[ndx] == 0 )
		{
			rv++;
			
			GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : ERROR : "
				" failed to recieve update of <%s> over a <%lf> second "
				"interval\n", nameArray[ndx], endTime );
		}		
	}

	if ( rv == 0 )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : PASS\n" );
	}
	else
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTest : FAIL : "
				" failed to recieve <%d> updates\n", rv );
	}

	return rv;
}


/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int updateReceivedTimerTest
( 
	char ** nameArray,
	int arraySize
)
{
	int ndx;
	int rv = 0;
	stat_element_t * statElem;

	struct timeval lastUpdateTime[arraySize];
	struct timeval nextUpdateTime[arraySize];
	double elapsed;

	struct timeval startTime;
	struct timeval nowTime;

	const double calcStartTime = 1; // seconds
	const double endTime = 1 + calcStartTime; // seconds

	double meanTime[arraySize];
	double maxTime[arraySize];
	double minTime[arraySize];
	int count[arraySize];

	// init variables
	for( ndx = 0; ndx < arraySize; ndx ++ )
	{
		gettimeofday( &(lastUpdateTime[ndx]), NULL );

		meanTime[ndx] = 0;
		maxTime[ndx] = 0;
		minTime[ndx] = endTime;
		count[ndx] = 0;
	}
	
	gettimeofday( &startTime, NULL );
	
	GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTimerTest : check for "
		"updates, please wait <%lf> seconds\n", endTime );

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// loop
	while( 1 )
	{
		gettimeofday( &nowTime, NULL );

		elapsed = (nowTime.tv_sec + 
			1.0e-6 * nowTime.tv_usec) -
			(startTime.tv_sec + 
			1.0e-6 * startTime.tv_usec); 
		
		// if time for start to now is greater than end, then break
		if ( elapsed > endTime )
		{	
			break;
		}
		
		statElem = gpStatClientListenUpdate();

		// if valid statElem and net time is greater than calcStartTime,
		// then proccess element
		if ( statElem != NULL  && elapsed > calcStartTime )
		{
			// look for name ndx
			for( ndx = 0; ndx < arraySize; ndx++ )
			{
				if( strcmp( statElem->name, nameArray[ndx] ) == 0 )
				{
					break;
				}
			}
			
			count[ndx]++;

			// calc time between updates
			gettimeofday( &(nextUpdateTime[ndx]), NULL );
			
			elapsed = ( nextUpdateTime[ndx].tv_sec + 
					1.0e-6 * nextUpdateTime[ndx].tv_usec ) -
					( lastUpdateTime[ndx].tv_sec + 
					1.0e-6 * lastUpdateTime[ndx].tv_usec );
			
			lastUpdateTime[ndx] = nextUpdateTime[ndx];

			// calc statistics
			meanTime[ndx] += elapsed;

			if ( elapsed > maxTime[ndx] ) 
			{ 
				maxTime[ndx] = elapsed; 
			}
			else if ( elapsed < minTime[ndx] ) 
			{ 
				minTime[ndx] = elapsed; 
			}
		}
		else if ( statElem != NULL )
		{
			// look for name ndx
			for( ndx = 0; ndx < arraySize; ndx++ )
			{
				if( strcmp( statElem->name, nameArray[ndx] ) == 0 )
				{
					break;
				}
			}

			// required so initial calc time is valid
			gettimeofday( &(lastUpdateTime[ndx]), NULL );
		}
	}

	for ( ndx = 0; ndx < arraySize; ndx++ )
	{
		meanTime[ndx] = meanTime[ndx] / count[ndx];

		GPI_DEBUG( GPI_DEBUG_MIN, " : updateReceivedTimerTest : update "
			"receaved for <%s>\n\t at delay time of [msec] : mean <%lf>, "
			"min <%lf>, max <%lf>, # samples = <%d> over a <%lf> second "
			"interval\n", nameArray[ndx], 
			1.0e3 * meanTime[ndx], 1.0e3 * minTime[ndx], 
			1.0e3 * maxTime[ndx], count[ndx], endTime - calcStartTime  );
	}

	return rv;
}

/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int periodSubscribeTest
( 
	void 
)
{
	int ndx;
	int err;
	int rv = 0;
	char * nameArray[] = { "tlc.header.heartbeat", "aoc.header.heartbeat", 
					"cal.header.heartbeat", "ifs.header.heartbeat", 
					"tlc.header.state", "aoc.header.state", 
					"cal.header.state", "ifs.header.state" };

	double periodArray[] = { -1, 0, 0.001, 0.01, 0.1, 1, 10, 1e9 };
	int arraySize = 8;
	
	for( ndx = 0; ndx < arraySize; ndx++ )
	{
		err = gpStatClientSubscribe( nameArray[ndx], periodArray[ndx], 
			0, 1 );

		if ( err != 0 )
		{
			rv++;
			
			GPI_DEBUG( GPI_DEBUG_MIN, " : periodSubscribeTest : ERROR "
				": subscription <%s>, period = <%lf>, error = <%d>\n", 
				nameArray[ndx], periodArray[ndx], err );

			return err;
		}
		else
		{
			GPI_DEBUG( GPI_DEBUG_MIN, " : periodSubscribeTest : successfully"
				": subscribed to <%s> with period = <%lf>\n", 
				nameArray[ndx], periodArray[ndx] );

		}
	}

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// TODO: check periods

	return rv;
}


/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static int configSubscribeTest
( 
	void 
)
{
	unsigned int ndx;
	int err;
	stat_element_t * statElem;
	
	for( ndx = 0; ndx < subTable->currentSize; ndx ++ )
	{
		GPI_DEBUG( GPI_DEBUG_MIN, " : configSubscribeTest : "
			"subscribing to <%s>\n", subTable->subArray[ndx]->name );
		
		err = gpStatClientSubscribe( subTable->subArray[ndx]->name, 
			subTable->subArray[ndx]->period, 0, 
			subTable->subArray[ndx]->comp );
		
		if ( err != 0 )
		{
			GPI_DEBUG( GPI_DEBUG_MIN, " : configSubscribeTest : ERROR "
				": subscription <%s>, error = <%d>\n", 
				subTable->subArray[ndx]->name, err );
		}
	}	

	/* tells the SS to start sending updates now */
	gpStatClientStartUpdates();

	// loop
	while( 1 )
	{
		statElem = gpStatClientListenUpdate();
		
		// if valid statElem and net time is greater than calcStartTime,
		// then proccess element
		if ( statElem != NULL  )
		{
			// TODO: should use statElem data type to cast data
			GPI_DEBUG( GPI_DEBUG_NONE, " : configSubscribeTest : <%s> = "
				"<%d>\n", statElem->name, *( (int *)statElem->addr ) );
		}
		else
		{
			if ( gpStatClientCheckConnection() != 0 )
			{
				GPI_DEBUG( GPI_DEBUG_NONE, " : configSubscribeTest : "
					"ERROR : sockets unexpectedly closed\n");
					
				/* break while loop, since connect is closed */
				break;
			}
		}
	}

	GPI_DEBUG( GPI_DEBUG_MIN, " : configSubscribeTest : PASS\n" );

	return 0;
}

/*
 ************************************************************************
 *+
 * FUNCTION: 
 *
 * RETURNS: 
 *
 * DESCRIPTION:
 *
 *-
 ************************************************************************
 */
static statSubTable_t * readConfig
(
	const char * configFile
)
{
	int err;
	unsigned int ndx;
	
	FILE * pFile;
	char line[ GPI_MAX_STRING_LEN ];
	
	char name[ GPI_MAX_NAME_LEN ];
	double unused;
	double period;
	int comp;
	
	statSubTable_t * timerTable = NULL;
	
	if ( configFile == NULL )
	{
		return NULL;
	}

	// open config file
	pFile = fopen( configFile, "r" );

	if ( pFile == NULL )
	{
		return NULL;
	}

	timerTable = (statSubTable_t *) malloc( 
		sizeof(statSubTable_t) );

	/* set initial values for time table */
	timerTable->maxSize = 2500;
	timerTable->currentSize = 0;
	timerTable->subArray = (statSub_t **) malloc( timerTable->maxSize 
		* sizeof(statSub_t *) );


	// reads config file and add to timer table
	ndx = 0;
	while ( fgets( line, sizeof(line), pFile ) != NULL )
	{
		// ignore line if starts with "#"
		if ( memcmp( line, "#", sizeof(char) ) != 0 ) 
		{
			// parse line
			err = sscanf( line, "%s %lf %lf %d", name, &unused, 
				&period, &comp ); 
			
			if ( err != 4 )
			{
				GPI_DEBUG( GPI_DEBUG_MIN, " : readConfig : "
					"ERROR : unable to read line <%s>\n", line);
				continue;
			}
			else
			{
				GPI_DEBUG( GPI_DEBUG_MAX, " : readConfig : "
					"reading line <%s>\n", line);
			}
			
			// if required space is larger then allocated space
			if ( timerTable->currentSize >= timerTable->maxSize )
			{
				GPI_DEBUG( GPI_DEBUG_NONE, " : readConfig : "
					"ERROR : too many variables : expecting max = <%d>\n"
					"No more variables will be added\n", 
					timerTable->maxSize );

				/* close file */
				fclose( pFile );

				return timerTable;
			}
		
			/* allocate space for timer struct */
			timerTable->subArray[ timerTable->currentSize ] = 
				( statSub_t * ) malloc( sizeof(statSub_t) );

			strcpy( timerTable->subArray[ timerTable->currentSize ]->name,
				name );
			timerTable->subArray[ timerTable->currentSize ]->period = period;
			timerTable->subArray[ timerTable->currentSize ]->comp = comp;

			timerTable->currentSize++;
		}
	}
	
	/* close file */
	fclose( pFile );

	return timerTable;
}


#ifdef __cplusplus
}
#endif
