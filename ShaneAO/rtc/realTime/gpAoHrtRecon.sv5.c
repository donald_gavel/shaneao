/**********************************************************************************************
*
* COPYRIGHT (C) 2008
*
* LAWRENCE LIVERMORE NATIONAL LABORATORY
* ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
*
***********************************************************************************************/
/*!
 *
 *	@file gpAoHrtRecon.c
 *
 * 	@version $Id: gpAoHrtRecon.sv5.c,v 1.1 2012/09/12 19:47:43 will Exp $
 * 	
 *	@brief 
 *	Perform wavefront reconstruction and related tasks.  
 *
 *	@b DESCRIPTION:
 *	This component contains functions to perform the computations necessary for wavefront
 *	reconstruction, apply the control law for the DM actuators,	and send the results on to be 
 *	output to the DMs.  The reconstructor is the Fourier Tranform Reconstructor and the control
 *	law is the Optimizing Fourier Controller (OFC).  
 *
 *	The Recon thread is included in this component.
 *
 *	This FTR code is based on the algProto example directory (currently @ /opt/gpi/code/algProto)
 *	
 *
 *	@b AUTHOR:
 *	DWP & SMJ & VR	DATE: 11/25/2008
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoHrtRecon()
 *	-# @c gpAoHrtRecon_Init()
 *	-# @c gpAoHrtRecon_InitMemory()
 *	-# @c gpAoHrtRecon_CleanMemory()
 *	-# @c gpAoHrtRecon_ProcCmd()
 *	-# @c gpAoHrtRecon_ProcessTweeter()
 *	-# @c gpAoHrtRecon_ProcessWoofer()
 *	-# @c gpAoHrtRecon_ProcessStates()
 *	-# @c gpAoHrtRecon_CloseOpenOFCLoop()
 *	-# @c gpAoHrtRecon_CloseOpenTweeterLoop()
 *	-# @c gpAoHrtRecon_CloseOpenWooferLoop()
 *	-# @c gpAoHrtRecon_CalcStats()
 *	-# @c gpAoHrtRecon_OffloadM1M2()
 *	-# @c gpAoHrtRecon_InFuncStart()
 *	-# @c gpAoHrtRecon_ProcessMakeInFunc()
 *	-# @c gpAoHrtRecon_StopMakeInFunc()
 *	-# @c gpAoHrtRecon_InitPupStartEnd()
 *	-# @c gpAoHrtRecon_InitTweeterSlaving()
 *	-# @c gpAoHrtRecon_InsertTwtSlave()
 *	-# @c gpAoHrtRecon_ReconFTR()
 *	-# @c gpAoHrtRecon_FTRAlgoThread()
 *	-# @c gpAoHrtRecon_FTRAlgoProcess()
 *	-# @c gpAoHrtRecon_FTRAlgoExtendPupil()
 *	-# @c gpAoHrtRecon_FTRAlgoInit()
 *	-# @c gpAoHrtRecon_InitThreading()
 *	-# @c gpAoHrtRecon_CleanThreading()
 *	-# @c gpAoHrtRecon_FTRAlgoInitMemory()
 *	-# @c gpAoHrtRecon_FTRAlgoCleanMemory()
 *	-# @c dumpCornerOfaMatrix()
 *	-# @c writeCompleteFitsFile()
 *	-# @c openFitsForWrite()
 *
 *	@b STATIC/LOCAL FUNCTION NAMES:
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#if (RTL_SYSTEM != 0)
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <math.h>
#else
#define _GNU_SOURCE
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <semaphore.h>
#include <sched.h>
#include <unistd.h>
#include <math.h>
#include <time.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/types.h> 
#include <linux/unistd.h> 
#endif

#if (RTL_SYSTEM == 0)
#include <gpUtGeneral.h>
#include "gpAoGmb.h"
#else
#include "gpUtGmbHrt.h"
#endif

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "gpAoHrtCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoHrtReconOpt.h"
#include "gpAoHrtParse.h" 
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"
#include "gpAoHrtFfts.h"

#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 ) )
#include "fitsio.h"
#endif

#define PD_SERIALNUMBER_SIZE 1

pid_t getpid( void );


//XXXdwpBadActSlaves -- start -- for testing bad actuator slaving
static const int twtSlavesBadActs_IDL[BAD_ACTS_SLVS][eTwtSlaveMaxRowCols] =  // tweeter slaving variables for bad actuators, IDL format
//     dest      src1      src2      src3
{ 
	{  13,  7,  14,  7,  13,  8,  -1,  -1},
	{  12,  8,  11,  8,  13,  8,  12,  9},
	{  40,  15,  39,  15,  40,  14,  40,  16},
	{  41,  16,  40,  16,  42,  16,  41,  17},
	{  21,  26,  20,  26,  21,  25,  21,  27},
	{  22,  27,  21,  27,  23,  27,  22,  28},
	{  35,  40,  34,  40,  36,  40,  35,  39},
	{  34,  41,  33,  41,  34,  40,  -1,  -1},
	{  19,  3,  20,  3,  19,  4,  -1,  -1},  // dim ring start
	{  28,  3,  27,  3,  28,  4,  -1,  -1},
	{  16,  4,  17,  4,  16,  5,  -1,  -1},
	{  31,  4,  30,  4,  31,  5,  -1,  -1},
	{  14,  5,  15,  5,  14,  6,  -1,  -1},
	{  33,  5,  32,  5,  33,  6,  -1,  -1},
	{  13,  6,  14,  6,  13,  7,  -1,  -1},
	{  35,  6,  34,  6,  35,  7,  -1,  -1},
	{  36,  7,  35,  7,  36,  8,  -1,  -1},
	{  10,  8,  11,  8,  10,  9,  -1,  -1},
	{  37,  8,  36,  8,  37,  9,  -1,  -1},
	{  9,  9,  10,  9,  9,  10,  -1,  -1},
	{  38,  9,  37,  9,  38,  10,  -1,  -1},
	{  8,  10,  9,  10,  8,  11,  -1,  -1},
	{  39,  10,  38,  10,  39,  11,  -1,  -1},
	{  7,  11,  8,  11,  7,  12,  -1,  -1},
	{  40,  11,  39,  11,  40,  12,  -1,  -1},
	{  6,  12,  7,  12,  6,  13,  -1,  -1},
	{  5,  14,  6,  14,  5,  15,  -1,  -1},
	{  4,  16,  5,  16,  4,  17,  -1,  -1},
	{  43,  16,  42,  16,  43,  17,  -1,  -1},
	{  3,  19,  4,  19,  3,  20,  -1,  -1},
	{  44,  19,  43,  19,  44,  20,  -1,  -1},
	{  22,  21,  21,  21,  22,  20,  -1,  -1},
	{  25,  21,  26,  21,  25,  20,  -1,  -1},
	{  21,  22,  20,  22,  21,  21,  -1,  -1},
	{  26,  22,  27,  22,  26,  21,  -1,  -1},
	{  21,  25,  20,  25,  21,  26,  -1,  -1},
	{  26,  25,  27,  25,  26,  26,  -1,  -1},
	{  25,  26,  26,  26,  25,  27,  -1,  -1},
	{  3,  28,  4,  28,  3,  27,  -1,  -1},
	{  44,  28,  43,  28,  44,  27,  -1,  -1},
	{  4,  31,  5,  31,  4,  30,  -1,  -1},
	{  43,  31,  42,  31,  43,  30,  -1,  -1},
	{  5,  33,  6,  33,  5,  32,  -1,  -1},
	{  42,  33,  41,  33,  42,  32,  -1,  -1},
	{  6,  35,  7,  35,  6,  34,  -1,  -1},
	{  41,  35,  40,  35,  41,  34,  -1,  -1},
	{  7,  36,  8,  36,  7,  35,  -1,  -1},
	{  40,  36,  39,  36,  40,  35,  -1,  -1},
	{  8,  37,  9,  37,  8,  36,  -1,  -1},
	{  39,  37,  38,  37,  39,  36,  -1,  -1},
	{  9,  38,  10,  38,  9,  37,  -1,  -1},
	{  38,  38,  37,  38,  38,  37,  -1,  -1},
	{  10,  39,  11,  39,  10,  38,  -1,  -1},
	{  37,  39,  36,  39,  37,  38,  -1,  -1},
	{  11,  40,  12,  40,  11,  39,  -1,  -1},
	{  36,  40,  35,  40,  36,  39,  -1,  -1},
	{  12,  41,  13,  41,  12,  40,  -1,  -1},
	{  14,  42,  15,  42,  14,  41,  -1,  -1},
	{  33,  42,  32,  42,  33,  41,  -1,  -1},
	{  16,  43,  17,  43,  16,  42,  -1,  -1},
	{  31,  43,  30,  43,  31,  42,  -1,  -1},
	{  19,  44,  20,  44,  19,  43,  -1,  -1},
	{  28,  44,  27,  44,  28,  43,  -1,  -1},
	{  22,  26,  21,  26,  22,  27,  -1,  -1},  // center obsc start
	{  24,  26,  25,  26,  24,  27,  -1,  -1},
	{  23,  26,  22,  26,  24,  26,  23,  27},
	{  26,  24,  27,  24,  26,  25,  -1,  -1},
	{  21,  24,  20,  24,  21,  25,  -1,  -1},
	{  26,  23,  27,  23,  26,  22,  26,  24},
	{  21,  23,  20,  23,  21,  22,  21,  24},
	{  24,  21,  25,  21,  24,  20,  -1,  -1},
	{  23,  21,  22,  21,  24,  21,  23,  20},
	{  25,  25,  26,  25,  25,  26,  -1,  -1},
	{  22,  25,  21,  25,  22,  26,  -1,  -1},
	{  25,  22,  26,  22,  25,  21,  -1,  -1},
	{  22,  22,  21,  22,  22,  21,  -1,  -1},
	{  24,  25,  25,  25,  24,  26,  -1,  -1},
	{  23,  25,  22,  25,  24,  25,  23,  26},
	{  25,  24,  26,  24,  25,  25,  -1,  -1},
	{  22,  24,  21,  24,  22,  25,  -1,  -1},
	{  25,  23,  26,  23,  25,  22,  25,  24},
	{  22,  23,  21,  23,  22,  22,  22,  24},
	{  24,  22,  25,  22,  24,  21,  -1,  -1},
	{  23,  22,  22,  22,  24,  22,  23,  21},
	{  24,  24,  25,  24,  24,  25,  -1,  -1},
	{  23,  24,  22,  24,  24,  24,  23,  25},
	{  24,  23,  25,  23,  24,  22,  24,  24},
	{  23,  23,  22,  23,  24,  23,  23,  22}
};
//XXXdwpBadActSlaves -- end -- for testing bad actuator slaving


//*******************************************************************************
//** General Global Variables
//*******************************************************************************

#if ( TIMER_TEST != 0 )
long reconFrameID = -1;
struct timespec timerTimes[NUM_TIME_SETS][eTimerMaxTimes];
#endif

int	 		tweeterSlaves[APERTURE_GRID_AREA][eTwtSlaveMaxFields];  // tweeter slaving variables

short		hrt_ReconRunCmd = eCmdNone;							// the 'long' command that is currently being executed, if any

short		hrt_AoTwtLoopStat;									// indicates the status of the tweeter ao loop (open, opening, closing, or closed)
short		hrt_AoWfrLoopStat;									// indicates the status of the woofer ao loop (open, opening, closing, or closed)
short		hrt_AoTTLoopStat;									// indicates the status of the TT ao loop (open, opening, or closed)

short		hrt_AoTwtLoopRequest;								// request and open close state change for tweeter loop ( -1: no change 0: open, 1: close )
short		hrt_AoWfrLoopRequest;								// request and open close state change for woofer loop  ( -1: no change 0: open, 1: close )

short		hrt_optimizer_OFC_Stat;								// indicates the status of the OFC Optimizer (open, opening, closing, or closed)

short		hrt_CMAvgN;											// number of centroids to average when determining a system matrix

short		hrt_MirrorShapeAvgN;								// number of DM voltages to average when determining flat and sharp mirror values

boolean 	currentlyCyclingActuator = false;					// indicates whether an actuator is currently cycling


// Woofer Tweeter Shapes / Offsets
//
short		hrt_updateTwtShape;									// On next frame update tweeter with shape and/or offset  - overwrites recon data
																// > 0x1: labFlat, 0x2: sysFlat, 0x4: dmShape, 0x100: offset
float		hrt_twtLabFlat[ MAX_TWEETER_ACTS ];					// current twt mirror 'shape' values (for the 'Lab' flat)
float		hrt_twtSysFlat[ MAX_TWEETER_ACTS ];					// current twt mirror 'shape' values (for the 'Sys' rlat)
float		hrt_twtDmShape[ MAX_TWEETER_ACTS ];					// current twt mirror 'shape' values (for the 'DM Shape' )

float		hrt_twtHostOffset[MAX_TWEETER_ACTS];				// current twt mirror 'shape' values (for the Host Specified Offset)

float		hrt_tt_x_offset;
float		hrt_tt_y_offset;

short		hrt_updateWfrShape;									// On next frame update woofer with shape and/or offset - overwrites recon data
																// > 0x1: labFlat, 0x2: sysFlat, 0x4: dmShape, 0x100: offset
														
float		hrt_wfrLabFlat[ MAX_WOOFER_ACTS ];					// current wfr mirror 'shape' values (for the 'Lab' flat)
float		hrt_wfrSysFlat[ MAX_WOOFER_ACTS ];					// current wfr mirror 'shape' values (for the 'Sys' rlat)
float		hrt_wfrDmShape[ MAX_WOOFER_ACTS ];					// current wfr mirror 'shape' values (for the 'DM Shape' )

float		hrt_wfrHostOffset[ MAX_WOOFER_ACTS ];				// current wfr mirror 'shape' values (for the Host Specified Offset)

float		hrt_twtCleanup[MAX_TWEETER_CLEAN_MODES][MAX_TWEETER_ACTS];	// tweeter mode cleanup matrix
float 		hrt_wfrCleanup[MAX_WOOFER_CLEAN_MODES][MAX_WOOFER_ACTS];	// woofer mode cleanup matrix

float		hrt_mode_cleanup_thresh;							// mode cleanup threshold
float		hrt_mode_cleanup_gain;								// mode cleanup gain

float		hrt_modal_loss_dm;									// modal loss - twt - from param file
float		hrt_modal_loss_tt;									// modal loss - wfr - from param file

// Modal gains
//
short		hrt_selectModalGains[ APERTURE_GRID_AREA ];			// selector for which type gain to apply when loops are closed
																//			( 1:OFC optimizer set, 4: Host File Set, 8: Initial File Set )

float		hrt_workingModalGains[ APERTURE_GRID_AREA ];		// Working gains applied when loop is closed ( source determined by hrt_selectModalGains )

float		hrt_hostModalGains[ APERTURE_GRID_AREA ];			// Modal gains set by 'Host File'
float		hrt_initialModalGains[ APERTURE_GRID_AREA ];		// Modal gains set by 'Initial File'
float		hrt_ofcOutputGains[ APERTURE_GRID_AREA ];			// Recon opt modal gains that are internally generated - not necessarily applied

int			hrt_twtCoeffUsed_Array[APERTURE_GRID_AREA ];		// Array indicating Tweeter modes that are used for FTR ( currently used in integrator)

// Influence Function data
//
int			makeInFuncModes[ APERTURE_GRID_SIZECOL ][ APERTURE_GRID_SIZEROW ][2];	// Storage location for 'makeInFuncMode' to drive the FRT while processing InFunc
int			makeInFuncState;														// Current makeInFunc State (enum) when running 'makeInFunc'
int			makeInFunc_UpdateModes = 0;												// Flag to FTR to update modes so that tweeter shape can be read ( 0: no update, 1: update frame)


//*******************************************************************************
//** Reconstructor-specific Global Variables
//*******************************************************************************
//FTR worker thread variables
const short	ReconAlgFTR_RowsPerThread = APERTURE_GRID_SIZEROW / FTR_NUM_THREADS;	//IMPORTANT: APERTURE_GRID_SIZEROW MUST be evenly divisible by FTR_NUM_THREADS
const short	ReconAlgFTR_ColsPerThread = APERTURE_GRID_SIZECOL / FTR_NUM_THREADS;	//IMPORTANT: APERTURE_GRID_SIZECOL MUST be evenly divisible by FTR_NUM_THREADS

volatile char	ReconAlgFTRThreadStat[NUM_TOTAL_ALG_THREADS];								// sub-thread status variables. these are chars to make sure they're atomic


//*******************************************************************************
//** General Local Variables
//*******************************************************************************
// pupil start and end indexes for rows and columns
static short pupRowStarts[APERTURE_GRID_SIZEROW], pupRowEnds[APERTURE_GRID_SIZEROW], pupColStarts[APERTURE_GRID_SIZECOL], pupColEnds[APERTURE_GRID_SIZECOL];

static const MakeInFuncStateDriver makeInFuncSettings[] = 
{ 
	{  StateInFunc_Start,		false	},
                           
	{  StateInFunc_SetMode,		true	},
	{  StateInFunc_Wait_FTR,	true	},
	{  StateInFunc_SetShape,    true	},
	{  StateInFunc_Wait_Shape,	true	},
	{  StateInFunc_Average,		true	},
                           
	{  StateInFunc_SetMode,		false	},
	{  StateInFunc_Wait_FTR,	false	},
	{  StateInFunc_SetShape,    false	},
	{  StateInFunc_Wait_Shape,	false	},
	{  StateInFunc_Average,		false	},

	{  StateInFunc_SaveData,	false	},
	
	{  StateInFunc_Idle,		false	}
};

//XXXdwpBadActSlaves -- start -- for testing bad actuator slaving
static int tweeterSlavesTmp[APERTURE_GRID_AREA][eTwtSlaveMaxFields];  // temporary tweeter slaving variables used to set up final list
//XXXdwpBadActSlaves -- end -- for testing bad actuator slaving

//*******************************************************************************
//** Reconstructor-specific Local Variables/define/constant
//*******************************************************************************

#if ( TIMER_TEST != 0 )
static int  timerNums[eTimerMaxTimes];
static int  timerMins[eTimerMaxTimes], timerMaxs[eTimerMaxTimes];
static long timerSums[eTimerMaxTimes];
#endif

// Pointer to 1D array of input centroid data (used by the worker threads only)
static float* ReconAlgFTR_xCentsCurrent;
static float* ReconAlgFTR_yCentsCurrent;

// Pointer to Current Recon buffer - used in threads for saving and getting data
static ReconBuffType* ReconAlgFTR_ReconBuf;
static ReconBuffType* LastReconAlgFTR_ReconBuf;

// Tip/Tilt counters/accumulators used for extending the pupils using the tip and tilt offsets (used by the worker threads only)
static float ReconAlgFTR_ttSumX[APERTURE_GRID_SIZEROW];		// single row accumulation of centroid data
static float ReconAlgFTR_ttSumY[APERTURE_GRID_SIZECOL];		// single column accumulation of centroid data
static short ReconAlgFTR_ttSumCntX[APERTURE_GRID_SIZEROW];	// number of centroid in corresponding row (used by the worker threads only, initialized globally once)
static short ReconAlgFTR_ttSumCntY[APERTURE_GRID_SIZECOL];	// number of centroid in corresponding column (used by the worker threads only, initialized globally once)
static float ReconAlgFTR_ttOffsetAccX[FTR_NUM_THREADS/2];	// tip group of rows accumulation of centroid data from worker threads
static float ReconAlgFTR_ttOffsetAccY[FTR_NUM_THREADS/2];	// tilt group of columns accumulation of centroid data from worker threads

static float ReconAlgFTR_tipTiltVector[MAX_NUM_SUBAPS];		// tip/tilt vector used to calculate T/T

//Tip/Tilt offset error
static float ReconAlgFTR_ttOffsetX, ReconAlgFTR_ttOffsetY;	//Tip and Tilt final offset

//*******************************************************************************
//** fft-related variables -- start

// fft configuration parameters
static const int fft_n_row = APERTURE_GRID_SIZEROW;
static const int fft_n_col = APERTURE_GRID_SIZECOL;

#if ( STANDALONE_FFT == 0 )
// fftw plans for the FFTs
static fftwf_plan		 ReconAlgFTR_fftPlanRows[FTR_NUM_THREADS];
static fftwf_plan		 ReconAlgFTR_fftPlanCols[FTR_NUM_THREADS];

// pointers to real and complex arrays used for FFTs (2-dimansional spatial data stored in 1-dimensional arrays)
static fft_data_type*	 ReconAlgFTR_ExtendXD;  // pupil-extended centroid data -- inputs to FFTs
static fft_complex_type* ReconAlgFTR_xC;		// complex data for row FFTs
static fft_complex_type* ReconAlgFTR_xCT;		// transposed complex data to do row-major order FFTs on columns

static fft_data_type*	 ReconAlgFTR_ExtendYD;  // pupil-extended centroid data -- inputs to FFTs
static fft_complex_type* ReconAlgFTR_yC;		// complex data for row FFTs
static fft_complex_type* ReconAlgFTR_yCT;		// transposed complex data to do row-major order FFTs on columns

#else
// arrays of real and complex arrays used for FFTs (2-dimansional spatial data stored in 1-dimensional arrays)
static fft_data_type	ReconAlgFTR_ExtendXD[APERTURE_GRID_AREA * sizeof(fft_data_type)];  // pupil-extended centroid data -- inputs to FFTs
static fft_complex_type ReconAlgFTR_xC[APERTURE_GRID_AREA * sizeof(fft_complex_type)];	   // complex data for row FFTs
static fft_complex_type ReconAlgFTR_xCT[APERTURE_GRID_AREA * sizeof(fft_complex_type)];	   // transposed complex data to do row-major order FFTs on columns

static fft_data_type	ReconAlgFTR_ExtendYD[APERTURE_GRID_AREA * sizeof(fft_data_type)];  // pupil-extended centroid data -- inputs to FFTs
static fft_complex_type ReconAlgFTR_yC[APERTURE_GRID_AREA * sizeof(fft_complex_type)];	   // complex data for row FFTs
static fft_complex_type ReconAlgFTR_yCT[APERTURE_GRID_AREA * sizeof(fft_complex_type)];	   // transposed complex data to do row-major order FFTs on columns
#endif

// pointers to split reconstruction data
static fft_data_type*		ReconAlgFTR_rowStartsF[FTR_NUM_THREADS];

static fft_complex_type*	ReconAlgFTR_rowStartsC[FTR_NUM_THREADS];
static fft_complex_type*	ReconAlgFTR_colStartsC[FTR_NUM_THREADS];
static fft_complex_type*	ReconAlgFTR_rowStartsCT[FTR_NUM_THREADS];
static fft_complex_type*	ReconAlgFTR_colStartsCT[FTR_NUM_THREADS];

// reconstruction Fourier filter values
fft_complex_type			ReconAlgFTR_filterX[APERTURE_GRID_AREA];
fft_complex_type			ReconAlgFTR_filterY[APERTURE_GRID_AREA];
fft_data_type				ReconAlgFTR_filterDenom[APERTURE_GRID_AREA];

static fft_complex_type		ReconAlgFTR_filterNumerator[APERTURE_GRID_AREA];

//** fft-related variables -- end
//*******************************************************************************

// worker thread variables
static rtl_pthread_t ReconAlgFTRThreads[ NUM_TOTAL_ALG_THREADS ];  //sub threads
static short		 ReconAlgFTRThreads_processNum[ NUM_TOTAL_ALG_THREADS ];

// M1 and M2 offload variables
// IMPORTANT: none of the following running average M's can be greater than 
//			  > MAX_OFFLOAD_FILTER_VALS -- the code does NOT check !!
// IMPORTANT: the following running average M's correspond to these
//			  > frame rates:     250, 500, 1000, 1500, 2000
static const int m1FilterMs[] = { 17,  35,   69,  104,  139 };
static const int m2FilterMs[] = { 14,  28,   56,   83,  111 };

static float crntRawM1Vals[eM1MaxVals];
static float rawM1FilterVals[eM1MaxVals][MAX_OFFLOAD_FILTER_VALS];  // raw running average filter values for M1
static int   m1FilterNdxs[eM1MaxVals];								// 'running' indexes into rawM1FilterVals[][]
static float crntFilteredM1Vals[eM1MaxVals];
static float crntRotatedM1Vals[eM1MaxVals];

static float crntRawM2Vals[eM2MaxVals];
static float rawM2FilterVals[eM2MaxVals][MAX_OFFLOAD_FILTER_VALS];  // raw running average filter values for M2
static int   m2FilterNdxs[eM2MaxVals];								// 'running' indexes into rawM2FilterVals[][]
static float crntFilteredM2Vals[eM2MaxVals];
static float crntRotatedM2Vals[eM2MaxVals];


//*******************************************************************************
// Local Function Prototypes
//*******************************************************************************
static void	 gpAoHrtRecon_Init( void );
static void	 gpAoHrtRecon_ProcCmd( short cmdNdx );
static void	 gpAoHrtRecon_ProcessTweeter( ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr );
static void	 gpAoHrtRecon_ProcessWoofer( ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr );
static void	 gpAoHrtRecon_ProcessStates( ReconBuffType* reconBufPtr );

static void	 gpAoHrtRecon_CalcStats( CentBuffType* centBufPtr, ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr );

static int	 gpAoHrtRecon_InFuncStart( void );
static int	 gpAoHrtRecon_ProcessMakeInFunc( CentBuffType* centBufPtr, ReconBuffType* reconBufPtr );
static int	 gpAoHrtRecon_StopMakeInFunc( void );

static void	 gpAoHrtRecon_InitPupStartEnd( void );
static void	 gpAoHrtRecon_InitTweeterSlaving( void );
static void  gpAoHrtRecon_InsertTwtSlave(int destRow, int destCol, int srcRow, int srcCol);

static short gpAoHrtRecon_FTRAlgoProcess( short thNdx, char threadCmnd );

#if ( RECON_DEBUG != 0 )
	static int			dumpCornerOfMatrix( void *pIn, int rowSize, int complexFlag, int dType );
#endif

#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 )  )
	static fitsfile*		openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType );
	static void				writeCompleteFitsFile( char *filename, long dimSize[], int numDim, int dataType, void *data);
	static void				writeFitsBlock( fitsfile* fptr, long size, int dataType, void* pData, long _fpixel[] );
	static void				closeFits( fitsfile* fptr );
	static void				flushFitsBuffer( fitsfile* fptr );
	static fitsfile*		last_fptr = NULL;
#endif

static int		gpAoHrtRecon_CloseOpenOFCLoop( int newState );
static int		gpAoHrtRecon_CloseOpenTweeterLoop( int newState );
static int		gpAoHrtRecon_CloseOpenWooferLoop( int newState );

static void		gpAoHrtRecon_OffloadM1M2( ReconBuffType*  reconBufPtr );

static void		gpAoHrtRecon_ReconFTR( float xyCents[], ReconBuffType*  reconBufPtr,  ReconBuffType* lastReconBufPtr );

static void*	gpAoHrtRecon_FTRAlgoThread( void *threadNdx );
static void		gpAoHrtRecon_FTRAlgoExtendPupil( float xyCents[] );
static void		gpAoHrtRecon_FTRAlgoInit( void );
static void		gpAoHrtRecon_FTRAlgoInitMemory( void );
static void		gpAoHrtRecon_FTRAlgoCleanMemory( void );


// Local function prototypes
 
__inline__ unsigned long long int rdtsc( void );



/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon( void* _arg )
 *
 *	@brief
 *  This unit is the 'Recon' thread mainline.
 *
 *	@b DESCRIPTION:
 *	The main thread for the 'Recon' calculations in the Hrt software.  This is 
 *	the main thread for the 'Recon' routines.  The unit initializes the recon thread, 
 *	waits for new centroid data, does the reconstruction, and passes the results 
 *	on to be parsed.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon( (void*)NULL );
 * 
 *	@param[in] _arg (void*): needed for pthread_create(), not currently used
 *
 *	@return 
 *	0 :  always 0 (NULL); should never be returned
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void *gpAoHrtRecon( void* _arg )
{
	diagFIFOMsgType			  diagFIFOMsg;
	CentBuffType*			  centBuffPtr;
	short					  currNdx = 0;
#if ( TIMER_TEST == 0 )
	short					  testNdx;
#else
 	int						  i, ii, tmpTimerNdx;
 	int						  timerNanosecs, timerNanosecsSave[eTimerMaxTimes];
	struct timespec			  tmpTime;
#endif
	static int				  overflowCntr = 0, overflowCheckCntr = 0;

	static ReconBuffType*	  lastReconBufPtr = NULL;
	ReconBuffType*			  reconBuffPtr;
	DataBufferElemStatusType* bufStatPtr;

	static int ttPrintCntr = 0;
	char	   ttText[DIAG_STRING_MAX_LENGTH];

#if (RTL_SYSTEM == 0)
	cpu_set_t mask;
	int		  cpuNum = HRT_CPU_RECON;
	CPU_ZERO( &mask );
	CPU_SET( cpuNum, &mask);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon) - CPU #",  cpuNum );
	if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon) - Executing sched_setaffinity() for CPU #",  cpuNum );
	sched_getaffinity( 0, sizeof( mask ), &mask);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else
	SysConSendStrngIntParm(eDebugLevelMax, "INFO Recon thread id: %d", rtl_pthread_self());
#endif

//	initialize variables used by the reconstruction thread
	gpAoHrtRecon_Init();

//	enter an infinite loop
//	wait for camera data to be available and report an error if one occurs in rtl_sem_wait()
	while (!QuitReconThread)
	{
#if (RTL_SYSTEM == 0)
 		if (rtl_sem_wait(&ReconSem) < 0)
		{
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (recon.c) waiting on ReconSem, errno = ", rtl_errno);
			continue;
		}
#else
		boolean executeFlag = false;
		struct rtl_timespec ts;
		if (ReconDataAvailFlag > 0)
		{
			rtl_a_decr(&ReconDataAvailFlag);
			executeFlag = true;
		}
		else
		{
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &ts);
			ts.tv_nsec += 10000;  // 10 microsecs
			rtl_timespec_normalize(&ts);
			if (rtl_sem_timedwait(&ReconSem, &ts) == 0)
				executeFlag = true;
		}
		if (!executeFlag)
			continue;
#endif

//		if a command is pending, call gpAoHrtRecon_ProcCmd() to process it
		if (hrtReconDataPtr->cmdStart != eCmdNone)
		{
			SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon) command received ", hrtReconDataPtr->cmdStart);
			gpAoHrtRecon_ProcCmd(hrtReconDataPtr->cmdStart);
			hrtReconDataPtr->cmdStart = eCmdNone;
			continue;
		}

//		increment the centroid data read index and make sure that it is no more than one less 
//		> than the write index (if it is, we're getting behind in the reconstruction processing)
		{
#if ( TIMER_TEST != 0 )
			reconFrameID++;
			tmpTimerNdx = reconFrameID % NUM_TIME_SETS;
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterReconSem]);
#endif

//			Set the recon 'read' buffer pointer to the next centroid buffer 
			currNdx = hrtReconDataPtr->reconBufNdx;
			centBuffPtr = &hrtCentDataPtr->centroidBuffs[ currNdx ];

			if ((hrtFileDataShmPtr->tlcOnFlag < 0) &&
				((currNdx % LegalFrameRates[hrt_camFrameRate]) == 0))
				SysConSendStrngIntParm( eDebugLevelMin, "(frame # rec) ", currNdx );

			#if ( RECON_DEBUG != 0 )
				HRT_PRINTF( "DEBUG (gpAoHrtRecon): Dump of X-Data Slopes BEFORE removing TT or Extending");
				dumpCornerOfMatrix( 	centBuffPtr->xyCents, APERTURE_GRID_SIZEROW, 0, TFLOAT );
				HRT_PRINTF( "DEBUG (gpAoHrtRecon): Dump of Y-Data Slopes BEFORE removing TT or Extending");
				dumpCornerOfMatrix( 	centBuffPtr->xyCents+APERTURE_GRID_AREA, APERTURE_GRID_SIZEROW, 0, TFLOAT );
			#endif

			// Set pointer to frame status 
			bufStatPtr = &(hrtDataFlowControlPtr->bufStat[ currNdx ]);

			// get the current cam write index and check for overflow
#if ( TIMER_TEST != 0 )
			if ((camFrameID - reconFrameID) > 2)
				overflowCntr++;
#else
			testNdx = hrtCamDataPtr->camWriteNdx;
			if ((testNdx - currNdx) < 0 )
				testNdx += NUM_HRT_DATA_BUFFS;  // take care of wrap, if that has occurred
			if ((testNdx - currNdx) > 2 )
				overflowCntr++;
#endif
			if ( ++overflowCheckCntr >= LegalFrameRates[hrt_camFrameRate] )
			{
				overflowCheckCntr = 0;
				if (overflowCntr > 0)
				{
					SysConSendStrngIntParm (eDebugLevelWarn, "WARN (gpAoHrtRecon) Overrun rb: >2 bufs from cam (chk 1/sec): ", overflowCntr);
					overflowCntr = 0;
				}
			}

			// Everything looks okay, or Warnings are sent
			//
			// Flag to world that recon has started
			bufStatPtr->hrtBufState = eBufReconProc;

			// write recon data into the same frame that we're reading cent data from
			reconBuffPtr = &hrtReconDataPtr->reconBuffs[ currNdx ];

			// perform FTR reconstruction
			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
				SysConSendStrngIntParm(eDebugLevelMax, "INFO (rec_ftr) - start ", currNdx );

			// data in reconBufPtr->reconTwtPhase/reconBufPtr->reconWfrData will be updated
			gpAoHrtRecon_ReconFTR( centBuffPtr->xyCents, reconBuffPtr, lastReconBufPtr );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterFTR]);
#endif

			//	perform phase parsing
			gpAoHrtParse_ParseFTR( reconBuffPtr );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterParse]);
#endif

			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
				SysConSendStrngIntParm(eDebugLevelMax, "INFO (rec_ftr) - parse done ", currNdx );

			// Decisions about Open/Closed are made in following routine - this routine also takes care of handling 'shape' updates
			gpAoHrtRecon_ProcessTweeter( reconBuffPtr, lastReconBufPtr  );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterProcTWT]);
#endif

			// Decisions about Open/Closed are made in following routine - this routine also takes care of handling 'shape' updates
			gpAoHrtRecon_ProcessWoofer( reconBuffPtr, lastReconBufPtr );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterProcWFR]);
#endif

			if( (eCmdSetShape == hrtReconDataPtr->cmdCrnt) && (hrt_updateTwtShape == 0) && (hrt_updateWfrShape == 0) )
			{
				AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
				SysConSendStrngNoParm(eDebugLevelMax, "INFO 'setShapes' complete"  );
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}

			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
				SysConSendStrngIntParm(eDebugLevelMax, "INFO (rec_ftr) - done ", currNdx );

//			if a cycleActuator command is active, call gpAoHrtVolt_ProcessReg() to move the actuator(s)
//			else if StoreShapecommand is executing, call gpAoHrtVolt_MirrorShapeProcess() to calc/save mirror shape
//			else if eCmdMakeInFunc is executing, call gpAoHrtRecon_ProcessMakeInFunc to calc/save mirror shape
			if( hrtFileDataShmPtr->cycleActActive )
				gpAoHrtVolt_ProcessReg( reconBuffPtr );
			else if( hrt_ReconRunCmd == eCmdStoreShape )
				gpAoHrtVolt_MirrorShapeProcess( reconBuffPtr );
			else if( hrt_ReconRunCmd == eCmdMakeInFunc )
				gpAoHrtRecon_ProcessMakeInFunc( centBuffPtr, reconBuffPtr );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterLongCmd]);
#endif
			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
				SysConSendStrngIntParm(eDebugLevelMax, "INFO (rec_long_cmd_processing) - done ", currNdx );

			if( ttPrintCntr++ >= LegalFrameRates[hrt_camFrameRate] )
			{
				ttPrintCntr = 0;
				if ((hrtFileDataShmPtr->tlcOnFlag < 0) && 	  // don't bother doing this, if we're not going to send it
					(eDebugLevelMin <= hrtSystemDebugLevel))
				{
//					unfortunately, RTLinux doesn't handle %f
					volatile int tmpInt1, tmpInt2;
					float tmpFloats[8];
					char tmpStrings[8][16];

					tmpFloats[0] = reconBuffPtr->reconTTData[eTTxErrorCentUnits];
					tmpFloats[1] = reconBuffPtr->reconTTData[eTTyErrorCentUnits]; 
					tmpFloats[2] = reconBuffPtr->reconTTData[eTTxErrorAngle];
					tmpFloats[3] = reconBuffPtr->reconTTData[eTTyErrorAngle];
					tmpFloats[4] = reconBuffPtr->reconTTData[eTTxSettingAngle];
					tmpFloats[5] = reconBuffPtr->reconTTData[eTTySettingAngle];
					tmpFloats[6] = reconBuffPtr->reconTTData[eTTxSettingVolts];
					tmpFloats[7] = reconBuffPtr->reconTTData[eTTySettingVolts];

					for (i = 0; i < 8; i++)
					{
						tmpInt1 = (int)tmpFloats[i];
						tmpInt2 = abs((int)((tmpFloats[i] - (float)tmpInt1) * 100000.0));
						tmpInt1 = abs(tmpInt1);
						if (tmpFloats[i] < 0.0)
							sprintf(tmpStrings[i], "-%d.%05d", tmpInt1, tmpInt2);
						else
							sprintf(tmpStrings[i], "%d.%05d", tmpInt1, tmpInt2);
					}
					if( hrt_AoTTLoopStat == eAOLoopClosed )
					{
						sprintf(ttText, "TT Clos: ErrCU: %s, %s ErrAngl: %s, %s SetAngl: %s, %s SetVlts: %s, %s", 
										tmpStrings[0], tmpStrings[1], tmpStrings[2], tmpStrings[3], 
										tmpStrings[4], tmpStrings[5], tmpStrings[6], tmpStrings[7]);
					}
					else
					{
						sprintf(ttText, "TT Open: ErrCU: %s, %s ErrAngl: %s, %s SetAngl: %s, %s SetVlts: %s, %s", 
										tmpStrings[0], tmpStrings[1], tmpStrings[2], tmpStrings[3], 
										tmpStrings[4], tmpStrings[5], tmpStrings[6], tmpStrings[7]);
					}
					SysConSendStrngNoParm(eDebugLevelMin, ttText);
				}
			}

//			now that the hard real-time loop stuff is done, calculate statistics
			gpAoHrtRecon_CalcStats( centBuffPtr, reconBuffPtr, lastReconBufPtr );

//			then project out Zernike modes to offload to M1 and M2
			gpAoHrtRecon_OffloadM1M2( reconBuffPtr );

//			and then load the current fourier modes into the periodograms list for the optimizer and execute the 
//			> optimizer state machine (the optimizer threads run in the background to compute the optimized gains)
		  	gpAoHrtReconOpt_ReconOptFTR( &(reconBuffPtr->ftrFFTModesData[0][0]), &(hrt_ofcOutputGains[0]) );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterOpt]);
#endif

			switch ( hrt_optimizer_OFC_Stat )
			{
				case eAOLoopOpening:
				case eAOLoopClosed:
					for( ii=0; ii<MAX_MODES; ii++ )
					{
						hrt_workingModalGains[ ii ] = hrt_ofcOutputGains[ ii ];		// Set to internally generated values
						reconBuffPtr->reconOptGains[ ii ] = hrt_workingModalGains[ ii ];  // this is a copy of the current working gains - for display
					}
					break;

				case eAOLoopClosing:
					for( ii=0; ii<MAX_MODES; ii++ )
					{
						hrt_ofcOutputGains[ ii ] = hrt_workingModalGains[ ii ];
						reconBuffPtr->reconOptGains[ ii ] = hrt_workingModalGains[ ii ];  // this is a copy of the current working gains - for display
					}
					hrt_optimizer_OFC_Stat = eAOLoopClosed;
					break;

				case eAOLoopOpen:
					for( ii=0; ii<MAX_MODES; ii++ )
					{
						switch( hrt_selectModalGains[ ii ] )
						{
							case 0x4: // Host values
								hrt_workingModalGains[ ii ] = hrt_hostModalGains[ ii ];			  // set to Host values
								reconBuffPtr->reconOptGains[ ii ] = hrt_workingModalGains[ ii ];  // this is a copy of the current working gains - for display
								break;

							default:
							case 0x8: // Initial values
								hrt_workingModalGains[ ii ] = hrt_initialModalGains[ ii ];		  // set to initial values
								reconBuffPtr->reconOptGains[ ii ] = hrt_workingModalGains[ ii ];  // this is a copy of the current working gains - for display
								break;
						}
					}
			}
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterGainCopy]);
#endif

			if( bufStatPtr->dispDoUpdate == true )
			{
				// Set true before SRT has started move - the SRT should clear when it is done
				hrtDataFlowControlPtr->bufStat[ currNdx ].inDisplayProcess = true;
				bufStatPtr->dispDoUpdate = false;

				diagFIFOMsg.diagID = eDisplayData;
				diagFIFOMsg.diagStat = eBufIdle;
				diagFIFOMsg.diagReadNdx = currNdx;

				if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
					SysConSendStrngIntParm(eDebugLevelErr, "ERR (recon.c) writing recon data to diag FIFO, errno = ", rtl_errno);
			}

			// Note: The TT is written immediately after the centroids are calculated, therefore it is NOT updated at this point.
			// Process state changes at the END of current frame so that everything setup for next frame ( NO changes in middle )
			gpAoHrtRecon_ProcessStates( reconBuffPtr );
#if ( TIMER_TEST != 0 )
			rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterProcStates]);
#endif

			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
				SysConSendStrngIntParm(eDebugLevelMax, "(rec_done) ", currNdx );

			hrtReconDataPtr->reconBufNdx  = (++currNdx) % NUM_HRT_DATA_BUFFS;
			lastReconBufPtr = reconBuffPtr;
			bufStatPtr->hrtBufState = eBufIdle;

#if ( TIMER_TEST == 0 )
			reconBuffPtr->rdtscFrameDoneStamp = rdtsc();
#else
			reconBuffPtr->timerTimesToPrint[0] = rtl_timespec_to_ns(&timerTimes[tmpTimerNdx][eTimerThisIntr]);
			reconBuffPtr->timerTimesToPrint[1] = rtl_timespec_to_ns(&timerTimes[tmpTimerNdx][eTimerAfterCentroid]);
			reconBuffPtr->timerTimesToPrint[2] = rtl_timespec_to_ns(&timerTimes[tmpTimerNdx][eTimerAfterLongCmd]);
			reconBuffPtr->timerTimesToPrint[3] = rtl_timespec_to_ns(&timerTimes[tmpTimerNdx][eTimerFTRAfterGainCopy]);

			tmpTime = timerTimes[tmpTimerNdx][eTimerThisIntr];
			rtl_timespec_sub(&tmpTime, &timerTimes[tmpTimerNdx][eTimerLastIntr]);
			timerNanosecs = tmpTime.tv_nsec;
			if (timerNanosecs > 0)
			{
				timerNums[eTimerLastIntr]++;
				timerSums[eTimerLastIntr] += timerNanosecs;
				if (timerNanosecs < timerMins[eTimerLastIntr])
					 timerMins[eTimerLastIntr] = timerNanosecs;
				if (timerNanosecs > timerMaxs[eTimerLastIntr])
					 timerMaxs[eTimerLastIntr] = timerNanosecs;
				timerNanosecsSave[eTimerLastIntr] = timerNanosecs;
			}

//			timerTimes[tmpTimerNdx][eTimerProbe1] = timerTimes[tmpTimerNdx][eTimerAfterLongCmd];  // use probe1 for total hard-loop time
			for (i = eTimerThisIntr + 1; i < eTimerAfterCentPupTrack; i++)
			{
				tmpTime = timerTimes[tmpTimerNdx][i];
				rtl_timespec_sub(&tmpTime, &timerTimes[tmpTimerNdx][eTimerThisIntr]);  // to calc from beginning of interrupt
				timerNanosecs = tmpTime.tv_nsec;
				if (timerNanosecs > 0)
				{
					timerNums[i]++;
					timerSums[i] += timerNanosecs;
					if (timerNanosecs < timerMins[i])
						 timerMins[i] = timerNanosecs;
					if (timerNanosecs > timerMaxs[i])
						 timerMaxs[i] = timerNanosecs;
					timerNanosecsSave[i] = timerNanosecs;
				}
			}
			for (i = eTimerAfterCentPupTrack; i < eTimerMaxTimes; i++)  // can't calc these from previous time because 
			{															// > they don't happen all the time
				tmpTime = timerTimes[tmpTimerNdx][i];
				rtl_timespec_sub(&tmpTime, &timerTimes[tmpTimerNdx][eTimerThisIntr]);
				timerNanosecs = tmpTime.tv_nsec;
				if (timerNanosecs > 0)
				{
					timerNums[i]++;
					timerSums[i] += timerNanosecs;
					if (timerNanosecs < timerMins[i])
						 timerMins[i] = timerNanosecs;
					if (timerNanosecs > timerMaxs[i])
						 timerMaxs[i] = timerNanosecs;
					timerNanosecsSave[i] = timerNanosecs;
				}
			}

			if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
			{
				if (hrtFileDataShmPtr->tlcOnFlag < 0)
				{
					int tmpInt, tmpMin, tmpMax;
					char timeText[DIAG_STRING_MAX_LENGTH * 2], tmpText[DIAG_STRING_MAX_LENGTH];

					tmpInt = timerNums[eTimerLastIntr] * 1000;  // the times 1000 it to convert to microsecs
					tmpMin = timerMins[eTimerLastIntr] / 1000;
					tmpMax = timerMaxs[eTimerLastIntr] / 1000;
					sprintf(timeText, "TIMES last: %ld[%d]%d,%d,%d this: 0[%d]0,0,0 ", 
									  timerSums[eTimerLastIntr] / tmpInt, eTimerLastIntr, tmpMin, tmpMax - tmpMin, tmpMax, 
									  									  eTimerThisIntr);

					tmpInt = timerNums[eTimerAfterTT] * 1000;  // the times 1000 it to convert to microsecs
					tmpMin = timerMins[eTimerAfterTT] / 1000;
					tmpMax = timerMaxs[eTimerAfterTT] / 1000;
					sprintf(tmpText, "TT: %ld[%d]%d,%d,%d ", 
									 timerSums[eTimerAfterTT] / tmpInt, eTimerAfterTT, tmpMin, tmpMax - tmpMin, tmpMax);
					strcat(timeText, tmpText);

					tmpInt = timerNums[eTimerAfterLongCmd] * 1000;  // the times 1000 it to convert to microsecs
					tmpMin = timerMins[eTimerAfterLongCmd] / 1000;
					tmpMax = timerMaxs[eTimerAfterLongCmd] / 1000;
					sprintf(tmpText, "DMs: %ld[%d]%d,%d,%d ", 
									 timerSums[eTimerAfterLongCmd] / tmpInt, eTimerAfterLongCmd, tmpMin, tmpMax - tmpMin, tmpMax);
					strcat(timeText, tmpText);

					tmpInt = timerNums[eTimerAfterProcStates] * 1000;  // the times 1000 it to convert to microsecs
					tmpMin = timerMins[eTimerAfterProcStates] / 1000;
					tmpMax = timerMaxs[eTimerAfterProcStates] / 1000;
					sprintf(tmpText, "all: %ld[%d]%d,%d,%d ", 
									 timerSums[eTimerAfterProcStates] / tmpInt, eTimerAfterProcStates, tmpMin, tmpMax - tmpMin, tmpMax);
					strcat(timeText, tmpText);

					SysConSendStrngNoParm(eDebugLevelMin, timeText);
				}

				for (i = 0; i < eTimerMaxTimes; i++)
				{
					timerNums[i] = 0;
					timerMins[i] = INT_MAX;
					timerMaxs[i] = 0;
					timerSums[i] = 0;
				}
			}
#endif
		}
		hrtFileDataShmPtr->watchDogBits |= WatchDog_ReconBit;
	}	// End While

	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon) quitting recon loop");
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_Init( void )
 *
 *	@brief
 *  Initialize variables used by the Recon thread.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the reconstruction thread.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_Init(  );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_Init( void )
{
	int					i, retVal;
	struct rtl_timespec	next;

//	initialize variables
	hrt_AoTwtLoopStat = eAOLoopOpen;				 // Initialize all the loops to open (hrt side)
	hrt_AoWfrLoopStat = eAOLoopOpen;
	hrt_AoTTLoopStat = eAOLoopOpen;
	hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopOpen;	 // and tell the SRT side the loop status
	hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopOpen;
	hrtFileDataShmPtr->aoTTLoopStat = eAOLoopOpen;

	hrt_AoTwtLoopRequest = -1; // Request is 'noChange'
	hrt_AoWfrLoopRequest = -1; // Request is 'noChange'

	hrt_optimizer_OFC_Stat = eAOLoopOpen;

	hrtReconDataPtr->cmdStart = eCmdNone;
	hrtReconDataPtr->cmdCrnt = eCmdNone;

	hrtReconDataPtr->reconBufNdx = 0;
	hrtReconDataPtr->psdWriteNdx = 0;

	hrt_updateTwtShape = 0;
	hrt_updateWfrShape = 0;

	makeInFuncState = 0;					   // Current makeInFunc State (enum) when running 'makeInFunc'
	makeInFunc_UpdateModes = 0;				   // Flag to FTR to update modes so that tweeter shape can be read ( 0: no update, 1: update frame)
	hrtFileDataShmPtr->makeInFunc_numAvg = 5;  // Number of frames to average
	
//	call gpAoHrtVolt_Init() to initialize Volt variables
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_Init): do gpAoHrtVolt_Init()"); 
	if( gpAoHrtVolt_Init() )
	{
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_Init) gpAoHrtVolt_Init() failed - Init failed - returning");
		ReconInitDone = false;
		return;
	}

#if ( TIMER_TEST != 0 )
	for (i = 0; i < eTimerMaxTimes; i++)
	{
		timerNums[i] = 0;
		timerMins[i] = INT_MAX;
		timerMaxs[i] = 0;
		timerSums[i] = 0;
	}
#endif

//	spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
	while (!OkayToFinishInit)
	{
		next.tv_sec = 0;
		next.tv_nsec = 100000000;
		if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_Init) from nanosleep(), retVal = ", retVal);
	}

//	generate the pupil start and end index from the pupil definition (requires file data from the SRT)
//	Note that if the pupil definition is reloaded, this local array is not updated at this time
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_Init) start gpAoHrtRecon_InitPupStartEnd()" ); 
	gpAoHrtRecon_InitPupStartEnd();

//	find the tweeter slave actuators
	gpAoHrtRecon_InitTweeterSlaving();

//	call the reconstruction algorithm initialize functions
	//	call gpAoHrtRecon_FTRAlgoInit() to initialize FTR variables and threads
	gpAoHrtRecon_FTRAlgoInit();
	//	call gpAoHrtReconOpt_FTRAlgoInit() to initialize FTR optimization variables and threads
	gpAoHrtReconOpt_FTRAlgoInit();
	//	call gpAoHrtParse_FTRAlgoInit() to initialize FTR parse variables and threads
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_Init): calling gpAoHrtParse_FTRAlgoInit()"); 
	gpAoHrtParse_FTRAlgoInit();

//	call gpAoHrtTipTilt_Init() to initialize tip/tilt variables
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_Init): do gpAoHrtTipTilt_Init()"); 
	gpAoHrtTipTilt_Init();

	if( hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff < 1  )
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_Init) Twt Coeff num < 1. hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff =  ", hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff);
	if( hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff < 1 )
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_Init) Wfr Coeff num < 1. hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff =  ", hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff);
	if( hrtFileDataShmPtr->numTT_Phase2VoltsCoeff < 1 )
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_Init) TT Coeff num < 1. hrtFileDataShmPtr->numTT_Phase2VoltsCoeff =  ", hrtFileDataShmPtr->numTT_Phase2VoltsCoeff);

	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_Init): do gpAoHrtVolt_StopReg()"); 
	gpAoHrtVolt_StopReg( NULL );
	ReconInitDone = true;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_InitMemory( )
 *
 *	@brief
 *	Allocate Recon memory.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to start any worker threads used by the recon process.
 *	@par
 *
 *	
 *	@b Usage: gpAoHrtRecon_InitMemory( );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
void gpAoHrtRecon_InitMemory( void )
{
	// call gpAoHrtRecon_FTRAlgoInitMemory() to allocate FTR memory
	gpAoHrtRecon_FTRAlgoInitMemory();
	// call gpAoHrtReconOpt_FTRAlgoInitMemory() to allocate FTR Optimization memory
	gpAoHrtReconOpt_FTRAlgoInitMemory();
	// call gpAoHrtParse_FTRAlgoInitMemory() to allocate FTR parse memory
	gpAoHrtParse_FTRAlgoInitMemory();

	return;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CleanMemory( )
 *
 *	@brief
 *	Clean Recon allocated memory.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to de-allocate memory used by this module.
 *	@par
 *
 *	
 *	@b Usage: gpAoHrtRecon_CleanMemory( );
 * 
 *	\@param[in] None
 *
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
void gpAoHrtRecon_CleanMemory( void )
{
	// call gpAoHrtRecon_FTRAlgoCleanMemory() to clean FTR memory
	gpAoHrtRecon_FTRAlgoCleanMemory();
	// call gpAoHrtReconOpt_FTRAlgoCleanMemory() to clean FTR optimization memory
	gpAoHrtReconOpt_FTRAlgoCleanMemory();
	// call gpAoHrtParse_FTRAlgoCleanMemory() to clean FTR parse memory
	gpAoHrtParse_FTRAlgoCleanMemory();

	return;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_ProcCmd( short cmdNdx )
 *
 *	@brief
 *	Process Recon commands from the host.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to process Recon commands from the host (via the SRT side).
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_ProcCmd( (short) cmdNdx );
 * 
 *	@param[in]  cmdNdx (short):  index into CmdTable[] for the command to be processed
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_ProcCmd( short cmdNdx )
{
	int				retValue = 0;
	ReconBuffType*	reconBuffPtr = &hrtReconDataPtr->reconBuffs[ hrtReconDataPtr->reconBufNdx ];
	
	cmdNdx = gpAoHrtTipTilt_ProcCmd( cmdNdx );
	if( cmdNdx < 0 )
		return;			// Command handled by TipTilt

	//	switch on command ID corresponding to command index
	//
	//
	switch (CmdTable[cmdNdx].idNum)
	{
		case eCmdAbort:		   // abort long commands
			switch (hrt_ReconRunCmd)
			{
				case eCmdReserved1:
					break;

				case eCmdStoreShape:
					gpAoHrtVolt_MirrorShapeAbort();
					break;

				case eCmdMakeInFunc:
					gpAoHrtRecon_StopMakeInFunc();
					break;

				default:
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) unexpected command abort received by Recon");
					break;
			}
			break;

		case eCmdStorMdlGains:
			{
				int ii;

				for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
					hrtFileDataShmPtr->initialModalGains[ii] = hrt_initialModalGains[ii];
				gpAoHrtFileData_Put(eFileTypeInitialModalGains);
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}
			break;

		case eCmdClsOpn:	// closes or opens one of the 3 AO loops
			gpAoHrtRecon_CloseOpenTweeterLoop( hrtReconDataPtr->cmdIntParam[0] );
			gpAoHrtRecon_CloseOpenWooferLoop( hrtReconDataPtr->cmdIntParam[1] );
			gpAoHrtTipTilt_CloseOpenTTLoop( hrtReconDataPtr->cmdIntParam[2] );

			// Set Shm status so that it can be sent through to GMB status
			SysConSendStrngNoParm(eDebugLevelMin, "INFO 'closeOpenLp' complete");
			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );

			hrtReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdClsOpnOFC:	// closes or opens the OFC loop
			retValue = gpAoHrtRecon_CloseOpenOFCLoop( hrtReconDataPtr->cmdIntParam[0] );  
			
			switch( retValue)
			{
				case 0:
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, retValue, GPI_STATE_IDLE, "" );
					break;
				case 1:
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, retValue, GPI_STATE_ERROR, "AO Loops not both closed" );
					break;
				case 2:
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, retValue, GPI_STATE_ERROR, "Bad State requested - must be 0 or 1 " );
					break;
			}

			SysConSendStrngNoParm(eDebugLevelMin, "INFO 'closeOpenLp_OFC' complete");
			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );

			hrtReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdOffload:
			{
				int cmdErr = 0;

				// Mask for the bit you want
				hrtFileDataShmPtr->offloadM1M2 &= 0x3;

				// M1 Select
				switch( hrtReconDataPtr->cmdIntParam[0] )
				{
					case 0:
						hrtFileDataShmPtr->offloadM1M2 &= 0x2;  // turn M1 off, leave M2 alone
						break;

					case 1:
						hrtFileDataShmPtr->offloadM1M2 |= 0x1;  // turn M1 on, leave M2 alone
						break;

					case -1:
						break;

					default:
						cmdErr = 1;
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) 'offloadM1M2' arg 1 incorrect - value = ",
											   hrtReconDataPtr->cmdIntParam[0] );
						break;

				}

				// M2 Select
				switch( hrtReconDataPtr->cmdIntParam[1] )
				{
					case 0:
						hrtFileDataShmPtr->offloadM1M2 &= 0x1;	// turn M2 off, leave M1 alone
						break;

					case 1:
						hrtFileDataShmPtr->offloadM1M2 |= 0x2;	// turn M2 on, leave M1 alone
						break;

					case -1:
						break;

					default:
						cmdErr = 1;
						SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) 'offloadM1M2' arg 2 incorrect - value = ",
											   hrtReconDataPtr->cmdIntParam[1] );
						break;
				}

				hrtFileDataShmPtr->offloadZernRotationAngle = hrtReconDataPtr->cmdIntParam[2];

				// Tell the SRT that params have changed
				gpAoHrtFileData_Put( eFileTypeParms );

				// Set Shm status so that it can be sent through to GMB status
				if( cmdErr != 0 )
				{
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Invalid selection values" );
					SysConSendStrngNoParm(eDebugLevelErr, "ERR 'set offloadM1M2' invalid selection value");
				}
				else
				{
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
					SysConSendStrngNoParm(eDebugLevelMin, "INFO 'set offloadM1M2' complete");
				}
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}
			break;

		case eCmdSelectGainSrc:
			{
				// SelectValue
				//------------
				//	4: Use Host supplied value set of gains
				//	8: Use standardInitialValue set of gains
				//
				int ii;
				int	selectValue = hrtReconDataPtr->cmdIntParam[0];

				switch( selectValue )
				{
					case 0x4:	// host
					case 0x8:	// initial
						gpAoHrtReconOpt_FTRAlgoInit();			// restart ReconOpt nicely
						for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
							hrt_selectModalGains[ ii ] = selectValue;

						// don't Tell the SRT that params have changed -- because this isn't in parms
						// gpAoHrtFileData_Put( eFileTypeParms );

						// Set Shm status so that it can be sent through to GMB status
						AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
						SysConSendStrngNoParm(eDebugLevelMin, "INFO 'opt select' complete");
						break;

					default:
						// Set Shm status so that it can be sent through to GMB status
						AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Unexpected selection value" );
						SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) unexpected 'selection' value for 'setOptimizeChoices'" );
						break;
				}
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}
			break;

		case eCmdSelectInFunc:
			switch( hrtReconDataPtr->cmdIntParam[0] )
			{
				case 0:
				case 2:
					hrtFileDataShmPtr->selectorForInFuncMultiplier = hrtReconDataPtr->cmdIntParam[0];

					// don't Tell the SRT that params have changed -- because selectorForInFuncMultiplier isn't in parms
					// gpAoHrtFileData_Put( eFileTypeParms );

					// Set Shm status so that it can be sent through to GMB status
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
					SysConSendStrngNoParm(eDebugLevelMin, "INFO 'select InFunc Source' complete");
					break;

				default:
					AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Invalid selection value ( must be 0 or 2)" );
					SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) unexpected 'selection' value for 'selectInFunGains'" );
					break;
			}

			hrtReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdReserved1:
			break;

		case eCmdCycleAct:			// starts moving actuator x up and down at 1 Hz
			if (gpAoHrtVolt_StartReg((short)hrtReconDataPtr->cmdIntParam[0], reconBuffPtr) != 0)
			{
				AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Error starting actuator cycling -- in wrong mode" );
			}
			else
			{
				currentlyCyclingActuator = true;
				AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
				SysConSendStrngIntParm(eDebugLevelMax, "INFO actuator started: ", hrtReconDataPtr->cmdIntParam[0] );
			}
			hrtReconDataPtr->cmdCrnt = eCmdNone;
			break;

		case eCmdMakeInFunc:					// Set mirror to sin/cos for one mode of FTR
			{
				hrtFileDataShmPtr->makeInFunc_xMode = hrtReconDataPtr->cmdIntParam[0];
				hrtFileDataShmPtr->makeInFunc_yMode = hrtReconDataPtr->cmdIntParam[1];
				hrtFileDataShmPtr->makeInFunc_amplitude = hrtReconDataPtr->cmdFloatParam[2];
				gpAoHrtRecon_InFuncStart( );
				SysConSendStrngNoParm(eDebugLevelMax, "INFO 'makeInFunc' started");
			}
			break;

		case eCmdCycleEnd:			// stops moving all actuators that were started with reg
			if( hrtFileDataShmPtr->cycleActActive )
				gpAoHrtVolt_StopReg( reconBuffPtr );
			else
			{
				AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
				hrtReconDataPtr->cmdCrnt = eCmdNone;
			}
			currentlyCyclingActuator = false;
			break;

		case eCmdStoreShape:			// averages and saves actuator voltages as mirror values (long command)
			gpAoHrtVolt_MirrorShapeStart( hrtReconDataPtr->cmdIntParam[0] );
			break;
			
		case eCmdSetShape:			// loads the latest stored Lab flat mirror values
			hrt_updateTwtShape = hrtReconDataPtr->cmdIntParam[0] & 0x07;
			hrt_updateWfrShape = ( (hrtReconDataPtr->cmdIntParam[0]) >> 4 ) & 0x07;

			if( hrtReconDataPtr->cmdIntParam[0] & 0x0100 )
				hrt_updateTwtShape |= 0x100;

			if( hrtReconDataPtr->cmdIntParam[0] & 0x0200 )
				hrt_updateWfrShape |= 0x100;
			
			SysConSendStrngIntParm(eDebugLevelMax, "INFO setting mirror voltages flagged for update: ", hrtReconDataPtr->cmdIntParam[0]);
//XXXdwp110531			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
			break;

		default:
			if( cmdNdx >= 0 )
				SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcCmd) unexpected command (recon), cmdNdx = ", cmdNdx);
			break;
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_ProcessTweeter( ReconBuffType* reconBufPtr,  ReconBuffType* lastReconBufPtr )
 *
 *	@brief
 *	Process Tweeter data and  perform self-optimizing reconstruction if appropriate
 *	
 *	@b DESCRIPTION:
 *	After Reconstructor is finished, this module handles the application of data to the Tweeter.
 *	It will handle the data based on the 'open', 'closed', or 'hold' state 
 *	This will use the Optimizing Fourier Controller (OFC) for each spatial mode.
 *
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_ProcessTweeter( (ReconBuffType*) reconBufPtr,  (ReconBuffType*) lastReconBufPtr );
 * 
 *	@param[in] reconBufPtr		(ReconBuffType*):	Current recon data ( twt, wfr and tt )
 *	@param[in] lastReconBufPtr	(ReconBuffType*):	Previous recon data ( twt, wfr and tt )
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void gpAoHrtRecon_ProcessTweeter( ReconBuffType* reconBufPtr,  ReconBuffType* lastReconBufPtr )
{
	int ii;
	int	xx, yyIn, yyOut, tmpNdx;
	int respFromConv;

	static int oldWfrVecSize = -1;
	static int numActiveCleanupModes = 0;
	static int cleanupModeNdx = 0;
	float	   cleanupCoeff;

	reconBufPtr->twt_shape_update = hrt_updateTwtShape;
	reconBufPtr->reconClipCleanupData[eClipCleanupTwtClipCount] = 0;

	// update tweeter, based on current tweeter state
	switch( hrt_AoTwtLoopStat )
	{
		case eAOLoopOpen:
			if( hrt_updateTwtShape != 0 )
			{
				
				if( hrt_updateTwtShape & 0x1 )
				{
					// set Lab Flat shape
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
						reconBufPtr->reconTwtPhase[ii] = hrt_twtLabFlat[ii];

				}
				else if ( hrt_updateTwtShape & 0x2 )
				{
					// set SysFlat Shape 
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
						reconBufPtr->reconTwtPhase[ii] = hrt_twtSysFlat[ii];
				}
				else if ( hrt_updateTwtShape & 0x4 )
				{
					// set hostDefinedShape
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
						reconBufPtr->reconTwtPhase[ii] = hrt_twtDmShape[ii];
				}
				if( hrt_updateTwtShape & 0x100 )
				{
					// add in offset if necessary
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
						reconBufPtr->reconTwtPhase[ii] += hrt_twtHostOffset[ii];
				}
				hrt_updateTwtShape = 0;

				// Write out the updated phase to the tweeter
				if( gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, reconBufPtr->reconTwtVolts, reconBufPtr->reconTwtUnclippedPhase  ) <= 0) 
					gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );
			}
			else
			{
				if( lastReconBufPtr != NULL )
				{
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
					{
						reconBufPtr->reconTwtPhase[ii] = lastReconBufPtr->reconTwtPhase[ii];
						reconBufPtr->reconTwtVolts[ii] = lastReconBufPtr->reconTwtVolts[ii];
					}
				}
				else
				{
					for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
					{
						reconBufPtr->reconTwtPhase[ii] = 0.0f;
						reconBufPtr->reconTwtVolts[ii] = 0.0f;
					}
				}
			}
			break;

		case eAOLoopOpening:
			// If loop is opening and 'lastReconBufPtr' is set then copy forward the previous tweeter phase and voltage values
			if( lastReconBufPtr != NULL )
			{
				for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
				{
					reconBufPtr->reconTwtPhase[ii] = lastReconBufPtr->reconTwtPhase[ii];
					reconBufPtr->reconTwtVolts[ii] = lastReconBufPtr->reconTwtVolts[ii];
				}
			}
			else
			{
				for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
				{
					reconBufPtr->reconTwtPhase[ii] = 0.0f;
					reconBufPtr->reconTwtVolts[ii] = 0.0f;
				}
			}
			hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopOpen;
			hrt_AoTwtLoopStat = eAOLoopOpen;
			break;

		case eAOLoopClosing:
			// If loop is closing, zero the integrators
			// IMPORTANT -- this implies that the tweeter should be commanded to its bias before closing the loop
			for( ii = 0; ii < MAX_TWEETER_ACTS; ii++)
			{
				reconBufPtr->reconTwtPhase[ii] = 0.0;
				reconBufPtr->reconTwtVolts[ii] = 0.0;
			}
			hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopAlmostClosed;
			hrt_AoTwtLoopStat = eAOLoopAlmostClosed;
			break;

		case eAOLoopAlmostClosed:
		case eAOLoopClosed:
//			flip data in y for tweeter, integrate, and gather cleanup data
			if (oldWfrVecSize != ParseAlgFTR_wfrVecSize)  // yes, this is supposed to be Wfr and wfr
			{
				oldWfrVecSize = ParseAlgFTR_wfrVecSize;
				cleanupModeNdx = 0;
				numActiveCleanupModes = 1 + ParseAlgFTR_wfrVecSize * 2;  // this is the number of controlled woofer modes plus 1 for waffle
			}
			cleanupCoeff = 0.0;
			if( lastReconBufPtr == NULL )
			{
				for( yyIn=0, yyOut=NUM_SUBAPS_ROWS_PER_FRAME-1; yyIn<NUM_SUBAPS_ROWS_PER_FRAME; yyIn++, yyOut--)
				{
					for( xx=0; xx<NUM_SUBAPS_PER_ROW; xx++ )
					{
						tmpNdx = xx + yyOut * NUM_SUBAPS_PER_ROW;
						reconBufPtr->reconTwtPhase[ tmpNdx ] = 
							reconBufPtr->reconRawTwtPhase[ xx + yyIn * NUM_SUBAPS_PER_ROW];
						cleanupCoeff += reconBufPtr->reconTwtPhase[tmpNdx] * hrt_twtCleanup[cleanupModeNdx][tmpNdx];
					}
				}
			}
			else
			{
				for( yyIn=0, yyOut=NUM_SUBAPS_ROWS_PER_FRAME-1; yyIn<NUM_SUBAPS_ROWS_PER_FRAME; yyIn++, yyOut--)
				{
					for( xx=0; xx<NUM_SUBAPS_PER_ROW; xx++ )
					{
						tmpNdx = xx + yyOut * NUM_SUBAPS_PER_ROW;
						reconBufPtr->reconTwtPhase[ tmpNdx ] = 
							lastReconBufPtr->reconTwtPhase[ tmpNdx ] * hrt_modal_loss_dm +
								reconBufPtr->reconRawTwtPhase[ xx + yyIn * NUM_SUBAPS_PER_ROW];
						cleanupCoeff += reconBufPtr->reconTwtPhase[tmpNdx] * hrt_twtCleanup[cleanupModeNdx][tmpNdx];
					}
				}
			}

//			clean up one tweeter mode per frame
			if (fabs(cleanupCoeff) < hrt_mode_cleanup_thresh)
				cleanupCoeff = 0.0;
			cleanupCoeff *= hrt_mode_cleanup_gain;

			for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
				reconBufPtr->reconTwtPhase[ii] -= cleanupCoeff * hrt_twtCleanup[cleanupModeNdx][ii];

			reconBufPtr->reconClipCleanupData[eClipCleanupTwtPiston] = 0.0;  // note -- not used at present
			reconBufPtr->reconClipCleanupData[eClipCleanupTwtCleanupMode] = cleanupModeNdx;
			reconBufPtr->reconClipCleanupData[eClipCleanupTwtCleanupValue] = cleanupCoeff;

			if (++cleanupModeNdx >= numActiveCleanupModes)
				cleanupModeNdx = 0;

//			convert tweeter phase to volts and output to mirror
			respFromConv = gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, 
													reconBufPtr->reconTwtVolts, 
													reconBufPtr->reconTwtUnclippedPhase );
			if( respFromConv > 0)
				SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcessTweeter) 'gpAoHrtVolt_ConvTweeter' failed" );
			else
			{
				gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );
				reconBufPtr->reconClipCleanupData[eClipCleanupTwtClipCount] = -respFromConv;  // respFromConv will be negative if clipping occurred
			}

			if (hrt_AoTwtLoopStat == eAOLoopAlmostClosed)
			{
				hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopClosed;  // this is to match gpAoHrtRecon_ProcessWoofer()
				hrt_AoTwtLoopStat = eAOLoopClosed;
			}
			break;
	}
	return;	
}
	

/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_ProcessWoofer( ReconBuffType* reconBufPtr,  ReconBuffType* lastReconBufPtr )
 *
 *	@brief
 *	Process Woofer data to perform self-optimizing reconstruction.  
 *	
 *	@b DESCRIPTION:
 *	After Reconstructor is finished, this module handles the application of data to the Woofer.
 *	This will use the Optimizing Fourier Controller (OFC) for each spatial mode.
 *
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_ProcessWoofer(  (ReconBuffType*) reconBufPtr,  (ReconBuffType*) lastReconBufPtr );
 * 
 *	@param[in] reconBufPtr		(ReconBuffType*):	Current recon data ( twt, wfr and tt )
 *	@param[in] lastReconBufPtr	(ReconBuffType*):	Previous recon data ( twt, wfr and tt )
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void gpAoHrtRecon_ProcessWoofer( ReconBuffType* reconBufPtr,  ReconBuffType* lastReconBufPtr )
{
	int ii;
	int	yy, xxIn, xxOut, tmpNdx;
	int respFromConv;

	static int oldWfrVecSize = -1;
	static int numActiveCleanupModes = 0;
	static int cleanupModeNdx = 0;
	float	   cleanupCoeff;

	float tmpReconWfrPhase[MAX_WOOFER_ACTS];  //XXXdwp110516 -- for testing only

	reconBufPtr->wfr_shape_update = hrt_updateWfrShape;
	reconBufPtr->reconClipCleanupData[eClipCleanupWfrClipCount] = 0;

	// update woofer based on the current woofer state
	switch( hrt_AoWfrLoopStat )
	{
		case eAOLoopOpen:
			if( hrt_updateWfrShape != 0 )
			{
				if( hrt_updateWfrShape & 0x1 )
				{
					// set Lab Flat
					for( ii = 0; ii < MAX_WOOFER_ACTS; ii++)
						reconBufPtr->reconWfrPhase[ii] = hrt_wfrLabFlat[ii];

				}
				else if ( hrt_updateWfrShape & 0x2 )
				{
					// set Sys Flat
					for( ii = 0; ii < MAX_WOOFER_ACTS; ii++)
						reconBufPtr->reconWfrPhase[ii] = hrt_wfrSysFlat[ii];
				}
				else if ( hrt_updateWfrShape & 0x4 )
				{
					// set HostDefined Shape
					for( ii = 0; ii < MAX_WOOFER_ACTS; ii++)
						reconBufPtr->reconWfrPhase[ii] = hrt_wfrDmShape[ii];
				}
				if( hrt_updateWfrShape & 0x100 )
				{
					// add in offset if necessary
					for( ii = 0; ii < MAX_WOOFER_ACTS; ii++)
						reconBufPtr->reconWfrPhase[ii] += hrt_wfrHostOffset[ii];
				}	
				hrt_updateWfrShape = 0;

				// Write out the updated phase
				if( gpAoHrtVolt_ConvWoofer( reconBufPtr->reconWfrPhase, reconBufPtr->reconWfrVolts, reconBufPtr->reconWfrUnclippedPhase ) <= 0) 
					gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconWfrVolts, false );
			}
			else
			{
//				when loops are open, pass the unintegrated raw woofer phase through in reconWfrUnclippedPhase[], 
//				> primarily for diagnostic purposes and to test M1 and M2 offloading
				for( yy=0; yy<WOOFER_COL_SIZE; yy++)
				{
					for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
					{
						tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
						reconBufPtr->reconWfrUnclippedPhase[ tmpNdx ] = 
							reconBufPtr->reconRawWfrPhase[ xxIn + yy * WOOFER_ROW_SIZE ];
					}
				}

// 				if lastReconBufPtr is not NULL, copy forward the previous woofer phase values
				if( lastReconBufPtr != NULL )
				{
					for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
					{
						reconBufPtr->reconWfrPhase[ii] = lastReconBufPtr->reconWfrPhase[ii];
						reconBufPtr->reconWfrVolts[ii] = lastReconBufPtr->reconWfrVolts[ii];
					}
				}
				else
				{
					for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
					{
						reconBufPtr->reconWfrPhase[ii] = 0.0f;
						reconBufPtr->reconWfrVolts[ii] = 0.0f;
					}
				}
			}
			break;

		case eAOLoopOpening:
			// If loop is opening and 'lastReconBufPtr' is set then copy forward the previous woofer phase values
			if( lastReconBufPtr == NULL )
			{
				for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
				{
					reconBufPtr->reconWfrPhase[ii] = 0.0;
					reconBufPtr->reconWfrVolts[ii] = 0.0;
				}
			}
			else
			{	
				for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
				{
					reconBufPtr->reconWfrPhase[ii] = lastReconBufPtr->reconWfrPhase[ii];
					reconBufPtr->reconWfrVolts[ii] = lastReconBufPtr->reconWfrVolts[ii];
				}
			}
			hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopOpen;
			hrt_AoWfrLoopStat = eAOLoopOpen;
			break;

		case eAOLoopClosing:
			// If loop is closing, zero the integrators
			// IMPORTANT -- this implies that the woofer should be commanded to its bias before closing the loop
			for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
			{
				reconBufPtr->reconWfrPhase[ii] = 0.0;
				reconBufPtr->reconWfrVolts[ii] = 0.0;
			}
			hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopAlmostClosed;
			hrt_AoWfrLoopStat = eAOLoopAlmostClosed;
			break;

		case eAOLoopAlmostClosed:
		case eAOLoopClosed:
//			flip data in x for woofer, integrate, and gather cleanup data
			if (oldWfrVecSize != ParseAlgFTR_wfrVecSize)
			{
				oldWfrVecSize = ParseAlgFTR_wfrVecSize;
				cleanupModeNdx = 0;
				numActiveCleanupModes = NUM_ACTIVE_WFR_ACTS - ParseAlgFTR_wfrVecSize * 2;  // this is the number of uncontrolled woofer modes
			}
			cleanupCoeff = 0.0;
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					reconBufPtr->reconWfrPhase[tmpNdx] = 
						reconBufPtr->reconTTWfrPhase[tmpNdx] * hrt_modal_loss_dm +
						reconBufPtr->reconRawWfrPhase[ xxIn + yy * WOOFER_ROW_SIZE ];
					cleanupCoeff += reconBufPtr->reconWfrPhase[tmpNdx] * hrt_wfrCleanup[cleanupModeNdx][tmpNdx];
				}
			}

//			clean up one woofer mode per frame
			if (fabs(cleanupCoeff) < hrt_mode_cleanup_thresh)
				cleanupCoeff = 0.0;
			cleanupCoeff *= hrt_mode_cleanup_gain;

			for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
				reconBufPtr->reconWfrPhase[ii] -= cleanupCoeff * hrt_wfrCleanup[cleanupModeNdx][ii];

			reconBufPtr->reconClipCleanupData[eClipCleanupWfrPiston] = 0.0;  // note -- not used at present
			reconBufPtr->reconClipCleanupData[eClipCleanupWfrCleanupMode] = cleanupModeNdx;
			reconBufPtr->reconClipCleanupData[eClipCleanupWfrCleanupValue] = cleanupCoeff;

			if (++cleanupModeNdx >= numActiveCleanupModes)
				cleanupModeNdx = 0;

//XXXdwp110516 -- for testing only -- start
			for( yy=0; yy<WOOFER_COL_SIZE; yy++)
			{
				for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
				{
					tmpNdx = xxOut + yy * WOOFER_ROW_SIZE;
					tmpReconWfrPhase[tmpNdx] = reconBufPtr->reconWfrPhase[tmpNdx] + hrt_wfrLabFlat[xxIn + yy * WOOFER_ROW_SIZE];
				}
			}
//XXXdwp110516 -- for testing only -- end
//			convert woofer phase to volts and output to mirror
			respFromConv = gpAoHrtVolt_ConvWoofer( tmpReconWfrPhase,  //XXXdwp110516 -- for testing only -- remove this
//XXXdwp110516 -- for testing only -- put this back			respFromConv = gpAoHrtVolt_ConvWoofer( reconBufPtr->reconWfrPhase, 
												   reconBufPtr->reconWfrVolts, 
												   reconBufPtr->reconWfrUnclippedPhase );
			if( respFromConv > 0)
				SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_ProcessWoofer) 'gpAoHrtVolt_ConvWoofer' failed");
			else
			{
				gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconWfrVolts, false );
				reconBufPtr->reconClipCleanupData[eClipCleanupWfrClipCount] = -respFromConv;  // respFromConv will be negative if clipping occurred
			}

			if (hrt_AoWfrLoopStat == eAOLoopAlmostClosed)
			{
				hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopClosed;  // this is because we can't fully close until there's signal 
				hrt_AoWfrLoopStat = eAOLoopClosed;				   // > on the integrators, primarily for gpAoHrtTipTilt_Process()
			}
			break;
	}
	return;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_ProcessStates( ReconBuffType* reconBufPtr  )
 *
 *	@brief
 *	This module tests and updates woofer and tweeter states, as necessary.  
 *	
 *	@b DESCRIPTION:
 *	This module tests and updates woofer and tweeter states, as necessary.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_ProcessStates( (ReconBuffType*) reconBufPtr  );
 * 
 *	@param[ ] none
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void gpAoHrtRecon_ProcessStates( ReconBuffType* reconBufPtr  )
{
	int i;

	reconBufPtr->twt_state = hrt_AoTwtLoopStat;
	reconBufPtr->wfr_state = hrt_AoWfrLoopStat;
	reconBufPtr->tt_state = hrt_AoTTLoopStat;
	reconBufPtr->ofc_state = hrt_optimizer_OFC_Stat;  // open or closed
	reconBufPtr->ofc_step  = ReconOpt_State;		  // step in the OFC state machine

//	variables used in dark-corner background / dark level correction (using unilluminated corner pixels)
//	> IMPORTANT -- The values that go in these variables are actually determined in the centroider thread!  
//	> IMPORTANT -- This is simply to save yet another dumpDiag data packet while making sure the dark-corner 
//	> IMPORTANT -- correction works.  
	reconBufPtr->hrt_darkCornerCorrectSwitch = hrt_darkCornerCorrectSwitch;
	reconBufPtr->darkCornerPrevFrameRateNdx = darkCornerPrevFrameRateNdx;
	reconBufPtr->darkCornerBoxcarIndex = darkCornerBoxcarIndex; 
	reconBufPtr->darkCornerBoxcarN = darkCornerBoxcarN;
	for ( i = 0; i < DARK_CORNER_NUM_PIXELS; i++ )
	{
		reconBufPtr->darkCornerBoxcarSums[i] = darkCornerBoxcarSums[i];
		reconBufPtr->darkCornerBoxcarAves[i] = darkCornerBoxcarAves[i];
	}
	reconBufPtr->darkCornerCorrection = darkCornerCorrection;

	// test if we need to update the Woofer State
	switch( hrt_AoWfrLoopStat )
	{
		case eAOLoopOpen:
			if( hrt_AoWfrLoopRequest == 1 )			// 'close' request
			{
				hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopClosing;
				hrt_AoWfrLoopStat = eAOLoopClosing;
			}
			hrt_AoWfrLoopRequest = -1;
			break;

		case eAOLoopClosed:
			if( hrt_AoWfrLoopRequest == 0 )			// 'open' request
			{
				hrtFileDataShmPtr->aoWfrLoopStat = eAOLoopOpening;
				hrt_AoWfrLoopStat = eAOLoopOpening;
			}
			hrt_AoWfrLoopRequest = -1;
			break;

	}

	// test if we need to update the Tweeter State
	switch( hrt_AoTwtLoopStat )
	{
		case eAOLoopOpen:
			if( hrt_AoTwtLoopRequest == 1 )			// 'close' request
			{
				hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopClosing;
				hrt_AoTwtLoopStat = eAOLoopClosing;
			}
			hrt_AoTwtLoopRequest = -1;
			break;

		case eAOLoopClosed:
			if( hrt_AoTwtLoopRequest == 0 )			// 'open' request
			{
				hrtFileDataShmPtr->aoTwtLoopStat = eAOLoopOpening;
				hrt_AoTwtLoopStat = eAOLoopOpening;
			}
			hrt_AoTwtLoopRequest = -1;
			break;
	}

	return;
}

/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CloseOpenOFCLoop( int newState )
 *
 *	@brief
 *	Close the twt AO loop.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to close or open the OFC loop.  This is done by 
 *	initializing variables and setting the hrt_optimizer_OFC_Stat flag to indicate that the 
 *	loop is closed or open.
 *	@par
 *	
 *	@b Usage: gpAoHrtRecon_CloseOpenOFCLoop( );
 * 
 *	@param[in]  newState (int) : 0: open, 1: close
 *
 *	@return 
 *		0: no error
 *		1: "AO Loops not both closed";
 *		2: "Bad State requested - must be 0 or 1 ";
 *	 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int gpAoHrtRecon_CloseOpenOFCLoop( int newState )
{
	switch( newState )
	{
		default:
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_CloseOpenOFCLoop) OFC OnOff - invalid state requested: ", newState);
			return 2;
			break;

		case 0:
			//	set flag indicating that the OFC loop is opening
			hrt_optimizer_OFC_Stat = eAOLoopOpening;
			break;

		case 1:
			if(  (hrt_AoTwtLoopStat == eAOLoopClosed ) && (hrt_AoWfrLoopStat == eAOLoopClosed ) )
			{
				//	set flag indicating that the OFC loop is closing
				hrt_optimizer_OFC_Stat = eAOLoopClosing;
				gpAoHrtReconOpt_FTRAlgoStart();
				break;
			}

			SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_CloseOpenOFCLoop) OFC can only be closed if both WFR and TWT are closed already");
			return 1;
			break;
	}
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CloseOpenTweeterLoop( int newState )
 *
 *	@brief
 *	Close the twt AO loop.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to close the twt AO loop.  This is done by 
 *	initializing variables and setting the AOLoopStat flag to indicate that the 
 *	loop is closed.
 *	@par
 *	
 *	@b Usage: gpAoHrtRecon_CloseTwtLoop( );
 * 
 *	@param[in]  newState (int) : 0: open, 1: close, -1: remain same
 *
 *	@return 
 *	0: no error, else error
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int gpAoHrtRecon_CloseOpenTweeterLoop( int newState )
{
	switch( newState )
	{
	case 0:
		//	set flag indicating that the AO loop is opening
		hrt_AoTwtLoopRequest = 0;
		break;

	case 1:
		//	initialize variables
		hrt_AoTwtLoopRequest = 1;
		break;

	default:		// Typically - no change
		hrt_AoTwtLoopRequest = -1;
		break;
	}

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CloseOpenWooferLoop( int newState )
 *
 *	@brief
 *	Open the wfr AO loop.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to open the wfr AO loop.  This is done by setting 
 *	the AOLoopStat flag to indicate that the loop is opening.  The loop will be fully 
 *	open the next time gpAoHrtRecon_Process() executes.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_CloseOpenWooferLoop( newState );
 * 
 *	@param[in]  newState (int):	0: Open, 1: Close, -1: no Change
 *
 *	@return 
 *	Err Code ( 0: no error else error code )
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static int gpAoHrtRecon_CloseOpenWooferLoop( int newState )
{
	switch( newState )
	{
	case 0:
		//	set flag indicating that the AO loop is opening
		hrt_AoWfrLoopRequest = 0;
		break;

	case 1:
		//	initialize variables
		hrt_AoWfrLoopRequest = 1;
		break;

	default:		// Typically - no change
		hrt_AoWfrLoopRequest = -1;
		break;
	}
	return 0;	
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CalcStats( CentBuffType* centBufPtr, ReconBuffType*  reconBufPtr, ReconBuffType* lastReconBufPtr )
 *
 *	@brief
 *	Accumulates data and calculates statistics periodically.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to accumulate data and calculate statistics 
 *	periodically.  The statistics calculated are SNR, RMS WFS error over time and
 *	space, and r0.  For more information, please see GPI_AO_StatCalcs.docx, by 
 *	D. Palmer.  This function is called after the real-time critical processing
 *	has been completed.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_CalcStats( centBufPtr, reconBufPtr, lastReconBufPtr );
 * 
 *	@param[in] centBufPtr		(CentBuffType*):	current centroid buffer pointer
 *	@param[in] reconBufPtr		(ReconBuffType*):	current recon buffer pointer
 *	@param[in] lastReconBufPtr	(ReconBuffType*):	previous recon data pointer
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static float intensitiesForSNR[MAX_FRAME_RATE][4];

static void gpAoHrtRecon_CalcStats( CentBuffType* centBufPtr, ReconBuffType* reconBufPtr, ReconBuffType* lastReconBufPtr )
{
	char   statsText[DIAG_STRING_MAX_LENGTH];
	int	   i, j, ndx;
	float  xCent, yCent;
	float* pPupI;
	double sumOfSquares = 0.0;
	float  tip, tilt, tipTilt;
	float  smoothedTT, variance;

	static int framesCntr = 0;
	static const int snrIndexes[4] = {
		15 * APERTURE_GRID_SIZEROW + 18,
		32 * APERTURE_GRID_SIZEROW + 18,
		15 * APERTURE_GRID_SIZEROW + 29,
		32 * APERTURE_GRID_SIZEROW + 29 };
	static float sumOfIntens[4] = { 0.0, 0.0, 0.0, 0.0 };
	static float allSumOfSquares = 0.0;
	static boolean needToResetR0 = false;
	static float crntN = 0.0, prevTT = 0.0;
	static float  newSNR, newRmsWfsError, newR0;

//	SNR -- store intensities for 4 well-illuminated sub-aps for each frame and sum them for later
	for(i = 0; i < 4; i++)
	{
		intensitiesForSNR[framesCntr][i] = centBufPtr->intens[snrIndexes[i]];
		sumOfIntens[i] += intensitiesForSNR[framesCntr][i];
	}

//	RMS WFS error over time and space -- sum the squares of the active centroid 
//	> values and store the result for each frame
	for(i = 0; i < APERTURE_GRID_SIZEROW; i++)
	{
		if (pupRowStarts[i] > 0)  // row isn't empty
		{
			pPupI = &(hrtFileDataShmPtr->aperturePattern[i][0]);  // point at start of current active row in aperture
			ndx = i * APERTURE_GRID_SIZEROW;					  // walking down by rows
			for( j = pupRowStarts[i]; j <= pupRowEnds[i]; j++)	  // active area for this row
			{
				if(pPupI[j] != 0.0)  //test for the secondary obscuration
				{
					xCent = centBufPtr->xyCents[ndx + j];
					yCent = centBufPtr->xyCents[MAX_NUM_SUBAPS + ndx + j];
					sumOfSquares += xCent * xCent;
					sumOfSquares += yCent * yCent;
				}
			}
		}
	}
	sumOfSquares /= hrt_numSubaps * 2;
	allSumOfSquares += sumOfSquares;

//	r0
//	calculate overall tip and tilt for each frame
	if( lastReconBufPtr == NULL )
	{
		tip  = reconBufPtr->reconTTData[eTTxErrorAngle];
		tilt = reconBufPtr->reconTTData[eTTyErrorAngle];
	}
	else
	{
		tip  = reconBufPtr->reconTTData[eTTxErrorAngle] + 
			   (lastReconBufPtr->reconTTData[eTTxSettingAngle] + lastReconBufPtr->reconTTData[eTTxSettingAngleWoofer]);
		tilt = reconBufPtr->reconTTData[eTTyErrorAngle] + 
			   (lastReconBufPtr->reconTTData[eTTySettingAngle] + lastReconBufPtr->reconTTData[eTTySettingAngleWoofer]);
	}
	tipTilt = sqrt(tip * tip + tilt * tilt);

//	smooth tip/tilt over time (r0 estimates calculated with this approach will only be close to correct on 
//	> average (instantaneous calculations will vary widely))
	if (hrt_AoTTLoopStat == eAOLoopOpen)
		needToResetR0 = true;
	if (needToResetR0 && (hrt_AoTTLoopStat == eAOLoopClosed))  // reset the rolling average when the loop closes
	{
		needToResetR0 = false;
		crntN = 0.0;
	}
	if (crntN > (float)LegalFrameRates[hrt_camFrameRate])  // in case the frame rate has been lowered
		crntN = (float)LegalFrameRates[hrt_camFrameRate];
	else if (crntN < (float)LegalFrameRates[hrt_camFrameRate])
		crntN += 1.0;
	smoothedTT = (prevTT * (crntN - 1.0) + tipTilt) / crntN;
	prevTT = smoothedTT;

//	calculate the variance and then r0 (the hard-coded values are not subject to change)
	variance = 0.00956 * smoothedTT;
	variance *= variance;
	newR0 = 63.755 / pow(variance, 0.6);

//	finish stats calculations once per second
	if (++framesCntr >= LegalFrameRates[hrt_camFrameRate])
	{
		float intensMeans[4], stdDevs[4], tmpFloat;

//		SNR -- calc the mean and standard deviation for each of the 4 stored 
//		> sub-ap intensity sets and the means divided by the standard deviations; 
//		> the average of these is the measured SNR
		for(i = 0; i < 4; i++)
		{
			intensMeans[i] = sumOfIntens[i] / framesCntr;
			stdDevs[i] = 0.0;
		}
		for(i = 0; i < framesCntr; i++)
		{
			for( j = 0; j < 4; j++)
			{
				tmpFloat = intensitiesForSNR[i][j] - intensMeans[j];
				stdDevs[j] += tmpFloat * tmpFloat;
			}
		}
		newSNR = 0.0;
		for(i = 0; i < 4; i++)
		{
			stdDevs[i] = sqrt(stdDevs[i] / (framesCntr - 1));
			if (stdDevs[i] != 0.0)  // extremely unlikely, but check anyway
				newSNR += intensMeans[i] / stdDevs[i];
			sumOfIntens[i] = 0.0;
		}
		newSNR /= 4.0;

//		RMS WFS error over time and space -- calc the mean and square root and convert to mas
		newRmsWfsError = sqrt(allSumOfSquares / (float)framesCntr) * MAS__PER__MICRON_SLOPE_PER_SUBAP;
		allSumOfSquares = 0.0;

//		r0 -- all r0 calcs are done every frame

//		print results, if we're in the right mode
		if ((hrtFileDataShmPtr->tlcOnFlag < 0) && 	  // don't bother doing this, if we're not going to send it
			(eDebugLevelMin <= hrtSystemDebugLevel))
		{
//			unfortunately, RTLinux doesn't handle %f
			volatile int tmpInt1, tmpInt2;
			float tmpFloats[3];
			char tmpStrings[3][16];

			tmpFloats[0] = newSNR;
			tmpFloats[1] = newRmsWfsError;
			tmpFloats[2] = newR0;

			for (i = 0; i < 3; i++)
			{
				tmpInt1 = (int)tmpFloats[i];
				tmpInt2 = abs((int)((tmpFloats[i] - (float)tmpInt1) * 100000.0));
				tmpInt1 = abs(tmpInt1);
				if (tmpFloats[i] < 0.0)
					sprintf(tmpStrings[i], "-%d.%05d", tmpInt1, tmpInt2);
				else
					sprintf(tmpStrings[i], "%d.%05d", tmpInt1, tmpInt2);
			}
			sprintf(statsText, "Stats: SNR: %s, RMS WFS error: %s, r0: %s", tmpStrings[0], tmpStrings[1], tmpStrings[2]);
			SysConSendStrngNoParm(eDebugLevelMin, statsText);
		}

		framesCntr = 0;
	}

//	store results in the statistics array in the current recon buffer, to be reported 
//	> to the host and for diagnostic purposes
	reconBufPtr->statistics[eStatsSNR]						 = newSNR;
	reconBufPtr->statistics[eStatsRmsWfsError]				 = newRmsWfsError;
	reconBufPtr->statistics[eStatsR0]						 = newR0;
	reconBufPtr->statistics[eStatsSNR_Intensities_0]		 = intensitiesForSNR[framesCntr][0];
	reconBufPtr->statistics[eStatsSNR_Intensities_1]		 = intensitiesForSNR[framesCntr][1];
	reconBufPtr->statistics[eStatsSNR_Intensities_2]		 = intensitiesForSNR[framesCntr][2];
	reconBufPtr->statistics[eStatsSNR_Intensities_3]		 = intensitiesForSNR[framesCntr][3];
	reconBufPtr->statistics[eStatsRmsWfsError_sumOfSquares]	 = sumOfSquares;
	reconBufPtr->statistics[eStatsR0_tip]					 = tip;
	reconBufPtr->statistics[eStatsR0_tilt]					 = tilt;
	reconBufPtr->statistics[eStatsR0_tipTilt]				 = tipTilt;
	reconBufPtr->statistics[eStatsR0_crntN]					 = crntN;
	reconBufPtr->statistics[eStatsR0_smoothedTT]			 = smoothedTT;
	reconBufPtr->statistics[eStatsR0_variance]				 = variance;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_OffloadM1M2( ReconBuffType*  reconBufPtr )
 *
 *	@brief
 *	Project off and filter Zernike modes to be offloaded to M1 and M2.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to project off and filter Zernike modes to be 
 *	offloaded to M1 and M2.  The data is triply buffered, to facilitate asynchronous
 *	use by the SRT.  The modes are projected off and filtered in steps (or states) 
 *	as follows (done in round robin fashion (one per frame)): 
 *	@par
 *		1.	project next of 16 high order Zernike modes	for M1
 *	@par
 *		2.	filter high order mode projected in 1
 *	@par
 *		3.	project focus
 *	@par
 *		4.	filter focus
 *	@par
 *		5.	project next of 16 high order Zernike modes for M1
 *	@par
 *		6.	filter high order mode projected in 5
 *	@par
 *		7.	filter tip calculated by T/T code
 *	@par
 *		8.	filter tilt calculated by T/T code
 *	@par
 *	For more information, see M1_M2_Mode_Projections_withNotes.doc by D. Palmer
 *	and zernikeoffload1.doc by J-P. Veran.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_OffloadM1M2( reconBufPtr );
 * 
 *	@param[in] reconBufPtr	(ReconBuffType*):	current recon buffer pointer
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void	gpAoHrtRecon_OffloadM1M2( ReconBuffType*  reconBufPtr )
{
	boolean m1Done = false, m2Done = false;
	int i, j;
	int localM;
	float r1x = 1.0, r1y = 0.0, r2x = 0.0, r2y = 1.0;  // rotation coefficients
	static int offloadState = 0, m1ModeNdx = 0;
	static int m1WriteBuffNdx = 0, m2WriteBuffNdx = 0;

	switch( offloadState )
	{
		case 0:	 // init filters, etc. -- at present, this just happens once -- could happen when woofer loop closes
			for ( i = 0; i < eM1MaxVals; i++ )
			{
				m1FilterNdxs[i] = 0;
				for ( j = 0; j < MAX_OFFLOAD_FILTER_VALS; j++ )
					rawM1FilterVals[i][j] = 0.0;
				crntRotatedM1Vals[i] = 0.0;
			}
			for ( i = 0; i < eM2MaxVals; i++ )
			{
				m2FilterNdxs[i] = 0;
				for ( j = 0; j < MAX_OFFLOAD_FILTER_VALS; j++ )
					rawM2FilterVals[i][j] = 0.0;
				crntRotatedM2Vals[i] = 0.0;
			}
			break;

		case 1:	 // project out next of 16 high order Zernike modes for M1 ---  don't combine cases 1 and 5 -- we
		case 5:	 // project out next of 16 high order Zernike modes for M1 ---  > need 8 states for filter timing
			crntRawM1Vals[m1ModeNdx] = 0.0;
			for ( i = 0; i < MAX_WOOFER_ACTS; i++ )
				crntRawM1Vals[m1ModeNdx] += hrtFileDataShmPtr->m1ZernikeVectors[m1ModeNdx][i] * reconBufPtr->reconWfrUnclippedPhase[i];

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2MaxVals + m1ModeNdx + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 0;  // 0 = the mode was just projected
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM1Vals[m1ModeNdx];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM1Vals[m1ModeNdx];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM1Vals[m1ModeNdx];
			break;

		case 2:	 // filter high order M1 mode projected out in 1, above ---  don't combine cases 2 and 6 -- we
		case 6:	 // filter high order M1 mode projected out in 5, above ---  > need 8 states for filter timing
			localM = m1FilterMs[hrt_camFrameRate];
			if ( ++m1FilterNdxs[m1ModeNdx] >= localM )
				m1FilterNdxs[m1ModeNdx] = 0;
			rawM1FilterVals[m1ModeNdx][m1FilterNdxs[m1ModeNdx]] = crntRawM1Vals[m1ModeNdx];

			crntFilteredM1Vals[m1ModeNdx] = 0.0;
			for ( i = 0; i < localM; i++ )
				crntFilteredM1Vals[m1ModeNdx] += rawM1FilterVals[m1ModeNdx][i];
			crntFilteredM1Vals[m1ModeNdx] /= (float)localM;

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2MaxVals + m1ModeNdx + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 1;  // 1 = the mode was just filtered
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM1Vals[m1ModeNdx];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM1Vals[m1ModeNdx];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM1Vals[m1ModeNdx];

			if ( ++m1ModeNdx >= eM1MaxVals )
			{
				m1ModeNdx = 0;
				m1Done = true;
			}
			break;

		case 3:	 // project out focus
			crntRawM2Vals[eM2Focus] = 0.0;
			for ( i = 0; i < MAX_WOOFER_ACTS; i++ )
				crntRawM2Vals[eM2Focus] += hrtFileDataShmPtr->focusZernikeVector[i] * reconBufPtr->reconWfrUnclippedPhase[i];

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2Focus + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 0;  // 0 = the mode was just projected
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM2Vals[eM2Focus];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM2Vals[eM2Focus];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM2Vals[eM2Focus];
			break;

		case 4:	 // filter focus
			localM = m2FilterMs[hrt_camFrameRate];
			if ( ++m2FilterNdxs[eM2Focus] >= localM )
				m2FilterNdxs[eM2Focus] = 0;
			rawM2FilterVals[eM2Focus][m2FilterNdxs[eM2Focus]] = crntRawM2Vals[eM2Focus];

			crntFilteredM2Vals[eM2Focus] = 0.0;
			for ( i = 0; i < localM; i++ )
				crntFilteredM2Vals[eM2Focus] += rawM2FilterVals[eM2Focus][i];
			crntFilteredM2Vals[eM2Focus] /= (float)localM;

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2Focus + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 1;  // 1 = the mode was just filtered
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM2Vals[eM2Focus];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM2Vals[eM2Focus];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM2Vals[eM2Focus];

			m2Done = true;
			break;

		case 7:	 // filter tip calculated by T/T code
			crntRawM2Vals[eM2Tip] = reconBufPtr->reconTTData[eTTxSettingAngle];

			localM = m2FilterMs[hrt_camFrameRate];
			if ( ++m2FilterNdxs[eM2Tip] >= localM )
				m2FilterNdxs[eM2Tip] = 0;
			rawM2FilterVals[eM2Tip][m2FilterNdxs[eM2Tip]] = crntRawM2Vals[eM2Tip];

			crntFilteredM2Vals[eM2Tip] = 0.0;
			for ( i = 0; i < localM; i++ )
				crntFilteredM2Vals[eM2Tip] += rawM2FilterVals[eM2Tip][i];
			crntFilteredM2Vals[eM2Tip] /= (float)localM;

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2Tip + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 1;  // 1 = the mode was just filtered
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM2Vals[eM2Tip];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM2Vals[eM2Tip];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM2Vals[eM2Tip];
			break;

		case 8:	 // filter tilt calculated by T/T code
			crntRawM2Vals[eM2Tilt] = reconBufPtr->reconTTData[eTTySettingAngle];

			localM = m2FilterMs[hrt_camFrameRate];
			if ( ++m2FilterNdxs[eM2Tilt] >= localM )
				m2FilterNdxs[eM2Tilt] = 0;
			rawM2FilterVals[eM2Tilt][m2FilterNdxs[eM2Tilt]] = crntRawM2Vals[eM2Tilt];

			crntFilteredM2Vals[eM2Tilt] = 0.0;
			for ( i = 0; i < localM; i++ )
				crntFilteredM2Vals[eM2Tilt] += rawM2FilterVals[eM2Tilt][i];
			crntFilteredM2Vals[eM2Tilt] /= (float)localM;

			reconBufPtr->reconOffloadDiagData[eOffloadMode]		   = eM2Tilt + 1;
			reconBufPtr->reconOffloadDiagData[eOffloadOp]		   = 1;  // 1 = the mode was just filtered
			reconBufPtr->reconOffloadDiagData[eOffloadRawVal]	   = crntRawM2Vals[eM2Tilt];
			reconBufPtr->reconOffloadDiagData[eOffloadFilteredVal] = crntFilteredM2Vals[eM2Tilt];
			reconBufPtr->reconOffloadDiagData[eOffloadRotatedVal]  = crntRotatedM2Vals[eM2Tilt];
			break;

		default:  // should never happen
			SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon.c) invalid OffloadM1M2 state");
			break;
	}
	if ( ++offloadState > 8 )
		offloadState = 1;

//	calculate rotation coefficients, if necessary
	if (m1Done || m2Done)
	{
		r1x =  cos(hrtFileDataShmPtr->offloadZernRotationAngle * RAD);
		r1y = -sin(hrtFileDataShmPtr->offloadZernRotationAngle * RAD);
		r2x =  sin(hrtFileDataShmPtr->offloadZernRotationAngle * RAD);
		r2y =  cos(hrtFileDataShmPtr->offloadZernRotationAngle * RAD);
	}

	if (m1Done)
	{
		crntRotatedM1Vals[eM1AstigCos]		= r1x * crntFilteredM1Vals[eM1AstigCos] 	+ r1y * crntFilteredM1Vals[eM1AstigSin];
		crntRotatedM1Vals[eM1AstigSin]		= r2x * crntFilteredM1Vals[eM1AstigCos] 	+ r2y * crntFilteredM1Vals[eM1AstigSin];
		crntRotatedM1Vals[eM1ComaX]			= r1x * crntFilteredM1Vals[eM1ComaX] 		+ r1y * crntFilteredM1Vals[eM1ComaY];
		crntRotatedM1Vals[eM1ComaY]			= r2x * crntFilteredM1Vals[eM1ComaX] 		+ r2y * crntFilteredM1Vals[eM1ComaY];
		crntRotatedM1Vals[eM1Spherical]		= 		crntFilteredM1Vals[eM1Spherical];
		crntRotatedM1Vals[eM1TrefoilCos]	= r1x * crntFilteredM1Vals[eM1TrefoilCos] 	+ r1y * crntFilteredM1Vals[eM1TrefoilSin];
		crntRotatedM1Vals[eM1TrefoilSin]	= r2x * crntFilteredM1Vals[eM1TrefoilCos] 	+ r2y * crntFilteredM1Vals[eM1TrefoilSin];
		crntRotatedM1Vals[eM1SecAstigCos]	= r1x * crntFilteredM1Vals[eM1SecAstigCos]	+ r1y * crntFilteredM1Vals[eM1SecAstigSin];
		crntRotatedM1Vals[eM1SecAstigSin]	= r2x * crntFilteredM1Vals[eM1SecAstigCos]	+ r2y * crntFilteredM1Vals[eM1SecAstigSin];
		crntRotatedM1Vals[eM1SecComaX]		= r1x * crntFilteredM1Vals[eM1SecComaX]		+ r1y * crntFilteredM1Vals[eM1SecComaY];
		crntRotatedM1Vals[eM1SecComaY]		= r2x * crntFilteredM1Vals[eM1SecComaX]		+ r2y * crntFilteredM1Vals[eM1SecComaY];
		crntRotatedM1Vals[eM1SecSpherical]  =		crntFilteredM1Vals[eM1SecSpherical];
		crntRotatedM1Vals[eM1TetrafoilX]	= r1x * crntFilteredM1Vals[eM1TetrafoilX]	+ r1y * crntFilteredM1Vals[eM1TetrafoilY];
		crntRotatedM1Vals[eM1TetrafoilY]	= r2x * crntFilteredM1Vals[eM1TetrafoilX]	+ r2y * crntFilteredM1Vals[eM1TetrafoilY];
		crntRotatedM1Vals[eM1SecTrefX]		= r1x * crntFilteredM1Vals[eM1SecTrefX]		+ r1y * crntFilteredM1Vals[eM1SecTrefY];
		crntRotatedM1Vals[eM1SecTrefY]		= r2x * crntFilteredM1Vals[eM1SecTrefX]		+ r2y * crntFilteredM1Vals[eM1SecTrefY];

		if (hrt_AoWfrLoopStat != eAOLoopClosed)
		{
			for ( i = 0; i < eM1MaxVals; i++ )
				hrtFileDataShmPtr->crntM1Vals[m1WriteBuffNdx][i] = 0.0;  // send only zeros, if the woofer loop isn't closed
		}
		else
		{
			for ( i = 0; i < eM1MaxVals; i++ )
				hrtFileDataShmPtr->crntM1Vals[m1WriteBuffNdx][i] = crntRotatedM1Vals[i] / 1000.0;  // convert from microns to millimeters
		}
		hrtFileDataShmPtr->m1ReadBuffNdx = m1WriteBuffNdx;
		if ( ++m1WriteBuffNdx >= 3)
			m1WriteBuffNdx = 0;
	}

	if (m2Done)
	{
		crntRotatedM2Vals[eM2Tip]   = r1x * crntFilteredM2Vals[eM2Tip] + r1y * crntFilteredM2Vals[eM2Tilt];
		crntRotatedM2Vals[eM2Tilt]  = r2x * crntFilteredM2Vals[eM2Tip] + r2y * crntFilteredM2Vals[eM2Tilt];
		crntRotatedM2Vals[eM2Focus] =		crntFilteredM2Vals[eM2Focus];

		if (hrt_AoWfrLoopStat != eAOLoopClosed)
		{
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Tip] = 0.0;    // send only zeros, 
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Tilt] = 0.0;   // > if the woofer loop 
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Focus] = 0.0;  // > isn't closed
		}
		else
		{
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Tip] = 
				crntRotatedM2Vals[eM2Tip] * M2_TT_MM_PER_MAS;		   // convert from mas to mm spot displacement
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Tilt] = 
				crntRotatedM2Vals[eM2Tilt]* M2_TT_MM_PER_MAS;		   // convert from mas to mm spot displacement
			hrtFileDataShmPtr->crntM2Vals[m2WriteBuffNdx][eM2Focus] = 
				crntRotatedM2Vals[eM2Focus] / 1000.0;				   // convert from microns to millimeters
		}
		hrtFileDataShmPtr->m2ReadBuffNdx = m2WriteBuffNdx;
		if ( ++m2WriteBuffNdx >= 3)
			m2WriteBuffNdx = 0;
	}
}


/*!
 *******************************************************************************
 *
 *	@fn int gpAoHrtRecon_InFuncStart( void )
 *
 *	@brief
 *	The purpose of this unit is to start the process of gathering data to create
 *	the tweeter influence function matrix.  
 *
 *	@b DESCRIPTION:  
 *	The purpose of this unit is to start the process of gathering data to create
 *	the tweeter influence function matrix.  This is done by applying a series of
 *	sin waves to the tweeter and collecting data.  The data is then processed 
 *	off-line to produce the matrix.  
 *	@par
 *
 *	@b Usage: retVal = gpAoHrtRecon_InFuncStart( );
 * 
 *	@param[in] none
 *	
 *	@return 
 *		0: No Error  else error
 * 
 *	@exception None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
int gpAoHrtRecon_InFuncStart( void )
{
	makeInFuncState = 0;
	hrt_ReconRunCmd = eCmdMakeInFunc;
	SysConSendStrngNoParm(eDebugLevelMax, "INFO: Starting InFunc for one mode pair");

	// Note: "hrtReconDataPtr->cmdCrnt" is already set 
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn int gpAoHrtRecon_ProcessMakeInFunc(  ReconBuffType* reconBufPtr )
 *
 *	@brief
 *	This unit is the processor for gathering data to create the tweeter influence 
 *	function matrix.  
 *
 *	@b DESCRIPTION:  
 *	This unit is the processor for gathering data to create the tweeter influence 
 *	function matrix.  This is done by applying a series of sin waves to the tweeter 
 *	and collecting data.  The data is then processed off-line to produce the matrix.  
 *	@par
 *
 *	@b Usage: retVal = gpAoHrtRecon_ProcessMakeInFunc( (ReconBuffType*) reconBufPtr   );
 * 
 *	@b Param None.
 *	
 *	@return 
 *		0: No Error  else error
 * 
 *	@exception None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoHrtRecon_ProcessMakeInFunc(  CentBuffType* centBufPtr, ReconBuffType* reconBufPtr  )
{
	short		ii, jj;
	static int	waitCounter;

//	execute the current state in the makeInFunc state machine 
	switch (makeInFuncSettings[ makeInFuncState ].state)
	{
//		start: initialize variables
		case StateInFunc_Start:
			makeInFuncState++;
			break;

		case StateInFunc_SetMode:
			for( jj=0; jj<APERTURE_GRID_SIZECOL; jj++ )
				for( ii=0; ii<APERTURE_GRID_SIZEROW; ii++ )
					makeInFuncModes[ jj ][ ii ][0] = makeInFuncModes[ jj ][ ii ][0] = 0.0f;

			if( makeInFuncSettings[ makeInFuncState ].sine_select )
				makeInFuncModes[ hrtFileDataShmPtr->makeInFunc_yMode ][ hrtFileDataShmPtr->makeInFunc_xMode ][0] = hrtFileDataShmPtr->makeInFunc_amplitude;
			else
				makeInFuncModes[ hrtFileDataShmPtr->makeInFunc_yMode ][ hrtFileDataShmPtr->makeInFunc_xMode ][1] = hrtFileDataShmPtr->makeInFunc_amplitude;

			makeInFunc_UpdateModes = 1;
			waitCounter = 0;
			makeInFuncState++;
			break;
	
		case StateInFunc_Wait_FTR:
			if( ++waitCounter > hrtFileDataShmPtr->makeInFunc_numAvg )
				makeInFuncState++;

			break;
	
		case StateInFunc_SetShape:
			// read Shape phase and add to sys Flat and store to memory
			if( makeInFuncSettings[ makeInFuncState ].sine_select )
			{
				for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
				{
					hrtFileDataShmPtr->makeInFuncTweeterAppliedPhase[0][ ii ] = reconBufPtr->reconRawTwtPhase[ ii ];
					// Now apply calculated mode response to sysFlat and prep to apply to mirror
					reconBufPtr->reconTwtPhase[ ii ] = hrt_twtSysFlat[ ii ] + reconBufPtr->reconRawTwtPhase[ ii ];
				}
			}
			else
			{
				for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
				{
					hrtFileDataShmPtr->makeInFuncTweeterAppliedPhase[1][ ii ] = reconBufPtr->reconRawTwtPhase[ ii ];
					// Now apply calculated mode response to sysFlat and prep to apply to mirror
					reconBufPtr->reconTwtPhase[ ii ] = hrt_twtSysFlat[ ii ] + reconBufPtr->reconRawTwtPhase[ ii ];
				}
			}

			// Set the mirror to this shape
			gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, reconBufPtr->reconTwtVolts, NULL );
			gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );

			makeInFunc_UpdateModes = 0;
			waitCounter = 0;
			makeInFuncState++;
			break;
	
		case StateInFunc_Wait_Shape:
			if( ++waitCounter > hrtFileDataShmPtr->makeInFunc_numAvg )
			{
				if( makeInFuncSettings[ makeInFuncState ].sine_select )
				{
					for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
					{
						hrtFileDataShmPtr->makeInFuncTweeterCentAverage[0][ ii ] = 0.0f;						// Storage location for 'makeInFuncMode' shape - read from FTR
						hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[0][ ii ] = 0.0f;					// Storage location for 'makeInFuncMode' shape - read from FTR
					}
				}
				else
				{
					for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
					{
						hrtFileDataShmPtr->makeInFuncTweeterCentAverage[1][ ii ] = 0.0f;						// Storage location for 'makeInFuncMode' shape - read from FTR
						hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[1][ ii ] = 0.0f;					// Storage location for 'makeInFuncMode' shape - read from FTR
					}
				}
			}				

			makeInFuncState++;
			waitCounter = 0;
			break;
	
		case StateInFunc_Average:
			if(  ++waitCounter > hrtFileDataShmPtr->makeInFunc_numAvg )
			{
				makeInFuncState++;
				break;
			}

			if( makeInFuncSettings[ makeInFuncState ].sine_select )
			{
				for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
				{
					hrtFileDataShmPtr->makeInFuncTweeterCentAverage[0][ ii ] += centBufPtr->xyCents[ ii ];						// Storage location for 'makeInFuncMode' shape - read from FTR
					hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[0][ ii ] += reconBufPtr->reconRawTwtPhase[ ii ];					// Storage location for 'makeInFuncMode' shape - read from FTR
				}
			}
			else
			{
				for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
				{
					hrtFileDataShmPtr->makeInFuncTweeterCentAverage[1][ ii ] += centBufPtr->xyCents[ ii ];					// Storage location for 'makeInFuncMode' shape - read from FTR
					hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[1][ ii ] += reconBufPtr->reconRawTwtPhase[ ii ];					// Storage location for 'makeInFuncMode' shape - read from FTR
				}
			}

			break;
	
		case StateInFunc_SaveData:
			for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
			{
				hrtFileDataShmPtr->makeInFuncTweeterCentAverage[ 0 ][ ii ] /= hrtFileDataShmPtr->makeInFunc_numAvg;				// Storage location for 'makeInFuncMode' shape - read from FTRc
				hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[ 0 ][ ii ] /= hrtFileDataShmPtr->makeInFunc_numAvg;
				hrtFileDataShmPtr->makeInFuncTweeterCentAverage[ 1 ][ ii ] /= hrtFileDataShmPtr->makeInFunc_numAvg;				// Storage location for 'makeInFuncMode' shape - read from FTRc
				hrtFileDataShmPtr->makeInFuncTweeterCalcPhaseAverage[ 1 ][ ii ] /= hrtFileDataShmPtr->makeInFunc_numAvg;
			}

			gpAoHrtFileData_Put( eFileTypeMakeInFunc );
			makeInFuncState++;
			break;
	
		case StateInFunc_Idle:
			hrt_ReconRunCmd = eCmdNone;
			makeInFuncState = 0;

			// Set Shm status so that it can be sent through to GMB status
			AOC_HRT_GMB_WRITE_CMD_STATUS( hrtReconDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
			hrtReconDataPtr->cmdCrnt = eCmdNone;

			SysConSendStrngNoParm(eDebugLevelMin, "INFO 'makeInFunc' complete");
			break;

		default:	
		case StateInFunc_Invalid:
			break;
	}

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn int gpAoHrtRecon_StopMakeInFunc( void )
 *
 *	@brief
 *	The purpose of this unit is to abort the process of gathering data to create 
 *	the tweeter influence function matrix.
 *
 *	@b DESCRIPTION:  
 *	The purpose of this unit is to abort the process of gathering data to create 
 *	the tweeter influence function matrix.
 *	@par
 *
 *	@b Usage: retVal = gpAoHrtRecon_StopMakeInFunc( );
 * 
 *	@b Param None.
 *	
 *	@return 
 *		0: No Error  else error
 * 
 *	@exception None.
 *
 *	@par @b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoHrtRecon_StopMakeInFunc( void )
{
	AOC_HRT_GMB_WRITE_CMD_STATUS(  hrt_ReconRunCmd, 0, GPI_STATE_IDLE, "" );
	SysConSendStrngNoParm(eDebugLevelMin, "INFO 'makeInFunc' aborted");
	hrt_ReconRunCmd = eCmdNone;
	hrtReconDataPtr->cmdCrnt = eCmdNone;
	makeInFuncState = 0;
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_InitPupStartEnd( )
 *
 *	@brief
 *	Initialize pupRowStarts,pupRowEnds,pupColStarts,pupColEnds and find tweeter slave actuators.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this function is to initiliza the rows start and end indexes as well as the
 *  columns start and row indexes describing the location of the pupil. It is using the pupil 
 *	definition from hrtFileDataShmPtr->aperturePattern[ii][jj] set up in hostICon.c.  
 *	@par
 *	This function also sets up the tip/tilt (TT) vector.  At present, this vector 
 *	gives each sub-ap equal weight and results in the mean values for X and Y.  If 
 *	necessary, this vector could be read in as a file or calculated to give edge (partially 
 *	illuminated) sub-aps less weight, or some other such thing.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_InitPupStartEnd( );
 * 
 *	\@param[in] None
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_InitPupStartEnd( void )
{
	int	  ii, jj;
	float ttCntInverse;

//	determine the pupil start and end indexes for the rows and columns; this takes into account the 
//  possibility that the pupil could start or end on the first or last row or column; but, if this
//  is true, the threads won't work properly (so, might want to flag an error)
	for( ii = 0; ii < APERTURE_GRID_SIZEROW; ii++)
	{
		if (hrtFileDataShmPtr->aperturePattern[ii][0] == 0)
			pupRowStarts[ii] = -1;  // set to -1 in case the row is empty
		else
			pupRowStarts[ii] = 0;

		if (hrtFileDataShmPtr->aperturePattern[ii][APERTURE_GRID_SIZECOL - 1] == 0)
			pupRowEnds[ii] = -1;  // set to -1 in case the row is empty
		else
			pupRowEnds[ii] = APERTURE_GRID_SIZECOL - 1;
	}
	for( jj = 0; jj < APERTURE_GRID_SIZECOL; jj++)
	{
		if (hrtFileDataShmPtr->aperturePattern[0][jj] == 0)
			pupColStarts[jj] = -1;  // set to -1 in case the column is empty
		else
			pupColStarts[jj] = 0;

		if (hrtFileDataShmPtr->aperturePattern[APERTURE_GRID_SIZEROW - 1][jj] == 0)
			pupColEnds[jj] = -1;  // set to -1 in case the column is empty
		else
			pupColEnds[jj] = APERTURE_GRID_SIZEROW - 1;
	}

	for(ii = 0; ii < APERTURE_GRID_SIZEROW; ii++)
	{
		for(jj = 0; jj < (APERTURE_GRID_SIZECOL - 1); jj++)
		{
			if ((hrtFileDataShmPtr->aperturePattern[ii][jj] != 1) && (hrtFileDataShmPtr->aperturePattern[ii][jj + 1] == 1))
			{
				pupRowStarts[ii] = jj + 1;
				break;
			}
		}
	}
	for( ii = APERTURE_GRID_SIZEROW - 1; ii >= 0; ii--)
	{
		for( jj = APERTURE_GRID_SIZECOL - 1; jj >= 1; jj--)
		{
			if ((hrtFileDataShmPtr->aperturePattern[ii][jj] != 1) && (hrtFileDataShmPtr->aperturePattern[ii][jj - 1] == 1))
			{
				pupRowEnds[ii] = jj - 1;
				break;
			}
		}
	}
	for(jj = 0; jj < APERTURE_GRID_SIZECOL; jj++)
	{
		for(ii = 0; ii < (APERTURE_GRID_SIZEROW - 1); ii++)
		{
			if ((hrtFileDataShmPtr->aperturePattern[ii][jj] != 1) && (hrtFileDataShmPtr->aperturePattern[ii + 1][jj] == 1))
			{
				pupColStarts[jj] = ii + 1;
				break;
			}
		}
	}
	for( jj = APERTURE_GRID_SIZECOL - 1; jj >= 0; jj--)
	{
		for( ii = APERTURE_GRID_SIZEROW - 1; ii >= 1; ii--)
		{
			if ((hrtFileDataShmPtr->aperturePattern[ii][jj] != 1) && (hrtFileDataShmPtr->aperturePattern[ii - 1][jj] == 1))
			{
				pupColEnds[jj] = ii - 1;
				break;
			}
		}
	}

//	Reset the counters and then set them up
	for( ii = 0; ii < APERTURE_GRID_SIZEROW; ii++)
		ReconAlgFTR_ttSumCntX[ii] = 0;
	for( ii = 0; ii < APERTURE_GRID_SIZECOL; ii++)
		ReconAlgFTR_ttSumCntY[ii] = 0;
	ttCntInverse = 0.0;  // used to calculate TT vector, below

	for( ii=0; ii<APERTURE_GRID_SIZEROW; ii++)
	{
		for( jj=0; jj<APERTURE_GRID_SIZECOL; jj++)
		{
			if (hrtFileDataShmPtr->aperturePattern[ii][jj] != 0)
			{
				ReconAlgFTR_ttSumCntX[ii]++;
				ReconAlgFTR_ttSumCntY[jj]++;
				ttCntInverse += 1.0;
			}
		}
	}
	ttCntInverse = 1.0 / ttCntInverse;

//	now run through the sub-ap pattern one more time to calculate the TT vector
	for( ii = 0; ii < APERTURE_GRID_SIZEROW; ii++ )
	{
		for( jj = 0; jj < APERTURE_GRID_SIZECOL; jj++ )
		{
			if (hrtFileDataShmPtr->aperturePattern[ii][jj] == 0)
				ReconAlgFTR_tipTiltVector[ii * APERTURE_GRID_SIZECOL + jj] = 0.0;
			else
				ReconAlgFTR_tipTiltVector[ii * APERTURE_GRID_SIZECOL + jj] = ttCntInverse;
		}
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_InitTweeterSlaving( )
 *
 *	@brief
 *	Find tweeter slave actuators.  
 *
 *	@b DESCRIPTION:
 *	The purpose of this function is to find the tweeter slave actuators.  Each slave 
 *	'destination' actuator can have one or two 'source' actuators.  When the slave 
 *	actuators are used, the phase values for the source actuators are averaged to give 
 *	the phase value for the destination actuator.  
 *
 *	@b Usage: gpAoHrtRecon_InitTweeterSlaving( );
 * 
 *	\@param[in] None
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static float twtActPattern[NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];  // used for tweeter slaving

static void gpAoHrtRecon_InitTweeterSlaving( void )
{
	static const int numSubapCols = NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP;
	short ii, jj;

//	make sure the tweeter actuator pattern starts as all zeros
	for(ii = 0; ii < APERTURE_GRID_SIZECOL; ii++)
	{
		for(jj = 0; jj < APERTURE_GRID_SIZEROW; jj++)
			twtActPattern[ii][jj] = 0;
	}

//	OR the sub-ap pattern with itself shifted right 1, down 1, and down/right 1 (this is how we 
//	> determine the active tweeter actuators from the active sub-aperture pattern))
	for(ii = 1; ii < APERTURE_GRID_SIZECOL; ii++)  // note that this does NOT test the first row
	{
		for(jj = 1; jj < APERTURE_GRID_SIZEROW; jj++)  // note that this does NOT test the first column
		{
			if ((hrtFileDataShmPtr->aperturePattern[ii    ][jj    ] > 0) ||  // sub-aperture pattern
				(hrtFileDataShmPtr->aperturePattern[ii    ][jj - 1] > 0) ||  // sub-aperture pattern shifted right 1
				(hrtFileDataShmPtr->aperturePattern[ii - 1][jj    ] > 0) ||	 // sub-aperture pattern shifted down 1
				(hrtFileDataShmPtr->aperturePattern[ii - 1][jj - 1] > 0))	 // sub-aperture pattern shifted down and right 1
				twtActPattern[APERTURE_GRID_SIZEROW - 1 - ii][jj] = 1;  // aperturePattern[][] is upside down wrt tweeter
		}
	}

//	intialize all tweeter slave fields to -1 (-1 terminates the list and also indicates whether there
//	> is a second slave source or not)
	for (ii = 0; ii < APERTURE_GRID_AREA; ii++)
	{
		for (jj = 0; jj < eTwtSlaveMaxFields; jj++)
		{
			tweeterSlaves[ii][jj] = -1;
			tweeterSlavesTmp[ii][jj] = -1;
		}
	}

//	find the first ring of tweeter slave actuators
	for(ii = 0; ii < APERTURE_GRID_SIZECOL; ii++)
	{
		for(jj = 0; jj < (APERTURE_GRID_SIZEROW - 1); jj++)
		{
			if ((twtActPattern[ii][jj] != 1) && (twtActPattern[ii][jj + 1] == 1))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii, jj + 1);  // destRow, destCol, srcRow, srcCol
				twtActPattern[ii][jj] = 2;  // indicate that the location is in the first ring of slaves
				break;
			}
		}
	}
	for( ii = APERTURE_GRID_SIZECOL - 1; ii >= 0; ii--)
	{
		for( jj = APERTURE_GRID_SIZEROW - 1; jj >= 1; jj--)
		{
			if ((twtActPattern[ii][jj] != 1) && (twtActPattern[ii][jj - 1] == 1))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii, jj - 1);  // destRow, destCol, srcRow, srcCol
				twtActPattern[ii][jj] = 2;  // indicate that the location is in the first ring of slaves
				break;
			}
		}
	}
	for(jj = 0; jj < APERTURE_GRID_SIZEROW; jj++)
	{
		for(ii = 0; ii < (APERTURE_GRID_SIZECOL - 1); ii++)
		{
			if ((twtActPattern[ii][jj] != 1) && (twtActPattern[ii + 1][jj] == 1))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii + 1, jj);  // destRow, destCol, srcRow, srcCol
				twtActPattern[ii][jj] = 2;  // indicate that the location is in the first ring of slaves
				break;
			}
		}
	}
	for( jj = APERTURE_GRID_SIZEROW - 1; jj >= 0; jj--)
	{
		for( ii = APERTURE_GRID_SIZECOL - 1; ii >= 1; ii--)
		{
			if ((twtActPattern[ii][jj] != 1) && (twtActPattern[ii - 1][jj] == 1))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii - 1, jj);  // destRow, destCol, srcRow, srcCol
				twtActPattern[ii][jj] = 2;  // indicate that the location is in the first ring of slaves
				break;
			}
		}
	}

//	find the second ring of tweeter slave actuators -- this MUST come after the first ring
	for(ii = 0; ii < APERTURE_GRID_SIZECOL; ii++)
	{
		for(jj = 0; jj < (APERTURE_GRID_SIZEROW - 1); jj++)
		{
			if ((twtActPattern[ii][jj] == 0) && (twtActPattern[ii][jj + 1] != 0))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii, jj + 1);  // destRow, destCol, srcRow, srcCol
				break;
			}
		}
	}
	for( ii = APERTURE_GRID_SIZECOL - 1; ii >= 0; ii--)
	{
		for( jj = APERTURE_GRID_SIZEROW - 1; jj >= 1; jj--)
		{
			if ((twtActPattern[ii][jj] == 0) && (twtActPattern[ii][jj - 1] != 0))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii, jj - 1);  // destRow, destCol, srcRow, srcCol
				break;
			}
		}
	}
	for(jj = 0; jj < APERTURE_GRID_SIZEROW; jj++)
	{
		for(ii = 0; ii < (APERTURE_GRID_SIZECOL - 1); ii++)
		{
			if ((twtActPattern[ii][jj] == 0) && (twtActPattern[ii + 1][jj] != 0))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii + 1, jj);  // destRow, destCol, srcRow, srcCol
				break;
			}
		}
	}
	for( jj = APERTURE_GRID_SIZEROW - 1; jj >= 0; jj--)
	{
		for( ii = APERTURE_GRID_SIZECOL - 1; ii >= 1; ii--)
		{
			if ((twtActPattern[ii][jj] == 0) && (twtActPattern[ii - 1][jj] != 0))
			{
				gpAoHrtRecon_InsertTwtSlave(ii, jj, ii - 1, jj);  // destRow, destCol, srcRow, srcCol
				break;
			}
		}
	}

//XXXdwpBadActSlaves -- start -- for testing bad actuator slaving
//	move the bad actuators slaving into tweeterSlaves[][], compensating for IDL -- this MUST come first
	for( ii = 0; ii < BAD_ACTS_SLVS; ii++ )
	{
		tweeterSlaves[ii][eTwtSlaveDestRow] = NUM_SUBAPS_ROWS_PER_FRAME - 1 - twtSlavesBadActs_IDL[ii][eTwtSlaveDestCol];
		tweeterSlaves[ii][eTwtSlaveDestCol] = twtSlavesBadActs_IDL[ii][eTwtSlaveDestRow];
		tweeterSlaves[ii][eTwtSlaveDestNdx] = tweeterSlaves[ii][eTwtSlaveDestRow] * numSubapCols + tweeterSlaves[ii][eTwtSlaveDestCol];

		tweeterSlaves[ii][eTwtSlaveSrc1Row] = NUM_SUBAPS_ROWS_PER_FRAME - 1 - twtSlavesBadActs_IDL[ii][eTwtSlaveSrc1Col];
		tweeterSlaves[ii][eTwtSlaveSrc1Col] = twtSlavesBadActs_IDL[ii][eTwtSlaveSrc1Row];
		tweeterSlaves[ii][eTwtSlaveSrc1Ndx] = tweeterSlaves[ii][eTwtSlaveSrc1Row] * numSubapCols + tweeterSlaves[ii][eTwtSlaveSrc1Col];

		if (twtSlavesBadActs_IDL[ii][eTwtSlaveSrc2Row] != -1)
		{
			tweeterSlaves[ii][eTwtSlaveSrc2Row] = NUM_SUBAPS_ROWS_PER_FRAME - 1 - twtSlavesBadActs_IDL[ii][eTwtSlaveSrc2Col];
			tweeterSlaves[ii][eTwtSlaveSrc2Col] = twtSlavesBadActs_IDL[ii][eTwtSlaveSrc2Row];
			tweeterSlaves[ii][eTwtSlaveSrc2Ndx] = tweeterSlaves[ii][eTwtSlaveSrc2Row] * numSubapCols + tweeterSlaves[ii][eTwtSlaveSrc2Col];
		}

		if (twtSlavesBadActs_IDL[ii][eTwtSlaveSrc3Row] != -1)
		{
			tweeterSlaves[ii][eTwtSlaveSrc3Row] = NUM_SUBAPS_ROWS_PER_FRAME - 1 - twtSlavesBadActs_IDL[ii][eTwtSlaveSrc3Col];
			tweeterSlaves[ii][eTwtSlaveSrc3Col] = twtSlavesBadActs_IDL[ii][eTwtSlaveSrc3Row];
			tweeterSlaves[ii][eTwtSlaveSrc3Ndx] = tweeterSlaves[ii][eTwtSlaveSrc3Row] * numSubapCols + tweeterSlaves[ii][eTwtSlaveSrc3Col];
		}
	}

//	move slave rings into tweeterSlaves[][] -- this MUST come second
	ii = 0;
	while (tweeterSlavesTmp[ii][eTwtSlaveDestRow] != -1)
	{
		for (jj = 0; jj < eTwtSlaveMaxFields; jj++)
			tweeterSlaves[BAD_ACTS_SLVS + ii][jj] = tweeterSlavesTmp[ii][jj];
		ii++;
	}
//XXXdwpBadActSlaves -- end -- for testing bad actuator slaving
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_InsertTwtSlave(int destRow, int destCol, int srcRow, int srcCol)
 *
 *	@brief
 *	Helper junction to insert tweeter slave actuators into the tweeter slave actuator list.  
 *
 *	@b DESCRIPTION:
 *	Helper junction to insert tweeter slave actuators into the tweeter slave actuator list.  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_InsertTwtSlave(destRow, destCol, srcRow, srcCol);
 * 
 *	@param[in]  destRow			(int):		slave actuator destination row
 *	@param[in]  destCol			(int):		slave actuator destination column
 *	@param[in]  srcRow			(int):		slave actuator source row
 *	@param[in]  srcRow			(int):		slave actuator source column
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_InsertTwtSlave(int destRow, int destCol, int srcRow, int srcCol)
{
	static const int numSubapCols = NUM_PIX_IN_ROW / NUM_CAM_COLS_PER_SUBAP;
	int i;

//	make sure neither actuator is out of bounds and bail if one is
	if ((destRow < 0) || (destRow >= APERTURE_GRID_SIZEROW) ||
		(destCol < 0) || (destCol >= APERTURE_GRID_SIZECOL) ||
		(srcRow  < 0) || (srcRow  >= APERTURE_GRID_SIZEROW) ||
		(srcCol  < 0) || (srcCol  >= APERTURE_GRID_SIZECOL))
	{
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_InsertTwtSlave) actuator out of bounds");
		return;
	}

//	look for the destination actuator in the list
	i = 0;
	while ((i < (APERTURE_GRID_AREA - 1)) &&  // "APERTURE_GRID_AREA - 1" is to make sure a terminator stays on the list
		   ((tweeterSlavesTmp[i][eTwtSlaveDestRow] != destRow) || (tweeterSlavesTmp[i][eTwtSlaveDestCol] != destCol)) && 
		   (tweeterSlavesTmp[i][eTwtSlaveDestRow] != -1))
		i++;

//	insert the source actuator as appropriate
	if (i >= (APERTURE_GRID_AREA - 1))
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_InsertTwtSlave) tweeter slave list is full");
	else if (tweeterSlavesTmp[i][eTwtSlaveDestRow] == -1)  // destination actuator not in list yet, so add it and the first source
	{
		tweeterSlavesTmp[i][eTwtSlaveDestRow] = destRow;
		tweeterSlavesTmp[i][eTwtSlaveDestCol] = destCol;
		tweeterSlavesTmp[i][eTwtSlaveDestNdx] = destRow * numSubapCols + destCol;

		tweeterSlavesTmp[i][eTwtSlaveSrc1Row] = srcRow;
		tweeterSlavesTmp[i][eTwtSlaveSrc1Col] = srcCol;
		tweeterSlavesTmp[i][eTwtSlaveSrc1Ndx] = srcRow * numSubapCols + srcCol;
	}
	else if (tweeterSlavesTmp[i][eTwtSlaveSrc2Row] != -1)
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_InsertTwtSlave) too many source actuators for destination");
	else    // destination actuator is in list, so add second source
	{
		tweeterSlavesTmp[i][eTwtSlaveSrc2Row] = srcRow;
		tweeterSlavesTmp[i][eTwtSlaveSrc2Col] = srcCol;
		tweeterSlavesTmp[i][eTwtSlaveSrc2Ndx] = srcRow * numSubapCols + srcCol;
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_ReconFTR( float xyCents[], ReconBuffType*  reconBufPtr,  ReconBuffType* lastReconBufPtr  )
 *
 *	@brief
 *	Perform the Fourier Transform Reconstruction (FTR).  
 *
 *	@b DESCRIPTION:
 *	Perform the Fourier Transform Reconstruction (FTR).  
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_ReconFTR( (float*) xyCents, (ReconBuffType*) reconBufPtr, (ReconBuffType*) lastReconBufPtr  );
 * 
 *	@param[in]  xyCents			(float[]):		Current X centroids followed by current Y centroids
 *	@param[out] reconBufPtr		(ReconBuffType*):	Current recon data pointer
 *	@param[out] lastReconBufPtr (ReconBuffType*):	Previous recon data pointer
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_ReconFTR( float xyCents[], ReconBuffType*  reconBufPtr,  ReconBuffType* lastReconBufPtr  )
{
	short	ii;
	short	halfNumThreads = (FTR_NUM_THREADS / 2);
#if ( TIMER_TEST != 0 )
	int		tmpTimerNdx;
#endif
#if ( RECON_DEBUG_DUMP != 0 )
	long	dimSize[10];
#endif

	//	Cache the current input data pointer for access by the worker threads
	ReconAlgFTR_xCentsCurrent = &(xyCents[0]);
	ReconAlgFTR_yCentsCurrent = &(xyCents[ MAX_NUM_SUBAPS ]);
	ReconAlgFTR_ReconBuf = reconBufPtr;
	LastReconAlgFTR_ReconBuf = lastReconBufPtr;

#if ( TIMER_TEST != 0 )
	tmpTimerNdx = reconFrameID % NUM_TIME_SETS;
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRStart]);
#endif
	
	//	perform Sums on the rows and cols in parallel (on both x and y data) - -  this is used by the pupil extension function
	for( ii=0; ii<FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartTT_Sums;

	for( ii=0; ii<FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
		while( ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) { }
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterSums]);
#endif
	
	//	Combine the tip/tilt data
	ReconAlgFTR_ttOffsetY = ReconAlgFTR_ttOffsetX = 0.0;
	for( ii=0; ii<halfNumThreads; ii++)
	{
		ReconAlgFTR_ttOffsetX += ReconAlgFTR_ttOffsetAccX[ii];	// combine all the tip/tilt sums from all worker threads -- these 
		ReconAlgFTR_ttOffsetY += ReconAlgFTR_ttOffsetAccY[ii];	// > have already been weighted by ReconAlgFTR_tipTiltVector[]
	}

	#if ( RECON_DEBUG_DUMP != 0 )
		dimSize[0] = APERTURE_GRID_AREA;
		dimSize[1] = 2;
		writeCompleteFitsFile( "cent_before_extend.fits", dimSize, 2, TFLOAT, xyCents );
	#endif

//	call gpAoHrtTipTilt_Process() to apply the tip/tilt control law and, if we're in closed loop, output voltages to 
//	> the TT stage and the woofer DM (tip/tilt is split between those 2 devices because the TT stage is quite slow)
	gpAoHrtTipTilt_Process( ReconAlgFTR_ttOffsetX, ReconAlgFTR_ttOffsetY, reconBufPtr, lastReconBufPtr );
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterTT]);
#endif

	//	extend the rows and columns (done in the main thread here)
	gpAoHrtRecon_FTRAlgoExtendPupil(xyCents);

	#if ( RECON_DEBUG_DUMP != 0 )
		dimSize[0] = APERTURE_GRID_SIZEROW;
		dimSize[1] = APERTURE_GRID_SIZECOL;
		writeCompleteFitsFile( "cent_ext_x.fits", dimSize, 2, TFLOAT, ReconAlgFTR_ExtendXD );
		writeCompleteFitsFile( "cent_ext_y.fits", dimSize, 2, TFLOAT, ReconAlgFTR_ExtendYD );
	#endif

#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterExtend]);
#endif

	#if ( RECON_DEBUG != 0 )
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of X-Data Extended Pupil Aperture ");
		dumpCornerOfMatrix( ReconAlgFTR_ExtendXD, APERTURE_GRID_SIZEROW, 0, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of Y-Data Extended Pupil Aperture ");
		dumpCornerOfMatrix( ReconAlgFTR_ExtendYD, APERTURE_GRID_SIZEROW, 0, TFLOAT );
	#endif

	//	perform 1d row ffts in parallel (on both x and y data)
	for( ii=0; ii<FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartFFTRows;
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
		while( ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterRowFFT]);
#endif

	#if ( RECON_DEBUG != 0 )
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump x data after FFT ROWS ");
		dumpCornerOfMatrix( ReconAlgFTR_xCT, APERTURE_GRID_SIZECOL*2, 1, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump y data after FFT ROWS ");
		dumpCornerOfMatrix( ReconAlgFTR_yCT, APERTURE_GRID_SIZECOL*2, 1, TFLOAT );
	#endif

	#if ( RECON_DEBUG_DUMP != 0 )
		dimSize[0] = APERTURE_GRID_SIZEROW*2;
		dimSize[1] = APERTURE_GRID_SIZECOL;
		writeCompleteFitsFile( "fft_out_raw_x_before_reconFilter_rows.fits", dimSize, 2, TFLOAT, ReconAlgFTR_xCT );
		writeCompleteFitsFile( "fft_out_raw_y_before_reconFilter_rows.fits", dimSize, 2, TFLOAT, ReconAlgFTR_yCT );
	#endif

//	perform 1d column ffts in parallel to complete the 2d ffts (on both x and y data)
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartFFTCols;
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
		while( ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterColFFT]);
#endif

	#if ( RECON_DEBUG != 0 )
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump x data after FFT COLS ");
		dumpCornerOfMatrix( ReconAlgFTR_xCT, APERTURE_GRID_SIZECOL*2, 1, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump y data after FFT COLS ");
		dumpCornerOfMatrix( ReconAlgFTR_yCT, APERTURE_GRID_SIZECOL*2, 1, TFLOAT );
	#endif

	#if ( RECON_DEBUG_DUMP != 0 )
		dimSize[0] = APERTURE_GRID_SIZEROW*2;
		dimSize[1] = APERTURE_GRID_SIZECOL;
		writeCompleteFitsFile( "fft_out_raw_x_before_reconFilter.fits", dimSize, 2, TFLOAT, ReconAlgFTR_xCT );
		writeCompleteFitsFile( "fft_out_raw_y_before_reconFilter.fits", dimSize, 2, TFLOAT, ReconAlgFTR_yCT );
	#endif

//	apply Fourier filter to rows in parallel (using both x and y data and producing combined phase data)
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartReconFilter;
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
		while( ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterFilter]);
#endif

	#if ( RECON_DEBUG != 0 )
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of filter X");
		dumpCornerOfMatrix( ReconAlgFTR_filterX, APERTURE_GRID_SIZEROW*2, 1, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of filter Y");
		dumpCornerOfMatrix( ReconAlgFTR_filterY, APERTURE_GRID_SIZEROW*2, 1, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of filternDenominator");
		dumpCornerOfMatrix( ReconAlgFTR_filterDenom, APERTURE_GRID_SIZEROW, 0, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of filter Numerator");
		dumpCornerOfMatrix( ReconAlgFTR_filterNumerator, APERTURE_GRID_SIZEROW*2, 1, TFLOAT );
		HRT_PRINTF( "\n\nDEBUG (gpAoHrtRecon_ReconFTR) Dump of data after the RECON-FILTER");
		dumpCornerOfMatrix( ReconAlgFTR_ReconBuf->ftrFFTModesData, APERTURE_GRID_SIZEROW*2, 1, TFLOAT );
	#endif

	#if ( RECON_DEBUG_DUMP != 0 )
		dimSize[0] = APERTURE_GRID_SIZEROW*2;
		dimSize[1] = APERTURE_GRID_SIZECOL;
		writeCompleteFitsFile( "fft_data_after_reconFilter.fits", dimSize, 2, TFLOAT, ReconAlgFTR_ReconBuf->ftrFFTModesData );
	#endif

//	apply optimizer gains
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_StartReconApplyOptGains;
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)  // wait for all of the threads to finish
		while( ReconAlgFTRThreadStat[ii] != eReconAlgFTRTh_Wait) {}
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerFTRAfterIntegrate]);
#endif

	return;
}

/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_FTRAlgoThread( void *threadNdx )
 *	@fn gpAoHrtRecon_FTRAlgoProcess( short thNdx, char threadCmnd )
 *
 *	@brief
 *  FTR sub-thread implementation
 *
 *	@b DESCRIPTION:
 *	These units are the threads to perform several functions in parallel:
 *	1. extends 16 rows of the pupil (NOTE that this will operate on either x or y data; but, 
 *		should only be used with x)
 *	2. extends 16 columns of the pupil (NOTE that this will operate on either x or y data; but, 
 *		should only be used with y)
 *	3. 1d ffts on 16 rows
 *	4. 1d ffts on 16 columns
 *	5. apply Fourier filter to 8 rows (doesn't have to be done to columns)
 *	6. 1d iffts on 8 columns
 *	7. 1d iffts on 8 rows
 *	
 *	The idea is to have the threads process:
 *	a. extend the rows and columns in parallel (on x and y data respectively)
 *	b. perform 1d row ffts in parallel (on both x and y data)
 *	c. perform 1d column ffts in parallel to complete the 2d ffts (on both x and y data)
 *	d. apply Fourier filter to rows in parallel (using both x and y data and producing combined phase data)
 *	e. perform 1d column iffts in parallel (on the phase data)
 *	f. perform 1d row iffts in parallel to complete the 2d iffts (on the phase data)
 *	
 *	Regarding pupil extension, this is done to try to compensate for zero-padding around the pupil.  
 *	At the moment, it simply appends half the negated sum of each row and column to just outside the 
 *	active area of the corresponding row and column, in the x and y data, respectively.  It seems to 
 *	me that this will work with data that is zero-mean; but the farther the data gets from zero-mean, 
 *	the less it will work.  For data with piston, tip, and tilt removed, this may not matter.  
 *	
 *	IMPORTANT - this unit assumes that the pupil does not start or end on the first or last
 *	row or column; but, it DOES NOT check!
 *	@par
 *	
 *	@b Usage: gpAoHrtRecon_FTRAlgoThread( &threadNdx )
 *	@b Usage: gpAoHrtRecon_FTRAlgoProcess( thNdx, threadCmnd )
 * 
 *	@param[in]  threadNdx	(void*):	Thread index (the ID for the thread)
 *	@param[in]  thNdx		(short):	Thread index
 *	@param[in]  threadCmnd	(char):		Thread command to be proecessed
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void *gpAoHrtRecon_FTRAlgoThread( void *threadNdx )
{
#if (RTL_SYSTEM == 0)
	int	  ii;
#else
	int	  myCPU, printCntr = 0;
#endif
	char  threadCmnd = '\000';
	short thNdx = *(short *)threadNdx;

	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon_FTRAlgoThread) Thread started: ", thNdx);

#if (RTL_SYSTEM == 0)
	cpu_set_t mask;
	int cpuNum = HRT_CPU_RECON_THREADS_START + thNdx;
	CPU_ZERO( &mask );
	CPU_SET( cpuNum, &mask);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon_FTRAlgoThread) CPU #",  cpuNum );
	if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoThread) sched_setaffinity() for CPU #", cpuNum);
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon_FTRAlgoThread) Recon Worker thread #", thNdx );
#else
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon_FTRAlgoThread) Recon worker thread id: ", rtl_pthread_self());
#endif

	while( ReconAlgFTRThreadStat[thNdx] != eReconAlgFTRTh_Quit )
	{
#if (RTL_SYSTEM != 0)
//		check for the right CPU
		myCPU = rtl_getcpuid();
		if (myCPU != (HRT_CPU_RECON_THREADS_START + thNdx))
		{
			if (printCntr < 10)
			{
				printCntr++;
				SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoThread) wrong CPU for thread: ", thNdx );
			}
		}
#endif

		// Wait until a command is received or quit is requested
		while( ReconAlgFTRThreadStat[thNdx] == eReconAlgFTRTh_Wait )  // spin on threadStat
#if (RTL_SYSTEM == 0)
			;
#else
//		needed to add this rtl_nanosleep() so RTL watchdog wouldn't time out
		{
			int retVal;
			struct rtl_timespec next;
			next.tv_sec = 0;
			next.tv_nsec = 5000;  // also tried 1000 and 10000
			if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
				SysConSendStrngIntParm(eDebugLevelWarn, "WARN: nanosleep() in gpAoHrtSysCon() 2, retVal = ", retVal);
		}
#endif

#if (RTL_SYSTEM == 0)
		for( ii=0; ii<CPU_SETSIZE; ii++)
		{
			if( ii == cpuNum )
			{
				if( !CPU_ISSET( cpuNum, &mask) )
					SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoThread) wrong CPU for threadNdx ", thNdx );
			}
			else 
			{
				if( CPU_ISSET( ii, &mask) )
					SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoThread) extra CPU for threadNdx ", thNdx );
			}
		}
#endif
 
		// If a command is received then process it
		if( ReconAlgFTRThreadStat[thNdx] == eReconAlgFTRTh_Quit)
			break;

		// save the command, since we're about to overwrite it
		threadCmnd = ReconAlgFTRThreadStat[thNdx];
		ReconAlgFTRThreadStat[thNdx] = eReconAlgFTRTh_InProg;

		if( (FTR_START_THREAD <= thNdx) && (FTR_LIMIT_THREAD > thNdx) )
		{
			// attempt to execute FTR algorithm
			if( gpAoHrtRecon_FTRAlgoProcess( thNdx, threadCmnd ) == 1)
			{
				ReconAlgFTRThreadStat[thNdx] = eReconAlgFTRTh_Wait;
				continue;
			}
		}
		
		if( (FTR_START_THREAD <= thNdx) && (FTR_LIMIT_THREAD > thNdx) )
		{
			// attempt to execute parse
			if( gpAoHrtParse_FTRAlgoProcess( thNdx, threadCmnd ) == 1)
			{
				ReconAlgFTRThreadStat[thNdx] = eReconAlgFTRTh_Wait;
				continue;
			}
		}

		if( (OPT_START_THREAD <= thNdx) && (OPT_LIMIT_THREAD > thNdx) )
		{
			// attempt to execute optimization, if any
			if( gpAoHrtReconOpt_FTRAlgoProcess( thNdx, threadCmnd ) == 1)
			{
				ReconAlgFTRThreadStat[thNdx] = eReconAlgFTRTh_Wait;
				continue;
			}
		}
	}

	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtRecon_FTRAlgoThread) quitting worker threadNdx ", thNdx);
	return NULL;
}


static short gpAoHrtRecon_FTRAlgoProcess(  short thNdx, char threadCmnd )
{
	short			tmpNdx, tmpNdx_t;
	short			ii, jj, kk;
	short			startCnt, endCnt;
	float			tmpSumForExtend, tmpSumForTT;
	float			xTmpR, xTmpI, yTmpR, yTmpI, pTmpR, pTmpI;
	float			ac, bd, ad, bc;
	fft_complex_type	*rowsC, *colsC;
	short			commandComplete = 1;

	switch ( threadCmnd )
	{

//	********************************************
//	**  Perform pre-computation for tip/tilt  **
//	**  estimation and pupil extension        **
//	********************************************
	case eReconAlgFTRTh_StartTT_Sums:
		// Columns accumulations on y data are done in thread 0 to (FTR_NUM_THREADS/2 - 1)
		if( thNdx < FTR_NUM_THREADS/2)
		{
			int myThNdx = thNdx;
			startCnt = ( myThNdx ) * ReconAlgFTR_RowsPerThread * 2;
			endCnt = startCnt + ReconAlgFTR_RowsPerThread * 2;

			ReconAlgFTR_ttOffsetAccY[myThNdx] = 0;

			for( jj=startCnt; jj<endCnt; jj++ )
			{
				tmpSumForExtend = 0.0;
				tmpSumForTT = 0.0;

				for( ii=0; ii<APERTURE_GRID_SIZECOL; ii++ )
				{
					if( hrtFileDataShmPtr->aperturePattern[ii][jj] != 0)
					{
						tmpSumForExtend += ReconAlgFTR_yCentsCurrent[ ii * APERTURE_GRID_SIZEROW + jj ];
						tmpSumForTT += ReconAlgFTR_yCentsCurrent[ ii * APERTURE_GRID_SIZEROW + jj ] *
									   ReconAlgFTR_tipTiltVector[ ii * APERTURE_GRID_SIZEROW + jj ];
					}
				}
				ReconAlgFTR_ttSumY[jj] = tmpSumForExtend;
				ReconAlgFTR_ttOffsetAccY[myThNdx] += tmpSumForTT;
			}
		}
		// Row accumulations on x data are done in thread (FTR_NUM_THREADS/2) to (FTR_NUM_THREADS - 1)
		else
		{
			int myThNdx = thNdx - FTR_NUM_THREADS/2;
			startCnt = ( myThNdx ) * ReconAlgFTR_RowsPerThread * 2;
			endCnt = startCnt + ReconAlgFTR_RowsPerThread * 2;

			ReconAlgFTR_ttOffsetAccX[myThNdx] = 0;

			for( ii=startCnt; ii<endCnt; ii++ )
			{
				tmpSumForExtend = 0.0;
				tmpSumForTT = 0.0;
				
				for( jj=0; jj<APERTURE_GRID_SIZEROW; jj++ )
				{
					if( hrtFileDataShmPtr->aperturePattern[ ii ][ jj ] != 0)
					{
						tmpSumForExtend += ReconAlgFTR_xCentsCurrent[ jj + ii * APERTURE_GRID_SIZEROW ];
						tmpSumForTT += ReconAlgFTR_xCentsCurrent[ jj + ii * APERTURE_GRID_SIZEROW ] * 
									   ReconAlgFTR_tipTiltVector[ jj + ii * APERTURE_GRID_SIZEROW ];
					}
				}
				ReconAlgFTR_ttSumX[ii] = tmpSumForExtend;
				ReconAlgFTR_ttOffsetAccX[myThNdx] += tmpSumForTT;
			}
		}
		return commandComplete;

//	********************************************
//	**  Perform FFT on the rows               **
//	**  Input is 2D pupil extended tip/tilt   **
//	**  centroid data                         **
//	********************************************
	case eReconAlgFTRTh_StartFFTRows:
//		rows, real to complex
#if ( STANDALONE_FFT == 0 )
		fftwf_execute(ReconAlgFTR_fftPlanRows[thNdx]);
#else
		fft_r2cf_48(&ReconAlgFTR_rowStartsF[thNdx][0],     // first even element in float input array
					&ReconAlgFTR_rowStartsF[thNdx][1],     // first odd element in float input array
					&ReconAlgFTR_rowStartsC[thNdx][0][0],  // first real element in complex output array
					&ReconAlgFTR_rowStartsC[thNdx][0][1],  // first imaginary element in complex output array
					ReconAlgFTR_RowsPerThread * 2);
#endif

//		mirror complex conjugate onto right half
		rowsC = ReconAlgFTR_rowStartsC[thNdx];
		for( ii = 0; ii < ReconAlgFTR_RowsPerThread * 2; ii++)
		{
			kk = APERTURE_GRID_SIZECOL / 2 - 1;
			for( jj = (APERTURE_GRID_SIZECOL / 2 + 1); jj < APERTURE_GRID_SIZECOL; jj++)
			{
				rowsC[ii * APERTURE_GRID_SIZECOL + jj][0] = rowsC[ii * APERTURE_GRID_SIZECOL + kk][0];
				rowsC[ii * APERTURE_GRID_SIZECOL + jj][1] = -rowsC[ii * APERTURE_GRID_SIZECOL + kk][1];
				kk--;
			}
		}

//		transpose matrices
		rowsC = ReconAlgFTR_rowStartsC[thNdx];
		colsC = ReconAlgFTR_colStartsCT[thNdx];
		for( jj = 0; jj < APERTURE_GRID_SIZECOL; jj++)
		{
			for( ii = 0; ii < ReconAlgFTR_RowsPerThread * 2; ii++)
			{
				colsC[jj * APERTURE_GRID_SIZEROW + ii][0] = rowsC[ii * APERTURE_GRID_SIZECOL + jj][0];
				colsC[jj * APERTURE_GRID_SIZEROW + ii][1] = rowsC[ii * APERTURE_GRID_SIZECOL + jj][1];
			}
		}
		return commandComplete;

//	********************************************
//	**  Perform FFT on the columns            **
//	**  Input is 2D transposed row FFT data   **
//	********************************************
	case eReconAlgFTRTh_StartFFTCols:

//		columns, complex to complex
#if ( STANDALONE_FFT == 0 )
		fftwf_execute(ReconAlgFTR_fftPlanCols[thNdx]);
#else
		fft_c2c_48(&ReconAlgFTR_rowStartsCT[thNdx][0][0],  // first real element in complex input array
				   &ReconAlgFTR_rowStartsCT[thNdx][0][1],  // first imaginary element in complex input array
				   &ReconAlgFTR_rowStartsCT[thNdx][0][0],  // first real element in complex output array
				   &ReconAlgFTR_rowStartsCT[thNdx][0][1],  // first imaginary element in complex output array
				   ReconAlgFTR_ColsPerThread * 2);
#endif
		return commandComplete;

//	********************************************
//	**  Perform the reconstruction filter     **
//	********************************************
	case eReconAlgFTRTh_StartReconFilter:
		for( ii=0; ii<(APERTURE_GRID_SIZECOL * ReconAlgFTR_RowsPerThread); ii++)  //note that filter values were transposed when they were read in
		{
			tmpNdx = thNdx * APERTURE_GRID_SIZECOL * ReconAlgFTR_RowsPerThread + ii;  //init this outside the loop and increment it at end

//			to multiply  (a + ib) by conj(c + id) => (a + ib) * (c - id) :
//				R = ac + bd
//				I = bc - ad
//
//			multiply X by its filter values (so:	a = ReconAlgFTR_xCT[][0], b = ReconAlgFTR_xCT[][1], 
//			>>										c = ReconAlgFTR_filterX[][0], and d = ReconAlgFTR_filterX[][1])
			ac = ReconAlgFTR_xCT[tmpNdx][0] * ReconAlgFTR_filterX[tmpNdx][0];
			bd = ReconAlgFTR_xCT[tmpNdx][1] * ReconAlgFTR_filterX[tmpNdx][1];
			ad = ReconAlgFTR_xCT[tmpNdx][0] * ReconAlgFTR_filterX[tmpNdx][1];
			bc = ReconAlgFTR_xCT[tmpNdx][1] * ReconAlgFTR_filterX[tmpNdx][0];

			xTmpR = ac + bd;
			xTmpI = bc - ad;

//			multiply Y by its filter values (so, a = ReconAlgFTR_yCT[][0], b = ReconAlgFTR_yCT[][1],
//			>>									 c = ReconAlgFTR_filterY[][0], and d = ReconAlgFTR_filterY[][1])
			ac = ReconAlgFTR_yCT[tmpNdx][0] * ReconAlgFTR_filterY[tmpNdx][0];
			bd = ReconAlgFTR_yCT[tmpNdx][1] * ReconAlgFTR_filterY[tmpNdx][1];
			ad = ReconAlgFTR_yCT[tmpNdx][0] * ReconAlgFTR_filterY[tmpNdx][1];
			bc = ReconAlgFTR_yCT[tmpNdx][1] * ReconAlgFTR_filterY[tmpNdx][0];

			yTmpR = ac + bd;
			yTmpI = bc - ad;

//			add y and x
			pTmpR = xTmpR + yTmpR;
			pTmpI = xTmpI + yTmpI;

			// + pTmpI * ReconAlgFTR_filterNumerator[tmpNdx][1]); // multiply by the pre-computed inverse instead of dividing
			ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][0] = (pTmpR * ReconAlgFTR_filterNumerator[tmpNdx][0] );

			// - pTmpR * ReconAlgFTR_filterNumerator[tmpNdx][1]); // multiply by the pre-computed inverse instead of dividing
			ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][1] = (pTmpI * ReconAlgFTR_filterNumerator[tmpNdx][0] );
			
		} // for loop
		return commandComplete;

//	****************************
//	**  Apply optimizer gains **
//	****************************
	case eReconAlgFTRTh_StartReconApplyOptGains:

		if( (hrt_AoTwtLoopStat == eAOLoopOpen) && (hrt_AoWfrLoopStat == eAOLoopOpen) && (hrt_optimizer_OFC_Stat == eAOLoopOpening ) )
			hrt_optimizer_OFC_Stat = eAOLoopOpen;

		for( ii=0; ii<(APERTURE_GRID_SIZECOL * ReconAlgFTR_RowsPerThread); ii++)  //note that filter values were transposed when they were read in
		{
			tmpNdx = thNdx * APERTURE_GRID_SIZECOL * ReconAlgFTR_RowsPerThread + ii;
			tmpNdx_t = (tmpNdx % APERTURE_GRID_SIZECOL) * APERTURE_GRID_SIZECOL +  (int)(tmpNdx / APERTURE_GRID_SIZECOL) ;

			if( hrt_twtCoeffUsed_Array[tmpNdx_t] )
			{
				switch( hrt_AoTwtLoopStat )
				{
					case eAOLoopOpen:
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] = 
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][0];
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] = 
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][1];
						break;

					case eAOLoopClosing:
					case eAOLoopAlmostClosed:	
					case eAOLoopClosed:	
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] =
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][0] * -hrt_workingModalGains[tmpNdx];  //XXXdwp110531 -- made negative
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] =
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][1] * -hrt_workingModalGains[tmpNdx];  //XXXdwp110531 -- made negative
						break;

					case eAOLoopOpening:
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] = 
							LastReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0];
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] = 
							LastReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1];
						break;

					default:
						SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_FTRAlgoProcess) bad hrt_AoTwtLoopStat = ", hrt_AoTwtLoopStat );
				}
			}
			else
			{
				switch( hrt_AoWfrLoopStat )
				{
					case eAOLoopOpen:
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] = 
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][0];
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] = 
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][1];
						break;

					case eAOLoopClosing:	
					case eAOLoopAlmostClosed:	
					case eAOLoopClosed:	
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] =
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][0] * -hrt_workingModalGains[tmpNdx];  //XXXdwp110531 -- made negative
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] =
							ReconAlgFTR_ReconBuf->ftrFFTModesData[tmpNdx][1] * -hrt_workingModalGains[tmpNdx];  //XXXdwp110531 -- made negative
						break;

					case eAOLoopOpening:
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0] = 
							LastReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][0];
						ReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1] = 
							LastReconAlgFTR_ReconBuf->ftrFFTModesDataOptimized[tmpNdx][1];
						break;

					default:
						SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_FTRAlgoProcess) bad hrt_AoWfrLoopStat = ", hrt_AoWfrLoopStat );
						break;
				}
			}
		} // for loop
		return commandComplete;
	}
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn static void gpAoHrtRecon_FTRAlgoExtendPupil(  float xyCents[] )
 *
 *	@brief
 *	Extend the pupil.
 *
 *	@b DESCRIPTION:
 *	This unit extends the pupil to try to compensate for zero-padding around the pupil.  At the 
 *	moment, it simply appends half the negated sum of each row and column to just outside the active
 *	area of the corresponding row and column, in the x and y data, respectively.  
 *	
 *	It seems to me that this will work with data that is zero-mean, on average; but the farther
 *	the data gets from zero-mean, the less it will work.  For data with piston, tip, and tilt
 *	removed, this may not matter.  
 *	
 *	IMPORTANT - this unit assumes that the pupil does not start or end on the first or last
 *	row or column; but, it DOES NOT check!
 *  
 *  Function stores the results in local global variable
 *	@par
 *
 *	@b Usage: gpAoHrtRecon_FTRAlgoExtendPupil( (float[]) xyCents[] );
 * 
 *	@param[in]  xyCents[] (float[]) : 		Current X centroids followed by current Y centroids
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void gpAoHrtRecon_FTRAlgoExtendPupil( float xyCents[] )
{
	short  ii, jj, ndx;
	float  xExtend, yExtend;
	float* pPupI;

//	extend the x's (i.e., the input rows)
	for( ii=0; ii<APERTURE_GRID_SIZEROW; ii++)
	{
		if (pupRowStarts[ii] > 0) // row isn't empty
		{
			pPupI = &(hrtFileDataShmPtr->aperturePattern[ii][0]);		// Point at start of current active row in aperture
			ndx = ii * APERTURE_GRID_SIZEROW;							// walking down by rows

			for( jj=0; jj<pupRowStarts[ii]-1;jj++ )
				ReconAlgFTR_ExtendXD[ ndx + jj ] = 0.0;
			
			for( jj=pupRowEnds[ii]+1; jj<APERTURE_GRID_SIZEROW;jj++ )
				ReconAlgFTR_ExtendXD[ ndx + jj ] = 0.0;

			for( jj=pupRowStarts[ii]; jj<=pupRowEnds[ii]; jj++)			// active area for this row
			{
				// Tip/Tilt Correction of the centroid data (data is now stored in 2D)
				if( pPupI[jj] != 0)  //test for the 'zero' center area
					ReconAlgFTR_ExtendXD[ ndx + jj ] = xyCents[ ndx + jj ] - ReconAlgFTR_ttOffsetX;
			}

			xExtend = - (ReconAlgFTR_ttSumX[ii] - ReconAlgFTR_ttOffsetX * ReconAlgFTR_ttSumCntX[ii]) * 0.5;
			ReconAlgFTR_ExtendXD[ ndx + (pupRowStarts[ii] - 1)] = xExtend;
			ReconAlgFTR_ExtendXD[ ndx + (pupRowEnds[ii] + 1)] = xExtend;
		}
	}

//	extend the y's (i.e., the input columns)
	for( ii=0; ii<APERTURE_GRID_SIZECOL; ii++)
	{
		if (pupColStarts[ii] > 0) // column isn't empty
		{
			pPupI = &(hrtFileDataShmPtr->aperturePattern[0][ii]);		// Point at start of current active column in aperture

			for( jj=0; jj<pupColStarts[ii]-1; jj++ )
			{
				ndx = jj * APERTURE_GRID_SIZEROW;						// walking down by rows
				ReconAlgFTR_ExtendYD[ ndx + ii ] = 0.0;
			}
			
			for( jj=pupColEnds[ii]+1; jj<APERTURE_GRID_SIZECOL; jj++ )
			{
				ndx = jj * APERTURE_GRID_SIZEROW;						// walking down by rows
				ReconAlgFTR_ExtendYD[ ndx + ii ] = 0.0;
			}

			for( jj=pupColStarts[ii]; jj<=pupColEnds[ii]; jj++)			// active area for this column
			{
				ndx = jj * APERTURE_GRID_SIZEROW;						// walking down by rows

//				Tip/Tilt Correction of the centroid data (data is now stored in 2D)
				if( pPupI[ ndx ] != 0)  //test for the 'zero' center area
					ReconAlgFTR_ExtendYD[ ndx + ii] = xyCents[ ndx + ii + MAX_NUM_SUBAPS] - ReconAlgFTR_ttOffsetY;
			}
			yExtend = - (ReconAlgFTR_ttSumY[ ii ]  - ReconAlgFTR_ttOffsetY * ReconAlgFTR_ttSumCntY[ii]) * 0.5;
			ReconAlgFTR_ExtendYD[ (pupColStarts[ ii ] - 1) * APERTURE_GRID_SIZEROW + ii] = yExtend;
			ReconAlgFTR_ExtendYD[(pupColEnds[ ii ] + 1) * APERTURE_GRID_SIZEROW + ii] = yExtend;
		}
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_FTRAlgoInit( )
 *
 *	@brief
 *  Initialize variables used by the Recon FTR threads.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the FTR reconstrution algorithm.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_FTRAlgoInit( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_FTRAlgoInit( void )
{
	short ii,jj;
	int   halfNumThreads;

//	zero out the ReconAlgFTR_ExtendXD and ReconAlgFTR_ExtendXD arrays (used to store tip/tilt corrected extended centroid data)
//	Non-aperture points are not written so nooed to be redoing this in main line
	for(ii=0; ii<APERTURE_GRID_AREA; ii++)
		ReconAlgFTR_ExtendXD[ii] = ReconAlgFTR_ExtendYD[ii] = 0.0;

	//IMPORTANT: NUM_THREADS MUST be evenly divisible by 2
	if( (FTR_NUM_THREADS % 2) != 0 )
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoInit) FTR_NUM_THREADS must be divisible by two");

//	initialize configuration parameters
	halfNumThreads = FTR_NUM_THREADS / 2;

//	initialize the worker thread data pointers for the x centroid data
	for( ii = 0; ii < halfNumThreads; ii++)
	{
		ReconAlgFTR_rowStartsF[ii] = &ReconAlgFTR_ExtendXD[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];

		ReconAlgFTR_rowStartsC[ii] = &ReconAlgFTR_xC[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];
		ReconAlgFTR_colStartsC[ii] = &ReconAlgFTR_xC[ii * ReconAlgFTR_RowsPerThread * 2];
		ReconAlgFTR_rowStartsCT[ii] = &ReconAlgFTR_xCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];
		ReconAlgFTR_colStartsCT[ii] = &ReconAlgFTR_xCT[ii * ReconAlgFTR_RowsPerThread * 2];
	}

//	initialize the worker thread data pointers for the y centroid data
	for( ii = 0; ii < halfNumThreads; ii++)
	{
		ReconAlgFTR_rowStartsF[ii + halfNumThreads] = &ReconAlgFTR_ExtendYD[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];

		ReconAlgFTR_rowStartsC[ii + halfNumThreads] = &ReconAlgFTR_yC[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];
		ReconAlgFTR_colStartsC[ii + halfNumThreads] = &ReconAlgFTR_yC[ii * ReconAlgFTR_ColsPerThread * 2];
		ReconAlgFTR_rowStartsCT[ii + halfNumThreads] = &ReconAlgFTR_yCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL];
		ReconAlgFTR_colStartsCT[ii + halfNumThreads] = &ReconAlgFTR_yCT[ii * ReconAlgFTR_ColsPerThread * 2];
	}

//	pre-compute the inverse of the filter denominator AND apply the FFT normalization factor 
	for( ii = 0; ii < APERTURE_GRID_SIZEROW; ii++)
	{
		for( jj = 0; jj < APERTURE_GRID_SIZECOL; jj++)
		{
			ReconAlgFTR_filterNumerator[jj * APERTURE_GRID_SIZECOL + ii][0] = 1.0 / ( APERTURE_GRID_AREA * ReconAlgFTR_filterDenom[ jj * APERTURE_GRID_SIZECOL + ii] );
			ReconAlgFTR_filterNumerator[jj * APERTURE_GRID_SIZECOL + ii][1] = 0.0;
		}
	}
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_InitThreading( )
 *
 *	@brief
 *  Initialize worker threads used by the Recon FTR algorithm.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to start worker threads used by the FTR reconstrution algorithm.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_InitThreading( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
void gpAoHrtRecon_InitThreading( void )
{
	short				   ii;
	rtl_pthread_attr_t	   attr;
    struct rtl_sched_param param;

//	create threads to do computations
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtRecon_InitThreading) initializing threading");
	for( ii=0; ii<NUM_TOTAL_ALG_THREADS; ii++)
	{
		rtl_pthread_attr_init(&attr);
		rtl_pthread_attr_getschedparam(&attr, &param);
	    param.sched_priority += 15;
		rtl_pthread_attr_setschedparam(&attr, &param);
#if ( RTL_SYSTEM != 0 )
		rtl_pthread_attr_setfp_np(&attr, 1);
		if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_RECON_THREADS_START + ii) != 0 )
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtRecon_InitThreading) CPU Assignment failed for CPU #", HRT_CPU_RECON_THREADS_START + ii );
//NOTE that this isn't working:		rtl_pthread_attr_setreserve_np(&attr, 1);
#else
	pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
		ReconAlgFTRThreads_processNum[ ii ] = ii;
		ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_Wait;
		if( rtl_pthread_create( &ReconAlgFTRThreads[ii], &attr, gpAoHrtRecon_FTRAlgoThread, &(ReconAlgFTRThreads_processNum[ii]) ) != 0)	  // kick off thread
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtRecon_InitThreading) thread create failed for thread: ", ii );
	 	rtl_pthread_attr_destroy(&attr);  // don't need the attribute structure any more
	}
}

/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_CleanThreading( )
 *
 *	@brief
 *  Clean up the FTR threads data.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to gracefully terminate the threads and de-allocate memory.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_CleanThreading( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *		None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtRecon_CleanThreading( void )
{
	short ii;

//	wait a millisec
	usleep(1000);  // can't use rtl_nanosleep() in user-space

//	tell all  worker threads to quit
	for( ii = 0; ii < NUM_TOTAL_ALG_THREADS; ii++)
		ReconAlgFTRThreadStat[ii] = eReconAlgFTRTh_Quit;

//	wait a millisec
	usleep(1000);  // can't use rtl_nanosleep() in user-space
}

/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_FTRAlgoInitMemory( )
 *
 *	@brief
 *  Allocate memory used by the Recon FTR algorithm.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to allocate memory used by the FTR reconstrution algorithm.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_FTRAlgoInitMemory( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_FTRAlgoInitMemory( void )
{
#if ( STANDALONE_FFT == 0 )
	short	ii;

//	IMPORTANT: FTR_NUM_THREADS MUST be evenly divisible by 2
	if( (FTR_NUM_THREADS % 2) != 0 )
	{
		SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtRecon_FTRAlgoInitMemory) FTR_NUM_THREADS must be divisible by two");
		return;
	}
	short halfNumThreads = FTR_NUM_THREADS / 2;

	if (rtl_sem_wait(&FftwPlanSem) < 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCent) waiting on FftwPlanSem, errno = %d", rtl_errno);

//	allocate fftw memory and measure plans for the 3 ffts that have to be performed
    ReconAlgFTR_ExtendXD	= (fft_data_type*)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_data_type));
    ReconAlgFTR_xC			= (fft_complex_type *)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type));
    ReconAlgFTR_xCT			= (fft_complex_type *)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type));

    ReconAlgFTR_ExtendYD	= (fft_data_type*)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_data_type));
    ReconAlgFTR_yC			= (fft_complex_type *)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type));
    ReconAlgFTR_yCT			= (fft_complex_type *)fftwf_malloc( APERTURE_GRID_AREA * sizeof(fft_complex_type));

//	initialize the FFT plans for the x centroid data
	for( ii = 0; ii < halfNumThreads; ii++)
	{
	    ReconAlgFTR_fftPlanRows[ii] = fftwf_plan_many_dft_r2c( 1, &fft_n_col, ReconAlgFTR_RowsPerThread * 2, 
			 				&ReconAlgFTR_ExtendXD[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							&ReconAlgFTR_xC[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							FFTW_PATIENT );
	    ReconAlgFTR_fftPlanCols[ii] = fftwf_plan_many_dft( 1, &fft_n_row, ReconAlgFTR_ColsPerThread * 2, 
							&ReconAlgFTR_xCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							&ReconAlgFTR_xCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							FFTW_FORWARD, FFTW_PATIENT );
	}

//	initialize the FFT plans for the y centroid data
	for( ii = 0; ii < halfNumThreads; ii++)
	{
	    ReconAlgFTR_fftPlanRows[ii + halfNumThreads] = fftwf_plan_many_dft_r2c( 1, &fft_n_col, ReconAlgFTR_RowsPerThread * 2, 
			  				&ReconAlgFTR_ExtendYD[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							&ReconAlgFTR_yC[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							FFTW_PATIENT );
	    ReconAlgFTR_fftPlanCols[ii + halfNumThreads] = fftwf_plan_many_dft( 1, &fft_n_row, ReconAlgFTR_ColsPerThread * 2, 
							&ReconAlgFTR_yCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							&ReconAlgFTR_yCT[ii * ReconAlgFTR_RowsPerThread * 2 * APERTURE_GRID_SIZECOL], NULL, 1, APERTURE_GRID_SIZECOL,
							FFTW_FORWARD, FFTW_PATIENT );
	}

	rtl_sem_post(&FftwPlanSem);
#endif
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtRecon_FTRAlgoCleanMemory( )
 *
 *	@brief
 *  Clean up the FTR recon memory.
 *
 *	@b DESCRIPTION:
 *  The purpose of this unit is to de-allocate FTR recon memory.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtRecon_FTRAlgoCleanMemory( void );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtRecon_FTRAlgoCleanMemory( void )
{
#if ( STANDALONE_FFT == 0 )
	short	ii;

//	destroy fftw plans
	for( ii = 0; ii < FTR_NUM_THREADS; ii++)
	{
		fftwf_destroy_plan(ReconAlgFTR_fftPlanRows[ii]);
		fftwf_destroy_plan(ReconAlgFTR_fftPlanCols[ii]);
	}

//	free fftw memory
	fftwf_free(ReconAlgFTR_ExtendXD); 
	fftwf_free(ReconAlgFTR_xC); 
	fftwf_free(ReconAlgFTR_xCT); 
	fftwf_free(ReconAlgFTR_ExtendYD); 
	fftwf_free(ReconAlgFTR_yC); 
	fftwf_free(ReconAlgFTR_yCT); 
#endif
}


#if ( RECON_DEBUG != 0 )
/*!
 *******************************************************************************
 *	@fn dumpCornerOfaMatrix( )
 *
 *	@brief
 *  Dump corner of a 2D matrix
 *
 *	@b DESCRIPTION:
 * 		This currently only supports 2D type matrices ( will work on 2D if step size is set to 6 )
 *		Only supports FLOATS and LONGS  ( may or may not be complex - based on input flag )
 *	@par
 *	
 *
 *	@b Usage: dumpCornerOfaMatrix( void *pIn, int rowSize, int complexFlag, int dType );
 * 
 *	\@param[in] None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static int dumpCornerOfMatrix( void *pIn, int rowSize, int complexFlag, int dType )
{
	int ii, jj;

	if( dType == TDOUBLE )
	{
		if( complexFlag != 0)
		{
			HRT_PRINTF( "INFO - complex matrix corner dump at pointer: %p", pIn );
			for( jj=0; jj<10; jj++)
			{
				for( ii=0; (ii<10) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " (%9.5g, i%9.5g)  ", *((double*)pIn + jj*rowSize + ii*2), *((double*)pIn + jj*rowSize + ii*2 + 1) );

				double tmpSum_r = 0.0;
				double tmpSum_i = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum_r +=  *((double*)pIn + jj*rowSize + ii*2);
					tmpSum_i +=  *((double*)pIn + jj*rowSize + ii*2 + 1);
				}

				HRT_PRINTF_NOCRLF(" :  Sum real/img = ( %7.3g, %7.3g )", tmpSum_r, tmpSum_i );
			}
			HRT_PRINTF_NOCRLF(" ");
		}
		else
		{
			HRT_PRINTF( " INFO - matrix corner dump at pointer: %p", pIn );
			for( jj=0; jj<12; jj++)
			{
				for( ii=0; (ii<12) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " %9.5g, ", *( (double*)pIn+ jj*rowSize + ii ) );

				double tmpSum = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum +=  *((double*)pIn + jj*rowSize + ii );
				}
	
				HRT_PRINTF_NOCRLF(" Sum=%9.5g", tmpSum );
			}
			HRT_PRINTF_NOCRLF(" ");
		}
	}

	if( dType == TFLOAT )
	{
		if( complexFlag != 0)
		{
			HRT_PRINTF( "INFO - complex matrix corner dump at pointer: %p", pIn );
			for( jj=0; jj<10; jj++)
			{
				for( ii=0; (ii<10) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " (%7.3f, ii%7.3f)  ", *((float*)pIn + jj*rowSize + ii*2), *((float*)pIn + jj*rowSize + ii*2 + 1) );

				float tmpSum_r = 0.0;
				float tmpSum_i = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum_r +=  *((float*)pIn + jj*rowSize + ii*2);
					tmpSum_i +=  *((float*)pIn + jj*rowSize + ii*2 + 1);
				}

				HRT_PRINTF_NOCRLF(" :  Sum real/img = ( %7.3f, %7.3f )", tmpSum_r, tmpSum_i );
			}
			HRT_PRINTF_NOCRLF(" ");
		}
		else
		{
			HRT_PRINTF( " INFO - matrix corner dump at pointer: %p", pIn );
			for( jj=0; jj<12; jj++)
			{
				for( ii=0; (ii<12) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( "  %7.3f,  ", *( (float*)pIn+ jj*rowSize + ii ) );

				float tmpSum = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum +=  *((float*)pIn + jj*rowSize + ii );
				}
	
				HRT_PRINTF_NOCRLF(" Sum=%7.3f", tmpSum );
			}
			HRT_PRINTF_NOCRLF(" ");
		}
	}

	if( dType == TLONG )
	{
		if( complexFlag != 0)
		{
			HRT_PRINTF( "INFO - complex (long) matrix corner dump at pointer: %p", pIn );
			for( jj=0; jj<6; jj++)
			{
				for( ii=0; (ii<6) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " ( %ld, ii%ld  ", *((long*)pIn + jj*rowSize + ii * 2 ) , *((long*)pIn + jj*rowSize + ii * 2 + 1 )  );
				HRT_PRINTF_NOCRLF(" ");
			}
			HRT_PRINTF_NOCRLF(" ");
		}
		else
		{
			HRT_PRINTF( "INFO - matrix (long) corner dump at pointer: %p", pIn );
			for( jj=0; jj< 11; jj++)
			{
				for( ii=0; (ii<11) && (ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( "  %ld,  ", *( (long*)pIn+ jj*rowSize + ii ) );
				HRT_PRINTF_NOCRLF(" ");
			}
			HRT_PRINTF_NOCRLF(" ");
		}
	}
	return 0;
}
#endif


#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 ) )
/*!
 *******************************************************************************
 *
 *	@fn	static void writeCompleteFitsFile( )
 *
 *	@brief
 *	Writes a FITS file from a general array
 *
 *	@b DESCRIPTION:
 *
 *	@par
 *	
 *	@b Usage: writeCompleteFitsFile( char *filename, long* dimSize, int numDim, int dataType, void *data)
 * 
 *	\@param[ in ]	filename ( char *):	name ( and the directory) of the FITS file to write	 
 *					data ( void *):		points to an array which to be written; must be height by width array
 *					dimSize ( long[] ):	the length of one dimension of the 2D array 
 *					numDim ( int):		the length of another dimension of the 2D array
 *					dataType ( int):	data type, using cfitsio data type ( only TBYTE, TFLOAT, fornow)
 *
 *	\@param[ out ] a FITS file named "filename"
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void writeCompleteFitsFile( char *filename, long* dimSize, int numDim, int dataType, void *data)
{
	fitsfile*	fptr;
	char		fitsFilename[ _POSIX_PATH_MAX ];	// POSIX max path length = 256 bytes
	int			status = 0;
	long		dataAxes[ 4 ];						// will handle up to 4 dimensions
	long		fpixel[ 4 ];						// will handle up to 4 dimensions
	int			ii;
	long long	size = 1;

//	make sure the filename begins with '!' so fitsio will create the file and then open the file
	if( filename[ 0 ] == '!') 
		strcpy( fitsFilename, filename);
	else
	{
		strcpy( fitsFilename, "!");
		strcat( fitsFilename, filename);
	}
	fits_create_file( &fptr, fitsFilename, &status);

	for( ii=0; ii<4; ii++)
	{
		dataAxes[ ii ] = 0;
		fpixel[ ii ] = 1;
	}
	
//	create an image, write header keywords, and write out the data
	size = 1L;
	for( ii=0; ii<numDim; ii++)
	{
		dataAxes[ ii ] = dimSize[ ii ];
		size *= dimSize[ii];
	}

	switch ( dataType)
	{
		case TBYTE:
			fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status);
			break;
		case TDOUBLE:
			fits_create_img( fptr, DOUBLE_IMG, numDim, dataAxes, &status);
			break;
		case TLONG:
			fits_create_img( fptr, LONG_IMG, numDim, dataAxes, &status);
			break;
		case TSHORT:
			fits_create_img( fptr, SHORT_IMG, numDim, dataAxes, &status);
			break;
		case TFLOAT:
			fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status);
			break;
		default:
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (recon:writeCompleteFitsFile) this type not allowed: ", dataType );
			break;
	}

	fits_write_date( fptr, &status);
	fits_write_comment( fptr, "GPI algProto test data", &status);

	fits_write_pix( fptr, dataType, fpixel, size, data, &status);

	//	close the file
	fits_close_file( fptr, &status);

}	// end of writeCompleteFitsFile( )
#endif


#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 )  )
/*!
 *******************************************************************************
 *
 *	@fn	static void openFitsForWrite( )
 *
 *	@brief
 *	Open a FITS file, for later write,  for a general array - this is used when you want to write a fits file over time.
 *
 *	@b DESCRIPTION:
 *
 *	@par
 *	
 *	@b Usage: openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType)
 * 
 *	\@param[ in ]	filename ( char *):	name ( and the directory) of the FITS file to write	 
 *					dimSize ( long[] ):	the length of one dimension of the 2D array 
 *					numDim ( int):		the length of another dimension of the 2D array
 *					dataType ( int):	data type, using cfitsio data type ( only TBYTE, TFLOAT, fornow)
 *
 *	\@param[ out ] a FITS file named "filename"
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static 	fitsfile* openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType )
{
	fitsfile*	fptr = NULL;
	char		fitsFilename[ _POSIX_PATH_MAX ];	// POSIX max path length = 256 bytes
	int			status = 0;
	long		dataAxes[ 4 ];						// will handle up to 4 dimensions
	int			ii;
	char		tmpBuf[1024];

	last_fptr = NULL;		// reset so error not done when new file is opened

//	make sure the filename begins with '!' so fitsio will create the file and then open the file
	if( filename[ 0 ] == '!') 
		strcpy( fitsFilename, filename);
	else
	{
		strcpy( fitsFilename, "!");
		strcat( fitsFilename, filename);
	}

	if( fits_create_file( &fptr, fitsFilename, &status) != 0)
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3108): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, "  - Creating File " );
		SysConSendStrngNoParm(eDebugLevelErr, tmpBuf );
		HRT_PRINTF( tmpBuf );
		return NULL;
	}

//	create an image, write header keywords, and write out the data
	for( ii=0; ii<numDim; ii++)
		dataAxes[ ii ] = dimSize[ ii ];

	switch ( dataType)
	{
		case TBYTE:
			if( fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status) != 0)
			{
				strcpy( tmpBuf, "ERR (writeFitsBlock:3122): " );
				fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
				strcat( tmpBuf, " - Creating BYTE Image " );
				SysConSendStrngNoParm(eDebugLevelErr, tmpBuf );
				HRT_PRINTF( tmpBuf );
				return NULL;
			}
			break;

		case TFLOAT:
			if( fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status) != 0)
			{
				strcpy( tmpBuf, "ERR (writeFitsBlock:3133): " );
				fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
				strcat( tmpBuf, " - Creating FLOAT Image " );
				HRT_PRINTF( tmpBuf );
				SysConSendStrngNoParm( eDebugLevelErr, tmpBuf );
				return NULL;
			}
			break;

		default:
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (openFitsForWrite:3068): This 'type' not allowed (only allow BYTE and FLOAT currently)  :", dataType );
			break;
	}


	fits_write_date( fptr, &status);
	fits_write_comment( fptr, "GPI algProto test data", &status);

	if( status != 0 )
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3152): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, " - Status type: Comment " );
		HRT_PRINTF_ERR( tmpBuf );
		SysConSendStrngNoParm( eDebugLevelErr, tmpBuf );
		return NULL;
	}

	return fptr;
}
#endif

#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 ) )
// Write a block of data to the already open fits file - this expects _fpixel to have been set, so data is put into correct position
static void writeFitsBlock( fitsfile* fptr, long _size, int dataType, void* pData, long _fpixel[] )
{
	long		fpixel[ 4 ];						// will handle up to 4 dimensions
	int			status = 0;
	int			ii;
	char		tmpBuf[1024];
	long long	size;

	if( fptr == NULL )
			return;

	if( last_fptr != NULL && fptr != last_fptr )
	{
		HRT_PRINTF_ERR("*** Warning2: fits file ptr mismatch for RECON_DEBUG_DUMP" );
		last_fptr = fptr;
	}
	else
	{
		last_fptr = fptr;
	}


	size = _size;

	for( ii=0; ii<4; ii++)
		fpixel[ ii ] = _fpixel[ ii ];

	if( fits_write_pix( fptr, dataType, fpixel, size, pData, &status) != 0)
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3193): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, " - Writing data block " );
		SysConSendStrngNoParm(eDebugLevelErr, tmpBuf );
		HRT_PRINTF( tmpBuf );
		sprintf( tmpBuf, "         fptr = 0x%p, dataType=0x%x, fpixel[0]=0x%lx, fpixel[1]=0x%lx, size=0x%llx, pData=0x%p, status=0x%x ",   
														fptr, dataType, fpixel[0], fpixel[1],  size, pData, status);
		HRT_PRINTF( tmpBuf );

		return;
	}

	return;
}	// end of writeFitsBlock()
#endif

#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 )  )
// Close a previously opened fits file.  The file may or may not have been written to.
static void closeFits( fitsfile* fptr )
{
	int		status = 0;

	if( fptr == NULL )
			return;

	//	close the file
	fits_close_file( fptr, &status);

}	// end of closeFits()
#endif

#if ( ( RECON_DEBUG != 0 ) || ( RECON_DEBUG_DUMP != 0 )  )
static void flushFitsBuffer( fitsfile* fptr )
{
	int		status = 0;

	if( fptr == NULL )
			return;

	//	flush the buffer
	fits_flush_buffer( fptr, 0, &status);

}	// end of flushFitsBuffer()
#endif


/**********************************************************************************************
 
UNIT NAME:  rdtsc()
 
UNIT DESCRIPTION:
This unit executes the x86 RDTSC instruction.  
 
*** IMPORTANT: the reliability of results from RDTSC has decreased with multi-core       ***
*** processors - when testing with the uniprocessor version of fftw, this shouldn't be a ***
*** problem                                                                              ***
 
UNIT INPUTS:
    None
 
UNIT OUTPUTS:
    x - 64 bit value containing the number of CPU clock ticks since last reset
 
UNIT ERROR CODES:
     None
 
**********************************************************************************************/
 
//__inline__ unsigned long long int rdtsc()
//{
//    unsigned long long int x;
//    __asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));  // execute RDTSC instruction
//    return x;
//}
 
__inline__ unsigned long long rdtsc( )
{
  unsigned hi, lo;
  __asm__ __volatile__ ("rdtsc" : "=a"(lo), "=d"(hi));
  return ( (unsigned long long)lo)|( ((unsigned long long)hi)<<32 );
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
// vim:tabstop=4:softtabstop=4:shiftwidth=4
//**********************************************************************************************************
//
