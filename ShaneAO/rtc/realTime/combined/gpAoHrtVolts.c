/*******************************************************************************
 *    COPYRIGHT (C) 2008
 *    LAWRENCE LIVERMORE NATIONAL LABORATORY
 *    ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 ******************************************************************************/
/*!
 *    @file gpAoHrtVolts.c
 *
 *    @brief
 *    This component contains functions to interface to the SciMeasure WFS camera.
 *
 *    @b DESCRIPTION:
 *    This component is the Hard Real Time (Hrt) interface to the AO WFS camera.
 *    It performs the RT camera initialization, communicates with the SRT CamMngr
 *    module to issue commands (because the camera drivers are not strictly RTLinux
 *    compatible), and handles real-time processing for the camera.
 *
 *    @note The Cam() thread is included in this component.
 *
 *    @b AUTHOR:
 *    DWP & SMJ    DATE: 01/17/2008
 *
 *    @b FUNCTION NAMES:
 *    -# @c gpAoHrtVolt_Init()
 *    -# @c gpAoHrtVolt_ConvTweeter()
 *    -# @c gpAoHrtVolt_ConvWoofer()
 *    -# @c gpAoHrtVolt_ConvTT()
 *    -# @c gpAoHrtVolt_WriteTweeterVolts( )
 *    -# @c gpAoHrtVolt_WriteWooferVolts( )
 *    -# @c gpAoHrtVolt_WriteTTVolts( )
 *    -# @c gpAoHrtVolt_StartReg(  )
 *    -# @c gpAoHrtVolt_ProcessReg()
 *    -# @c gpAoHrtVolt_StopReg()
 *    -# @c gpAoHrtVolt_MirrorShapeStart()
 *    -# @c gpAoHrtVolt_MirrorShapeProcess()
 *    -# @c gpAoHrtVolt_MirrorShapeAbort()
 *
 *    @b REVISIONS:
 *
 ********************************************************************************/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <string.h>
#include <math.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <fcntl.h>
#include <sys/mman.h>
#endif

#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"

#if ( RTL_SYSTEM == 0 )
#include "gpAoSrtDpio2Drivers.h"
#else
#include "gpAoHrtDpio2Drivers.h"
#endif
#include "gpAoTwtTable.h"

// Defines
#define DNS_PER_VOLT_TWT   54.610  // digital numbers per volt for the tweeter (2^14 DNs / 300 volts)
#define DNS_PER_VOLT_WFR   81.919  // digital numbers per volt for the woofer  (2^16 DNs / 800 volts)
#define DNS_PER_VOLT_TT  1638.350  // digital numbers per volt for the T/T     (2^15 DNs /  20 volts)

// ** General Global Variables
int        hrt_dmSlavingSwitch;  // turns woofer and tweeter slaving on and off

// DM phase (nm) to volts - polynomial
float    hrt_twtPhaseToVoltsLookup[ MAX_TWEETER_ACTS ][ TWEETER_LOOKUP_BINS ];  // Calculated from the SRT COEFF
float    hrt_wfrPhaseToVoltsLookup[ MAX_WOOFER_ACTS ][ WOOFER_LOOKUP_BINS ];       // Calculated from the SRT COEFF
float    hrt_ttMasToVoltsLookup   [ MAX_TT_ACTS ][ TT_LOOKUP_BINS ];               // Calculated from the SRT COEFF

float    hrt_twtMinMicrons[ MAX_TWEETER_ACTS ];    // Calculated from the SRT COEFF
float    hrt_twtMaxMicrons[ MAX_TWEETER_ACTS ];    // Calculated from the SRT COEFF
float    hrt_wfrMinMicrons[ MAX_WOOFER_ACTS ];    // Calculated from the SRT COEFF
float    hrt_wfrMaxMicrons[ MAX_WOOFER_ACTS ];    // Calculated from the SRT COEFF
float    hrt_ttMinMAS[ MAX_TT_ACTS ];            // Calculated from the SRT COEFF
float    hrt_ttMaxMAS[ MAX_TT_ACTS ];            // Calculated from the SRT COEFF

float    hrt_twtMinVolts[ MAX_TWEETER_ACTS ];  // Calculated from the SRT COEFF
float    hrt_twtMaxVolts[ MAX_TWEETER_ACTS ];  // Calculated from the SRT COEFF
float    hrt_wfrMinVolts[ MAX_WOOFER_ACTS ];      // Calculated from the SRT COEFF
float    hrt_wfrMaxVolts[ MAX_WOOFER_ACTS ];      // Calculated from the SRT COEFF
float    hrt_ttMinVolts[ MAX_TT_ACTS ];          // Calculated from the SRT COEFF
float    hrt_ttMaxVolts[ MAX_TT_ACTS ];          // Calculated from the SRT COEFF

// Local Variables
//  Mirror Shape Averaging routine variables
static short mirrorShapeProcessCount;                        // counts sets of actuator data when running mirrorShapeProcess
static short mirrorShapeState;                                // current state of the mirrorShape processing
static short mirrorShapeDmShapeType;                        // Set the type of DM Shape that is to be stored ( currently one of 6)

static float mirrorShapeTwtPhaseSums[MAX_TWEETER_ACTS];    // summed actuator data for determining tweeter averages (phase Data)
static float mirrorShapeWfrPhaseSums[MAX_WOOFER_ACTS];    // summed actuator data for determining tweeter averages (phase Data)

// Registration (cycle actuators) - Actuator Move routines
static    char    regActiveTwtActs[MAX_TWEETER_ACTS];        // flags indicating which DM actuators are currently active for reg
static    char    regActiveWfrActs[MAX_WOOFER_ACTS];        // flags indicating which DM actuators are currently active for reg
static    char    regActiveTTActs[MAX_TT_ACTS];            // flags indicating which TT actuators are currently active for reg

static float    regFreq = 1.0;                            // frequency at which actuators are moved up and down
static float    regCntr;                                // counter to count frames per second for reg
static float    regIncrementor;                            // amount to increment regCntr by each time (+2 or -2)
static float    regFrameRate;                            // local frame rate for reg

static boolean    regQuitFlag = false;                    // indicates that the reg command should be quit
static boolean    regRoundRobin;                            // indicates that the reg command is in round-robin mode
static short    regRoundRobinNdx;                        // actuator being driven in round-robin mode reg command
static short    regRoundRobinFrameCntr;                    // counter for the amount of time each actuator is driven

static float    twtRegPhase[ MAX_TWEETER_ACTS ];
static float    wfrRegPhase[ MAX_WOOFER_ACTS ];

static float    twtLookupTableConstant[ MAX_TWEETER_ACTS ];
static float    wfrLookupTableConstant[ MAX_WOOFER_ACTS ];
static float    ttLookupTableConstant[ MAX_TT_ACTS ];

// Local Function Prototypes

/*!
 *******************************************************************************
 *    @fn gpAoHrtVolt_Init( void )
 *
 *    @brief
 *    Initialize variables used by the voltage conversion component.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to initialize the neccessary arrays to deal with Phase to
 *    voltage conversion for the Tweeter, Woofer, and T/T.
 *    @par
 *
 *    @b Usage: gpAoHrtVolt_Init( );
 *
 *    \@param[in] None
 *
 *    @return
 *        0: No Error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_Init( void )
{
int ii;

    regQuitFlag = false;
    regRoundRobin = false;

    // No value in the following tables should be zero -- so we're using zeroes
    // > to flag whether values have been calcuated
    for( ii=0; ii<MAX_TWEETER_ACTS; ii++ )
	twtLookupTableConstant[ ii ] = 0.0;
    for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
	wfrLookupTableConstant[ ii ] = 0.0;
    for( ii=0; ii<MAX_TT_ACTS; ii++ )
	ttLookupTableConstant[ ii ] = 0.0;

    return 0;
}

/*!
 *******************************************************************************
 *    @fn gpAoHrtVolt_ConvTweeter( float twtPhase[], float twtVolts[], float twtPhaseUnclipped[] )
 *
 *    @brief
 *    The purpose of this unit is to convert the twtPhase signals into voltage for output to DM.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to convert the tweeter phase to volts.  A lookup table is
 *    generated at startup and is used here to convert phase to volts.  All input phases must
 *    be in microns.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_ConvTweeter( twtPhase, twtVolts, twtPhaseUnclipped );
 *
 *    @param[in/out]    twtPhase[]            (float[]):    input array of tweeter phase in microns
 *                                                    output array of clipped tweeter phase
 *    @param[out]     twtVolts[]            (float[]):    output array of tweeter volts
 *    @param[out]        twtPhaseUnclipped[]    (float[]):    output array of unclipped tweeter phase
 *
 *    @return
 *        <0: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *         2: PolyFile not available (error)
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
static float twtPhaseTmp[MAX_TWEETER_ACTS];  // temporary tweeter phase data used for slaving

int
gpAoHrtVolt_ConvTweeter( float twtPhase[], float twtVolts[], float twtPhaseUnclipped[]  )
{
int     i, ii, ndx;
float   tmpPh;
int     retCode;
float   tmpFloat;
    //printf("  Start gpAoHrtVolt_ConvTweeter\n");

    if( hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff < 1 )
    {
	SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvTweeter) need the Twt Poly file" );
	return 2;
    }

    if( twtLookupTableConstant[ 0 ] == 0.0 )  // NOTE: this initialization is only done at startup
    {
	boolean flagError = false;
	for( ii = 0; ii < MAX_TWEETER_ACTS; ii++ )
	{
	    tmpFloat = hrt_twtMaxMicrons[ii] - hrt_twtMinMicrons[ii];
	    if (tmpFloat == 0.0)
	    {
		tmpFloat = 3.0;  // use default value to protect from divide by zero
		flagError = true;
	    }
	    twtLookupTableConstant[ ii ] = (float)(TWEETER_LOOKUP_BINS - 1) / tmpFloat;
	}
	if (flagError)
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvTweeter)   maxMicrons == minMicrons" );
    }

    //    convert from phase to volts
    retCode = 0;
    for( ii = 0; ii < MAX_TWEETER_ACTS; ii++ )
    {
	tmpPh = twtPhase[ ii ];
	if (twtPhaseUnclipped != NULL)
	    twtPhaseUnclipped[ ii ] = tmpPh;

	if ( tmpPh < hrt_twtMinMicrons[ii] )
	{
	    tmpPh = hrt_twtMinMicrons[ii];
	    twtPhase[ii] = tmpPh;
	    retCode--;
	}
	if ( tmpPh > hrt_twtMaxMicrons[ii] )
	{
	    tmpPh = hrt_twtMaxMicrons[ii];
	    twtPhase[ii] = tmpPh;
	    retCode--;
	}
	ndx = (int)(( tmpPh - hrt_twtMinMicrons[ ii ]) * twtLookupTableConstant[ ii ] );
	twtVolts[ ii ] = hrt_twtPhaseToVoltsLookup[ ii ][ ndx ];
    }

    //printf("  *End gpAoHrtVolt_ConvTweeter\n");
    return retCode;
}

/*!
 ********************************************************************************
 *    @fn gpAoHrtVolt_ConvWoofer( float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] )
 *
 *    @brief
 *    The purpose of this unit is to convert the wfrPhase signals into voltage for output to DM.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to convert the wfr phase to volts.  A lookup table is
 *    generated at startup and is used here to convert phase to volts.  All input phases
 *    must be in microns.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_ConvWoofer( wfrPhase, wfrVolts, wfrPhaseUnclipped );
 *
 *    @param[in/out]    wfrPhase[]            (float[]):    input array of woofer phase in microns
 *                                                    output array of clipped woofer phase
 *    @param[out]        wfrVolts[]            (float[]):    output array of woofer volts
 *    @param[out]        wfrPhaseUnclipped[]    (float[]):    output array of unclipped woofer phase
 *
 *    @return
 *        <0: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *         2: PolyFile not available (error)
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_ConvWoofer( float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] )
{
int            ii, ndx;
float        tmpPh;
int            retCode;
float        tmpFloat;

    if( hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff < 1 )
    {
	SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvWoofer) need the wfr Poly file" );
	return 2;
    }

    if( wfrLookupTableConstant[ 0 ] == 0.0 )  // NOTE: this initialization is only done at startup
    {
	boolean flagError = false;
	for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
	{
	    tmpFloat = hrt_wfrMaxMicrons[ii] - hrt_wfrMinMicrons[ii];
	    if (tmpFloat == 0.0)
	    {
		tmpFloat = 10.0;  // use default value to protect from divide by zero
		flagError = true;
	    }
	    wfrLookupTableConstant[ ii ] = (float)(WOOFER_LOOKUP_BINS-1) / tmpFloat;
	}
	if (flagError)
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvWoofer) maxMicrons == minMicrons" );
    }

    //    convert from phase to volts
    retCode = 0;
    for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
    {
	tmpPh = wfrPhase[ ii ];
	if (wfrPhaseUnclipped != NULL)
	    wfrPhaseUnclipped[ ii ] = tmpPh;

	if( tmpPh > hrt_wfrMinMicrons[ii] )  // IMPORTANT: this comparison seems backwards because woofer slopes are negative
	{
	    tmpPh = hrt_wfrMinMicrons[ii];
	    wfrPhase[ii] = tmpPh;
	    retCode--;
	}
	if( tmpPh < hrt_wfrMaxMicrons[ii] )  // IMPORTANT: this comparison seems backwards because woofer slopes are negative
	{
	    tmpPh = hrt_wfrMaxMicrons[ii];
	    wfrPhase[ii] = tmpPh;
	    retCode--;
	}
	ndx = (int)(( tmpPh - hrt_wfrMinMicrons[ii]) * wfrLookupTableConstant[ ii ] );
	wfrVolts[ ii ] = hrt_wfrPhaseToVoltsLookup[ ii ][ ndx ];
    }

    return retCode;
}

//XXXdwp110516 -- for testing only -- start -- so we can accept a 'temporary' woofer shape that has an offset, but
//XXXdwp110516 -- for testing only             > clip the actual woofer shape
/*!
 ********************************************************************************
 *    @fn gpAoHrtVolt_ConvWooferTst( float tmpWfrPhase[], float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] )
 *
 *    @brief
 *    The purpose of this unit is to convert the wfrPhase signals into voltage for output to DM.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to convert the wfr phase to volts.  A lookup table is
 *    generated at startup and is used here to convert phase to volts.  All input phases
 *    must be in microns.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_ConvWooferTst( tmpWfrPhase, wfrPhase, wfrVolts, wfrPhaseUnclipped );
 *
 *    @param[in]        tmpWfrPhase[]        (float[]):    'temporary' input array of woofer phase in microns
 *    @param[out]        wfrPhase[]            (float[]):    output array of clipped woofer phase
 *    @param[out]        wfrVolts[]            (float[]):    output array of woofer volts
 *    @param[out]        wfrPhaseUnclipped[]    (float[]):    output array of unclipped woofer phase
 *
 *    @return
 *        <0: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *         2: PolyFile not available (error)
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_ConvWooferTst( float tmpWfrPhase[], float wfrPhase[], float wfrVolts[], float wfrPhaseUnclipped[] )
{
int            ii, ndx;
float        tmpPh;
int            retCode;
float        tmpFloat;

    if( hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff < 1 )
    {
	SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvWooferTst) need the wfr Poly file" );
	return 2;
    }

    if( wfrLookupTableConstant[ 0 ] == 0.0 )  // NOTE: this initialization is only done at startup
    {
	boolean flagError = false;
	for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
	{
	    tmpFloat = hrt_wfrMaxMicrons[ii] - hrt_wfrMinMicrons[ii];
	    if (tmpFloat == 0.0)
	    {
		tmpFloat = 10.0;  // use default value to protect from divide by zero
		flagError = true;
	    }
	    wfrLookupTableConstant[ ii ] = (float)(WOOFER_LOOKUP_BINS-1) / tmpFloat;
	}
	if (flagError)
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvWoofer) maxMicrons == minMicrons" );
    }

    //    convert from phase to volts
    retCode = 0;
    for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
    {
	tmpPh = tmpWfrPhase[ ii ];
	if (wfrPhaseUnclipped != NULL)
	    wfrPhaseUnclipped[ ii ] = tmpPh;

	if( tmpPh > hrt_wfrMinMicrons[ii] )  // IMPORTANT: this comparison seems backwards because woofer slopes are negative
	{
	    wfrPhase[ii] -= tmpPh - hrt_wfrMinMicrons[ii];
	    tmpPh = hrt_wfrMinMicrons[ii];
	    retCode--;
	}
	if( tmpPh < hrt_wfrMaxMicrons[ii] )  // IMPORTANT: this comparison seems backwards because woofer slopes are negative
	{
	    wfrPhase[ii] += hrt_wfrMaxMicrons[ii] - tmpPh;
	    tmpPh = hrt_wfrMaxMicrons[ii];
	    retCode--;
	}
	ndx = (int)(( tmpPh - hrt_wfrMinMicrons[ii]) * wfrLookupTableConstant[ ii ] );
	wfrVolts[ ii ] = hrt_wfrPhaseToVoltsLookup[ ii ][ ndx ];
    }

    return retCode;
}
//XXXdwp110516 -- for testing only -- end

/*!
 ********************************************************************************
 *    @fn gpAoHrtVolt_ConvTT( float ttAngle[], float ttVolts[], float ttAngleUnclipped[] )
 *
 *    @brief
 *    The purpose of this unit is to convert the TT signals being output from TT angle to TT volts.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to convert the input angle to TT volts.  A lookup
 *    table is generated at startup and is used here to convert angle to volts.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_ConvTT(  ttAngle, ttVolts, ttAngleUnclipped );
 *
 *    @param[in/out]    ttAngle[]            (float[]):    input array of tt angles in 'mas'
 *                                                    output array of clipped tt angles
 *    @param[out]        tVolts[]            (float[]):    output array of tt volts
 *    @param[out]        ttAngleUnclipped[]    (float[]):    output array of unclipped tt angles
 *
 *    @return
 *        <1: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *         2: PolyFile not available (error)
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_ConvTT( float ttAngle[], float ttVolts[], float ttAngleUnclipped[] )
{
int            ii, ndx;
float        tmpAngle;
int            retCode;
float        tmpFloat;

    if( hrtFileDataShmPtr->numTT_Phase2VoltsCoeff < 1 )
    {
	SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvTT) need the TT Poly file" );
	return 2;
    }

    if( ttLookupTableConstant[ 0 ] == 0.0 )  // NOTE: this initialization is only done at startup
    {
	boolean flagError = false;
	for( ii=0; ii<MAX_TT_ACTS; ii++ )
	{
	    tmpFloat = hrt_ttMaxMAS[ii] - hrt_ttMinMAS[ii];
	    if (tmpFloat == 0.0)
	    {
		tmpFloat = 2000.0;  // use default value to protect from divide by zero
		flagError = true;
	    }
	    ttLookupTableConstant[ ii ] = (float)(TT_LOOKUP_BINS-1) / tmpFloat;
	}
	if (flagError)
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_ConvTT) maxMAS == minMAS" );
    }

    //    convert from angle to volts
    retCode = 0;
    for( ii = 0; ii < MAX_TT_ACTS; ii++ )
    {
	tmpAngle = ttAngle[ ii ];
	if (ttAngleUnclipped != NULL)
	    ttAngleUnclipped[ ii ] = tmpAngle;

	if( tmpAngle < hrt_ttMinMAS[ ii ] )
	{
	    tmpAngle = hrt_ttMinMAS[ ii ];
	    ttAngle[ ii ] = tmpAngle;
	    retCode--;
	}
	if( tmpAngle >  hrt_ttMaxMAS[ ii ] )
	{
	    tmpAngle = hrt_ttMaxMAS[ ii ];
	    ttAngle[ ii ] = tmpAngle;
	    retCode--;
	}
	ndx = (int)( ( tmpAngle - hrt_ttMinMAS[ ii ]) * ttLookupTableConstant[ ii ] );
	ttVolts[ ii ] = hrt_ttMasToVoltsLookup[ ii ][ ndx ];
    }

    return retCode;
}

/*!
 ********************************************************************************
 *    @fn gpAoHrtVolt_WriteTweeterVolts( float twtVolts[] )
 *
 *    @brief
 *    The purpose of this unit is to output voltages to the tweeter.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to output voltages to the tweeter.  The voltages are
 *    DMAed out to the Cambridge Innovations 1k DM drive electronics through a DPIO2
 *    parallel interface board,

 *    @par
 *    Note that voltages are range checked in this function.  However, the phase will
 *    already have been range checked.  So, the voltages should never be out of range.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_WriteTweeterVolts( twtvolts );
 *
 *    @param[in] twtVolts    (float[])    - volts for each actuator ('full' array only)
 *
 *    @return
 *        <0: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/

#define NUM_TWEETER_ROWS 32
#define NUM_TWEETER_COLS 32

#if ( TWT_TYPE == TWT_BMC_4K )

static const int numSubapRows = 8;
static const int numSubapCols = 8;

static const int numTweeterRows = NUM_TWEETER_ROWS;
static const int numTweeterCols = NUM_TWEETER_COLS;

static const int xOffset = 0;
static const int yOffset = 0;

static int gpAoTwtMapTmp[NUM_ACTUATORS_1024], gpAoTwtMap[NUM_TWEETER_ROWS][NUM_TWEETER_COLS];
#endif

int
gpAoHrtVolt_WriteTweeterVolts( float twtVolts[] )
{
    int    retCode = 0;

#if ( TWT_TYPE == TWT_BMC_4K )
    static int crntBuffNdx = 0;
    int           i, j;
    int           arrayNdx, tweeterNdx;  // at present (5/20/10), 'array' is 1D 48*48 and 'tweeter' is 1D 64*64

    static int firstTimeFlag = TRUE;

    static int Cntr = 0, CheckCntr = 0;

    // You don't want to close the loops with these printf() statements
    // active.
    // printf ("printing twtVolts[]\n");

    for( i = 0; i < NUM_TWEETER_ROWS; i++ )
    {
	// printf ("row %d: ", i);
	for( j = 0; j < NUM_TWEETER_COLS; j++ )
	{
	    arrayNdx = i * NUM_TWEETER_COLS + j;
	    // printf ("%f ", twtVolts[arrayNdx]);

	}
	// printf ("\n");
    }
    // printf ("\n");


    if ( ++CheckCntr >= TEST_INTERVAL * LegalFrameRates[hrt_camFrameRate] )
    {
	CheckCntr = 0;
	Cntr = 1;
    }

    //if(Cntr == 1)
    //    printf( " gpAoHrtVolt_WriteTweeterVolts - starting\n");

    if (firstTimeFlag == TRUE)
    {
	firstTimeFlag = FALSE;

	for( i = 0; i < NUM_TWEETER_ROWS; i++ )
	{
	    for( j = 0; j < NUM_TWEETER_COLS; j++ )
	    {
		gpAoTwtMap[j][i] = gpAoTwtTable[i * NUM_TWEETER_COLS + j];  // use this to transpose, rotate, flip, etc.

	    }
	}
    }

    // printf ("printing gpio2BufferTwt[][]\n");

    for( i = 0; i < NUM_TWEETER_ROWS; i++ )
    {
	// printf ("row %d: ", i);
	for( j = 0; j < NUM_TWEETER_COLS; j++ )
	{
	    arrayNdx = i * NUM_TWEETER_COLS + j;
	    tweeterNdx = gpAoTwtMap[yOffset + i][xOffset + j];
	    if ( twtVolts[arrayNdx] < hrt_twtMinVolts[arrayNdx] )
	    {
		dpio2BufferTWT[crntBuffNdx][tweeterNdx] = (unsigned short)(hrt_twtMinVolts[arrayNdx] * DNS_PER_VOLT_TWT);
		retCode--;
	    }
	    else if( twtVolts[arrayNdx] > hrt_twtMaxVolts[arrayNdx] )
	    {
		dpio2BufferTWT[crntBuffNdx][tweeterNdx] = (unsigned short)(hrt_twtMaxVolts[arrayNdx] * DNS_PER_VOLT_TWT);
		retCode--;
	    }
	    else
	    {
		dpio2BufferTWT[crntBuffNdx][tweeterNdx] = (unsigned short)(twtVolts[arrayNdx] * DNS_PER_VOLT_TWT);
	    }

	    dpio2BufferTWT[crntBuffNdx][tweeterNdx] = dpio2BufferTWT[crntBuffNdx][tweeterNdx] << 2;  // upper 14-bits are used
	    // printf ("%d ", dpio2BufferTWT[crntBuffNdx][tweeterNdx]);
	}
	// printf ("\n");
    }
    // printf ("\n");


    //if (Cntr == 1)
    //    printf(" (gpAoHrtVolt_WriteTweeterVolts) Starting Transfer\n");

    //    initiate the DMA transfer
#if ( RTL_SYSTEM == 0 )
    gpAoSrtDpio2StartDmaTransfer(dpio2DevnoTWT, 0, crntBuffNdx);  // errors are reported in the function; so we're not checking here
#else
    gpAoHrtDpio2StartDmaTransfer(dpio2DevnoTWT, 0, crntBuffNdx);  // errors are reported in the function; so we're not checking here
#endif

    crntBuffNdx ^= 0x1;  // switch buffers for next time
    Cntr = 0;

#else
    int ii;
    unsigned short    outputVoltsData[MAX_TWEETER_ACTS];

    for( ii=0; ii<MAX_TWEETER_ACTS; ii++)
    {
	if( twtVolts[ii] < hrt_twtMinVolts[ii] )
	{
	    outputVoltsData[ii] = (unsigned short)(hrt_twtMinVolts[ii] * DNS_PER_VOLT_TWT);
	    retCode--;
	}
	else if( twtVolts[ii] > hrt_twtMaxVolts[ii] )
	{
	    outputVoltsData[ii] = (unsigned short)(hrt_twtMaxVolts[ii] * DNS_PER_VOLT_TWT);
	    retCode--;
	}
	else
	{
	    outputVoltsData[ii] = (unsigned short)(twtVolts[ii] * DNS_PER_VOLT_TWT);
	}
    }
#endif

    return retCode;
}

/*!
 *******************************************************************************
 *    @fn gpAoHrtVolt_WriteWooferVolts( float wfrVolts[], boolean ttOnlyFlag )
 *
 *    @brief
 *    The purpose of this unit is to output voltages to the woofer.
 *
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to output voltages to the woofer/T/T.  The voltages
 *    are DMAed out to the Cambridge Innovations Cilas DM drive electronics through a DPIO2
 *    parallel interface board (there are 2 of these in the system, one for the tweeter
 *    and one for the woofer/T/T).
 *    @par
 *    Note that voltages are range checked in this function.  However, the phase will
 *    already have been range checked.  So, the voltages should never be out of range.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_WriteWooferVolts( wfrVolts );
 *
 *    @param[in] wfrVolts        (float[])    - woofer voltage array
 *    @param[in] ttOnlyFlag    (boolean)    - only update the tip/tilt values
 *
 *    @return
 *        true if the full requested move has been made
 *        false if the move had to be limited (see IMPORTANT: dV/dt, below)
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
#if ( WFR_TYPE == WFR_GPI_121 )
static const int maxWfrDNsStep = 2184;  // this is one-thirtieth of 65535 (i.e., 2^16, the woofer electronics full scale)  //XXXdwp110711

// this table contains the indexes into an 81 element array (a one-dimensional 9x9 array) for
// > the purpose of extracting data to be put into the Cambridge Innovations UINT16 DMA
// > packet -- see data81_InstertTo_CI_DMA_map[]
static const int data81_ExtractFor_CI_DMA_map[] = {  2,  3,  4,  5,  6, 10, 11, 12, 13, 14,
						    15, 16, 18, 19, 20, 21, 22, 23, 24, 25,
						    26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
						    36, 37, 38, 39, 40, 41, 42, 43, 44, 45,
						    46, 47, 48, 49, 50, 51, 52, 53, 54, 55,
						    56, 57, 58, 59, 60, 61, 62, 64, 65, 66,
						    67, 68, 69, 70, 74, 75, 76, 77, 78 };

// this table contains the indexes into the Cambridge Innovations UINT16 DMA packet for inserting data
// > from an 81 element array (a one-dimensional 9x9 array) -- see data81_ExtractFor_CI_DMA_map[]
static const int data81_InstertTo_CI_DMA_map[] = {  64, 53, 46, 32, 21, 78, 66, 55, 41, 34,
						    23, 11, 81, 73, 68, 56, 43, 36, 24, 13,
						    5,  83, 75, 70, 58, 45, 38, 26, 15,  7,
						    85, 77, 65, 60, 47, 33, 28, 16,  8, 87,
						    79, 67, 62, 48, 35, 30, 18, 10, 88, 80,
						    69, 57, 50, 37, 25, 20, 12, 82, 71, 59,
						    52, 39, 27, 22, 72, 61, 54, 40, 29 };

// these arrays provide a simple woofer slaving, setting an outer ring actuator to the value of its
// > single nearest neighbor -- these are indexes into the Cambridge Innovations UINT16 DMA packet --
// > so, current woofer actuator settings MUST be put into that array before these arrays are used
static const int simpleSlaveSource[] = {  5,  7,  8, 10, 11, 11, 12, 21, 22, 22, 29, 32, 40, 46,
					 53, 54, 61, 64, 72, 78, 78, 81, 82, 82, 83, 85, 87, 88, -1 };  // -1 is the terminator
static const int simpleSlaveDest[]   = {  0,  2,  4,  6,  3,  9,  1, 19, 14, 17, 31, 95, 42, 44,
					 51, 49, 63, 96, 74, 76, 86, 92, 84, 90, 94, 89, 91, 93, -1 };  // -1 is the terminator

// these are the indexes into the Cambridge Innovations UINT16 DMA packet for the tip and tilt data
static const int tiltNdxPlus  = 97;   // Cambridge Innovations channel 101
static const int tiltNdxMinus = 99;   // Cambridge Innovations channel 102
static const int tipNdxPlus   = 101;  // Cambridge Innovations channel 103 -- Note that these have replaced
static const int tipNdxMinus  = 103;  // Cambridge Innovations channel 104 -- > channels 99 and 100

// these are used for setting tip and tilt values
static const unsigned short ttDNLowerLimit  = 0x4000;
static const unsigned short ttDNMidRange    = 0x8000;
static const unsigned short ttDNUpperLimit  = 0xC000;

static unsigned short tipDNPlus   = 0x8000;  // ttDNMidRange;  // setting both minus and plus to 0x8000
static unsigned short tipDNMinus  = 0x8000;  // ttDNMidRange;  // > gives a net differential voltage of 0V
static unsigned short tiltDNPlus  = 0x8000;  // ttDNMidRange;  // setting both minus and plus to 0x8000
static unsigned short tiltDNMinus = 0x8000;  // ttDNMidRange;  // > gives a net differential voltage of 0V

static float ttOnlyWfrVolts[MAX_WOOFER_ACTS];  // used only if writing TT only the first time
#endif

boolean
gpAoHrtVolt_WriteWooferVolts( float wfrVolts[], boolean ttOnlyFlag )
{
boolean allDone = true;

#if ( WFR_TYPE == WFR_GPI_121 )
static unsigned short crntBufferWFR[sizeInBytesWFR / 2];
static int              crntBuffNdx = 0;
static int              prevBuffNdx = 1;
static boolean          firstTimeFlag = true;

int               i, tmpExtractNdx;
float           tmpWfrVolts;
int                  tmpWfrDNs, tmpWfrDNsStep, wfrStepCrct;
unsigned short tmpDpioVal;

    if (!ttOnlyFlag || firstTimeFlag)
    {
	// this is simply to ensure that if only the TT is written to first, that the woofer doesn't get
	// > jammed to its minimum setting (0 DNs)
	if (ttOnlyFlag && firstTimeFlag)
	{
	    wfrVolts = ttOnlyWfrVolts;
	    for( i = 0; i < NUM_ACTIVE_WFR_ACTS; i++ )
		wfrVolts[data81_ExtractFor_CI_DMA_map[i]] = 0.0;  // 0.0 volts (mid-range)
	}
	firstTimeFlag = false;

	for( i = 0; i < NUM_ACTIVE_WFR_ACTS; i++ )
	{
	    tmpExtractNdx = data81_ExtractFor_CI_DMA_map[i];
	    tmpWfrVolts = wfrVolts[tmpExtractNdx];

	    if( tmpWfrVolts < hrt_wfrMinVolts[tmpExtractNdx] )
		tmpWfrVolts = hrt_wfrMinVolts[tmpExtractNdx];
	    else if( tmpWfrVolts > hrt_wfrMaxVolts[tmpExtractNdx] )
		tmpWfrVolts = hrt_wfrMaxVolts[tmpExtractNdx];
	    tmpWfrVolts -= hrt_wfrMinVolts[tmpExtractNdx];  // this is to always give a plus DN, starting from 0
	    tmpWfrDNs = (int)(tmpWfrVolts * DNS_PER_VOLT_WFR);

	    //XXXdwp110711 -- start -- also tmpWfrDNs (which is new), maxWfrDNsStep, etc.
	    //     IMPORTANT: due to hardware limitations (the possibility of blowing a fuse on a driver board), the dV/dt
	    //     > is being limited to 1/30 of full-scale per time step (see softwareLimitsForGpiWoofer_fromMikeB.txt)
	    //     > NOTE that slaved actuators are not checked, because they simply follow their neighbors; if a more
	    //     > sophisticated approach is added, they might need to be explicitly checked
	    tmpWfrDNsStep = tmpWfrDNs - (int)dpio2BufferWFR[prevBuffNdx][data81_InstertTo_CI_DMA_map[i]];
	    if (tmpWfrDNsStep > maxWfrDNsStep)
	    {
		wfrStepCrct = tmpWfrDNsStep - maxWfrDNsStep;
		tmpWfrDNs -= wfrStepCrct;
		wfrVolts[tmpExtractNdx] = (float)tmpWfrDNs / DNS_PER_VOLT_WFR + hrt_wfrMinVolts[tmpExtractNdx];
		allDone = false;
	    }
	    else if (tmpWfrDNsStep < -maxWfrDNsStep)
	    {
		wfrStepCrct = -tmpWfrDNsStep - maxWfrDNsStep;
		tmpWfrDNs += wfrStepCrct;
		wfrVolts[tmpExtractNdx] = (float)tmpWfrDNs / DNS_PER_VOLT_WFR + hrt_wfrMinVolts[tmpExtractNdx];
		allDone = false;
	    }    //XXXdwp110711 -- end

	    tmpDpioVal = (unsigned short)tmpWfrDNs;
	    crntBufferWFR[data81_InstertTo_CI_DMA_map[i]] = tmpDpioVal;
	}

	// do simple slaving
	if ((hrt_dmSlavingSwitch & DM_SLAVE_SWITCH_WFR_BIT) > 0)
	{
	    i = 0;
	    while (simpleSlaveSource[i] >= 0)
	    {
		crntBufferWFR[simpleSlaveDest[i]] = crntBufferWFR[simpleSlaveSource[i]];
		i++;
	    }
	}
    }

    //    set tip and tilt values
    crntBufferWFR[tipNdxPlus]   = tipDNPlus;
    crntBufferWFR[tipNdxMinus]  = tipDNMinus;
    crntBufferWFR[tiltNdxPlus]  = tiltDNPlus;
    crntBufferWFR[tiltNdxMinus] = tiltDNMinus;

    //    move current woofer / T/T settings into current dpio2 buffer
    for (i = 0; i < (sizeInBytesWFR / 2); i++)
	dpio2BufferWFR[crntBuffNdx][i] = crntBufferWFR[i];

    //    initiate the DMA transfer

#if ( RTL_SYSTEM == 0 )
    gpAoSrtDpio2StartDmaTransfer(dpio2DevnoWFR, 0, crntBuffNdx);  // errors are reported in the function; so we're not checking here
#else
    gpAoHrtDpio2StartDmaTransfer(dpio2DevnoWFR, 0, crntBuffNdx);  // errors are reported in the function; so we're not checking here
#endif

    prevBuffNdx = crntBuffNdx;  // switch buffer indexes
    crntBuffNdx ^= 0x1;            // > for next time

#else
    int             ii;
    unsigned int outputVoltsData[MAX_WOOFER_ACTS];

    for( ii=0; ii<MAX_WOOFER_ACTS; ii++)
    {
	if( wfrVolts[ii] > hrt_wfrMaxVolts[ii] )
	{
	    outputVoltsData[ii] = hrt_wfrMaxVolts[ii];
	    continue;
	}
	if( wfrVolts[ii] < hrt_wfrMinVolts[ii] )
	{
	    outputVoltsData[ii] = hrt_wfrMinVolts[ii];
	    continue;
	}
	outputVoltsData[ii] = (unsigned int)(wfrVolts[ii]);
    }
#endif

    return allDone;
}

/*!
 ********************************************************************************
 *    @fn gpAoHrtVolt_WriteTTVolts( float ttVolts[], boolean physicallyWriteFlag )
 *
 *    @brief
 *    The purpose of this unit is to write out the TT mirror voltage to the hardware.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to write out the TT mirror voltage to the hardware.
 *    This is done by converting to digital numbers and storing them in global
 *    variables to be picked up when the woofer is written.
 *    @par
 *    IMPORTANT: The woofer buffer (including the new TT values) is written out
 *    from this function (using gpAoHrtVolt_WriteWooferVolts()), in order to output
 *    the new TT values as soon as possible.
 *    @par
 *    Differential +/-5V outputs are used to generate the +/-10V range needed by the
 *    PI E-712 TT controller.  The software writes complementary values, limited to
 *    the 0x4000 � 0xC000 (+/-5V) range.  See "SAM97 Driver - LV Outputs1.pdf" for
 *    more information and a table of examples.
 *    @par
 *    Note that voltages are range checked in this function.  However, the angles will
 *    already have been range checked.  So, the voltages should never be out of range.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_WriteTTVolts(  ttVolts[] );
 *
 *    @param[in] ttVolts                (float[])    - tt voltage array
 *    @param[in] physicallyWriteFlag    (boolean)    - physically write to the stage
 *
 *    @return
 *        <0: Clipping occurred ( this is a warning -- processing continues )
 *         0: No Error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_WriteTTVolts( float ttVolts[], boolean physicallyWriteFlag )
{
int ii;
int    retCode = 0;

#if ( WFR_TYPE == WFR_GPI_121 )
float           tmpTTVolts;
unsigned short tmpDpioVal;
unsigned short ttDNMinus;  // = ttDNMidRange;  // setting both minus and plus to 0x8000
unsigned short ttDNPlus;   // = ttDNMidRange;  // > gives a net differential voltage of 0V

    for( ii=0; ii<MAX_TT_ACTS; ii++ )
    {
	tmpTTVolts = ttVolts[ ii ];

	if( tmpTTVolts > hrt_ttMaxVolts[ ii ] )
	    tmpTTVolts = hrt_ttMaxVolts[ ii ];
	else if( tmpTTVolts <  hrt_ttMinVolts[ ii ] )
	    tmpTTVolts = hrt_ttMinVolts[ ii ];

	tmpTTVolts -= hrt_ttMinVolts[ii];  // this is to always start the  DNs from 0
	tmpDpioVal = (unsigned short)(tmpTTVolts * DNS_PER_VOLT_TT);

	ttDNPlus = ttDNLowerLimit + tmpDpioVal;
	if ((ttDNPlus < ttDNLowerLimit) || (ttDNPlus > ttDNUpperLimit))  //XXX keep this belt-and-suspenders testing for now
	{
	    ttDNPlus = ttDNMidRange;
	    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtVolt_WriteTTVolts) ttDNPlus out of range: ", ttDNPlus);
	    retCode--;
	}

	ttDNMinus = ttDNUpperLimit - tmpDpioVal;
	if ((ttDNMinus < ttDNLowerLimit) || (ttDNMinus > ttDNUpperLimit))  //XXX keep this belt-and-suspenders testing for now
	{
	    ttDNMinus = ttDNMidRange;
	    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtVolt_WriteTTVolts) ttDNMinus out of range: ", ttDNMinus);
	    retCode--;
	}

	if ( ii == 0 )  // tip
	{
	    tipDNPlus  = ttDNPlus;
	    tipDNMinus = ttDNMinus;
	}
	else            // tilt
	{
	    tiltDNPlus  = ttDNPlus;
	    tiltDNMinus = ttDNMinus;
	}
    }

    //    if we're to physically write, then do it -- the reason we wouldn't is because TT applied to the woofer mirror
    //    > may be about to be written anyway (and we don't want to do 2 writes if we don't need to)
    if (physicallyWriteFlag)
	gpAoHrtVolt_WriteWooferVolts( NULL, true );  // need this to DMA data out to drive electronics (that are shared with woofer)
#else
    for( ii=0; ii<MAX_TT_ACTS; ii++)
    {
	if( ttVolts[ ii ] > hrt_ttMaxVolts[ ii ] )
	{
	    ttVolts[ ii ] =  hrt_ttMaxVolts[ ii ];
	    retCode--;
	}
	if( ttVolts[ ii ] <  hrt_ttMinVolts[ ii ] )
	{
	    ttVolts[ ii ] =  hrt_ttMinVolts[ ii ];
	    retCode--;
	}
    }
#endif

    return retCode;
}

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_StartReg( short actNum,  ReconBuffType* reconBufPtr )
 *
 *    @brief
 *    The purpose of this unit is to start moving the indicated actuator up and down at 1 Hz.  Note
 *    that more than one actuator can be moved at a time
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to start moving the indicated actuator up and down at 1 Hz.  Note
 *    that more than one actuator can be moved at a time (i.e., the reg command can be executed multiple
 *    consecutive times).
 *    @par
 *    All available actuators can be moved, NOT just the 'active' actuator pattern. ( for registration etc.)
 *    If the input actuator value is set to (-1), then the system is put into a 'roundRobin' mode that
 *    will walk through each of the actuators on all of the mirrors.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_StartReg( short actNum,  ReconBuffType* reconBufPtr );
 *
 *    @param[in] actNum    (int)    - actuator to move ( this is included with the rest that are currently moving )
 *                                -  if (-1) then start round robin mode.
 *
 *    @return
 *        0: No Error  else error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_StartReg( short actNum,  ReconBuffType* reconBufPtr  )
{
short i;

    regQuitFlag = false;
    printf("                 Starting gpAoHrtVolt_StartReg\n");
    //    if we're to run in round-robin mode make sure we can and set things up
    if (actNum == -1)
    {
	printf("                   gpAoHrtVolt_StartReg actnUM = -1\n");
	// Request for 'round robin mode' (actNum = -1 )
	if (hrtFileDataShmPtr->cycleActActive)
	{
	    printf("                       ERR (Volt_StartReg) running 'cycling cmd' - can't start 'round-robin'\n");
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (Volt_StartReg) running 'cycling cmd' - can't start 'round-robin'");
	    return 1;
	}

	for( i=0; i<MAX_TWEETER_ACTS; i++)
	    twtRegPhase[i] = 0.0;

	for( i=0; i<MAX_WOOFER_ACTS; i++)
	    wfrRegPhase[i] = 0.0;

	reconBufPtr->reconTTData[eTTxSettingAngle] = 0.0;
	reconBufPtr->reconTTData[eTTySettingAngle] = 0.0;

	regRoundRobinNdx = 0;
	regRoundRobinFrameCntr = 0;
	regRoundRobin = true;
	regFrameRate = (float)( LegalFrameRates [hrt_camFrameRate] );

	hrtFileDataShmPtr->cycleActActive = true;
	SysConSendStrngNoParm(eDebugLevelMax, "INFO Starting cycleReg - roundRobin");
	printf("               INFO Starting cycleReg - roundRobin\n)");
    }
    else
    {
	printf("                   gpAoHrtVolt_StartReg: actNAum = '%d'\n", actNum);
	if (regRoundRobin)
	{
	    printf("                       ERR (Volt_StartReg) running 'round-robin' - can't start 'cycling'\n");
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (Volt_StartReg) running 'round-robin' - can't start 'cycling'");
	    return 1;
	}

	printf("                   gpAoHrtVolt_StartReg, not round robin\n");
	// if the reg command isn't currently running
	//     initialize variables
	//     set flag to indicate that reg command is running
	if (!hrtFileDataShmPtr->cycleActActive)
	{
	    for (i=0; i<MAX_TWEETER_ACTS; i++)
	    {
		regActiveTwtActs[i] = false;
		twtRegPhase[i] = 0.0;
	    }
	    for (i= 0; i<MAX_WOOFER_ACTS; i++)
	    {
		regActiveWfrActs[i] = false;
		wfrRegPhase[i] = 0.0;
	    }
	    for (i=0; i<MAX_TT_ACTS; i++)
		regActiveTTActs[i] = false;

	    reconBufPtr->reconTTData[eTTxSettingAngle] = 0.0;
	    reconBufPtr->reconTTData[eTTySettingAngle] = 0.0;

	    regCntr = 0;
	    regFrameRate = (float)( LegalFrameRates [hrt_camFrameRate] );
	    regIncrementor = 2.0 * regFreq;

	    hrtFileDataShmPtr->cycleActActive = true;
	}

	// set flag to indicate which actuator should be moved
	if ((actNum >= 0) && (actNum < MAX_TWEETER_ACTS ))
	    regActiveTwtActs[ actNum ] = true;
	else if( (actNum >= 5000) && (actNum < (5000+MAX_WOOFER_ACTS ) ) )
	    regActiveWfrActs[ actNum - 5000 ] = true;
	else if( (actNum >= 6000) && (actNum <  (6000+MAX_TT_ACTS) ) )
	    regActiveTTActs[ actNum - 6000 ] = true;
	else
	{
	    printf("                       ERR (gpAoHrtVolt_StartReg) invalid cycleActuator code\n");
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_StartReg) invalid cycleActuator code\n");
	}
    } // end else

    printf("                 Ending gpAoHrtVolt_StartReg\n");
    return 0;
}

/*!
 ********************************************************************************
 *    @fn long int randomLocal()
 *    @fn long int srandomLocal()
 *
 *    @brief
 *    Random number generator.
 *
 *    @b DESCRIPTION:
 *    This random number generator is exactly the one described in "Random Number
 *    Generators: Good Ones are Hard to Find," Stephen K. Park and Keith W. Miller,
 *    Communications of the ACM, Volume 31, Number 10, October 1988.
 *    @par
 *    This generator is proposed as the "minimal standard" that will port to all
 *    systems where the maximum integer size is 2^32 - 1 or larger.
 *    @par
 *    We're using it to inject a random signal into the T/T to measure the controller
 *    transfer function, because RTLinux does not include rand().
 *    @par
 *    From: http://rtlinux.lzu.edu.cn/mailing_list/rtl.w5archive/9905/msg00018.html
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_StartReg( short actNum,  ReconBuffType* reconBufPtr );
 *
 *    @param[in] s    (long int)    - seed
 *
 *    @return
 *        (long int) random number
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
#define RAND_MAX 2147483647
#define A 16807
#define Q 127773
#define R 2836
#define MODULUS 2147483647

static long int seed = 65521;

long int
randomLocal(void)
{
long int lo, hi, test;

    hi = seed / Q;
    lo = seed % Q;
    test = A * lo - R * hi;

    if (test > 0)
	seed = test;
    else
	seed = test + MODULUS;

    return seed;
}

long int
srandomLocal(long int s)
{
    return seed = s % MODULUS;
}

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_ProcessReg(  ReconBuffType* reconBufPtr )
 *
 *    @brief
 *    This is the 'working routine' for the cycle actuator comamnds.
 *
 *    @b DESCRIPTION:
 *    This is the 'working routine' for the cycle actuator comamnds.  If starting up,
 *    call gpAoHrtVolt_StartReg( short actNum ).
 *    @par
 *    The purpose of this unit is to move the indicated actuator(s) up and down at 1 Hz.  Note
 *    that more than one actuator can be moved at a time (i.e., this command can be executed multiple
 *    consecutive times). If the input actuator value is set to (-1), then the system is put into a
 *    'roundRobin' mode that will walk through each of the actuators on all of the mirrors.
 *    All available actuators will be moved, NOT just the 'active' actuator pattern. ( for registration etc.)
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_ProcessReg( (ReconBuffType*) reconBufPtr   );
 *
 *    @b Param None.
 *
 *    @return
 *        0: No Error  else error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_ProcessReg(  ReconBuffType* reconBufPtr  )
{
int    i;
int    xx, xxIn, xxOut;
int    yy, yyIn, yyOut;
    //    if we're to quit, set flags appropriately and zero all of the actuators
    if (regQuitFlag)
    {
	hrtFileDataShmPtr->cycleActActive = false;
	regRoundRobin = false;

	if( reconBufPtr != NULL )
	{
	    for (i = 0; i < MAX_TWEETER_ACTS; i++)
		reconBufPtr->reconTwtPhase[i] = 0.0;
	    gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, reconBufPtr->reconTwtVolts, NULL );
	    gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );

	    for (i = 0; i < MAX_WOOFER_ACTS; i++)
		reconBufPtr->reconWfrPhase[i] = 0.0;
	    gpAoHrtVolt_ConvWoofer(  reconBufPtr->reconWfrPhase, reconBufPtr->reconWfrVolts, NULL );
	    gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconWfrVolts, false );

	    reconBufPtr->reconTTData[eTTxSettingAngle] = 0.0;
	    reconBufPtr->reconTTData[eTTySettingAngle] = 0.0;
	    gpAoHrtVolt_ConvTT( &reconBufPtr->reconTTData[eTTxSettingAngle],
				&reconBufPtr->reconTTData[eTTxSettingVolts], NULL );
	    gpAoHrtVolt_WriteTTVolts( &reconBufPtr->reconTTData[eTTxSettingVolts], true );
	}

	hrtReconDataPtr->cmdCrnt = eCmdNone;
	SysConSendStrngNoParm(eDebugLevelMin, "INFO 'stopped actuators' complete");
	return 0;
    }

    //    otherwise, adjust the regCntr and continue
       regCntr += regIncrementor;
    if (regCntr <= 0.0)
    {
	regCntr = 0;
	regFrameRate = (float)( LegalFrameRates [hrt_camFrameRate] );
	regIncrementor = 2.0 * regFreq;
    }
    else if (regCntr >= regFrameRate)
    {
	regCntr = regFrameRate;
	regIncrementor = -2.0 * regFreq;
    }

    //    if we're in round-robin mode
    if (regRoundRobin)
    {
	// if we're finished with the current actuator  move on to the next
	if (++regRoundRobinFrameCntr >= (regFrameRate * 5))
	{
	    regRoundRobinFrameCntr = 0;

	    if( regRoundRobinNdx >= 0 && regRoundRobinNdx < MAX_TWEETER_ACTS )
		twtRegPhase[ regRoundRobinNdx ] = 0.0;
	    else if( regRoundRobinNdx >= 5000 && regRoundRobinNdx < (5000+MAX_WOOFER_ACTS) )
		wfrRegPhase[ regRoundRobinNdx - 5000 ] = 0.0;
	    else if( regRoundRobinNdx >= 6000 && regRoundRobinNdx < (6000+MAX_TT_ACTS) )
		reconBufPtr->reconTTData[ eTTxSettingAngle + regRoundRobinNdx - 6000 ] = 0.0;

	    // Update NDX for this pass
	    if( ++regRoundRobinNdx > MAX_TWEETER_ACTS && regRoundRobinNdx < 5000 )
		regRoundRobinNdx = 5000;
	    else if( regRoundRobinNdx > (5000+MAX_WOOFER_ACTS) && regRoundRobinNdx < 6000 )
		regRoundRobinNdx = 6000;
	    else if( regRoundRobinNdx > (6000+MAX_TT_ACTS) )
		regRoundRobinNdx = 0000;

	    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtVolt_ProcessReg) moving actuator: ", regRoundRobinNdx);
	}

	if( regRoundRobinNdx >= 0 && regRoundRobinNdx< MAX_TWEETER_ACTS )
	{
	    // set the voltage for the current actuator
	    twtRegPhase[ regRoundRobinNdx ] =  hrt_twtMinMicrons[regRoundRobinNdx] + regCntr / regFrameRate *
					      (hrt_twtMaxMicrons[regRoundRobinNdx] -
					       hrt_twtMinMicrons[regRoundRobinNdx]);

	    // Flip Tweeter Y to match IDL
	    for( yyIn=0, yyOut=NUM_TWEETER_ROWS-1; yyIn<NUM_TWEETER_ROWS; yyIn++, yyOut--)    // mrr DM
		for( xx=0; xx<NUM_TWEETER_COLS; xx++ )    // mrr DM
		    reconBufPtr->reconTwtPhase[ xx + yyIn * NUM_TWEETER_COLS] = twtRegPhase[ xx + yyOut * NUM_TWEETER_COLS ];    // mrr DM

	    // update the Tweeter with the new phase
	    gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, reconBufPtr->reconTwtVolts, NULL );
	    gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );
	}

	else if( regRoundRobinNdx >= 5000 && regRoundRobinNdx < (5000+MAX_WOOFER_ACTS) )
	{
	    // set the voltage for the current actuator
	    wfrRegPhase[ regRoundRobinNdx - 5000 ] =  hrt_wfrMinMicrons[regRoundRobinNdx - 5000] + regCntr / regFrameRate *
						      (hrt_wfrMaxMicrons[regRoundRobinNdx - 5000] -
						      hrt_wfrMinMicrons[regRoundRobinNdx - 5000]);

	    // Flip Woofer X to match IDL
	    for( yy=0; yy<WOOFER_COL_SIZE; yy++)
		for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
		    reconBufPtr->reconWfrPhase[ xxOut + yy * WOOFER_ROW_SIZE] = wfrRegPhase[ xxIn + yy * WOOFER_ROW_SIZE ];

	    // update the Woofers with the new phase
	    gpAoHrtVolt_ConvWoofer(  reconBufPtr->reconWfrPhase, reconBufPtr->reconWfrVolts, NULL );
	    gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconWfrVolts, false );
	}
	else if( regRoundRobinNdx >= 6000 && regRoundRobinNdx <= 6001 )
	{
	    // set the voltage for the current actuator
	    reconBufPtr->reconTTData[ eTTxSettingAngle + regRoundRobinNdx - 6000 ] =
		 hrt_ttMinMAS[regRoundRobinNdx - 6000] + regCntr / regFrameRate *
		(hrt_ttMaxMAS[regRoundRobinNdx - 6000] -
		 hrt_ttMinMAS[regRoundRobinNdx - 6000]);

	    // update the TT with the new Angle
	    gpAoHrtVolt_ConvTT( &reconBufPtr->reconTTData[eTTxSettingAngle],
				&reconBufPtr->reconTTData[eTTxSettingVolts], NULL );
	    gpAoHrtVolt_WriteTTVolts( &reconBufPtr->reconTTData[eTTxSettingVolts], true );
	}
    }
    else    // not round robin mode
    {
	//    update the voltages for each actuator that is being moved else set actuator to zero
	for( i=0; i<MAX_TWEETER_ACTS; i++)
	{
	    if (regActiveTwtActs[i])
	    {
		twtRegPhase[i] = hrt_twtMinMicrons[i] + regCntr / regFrameRate * (hrt_twtMaxMicrons[i] - hrt_twtMinMicrons[i]);
	    }
	    else
		twtRegPhase[i] = 0.0;
	}
	for( i=0; i<MAX_WOOFER_ACTS; i++)
	{
	    if (regActiveWfrActs[i])
	    {
		wfrRegPhase[i] = hrt_wfrMinMicrons[i] + regCntr / regFrameRate * (hrt_wfrMaxMicrons[i] - hrt_wfrMinMicrons[i]);
		/*/TEST -- start -- square wave
		if (regIncrementor < 0.0)
		{
		    wfrRegPhase[i] =  hrt_wfrMinMicrons[i] +
				     (hrt_wfrMaxMicrons[i] - hrt_wfrMinMicrons[i]) / 2.0 -
				     (hrt_wfrMaxMicrons[i] - hrt_wfrMinMicrons[i]) / 128.0;
		}
		else
		{
		    wfrRegPhase[i] =  hrt_wfrMinMicrons[i] +
				     (hrt_wfrMaxMicrons[i] - hrt_wfrMinMicrons[i]) / 2.0 +
				     (hrt_wfrMaxMicrons[i] - hrt_wfrMinMicrons[i]) / 128.0;
		}
		//TEST -- end */
	    }
	    else
		wfrRegPhase[i] = 0.0;
	}
	for( i=0; i<MAX_TT_ACTS; i++)
	{
	    if (regActiveTTActs[i])
	    {
		reconBufPtr->reconTTData[eTTxSettingAngle + i] = hrt_ttMinMAS[i] + regCntr / regFrameRate * (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i]);
		/*/TEST -- start -- square wave
		if (regIncrementor < 0.0)
		{
		    reconBufPtr->reconTTData[eTTxSettingAngle + i] =  hrt_ttMinMAS[i] +
								     (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i]) / 2.0 -
								     (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i]) / 16.0;
		}
		else
		{
		    reconBufPtr->reconTTData[eTTxSettingAngle + i] =  hrt_ttMinMAS[i] +
								     (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i]) / 2.0 +
								     (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i]) / 16.0;
		}
		//TEST -- end */
		/*/TEST -- start -- inject a random signal into TT to measure transfer function
		float sigAttenuator, randReal;
		sigAttenuator = 0.25;  // attenuate the TT signal, since it's saturating the wfs sub-aps
		randReal = (float)((double)randomLocal() / (double)RAND_MAX);  // should be between 0.0 and 1.0
		reconBufPtr->reconTTData[eTTxSettingAngle + i] =
		    (hrt_ttMinMAS[i] + randReal * (hrt_ttMaxMAS[i] - hrt_ttMinMAS[i])) * sigAttenuator;
		//TEST -- end */
	    }
	    else
		reconBufPtr->reconTTData[eTTxSettingAngle + i] = 0.0;
	}

	// Flip Tweeter Y to match IDL
	for( yyIn=0, yyOut=NUM_TWEETER_ROWS-1; yyIn<NUM_TWEETER_ROWS; yyIn++, yyOut--)
	    for( xx=0; xx<NUM_TWEETER_COLS; xx++ )
		reconBufPtr->reconTwtPhase[ xx + yyOut * NUM_TWEETER_COLS] = twtRegPhase[ xx + yyIn * NUM_TWEETER_COLS ];

	gpAoHrtVolt_ConvTweeter( reconBufPtr->reconTwtPhase, reconBufPtr->reconTwtVolts, NULL );
	gpAoHrtVolt_WriteTweeterVolts( reconBufPtr->reconTwtVolts );

	// Flip Woofer X to match IDL
	for( yy=0; yy<WOOFER_COL_SIZE; yy++)
	    for( xxIn=0, xxOut=WOOFER_ROW_SIZE-1; xxIn<WOOFER_ROW_SIZE; xxIn++, xxOut-- )
		reconBufPtr->reconWfrPhase[ xxOut + yy * WOOFER_ROW_SIZE] = wfrRegPhase[ xxIn + yy * WOOFER_ROW_SIZE ];

	gpAoHrtVolt_ConvWoofer(  reconBufPtr->reconWfrPhase, reconBufPtr->reconWfrVolts, NULL );
	gpAoHrtVolt_WriteWooferVolts( reconBufPtr->reconWfrVolts, false );

	gpAoHrtVolt_ConvTT( &reconBufPtr->reconTTData[eTTxSettingAngle],
			    &reconBufPtr->reconTTData[eTTxSettingVolts], NULL );
	gpAoHrtVolt_WriteTTVolts( &reconBufPtr->reconTTData[eTTxSettingVolts], true );
    }

    return 0;
}

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_StopReg(  ReconBuffType* reconBufPtr  )
 *
 *    @brief
 *    The purpose of this unit is to stop moving all actuators that were started with reg.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to stop moving all actuators that were started with reg.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_StopReg(  (ReconBuffType*) reconBufPtr );
 *
 *    @b Param None.
 *
 *    @return
 *        0: No Error  else error
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_StopReg(  ReconBuffType* reconBufPtr  )
{
    regQuitFlag = true;
    return 0;
}

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_MirrorShapeStart( short dmShape_BitMap )
 *
 *    @brief
 *    Random number generator.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to start the processing of the storeShapes
 *    command by initializing the actuator sums (to be used to compute averages)
 *    and setting counters and flags necessary to run the command.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_MirrorShapeStart( dmShape_BitMap );
 *
 *    @param[in] dmShape_BitMap    (short)    - indicatess which shape is being started
 *
 *    @return
 *        (int) 0
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_MirrorShapeStart( short dmShape_BitMap )
{
short i;

    hrt_ReconRunCmd = eCmdNone;

    //    initialize tweeter shape sums to zeroes
    for( i=0; i<MAX_TWEETER_ACTS; i++)
	mirrorShapeTwtPhaseSums[i] = 0.0;

    //    initialize woofer shape sums to zeroes
    for( i=0; i<MAX_WOOFER_ACTS; i++)
	mirrorShapeWfrPhaseSums[i] = 0.0;

    //    initialize sum counter and command state and set flag to indicate that the mflat or msharp command is running
    mirrorShapeProcessCount = 0;                        // counts sets of actuator data when running mirrorShapeProcess

    switch( dmShape_BitMap )
    {
	case 0x01:
	case 0x02:
	case 0x04:
	    mirrorShapeState = eCmdStateSumTwt;                    // current state of the mirrorShape processing is Twt
	    break;

	case 0x10:
	case 0x20:
	case 0x40:
	    mirrorShapeState = eCmdStateSumWfr;                    // current state of the mirrorShape processing is Wfr
	    break;

	default: // if more than one bit is set or no bits are set
	    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtVolt_MirrorShapeStart) wrong bits set in the storeShape request: ", dmShape_BitMap);
	    hrt_ReconRunCmd = eCmdNone;
	    hrtReconDataPtr->cmdCrnt = eCmdNone;
	    return 1;
    }

    mirrorShapeDmShapeType = dmShape_BitMap;
    hrt_ReconRunCmd = eCmdStoreShape;
    return 0;
}

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_MirrorShapeProcess( ReconBuffType* reconBufPtr )
 *
 *    @brief
 *    Process data for the the storeShapes commands.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to process data for the storeShapes (woofer or tweeter )
 *    commands by receiving MirrorShapeAvgN sets of actuator data, averaging that data,
 *    and writing the result to a file when done.  All commands are handled by this function
 *    because the processing is virtually identical, except for the file that the data is
 *    written to at the end.
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_MirrorShapeProcess( reconBufPtr );
 *
 *    @param[in] reconBufPtr    (ReconBuffType*) - buffer containing current mirror shape data
 *
 *    @return
 *        (int) 0
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_MirrorShapeProcess( ReconBuffType* reconBufPtr )
{
short i;

    //    sum or average and store actuator data, depending on mirrorShapeState
    switch( mirrorShapeState )
    {
	case eCmdStateSumTwt:
	    //     add current actuator voltage data to mirrorShapeTwtSums sums
	    for (i = 0; i < MAX_TWEETER_ACTS; i++)
		mirrorShapeTwtPhaseSums[i] += reconBufPtr->reconTwtPhase[i];

	    //     increment sum counter and change state to average data if we have enough data
	    if (++mirrorShapeProcessCount >= hrt_MirrorShapeAvgN)
		mirrorShapeState = eCmdStateAvg;
	    break;

	case eCmdStateSumWfr:
	    //     add current actuator voltage data to mirrorShapeWfrSums sums
	    for (i = 0; i < MAX_WOOFER_ACTS; i++)
		mirrorShapeWfrPhaseSums[i] += reconBufPtr->reconWfrPhase[i];

	    //     increment sum counter and change state to average data if we have enough data
	    if (++mirrorShapeProcessCount >= hrt_MirrorShapeAvgN)
		mirrorShapeState = eCmdStateAvg;
	    break;

	case eCmdStateAvg:
	    //     average the actuator voltage sums
	    //     command the SRT to store the data to disk
	    if (hrt_ReconRunCmd == eCmdStoreShape)
	    {
		switch( mirrorShapeDmShapeType )
		{
		    case eDmShape_TwtLabFlat:
			for (i = 0; i < MAX_TWEETER_ACTS; i++)
			    hrtFileDataShmPtr->twtLabFlat[i] = mirrorShapeTwtPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeTwtLabFlat );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Tweeter Lab Flat Storage complete");
			break;

		    case eDmShape_TwtSysFlat:
			for (i = 0; i < MAX_TWEETER_ACTS; i++)
			    hrtFileDataShmPtr->twtSysFlat[i] = mirrorShapeTwtPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeTwtSysFlat );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Tweeter Sys Flat Storage complete");
			break;

		    case eDmShape_TwtDmShape:
			for (i = 0; i < MAX_TWEETER_ACTS; i++)
			    hrtFileDataShmPtr->twtDmShape[i] = mirrorShapeTwtPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeTwtDmShape );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Tweeter Dm Shape Storage complete");
			break;

		    case eDmShape_WfrLabFlat:
			for (i = 0; i < MAX_WOOFER_ACTS; i++)
			    hrtFileDataShmPtr->wfrLabFlat[i] = mirrorShapeWfrPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeWfrLabFlat );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Woofer Lab Flat Storage complete");
			break;

		    case eDmShape_WfrSysFlat:
			for (i = 0; i < MAX_WOOFER_ACTS; i++)
			    hrtFileDataShmPtr->wfrSysFlat[i] = mirrorShapeWfrPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeWfrSysFlat );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Woofer Sys Flat Storage complete");
			break;

		    case eDmShape_WfrDmShape:
			for (i = 0; i < MAX_WOOFER_ACTS; i++)
			    hrtFileDataShmPtr->wfrDmShape[i] = mirrorShapeWfrPhaseSums[i] / (float)mirrorShapeProcessCount;
			gpAoHrtFileData_Put( eFileTypeWfrDmShape );
			SysConSendStrngNoParm(eDebugLevelMin, "INFO Woofer Dm Shape Storage complete");
			break;
		}
	    }

			// RESPONSE storeShapes
			diagCmdResponse(CmdRespStoreShapes, 0, NULL, NULL, NULL);

	    //     set flags to indicate that command is not running
	    hrt_ReconRunCmd = eCmdNone;
	    hrtReconDataPtr->cmdCrnt = eCmdNone;
	    break;

	default:
	    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtVolt_MirrorShapeProcess) unexpected mirrorShapeState");
	    break;
    }

    return 0;
}//

/*!
 ********************************************************************************
 *    @fn int gpAoHrtVolt_MirrorShapeAbort( )
 *
 *    @brief
 *    Process data for the the storeShapes commands.
 *
 *    @b DESCRIPTION:
 *    The purpose of this unit is to abort the currently running storeShapes command
 *    by simply setting the command flags to indicate that no command is executing
 *    (since the new shape data is stored in a temporary buffer until the command
 *    completes, there is no additional cleanup to be done here).
 *    @par
 *
 *    @b Usage: retVal = gpAoHrtVolt_MirrorShapeAbort(  );
 *
 *    @return
 *        (int) 0
 *
 *    @exception None.
 *
 *    @par @b PRIOR REQUIREMENTS:
 *    None.
 *
 *    @callgraph
 ********************************************************************************/
int
gpAoHrtVolt_MirrorShapeAbort( void )
{
    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'storeShape' aborted");
    hrt_ReconRunCmd = eCmdNone;
    hrtReconDataPtr->cmdCrnt = eCmdNone;
    mirrorShapeState = eCmdState_None;
    return 0;
}


//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
//
