/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtCamTT.c
 *
 *  @brief
 *  This component contains functions to interface to the TT camera.
 *
 *  @b DESCRIPTION:
 *  This component is the Hard Real Time (HRT) interface to the AO TT camera.
 *  It performs the RT camera initialization, communicates with the SRT CamMngr
 *  module to issue commands (because the camera drivers are not strictly RTLinux
 *  compatible), and handles real-time processing for the camera.
 *
 *  @note The Cam() thread is included in this component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 11/25/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c gpAoHrtCamTT()
 *  -# @c gpAoHrtCamTT_Init()
 *  -# @c gpAoHrtCamTT_IntrInit()
 *  -# @c gpAoHrtCamTT_IntrHandler()
 *  -# @c gpAoHrtCamTT_ProcCmd()
 *  -# @c gpAoHrtCamTT_SendCmd()
 *  -# @c gpAoHrtCamTT_LoadDataBuffer()
 *  -# @c gpAoHrtCamTT_DeIntlv128x86()
 *  -# @c gpAoHrtCamTT_DeIntlv128x128()
 *  -# @c gpAoHrtCamTT_AutoParms()
 *  -# @c gpAoHrtCamTT_DrvrWatch()
 *  -# @c gpAoHrtCamTT_LoadSimWtData()
 *  -# @c gpAoHrtCamTT_LoadSimTTData()
 *  -# @c gpAoHrtCamTT_LoadSimPupilMoveData()
 *  -# @c gpAoHrtCamTT_LoadSimZernikes()
 *  -# @c gpAoHrtCamTT_ComputeZernikes()
 *
 *  @see gpAoSrtCamMngrTT.c
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif

#include "gpAoHrtCamTT.h"
#include "gpAoSrtCamMngrTT.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#if (CAMTT_TYPE == TT_REAL_GPI)
#if (RTL_SYSTEM != 0)
typedef unsigned char           u_char;
typedef unsigned short          u_short;
typedef unsigned int            u_int;
typedef long long               __kernel_loff_t;
typedef __kernel_loff_t         loff_t;
#include "edt_lnx_kernel_Local.h"
#else
#include "edtinc.h"
#endif
#endif


// Defines


// Global Variables
#if ( TIMER_TEST != 0 )
// a sequential counter incremented every time a valid frame is sent to the centroider
long camTTFrameID = -1;

#endif

short   hrt_useTtSimData;       // flag indicating whether to use simulated tt data ( 0: no, 1: yes )

short   hrt_camTTFrameRate;     // camera frame rate INDEX (NOT the actual frame rate!)
short   hrt_camTTGain;          // camera gain (0, 1, 2, or 3)

short   CamTTFrameRateNdx = 0;  // index for current camera rate into LegalFrameRatesTT[]

int     TT_irq = -1;

#if ( RTL_SYSTEM != 0 )
// WARNING: LegalFrameRatesTT[] is defined in both gpAoHrtCamTT.c (for the HRT) and gpAoSrtCamMngrTT.c (for the SRT)
/* WARNING:  The code below ASSUMES that the LegalFrameRates array is ordered. */
#if ( TTNUM_ROWS_IN_CAM_ACTUAL == 86 )
// IMPORTANT: note that 2000 fps isn't actually that fast in 128x86 mode, it's just a REP of 0 for testing
// IMPORTANT: DO NOT collect 10 seconds of diag data at 2000 fps -- there aren't enough buffers (collect up to 7 secs)!
// const short  LegalFrameRatesTT[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!   // mrr
const short     LegalFrameRatesTT[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator     // mrr
#else
// IMPORTANT: note that 1500 fps isn't actually that fast in 128x128 mode, it's just a REP of 0 for testing
//XXX put this back after testing!  const short LegalFrameRatesTT[] = { 250, 500, 1000, 1500,   -1, -1 };  // -1 is the terminator
//XXXdwp110615 const short      LegalFrameRatesTT[] = { 250, 500, 1000, 1500, 2000, -1 };  // -1 is the terminator
// const short  LegalFrameRatesTT[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!
const short     LegalFrameRatesTT[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator     // mrr
#endif
#endif

void* TT_dev = NULL;

boolean TTOpenLoopMode = true;

// Local Variables
#if ( TIMER_TEST != 0 )
static struct timespec intrTime, prevIntrTime = { 0, 0 }, afterWaitTime, afterEDTIntrProc, afterIRQEnable;
#endif

#if ((RTL_SYSTEM != 0) && (CAMTT_TYPE == TT_REAL_GPI))
// These are the kernel pages for the EDT ring buffers in RTL mode;
// camera frames are constructed from these pages
static u_char   **edtKernelPages[TTnumbufs];
static int      nEdtKernelPages[TTnumbufs];

// the following set of arrays is oversized by RTL_PAGE_SIZE because they
// MUST be big enough for the frame rounded up to an integer number of
// RTL_PAGE_SIZEs
static unsigned short   ttCamBuffsForRTL[TTnumbufs][TTNUM_ROWS_IN_CAM_ACT_160 * TTNUM_PIX_IN_ROW_ACT_160 + RTL_PAGE_SIZE];

static unsigned long    oldAffinity;
#endif

static unsigned short *buf_array[TTnumbufs];    // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other
static unsigned short *buf_array_160[TTnumbufs];  // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other

static unsigned int    edtDoneCount_sim = 0;
static unsigned short  ttCamBuffsFor128[TTnumbufs][TTNUM_ROWS_IN_CAM_ACTUAL * TTNUM_PIX_IN_ROW_ACTUAL];
static unsigned short  ttCamBuffsForSim[TTnumbufs][TTNUM_ROWS_IN_CAM_ACTUAL * TTNUM_PIX_IN_ROW_ACTUAL];
static unsigned short *buf_array_sim[TTnumbufs];

static int BadCntr = 0;

static unsigned short CamTTIntrWatchCntr = -1;  // watchdog counter for camera interrupt

static int      interruptWatch = 0;

static int      CheckCntr = 0;
static int      Cntr = 0;

// the following are out here so as not to make the stack too big

// map to deinterleave camera data for 128x86
static int deIntlvMap[TTNUM_ROWS_IN_CAM_FOR_SUBAPS * TTNUM_PIX_IN_ROW_FOR_SUBAPS];
// temporary map, before removing every fifth column
static int tmpMap[TTNUM_ROWS_IN_CAM_ACTUAL * TTNUM_PIX_IN_ROW_ACTUAL];
// temporary map, before removing every fifth column
static int tmpMap160[TTNUM_ROWS_IN_CAM_ACT_160 * TTNUM_PIX_IN_ROW_ACT_160];
// map to deinterleave camera data for 160x160
static int deIntlvMap160[TTNUM_ROWS_IN_CAM_ACT_160 * TTNUM_PIX_IN_ROW_ACT_160];
// temporary destination, before removing every fifth column
static unsigned short tmpDest[ TTNUM_ROWS_IN_CAM_ACTUAL ] [TTNUM_PIX_IN_ROW_ACTUAL];

// Local Function Prototypes
static void         gpAoHrtCamTT_Init( void );
static unsigned int gpAoHrtCamTT_IntrHandler(unsigned int irq, struct rtl_frame *regs);
static void         gpAoHrtCamTT_ProcCmd( short cmdNdx );
static void         gpAoHrtCamTT_SendCmd( short cmd, short param );
static void         gpAoHrtCamTT_LoadDataBuffer( int bufNdx );
static void         gpAoHrtCamTT_DeIntlv128x86( unsigned short *src, unsigned short dest[TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW]);
static void         gpAoHrtCamTT_DeIntlv128x128( unsigned short *src, unsigned short dest[TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW]);
static void         gpAoHrtCamTT_LoadSimTtData( unsigned short buf_array[], int curbuf );
static void         gpAoHrtCamTT_LoadSimTTData( unsigned short buf_array[] );
static void         gpAoHrtCamTT_LoadSimPupilMoveData( unsigned short buf_array[] );
static void         gpAoHrtCamTT_LoadSimZernikes( unsigned short buf_array[] );
static void         gpAoHrtCamTT_ComputeZernikes( void );













/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT( void* arg )
 *
 *  @brief
 *  This unit is the TT camera thread mainline.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the TT camera and then wait for commands
 *  from the host or data from the camera (via the camera interrupt service routine) and
 *  then take action as appropriate.
 *  @par
 *
 *  @b Usage: gpAoHrtCamTT( arg );
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
gpAoHrtCamTT( void *arg )
{
    boolean     waitBeforeIntrEnable;
    int         curbuf = 0;
    static int  doneCount = 0;
    static int  lastDoneIntr = 0;

#if ( TIMER_TEST != 0 )
    int tmpTimerNdx;
#endif

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    struct rtl_timespec     next, remaining;
#endif

#   if ADD_TIP_TILT
    hrtCamTTDataPtr->edtBufStat = eEdtBufStatNotInitted;
    hrtCamTTDataPtr->intrIgnore = false;
#   endif /* ADD_TIP_TILT */

    // perform TT camera initializations (including initializing the camera interrupt)
    gpAoHrtCamTT_Init();

    // send commands to initialize the EDT camera interface in user space
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInit2 -- start");
#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    next.tv_sec = 2;
    next.tv_nsec = 0;
    while( rtl_nanosleep( &next, &remaining ) < 0 )
        memcpy( &next, &remaining, sizeof( next ) );
#endif
    gpAoHrtCamTT_SendCmd (eTTInitialize2, 0);
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInit2 -- end");

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    next.tv_sec = 2;
    next.tv_nsec = 0;
    while( rtl_nanosleep( &next, &remaining ) < 0 )
        memcpy( &next, &remaining, sizeof( next ) );
#endif
    gpAoHrtCamTT_SendCmd (eTTSetRep, LegalFrameRatesTT[ hrt_camTTFrameRate ]);
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInitRep -- end");

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    next.tv_sec = 2;
    next.tv_nsec = 0;
    while( rtl_nanosleep( &next, &remaining ) < 0 )
        memcpy( &next, &remaining, sizeof( next ) );
#endif
    gpAoHrtCamTT_SendCmd (eTTSetAtten, (3 - hrt_camTTGain));
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInitAtten -- end");

    CamTTIntrWatchCntr = 0;

#if (RTL_SYSTEM == 0)
    cpu_set_t mask;
    int       cpuNum = HRT_CPU_TT_CAM;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT) CPU requested: ", cpuNum);
    if( sched_setaffinity( 0, sizeof( mask ), &mask ) != 0 )
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCamTT) executing sched_setaffinity() for CPU #",  cpuNum);
    sched_getaffinity( 0, sizeof( mask ), &mask );
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT): CPU_ISSET: ", CPU_ISSET( cpuNum, &mask));
#else
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT): Cam thread id: ", rtl_pthread_self());
#endif

    // enter infinite loop
    while (!QuitCamera_TT_Thread)
    {
        // this semaphore is currently posted in two places:
        // in sysCon in response to TT Host commands and in intr_handler
        // in response to incoming frame data
        if (rtl_sem_trywait(&TTSem) < 0)
        {
            rtl_usleep(1);
            continue;
        }
#if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterWaitTime);
#endif

        waitBeforeIntrEnable = false;

        if(  hrtCamTTDataPtr == NULL )
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_Init): hrtCamTTDataPtr is NULL" );
        // if a command is pending, call gpAoHrtCamTT_ProcCmd() to process it
        else if (hrtCamTTDataPtr->cmdStart != eCmdNone)
        {
            gpAoHrtCamTT_ProcCmd(hrtCamTTDataPtr->cmdStart);
            hrtCamTTDataPtr->cmdStart = eCmdNone;
        }
        // ignore the interrupt if we're doing serial communications
        else if (hrtCamTTDataPtr->intrIgnore)
        {
#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
            edt_intr_process(TT_dev);       // let EDT do its interrupt processing
            rtl_hard_enable_irq(TT_irq);    // re-enable the rtl irq
#endif
        }
#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
        else if (hrtCamTTDataPtr->edtBufStat == eEdtBufStatNotInitted)
            waitBeforeIntrEnable = true;
        else if (hrtCamTTDataPtr->edtBufStat == eEdtBufStatReadNew)
        {


            int i;
            for (i = 0; i < TTnumbufs; i++)
            {
                // get the kernel pages for each ring buffer
                edtKernelPages[i] = (u_char **)edt_get_buf_kbuffer_pages(TT_dev, i, &nEdtKernelPages[i]);
                SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init) npages: ", nEdtKernelPages[i]);
            }
            hrtCamTTDataPtr->edtBufStat = eEdtBufStatOkay;
            waitBeforeIntrEnable = true;
        }
#endif
        // process the interrupt
        else
        {
            CamTTIntrWatchCntr++;   // bump up TT camera watchdog counter

#if (CAMTT_TYPE != TT_REAL_GPI)
            // get the current done counter -- must be simulation mode
            doneCount = edtDoneCount_sim;
#else   /* CAMTT_TYPE */
            // let EDT do its interrupt processing
            edt_intr_process(TT_dev);

#if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterEDTIntrProc);
#endif

            if (hrt_useTtSimData == 1)
            {
                // get the current done counter -- simulation mode
                doneCount = edtDoneCount_sim;
                edtDoneCount_sim++;
            }
            else
            {
                // get the current EDT done counter
                doneCount = edt_get_done(TT_dev);
            }

            rtl_hard_enable_irq(TT_irq);           // re-enable the rtl irq

#if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterIRQEnable);
#endif
#endif  /* CAMTT_TYPE */

            if (doneCount < 7)
            {
                // doneCount goes to zero when switching modes,
                // let it settle
                lastDoneIntr = doneCount;
            }
            else if (lastDoneIntr == doneCount)
            {
                // interrupt could be a serial interrupt or sg ready
                // (not sure what "sg ready" is)
                // note -- not doing anything with BadCntr at present
                ++BadCntr;
            }
            else
            {
                // everything's okay, so proceed
#if ( TIMER_TEST != 0 )
                camTTFrameID++;
                tmpTimerNdx = camTTFrameID % NUM_TIME_SETS;


                timerTimes[tmpTimerNdx][eTimerLastTTIntr] = prevIntrTime;
                prevIntrTime = intrTime;


                timerTimes[tmpTimerNdx][eTimerThisTTIntr] = intrTime;
                timerTimes[tmpTimerNdx][eTimerAfterTTWake] = afterWaitTime;
                timerTimes[tmpTimerNdx][eTimerAfterTTEDTIntrProc] = afterEDTIntrProc;
                timerTimes[tmpTimerNdx][eTimerAfterTTIRQEnable] = afterIRQEnable;
#endif

                lastDoneIntr = doneCount;

                // the last DMA buffer is one less than DoneCount
                curbuf = (doneCount - 1) % TTnumbufs;



                gpAoHrtCamTT_LoadDataBuffer(curbuf);
            }
        }

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
        if (waitBeforeIntrEnable)
        {
            // the following 2 delays seem to be needed to avoid being inundated with EDT interrupts (that crash the kernel)
            next.tv_sec = 0;
            next.tv_nsec = 6000000;  // 6 millisecs -- tried 2, 4, and 5; 2 and 4 were too fast, 5 was okay
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );
            edt_intr_process(TT_dev);  // let EDT do its interrupt processing

            next.tv_sec = 0;
            next.tv_nsec = 6000000;  // 6 millisecs -- tried 2, 4, and 5; 2 and 4 were too fast, 5 was okay
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );
            rtl_hard_enable_irq(TT_irq);   // re-enable the rtl irq
        }
#endif

        hrtFileDataShmPtr->watchDogBits |= WatchDog_TTCamBit;
        interruptWatch = 0;
    }
    return NULL;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_Init( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *  @par
 *
 *  @b Usage: gpAoHrtCamTT_Init( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

static void
gpAoHrtCamTT_Init( void )
{
    int                                     i, ii;
    struct rtl_timespec     next, remaining;

    // make sure buf_array pointer start out as null
    for (i = 0; i < TTnumbufs; i++)
    {
        buf_array_160[i] = NULL;
    }

#   if ADD_TIP_TILT
    // initialize variables
    hrtCamTTDataPtr->cmdStart   = eCmdNone;
    hrtCamTTDataPtr->cmdCrnt    = eCmdNone;

    hrtCamTTDataPtr->camWriteNdx = 0; // Indexs are incremented at end

    hrtCamTTDataPtr->edtBufStat = eEdtBufStatNotInitted;
    hrtCamTTDataPtr->intrIgnore = false;

    // initialize the circular buffer status flags
    for( ii=0; ii<TTNUM_HRT_DATA_BUFFS; ii++)
    {
        (hrtTTDataFlowControlPtr->bufStat[ii]).hrtBufState      = eBufIdle;
        (hrtTTDataFlowControlPtr->bufStat[ii]).dispDoUpdate     = false;
        (hrtTTDataFlowControlPtr->bufStat[ii]).inDisplayProcess = false;
        (hrtTTDataFlowControlPtr->bufStat[ii]).inSaveProcess    = false;
    }

    hrtCamTTDataPtr->rawDispWriteBuffNdx = 0;
    hrtCamTTDataPtr->rawDispReadBuffNdx = 1;
#   endif /* ADD_TIP_TILT */

    // spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
    printf("          CamTT_Init: starting to spin on !OkayToFinishInit\n");
    while (!OkayToFinishInit)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        while( rtl_nanosleep( &next, &remaining ) < 0 )
            memcpy( &next, &remaining, sizeof( next ) );
    }
    printf("          CamTT_Init: Ending spinning on !OkayToFinishInit\n");


    // send command to do the first step of EDT camera interface
    // initialization in user space  and wait a second for that to happen
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInit1 -- start");
    gpAoHrtCamTT_SendCmd (eTTInitialize1, 0);
    next.tv_sec = 1;
    next.tv_nsec = 0;
    while ( rtl_nanosleep( &next, &remaining ) < 0 )
        memcpy( &next, &remaining, sizeof( next ) );
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init): CCC camInit1 -- end");

printf("          CamTT_Init: did init 1\n");

#if (CAMTT_TYPE == TT_REAL_GPI)
#if (RTL_SYSTEM == 0)
    // user-side mode
    if (TT_dev == NULL)
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_Init) TT_dev not set yet");  // this shouldn't happen, but check anyway
    else if ((pdv_multibuf(TT_dev, TTnumbufs)) != 0)
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_Init) failed to configure ring buffers");
    else
    {
        unsigned char **tmp_array = edt_buffer_addresses(TT_dev);
        for (i = 0; i < TTnumbufs; i++)
            buf_array_160[i] = (unsigned short *)tmp_array[i];
    }
#else   /* RTL_SYSTEM */
    // RTLinux mode
    if (TT_dev == NULL)
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_Init) TT_dev not set yet");  // this shouldn't happen, but check anyway
    else
    {
        for (i = 0; i < TTnumbufs; i++)
            buf_array_160[i] = ttCamBuffsForRTL[i];
    }
#endif  /* RTL_SYSTEM */
#endif  /* CAMTT_TYPE */

printf("          CamTT_Init: about to save\n");

    // save data for simulated data mode
    for (i = 0; i < TTnumbufs; i++)
    {
        buf_array[i]     = ttCamBuffsFor128[i];  // added for use with software ROI from 160x160 to 128x128
        buf_array_sim[i] = ttCamBuffsForSim[i];
    }

    CamTTInitDone = true;
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init) CamTTInit done");

printf("          CamTT_Init: init done\n");
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_IntrInit( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize the tt camera interrupt.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the tt camera interrupt.  This
 *  MUST be separate from the other cam initialization so that it can be called
 *  from SysCon while in RTCore main() initalization mode.  Also note that
 *  gpAoSrtCamMngr_StartCam() (on the SRT side) MUST have been executed by the
 *  time this function is called.
 *  @par
 *
 *  @b Usage: gpAoHrtCamTT_IntrInit( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

void
gpAoHrtCamTT_IntrInit( void )
{
#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    unsigned long affinity = 1 << HRT_CPU_TT_CAM;

    // get the IRQ# from the EDT driver and install a handler for it
    TT_irq = edt_get_irq_value(TT_UNIT_NUM);

    if (TT_irq < 0)
    {
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_Init) failed to get irq value: ",  TT_irq);
    }
    else
    {
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_Init) TT camera using irg ", TT_irq);

        TT_dev = edt_get_dev_from_irq(TT_irq);
        edt_take_interrupt(TT_irq, TT_dev);

        if ((rtl_request_irq( TT_irq, gpAoHrtCamTT_IntrHandler )) != 0)
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_IntrInit) in rtl_request_irq()");

        if (rtl_irq_set_affinity(TT_irq, &affinity, &oldAffinity) != 0)
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_IntrInit) in rtl_irq_set_affinity()");
    }
#endif
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_IntrHandler( unsigned int irq, struct rtl_frame *regs )
 *
 *  @brief
 *  Process commands originating in the HRT Camera control component (File: gpAoHrtCamTT.c) .
 *
 *  @b DESCRIPTION:
 *  This routine is called in response to an interrupt generated by the EDT driver upon
 *  receipt of a complete frame of camera data.  It's sole purpose is to check error
 *  conditions and activate a semaphore which triggers the processing of the camera frame data.
 *  @par
 *
 *  @b Usage: gpAoHrtCamTT_IntrHandler(  unsigned int irq, struct rtl_frame *regs );
 *
 *  @param[in] irq (int):
 *  @param[in] regs (struct rtl_frame *):
 *
 *  @return 0: None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static unsigned int
gpAoHrtCamTT_IntrHandler(unsigned int irq, struct rtl_frame *regs)
{
#if ( TIMER_TEST != 0 )
    rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#endif

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    if (++interruptWatch < 1000000)
    {
        rtl_sem_post(&TTSem);
    }
#endif
    return 0;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_ProcCmd( short cmdNdx )
 *
 *  @brief
 *  Process camera commands received from the host.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process camera commands received from the host,
 *  via the SRT side, such as automatically setting the optimal frame rate, forcing
 *  the frame rate to a particular value, etc.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_ProcCmd( short cmdNdx );
 *
 *  @param[in] cmdNdx (short): index into CmdTable[] for the command to be processed
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
gpAoHrtCamTT_ProcCmd( short cmdNdx )
{
    struct rtl_timespec next, remaining;

    // switch on command ID corresponding to command index
    switch (CmdTable[cmdNdx].idNum)
    {
        case eCmdAbort:            // abort long commands
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_ProcCmd) unexpected command abort received by cam");
            break;

#       if ADD_TIP_TILT
        case eCmdTTCamInit:     // initialize SciMeasure TT camera
            gpAoHrtCamTT_SendCmd(eTTInitialize1, 0);
            next.tv_sec = 3;
            next.tv_nsec = 0;
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );

            gpAoHrtCamTT_SendCmd(eTTInitialize2, 0);
            next.tv_sec = 7;
            next.tv_nsec = 0;
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );

            gpAoHrtCamTT_SendCmd(eTTSetRep, LegalFrameRatesTT[ hrt_camTTFrameRate ]);
            gpAoHrtCamTT_SendCmd(eTTSetAtten, (3 - hrt_camTTGain));
            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdTTClose:    // close TT loop
            // make sure we're in lgs mode
            //if (GSMode != eGSModeLGS)
            //    // report an error if we're not
            //    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_ProcCmd) system must be in lgs mode");
            //else
            {
                // otherwise, close the loop
                if (TTOpenLoopMode)  // make sure we're actually changing modes
                {
                    //++closeCntr;  //XXXTRACER

                    gpAoHrtCamTT_SendCmd (eTTMapClose, 0);
                    //until it exists TipTiltAPDCloseLoop();  // have to do the APD close loop stuff, too
                    TTOpenLoopMode = false;
                }
            }

            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdTTOpen:     // open TT loop
            if (!TTOpenLoopMode)  // make sure we're actually changing modes
            {
                //++openCntr;  //XXXTRACER

                //until it exists TipTiltAPDOpenLoop();  // have to do the APD open loop stuff, too
                gpAoHrtCamTT_SendCmd (eTTMapOpen, 0);
                TTOpenLoopMode = true;
            }

            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdTTCamRateAuto: // do 'autoCamRate' setting
            {
                SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_ProcCmd) Command is not supported");
                hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            }
            break;

        case eCmdTTCamRate:     // set frame rate of the camera (per second)
            {
                int tmpRate = hrtCamTTDataPtr->cmdIntParam[0];
                CamTTFrameRateNdx = 0;  // find a legal camera rate
                /* WARNING:  The following code ASSUMES that the LegalFrameRates array is ordered. */
                while ((tmpRate > LegalFrameRatesTT[CamTTFrameRateNdx]) && (LegalFrameRatesTT[CamTTFrameRateNdx + 1] != -1))
                    CamTTFrameRateNdx ++;
                hrt_camTTFrameRate = CamTTFrameRateNdx;
                gpAoHrtCamTT_SendCmd( eTTSetRep, LegalFrameRatesTT[hrt_camTTFrameRate] );
                gpAoHrtFileData_Put( eFileTypeParms );
                gpAoHrtCent_GetCrntDarkBackFlat( hrt_camTTFrameRate );  // get dark, back, and flat data for current frame rate
                hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            }
            break;

        case eCmdTTCamGain:     // set camera gain (0, 1, 2, or 3)
            hrt_camTTGain = hrtCamTTDataPtr->cmdIntParam[0];
            gpAoHrtCamTT_SendCmd (eTTSetAtten, (3 - hrt_camTTGain));
            gpAoHrtFileData_Put(eFileTypeParms);
            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdTTCamSRate:    // read frame rate from camera
            gpAoHrtCamTT_SendCmd (eTTReadRep, 0);
            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdTTCamSGain:    // read gain from camera
            gpAoHrtCamTT_SendCmd (eTTReadAtten, 0);
            hrtCamTTDataPtr->cmdCrnt = eCmdNone;
            break;
#       endif /* ADD_TIP_TILT */

        default:
            SysConSendStrngIntParm(eDebugLevelErr, "ERR (cam_TT.c): unexpected command received by cam, cmdNdx = ", cmdNdx);
            break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_SendCmd( short cmd, short cmdParam )
 *
 *  @brief
 *  Send a command and associated parameter (cmd dependent) to the SRT camera manager
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send messages to the SRT side (by using the FIFO diagnostic
 *  message process) to write commands to the camera and/or to change the DMA mapping (to accommodate
 *  changes in frame size).
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_SendCmd( short cmd, short cmdParam );
 *
 *  @param[in] cmd (int):  Cmd from HRT side that needs to be passed to SRT side manager
 *  @param[in] cmdParam (int): Paramter value (use depends on the command) for this particular command
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
gpAoHrtCamTT_SendCmd( short cmd, short param )
{
    static diagFIFOMsgType diagFIFOMsg;

    // set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagTTCmd;
    diagFIFOMsg.cmdNdx = cmd;
    diagFIFOMsg.intParam[0] = param;

    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (cam_TT.c): error writing gpAoHrtCamTT command to diag FIFO, errno = ", rtl_errno);
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_LoadDataBuffer( int bufNdx )
 *
 *  @brief
 *  Process frames from the TT camera.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process frames from the TT camera.  This means
 *  finding the next available slot in the camera data circular buffer, de-interleaving
 *  the data from the camera's several amplifiers, and alerting the centroider that
 *  new data is available.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_LoadDataBuffer( int bufNdx );
 *
 *  @param[in] bufNdx (int):  index of the SciMeasure camera buffer to process
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInvalidFileCmd
 *  @exception exceptionFileErr
 *
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
gpAoHrtCamTT_LoadDataBuffer(int bufNdx)
{
    int                         i, j;
#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    static const int            pageSizeInUints = RTL_PAGE_SIZE / 2;
#endif
    long                        tmpWrtNdx;
    CamTTBuffType*              camBuffPtr;
    DataBufferElemStatusType*   bufStatPtr;

    static int dispCntr = 0;
    static int overflowCntr = 0, overflowCheckCntr = 0;

#   if USE_TIP_TILT
    // Set a temp write index to cameraWriteNdx for validation
    //
    tmpWrtNdx = hrtCamTTDataPtr->camWriteNdx;
    bufStatPtr = &(hrtTTDataFlowControlPtr->bufStat[ tmpWrtNdx ]);

    // if this buffer is busy, then we have an overrun condition ( diag data dump, processing, (or display??) )
    if ( bufStatPtr->hrtBufState != eBufIdle )
    overflowCntr++;
    if ( ++overflowCheckCntr >= LegalFrameRatesTT[hrt_camTTFrameRate] ) // once a second
    {
        overflowCheckCntr = 0;
        if (overflowCntr > 0)
        {
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_LoadDataBuffer) circ. buf overrun: (chk 1/sec), writeNdx = ", tmpWrtNdx);
            overflowCntr = 0;
        }
    }

    if ( bufStatPtr->inDisplayProcess != false )    // Highly unexpected
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_LoadDataBuffer) display overrun - this frame was not processed for display");

    if ( bufStatPtr->inSaveProcess != false )       // Somewhat expected - particularly at debug time
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_LoadDataBuffer) 'save to file' overrun - this frame is in midst of save to disk - buff Ndx #", tmpWrtNdx );

    // At this point this frame buffer appears to be viable (possibly with Warning(cam_TT.c)s!)
    //      - set frame state to cam Processing
    //      - update the camWriteNdx
    //      - set Point to buffer
    bufStatPtr->hrtBufState = eBufCamProc;
    camBuffPtr = &(hrtCamTTDataPtr->camBuffs[ tmpWrtNdx ]);

    // determine if the frame will have to be sent to SRT for display
    //      - if so - flag it here - the last HRT process (currently recon) will send FIFO message to SRT based on 'bufStatPtr->dispDoUpdate'
    //
    if( hrtTTDataFlowControlPtr->dispRate != 0)
    {
        if( ++hrtTTDataFlowControlPtr->dispCntr >= (float)LegalFrameRatesTT[ hrt_camTTFrameRate ] / hrtTTDataFlowControlPtr->dispRate )
        {
            hrtTTDataFlowControlPtr->dispCntr = 0;
            bufStatPtr->dispDoUpdate = true;
            if( ++(hrtTTDataFlowControlPtr->nextDispBuf) >= NUM_SRT_DISPLAY_BUFS )
                hrtTTDataFlowControlPtr->nextDispBuf = 0;
        }
        else
            bufStatPtr->dispDoUpdate = false;
    }

#if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    // when in RTL mode, construct camera frame from kernel pages
    // (or load sim data, if that's what we're using)
    if (hrt_useTtSimData == 1)
    {
        gpAoHrtCamTT_LoadSimTtData( buf_array_sim[bufNdx], bufNdx );
    }
    else
    {       // use kernel pages
        for (i = 0; i < nEdtKernelPages[bufNdx]; i++)
            memcpy(&buf_array_160[bufNdx][i * pageSizeInUints], edtKernelPages[bufNdx][i], RTL_PAGE_SIZE);
    }
#endif

    if (hrt_useTtSimData == 1)  // if we're using simulated data
    {
        gpAoHrtCamTT_DeIntlv128x128( buf_array_sim[bufNdx], (void *)&camBuffPtr->usedCamData[0][0][0] );
    }
    else    // not using simulated data
    {
        gpAoHrtCamTT_DeIntlv128x128( buf_array_160[bufNdx], (void *)&camBuffPtr->usedCamData[0][0][0] );
    }

#ifdef  NO_TT_DOES_NOT_USE_CENTROID_THREAD_OF_WFS
    //      tell the Centroid thread that data's ready
    bufStatPtr->hrtBufState = eBufReqCent;

    if (rtl_sem_post( &CentSem ) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_LoadDataBuffer) posting to CentSem, errno = ", rtl_errno);
#else   /* NO_TT_DOES_NOT_USE_CENTROID_THREAD_OF_WFS */
    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_LoadDataBuffer) NOT posting to CentSem, errno = ", rtl_errno);
#endif  /* NO_TT_DOES_NOT_USE_CENTROID_THREAD_OF_WFS */

    // now that we've sent the data off to the centroider, move the
    // temporary destination matrix from gpAoHrtCamTT_DeIntlv128x128()
    // to a raw display buffer at 10 fps -- this will get picked up
    // by dispttraw, if dispttraw is running
    if (++dispCntr >= (LegalFrameRatesTT[hrt_camTTFrameRate] / 10))
    {
        dispCntr = 0;

        hrtCamTTDataPtr->rawDispWriteBuffNdx ^= 0x1;
        for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
                hrtCamTTDataPtr->rawDispBuffs[hrtCamTTDataPtr->rawDispWriteBuffNdx][i][j] = tmpDest[i][j];
        }
        hrtCamTTDataPtr->rawDispReadBuffNdx = hrtCamTTDataPtr->rawDispWriteBuffNdx;
    }

    if ( tmpWrtNdx % LegalFrameRatesTT[ hrt_camTTFrameRate ] == 0 )
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_LoadDataBuffer): Buf #", tmpWrtNdx  );

    if( ++tmpWrtNdx >= TTNUM_HRT_DATA_BUFFS)
    {
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_LoadDataBuffer): Normal Buffer Wrap, WriteNdx = ", tmpWrtNdx);
        tmpWrtNdx = 0;
    }
    hrtCamTTDataPtr->camWriteNdx = tmpWrtNdx;
#   endif   /* USE_TIP_TILT */
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_DeIntlv128x86( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x86 length camera frame into a 96x96 destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x86 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
gpAoHrtCamTT_DeIntlv128x86( unsigned short *src, unsigned short dest[TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW])
{
    int i, j, k;
    int colCntr, colSkipper, rowCntr;
    static boolean firstTime = TRUE;

    if (hrt_useTtSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[TTNUM_SEGS_IN_CAM_160] = { 12, 8, 4, 0, 2, 6, 10, 14, 15, 11, 7, 3, 1, 5, 9, 13 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DeIntlv128x86) Setting up de-interleave map");
            firstTime = false;

            // first create a map to deinterleave into 128 element rows
            for (i = 0; i < (TTNUM_ROWS_IN_CAM_ACTUAL / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (TTNUM_SEGS_IN_CAM / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < TTNUM_PIX_IN_SEG_ROW; k++)
                    {
                        tmpMap[i * TTNUM_PIX_IN_ROW_ACTUAL +
                               j * TTNUM_PIX_IN_SEG_ROW +
                               k] =
                            i * TTNUM_PIX_IN_ROW_ACTUAL * 2 + k * TTNUM_SEGS_IN_CAM + segOrder[j];

                        tmpMap[(TTNUM_ROWS_IN_CAM_ACTUAL - 1 - i) * TTNUM_PIX_IN_ROW_ACTUAL +
                               (TTNUM_SEGS_IN_CAM / 2 - 1 - j) * TTNUM_PIX_IN_SEG_ROW +
                               (TTNUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * TTNUM_PIX_IN_ROW_ACTUAL * 2 + k * TTNUM_SEGS_IN_CAM + segOrder[j + TTNUM_SEGS_IN_CAM / 2];
                    }
                }
            }

            // now skip every third column on a per row basis -- this gets us down to the final map
            k = 0;
            for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
                {
                    if (((j + 1) % 3) > 0)
                    {
                        deIntlvMap[k] = tmpMap[i * TTNUM_PIX_IN_ROW_ACTUAL + j];
                        k++;
                    }
                }
            }
        }

        // deinterleave the source array into the destination matrix, offsetting the destination matrix as necessary
        // > (IMPORTANT: it is assumed that the unused part of the destination matrix is never written to; so, it is
        // > not set to zeroes here)
        k = 0;
        rowCntr = 0;                                    // this is for dispraw and can be removed when dispraw isn't needed
        for (i = TTROWS_OFFSET; i < (TTROWS_OFFSET + TTNUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;                                // this is for dispraw and can be removed when dispraw isn't needed
            colSkipper = 0;                             // this is for dispraw and can be removed when dispraw isn't needed
            for (j = TTPIX_IN_ROW_OFFSET; j < (TTPIX_IN_ROW_OFFSET + TTNUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[deIntlvMap[k]];
                tmpDest[rowCntr][colCntr] = dest[i][j]; // this is for dispraw and can be removed when dispraw isn't needed
                colCntr += 1 + colSkipper;              // this is for dispraw and can be removed when dispraw isn't needed
                colSkipper ^= 0x1;                      // this is for dispraw and can be removed when dispraw isn't needed
                k++;
            }
            rowCntr++;                                  // this is for dispraw and can be removed when dispraw isn't needed
        }
    }
    else
    {
        // save data for dispraw
        k = 0;
        for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
            }
        }

        // don't de-interleave, if we're using simulated data (but skip every third column)
        rowCntr = 0;
        for (i = TTROWS_OFFSET; i < (TTROWS_OFFSET + TTNUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;
            colSkipper = 0;
            for (j = TTPIX_IN_ROW_OFFSET; j < (TTPIX_IN_ROW_OFFSET + TTNUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[rowCntr * TTNUM_PIX_IN_ROW_ACTUAL + colCntr];
                colCntr += 1 + colSkipper;
                colSkipper ^= 0x1;
            }
            rowCntr++;
        }
    }
}

/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_DeIntlv128x128( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x128 length camera frame into a 96x96 length destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x128 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *  This unit also calls a function to display the raw camera data to the monitor
 *  at 20 fps.  For this reason and because de-interleaving is less efficient, this
 *  function is quite a bit slower than gpAoHrtCamTT_DeIntlv128x86 and, so,  should only
 *  be used for test purposes.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- start XXX
// XXX this could definitely be more efficient !! XXX
static void
gpAoHrtCamTT_DeIntlv128x128( unsigned short *src, unsigned short dest[TTNUM_ROWS_IN_CAM][TTNUM_PIX_IN_ROW])
{
    int i, j, k;
    int rowCntr, rowSkipper, colCntr, colSkipper;
    static boolean firstTime = TRUE;

    if ( ++CheckCntr >= TEST_INTERVAL * LegalFrameRatesTT[hrt_camTTFrameRate] )
    {
        CheckCntr = 0;
        Cntr = 1;
    }

    if (hrt_useTtSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[TTNUM_SEGS_IN_CAM_160] = { 3, 13, 0, 10, 4, 14, 1, 11, 2, 12, 17, 7, 16, 6, 19, 9, 15, 5, 18, 8 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DeIntlv128x128) Setting up de-interleave map");
            firstTime = false;

            // create a map to deinterleave into 128 element rows
            for (i = 0; i < (TTNUM_ROWS_IN_CAM_ACT_160 / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (TTNUM_SEGS_IN_CAM_160 / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < TTNUM_PIX_IN_SEG_ROW; k++)
                    {
                        tmpMap160[i * TTNUM_PIX_IN_ROW_ACT_160 +
                                  j * TTNUM_PIX_IN_SEG_ROW +
                                  k] =
                            i * TTNUM_PIX_IN_ROW_ACT_160 * 2 + k * TTNUM_SEGS_IN_CAM_160 + segOrder[j];

                        tmpMap160[(TTNUM_ROWS_IN_CAM_ACT_160 - 1 - i) * TTNUM_PIX_IN_ROW_ACT_160 +
                                  (TTNUM_SEGS_IN_CAM_160 / 2 - 1 - j) * TTNUM_PIX_IN_SEG_ROW +
                                  (TTNUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * TTNUM_PIX_IN_ROW_ACT_160 * 2 + k * TTNUM_SEGS_IN_CAM_160 + segOrder[j + TTNUM_SEGS_IN_CAM_160 / 2];
                    }
                }
            }

            //                      rotate the map 180 degrees
            for (i = 0; i < TTNUM_ROWS_IN_CAM_ACT_160; i++)
            {
                for (j = 0; j < TTNUM_PIX_IN_ROW_ACT_160; j++)
                    deIntlvMap160[i * TTNUM_PIX_IN_ROW_ACT_160 + j] = tmpMap160[i * TTNUM_PIX_IN_ROW_ACT_160 + j];
            }
        }

        //      deinterleave the source array into a temporary destination matrix -- deinterleaving could be done more quickly if
        //      > we didn't go through tmpDest[][] -- but, for now, tmpDest[][] is needed for dispraw
        k = 0;  // mrr DM

        rowCntr = TTROWS_OFFSET_160;                    // mrr 48
        for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)    // mrr 64
        {
            colCntr = TTPIX_IN_ROW_OFFSET_160;      // mrr 48
            for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)     // mrr 64
            {   // starting at row 48, col 48 copy 64 pixel for 64 rows.  tmpDest now holds the center 64x64 pixels of the 160x160 array
                tmpDest[i][j] = src[deIntlvMap160[rowCntr * TTNUM_PIX_IN_ROW_ACT_160 + colCntr]] >> 2;    // mrr DM       // mrr 160 // mrr why >> 2? to reduce 16 bits to 14 ???
                //tmpDest[i][j] = k++;
                colCntr++;
            }
            rowCntr++;
        }

        // now skip every fifth column on a per row basis and every fifth row, offsetting the destination matrix
        // > as necessary -- this gets us down to the final frame for processing
        rowCntr = 12;   // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = TTROWS_OFFSET; i < (TTROWS_OFFSET + TTNUM_ROWS_IN_CAM_FOR_SUBAPS); i++)      // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;   // the start in tmpDest
            colSkipper = 0;
            for (j = TTPIX_IN_ROW_OFFSET; j < (TTPIX_IN_ROW_OFFSET + TTNUM_PIX_IN_ROW_FOR_SUBAPS); j++)   // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {   // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {   // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }       // end of not using sim data
    else
    {       // using sim data
        //  save data for dispraw
        k = 0;
        for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
                //tmpDest[i][j] = src[i][j];
                if (Cntr == 1)
                {
                //      printf( "%d ", tmpDest[i][j]);
                }
            }
            if (Cntr == 1)
            {
                //printf( "\n ");
            }
        }

        rowCntr = 12;   // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = TTROWS_OFFSET; i < (TTROWS_OFFSET + TTNUM_ROWS_IN_CAM_FOR_SUBAPS); i++)      // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;   // the start in tmpDest
            colSkipper = 0;
            for (j = TTPIX_IN_ROW_OFFSET; j < (TTPIX_IN_ROW_OFFSET + TTNUM_PIX_IN_ROW_FOR_SUBAPS); j++)   // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {   // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {   // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }
    Cntr = 0;
}

// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- end XXX


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_DrvrWatch( void* arg )
 *
 *  @brief
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the tt camera or load simulated data when in user-side mode.
 *
 *  @b DESCRIPTION:
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the tt camera or load simulated data when in user-side mode.
 *  @par
 *  When in RTL mode, the thread continually sets a 7 second timer and, whenever
 *  it times out, checks a TT camera frame counter to make sure that frames have
 *  been received.  If no frames have been received, an error condition is raised.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_DrvrWatch(void* arg);
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception exceptionCameraTimout  The camera frames have been missing for > 7 sec.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
gpAoHrtCamTT_DrvrWatch( void *arg )
{
    struct rtl_timespec next, remaining;

    SysConSendStrngIntParm(eDebugLevelMax, "INFO Camera Type: ", CAMTT_TYPE);

#if (RTL_SYSTEM == 0)
    cpu_set_t mask;
    int               cpuNum = HRT_CPU_TT_CAM;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DrvrWatch) - CPU #",  cpuNum );
    if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCamTT_DrvrWatch) executing sched_setaffinity() for CPU #",  cpuNum );
    sched_getaffinity( 0, sizeof( mask ), &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DrvrWatch) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DrvrWatch) CamWatch thread id: ", rtl_pthread_self());
#endif

    //  spin on the InitDone flags for the other threads
    while (!CentInitDone || !ReconInitDone || !CamWFSInitDone || !CamTTInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;  // 100mS
        while( rtl_nanosleep( &next, &remaining ) < 0 )
            memcpy( &next, &remaining, sizeof( next ) );
    }
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCamTT_DrvrWatch) Initialization appears to be done");

    while (!QuitCamTTWatchThread)
    {
        boolean postTtSem;
        postTtSem = false;

#if (CAMTT_TYPE != TT_REAL_GPI)  // no real camera
        {
            // if we don't have a real camera, use the system clock to simulate a frame rate
            next.tv_sec = 0;
            next.tv_nsec = 999999999L;      // 1 Sec, even if (float)LegalFrameRatesTT[ hrt_camTTFrameRate ] not set correctly
            if( LegalFrameRatesTT[ hrt_camTTFrameRate ] != 0 )
                next.tv_nsec = 999999999L / LegalFrameRatesTT[ hrt_camTTFrameRate ];
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );

            hrt_useTtSimData = 1;  // make sure we're using simulated data
            postTtSem = true;

#if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#endif  /* TIMER_TEST */
        }

#else  // (CAMTT_TYPE == TT_REAL_GPI)
#if (RTL_SYSTEM == 0) // real camera, user-side mode -- this will only work for the user-side code!!
        {
            static int timeouts, last_timeouts = 0, recovering_timeout = FALSE;

            pdv_wait_image(TT_dev);           // wait for the last image
#if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#endif  /* TIMER_TEST */
            pdv_start_images(TT_dev, 1);  // start the next image

            // check for a camera timeout
            timeouts = pdv_timeouts(TT_dev);
            if (timeouts > last_timeouts)
            {
                pdv_timeout_restart(TT_dev, TRUE);
                last_timeouts = timeouts;
                recovering_timeout = TRUE;
                SysConSendStrngIntParm(eDebugLevelWarn, "WARN - SMC TT camera timeout", timeouts);
            }
            else if (recovering_timeout)
            {
                pdv_timeout_restart(TT_dev, TRUE);
                recovering_timeout = FALSE;
            }
            else
                postTtSem = true;
        }

#else  // real camera, RTL mode -- this is just the camera watchdog timer
        {
            // wait for 7 seconds (increased from 1 sec to accommodate longer delay when switching camera programs)
            next.tv_sec = 7;
            next.tv_nsec = 0;
            while( rtl_nanosleep( &next, &remaining ) < 0 )
                memcpy( &next, &remaining, sizeof( next ) );

            // make sure we've received camera frames and, if not - inform the user
            if (CamTTIntrWatchCntr == 0)
            {
                CamTTIntrWatchCntr = -1;
                SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCamTT_DrvrWatch) SMC TT camera watchdog timeout");
            }
            else if (CamTTIntrWatchCntr > 0)
                CamTTIntrWatchCntr = 0;
        }
#endif  /* RTL_SYSTEM */
#endif  /* CAMTT_TYPE */

#if ((CAMTT_TYPE != TT_REAL_GPI) || (RTL_SYSTEM == 0))
        // if we're using simulated data, load it
        if (hrt_useTtSimData == 1)
        {
            int curbuf;
            curbuf = edtDoneCount_sim % TTnumbufs;
            gpAoHrtCamTT_LoadSimTtData( buf_array_sim[curbuf], curbuf );
            edtDoneCount_sim++;
        }

        // if we need to post the tt semaphore, post it
        if (postTtSem)
        {
            rtl_sem_post(&TTSem);
        }
#endif
    }
    return NULL;
}


/*!
 *******************************************************************************
 *  @fn gpAoHrtCamTT_LoadSimTtData( unsigned short buf_array[], int curbuf )
 *
 *  @brief
 *  This unit loads simulated tt data into the buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated tt data into the buffer supplied either from a
 *  file (loaded at system startup) or by generating it in one of several ways,
 *  depending on how flags are set in the code.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_LoadSimTtData(&buf_array, curbuf);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *  @param[in] curbuf:  current tt buffer (used to choose the right simulated data)
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
enum { eSimFromFile, eSimRamp, eSimTipTilt, eSimPupilMove, eSimZernike, eSimCombo };
//const static int simSelect = eSimZernike;  // choose simulated data   // mrr
const static int simSelect = eSimFromFile;  // choose simulated data
static unsigned short buf_array2[TTNUM_ROWS_IN_CAM_ACTUAL * TTNUM_PIX_IN_ROW_ACTUAL];

static void
gpAoHrtCamTT_LoadSimTtData( unsigned short buf_array[], int curbuf )
{
    static int  testVal = 0;
    int         i, j;

    if (buf_array == NULL)  // make sure the buf_array pointer exists
        return;

    switch (simSelect)
    {
        case eSimFromFile:
        default:
            //      if file-based data is available, use it; otherwise, zero the buffer
            if( hrtCamTTDataPtr->simFileRead  )
            {
                for (i = 0; i < (TTNUM_ROWS_IN_CAM_ACTUAL); i++)
                {
                    for (j = 0; j < (TTNUM_PIX_IN_ROW_ACTUAL ); j++)
                    {
                        // buf_array[i * TTNUM_PIX_IN_ROW_ACTUAL + j] = hrtCamTTDataPtr->simDataBuffs[curbuf][i][j];        // mrr
                        buf_array[i * TTNUM_PIX_IN_ROW_ACTUAL + j] = hrtCamTTDataPtr->simDataBuffs[0][i][j];        // we only loaded the first buffer // mrr
                    }
                }
            }
            else  // in case the file doesn't exist, zero the buffer
            {
                for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
                {
                    for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
                        buf_array[i * TTNUM_PIX_IN_ROW_ACTUAL + j] = 0.0;
                }
            }
            break;

        case eSimRamp:
            // just loop through sequential data values
            for (i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < TTNUM_PIX_IN_ROW_ACTUAL; j++)
                {
                    buf_array[i * TTNUM_PIX_IN_ROW_ACTUAL + j] = testVal;
                    if (++testVal > 12345)
                        testVal = 0;
                }
            }
            break;

        case eSimTipTilt:
            gpAoHrtCamTT_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the tt
            break;

        case eSimPupilMove:
            gpAoHrtCamTT_LoadSimPupilMoveData( buf_array );  // put low frequency plus high frequency pupil move data, in X and or Y
            break;

        case eSimZernike:
            gpAoHrtCamTT_LoadSimZernikes( buf_array );  // put Zernike modes on the tt
            break;

        case eSimCombo:
            gpAoHrtCamTT_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the tt
            gpAoHrtCamTT_LoadSimZernikes( buf_array2 );  // put Zernike modes on the tt
            for ( i = 0; i < TTNUM_ROWS_IN_CAM_ACTUAL * TTNUM_PIX_IN_ROW_ACTUAL; i++)
                buf_array[i] = buf_array[i] / 2 + buf_array2[i] / 2;
            break;
    }
}

/*!
 *******************************************************************************
 *  @fn gpAoHrtCamTT_LoadSimTTData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the tt buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the tt buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_LoadSimTTData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static const boolean makeTip  = true;  // turn tip on or off
static const boolean makeTilt = true;  // turn tilt on or off
static const boolean makeSquare = true;  // make a square wave instead of a sine wave
static const int lowFreqTT  = 1;  // 7;  //  25;  // low frequency TT in Hz  -- these may be approximate, depending on how
static const int highFreqTT = 1;  // 100;  // high frequency TT in Hz   -- > well they divide into the frame rate
static const float lowFreqGainTT  = 0.50;  // gain to be applied to low frequency TT (0.0 means no low freq)
static const float highFreqGainTT = 0.00;  // gain to be applied to high frequency TT (0.0 means no high freq)

static void
gpAoHrtCamTT_LoadSimTTData( unsigned short buf_array[] )
{
int         i, j;
int         rowCntr, rowSkipper, colCntr, colSkipper;
int         stepsPerCycleLow, stepsPerCycleHigh;
static int  stepCntrLow = -1, stepCntrHigh = -1;
float       lowLeftBot, lowRightTop, highLeftBot, highRightTop; // these are for low and high frequency -- left/bottom
                                                                // > pixel (for tip/tilt) and right/top pixel

    float tipVector[TTNUM_PIX_IN_ROW_FOR_SUBAPS], tiltVector[TTNUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up tip and tilt vectors
    if (lowFreqTT <= 0)
        stepsPerCycleLow = 1;
    else
        stepsPerCycleLow = LegalFrameRatesTT[hrt_camTTFrameRate] / lowFreqTT;
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;

    if (highFreqTT <= 0)
        stepsPerCycleHigh = 1;
    else
        stepsPerCycleHigh = LegalFrameRatesTT[hrt_camTTFrameRate] / highFreqTT;
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowLeftBot   = (1.0 + sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    lowRightTop  = (1.0 - sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    highLeftBot  = (1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;
    highRightTop = (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;

    if (makeSquare)
    {
        if (lowLeftBot < 4000)
            lowLeftBot = 0;
        else
            lowLeftBot = 1000;

        if (lowRightTop < 4000)
            lowRightTop = 0;
        else
            lowRightTop = 1000;

        if (highLeftBot < 4000)
            highLeftBot = 0;
        else
            highLeftBot = 1000;

        if (highRightTop < 4000)
            highRightTop = 0;
        else
            highRightTop = 1000;
    }

    if (!makeTip)
    {
        for (i = 0; i < TTNUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            tipVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < TTNUM_PIX_IN_ROW_FOR_SUBAPS; i += 2)
        {
            tipVector[i    ] = lowLeftBot  + highLeftBot;
            tipVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    if (!makeTilt)
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            tiltVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
        {
            tiltVector[i    ] = lowLeftBot  + highLeftBot;
            tiltVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    // combine the tip and tilt data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * TTNUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(tiltVector[i] + tipVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( TTNUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_LoadSimPupilMoveData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the tt buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the tt buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_LoadSimPupilMoveData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static const boolean makePupilMoveX = true;  // turn X on or off
static const boolean makePupilMoveY = true;  // turn Y on or off

static const float lowFreqPM  = 0.1;  // low frequency pupil move in Hz  -- these may be approximate, depending on how
static const float highFreqPM = 1.0;  // high frequency pupil move in Hz -- > well they divide into the frame rate

static const float lowFreqGainPM  = 1.0;  // gain to be applied to low frequency PM (0.0 means no low freq)
static const float highFreqGainPM = 1.0;  // gain to be applied to high frequency PM (0.0 means no high freq)

static void
gpAoHrtCamTT_LoadSimPupilMoveData( unsigned short buf_array[] )
{
    int         i, j;
    int         rowCntr, rowSkipper, colCntr, colSkipper;
    int         stepsPerCycleLow, stepsPerCycleHigh;
    static int  stepCntrLow = -1, stepCntrHigh = -1;
    float       lowStart, lowIncr, highStart, highIncr;
    float       xVector[TTNUM_PIX_IN_ROW_FOR_SUBAPS], yVector[TTNUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up X and Y vectors
    stepsPerCycleLow = (int)((float)LegalFrameRatesTT[hrt_camTTFrameRate] / lowFreqPM);
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;
    stepsPerCycleHigh = (int)((float)LegalFrameRatesTT[hrt_camTTFrameRate] / highFreqPM);
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowStart =   (1.0 - sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0;
    lowIncr  = (((1.0 + sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0) - lowStart) /
        TTNUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if TTNUM_PIX_IN_ROW_FOR_SUBAPS == TTNUM_ROWS_IN_CAM_FOR_SUBAPS

    highStart =   (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0;
    highIncr  = (((1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0) - highStart) /
        TTNUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if TTNUM_PIX_IN_ROW_FOR_SUBAPS == TTNUM_ROWS_IN_CAM_FOR_SUBAPS

    if (!makePupilMoveX)
    {
        for (i = 0; i < TTNUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < TTNUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    if (!makePupilMoveY)
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    // combine the X and Y data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * TTNUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(yVector[i] + xVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( TTNUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_LoadSimZernikes( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated Zernike modes into the tt buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated Zernike modes into the tt buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_LoadSimZernikes(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

#define MAX_Z_COEFFS    66
#define NUM_M1_M2_ZERNS 20

static const float zernFreq =   0.0;  // frequency at which to modulate Zernike modes -- 0.0 means no modulation
static const float zernGain =   0.5;  // gain to be applied to Zernike modes

// Set each of the following to between -1 and 1
static const float
zernSelect[NUM_M1_M2_ZERNS] =
{
     0.0,   // Z0   Piston -- not used, leave it set to zero
     0.0,   // Z1   Tip     M2
     0.0,   // Z2   Tilt    M2
     1.0,   // Z3   Focus   M2
     0.0,   // Z4   Astigmatism (cosine)            M1
     0.0,   // Z5   Astigmatism (sine)              M1
     1.0,   // Z6   Coma x                          M1
     0.0,   // Z7   Coma y                          M1
     0.0,   // Z8   Spherical                       M1
     1.0,   // Z9   Trefoil (cosine)                M1
     0.0,   // Z10  Trefoil (sine)                  M1
     0.0,   // Z11  Secondary astigmatism (cosine)  M1
     1.0,   // Z12  Secondary astigmatism (sine)    M1
     0.0,   // Z13  Secondary coma x                M1
     0.0,   // Z14  Secondary coma y                M1
     0.0,   // Z15  Secondary spherical             M1
     0.0,   // Z16  Tetrafoil x                     M1
     0.0,   // Z17  Tetrafoil y                     M1
     0.0,   // Z18  Secondary trefoil x             M1
     0.0,   // Z19  Secondary trefoil y             M1
};

static float pupilZernikes[MAX_Z_COEFFS][TTNUM_ROWS_IN_CAM_FOR_SUBAPS][TTNUM_PIX_IN_ROW_FOR_SUBAPS];
static float zernikes[TTNUM_PIX_IN_ROW_FOR_SUBAPS][TTNUM_ROWS_IN_CAM_FOR_SUBAPS];  // shape constructed from selected Zernike modes

static void
gpAoHrtCamTT_LoadSimZernikes( unsigned short buf_array[] )
{
    static boolean  firstTimeFlag = true;
    int             i, j, k;
    int             rowCntr, rowSkipper, colCntr, colSkipper;
    float           minVal, maxVal;

    int             stepsPerCycle;
    static int      stepCntr = -1;
    float           freqMultiplier;

    // if it hasn't been done yet, calculate the Zernike modes use to construct the shape
    // > to be put in the tt buffer
    if (firstTimeFlag)
    {
        firstTimeFlag = false;
        gpAoHrtCamTT_ComputeZernikes();
    }

    // now, construct the shape from the selected Zernike modes
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            zernikes[i][j] = 0.0;
    }
    for (k = 0; k < NUM_M1_M2_ZERNS; k++)
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
                zernikes[i][j] += pupilZernikes[k][i][j] * zernSelect[k];
        }
    }

    // so the sub-aps look more like slopes, subtract the minimum value from each one (this
    // > will also make all the values >= zero)
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
    {
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j += 2)
        {
            minVal = (float)1.0e10;
            if (zernikes[i    ][j    ] < minVal) minVal = zernikes[i    ][j    ];
            if (zernikes[i    ][j + 1] < minVal) minVal = zernikes[i    ][j + 1];
            if (zernikes[i + 1][j    ] < minVal) minVal = zernikes[i + 1][j    ];
            if (zernikes[i + 1][j + 1] < minVal) minVal = zernikes[i + 1][j + 1];
            zernikes[i    ][j    ] -= minVal;
            zernikes[i    ][j + 1] -= minVal;
            zernikes[i + 1][j    ] -= minVal;
            zernikes[i + 1][j + 1] -= minVal;
        }
    }

    // calculate the multiplier for this step in the sin wave modulation (the Zernike modes shape can
    // > be temporally modulated to test the M1 and M2 offloading filters)
    if (zernFreq == 0.0)
        freqMultiplier = zernGain;
    else
    {
        stepsPerCycle = (int)((float)LegalFrameRatesTT[hrt_camTTFrameRate] / zernFreq);
        if (++stepCntr >= stepsPerCycle)
            stepCntr = 0;
        freqMultiplier = sin((float)stepCntr / (float)stepsPerCycle * TWO_PI) * zernGain;  // apply the gain here, too
    }

    // modulate the shape
    maxVal = 0.0;
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            if (zernikes[i][j] > maxVal)
                maxVal = zernikes[i][j];
        }
    }
    if (maxVal > 0.0)
    {
        for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            {
                zernikes[i][j] =  1.0 - zernikes[i][j] / maxVal * 2.0;  // normalize to between -1.0 and 1.0
                zernikes[i][j] =  1.0 - zernikes[i][j] * freqMultiplier;// this will now be between 0.0 and 2.0, centered around 1.0
                zernikes[i][j] *= 8000.0;                               // put into DN-like units (the tt camera is 14-bits)
            }
        }
    }

    // put the zernike modes into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < TTNUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * TTNUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)zernikes[i][j];
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( TTNUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCamTT_ComputeZernikes( void )
 *
 *  @brief
 *  This unit calculates values for all Zernike modes.
 *
 *  @b DESCRIPTION:
 *  This unit calculates values for all Zernike modes.
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtCamTT_ComputeZernikes();
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static double rho[TTNUM_ROWS_IN_CAM_FOR_SUBAPS][TTNUM_PIX_IN_ROW_FOR_SUBAPS];
static double theta[TTNUM_ROWS_IN_CAM_FOR_SUBAPS][TTNUM_PIX_IN_ROW_FOR_SUBAPS];

static void
gpAoHrtCamTT_ComputeZernikes( void )
{
    int i, j, k;
    // IMPORTANT: this will only work right if
    // TTNUM_PIX_IN_ROW_FOR_SUBAPS == TTNUM_ROWS_IN_CAM_FOR_SUBAPS
    int xyOffset = TTNUM_PIX_IN_ROW_FOR_SUBAPS / 2;
    double x, y;
    double rho1, rho2, rho3, rho4, rho5, rho6, rho7, rho8, rho9, rho10;

    for (i = 0; i < MAX_Z_COEFFS; i++)
    {
        for (j = 0; j < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
        {
            for (k = 0; k < TTNUM_PIX_IN_ROW_FOR_SUBAPS; k++)
                pupilZernikes[i][j][k] = 0.0;
        }
    }

    // we're putting the zernike modes on a square grid; but, the equations are easier
    // > in polar form; so, compute rho and theta for each point
    for (j = 0; j < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        x = (double)(j - xyOffset);

        for (k = 0; k < TTNUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            y = (double)(k - xyOffset);

            rho[j][k] = sqrt(x * x + y * y) / (double)xyOffset;  // rho will be between 0 and 1
            theta[j][k] = atan2(y, x);  // theta will be between -pi and pi
        }
    }

    for (j = 0; j < TTNUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        for (k = 0; k < TTNUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            if (rho[j][k] >= 0.0)
            {
                rho1  = rho[j][k];
                rho2  = rho1 * rho[j][k];
                rho3  = rho2 * rho[j][k];
                rho4  = rho3 * rho[j][k];
                rho5  = rho4 * rho[j][k];
                rho6  = rho5 * rho[j][k];
                rho7  = rho6 * rho[j][k];
                rho8  = rho7 * rho[j][k];
                rho9  = rho8 * rho[j][k];
                rho10 = rho9 * rho[j][k];

/*  Z0 */       pupilZernikes[0][j][k] = 1.0;  // 0 - piston

/*  Z1 */       pupilZernikes[1][j][k] = (float)(rho1 * cos(theta[j][k]));                                      // 2 - tilt
/*  Z2 */       pupilZernikes[2][j][k] = (float)(rho1 * sin(theta[j][k]));                                      // 1 - tip
/*  Z3 */       pupilZernikes[3][j][k] = (float)(2.0 * rho2 - 1.0);                                             // 4 - focus

/*  Z4 */       pupilZernikes[4][j][k]  = (float)(rho2 * cos(2.0 * theta[j][k]));                               // 5 - astig with axis at 0 or pi/2
/*  Z5 */       pupilZernikes[5][j][k]  = (float)(rho2 * sin(2.0 * theta[j][k]));                               // 3 - astig with axis at +/- pi/4
/*  Z6 */       pupilZernikes[6][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * cos(theta[j][k]));                // 8 - third order coma along y axis
/*  Z7 */       pupilZernikes[7][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * sin(theta[j][k]));                // 7 - third order coma along x axis
/*  Z8 */       pupilZernikes[8][j][k]  = (float)(6.0 * rho4 - 6.0 * rho2 + 1.0);                               // 12 - third order spherical
/*  Z9 */       pupilZernikes[9][j][k]  = (float)(rho3 * cos(3.0 * theta[j][k]));                               // 9 - triangular astig with base on y axis
/* Z10 */       pupilZernikes[10][j][k] = (float)(rho3 * sin(3.0 * theta[j][k]));                               // 6 - triangular astig with base on x axis
/* Z11 */       pupilZernikes[11][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * cos(2.0 * theta[j][k]));          // 13
/* Z12 */       pupilZernikes[12][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * sin(2.0 * theta[j][k]));          // 11
/* Z13 */       pupilZernikes[13][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * cos(theta[j][k])); // 18
/* Z14 */       pupilZernikes[14][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * sin(theta[j][k])); // 17
/* Z15 */       pupilZernikes[15][j][k] = (float)(20.0 * rho6 - 30.0 * rho4 + 12.0 * rho2 - 1.0);               // 24
/* Z16 */       pupilZernikes[16][j][k] = (float)(rho4 * cos(4.0 * theta[j][k]));                               // 14
/* Z17 */       pupilZernikes[17][j][k] = (float)(rho4 * sin(4.0 * theta[j][k]));                               // 10
/* Z18 */       pupilZernikes[18][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3)                                                 * cos(3.0 * theta[j][k])); // 19
/* Z19 */       pupilZernikes[19][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3)                                                 * sin(3.0 * theta[j][k])); // 16

                pupilZernikes[20][j][k] = (float)(        rho5                                                               * sin(5.0 * theta[j][k])); // 15
                pupilZernikes[21][j][k] = (float)(        rho5                                                               * cos(5.0 * theta[j][k])); // 20

                pupilZernikes[22][j][k] = (float)(        rho6                                                               * sin(6.0 * theta[j][k])); // 21
                pupilZernikes[23][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)                                                * sin(4.0 * theta[j][k])); // 22
                pupilZernikes[24][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2)                                  * sin(2.0 * theta[j][k])); // 23
                pupilZernikes[25][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2)                                  * cos(2.0 * theta[j][k])); // 25
                pupilZernikes[26][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)                                                * cos(4.0 * theta[j][k])); // 26
                pupilZernikes[27][j][k] = (float)(        rho6                                                               * cos(6.0 * theta[j][k])); // 27

                pupilZernikes[28][j][k] = (float)(        rho7                                                               * sin(7.0 * theta[j][k])); // 28
                pupilZernikes[29][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                                                * sin(5.0 * theta[j][k])); // 29
                pupilZernikes[30][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)                                  * sin(3.0 * theta[j][k])); // 30
                pupilZernikes[31][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1)                     * sin(    theta[j][k]));   // 31
                pupilZernikes[32][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1)                     * cos(    theta[j][k]));   // 32
                pupilZernikes[33][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)                                  * cos(3.0 * theta[j][k])); // 33
                pupilZernikes[34][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                                                * cos(5.0 * theta[j][k])); // 34
                pupilZernikes[35][j][k] = (float)(        rho7                                                               * cos(7.0 * theta[j][k])); // 35

                pupilZernikes[36][j][k] = (float)(        rho8                                                               * sin(8.0 * theta[j][k])); // 36
                pupilZernikes[37][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                                               * sin(6.0 * theta[j][k])); // 37
                pupilZernikes[38][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)                                 * sin(4.0 * theta[j][k])); // 38
                pupilZernikes[39][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2)                   * sin(2.0 * theta[j][k])); // 39
                pupilZernikes[40][j][k] = (float)( 70.0 * rho8 - 140.0 * rho6 + 90.0 * rho4 - 20.0 * rho2 + 1.0);                                       // 40
                pupilZernikes[41][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2)                   * cos(2.0 * theta[j][k])); // 41
                pupilZernikes[42][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)                                 * cos(4.0 * theta[j][k])); // 42
                pupilZernikes[43][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                                               * cos(6.0 * theta[j][k])); // 43
                pupilZernikes[44][j][k] = (float)(        rho8                                                               * cos(8.0 * theta[j][k])); // 44

                pupilZernikes[45][j][k] = (float)(         rho9                                                              * sin(9.0 * theta[j][k])); // 45
                pupilZernikes[46][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                                              * sin(7.0 * theta[j][k])); // 46
                pupilZernikes[47][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                               * sin(5.0 * theta[j][k])); // 47
                pupilZernikes[48][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)                 * sin(3.0 * theta[j][k])); // 48
                pupilZernikes[49][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1)    * sin(      theta[j][k])); // 49
                pupilZernikes[50][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1)    * cos(      theta[j][k])); // 50
                pupilZernikes[51][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)                 * cos(3.0 * theta[j][k])); // 51
                pupilZernikes[52][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                               * cos(5.0 * theta[j][k])); // 52
                pupilZernikes[53][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                                              * cos(7.0 * theta[j][k])); // 53
                pupilZernikes[54][j][k] = (float)(         rho9                                                              * cos(9.0 * theta[j][k])); // 54

                pupilZernikes[55][j][k] = (float)(         rho10                                                             * sin(10.0 * theta[j][k]));// 55
                pupilZernikes[56][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                                             * sin( 8.0 * theta[j][k]));// 56
                pupilZernikes[57][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                              * sin( 6.0 * theta[j][k]));// 57
                pupilZernikes[58][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)               * sin( 4.0 * theta[j][k]));// 58
                pupilZernikes[59][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * sin( 2.0 * theta[j][k]));// 59
                pupilZernikes[60][j][k] = (float)( 252.0 * rho10 - 630.0 * rho8 + 560.0 * rho6 - 210.0 * rho4 + 30.0 * rho2 - 1.0);                     // 60
                pupilZernikes[61][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * cos( 2.0 * theta[j][k]));// 61
                pupilZernikes[62][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)               * cos( 4.0 * theta[j][k]));// 62
                pupilZernikes[63][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                              * cos( 6.0 * theta[j][k]));// 63
                pupilZernikes[64][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                                             * cos( 8.0 * theta[j][k]));// 64
                pupilZernikes[65][j][k] = (float)(         rho10                                                             * cos(10.0 * theta[j][k]));// 65
            }
        }
    }
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
