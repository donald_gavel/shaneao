#ifndef HOST_ICON_CENTRAL_H
#define HOST_ICON_CENTRAL_H

#include <semaphore.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

#include "gpUtGmbHrt.h" // mrr

#include "gpAoConstant.h"
#include "WFSCam.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtCamMngrTT.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoShmMap1.h"
#include "gpAoShmMap2.h"

#include "fitsio.h"

#include "gpAoStubs.h"
#include "gpAoHrtSysCon.h"


#include "eventthread.h"
#include "hostIConListener.h"
#include "updateval.h"
#include "virtktlkeys.h"
#include "inter.h"


class CommandData {
 public:
    CommandData();
    ~CommandData(){};

    int   m_cmdNdx;

    int   m_intParam[MAX_NUM_CMD_PARAMS];
    float m_floatParam[MAX_NUM_CMD_PARAMS];
    char  m_strngParam[MAX_NUM_CMD_PARAMS][MAX_FILENAME_LENGTH];
};



class CCentral : public CEventThread
{
public:
    CCentral();
    ~CCentral();

    // Status message ids
    enum {
        SUCCESS = 0,
        FAILURE = -1
    };

    enum {
        ACKED = 9001
    };

    // Message ID that this event thread understands.
    enum {
	FIRST_CONTROL_MSG                      = 2000,
	CONTROL_INFO_UPDATE                    = 2001,
	UPDATED_PARAMS                         = 2002,
	HOSTICONPROCCMD                        = 2003,
	HOST_SEND_STRNG_NOPARM                 = 2004,
	HOST_SEND_STRNG_INT                    = 2005,
	HOST_SEND_STRNG_FLT                    = 2006,
	EXAM_DIAG_CMD_ERR_STRNG_NO_PARM        = 2007,
	UPDATE_MSG                             = 2008,
	DIAG_CMD_RESPONSE                      = 2009,
        UPDATE_TELEM                           = 2010,
        UPDATE_TRATE                           = 2011,
        UPDATE_DUMPDATA                        = 2012,
        UPDATE_SETCAMRATE                      = 2013,
        UPDATE_SETCENTTHRESH                   = 2014,
        UPDATE_CLOSEOPENLOOPS                  = 2015,
        UPDATE_QUIT                            = 2016,
        

	LAST_CONTROL_MSG  = 9999
    };

    static void* connector(void *thisPointer);

    virtual void vGetMsgRange(int *first, int *last){
	*first=FIRST_CONTROL_MSG;*last=LAST_CONTROL_MSG;};
    virtual bool vInMsgRange(int msg){
	return ((FIRST_CONTROL_MSG <= msg) && (msg <= LAST_CONTROL_MSG));};
    virtual int vStartupActions();

    // Local Variables
    int cmdFIFOfd;   // fd for FIFO used to send command messages to the HRT side
    pthread_t hostDiagThread;  // thread handles   // &&& change to CThread
    pthread_t DM_Thread;  // thread handles     // mrr   // &&& change to CThread

    // Global Function Prototype
    int queControlInfoUpdate();
    int queHostIConProcCmd(const char *msgbuf, int subCmdFlag);
    int queHostIConProcCmd(const char *msgbuf, int subCmdFlag, int length);
    int queHostIConSendStrngNoParm(int debugLevel, const char *str);
    int queHostIConSendStrngIntParm(int debugLevel, const char *str, int n);
    int queHostIConSendStrngFltParm(int debugLevel, const char *str, float f);

    int queCmdResponse(diagFIFOMsgType *pMsg);

    int queExamine_eDiagCmdErrStrngNoParm(const char *diagString);
    int queUpdatedParams();

    int queUpdateMsg(const char *str);
    int queUpdateTelem(int status, int val);
    int queUpdateTRate(int status, int val);
    int queUpdateSetCamRate(int status, int val);
    int queUpdateSetCentThresh(int status, int val);
    int queUpdateDumpData(int status, int p1, double p2, int p3);

    int setNEEDFUNC(CKTLKeyInt *pkey, int val);
    int setNEEDFUNC(CKTLKeyString *pkey, string val);
    int setNEEDFUNC(CKTLKeyFloat *pkey, double val);


    int setDWIM(string val);

    // Functions for text commands
    int  set_command_int(string fname, int id, int val, int min, int max);
    int  set_command_string(string fname, int id, string val);
    int  set_telem(int val);
    int  set_trate(int val);
    int  set_debugA(int val);
    int  set_debugB(int val);

    int  set_dumpData(int ival, int bit, double sec, int dec);
    int  set_setCamRate(int val);
    int  set_setCentThresh(int val);
    int  set_loopTweet(int val);
    int  set_loopWoof(int val);
    int  set_setShapeTweet(int val);
    int  set_setShapeWoof(int val);
    int  set_storeShapes(int val);
    int  set_cycleActuators(int val);
    int  set_stopCycling(int val);
    int  set_readConfig(int val);
    int  set_quit(int val);
    int  set_abort(int val);

    int  set_setTweeterShape(string sval);
    int  set_setWooferShape(string sval);
    int  set_setTweeterOffset(string sval);
    int  set_setWooferOffset(string sval);


    int GetCommandIndex(const char* cmdStr);
    int GetCommandIndex(int idNum);
    void ReadAperture( float *pCurrAp, short *pSubApCnt, short *pSkippedRowsTop, short *pSkippedRowsBot );

protected:
    void CenterThread();      // the main function for this thread
    int Initialize();
    void MakeCommandMaps();
    int vMsgHandler(CQMsg *pqmsg);

    int doControlInfoUpdate(CQMsg *pqmsg);
    int ControlInfoUpdate();

    int doUpdatedParams(CQMsg *pqmsg);

    int doHostIConProcCmd(CQMsg *pqmsg);
    int doHostIConSendStrngNoParm(CQMsg *pqmsg);
    int doHostIConSendStrngIntParm(CQMsg *pqmsg);
    int doHostIConSendStrngFltParm(CQMsg *pqmsg);
    int doExamine_eDiagCmdErrStrngNoParm(CQMsg *pqmsg);
    int doUpdateMsg(CQMsg *pqmsg);
    int doCmdResponse(CQMsg *pqmsg);
    int doUpdateTelem(CQMsg *pqmsg);
    int doUpdateTRate(CQMsg *pqmsg);
    int doUpdateDumpData(CQMsg *pqmsg);
    int doUpdateSetCamRate(CQMsg *pqmsg);
    int doUpdateSetCentThresh(CQMsg *pqmsg);

    int   HostIConProcCmd(const char msgBuf[], int subCmdFlag);
    int   HostIConProcCmdBase(const char msgBuf[], int subCmdFlag);
    int   HostIConProcCmdBase(CommandData &cmdData);
    void  HostIConSendStrngNoParm(int debugLevel, const char *strngPtr );
    void  HostIConSendStrngIntParm(int debugLevel, const char *strngPtr, int intParam );
    void  HostIConSendStrngFltParm(int debugLevel, const char *strngPtr, float floatParam );

    void  updateSharedMemoryVals();
    void  updateSharedMemoryFile();

    void ReadFits2D(const char *filename, int height, int width, int dataType, void *data);

    CHostIConListener *m_pHostIConListener;
    struct sockaddr_in  m_address;

    // Make certain the following get initialized to 0.0.  TODO, looks like m_tmpDatag is no longer used, delete
    float m_tmpData[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];
    float m_tmpDatag[3][NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];




    ////////////////////////////////////////////////
    //
    // This is a simple class used to trigger events
    // after short time intervals.
    //
    class CTriggerThread : public CThread
    {
    public:
	CTriggerThread();
	CTriggerThread(CCentral *pMaster);
	~CTriggerThread();

	int StopThread();

	static void* connector(void *thisPointer);

    protected:
	int Initialize();
	int TriggerLoop();
	CCentral *m_pMaster;
	CMutex m_LoopMutex;
	bool m_bLoop;
    };

    CTriggerThread *m_pTriggerThread;

 private:
    map<int, int> m_mapById;
    map<string, int> m_mapByName;

};



extern CCondVarMutex    *g_pAllDone;
extern CCentral         *g_pCCentral;

extern int g_rtl_system; // variable version of RTL_SYSTEM compilation flag so all code can be compiled.

#endif


