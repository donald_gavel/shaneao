/*******************************************************************************
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITED
 ******************************************************************************/
/*!
 *  @file gpAoHrtSysCon.c
 *
 *  @brief
 *  System Controller for the Hard-Real-Time (HRT) components.  Provides:
 *  Startup-init, Closedown-Cleanup, Command-Status interface to the SRT system.
 *
 *  @b DESCRIPTION:
 *  This component is the Hard Real Time (Hrt) system controller for the AOC AO
 *  control system. It performs the RTLinux module initialization and cleanup,
 *  semaphore initialization, and provides the task that waits for commands from
 *  the soft realtime processor (that communicates and translates command and
 *  status to and from the host) and dispatches them to other components as
 *  appropriate. Virtually all HRT system initialization is done here to
 *  simplify cleanup.
 *
 *  The gpAoHrtSysCon thread is also included in this component. Its purpose is
 *  to wait on the command FIFO from the Soft Real Time (SRT) side and dispatch
 *  commands as they are received.
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 12/17/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c initHRT() or main()
 *  -# @c cleanupHRT()
 *  -# @c gpAoHrtSysCon_GlobalsInit()
 *  -# @c gpAoHrtSysCon_GlobalsCleanup()
 *  -# @c gpAoHrtSysCon()
 *  -# @c SysConSendStrngNoParm()
 *  -# @c SysConSendStrngIntParm()
 *  -# @c SysConSendStrngFltParm()
 *  -# @c SysConSendStrngStrParm()
 *
 *  @b REVISIONS:
 *
 ********************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <semaphore.h>
#include <math.h>
#include <string.h>
#include <sys/mman.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <error.h>
#include <limits.h>
#include <sys/time.h>
#include <linux/unistd.h>
#endif

#include "locks.h"
#include "gpAoHrtRecon.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoShmMap1.h"
#include "gpAoShmMap2.h"

#if (RTL_SYSTEM == 0)
#include "gpAoSrtDpio2Drivers.h"
#else
#include "gpAoHrtDpio2Drivers.h"
#endif

#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
typedef unsigned char           u_char;
typedef unsigned short          u_short;
typedef unsigned int            u_int;
typedef long long               __kernel_loff_t;
typedef __kernel_loff_t         loff_t;
#include "edt_lnx_kernel_Local.h"
#endif

// Set to 0 to turn off printfs for strings set to the SRT (normal).
// Set to 1 to turn them on, for testing.
#define PRINTF_SEND_STRNGS_SC 0

// Global Variables
char    OkayToFinishInit = false;       // flag indicating that it is okay
                                        // for threads to complete their
                                        // initializations

rtl_sem_t       WFSSem;                 // SciMeasure camera thread semaphore
rtl_sem_t       TTSem;                  // TT camera thread semaphore
rtl_sem_t       CentSem;                // centroid thread semaphore
rtl_sem_t       ReconSem;               // recon thread semaphore
rtl_sem_t       FftwPlanSem;            // fftw plan mutual exclusion semaphore

int             DiagFIFOfd;             // fd for FIFO used to send messages
                                        // to the diagnostic data handler
int             CmdFIFOfd;              // fd for FIFO used to receive command
                                        // messages from the host interface

int             hrtSystemDebugLevel = 0;

FileDataShmType*        hrtFileDataShmPtr       = NULL; // file data shared memory
CamDataType*            hrtCamDataPtr           = NULL; // camera shared memory data
CamTTDataType*          hrtCamTTDataPtr         = NULL; // camera shared memory data
CentDataType*           hrtCentDataPtr          = NULL; // centroid shared memory data
ReconDataType*          hrtReconDataPtr         = NULL; // recon shared memory data
HrtDataFlowControlType* hrtDataFlowControlPtr   = NULL; // Data Flow Control variables
HrtDataFlowControlType* hrtTTDataFlowControlPtr = NULL; // TT Data Flow Control variables

// initalization flags for threads
char                    CamWFSInitDone = false;
char                    CentInitDone = false;
char                    ReconInitDone = false;
char                    CamTTInitDone = false;

// thread quit flags
boolean                 QuitCamWFSWatchThread = false;
boolean                 QuitCamTTWatchThread = false;
boolean                 QuitCamera_WFS_Thread = false;
boolean                 QuitCamera_TT_Thread = false;
boolean                 QuitCentroidThread = false;
boolean                 QuitReconThread = false;
boolean                 QuitSysConThread = false;

#if ( RTL_SYSTEM != 0 )
#include "gpAoCmdTable.h"
#endif

// Local Variables
static rtl_pthread_t    camWFSWatchThread;
static rtl_pthread_t    camTTWatchThread;
static rtl_pthread_t    camera_WFS_Thread;
static rtl_pthread_t    camera_TT_Thread;
static rtl_pthread_t    centroidThread;
static rtl_pthread_t    reconThread;
static rtl_pthread_t    sysConThread;

static int              mmfd, mmfd2;              // mmap file descriptors
static ShmMap1Type      *shmMap1Ptr;    // start of shared memory, not recon
static ShmMap2Type      *shmMap2Ptr;    // start of shared memory, recon

static char FTR_wfrCmFilenameSave[MAX_FILENAME_LENGTH] = "";

// Local Function Prototypes
void            cleanupHRT(void);
static int      gpAoHrtSysCon_GlobalsInit(void);
static void     gpAoHrtSysCon_GlobalsCleanup(void);
static void*    gpAoHrtSysCon( void *arg );

void shutdownHRT(void);

/*!
 *******************************************************************************
 *  @fn main( void )
 *
 *  @brief
 *  Mainline for AOC Hard Real Time (HRT) module, when in RTLinux mode. Performs
 *  initialization for the HRT when in user-side mode.
 *
 *  @b DESCRIPTION:
 *  This unit is the mainline for Hard Real Time (HRT) module, when in RTLinux
 *  mode. It performs initialization for the HRT when in user-side mode. Shared
 *  memory is allocated, threads and semaphores are created, etc.
 *  @par
 *
 *  IMPORTANT: this unit needs to be called main() when in RTLinux mode. In
 *  user-side mode, it is called initHRT().
 *  @par
 *
 *  @b Usage:
 *  retVal = initHRT();  (when in user-side mode)
 *
 *  @b Param None.
 *
 *  @return
 *  (int) 0 = success
 *  (int) !0 = error codes as set by system calls
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
#if ( RTL_SYSTEM == 0 )
int
initHRT(void)
#else
int
main(void)
#endif
{
int retVal;
int srtHeartbeat = -1;
boolean srtAlive = true;
rtl_pthread_attr_t attr;

#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))     // mrr 4K
int dpio2Errno;
#endif
struct rtl_sched_param param;

    // can't send this message to the host because the FIFO isn't set up yet
    printf ( "\n================\nStarting hard real-time process ==== Ver %s ====\n================\n", HRT_SW_VERSION );

    printf( "\n  initHRT:Syscon: Starting Globals Init\n"); // mrr DM

    //      initialize pointers to global/shared variables
    if ((retVal = gpAoHrtSysCon_GlobalsInit()) !=  0)
    {
        printf( " initHRT:Syscon: **** Global Init ERROR\n");   // mrr DM
        return retVal;  // error message has already been displayed
    }

    printf( "    initHRT:Syscon: Ending Globals Init\n");   // mrr DM

    //      if either dpio2 DM is to be used, init them both (which means that both dpio2 boards need to be in the
    //      > AOC; but both DMs don't necessarily need to be connected to the dpio2 boards)
    //      > IMPORTANT: because memory is allocated in the dpio2 init functions, they MUST be executed in this context
#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))     // mrr 4K
    //      initialize the dpio2 drivers to control the GPI woofer (including the T/T stage) and the GPI
    //      > tweeter -- there are 2 dpio2 boards (one per DM); but, as would be expected, both boards use
    //      > with the same driver software

    printf( "  initHRT:Syscon: Calling dpio2 Init\n");      // mrr DM


    // #if ( RTL_SYSTEM == 0 ) dpio2Errno = gpAoSrtDpio2Init();  #else dpio2Errno = gpAoHrtDpio2Init(); #endif
    dpio2Errno = gpAoHrtDpio2Init(); // Need to talk to real drivers

    if (dpio2Errno < 0)
    {
        //      can't send this message to the host because the FIFO isn't set up yet
        printf( "initHRT:Syscon: ERR (initHRT): dpio2 driver init failed - return = %d\n", dpio2Errno );
    }
#endif

    printf( "  initHRT:*Syscon: Returning from dpio2 Init\n");      // mrr DM


    printf( "  initHRT:Syscon: Starting     gpAoHrtFileData_Init()\n");     // mrr DM

    //      initalize the file shared memory variables
    gpAoHrtFileData_Init();

    printf( "  initHRT:*Syscon: ending      gpAoHrtFileData_Init()\n");     // mrr DM

    //      init hrtShutdown in shared memory and the srtIsUpFlag in preparation for spinning on it
    hrtFileDataShmPtr->hrtShutdown = false;
    hrtFileDataShmPtr->srtIsUpFlag = false;

    printf( "  initHRT:Syscon: Opening FIFO 0\n");  // mrr DM
    //      open FIFOs for HRT/SRT communication, returning errno if an error occurs (the SRT will spin on the command FIFO
    //      > until it is opened)
    //#if ( RTL_SYSTEM != 0 )
    //     if ((DiagFIFOfd = rtl_open("/dev/rtf0", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_WRONLY)) < 0)
    //#else
    //if ((DiagFIFOfd = rtl_open(RTC_PIPE_0, RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDWR, S_IRUSR | S_IWUSR)) < 0)
    //#endif
    if ((DiagFIFOfd = rtl_open("/dev/rtf0", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_WRONLY)) < 0)
    {
        //      can't send this message to the host because the FIFO setup failed
        printf( "initHRT:Syscon: ERR (initHRT) creating diagnostic output FIFO 0, errno = %d\n", rtl_errno);
        return rtl_errno;
    }

     printf( "  initHRT:Syscon: Opening FIFO 1\n"); 
    //#if ( RTL_SYSTEM != 0 )
    //    if ((CmdFIFOfd = rtl_open("/dev/rtf1", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY)) < 0)
    //#else
    //    if ((CmdFIFOfd = rtl_open(RTC_PIPE_1, RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY, S_IRUSR | S_IWUSR)) < 0)
    //#endif
    if ((CmdFIFOfd = rtl_open("/dev/rtf1", RTL_O_NONBLOCK | RTL_O_CREAT | RTL_O_RDONLY)) < 0)
    {
        //      can't send this message to the host because the FIFO setup failed
        printf( "initHRT:Syscon: ERR (initHRT) creating diagnostic output FIFO 1, errno = %d\n", rtl_errno);
        return rtl_errno;
    }
    printf( "  initHRT:*Syscon: Ending FIFO's\n");  // mrr DM

    printf( "  initHRT:Syscon: Creating Semaphores's\n");   // mrr DM

    // create mutexes
    if (wfsLockInit() < 0) {
        printf("Faile to create WFS mutex!\n");
        return rtl_errno;
    }

    //      create semaphores, returning errno if an error occurs
    if (rtl_sem_init(&WFSSem, 1, 0) < 0)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR creating WFSSem, errno = ", rtl_errno);
        return rtl_errno;
    }
    if (rtl_sem_init(&CentSem, 1, 0) < 0)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR creating CentSem, errno = ", rtl_errno);
        return rtl_errno;
    }
    if (rtl_sem_init(&ReconSem, 1, 0) < 0)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR creating ReconSem, errno = ", rtl_errno);
        return rtl_errno;
    }
    if(rtl_sem_init(&FftwPlanSem, 1, 1) < 0)  // create this one unlocked (third parameter set to 1)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR creating FftwPlanSem semaphore, errno = ", rtl_errno);
        return rtl_errno;
    }
#   if USE_TIP_TILT
    if (rtl_sem_init(&TTSem, 1, 0) < 0)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR creating TTSem, errno = ", rtl_errno);
        return rtl_errno;
    }
#   endif /* USE_TIP_TILT */
    printf( "  initHRT:*Syscon: Ending Semaphores's\n");    // mrr DM

    printf( "  initHRT:Syscon: Starting to create threads\n");      // mrr DM

    // create threads, returning the value returned by pthread_create()
    // if an error occurs
    // (note that, in the following, higher numbers mean higher priorities)



    // camWFSWatchThread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 5;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camWFSWatch) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_WFS_WATCH) != 0 )
    {
        printf("WARN (initHRT:camWFSWatch) CPU Assignment failed for CPU #%d", HRT_CPU_WFS_WATCH );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:camWFSWatch) CPU Assignment failed: ", HRT_CPU_WFS_WATCH );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camWFSWatch) now running on CPU %d\n", retVal);
#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif

    if ((retVal = rtl_pthread_create(&camWFSWatchThread, &attr, WFSCam_DrvrWatch, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:camWFSWatch) creating, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);



    //      camera_WFS_Thread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 10;  // 3;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camera_WFS) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_WFS_CAM) != 0 )
    {
        printf("WARN (initHRT:camera_WFS) CPU Assignment failed for CPU #%d", HRT_CPU_WFS_CAM );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:camera_WFS) CPU Assignment failed: ", HRT_CPU_WFS_CAM );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camera_WFS) now running on CPU %d\n", retVal);

#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
    if ((retVal = rtl_pthread_create(&camera_WFS_Thread, &attr, WFSCam, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:camera_WFS) creating, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);



#   if USE_TIP_TILT
    // camTTWatchThread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 5;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camTTWatch) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_TT_WATCH) != 0 )
    {
        printf("WARN (initHRT:camTTWatch) CPU Assignment failed for CPU #%d", HRT_CPU_TT_WATCH );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:camTTWatch) CPU Assignment failed: ", HRT_CPU_TT_WATCH );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camTTWatch) now running on CPU %d\n", retVal);
#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif

    if ((retVal = rtl_pthread_create(&camTTWatchThread, &attr, gpAoHrtCamTT_DrvrWatch, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:camTTWatch) creating, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);



    //      camera_TT_Thread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 10;  // 3;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camera_TT) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_TT_CAM) != 0 )
    {
        printf("WARN (initHRT:camera_TT) CPU Assignment failed for CPU #%d", HRT_CPU_TT_CAM );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:camera_TT) CPU Assignment failed: ", HRT_CPU_TT_CAM );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:camera_TT) now running on CPU %d\n", retVal);

#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
    if ((retVal = rtl_pthread_create(&camera_TT_Thread, &attr, gpAoHrtCamTT, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:camera_TT) creating, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);
#   endif /* USE_TIP_TILT */



    // centroidThread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 13;  // 4;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:centroid) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_CENT) != 0 )
    {
        printf("WARN (initHRT:centroid) CPU Assignment failed for CPU #%d", HRT_CPU_CENT );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:centroid) CPU Assignment failed: ", HRT_CPU_CENT );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:centroid) now running on CPU %d\n", retVal);

#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
    if ((retVal = rtl_pthread_create(&centroidThread, &attr, gpAoHrtCent, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:centroid) creating, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);

    //      Reconstruction initialization
    //      Memory initialization
    gpAoHrtRecon_InitMemory();



    //      Init main reconstruction thread
    // reconThread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 15;  // 2;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:recon) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_RECON) != 0 )
    {
        printf("WARN (initHRT:recon) CPU Assignment failed for CPU #%d", HRT_CPU_RECON );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:recon) CPU Assignment failed: ", HRT_CPU_RECON );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:recon) now running on CPU %d\n", retVal);

#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
    if ((retVal = rtl_pthread_create(&reconThread, &attr, gpAoHrtRecon, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:recon) creating recon thread, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);

    //      Init reconstruction worker threads
    // worker Threads
    gpAoHrtRecon_InitThreading();



    // sysConThread
    rtl_pthread_attr_init(&attr);
    rtl_pthread_attr_getschedparam(&attr, &param);
    param.sched_priority += 1;
    rtl_pthread_attr_setschedparam(&attr, &param);

#if ( RTL_SYSTEM != 0 )
    rtl_pthread_attr_setfp_np(&attr, 1);

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:sysCon) started on CPU %d\n", retVal);

    if( rtl_pthread_attr_setcpu_np(&attr, HRT_CPU_SYSCON) != 0 )
    {
        printf("WARN (initHRT:sysCon) CPU Assignment failed for CPU #%d", HRT_CPU_SYSCON );
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (initHRT:sysCon) CPU Assignment failed: ", HRT_CPU_SYSCON );
    }

    rtl_pthread_attr_getcpu_np(&attr, &retVal);
    printf("(initHRT:sysCon) now running on CPU %d\n", retVal);

#else
    pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
#endif
    if ((retVal = rtl_pthread_create(&sysConThread, &attr, gpAoHrtSysCon, 0)) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (initHRT:sysCon) creating sysCon thread, retVal = ", retVal);
    rtl_pthread_attr_destroy(&attr);

    printf( "  initHRT:*Syscon: Ending create threads\n");  // mrr DM

    printf( "  initHRT:Syscon: Starting to spin on hrtFileDataShmPtr->srtIsUpFlag\n");      // mrr DM

    //      initialize the wfs camera interrupt (must be done from this context -- so
    //      > spin on the srtIsUpFlag first)
#if ( RTL_SYSTEM != 0 )
    while (!hrtFileDataShmPtr->srtIsUpFlag)
    {
    // This could be made modestly more efficient by
    // blocking on a semaphore with a long timeout.
        usleep(50000);
    }

printf( "  initHRT:*Syscon: srtIsUpFlag is now true\n");

#endif

    // intrinit makes WFS_dev not NULL
    // how do we know that WFSCam thread is ready for the interrupt?
    //
    WFSCam_IntrInit();

#   if USE_TIP_TILT
    //      initialize the tt camera interrupt
    gpAoHrtCamTT_IntrInit();
#   endif /* USE_TIP_TILT */



printf( "  initHRT:*Syscon: initialization complete, waiting on hrtShutdown\n");

#if ( RTL_SYSTEM != 0 )
    while (hrtFileDataShmPtr->hrtShutdown == false)
    {
        // This could be made modestly more efficient by
        // blocking on a semaphore with a long timeout.
        usleep(50000);

        // Check to see if the soft realtime code is still
        // running. If it is not, quit.

        if ( srtAlive == false )
        {
            if ( srtHeartbeat != hrtFileDataShmPtr->srtHeartbeat )
            {
                srtHeartbeat = hrtFileDataShmPtr->srtHeartbeat;
                srtAlive = true;
            }
            else
            {
                // The srtAlive flag is false, and the
                // heartbeat has not changed in 50 milliseconds;
                // it should be changing every 10 milliseconds
                // (see ../soft/FileMngr.C).

                printf( "  initHRT:*Syscon: no srtHeartbeat update in 50 milliseconds, shutting down\n");
                shutdownHRT();
            }
        }
        else if ( srtHeartbeat == hrtFileDataShmPtr->srtHeartbeat )
        {
            // It looks like the soft realtime code is dead.
            // If it remains dead the next time through the
            // loop, the hard realtime code will quit.

            srtAlive = false;
        }
        else
        {
            srtHeartbeat = hrtFileDataShmPtr->srtHeartbeat;
        }
    }

    printf( "\n    ****  initHRT:Syscon shutdown initiated\n");


    // Give the soft realtime code a chance to exit, if it hasn't
    // already.

    retVal = 3;

    while (hrtFileDataShmPtr->srtShutdown == false && retVal > 0)
    {
        // 200 milliseconds. More than enough time for the soft
        // realtime code to exit. Trying three times is beyond
        // generous.

        printf( "    ****  initHRT:Syscon waiting on soft realtime...\n");
            usleep (200000);
        retVal--;
    }


    printf( "    ****  initHRT:Syscon Main has quit\n");

    cleanupHRT();
#endif
    printf ( "\n================\n     initHRT:syscon Main has quit and finished Cleanup     \n================\n" );

    return 0;
}



/*******************************************************************************
 *
 * shutdownHRT(): signal the hard realtime code to cleanly shut down.
 *
 ******************************************************************************/
void
shutdownHRT(void)
{
    //    Set the 'quit' flag.
    hrtFileDataShmPtr->hrtShutdown = true;
}



/*!
 *******************************************************************************
 *  @fn cleanupHRT( void )
 *
 *  @brief
 *  Cleanup and dispose of variables for HRT system control.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to perform RTLinux cleanup for the AOC Hard Real-
 *  Time (HRT) module ~ Threads and semaphores are closed and deallocated.
 *  @par
 *
 *  @b Usage: cleanupHRT()
 *
 *  @b Param None.
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
void
cleanupHRT(void)
{
    SysConSendStrngNoParm(eDebugLevelMin, "INFO (cleanupHRT): Cleaning up hard real-time process");

    //      clean up the dpio2 DM drivers
#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))     // mrr 4K
#if ( RTL_SYSTEM == 0 )
    gpAoSrtDpio2Cleanup();
#else
    gpAoHrtDpio2Cleanup();
#endif
#endif

    //      free up the EDT irq
#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    if (WFS_irq >= 0)
    {
        edt_restore_os_interrupt(WFS_irq, WFS_dev);
        rtl_free_irq (WFS_irq);
    }
#endif

#   if USE_TIP_TILT
    //      free up the EDT irq
#   if ((CAMTT_TYPE == TT_REAL_GPI) && (RTL_SYSTEM != 0))
    if (TT_irq >= 0)
    {
        edt_restore_os_interrupt(TT_irq, TT_dev);
        rtl_free_irq (TT_irq);
    }
#   endif
#   endif  /* USE_TIP_TILT */

    //      tell threads to quit
    usleep(1000);  // wait a millisec
    QuitCamWFSWatchThread = true;
    usleep(1000);  // wait a millisec
    QuitCamera_WFS_Thread = true;
#   if USE_TIP_TILT
    usleep(1000);  // wait a millisec
    QuitCamTTWatchThread = true;
    usleep(1000);  // wait a millisec
    QuitCamera_TT_Thread = true;
#   endif /* USE_TIP_TILT */
    usleep(1000);  // wait a millisec
    QuitCentroidThread = true;
    usleep(1000);  // wait a millisec
    QuitReconThread = true;
    usleep(1000);  // wait a millisec
    QuitSysConThread = true;
    usleep(600000);  // wait 0.6 seconds (SysCon has a long delay)

    gpAoHrtRecon_CleanThreading();

    //      destroy semaphores
#   if USE_TIP_TILT
    rtl_sem_destroy(&TTSem);
#   endif /* USE_TIP_TILT */
    rtl_sem_destroy(&WFSSem);
    rtl_sem_destroy(&CentSem);
    rtl_sem_destroy(&ReconSem);
    rtl_sem_destroy(&FftwPlanSem);

    //      close FIFOs
    rtl_close( DiagFIFOfd );
    rtl_close( CmdFIFOfd );

    //      clean recon memory
    gpAoHrtRecon_CleanMemory();

    //      clean up shared memory
    gpAoHrtSysCon_GlobalsCleanup();
}

/*!
 *******************************************************************************
 *  @fn gpAoHrtSysCon_GlobalsInit( void )
 *
 *  @brief
 *  This unit sets up the mmap for and initializes pointers to global/shared variables
 *  Initialize global variables for HRT system control.
 *
 *  @b DESCRIPTION:
 *  Pointers to virtually all global memory variables are set up here so that they can all be
 *  part of the same mmap region to facilitate sharing the variables between several threads on
 *  RTLinux and Linux on multiple processors.  Global memory is used in this application so that
 *  large chunks of data will not have to be moved around (in most cases, data ia put in memory
 *  once and never moved again).
 *
 *  IMPORTANT: pointers to variables are set up here, but the variables are NOT initialized.  That
 *  is up to the code that is responsible for the data.  GlobalsInit() must be called before any
 *  tasks are initialized.
 *
 *  @par
 *
 *  @b Usage:
 *  retVal = gpAoHrtSysCon_GlobalsInit();
 *
 *  @b Param None.
 *
 *  @return
 *  (int) 0 = success
 *  (int) !0 = failure - system error codes are returned.
 *
 *  @exception
 *  None.
 *
 *  @see
 *  gpAoHrtSysCon_GlobalsCleanup
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************/
static int
gpAoHrtSysCon_GlobalsInit(void)
{
    printf("INFO (gpAoHrtSysCon_GlobalsInit) Attempting to open shared memory; size: %ld\n", sizeof(ShmMap1Type) );  // can't send this error to the host!
    printf("INFO (gpAoHrtSysCon_GlobalsInit) Attempting to open shared memory 2; size: %ld\n", sizeof(ShmMap2Type) );  // can't send this error to the host!

    //      create the shared memory region, returning errno if an error occurs (passing a non-zero
    //      > value for the mode means a node in Linux is also created (as opposed to just RTLinux))
#if ( RTL_SYSTEM != 0 )
    if ((mmfd = rtl_shm_open( "/dev/AOC_mmap", RTL_O_CREAT, 0666)) < 0)
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Creating shared mem on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
    else if ((mmfd2 = rtl_shm_open( "/dev/AOC_mmap2", RTL_O_CREAT, 0666)) < 0)
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Creating shared mem 2 on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
#else
    if ((mmfd = rtl_shm_open( "./AOC_mmap", O_RDWR | RTL_O_CREAT, S_IRUSR | S_IWUSR)) < 0)
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Creating shared mem on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
    else if ((mmfd2 = rtl_shm_open( "./AOC_mmap2", O_RDWR | RTL_O_CREAT, S_IRUSR | S_IWUSR)) < 0)
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Creating shared mem 2 on HRT side, errno = %d\n", rtl_errno);  // can't send this error to the host!
#endif

    //      size the shared memory region, returning errno if an error occurs
    if (rtl_ftruncate(mmfd, sizeof(ShmMap1Type)) < 0)
    {
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Error sizing shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
        return rtl_errno;
    }
    else if (rtl_ftruncate(mmfd2, sizeof(ShmMap2Type)) < 0)
    {
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Error sizing shared mem 2, errno = %d\n", rtl_errno);  // can't send this error to the host!
        return rtl_errno;
    }

    printf("  sizeof(ShmMap1Type)=%ld\n  sizeof(FileDataShmType)=%ld\n  sizeof(CamDataType)=%ld\n"
           "  sizeof(CentDataType)=%ld\n  sizeof(HrtDataFlowControlType)=%ld\n\n"
           "  sizeof(CamTTDataType)=%ld\n NUM_HRT_DATA_BUFFS=%d\n", 
           sizeof(ShmMap1Type), sizeof(FileDataShmType), sizeof(CamDataType), 
           sizeof(CentDataType), sizeof(HrtDataFlowControlType), 
           sizeof(CamTTDataType), NUM_HRT_DATA_BUFFS);
 

    //      get the pointer to the shared memory region, returning errno if an error occurs
    if ((shmMap1Ptr = (ShmMap1Type *)rtl_mmap(0, sizeof(ShmMap1Type), RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, mmfd, 0)) == RTL_MAP_FAILED)
    {
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Error getting pointer to shared mem, errno = %d\n", rtl_errno);  // can't send this error to the host!
        return rtl_errno;
    }
    if ((shmMap2Ptr = (ShmMap2Type *)rtl_mmap(0, sizeof(ShmMap2Type), RTL_PROT_READ | RTL_PROT_WRITE, RTL_MAP_SHARED, mmfd2, 0)) == RTL_MAP_FAILED)
    {
        printf("ERR (gpAoHrtSysCon_GlobalsInit): Error getting pointer to shared mem 2, errno = %d\n", rtl_errno);  // can't send this error to the host!
        return rtl_errno;
    }
    printf("INFO (gpAoHrtSysCon_GlobalsInit) shared memory allocation for HRT is okay\n");  // can't send this error to the host!
   
    //      set up the pointers to the global variables
    hrtFileDataShmPtr       = &(shmMap1Ptr->FileDataShm);
    hrtCamDataPtr           = &(shmMap1Ptr->CamDataShm);
    hrtCentDataPtr          = &(shmMap1Ptr->CentDataShm);
    hrtDataFlowControlPtr   = &(shmMap1Ptr->DataFlowControlShm);
#   if ADD_TIP_TILT
    hrtCamTTDataPtr         = &(shmMap1Ptr->CamTTDataShm);
    hrtTTDataFlowControlPtr = &(shmMap1Ptr->TTDataFlowControlShm);
#   endif /* ADD_TIP_TILT */

    hrtReconDataPtr         = &(shmMap2Ptr->ReconDataShm);

    // can't send the following errors to the host!
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of ShmMap1 = 0x%lx, ptr = %p\n",
    sizeof(ShmMap1Type), shmMap1Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of FileData = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(FileDataShmType), hrtFileDataShmPtr, (char *)hrtFileDataShmPtr - (char *)shmMap1Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of CamData = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(CamDataType), hrtCamDataPtr, (char *)hrtCamDataPtr - (char *)shmMap1Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of CentData = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(CentDataType), hrtCentDataPtr, (char *)hrtCentDataPtr - (char *)shmMap1Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of DataFlowCtrl = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(HrtDataFlowControlType), hrtDataFlowControlPtr, (char *)hrtDataFlowControlPtr - (char *)shmMap1Ptr);
#   if ADD_TIP_TILT
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of CamTTData = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(CamTTDataType), hrtCamTTDataPtr, (char *)hrtCamTTDataPtr - (char *)shmMap1Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of TTDataFlowCtrl = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(HrtDataFlowControlType), hrtTTDataFlowControlPtr, (char *)hrtTTDataFlowControlPtr - (char *)shmMap1Ptr);
#   endif /* ADD_TIP_TILT */

    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of ShmMap2 = 0x%lx, ptr = %p\n",
    sizeof(ShmMap2Type), shmMap2Ptr);
    printf( "INFO (gpAoHrtSysCon_GlobalsInit): Size of ReconData = 0x%lx, ptr = %p, offset = 0x%lx\n",
    sizeof(ReconDataType), hrtReconDataPtr, (char *)hrtReconDataPtr - (char *)shmMap2Ptr);

    return 0;
}

/*!
 *******************************************************************************
 *  @fn gpAoHrtSysCon_GlobalsCleanup( void )
 *
 *  @brief
 *  This unit cleans up the mmap for the initialized pointers to global/shared variables
 *  for HRT system control.
 *
 *  @b DESCRIPTION:
 *  Pointers to virtually all global memory variables will be deallocated and removed here. This
 *  is a reversal of the process done in 'gpAoHrtSysCon_GlobalsInit' HOWEVER, the associated
 *  memory is still allocated when this is called, it will be deallocated, if at all possible.
 *  @par
 *
 *  IMPORTANT: This code WILL deallocate memory
 *  @par
 *
 *  @b Usage:
 *  gpAoHrtSysCon_GlobalsCleanup();
 *
 *  @b Param None.
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @see
 *  gpAoHrtSysCon_GlobalsInit
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  gpAoHrtSysCon_GlobalsInit() must have been called.
 *
 *  @callgraph
 ********************************************************************************/
static void
gpAoHrtSysCon_GlobalsCleanup(void)
{
//      unmap, close, and destroy the global memory regions
    rtl_munmap(shmMap1Ptr, sizeof(ShmMap1Type));
    rtl_close(mmfd);
    rtl_munmap(shmMap2Ptr, sizeof(ShmMap2Type));
    rtl_close(mmfd2);

#if ( RTL_SYSTEM != 0 )
    rtl_shm_unlink( "/dev/AOC_mmap" );
    rtl_shm_unlink( "/dev/AOC_mmap2" );
#else
    rtl_shm_unlink( "./AOC_mmap" );
    rtl_shm_unlink( "./AOC_mmap2" );
#endif
}

/*!
 *******************************************************************************
 *  @fn gpAoHrtSysCon( void* dummy )
 *
 *  @brief
 *  System controller thread for the Hard Real Time (Hrt) side.
 *
 *  @b DESCRIPTION:
 *  This unit is the system controller thread for the Hard Real Time (Hrt) side.  Its
 *  purpose is to wait on the command FIFO from the Soft Real Time (Srt) side and
 *  dispatch commands as they are received.
 *  @par
 *  IMPORTANT:  RTLinux does not support blocking FIFOs.  So, the FIFO is polled.
 *  @par
 *
 *  @b Usage: gpAoHrtSysCon( NULL );
 *
 *  @param[in] dummy (void *):  Needed for pthread_create(), not currently used.
 *
 *  @return
 *  0 :  always 0 (NULL); should never return
 *
 *  @exception
 *  None.
 *
 *  @see
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static void *
gpAoHrtSysCon( void *arg )
{
int                     retVal;
short           i, ii;
short           cmdAction;
struct rtl_timespec next;
cmdFIFOMsgType  cmdFIFOMsg;
static short    sysConCmdCrnt = eCmdNone;  // This is used to handle multi-step comamnds that require SRT side support

#if (RTL_SYSTEM == 0)
    cpu_set_t mask;
    int       cpuNum = HRT_CPU_SYSCON;

    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) - CPU #",  cpuNum );
    if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) executing sched_setaffinity() for CPU #",  cpuNum );
    sched_getaffinity( 0, sizeof( mask ), &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else
    printf ("      Syscon: Starting SysCon thread id: %d\n", rtl_pthread_self());   // mrr DM
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) SysCon thread id: ", rtl_pthread_self());
#endif

#if (RTL_SYSTEM != 0)
    printf("    hard gpAoHrtSysCon() CPU:thread %d:%d\n",
    rtl_getcpuid(),rtl_pthread_self());   // sla debugging hang
#endif

//      spin on the srtIsUpFlag in shared memory until the SRT is up
    while (!hrtFileDataShmPtr->srtIsUpFlag)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            printf("WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = %d\n", retVal);  // cannot send this to the host!
    }
    printf("      Syscon: SRT is Up\n");    // mrr DM

    //      command the SRT to read all file data, spin until it is received into shared memory
    //      > (this does NOT load it into HRT local memory)
    printf("      Syscon: Starting to read data\n");        // mrr DM
    gpAoHrtFileData_Cmd(eFileReadAllData);
    printf("      *Syscon: Ending reading data\n"); // mrr DM
    printf("      Syscon: Starting to wait for hrtFileDataShmPtr->allDataAvailFromFile\n"); // mrr DM
    while (!hrtFileDataShmPtr->allDataAvailFromFile)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);
    }
    printf("      *Syscon: Finished waiting for hrtFileDataShmPtr->allDataAvailFromFile\n");        // mrr DM

    //      Now test if data was read successfully and if so, then move it into program variables
    if (!gpAoHrtFileData_Get(eFileTypeParms))
        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading parms data at startup, defaults being used");
    else
    {
        if (!gpAoHrtFileData_Get(eFileTypeWfsDark_0))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_0 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsDark_1))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_1 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsDark_2))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_2 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsDark_3))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_3 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsDark_4))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Dark_4 data at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_0))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_0 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_1))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_1 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_2))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_2 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_3))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_3 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_4))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfsBkGrnd_4 data at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_0))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_0 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_1))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_1 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_2))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_2 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_3))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_3 data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_4))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading wfs Flat_4 data at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeRefCentQ))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcentQ data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeRefOffs))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcent offset data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeRefRePhaseOffs))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Cal RePhase data at startup, defaults being used");
        //      if (!gpAoHrtFileData_Get(eFileTypeReserve1))
        //      SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Reserve1 file at startup, defaults being used");
        //      if (!gpAoHrtFileData_Get(eFileTypeReserve2))
        //      SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Reserve2 file at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeTwtDmCoeff))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for TWT phase to volts, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrDmCoeff))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for WFR phase to volts, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTTDmCoeff))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading coeff file for TT to volts, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeTwtLabFlat))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt lab flat data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTwtSysFlat))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt sys flat data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTwtDmShape))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt dm shape data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTwtHostOffset))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt host offset data at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeWfrLabFlat))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr lab flat data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrSysFlat))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr sys flat data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrDmShape))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr dm shape data at startup, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrHostOffset))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr host offset data at startup, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeTwtCleanup))          // tweeter cleanup matrix
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Cleanup matrix, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTwtReconFilter))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Recon Filter, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeWfrCM_FTR))           // woofer control matrix for FTR calculations data
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr CM (FTR), defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrCleanup))          // woofer cleanup matrix
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr SM (FTR), defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrVecList))          // wfr extraction vector selection list
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Vector List, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeTwtZeroList))         // twt zeros after wfr extraction
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Zero List, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypetwtInfFuncMask))              // twt inflence functions / twt selection mask
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading MEMS Infl. Func., defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeWfrFtrActMap))                // twt inflence functions / twt selection mask
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR FTR Act Map, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeInitialModalGains))           // FTR optimizer initial gains
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Inital Gain Values, defaults being used");
        if (!gpAoHrtFileData_Get(eFileTypeHostModalGains))              // FTR optimizer host specified  gains
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Gain Values, defaults being used");

        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_1))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #1, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_2))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #2, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_3))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #3, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_4))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #4, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_5))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #5, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_6))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #6, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_7))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #7, previous values being used");
        if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_8))
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #8, previous values being used");
    }
    printf("      *Syscon: Ending SRT Data tested\n");      // mrr DM

    for( ii=0; ii<eFileTypeNum; ii++)
        hrtFileDataShmPtr->dataFileError[ ii ] = false;

    hrtFileDataShmPtr->someDataAvailFromFile = false;
    hrtFileDataShmPtr->allDataAvailFromFile = false;

    printf("      Syscon: Alert Other threads it is ok to finish init\n");  // mrr DM

    //      alert other HRT threads that it is okay to finish their initializations and wait for them to do it
    OkayToFinishInit = true;
    while (!CentInitDone || !ReconInitDone || !CamWFSInitDone)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);
    }
    printf("      *Syscon: All threads have finished init-ing\n");  // mrr DM

    //      enter an infinite loop
    while (!QuitSysConThread)
    {
        //      pause for half a second
        //      Note! The following delay will also allow HRT->SRT commands for file data to execute.  If this timer
        //      is removed or replaced with a 'wait' that wakes up on each command, the file reads may not have
        //      been completed.
        next.tv_sec = 0;
        next.tv_nsec = 500000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) rtl_nanosleep(), retVal = ", retVal);

        //      check whether file data is available and, if it is, move it into program variables
        switch( sysConCmdCrnt )
        {
            case eCmdSysConParms:

            if (hrtFileDataShmPtr->allDataAvailFromFile)
            {
                if (!gpAoHrtFileData_Get(eFileTypeParms))
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading parms data, all previous values being used");
                else
                {
                    if (!gpAoHrtFileData_Get(eFileTypeWfsDark_0))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_0 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsDark_1))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_1 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsDark_2))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_2 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsDark_3))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_3 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsDark_4))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Dark_4 data, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_0))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_0 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_1))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_1 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_2))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_2 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_3))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_3 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsBkGrnd_4))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs BackGround_4 data, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_0))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_0 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_1))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_1 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_2))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_2 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_3))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_3 data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfsFlat_4))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfs Flat_4 data, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeRefCentQ))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcentQ data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeRefOffs))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading refcent offset data at startup, defaults being used");
                    if (!gpAoHrtFileData_Get(eFileTypeRefRePhaseOffs))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading RePhase data, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeTwtDmCoeff))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Coeff for 'phs to Vlt' table, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrDmCoeff))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Coeff for 'phs to Vlt' table, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTTDmCoeff))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading TT Coeff for 'phs to Vlt' table, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeTwtLabFlat))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt flat Lab data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTwtSysFlat))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt flat Sys data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTwtDmShape))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt dm shape data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTwtHostOffset))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt host offset data at startup, defaults being used");

                    if (!gpAoHrtFileData_Get(eFileTypeWfrLabFlat))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Lab flat data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrSysFlat))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Sys flat data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrDmShape))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr dm shape data, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrHostOffset))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr host offset data at startup, defaults being used");

                    if (!gpAoHrtFileData_Get(eFileTypeTwtCleanup))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Cleanup matrix, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTwtReconFilter))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt ReconFilter (FTR), previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeWfrCM_FTR))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR CM (FTR), previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrCleanup))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR SM (FTR), previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrVecList))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading WFR Vect List (FTR), previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeTwtZeroList))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Twt Zero List, defaults being used");
                    if (!gpAoHrtFileData_Get(eFileTypetwtInfFuncMask))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Tweeter InfFuncMask (FTR), previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeWfrFtrActMap))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr Act Map (FTR), previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeInitialModalGains))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Init Gain, 1.0 values being set");
                    if (!gpAoHrtFileData_Get(eFileTypeHostModalGains))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Host Gains, previous values being used");

                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_1))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #1, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_2))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #2, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_3))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #3, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_4))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #4, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_5))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #5, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_6))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #6, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_7))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #7, previous values being used");
                    if (!gpAoHrtFileData_Get(eFileTypeApoRefCentOff_8))
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading APO Ref Offs #8, previous values being used");
                }

                for( ii=0; ii<eFileTypeNum;ii++)
                    hrtFileDataShmPtr->dataFileError[ ii ] = false;
                hrtFileDataShmPtr->allDataAvailFromFile = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                SysConSendStrngNoParm(eDebugLevelMin, "INFO 'readConfig' complete");
                // RESPONSE readConfig
                diagCmdResponse(CmdRespReadConfig, 0,NULL, NULL, NULL);
                sysConCmdCrnt = eCmdNone;
            }
            break;

            case eCmdFillFtrWfrCm:
            if ( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrCM_FTR ])
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Wfr CM file on HRT side");
                strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR], FTR_wfrCmFilenameSave);  // restore original Wfr CM filename
                hrtFileDataShmPtr->dataFileError[ eFileTypeWfrCM_FTR ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            if (hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR])
            {
                if (!gpAoHrtFileData_Get( eFileTypeWfrCM_FTR ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading FTR Wfr CM file, previous values being used");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
                gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr CM filename
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[eFileTypeWfrCM_FTR] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            break;

            case eCmdRefOffs:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefOffs ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offsets file");
                hrtFileDataShmPtr->dataFileError[ eFileTypeRefOffs ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            // We have new ref Offset FileName
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeRefOffs ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offsets file");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefOffs ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new Ref Off filename
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
            }
            break;

            case eCmdRefRePhaseOffs:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefRePhaseOffs ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref CAL re-Phase Offs");
                hrtFileDataShmPtr->dataFileError[ eFileTypeRefRePhaseOffs ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            // We have new ref Re-Phase Offsets
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeRefRePhaseOffs ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref CAL re-Phase Offs file");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefRePhaseOffs ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the ref Re-Phase Offsets filename
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
            }
            break;

            case eCmdSetSimCalData:  // this is basically a no-op -- it is run in response to a setSimCalData command that is handled on the SRT side
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeRefMapOffs ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offset Map file");
                hrtFileDataShmPtr->dataFileError[ eFileTypeRefMapOffs ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            // We have new Cal Ref Offset Map
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeRefMapOffs ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Ref Offset Map file");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeRefMapOffs ] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
            }
            break;

            case eCmdSetTwt:
            printf ( " HRT eCmdSetTwt-1\n" );       // mrr twt
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeTwtDmShape ] )
            {
                printf ( " HRT eCmdSetTwt-1: Error in data file\n" );   // mrr twt
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host shape file for Tweeter on HRT side");
                hrtFileDataShmPtr->dataFileError[ eFileTypeTwtDmShape ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdSetTwt failed
                diagCmdResponse(CmdRespSetTweeterShape, -1, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtDmShape ] );
                break;
            }

            // We have new twtShapeFileName
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ])
            {
                printf ( " HRT eCmdSetTwt: new file name\n" );  // mrr twt
                if (!gpAoHrtFileData_Get( eFileTypeTwtDmShape ))
                {       // oops! error
                    printf ( " HRT eCmdSetTwt: error in new file name\n" ); // mrr twt
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host DmShape file for tweeter");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
                    sysConCmdCrnt = eCmdNone;
                    // Response - eCmdSetTwt FAILED
                    diagCmdResponse(CmdRespSetTweeterShape, -1, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtDmShape ]  );
                    break;
                }
                else
                {       // Got the data
                    printf ( " HRT eCmdSetTwt: new file name data avail == false\n" );      // mrr twt
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtDmShape ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new Twt DM Shape filename
                    sysConCmdCrnt = eCmdNone;
                    // Response - eCmdSetTwt SUCCESS
                    diagCmdResponse(CmdRespSetTweeterShape, 0, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtDmShape ] );
                    break;
                }
            }
            break;

            case eCmdTwtOff:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeTwtHostOffset] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host 'offset' file for Tweeter on HRT side");
                hrtFileDataShmPtr->dataFileError[ eFileTypeTwtHostOffset ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdTwtOff FAILED
                diagCmdResponse(CmdRespTweeterOffset, -1, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtHostOffset ] );
                break;
            }
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeTwtHostOffset ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host offset file for tweeter");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
                    sysConCmdCrnt = eCmdNone;
                    // Response - eCmdTwtOff FAILED
                    diagCmdResponse(CmdRespTweeterOffset, -1, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtHostOffset ] );
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeTwtHostOffset ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new Ref Off filename
                    sysConCmdCrnt = eCmdNone;
                    // Response - eCmdTwtOff SUCCESS
                    diagCmdResponse(CmdRespTweeterOffset, 0, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtHostOffset ] );
                    break;
                }
            }
            break;

            case eCmdSetWfr:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrDmShape] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host shape file for Woofer");
                hrtFileDataShmPtr->dataFileError[ eFileTypeWfrDmShape] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdSetWfr FAILED
                diagCmdResponse(CmdRespSetWooferShape, -1, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[eFileTypeWfrDmShape] );
                break;
            }
            // We have new wfrShapeFileName
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeWfrDmShape ))
                {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host DmShape file for woofer");
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdSetWfr FAILED
                diagCmdResponse(CmdRespSetWooferShape, -1, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[eFileTypeWfrDmShape] );
                break;
                }
                else
                {
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrDmShape ] = false;
                gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr dm sahpe filename
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdSetWfr SUCCESS
                diagCmdResponse(CmdRespSetWooferShape, 0, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[eFileTypeWfrDmShape] );
                break;
                }
            }
            break;

            case eCmdWfrOff:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeWfrHostOffset ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Offset file for Woofer on HRT side");
                hrtFileDataShmPtr->dataFileError[ eFileTypeWfrHostOffset ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
                sysConCmdCrnt = eCmdNone;
                // Response - eCmdWfrOff FAILED
                diagCmdResponse(CmdRespWooferOffset, -1, NULL, NULL,
                                hrtFileDataShmPtr->crntFilenames[eFileTypeWfrHostOffset] );
                break;
            }
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeWfrHostOffset ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading host Offset file for woofer");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
                    sysConCmdCrnt = eCmdNone;
                    //  Response - eCmdWfrOff FAILED
                    diagCmdResponse(CmdRespWooferOffset, -1, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[eFileTypeWfrHostOffset] );
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeWfrHostOffset ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new Wfr Off filename
                    sysConCmdCrnt = eCmdNone;
                    // Response - eCmdWfrOff SUCCESS
                    diagCmdResponse(CmdRespWooferOffset, 0, NULL, NULL,
                                    hrtFileDataShmPtr->crntFilenames[eFileTypeWfrHostOffset] );
                    break;
                }
            }
            break;

            case eCmdSetHstMdlGains:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeHostModalGains ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Initial Opt Gains (HRT)");
                hrtFileDataShmPtr->dataFileError[ eFileTypeHostModalGains ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeHostModalGains ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Host Opt Gains" );
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeHostModalGains ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new modal gains filename
                    sysConCmdCrnt = eCmdNone;
                    break;
                }
            }
            break;

            case eCmdSetGainSrcs:
            if( hrtFileDataShmPtr->dataFileError[ eFileTypeModalGainSelectors ] )
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt modal selection choices (HRT)");
                hrtFileDataShmPtr->dataFileError[ eFileTypeModalGainSelectors ] = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
                sysConCmdCrnt = eCmdNone;
                break;
            }
            if (hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ])
            {
                if (!gpAoHrtFileData_Get( eFileTypeModalGainSelectors ))
                {
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading Opt Modal Selector File");
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
                    sysConCmdCrnt = eCmdNone;
                }
                else
                {
                    hrtFileDataShmPtr->someDataAvailFromFile = false;
                    hrtFileDataShmPtr->dataAvailFromFile[ eFileTypeModalGainSelectors ] = false;
                    gpAoHrtFileData_Put(eFileTypeParms);  // store the new modal gain choices filename
                    sysConCmdCrnt = eCmdNone;
                }
            }
            break;

            default:
            if ((hrtFileDataShmPtr->allDataAvailFromFile) || (hrtFileDataShmPtr->someDataAvailFromFile))
            {
                SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) unexpected file data on HRT side");
                hrtFileDataShmPtr->allDataAvailFromFile = false;
                hrtFileDataShmPtr->someDataAvailFromFile = false;
                for (i = 0; i < eFileTypeNum; i++)
                    hrtFileDataShmPtr->dataAvailFromFile[i] = false;
                sysConCmdCrnt = eCmdNone;
            }
            break;
        }

        //      check command FIFO for message
        retVal = rtl_read(CmdFIFOfd, &cmdFIFOMsg, sizeof(cmdFIFOMsgType));
        if( retVal < 0 && rtl_errno != EAGAIN )
            SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) reading from command FIFO, HRT side, errno = ", rtl_errno);

        //      if there is a message, send the command off to the appropriate thread
        else if (retVal > 0)
        {
            if (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdAbort)
            {
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) command is ABORT command");
                // Abort is handled special at this point - it is NOT handled below.
                cmdAction = eCmdActNone;        // no action will be taken in this routine but send to appropriate thread
                if (hrtCamDataPtr->cmdCrnt != eCmdNone)
                    cmdAction = eCmdActToCam;
#               if ADD_TIP_TILT
                else if (hrtCamTTDataPtr->cmdCrnt != eCmdNone)
                    cmdAction = eCmdActToTTCam;
#               endif /* ADD_TIP_TILT */
                else if (hrtCentDataPtr->cmdCrnt != eCmdNone)
                    cmdAction = eCmdActToCent;
                else if (hrtReconDataPtr->cmdCrnt != eCmdNone)
                    cmdAction = eCmdActToRecon;
                else if (sysConCmdCrnt != eCmdNone )
                    sysConCmdCrnt = eCmdNone;
                // RESPONSE abort
                diagCmdResponse(CmdRespAbort, 0, NULL, NULL, NULL);
            }
            else
            {
                // Check validity of command at this point
                if ((hrtCamDataPtr->cmdCrnt == eCmdNone)   && (hrtCentDataPtr->cmdCrnt == eCmdNone) &&
#                   if ADD_TIP_TILT
                    (hrtCamTTDataPtr->cmdCrnt == eCmdNone) &&
#                   endif /* ADD_TIP_TILT */
                    (hrtReconDataPtr->cmdCrnt == eCmdNone) && (sysConCmdCrnt == eCmdNone) && !currentlyCyclingActuator)
                {
                    // If nothing going on then allow command to proceed
                    cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
                }
                else if (((CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdCycleAct) ||   // allow multiple cycleActuator commands
                      (CmdTable[cmdFIFOMsg.cmdNdx].idNum == eCmdCycleEnd)) &&  // > or the stopCycling command
                      currentlyCyclingActuator)
                {
                    // if special case (cycleActuators or stopCycling), then allow command to proceed
                    cmdAction = CmdTable[cmdFIFOMsg.cmdNdx].action;
                }
                else
                {
                    // Another command is in progress - flag error and kill the new command
                    cmdAction = eCmdActNone;
                    if( hrtCamDataPtr->cmdCrnt != eCmdNone )
                        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Cam cmnd running = ", (int)(hrtCamDataPtr->cmdCrnt) );
#                   if ADD_TIP_TILT
                    if( hrtCamTTDataPtr->cmdCrnt != eCmdNone )
                        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) TT Cam cmnd running = ", (int)(hrtCamTTDataPtr->cmdCrnt) );
#                   endif /* ADD_TIP_TILT */
                    if( hrtCentDataPtr->cmdCrnt != eCmdNone )
                        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Cent cmnd running = ", (int)(hrtCentDataPtr->cmdCrnt) );
                    if( hrtReconDataPtr->cmdCrnt != eCmdNone )
                        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Recon cmnd running = ", (int)(hrtReconDataPtr->cmdCrnt) );
                    if( sysConCmdCrnt != eCmdNone )
                        SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) SysCon cmnd running = ", sysConCmdCrnt );
                    if( currentlyCyclingActuator )
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) Actuator cycling" );
                }

                // If we still have a command, check the state of the AO loop and verify command is okay
                if (cmdAction != eCmdActNone)
                {
                    if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTwtOpenBit) > 0) && (hrt_AoTwtLoopStat != eAOLoopOpen))
                    {
                        cmdAction = eCmdActNone;
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Twt AO loop must be open");
                    }
                    if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdWfrOpenBit) > 0) && (hrt_AoWfrLoopStat != eAOLoopOpen))
                    {
                        cmdAction = eCmdActNone;
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Wfr AO loop must be open");
                    }
                    if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTTOpenBit) > 0) && (hrt_AoTTLoopStat != eAOLoopOpen))
                    {
                        cmdAction = eCmdActNone;
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, TT AO loop must be open");
                    }
                    if (cmdAction == eCmdActNone)
                    {
                    }
                    else
                    {
                        if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTwtClosedBit) > 0) && (hrt_AoTwtLoopStat != eAOLoopClosed))
                        {
                            cmdAction = eCmdActNone;
                            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Twt AO loop must be closed");
                        }
                        if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdWfrClosedBit) > 0) && (hrt_AoWfrLoopStat != eAOLoopClosed))
                        {
                            cmdAction = eCmdActNone;
                            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, Wfr AO loop must be closed");
                        }
                        if (((CmdTable[cmdFIFOMsg.cmdNdx].loopRestrict & eCmdTTClosedBit) > 0) && (hrt_AoTTLoopStat != eAOLoopClosed))
                        {
                            cmdAction = eCmdActNone;
                            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) command not allowed, TT AO loop must be closed");
                        }
                        if (cmdAction == eCmdActNone)
                            printf("    ***** GPI ERRORAO loop must be closed\n" );
                    }
                }
            }

            SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) valid command ready to execute: ", cmdFIFOMsg.cmdNdx);
            // Okay - if here - execute the appropriate command
            switch (cmdAction)
            {
            case eCmdActNone:
                break;

            case eCmdActToCam:
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to camera");
                hrtCamDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
                hrtCamDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
                for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
                {
                    if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
                        hrtCamDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
                    else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
                        hrtCamDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
                }
                if (rtl_sem_post(&WFSSem) < 0)
                    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to WFSSem, errno = ", rtl_errno);
                break;

#           if USE_TIP_TILT
            case eCmdActToTTCam:
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to TT camera");
                hrtCamTTDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
                hrtCamTTDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
                for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
                {
                if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
                    hrtCamTTDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
                else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
                    hrtCamTTDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
                }
                if (rtl_sem_post(&TTSem) < 0)
                    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to TTSem, errno = ", rtl_errno);
                break;
#           endif /* USE_TIP_TILT */

            case eCmdActToCent:
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to centroider");
                hrtCentDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
                hrtCentDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
                for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
                {
                if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
                    hrtCentDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
                else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
                    hrtCentDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
                }
                if (rtl_sem_post(&CentSem) < 0)
                    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to CentSem, errno = ", rtl_errno);
                break;

            case eCmdActToRecon:
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to recon");
                hrtReconDataPtr->cmdCrnt = cmdFIFOMsg.cmdNdx;
                hrtReconDataPtr->cmdStart = cmdFIFOMsg.cmdNdx;
                for( ii=0; ii<MAX_NUM_CMD_PARAMS; ii++)
                {
                if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmInt)
                    hrtReconDataPtr->cmdIntParam[ii] = cmdFIFOMsg.intParam[ii];
                else if (CmdTable[cmdFIFOMsg.cmdNdx].paramIndicator[ii] == eCmdParmFlt)
                    hrtReconDataPtr->cmdFloatParam[ii] = cmdFIFOMsg.floatParam[ii];
                }
                if (rtl_sem_post(&ReconSem) < 0)
                    SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtSysCon) posting command to ReconSem, errno = ", rtl_errno);
                break;

            case eCmdActToHRT:
                SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtSysCon) sending command to HRT");
                switch( CmdTable[cmdFIFOMsg.cmdNdx].idNum )
                {
                case eCmdSysConParms:   // reads all parameter data files
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    gpAoHrtFileData_Cmd(eFileReadAllData);
                    break;

                case eCmdSetDebug:
                    SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug");
                    if      ( !strcmp( "none", cmdFIFOMsg.strngParam[0]) || !strcmp( "0", cmdFIFOMsg.strngParam[0]) ) {
                        hrtSystemDebugLevel = 0;
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug 0");
                    } else if ( !strcmp( "min",  cmdFIFOMsg.strngParam[0]) || !strcmp( "1", cmdFIFOMsg.strngParam[0]) ) {
                        hrtSystemDebugLevel = 1;
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug 1");
                    } else if ( !strcmp( "max",  cmdFIFOMsg.strngParam[0]) || !strcmp( "2", cmdFIFOMsg.strngParam[0]) ) {
                        hrtSystemDebugLevel = 2;
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug 2");
                    } else if ( !strcmp( "tlc",  cmdFIFOMsg.strngParam[0]) || !strcmp( "-1", cmdFIFOMsg.strngParam[0]) ) {
                        hrtFileDataShmPtr->tlcOnFlag = 1;  // TLC on
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug -1");
                    } else if ( !strcmp( "host", cmdFIFOMsg.strngParam[0]) || !strcmp( "-2", cmdFIFOMsg.strngParam[0]) ) {
                        hrtFileDataShmPtr->tlcOnFlag = 0;  // TLC off -- do not send once per second messages
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug -2");
                    } else if ( !strcmp( "time", cmdFIFOMsg.strngParam[0]) || !strcmp( "-3", cmdFIFOMsg.strngParam[0]) ) {
                        hrtFileDataShmPtr->tlcOnFlag = -1;  // TLC off -- send once per second messages
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug -3");
                    } else {
                        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) invalid setDebug level");
                        SysConSendStrngNoParm(eDebugLevelErr, "eCmdSetDebug invalid");
                    }
                    hrtFileDataShmPtr->systemDebug = hrtSystemDebugLevel;
                    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'setDebug' complete");
                    gpAoHrtFileData_Put( eFileTypeParms );  // store the new debug level in parms
                    {
                        int iarr[MAX_NUM_CMD_PARAMS];
                        iarr[0] = hrtSystemDebugLevel;
                        switch (hrtFileDataShmPtr->tlcOnFlag) {
                        case 1:
                            iarr[1] = -1;
                            break;
                        case 0:
                            iarr[1] = -2;
                            break;
                        case -1:
                            iarr[1] = -3;
                            break;
                        default:
                            iarr[1] = hrtFileDataShmPtr->tlcOnFlag;
                        }
                        diagCmdResponse(CmdRespDebug, 0, iarr, NULL, NULL);
                    }
                    break;

                case eCmdRefOffs:
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefOffs ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdRefOffs );
                    break;

                case eCmdRefRePhaseOffs:
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefRePhaseOffs ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdRefRePhaseOffs );
                    break;

                case eCmdSetSimCalData:  // this is run in response to a setSimCalData command that is handled on the SRT side
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeRefMapOffs ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdRefMapOffs );
                    break;

                case eCmdSetTwt:  // reads the tweeter Shape file from the host
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtDmShape ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd(eFileRdTwtDmShapeData);
                    break;

                case eCmdTwtOff:  // reads the tweeter Offset file from the host
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeTwtHostOffset ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd(eFileRdTwtHostOffsetData);
                    break;

                case eCmdSetWfr:  // reads the woofer Shape file from the host
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrDmShape], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd(eFileRdWfrDmShapeData);
                    break;

                case eCmdRcvApoRefOffs:
                    {
                        int fileType = -1;
                        int rdType  = -1;
                        int     apoNum = -1;

                        sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                        apoNum = cmdFIFOMsg.intParam[0];

                        switch( apoNum )
                        {
                            case 1:
                            fileType = eFileTypeApoRefCentOff_1;
                            rdType = eFileRdApoRefCentOff_1;
                            break;
                            case 2:
                            fileType = eFileTypeApoRefCentOff_2;
                            rdType = eFileRdApoRefCentOff_2;
                            break;
                            case 3:
                            fileType = eFileTypeApoRefCentOff_3;
                            rdType = eFileRdApoRefCentOff_3;
                            break;
                            case 4:
                            fileType = eFileTypeApoRefCentOff_4;
                            rdType = eFileRdApoRefCentOff_4;
                            break;
                            case 5:
                            fileType = eFileTypeApoRefCentOff_5;
                            rdType = eFileRdApoRefCentOff_5;
                            break;
                            case 6:
                            fileType = eFileTypeApoRefCentOff_6;
                            rdType = eFileRdApoRefCentOff_6;
                            break;
                            case 7:
                            fileType = eFileTypeApoRefCentOff_7;
                            rdType = eFileRdApoRefCentOff_7;
                            break;
                            case 8:
                            fileType = eFileTypeApoRefCentOff_8;
                            rdType = eFileRdApoRefCentOff_8;
                            break;
                        }
                        strcpy( hrtFileDataShmPtr->crntFilenames[ fileType ], cmdFIFOMsg.strngParam[1]);
                        gpAoHrtFileData_Cmd( rdType );
                    }
                    break;

                case eCmdWfrOff:  // reads the woofer Offset file from the host
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrHostOffset], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdWfrHostOffsetData);
                    break;

                case eCmdSetHstMdlGains:
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeHostModalGains ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdHostModalGains );
                    break;

                case eCmdSetGainSrcs:
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( hrtFileDataShmPtr->crntFilenames[ eFileTypeModalGainSelectors ], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd( eFileRdModalGainSelectors );
                    break;

                case eCmdFillFtrWfrCm:  // reads the specified ftr wfr cm file
                    sysConCmdCrnt = cmdFIFOMsg.cmdNdx;
                    strcpy( FTR_wfrCmFilenameSave, hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR]);  // save cmFilename in case of error
                    strcpy( hrtFileDataShmPtr->crntFilenames[eFileTypeWfrCM_FTR], cmdFIFOMsg.strngParam[0]);
                    gpAoHrtFileData_Cmd(eFileReadFtrWfrCMData);
                    break;

                default:
                    SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtSysCon) unexpected command received by syscon, cmdNdx = ", cmdFIFOMsg.cmdNdx);
                    break;
                }
                break;
            }
        }
    }
    return NULL;
}

/*!
 *******************************************************************************
 *  @fn SysConSendStrngNoParm()
 *  @fn SysConSendStrngIntParm()
 *  @fn SysConSendStrngFltParm()
 *  @fn SysConSendStrngStrParm()
 *
 *  @brief
 *  These units send null-terminated strings to hostDiag on the SRT side.
 *
 *  @b DESCRIPTION:
 *  These units send null-terminated strings to hostDiag on the SRT side. The strings
 *  are generally status or error messages to be logged, output to the console, or sent
 *  to the bit bucket.
 *  @par
 *
 *  @b Usage: SysConSendStrngNoParm(  msgDebugLevel, strngPtr )
 *  @b Usage: SysConSendStrngIntParm( msgDebugLevel, strngPtr, intParam )
 *  @b Usage: SysConSendStrngFltParm( msgDebugLevel, strngPtr, floatParam )
 *  @b Usage: SysConSendStrngStrParm( msgDebugLevel, strngPtr, strParamPtr )
 *
 *  @param[in] msgDebugLevel (int): message debug level
 *  @param[in] strngPtr      (char*):       pointer to null-terminated character string
 *  @param[in] intParam      (int): integer parameter
 *  @param[in] floatParam    (float):       float parameter
 *  @param[in] strParam      (char*):       string parameter
 *
 *  @exception
 *  None.
 *
 *  @see
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
void
SysConSendStrngNoParm( int msgDebugLevel, char *strngPtr )
{
short stringLength;
struct
{
    diagFIFOMsgType diagFIFOMsg;
    char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

    if (msgDebugLevel <= hrtSystemDebugLevel)  // do not send the message, if we do not need to
    {
    diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;
    diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

    stringLength = strlen(strngPtr) + 1;
    if (stringLength > DIAG_STRING_MAX_LENGTH)
    {
        stringLength = DIAG_STRING_MAX_LENGTH;
        strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
        diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
    }
    else
        strcpy( diagMsg.diagString, strngPtr);

    diagMsg.diagFIFOMsg.stringLength = stringLength;

    if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
        //      can't send this message to the host because the FIFO seems to have failed
        printf( "ERR (SysConSendStrngNoParm) writing noParm message to diag FIFO, HRT side, errno = %d, %s\n", rtl_errno, strngPtr );
    }

    if (PRINTF_SEND_STRNGS_SC != 0)
        printf( "HRT    - %s\n", diagMsg.diagString);
    }
}

void
SysConSendStrngIntParm( int msgDebugLevel, char *strngPtr, int intParam )
{
short stringLength;
struct
{
    diagFIFOMsgType diagFIFOMsg;
    char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

    if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
    {
    diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngIntParm;
    diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

    stringLength = strlen(strngPtr) + 1;
    if (stringLength > DIAG_STRING_MAX_LENGTH)
    {
        stringLength = DIAG_STRING_MAX_LENGTH;
        strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
        diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
    }
    else
        strcpy( diagMsg.diagString, strngPtr);

    diagMsg.diagFIFOMsg.stringLength = stringLength;
    diagMsg.diagFIFOMsg.intParam[0] = intParam;

    if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
        //      can't send this message to the host because the FIFO seems to have failed
        printf( "ERR (SysConSendStrngIntParm) writing intParm message to diag FIFO, HRT side, errno = %d, %s %d\n", rtl_errno, strngPtr, intParam );
    }

    if (PRINTF_SEND_STRNGS_SC != 0)
        printf( "HRT    - %s %d\n", diagMsg.diagString, intParam);
    }
}

void
SysConSendStrngFltParm( int msgDebugLevel, char *strngPtr, float floatParam )
{
short stringLength;
struct
{
    diagFIFOMsgType diagFIFOMsg;
    char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

    if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
    {
    diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngFltParm;
    diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

    stringLength = strlen(strngPtr) + 1;
    if (stringLength > DIAG_STRING_MAX_LENGTH)
    {
        stringLength = DIAG_STRING_MAX_LENGTH;
        strncpy(diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
        diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
    }
    else
        strcpy( diagMsg.diagString, strngPtr);

    diagMsg.diagFIFOMsg.stringLength = stringLength;
    diagMsg.diagFIFOMsg.floatParam[0] = floatParam;

    if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
        //      can't send this message to the host because the FIFO seems to have failed
        printf( "ERR (SysConSendStrngFltParm) writing floatParm message to diag FIFO, HRT side, errno = %d, %s %g\n", rtl_errno, strngPtr, floatParam );
    }

    if (PRINTF_SEND_STRNGS_SC != 0)
        printf( "HRT    - %s %f\n", diagMsg.diagString, floatParam);
    }
}

void
SysConSendStrngStrParm( int msgDebugLevel, char* strngPtr, char* strParam )
{
short stringLength;
struct
{
    diagFIFOMsgType diagFIFOMsg;
    char diagString[DIAG_STRING_MAX_LENGTH];
} diagMsg;

    if (msgDebugLevel <= hrtSystemDebugLevel)  // don't send the message, if we don't need to
    {
    diagMsg.diagFIFOMsg.diagID = eDiagCmdErrStrngNoParm;
    diagMsg.diagFIFOMsg.debugLevel = msgDebugLevel;

    stringLength = strlen(strngPtr) + strlen(strParam) + 1;
    if (stringLength > DIAG_STRING_MAX_LENGTH)
    {
        stringLength = DIAG_STRING_MAX_LENGTH;
        strncpy( diagMsg.diagString, strngPtr, (DIAG_STRING_MAX_LENGTH - 1));
        if( (stringLength - strlen( strngPtr) ) > 1 )
        strncat( diagMsg.diagString, strParam, (stringLength - strlen( strngPtr)));
        diagMsg.diagString[DIAG_STRING_MAX_LENGTH - 1] = '\0';
    }
    else
    {
        strcpy(diagMsg.diagString, strngPtr);
        strcat(diagMsg.diagString, strParam);
    }

    diagMsg.diagFIFOMsg.stringLength = stringLength;

    if (rtl_write(DiagFIFOfd, &diagMsg, (sizeof(diagFIFOMsgType) + stringLength)) < 0)
    {
        //      can't send this message to the host because the FIFO seems to have failed
        printf( "ERR (SysConSendStrngStrParm) writing strParm message to diag FIFO, HRT side, errno = %d, %s %s\n", rtl_errno, strngPtr, strParam );
    }

    if (PRINTF_SEND_STRNGS_SC != 0)
        printf( "HRT    - %s\n", diagMsg.diagString);
    }
}



/*
 *  Send a response to the SRT indicating if a command succeeded or failed.
 *  respId needs to be a unique Id number known to both HRT and SRT
 *  status - 0 indicates success, all other values indicate a problem/failure.
 *  Parameters can be sent back via iarr, farr, and str. Or they can be set to NULL.
 *  iarr and farr are limited to MAX_NUM_CMD_PARAMS elements (3 at last check)
 *  str is one null terminated string limited to MAX_FILENAME_LENGTH characters.
 */
void
diagCmdResponse(int respId, int status, int iarr[], float farr[], const char str[])
{
    diagFIFOMsgType dmsg;
    dmsg.diagID = eDiagCmdResponse;
    dmsg.cmdNdx = respId;
    dmsg.diagStat = status; // success

    if (iarr != NULL) {
        int j;
        for (j = 0; j< MAX_NUM_CMD_PARAMS; j++) {
            dmsg.intParam[j] = iarr[j];
        }
    }

    if (farr != NULL) {
        int j;
        for (j = 0; j< MAX_NUM_CMD_PARAMS; j++) {
            dmsg.floatParam[j] = iarr[j];
        }
    }

    if (str != NULL) {
        strncpy(dmsg.strngParam[0], str, MAX_FILENAME_LENGTH);
        dmsg.strngParam[0][MAX_FILENAME_LENGTH - 1] = '\0'; // Make certain null terminated.
    }

    if (rtl_write(DiagFIFOfd, &dmsg, sizeof(diagFIFOMsgType)) < 0) {
        //      can't send this message to the host because the FIFO seems to have failed
        printf( "ERR (diagCmdResponse) writing message to diag FIFO, HRT side, errno = %d, id=%d stat=%d\n",
                rtl_errno, respId, status);
    }
}


//******************************************************************************
// * End of file
//******************************************************************************
