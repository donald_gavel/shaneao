#include <stdlib.h>

#include "edtinc.h"


#define  WFS_UNIT_NUM 1
#define  WFS_BUFFERS  4
#define ACK 6 

static Dependent *dd_p = NULL;
static PdvDev    *wfs_pdv_p = NULL; 
static Edtinfo    edtinfo;
static PdvDev    *WFS_dev = NULL;
static int        crntFrameRateNdx = 0;
static int        ccd_width, ccd_height, ccd_depth;

//volatile static unsigned short *pImage = NULL;
 volatile static unsigned char *pImage = NULL;

static const char *cfgStr = "/usr/local/lick/data/EDTpdvCamera_config/CCID66_160x160_pe8.cfg";
//static const char *cfgStr = "/opt/EDTpdv/camera_config/CCID66_160x160_pe8.cfg";

static const short  cam_WFS_configProgs[]             = {    0,     0,     1,     2,     3,     5    };  // configuration programs for fram
static const unsigned short cam_WFS_configRepCounts[] = { 7900,  2900,  1893,   671,   194,    35    };  // and rep counts (or, accumulator

static const short     LegalFrameRates[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator  

static const float ccdTemperatureLUT[] = {
925.3, 120.6,  95.7,  82.4,  73.5,  66.9,  61.7,  57.4,  53.8,  50.6,
 47.8,  45.4,  43.2,  41.1,  39.3,  37.6,  36,    34.5,  33.2,  31.9,
 30.6,  29.5,  28.4,  27.4,  26.4,  25.5,  24.6,  23.7,  22.9,  22.1,
 21.3,  20.6,  19.9,  19.2,  18.6,  17.9,  17.3,  16.7,  16.2,  15.6,
 15.1,  14.5,  14,    13.5,  13,    12.5,  12.1,  11.6,  11.2,  10.8,
 10.3,   9.9,   9.5,   9.1,   8.7,   8.4,   8,     7.6,   7.3,   6.9,
  6.6,   6.2,   5.9,   5.6,   5.3,   5,     4.7,   4.4,   4.1,   3.8,
  3.5,   3.2,   2.9,   2.6,   2.4,   2.1,   1.8,   1.6,   1.3,   1.1,
  0.8,   0.6,   0.3,   0.1,  -0.1,  -0.4,  -0.6,  -0.8,  -1,    -1.2,
 -1.5,  -1.7,  -1.9,  -2.1,  -2.3,  -2.5,  -2.7,  -2.9,  -3.1,  -3.3,
 -3.5,  -3.7,  -3.9,  -4,    -4.2,  -4.4,  -4.6,  -4.8,  -4.9,  -5.1,
 -5.3,  -5.5,  -5.6,  -5.8,  -6,    -6.1,  -6.3,  -6.5,  -6.6,  -6.8,
 -6.9,  -7.1,  -7.2,  -7.4,  -7.5,  -7.7,  -7.8,  -8,    -8.1,  -8.3,
 -8.4,  -8.6,  -8.7,  -8.8,  -9,    -9.1,  -9.2,  -9.4,  -9.5,  -9.6,
 -9.8,  -9.9, -10,   -10.2, -10.3, -10.4, -10.5, -10.7, -10.8, -10.9,
-11,   -11.2, -11.3, -11.4, -11.5, -11.6, -11.7, -11.9, -12,   -12.1,
-12.2, -12.3, -12.4, -12.5, -12.6, -12.8, -12.9, -13,   -13.1, -13.2,
-13.3, -13.4, -13.5, -13.6, -13.7, -13.8, -13.9, -14,   -14.1, -14.2,
-14.3, -14.4, -14.5, -14.6, -14.7, -14.8, -14.9, -15,   -15.1, -15.2,
-15.3, -15.4, -15.5, -15.5, -15.6, -15.7, -15.8, -15.9, -16,   -16.1,
-16.2, -16.3, -16.3, -16.4, -16.5, -16.6, -16.7, -16.8, -16.9, -16.9,
-17,   -17.1, -17.2, -17.3, -17.4, -17.4, -17.5, -17.6, -17.7, -17.8,
-17.8, -17.9, -18,   -18.1, -18.1, -18.2, -18.3, -18.4, -18.5, -18.5,
-18.6, -18.7, -18.8, -18.8, -18.9, -19,   -19,   -19.1, -19.2, -19.3,
-19.3, -19.4, -19.5, -19.6, -19.6, -19.7, -19.8, -19.8, -19.9, -20,
-20,   -20.1, -20.2, -20.2, -20.3, -20.4
};




int ct(int max);
static void gpAoSrtCamMngr_SendToCam (char outString[], char inString[]);


static int waitForImage(int i);
static int handleImage(const char *bmpfname, int i);
static void save_image(u_char * image_p, int s_width, int s_height, int s_depth, const char *tmpname, int count);




int main(int argc, char *argv[]) {
    return ct(10);
}





int ct(int max_images) {
    char textBuf[2000];
    char inString[2000];
    char devname[500];
    int param;
    int temperatureCode;
    int retVal;
    int doNotCare;
    float   ccdTemperature;
    int jj;


    param =  LegalFrameRates[ 2 ];



    // gpAoSrtCamMngr_StartCam
    strcpy(devname, EDT_INTERFACE);
    printf("INFO (gpAoSrtCamMngr_StartCam) Opening camWFS EDTpdv device, unit: %d\n", WFS_UNIT_NUM);
    printf("&&&pdv_alloc_dependent\n");
    dd_p = pdv_alloc_dependent();

    printf("&&&pdv_open_channel\n");
    wfs_pdv_p = pdv_open_channel(devname, WFS_UNIT_NUM, 0 /* channel */);
    //wfs_pdv_p = edt_open_channel(devname, WFS_UNIT_NUM, 0 /* channel */);

    WFS_dev = wfs_pdv_p; 

    // WFSCam -> eWFSInitialize1:
    printf("CamMngr_ProcCmd eWFSInitialize1\n");
    
    if (wfs_pdv_p != NULL) {
        printf("&&&pdv_readcfg %s\n", cfgStr);
        if (pdv_readcfg(cfgStr, dd_p, &edtinfo) < 0) {
            printf("CamMngr_ProcCmd eWFSInitialize1 pdv_readcfg  failed  %s\n", cfgStr);
        }
        // init the cam
        printf("&&&pdv_initcam WFS_UNIT_NUM=%d %s\n", WFS_UNIT_NUM, cfgStr);
        
        if (pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, cfgStr, NULL, 0) < 0) {
            printf("***CamMngr_ProcCmd eWFSInitialize1 pdv_initcam a failed***  b\n");
        }    
    }






    ccd_width = pdv_get_width(wfs_pdv_p);
    ccd_height = pdv_get_height(wfs_pdv_p);
    ccd_depth = pdv_get_depth(wfs_pdv_p);


    // WFSCam
    if (pdv_multibuf(WFS_dev, WFS_BUFFERS) != 0)   // ((pdv_multibuf(WFS_dev, WFS_BUFFERS)) != 0)
        printf("WARN (WFSCam_Init) failed to configure ring buffers\n");
   




    // WFSCam ->    case eWFSInitialize2:
    printf("&&&  eWFSInitialize2\n");

    if (wfs_pdv_p != NULL) {
        //LogNotice("&&&pdv_multibuf(wfs_pdv_p");
        //res = pdv_multibuf(wfs_pdv_p, WFS_BUFFERS); // COMBINED - this seems unecessary for combined
        //if (res != 0) {  // need to do this to make driver happy
        //    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngr_ProcCmd) failed to configure EDT ring buffers");
        //    LogErr("CamMngr_ProcCmd eWFSInitialize2 pdv_multibuf failed");
        //}
        sleep(1);
        printf("   WFSInit2 turn off cam 1st\n");
        printf("&&& turn off cam\n");
        gpAoSrtCamMngr_SendToCam ("@SEQ 0", NULL);  // make sure SciMeasure sequencing is off
        usleep(10000);
        printf("   WFSInit2 turn off cam 2nd\n");
        gpAoSrtCamMngr_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
        usleep(10000);
        printf("   WFSInit2 turn off cam 3rd\n");
        gpAoSrtCamMngr_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
        usleep(10000);
        printf("&&&pdv_start_images // stop the free-run aqui\n");
        pdv_start_images(wfs_pdv_p, 1);   // stop the free-run acquistion
        usleep(10000);
        printf("   WFSInit2 set camera");
        sprintf (textBuf, "@RCL %d", cam_WFS_configProgs[0]);
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);   // set the camera program
        usleep(1000000);
        printf("   WFSInit2 check temp\n");
        sprintf (textBuf, "@TMP 69");                      // make sure the CCD temperature is -10 C (the setting value is
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);  // > unitless and was determined empirically)
        printf("   WFSInit2 configRepCounts\n");
        sprintf (textBuf, "@REP %d", cam_WFS_configRepCounts[0]);  // this will quickly be over-ridden by eWFSSetRep
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);
        printf("   WFSInit2 start sending data from camera\n");
        gpAoSrtCamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
        //LogNotice("   WFSInit2 start free run acquisition");
        //LogNotice("&&&pdv_start_images   // start free run acqui");
        //printf("&&&pdv_start_images 0  // start\n");
        //pdv_start_images(wfs_pdv_p, 0);  // start the free-run acquistion

        printf("   WFSInit2 complete\n");
        printf("INFO 'set wfscami' complete\n");
    }




    // WFSCam ->  case eWFSSetRep:
    printf("CamMngr_ProcCmd eWFSSetRep\n");
    printf("&&& eWFSSetRep\n");

    if (wfs_pdv_p != NULL) {
        float ccdTempKelvins = 0.0;
        gpAoSrtCamMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
        usleep(10000);
        //printf("&&&pdv_start_images 1 // stop the free-run 2nd\n");
        //pdv_start_images(wfs_pdv_p, 1);                         // stop the free-run acquistion
        //  usleep(10000);

        //                              find a legal camera rate ('param' is the desired frame rate) and then set camera operating parameters
        crntFrameRateNdx = 0;
        while ((param > LegalFrameRates[crntFrameRateNdx]) && (LegalFrameRates[crntFrameRateNdx + 1] != -1))
            crntFrameRateNdx++;

        printf("&&&  LegalFrameRates[%d]=%d\n", crntFrameRateNdx, LegalFrameRates[crntFrameRateNdx]);

        sprintf (textBuf, "@RCL %d", cam_WFS_configProgs[crntFrameRateNdx]);
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);   // set the camera program
        usleep(1000000);
        
        sprintf (textBuf, "@REP %d", cam_WFS_configRepCounts[crntFrameRateNdx]);  // set rep counts (or, accumulator values in SMC terminology)
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);
        usleep(1000000);
        
        gpAoSrtCamMngr_SendToCam ("@TMP?", inString);  // query the temperature
        temperatureCode = -1;
        retVal = sscanf(&inString[1], "@TMP! #0:%d; #1:%d;", &doNotCare, &temperatureCode);
        if ((temperatureCode < 0) || (temperatureCode > 255)) {
            printf("ERR (gpAoSrtCamMngr_ProcCmd) invalid ccd temperature code: %d\n", temperatureCode);
        } else {
            ccdTemperature = ccdTemperatureLUT[temperatureCode];  // ccdTemperature will be in degrees C
            ccdTempKelvins = ccdTemperature + 273.15;
            printf("INFO (gpAoSrtCamMngr_ProcCmd) ccd temperature (degrees C): %f\n", ccdTemperature);
        }

        // test CCD temperature limits for TLC -- most TLC errors have persistence counters; but, because the CCD temp
        //           > should never be out of range (and because it's tested so infrequently), being out of range once is enough

        //LogNotice("&&&pdv_multibuf(  2nd");
        //if ((pdv_multibuf(wfs_pdv_p, WFS_BUFFERS)) != 0)  // need to do this to make driver happy
        //    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngr_ProcCmd) failed to configure EDT ring buffers");
        usleep(10000);

        gpAoSrtCamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
        //  printf("&&&pdv_start_images 0   free run 2nd\n");
        //  pdv_start_images(wfs_pdv_p, 0);                         // start the free-run acquistion


        printf("INFO 'setCamRate' to %d\n", LegalFrameRates[crntFrameRateNdx]);
    }

        // RESPONSE setCamRate SUCCESS - actual rate may not match requested rate and be successful (include CCD temp ?)
    // g_pCCentral->queUpdateSetCamRate(CKTLInt::UPDATED, LegalFrameRates[crntFrameRateNdx]);





    // WFSCam ->   case eWFSSetAtten:
    printf("CamMngr_ProcCmd eWFSSetAtten\n");
    if (wfs_pdv_p != NULL) {

            //srtCamDataPtr->edtBufStat = eEdtBufStatNotInitted;
            //printf("srt set srtCamDataPtr->edtBufStat = eEdtBufStatNotInitted;   d\n");

        gpAoSrtCamMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
        //printf("&&&pdv_start_images  stop free run acwui 3rd\n");
        //pdv_start_images(wfs_pdv_p, 1);                         // stop the free-run acquistion

        sprintf (textBuf, "@AAM %d", param);    // param is the attenuation for all modules
        gpAoSrtCamMngr_SendToCam (textBuf, NULL);

        printf("INFO 'setCamGain' complete\n");
     
        //LogNotice("&&&pdv_multibuf   3rd");
        //if ((pdv_multibuf(wfs_pdv_p, WFS_BUFFERS)) != 0)  // need to do this to make driver happy
        //    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngr_ProcCmd) failed to configure EDT ring buffers");


        usleep(10000);
        gpAoSrtCamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
        //printf("&&&pdv_start_images  start free run 4th\n");
        //   pdv_start_images(wfs_pdv_p, 0);  

            // tell HRT to (re)get the kernel pages for the ring buffers
            //srtCamDataPtr->edtBufStat = eEdtBufStatReadNew;
            //printf("srt set srtCamDataPtr->edtBufStat = eEdtBufStatReadNew;    e\n");

    }



    //pdv_start_images(WFS_dev, 0);  // start the next image
    for(jj=0;jj<max_images;jj++){
        pdv_start_images(WFS_dev, 1);
        waitForImage(jj);
    }


    // stop
    pdv_start_images(WFS_dev, 1);

    return 0;
}



int waitForImage(int i)  {
    static int timeouts, last_timeouts = 0, recovering_timeout = FALSE;
    unsigned char *pI;  
    printf("&&&pdv_wait_image\n");
    //wfsLock();
    pI = pdv_wait_image(WFS_dev);       // wait for the last image
    if (pImage != NULL)   {
        printf("pImage not handled before new image taken!!!\n");
    }
    pImage = pI;
    //wfsUnlock();
    
    
    //wfsLock();
    //printf("&&&pdv_start_images 1 hrt\n"); 
    //pdv_start_images(WFS_dev, 1);  // start the next image
    
    // check for a camera timeout
    timeouts = pdv_timeouts(WFS_dev);
    //wfsUnlock();
    
    printf("&&&pdv_timeouts timeouts=%d last_timeout=%d\n", timeouts, last_timeouts);
    if (timeouts > last_timeouts) {
        printf("&&& timed out\n");
        //wfsLock();
        pdv_timeout_restart(WFS_dev, TRUE);
        //wfsUnlock();
        last_timeouts = timeouts;
        recovering_timeout = TRUE;
        printf("WARN - SMC WFS camera user mode timeout %d\n", timeouts);
    } else if (recovering_timeout) {
        printf("&&& timeout_recovery\n");
        //wfsLock();
        pdv_timeout_restart(WFS_dev, TRUE);
        //wfsUnlock();
        recovering_timeout = FALSE;
    } else {
        printf("&&& setting postWfsSem = true\n");
        // post semaphore
        //postWfsSem = true;
        
        handleImage("image_ct", i);
    }

    return 0;
}





int handleImage(const char *bmpfname, int i) {
    u_char *im_p;
    printf("handleImage\n");
    im_p = pImage;
    save_image(im_p, ccd_width, ccd_height, ccd_depth, bmpfname, i);
    pImage = NULL;
    return 0;
}









#define LOCAL_IN_STRING_LENGTH 256
#define DIAG_STRING_MAX_LENGTH 500

static void gpAoSrtCamMngr_SendToCam (char outString[], char inString[])
{
    printf("CamMngr_SendToCam\n");

    int    retVal, inCount;
    char   localInString[LOCAL_IN_STRING_LENGTH];
    char   tmpText[DIAG_STRING_MAX_LENGTH];
    struct timespec next;
    
    //  don't bother doing anything, if there's no EDT board or the camera isn't on
    //XXXdwp101124  if ((wfs_pdv_p == NULL) || (pdv_cl_camera_connected(wfs_pdv_p) == 0))  //XXX looks like pdv_cl_camera_connected() isn't working
    if (wfs_pdv_p == NULL)
        return;

    sprintf(tmpText, "INFO (gpAoSrtCamMngr_SendToCam) sending to WFS camera: %s", outString);
    printf("srt %s\n",tmpText);

    //  indicate that interrrupt should be ignored (because we're doing serial comms)
    //srtCamDataPtr->intrIgnore = true;
    //printf("srt _SendToCam intrIgnore = true\n");

    //  make sure the string to be returned is null terminated
    if (inString != NULL)
        inString[0] = '\0';

    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

    //  flush any junk from the read buffer and issue command to camera
    printf("&&&pdv_serial_wait\n");
    if ((inCount = pdv_serial_wait(wfs_pdv_p, 1, sizeof(localInString) - 1)) != 0)
    {
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator
        printf("&&&pdv_serial_read\n");
        pdv_serial_read(wfs_pdv_p, localInString, inCount);
    }
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

    printf("&&&pdv_serial_command\n");
    retVal = pdv_serial_command(wfs_pdv_p, outString);
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

//  if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
    if (retVal != 0) {
        printf("ERR (gpAoSrtCamMngr_SendToCam) from pdv_serial_command(): %d \n", retVal);
    } else {
        if (inString == NULL) {
            printf("&&&pdv_serial_wait a\n");
            inCount = pdv_serial_wait(wfs_pdv_p, 1000, 1);
            printf("&&&   inCount=%d\n", inCount);
        } else {
            printf("&&&pdv_serial_wait b");
            // do not lock mutex here, lock in clling function
            inCount = pdv_serial_wait(wfs_pdv_p, 2000, LOCAL_IN_STRING_LENGTH - 1);  // the '- 1' is to accommodate a null terminator
        }
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator

        next.tv_sec = 0;
        next.tv_nsec = 300000000;
        nanosleep(&next, NULL);

        if (inCount == 0) {
            printf("ERR (gpAoSrtCamMngr_SendToCam) no response from CamWFS 1\n");
        } else {
            printf("&&&pdv_serial_read 2nd\n");
            retVal = pdv_serial_read(wfs_pdv_p, localInString, inCount);
            if (retVal == 0) {
                printf("ERR (gpAoSrtCamMngr_SendToCam) no response from CamWFS 2\n");
            } else if (retVal != inCount) {
                printf("ERR (gpAoSrtCamMngr_SendToCam) from pdv_serial_read(): %d\n", retVal);
            } else if (localInString[0] != ACK) {
                printf("ERR (gpAoSrtCamMngr_SendToCam) no ACK from CamWFS\n");
            } else if (retVal > 2) {
                localInString[retVal - 2] = '\0';
                printf("INFO (gpAoSrtCamMngr_SendToCam) msg of lngth %d from CamWFS: %s", retVal, &localInString[1]);
                if (inString != NULL)
                    strcpy(inString, localInString);
            }
        }
    }

    //  indicate that interrupts should no longer be ignored
    //srtCamDataPtr->intrIgnore = false;
    //printf("srt _SendToCam  intrIgnore = false\n");
}



static void
save_image(u_char * image_p, int s_width, int s_height, int s_depth, const char *inName, int count)
{
    int     s_db = bits2bytes(s_depth);
    char    fname[256];
    char    tmpname[500];

    u_char *bbuf = NULL;

    strcpy(tmpname, inName);
    if ((strcmp(&tmpname[strlen(tmpname) - 4], ".bmp") == 0)
	|| (strcmp(&tmpname[strlen(tmpname) - 4], ".BMP") == 0))
	tmpname[strlen(tmpname) - 4] = '\0';
    sprintf(fname, "%s%02d.bmp", tmpname, count);


    /*
     * write bmp file on Windows systems, or Sun Raster on Unux/Linux
     * systems. Switch on number of bytes per pixel
     */
    switch (s_db)
    {
    case 1:

	dvu_write_bmp(fname, image_p, s_width, s_height);

	break;

    case 2:
	printf("converting %dx%dx%d image to 8 bits, writing to %s\n",
	       s_width, s_height, s_depth, fname);

	if (!bbuf)
	    bbuf = (u_char *) pdv_alloc(s_width * s_height);

	if (bbuf == NULL)
	{
	    pdv_perror("data buf malloc");
	    exit(1);
	}
	dvu_word2byte((u_short *) image_p, (u_char *) bbuf,
		      s_width * s_height, s_depth);
	dvu_write_bmp(fname, bbuf, s_width, s_height);

	break;

    case 3:
	printf("writing %dx%dx%d bmp file to %s\n",
	       s_width, s_height, s_depth, fname);

	dvu_write_bmp_24(fname, (u_char *) image_p, s_width, s_height);


	break;

    default:
	printf("invalid image depth for file write...!\n");
	break;
    }
}
