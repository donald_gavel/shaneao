/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoSrtCamMngr.c
 *
 *  @brief
 *  Interface, via the SRT side, between the HRT and the TT Camera.
 *
 *  @b DESCRIPTION:
 *  This component contains functions to provide the interface, via the SRT side,
 *  between the HRT and the TT Camera, including setup, initialization, operating
 *  parameter changes (e.g., frame rate), and status queries.  This indirect interface
 *  needs to be used because the camera driver is not strictly RTLinux compatible.
 *  The complimentary file on the HRT side is 'gpAoHrtCamTT.c'.
 *  @par
 *
 *  NOTE that direct HRT camera control would be preferable and will be used, if it
 *  becomes available.
 *  @par
 *
 *  @b AUTHOR:
 *  SMJ & DWP       DATE: 12/17/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c gpAoSrtCamMngrTT_ProcCmd()
 *  -# @c gpAoSrtCamMngrTT_StartCam()
 *  -# @c gpAoSrtCamMngrTT_StopCam()
 *  -# @c gpAoSrtCamMngrTT_TotalBufferSize()
 *  -# @c gpAoSrtCamMngrTT_SendToCam()
 *
 *
 *  @see gpAoHrtCamTT.c
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
//dwp100318 #include <sys/user.h>  // this apparently has PAGE_SIZE !!
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "WFSCam.h"
#include "gpAoHrtCamTT.h"
#include "gpAoHrtCent.h"
#include "gpAoSrtCamMngrTT.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoHrtParse.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
// #include "gpAoHrtSysCon.h"

#include "fitsio.h"

#if ( CAMTT_TYPE == TT_REAL_GPI )
#include "edtinc.h"
#endif  /* CAMTT_TYPE */


// Defines

#define ACK 6  // acknowledge from the SciMeasure Camera

// Local Variables

#if ( CAMTT_TYPE == TT_REAL_GPI )
static PdvDev    *tt_pdv_p = NULL;  // pointer for the TT PDV/EDT device
static Dependent *dd_p = NULL;
static Edtinfo    edtinfo;
static short      crntFrameRateNdx = 0;  // current index into the following arrays
#endif  /* CAMTT_TYPE */

#define NFRplus1 (NUM_FRAME_RATES_TT+1)

// WARNING: LegalFrameRatesTT[] is defined in both gpAoHrtCamTT.c (for the HRT) and gpAoSrtCamMngrTT.c (for the SRT)
const short     LegalFrameRatesTT[NFRplus1] = { 50, 100, 250, 500, -1 };  // -1 is the terminator

/*
The LittleJoe sequences are as follows:         RepCnts
Frame   Frame   Total     Reqd  Min              Addl   Addl    Actual  Actual
Rate    Period  Steps/    Prog  Frame    Integ  Integ   Steps/  Frame            Error
(Hz)    (us)    Frame     No    Steps    Steps  Incs    Frame   Rate(Hz)          (%)
    1                                           19960
   50                                             360
  100                                             160
  200                                              60
  250                                              40
  350                                              17
  500                                               0
*/

//                                              Frame rates:                     50    100    250    500
                                // configuration programs for frame rates
static const short              cam_TT_configProgs[NUM_FRAME_RATES_TT]     = {    0,     1,     2,     3 };
                                // and rep counts (or, accumulator values in SMC terminology)
static const unsigned short     cam_TT_configRepCounts[NUM_FRAME_RATES_TT] = {  360,   160,    40,     0 };

// IMPORTANT: these are just place holders for now!
static const float cam_TT_fps_offset[NUM_FRAME_RATES_TT] = { 0.0,  0.0,  0.0,  0.0 };  // offset used in frames/sec calculation
static const float cam_TT_fps_factor[NUM_FRAME_RATES_TT] = { 1.0,  1.0,  1.0,  1.0 };  // factor used in frames/sec calculation

// Local Function Prototypes

static void gpAoSrtCamMngrTT_SendToCam (char outString[], char inString[]);

static int readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType );


/*!
 *******************************************************************************
 *
 *  @fn gpAoSrtCamMngrTT_ProcCmd( short cmd, int param )
 *
 *  @brief
 *  Process commands originating in the HRT Camera control component (File: gpAoHrtCamTT.c).
 *
 *  @b DESCRIPTION:
 *  Camera commands are under SRT software control because the current RTLinux camera driver
 *  supports the high-speed camera interface; but, it does not support issuing commands.
 *  So, commands are passed from the HRT Camera control component and, then, this unit
 *  communicates the command to the camera hardware.
 *  @par Current list of commands:
 *  @par
 *          - eTTInitialize1
 *          - eTTInitialize2
 *          - eTTSetRep : Set REP.
 *          - eTTSetAtten : Set ATTEN.
 *          - eTTReadRep : Read and report the REP as a frame rate.
 *          - eTTReadAtten : Read and report the attenuation as a gain.
 *
 *
 *  @b Usage: gpAoSrtCamMngrTT_ProcCmd( cmd, cmdParam );
 *
 *  @param[in] cmd (short):  Cmd from HRT side that needs to be passed to the h/w thru the SRT
 *  @param[in] param (int): Parameter value (use depends on the command) for this particular command
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void gpAoSrtCamMngrTT_ProcCmd (short cmd, int param)
{
#   if ( CAMTT_TYPE == TT_REAL_GPI )
    short   i;
    short   Gain;
    int     Prog, Count;
    int     doNotCare, temperatureCode;
    int     retVal, Atten;
    float   ccdTemperature;
    char    textBuf[32], inString[64];
#   endif   /* CAMTT_TYPE */
    float   FrameRate;
    //static int crntLoopProg;

    // switch on command ID corresponding to command index
    switch (cmd)
    {
        case eTTInitialize1:
        printf("TT init 1\n");
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
#               if ADD_TIP_TILT
#               ifdef TEST_60X60
                // possible alternate mode of operation is central 60x60 pixels binned by 1
                if (pdv_readcfg(EDTCFGDIR "/Lil-Joe-60x60CL.cfg", dd_p, &edtinfo) != 0) // read the open loop config file
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to read 60x60 ol config file");
                if (pdv_initcam(tt_pdv_p, dd_p, TT_UNIT_NUM, &edtinfo, EDTCFGDIR "/Lil-Joe-60x60CL.cfg", NULL, 0) != 0) // init the cam
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to init 60x60 ol cam");
#               else    /* TEST_60X60 */
                // usual mode of operation is 20x20 comprised of central 60x60 pixels binned by 3
                if (pdv_readcfg(EDTCFGDIR "/Lil-Joe-20x20CL.cfg", dd_p, &edtinfo) != 0) // read the open loop config file
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to read 20x20 ol config file");
                if (pdv_initcam(tt_pdv_p, dd_p, TT_UNIT_NUM, &edtinfo, EDTCFGDIR "/Lil-Joe-20x20CL.cfg", NULL, 0) != 0) // init the cam
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to init 20x20 ol cam");
#               endif   /* TEST_60X60 */
#               endif   /* ADD_TIP_TILT */
            }
#           endif   /* CAMTT_TYPE */
            break;

        case eTTInitialize2:
        printf("TT init 2\n");
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
#               if ADD_TIP_TILT
#               if (RTL_SYSTEM != 0)
                srtCamTTDataPtr->edtBufStat = eEdtBufStatNotInitted;

                if ((pdv_multibuf(tt_pdv_p, TTnumbufs)) != 0)  // need to do this to make driver happy
                    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngrTT_ProcCmd) failed to configure EDT ring buffers");
                sleep(1);
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // make sure Little Joe sequencing is off
                usleep(10000);
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
                usleep(10000);
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
                usleep(10000);
                pdv_start_images(tt_pdv_p, 1);                         // stop the free-run acquistion
                usleep(10000);
#               endif   /* RTL_SYSTEM */

                sprintf (textBuf, "@RCL %d", cam_TT_configProgs[0]);
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);   // set the camera program
                usleep(1000000);

                sprintf (textBuf, "@TMP 69");                      // make sure the CCD temperature is -10 C (the setting value is
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);  // > unitless and was determined empirically)

                sprintf (textBuf, "@REP %d", cam_TT_configRepCounts[0]);  // this will quickly be over-ridden by eTTSetRep
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);

                gpAoSrtCamMngrTT_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
#               if (RTL_SYSTEM != 0)
                pdv_start_images(tt_pdv_p, 0);                         // start the free-run acquistion

                srtCamTTDataPtr->edtBufStat = eEdtBufStatReadNew;
#               endif   /* RTL_SYSTEM */
#               endif   /* ADD_TIP_TILT */

                HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'set ttcami' complete");
            }
#           endif   /* CAMTT_TYPE */
            break;

        case eTTMapOpen:
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
                printf("traceL5TT\n");  //dwp100405
#               if ADD_TIP_TILT
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
                pdv_start_images(tt_pdv_p, 1);      // stop the free-run acquistion
                edt_disable_ring_buffers(tt_pdv_p);                                           // disable ring buffers

#               ifdef TEST_60X60
                if (pdv_readcfg(EDTCFGDIR "/Lil-Joe-60x60CL.cfg", dd_p, &edtinfo) != 0) // read the open loop config file  //XXX20
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to read 60x60 ol config file");
                if (pdv_initcam(tt_pdv_p, dd_p, TT_UNIT_NUM, &edtinfo, EDTCFGDIR "/Lil-Joe-60x60CL.cfg", NULL, 0) != 0)  // init the cam  //XXX20
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to init 60x60 ol cam");
#               else
                if (pdv_readcfg(EDTCFGDIR "/Lil-Joe-20x20CL.cfg", dd_p, &edtinfo) != 0) // read the open loop config file  //XXX20
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to read 20x20 ol config file");
                if (pdv_initcam(tt_pdv_p, dd_p, TT_UNIT_NUM, &edtinfo, EDTCFGDIR "/Lil-Joe-20x20CL.cfg", NULL, 0) != 0)  // init the cam  //XXX20
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to init 20x20 ol cam");
#               endif
                if ((pdv_multibuf(tt_pdv_p, TTnumbufs)) != 0)  // need to do this to make driver happy
                    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngrTT_ProcCmd) failed to configure EDT ring buffers");
                usleep(10000);

                //crntLoopProg = OpenLoopProg;
                //sprintf (textBuf, "@PRG %d", OpenLoopProg);
                //gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);   // set the camera program

                gpAoSrtCamMngrTT_SendToCam ("@SEQ 1", NULL);  // and restart the camera data
                pdv_start_images(tt_pdv_p, 0);      // and then re-start the free-run acquistion
#               endif   /* ADD_TIP_TILT */

                HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'ttopen' complete");
                printf("traceL6TT\n");  //dwp100405
            }
#           endif  /* CAMTT_TYPE */
            break;

        case eTTMapClose:
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
                printf("traceL7TT\n");  //dwp100405
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);    // stop sending data from the camera
                pdv_start_images(tt_pdv_p, 1);                  // stop the free-run acquistion

                edt_disable_ring_buffers(tt_pdv_p);             // disable ring buffers
#               if ADD_TIP_TILT
                if (pdv_readcfg(EDTCFGDIR "/Lil-Joe-6x6CL.cfg",   dd_p, &edtinfo) != 0) // read the closed loop config file
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to read 6x6 cl config file");
#               endif   /* ADD_TIP_TILT */
#               if USE_TIP_TILT
                if (pdv_initcam(tt_pdv_p, dd_p, TT_UNIT_NUM, &edtinfo, EDTCFGDIR "/Lil-Joe-6x6CL.cfg",   NULL, 0) != 0)  // init the cam
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) failed to init 6x6 cl cam");
#               endif   /* USE_TIP_TILT */
                if ((pdv_multibuf(tt_pdv_p, TTnumbufs)) != 0)  // need to do this to make driver happy
                    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngrTT_ProcCmd) failed to configure EDT ring buffers");
                usleep(10000);

                //crntLoopProg = CloseLoopProg;
                //sprintf (textBuf, "@PRG %d", CloseLoopProg);
                //gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);   // set the camera program

                gpAoSrtCamMngrTT_SendToCam ("@SEQ 1", NULL);  // and restart the camera data
                pdv_start_images(tt_pdv_p, 0);      // and then re-start the free-run acquistion
                printf("traceL8TT\n");  //dwp100405
            }
#           endif  /* CAMTT_TYPE */

            //g_pReconDataPtr->ttCamCloseDone = true;
            break;

        case eTTSetRep:
#           if ( CAMTT_TYPE != TT_REAL_GPI )
// mrr                  AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamRate, 0, GPI_STATE_IDLE, "" );
#           else    /* CAMTT_TYPE */
            if (tt_pdv_p != NULL)
            {
                float ccdTempKelvins = 0.0;

#               if  ADD_TIP_TILT
#               if (RTL_SYSTEM != 0)
                srtCamTTDataPtr->edtBufStat = eEdtBufStatNotInitted;

                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
                usleep(10000);
                pdv_start_images(tt_pdv_p, 1);                         // stop the free-run acquistion
                usleep(10000);
#               endif   /* RTL_SYSTEM */

                // find a legal camera rate ('param' is the desired frame rate) and then set camera operating parameters
                crntFrameRateNdx = 0;
                while ((param > LegalFrameRatesTT[crntFrameRateNdx]) && (LegalFrameRatesTT[crntFrameRateNdx + 1] != -1))
                    crntFrameRateNdx++;

                sprintf (textBuf, "@RCL %d", cam_TT_configProgs[crntFrameRateNdx]);
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);   // set the camera program
                usleep(1000000);

                sprintf (textBuf, "@REP %d", cam_TT_configRepCounts[crntFrameRateNdx]);  // set rep counts (or, accumulator values in SMC terminology)
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);
                usleep(1000000);

                gpAoSrtCamMngrTT_SendToCam ("@TMP?", inString);  // query the temperature
                temperatureCode = -1;
                retVal = sscanf(&inString[1], "@TMP! #0:%d; #1:%d;", &doNotCare, &temperatureCode);
                if ((temperatureCode < 0) || (temperatureCode > 255))
                    HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) invalid ccd temperature code: ", temperatureCode);
                else
                {
                    ccdTemperature = ccdTemperatureLUT[temperatureCode];  // ccdTemperature will be in degrees C
                    ccdTempKelvins = ccdTemperature + 273.15;
                    HostDiagSendStrngFltParm(eDebugLevelMin, "INFO (gpAoSrtCamMngrTT_ProcCmd) ccd temperature (degrees C): ", ccdTemperature);
                }

                // test CCD temperature limits for TLC -- most TLC errors have persistence counters; but, because the CCD temp
                // > should never be out of range (and because it's tested so infrequently), being out of range once is enough
                if (ccdTempKelvins < srtFileDataShmPtr->rangesTtCcdTemp[1])
                {
                    if (ccdTempKelvins < srtFileDataShmPtr->rangesTtCcdTemp[0])
                    {
                        srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrTtCCD] = eTlcErrLowLow;
                        srtFileDataShmPtr->tlcErrStatArray[eTlcErrTtCCD] = eTlcErrLowLow;
                    }
                    else
                    {
                        srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrTtCCD] = eTlcErrLow;
                        srtFileDataShmPtr->tlcErrStatArray[eTlcErrTtCCD] = eTlcErrLow;
                    }
                }
                else if (ccdTempKelvins > srtFileDataShmPtr->rangesTtCcdTemp[2])
                {
                    if (ccdTempKelvins > srtFileDataShmPtr->rangesTtCcdTemp[3])
                    {
                        srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrTtCCD] = eTlcErrHighHigh;
                        srtFileDataShmPtr->tlcErrStatArray[eTlcErrTtCCD] = eTlcErrHighHigh;
                    }
                    else
                    {
                        srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrTtCCD] = eTlcErrHigh;
                        srtFileDataShmPtr->tlcErrStatArray[eTlcErrTtCCD] = eTlcErrHigh;
                    }
                }
                else
                    srtFileDataShmPtr->tlcErrStatArray[eTlcErrTtCCD] = eTlcErrNone;
                srtFileDataShmPtr->tlcErrCCDTempKelvins = ccdTempKelvins;

#               if (RTL_SYSTEM != 0)
                if ((pdv_multibuf(tt_pdv_p, TTnumbufs)) != 0)  // need to do this to make driver happy
                    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngrTT_ProcCmd) failed to configure EDT ring buffers");
                usleep(10000);
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
                pdv_start_images(tt_pdv_p, 0);                         // start the free-run acquistion

                srtCamTTDataPtr->edtBufStat = eEdtBufStatReadNew;
#               endif   /* RTL_SYSTEM */
#               endif   /* ADD_TIP_TILT */

                HostDiagSendStrngIntParm(eDebugLevelMin, "INFO 'setCamRate' to ", LegalFrameRatesTT[crntFrameRateNdx]);
// mrr                          AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamRate, 0, GPI_STATE_IDLE, "" );
            }
#           endif   /* CAMTT_TYPE */
            break;

        case eTTSetAtten:
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
#               if  ADD_TIP_TILT
#               if (RTL_SYSTEM != 0)
                srtCamTTDataPtr->edtBufStat = eEdtBufStatNotInitted;

                gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
                pdv_start_images(tt_pdv_p, 1);                         // stop the free-run acquistion
#               endif   /* RTL_SYSTEM */

                sprintf (textBuf, "@AAM %d", param);    // param is the attenuation for all modules
                gpAoSrtCamMngrTT_SendToCam (textBuf, NULL);

                HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'setCamGain' complete");
// mrr                          AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamGain, 0, GPI_STATE_IDLE, "" );

#               if (RTL_SYSTEM != 0)
                if ((pdv_multibuf(tt_pdv_p, TTnumbufs)) != 0)  // need to do this to make driver happy
                    HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoSrtCamMngrTT_ProcCmd) failed to configure EDT ring buffers");
                usleep(10000);
                gpAoSrtCamMngrTT_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera
                pdv_start_images(tt_pdv_p, 0);                         // start the free-run acquistion

                srtCamTTDataPtr->edtBufStat = eEdtBufStatReadNew;
#               endif   /* RTL_SYSTEM */
#               endif   /* ADD_TIP_TILT */
            }
#           endif   /* CAMTT_TYPE */
            break;

        // NOTE that neither eTTReadRep nor eTTReadAtten are used at present
        case eTTReadRep:  // read and report the REP as a frame rate
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
                gpAoSrtCamMngrTT_SendToCam ("@PRG?", inString);
                // reply format: "ACK@PRG! n" , where ACK is 0x6 and n is the program setting
                retVal = sscanf (&inString[7], "%x", &Prog);

                gpAoSrtCamMngrTT_SendToCam ("@REP?", inString);
                // reply format: "ACK@REP! $00hhhh" , where ACK is 0x6 and hhhh is the hex rep count
                retVal = sscanf (&inString[10], "%x", &Count);

                i = 0;
                while ((Prog != cam_TT_configProgs[i]) && (LegalFrameRatesTT[i] != -1))
                    i++;
                while ((Count != cam_TT_configRepCounts[i]) && (LegalFrameRatesTT[i] != -1))
                    i++;

                if (LegalFrameRatesTT[i] == -1)
                    HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (eTTReadRep) invalid Prog or Count");
                else
                {
                    FrameRate = 1.0 / ((cam_TT_fps_factor[i] * Count) + cam_TT_fps_offset[i]);
                    HostDiagSendStrngFltParm(eDebugLevelMin, "INFO -- TT camera rate: ", FrameRate);
                    HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamRate' complete");
                }
            }
#           else  // simulated
            FrameRate = LegalFrameRatesTT[ srtFileDataShmPtr->ttCamFrameRate ];
            HostDiagSendStrngFltParm(eDebugLevelMin, "INFO -- TT camera rate: ", FrameRate);
            HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamRate' complete");
#           endif /* CAMTT_TYPE */
            break;

        // NOTE that neither eTTReadRep nor eTTReadAtten are used at present
        case eTTReadAtten:             // read and report the attenuation as a gain
#           if ( CAMTT_TYPE == TT_REAL_GPI )
            if (tt_pdv_p != NULL)
            {
                gpAoSrtCamMngrTT_SendToCam ("@AIM? #0", inString);
                // reply format: "ACK@AIM! #0:V", where ACK is 0x6 and V is the atten value
                retVal = sscanf (&inString[10], "%d", &Atten);
                Gain = 3 - Atten;

                HostDiagSendStrngIntParm(eDebugLevelMin, "INFO -- TT camGain: ", Gain);
                HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamGain' complete");
            }
#           endif   /* CAMTT_TYPE */
            break;

        default:
            HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_ProcCmd) unexpected command in cam_TT_Mngr: ", cmd);
            break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoSrtCamMngrTT_StartCam( void )
 *
 *  @brief
 *  Initialize the TT Camera
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the TT Cam driver and set up the shared
 *  memory for the DMA ring buffers.  The memory size is provided from the HRT in
 *  cam_TT.
 *  @par
 *
 *  - Request shared memory size from HRT Cam module.
 *  - Open/allocate shared memory
 *  - Init TT Camera Interface
 *  @par
 *
 *  @b Usage:
 *  gpAoSrtCamMngrTT_StartCam( );
 *
 *  @b Param None.
 *
 *  @return
 *  None.
 *
 *  @exception
 *  Invalid fileCmd or problem opening, reading, shared memory.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

int gpAoSrtCamMngrTT_StartCam(void)
{
#   if (CAMTT_TYPE == TT_REAL_GPI)
    char devname[128];

    // open the edt interface (EDT_INTERFACE is defined in edtdef.h (included via edtinc.h))
    strcpy(devname, EDT_INTERFACE);
    HostDiagSendStrngIntParm(eDebugLevelMin, "INFO (gpAoSrtCamMngrTT_StartCam) Opening camTT EDTpdv device, unit: ", TT_UNIT_NUM);
#   if USE_TIP_TILT
    tt_pdv_p = pdv_open_channel(devname, TT_UNIT_NUM, 0 /* channel */);
#   endif /* USE_TIP_TILT */
    if (tt_pdv_p == NULL)
    {
        HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_StartCam) pdv_open failed, unit:", TT_UNIT_NUM);
        return 0;
    }
    dd_p = pdv_alloc_dependent();

#   if ( RTL_SYSTEM == 0 )
    TT_dev = tt_pdv_p;  // this will only work on the user-side
#   endif   /* RTL_SYSTEM */
#   endif   /* CAMTT_TYPE */
    return 1;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoSrtCamMngrTT_StopCam()
 *
 *  @brief
 *  Initialize the TT Camera
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to close the EDT driver and unmap the shared
 *  memory for the DMA ring buffers.
 *
 *  @b Usage:
 *  gpAoSrtCamMngrTT_StopCam( );
 *
 *  @b Param None.
 *
 *  @return
 *  None.
 *
 *  @exception
 *  Invalid fileCmd or problem opening, reading, shared memory.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void gpAoSrtCamMngrTT_StopCam(void)
{
#if (CAMTT_TYPE == TT_REAL_GPI)
    if (tt_pdv_p != NULL)
    {
        gpAoSrtCamMngrTT_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
#if (RTL_SYSTEM != 0)
        pdv_start_images(tt_pdv_p, 1);                         // stop the free-run acquistion
#endif
        pdv_close(tt_pdv_p);
    }
#endif
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoSrtCamMngrTT_SendToCam( char outString[], char inString[] )
 *
 *  @brief
 *  Send an ASCII command to the camera and read the response.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send an ASCII command to the camera and read the response.
 *
 *  @b Usage:
 *  gpAoSrtCamMngrTT_SendToCam( outString, inString )
 *
 *  @param[in] inString (char[]): The command string to send to camera
 *  @param[out] outString (char[]): A char array to put the camera response into, if any, or NULL
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

#define LOCAL_IN_STRING_LENGTH 256

static void gpAoSrtCamMngrTT_SendToCam (char outString[], char inString[])
{
#   if ( CAMTT_TYPE == TT_REAL_GPI )
    int    retVal, inCount;
    char   localInString[LOCAL_IN_STRING_LENGTH], errMsg[128];
    char   tmpText[DIAG_STRING_MAX_LENGTH];
    struct timespec next;

    // don't bother doing anything, if there's no EDT board or the camera isn't on
    //XXX looks like pdv_cl_camera_connected() isn't working
    //XXXdwp101124  if ((tt_pdv_p == NULL) || (pdv_cl_camera_connected(tt_pdv_p) == 0))
    if (tt_pdv_p == NULL)
    {
        sprintf(tmpText, "INFO (gpAoSrtCamMngrTT_SendToCam) tt_pdv_p is NULL, not sending: %s", outString);
        HostDiagSendStrngNoParm(eDebugLevelMin, tmpText);
        return;
    }

    sprintf(tmpText, "INFO (gpAoSrtCamMngrTT_SendToCam) sending to TT camera: %s", outString);
    HostDiagSendStrngNoParm(eDebugLevelMin, tmpText);

#   if  USE_TIP_TILT
    // indicate that interrrupt should be ignored (because we're doing serial comms)
    srtCamTTDataPtr->intrIgnore = true;

    // make sure the string to be returned is null terminated
    if (inString != NULL)
        inString[0] = '\0';

    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

    // flush any junk from the read buffer and issue command to camera
    if ((inCount = pdv_serial_wait(tt_pdv_p, 1, sizeof(localInString) - 1)) != 0)
    {
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator
        pdv_serial_read(tt_pdv_p, localInString, inCount);
    }
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

    retVal = pdv_serial_command(tt_pdv_p, outString);
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);

    // if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
    if (retVal != 0)
        HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_SendToCam) from pdv_serial_command(): ", retVal);
    else
    {
        if (inString == NULL)
            inCount = pdv_serial_wait(tt_pdv_p, 1000, 1);
        else
            inCount = pdv_serial_wait(tt_pdv_p, 2000, LOCAL_IN_STRING_LENGTH - 1);  // the '- 1' is to accommodate a null terminator
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator

        next.tv_sec = 0;
        next.tv_nsec = 300000000;
        nanosleep(&next, NULL);

        if (inCount == 0)
            HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_SendToCam) no response from CamTT 1");
        else
        {
            retVal = pdv_serial_read(tt_pdv_p, localInString, inCount);
            if (retVal == 0)
                HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_SendToCam) no response from CamTT 2");
            else if (retVal != inCount)
                HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_SendToCam) from pdv_serial_read(): ", retVal);
            else if (localInString[0] != ACK)
                HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (gpAoSrtCamMngrTT_SendToCam) no ACK from CamTT");
            else if (retVal > 2)
            {
                localInString[retVal - 2] = '\0';
                sprintf(errMsg, "INFO (gpAoSrtCamMngrTT_SendToCam) msg of lngth %d from CamTT: %s", retVal, &localInString[1]);
                HostDiagSendStrngNoParm(eDebugLevelMin, errMsg);
                if (inString != NULL)
                    strcpy(inString, localInString);
            }
        }
    }

    // indicate that interrupts should no longer be ignored
    srtCamTTDataPtr->intrIgnore = false;
#   endif   /* USE_TIP_TILT */
#   endif  // ( CAMTT_TYPE == TT_REAL_GPI )
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoSrtCamMngrTT_ReadSimTtData( char *filename )
 *
 *  @brief
 *  The purpose of this unit is to read a simulated tt data fits file into shared memory.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to read a simulated tt data fits file into shared memory
 *  for use in sim mode.
 *
 *  @b Usage:
 *  gpAoSrtCamMngrTT_ReadSimTtData( filename )
 *
 *  @param[in] filename (char[]): name of fits file containing simulated data
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
int
gpAoSrtCamMngrTT_ReadSimTtData( char *fileName )
{
    //int        nDim = 3;      // mrr
    int         nDim = 2;
    long        dimSize[10];
    int         dataType = TUSHORT;

    //dimSize[0] = TTNUM_ROWS_IN_CAM;   // mrr
    //dimSize[1] = TTNUM_PIX_IN_ROW;
    //dimSize[2] = TTnumbufs;
    dimSize[0] = TTNUM_ROWS_IN_CAM_ACTUAL;
    dimSize[1] = TTNUM_PIX_IN_ROW_ACTUAL;
    dimSize[2] = TTnumbufs;

    printf ( " (gpAoSrtCamMngrTT_ReadSimTtData) attempting to read simulation data from: %s, nDim = %d\n", fileName, nDim);

#   if  ADD_TIP_TILT
    if( readFits( fileName, &srtCamTTDataPtr->simDataBuffs[0][0][0], &nDim, dimSize, &dataType )) {
        srtCamTTDataPtr->simFileRead = true;
        printf (" (gpAoSrtCamMngrTT_ReadSimTtData) successful read of simulation data\n");
    } else {
        printf("WARN (gpAoSrtCamMngrTT_ReadSimTtData) File read failed for simulation data\n");
        srtCamTTDataPtr->simFileRead = false;
        return 0;
    }
#   endif   /* ADD_TIP_TILT */
    return 1;
}


/**********************************************************************************************

UNIT NAME:  readFits()

UNIT DESCRIPTION:
This unit reads a fits file from disk.

UNIT INPUTS:
    filename - name of fits file to read
    data - array into which data should be read (must be preallocated)

UNIT OUTPUTS:
    data - data that was read

UNIT ERROR CODES:
    Returns 1 if OKAY
    Returns 0 if error


**********************************************************************************************/

static int readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType )
{
    fitsfile *fptr;
    int bits_per_pixel;
    int status = 0;
    int nDim = 0;
    long dimSize[10];
    long nElem = 0L;
    int ii;
    long fpixel[10];

    // check for data
    if( pData == NULL)
        return 0;

    printf(" (camMngrTT readFits) attempting to open %s. pNDim = %d\n", filename, *pNDim);

    //      open the file
    fits_open_file(&fptr, filename, READONLY, &status);
    if( status != 0)
    {
printf("\n********************** %s  (camMngrTT readFits) failed to open. status = %d\n\n", filename, status);
        pData = NULL;
        return 0;
    }

    // Not allocating - so check that input values same as file specs.
    fits_get_img_param( fptr, 3, &bits_per_pixel, &nDim, &dimSize[0], &status );
    if( status != 0)
    {
printf("\n********************** %s  (camMngrTT readFits) failed param status = %d\n\n", filename, status);
        pData = NULL;
        return 0;
    }
    printf(" (camMngrTT readFits)  sim data bits-per-pixel = %d\n", bits_per_pixel);
    if( nDim != *pNDim )
    {
printf("\n********************** %s  (camMngrTT readFits) failed nDim = %d    pNDim = %d\n\n", filename, nDim,  *pNDim);
        pData = NULL;
        fits_close_file( fptr, &status );
        return 0;
    }

    nElem = 1;
    for( ii=0; ii< nDim; ii++)
    {
        fpixel[ii] = 1;
        nElem *= dimSize[ii];
        if( dimSize[ii] != pDimSize[ii])
        {
printf("\n********************** %s  (camMngrTT readFits) failed DimSize[ii] = %ld    pDimSize[ii] = %ld    ii = %d\n\n", filename, dimSize[ii],  pDimSize[ii], ii );
            pData = NULL;
            fits_close_file( fptr, &status );
            return 0;
        }
    }

    // All seems happy so read file
    fits_read_pix( fptr, *pDataType, fpixel, nElem, NULL, pData, NULL, &status);

    //      close the file
    fits_close_file(fptr, &status);

    return 1;
}


//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
