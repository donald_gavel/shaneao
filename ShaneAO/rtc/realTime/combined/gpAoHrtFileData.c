/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file gpAoHrtFileData.c
 *
 *  @brief
 *  Moves data to and from shared memory for file reading and writing by the SRT.
 *
 *  @b DESCRIPTION:
 *  This component contains functions to put and get data to and from shared memory
 *  and to command the SRT side to read and write files as necessary.  Although
 *  these functions are in a separate file, they are called from and, therefore,
 *  run as part of other threads on the HRT side.  This component's complement on
 *  the SRT side is gpAoSrtFileMngr.c.
 *
 *  File data is moved to and from the SRT side, through shared memory, for writing
 *  and reading because RTLinux cannot do disk accesses (from the kernel).  If this
 *  changes or another RTOS is used, this shuffling may not be necessary.
 *
 *  @b AUTHOR:
 *  DWP & SMJ    DATE: 11/26/08
 *
 *  @b FUNCTION NAMES:
 *  -# @c gpAoHrtFileData_Init()
 *  -# @c gpAoHrtFileData_Cmd()
 *  -# @c gpAoHrtFileData_Get()
 *  -# @c gpAoHrtFileData_Put()
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <string.h>
#include <sys/mman.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#endif

#include "gpAoHrtParse.h"
#include "gpAoHrtFileData.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"

// Global Variables


// Local Variables

static float hrt_calSNR_Gain;  // not used on the HRT side, at present


// Local Function Prototypes


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtFileData_Init( void )
 *
 *  @brief
 *  Initialize variables for gpAoHrtFileData.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize both the global and the HRT program
 *  variables used by the HRT-side  file data handler.
 *
 *  @par
 *
 *  @b Usage:
 *  retVal = gpAoHrtFileData_Init();
 *
 *  @b Param None.
 *
 *  @return
 *  (int) 0 = success.
 *
 *  @exception None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void gpAoHrtFileData_Init( void )
{
    short ii, jj, kk, mm;

//  initialize shared memory control variables
    hrtFileDataShmPtr->allDataAvailFromFile = false;
    hrtFileDataShmPtr->someDataAvailFromFile = false;

    for (ii = 0; ii < eFileTypeNum; ii++)
    {
        hrtFileDataShmPtr->dataAvailFromFile[ii] = false;
        hrtFileDataShmPtr->dataShmLockHRT[ii] = false;
        hrtFileDataShmPtr->dataShmLockSRT[ii] = false;
    }

//  initialize shared memory data variables (almost all of these should be overwritten by file data)
//  parms data
    hrtFileDataShmPtr->systemDebug = 0;  // default to no debug
#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
    hrtFileDataShmPtr->tlcOnFlag = -1;    // default to TLC off, send once per second messages
#else
    hrtFileDataShmPtr->tlcOnFlag = 1;    // default to TLC on
#endif

    for (ii = 0; ii < eFileTypeNum; ii++)
        hrtFileDataShmPtr->crntFilenames[ii][0] = '\0';

    hrt_useWfsSimData = hrtFileDataShmPtr->useWfsSimData = 0;  // do not use simulated data
    hrt_applyCalData = hrtFileDataShmPtr->applyCalData = 0;    // do not apply Cal data
    hrtFileDataShmPtr->selectorForInFuncMultiplier = 2;           // do use influence function gains -- this is NOT stored in parms

    hrt_applyBackData = hrtFileDataShmPtr->applyBackData = INIT_WFS_BACK_APPLY;
    hrt_applyDarkData = hrtFileDataShmPtr->applyDarkData = INIT_WFS_DARK_APPLY;
    hrt_applyFlatData = hrtFileDataShmPtr->applyFlatData = INIT_WFS_FLAT_APPLY;

    hrt_BacksAvgN = hrtFileDataShmPtr->wfsBacksAvgN = INIT_WFSDARKBACK_AVGN;
    hrt_FlatsAvgN = hrtFileDataShmPtr->wfsFlatsAvgN = INIT_WFSFLAT_AVGN;
    hrt_centAvgN = hrtFileDataShmPtr->centAvgN = INIT_REFCENT_AVGN;
    hrt_MirrorShapeAvgN = hrtFileDataShmPtr->MirrorShapeAvgN = INIT_MIRRORSHAPE_AVGN;

    for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
       hrt_selectModalGains[ ii ] = hrtFileDataShmPtr->selectModalGains[ii] = 0x8;            // Default to 'standardInitialValue' set of gains

    hrt_smAvgN = hrtFileDataShmPtr->smAvgN = INIT_SM_AVGN;

    hrt_camFrameRate = hrtFileDataShmPtr->wfsCamFrameRate = INIT_FRAME_RATE_INDEX;        // Sets the INDEX into 'legal_frameRate' array

    hrtFileDataShmPtr->wfsCamGain = hrt_camGain = INIT_CAM_GAIN;
    hrtFileDataShmPtr->calSNR_Gain = 1.0;

    hrt_mode_cleanup_thresh = 0.0;
    hrt_mode_cleanup_gain = 0.0;

    hrt_modal_loss_dm = hrtFileDataShmPtr->param_modal_loss_dm;
    hrt_modal_loss_tt = hrtFileDataShmPtr->param_modal_loss_tt;

    hrtFileDataShmPtr->param_tt_x_offset = hrt_tt_x_offset = 0.0;
    hrtFileDataShmPtr->param_tt_y_offset = hrt_tt_y_offset = 0.0;

    hrtFileDataShmPtr->wfsCamGain = hrt_camGain = INIT_CAM_GAIN;
    hrtFileDataShmPtr->calSNR_Gain = 1.0;

    hrtFileDataShmPtr->centThresh = hrt_CentThresh = INIT_CENT_THRESH;

    hrtFileDataShmPtr->centroidGainX = hrt_centroidGainX = 1.0;
    hrtFileDataShmPtr->centroidGainY = hrt_centroidGainY = 1.0;

    hrtFileDataShmPtr->darkCornerCorrectSwitch = hrt_darkCornerCorrectSwitch = 0.0;  // default is off

    hrtFileDataShmPtr->offloadM1M2 = 0;                  // default is off
    hrtFileDataShmPtr->offloadZernRotationAngle = 0;  // default is zero degrees
    hrtFileDataShmPtr->offloadZernFlip = 0;              // default is do not flip

    hrtFileDataShmPtr->ttGainStage = hrt_TTGainStage = INIT_TT_GAIN;
    hrtFileDataShmPtr->ttGainWfrSurface = hrt_TTGainWfrSurface = INIT_TT_GAIN;

    hrtFileDataShmPtr->ttSplitSwitch = hrt_ttSplitSwitch = 0;  // split TT between TT stage and woofer
    hrtFileDataShmPtr->ttLQGSwitch = hrt_ttLQGSwitch = true;   // turns TT LQG filter on
    hrtFileDataShmPtr->ttWfr9ActMicronsPerMASX = hrt_ttWfr9ActMicronsPerMASX = 0.01125;
    hrtFileDataShmPtr->ttWfr9ActMicronsPerMASY = hrt_ttWfr9ActMicronsPerMASY = 0.01125;

    hrtFileDataShmPtr->ttCompDelay_microSecs = hrt_ttCompDelay_microSecs = 0.0;

    hrtFileDataShmPtr->ttSeeing = hrt_ttSeeing = 0.0;
    hrtFileDataShmPtr->ttWindPower = hrt_ttWindPower = 0.0;
    hrtFileDataShmPtr->ttStarBrightness = hrt_ttStarBrightness = 0.0;

    hrtFileDataShmPtr->m1ReadBuffNdx = -1;  // read index into triply buffered M1 offload buffer -- -1 means no buffer has been written yet
    hrtFileDataShmPtr->m2ReadBuffNdx = -1;  // read index into triply buffered M2 offload buffer -- -1 means no buffer has been written yet

    hrtFileDataShmPtr->dmSlavingSwitch = hrt_dmSlavingSwitch = (DM_SLAVE_SWITCH_WFR_BIT | DM_SLAVE_SWITCH_TWT_BIT);  // default is both on
    hrtFileDataShmPtr->twtNeighborLimit = hrt_twtNeighborLimit = 0.4;  // default tweeter neighbor interactuator limit, in microns

    hrtFileDataShmPtr->localWaffleGain = hrt_localWaffleGain = 0.0;     // gain applied to local waffle suppressor  //XXXdwp110816
    hrtFileDataShmPtr->overThreshFraction = hrt_overThreshFraction = 0.0;  // fraction of sub-aps whose intensity must be over the centThresh for frame to be used  //XXXdwp110816

    for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
        hrt_twtCoeffUsed_Array[ii] = 0;

//  wfs: bkGrnd, dark, flat  data
    for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
    {
        for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
        {
            for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
            {
                hrt_wfsDarkData[ii][jj][kk] = 0;
                hrt_wfsBkGrndData[ii][jj][kk] = 0;
                hrt_wfsFlatData[ii][jj][kk] = 1.0f;
                for( mm=0; mm<5; mm ++)
                {
                    hrtFileDataShmPtr->wfsDarkData[mm][ii][jj][kk]   = 0;
                    hrtFileDataShmPtr->wfsBkGrndData[mm][ii][jj][kk] = 0;
                    hrtFileDataShmPtr->wfsFlatData[mm][ii][jj][kk]   = 1.0f;
                }
            }
        }
    }

//  dm polys
    hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff = MAX_NUM_DM_COEFF;
    for( jj=0; jj<MAX_TWEETER_ACTS; jj++)
    {
        hrtFileDataShmPtr->twtPhaseToVoltsCoeff[0][ii] = -400.0f;
        hrtFileDataShmPtr->twtPhaseToVoltsCoeff[1][ii] = 0.0f;
        hrtFileDataShmPtr->twtPhaseToVoltsCoeff[2][ii] = 0.03f;
        for (ii=3; ii<MAX_NUM_DM_COEFF; ii++)
            hrtFileDataShmPtr->twtPhaseToVoltsCoeff[ii][jj] = 0.0f;
    }

    hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff = MAX_NUM_DM_COEFF;
    for( jj=0; jj<MAX_WOOFER_ACTS; jj++)
    {
        hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[0][ii] = -800.0f;
        hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[1][ii] = 0.0f;
        hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[2][ii] = 0.07f;
        for (ii=3; ii<MAX_NUM_DM_COEFF; ii++)
            hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[ii][jj] = 0.0f;
    }

    hrtFileDataShmPtr->numTT_Phase2VoltsCoeff = MAX_NUM_DM_COEFF;
    for( jj=0; jj<MAX_TT_ACTS; jj++)
    {
        hrtFileDataShmPtr->ttPhaseToVoltsCoeff[0][ii] = -1.0f;
        hrtFileDataShmPtr->ttPhaseToVoltsCoeff[1][ii] = 0.0f;
        hrtFileDataShmPtr->ttPhaseToVoltsCoeff[2][ii] = 0.1f;
        for (ii=3; ii<MAX_NUM_DM_COEFF; ii++)
            hrtFileDataShmPtr->ttPhaseToVoltsCoeff[ii][jj] = 0.0f;
    }

//  refcent data
//  centoff data
    for (ii = 0; ii < hrt_numCentroids; ii++)
    {
        hrtFileDataShmPtr->shm_refCentsQ[ ii ] = 0.0f;
        hrtFileDataShmPtr->shm_offCentsQ_offsets[ ii ] = 0.0f;
        hrtFileDataShmPtr->shm_offCentsQ_Cal_RePhase[ ii ] = 0.0f;
        hrtFileDataShmPtr->shm_offCentsQ_Cal[ ii ] = 0.0f;

        for( jj = 0; jj < 9; jj++ )
            hrtFileDataShmPtr->shm_offCentsQ_Apodizer[ jj ][ ii ] = 0.0f;

        hrt_currRefCentsQ[ii] = 0.0f;
    }

//  Twt lab flat, sys flat, shape data
    for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
    {
        hrtFileDataShmPtr->twtLabFlat[ii] = 0.0;
        hrt_twtLabFlat[ii] = 0.0;

        hrtFileDataShmPtr->twtSysFlat[ii] = 0.0;
        hrt_twtSysFlat[ii] = 0.0;

        hrtFileDataShmPtr->twtDmShape[ii] = 0.0;
        hrt_twtDmShape[ii] = 0.0;

        hrtFileDataShmPtr->twtHostDmOffset[ii] = 0.0;
        hrt_twtHostOffset[ii] = 0.0;
    }

//  Wfr lab flat, sys flat, shape data
    for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
    {
        hrtFileDataShmPtr->wfrLabFlat[ii] = 0.0;
        hrt_wfrLabFlat[ii] = 0.0;

        hrtFileDataShmPtr->wfrSysFlat[ii] = 0.0;
        hrt_wfrSysFlat[ii] = 0.0;

        hrtFileDataShmPtr->wfrDmShape[ii] = 0.0;
        hrt_wfrDmShape[ii] = 0.0;

        hrtFileDataShmPtr->wfrHostDmOffset[ii] = 0.0;
        hrt_wfrHostOffset[ii] = 0.0;
    }

//  twt cleanup matrix init
    for( ii = 0; ii < MAX_TWEETER_CLEAN_MODES; ii++)
    {
        for( jj = 0; jj < MAX_TWEETER_ACTS; jj++ )
        {
            hrtFileDataShmPtr->FTR_twtCleanup[ jj ][ ii ] = 0.0;  // indexes backwards due to IDL
            hrt_twtCleanup[ ii ][ jj ] = 0.0;
        }
    }

//  wfr cleanup matrix init
    for (ii = 0; ii < MAX_WOOFER_CLEAN_MODES; ii++)
    {
        for( jj = 0; jj < MAX_WOOFER_ACTS; jj++)
        {
            hrtFileDataShmPtr->FTR_wfrCleanup[ jj ][ ii ] = 0.0;  // indexes backwards due to IDL
            hrt_wfrCleanup[ ii ][ jj ] = 0.0;
        }
    }

//  filter real, img, denom
    for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
    {
        ReconAlgFTR_filterX[ ii ][ 0 ] = ReconAlgFTR_filterX[ ii ][ 1 ] = 1.0f;
        ReconAlgFTR_filterY[ ii ][ 0 ] = ReconAlgFTR_filterY[ ii ][ 1 ] = 1.0f;
        ReconAlgFTR_filterDenom[ ii ] = 1.0f;

        hrtFileDataShmPtr->FTR_reconFilter[0][ ii ] = 1.0f;
        hrtFileDataShmPtr->FTR_reconFilter[1][ ii ] = 1.0f;
        hrtFileDataShmPtr->FTR_reconFilter[2][ ii ] = 1.0f;
        hrtFileDataShmPtr->FTR_reconFilter[3][ ii ] = 1.0f;
        hrtFileDataShmPtr->FTR_reconFilter[4][ ii ] = 1.0f;

        hrt_hostModalGains[ ii ] = hrtFileDataShmPtr->hostModalGains[ ii ] = 0.11f;
        hrt_initialModalGains[ ii ] = hrtFileDataShmPtr->initialModalGains[ ii ] = 0.22f;

        ((float *)ParseAlgFTR_TwtInfFuncMask)[ ii ] = 1.0;
        ((float *)(hrtFileDataShmPtr->FTR_TwtInfFuncMask))[ ii ] = 1.0;
    }

    ParseAlgFTR_wfrVecSize = -1;

    for( ii=0; ii<MAX_WFR_VEC_SIZE; ii++)
    {
        for( jj=0; jj<NUM_ACTIVE_WFR_ACTS; jj++)
        {
            ParseAlgFTR_WfrControlMatrix[ ii ][ 2*jj ] = 0.0;
            ParseAlgFTR_WfrControlMatrix[ ii ][ 2*jj + 1 ] = 0.0;
            hrtFileDataShmPtr->FTR_wfrControlMatrix[ ii ][ 2*jj ] = 0.0;
            hrtFileDataShmPtr->FTR_wfrControlMatrix[ ii ][ 2*jj + 1 ] = 0.0;
        }

        hrtFileDataShmPtr->FTR_wfrVectorList[ ii ] = 0;
        ParseAlgFTR_WfrVectorList[ ii ] = 0;
    }

    for( ii=0; ii<(MAX_WFR_VEC_SIZE*2); ii++)
        ParseAlgFTR_TwtZeroList[ ii ] = -1;        // force error if this is not set

    for( jj=0; jj<NUM_ACTIVE_WFR_ACTS; jj++)
        ParseAlgFTR_wfrActMap[ jj ] = jj;            // Create dummy map for now

    // Set file errors to all 'false' at this point
    for( ii=0; ii<eFileTypeNum ; ii++)
    {
        hrtFileDataShmPtr->dataFileError[ ii ] = false;
    }
}

/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtFileData_Cmd( short cmd )
 *
 *  @brief
 *  Send commands to the SRT to read and write files.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send commands to the SRT to read and write files
 *  into and from shared memory.
 *
 *  NOTE1 that this is the only function in this code that must be re-entrant.
 *  NOTE2 This code does NOT move data into/out of the HRT program memory.
 *
 *  @par
 *
 *  @b Usage:
 *  retVal = gpAoHrtFileData_Cmd( cmd );
 *
 *  @param[in] cmd (int): Indicates to the SRT side which files should be read or written.
 *
 *  @return
 *  (int) 0 = success, !=0 = failure.
 *
 *  @exception
 *  Error writing the command to the SRT FIFO.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void gpAoHrtFileData_Cmd( short cmd )
{
    diagFIFOMsgType diagFIFOMsg;

//  set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagCmdFile;
    diagFIFOMsg.cmdNdx = cmd;
    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR writing file command to diag FIFO, errno = ", rtl_errno);
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtFileData_Get( short fileType )
 *
 *  @brief
 *  Get new file data from shared memory.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to check whether there is new file data in shared memory
 *  and, if there is, move it to HRT program variables, copies of the global shared memory.
 *
 *  @par
 *
 *  @b Usage:
 *  retVal = gpAoHrtFileData_Get( fileType );
 *
 *  @param[in] fileType (void*): Indicates file type to check and get data for.
 *
 *  @return
 *  (int) 0 = success, !=0 = failure.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  Unexpected file type.
 *
 *  @callgraph
 *******************************************************************************
 */

char gpAoHrtFileData_Get( short fileType )
{
    short ii, jj, kk;

//  if there is new file data in shared memory
    if (hrtFileDataShmPtr->dataAvailFromFile[fileType])
    {
//      move it to program variables
        switch (fileType)
        {
        case eFileTypeParms:    // parameter data
#if (CAM_TYPE != WFS_REAL_GPI)
            hrt_useWfsSimData = 1;  // it there's no real wfs camera, force the use of simulated data (regardless of parms file)
#else
            hrt_useWfsSimData = hrtFileDataShmPtr->useWfsSimData;
#endif

            hrt_applyBackData = hrtFileDataShmPtr->applyBackData;
            hrt_applyDarkData = hrtFileDataShmPtr->applyDarkData;
            hrt_applyFlatData = hrtFileDataShmPtr->applyFlatData;

            hrt_BacksAvgN = hrtFileDataShmPtr->wfsBacksAvgN;
            hrt_FlatsAvgN = hrtFileDataShmPtr->wfsFlatsAvgN;
            hrt_centAvgN = hrtFileDataShmPtr->centAvgN;
            hrt_smAvgN = hrtFileDataShmPtr->smAvgN;
            hrt_MirrorShapeAvgN = hrtFileDataShmPtr->MirrorShapeAvgN;

            hrt_camFrameRate = hrtFileDataShmPtr->wfsCamFrameRate;

            hrt_camGain = hrtFileDataShmPtr->wfsCamGain;
            hrt_calSNR_Gain = hrtFileDataShmPtr->calSNR_Gain;

            hrt_CentThresh = hrtFileDataShmPtr->centThresh;

            hrt_centroidGainX = hrtFileDataShmPtr->centroidGainX;
            hrt_centroidGainY = hrtFileDataShmPtr->centroidGainY;

            hrt_darkCornerCorrectSwitch = hrtFileDataShmPtr->darkCornerCorrectSwitch;

            hrt_TTGainStage = hrtFileDataShmPtr->ttGainStage;
            hrt_TTGainWfrSurface = hrtFileDataShmPtr->ttGainWfrSurface;

            hrt_ttSplitSwitch = hrtFileDataShmPtr->ttSplitSwitch;
            hrt_ttLQGSwitch = hrtFileDataShmPtr->ttLQGSwitch;
            hrt_ttWfr9ActMicronsPerMASX = hrtFileDataShmPtr->ttWfr9ActMicronsPerMASX;
            hrt_ttWfr9ActMicronsPerMASY = hrtFileDataShmPtr->ttWfr9ActMicronsPerMASY;

            hrt_ttCompDelay_microSecs = hrtFileDataShmPtr->ttCompDelay_microSecs;

            hrt_ttSeeing = hrtFileDataShmPtr->ttSeeing;
            hrt_ttWindPower = hrtFileDataShmPtr->ttWindPower;
            hrt_ttStarBrightness = hrtFileDataShmPtr->ttStarBrightness;

            hrt_mode_cleanup_thresh = hrtFileDataShmPtr->param_mode_cleanup_thresh;
            hrt_mode_cleanup_gain = hrtFileDataShmPtr->param_mode_cleanup_gain;

            hrt_modal_loss_dm = hrtFileDataShmPtr->param_modal_loss_dm;
            hrt_modal_loss_tt = hrtFileDataShmPtr->param_modal_loss_tt;

            hrt_tt_x_offset = hrtFileDataShmPtr->param_tt_x_offset;
            hrt_tt_y_offset = hrtFileDataShmPtr->param_tt_y_offset;

            ParseAlgFTR_wfrVecSize = hrtFileDataShmPtr->FTR_wfrVecSize;

            hrt_dmSlavingSwitch = hrtFileDataShmPtr->dmSlavingSwitch;
            hrt_twtNeighborLimit = hrtFileDataShmPtr->twtNeighborLimit;

            hrt_localWaffleGain = hrtFileDataShmPtr->localWaffleGain;         //XXXdwp110816
            hrt_overThreshFraction = hrtFileDataShmPtr->overThreshFraction;  //XXXdwp110816

            hrtSystemDebugLevel = hrtFileDataShmPtr->systemDebug;

            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read parms Config' complete");
            diagCmdResponse(CmdRespParmsLoaded, 0, NULL, NULL, NULL);
            break;

        case eFileTypeWfsBkGrnd_0:    // wfs BkGrnd data
        case eFileTypeWfsBkGrnd_1:    // wfs BkGrnd data
        case eFileTypeWfsBkGrnd_2:    // wfs BkGrnd data
        case eFileTypeWfsBkGrnd_3:    // wfs BkGrnd data
        case eFileTypeWfsBkGrnd_4:    // wfs BkGrnd data
//          note that, regardless of the fileType, the hrt_wfsBkGrnd arrays are set for the current camera frame rate
//          > gpAoHrtCent_GetCrntDarkBackFlat() gets the right values when the frame rate is changed
            for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
            {
                for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
                {
                for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
                    hrt_wfsBkGrndData[ii][jj][kk] = hrtFileDataShmPtr->wfsBkGrndData[hrt_camFrameRate][ii][jj][kk];
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'reading wfs BkGrnd file' complete");
            break;

        case eFileTypeWfsDark_0:    // wfs Dark data
        case eFileTypeWfsDark_1:    // wfs Dark data
        case eFileTypeWfsDark_2:    // wfs Dark data
        case eFileTypeWfsDark_3:    // wfs Dark data
        case eFileTypeWfsDark_4:    // wfs Dark data
//          note that, regardless of the fileType, the hrt_wfsDark arrays are set for the current camera frame rate
//          > gpAoHrtCent_GetCrntDarkBackFlat() gets the right values when the frame rate is changed
            for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
            {
                for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
                {
                for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
                    hrt_wfsDarkData[ii][jj][kk] = hrtFileDataShmPtr->wfsDarkData[hrt_camFrameRate][ii][jj][kk];
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wfs dark file' complete");
            break;

        case eFileTypeWfsFlat_0:    // wfs Flat data
        case eFileTypeWfsFlat_1:    // wfs Flat data
        case eFileTypeWfsFlat_2:    // wfs Flat data
        case eFileTypeWfsFlat_3:    // wfs Flat data
        case eFileTypeWfsFlat_4:    // wfs Flat data
//          note that, regardless of the fileType, the hrt_wfsFlat arrays are set for the current camera frame rate
//          > gpAoHrtCent_GetCrntDarkBackFlat() gets the right values when the frame rate is changed
            for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
            {
                for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
                {
                for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
                    hrt_wfsFlatData[ii][jj][kk] = hrtFileDataShmPtr->wfsFlatData[hrt_camFrameRate][ii][jj][kk];
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'reading wfs Flat file' complete");
            break;

        case eFileTypeRefCentQ:  // reference centroid, quad-cell
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read refcentQ file' complete");
            break;

        case eFileTypeRefOffs:
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Ref Offsets' complete");
            break;

        case eFileTypeRefRePhaseOffs:
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Ref Re-Phase Offsets' complete");
            break;

        case eFileTypeRefMapOffs:  // this is run in response to a setSimCalData command that's handled on the SRT side
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Sim Cal Data' complete");
            break;

        case eFileTypeReserve1:
            break;

        case eFileTypeReserve2:
            break;

        case eFileTypeTwtDmCoeff:
        {
            int        numCoeff = hrtFileDataShmPtr->numTwt_Phase2VoltsCoeff;
            double    ph_per_step;
            double    a, b, c, v, mu;  // v is volts, mu is microns (phase)

            for( ii=0; ii<MAX_TWEETER_ACTS; ii++ )
            {
                //      IMPORTANT - start -- this is to keep from getting RTL watchdogged
                int retVal;
                struct rtl_timespec    next;
                next.tv_sec = 0;
                next.tv_nsec = 10000;
                if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
                    SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtFileData_Get) rtl_nanosleep(), retVal = ", retVal);
                //                        IMPORTANT - end -- this is to keep from getting RTL watchdogged
                hrt_twtMinVolts[ii] = hrtFileDataShmPtr->twtMinVolts[ii];
                hrt_twtMaxVolts[ii] = hrtFileDataShmPtr->twtMaxVolts[ii];
                //printf("i = %d\n  min  = %f\n  max  = %f\n",ii,hrt_twtMinVolts[ii],hrt_twtMaxVolts[ii]);

                hrt_twtMinMicrons[ii] = hrt_twtMaxMicrons[ii] = 0.0;

                for( jj=numCoeff-1; jj>=0; jj-- )  // this evaluates the polynomial to calculate min and max microns
                {
                    hrt_twtMinMicrons[ii] *= hrt_twtMinVolts[ii];
                    hrt_twtMinMicrons[ii] += hrtFileDataShmPtr->twtPhaseToVoltsCoeff[jj][ii];

                    hrt_twtMaxMicrons[ii] *= hrt_twtMaxVolts[ii];
                    hrt_twtMaxMicrons[ii] += hrtFileDataShmPtr->twtPhaseToVoltsCoeff[jj][ii];
                }
                //printf("  minu = %f\n  maxu = %f\n",ii,hrt_twtMinMicrons[ii],hrt_twtMaxMicrons[ii]);

                ph_per_step = (double) ( hrt_twtMaxMicrons[ii] - hrt_twtMinMicrons[ii] ) / TWEETER_LOOKUP_BINS;

                a = hrtFileDataShmPtr->twtPhaseToVoltsCoeff[ 2 ][ ii ];  // 2nd order
                b = hrtFileDataShmPtr->twtPhaseToVoltsCoeff[ 1 ][ ii ];  // 1st order
                c = hrtFileDataShmPtr->twtPhaseToVoltsCoeff[ 0 ][ ii ];  // 0th order
                if (a == 0)
                {
                    if (b == 0)
                    {
                        for( jj = 0; jj < TWEETER_LOOKUP_BINS; jj++)  // constant term only -- error
                            hrt_twtPhaseToVoltsLookup[ii][jj] = 0.0;
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR twt Phs2Vlt coeffs have constant term only");
                    }
                    else
                    {
                        for( jj = 0; jj < TWEETER_LOOKUP_BINS; jj++)  // linear only
                        {
                            mu = ph_per_step * jj + hrt_twtMinMicrons[ii];
                            v = (mu - c) / b;
                            hrt_twtPhaseToVoltsLookup[ii][jj] = v;
                        }
                    }
                }
                else
                {
                    for( jj = 0; jj < TWEETER_LOOKUP_BINS; jj++)  // quadratic
                    {
                        mu = ph_per_step * jj + hrt_twtMinMicrons[ii];
                        v = (-b + sqrt(b * b - 4.0 * a * (c - mu))) / (2.0 * a);  // evaluate the quadratic formula, offsetting
                        hrt_twtPhaseToVoltsLookup[ii][jj] = v;                      // > the curve in y (i.e., c - mu)
                    }
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read/create twt Phs2Vlt Lookup Table' complete");
        }
        break;

        case eFileTypeWfrDmCoeff:
        {
            int        numCoeff = hrtFileDataShmPtr->numWfr_Phase2VoltsCoeff;
            double    ph_per_step;
            double    a, b, v, mu;  // v is volts, mu is microns (phase)

            for( ii=0; ii<MAX_WOOFER_ACTS; ii++ )
            {
                hrt_wfrMinVolts[ii] =  hrtFileDataShmPtr->wfrMinVolts[ii];
                hrt_wfrMaxVolts[ii] =  hrtFileDataShmPtr->wfrMaxVolts[ii];

                hrt_wfrMinMicrons[ii] = hrt_wfrMaxMicrons[ii] = 0.0;

                for( jj=numCoeff-1; jj>=0; jj-- )  // this evaluates the polynomial to calculate min and max microns
                {
                    hrt_wfrMinMicrons[ii] *= hrt_wfrMinVolts[ii];
                    hrt_wfrMinMicrons[ii] += hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[jj][ii];

                    hrt_wfrMaxMicrons[ii] *= hrt_wfrMaxVolts[ii];
                    hrt_wfrMaxMicrons[ii] += hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[jj][ii];
                }

                ph_per_step = (double)( hrt_wfrMaxMicrons[ii] - hrt_wfrMinMicrons[ii] ) / WOOFER_LOOKUP_BINS;

                a = hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[ 1 ][ ii ];  // 1st order
                b = hrtFileDataShmPtr->wfrPhaseToVoltsCoeff[ 0 ][ ii ];  // 0th order
                if (a == 0)
                {
                    for( jj = 0; jj < WOOFER_LOOKUP_BINS; jj++)  // constant term only -- error
                        hrt_wfrPhaseToVoltsLookup[ii][jj] = 0.0;
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR wfr Phs2Vlt coeffs have constant term only");
                }
                else
                {
                    for( jj = 0; jj < WOOFER_LOOKUP_BINS; jj++)  // linear
                    {
                        mu = ph_per_step * jj + hrt_wfrMinMicrons[ii];
                        v = (mu - b) / a;
                        hrt_wfrPhaseToVoltsLookup[ii][jj] = v;
                    }
                }
            }
            SysConSendStrngNoParm(eDebugLevelMax, "Notice: done creating wfr Phs2Vlt Lookup Table");
        }
        break;

        case eFileTypeTTDmCoeff:
        {
            int     numCoeff = hrtFileDataShmPtr->numTT_Phase2VoltsCoeff;
            double  mas_per_step;
            double  a, b, v, mas;  // v is volts, mas is milli-arcsecs

            for( ii=0; ii<MAX_TT_ACTS; ii++ )
            {
                hrt_ttMinVolts[ii] = hrtFileDataShmPtr->ttMinVolts[ii];
                hrt_ttMaxVolts[ii] = hrtFileDataShmPtr->ttMaxVolts[ii];

                hrt_ttMinMAS[ii] = hrt_ttMaxMAS[ii] = 0.0;

                for( jj=numCoeff-1; jj>=0; jj-- )  // this evaluates the polynomial to calculate min and max MAS
                {
                    hrt_ttMinMAS[ii] *= hrt_ttMinVolts[ii];
                    hrt_ttMinMAS[ii] += hrtFileDataShmPtr->ttPhaseToVoltsCoeff[jj][ii];

                    hrt_ttMaxMAS[ii] *= hrt_ttMaxVolts[ii];
                    hrt_ttMaxMAS[ii] += hrtFileDataShmPtr->ttPhaseToVoltsCoeff[jj][ii];
                }

                mas_per_step = (double)( hrt_ttMaxMAS[ii] - hrt_ttMinMAS[ii] ) / TT_LOOKUP_BINS;

                a = hrtFileDataShmPtr->ttPhaseToVoltsCoeff[ 1 ][ ii ];  // 1st order
                b = hrtFileDataShmPtr->ttPhaseToVoltsCoeff[ 0 ][ ii ];  // 0th order
                if (a == 0)
                {
                    for( jj = 0; jj < TT_LOOKUP_BINS; jj++)  // constant term only -- error
                        hrt_ttMasToVoltsLookup[ii][jj] = 0.0;
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR tt Phs2Vlt coeffs have constant term only");
                }
                else
                {
                    for( jj = 0; jj < TT_LOOKUP_BINS; jj++)  // linear
                    {
                        mas = mas_per_step * jj + hrt_ttMinMAS[ii];
                        v = (mas - b) / a;
                        hrt_ttMasToVoltsLookup[ii][jj] = v;
                    }
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read/create TT Angle2VltPoly' complete");
        }
        break;

        case eFileTypeTwtLabFlat:     // DM Lab flat data
            for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
                hrt_twtLabFlat[ii] = hrtFileDataShmPtr->twtLabFlat[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt lab flat' complete");
            break;

        case eFileTypeTwtSysFlat:     // DM Sys flat data
            for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
                hrt_twtSysFlat[ii] = hrtFileDataShmPtr->twtSysFlat[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt sys flat' complete");
            break;

        case eFileTypeTwtDmShape:    // DM Dm Shape data
            printf ( " HRTFileData_Get: Copying data from shm\n" );    // mrr twt
#           ifdef DOTWTSLOW
            for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
                hrt_twtDmShape[ii] = hrtFileDataShmPtr->twtDmShape[ii];
#           else  /* DOTWTSLOW */
            memcpy( hrt_twtDmShape, hrtFileDataShmPtr->twtDmShape, MAX_TWEETER_ACTS * sizeof(float) );
#           endif /* DOTWTSLOW */
#           ifdef DONOTWRITETWT
            printf(" hrt_twtDmShape diagonal");
            for (ii=0; ii<NUM_SUBAPS_PER_ROW; ii++)
            {
                // print a diagonal subsample for checking
                printf(" %d,%d=%f",
                ii, ii, hrt_twtDmShape[ii * NUM_SUBAPS_PER_ROW + ii]);
            }
            printf("\n");
#           endif /* DONOTWRITETWT */
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt Dm Shape' complete");
            printf ( "    HRTFileData_Get: Copy twtDmShape from shm complete\n" );    // mrr twt
            break;

        case eFileTypeTwtHostOffset:    // DM Dm Shape data
            for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
                hrt_twtHostOffset[ii] = hrtFileDataShmPtr->twtHostDmOffset[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt Host Offset' complete");
            break;

        case eFileTypeWfrLabFlat:     // DM lab flat data
            for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
                hrt_wfrLabFlat[ii] = hrtFileDataShmPtr->wfrLabFlat[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wf lab flat' complete");
            break;

        case eFileTypeWfrSysFlat:     // DM sys flat data
            for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
                hrt_wfrSysFlat[ii] = hrtFileDataShmPtr->wfrSysFlat[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wf sys flat' complete");
            break;

        case eFileTypeWfrDmShape:    // DM Dm shape data
            for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
                hrt_wfrDmShape[ii] = hrtFileDataShmPtr->wfrDmShape[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wf Dm shape' complete");
            break;

        case eFileTypeWfrHostOffset:    // DM Dm shape data
            for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
                hrt_wfrHostOffset[ii] = hrtFileDataShmPtr->wfrHostDmOffset[ii];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wf offset' complete");
            break;

        case eFileTypeTwtCleanup:
            for( ii = 0; ii < MAX_TWEETER_CLEAN_MODES; ii++)
            {
                for( jj = 0; jj < MAX_TWEETER_ACTS; jj++ )
                    hrt_twtCleanup[ii][jj] = hrtFileDataShmPtr->FTR_twtCleanup[jj][ii];  // indexes backwards due to IDL
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Twt Cleanup' complete");
            break;

        case eFileTypeTwtReconFilter:        // twt 'complex' (x,y) filter data
            for (ii = 0; ii < APERTURE_GRID_AREA; ii++)
            {
                ReconAlgFTR_filterX[ ii ][0] = hrtFileDataShmPtr->FTR_reconFilter[0][ ii ];
                ReconAlgFTR_filterY[ ii ][0] = hrtFileDataShmPtr->FTR_reconFilter[1][ ii ];

                ReconAlgFTR_filterX[ ii ][1] = hrtFileDataShmPtr->FTR_reconFilter[2][ ii ];
                ReconAlgFTR_filterY[ ii ][1] = hrtFileDataShmPtr->FTR_reconFilter[3][ ii ];

                ReconAlgFTR_filterDenom[ ii ] = hrtFileDataShmPtr->FTR_reconFilter[4][ ii ];
            }
            SysConSendStrngNoParm(eDebugLevelMax, "INFO 'read Twt FTR recon Filter' complete");
            break;

        case eFileTypeWfrCM_FTR:                // wfr control matrix for FTR calculations data
            for( jj=0;jj<NUM_ACTIVE_WFR_ACTS; jj++)
                for (ii = 0; ii<(hrtFileDataShmPtr->FTR_wfrVecSize*2); ii++)
                ParseAlgFTR_WfrControlMatrix[ jj ][ ii ] = hrtFileDataShmPtr->FTR_wfrControlMatrix[ jj ][ ii ];

            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wfr FTR CM' complete");
            break;

        case eFileTypeWfrCleanup:
            for (ii = 0; ii < MAX_WOOFER_CLEAN_MODES; ii++)
            {
                for( jj = 0; jj < MAX_WOOFER_ACTS; jj++)
                hrt_wfrCleanup[ii][jj] = hrtFileDataShmPtr->FTR_wfrCleanup[jj][ii];  // indexes backwards due to IDL
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Wfr Cleanup' complete");
            break;


        case eFileTypeWfrVecList:                // wfr extraction vector selection list
            for( ii=0; ii<MAX_WFR_VEC_SIZE; ii++)    // move the entire array, even if some is not set
                ParseAlgFTR_WfrVectorList[ ii ] = hrtFileDataShmPtr->FTR_wfrVectorList[ ii ];
            SysConSendStrngNoParm(eDebugLevelMax, "INFO 'read Wfr Extract Vector' complete");
            break;

        case eFileTypeTwtZeroList:                // twt modes to zero after woofer extraction
            // Note: This array is used, semi-directly, by parse routines and is also used to fill in 'hrt_twtCoeffUsed_Array' for recon intwgrator
            for( ii = 0; ii < APERTURE_GRID_AREA; ii++ )
                hrt_twtCoeffUsed_Array[ ii ] = 1;        // preset entire array to one
            for( ii = 0; ii < (MAX_WFR_VEC_SIZE * 2); ii++)
                ParseAlgFTR_TwtZeroList[ ii ] = hrtFileDataShmPtr->FTR_twtZeroList[ ii ];  // Parse vector
            for( ii = 0; ii < (ParseAlgFTR_wfrVecSize * 2); ii++)  ///xxxdwp110708
                hrt_twtCoeffUsed_Array[ ParseAlgFTR_TwtZeroList[ ii ] ] = 0;               // Twt Coeff Used through parse - for integrator
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt Zero List' complete");
            break;

        case eFileTypetwtInfFuncMask:            // twt FTR Coeff selection mask
            for( ii=0; ii<APERTURE_GRID_SIZEROW; ii++)
            {
                for( jj=0; jj<APERTURE_GRID_SIZECOL; jj++)
                {
                if( hrtFileDataShmPtr->FTR_TwtInfFuncMask[ii] [jj] > hrtFileDataShmPtr->inFuncMinGain )
                    ParseAlgFTR_TwtInfFuncMask[ ii ][ jj ] = 1.0 / hrtFileDataShmPtr->FTR_TwtInfFuncMask[ii] [jj];
                else
                    ParseAlgFTR_TwtInfFuncMask[ ii ][ jj ] = 1.0 / hrtFileDataShmPtr->inFuncMinGain;
                }
            }
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read twt mask' complete");
            break;

        case eFileTypeWfrFtrActMap:          // map of FTR CM output to woofer actators
            for( ii=0; ii<NUM_ACTIVE_WFR_ACTS; ii++)
                ParseAlgFTR_wfrActMap[ ii ] = hrtFileDataShmPtr->FTR_wfrActMap[ ii ];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read wfr Active Act map' complete");
            break;

        case eFileTypeInitialModalGains:  // FTR optimizer initial gains
            for( ii=0; ii<APERTURE_GRID_AREA; ii++)
                hrt_initialModalGains[ ii ] = hrtFileDataShmPtr->initialModalGains[ ii ];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read Opt Initial gains' complete");
            break;

        case eFileTypeHostModalGains:      // FTR optimizer host specified gains
            for( ii=0; ii<APERTURE_GRID_AREA; ii++)
                hrt_hostModalGains[ ii ] = hrtFileDataShmPtr->hostModalGains[ ii ];
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'read opt Host Gains' complete");
            break;

        default:
            SysConSendStrngIntParm(eDebugLevelErr, "ERR unexpected file type in FileDataGet(), ", fileType);
            break;
        } /* end switch */
        hrtFileDataShmPtr->dataAvailFromFile[fileType] = false;
        return true;
    } /* end if */
    return false;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtFileData_Put( int fileType )
 *
 *  @brief
 *  Put data to shared memory and command the SRT to write it to disk.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to move data from HRT program variables into shared
 *  memory and command the SRT to write the data to a file.  The unit locks shared
 *  memory for writing with a semaphaore.
 *  @par
 *
 *  @b Usage:
 *  retVal = gpAoHrtFileData_Put( fileType );
 *
 *  @param[in] fileType (void *): Indicates file type to put data for.
 *
 *  @return
 *  (int) 0 = success, !=0 = failure.
 *
 *  @exception
 *  Unexpected file type or shared-memory sharing violation.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

int gpAoHrtFileData_Put( int fileType )
{
    short ii, jj, kk;

    //    try to lock shared memory and report an error if it's already locked
    hrtFileDataShmPtr->dataShmLockHRT[fileType] = true;

    if( hrtFileDataShmPtr->dataShmLockSRT[fileType] )
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR - HRT attempting to write to shared memory while locked, ", fileType);

        //    unlock shared memory
        hrtFileDataShmPtr->dataShmLockHRT[fileType] = false;
        return 1;
    }

    //  move the data to be written to disk into shared memory
    //  call gpAoHrtFileData_Cmd() to command the SRT to write this data to file
    switch (fileType)
    {
    case eFileTypeCmdStatus:
        gpAoHrtFileData_Cmd( eFileWriteCmdStatus );
        break;

    case eFileTypeParms:    // parameter data
        hrtFileDataShmPtr->useWfsSimData = hrt_useWfsSimData;

        hrtFileDataShmPtr->applyBackData = hrt_applyBackData;
        hrtFileDataShmPtr->applyDarkData = hrt_applyDarkData;
        hrtFileDataShmPtr->applyFlatData = hrt_applyFlatData;

        hrtFileDataShmPtr->wfsBacksAvgN = hrt_BacksAvgN;
        hrtFileDataShmPtr->wfsFlatsAvgN = hrt_FlatsAvgN;
        hrtFileDataShmPtr->centAvgN = hrt_centAvgN;
        hrtFileDataShmPtr->smAvgN = hrt_smAvgN;
        hrtFileDataShmPtr->MirrorShapeAvgN = hrt_MirrorShapeAvgN;

        hrtFileDataShmPtr->wfsCamFrameRate = hrt_camFrameRate;
        hrtFileDataShmPtr->wfsCamGain = hrt_camGain;
        hrtFileDataShmPtr->calSNR_Gain = hrt_calSNR_Gain;

        for( ii=0; ii<APERTURE_GRID_AREA; ii++ )
            hrtFileDataShmPtr->selectModalGains[ ii ] = hrt_selectModalGains[ ii ];

        hrtFileDataShmPtr->centThresh = hrt_CentThresh;

        hrtFileDataShmPtr->centroidGainX = hrt_centroidGainX;
        hrtFileDataShmPtr->centroidGainY = hrt_centroidGainY;

        hrtFileDataShmPtr->darkCornerCorrectSwitch = hrt_darkCornerCorrectSwitch;

        hrtFileDataShmPtr->ttGainStage = hrt_TTGainStage;
        hrtFileDataShmPtr->ttGainWfrSurface = hrt_TTGainWfrSurface;

        hrtFileDataShmPtr->ttSplitSwitch = hrt_ttSplitSwitch;
        hrtFileDataShmPtr->ttLQGSwitch = hrt_ttLQGSwitch;
        hrtFileDataShmPtr->ttWfr9ActMicronsPerMASX = hrt_ttWfr9ActMicronsPerMASX;
        hrtFileDataShmPtr->ttWfr9ActMicronsPerMASY = hrt_ttWfr9ActMicronsPerMASY;

        hrtFileDataShmPtr->ttCompDelay_microSecs = hrt_ttCompDelay_microSecs;

        hrtFileDataShmPtr->ttSeeing = hrt_ttSeeing;
        hrtFileDataShmPtr->ttWindPower = hrt_ttWindPower;
        hrtFileDataShmPtr->ttStarBrightness = hrt_ttStarBrightness;

        hrtFileDataShmPtr->param_mode_cleanup_thresh = hrt_mode_cleanup_thresh;
        hrtFileDataShmPtr->param_mode_cleanup_gain = hrt_mode_cleanup_gain;

        hrtFileDataShmPtr->param_modal_loss_dm = hrt_modal_loss_dm;
        hrtFileDataShmPtr->param_modal_loss_tt = hrt_modal_loss_tt;

        hrtFileDataShmPtr->param_tt_x_offset = hrt_tt_x_offset;
        hrtFileDataShmPtr->param_tt_y_offset = hrt_tt_y_offset;

        hrtFileDataShmPtr->FTR_wfrVecSize = ParseAlgFTR_wfrVecSize;

        hrtFileDataShmPtr->dmSlavingSwitch = hrt_dmSlavingSwitch;
        hrtFileDataShmPtr->twtNeighborLimit = hrt_twtNeighborLimit;

        hrtFileDataShmPtr->localWaffleGain = hrt_localWaffleGain;         //XXXdwp110816
        hrtFileDataShmPtr->overThreshFraction = hrt_overThreshFraction;  //XXXdwp110816

        gpAoHrtFileData_Cmd( eFileWriteParmsData );
        break;

    case eFileTypeWfsBkGrnd_0:    // wfs BkGrnd data
    case eFileTypeWfsBkGrnd_1:    // wfs BkGrnd data
    case eFileTypeWfsBkGrnd_2:    // wfs BkGrnd data
    case eFileTypeWfsBkGrnd_3:    // wfs BkGrnd data
    case eFileTypeWfsBkGrnd_4:    // wfs BkGrnd data
        for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
        {
            for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
            {
                for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
                    hrtFileDataShmPtr->wfsBkGrndData[fileType - eFileTypeWfsBkGrnd_0][ii][jj][kk] = hrt_wfsBkGrndData[ii][jj][kk];
            }
        }
        gpAoHrtFileData_Cmd( eFileWriteWfsBkGrndData_0 + hrt_camFrameRate );
        break;

    case eFileTypeWfsDark_0:    // wfs Dark data
    case eFileTypeWfsDark_1:    // wfs Dark data
    case eFileTypeWfsDark_2:    // wfs Dark data
    case eFileTypeWfsDark_3:    // wfs Dark data
    case eFileTypeWfsDark_4:    // wfs Dark data
        for (ii = 0; ii < NUM_SUBAPS_ROWS_PER_FRAME; ii++)
        {
            for (jj = 0; jj < NUM_CAM_ROWS_PER_SUBAP; jj++)
            {
                for (kk = 0; kk < NUM_PIX_IN_ROW; kk++)
                    hrtFileDataShmPtr->wfsDarkData[fileType - eFileTypeWfsDark_0][ii][jj][kk] = hrt_wfsDarkData[ii][jj][kk];
            }
        }
        gpAoHrtFileData_Cmd(eFileWriteWfsDarkData_0 + hrt_camFrameRate);
        break;

    case eFileTypeWfsFlat_0:    // wfs Flat data
    case eFileTypeWfsFlat_1:    // wfs Flat data
    case eFileTypeWfsFlat_2:    // wfs Flat data
    case eFileTypeWfsFlat_3:    // wfs Flat data
    case eFileTypeWfsFlat_4:    // wfs Flat data
        for( ii=0; ii<NUM_SUBAPS_ROWS_PER_FRAME; ii++)
        {
            for( jj=0; jj<NUM_CAM_ROWS_PER_SUBAP; jj++)
            {
                for( kk=0; kk<NUM_PIX_IN_ROW; kk++)
                    hrtFileDataShmPtr->wfsFlatData[fileType - eFileTypeWfsFlat_0][ii][jj][kk] = hrt_wfsFlatData[ii][jj][kk];
            }
        }
//XXXdwp110621 -- start
        for(ii = 0; ii < (FLATTER_NUM_STEPS * 4); ii++)
        {
            for(jj = 0; jj < (FLATTER_NUM_STEPS * 4); jj++)
                hrtFileDataShmPtr->flatTestStepsData[ii][jj] = flatTestStepsData[ii][jj];
        }
//XXXdwp110621 -- end
        gpAoHrtFileData_Cmd(eFileWriteWfsFlatData_0 + hrt_camFrameRate);
        break;

    case eFileTypeRefCentQ:  // reference centroid, quad-cell
        gpAoHrtFileData_Cmd(eFileWriteRefCentQData);
        break;

    case eFileTypeRefOffs:  // reference centroid, quad-cell
        // already in shared memeory
        gpAoHrtFileData_Cmd(eFileWriteRefOffsQData);
        break;

    case eFileTypeInitialModalGains:
        gpAoHrtFileData_Cmd( eFileWriteOptGains );
        break;

    case eFileTypeTwtSM:
        gpAoHrtFileData_Cmd( eFileWriteTwtSM );
        break;

    case eFileTypeReserve4:
        break;

    case eFileTypeTwtLabFlat:     // Twt lab flat data
        for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
            hrt_twtLabFlat[ii] = hrtFileDataShmPtr->twtLabFlat[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvTweeter( hrt_twtLabFlat, hrtFileDataShmPtr->twtLabFlat_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteTwtLabFlat);
        break;

    case eFileTypeTwtSysFlat:     // Twt sys flat data
        for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
            hrt_twtSysFlat[ii] = hrtFileDataShmPtr->twtSysFlat[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvTweeter( hrt_twtSysFlat, hrtFileDataShmPtr->twtSysFlat_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteTwtSysFlat);
        break;

    case eFileTypeTwtDmShape:    // Twt DM shape data
        for (ii = 0; ii < MAX_TWEETER_ACTS; ii++)
            hrt_twtDmShape[ii] = hrtFileDataShmPtr->twtDmShape[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvTweeter( hrt_twtDmShape, hrtFileDataShmPtr->twtDmShape_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteTwtDmShape);
        break;

    case eFileTypeWfrLabFlat:     // Wfr Lab flat data
        for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
            hrt_wfrLabFlat[ii] = hrtFileDataShmPtr->wfrLabFlat[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvWoofer( hrt_wfrLabFlat, hrtFileDataShmPtr->wfrLabFlat_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteWfrLabFlat);
        break;

    case eFileTypeWfrSysFlat:     // Wfr Sys flat data
        for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
            hrt_wfrSysFlat[ii] = hrtFileDataShmPtr->wfrSysFlat[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvWoofer( hrt_wfrSysFlat, hrtFileDataShmPtr->wfrSysFlat_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteWfrSysFlat);
        break;

    case eFileTypeWfrDmShape:    // Wfr DM shape data
        for (ii = 0; ii < MAX_WOOFER_ACTS; ii++)
            hrt_wfrDmShape[ii] = hrtFileDataShmPtr->wfrDmShape[ii];

        // The conversion is done here - as it is not accessable to the SRT side
        if( gpAoHrtVolt_ConvWoofer( hrt_wfrDmShape, hrtFileDataShmPtr->wfrDmShape_volts, NULL ) <= 0)
            gpAoHrtFileData_Cmd(eFileWriteWfrDmShape);
        break;

    case eFileTypeMakeInFunc:
        gpAoHrtFileData_Cmd( eFileWriteMakeInFunc );
        break;

    default:
        SysConSendStrngIntParm(eDebugLevelErr, "ERR - unexpected file type in gpAoHrtFileData_Put(), ", fileType);
        break;
    } // End Switch

    //    unlock shared memory
    hrtFileDataShmPtr->dataShmLockHRT[fileType] = false;
    return 0;
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
