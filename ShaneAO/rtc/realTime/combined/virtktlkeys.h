#ifndef VIRTKTLKEYS_H_564002
#define VIRTKTLKEYS_H_564002

#include "interfacet.h"


class CKTLInt : public CKTLKeyInt
{
 public:
    enum {
        UPDATED = 0,
        WAITING = 1,
        FAILED  = -1
    };
    CKTLInt(const char *name, int key, int val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);

    virtual void SetRespStatusToWaiting() { m_respStatus = WAITING;};
    virtual int  GetRespStatus() {return m_respStatus;};
    virtual void UpdateVal(int status, int val, ConnectionID id) {
        SetOnlyVal(val, id);
        if (status == UPDATED) {
            m_respStatus = UPDATED;
        } else {
            m_respStatus = FAILED;
        }
    };
    virtual void UpdateFailed() { m_respStatus = FAILED; };
    virtual CQData* BuildData() {return NULL;}; // only needed in special cases
    virtual int TestComplete(int respVal, int val, CQData *pData);
 protected:
    int   m_respStatus; // Response Status - one of UPDATED, WAITING, or FAILED
};


class CKTLFloat : public CKTLKeyFloat
{
 public:
    enum {
        UPDATED = 0,
        WAITING = 1,
        FAILED  = -1
    };
    CKTLFloat(const char *name, int key, double val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);
    // Use the pure virtual functions when most keywords are
    // working to get compilation errors about which keywords
    // are not finished.
    //virtual int setValue(double val) = 0;
    //virtual int TestComplete(double resp) = 0;
    virtual int SetValue(double val) {
        LogWarn("CAOKeyFloat %s has no setValue", GetName());
        return -1;
    };
    virtual CQData* BuildData() {return NULL;}; // only needed in special cases
    virtual int TestComplete(double respVal, double val, CQData *pData) {
        LogErr("CKTLFloat %s  TestComplete NEEDS RANGE", GetName());
        if (pData != NULL) {   LogWarn("CKTLInt::TestComplete unexpected data is being ignored"); }
        if (m_respStatus == WAITING) return 1; // waiting
        if (m_respStatus == UPDATED && respVal == val) return 0; // complete TODO - replace respVal == val with range check
        return -1; // failed
    };
 protected:
    int   m_respStatus; // Response Status - one of UPDATED, WAITING, or FAILED
};


class CKTLString : public CKTLKeyString
{
 public:
    enum {
        UPDATED = 0,
        WAITING = 1,
        FAILED  = -1
    };
    CKTLString(const char *name, int key, const char *val, int readWrite, 
	      KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc);

    ~CKTLString() {};
    virtual void UpdateVal(int status, string val, ConnectionID id);

    virtual void SetRespStatusToWaiting() { m_respStatus = WAITING;};
    virtual int  GetRespStatus() {return m_respStatus;};
    virtual void UpdateFailed() { m_respStatus = FAILED; };
    virtual CQData* BuildData() {return NULL;}; // only needed in special cases
    virtual int TestComplete(string respVal, string val, CQData *pData);
 protected:
    int   m_respStatus; // Response Status - one of UPDATED, WAITING, or FAILED

};














#endif
