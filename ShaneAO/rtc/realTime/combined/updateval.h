#ifndef UPDATE_VAL_H_59302
#define UPDATE_VAL_H_59302

#include <string>

#include "general.h"

class CUpdateVal 
{
public:
  CUpdateVal() {m_updated = false; m_tv.tv_sec = 0; m_tv.tv_usec = 0;};
  void     clearUpdated()  {m_updated = false;};
  boolean  getUpdated()    {return m_updated;};
  structTV getUpdateTime() { return m_tv;};
  boolean  getChanged() { return m_changed;};
  void     clearChanged() {m_changed = false;};
protected:
  boolean     m_updated;   // The value has been set after being 
  boolean     m_changed; // optional, indicates value has changed since the last time it was updated in g_pInter
  structTV    m_tv;
};
  

class CUpdateString : public CUpdateVal
{
public:
  CUpdateString() { m_val = ""; };
  string getVal() {return m_val;};  
  void   setVal(string val) { if (m_val.compare(val)!=0){ m_val=val; m_changed = true;} 
                              gettimeofday(&m_tv, NULL); m_updated = true;};
protected:
  string m_val;
};

class CUpdateInt : public CUpdateVal
{
 public:
    CUpdateInt() { m_val = 0; } ;
    int   getVal() {return m_val;};
    void  setVal(int val) { if (m_val != val){ m_val=val; m_changed = true;} 
                            gettimeofday(&m_tv, NULL); m_updated = true;};

    int testComplete(int val) {
        if (getUpdated()) {
            if (getVal() == val) {
                return 0; // completed successfully
            } else {
                return -1; // cancelled
            }
        } 
        return 1; // waiting for response from controller.
    };
 protected:
    int m_val;
};









#endif
