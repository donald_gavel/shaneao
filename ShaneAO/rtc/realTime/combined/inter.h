#ifndef INTERFACET_AO_H
#define INTERFACET_AO_H

#include <stdio.h>
#include <iostream>
#include <list>
#include <map>
#include <sys/poll.h>
#include "interfacet.h"
#include "acceptthread.h"
#include "ktlkeyword.h"
#include "simple_timer.h"
#include "ktlfile.h"
#include "aoktlkeys.h"

// Temporary error, used as filled in the .C files until an explicit error
// is created.
#define ERR_TEMPORARY_ERR -1  
#define ERROR_TEMP -1
#define ERR_NAK -1

//
// CInterPoco controls communication between poco and other Processes. 
// It is unusual in that it is based on CEventThread but it will not be running 
// in a separate thread. It will be part of the Central thread.
// It is based on CEventThread so that it can take advantage of
// the CEventThread virtual functions in message passing.
//
class CInterAO : public CInterface
{
public:
    CInterAO();
    ~CInterAO();

    int  InterInit();

    virtual void vControlInfoUpdate();
    
    int  userMsg(string msg);
    int  userErr(string msg);


    int  updateTELEM(int status, int val);
    int  updateTRATE(int status,int val); 
    int  updateSETDEBUG(int status,int valA, int valB);
    int  updateDUMPDATA(int status, int val, int bit, double sec, int dec);
    int  updateSETCAMRATE(int status, int val);
    int  updateSETCENTTHRESH(int status, int val);
    int  updateLOOPTWEET(int status, int val);
    int  updateLOOPWOOF(int status, int val);
    int  updateQUIT(int status, int val);
    int  updateABORT(int status, int val);
    int  updateSETSHAPETWEET(int status, int val);
    int  updateSETSHAPEWOOF(int status, int val);
    int  updateSTORESHAPES(int status, int val);
    int  updateCYCLEACTUATOR(int status, int val);
    int  updateSTOPCYCLING(int status, int val);
    int  updateREADCONFIG(int status, int val);

    int updateSETTWEETERSHAPE(int status, string val);
    int updateSETWOOFERSHAPE(int status, string val);
    int updateSETTWEETEROFFSET(int status, string val);
    int updateSETWOOFEROFFSET(int status, string val);






    enum {
	BLAH_COMMAND = 10000,
	LAST_ENUM = 19999
    };

    virtual void vGetMsgRange(int *first, int *last){
	*first=INTR_TXT_COMMAND;*last=LAST_ENUM;};
    bool inMsgRange(int msg){
	return ((INTR_TXT_COMMAND <= msg) && (msg <= LAST_ENUM));};





protected:    
    void BroadcastNow(CKTLKeyWord *pkw);

    // KTL action functions - actions to take when the keyword
    // value changes.
    // All must match type KTLChangeActionFunc
    static int afReadOnly       (CQData*, CEventThread*, ConnectionID);
    static int afAO             (CQData*, CEventThread*, ConnectionID);
    static int afKTLv           (CQData*, CEventThread*, ConnectionID);
    static int afDumpData       (CQData*, CEventThread*, ConnectionID);
    static int afFile           (CQData*, CEventThread*, ConnectionID);
    static int afTODO           (CQData*, CEventThread*, ConnectionID);
    
    // KTL delayed response functions
    static int drInt(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drKTLv(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drDumpData(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drFile(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);
    static int drDeleteMe(CQData *pData, CEventThread *pEvent, 
		     ConnectionID conID, int seq, int response, int send_2nd, 
		     structTV timeout, int logicPath);

    virtual int vSetupKTLKeyWords();

    // keywords
    CKTLKeyString *m_pMSG;
    CKTLKeyString *m_pMSER;
    CKTLKeyInt    *m_pHEART;
    CKTLKeyString *m_pDWIM;
    
    CKTLKeyInt    *m_pESTOP;
    CKTLInt       *m_pQUIT;
    CKTLInt       *m_pABORT;
    
    CKTLInt       *m_pTELEM;
    CKTLInt       *m_pTRATE;
    CKTLInt       *m_pDEBUGA;
    CKTLInt       *m_pDEBUGB;
    CKTLInt       *m_pSETCAMRATE;
    CKTLInt       *m_pSETCENTTHRESH;
    CKTLInt       *m_pLOOPTWEET;
    CKTLInt       *m_pLOOPWOOF;
    CKTLInt       *m_pSETSHAPETWEET;
    CKTLInt       *m_pSETSHAPEWOOF;
    CKTLInt       *m_pSTORESHAPES;
    CKTLInt       *m_pCYCLEACTUATOR;
    CKTLInt       *m_pSTOPCYCLING;
    CKTLInt       *m_pREADCONFIG;

    CKTLInt       *m_pDUMPDATAGO;
    CKTLKeyInt    *m_pDUMPDATABIT;
    CKTLKeyFloat  *m_pDUMPDATASEC;
    CKTLKeyInt    *m_pDUMPDATADEC;

    
    CKTLKeyInt    *m_pAUTOCAMRATE;
    CKTLKeyInt    *m_pCAMGAIN;
    CKTLKeyInt    *m_pDATA;
    CKTLKeyInt    *m_pIMAGES;
    CKTLKeyInt    *m_pINITWFSCAM;

    CKTLString *m_pSETTWEETERSHAPE;
    CKTLString *m_pSETWOOFERSHAPE;
    CKTLString *m_pSETTWEETEROFFSET;
    CKTLString *m_pSETWOOFEROFFSET;
    

    CKTLFile   *m_pTESTFILE;



};

extern CInterAO *g_pInter;

extern ConnectionID gOurID;

#endif





















