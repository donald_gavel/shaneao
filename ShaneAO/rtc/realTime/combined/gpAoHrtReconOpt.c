/**********************************************************************************************
*
* COPYRIGHT (C) 2009
*
* LAWRENCE LIVERMORE NATIONAL LABORATORY
* ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
*
***********************************************************************************************/
/*!
 *
 *	@file gpAoHrtReconOpt.c
 *
 *	@brief 
 *	Perform wavefront reconstruction optimization and related tasks.	
 *
 *	@b DESCRIPTION:
 *	This component contains functions to perform the computations necessary forwavefront
 *	reconstruction optimization ( forFTR only)
 *
 *	The Optimization threads are included in this component.
 *
 *	@b AUTHOR:
 *	DWP & SMJ & VJR	DATE: 05/28/2009
 *
 *	@b STATIC ( LOCAL) FUNCTION NAMES:
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoHrtReconOpt_FTRAlgoInit( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoInitThreading( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoCleanThreading( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoInitMemory( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoInitFftPlans( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoCleanMemory( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoStart( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoStop( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoProcess( )
 *	-# @c gpAoHrtReconOpt_FTRAlgoThread( )
 *	-# @c gpAoHrtReconOpt_ReconOptFTR( )
 *
 *	@b STATIC/LOCAL FUNCTION NAMES:
 *	-# @c optConvertToComplex( int map[ ][ 3 ], float series[ ], 
 *	-# @c optFillToHermitian( int hermitMap[ ][ 3 ], float planeIn[ ], float planeOut[ ], int twoDLength)
 *	-# @c optMakeMap( int map[ ][ 3 ], int oneDLength, int twoDLength )
 *	-# @c optSetPerFreqAndCtXfrFunc( )
 *	-# @c optCfuncs( )
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <semaphore.h>
#include <math.h>
#include <string.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


#include "gpAoHrtRecon.h"
#include "gpAoHrtReconOpt.h"
#include "gpAoHrtParse.h" 
#include "gpAoHrtSysCon.h"
#include "gpAoHrtFfts.h"


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
#include "fitsio.h"
#endif

// STANDALONE_COMPLEX, below, is used for flexibility in testing -- normally, it
// > should follow STANDALONE_FFT (when RTL_SYSTEM is 1, both STANDALONE_FFT and 
// > STANDALONE_COMPLEX MUST be 1)
#if ( STANDALONE_FFT == 0 )
#define STANDALONE_COMPLEX 0
#else
#define STANDALONE_COMPLEX 1
#endif

#if ( STANDALONE_COMPLEX == 0 )
#include <complex.h>
typedef float complex optComplex;
#else
typedef float		  optComplex[2];
static optComplex oneC   = { 1.0,   0.0 };
#endif

//*******************************************************************************
//** General Global Variables
//*******************************************************************************
//

// Optimization processing status (step)
ReconOpt_State_t ReconOpt_State;


//*******************************************************************************
//** General Local Variables
//*******************************************************************************
//
#define	xPix 1  //	 no extra pixel bits

static	int				  num_kl_valid;							   // counts the number of kl_valid[ ][ ]
static	char			  kl_valid[ GPI_PUP_DIA ][ GPI_PUP_DIA ];  // indicates which temporal data series should be operated on
static	char			  kl_valid1[ MAX_MODES ];				   // indicates which temporal data series should be operated on ( 1-d duplicate)

#if ( STANDALONE_FFT == 0 )
static	fft_complex_type* perData_A;
static	fft_complex_type* perData_B;
static	fftwf_plan		  perPlan_A[ OPT_NUM_THREADS ];
static	fftwf_plan		  perPlan_B[ OPT_NUM_THREADS ];
#else
static	fft_complex_type  perData_A[MAX_MODES * PER_LEN];
static	fft_complex_type  perData_B[MAX_MODES * PER_LEN];
#endif

#if (( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 ))
static	fft_complex_type  tmpPerData_A[MAX_MODES * PER_LEN];
static	fft_complex_type  tmpPerData_B[MAX_MODES * PER_LEN];
#endif

// calculate a "unit sanity" scaling factor (applied below, when data is moved to DFT buffers)
float	freqDomainScaling = 0.0f;

#if ( RECONOPT_TEST != 0 )
static	float	testDataR[ MAX_MODES ][ OPT_TOTAL_LEN ];
static	float	testDataI[ MAX_MODES ][ OPT_TOTAL_LEN ];
#endif

static int				count_group_A = 0;
static int				count_group_B = 0;
static int				count_ffts = 0;
static int				count_PostProcess = 0;
static int				count_testDataNdx = 0;

static int				convertMap[ MAX_MODES ][ 3 ];						// map used to convert from real to complex

static float			subapMask[ GPI_PUP_DIA + 1 ][ GPI_PUP_DIA + 1 ];	// used for calculation of pinGrid
static unsigned char	pinGrid[ GPI_PUP_DIA ][ GPI_PUP_DIA ];
static int				numOnesInPinGrid = 0;								// this is used below

static int				num_kl_per_thread;

static float			currAvg[ 2 ][ MAX_MODES ];							// area where current running average is kept - used during test as well -
static float			currAvgBuffer[ OPT_AVG_LEN ][ 2 ][ MAX_MODES ];		// area to store circular averaging buffer
static int				currAvgBufNdx;										// index into circular averaging buffer
static int				fullAvgCntr = 0;									// used to calc full averages to mitigate trunction error

static float			currAvgSnapShotA[ 2 ][ MAX_MODES ];					// area where snapshot of average for current frame is kept ( same average is used over entire fft frame)
static float			currAvgSnapShotB[ 2 ][ MAX_MODES ];					// area where snapshot of average for current frame is kept ( same average is used over entire fft frame)
																			// Note, these are kept in [2][MAX_MODES] format so that real/imag can be read and written 

static fft_complex_type rawReconOptHalfData1[HALF_PER_LEN][MAX_MODES];
static fft_complex_type	rawReconOptHalfData2[HALF_PER_LEN][MAX_MODES];		// Temp 'temporal' collection area ( Ping/Pong Buffers) 

static float			optGainWorking[ GPI_PUP_DIA ][ GPI_PUP_DIA ];	
static float			optGains[ GPI_PUP_DIA ][ GPI_PUP_DIA ];	
static float*			pReconOptGains;

static float			hammWinVals[ PER_LEN ];				// hamming window values
static float			hannWinVals[ PER_LEN ];				// hanning window values
static float			blackWinVals[ PER_LEN ];			// blackman window values

static double			hammTotWinSquared, hannTotWinSquared, blackTotWinSquared;		// window total squared values

static short			camFR_forOptimizer = -1;						// current camera frame rate (-1: force update the first time thru

static optComplex		wfsContin[ PER_LEN ];
static optComplex		dmContin[ PER_LEN ];
static optComplex		delayContin[ PER_LEN ];
static optComplex		zInv[ PER_LEN ];

static optComplex		delayTerm[ PER_LEN ];
static optComplex		cofZDenom_Inv[ PER_LEN ];

static optComplex		cofZ[OPT_NUM_THREADS];
static float			tfToConvertToPhase[OPT_NUM_THREADS];
static float 			tfToConvertToPhase2[OPT_NUM_THREADS][NUM_GAIN_STEPS][PER_LEN];

#if ( RECONOPT_TEST != 0 )
	#define DEFAULT_OPT_TEST_FRAME_RATE		1500
#endif

//Pointer to recon buffer pointer ( used by the parser commands of worker threads only)
//static ReconBuffType*	ReconOptAlgFTR_reconOptBufPtrCurrent;
//static ReconBuffType*	reconOptBufPtr[ OPT_TOTAL_LEN ];	// forreconOptFTR use

//*******************************************************************************
//** General Local Function Prototypes
//*******************************************************************************

#if ( STANDALONE_COMPLEX != 0 )
//	collection of functions to perform complex math (because the C complex library is 
//	> not available in the kernel).  Given x = a, ib and y = c, id, the 
	static inline void optCaddf( optComplex opC1, optComplex opC2, optComplex resC );
	static inline void optCsubf( optComplex opC1, optComplex opC2, optComplex resC );
	static inline void optCmulf( optComplex opC1, optComplex opC2, optComplex resC );
	static inline void optCdivf( optComplex opC1, optComplex opC2, optComplex resC );
	static inline void optCabsf( optComplex opC1, float *resF );
	static inline void optCexpf( optComplex opC1, optComplex resC );
#endif

#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
	static int			dumpSliceOfComplexDataCube( fft_complex_type* pIn, int sliceRowSize, int sliceColSize, int cubeDepth );
	static int			dumpSliceOfDataCube( void *pIn, int sliceRowSize, int sliceColSize, int cubeDepth, int dType );
	static int			dumpCornerOfComplexMatrix( void *pIn, int rowSize, int totRows, int dType );
	static int			dumpCornerOfMatrix( void *pIn, int rowSize, int totRows, int dType, boolean extend );
	static void			optReadFitsSquare( char *filename, int heightWidth, int depth, int dataType, void *data);
	static void			writeCompleteFitsFileSquare( char *filename, int heightWidth, int dataType, void *data);
	static void			writeCompleteFitsFile( char *filename, long dimSize[], int numDim, int dataType, void *data);
	static int			readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType );

	static fitsfile*	openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType );
	static void			writeFitsBlock( fitsfile* fptr, long size, int dataType, void* pData, long _fpixel[] );
	static void			closeFits( fitsfile* fptr );
	static void			flushFitsBuffer( fitsfile* fptr );

	static long			OptSequenceNumber = 0;
	static long			ftrSequenceNumber = 0;
	static fitsfile*	dataFPtr = NULL;

	static fitsfile*	last_fptr = NULL;
#endif

static void		optCalcWinVals( void );

//map[ ][ ] is convertMap[ MAX_MODES ][ 3 ]
static void		optConvertToComplex( int map[ ][ 3 ], float series[ ], fft_complex_type matrix[ ],  int oneDLength, int twoDLength);

// hermitMap has dimenson [ MAX_MODES ][ 3 ]
static void		optFillToHermitian( int hermitMap[ ][ 3 ], float planeIn[ ], float planeOut[ ], int twoDLength); 

static void		optMakeMap( int map[ ][ 3 ], int oneDLength, int twoDLength );
static boolean	optSetPerFreqAndCtXfrFunc( void );

static void		gpAoHrtReconOpt_FTRAlgoGetData( fft_complex_type* pRawBuf, float* pFtrFFTModesData );


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_ReconOptFTR( float* pFtrFFTModesData,  float* pOutGains )
 *
 *	@brief
 *	Perform the Fourier Transform reconstruction optimization steps needed per frame.
 *
 *	@b DESCRIPTION:
 *	Perform the Fourier Transform reconstruction optimization steps needed per frame.
 *	@par
 *
 *	@b Usage: gpAoHrtReconOpt_ReconOptFTR( pFtrFFTModesData, pOutGains );
 * 
 *	@param [in] pFtrFFTModesData (float*):	frame of raw input data 
 *	@param [out] pOutGains (float*):		newly calculated gains (only updated occasionally)
 * 
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtReconOpt_ReconOptFTR( float* pFtrFFTModesData,  float* pOutGains )
{
/* This function is called from  gpAoHrtRecon_ReconFTR by passing &( reconBufPtr[ 0 ]) as an argument. 
 * One frame of raw data pointed to by reconBufPtr is ready. Data collection is done in 'TEST' mode but is then
 *  overwritten with TEST data from file 
 *
 * Does the following optimization steps:
 *
 * 1. calculate means, 
 * 2. average periodograms, and
 * 3. obtain new gains
 *
*/
	short ii;
	short done;
#if ( TIMER_TEST != 0 )
	int	  tmpTimerNdx;
#endif
	
	pReconOptGains = pOutGains;

	freqDomainScaling = sqrt( (float)MAX_MODES / numOnesInPinGrid);

#if ( TIMER_TEST != 0 )
	tmpTimerNdx = reconFrameID % NUM_TIME_SETS;
#endif

	//	ReconOpt thread processing controller - based on data section in progress
	switch( ReconOpt_State )
	{
		case eReconOpt_State_Init:
			{
				for( ii=0; ii<MAX_MODES; ii++ )
					hrt_ofcOutputGains[ii] = hrt_workingModalGains[ ii ];	// Set optimizer output to working values 
				
				//	call gpAoHrtReconOpt_FTRAlgoInit() to initialize FTR optimization variables and threads
				SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_ReconOptFTR) initting ReconOptimizer");
				gpAoHrtReconOpt_FTRAlgoInit();
			}
			break;

		case eReconOpt_State_Start:
			{
				double dSum;

				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @1");
				#endif

				count_group_A = 0;
				count_group_B = 0;
				count_ffts = 0;
				count_PostProcess = 0;
				count_testDataNdx = 0;

				optSetPerFreqAndCtXfrFunc( );

				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR): Corner of the currAvg(R) matrix ");
					dumpCornerOfMatrix( (void*)(&currAvg[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );

					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR): Corner of the currAvg(I) matrix ");
					dumpCornerOfMatrix( (void*)(&currAvg[1][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );

					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR):	Corner of the currAvgSnapShotA(R) matrix ");					dumpCornerOfMatrix( (void*)(&currAvgSnapShotA[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR):	Corner of the currAvgSnapShotA(I) matrix ");
					dumpCornerOfMatrix( (void*)(&currAvgSnapShotA[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );

					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR):	Corner of the currAvgSnapShotB(R) matrix ");
					dumpCornerOfMatrix( (void*)(&currAvgSnapShotB[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
					HRT_PRINTF_MIN("INFO (gpAoHrtReconOpt_ReconOptFTR):	Corner of the currAvgSnapShotB(I) matrix ");
					dumpCornerOfMatrix( (void*)(&currAvgSnapShotB[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
				#endif


				#if ( RECONOPT_TEST != 0 )
					HRT_PRINTF( "\n Corner of the testDataR matrix (eReconOpt_State_Collect_Group_A1)");
					dumpCornerOfMatrix( (void*)(&testDataR[0][0]), OPT_TOTAL_LEN, MAX_MODES, TFLOAT, false );

					HRT_PRINTF( "\n Corner of the testDataI matrix (eReconOpt_State_Collect_Group_A1)");
					dumpCornerOfMatrix( (void*)(&testDataI[0][0]), OPT_TOTAL_LEN, MAX_MODES, TFLOAT, false );
				#endif

				dSum = 0.0;
				for( ii=0; ii<PER_LEN; ii++)
					dSum += hannWinVals[ii];

				#if ( RECONOPT_DEBUG_DUMP != 0 )
					// Set up to dump all the modal data for each optimization pass
					{
						char *dataDir = RTC_DATA_DIR;
						char  pathName[1024];
						long  dimSize[4];

						dimSize[0] = MAX_MODES * 2;	
						dimSize[1] = OPT_TOTAL_LEN;

						sprintf( pathName, "%soptdata/optData_%ld", dataDir, OptSequenceNumber );
						mkdir( pathName, S_IRWXU | S_IRWXG | S_IRWXO );
						strcat( pathName, "/ftrModeData_raw.fits" );
						dataFPtr = openFitsForWrite( pathName , dimSize, 2, TFLOAT );
						if( dataFPtr == NULL)
						{
							HRT_PRINTF( "ERR (in DEBUG) (gpAoHrtReconOpt_ReconOptFTR): UNABLE TO OPEN THE FTR MODE DUMP FILE \n ");
						}
					}
					ftrSequenceNumber = 0;
				#endif
				ReconOpt_State = eReconOpt_State_Collect_Group_A1;
			}
			//  FALL THROUGH!!!!  - Intentional

		//	********************************************
		//	** Collecting Data for A1 only ( no fft on Bn) -- Done by the foreground at this point
		//	********************************************
		case eReconOpt_State_Collect_Group_A1:
			{ 
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @2");
				#endif

				gpAoHrtReconOpt_FTRAlgoGetData( &rawReconOptHalfData1[count_group_A][0], pFtrFFTModesData );
				count_group_A++;

				if( count_group_A == HALF_PER_LEN )
				{
					// Verify the threads are not busy
					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					{
						if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						{
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtReconOpt_ReconOptFTR) Optimizer OVERRUN Collecting Group A1");
							break;
						}
					}

					count_group_B = 0;

					#if ( RECONOPT_DEBUG != 0 )
						HRT_PRINTF( "\n Slice Corner of the rawReconOptHalfData1 matrix (eReconOpt_State_Collect_Group_A1)");
						dumpSliceOfComplexDataCube( (void*)( rawReconOptHalfData1 ),  GPI_PUP_DIA, GPI_PUP_DIA, HALF_PER_LEN );
					#endif

					// Do not wait for threads to complete
					ReconOpt_State = eReconOpt_State_Collect_Group_A2B1;

					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)	// Start all threads 'pre-processing data'
						ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PreProcess_A1;
				}
#if ( TIMER_TEST != 0 )
				rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTAfterA1]);
#endif
			}
			break;

		case eReconOpt_State_Collect_Group_A1B2:
			{ 
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @3");
				#endif

				gpAoHrtReconOpt_FTRAlgoGetData( &rawReconOptHalfData1[count_group_A][0], pFtrFFTModesData );

				count_group_A++;
				count_group_B++;

				if( count_group_B == PER_LEN )
				{
					// Verify the threads are not busy
					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					{
						if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						{
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtReconOpt_ReconOptFTR) Optimizer OVERRUN Collecting Group A1B2");
							break;
						}
					}

					count_group_B = 0;

					#if ( RECONOPT_DO_BLOCK_AVG==0 )
					for( ii=0; ii<MAX_MODES; ii++)
					{
						currAvgSnapShotB[0][ii] = currAvg[0][ii];
						currAvgSnapShotB[1][ii] = currAvg[1][ii];
					}
					#endif

					#if ( RECONOPT_DEBUG_DUMP != 0 )
					{
						long dimSize[4];
						char tmpFName[512];

						sprintf( tmpFName, "%soptdata/optData_%ld/currAvgSnapShot_B_%d.fits", dataDir, OptSequenceNumber, count_ffts  );
						dimSize[0] = MAX_MODES;
						dimSize[1] = 2;
						writeCompleteFitsFile( tmpFName, dimSize, 2, TFLOAT, &currAvgSnapShotB[0][0] );
					}
					#endif

					#if ( RECONOPT_DEBUG != 0 )
						HRT_PRINTF( "INFO (gpAoHrtReconOpt_ReconOptFTR): Slice Corner of the rawReconOptHalfData1 matrix (eReconOpt_State_Collect_Group_A1B2)");
						dumpSliceOfComplexDataCube( (void*)( rawReconOptHalfData1 ),  GPI_PUP_DIA, GPI_PUP_DIA, HALF_PER_LEN );
					#endif

					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
						ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PreProcess_A1B2_FFTB;

					// We need to kick off another FFT.  If it is > NUM_FFT then wait for'pre-process' to complete, else get more data
					if( ++count_ffts >= NUM_FFTS )
					{
						SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_ReconOptFTR) A1B2 - Wait Preprocess done");
						ReconOpt_State = eReconOpt_State_Wait_PreProcess_Done;			// Done data collection, start wait to do final processing
						count_PostProcess = 0;											// Will be used to verify that entire period is skipped forpost process
					}
					else
					{
						// Do not wait for threads to complete
						SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_ReconOptFTR) A1B2 - Start A2B1 Collection");
						ReconOpt_State = eReconOpt_State_Collect_Group_A2B1; 			// Not done data collection, get next group ( A2B1)
					}
				}
#if ( TIMER_TEST != 0 )
				rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTAfterA1B2]);
#endif
			}
			break;

		case eReconOpt_State_Collect_Group_A2B1:
			{ 
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @4");
				#endif

				gpAoHrtReconOpt_FTRAlgoGetData( &rawReconOptHalfData2[count_group_B][0], pFtrFFTModesData );
				count_group_A++;
				count_group_B++;

				if( count_group_A == PER_LEN )
				{
					// Verify the threads are not busy
					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
						if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						{
							SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtReconOpt_ReconOptFTR) Optimizer OVERRUN Collecting Group A2B1");
							break;
						}

					count_group_A = 0;

					#if ( RECONOPT_DEBUG != 0 )
						HRT_PRINTF( "\n Slice Corner of the rawReconOptHalfData2 matrix (eReconOpt_State_Collect_Group_A2B1)");
						dumpSliceOfComplexDataCube( (void*)( rawReconOptHalfData2 ),  GPI_PUP_DIA, GPI_PUP_DIA, HALF_PER_LEN );
					#endif

					#if ( RECONOPT_DO_BLOCK_AVG==0 )
					for( ii=0; ii<MAX_MODES; ii++)
					{
						currAvgSnapShotA[0][ii] = currAvg[0][ii];
						currAvgSnapShotA[1][ii] = currAvg[1][ii];
					}
					#endif

					#if ( RECONOPT_DEBUG_DUMP != 0 )
					{
						long dimSize[4];
						char tmpFName[512];

						sprintf( tmpFName, "%soptdata/optData_%ld/currAvgSnapShot_A_%d.fits", dataDir, OptSequenceNumber, count_ffts  );
						dimSize[0] = MAX_MODES;
						dimSize[1] = 2;
						writeCompleteFitsFile( tmpFName, dimSize, 2, TFLOAT, &currAvgSnapShotA[0][0] );
					}
					#endif

					for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
						ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PreProcess_A2B1_FFTA;

					// We need to kick off another FFT.  If it is > NUM_FFT then wait for'pre-process' to complete, else get more data
					if( ++count_ffts >= NUM_FFTS )
					{
						SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_ReconOptFTR) A2B1 - Wait Preprocess done");
						ReconOpt_State = eReconOpt_State_Wait_PreProcess_Done;			// Done data collection, start wait to do final processing
						count_PostProcess = 0;											// Will be used to verify that entire period is skipped for post process
					}
					else
					{
						// Do not wait for threads to complete
						SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_ReconOptFTR) A2B1 - Start A1B2 Collection");
						ReconOpt_State = eReconOpt_State_Collect_Group_A1B2;			// Not done data collection, get next group ( A1B2)
					}
				}
#if ( TIMER_TEST != 0 )
				rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTAfterA2B1]);
#endif
			}
			break;

		case eReconOpt_State_Wait_PreProcess_Done:
			{
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @5");
				#endif

				++count_PostProcess;

				// walk through the threads and see if they are all done
				done = 1;		// 'pre'-flag that all are done - will be reset in test if we are not
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						done = 0;  // flag that at least one thread is not done

				// test if any threads are still busy 
				if( done == 0 )
					break;  // do not start post process if not done yet - exit switch and try again next frame

				// if here then all threads are done - start post process
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PostProcess_1;

				ReconOpt_State = eReconOpt_State_Wait_PostProcess_1_Done;  // Wait for the Post processing to complete

			}
			break;

		case eReconOpt_State_Wait_PostProcess_1_Done:
			{
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @6");
				#endif

				count_PostProcess++;

				// walk through the threads and see if they are all done
				done = 1;		// 'pre'-flag that all are done - will be reset in test if we are not
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						done = 0;  // flag that at least one thread is not done

				// test if any threads are still busy 
				if( done == 0 )
					break;  // do not start post process if not done yet - exit switch and try again next frame

				// if here then all threads are done - start post process
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PostProcess_2;

				ReconOpt_State = eReconOpt_State_Wait_PostProcess_2_Done;  // Wait for the Post processing period to end ( HALF_PER_LEN )
			}
			break;

		case eReconOpt_State_Wait_PostProcess_2_Done:
			{
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @7");
				#endif

				count_PostProcess++;

				// walk through the threads and see if they are all done
				done = 1;		// 'pre'-flag that all are done - will be reset in test if we are not
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						done = 0;  // flag that at least one thread is not done

				// test if any threads are still busy 
				if( done == 0 )
					break;  // do not start post process if not done yet -  exit switch and try again next frame

				// if here then all threads are done - start post process
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					ReconAlgFTRThreadStat[ ii ] = eReconAlgFTRTh_PostProcess_3;

				ReconOpt_State = eReconOpt_State_Wait_PostProcess_3_Done;  // Wait for the Post processing period to end ( HALF_PER_LEN )
			}
			break;

		case eReconOpt_State_Wait_PostProcess_3_Done:
			{
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @8");
				#endif

				count_PostProcess++;

				// walk through the threads and see if they are all done
				done = 1;		// 'pre'-flag that all are done - will be reset in test if we are not
				for( ii=OPT_START_THREAD; ii<OPT_LIMIT_THREAD; ii++)
					if( ReconAlgFTRThreadStat[ ii ] != eReconAlgFTRTh_Wait)
						done = 0;  // flag that at least one thread is not done

				// test if any threads are still busy 
				if( done == 0 )
					break;  // do not start post process if not done yet -  exit switch and try again next frame

				ReconOpt_State = eReconOpt_State_WaitPostProcessPeriod;  // Wait for the Post processing period to end ( HALF_PER_LEN )
			}
			break;

		case eReconOpt_State_WaitPostProcessPeriod:
			{
				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( " @9");
				#endif

				if( ++count_PostProcess > PER_LEN )
					SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtReconOpt_ReconOptFTR) Optimizer OVERRUN postProcess", count_PostProcess);

				if( count_PostProcess >= PER_LEN )
				{
					#if ( RECONOPT_DEBUG_DUMP != 0  )
						// close modal data for diagnostics
						{
							OptSequenceNumber++;

							flushFitsBuffer( dataFPtr );
							closeFits( dataFPtr );
						}
					#endif

					// Restart for next Optimizer frame
					ReconOpt_State = eReconOpt_State_Start;
				}
			}
			break;

		default:
			#if ( RECONOPT_DEBUG != 0 )
				HRT_PRINTF( " @10");
			#endif
	
			ReconOpt_State = eReconOpt_State_Init;			// do Opt steps on testData set
			break;
	}	// end of switch ( ReconOpt_State)

	return;
}	// gpAoHrtReconOpt_ReconOptFTR( ReconBuffType* reconBufPtr)


/*!
 *******************************************************************************
 *
 *	@fn static void gpAoHrtReconOpt_FTRAlgoGetData( fft_complex_type* pRawBuf, float* pFtrFFTModesData )
 * 
 *	@brief
 *	FTR Recon Optimization - Get and move the next frame of data into a new temporary buffer.
 *
 *	@b DESCRIPTION:
 *	This performs loading the data to the optimizer  (may be test data if turned on)
 *	The data has the current MEAN subtracted amd is multiplied by the 'freqDomainScaling' value
 *	@par
 *	
 *
 *	@b Usage:  gpAoHrtReconOpt_FTRAlgoGetData( (fft_complex_type*) pRawBuf, (float*) pFtrFFTModesData )
 * 
 *	@param[ in ]	pRawBuf		(fft_complex_type*):	Buffer - where data is to be put (NOTE! Only half a PER_LEN is collected at one time)
 *	@param[ in ]	pFtrFFTModesData	(float*):		Pointer to buffer where raw RECON data is currently
 *	
 *	@return void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoHrtReconOpt_FTRAlgoGetData( fft_complex_type* pRawBuf, float* pFtrFFTModesData )
{
	int    ii, jj;
	double fullAvgR, fullAvgI;

#if ( TIMER_TEST != 0 )
	int	tmpTimerNdx;
	tmpTimerNdx = reconFrameID % NUM_TIME_SETS;
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTStartGetData]);
#endif

	// Update 'currAvg' with current data, if we are actually calculating average
	// If we are using the test case then possible reading in the overall average for the current set of data
	// ---- All of this is done before
	//
	#if ( RECONOPT_DO_BLOCK_AVG == 0 )
	{
		float tempNewR, tempNewI;
		for( ii = 0; ii < MAX_MODES; ii++)
		{
			if ( ii == fullAvgCntr )  // every MAX_MODES, calculate the full average for each mode, to mitigate truncation error
			{
				tempNewR = pFtrFFTModesData[ ii * 2 + 0 ];
				currAvgBuffer[ currAvgBufNdx ][ 0 ][ ii ] = tempNewR;  // put new value in current position
				pRawBuf[ ii ][ 0 ] = tempNewR;

				tempNewI = pFtrFFTModesData[ ii * 2 + 1 ];
				currAvgBuffer[ currAvgBufNdx ][ 1 ][ ii ] = tempNewI;  // put new value in current position
				pRawBuf[ ii ][ 1 ] = tempNewI;

				fullAvgR = 0.0;
				fullAvgI = 0.0;
				for( jj = 0; jj < OPT_AVG_LEN; jj++)
				{
					fullAvgR += (double)currAvgBuffer[ jj ][ 0 ][ ii ];
					fullAvgI += (double)currAvgBuffer[ jj ][ 1 ][ ii ];
				}
				currAvg[ 0 ][ ii ] = (float)( fullAvgR * (double)OPT_AVG_LEN_INVERSE );
				currAvg[ 1 ][ ii ] = (float)( fullAvgI * (double)OPT_AVG_LEN_INVERSE );
			}
			else
			{
				currAvg[ 0 ][ ii ] -= ( currAvgBuffer[ currAvgBufNdx ][ 0 ][ ii ] * OPT_AVG_LEN_INVERSE );	// remove old value at current position
				tempNewR = pFtrFFTModesData[ ii * 2 + 0 ];
				currAvgBuffer[ currAvgBufNdx ][ 0 ][ ii ] = tempNewR;  // put new value in current position
				pRawBuf[ ii ][ 0 ] = tempNewR;
				currAvg[ 0 ][ ii ] += ( tempNewR * OPT_AVG_LEN_INVERSE );  // add new value at current position

				currAvg[ 1 ][ ii ] -= ( currAvgBuffer[ currAvgBufNdx ][ 1 ][ ii ] * OPT_AVG_LEN_INVERSE );	// remove old value at current position
				tempNewI = pFtrFFTModesData[ ii * 2 + 1 ];
				currAvgBuffer[ currAvgBufNdx ][ 1 ][ ii ] = tempNewI;  // put new value in current position
				pRawBuf[ ii ][ 1 ] = tempNewI;
				currAvg[ 1 ][ ii ] += ( tempNewI * OPT_AVG_LEN_INVERSE );  // add new value at current position
			}

			#if ( RECONOPT_TEST != 0 )  // overwrite the data we just got with known data (in testData R/I)
			{
				pRawBuf[ ii ][ 0 ] = testDataR[ii][ count_testDataNdx ];
				pRawBuf[ ii ][ 1 ] = testDataI[ii][ count_testDataNdx ];
			}
			#endif
		}
		currAvgBufNdx = ( currAvgBufNdx + 1 ) % OPT_AVG_LEN;  // move to the next location in the averaging buffers (for next time)
		fullAvgCntr = ( fullAvgCntr + 1 ) % MAX_MODES;
	}
	#else
	// In this case - we are moving data out of the Recon buffers into a 'single local temporal' buffer for the optimizer threads
	for( ii=0; ii<MAX_MODES; ii++)
	{
		pRawBuf[ ii ][ 0 ] = *pFtrFFTModesData++;
		pRawBuf[ ii ][ 1 ] = *pFtrFFTModesData++;

		#if ( RECONOPT_TEST != 0 )
		{
			// In this case - we will overwrite the data we just got with known data (in testData R/I)
			pRawBuf[ ii ][ 0 ] = testDataR[ii][ count_testDataNdx ]; //smj-091118 * freqDomainScaling;
			pRawBuf[ ii ][ 1 ] = testDataI[ii][ count_testDataNdx ]; //smj-091118 * freqDomainScaling;
		}
		#endif
	}
	#endif
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTAfterCrntAvg]);
#endif

	#if ( RECONOPT_DEBUG_DUMP != 0 )
		// Dump current frame modal data for diagnostics
		{
			long	fpixel[10];

			fpixel[ 0 ] = 1;
			fpixel[ 1 ] = ++ftrSequenceNumber;

			writeFitsBlock( dataFPtr, (MAX_MODES * 2), TFLOAT, (float*)pRawBuf, fpixel );
			flushFitsBuffer( dataFPtr );
		}
	#endif

	#if ( RECONOPT_TEST != 0 )
		count_testDataNdx++;
	#endif
#if ( TIMER_TEST != 0 )
	rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerOPTAfterMoveToOpt]);
#endif

	return;
}


/*!
 *******************************************************************************
 *
 *	@fn short gpAoHrtReconOpt_FTRAlgoProcess( short thNdx, char threadCmnd) 
 *
 *	@brief
 *	FTR Recon Optimization worker thread commands
 *
 *	@b DESCRIPTION:
 *	This performs loading the data to the optimizer and applying the optimized
 *	 gain into the reconstruction. Called by Recon worker threads:
 *	 gpAoHrtRecon_FTRAlgoThread( void *threadNdx)
 *	@par
 *	
 *
 *	@b Usage: short gpAoHrtReconOpt_FTRAlgoProcess( short thNdx, char threadCmnd )
 * 
 *	@param[ in ]	thNdx		( short):	Calling working thread number
 *	@param[ in ]	threadCmnd	( char):	Recon Optimization command
	*
 *	@return 
 *	return a flag indicating whether the command was processed
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

short gpAoHrtReconOpt_FTRAlgoProcess( short thNdx, char threadCmnd )
{
	short		CmdProcessed = 1;

	int			ii, jj, jj1;
	double		tmpReal, tmpImag, tmpFloat;
	long long	ndx_in, ndx_out;
#if ( STANDALONE_FFT == 0 )
	long long	fftDataNdx, modalPsdNdx;
#endif

	int			optThNdx;
	float*		pModalPsd = NULL;
	float*		pModalPsdJointInput = NULL;

	// Got a cmd, make sure it's not a Quit
	if( threadCmnd == eReconAlgFTRTh_Quit)
		return CmdProcessed;

	optThNdx = thNdx - (short)OPT_START_THREAD;

	// execute the ReconOpt cmd
	switch( threadCmnd )
	{
		case eReconAlgFTRTh_Opt_Start:
			{
				// No-op right now - this might be used to speed up restarts
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PreProcess_A1:
			{
#if (RTL_SYSTEM != 0)
//				moving all of the data below, all at once, was slowing down other threads, causing jitter in
//				> overall loop processing -- so, we've added a nanosleep() using the following timing
				struct rtl_timespec	next, remaining;
				long numMoves, nanosecsToMoveAll, nanosecsBetweenMoves;
				numMoves = MAX_MODES / OPT_NUM_THREADS;
				nanosecsToMoveAll = HALF_PER_LEN * 1000000000L / LegalFrameRates[hrt_camFrameRate];
				nanosecsBetweenMoves = nanosecsToMoveAll / numMoves / 2;  //divide by 2 to give time to move and for safety
				if (nanosecsBetweenMoves >= 1000000000L)
					nanosecsBetweenMoves = 999999999L;
#endif

				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( "\n(reconOpt) PreProc_A1: #%d", thNdx );
					HRT_PRINTF( "\n               optThrNdx: #%d", optThNdx );
				#endif

				ndx_in =  MAX_MODES / OPT_NUM_THREADS * optThNdx;				// First half of A
				ndx_out = num_kl_per_thread * optThNdx * PER_LEN;
				for( ii=0; ii<( MAX_MODES/OPT_NUM_THREADS ); ii++ )
				{
#if (RTL_SYSTEM != 0)
					next.tv_sec = 0;
					next.tv_nsec = nanosecsBetweenMoves;
					while( rtl_nanosleep( &next, &remaining ) < 0 )
						memcpy( &next, &remaining, sizeof( next ) );
#endif

					// Only init the part we will use in this thread
					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ MAX_MODES/OPT_NUM_THREADS * optThNdx + ii ][ 0 ]);
					for( jj1=0; jj1<PER_LEN; jj1++ )
						pModalPsd[ jj1 ] = 0.0;

					#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
						// Note that if we are using kl_valid - this is the place where the data is decimated ( unused modes are removed ).
						if( !kl_valid1[ ii + MAX_MODES/OPT_NUM_THREADS * optThNdx ] )
						{
							ndx_in++;
							continue;
						}
					#endif

					for( jj1=0; jj1<HALF_PER_LEN; jj1++)
					{
						perData_A[ ndx_out + jj1 ][ 0 ] = rawReconOptHalfData1[ jj1 ][ ndx_in ][ 0 ] * hannWinVals[ jj1 ];	
						perData_A[ ndx_out + jj1 ][ 1 ] = rawReconOptHalfData1[ jj1 ][ ndx_in ][ 1 ] * hannWinVals[ jj1 ];	
					}
					ndx_out += PER_LEN;
					ndx_in++;
				}
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PreProcess_A2B1_FFTA:
			{
				#if ( RECONOPT_DEBUG != 0 )
					if( optThNdx == 0 )
					{
						HRT_PRINTF( "\n(reconOpt) PreProc_A2B1: #%d", thNdx );
						HRT_PRINTF( "\n               optThrNdx: #%d", optThNdx );
						HRT_PRINTF( "\n      num_kl_per_thread = %d", num_kl_per_thread );
					}
				#endif

				ndx_in =  MAX_MODES / OPT_NUM_THREADS * optThNdx;
				ndx_out = num_kl_per_thread * optThNdx * PER_LEN;
				for( ii = 0; ii < ( MAX_MODES / OPT_NUM_THREADS); ii++ )
				{
					#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
						// Note that if we are using kl_valid - this is the place where the data is decimated ( unused modes are removed ).
						if( !kl_valid1[ ii + MAX_MODES/OPT_NUM_THREADS * optThNdx ] )
						{
							ndx_in++;
							continue;
						}
					#endif

					for( jj1 = 0; jj1 < HALF_PER_LEN; jj1++)
					{
// 						move the 2nd half of the 'A' data
						perData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ] = ( rawReconOptHalfData2[ jj1 ][ ndx_in ][ 0 ]  - currAvgSnapShotA[0][ndx_in] ) * hannWinVals[ jj1 + HALF_PER_LEN ];	
						perData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ] = ( rawReconOptHalfData2[ jj1 ][ ndx_in ][ 1 ]  - currAvgSnapShotA[1][ndx_in] ) * hannWinVals[ jj1 + HALF_PER_LEN ];

						perData_A[ ndx_out + jj1 ][ 0 ] -= currAvgSnapShotA[0][ndx_in] * hannWinVals[ jj1 ];
						perData_A[ ndx_out + jj1 ][ 1 ] -= currAvgSnapShotA[1][ndx_in] * hannWinVals[ jj1 ];

//						move the 1st half of the 'B' data
						perData_B[ ndx_out + jj1 ][ 0 ] = ( rawReconOptHalfData2[ jj1 ][ ndx_in ][ 0 ] ) * hannWinVals[ jj1 ];	
						perData_B[ ndx_out + jj1 ][ 1 ] = ( rawReconOptHalfData2[ jj1 ][ ndx_in ][ 1 ] ) * hannWinVals[ jj1 ];	

#if (( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 ))
						tmpPerData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ] = perData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ];
						tmpPerData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ] = perData_A[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ];
						tmpPerData_A[ ndx_out + jj1 ][ 0 ] = perData_A[ ndx_out + jj1 ][ 0 ];
						tmpPerData_A[ ndx_out + jj1 ][ 1 ] = perData_A[ ndx_out + jj1 ][ 1 ];
#endif
					}

#if ( STANDALONE_FFT != 0 )
// 					perform FFT on 'A' data (if we're not using fftw)
					spiral_fft_float(PER_LEN, 1, &perData_A[ndx_out][0], &perData_A[ndx_out][0]);

// 					sum up the temporal data for each mode handled by this thread
//					for a reason that eludes me, all fftw modes other than DC (mode 0) are in the reverse order than other ffts
					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ ndx_in ][ 0 ]);
					tmpReal = perData_A[ ndx_out ][ 0 ] / PER_LEN;  // have to normalize fft's output, first
					tmpImag = perData_A[ ndx_out ][ 1 ] / PER_LEN;
					tmpFloat = tmpReal * tmpReal + tmpImag * tmpImag;  // calculate the modulus squared
					pModalPsd[ 0 ] += ((tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS);  // and normalize the result
					for( jj1 = 1; jj1 < PER_LEN; jj1++)
					{
						tmpReal = perData_A[ ndx_out + jj1 ][ 0 ] / PER_LEN;	 // have to normalize fft's output, first
						tmpImag = perData_A[ ndx_out + jj1 ][ 1 ] / PER_LEN;
						tmpFloat = tmpReal * tmpReal + tmpImag * tmpImag;		 // calculate the modulus
						pModalPsd[ PER_LEN - jj1 ] += ((tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS);  // and normalize the result
					}
#endif

					ndx_in++;
					ndx_out += PER_LEN;
				}

				#if ( RECONOPT_DEBUG != 0 )
					{
						HRT_PRINTF( "\n (Ndx=%d) Corner of the OPT (IN) FFT A matrix (AFTER WINDOW) (Count = %d) ", optThNdx, count_ffts);
						dumpSliceOfComplexDataCube( &tmpPerData_A[ num_kl_per_thread * PER_LEN * optThNdx ],  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN );
					}	
				#endif
				#if ( RECONOPT_DEBUG_DUMP != 0 )
					if( optThNdx == 0 )
					{
						long	dimSize[4];
						char	fname[1024];
						dimSize[0] = PER_LEN;
						dimSize[1] = MAX_MODES * 2;
						sprintf( fname, "%soptdata/optData_%ld/fft_in_data_A_%x.fits", dataDir, OptSequenceNumber, count_ffts);
						writeCompleteFitsFile( fname, dimSize, 2, TFLOAT, (void *)tmpPerData_A);
					}
				#endif

#if ( STANDALONE_FFT == 0 )
// 				perform FFT on 'A' Group (if we're using fftw)
				fftwf_execute( perPlan_A[ optThNdx ]  );

				// Sum up the temporal data for each mode handled by this thread
				fftDataNdx = num_kl_per_thread * optThNdx * PER_LEN;
				modalPsdNdx = MAX_MODES/OPT_NUM_THREADS * optThNdx;
				for( ii=0; ii<(MAX_MODES/OPT_NUM_THREADS); ii++, modalPsdNdx++)	 // ii is 'offset' for current mode that is being processed
				{
					#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )  // Turn on - KL_VALID handling
						if( !kl_valid1[ modalPsdNdx ] )
							continue;
					#endif

					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ modalPsdNdx ][ 0 ]);
					for( jj1=0; jj1<PER_LEN; jj1++)
					{
						tmpReal = perData_A[ fftDataNdx + jj1 ][ 0 ] / PER_LEN;	 // have to normalize fft's output, first
						tmpImag = perData_A[ fftDataNdx + jj1 ][ 1 ] / PER_LEN;
						tmpFloat = tmpReal * tmpReal + tmpImag * tmpImag;		 // calculate the modulus squared
						pModalPsd[ jj1 ] += ((tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS);  // and normalize the result
					}
					fftDataNdx += PER_LEN;
				}
#endif

				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( "\n (Ndx=%d) Corner of the OPT (OUT) FFT A matrix (Count = %d) ", optThNdx, count_ffts);
					dumpSliceOfComplexDataCube( &perData_A[num_kl_per_thread * PER_LEN * optThNdx ],  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN );
					HRT_PRINTF( "\n (Ndx=%d) Corner of the Modal PSDs after summing in A2B1 ", optThNdx );
					dumpSliceOfDataCube( &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[0][0]),  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN, TFLOAT );
				#endif
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PreProcess_A1B2_FFTB:
			{
				#if ( RECONOPT_DEBUG != 0 )
					if( optThNdx == 0 )
					{
						HRT_PRINTF( "\n(reconOpt) PreProc_A1B2: #%d", thNdx );
						HRT_PRINTF( "\n               optThrNdx: #%d", optThNdx );
						HRT_PRINTF( "\n      num_kl_per_thread = %d", num_kl_per_thread );
					}
				#endif

				ndx_in =  MAX_MODES / OPT_NUM_THREADS * optThNdx;
				ndx_out = num_kl_per_thread * optThNdx * PER_LEN;
				for( ii = 0; ii < ( MAX_MODES / OPT_NUM_THREADS); ii++ )
				{
					#if ( RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
						// Note that if we are using kl_valid - this is the place where the data is decimated ( unused modes are removed ).
						if( !kl_valid1[ ii + MAX_MODES/OPT_NUM_THREADS * optThNdx ] )
						{
							ndx_in++;
							continue;
						}
					#endif

					for( jj1 = 0; jj1 < HALF_PER_LEN; jj1++)
					{
// 						move the 1st half of the 'A' data
						perData_A[ ndx_out + jj1 ][ 0 ] = ( rawReconOptHalfData1[ jj1 ][ ndx_in ][ 0 ] ) * hannWinVals[ jj1 ];
						perData_A[ ndx_out + jj1 ][ 1 ] = ( rawReconOptHalfData1[ jj1 ][ ndx_in ][ 1 ] ) * hannWinVals[ jj1 ];

//						move the 2nd half of the 'B' data
						perData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ] = ( rawReconOptHalfData1[ jj1 ][ ndx_in ][ 0 ] - currAvgSnapShotB[0][ndx_in] ) * hannWinVals[ jj1 + HALF_PER_LEN ];
						perData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ] = ( rawReconOptHalfData1[ jj1 ][ ndx_in ][ 1 ] - currAvgSnapShotB[1][ndx_in] ) * hannWinVals[ jj1 + HALF_PER_LEN ];

						perData_B[ ndx_out + jj1 ][ 0 ] -= currAvgSnapShotB[0][ndx_in] * hannWinVals[ jj1 ];
						perData_B[ ndx_out + jj1 ][ 1 ] -= currAvgSnapShotB[1][ndx_in] * hannWinVals[ jj1 ];

#if (( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 ))
						tmpPerData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ] = perData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 0 ];
						tmpPerData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ] = perData_B[ ndx_out + jj1 + HALF_PER_LEN ][ 1 ];
						tmpPerData_B[ ndx_out + jj1 ][ 0 ] = perData_B[ ndx_out + jj1 ][ 0 ];
						tmpPerData_B[ ndx_out + jj1 ][ 1 ] = perData_B[ ndx_out + jj1 ][ 1 ];
#endif
					}

#if ( STANDALONE_FFT != 0 )
// 					perform FFT on 'B' data (if we're not using fftw)
					spiral_fft_float(PER_LEN, 1, &perData_B[ndx_out][0], &perData_B[ndx_out][0]);

// 					sum up the temporal data for each mode handled by this thread
//					for a reason that eludes me, all fftw modes other than DC (mode 0) are in the reverse order than other ffts
					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ ndx_in ][ 0 ]);
					tmpReal = perData_B[ ndx_out ][ 0 ] / PER_LEN;								   // have to normalize fft's output, first
					tmpImag = perData_B[ ndx_out ][ 1 ] / PER_LEN;
					tmpFloat = tmpReal * tmpReal + tmpImag * tmpImag;								   // calculate the modulus
					pModalPsd[ 0 ] += ((tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS)  ;  // and normalize the result
					for( jj1 = 1; jj1 < PER_LEN; jj1++)
					{
						tmpReal = perData_B[ ndx_out + jj1 ][ 0 ] / PER_LEN;			// have to normalize fft's output, first
						tmpImag = perData_B[ ndx_out + jj1 ][ 1 ] / PER_LEN;
						tmpFloat = tmpReal * tmpReal + tmpImag * tmpImag;				// calculate the modulus
						pModalPsd[ PER_LEN - jj1 ] += ((tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS)  ;  // and normalize the result
					}
#endif

					ndx_in++;
					ndx_out += PER_LEN;
				}

				#if ( RECONOPT_DEBUG != 0 )
					{
						HRT_PRINTF( "\n (Ndx=%d) Corner of the OPT (IN) FFT B matrix (Count = %d) ", optThNdx, count_ffts);
						dumpSliceOfComplexDataCube( &tmpPerData_B[ num_kl_per_thread * PER_LEN * optThNdx ],  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN );
					}
				#endif
				#if ( RECONOPT_DEBUG_DUMP != 0 )
					if( optThNdx == 0 )
					{
						long dimSize[4];
						char fname[1024];
						dimSize[0] = PER_LEN;
						dimSize[1] = MAX_MODES * 2;
						sprintf( fname, "%soptdata/optData_%ld/fft_in_data_B_%x.fits", dataDir, OptSequenceNumber, count_ffts);
						writeCompleteFitsFile( fname, dimSize, 2, TFLOAT, (void *)tmpPerData_B);
					}
				#endif

#if ( STANDALONE_FFT == 0 )
// 				perform FFT on 'B' Group (if we're using fftw)
				fftwf_execute( perPlan_B[ optThNdx ] );

				fftDataNdx = num_kl_per_thread * optThNdx * PER_LEN;
				modalPsdNdx = MAX_MODES/OPT_NUM_THREADS * optThNdx;
				for( ii=0; ii<(MAX_MODES/OPT_NUM_THREADS); ii++, modalPsdNdx++)				// ii is 'offset' for current mode that is being processed
				{
					#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
						if( !kl_valid1[ modalPsdNdx ] )
							continue;
					#endif

					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ modalPsdNdx ][ 0 ]);
					for( jj1=0; jj1<PER_LEN; jj1++)
					{
						tmpReal = perData_B[ fftDataNdx + jj1 ][ 0 ] / PER_LEN;			// have to normalize fft's output, first
						tmpImag = perData_B[ fftDataNdx + jj1 ][ 1 ] / PER_LEN;
						tmpFloat =  tmpReal * tmpReal + tmpImag * tmpImag;				// calculate the modulus
						pModalPsd[ jj1 ] += ( (tmpFloat * PER_LEN / hannTotWinSquared) / (float)NUM_FFTS ) ;  // and normalize the result
					}
					fftDataNdx += PER_LEN;
				}
#endif

				#if ( RECONOPT_DEBUG != 0 )
					HRT_PRINTF( "\n (Ndx=%d) Corner of the OPT (OUT) FFT B matrix (Count = %d) ", optThNdx, count_ffts);
					dumpSliceOfComplexDataCube( &perData_B[ num_kl_per_thread * PER_LEN * optThNdx ],  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN );
					HRT_PRINTF( "\n (Ndx=%d) Corner of the Modal PSDs after summing in A1B2 ", optThNdx );
					dumpSliceOfDataCube(  &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[0][0]),  GPI_PUP_DIA, GPI_PUP_DIA, PER_LEN, TFLOAT );
				#endif
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PostProcess_1:
			// ********************************************
			// **	Find Optimal New Gains				**
			// ********************************************
			{
			 	int	jj, currMode;
				long long	modeNdx;

				//	calculate leaky-integrator coefficients corresponding to 'previous' gains, 
				//		determine corresponding transfer function, and then convert closed-loop measurement PSDs to
				//		open-loop measurement PSD
				modeNdx = MAX_MODES / OPT_NUM_THREADS * optThNdx;
				for( currMode=0; currMode<(MAX_MODES/OPT_NUM_THREADS); currMode++, modeNdx++ )	// do modes assigned to this thread
				{
					#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
						if( !kl_valid1[ modeNdx ] )
						{
							pModalPsdJointInput = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsdsJointInputs[ modeNdx ][ 0 ]);

							for( jj=0; jj<PER_LEN; jj++)
								pModalPsdJointInput[ jj ] = 0.0;
							continue;
						}
					#endif

					pModalPsd = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ modeNdx ][ 0 ]);
					pModalPsdJointInput = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsdsJointInputs[ modeNdx ][ 0 ]);

					// Has to be calculated each time, because the gains ( 	hrt_workingModalGains ) will be changing.
					for( jj=0; jj<PER_LEN; jj++)
					{
#if ( STANDALONE_COMPLEX == 0 )
						#if ( RECONOPT_TEST != 0 )
							cofZ[optThNdx] = TEST_GAIN * cofZDenom_Inv[ jj ];
						#else	
							if( (hrt_AoTwtLoopStat != eAOLoopClosed) || (hrt_AoWfrLoopStat != eAOLoopClosed) )
							 	cofZ[optThNdx] = cofZDenom_Inv[ jj ];
							else
							 	cofZ[optThNdx] = hrt_workingModalGains[ modeNdx ] * cofZDenom_Inv[ jj ];
						#endif
						tfToConvertToPhase[optThNdx] = cabsf( 1.0 + delayTerm[ jj ] * cofZ[optThNdx]);
						tfToConvertToPhase[optThNdx] *= tfToConvertToPhase[optThNdx];

						pModalPsdJointInput[ jj ] = pModalPsd[ jj ] * tfToConvertToPhase[optThNdx];
#else
						optComplex tmpC;
						#if ( RECONOPT_TEST != 0 )
							cofZ[optThNdx][0] = TEST_GAIN * cofZDenom_Inv[ jj ][0];
							cofZ[optThNdx][1] = TEST_GAIN * cofZDenom_Inv[ jj ][1];
						#else	
							if( (hrt_AoTwtLoopStat != eAOLoopClosed) || (hrt_AoWfrLoopStat != eAOLoopClosed) )
							{
//XXXdwp100808							 	cofZ[optThNdx][0] = cofZDenom_Inv[ jj ][0];
//XXXdwp100808							 	cofZ[optThNdx][1] = cofZDenom_Inv[ jj ][1];
							 	cofZ[optThNdx][0] = TEST_GAIN * cofZDenom_Inv[ jj ][0];  //XXXdwp100808
							 	cofZ[optThNdx][1] = TEST_GAIN * cofZDenom_Inv[ jj ][1];  //XXXdwp100808
							}
							else
							{
							 	cofZ[optThNdx][0] = hrt_workingModalGains[ modeNdx ] * cofZDenom_Inv[ jj ][0];
							 	cofZ[optThNdx][1] = hrt_workingModalGains[ modeNdx ] * cofZDenom_Inv[ jj ][1];
							}
						#endif
						optCmulf(delayTerm[ jj ], cofZ[optThNdx], tmpC);  // tmpC = delayTerm[ jj ] * cofZ[optThNdx]
						tmpC[0] += 1.0;							// tmpC = 1.0 + delayTerm[ jj ] * cofZ[optThNdx]
						optCabsf(tmpC, &tfToConvertToPhase[optThNdx]);	// tfToConvertToPhase[optThNdx] = cabsf( 1.0 + delayTerm[ jj ] * cofZ[optThNdx]);
						tfToConvertToPhase[optThNdx] *= tfToConvertToPhase[optThNdx];
						pModalPsdJointInput[ jj ] = pModalPsd[ jj ] * tfToConvertToPhase[optThNdx];
#endif
					}
				}
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PostProcess_2:
			{
				long long	frameNdx;
			 	int	currMode;

				float minG, deltaG;
				if(( LegalFrameRates [hrt_camFrameRate] ) == 1500)  //XXX only slightly better than hard-coded
					minG = GAIN_1PT5K_FPS / NUM_GAIN_STEPS;		    //XXX only slightly better than hard-coded
				else												//XXX only slightly better than hard-coded
					minG = GAIN_OTHER_FPS / NUM_GAIN_STEPS;			//XXX only slightly better than hard-coded
				deltaG = minG;

				#if ( RECONOPT_DEBUG != 0 )
					{
						double dSum = 0.0;
						for( jj=0; jj<PER_LEN; jj++)
#if ( STANDALONE_COMPLEX == 0 )
							dSum += delayTerm[jj];
#else
							dSum += delayTerm[jj][0];
#endif
						HRT_PRINTF( "INFO (gpAoHrtReconOpt_FTRAlgoProcess:1406): delayTerm SUM = %9.5f ", dSum);
					}
				#endif

				// Minimize control law over range of leaky-integrator gains to find optimal new gains
				{
					float gains[ NUM_GAIN_STEPS ];

					for( ii = 0; ii < NUM_GAIN_STEPS; ii++)
					{
						gains[ ii ] = ii * deltaG + minG;		// put gain steps into NUM_GAIN_STEPS element array to test section min finder

						for( jj=0; jj<PER_LEN; jj++)		// do all frames in periodogram length
						{ 
							optComplex tmpC;

							frameNdx = jj;		// frameNdx will be used after threading is done
#if ( STANDALONE_COMPLEX == 0 )
							cofZ[optThNdx] = gains[ ii ] * cofZDenom_Inv[ frameNdx ];
							tfToConvertToPhase[optThNdx] = cabsf( 1.0 / (1.0 + delayTerm[ frameNdx ] * cofZ[optThNdx]));	// [ XXX chk divide by zero ?? ]
							tfToConvertToPhase2[optThNdx][ii][frameNdx] = tfToConvertToPhase[optThNdx] * tfToConvertToPhase[optThNdx];
#else
							cofZ[optThNdx][0] = gains[ ii ] * cofZDenom_Inv[ frameNdx ][0];
							cofZ[optThNdx][1] = gains[ ii ] * cofZDenom_Inv[ frameNdx ][1];

							optCmulf(delayTerm[ frameNdx ], cofZ[optThNdx], tmpC);  // tmpC = delayTerm[ frameNdx ] * cofZ[optThNdx]
							tmpC[0] += 1.0;											// tmpC = 1.0 + delayTerm[ frameNdx ] * cofZ[optThNdx]
							optCdivf(oneC, tmpC, tmpC);								// tmpC = 1.0 / (1.0 + delayTerm[ frameNdx ] * cofZ[optThNdx])
							optCabsf(tmpC, &tfToConvertToPhase[optThNdx]);			// tfToConvertToPhase[optThNdx] = cabsf( 1.0 / ( 1.0 + delayTerm[ frameNdx ] * cofZ[optThNdx]))

							tfToConvertToPhase2[optThNdx][ii][frameNdx] = tfToConvertToPhase[optThNdx] * tfToConvertToPhase[optThNdx];
#endif
						}
					}

					#if ( RECONOPT_DEBUG != 0 )
						if( optThNdx == 0 )
						{
							HRT_PRINTF( "\n (Ndx=%d) Corner of the tfToConvertToPhase2 matrix ", thNdx);
							dumpCornerOfMatrix( (void*)(&tfToConvertToPhase2[optThNdx][0][0]), PER_LEN, 100, TFLOAT, false );
						}
					#endif

					// ??	- Entire data set??? i.e., 8192 frames???
					currMode = MAX_MODES/OPT_NUM_THREADS * optThNdx;
					for( ii=0; ii<MAX_MODES/OPT_NUM_THREADS; ii++, currMode++)
					{
						int minErrorNdx = 0;

						int gainNdxCenter = ( NUM_GAIN_STEPS + 1) / 2 - 1;
						int splitter	  = ( NUM_GAIN_STEPS + 1) / 4;
						int gainNdxLeft   = gainNdxCenter - splitter;
						int gainNdxRight  = gainNdxCenter + splitter;

						float errorPowerLeft, errorPowerCenter, errorPowerRight;
                        
						#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn on - KL_VALID handling
							if( !kl_valid1[ currMode ] )
							{
								optGainWorking[ (int)(currMode / GPI_PUP_DIA) ][ currMode % GPI_PUP_DIA ] = 0.0;
								continue;
							}
						#endif

						splitter = splitter / 2;
						errorPowerCenter = 0.0;

						pModalPsdJointInput = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsdsJointInputs[ currMode ][ 0 ]);
						for( jj=0; jj<PER_LEN; jj++)
							errorPowerCenter += tfToConvertToPhase2[optThNdx][gainNdxCenter][jj] * pModalPsdJointInput[ jj ];
                        
						while ( splitter >= 0)
						{
							errorPowerLeft = errorPowerRight = 0.0;
                        
							pModalPsdJointInput = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsdsJointInputs[ currMode ][ 0 ]);
							for( jj=0; jj<PER_LEN; jj++)
							{
							 	errorPowerLeft += tfToConvertToPhase2[optThNdx][gainNdxLeft][jj] * pModalPsdJointInput[ jj ];
								errorPowerRight += tfToConvertToPhase2[optThNdx][gainNdxRight][jj] * pModalPsdJointInput[ jj ];
							}
                        
							if( ( errorPowerLeft<errorPowerCenter) && ( errorPowerLeft<errorPowerRight))
							{
								gainNdxCenter = gainNdxLeft;
								errorPowerCenter = errorPowerLeft;
								gainNdxRight = gainNdxLeft + splitter;
								gainNdxLeft = gainNdxLeft - splitter;
							}
							else if( ( errorPowerCenter<errorPowerLeft) && ( errorPowerCenter<errorPowerRight))
							{
								gainNdxLeft = gainNdxCenter - splitter;
								gainNdxRight = gainNdxCenter + splitter;
							}
							else if( ( errorPowerRight<errorPowerLeft) && ( errorPowerRight<errorPowerCenter))
							{
							 	gainNdxLeft = gainNdxRight - splitter;
								gainNdxCenter = gainNdxRight;
								errorPowerCenter = errorPowerRight;
								gainNdxRight = gainNdxRight + splitter;
							}
							else					// at least 2 of the values are equal
								splitter = 0;		// > so, force the center index to be used
                        
							if( splitter > 1 )
								splitter /= 2;
							else if( splitter > 0 )
								splitter = 0;
							else
							{
								minErrorNdx = gainNdxCenter;
								splitter = -1;
							}
						}	// end while ( splitter>=0)

						optGainWorking[ (int)(currMode / GPI_PUP_DIA) ][ currMode % GPI_PUP_DIA ] = gains[ minErrorNdx ];
					}

					#if ( RECONOPT_DEBUG != 0 )
						{
							HRT_PRINTF( "\n (Ndx=%d) Corner of the optGainWorking before hermitian ", thNdx);
							dumpCornerOfMatrix( (void*)(&optGainWorking[0][0]), GPI_PUP_DIA, GPI_PUP_DIA/2, TFLOAT, false );

							HRT_PRINTF( "\n (Ndx=%d) Corner of the optGainWorking (@row:GPI_PUP_DIA/2) before hermitian ", thNdx);
							dumpCornerOfMatrix( (void*)(&optGainWorking[GPI_PUP_DIA/2][0]), GPI_PUP_DIA, GPI_PUP_DIA/2, TFLOAT, false );

							HRT_PRINTF( "\n (Ndx=%d) Corner of convertMap before hermitian ", thNdx);
							for( ii=0; ii<30; ii++ )
								HRT_PRINTF( "\n %6d,  %6d,  %6d ", convertMap[ii][0] , convertMap[ii][1], convertMap[ii][2] );
						}
					#endif
				}
			}
			return CmdProcessed;

		case eReconAlgFTRTh_PostProcess_3:
			{
				if( optThNdx != 0 )
					return CmdProcessed;
	
				// This is only place 'optFillToHermitian' is used
				optFillToHermitian( 	convertMap, 				// map 
										&optGainWorking[ 0 ][ 0 ],	// input matrix
										&optGains[ 0 ][ 0 ],		// out Matrix
										GPI_PUP_DIA);				// cnt for one side of sq. matrix

				// This section prints out stuff calculated in previous post process.  This is done so that all threads have completed.		
				#if ( RECONOPT_DEBUG_DUMP != 0 )
					{
						long	dimSize[4];

						char	fname[1024];

						float*	cl_psds;
						float*	ol_psds;

						HRT_PRINTF( "\n(reconOpt) PostProcess: #%d", thNdx );
						HRT_PRINTF( "\n            optThrNdx: #%d", optThNdx );

						cl_psds = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsds[ 0 ][ 0 ]);
						ol_psds = &(hrtReconDataPtr->psdBuffs[ hrtReconDataPtr->psdWriteNdx ].modalPsdsJointInputs[ 0 ][ 0 ]);

						HRT_PRINTF( "\n (Ndx=%d) Corner of the modalPsds matrix ", thNdx);
						dumpCornerOfMatrix( (void*)cl_psds, PER_LEN, MAX_MODES, TFLOAT, true );

						HRT_PRINTF( "\n (Ndx=%d) Corner of the modalPsdsJointInputs matrix ", thNdx);
						dumpCornerOfMatrix( (void*)ol_psds, PER_LEN, MAX_MODES, TFLOAT, true );

						dimSize[0] = PER_LEN;
						dimSize[1] = MAX_MODES;

						sprintf( fname, "%soptdata/optData_%ld/cl_psds.fits", dataDir, OptSequenceNumber);
						writeCompleteFitsFile( fname, dimSize, 2, TFLOAT, (void *)cl_psds);

						sprintf( fname, "%soptdata/optData_%ld/ol_psds.fits", dataDir, OptSequenceNumber);
						writeCompleteFitsFile( fname, dimSize, 2, TFLOAT, (void *)ol_psds);

						HRT_PRINTF( "\n hannTotWinSquared = %9.5f\n", hannTotWinSquared );
					}
				#endif

				#if ( RECONOPT_DEBUG != 0 )
					{
						HRT_PRINTF( "\n (Ndx=%d) Corner of the optGains after hermitian ", thNdx);
						dumpCornerOfMatrix( (void*)(&optGains[0][0]), GPI_PUP_DIA, GPI_PUP_DIA/2, TFLOAT, false );

						HRT_PRINTF( "\n (Ndx=%d) Corner of the optGains (@row:GPI_PUP_DIA/2) after hermitian ", thNdx);
						dumpCornerOfMatrix( (void*)(&optGains[GPI_PUP_DIA/2][0]), GPI_PUP_DIA, GPI_PUP_DIA/2, TFLOAT, false );
					}
				#endif

				#if ( RECONOPT_DEBUG_DUMP != 0 )
					{
						char	fname[1024];
						long	dimSize[4];

						sprintf( fname, "%soptdata/optData_%ld/gains.fits", dataDir, OptSequenceNumber );
						dimSize[0] = dimSize[1] = GPI_PUP_DIA;
						writeCompleteFitsFile( fname, dimSize, 2, TFLOAT, (void *)(&optGains[0][0]) );

						// This is duplicated below - but is handy even if dumping files
						HRT_PRINTF( "\n (Ndx=%d) Corner of the optGains after shift/write ", thNdx);
						dumpCornerOfMatrix( (void*)(&optGains[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
					}
				#endif

				#if ( RECONOPT_DEBUG != 0 )
					{
						HRT_PRINTF( "\n (Ndx=%d) Corner of the optGains after shift/write ", thNdx);
						dumpCornerOfMatrix( (void*)(&optGains[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
					}
				#endif

				hrtReconDataPtr->psdWriteNdx = (hrtReconDataPtr->psdWriteNdx + 1) % NUM_HRT_PSD_BUFFS;

				#if ( RECONOPT_DEBUG != 0 )
					writeCompleteFitsFileSquare( "optProtoResult.fits", GPI_PUP_DIA, TFLOAT, optGains);
				#endif

				if( ( hrt_optimizer_OFC_Stat == eAOLoopClosed ) || ( hrt_optimizer_OFC_Stat == eAOLoopClosing ) )
					// If turned on - allow the OFC gains to be moved to the OFC storage location else use working gains
				{
					hrt_optimizer_OFC_Stat = eAOLoopClosed;
					// Store new gains in the hrt_optModalgain area
					for( jj=0; jj<GPI_PUP_DIA; jj++)		// do the rows assigned to this thread
						for( ii=0; ii<GPI_PUP_DIA; ii++)
							pReconOptGains[ ii + jj * GPI_PUP_DIA] = optGains[ ii ][ jj ];
				}
				else
				{	
					// If optimizer is not on, simply move working values to optimizer gains
					for( jj=0; jj<GPI_PUP_DIA; jj++)		// do the rows assigned to this thread
						for( ii=0; ii<GPI_PUP_DIA; ii++)
							pReconOptGains[ ii + jj * GPI_PUP_DIA] = hrt_workingModalGains[  ii + jj * GPI_PUP_DIA ];
				}
			}
			return CmdProcessed;	// end of case eReconAlgFTRTh_OptGains:
	}	// end of switch( thrCmd)
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoInit( void ) 
 *
 *	@brief
 *	Initialize variables used by the FTR Reconstruction Optimization algorithm.
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to initialize variables used by the FTR 
 *	Reconstruction Optimization algorithm including:
 *
 *	1.	Initialize gain storage initial values and pointers, 
 *		Setup #of rows that foreach Opt thread to handle, and 
 *		Create and kick-off Opt threads, 
 *	2.	Setup aperture foractual Gemini pupil forIDL testing ( calculate numOnesInPinGrid )
 *	3.	Create conversion map, 
 *	4.	Allocate fft memory for periodogram data and plan, 
 *	5.	Setup frequencies & continuous-time functions forperiodograms ( based on frame rate), 
 *	6.	Initialize previous modal gains currentModalGains[ ] to 0.3 ( Done in the fileData_init routine)
 *	7.	for the RECONOPT_TEST, load in the entire Fourier modes, i.e., OPT_TOTAL_LEN ( 8192 frames)
 *		from FITS files "fouriermodes_for_dave_real.fits" & "fouriermodes_for_dave_imag.fits", 
 *	8.	?Calcultes leaky-integrator coefficients
 *	x.	Initialize ReconOpt threading ( )
 *	@par
 *
 *	@b Usage: gpAoHrtReconOpt_FTRAlgoInit( );
 * 
 *	\@param[ in ] None
 *
 *	@return None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
// these definitions were in gpAoHrtReconOpt_FTRAlgoInit, but were crashing the stack in the kernel (for RTLinux)
static float			tmpSeries[MAX_MODES];				  // tmp series simply to use optConvertToComplex( ) to get kl_valid[ ][ ]
static fft_complex_type tmpMatrix[GPI_PUP_DIA][GPI_PUP_DIA];  // tmp matrix used by optConvertToComplex( ) to get kl_valid[ ][ ]

void gpAoHrtReconOpt_FTRAlgoInit( void )
{
	short	ii, jj, in, jn;

	ReconOpt_State = eReconOpt_State_Start;

//**1. Initializes ping/pong gain storage to initial values
	for( jj=0; jj<GPI_PUP_DIA; jj++ )
	{
		for( ii=0; ii<GPI_PUP_DIA; ii++ )
			subapMask[ jj + 1 ][ ii + 1 ] = hrtFileDataShmPtr->aperturePattern[ jj ][ ii ];
	}

	for (in=0; in<GPI_PUP_DIA; in++)
	{
		subapMask[ 0 ][ in ] = subapMask[ GPI_PUP_DIA ][ in ];  // 'shift' down
		subapMask[ in ][ 0 ] = subapMask[ in ][ GPI_PUP_DIA ];  // > and right
	}

	numOnesInPinGrid = 0;  // this is used below
	for( in=0; in<GPI_PUP_DIA; in++)	  // if any pixel in a 2x2 area including the current 
	{									  // > pixel and those to the left and below it are set, 
		for( jn=0; jn<GPI_PUP_DIA; jn++)  // > use the pixel
		{
			if ((subapMask[ in ][ jn ] + subapMask[ in + 1 ][ jn ] + subapMask[ in ][ jn + 1 ] + subapMask[ in + 1 ][ jn + 1 ]) < 1.0)
				pinGrid[ in ][ jn ] = 0;
			else
			{
				pinGrid[ in ][ jn ] = 1;
				numOnesInPinGrid++;
			}
		}
	}
	SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_FTRAlgoInit) numOnesInPinGrid = ", numOnesInPinGrid);

/***3. Create conversion map ( per frame) - moved from optimizeProto.c
 	****************************************************************************************************************************
	This section of code performs a one time creation for a map and saves the result in ( int) convertMap[ MAX_MODES ][ 3 ] for
	later use.
	****************************************************************************************************************************
*/
	{
		int	ii;	
		#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn On
			int	kk, ll;	
		#endif

		optMakeMap( convertMap, MAX_MODES, GPI_PUP_DIA);	// get a conversion map from optMakeMap( ) in convertMap

		for( ii = 0; ii < MAX_MODES; ii++)
			tmpSeries[ ii ] = 1.0;

		optConvertToComplex( convertMap, tmpSeries, &tmpMatrix[ 0 ][ 0 ], MAX_MODES, GPI_PUP_DIA);

		num_kl_valid = 0;

		#if (  RECONOPT_TURN_OFF_KL_VALID == 0 )		// Turn On
			for( ii=0; ii<MAX_MODES; ii++)
				kl_valid1[ ii ] = false;

			for( kk=0; kk<GPI_PUP_DIA; kk++)
			{
			 	for( ll=0; ll<GPI_PUP_DIA; ll++)
				{
					if( ( tmpMatrix[ kk ][ ll ][ 0 ] == 0.0) && ( tmpMatrix[ kk ][ ll ][ 1 ] == 0.0))  // kl_valid[ ][ ]=0 if Real & Img both zero
					{
						kl_valid[ kk ][ ll ] = false;
					}
					else if( ( kk==0 && ll==0) || ( ( kk==GPI_PUP_DIA/2) && ( ll==GPI_PUP_DIA/2)))  // for kl_valid[ 0 ][ 0 ] and
					{
						//	 k1_valid[ GPI_PUP_DIA/2 ][ GPI_PUP_DIA/2 ]
						kl_valid[ kk ][ ll ] = false;	// piston ( i.e., [ 0 ][ 0 ]) & waffle ( i.e., [ GPI_PUP_DIA/2 ][ GPI_PUP_DIA/2 ]) are not valid? 
					}
					else						// forall other valid modes 
					{
						kl_valid[ kk ][ ll ] = true;									// This may or may not be useful later...
						num_kl_valid++;
					}
				}
			}
        
			// Move results into a single dimensional array for easier access below.
			for( ii=0; ii<MAX_MODES; ii++)
				if( kl_valid[ (int)(ii / GPI_PUP_DIA) ][ (int)(ii % GPI_PUP_DIA) ] == true )
					kl_valid1[ ii ] = true;
		#else
			for( ii=0; ii<MAX_MODES; ii++)
			{
				kl_valid1[ ii ] = true;
				kl_valid[ 0 ][ ii ] = true;
			}
			num_kl_valid = MAX_MODES;
		#endif

		// Handle case where num_kl_valid is odd ( may not be necessary. )
		if( num_kl_valid % 2 == 0 )
			num_kl_per_thread = num_kl_valid / OPT_NUM_THREADS;
		else
			num_kl_per_thread = (num_kl_valid+1) / OPT_NUM_THREADS;
		SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_FTRAlgoInit) NUM_KL_VALID = ", num_kl_valid );

		// pre-calculates the windowing values for the hanning, hamming, and blackman windows used by prior to fft.	 
		optCalcWinVals( );		// one time calculation

	}	// ************************ Done with conversion map creation

//4.Allocate fft memory forperiodogram data and plan - the task is done in gpAoHrtReconOpt_FTRAlgoInitMemory( ), 
//	which is called by gpAoHrtRecon_InitMemory( ). Therefore, no need to allocate the fft memory again.
//	****************************************************************************************************************************
//	The results are saved in perData[ ] ( a ptr to the allocated fft_complex_type memory block) and perPlan forlater use. Note that the
//	number of modes that will actually be processed is 'num_kl_valid[ GPI_PUP_DIA ][ GPI_PUP_DIA ]' as determined in the section
//	of code above
//	****************************************************************************************************************************

//5. Setup the frequencies for the periodograms and determine the continuous-time xfr functions - ( moved from
//		 optimizeProto.c as a new function). This may need to be done per each periodogram in real life and/or need done when
//		 camFR_forOptimizer is changed...
//	****************************************************************************************************************************
//	This function setups frequencies corresponding to the periodograms and determines the continuous-time transforfunctions
//	to be used fordetermining open loop response & inverting control loop processes. After this call, and the initialization
//	of previous modal gains, it is necessary to call optCalcLeakyCoefs( ) to calculate leaky-inegrator coef forconverting
//	closed-loop measurement PSDs to open-loop measurement PSDs. 	
//	****************************************************************************************************************************

	if( optSetPerFreqAndCtXfrFunc( ) )	// ensure processing the current camera rate 
	{ 
	}	// ************************ Done with setup frequencies & continuous-time xfr functions

//6. Initialize modal gains, "hrt_optModalGains[ ii ]", to 0.3 (currently done in the init routine : gpAoHrtFileData_Init)
//

//7. forRECON_TEST, load entire OPT_TOTAL_LEN ( i.e., 8192 frames) of temporal data from FITS files from optimizeProto.c
// 	****************************************************************************************************************************
//	This section of code loads the entire OPT_TOTAL_LEN from two FITS files "fouriermodes_for_dave_real.fits" and
//	"fouriermodes_for_dave_imag.fits" into 
//	testDataR[ MAX_MODES ][ OPT_TOTAL_LEN ] and testDataI[ MAX_MODES ][ OPT_TOTAL_LEN ]
//	****************************************************************************************************************************

	#if ( RECONOPT_TEST != 0 )
	{
		static	float	testDataR_Temp[ OPT_TOTAL_LEN ][ GPI_PUP_DIA ][ GPI_PUP_DIA ];	// indices swapped because IDL
		static	float	testDataI_Temp[ OPT_TOTAL_LEN ][ GPI_PUP_DIA ][ GPI_PUP_DIA ];	// indices swapped because IDL

		int ii, jj, kk;

		HRT_PRINTF_MIN( "INFO (gpAoHrtReconOpt_FTRAlgoInit): Attempt to read the 'RECONOPT_TEST' data input files\n");
		optReadFitsSquare( "data/config/optTestData_real.fits", GPI_PUP_DIA, OPT_TOTAL_LEN, TFLOAT, &testDataR_Temp[ 0 ][ 0 ][ 0 ] );
		optReadFitsSquare( "data/config/optTestData_imag.fits", GPI_PUP_DIA, OPT_TOTAL_LEN, TFLOAT, &testDataI_Temp[ 0 ][ 0 ][ 0 ] );

		for( ii=0; ii<GPI_PUP_DIA; ii++ )
			for( jj=0; jj<GPI_PUP_DIA; jj++ )
				for( kk = 0; kk < OPT_TOTAL_LEN; kk++ )
				{
					testDataR[ ii * GPI_PUP_DIA + jj ][ kk ] = testDataR_Temp[ kk ][ jj ][ ii ];	// straighten out IDL data
					testDataI[ ii * GPI_PUP_DIA + jj ][ kk ] = testDataI_Temp[ kk ][ jj ][ ii ];	// straighten out IDL data
				}

		#if ( RECONOPT_DEBUG != 0 )
			HRT_PRINTF( "\n Corner of the input Data matrix (real) ");
			dumpCornerOfMatrix( (void*)(&testDataR[0][0]), OPT_TOTAL_LEN, MAX_MODES, TFLOAT, false );
	
			HRT_PRINTF( "\n Corner of the input Data matrix (img) ");
			dumpCornerOfMatrix( (void*)(&testDataI[0][0]), OPT_TOTAL_LEN, MAX_MODES, TFLOAT, false );
		#endif

		// clear out the circular buffer
		for( currAvgBufNdx=0; currAvgBufNdx<OPT_AVG_LEN; currAvgBufNdx++ )
		{
			for( ii=0; ii<MAX_MODES; ii++ )
			{
				currAvgBuffer[ currAvgBufNdx ][ 0 ][ ii ] = 0.0f;
				currAvgBuffer[ currAvgBufNdx ][ 1 ][ ii ] = 0.0f;
			}
		}
		currAvgBufNdx = 0;

		#if ( RECONOPT_DO_BLOCK_AVG == 0 )
			// zero out the running average
			HRT_PRINTF( "\n Zero the currAvg arrays (real/image)\n");
			for( ii=0; ii<MAX_MODES; ii++)
			{
				currAvg[ 0 ][ ii ] = currAvg[ 1 ][ ii ] = 0.0f;
				currAvgSnapShotA[ 0 ][ ii ] = currAvgSnapShotA[ 1 ][ ii ] = 0.0f;
				currAvgSnapShotB[ 0 ][ ii ] = currAvgSnapShotB[ 1 ][ ii ] = 0.0f;
			}
		#else
			// precalculate the total average to use as running average for test
			for( jj=0; jj<MAX_MODES; jj++)
			{
				currAvgSnapShotA[0][jj] = currAvgSnapShotA[1][jj] = 0.0f;
				for( ii=0; ii<OPT_TOTAL_LEN; ii++ )
				{
					currAvgSnapShotA[0][jj] += testDataR[jj][ii];
					currAvgSnapShotA[1][jj] += testDataI[jj][ii];
				}
				currAvgSnapShotA[0][jj] *= OPT_TOTAL_LEN_INVERSE;
				currAvgSnapShotA[1][jj] *= OPT_TOTAL_LEN_INVERSE;
				currAvgSnapShotB[0][jj] = currAvgSnapShotA[0][jj];
				currAvgSnapShotB[1][jj] = currAvgSnapShotA[1][jj];
			}
		#endif

		#if ( RECONOPT_DEBUG != 0 )
			HRT_PRINTF( "\n Corner of the Average(R) matrix");
			dumpCornerOfMatrix( (void*)(&currAvg[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
	
			HRT_PRINTF( "\n Corner of the Average(I) matrix");
			dumpCornerOfMatrix( (void*)(&currAvg[1][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );

			HRT_PRINTF( "\n Corner of the Average A (R) matrix");
			dumpCornerOfMatrix( (void*)(&currAvgSnapShotA[0][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
	
			HRT_PRINTF( "\n Corner of the Average A (I) matrix");
			dumpCornerOfMatrix( (void*)(&currAvgSnapShotA[1][0]), GPI_PUP_DIA, GPI_PUP_DIA, TFLOAT, false );
		#endif
	}	// ************************ Done with load test temporal data for RECONOPT_TEST
	#endif

//8. Calculates leaky-integrator coefficients ( per periodogram, wait until one periodogram is ready?))
// 	****************************************************************************************************************************
//	The optCalculLeakyCoefs( ) function calculates leaky-integrator coefficients corresponding to the 'previous' modal
//	gains, determines corresponding transfer function forconverting closed-loop measrement PSDs to open-loop reasurement PSDs.
//	It is necessary to call this function to generate the open-loop PSDs based on the calculated factors from
//	optSetPerFreqAndCtXfrFunc( ) and the 'previous' modal gain currentModalGains[ ]...
// 	****************************************************************************************************************************
//	{ optCalcLeakyCoefs( );				// per PER_LEN
//	}	// ************************ Done with Calculate Leaky-integrator coefficients & onvert Closed-loop to Open-loop PSDs 

#if ( STANDALONE_FFT == 0 )
	#if ( RECONOPT_DEBUG != 0 )
		HRT_PRINTF_MIN( "INFO (gpAoHrtReconOpt_FTRAlgoInit:1988): Attempting to make ReconOpt Plans for fftw");
	#endif
	gpAoHrtReconOpt_FTRAlgoInitFftPlans( );
#endif
}	// end of gpAoHrtReconOpt_FTRAlgoInit( )


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoInitMemory( void) - called by gpAoHrtRecon_InitMemory( )
 *
 *	@brief
 *	Allocate memory used by the FTR Reconstruction Optimization algorithm. Specifically, the fft memory for
 *	periodogram data and plan.
 *
 *	@b DESCRIPTION:
 *	This function allocates fftw memory forperiodogram data and plan. The results are saved in perData ( a ptr to the
 *	allocated fft_complex_type memory block) and perPlan forlater use. Note that the number of modes that will actually
 *	be processed is 'num_kl_valid[ GPI_PUP_DIA ][ GPI_PUP_DIA ]', which is determined in the code section of conversion map
 *	creation.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtReconOpt_FTRAlgoInitMemory( void);
 * 
 *	\@param[ in ]	PER_LEN ( define):	periodogram length ( defined in gpAoHrtReconOpt.h)
 *				GPI_PUP_DIA( define):diameter of extended pupil in GPI pixels ( defined in gpAoHrtReconOpt.h)
 *				sizeof( fft_complex_type) 
 *
 *	@return:perData points to the allocated fftw memory block
 *			perPlan memory forPlan			
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtReconOpt_FTRAlgoInitMemory( void)
{
#if ( STANDALONE_FFT == 0 )
	perData_A = (fft_complex_type *)fftwf_malloc(MAX_MODES * PER_LEN * sizeof( fft_complex_type));
	perData_B = (fft_complex_type *)fftwf_malloc(MAX_MODES * PER_LEN * sizeof( fft_complex_type));
#endif
}	//	end of gpAoHrtReconOpt_FTRAlgoInitMemory 


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoInitFftPlans( void) - called by gpAoHrtRecon_Init( )
 *
 *	@brief
 *		Construct plans used by the FTR Reconstruction Optimization algorithm. Specifically, the fftw periodogram plans.
 *
 *	@b DESCRIPTION:
 *	This function allocates fftw  plans for the optimizer. The results are saved in perPlan_A and perPlan_B for later use.
 *	Note that the number of modes that will actually be processed is 'num_kl_valid[ MAX_MODES ]', which is determined in 
 *	the code section of conversion map creation.
 *	@par
 *	
 *
 *	@b Usage: gpAoHrtReconOpt_FTRAlgoInitFftPlans( void);
 * 
 *	\@param[ in ]	PER_LEN ( define):	periodogram length ( defined in gpAoHrtReconOpt.h)
 *				GPI_PUP_DIA( define):diameter of extended pupil in GPI pixels ( defined in gpAoHrtReconOpt.h)
 *				sizeof( fft_complex_type) 
 *
 *	@return: perPlan memory for Plan
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtReconOpt_FTRAlgoInitFftPlans( void)
{
#if ( STANDALONE_FFT == 0 )
	int perLenInt = PER_LEN;
	int	ii = 0;

	if( num_kl_per_thread <=0 )
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (reconOpt:InitMemory) num_kl_per_thread must be > 0, it = ", num_kl_per_thread);

	// allocated on per thread basis
	if (rtl_sem_wait(&FftwPlanSem) < 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR waiting on FftwPlanSem, errno = ", rtl_errno);

	for( ii=0; ii<OPT_NUM_THREADS; ii++)
	{
		perPlan_A[ ii ] = fftwf_plan_many_dft( 1,									// int rank
						&perLenInt,													// const int *n
						num_kl_per_thread,											// int howmany

						&perData_A[ num_kl_per_thread * ii * PER_LEN ],				// fft_complex_type *in
						NULL,														// const int *inembed
						1,															// int istride
						PER_LEN,													// int idist

						&perData_A[ num_kl_per_thread * ii * PER_LEN ],				// fft_complex_type *out
						NULL,														// const int *onembed
						1,															// int ostride
						PER_LEN,													// int odist

						FFTW_FORWARD,												// int sign 
						FFTW_PATIENT);												// unsigned flags

		if( perPlan_A[ ii ] == NULL )
			SysConSendStrngIntParm(eDebugLevelErr, "INFO (gpAoHrtReconOpt_FTRAlgoInitFftPlans) FFTW planning FAILED, PER_PLAN_A @ ii = ", ii );

		perPlan_B[ ii ] = fftwf_plan_many_dft( 1,									// int rank
						&perLenInt,													// const int *n
						num_kl_per_thread,											// int howmany

						&perData_B[ num_kl_per_thread * ii * PER_LEN ],				// fft_complex_type *in
						NULL,														// const int *inembed
						1,															// int istride
						PER_LEN,													// int idist

						&perData_B[ num_kl_per_thread * ii * PER_LEN ],				// fft_complex_type *out
						NULL,														// const int *onembed
						1,															// int ostride
						PER_LEN,													// int odist
	
						FFTW_FORWARD,												// int sign 
						FFTW_PATIENT);												// unsigned flags

		if(	perPlan_B[ ii ] == NULL )
			SysConSendStrngIntParm(eDebugLevelErr, "INFO (gpAoHrtReconOpt_FTRAlgoInitFftPlans) FFTW planning FAILED, PER_PLAN_B @ ii = ", ii );
	}
	rtl_sem_post(&FftwPlanSem);
	SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtReconOpt_FTRAlgoInitFftPlans) all opt FFTW plans done");
#endif
}	//	end of gpAoHrtReconOpt_FTRAlgoInitFftPlans 


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoCleanMemory( void) - called by gpAoHrtRecon_CleanMemory( )
 *
 *	@brief
 *	Clean up the allocated memory used by FTR Reconstruction Optimization. Specifically, the fftw memory
 *	allocated forhandling periodogram data and plan.
 *
 *	@b DESCRIPTION:
 *	The purpose of this function is to de-allocate FTR reconstruction optimization memory. Specifically the
 *	fftw memory block allocated forhandling periodogram data and plan 
 *
 *	@par
 *	
 *	@b Usage: gpAoHrtReconOpt_FTRAlgoCleanMemory( void);
 * 
 *	\@param[ in ]	perData points to the allocated fftw memory block
 *				perPlan memory forPlan
 *	\@param[ out ]memory blocks pointed to by perData and allocated forperPlan are released
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtReconOpt_FTRAlgoCleanMemory( void)
{
#if ( STANDALONE_FFT == 0 )
	int	ii;

	for( ii=0; ii<OPT_NUM_THREADS; ii++)
	{
		fftwf_destroy_plan( perPlan_A[ii] );
		fftwf_destroy_plan( perPlan_B[ii] );
	}
	
	fftwf_free( perData_A );
	fftwf_free( perData_B );
#endif
}


/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoStart( void )
 *
 *	@brief
 *	Start FTR Optimization
 *
 *	@b DESCRIPTION:
 *	The purpose of this is to start the optimization. It will re-start if already started
 *
 *	@par
 *	
 *	@b Usage: gpAoHrtReconOpt_FTRAlgoStart( void );
 * 
 *	\@param[ in ] None
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtReconOpt_FTRAlgoStart( void )
{
	ReconOpt_State = eReconOpt_State_Init;
}

/*!
 *******************************************************************************
 *
 *	@fn void gpAoHrtReconOpt_FTRAlgoStop( void )
 *
 *	@brief
 *	stop the FTR Optimization
 *
 *	@b DESCRIPTION:
 *	The purpose of this unit is to stop the FTR optimization
 *
 *	@par
 *	
 *	R@b Usage: gpAoHrtReconOpt_FTRAlgoStop( void );
 * 
 *	\@param[ in ] None
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
void gpAoHrtReconOpt_FTRAlgoStop( void )
{
	hrt_optimizer_OFC_Stat = eAOLoopOpening;
}


// *****************************************************************************
// code sections from here to the end are moved/modified from optimizeProto.c
/*!
 *******************************************************************************
 *	@fn static void optCalcWinVals ( void)
 *
 *	@brief
 *	Calculates windowing values for the hanning, hamming, and blackman windows.  
 *
 *	@b DESCRIPTION:
 * 	This function uses the pre-defined PER_LEN to calculate the windowing values for
 *	the hanning, hamming, and blackman windows.  
 *	@par
 *	
 *
 *	@b Usage: optCalcWinVals ( void)
 * 
 *	\@param[ in ]
 *		None
 *
 *	\@param[ out ]	to the following local globals:
 *				hammWinVals[ PER_LEN ] ( float):	hamming window values
 *				hannWinVals[ PER_LEN ] ( float):	hanning window values
 *				blackWinVals[ PER_LEN ]( float):	blackman window values
 *				hammTotWinSquared ( float):	sum of the squares of the hamming window values
 *				hannTotWinSquared ( float):	sum of the squares of the hanning window values
 *				blackTotWinSquared( float):	sum of the squares of the blcakman window values
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void optCalcWinVals ( void)
{
	int ii;

	hammTotWinSquared = 0.0;
	hannTotWinSquared = 0.0;
	blackTotWinSquared = 0.0;

	for( ii = 0; ii < PER_LEN; ii++)
	{
		hammWinVals[ ii ] = 0.54 - 0.46 * cos( 2.0 * PI * (double)ii / ( PER_LEN - 1));
		hammTotWinSquared += hammWinVals[ ii ] * hammWinVals[ ii ];

		hannWinVals[ ii ] = 0.5 - 0.5 * cos( 2.0 * PI * (double)ii / ( PER_LEN - 1));
		hannTotWinSquared += hannWinVals[ ii ] * hannWinVals[ ii ];

		blackWinVals[ ii ] = 0.42 - 0.5 * cos( 2.0 * PI * (double)ii / ( PER_LEN - 1)) + 0.08 * cos( 4.0 * PI * (double)ii / (PER_LEN - 1));
		blackTotWinSquared += blackWinVals[ ii ] * blackWinVals[ ii ];
	}
}	// end of optCalcWinVals( )


/*!
 *******************************************************************************
 *	@fn static void optConvertToComplex( int map[ ][ 3 ], float series[ ], fft_complex_type matrix[ ], int oneDLength, int twoDLength )
 *
 *	@brief
 *	Converts cosine & sine components to a square matrix of complex values.
 *
 *	@b DESCRIPTION:
 *		Converts cosine and sine components of 'series' to a square matrix of
 *		complex values. NOTE that this unit handles only the IDL code's "single
 *		time step" case ( i.e., 2D, not 3D), which may be all the Recon
 *		optimization requires for the time being.
 *	@par
 *
 *	@b Usage: optConvertToComplex( int map[ ][ 3 ], float series[ ], fft_complex_type matrix[ ], int oneDLength, int twoDLength);
 * 
 *	@param[ in ]	map			( int[][3]):	map is used to perform the conversion (map is the convertMap[ MAX_MODES ][ 3 ])
 *	@param[ in ]	series[ ]	( float):		array of cos and sin components to be converted to a square complex matrix
 *	@param[ in ]	matrix[ ]	( fft_complex_type):sqr matrix where complex data should go ( passed as a 1D array so as not to have to fix dim lngths)
 *	@param[ in ]	oneDLength	( int):			length of the series ( this should be twoDLength squared)
 *	@param[ in ]	twoDLength	( int):			length of the 2 dimensions of the square matrix
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void optConvertToComplex( int map[ ][ 3 ], float series[ ], fft_complex_type matrix[ ], int oneDLength, int twoDLength)
{
	int ii, t;
	int kk, ll, s;						// apologies forusing 'll', just following the IDL code

//	set the whole matrix to zeros
	for( ii = 0; ii < oneDLength; ii++)	// remember, oneDLength = twoDLength * twoDLength
	{
		matrix[ ii ][ 0 ] = 0.0;	// real part
		matrix[ ii ][ 1 ] = 0.0;	// imaginary part
	}

//	do the conversion
	for( t = 0; t < oneDLength; t++)
	{	
		kk = map[ t ][ 0 ];							// map is convertMap[ MAX_MODES ][ 3 ]
		ll = map[ t ][ 1 ];							// watch out for 'll' in the following
		s = map[ t ][ 2 ];

		if( s == 0)
			matrix[ kk * twoDLength + ll ][ 0 ] = series[ t ];	// real part ( img part already equals zero)
		else if( s == 1)
		{
			matrix[ kk * twoDLength + ll ][ 0 ] = series[ t ];		// real part
			matrix[ kk * twoDLength + ll ][ 1 ] = series[ t + 1 ];	// imaginary part
		}
	}
}	// end of optConvertToComplex( )


/*!
 *******************************************************************************
 *	@fn static void optFillToHermitian(  int hermitMap[ ][ 3 ], float planeIn[ ], float planeOut[ ], int twoDLength )
 *
 *	@brief
 *	Converts a half plane real or complex signal into the full hermitian plane
 *
 *	@b DESCRIPTION:
 *		This function converts a real or complex signal that's only a half-plane into the full
 *		hermitian plane. Note that at present it handles only the float case ( not the complex case)
 *		and that the float represents the real portion of a complex number, because that's all that
 *		the optimize code requires.
 *
 *		ALSO, at present, this unit 'outputs' a matrix different than the 'input' one.	Without too
 *		much thought, the matrix could probably be filled out in place.
 *	
 *	@par
 *
 *	@b Usage:	optFilltoHermitian( (int) hermitMap[ MAX_MODES ][ 3 ], (float*) planeIn, (float*) planeOut, (int) twoDLength)
 * 
 *	@param[ in ]	hermitMap	( int [ ][ 3 ] ):	map to fill out the hermitian matrix ( same as convertMap[ ][ ], forconvenience)		
 *	@param[ in ]	planeIn		( float[]):			the half-plane ( this is passed as a 1D array so as not to have to  fix dimension lengths)
 *	@param[ in ]	twoDLength	( int) :			the length of the 2 dimensions of the square matrix ( the plane)	
 *	@param[ out ]	planeOut[ ] ( float):			the full hermitian plane ( this is also passed as a 1D array so as not	to have to fix dimension lengths)
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static void optFillToHermitian( int hermitMap[ ][ 3 ], float planeIn[ ], float planeOut[ ], int twoDLength)
{
	int ii, t;
	int kk, ll, ss;	// apologies forusing 'll', just following the IDL code
	int oneDLength = twoDLength * twoDLength;

//	init the out-plane from the in-plane
	for( ii = 0; ii < oneDLength; ii++)
		planeOut[ ii ] = planeIn[ ii ];

	for( t = 0; t < oneDLength; t++)
	{
		kk = hermitMap[ t ][ 0 ];
		ll = hermitMap[ t ][ 1 ];	// watch out for'll' in the following
		ss = hermitMap[ t ][ 2 ];

		if( ss != -1)	// a minus ss indicates a location that will be filled in; so, don't process it directly
		{
			if( kk == 0)
			{
				if( ( ll != 0) && ( ll != ( twoDLength / 2)))	// do nothing forpiston: kk=0, ll=0;
																//	 or half-waffle: kk=0, ll=twoDLength/2
					planeOut[ twoDLength - ll ] = planeIn[ ll ];	// reflect the top row across the y axis
			}
			else if( ll == 0)
			{
				if( kk != ( twoDLength / 2))	// do nothing forhalf-waffle: kk=twoDLength/2, ll=0
					planeOut[ ( twoDLength - kk) * twoDLength ] = planeIn[ kk * twoDLength ];	// reflect the left
																						//	 column across x axis
			}
			else if( kk == ( twoDLength / 2))
			{
				if( ll != ( twoDLength / 2))	// do nothing forwaffle: kk=twoDLength/2, ll=twoDLength/2
					planeOut[ ( twoDLength / 2) * twoDLength + twoDLength - ll ] =			// reflect the center
						planeIn[ ( twoDLength / 2) * twoDLength + ll ];						//	 across y axis 
			}
			else if( ll == ( twoDLength / 2))
			{
				planeOut[ ( twoDLength - kk) * twoDLength + twoDLength / 2 ] =				// reflect the center
						planeIn[ kk * twoDLength + twoDLength / 2 ];						//	 across x axis	
			}
			else
				planeOut[ ( twoDLength - kk) * twoDLength + twoDLength - ll ] =				// reflect across diagonal
					planeIn[ kk * twoDLength + ll ];
		}
	}
}	// end of optFillToHermitian( )


/*!
 *******************************************************************************
 *	@fn static void optMakeMap( int map[ ][ 3 ], int oneDLength, int twoDLength )
 *
 *	@brief
 *	Returns a mapping to go from a 1D array to a square 2D matrix.
 *
 *	@b DESCRIPTION:
 *		This unit creates and returns a mapping to go from a 1D array to a square 2D matrix.
 *		It assumes that the data in the 1D array are interleaved cosine and sine components
 *		of complex values (as in Euler's identity).	It also assumes that the data in the 1D
 *		array are the lower triangle of the matrix, including the diagonal. Finally, the
 *		function takes invisible Shack-Hartmann modes into account when creating the mapping.
 *
 *		The 3 elements of each map[ x ][ 3 ] are:
 *		[ 0 ] is jj, [ 1 ] is kk: the x and y coordinates in the 2D square matrix
 *		[ 2 ] is s: 1 means cosine, -1 sine, 0 means cosine only ( no corresponding sine)
 *
 *		NOTE: The unit adds the bottom half of the second diagonal above the diagonal and removes
 *		the last half of left column and bottom row.	This is for Hermitian symmetry, which is
 *		easier to see by shifting the matrix 24, 24 ( although, the truth is, it doesn't seem quite
 *		right).
 *	@par
 *
 *	@b Usage:	optMakeMap( (int[][3]) map, (int) oneDLength, (int) twoDLength)
 * 
 *	@param[ in ]	map[ ][ 3 ] ( int):	an int array into wich mapping should be put
 *	@param[ in ]	oneDLength	( int):	length of the series ( this should be twoDLength squared)
 *	@param[ in ]	twoDLength	( int):	length of the 2 dimensions of the square matrix
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
// this definition was in gpAoHrtReconOpt_FTRAlgoInit, but was crashing the stack in the kernel (for RTLinux)
static int matrixTemp[GPI_PUP_DIA][GPI_PUP_DIA][xPix];  //IMPORTANT: for this to work, twoDLength must equal GPI_PUP_DIA

static void optMakeMap( int map[ ][ 3 ], int oneDLength, int twoDLength )
{
	int ii, jj, kk;
	int cntr, rowSum, off;

	for( ii=0; ii<GPI_PUP_DIA; ii++)
		for( jj=0; jj<GPI_PUP_DIA; jj++)
			for( kk=0; kk<xPix; kk++)
				matrixTemp[ ii ][ jj ][ kk ] = 0;

//	piston
	map[ 0 ][ 0 ] = 0;
	map[ 0 ][ 1 ] = 0;
	map[ 0 ][ 2 ] = 0;
	cntr = 1;
	matrixTemp[ 0 ][ 0 ][ 0 ] = 1;

//	regular modes
	for( rowSum = 1; rowSum < ( twoDLength / 2); rowSum++)
	{
		for( off = 0; off <= rowSum; off++)
		{
			kk = off;
			jj = rowSum - off;

			map[ cntr ][ 0 ] = jj;	// the cosine mode
			map[ cntr ][ 1 ] = kk;
			map[ cntr ][ 2 ] = 1;
			cntr++;

			map[ cntr ][ 0 ] = jj;	// the sine mode
			map[ cntr ][ 1 ] = kk;
			map[ cntr ][ 2 ] = -1;
			cntr++;

			matrixTemp[ jj ][ kk ][ 1 ] = 1;
		}
	}

//	half-waffle, y-freq = 0
	map[ cntr ][ 0 ] = twoDLength / 2;
	map[ cntr ][ 1 ] = 0;
	map[ cntr ][ 2 ] = 0;
	cntr++;
	matrixTemp[ twoDLength / 2 ][ 0 ][ 2 ] = 1;

//	more regular modes
	rowSum = twoDLength / 2;
	for( off = 1; off < rowSum; off++)
	{
		kk = off;
		jj = rowSum - off;
		
		map[ cntr ][ 0 ] = jj;	// the cosine mode
		map[ cntr ][ 1 ] = kk;
		map[ cntr ][ 2 ] = 1;
		cntr++;

		map[ cntr ][ 0 ] = jj;	// the sine mode
		map[ cntr ][ 1 ] = kk;
		map[ cntr ][ 2 ] = -1;
		cntr++;

		matrixTemp[jj][kk][3] = 1;
	}

//	half-waffle, y-freq = 0
	map[cntr][0] = 0;
	map[cntr][1] = twoDLength / 2;
	map[cntr][2] = 0;
	cntr++;
	matrixTemp[0][twoDLength / 2][4] = 1;

//	more regular modes
	for (rowSum = (twoDLength / 2 + 1); rowSum < twoDLength; rowSum++)
	{
	    for (off = 1; off < rowSum; off++)
		{
	        kk = off;
	        jj = rowSum - off;

	        map[cntr][0] = jj;  // the cosine mode
	        map[cntr][1] = kk;
	        map[cntr][2] = 1;
	        cntr++;

	        map[cntr][0] = jj;  // the sine mode
	        map[cntr][1] = kk;
	        map[cntr][2] = -1;
	        cntr++;

			matrixTemp[jj][kk][5] = 1;
		}
	}

//	more regular modes
	rowSum = twoDLength;
    for (off = 1; off < rowSum / 2; off++)
	{
        kk = off;
        jj = rowSum - off;

        map[cntr][0] = jj;  // the cosine mode
        map[cntr][1] = kk;
        map[cntr][2] = 1;
        cntr++;

        map[cntr][0] = jj;  // the sine mode
        map[cntr][1] = kk;
        map[cntr][2] = -1;
        cntr++;

		
		matrixTemp[ jj ][ kk ][ 6 ] = 1;
	}

	map[ cntr ][ 0 ] = twoDLength / 2;
	map[ cntr ][ 1 ] = twoDLength / 2;
	map[ cntr ][ 2 ] = 0;

	cntr++;

	matrixTemp[ twoDLength / 2 ][ twoDLength / 2 ][ 7 ] = 1;
}	// end of optMakeMap( )


/*!
 *******************************************************************************
 *	@fn	static boolean optSetPerFreqAndCtXfrFunc( void );
 *
 *	@brief
 *	Setup frequencies corresponding to periodogram and continuous-time transfer functions
 *
 *	@b DESCRIPTION:
 *		This function sets up the frequencies corresponding to the periodograms and the continuous-time transfer functions
 *		to be used in determining open loop response and inverting the control loop. It sets up the frequency and the 
 *		necessary variables automatically by taking the camera frame rate provided by camFR. It returns a "false" ( i.e., 0)
 *		without processing anything if the current camera rate hasn't been changed; otherwise, it performs the processes
 *		according to the new camera frame rate specified by camFR and returns a "true" ( i.e., 1). In addition to the camera
 *		frame rate, the process also depends on the length of the periodogram, PER_LEN, defined in gpAoHrtReconOpt.h. However, 
 *		it is difficult forthis function to dynamically re-calculate the frequency due to the change of PER_LEN. This is
 *		because that there are many gpAoHrtReconOpt ariables with their size defining by PER_LEN; therefore it requires a
 *		re-make of gpAoHrtReconOpt should a new PER_LEN is desired.		
 *			The following variables are updated based on the frame rate camFR:	
 *					wfsContin[ PER_LEN ] ( float complex), 
 *					dmContin[ PER_LEN ] ( float complex), 
 *					delayContin[ PER_LEN ] ( float complex0, and
 *					zInv[ PER_LEN ] ( float complex) 
 *
 *	@par
 *	
 *	@b Usage: boolean optSetPerFreqAndCtXfrFunc( (short) camFR)
 * 
 *	\@param[ in ]	none
 *
 *	@return:	true ( 1) freqrency and continuous-time transfer function are updated according to a new camera frame rate
 *				false( 0) no processes are done because the camera frame rate hasn't been changed
 *	
 *	@callgraph
 *******************************************************************************
 */
static boolean optSetPerFreqAndCtXfrFunc( void )
{
	int		   ii, jj;
	float	   deltaHz;
	float	   hz[ PER_LEN ];
	optComplex ss[ PER_LEN ];
	float	   tau;
#if ( STANDALONE_COMPLEX != 0 )
	optComplex tmpC, tmpC2;
#endif

#if ( RECONOPT_TEST != 0 )
	camFR_forOptimizer = DEFAULT_OPT_TEST_FRAME_RATE;
#else
	// Test if this has already been calculated
	if( ( LegalFrameRates [hrt_camFrameRate] ) == camFR_forOptimizer)
		return false;

	// If not test mode and if not the same as last time, then update and recalculate the settings
	camFR_forOptimizer = ( LegalFrameRates [hrt_camFrameRate] );
#endif

	deltaHz = (camFR_forOptimizer/(float)PER_LEN);
	tau = 1.0 / camFR_forOptimizer;

	// ******** setup frequencies corresponding to the periodograms
		
	hz[ 0 ] = 0.0;

	for( ii=1; ii<PER_LEN/2; ii++)
		hz[ ii ] = hz[ ii-1 ] + deltaHz;

	hz[ PER_LEN/2 ] = -camFR_forOptimizer / 2;

	for( ii=(PER_LEN/2 + 1); ii<PER_LEN; ii++)
		hz[ ii ] = hz[ ii-1 ] + deltaHz;

	// ******** detemine continuous-time transfer functions to be used in determining open loop response and inverting
	//			the control loop
		
#if ( STANDALONE_COMPLEX == 0 )
	ss[0]			= I * 2.0 * PI * hz[0];  // ss is complex - force imaginary part
	wfsContin[0]	= 1.0;					 // need to do this because ss[ ii ] will cause
	dmContin[0]		= 1.0;					 // > a divide by zero for the first element
	delayContin[0]	= cexpf( -tau * ss[0]);
	zInv[0]			= delayContin[0];

	for( ii=1; ii<PER_LEN; ii++)
	{
		ss[ii]			= I * 2.0 * PI * hz[ii];
		wfsContin[ii]	= ( 1.0 - cexpf( -tau * ss[ii]))/( tau * ss[ii]);
		dmContin[ii]	= wfsContin[ii];
		delayContin[ii]	= cexpf( -tau * ss[ii]);
		zInv[ii]		= delayContin[ii];
	}

	for( jj=0; jj<PER_LEN; jj++)
	{
		delayTerm[jj] = wfsContin[jj] * dmContin[jj] * delayContin[jj];
		cofZDenom_Inv[jj] = 1.0 / ( 1.0 - hrt_modal_loss_dm * zInv[jj] ); 
	}
#else
	ss[0][0]		= 0.0;
	ss[0][1]		= 2.0 * PI * hz[0];  // ss is complex - force imaginary part
	wfsContin[0][0]	= 1.0;				 // need to do this because ss[ ii ] will cause
	wfsContin[0][1]	= 0.0;				 // > a divide by zero for the first element
	dmContin[0][0]	= 1.0;
	dmContin[0][1]	= 0.0;
	tmpC[0] = -tau;					 // tmpC - -tau
	tmpC[1] = 0.0;
	optCmulf(tmpC, ss[0], tmpC);     // tmpC = -tau * ss[0]
	optCexpf(tmpC, delayContin[0]);	 // delayContin[0] = cexpf( -tau * ss[0] )
	zInv[0][0] = delayContin[0][0];
	zInv[0][1] = delayContin[0][1];

	for( ii=1; ii<PER_LEN; ii++)
	{
		ss[ii][0]		= 0.0;
		ss[ii][1]		= 2.0 * PI * hz[ii];

		tmpC[0] = -tau;							// tmpC = -tau
		tmpC[1] = 0.0;
		optCmulf(tmpC, ss[ii], tmpC);			// tmpC = -tau * ss[ii]
		optCexpf(tmpC, delayContin[ii]);	    // delayContin[ii] = cexpf( -tau * ss[ii] )
		optCsubf(oneC, delayContin[ii], tmpC);  // tmpC = 1.0 - cexpf( -tau * ss[ii] )
		tmpC2[0] = tau;							// tmpC2 = tau
		tmpC2[1] = 0.0;
		optCmulf(tmpC2, ss[ii], tmpC2);			// tmpC2 = tau * ss[ii]
		optCdivf(tmpC, tmpC2, wfsContin[ii]);	// wfsContin[ii] = (1.0 - cexpf( -tau * ss[ii])) / (tau * ss[ii])

		dmContin[ii][0] = wfsContin[ii][0];
		dmContin[ii][1] = wfsContin[ii][1];

		zInv[ii][0] = delayContin[ii][0];
		zInv[ii][1] = delayContin[ii][1];
	}

	for( jj=0; jj<PER_LEN; jj++)
	{
		optCmulf(wfsContin[jj], dmContin[jj],    delayTerm[jj]);  // delayTerm[jj] = wfsContin[jj] * dmContin[jj]
		optCmulf(delayTerm[jj], delayContin[jj], delayTerm[jj]);  // delayTerm[jj] = wfsContin[jj] * dmContin[jj] * delayContin[jj];

		tmpC[0] = hrt_modal_loss_dm;				// tmpC = hrt_modal_loss_dm
		tmpC[1] = 0.0;
		optCmulf( tmpC, zInv[jj], tmpC);			// tmpC = hrt_modal_loss_dm * zInv[jj]
		optCsubf( oneC, tmpC, tmpC );				// tmpC = 1.0 - hrt_modal_loss_dm * zInv[jj]
		optCdivf( oneC, tmpC, cofZDenom_Inv[jj] );  // cofZDenom_Inv[jj] = 1.0 / (1.0 - hrt_modal_loss_dm * zInv[jj])
	}
#endif

	return true;
}	// end of optSetPerFreqAndCtXfrFunc( )


#if ( STANDALONE_COMPLEX != 0 )
/*!
 *******************************************************************************
 *
 *	@fn	static void optCfuncs( )
 *
 *	@brief
 *	This is a collection of functions to perform complex math.  
 *
 *	@b DESCRIPTION: 
 *	This is a collection of functions to perform complex math because the C complex
 *	library is not available in the kernel.  Given x = a, ib and y = c, id, the 
 *	functions included are:
 *	@par
 *	optCaddf (addition): x + y = (a + c), i(b + d)
 *	@par
 *	optCsubf (subtraction): x - y = (a - c), i(b - d)
 *	@par
 *	optCmulf (multiplication): x * y = (ac - bd), i(ad + bc) 
 *	@par
 *	optCdivf (division): x / y = (ac + bd) / (c*c + d*d), i(bc - ad) / (c*c + d*d)
 *	@par
 *	optCabsf (absolute value, aka, modulus): cabsf(x) = sqrt( a * a + b * b )
 *	@par
 *	optCexpf (exponentiation): cexpf(x) = (pow(e, a) * cos(b)), i(pow(e, a) * sin(b))
 *	@par
 *	
 *	@b Usage: optCfuncs( opC1, opC2, resC )
 * 
 *	@param[ in ] 	opC1	( optComplex ):	operand 1 (x in the above)
 *	@param[ in ] 	opC2	( optComplex ):	operand 2 (y in the above, for binary operators)
 *	@param[ out ]	resC	( optComplex ):	result (optCabsf's result is float)
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static inline void optCaddf( optComplex opC1, optComplex opC2, optComplex resC )
{
//	given x = a, ib and y = c, id
//	optCaddf (addition): x + y = (a + c), i(b + d)
	resC[0] = opC1[0] + opC2[0];
	resC[1] = opC1[1] + opC2[1];
}

static inline void optCsubf( optComplex opC1, optComplex opC2, optComplex resC )
{
//	given x = a, ib and y = c, id
//	optCsubf (subtraction): x - y = (a - c), i(b - d)
	resC[0] = opC1[0] - opC2[0];
	resC[1] = opC1[1] - opC2[1];
}

static inline void optCmulf( optComplex opC1, optComplex opC2, optComplex resC )
{
//	given x = a, ib and y = c, id
//	optCmulf (multiplication): x * y = (ac - bd), i(ad + bc) 
	double a = opC1[0], b = opC1[1], c = opC2[0], d = opC2[1];
	resC[0] = (float)(a * c - b * d);
	resC[1] = (float)(a * d + b * c);
}

static inline void optCdivf( optComplex opC1, optComplex opC2, optComplex resC )
{
//	given x = a, ib and y = c, id
//	optCdivf (division): x / y = (ac + bd) / (c*c + d*d), i(bc - ad) / (c*c + d*d)
	double a = opC1[0], b = opC1[1], c = opC2[0], d = opC2[1];
	double denom = c * c + d * d;
	if (denom == 0.0)
	{
		resC[0] = 0.0;  //XXX may want to make this NAN
		resC[1] = 0.0;	//XXX may want to make this NAN
	}
	else
	{
		resC[0] = (float)((a * c + b * d) / denom);
		resC[1] = (float)((b * c - a * d) / denom);
	}
}

static inline void optCabsf( optComplex opC1, float *resF )
{
//	given x = a, ib and y = c, id
//	optCabsf (absolute value, aka, modulus): cabsf(x) = sqrt( a * a + b * b )
	double a = opC1[0], b = opC1[1];
	*resF = (float)sqrt( a * a + b * b );
}

static inline void optCexpf( optComplex opC1, optComplex resC )
{
//	given x = a, ib and y = c, id
//	optCexpf (exponentiation): cexpf(x) = (pow(e, a) * cos(b)), i(pow(e, a) * sin(b))
	const double e = 2.71828182845905;
	double a = opC1[0], b = opC1[1], powEA;
	powEA   = pow(e, a);
	resC[0] = (float)(powEA * cos(b));
	resC[1] = (float)(powEA * sin(b));
}
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *
 *	@fn	static void optReadFitsSquare( )
 *
 *	@brief
 *	Reads a FITS file into a square array
 *
 *	@b DESCRIPTION: This function reads a FITS file into a square array. for the time being, the caller
 *					can only specify depth with the value that is less than or equal to 1
 *
 *	@par
 *	
 *	@b Usage: optReadFitsSquare( char *filename, int heightWidth, int depth, int dataType, void *data)
 * 
 *	\@param[ in ]	filename ( char *):	name ( and the directory) of the FITS file to be read	 
 *				data ( void *):		points to an array into which data should be read; the array must
 *									be ( heightWidth * heightWidth) in dimension
 *				heightWidth ( int):	the length of the 2D square array ( i.e., data)	
 *				depth ( int):		the length of the 3rd dimension of the array; if 1, a 2D array is read
 *				dataType ( int):		data type, using cfitsio data type ( TBYTE, TFLOAT, etc.)
 *
 *	\@param[ out ] data ( void *):		array pointed to by data fills with the FITS file 
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void optReadFitsSquare( char *filename, int heightWidth, int depth, int dataType, void *data)
{
	fitsfile *fptr;
	int status = 0;
	long fpixel[ 4 ];	// will handle up to 4 dimensions; however, only 2D for the time being!

	// open the file
	if( fits_open_file( &fptr, filename, READONLY, &status) != 0)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (optReadFitsSquare) in fits_open_file( ):  ", status);
		return;
	}

	// read the data
	fpixel[ 0 ] = 1;
	fpixel[ 1 ] = 1;
	fpixel[ 2 ] = 1;

	if( depth < 1)
		depth = 1;

	if( fits_read_pix( fptr, dataType, fpixel, heightWidth * heightWidth * depth, NULL, data, NULL, &status) != 0)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (optReadFitsSquare) in fits_read_pix( ): ", status);
		return;
	}

	// close the file
	if( fits_close_file( fptr, &status) != 0)
		SysConSendStrngIntParm(eDebugLevelErr, "ERR (optReadFitsSquare) in fits_close_file( ): ", status);

} 	// end of optReadFitsSquare( )
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *
 *	@fn	static void writeCompleteFitsFileSquare( )
 *
 *	@brief
 *	Writes a FITS file to a file from a square array
 *
 *	@b DESCRIPTION:
 *
 *	@par
 *	
 *	@b Usage: writeCompleteFitsFileSquare( char *filename, int heightWidth, int dataType, void *data)
 * 
 *	\@param[ in ]	filename ( char *):	name ( and the directory) of the FITS file to write	 
 *				data ( void *):		points to an array which to be written into the specified FITS file;
 *									the data array must be ( heightWidth * heightWidth) in dimension
 *				heightWidth ( int):	the length of the 2D square array ( i.e., data) 
 *				dataType ( int):		data type, using cfitsio data type ( only TBYTE, TFLOAT, fornow)
 *
 *	\@param[ out ] data ( void *):		array pointed to by data fills with the FITS file 
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void writeCompleteFitsFileSquare( char *filename, int heightWidth, int dataType, void *data)
{
	fitsfile *fptr;
	char fitsFilename[ _POSIX_PATH_MAX ];	// POSIX max path length = 256 bytes
	int status = 0;
	long dataAxes[ 4 ];		// will handle up to 4 dimensions
	long fpixel[ 4 ];		// will handle up to 4 dimensions

//	make sure the filename begins with '!' so fitsio will create the file and then open the file
	if( filename[ 0 ] == '!') 
		strcpy( fitsFilename, filename);
	else
	{
		strcpy( fitsFilename, "!");
		strcat( fitsFilename, filename);
	}
	fits_create_file( &fptr, fitsFilename, &status);

//	create an image, write header keywords, and write out the data
	dataAxes[ 0 ] = heightWidth;
	dataAxes[ 1 ] = heightWidth;
	switch ( dataType)
	{
		case TBYTE:
			fits_create_img( fptr, BYTE_IMG, 2, dataAxes, &status);
			break;
		case TFLOAT:
			fits_create_img( fptr, FLOAT_IMG, 2, dataAxes, &status);
			break;
		default:
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (writeCompleteFitsFileSquare) this type not allowed: ", dataType );
			break;
	}

	fits_write_date( fptr, &status);
	fits_write_comment( fptr, "GPI algProto test data", &status);

	fpixel[ 0 ] = 1;
	fpixel[ 1 ] = 1;
	fits_write_pix( fptr, dataType, fpixel, heightWidth * heightWidth, data, &status);

//	close the file
	fits_close_file( fptr, &status);
}	// end of writeCompleteFitsFileSquare( )
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *
 *	@fn	static void writeCompleteFitsFile( )
 *
 *	@brief
 *	Writes a FITS file from a general array
 *
 *	@b DESCRIPTION:
 *
 *	@par
 *	
 *	@b Usage: writeCompleteFitsFile( char *filename, long* dimSize, int numDim, int dataType, void *data)
 * 
 *	\@param[ in ]	filename ( char *):	path and name of the fits file to write
 *					dimSize ( long[] ):	the dimensions of the array to be written
 *					numDim ( int):		the number or dimensions of the array
 *					dataType ( int):	data type, using cfitsio data type designators
 *					data ( void *):		points to the array to be written
 *
 *	\@param[ out ] a FITS file named "filename"
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void writeCompleteFitsFile( char *filename, long* dimSize, int numDim, int dataType, void *data)
{
	fitsfile*	fptr;
	char		fitsFilename[ _POSIX_PATH_MAX ];	// POSIX max path length = 256 bytes
	int			status = 0;
	long		dataAxes[ 4 ];						// will handle up to 4 dimensions
	long		fpixel[ 4 ];						// will handle up to 4 dimensions
	int			ii;
	long long	size = 1;

//	make sure the filename begins with '!' so fitsio will create the file and then open the file
	if( filename[ 0 ] == '!') 
		strcpy( fitsFilename, filename);
	else
	{
		strcpy( fitsFilename, "!");
		strcat( fitsFilename, filename);
	}
	fits_create_file( &fptr, fitsFilename, &status);

	for( ii=0; ii<4; ii++)
	{
		dataAxes[ ii ] = 0;
		fpixel[ ii ] = 1;
	}
	
//	create an image, write header keywords, and write out the data
	size = 1L;
	for( ii=0; ii<numDim; ii++)
	{
		dataAxes[ ii ] = dimSize[ ii ];
		size *= dimSize[ii];
	}

	switch ( dataType)
	{
		case TBYTE:
			fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status);
			break;
		case TFLOAT:
			fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status);
			break;
		default:
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (writeCompleteFitsFile) this type not allowed: ", dataType );
			break;
	}

	fits_write_date( fptr, &status);
	fits_write_comment( fptr, "GPI algProto test data", &status);

	fits_write_pix( fptr, dataType, fpixel, size, data, &status);

	//	close the file
	fits_close_file( fptr, &status);

}	// end of writeCompleteFitsFile( )
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *
 *	@fn	static void openFitsForWrite( )
 *
 *	@brief
 *	Open a FITS file, for write, for a general array
 *
 *	@b DESCRIPTION:
 *
 *	@par
 *	
 *	@b Usage: openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType)
 * 
 *	\@param[ in ]	filename ( char *):	name ( and the directory) of the FITS file to write	 
 *					dimSize ( long[] ):	the length of one dimension of the 2D array 
 *					numDim ( int):		the length of another dimension of the 2D array
 *					dataType ( int):	data type, using cfitsio data type ( only TBYTE, TFLOAT, fornow)
 *
 *	\@param[ out ] a FITS file named "filename"
 *
 *	@return 
 *	void
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */
static 	fitsfile* openFitsForWrite( char *filename, long* dimSize, int numDim, int dataType )
{
	fitsfile*	fptr = NULL;
	char		fitsFilename[ _POSIX_PATH_MAX ];	// POSIX max path length = 256 bytes
	int			status = 0;
	long		dataAxes[ 4 ];						// will handle up to 4 dimensions
	int			ii;
	char		tmpBuf[1024];

	last_fptr = NULL;  // Reset when you open new file

//	make sure the filename begins with '!' so fitsio will create the file and then open the file
	if( filename[ 0 ] == '!') 
		strcpy( fitsFilename, filename);
	else
	{
		strcpy( fitsFilename, "!");
		strcat( fitsFilename, filename);
	}

	if( fits_create_file( &fptr, fitsFilename, &status) != 0)
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3108): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, "  - Creating File " );
		SysConSendStrngNoParm(eDebugLevelErr, tmpBuf);
		HRT_PRINTF( tmpBuf );
		return NULL;
	}

//	create an image, write header keywords, and write out the data
	for( ii=0; ii<numDim; ii++)
		dataAxes[ ii ] = dimSize[ ii ];

	switch ( dataType)
	{
		case TBYTE:
			if( fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status) != 0)
			{
				strcpy( tmpBuf, "ERR (writeFitsBlock:3122): " );
				fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
				strcat( tmpBuf, " - Creating BYTE Image " );
				SysConSendStrngNoParm(eDebugLevelErr, tmpBuf);
				HRT_PRINTF( tmpBuf );
				return NULL;
			}
			break;

		case TFLOAT:
			if( fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status) != 0)
			{
				strcpy( tmpBuf, "ERR (writeFitsBlock:3133): " );
				fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
				strcat( tmpBuf, " - Creating FLOAT Image " );
				HRT_PRINTF( tmpBuf );
				SysConSendStrngNoParm(eDebugLevelErr, tmpBuf);
				return NULL;
			}
			break;

		default:
			SysConSendStrngIntParm(eDebugLevelErr, "ERR (openFitsForWrite) This 'type' not allowed (only allow BYTE and FLOAT currently) :", dataType );
			break;
	}


	fits_write_date( fptr, &status);
	fits_write_comment( fptr, "GPI algProto test data", &status);

	if( status != 0 )
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3152): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, " - Status type: Comment " );
		HRT_PRINTF_ERR( tmpBuf );
		SysConSendStrngNoParm(eDebugLevelErr, tmpBuf);
		return NULL;
	}

	return fptr;
}
#endif

#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
// Write a block of data to the already open fits file - this expects _fpixel to have been set, so data is put into correct position
static void writeFitsBlock( fitsfile* fptr, long _size, int dataType, void* pData, long _fpixel[] )
{
	long		fpixel[ 4 ];						// will handle up to 4 dimensions
	int			status = 0;
	int			ii;
	char		tmpBuf[1024];
	long long	size;

	if( fptr == NULL )
			return;

	if( last_fptr != NULL && fptr != last_fptr )
	{
		HRT_PRINTF_ERR("*** Warning: fits file ptr mismatch for RECONOPT_DEBUG_DUMP" );
		last_fptr = fptr;
	}
	else
	{
		last_fptr = fptr;
	}

	size = _size;

	for( ii=0; ii<4; ii++)
		fpixel[ ii ] = _fpixel[ ii ];

	if( fits_write_pix( fptr, dataType, fpixel, size, pData, &status) != 0)
	{
		strcpy( tmpBuf, "ERR (writeFitsBlock:3193): " );
		fits_get_errstatus( status, (tmpBuf + strlen(tmpBuf)) );
		strcat( tmpBuf, " - Writing data block " );
		SysConSendStrngNoParm(eDebugLevelErr, tmpBuf );
		HRT_PRINTF( tmpBuf );
		sprintf( tmpBuf, "         fptr = 0x%p, dataType=0x%x, fpixel[0]=0x%lx, fpixel[1]=0x%lx, size=0x%llx, pData=0x%p, status=0x%x ",   
														fptr, dataType, fpixel[0], fpixel[1],  size, pData, status);
		HRT_PRINTF( tmpBuf );

		return;
	}

	return;
}	// end of writeFitsBlock()
#endif

#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
// Close a previously opened fits file.  The file may or may not have been written to.
static void closeFits( fitsfile* fptr )
{
	int	status = 0;

	if( fptr == NULL )
		return;

	//	close the file
	fits_close_file( fptr, &status);

}	// end of closeFits()

#endif

#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
static void flushFitsBuffer( fitsfile* fptr )
{
	int		status = 0;

	if( fptr == NULL )
			return;

	//	flush the buffer
	fits_flush_buffer( fptr, 0, &status);

}	// end of flushFitsBuffer()
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *	@fn static int dumpSliceOfComplexDataCube( void *pIn, int sliceRowSize, int sliceColSize, int cubeDepth, int dType )
 *
 *	@brief
 *	Dump slice of data Cube
 *
 *	@b DESCRIPTION:
 * 		This function currently only supports 3D type matrices 
 *		Only supports FLOATS
 *	@par
 *	
 *
 *	@b Usage: dumpSliceOfDataCube( (void*)pIn, (int) sliceRowSize, (int) sliceColSize, (int) cubeDepth, (int) dType )
 * 
 *	\@param[ in ]
 *		None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int		dumpSliceOfComplexDataCube( fft_complex_type* pIn, int sliceRowSize, int sliceColSize, int cubeDepth )
{
	int		ii, jj;

	HRT_PRINTF_NOCRLF( "\n INFO - dump Complex Slice of dataCube (start ptr: %p)\n", pIn );

	for( jj=0; ((jj<7) && (jj<sliceColSize)) ; jj++)
	{
		for( ii=0; ( ii<7) && ( ii<sliceRowSize); ii++)
			HRT_PRINTF_NOCRLF( " (%9.5g, i%9.5g) ", pIn[  jj*sliceRowSize*cubeDepth + ii*cubeDepth][0], pIn[ jj*sliceRowSize*cubeDepth + ii*cubeDepth][1]  ) ;
		HRT_PRINTF_NOCRLF( "\n");
	}

	HRT_PRINTF_NOCRLF( "\n");

	return 0;
}
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *	@fn static int dumpSliceOfDataCube( void *pIn, int sliceRowSize, int sliceColSize, int cubeDepth, int dType )
 *
 *	@brief
 *	Dump slice of data Cube
 *
 *	@b DESCRIPTION:
 * 		This function currently only supports 3D type matrices 
 *		Only supports FLOATS
 *	@par
 *	
 *
 *	@b Usage: dumpSliceOfDataCube( (void*)pIn, (int) sliceRowSize, (int) sliceColSize, (int) cubeDepth, (int) dType )
 * 
 *	\@param[ in ]
 *		None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int		dumpSliceOfDataCube( void *pIn, int sliceRowSize, int sliceColSize, int cubeDepth, int dType )
{
	int		ii, jj;

	if( dType == TFLOAT )
	{
		HRT_PRINTF_NOCRLF( "\n INFO - dump Slice of dataCube (start ptr: %p)\n", pIn );

		for( jj=0; ((jj<10) && (jj<sliceColSize)) ; jj++)
		{
			for( ii=0; ( ii<10) && ( ii<sliceRowSize); ii++)
				HRT_PRINTF_NOCRLF( " %9.5g, ", *( ( float*)pIn+ jj*sliceRowSize*cubeDepth + ii*cubeDepth  ) );
			HRT_PRINTF_NOCRLF( "\n");
		}

		HRT_PRINTF_NOCRLF( "\n");
	}

	return 0;
}
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *	@fn static int dumpCornerOfaMatrix( )
 *
 *	@brief
 *	Dump corner of a 2D matrix
 *
 *	@b DESCRIPTION:
 * 		This function currently only supports 2D type matrices ( will work on 2D if step size is set to 6)
 *		Only supports FLOATS and LONGS ( complex dump is seperate function)
 *	@par
 *	
 *
 *	@b Usage: dumpCornerOfaMatrix( void *pIn, int rowSize, int complexFlag, int dType );
 * 
 *	\@param[ in ]
 *		None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int dumpCornerOfMatrix( void *pIn, int rowSize, int totRows, int dType, boolean extend )
{
	int		ii, jj;
	double	tmpSum = 0.0;

	if( dType == TFLOAT )
	{
		HRT_PRINTF_NOCRLF( "\n INFO - matrix corner dump at pointer: %p\n", pIn );
		for( jj=0; ((jj<10) && (jj<totRows)) ; jj++)
		{
			for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
				HRT_PRINTF_NOCRLF( " %9.5g, ", *( ( float*)pIn+ jj*rowSize + ii ) );

			tmpSum = 0.0;
			for( ii=0; ii<rowSize; ii++)
			{
				tmpSum += *( ( float*)pIn + jj*rowSize + ii );
			}

			HRT_PRINTF_NOCRLF( " Sum=%9.5g\n", tmpSum );
		}

		if( extend)
		{
			HRT_PRINTF_NOCRLF("\n@550:\n");
			for( jj=550; ( (jj<600) && (jj<totRows) ); jj++)
			{
				for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( " %9.5g, ", *( ( float*)pIn+ jj*rowSize + ii ) );
    
				tmpSum = 0.0;
				for( ii=0; ii<rowSize; ii++)
				{
					tmpSum += *( ( float*)pIn + jj*rowSize + ii );
				}

				HRT_PRINTF_NOCRLF( " Sum=%9.5g\n", tmpSum );
			}
		}

		tmpSum = 0.0;
		for( jj=0; jj<totRows; jj++ )
			for( ii=0; ii<rowSize; ii++)
			{
				tmpSum +=	*( ( float*)pIn + jj*rowSize + ii);
			}
		HRT_PRINTF_NOCRLF( "\n TOT FOR CORNER DUMP = ( %9.5f ) ", tmpSum );

		HRT_PRINTF_NOCRLF( "\n");
	}

	if( dType == TLONG )
	{
			HRT_PRINTF_NOCRLF( "\n INFO - matrix ( long) corner dump at pointer: %p\n", pIn );
			for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
			{
				for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
					HRT_PRINTF_NOCRLF( "	%ld, 	", *( ( long*)pIn+ jj*rowSize + ii ) );
				HRT_PRINTF_NOCRLF( "\n");
			}
			HRT_PRINTF_NOCRLF( "\n");
	}
	return 0;
}
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *	@fn static int dumpCornerOfComplexMatrix( )
 *
 *	@brief
 *	Dump corner of a 2D matrix
 *
 *	@b DESCRIPTION:
 * 		This function currently only supports 2D type matrices ( will work on 2D if step size is set to 6)
 *		Only supports COMPLEX -  FLOATS and LONGS
 *	@par
 *	
 *
 *	@b Usage: dumpCornerOfComplexMatrix( void *pIn, int rowSize, int totalRows, int dType );
 * 
 *	\@param[ in ]
 *		None
 *
 *	@return 
 *	None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int dumpCornerOfComplexMatrix( void *pIn, int rowSize, int totRows, int dType )
{
	int		ii, jj;
	double	tmpSum = 0.0;
	double	tmpSum_i = 0.0;


	if( dType == TFLOAT )
	{
		HRT_PRINTF_NOCRLF( "\n INFO - complex matrix corner dump at pointer: %p\n", pIn );
		for( jj=0; ((jj<10) && (jj<totRows)) ; jj++)
		{
			for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
				HRT_PRINTF_NOCRLF( " (%9.5g, i%9.5g) ", *( ( float*)pIn + jj*rowSize*2 + ii*2), *( ( float*)pIn + jj*rowSize + ii*2 + 1) );

			tmpSum = tmpSum_i = 0.0;
			for( ii=0; ii<rowSize; ii++)
			{
				tmpSum +=	*( ( float*)pIn + jj*rowSize*2 + ii*2);
				tmpSum_i +=	*( ( float*)pIn + jj*rowSize*2 + ii*2 + 1);
			}

			HRT_PRINTF_NOCRLF( " : Sum real/img = (%9.5g, %9.5g)\n", tmpSum, tmpSum_i );
		}

		tmpSum = tmpSum_i = 0.0;
		for( jj=0; jj<totRows; jj++ )
			for( ii=0; ii<rowSize; ii++)
			{
				tmpSum +=	*( ( float*)pIn + jj*rowSize*2 + ii*2);
				tmpSum_i +=	*( ( float*)pIn + jj*rowSize*2 + ii*2 + 1);
			}
		HRT_PRINTF_NOCRLF( "\n TOT FOR CORNER DUMP = (  %9.5f, %9.5f ) ", tmpSum, tmpSum_i );

		HRT_PRINTF_NOCRLF( "\n");
	}

	if( dType == TLONG )
	{
		HRT_PRINTF_NOCRLF( "\n INFO - complex ( long) matrix corner dump at pointer: %p\n", pIn );
		for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
		{
			for( ii=0; ( ii<10) && ( ii<rowSize); ii++)
				HRT_PRINTF_NOCRLF( " ( %ld, ii%ld	", *( ( long*)pIn + jj*rowSize + ii * 2 ) , *( ( long*)pIn + jj*rowSize + ii * 2 + 1 )	);
			HRT_PRINTF_NOCRLF( "\n");
		}
		HRT_PRINTF_NOCRLF( "\n");
	}
	return 0;
}
#endif


#if ( ( RECONOPT_DEBUG != 0 ) || ( RECONOPT_DEBUG_DUMP != 0 )  || ( RECONOPT_TEST != 0 ) )
/*!
 *******************************************************************************
 *	@fn readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType )
 *
 *	@brief
 *	This unit reads a fits file from disk. 
 *
 *	@b DESCRIPTION:
 * 		This unit reads a fits file from disk. 
 *	@par
 *	
 *
 *	@b Usage: readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType )
 * 
 *	@param[ in ]	filename	(char*):	name of fits file to read
 * 	@param[ in ]	data		(void*):	array into which data should be read ( must be preallocated )
 * 	@param[ in ]	pNDim		(int*):		pointer to the number of dimensions
 * 	@param[ in ]	pDimSize	(long*):	ptr to the array of dimensions
 * 	@param[ in ]	pDataType	(int*):		pointer to the type of data
 *
 *	@return 
 *			data - data that was read
 *			Returns 1 if OKAY
 *			Returns 0 if error
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType )
{
	fitsfile *fptr;
	int bits_per_pixel;
	int status = 0;
	int nDim = 0;
	long dimSize[ 10 ];	//will handle up to 4 dimensions
	long nElem = 0L;
	int ii;
	long fpixel[ 10 ];

	//	open the file
	fits_open_file( &fptr, filename, READONLY, &status);
	if( status != 0)
	{
		HRT_PRINTF( "ERR (readFits:3501): Open FITS File - name: '%s'", filename );
		pData = NULL;
		return 0;
	}

	// Not allocating - so data must be pre-allocated
	if( pData==NULL)
	{
		HRT_PRINTF( "ERR (readFits:3509): Open FITS File - name: '%s'", filename );
		pData = NULL;
		fits_close_file( fptr, &status );
		return 0;
	}

	fits_get_img_param( fptr, 3, &bits_per_pixel, &nDim, &dimSize[ 0 ], &status );
	if( status != 0)
	{
		HRT_PRINTF( "ERR (readFits:3518): Getting parameters: FITS File - name: '%s'", filename );
		pData = NULL;
		return 0;
	}

	// Not allocating - so check that input values same as file specs.
	if( nDim!=*pNDim )
	{
		HRT_PRINTF( "ERR (readFits:3526): Input allocation/type does not match file" );
		HRT_PRINTF( "				nDim = %d, *pNDim = %d", nDim, *pNDim );

		pData = NULL;
		fits_close_file( fptr, &status );
		return 0;
	}

	nElem = 1;
	for( ii=0; ii< nDim; ii++)
	{
		fpixel[ ii ] = 1;
		nElem *= dimSize[ ii ];
		if( dimSize[ ii ] != pDimSize[ ii ])
		{
			HRT_PRINTF( "ERR (readFits:3541): Input dimension sizes do not match file '%s'", filename);
			HRT_PRINTF( "				Dim: %d expected: %ld readin: %ld", ii, dimSize[ ii ], pDimSize[ ii ]);
			pData = NULL;
			fits_close_file( fptr, &status );
			return 0;
		}
	}

	fits_read_pix( fptr, *pDataType, fpixel, nElem, NULL, pData, NULL, &status);

	//	close the file
	fits_close_file( fptr, &status);


	return 1;
}
#endif 

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
