/*******************************************************************************
 *
 *	COPYRIGHT (C) 2010
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoHrtDpio2Drivers.c
 *
 * 	@version
 *
 *	@brief 
 *	This component contains the interface to the RTL version of the dpio2 drivers. 
 *
 *	@b DESCRIPTION:
 *	This component contains the interface to the RTL version of the dpio2 drivers.  
 *	Two dpio2 boards are used in GPI, one to drive the woofer (including the T/T 
 *	stage) and the other to drive the tweeter.  As would be expected, both boards 
 *	are driven with the same driver software.  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 06/19/2010
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoHrtDpio2Init()
 *	-# @c gpAoHrtDpio2StartDmaTransfer()
 *	-# @c gpAoHrtDpio2Cleanup()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/rtl_ioctl.h>
#include <rtl_fcntl.h>
#include <string.h>

#define VMOSA
#include "vmosa-linux.h"
#include "dpio2-linux.h"
#include "dpio2-linux-user-space.h"
#include "dpio2.h"
#include "Dpio2Defs.h"

// these functions are actually in rt_dpio2_funcs.c
extern HANDLE dpio2_Open			(int iDevno);
extern STATUS dpio2_Close			(HANDLE hHandle);
extern STATUS dpio2_ConfigurePioStrobes		(HANDLE hHandle, int iValue);
extern STATUS dpio2_ControlPioStrobes		(HANDLE hHandle, int iValue);
extern STATUS dpio2_InitializeForOutput		(HANDLE hHandle);
extern STATUS dpio2_EnableStrobeGeneration	(HANDLE hHandle, int iMode, unsigned int dwWantedFrequency, unsigned int *pdwResultantFrequency);
extern STATUS dpio2_ActivateFpdpInterface	(HANDLE hHandle);
extern STATUS dpio2_DmaTransfer			(HANDLE hHandle, UINT32 dwSizeInBytes, unsigned char *pData);
extern STATUS dpio2_DmaEnd			(HANDLE hHandle);

#include "gpAoHrtSysCon.h"
#include "gpAoHrtDpio2Drivers.h"


// Local Variables

static HANDLE hDpio2[2] = { NULL, NULL };  // one for the woofer and one for the tweeter


// Global Variables

unsigned short *dpio2BufferWFR[2];
unsigned short *dpio2BufferTWT[2];


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtDpio2StartDmaTransfer(int dpio2DeviceNumber, int desc_num, int buffNdx)
 *
 *	@brief
 *  This unit initiates a dpio2 DMA transfer for RTL operation.
 *
 *	@b DESCRIPTION:
 *  This unit initiates a dpio2 DMA transfer for RTL operation.  The transfer is 
 *	always out and is from the buffer previously associated with the indicated 
 *	dpio2DeviceNumber.  
 *
 *	@b Usage: gpAoHrtDpio2StartDmaTransfer(dpio2DeviceNumber, desc_num, buffNdx);
 * 
 *	@param[in]	dpio2DeviceNumber	(int):	dpio2 device number (0 or 1)
 *	@param[in]	desc_num			(int):	descriptor number (always 0)
 *	@param[in]	buffNdx				(int):	the index number for the current double buffer (0 or 1)
 *
 *	@return 
 *	 0 : no error
 *  -1 : error (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	DMA buffers must be set up.  
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoHrtDpio2StartDmaTransfer (int dpio2DeviceNumber, int desc_num, int buffNdx)
{
	static boolean needDmaEnd[2] = { false, false };  // one for the woofer and one for the tweeter

	static int errReported = 0;
	int errFlag = 0;

//	printf("		// mrr DMStarting gpAoHrtDpio2StartDmaTransfer\n");
	//	if the device hasn't been successfully opened, don't bother trying to do anything
	if (hDpio2[dpio2DeviceNumber] == NULL)
		return -1;

	if (dpio2DeviceNumber == dpio2DevnoTWT)
	{
	//		raise the PIO strobe used to sync the Cambridge Innovation drive electronics
		if (dpio2_ControlPioStrobes(hDpio2[dpio2DeviceNumber], 1) != OK)
			errFlag |= 0x1;
	}

	//	issue a pci_unmap_single() for the buffer most recently used for the specified device
	if (needDmaEnd[dpio2DeviceNumber])
	{
		needDmaEnd[dpio2DeviceNumber] = false;
 		dpio2_DmaEnd(hDpio2[dpio2DeviceNumber]);
	}

	//	lower the PIO strobe
	if (dpio2_ControlPioStrobes(hDpio2[dpio2DeviceNumber], 0) != OK)
		errFlag |= 0x2;

	//	transfer the frame buffer into the DPIO2 FIFO
	needDmaEnd[dpio2DeviceNumber] = true;
// mrr DM
//	if (dpio2DeviceNumber == dpio2DevnoWFR)
//	{
//		if (dpio2_DmaTransfer(hDpio2[dpio2DevnoWFR], (UINT32)sizeInBytesWFR, (char *)dpio2BufferWFR[buffNdx]) != OK)
//		{
//			needDmaEnd[dpio2DeviceNumber] = false;
//			errFlag |= 0x4;
//		}
//
////		raise the PIO strobe used to sync the Cambridge Innovation drive electronics
////		> IMPORTANT -- this seems dangerous because it assumes the DMA has completed (I think); but
////		> it seems to be what the woofer needs and it works
//		if (dpio2_ControlPioStrobes(hDpio2[dpio2DeviceNumber], 1) != OK)
//			errFlag |= 0x1;
//	}
//	else  // dpio2DevnoTWT
	{
		if (dpio2_DmaTransfer(hDpio2[dpio2DevnoTWT], (UINT32)sizeInBytesTWT, (char *)dpio2BufferTWT[buffNdx]) != OK)
		{
			needDmaEnd[dpio2DeviceNumber] = false;
			errFlag |= 0x8;
		}
	}

	//	if necessary, report an error (a few times, so we don't get zillions of messages)
	if (errFlag > 0)
	{
		if (errReported < 20)
		{
			errReported++;
			SysConSendStrngIntParm(eDebugLevelErr, "ERR RTL DPIO2: gpAoHrtDpio2StartDmaTransfer failed: ", dpio2DeviceNumber);
			printf("\n            ******* ERR RTL DPIO2: gpAoHrtDpio2StartDmaTransfer on device '%d' failed. ERROR: %d\n\n", dpio2DeviceNumber, errFlag);
		}
		return -1;
	}
	//	printf("       *Ending gpAoHrtDpio2StartDmaTransfer successfully\n");	// mrr DM
	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtDpio2Init( void )
 *
 *	@brief
 *  This unit initializes the two dpio2 boards for RTL operation.
 *
 *	@b DESCRIPTION:
 *  This unit initializes the two dpio2 boards for RTL operation.  If any error 
 *	occurs, the initialization aborts and neither board can be assumed to be 
 *	initialized.  
 *
 *	@b Usage: gpAoHrtDpio2Init();
 * 
 *	@param[in] None
 *
 *	@return 
 *	 0 : no error
 *  <0 : any of several errors (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	Two dpio2 boards must be installed in the system.
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoHrtDpio2Init(void)
{
	int devno;
	int requestedFreq, resultingFreq;

	printf( "    Dpio2Drivers: Starting DPIO2 init\n");	// mrr DM

	printf( "      Dpio2Drivers: Starting DPIO2 memory allocation\n");	// mrr DM
	
	//	allocate memory -- designate PHYSICAL allocation
//	dpio2BufferWFR[0] = (unsigned short *)rtl_calloc_mem(sizeInBytesWFR, RTL_O_PHYS | RTL_O_DMA);
//	dpio2BufferWFR[1] = (unsigned short *)rtl_calloc_mem(sizeInBytesWFR, RTL_O_PHYS | RTL_O_DMA);
	dpio2BufferTWT[0] = (unsigned short *)rtl_calloc_mem(sizeInBytesTWT, RTL_O_PHYS | RTL_O_DMA);
	dpio2BufferTWT[1] = (unsigned short *)rtl_calloc_mem(sizeInBytesTWT, RTL_O_PHYS | RTL_O_DMA);

//	if ((dpio2BufferWFR[0] == NULL) || (dpio2BufferWFR[1] == NULL) || 
//		(dpio2BufferTWT[0] == NULL) || (dpio2BufferTWT[1] == NULL))
	if ((dpio2BufferTWT[0] == NULL) || (dpio2BufferTWT[1] == NULL))
	{
		printf( "     Dpio2Drivers: **** ERROR in  DPIO2 memory allocation\n");	// mrr DM
        SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2 failed to allocate DM buffers");
		return -1;
	}

	printf( "      *Dpio2Drivers: Ending DPIO2 memory allocation\n");	// mrr DM


//	open and initialize 2 dpio2 devices, one for the woofer and one for the tweeter
//	for (devno = 0; devno < 2; devno++)	// mrr DM
	for (devno = 0; devno < 1; devno++)
	{
// 		open the dpio2 devices
		printf( "      Dpio2Drivers: Starting dpio2_Open\n");	// mrr DM

		hDpio2[devno] = dpio2_Open(devno);  // woofer
		if (hDpio2[devno] == NULL)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2: dpio2_Open failed");
			printf( "    Dpio2Drivers: **** dpio2_Open failed\n");	// mrr DM
			return -1;
		}
		printf( "      *Dpio2Drivers: Ending dpio2_Open\n");	// mrr DM

//		configure PIO strobes	
		if (dpio2_ConfigurePioStrobes(hDpio2[devno], 1) != OK)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2: dpio2_ConfigurePioStrobes failed");
			printf( "      *Dpio2Drivers: dpio2_ConfigurePioStrobes failed\n");	// mrr DM
			return -1;
		}

//		set DPIO2 hardware for output operation
		if (dpio2_InitializeForOutput(hDpio2[devno]) != OK)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2: dpio2_InitializeForOutput failed");
			printf( "      *Dpio2Drivers: dpio2_ConfigurePioStrobes failed\n");	// mrr DM
			return -1;
		}

//		activate front panel interface
		if (dpio2_ActivateFpdpInterface(hDpio2[devno]) != OK)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2: dpio2_ActivateFpdpInterface failed");
			printf( "      *Dpio2Drivers: dpio2_ConfigurePioStrobes failed\n");	// mrr DM
			return -1;
		}

//		enable dpio2 clock set to dpio2StrobeFreq (note that this is confusing because both the dpio2 clock and the PIO pulse
//		> used to sync the Cambridge Innovation drive electronics are called 'strobes')
// mrr DM
		//if (devno == dpio2DevnoWFR)
		//{
		//	requestedFreq = dpio2StrobeFreqWFR;
		//	printf( "\nrequestedFreq = dpio2StrobeFreqWFR\n\n");	// mrr DM
		//}
		//else
		{
			requestedFreq = dpio2StrobeFreqTWT;
			printf( "      Dpio2Drivers: == requestedFreq = dpio2StrobeFreqTWT\n");	// mrr DM
		}
		if (dpio2_EnableStrobeGeneration(hDpio2[devno], DPIO2_PROGRAMMABLE_OSCILLATOR, requestedFreq, &resultingFreq) != OK)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "ERR RTL DPIO2: dpio2_EnableStrobeGeneration failed");
			printf( "      *Dpio2Drivers: dpio2_ConfigurePioStrobes failed\n");	// mrr DM
			return -1;
		}
		if (requestedFreq != resultingFreq)
		{
			SysConSendStrngIntParm(eDebugLevelWarn, "WARN RTL DPIO2: dpio2StrobeFreq != resultingFreq: ", resultingFreq);
			printf( "      Dpio2Drivers: **** WARN RTL DPIO2: dpio2StrobeFreq != resultingFreq: error\n");	// mrr DM
		}
	}

	printf( "    *Dpio2driver: Ending DPIO2 init\n\n");	// mrr DM

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoHrtDpio2Cleanup( void )
 *
 *	@brief
 *  This unit cleans up the two dpio2 boards for RTL operation.
 *
 *	@b DESCRIPTION:
 *  This unit cleans up the two dpio2 boards for RTL operation.
 *
 *	@b Usage: gpAoHrtDpio2Cleanup();
 * 
 *	@param[in] None
 *
 *	@return 
 *	None.
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoHrtDpio2Cleanup ( void )
{
//	set strobes low and turn off
	dpio2_ConfigurePioStrobes(hDpio2[0], 0);
//	dpio2_ConfigurePioStrobes(hDpio2[1], 0);	// mrr DM

//	close the dpio2
	dpio2_Close(hDpio2[0]);
//	dpio2_Close(hDpio2[1]);	// mrr DM

// 	free memory
//	rtl_free_mem((char *)dpio2BufferWFR[0]);	// mrr DM
//	rtl_free_mem((char *)dpio2BufferWFR[1]);	// mrr DM
	rtl_free_mem((char *)dpio2BufferTWT[0]);
	rtl_free_mem((char *)dpio2BufferTWT[1]);
}
