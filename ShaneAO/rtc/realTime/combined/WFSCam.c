/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file WFSCam.c
 *
 *  @brief
 *  This component contains functions to interface to the SciMeasure WFS camera.
 *
 *  @b DESCRIPTION:
 *  This component is the Hard Real Time (HRT) interface to the AO WFS camera.
 *  It performs the RT camera initialization, communicates with the SRT CamMngr
 *  module to issue commands (because the camera drivers are not strictly RTLinux
 *  compatible), and handles real-time processing for the camera.
 *
 *  @note The Cam() thread is included in this component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 11/25/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c WFSCam()
 *  -# @c WFSCam_Init()
 *  -# @c WFSCam_IntrInit()
 *  -# @c WFSCam_IntrHandler()
 *  -# @c WFSCam_ProcCmd()
 *  -# @c WFSCam_SendCmd()
 *  -# @c WFSCam_LoadDataBuffer()
 *  -# @c WFSCam_DeIntlv128x86()
 *  -# @c WFSCam_DeIntlv128x128()
 *  -# @c WFSCam_AutoParms()
 *  -# @c WFSCam_DrvrWatch()
 *  -# @c WFSCam_LoadSimWfsData()
 *  -# @c WFSCam_LoadSimTTData()
 *  -# @c WFSCam_LoadSimPupilMoveData()
 *  -# @c WFSCam_LoadSimZernikes()
 *  -# @c WFSCam_ComputeZernikes()
 *
 *  @see gpAoSrtCamMngr.c
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#include "locks.h"

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


#include "WFSCam.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

#if (CAM_TYPE == WFS_REAL_GPI)
#if (RTL_SYSTEM != 0)
typedef unsigned char           u_char;
typedef unsigned short          u_short;
typedef unsigned int            u_int;
typedef long long               __kernel_loff_t;
typedef __kernel_loff_t         loff_t;
#include "edt_lnx_kernel_Local.h"
#else   /* RTL_SYSTEM */
#include "edtinc.h"
#endif  /* RTL_SYSTEM */
#endif  /* CAM_TYPE */


// Defines


// Global Variables
#if ( TIMER_TEST != 0 )
// a sequential counter incremented every time a valid frame is sent to the centroider
long camFrameID = -1;
int timerNdx = 0;
#endif

short   hrt_useWfsSimData;      // flag indicating whether to use simulated wfs data ( 0: no, 1: yes )

short   hrt_camFrameRate;       // camera frame rate INDEX (NOT the actual frame rate!)
short   hrt_camGain;            // camera gain (0, 1, 2, or 3)

short   CamFrameRateNdx = 0;    // index for current camera rate into LegalFrameRates[]

int     WFS_irq = -1;

#if ( RTL_SYSTEM != 0 )
// WARNING: LegalFrameRates[] is defined in both WFSCam.c (for the HRT) and gpAoSrtCamMngr.c (for the SRT)
/* WARNING:  The code below ASSUMES that the LegalFrameRates array is ordered. */
#if ( NUM_ROWS_IN_CAM_ACTUAL == 86 )
// IMPORTANT: note that 2000 fps isn't actually that fast in 128x86 mode, it's just a REP of 0 for testing
// IMPORTANT: DO NOT collect 10 seconds of diag data at 2000 fps -- there aren't enough buffers (collect up to 7 secs)!
// const short  LegalFrameRates[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!    // mrr
const short     LegalFrameRates[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator    // mrr
#else
// IMPORTANT: note that 1500 fps isn't actually that fast in 128x128 mode, it's just a REP of 0 for testing
//XXX put this back after testing!  const short    LegalFrameRates[] = { 250, 500, 1000, 1500,   -1, -1 };  // -1 is the terminator
//XXXdwp110615 const short      LegalFrameRates[] = { 250, 500, 1000, 1500, 2000, -1 };  // -1 is the terminator
// const short  LegalFrameRates[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!
const short     LegalFrameRates[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator    // mrr
#endif  /* NUM_ROWS_IN_CAM_ACTUAL */
#endif  /* RTL_SYSTEM */

void* WFS_dev = NULL;



// Local Variables
#if ( TIMER_TEST != 0 )
static struct timespec intrTime, prevIntrTime, afterWFSWake, afterEDTIntrProc, afterWFSIRQEnable = {0,0};
#endif

#if ((RTL_SYSTEM != 0) && (CAM_TYPE == WFS_REAL_GPI))
// These are the kernel pages for the EDT ring buffers in RTL mode;
// camera frames are constructed from these pages.
static u_char   **edtKernelPages[WFS_BUFFERS];
static int      nEdtKernelPages[WFS_BUFFERS];

// the following set of arrays is oversized by RTL_PAGE_SIZE because they
// MUST be big enough for the frame rounded up to an integer number of
// RTL_PAGE_SIZEs
static unsigned short   wfsCamBuffsForRTL[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160 + RTL_PAGE_SIZE];

static unsigned long    oldAffinity;
#endif

static unsigned short *buf_array[WFS_BUFFERS];  // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other
static unsigned short *buf_array_160[WFS_BUFFERS];  // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other

static unsigned int    edtDoneCount_sim = 0;
static unsigned short  wfsCamBuffsFor128[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];
static unsigned short  wfsCamBuffsForSim[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];
static unsigned short *buf_array_sim[WFS_BUFFERS];

static int      BadCntr = 0;

static int      CamWFSIntrWatchCntr = 0;  // watchdog counter for camera interrupt

static int      CamWFStrywaitCntr   = 0;    // watchdog counter
static int      CamWFSnullCDPCntr   = 0;    // watchdog counter
static int      CamWFScmdStartCntr  = 0;    // watchdog counter
static int      CamWFSintrIgnoreCntr= 0;    // watchdog counter
static int      CamWFSedtBufIniCntr = 0;    // watchdog counter
static int      CamWFSedtBufNewCntr = 0;    // watchdog counter
volatile static int      CamWFSIntrHandler   = 0;    // watchdog counter
static int      CamWFSWaitBefore    = 0;    // watchdog counter
static char*    CamWFSLastPath      = "none"; // last path taken in while loop
static char*    CamWFSLastNonWatchdog = "nada"; // last path taken, not reset by watchdog 

static int      interruptWatch = 0;

static int      CheckCntr = 0;
static int      Cntr = 0;

// The following maps are out here so as not to make the stack too big

// map to deinterleave camera data for 128x86
static int deIntlvMap[NUM_ROWS_IN_CAM_FOR_SUBAPS * NUM_PIX_IN_ROW_FOR_SUBAPS];
// temporary map, before removing every fifth column
static int tmpMap[NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];
// temporary map, before removing every fifth column
static int tmpMap160[NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160];
// map to deinterleave camera data for 160x160
static int deIntlvMap160[NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160];
// temporary destination, before removing every fifth column
static unsigned short tmpDest[ NUM_ROWS_IN_CAM_ACTUAL ][NUM_PIX_IN_ROW_ACTUAL];

// Local Function Prototypes
static void         WFSCam_Init( void );
static unsigned int WFSCam_IntrHandler(unsigned int irq, struct rtl_frame *regs);
static void         WFSCam_ProcCmd( short cmdNdx );
static void         WFSCam_SendCmd( short cmd, short param );
static void         WFSCam_LoadDataBuffer( int bufNdx );
static void         WFSCam_DeIntlv128x86( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]);
static void         WFSCam_DeIntlv128x128( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]);
static void         WFSCam_LoadSimWfsData( unsigned short buf_array[], int curbuf );
static void         WFSCam_LoadSimTTData( unsigned short buf_array[] );
static void         WFSCam_LoadSimPupilMoveData( unsigned short buf_array[] );
static void         WFSCam_LoadSimZernikes( unsigned short buf_array[] );
static void         WFSCam_ComputeZernikes( void );

static int lock_pdv_multibuf(PdvDev  *pdv_p, int numbufs);

volatile static unsigned short *pImage = NULL; // really should be protected by a mutex (posibly even a condition variable) instead of volatile, but we'll run with this for testing  COMBINED

// Copied from:
//
// http://software.intel.com/en-us/articles/memcpy-performance

void *sse_memcpy (void *__restrict__ b, const void *__restrict__ a, size_t n){
    char *s1 = b;
    const char *s2 = a;
    for(; 0<n; --n)*s1++ = *s2++;
    return b;
}


int lock_pdv_multibuf(PdvDev  *pdv_p, int numbufs) {
    int res;
    wfsLock();
    printf("&&&pdv_multibuf hrt lock\n");
    res = pdv_multibuf(pdv_p, numbufs);
    wfsUnlock();
    return res;
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam( void* arg )
 *
 *  @brief
 *  This unit is the WFS camera thread mainline.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the WFS camera and then wait for commands
 *  from the host or data from the camera (via the camera interrupt service routine) and
 *  then take action as appropriate.
 *  @par
 *
 *  @b Usage: WFSCam( arg );
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
WFSCam( void *arg )
{
    boolean     waitBeforeIntrEnable;
    int         curbuf = 0;
    static int  doneCount = 0;
    static int  lastDoneIntr = 0;

#if ( TIMER_TEST != 0 )
    int i;
#endif

    hrtCamDataPtr->edtBufStat = eEdtBufStatNotInitted;
    hrtCamDataPtr->intrIgnore = false;

 
    //printf("    hard WFSCam() CPU:thread %d:%d\n",
    //getcpuid(), pthread_self());   // sla debugging hang
  

    //    perform WFS camera initializations (including initializing the camera interrupt)
    WFSCam_Init();
    printf("    WFSCam(): init 1 done\n");   // sla debugging hang

    //    send commands to initialize the EDT camera interface in user space
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInit2 -- start");

    sleep(2);

    WFSCam_SendCmd (eWFSInitialize2, 0);
    printf("    WFSCam(): init 2 sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInit2 -- end");


    sleep(2);
#if 0
    WFSCam_SendCmd (eWFSSetRep, LegalFrameRates[ hrt_camFrameRate ]);
    printf("    WFSCam(): SetRep sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInitRep -- end");


    sleep(2);

    WFSCam_SendCmd (eWFSSetAtten, (3 - hrt_camGain));
    printf("    WFSCam(): SetAtten sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInitAtten -- end");
#endif

    CamWFSIntrWatchCntr = 0;

    
    cpu_set_t mask;
    int      cpuNum = HRT_CPU_WFS_CAM;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam) CPU requested: ", cpuNum);
    if( sched_setaffinity( 0, sizeof( mask ), &mask ) != 0 )
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFSCam) executing sched_setaffinity() for CPU #",  cpuNum);
    sched_getaffinity( 0, sizeof( mask ), &mask );
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam): CPU_ISSET: ", CPU_ISSET( cpuNum, &mask));


    printf("&&&WFS_dev=%p\n", WFS_dev);
    if (WFS_dev == NULL)
    {
        // suspect that this is a deadly condition
        printf("WFSCam(): WFS_dev NULL\n");
    }

    printf("    WFSCam(): into infloop\n"); // sla debugging hang

    // enter infinite loop
    while (!QuitCamera_WFS_Thread)
    {
        // This semaphore is currently posted in two places:
        // In sysCon in response to WFS Host commands and in
        // IntrHandler in response to incoming frame data.
        if (rtl_sem_trywait(&WFSSem) < 0)
        {
            // we were not able to lock WFSSem
            // but that is okay and normal unless
            if (rtl_errno != EAGAIN)
            {
                // this printf may be dangerously verbose
                printf("WFSCam(): trywait errno %d\n", rtl_errno);
            }
            CamWFStrywaitCntr++;    // bump up watchdog counter
            CamWFSLastPath = "trywait fail";
            rtl_usleep(1);
            // go back to top of while and again try to lock WFSSem
            continue;
        }
        // if we are here then we were able to lock WFSSem
        // this should mean that there is an image ready
        printf("&&& got sem\n");
#       if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterWFSWake);
#       endif   /* TIMER_TEST */

        waitBeforeIntrEnable = false;

        if(  hrtCamDataPtr == NULL )
        {
            printf("&&&loop hrtCamDataPtr == NULL\n");
            CamWFSnullCDPCntr++;    // bump up watchdog counter
            CamWFSLastPath = "hrtCamDataPtr == NULL";
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (WFSCam): hrtCamDataPtr is NULL" );
        }
        else if (hrtCamDataPtr->cmdStart != eCmdNone)
        {
            printf("&&&loop need to handle a command\n");
            CamWFScmdStartCntr++;   // bump up watchdog counter
            CamWFSLastPath = "cmdStart";
           
            // if a command is pending, call WFSCam_ProcCmd() to process it
            WFSCam_ProcCmd(hrtCamDataPtr->cmdStart);
            hrtCamDataPtr->cmdStart = eCmdNone;
        }
        else if (hrtCamDataPtr->intrIgnore)
        {
            printf("&&&loop intrIgnore\n");
            // ignore the interrupt if we're doing serial communications
            CamWFSintrIgnoreCntr++; // bump up watchdog counter
            CamWFSLastPath = "intrIgnore";

        }
        else
        {
            printf("&&&loop  DEL-cam proc interrupt\n");
            // there was nothing else more important
            // process the interrupt
            CamWFSIntrWatchCntr++;  // bump up WFS camera watchdog counter
            CamWFSLastPath = "proc interrupt";

#           if (CAM_TYPE != WFS_REAL_GPI)
            // get the current done counter -- must be simulation mode
            doneCount = edtDoneCount_sim;
#           else   /* CAM_TYPE says we have the real camera */
            // not real time system but real camera   COMBINED
            // do nothing here, handle it in WFSCam_LoadDataBuffer
            doneCount++;
            printf("&&&loop skip to handle loading of data in WFSCam_LoadDataBuffer\n");
#           endif  

            printf("&&&loop doneCount=%d\n", doneCount);
            if (doneCount < 7)
            {
                printf("&&& doneCount < 7\n");
                // doneCount goes to zero when switching modes,
                // let it settle
                lastDoneIntr = doneCount;
            }
            else if (lastDoneIntr == doneCount)
            {
                printf("&&& lastDoneIntr == doneCount\n");
                // interrupt could be a serial interrupt or sg ready
                // (not sure what "sg ready" is)
                // note -- not doing anything with BadCntr at present
                BadCntr++;
            }
            else
            {
                printf("&&&loop everything's okay, so proceed - LoadDataBuffer\n");
                // everything's okay, so proceed
#               if ( TIMER_TEST != 0 )
                camFrameID++;
                timerNdx = camFrameID % NUM_TIME_SETS;
                for (i = 0; i < eTimerMaxTimes; i++)
                    timerTimes[timerNdx][i] = intrTime;
                timerTimes[timerNdx][eTimerLastIntr] = prevIntrTime;
                prevIntrTime = intrTime;
                // No need to set eTimerThisIntr, the above loop
                // took care of it.
                // timerTimes[timerNdx][eTimerThisIntr] = intrTime;
                timerTimes[timerNdx][eTimerAfterWFSWake] = afterWFSWake;
                timerTimes[timerNdx][eTimerAfterEDTIntrProc] = afterEDTIntrProc;
                timerTimes[timerNdx][eTimerAfterWFSIRQEnable] = afterWFSIRQEnable;
#               endif   /* TIMER_TEST */

                lastDoneIntr = doneCount;

                // the last DMA buffer is one less than DoneCount
                curbuf = (doneCount - 1) % WFS_BUFFERS;

                // This next step represents the majority of the
                // local processing time.
                WFSCam_LoadDataBuffer(curbuf);
            }
        } // end else we actually processed the interrupt



        hrtFileDataShmPtr->watchDogBits |= WatchDog_CamBit;
        interruptWatch = 0;
        printf("&&&loop end\n");
    } // end while (!QuitCamera_WFS_Thread)

    // if we are here then we must have been told to quit
    return NULL;
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_Init( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *  @par
 *
 *  @b Usage: WFSCam_Init( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

static void
WFSCam_Init( void )
{
    int i;

//    make sure buf_array pointer start out as null
    for (i = 0; i < WFS_BUFFERS; i++)
    {
        buf_array_160[i] = NULL;
    }

//    initialize variables
    hrtCamDataPtr->cmdStart = eCmdNone;
    hrtCamDataPtr->cmdCrnt = eCmdNone;
    hrtCamDataPtr->stateHRT = 0;
    printf("state1 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);

    hrtCamDataPtr->camWriteNdx = 0;    // Indexs are incremented at end

    hrtCamDataPtr->edtBufStat = eEdtBufStatNotInitted;
    hrtCamDataPtr->intrIgnore = false;

//    initialize the circular buffer status flags
    for( i=0; i<NUM_HRT_DATA_BUFFS; i++)
    {
        (hrtDataFlowControlPtr->bufStat[i]).hrtBufState = eBufIdle;
        (hrtDataFlowControlPtr->bufStat[i]).dispDoUpdate = false;
        (hrtDataFlowControlPtr->bufStat[i]).inDisplayProcess = false;
        (hrtDataFlowControlPtr->bufStat[i]).inSaveProcess = false;
    }

    hrtCamDataPtr->rawDispWriteBuffNdx = 0;
    hrtCamDataPtr->rawDispReadBuffNdx = 1;


    // Let sysCon finish getting file data from the SRT before continuing.
    printf("          WFSCam_Init: starting to spin on !OkayToFinishInit\n");
    hrtCamDataPtr->stateHRT++;
    printf("state2 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);
    while (!OkayToFinishInit)
    {
        // Sleep for a tenth of a second.
        rtl_usleep(100000);
    }
    printf("          WFSCam_Init: Ending spin on !OkayToFinishInit\n");
    hrtCamDataPtr->stateHRT++;
    printf("state3 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);


    // send command to do the first step of EDT camera interface
    // initialization in user space  and wait a second for that to happen
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInit1 -- start");
    WFSCam_SendCmd (eWFSInitialize1, 0);
    hrtCamDataPtr->stateHRT++;
    printf("state4 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);
    rtl_usleep(1000000);
    printf("state5 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CCC camInit1 -- end");

#   if (CAM_TYPE == WFS_REAL_GPI)
#   if (RTL_SYSTEM == 0)
    // user-side mode
    if (WFS_dev == NULL)
    {
        // this shouldn't happen, but check anyway
        printf("WFSCam_Init(): user WFS_dev NULL\n");
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFSCam_Init) WFS_dev not set yet");
    }
    else if (lock_pdv_multibuf(WFS_dev, WFS_BUFFERS) != 0)   // ((pdv_multibuf(WFS_dev, WFS_BUFFERS)) != 0)
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFSCam_Init) failed to configure ring buffers");
    else
    {
        unsigned char **tmp_array = edt_buffer_addresses(WFS_dev);
        for (i = 0; i < WFS_BUFFERS; i++)
            buf_array_160[i] = (unsigned short *)tmp_array[i];
    }
#   else    /* RTL_SYSTEM */
    // RTLinux mode
    if (WFS_dev == NULL)
    {
        // this shouldn't happen, but check anyway
        printf("WFSCam_Init(): RTL WFS_dev NULL\n");
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFSCam_Init) WFS_dev not set yet");
    }
    else
    {
        for (i = 0; i < WFS_BUFFERS; i++)
            buf_array_160[i] = wfsCamBuffsForRTL[i];
    }
#endif  /* RTL_SYSTEM */
#endif  /* CAM_TYPE */
    hrtCamDataPtr->stateHRT++;
    printf("state6 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);


    // save data for simulated data mode
    for (i = 0; i < WFS_BUFFERS; i++)
    {
        buf_array[i]     = wfsCamBuffsFor128[i];  // added for use with software ROI from 160x160 to 128x128
        buf_array_sim[i] = wfsCamBuffsForSim[i];
    }

    printf("&&& CamWFSInitDone - allow WFSCam_DrvrWait to start taking images\n");
    CamWFSInitDone = true;
    hrtCamDataPtr->stateHRT++;
    printf("state7 H=%d S=%d\n", hrtCamDataPtr->stateHRT, hrtCamDataPtr->stateSRT);
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_Init): CamWFSInit done");
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_IntrInit( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize the wfs camera interrupt.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the wfs camera interrupt.  This
 *  MUST be separate from the other cam initialization so that it can be called
 *  from SysCon while in RTCore main() initalization mode.  Also note that
 *  gpAoSrtCamMngr_StartCam() (on the SRT side) MUST have been executed by the
 *  time this function is called.
 *  @par
 *
 *  @b Usage: WFSCam_IntrInit( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

void
WFSCam_IntrInit( void )
{
#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    unsigned long affinity = 1 << HRT_CPU_WFS_CAM;

    // get the IRQ# from the EDT driver and install a handler for it
    WFS_irq = edt_get_irq_value(WFS_UNIT_NUM);

    if (WFS_irq < 0)
    {
        printf("WFSCam_IntrInit(): WFS_irq < 0\n");
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFSCam_Init) failed to get irq value: ",  WFS_irq);
    }
    else
    {
        printf("WFSCam_IntrInit(): getting WFS_dev\n");
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_Init) WFS camera using irq ", WFS_irq);

        WFS_dev = edt_get_dev_from_irq(WFS_irq);
        edt_take_interrupt(WFS_irq, WFS_dev);

        if ((rtl_request_irq( WFS_irq, WFSCam_IntrHandler )) != 0)
            SysConSendStrngNoParm(eDebugLevelErr, "ERR in rtl_request_irq()");

        if (rtl_irq_set_affinity(WFS_irq, &affinity, &oldAffinity) != 0)
            SysConSendStrngNoParm(eDebugLevelErr, "ERR in rtl_irq_set_affinity()");
    }
#endif
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_IntrHandler( unsigned int irq, struct rtl_frame *regs )
 *
 *  @brief
 *  Process commands originating in the HRT Camera control component (File: WFSCam.c) .
 *
 *  @b DESCRIPTION:
 *  This routine is called in response to an interrupt generated by the EDT driver upon
 *  receipt of a complete frame of camera data.  It's sole purpose is to check error
 *  conditions and activate a semaphore which triggers the processing of the camera frame data.
 *  @par
 *
 *  @b Usage: WFSCam_IntrHandler(  unsigned int irq, struct rtl_frame *regs );
 *
 *  @param[in] irq (int):
 *  @param[in] regs (struct rtl_frame *):
 *
 *  @return 0: None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static unsigned int
WFSCam_IntrHandler(unsigned int irq, struct rtl_frame *regs)
{
#if ( TIMER_TEST != 0 )
    rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#endif

#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    if (++interruptWatch < 1000000)
    {
        CamWFSIntrHandler++;
        // unlock WFSSem
        rtl_sem_post(&WFSSem);
    } else {
        if (interruptWatch%100000 == 0) printf("HIT interruptWatch=%d\n", interruptWatch);
    }
#endif
    return 0;
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_ProcCmd( short cmdNdx )
 *
 *  @brief
 *  Process camera commands received from the host.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process camera commands received from the host,
 *  via the SRT side, such as automatically setting the optimal frame rate, forcing
 *  the frame rate to a particular value, etc.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_ProcCmd( short cmdNdx );
 *
 *  @param[in] cmdNdx (short): index into CmdTable[] for the command to be processed
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFSCam_ProcCmd( short cmdNdx )
{
//    switch on command ID corresponding to command index
    switch (CmdTable[cmdNdx].idNum)
    {
    case eCmdAbort:       // abort long commands
        SysConSendStrngNoParm(eDebugLevelErr, "ERR (cam_WFS.c) unexpected command abort received by cam");
        break;

    case eCmdCamWFSInit:   // initialize SciMeasure WFS camera  // nobody calls this
        printf("WFSCam_ProcCmd eCmdCamWFSInit");
        WFSCam_SendCmd(eWFSInitialize1, 0);
        rtl_usleep(3000000);

        WFSCam_SendCmd(eWFSInitialize2, 0);
        rtl_usleep(7000000);

        WFSCam_SendCmd(eWFSSetRep, LegalFrameRates[ hrt_camFrameRate ]);
        WFSCam_SendCmd(eWFSSetAtten, (3 - hrt_camGain));
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamRateAuto:      // do 'autoCamRate' setting
        {
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (Cam_ProcCmd) Command is not supported");
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        }
        break;

    case eCmdCamRate:      // set frame rate of the camera (per second)
        {
        int tmpRate = hrtCamDataPtr->cmdIntParam[0];
        CamFrameRateNdx = 0;  // find a legal camera rate
        /* WARNING:  The following code ASSUMES that the LegalFrameRates array is ordered. */
        while ((tmpRate > LegalFrameRates[CamFrameRateNdx]) && (LegalFrameRates[CamFrameRateNdx + 1] != -1))
            CamFrameRateNdx ++;
        hrt_camFrameRate = CamFrameRateNdx;
        WFSCam_SendCmd( eWFSSetRep, LegalFrameRates[hrt_camFrameRate] );
        gpAoHrtFileData_Put( eFileTypeParms );
        gpAoHrtCent_GetCrntDarkBackFlat( hrt_camFrameRate );  // get dark, back, and flat data for current frame rate
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        }
        break;

    case eCmdCamGain:      // set camera gain (0, 1, 2, or 3)
        hrt_camGain = hrtCamDataPtr->cmdIntParam[0];
        WFSCam_SendCmd (eWFSSetAtten, (3 - hrt_camGain));
        gpAoHrtFileData_Put(eFileTypeParms);
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamSRate:       // read frame rate from camera
        WFSCam_SendCmd (eWFSReadRep, 0);
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamSGain:     // read gain from camera
        WFSCam_SendCmd (eWFSReadAtten, 0);
        hrtCamDataPtr->cmdCrnt = eCmdNone;
        break;

    default:
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (cam_WFS.c): unexpected command received by cam, cmdNdx = ", cmdNdx);
        break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_SendCmd( short cmd, short cmdParam )
 *
 *  @brief
 *  Send a command and associated parameter (cmd dependent) to the SRT camera manager
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send messages to the SRT side (by using the FIFO diagnostic
 *  message process) to write commands to the camera and/or to change the DMA mapping (to accommodate
 *  changes in frame size).
 *  @par
 *
 *  @b Usage:
 *  WFSCam_SendCmd( short cmd, short cmdParam );
 *
 *  @param[in] cmd (int):  Cmd from HRT side that needs to be passed to SRT side manager
 *  @param[in] cmdParam (int): Paramter value (use depends on the command) for this particular command
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFSCam_SendCmd( short cmd, short param )
{
    static diagFIFOMsgType diagFIFOMsg;

//    set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagWFSCmd;
    diagFIFOMsg.cmdNdx = cmd;
    diagFIFOMsg.intParam[0] = param;

    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (cam_WFS.c): error writing WFSCam command to diag FIFO, errno = ", rtl_errno);
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_LoadDataBuffer( int bufNdx )
 *
 *  @brief
 *  Process frames from the WFS camera.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process frames from the WFS camera.  This means
 *  finding the next available slot in the camera data circular buffer, de-interleaving
 *  the data from the camera's several amplifiers, and alerting the centroider that
 *  new data is available.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_LoadDataBuffer( int bufNdx );
 *
 *  @param[in] bufNdx (int):  index of the SciMeasure camera buffer to process
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInvalidFileCmd
 *  @exception exceptionFileErr
 *
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFSCam_LoadDataBuffer(int bufNdx)
{
    int                         i, j;
#if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    static const int            pageSizeInUshorts = RTL_PAGE_SIZE / 2;
#endif
    long                        tmpWrtNdx;
    CamBuffType*                camBuffPtr;
    DataBufferElemStatusType*   bufStatPtr;

    static int dispCntr = 0;
    static int overflowCntr = 0, overflowCheckCntr = 0;
    printf("&&& _LoadDataBuffer\n");
    // Set a temp write index to cameraWriteNdx for validation
    //
    tmpWrtNdx = hrtCamDataPtr->camWriteNdx;
    bufStatPtr = &(hrtDataFlowControlPtr->bufStat[ tmpWrtNdx ]);

    // if this buffer is busy, then we have an overrun condition ( diag data dump, processing, (or display??) )
    if ( bufStatPtr->hrtBufState != eBufIdle )
    overflowCntr++;
    if ( ++overflowCheckCntr >= LegalFrameRates[hrt_camFrameRate] )    // once a second
    {
        overflowCheckCntr = 0;
        if (overflowCntr > 0)
        {
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFSCam_LoadDataBuffer) circ. buf overrun: (chk 1/sec), writeNdx = ", tmpWrtNdx);
            overflowCntr = 0;
        }
    }

    if ( bufStatPtr->inDisplayProcess != false )  // Highly unexpected
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFSCam_LoadDataBuffer) display overrun - this frame was not processed for display");

    if ( bufStatPtr->inSaveProcess != false )      // Somewhat expected - particularly at debug time
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFSCam_LoadDataBuffer) 'save to file' overrun - this frame is in midst of save to disk - buff Ndx #", tmpWrtNdx );

    // At this point this frame buffer appears to be viable (possibly with Warning(cam_WFS.c)s!)
    //    - set frame state to cam Processing
    //    - update the camWriteNdx
    //    - set Point to buffer
    bufStatPtr->hrtBufState = eBufCamProc;
    camBuffPtr = &(hrtCamDataPtr->camBuffs[ tmpWrtNdx ]);

    // determine if the frame will have to be sent to SRT for display
    //     - if so - flag it here - the last HRT process (currently recon) will send FIFO message to SRT based on 'bufStatPtr->dispDoUpdate'
    //
    if( hrtDataFlowControlPtr->dispRate != 0)
    {
        if( ++hrtDataFlowControlPtr->dispCntr >= (float)LegalFrameRates[ hrt_camFrameRate ] / hrtDataFlowControlPtr->dispRate )
        {
            hrtDataFlowControlPtr->dispCntr = 0;
            bufStatPtr->dispDoUpdate = true;
            if( ++(hrtDataFlowControlPtr->nextDispBuf) >= NUM_SRT_DISPLAY_BUFS )
                hrtDataFlowControlPtr->nextDispBuf = 0;
        }
        else
            bufStatPtr->dispDoUpdate = false;
    }



#   if  ((CAM_TYPE == WFS_REAL_GPI) )  // COMBINED
    // skip the copy operation and work directly with the camera buffer when using a real camera but not a real time system
    {
        printf("handling pImage\n");
        unsigned short *pI;
        pI = (unsigned short*)pImage;
        pImage = NULL;
        if (pI != NULL) {
            WFSCam_DeIntlv128x128( pI, (void *)&camBuffPtr->usedCamData[0][0][0] );
        }
    }
#   else // simulated camera or realt time system
    if (hrt_useWfsSimData == 1)  // if we're using simulated data
    {
        WFSCam_DeIntlv128x128( buf_array_sim[bufNdx], (void *)&camBuffPtr->usedCamData[0][0][0] );
    }

#endif

    //    tell the Centroid thread that data's ready
    bufStatPtr->hrtBufState = eBufReqCent;

    if (rtl_sem_post( &CentSem ) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFSCam_LoadDataBuffer) posting to CentSem, errno = ", rtl_errno);

#if ( TIMER_TEST != 0 )
    rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[timerNdx][eTimerAfterCentPost]);
#endif

    // now that we've sent the data off to the centroider, move the
    // temporary destination matrix from WFSCam_DeIntlv128x128()
    // to a raw display buffer at 10 fps -- this will get picked up
    // by dispraw, if dispraw is running
    if (++dispCntr >= (LegalFrameRates[hrt_camFrameRate] / 10))
    {
        dispCntr = 0;

        hrtCamDataPtr->rawDispWriteBuffNdx ^= 0x1;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                hrtCamDataPtr->rawDispBuffs[hrtCamDataPtr->rawDispWriteBuffNdx][i][j] = tmpDest[i][j];
        }
        hrtCamDataPtr->rawDispReadBuffNdx = hrtCamDataPtr->rawDispWriteBuffNdx;
    }

    if ( tmpWrtNdx % LegalFrameRates[ hrt_camFrameRate ] == 0 )
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_LoadDataBuffer): Buf #", tmpWrtNdx  );

    if( ++tmpWrtNdx >= NUM_HRT_DATA_BUFFS)
    {
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_LoadDataBuffer): Normal Buffer Wrap, WriteNdx = ", tmpWrtNdx);
        tmpWrtNdx = 0;
    }
    hrtCamDataPtr->camWriteNdx = tmpWrtNdx;
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_DeIntlv128x86( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x86 length camera frame into a 96x96 destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x86 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFSCam_DeIntlv128x86( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW])
{
    int i, j, k;
    int colCntr, colSkipper, rowCntr;
    static boolean firstTime = TRUE;

    if (hrt_useWfsSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[NUM_SEGS_IN_CAM_160] = { 12, 8, 4, 0, 2, 6, 10, 14, 15, 11, 7, 3, 1, 5, 9, 13 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_DeIntlv128x86): Setting up de-interleave map");
            firstTime = false;

            // first create a map to deinterleave into 128 element rows
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (NUM_SEGS_IN_CAM / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
                    {
                        tmpMap[i * NUM_PIX_IN_ROW_ACTUAL +
                               j * NUM_PIX_IN_SEG_ROW +
                               k] =
                            i * NUM_PIX_IN_ROW_ACTUAL * 2 + k * NUM_SEGS_IN_CAM + segOrder[j];

                        tmpMap[(NUM_ROWS_IN_CAM_ACTUAL - 1 - i) * NUM_PIX_IN_ROW_ACTUAL +
                               (NUM_SEGS_IN_CAM / 2 - 1 - j) * NUM_PIX_IN_SEG_ROW +
                               (NUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * NUM_PIX_IN_ROW_ACTUAL * 2 + k * NUM_SEGS_IN_CAM + segOrder[j + NUM_SEGS_IN_CAM / 2];
                    }
                }
            }

            // now skip every third column on a per row basis -- this gets us down to the final map
            k = 0;
            for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                {
                    if (((j + 1) % 3) > 0)
                    {
                        deIntlvMap[k] = tmpMap[i * NUM_PIX_IN_ROW_ACTUAL + j];
                        k++;
                    }
                }
            }
        }

        // deinterleave the source array into the destination matrix, offsetting the destination matrix as necessary
        // > (IMPORTANT: it is assumed that the unused part of the destination matrix is never written to; so, it is
        // > not set to zeroes here)
        k = 0;
        rowCntr = 0;                                    // this is for dispraw and can be removed when dispraw isn't needed
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;                                // this is for dispraw and can be removed when dispraw isn't needed
            colSkipper = 0;                             // this is for dispraw and can be removed when dispraw isn't needed
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[deIntlvMap[k]];
                tmpDest[rowCntr][colCntr] = dest[i][j]; // this is for dispraw and can be removed when dispraw isn't needed
                colCntr += 1 + colSkipper;              // this is for dispraw and can be removed when dispraw isn't needed
                colSkipper ^= 0x1;                      // this is for dispraw and can be removed when dispraw isn't needed
                k++;
            }
            rowCntr++;                                  // this is for dispraw and can be removed when dispraw isn't needed
        }
    }
    else
    {
        // save data for dispraw
        k = 0;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
            }
        }

        // don't de-interleave, if we're using simulated data (but skip every third column)
        rowCntr = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr];
                colCntr += 1 + colSkipper;
                colSkipper ^= 0x1;
            }
            rowCntr++;
        }
    }
}

/*!
 *******************************************************************************
 *
 *  @fn WFSCam_DeIntlv128x128( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x128 length camera frame into a 96x96 length destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x128 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *  This unit also calls a function to display the raw camera data to the monitor
 *  at 20 fps.  For this reason and because de-interleaving is less efficient, this
 *  function is quite a bit slower than WFSCam_DeIntlv128x86 and, so,  should only
 *  be used for test purposes.
 *  @par
 *  Quantifying 'slow': in December 2012 on rtc.ucolick.org, in the best
 *  case, this takes 12 microseconds to run; in the worst case, it takes
 *  about 65 microseconds. That's slightly less than 50% of the total
 *  per-frame computation time of the WFS camera thread.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- start XXX
// XXX this could definitely be more efficient !! XXX
static void
WFSCam_DeIntlv128x128( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW])
{
    int i, j, k;
    int rowCntr, rowSkipper, colCntr, colSkipper;
    static boolean firstTime = TRUE;

    if ( ++CheckCntr >= TEST_INTERVAL * LegalFrameRates[hrt_camFrameRate] )
    {
        CheckCntr = 0;
        Cntr = 1;
    }

    if (hrt_useWfsSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[NUM_SEGS_IN_CAM_160] = { 3, 13, 0, 10, 4, 14, 1, 11, 2, 12, 17, 7, 16, 6, 19, 9, 15, 5, 18, 8 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_DeIntlv128x128): Setting up de-interleave map");
            firstTime = false;

            // create a map to deinterleave into 128 element rows
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACT_160 / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (NUM_SEGS_IN_CAM_160 / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
                    {
                        tmpMap160[i * NUM_PIX_IN_ROW_ACT_160 +
                                  j * NUM_PIX_IN_SEG_ROW +
                                  k] =
                            i * NUM_PIX_IN_ROW_ACT_160 * 2 + k * NUM_SEGS_IN_CAM_160 + segOrder[j];

                        tmpMap160[(NUM_ROWS_IN_CAM_ACT_160 - 1 - i) * NUM_PIX_IN_ROW_ACT_160 +
                                  (NUM_SEGS_IN_CAM_160 / 2 - 1 - j) * NUM_PIX_IN_SEG_ROW +
                                  (NUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * NUM_PIX_IN_ROW_ACT_160 * 2 + k * NUM_SEGS_IN_CAM_160 + segOrder[j + NUM_SEGS_IN_CAM_160 / 2];
                    }
                }
            }

            //        rotate the map 180 degrees
            for (i = 0; i < NUM_ROWS_IN_CAM_ACT_160; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACT_160; j++)
                    deIntlvMap160[i * NUM_PIX_IN_ROW_ACT_160 + j] = tmpMap160[i * NUM_PIX_IN_ROW_ACT_160 + j];
            }
        }

        //    deinterleave the source array into a temporary destination matrix -- deinterleaving could be done more quickly if
        //    > we didn't go through tmpDest[][] -- but, for now, tmpDest[][] is needed for dispraw
        k = 0;    // mrr DM

        rowCntr = ROWS_OFFSET_160;        // mrr 48
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)    // mrr 64
        {
            colCntr = PIX_IN_ROW_OFFSET_160;    // mrr 48
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)    // mrr 64
            {   // starting at row 48, col 48 copy 64 pixel for 64 rows.  tmpDest now holds the center 64x64 pixels of the 160x160 array
                tmpDest[i][j] = src[deIntlvMap160[rowCntr * NUM_PIX_IN_ROW_ACT_160 + colCntr]] >> 2;    // mrr DM    // mrr 160 // mrr why >> 2? to reduce 16 bits to 14 ???
                //tmpDest[i][j] = k++;
                colCntr++;
            }
            rowCntr++;
        }

        // now skip every fifth column on a per row basis and every fifth row, offsetting the destination matrix
        // > as necessary -- this gets us down to the final frame for processing
        rowCntr = 12;    // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)    // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;    // the start in tmpDest
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)    // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {    // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {    // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }    // end of not using sim data
    else
    {    // using sim data
        //    save data for dispraw
        k = 0;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
                //tmpDest[i][j] = src[i][j];
                if (Cntr == 1)
                {
                //    printf( "%d ", tmpDest[i][j]);
                }
            }
            if (Cntr == 1)
            {
                //printf( "\n ");
            }
        }

        rowCntr = 12;   // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)    // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;    // the start in tmpDest
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)    // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {    // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {    // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }
    Cntr = 0;
}

// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- end XXX


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_DrvrWatch( void* arg )
 *
 *  @brief
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the wfs camera or load simulated data when in user-side mode.
 *
 *  @b DESCRIPTION:
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the wfs camera or load simulated data when in user-side mode.
 *  @par
 *  When in RTL mode, the thread continually sets a 7 second timer and, whenever
 *  it times out, checks a WFS camera frame counter to make sure that frames have
 *  been received.  If no frames have been received, an error condition is raised.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_DrvrWatch(void* arg);
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception exceptionCameraTimout  The camera frames have been missing for > 7 sec.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
WFSCam_DrvrWatch( void *arg )
{
    int res = -1; 
    SysConSendStrngIntParm(eDebugLevelMax, "INFO Camera Type: ", CAM_TYPE);

#if (RTL_SYSTEM == 0)
    cpu_set_t mask;
    int      cpuNum = HRT_CPU_WFS_CAM;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_DrvrWatch) - CPU #",  cpuNum );
    if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFSCam_DrvrWatch) executing sched_setaffinity() for CPU #",  cpuNum );
    sched_getaffinity( 0, sizeof( mask ), &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_DrvrWatch) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else   /* RTL_SYSTEM */
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFSCam_DrvrWatch) CamWatch thread id: ", rtl_pthread_self());
#endif  /* RTL_SYSTEM */

#if (RTL_SYSTEM != 0)
    printf("    hard WFSCam_DrvrWatch() CPU:thread %d:%d\n",
    rtl_getcpuid(),rtl_pthread_self());   // sla debugging hang
#endif

    //  spin on the InitDone flags for the other threads
    while (!CentInitDone || !ReconInitDone || !CamWFSInitDone)
    {
        // Sleep for a tenth of a second.
        rtl_usleep(100000);
    }
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFSCam_DrvrWatch) Initialization appears to be done");
    printf("    hard WFSCam_DrvrWatch() infloop\n");

    wfsLock();
    res = pdv_get_timeout(WFS_dev);
    wfsUnlock();
    printf("&&& pdv_get_timeout=%d\n", res);

    while (!QuitCamWFSWatchThread)
    {
        boolean postWfsSem;
        postWfsSem = false;

#if (CAM_TYPE != WFS_REAL_GPI)  // no real camera
        //{
            // if we don't have a real camera, use the system clock to
            // simulate a frame rate
        //  if ( LegalFrameRates[ hrt_camFrameRate ] != 0 )
        //  {
        //      rtl_usleep(1000000 / LegalFrameRates[ hrt_camFrameRate ]);
        //  }
        //  else
        //  {
        //      rtl_usleep(1000000);
        //  }
        //
        //  hrt_useWfsSimData = 1;  // make sure we're using simulated data
        //  postWfsSem = true;
        //
        //#if ( TIMER_TEST != 0 )
        //    rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
        //#endif  /* TIMER_TEST */
            //   }
        printf("&&& darn\n");
#else  // (CAM_TYPE == WFS_REAL_GPI)
#if (RTL_SYSTEM == 0) // real camera, user-side mode -- this will only work for the user-side code!!
        {
            static int timeouts, last_timeouts = 0, recovering_timeout = FALSE;
            unsigned short *pI;  
            printf("&&&pdv_wait_image\n");
            wfsLock();
            pI = (unsigned short*)(pdv_wait_image(WFS_dev));       // wait for the last image
            if (pImage != NULL) 
            {
                printf("pImage not handled before new image taken!!!\n");
            }
            pImage = pI;
            wfsUnlock();

#if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#endif  /* TIMER_TEST */

            wfsLock();
            //printf("&&&pdv_start_images 1 hrt\n"); 
            //pdv_start_images(WFS_dev, 1);  // start the next image
            
            // check for a camera timeout
            timeouts = pdv_timeouts(WFS_dev);
            wfsUnlock();

            printf("&&&pdv_timeouts timeouts=%d last_timeout=%d\n", timeouts, last_timeouts);
            if (timeouts > last_timeouts)
            {
                printf("&&& timed out\n");
                wfsLock();
                pdv_timeout_restart(WFS_dev, TRUE);
                wfsUnlock();
                last_timeouts = timeouts;
                recovering_timeout = TRUE;
                SysConSendStrngIntParm(eDebugLevelWarn, "WARN - SMC WFS camera user mode timeout", timeouts);
            }
            else if (recovering_timeout)
            {
                printf("&&& timeout_recovery\n");
                wfsLock();
                pdv_timeout_restart(WFS_dev, TRUE);
                wfsUnlock();
                recovering_timeout = FALSE;
            }
            else
            {
                printf("&&& setting postWfsSem = true\n");
                // post semaphore
                postWfsSem = true;
            }
        }

#else  // real camera, RTL mode -- this is just the camera watchdog timer
        {
            static struct timespec ts;

            // wait for 7 seconds (increased from 1 sec to accommodate longer delay when switching camera programs)
            rtl_usleep(7000000);

            rtl_clock_gettime(RTL_CLOCK_REALTIME, &ts);

            // make sure we've received camera frames and, if not - inform the user
            if (CamWFSIntrWatchCntr == 0)
            {
                printf("%ld bc %d tw %d nc %d cs %d ii %d ebi %d ebn %d ih=%d wait=%d iw=%d LP=%s  NW=%s\n",
                       ts.tv_sec,
                       BadCntr             ,
                       CamWFStrywaitCntr   ,
                       CamWFSnullCDPCntr   ,
                       CamWFScmdStartCntr  ,
                       CamWFSintrIgnoreCntr,
                       CamWFSedtBufIniCntr ,
                       CamWFSedtBufNewCntr,
                       CamWFSIntrHandler,
                       CamWFSWaitBefore,
                       interruptWatch,
                       CamWFSLastPath,
                       CamWFSLastNonWatchdog);

                if (strcmp(CamWFSLastPath, "watchdog") != 0) {
                    CamWFSLastNonWatchdog = CamWFSLastPath;
                }

                BadCntr             = 0;
                CamWFStrywaitCntr   = 0;
                CamWFSnullCDPCntr   = 0;
                CamWFScmdStartCntr  = 0;
                CamWFSintrIgnoreCntr= 0;
                CamWFSedtBufIniCntr = 0;
                CamWFSedtBufNewCntr = 0;
                CamWFSIntrHandler   = 0;
                CamWFSLastPath = "watchdog";

                SysConSendStrngNoParm(eDebugLevelWarn, "WARN - SMC WFS camera RTL watchdog timeout");
            }
            else if (CamWFSIntrWatchCntr > 0)
            {
                printf("%ld iwc %d\n",
                ts.tv_sec,
                CamWFSIntrWatchCntr );

                CamWFSIntrWatchCntr = 0;
            }
        }
#endif  /* RTL_SYSTEM */
#endif  /* CAM_TYPE */

#if ((CAM_TYPE != WFS_REAL_GPI) || (RTL_SYSTEM == 0))
        // if we're using simulated data, load it
        printf("&&& a hrt_useWfsSimData=%d  postWfsSem=%d\n",  hrt_useWfsSimData, postWfsSem);
        if (hrt_useWfsSimData == 1)
        {
            printf("&&& using simulated data\n");
            int curbuf;
            curbuf = edtDoneCount_sim % WFS_BUFFERS;
            WFSCam_LoadSimWfsData( buf_array_sim[curbuf], curbuf );
            edtDoneCount_sim++;
        }

        // if we need to post the wfs semaphore, post it
        if (postWfsSem)
        {
            // we are not the RTL system
            // so there was no interupt to unlock the semaphore
            // so we unlock WFSSem
            printf("&&& rtl_sem_post ********\n");
            rtl_sem_post(&WFSSem);
        }
#endif
    }
    return NULL;
}


/*!
 *******************************************************************************
 *  @fn WFSCam_LoadSimWfsData( unsigned short buf_array[], int curbuf )
 *
 *  @brief
 *  This unit loads simulated wfs data into the buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated wfs data into the buffer supplied either from a
 *  file (loaded at system startup) or by generating it in one of several ways,
 *  depending on how flags are set in the code.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_LoadSimWfsData(&buf_array, curbuf);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *  @param[in] curbuf:    current wfs buffer (used to choose the right simulated data)
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
enum { eSimFromFile, eSimRamp, eSimTipTilt, eSimPupilMove, eSimZernike, eSimCombo };
//const static int simSelect = eSimZernike;  // choose simulated data    // mrr
const static int simSelect = eSimFromFile;  // choose simulated data
static unsigned short buf_array2[NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];

static void
WFSCam_LoadSimWfsData( unsigned short buf_array[], int curbuf )
{
    static int    testVal = 0;
    int        i, j;

    if (buf_array == NULL)  // make sure the buf_array pointer exists
        return;

    switch (simSelect)
    {
    case eSimFromFile:
    default:
        //    if file-based data is available, use it; otherwise, zero the buffer
        if( hrtCamDataPtr->simFileRead  )
        {
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL); i++)
            {
                for (j = 0; j < (NUM_PIX_IN_ROW_ACTUAL ); j++)
                {
                    // buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = hrtCamDataPtr->simDataBuffs[curbuf][i][j];    // mrr
                    buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = hrtCamDataPtr->simDataBuffs[0][i][j];    // we only loaded the first buffer // mrr
                }
            }
        }
        else  // in case the file doesn't exist, zero the buffer
        {
            for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                    buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = 0.0;
            }
        }
        break;

    case eSimRamp:
        // just loop through sequential data values
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = testVal;
                if (++testVal > 12345)
                    testVal = 0;
            }
        }
        break;

    case eSimTipTilt:
        WFSCam_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the wfs
        break;

    case eSimPupilMove:
        WFSCam_LoadSimPupilMoveData( buf_array );  // put low frequency plus high frequency pupil move data, in X and or Y
        break;

    case eSimZernike:
        WFSCam_LoadSimZernikes( buf_array );  // put Zernike modes on the wfs
        break;

    case eSimCombo:
        WFSCam_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the wfs
        WFSCam_LoadSimZernikes( buf_array2 );  // put Zernike modes on the wfs
        for ( i = 0; i < NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL; i++)
            buf_array[i] = buf_array[i] / 2 + buf_array2[i] / 2;
        break;
    }
}

/*!
 *******************************************************************************
 *  @fn WFSCam_LoadSimTTData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_LoadSimTTData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static const boolean makeTip  = true;  // turn tip on or off
static const boolean makeTilt = true;  // turn tilt on or off
static const boolean makeSquare = true;  // make a square wave instead of a sine wave
static const int lowFreqTT  = 1;  // 7;  //  25;  // low frequency TT in Hz  -- these may be approximate, depending on how
static const int highFreqTT = 1;  // 100;  // high frequency TT in Hz    -- > well they divide into the frame rate
static const float lowFreqGainTT  = 0.50;  // gain to be applied to low frequency TT (0.0 means no low freq)
static const float highFreqGainTT = 0.00;  // gain to be applied to high frequency TT (0.0 means no high freq)

static void
WFSCam_LoadSimTTData( unsigned short buf_array[] )
{
int         i, j;
int         rowCntr, rowSkipper, colCntr, colSkipper;
int         stepsPerCycleLow, stepsPerCycleHigh;
static int  stepCntrLow = -1, stepCntrHigh = -1;
float       lowLeftBot, lowRightTop, highLeftBot, highRightTop; // these are for low and high frequency -- left/bottom
                                                                // > pixel (for tip/tilt) and right/top pixel

    float tipVector[NUM_PIX_IN_ROW_FOR_SUBAPS], tiltVector[NUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up tip and tilt vectors
    if (lowFreqTT <= 0)
        stepsPerCycleLow = 1;
    else
        stepsPerCycleLow = LegalFrameRates[hrt_camFrameRate] / lowFreqTT;
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;

    if (highFreqTT <= 0)
        stepsPerCycleHigh = 1;
    else
        stepsPerCycleHigh = LegalFrameRates[hrt_camFrameRate] / highFreqTT;
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowLeftBot   = (1.0 + sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    lowRightTop  = (1.0 - sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    highLeftBot  = (1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;
    highRightTop = (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;

    if (makeSquare)
    {
        if (lowLeftBot < 4000)
            lowLeftBot = 0;
        else
            lowLeftBot = 1000;

        if (lowRightTop < 4000)
            lowRightTop = 0;
        else
            lowRightTop = 1000;

        if (highLeftBot < 4000)
            highLeftBot = 0;
        else
            highLeftBot = 1000;

        if (highRightTop < 4000)
            highRightTop = 0;
        else
            highRightTop = 1000;
    }

    if (!makeTip)
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            tipVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i += 2)
        {
            tipVector[i    ] = lowLeftBot  + highLeftBot;
            tipVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    if (!makeTilt)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            tiltVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
        {
            tiltVector[i    ] = lowLeftBot  + highLeftBot;
            tiltVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    // combine the tip and tilt data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(tiltVector[i] + tipVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_LoadSimPupilMoveData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_LoadSimPupilMoveData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static const boolean makePupilMoveX = true;  // turn X on or off
static const boolean makePupilMoveY = true;  // turn Y on or off

static const float lowFreqPM  = 0.1;  // low frequency pupil move in Hz  -- these may be approximate, depending on how
static const float highFreqPM = 1.0;  // high frequency pupil move in Hz -- > well they divide into the frame rate

static const float lowFreqGainPM  = 1.0;  // gain to be applied to low frequency PM (0.0 means no low freq)
static const float highFreqGainPM = 1.0;  // gain to be applied to high frequency PM (0.0 means no high freq)

static void
WFSCam_LoadSimPupilMoveData( unsigned short buf_array[] )
{
    int         i, j;
    int         rowCntr, rowSkipper, colCntr, colSkipper;
    int         stepsPerCycleLow, stepsPerCycleHigh;
    static int  stepCntrLow = -1, stepCntrHigh = -1;
    float       lowStart, lowIncr, highStart, highIncr;
    float       xVector[NUM_PIX_IN_ROW_FOR_SUBAPS], yVector[NUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up X and Y vectors
    stepsPerCycleLow = (int)((float)LegalFrameRates[hrt_camFrameRate] / lowFreqPM);
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;
    stepsPerCycleHigh = (int)((float)LegalFrameRates[hrt_camFrameRate] / highFreqPM);
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowStart =   (1.0 - sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0;
    lowIncr  = (((1.0 + sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0) - lowStart) /
        NUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS

    highStart =   (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0;
    highIncr  = (((1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0) - highStart) /
        NUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS

    if (!makePupilMoveX)
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    if (!makePupilMoveY)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    // combine the X and Y data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(yVector[i] + xVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_LoadSimZernikes( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated Zernike modes into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated Zernike modes into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_LoadSimZernikes(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

#define MAX_Z_COEFFS    66
#define NUM_M1_M2_ZERNS 20

static const float zernFreq =   0.0;  // frequency at which to modulate Zernike modes -- 0.0 means no modulation
static const float zernGain =   0.5;  // gain to be applied to Zernike modes

// Set each of the following to between -1 and 1
static const float
zernSelect[NUM_M1_M2_ZERNS] =
{
     0.0,   // Z0   Piston -- not used, leave it set to zero
     0.0,   // Z1   Tip     M2
     0.0,   // Z2   Tilt    M2
     1.0,   // Z3   Focus   M2
     0.0,   // Z4   Astigmatism (cosine)            M1
     0.0,   // Z5   Astigmatism (sine)              M1
     1.0,   // Z6   Coma x                          M1
     0.0,   // Z7   Coma y                          M1
     0.0,   // Z8   Spherical                       M1
     1.0,   // Z9   Trefoil (cosine)                M1
     0.0,   // Z10  Trefoil (sine)                  M1
     0.0,   // Z11  Secondary astigmatism (cosine)  M1
     1.0,   // Z12  Secondary astigmatism (sine)    M1
     0.0,   // Z13  Secondary coma x                M1
     0.0,   // Z14  Secondary coma y                M1
     0.0,   // Z15  Secondary spherical             M1
     0.0,   // Z16  Tetrafoil x                     M1
     0.0,   // Z17  Tetrafoil y                     M1
     0.0,   // Z18  Secondary trefoil x             M1
     0.0,   // Z19  Secondary trefoil y             M1
};

static float pupilZernikes[MAX_Z_COEFFS][NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];
float zernikes[NUM_PIX_IN_ROW_FOR_SUBAPS][NUM_ROWS_IN_CAM_FOR_SUBAPS];  // shape constructed from selected Zernike modes

static void
WFSCam_LoadSimZernikes( unsigned short buf_array[] )
{
    static boolean  firstTimeFlag = true;
    int             i, j, k;
    int             rowCntr, rowSkipper, colCntr, colSkipper;
    float           minVal, maxVal;

    int             stepsPerCycle;
    static int      stepCntr = -1;
    float           freqMultiplier;

//  if it hasn't been done yet, calculate the Zernike modes use to construct the shape
//  > to be put in the wfs buffer
    if (firstTimeFlag)
    {
        firstTimeFlag = false;
        WFSCam_ComputeZernikes();
    }

//    now, construct the shape from the selected Zernike modes
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            zernikes[i][j] = 0.0;
    }
    for (k = 0; k < NUM_M1_M2_ZERNS; k++)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
                zernikes[i][j] += pupilZernikes[k][i][j] * zernSelect[k];
        }
    }

//  so the sub-aps look more like slopes, subtract the minimum value from each one (this
//  > will also make all the values >= zero)
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j += 2)
        {
            minVal = (float)1.0e10;
            if (zernikes[i    ][j    ] < minVal) minVal = zernikes[i    ][j    ];
            if (zernikes[i    ][j + 1] < minVal) minVal = zernikes[i    ][j + 1];
            if (zernikes[i + 1][j    ] < minVal) minVal = zernikes[i + 1][j    ];
            if (zernikes[i + 1][j + 1] < minVal) minVal = zernikes[i + 1][j + 1];
            zernikes[i    ][j    ] -= minVal;
            zernikes[i    ][j + 1] -= minVal;
            zernikes[i + 1][j    ] -= minVal;
            zernikes[i + 1][j + 1] -= minVal;
        }
    }

//  calculate the multiplier for this step in the sin wave modulation (the Zernike modes shape can
//  > be temporally modulated to test the M1 and M2 offloading filters)
    if (zernFreq == 0.0)
        freqMultiplier = zernGain;
    else
    {
        stepsPerCycle = (int)((float)LegalFrameRates[hrt_camFrameRate] / zernFreq);
        if (++stepCntr >= stepsPerCycle)
            stepCntr = 0;
        freqMultiplier = sin((float)stepCntr / (float)stepsPerCycle * TWO_PI) * zernGain;  // apply the gain here, too
    }

//  modulate the shape
    maxVal = 0.0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            if (zernikes[i][j] > maxVal)
                maxVal = zernikes[i][j];
        }
    }
    if (maxVal > 0.0)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            {
                zernikes[i][j] =  1.0 - zernikes[i][j] / maxVal * 2.0;  // normalize to between -1.0 and 1.0
                zernikes[i][j] =  1.0 - zernikes[i][j] * freqMultiplier;// this will now be between 0.0 and 2.0, centered around 1.0
                zernikes[i][j] *= 8000.0;                               // put into DN-like units (the wfs camera is 14-bits)
            }
        }
    }

//  put the zernike modes into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)zernikes[i][j];
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFSCam_ComputeZernikes( void )
 *
 *  @brief
 *  This unit calculates values for all Zernike modes.
 *
 *  @b DESCRIPTION:
 *  This unit calculates values for all Zernike modes.
 *  @par
 *
 *  @b Usage:
 *  WFSCam_ComputeZernikes();
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static double rho[NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];
static double theta[NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];

static void
WFSCam_ComputeZernikes( void )
{
    int i, j, k;
    // IMPORTANT: this will only work right if
    // NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS
    int xyOffset = NUM_PIX_IN_ROW_FOR_SUBAPS / 2;
    double x, y;
    double rho1, rho2, rho3, rho4, rho5, rho6, rho7, rho8, rho9, rho10;

    for (i = 0; i < MAX_Z_COEFFS; i++)
    {
        for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
        {
            for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
                pupilZernikes[i][j][k] = 0.0;
        }
    }

//  we're putting the zernike modes on a square grid; but, the equations are easier
//  > in polar form; so, compute rho and theta for each point
    for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        x = (double)(j - xyOffset);

        for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            y = (double)(k - xyOffset);

            rho[j][k] = sqrt(x * x + y * y) / (double)xyOffset;  // rho will be between 0 and 1
            theta[j][k] = atan2(y, x);  // theta will be between -pi and pi
        }
    }

    for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            if (rho[j][k] >= 0.0)
            {
                rho1  = rho[j][k];
                rho2  = rho1 * rho[j][k];
                rho3  = rho2 * rho[j][k];
                rho4  = rho3 * rho[j][k];
                rho5  = rho4 * rho[j][k];
                rho6  = rho5 * rho[j][k];
                rho7  = rho6 * rho[j][k];
                rho8  = rho7 * rho[j][k];
                rho9  = rho8 * rho[j][k];
                rho10 = rho9 * rho[j][k];

/*  Z0 */       pupilZernikes[0][j][k] = 1.0;  // 0 - piston

/*  Z1 */       pupilZernikes[1][j][k] = (float)(rho1 * cos(theta[j][k]));  // 2 - tilt
/*  Z2 */       pupilZernikes[2][j][k] = (float)(rho1 * sin(theta[j][k]));  // 1 - tip
/*  Z3 */       pupilZernikes[3][j][k] = (float)(2.0 * rho2 - 1.0);      // 4 - focus

/*  Z4 */       pupilZernikes[4][j][k]  = (float)(rho2 * cos(2.0 * theta[j][k]));                   // 5 - astig with axis at 0 or pi/2
/*  Z5 */       pupilZernikes[5][j][k]  = (float)(rho2 * sin(2.0 * theta[j][k]));                   // 3 - astig with axis at +/- pi/4
/*  Z6 */       pupilZernikes[6][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * cos(theta[j][k]));           // 8 - third order coma along y axis
/*  Z7 */       pupilZernikes[7][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * sin(theta[j][k]));           // 7 - third order coma along x axis
/*  Z8 */       pupilZernikes[8][j][k]  = (float)(6.0 * rho4 - 6.0 * rho2 + 1.0);                   // 12 - third order spherical
/*  Z9 */       pupilZernikes[9][j][k]  = (float)(rho3 * cos(3.0 * theta[j][k]));                   // 9 - triangular astig with base on y axis
/* Z10 */       pupilZernikes[10][j][k] = (float)(rho3 * sin(3.0 * theta[j][k]));                   // 6 - triangular astig with base on x axis
/* Z11 */       pupilZernikes[11][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * cos(2.0 * theta[j][k]));       // 13
/* Z12 */       pupilZernikes[12][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * sin(2.0 * theta[j][k]));       // 11
/* Z13 */       pupilZernikes[13][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * cos(theta[j][k]));  // 18
/* Z14 */       pupilZernikes[14][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * sin(theta[j][k]));  // 17
/* Z15 */       pupilZernikes[15][j][k] = (float)(20.0 * rho6 - 30.0 * rho4 + 12.0 * rho2 - 1.0);           // 24
/* Z16 */       pupilZernikes[16][j][k] = (float)(rho4 * cos(4.0 * theta[j][k]));                   // 14
/* Z17 */       pupilZernikes[17][j][k] = (float)(rho4 * sin(4.0 * theta[j][k]));                   // 10
/* Z18 */       pupilZernikes[18][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3) * cos(3.0 * theta[j][k]));       // 19
/* Z19 */       pupilZernikes[19][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3) * sin(3.0 * theta[j][k]));       // 16

                pupilZernikes[20][j][k] = (float)(    rho5                 * sin(5.0 * theta[j][k]));  // 15
                pupilZernikes[21][j][k] = (float)(    rho5                 * cos(5.0 * theta[j][k]));     // 20

                pupilZernikes[22][j][k] = (float)(    rho6                  * sin(6.0 * theta[j][k]));  // 21
                pupilZernikes[23][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)          * sin(4.0 * theta[j][k]));  // 22
                pupilZernikes[24][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2) * sin(2.0 * theta[j][k]));  // 23
                pupilZernikes[25][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2) * cos(2.0 * theta[j][k]));  // 25
                pupilZernikes[26][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)          * cos(4.0 * theta[j][k]));  // 26
                pupilZernikes[27][j][k] = (float)(    rho6                  * cos(6.0 * theta[j][k]));  // 27

                pupilZernikes[28][j][k] = (float)(    rho7                       * sin(7.0 * theta[j][k]));  // 28
                pupilZernikes[29][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                   * sin(5.0 * theta[j][k]));  // 29
                pupilZernikes[30][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)           * sin(3.0 * theta[j][k]));  // 30
                pupilZernikes[31][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1) * sin(       theta[j][k]));  // 31
                pupilZernikes[32][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1) * cos(       theta[j][k]));  // 32
                pupilZernikes[33][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)           * cos(3.0 * theta[j][k]));  // 33
                pupilZernikes[34][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                   * cos(5.0 * theta[j][k]));  // 34
                pupilZernikes[35][j][k] = (float)(    rho7                       * cos(7.0 * theta[j][k]));  // 35

                pupilZernikes[36][j][k] = (float)(    rho8                         * sin(8.0 * theta[j][k]));  // 36
                pupilZernikes[37][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                 * sin(6.0 * theta[j][k]));  // 37
                pupilZernikes[38][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)         * sin(4.0 * theta[j][k]));  // 38
                pupilZernikes[39][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2) * sin(2.0 * theta[j][k]));  // 39
                pupilZernikes[40][j][k] = (float)( 70.0 * rho8 - 140.0 * rho6 + 90.0 * rho4 - 20.0 * rho2 + 1.0);             // 40
                pupilZernikes[41][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2) * cos(2.0 * theta[j][k]));  // 41
                pupilZernikes[42][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)         * cos(4.0 * theta[j][k]));  // 42
                pupilZernikes[43][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                 * cos(6.0 * theta[j][k]));  // 43
                pupilZernikes[44][j][k] = (float)(    rho8                         * cos(8.0 * theta[j][k]));  // 44

                pupilZernikes[45][j][k] = (float)(     rho9                                * sin(9.0 * theta[j][k]));  // 45
                pupilZernikes[46][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                        * sin(7.0 * theta[j][k]));  // 46
                pupilZernikes[47][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                * sin(5.0 * theta[j][k]));  // 47
                pupilZernikes[48][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)        * sin(3.0 * theta[j][k]));  // 48
                pupilZernikes[49][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1) * sin(    theta[j][k]));  // 49
                pupilZernikes[50][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1) * cos(    theta[j][k]));  // 50
                pupilZernikes[51][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)        * cos(3.0 * theta[j][k]));  // 51
                pupilZernikes[52][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                * cos(5.0 * theta[j][k]));  // 52
                pupilZernikes[53][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                        * cos(7.0 * theta[j][k]));  // 53
                pupilZernikes[54][j][k] = (float)(     rho9                                * cos(9.0 * theta[j][k]));  // 54

                pupilZernikes[55][j][k] = (float)(         rho10                                   * sin(10.0 * theta[j][k]));    // 55
                pupilZernikes[56][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                           * sin( 8.0 * theta[j][k]));    // 56
                pupilZernikes[57][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                   * sin( 6.0 * theta[j][k]));    // 57
                pupilZernikes[58][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)           * sin( 4.0 * theta[j][k]));    // 58
                pupilZernikes[59][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * sin( 2.0 * theta[j][k]));  // 59
                pupilZernikes[60][j][k] = (float)( 252.0 * rho10 - 630.0 * rho8 + 560.0 * rho6 - 210.0 * rho4 + 30.0 * rho2 - 1.0);                // 60
                pupilZernikes[61][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * cos( 2.0 * theta[j][k]));  // 61
                pupilZernikes[62][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)               * cos( 4.0 * theta[j][k]));    // 62
                pupilZernikes[63][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                              * cos( 6.0 * theta[j][k]));    // 63
                pupilZernikes[64][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                           * cos( 8.0 * theta[j][k]));    // 64
                pupilZernikes[65][j][k] = (float)(         rho10                                   * cos(10.0 * theta[j][k]));    // 65
            }
        }
    }
}

//******************************************************************************
// * End of file
//******************************************************************************
