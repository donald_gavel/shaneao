#ifndef KTLFILE_H_4829237
#define KTLFILE_H_4829237

#include "virtktlkeys.h"


class CKTLFile;


class CKTLFileString : public CKTLString
{
 public:
    CKTLFileString(const char *name, int key, const char *val, int readWrite, 
                   KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                   CKTLFile *pKTLFile);
    ~CKTLFileString() {} ;
    CKTLFile *m_pKTLFile;
};

class CKTLFileInt : public CKTLInt
{
 public:
    CKTLFileInt(const char *name, int key, int val, int readWrite, 
                KTLChangeActionFunc actFunc, KTLDelayedResponseFunc drFunc,
                CKTLFile *pKTLFile);
    ~CKTLFileInt() {} ;
    CKTLFile *m_pKTLFile;
};


class CKTLFile
{
 public:

    CKTLFile(CInterface *pInter, string baseName,  
             KTLChangeActionFunc    af,
             KTLDelayedResponseFunc dr); 
    virtual ~CKTLFile();

    int SetFileName(string name) { return -1; } ;
    int SetFileStatus(int status) { return -1; };
    int TestComplete(string targval) { return -1; };
    int TestComplete(int targval) {return -1;};

    CKTLFileString *m_pName;
    CKTLFileInt    *m_pStatus;
    string          m_BaseName;
    CInterface     *m_pInter;

    KTLChangeActionFunc m_afFile;
    KTLDelayedResponseFunc m_drFile;



};




#endif
