#include "locks.h"


pthread_mutex_t g_wfsCamMu;

int wfsLockInit()
{
    return pthread_mutex_init(&g_wfsCamMu,NULL);
}

int wfsLock()
{
    return pthread_mutex_lock(&g_wfsCamMu);
}


int wfsUnlock()
{
    return pthread_mutex_unlock(&g_wfsCamMu);
}
