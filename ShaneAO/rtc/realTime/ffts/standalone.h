#ifndef _FFTW_STANDALONE_H
#define _FFTW_STANDALONE_H		1

typedef float R;
typedef int stride;
typedef R E;
typedef int INT;

#define WS(stride, i)  (stride * i)

/* hackery to prevent the compiler from ``optimizing'' induction
   variables in codelet loops. */
static volatile int an_int_guaranteed_to_be_zero = 0;
#define MAKE_VOLATILE_STRIDE(x) (x) = (x) ^ an_int_guaranteed_to_be_zero

#define K(x) ((E) x)
#define DK(name, value) const E name = K(value)

#define FMA(a, b, c) (((a) * (b)) + (c))
#define FMS(a, b, c) (((a) * (b)) - (c))
#define FNMA(a, b, c) (- (((a) * (b)) + (c)))
#define FNMS(a, b, c) ((c) - ((a) * (b)))

/* for codelets generated with -generic-arith flag */
#define ADD(a,b) ((a) + (b))
#define SUB(a,b) ((a) - (b))
#define MUL(a,b) ((a) * (b))
#define NEG(a) (-(a))
#define FMMA(a, b, c, d) (((c) * (d)) + ((a) * (b)))
#define FNMMS(a, b, c, d) (((c) * (d)) - ((a) * (b)))

#endif
