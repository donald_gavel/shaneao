ifndef LROOT
	LROOT = /usr/local/lick
endif

ifndef CONFIGPATH
	CONFIGPATH = ../../config
endif

include $(LROOT)/etc/Make.defs
include ../Mk.realtime

EXAOTEST = 0

# The large number of object files in this directory lend themselves
# to a parallel build.

MAKEFLAGS += -j6

ifeq "$(RTL_SYSTEM)" "0"
$(error Realtime build disabled: compile in ../user/)
endif


SBINFILES = gpAoHrtSysCon.rtl
FILES = $(SBINFILES)

ALSO_CLEAN = gpAoHrtSysCon.o.debug linkmap.txt a.out cscope.out *.ko


include $(CONFIGPATH)/make_defs/Mk.deps

MAKE_DEPS = Makefile $(CONFIGPATH)/make_defs/Mk.deps $(LROOT)/etc/Make.defs

INCLUDES = $(wildcard ../../include/*.h)
MAKE_DEPS += $(INCLUDES)

WR_LINUX=/opt/WR_Linux
RTCORE_BASE=$(WR_LINUX)/build/rtcore-base-5.2
MAKE_DEPS += $(RTCORE_BASE)/rtl.mk

include $(RTCORE_BASE)/rtl.mk


# Normalize the value of $(RTL_SYSTEM).
ifneq "$(RTL_SYSTEM)" "0"
	RTL_SYSTEM = 1
endif


# These objects will be linked together into the gpAoHrtSysCon.rtl binary.
# gpAoHrtSysCon.c is deliberately omitted from this list; the RTL rules
# will already create a dependency on gpAoHrtSysCon.c for gpAoHrtSysCon.rtl.
# The intermediate object file is translated into %.o.debug and %.o.stub.o
# files; since there are no explicit rules for those outputs, they cannot
# be added as dependencies for gpAoHrtSysCon.rtl above.

OBJECTS = gpAoHrtCent.o gpAoHrtFfts.o gpAoHrtFileData.o \
	  gpAoHrtParse.o gpAoHrtRecon.o gpAoHrtReconOpt.o gpAoHrtTipTilt.o \
	  gpAoHrtVolts.o WFSCam.o \
	  TTcam.o

# Dialing up or down the compiler optimization does not appear to make
# a significant difference in the CPU usage of the realtime processes.
#
# In October 2012, it was at -O3; the WindRiver makefile snippet uses a
# default of -O2.

CFLAGS := $(filter-out -msoft-float,$(CFLAGS))
# The most modern Intel architecture supported by the WindRiver gcc 4.3.2
# is 'core2'.
CFLAGS += -O3 -march=core2 -mtune=core2 -Wno-unused-function -N -static
CFLAGS += $(MATH_ADD_CFLAGS)

# The WindRiver Makefile does not use $(CPPFLAGS), it uses $(CFLAGS).

CFLAGS += -I../../include -I../ffts
CFLAGS += -DCAM_TYPE=${CAM_TYPE} \
	  -DTTCAM_TYPE=${TTCAM_TYPE} \
	  -DCENT_DEBUG=${CENT_DEBUG} \
	  -DEXAOTEST=${EXAOTEST} \
	  -DGMB_SIM=${GMB_SIM} \
	  -DGP_GMBLOCK_AOC=${GP_GMBLOCK_AOC} \
	  -DHRT=1 \
	  -DPARSE_DEBUG_DUMP=${PARSE_DEBUG_DUMP} \
	  -DPARSE_DEBUG=${PARSE_DEBUG} \
	  -DRECON_DEBUG_DUMP=${RECON_DEBUG_DUMP} \
	  -DRECON_DEBUG=${RECON_DEBUG} \
	  -DRECONOPT_DEBUG_DUMP=${RECONOPT_DEBUG_DUMP} \
	  -DRECONOPT_DEBUG=${RECONOPT_DEBUG} \
	  -DRECONOPT_DO_BLOCK_AVG=${RECONOPT_DO_BLOCK_AVG} \
	  -DRECONOPT_TEST=${RECONOPT_TEST} \
	  -DRECONOPT_TURN_OFF_KL_VALID=${RECONOPT_TURN_OFF_KL_VALID} \
	  -DRTL_SYSTEM=${RTL_SYSTEM} \
	  -DSTANDALONE_FFT=${STANDALONE_FFT} \
	  -DTEST_GAIN=${TEST_GAIN} \
	  -DTIMER_TEST=${TIMER_TEST} \
	  -DTWT_TYPE=${TWT_TYPE} \
	  -DWFR_TYPE=${WFR_TYPE}

# Clear the default LDFLAGS, which is not normally desirable, but we don't
# use them here. Clearing them avoids any confusion when linking the .rtl
# binary (see below).

LDFLAGS :=

RTL_LDFLAGS += $(MATH_ADD_LDFLAGS)
RTL_LDFLAGS += -L${RTL_LIBS_DIR}
RTL_LDFLAGS += -Map=linkmap.txt --cref

RTL_LDLIBS += -lrtlm

###############################################################################
# CAMERA SETUP STUFF
###############################################################################
ifeq (${CAM_TYPE}, 'WFS_REAL_GPI')
CFLAGS += -I/opt/EDTpdv -I/opt/EDTpdv/module
RTL_LDFLAGS += -L/opt/EDTpdv
RTL_LDLIBS += -lpdv
endif

###############################################################################
# TT CAMERA SETUP STUFF
###############################################################################
ifeq (${TTCAM_TYPE}, 'TT_REAL_GPI')
ifneq (${CAM_TYPE}, 'WFS_REAL_GPI')
CFLAGS += -I/opt/EDTpdv -I/opt/EDTpdv/module
RTL_LDFLAGS += -L/opt/EDTpdv
RTL_LDLIBS += -lpdv
endif
endif

###############################################################################
# TWT SETUP STUFF
###############################################################################
ifeq (${TWT_TYPE}, 'TWT_BMC_4K')
OBJECTS +=  gpAoHrtDpio2Drivers.o rt_dpio2_funcs.o rt_dpio2_osc.o
CFLAGS += -I../dpio2_RTL/dpio2_include
endif

###############################################################################
# WFR SETUP STUFF
###############################################################################
ifeq (${WFR_TYPE}, 'WFR_GPI_121')
ifneq (${TWT_TYPE}, 'TWT_BMC_4K')
OBJECTS +=  gpAoHrtDpio2Drivers.o rt_dpio2_funcs.o rt_dpio2_osc.o
CFLAGS += -I../dpio2_RTL/dpio2_include
endif
endif

# The LDFLAGS_% macro is used by the RT Rules.make file when linking
# the % binary. This is how we are expected to enumerate our object
# list. Unfortunately, the LDFLAGS_% macro is listed before LDFLAGS,
# which means that any -L directives occur *after* the object files
# are listed, which is backwards from canonical use.

# We explicitly construct the correct order locally, and use an
# empty LDFLAGS macro. It's not completely successful, because the
# RT Rules.make mixes in a few object files and -L directives before
# the LDFLAGS_% macro gets included.

LDFLAGS_gpAoHrtSysCon.rtl=$(RTL_LDFLAGS) $(OBJECTS) $(RTL_LDLIBS)

gpAoHrtSysCon.rtl: $(OBJECTS)

$(OBJECTS): $(MAKE_DEPS)


include $(RTL_DIR)/Rules.make
include $(LROOT)/etc/Make.rules
