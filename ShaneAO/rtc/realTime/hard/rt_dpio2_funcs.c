/*
 * DPIO2 RT-Linux Functional Layer
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef CONFIG_RTL_BSD
#include <sys/rtl_ioctl.h>
#include <rtl_fcntl.h>
#endif
#include <string.h>
#include "shared.h"

#define VMOSA
#include "vmosa-linux.h"
#include "dpio2-linux.h"
#include "dpio2-linux-user-space.h"
#include "dpio2.h"
#include "Dpio2Defs.h"

//#define MDL_DEBUG

// see rt_dpio2_osc.c
extern STATUS dpio2SetFrequency(HANDLE hHandle, UINT32 requestedFrequency,
                         UINT32 *presultingFrequency);

// DPIO2 Globals
static int            s_iDeviceOpenFlag[MAX_DPIO2_DEVICE] = {DPIO2_DEVICE_FREE, DPIO2_DEVICE_FREE};
static dpio2_handle_t s_hHandle        [MAX_DPIO2_DEVICE] = {0, 0};

// dpio2_Open
// Open kernel device. Map kernel and user space address. 
// iDevno: device number
HANDLE dpio2_Open (int iDevno)
{
  STATUS status;
  HANDLE handle;
  
  char tmpName[32];
  dpio2_hw_info_t hw_info[MAX_DPIO2_DEVICE];

  // Check if device number exist 
  if ( ( iDevno < 0 ) || ( iDevno > MAX_DPIO2_DEVICE-1 )) {
  	DPIO2_ERR_MSG (("The specified device number (%d) is not valid\n", iDevno));
  	return NULL;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [iDevno] == DPIO2_DEVICE_OPEN ) {
  	DPIO2_ERR_MSG (("The DPIO2 device number %d is already opened\n", iDevno));
  	return NULL;
  } else
  	s_iDeviceOpenFlag [iDevno] = DPIO2_DEVICE_OPEN;

  // Assign and clear handle struct 
  handle = &s_hHandle[iDevno];
  memset ( (char*) handle, 0, sizeof ( dpio2_handle_t ) );

  // open RT-Linux kernel mode driver
  sprintf(tmpName, "/dev/dpio2/rtl%d", iDevno);
  handle->fd = rtl_open ( tmpName, O_RDWR );

  if ( handle->fd < 0 ) {
    DPIO2_ERR_MSG (("Open %s failed! fd = %d, errno = %d\n",
		    tmpName /*"dpio2"*/, handle->fd, errno ));  //dwp100506
    return NULL;
  }
 
  // Get PCI configuration info like : vendor, device and revision id, 
  // pci bus and device number 
  status = ioctl ( handle->fd, DPIO2_GET_PCI_CONFIG_INFO, &handle->pci );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("DPIO2_GET_PCI_CONFIG_INFO failed! status = %d, errno =%d\n", status, errno));
    close ( handle->fd );
    return NULL;
  }
#ifdef MDL_DEBUG
  else
  { 
	printf("VENDOR ID = %04X\n", handle->pci.vendor_id);
	printf("DEVICE ID = %04X\n", handle->pci.device_id);
	printf("REV    ID = %02X\n", handle->pci.revision_id);
	printf("Device #  = %d\n",   handle->pci.device_number);
	printf("Bus    #  = %d\n",   handle->pci.bus_number);
  } 
#endif

  status = ioctl ( handle->fd, DPIO2_GET_HW_INFO, &hw_info[0] );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("DPIO2_GET_HW_INFO failed! status = %d, errno =%d\n", status, errno));
    close ( handle->fd );
    return NULL;
  }
#ifdef MDL_DEBUG
  else
  {
        printf("DEVNO       = %d\n",   hw_info[0].devno);
        printf("DEVICE ID   = %04X\n", hw_info[0].deviceId);
        printf("VENDOR ID   = %04X\n", hw_info[0].vendorId);
        printf("REVISION ID = %02X\n", hw_info[0].revisionId);
        printf("MODULE TYPE = %02X\n", hw_info[0].moduleType);
        printf("PCI BUS NO  = %02X\n", hw_info[0].pciBusNumber);
        printf("PCI DEV NO  = %02X\n", hw_info[0].pciDeviceNumber);
        printf("PCI FPGA VER= %02X\n", hw_info[0].pciFpgaVersion);
        printf("FE FPGA VER = %02X\n", hw_info[0].frontEndFpgaVersion);
  }
#endif

  // Map PCI resoures and addreses 
  handle->user.ctrl_mmap_size = DPIO2_PCI_CTRL_WINDOW_SIZE;
  handle->user.fifo_mmap_size = DPIO2_PCI_FIFO_WINDOW_SIZE;

  status = ioctl ( handle->fd, DPIO2_MMAP_SELECT, DPIO2_MMAP_CTRL_BASE);

  if ( status != OK ) {
    DPIO2_ERR_MSG (("DPIO2_MMAP_SELECT failed, errno = %d\n", errno ));
    close ( handle->fd );
    return NULL;
  }

  handle->user.ctrl_base =  mmap ( 0, DPIO2_PCI_CTRL_WINDOW_SIZE,
				   PROT_READ | PROT_WRITE, 
				   MAP_SHARED,
				   handle->fd, 0 );
  
  if ( (long) handle->user.ctrl_base == ERROR ) {
    DPIO2_ERR_MSG (("mmap failed, errno = %d\n", errno ));
    close ( handle->fd );
    return NULL;
  }
#ifdef MDL_DEBUG
  else
	printf("PCI control window map at %08x, length %08x\n", handle->user.ctrl_base, DPIO2_PCI_CTRL_WINDOW_SIZE);
#endif

  status = ioctl ( handle->fd, DPIO2_MMAP_SELECT, DPIO2_MMAP_FIFO_BASE );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("DPIO2_MMAP_SELECT failed, errno = %d\n", errno ));
    munmap ( handle->user.ctrl_base, DPIO2_PCI_CTRL_WINDOW_SIZE );
    close ( handle->fd );
    return NULL;
  } 

  handle->user.fifo_base = mmap ( 0, DPIO2_PCI_FIFO_WINDOW_SIZE,
				  PROT_READ | PROT_WRITE, 
				  MAP_SHARED,
				  handle->fd, 0 );
  
  if ( (long) handle->user.fifo_base == ERROR ) {
    DPIO2_ERR_MSG (("mmap failed, errno = %d\n", errno ));
    munmap ( handle->user.ctrl_base, DPIO2_PCI_CTRL_WINDOW_SIZE );
    close ( handle->fd );
    return NULL;
  }
#ifdef MDL_DEBUG
  else
        printf("PCI FIFO window map at %08x, length %08x\n", handle->user.fifo_base, DPIO2_PCI_FIFO_WINDOW_SIZE);
#endif

  // Set devno
  handle->devno = iDevno;

  // Mark the device as open
  s_iDeviceOpenFlag [iDevno] = DPIO2_DEVICE_OPEN;

  // Return HANDLE
  return (handle);
}

// dpio2_Close
// hHandle: device handle from dpio2_Open
STATUS dpio2_Close (HANDLE hHandle)
{
  // Check if device number exist 
  if ( hHandle == NULL) {
    DPIO2_ERR_MSG (("The specified handle number %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

  // Unmap PCI resoures and addreses
  if ( hHandle->user.ctrl_base ) {
    munmap ( hHandle->user.ctrl_base, DPIO2_PCI_CTRL_WINDOW_SIZE );
    hHandle->user.ctrl_base = 0;
  }
  
  if ( hHandle->user.fifo_base ) {
    munmap ( hHandle->user.fifo_base, DPIO2_PCI_FIFO_WINDOW_SIZE );
    hHandle->user.fifo_base = 0;
  }

  // Close device
  close (hHandle->fd);
  s_iDeviceOpenFlag [hHandle->devno] = DPIO2_DEVICE_FREE;
  return OK;
}

// dpio2_ConfigurePioStrobes
// configure PIO1 and PIO2 as output; set both high
// hHandle: device handle from dpio2_Open
// iValue:  enable/disable
STATUS dpio2_ConfigurePioStrobes (HANDLE hHandle, int iValue)
{
  STATUS status = OK;
  dpio2_kio_t ioc;

  // Check if device number exist
  if ( hHandle == NULL ) {
    DPIO2_ERR_MSG (("The specified device handle %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

//dwp100505 -- start
//enable and set both PIO1 and PIO2 here

  ioc.offset    = DPIO2_FRONT_END_OUTPUT_ENABLE_REGISTER;
  ioc.bit_mask  = (1<<DPIO2_BIT_NUM_ENABLE_PIO2_OUTPUT | 1<<DPIO2_BIT_NUM_ENABLE_PIO1_OUTPUT);
  ioc.u.dword   = 0;
  if (iValue)
        ioc.u.dword  |= (1<<DPIO2_BIT_NUM_ENABLE_PIO2_OUTPUT | 1<<DPIO2_BIT_NUM_ENABLE_PIO1_OUTPUT);
  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );
  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }

  ioc.offset    = DPIO2_FRONT_END_OUTPUT_VALUE_REGISTER;
  ioc.bit_mask  = (1<<DPIO2_BIT_NUM_PIO2_OUTPUT_VALUE  | 1<<DPIO2_BIT_NUM_PIO1_OUTPUT_VALUE);
  ioc.u.dword   = 0;
  if (iValue)
        ioc.u.dword  |= (1<<DPIO2_BIT_NUM_PIO2_OUTPUT_VALUE  | 1<<DPIO2_BIT_NUM_PIO1_OUTPUT_VALUE);
  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );
  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }
//dwp100505 -- end

  return OK;
}

// dpio2_ControlPioStrobes
// set/reset PIO2 (PIO1 left high)
// hHandle: device handle from dpio2_Open
// iValue:  enable/disable
STATUS dpio2_ControlPioStrobes (HANDLE hHandle, int iValue)
{
  STATUS status;
  dpio2_kio_t ioc;

  // Check if device number exist
  if ( hHandle == NULL ) {
    DPIO2_ERR_MSG (("The specified device handle %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

  ioc.offset    = DPIO2_FRONT_END_OUTPUT_VALUE_REGISTER;
  // only set PIO2 here (PIO1 left alone)
  ioc.bit_mask  = (1<<DPIO2_BIT_NUM_PIO2_OUTPUT_VALUE);// | 1<<DPIO2_BIT_NUM_PIO1_OUTPUT_VALUE);
  ioc.u.dword   = 0;

  if (iValue)
     ioc.u.dword |= (1<<DPIO2_BIT_NUM_PIO2_OUTPUT_VALUE);// | 1<<DPIO2_BIT_NUM_PIO1_OUTPUT_VALUE) ;

  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }

  return OK;
}

// dpio2_InitializeForOutput
// set DPIO2 as output device
// hHandle: device handle from dpio2_Open
STATUS dpio2_InitializeForOutput(HANDLE hHandle)
{
  STATUS status;
  dpio2_kio_t ioc;

  // Check if device number exist
  if ( hHandle == NULL ) {
    DPIO2_ERR_MSG (("The specified device handle %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

  ioc.offset    = DPIO2_CONFIGURATION_REGISTER;

  ioc.bit_mask  = (1<< DPIO2_BIT_NUM_ENABLE_AS_OUTPUT | 
                   1<< DPIO2_BIT_NUM_DISABLE_JUMPER_FOR_STROBE_GENERATION | 
                   1<< DPIO2_BIT_NUM_DISABLE_JUMPER_FOR_MASTER_INTERFACE  | 
                   1<< DPIO2_BIT_NUM_DISABLE_JUMPER_FOR_INPUT_OR_OUTPUT   |
		   1<< DPIO2_BIT_NUM_ENABLE_AS_MASTER_INTERFACE);

  ioc.u.dword = ioc.bit_mask;

  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }
  return OK;
}

// dpio2_EnableStrobeGeneration
// enable and control strobe generation
// hHandle: device handle from dpio2_Open
// iMode:   DPIO2_PROGRAMMABLE_OSCILLATOR or DPIO2_FIXED_OSCILLATOR
// dwWantedFrequency: desired frequency in Hz
// pdwResultantFrequency: actual frequency in Hz
STATUS dpio2_EnableStrobeGeneration(HANDLE hHandle, int iMode, unsigned int dwWantedFrequency, unsigned int *pdwResultantFrequency)
{
  STATUS status;
  dpio2_kio_t ioc;

  // Check if device number exist
  if ( hHandle == NULL ) {
    DPIO2_ERR_MSG (("The specified device handle %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

  ioc.offset   = DPIO2_CONFIGURATION_REGISTER;

  // turn on strobe generation, turn on/off programmable oscillator
  ioc.bit_mask =   (1<<DPIO2_BIT_NUM_ENABLE_STROBE_GENERATION | 1<<DPIO2_BIT_NUM_ENABLE_PECL_STROBE | 1<<DPIO2_BIT_NUM_ENABLE_100MHZ_FIFO_CLOCK);

  if (iMode == DPIO2_PROGRAMMABLE_OSCILLATOR)
    ioc.u.dword  = (1<<DPIO2_BIT_NUM_ENABLE_STROBE_GENERATION | 1<<DPIO2_BIT_NUM_ENABLE_PECL_STROBE | 1<<DPIO2_BIT_NUM_ENABLE_100MHZ_FIFO_CLOCK);
  else
    ioc.u.dword  = (1<<DPIO2_BIT_NUM_ENABLE_STROBE_GENERATION                                       | 1<<DPIO2_BIT_NUM_ENABLE_100MHZ_FIFO_CLOCK);

  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  status = dpio2SetFrequency(hHandle, dwWantedFrequency, pdwResultantFrequency);

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }
  return OK;
}

// dpio2_ActivateFpdpInterface
// Activate FPDP output interface
// hHandle: device handle from dpio2_Open
STATUS dpio2_ActivateFpdpInterface(HANDLE hHandle)
{
  STATUS status;
  dpio2_kio_t ioc;

  // Check if device number exist
  if ( hHandle == NULL ) {
    DPIO2_ERR_MSG (("The specified device handle %d is not valid\n", hHandle));
    return ERROR;
  }

  // Check if device is opened
  if ( s_iDeviceOpenFlag [hHandle->devno] != DPIO2_DEVICE_OPEN ) {
        DPIO2_ERR_MSG (("The DPIO2 device number %d is not opened\n", hHandle->devno));
        return ERROR;
  }

  ioc.offset   = DPIO2_FRONT_END_CONTROL_REGISTER_0;
  ioc.bit_mask = (1 << DPIO2_BIT_NUM_ACTIVATE_TRANSFER);
  ioc.u.dword  = ioc.bit_mask;

  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }
  return OK;
}

// dpio2_DmaTransfer
// Peform DMA transfer from memory to DPIO2 FIFO
// hHandle: device handle from dpio2_Open
// dwSizeInBytes: number of byte to transfer
// pData: pointer to data buffer (must be a physical contiguous lock-down)
STATUS dpio2_DmaTransfer(HANDLE hHandle, UINT32 dwSizeInBytes, unsigned char *pData)
{
  dpio2_kio_t ioc;
  STATUS status = OK;
  
  ioc.offset    = (unsigned long)pData;
  ioc.bit_mask  = dwSizeInBytes;

  status = ioctl ( hHandle->fd, DPIO2_DMA_START, &ioc);

  if ( status != OK ) {
    DPIO2_ERR_MSG (("DPIO2_DMA_START: failed! status = %d, errno =%d\n", status, errno));
    return ERROR;
  }

  return OK;
}


//dwp100505 -- start
// dpio2_DmaEnd
// End DMA transfer -- this is necessary simply to call pci_unmap_single()
// hHandle: device handle from dpio2_Open
STATUS dpio2_DmaEnd(HANDLE hHandle)
{
  dpio2_kio_t ioc;
  ioctl ( hHandle->fd, DPIO2_DMA_END, &ioc);

  return OK;
}
//dwp100505 -- end
