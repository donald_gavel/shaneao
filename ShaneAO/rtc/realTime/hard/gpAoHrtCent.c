/**********************************************************************************************
*
*   COPYRIGHT (C) 2008
*
*   LAWRENCE LIVERMORE NATIONAL LABORATORY
*   ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
*
***********************************************************************************************/
/*!
 *
 *  @file gpAoHrtCent.c
 *
 *  @brief
 *  Perform centroiding and centroid-related tasks.
 *
 *  @b DESCRIPTION:
 *  This component contains functions to perform quad-cell (2x2) centroiding and centroid-related
 *  tasks.
 *  @par
 *  IMPORTANT -- Note that, as a rule, centroids (including reference centroids) are stored
 *  internally in FTR-native units, namely MICRON_SLOPE_PER_SUBAP, and stored in files in MAS.
 *  @par
 *
 *  @b AUTHOR:
 *  DWP & SMJ    DATE: 11/25/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c gpAoHrtCent()
 *  -# @c gpAoHrtCent_Init()
 *  -# @c gpAoHrtCent_Restart()
 *  -# @c gpAoHrtCent_ProcCmd()
 *  -# @c gpAoHrtCent_DarkBackStart()
 *  -# @c gpAoHrtCent_DarkBackProcess()
 *  -# @c gpAoHrtCent_DarkBackAbort()
 *  -# @c gpAoHrtCent_FlatStart()
 *  -# @c gpAoHrtCent_FlatProcess()
 *  -# @c gpAoHrtCent_FlatAbort()
 *  -# @c gpAoHrtCent_RefCalcStart()
 *  -# @c gpAoHrtCent_RefCalcProcess()
 *  -# @c gpAoHrtCent_RefCalcAbort()
 *  -# @c gpAoHrtCent_GetCrntDarkBackFlat()
 *  -# @c gpAoHrtCent_AdjustCrntDarkBackFlat()
 *  -# @c gpAoHrtCent_ProcessQuad()
 *  -# @c gpAoHrtCent_OffsetRefCents()
 *  -# @c gpAoHrtCent_CalcPupil()
 *  -# @c gpAoHrtCent_PupilTrack_Init()
 *  -# @c gpAoHrtCent_PupilTrack()
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>

#if (RTL_SYSTEM == 0)
#include <stdlib.h>
#include <time.h>
#include <sched.h>
#include <sys/types.h>
#include <limits.h>
#include <sys/time.h>
#include <sys/types.h>
#include <linux/unistd.h>
#endif


#include "gpAoHrtCent.h"
#include "Cent_Aux.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"


__inline__ unsigned long long int rdtsc( void );  // in gpAoHrtRecon.c


#define DARK_CORNER_PERS_CNT 50   // this shouldn't really ever happen, so give it a relatively low persistence count
#define AO_LIGHT_PERS_CNT    500
#define PUPIL_LIGHT_PERS_CNT 500

// --------------------------------------------
// Global Variables

#if ( TIMER_TEST != 0 )
long  centFrameID = 0;
#endif

int hrt_numCentroids;
int hrt_numSubaps;

int hrt_CentThresh;  // value centroider subtracts from raw pixel data (then, if result is less than zero,
                         // > centroider sets raw data to zero)

int hrt_BacksAvgN;  // number of frames to average when determining WFS darkBacks values
int hrt_FlatsAvgN;  // number of frames to average when determining WFS Flats values

int hrt_applyBackData;  // flag whether or not to apply 'back Subtract' data to WFS ( 0: no, 1: yes )
int hrt_applyDarkData;  // flag whether or not to apply 'dark Subtract' data to WFS ( 0: no, 1: yes )
int hrt_applyFlatData;  // flag whether or not to apply 'flat Subtract' data to WFS ( 0: no, 1: yes )

int hrt_wfsBkGrndData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];  // current wfs Back data
int hrt_wfsDarkData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];    // current wfs dark data
float hrt_wfsFlatData[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];    // current wfs Flat data ( Only calculated on centroids!)

int hrt_centAvgN;                             // number of frames to average when determining reference centroids or offsets
float hrt_currRefCentsQ[MAX_NUM_CENTROIDS];  // current working reference centroids for quad-cell ( including offsets )

int hrt_applyCalData;  // apply CAL offset Data ( 0: no, 1: yes )

float hrt_centroidGainX;  // gain to apply to X cetroids (primarily to correct for gain difference between X and Y)
float hrt_centroidGainY;  // gain to apply to Y cetroids (primarily to correct for gain difference between X and Y)

float hrt_overThreshFraction;  // number of sub-aps whose intensity must be over the centThresh for frame to be used  //XXXdwp110816

// variables used in dark-corner background / dark level correction (using unilluminated corner pixels)
float hrt_darkCornerCorrectSwitch;  // turns dark level correction on and off
int   darkCornerPrevFrameRateNdx = -1;
int   darkCornerBoxcarIndex, darkCornerBoxcarN;
int   darkCornerBoxcarSums[DARK_CORNER_NUM_PIXELS];
float darkCornerBoxcarAves[DARK_CORNER_NUM_PIXELS];
float darkCornerCorrection;


// Local Variables

// --------------------------------------------
// ***********  reference, offsets, apodizer calc processing

static int      runCentCmd = eCmdNone;          // the 'long' command that is currently being executed, if any

static int      refcentState;                   // current state of 'calc cent process'

static int      centCntr;                       // counts sets of centroid data being averaged when doing 'calc cent process'
static float    centSums[MAX_NUM_CENTROIDS];    // summed centroid data while determining 'calc cent process'

// --------------------------------------------
// ***********  dark / back calc processing

static int  darkBackMode;   // Indicates wheather doing 'back', 'dark', 'flat'
static int  darkBackCntr;   // counts frames being averaged when determining darkBack
static int  darkBackState;  // current state of the process determining darkBack

static int  darkBackFlatSums[MAX_SUBAP_ROWS_PER_FRAME][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW];     // summed 'real' for darkBackFlat data

// --------------------------------------------
// ***********  flat control structures

typedef struct _flatSet
{
    int     activeSet;

    float   tipStartPos;
    float   tiltStartPos;

    float   tipStepSize;
    int     numTipSteps;

    float   tiltStepSize;
    int     numTiltSteps;

    int     pixelRow;
    int     pixelCol;
} flatSet;

//XXX static const int flatTestCentRow = 24, flatTestPixCol = 24;     // X24Y48 (corresponds to pixel row 48)  //XXXdwp110621 -- for flat field testing
//XXX static const int flatTestCentRow = 16, flatTestPixCol = 64;     // X64Y32 (corresponds to pixel row 32)  //XXXdwp110621 -- for flat field testing
static const int flatTestCentRow = 29, flatTestPixCol = 50;     // X50Y58 (corresponds to pixel row 58)  //XXXdwp110621 -- for flat field testing
            float flatTestStepsData[FLATTER_NUM_STEPS * 4][FLATTER_NUM_STEPS * 4];             //XXXdwp110621 -- for flat field testing
flatSet flatControlSets[ ] =
{
//XXX    { 1, -750.0,  250.0, 5.0, FLATTER_NUM_STEPS, 5.0, FLATTER_NUM_STEPS, 1, 0 },  // top left pixel
//XXX    { 1,  250.0,  250.0, 5.0, FLATTER_NUM_STEPS, 5.0, FLATTER_NUM_STEPS, 1, 1 },  // top right pixel
//XXX    { 1, -750.0, -750.0, 5.0, FLATTER_NUM_STEPS, 5.0, FLATTER_NUM_STEPS, 0, 0 },  // bottom left pixel
//XXX    { 1,  250.0, -750.0, 5.0, FLATTER_NUM_STEPS, 5.0, FLATTER_NUM_STEPS, 0, 1 },  // bottom right pixel
    { 1, -1000.0,     0.0, 10.0, FLATTER_NUM_STEPS, 10.0, FLATTER_NUM_STEPS, 1, 0 },  // top left pixel
    { 1,     0.0,     0.0, 10.0, FLATTER_NUM_STEPS, 10.0, FLATTER_NUM_STEPS, 1, 1 },  // top right pixel
    { 1, -1000.0, -1000.0, 10.0, FLATTER_NUM_STEPS, 10.0, FLATTER_NUM_STEPS, 0, 0 },  // bottom left pixel
//XXX    { 1, -1000.0, -1000.0, 20.0, FLATTER_NUM_STEPS, 20.0, FLATTER_NUM_STEPS, 0, 0 },  // bottom left pixel
    { 1,     0.0, -1000.0, 10.0, FLATTER_NUM_STEPS, 10.0, FLATTER_NUM_STEPS, 0, 1 },  // bottom right pixel
    { 0,     0.0,     0.0,  0.0,                 0,  0.0,                 0, 0, 0 }
};

static int flatSetCntr;        // "Set" cntr when doing flat processing (a set is typically a 10x10 group and there are 4 sets total)
static int flatAvgCntr;        // Frame cntr when doing averages during flat processing
static int flatWaitCntr;        // Camera cycles to wait after TT move (allow settling)
static int flatTipPosCntr;    // TT Position Counter - Determines current TT position (tip)
static int flatTiltPosCntr;   // TT Position Counter - Determines current TT position (tilt)
static int flatProcessState;  // current state of the process determining darkBack

static float    flatPeakAtSet[4][MAX_NUM_SUBAPS];
static boolean  flatBoundaryErrFlags[4][MAX_NUM_SUBAPS][2];         // 0 index is left/right, 1 index is top or bottom
static int      flatBoundaryErrPositions[4][MAX_NUM_SUBAPS][2];  // 0 index is left/right, 1 index is top or bottom

static float centCMWeightsX [ 4 ] = { -1.5, -0.5, 0.5, 1.5 };  // weights for computing X center of mass    // mrr
static float centCMWeightsY [ 4 ] = { -1.5, -0.5, 0.5, 1.5 };  // weights for computing Y center of mass    // mrr
int    CentThresh;        // value centroider subtracts from raw data (then, if result is less than zero,        // mrr
                        //  centroider sets raw data to zero)
static float xCentsSums;  // sum of X centroids used to compute mean    // mrr
static float yCentsSums;  // sum of Y centroids used to compute mean    // mrr


// --------------------------------------------
// defines for Pupil Tracking
//
#define MAX_PIX_FOR_PUP_TRACK 18   // the number of pixels to sum at the edges of the pupil
#define MOVING_AVG_M          900  // moving average M for pupil tracking

#define PUPILS_PER_PUP_TRACK_CENT_UNIT 0.046  // pupil movement is reported as fractions of the overall pupil
#define PUP_LOW_LIGHT_LIMIT               500.0

enum { eLightOkay, eLightStabilizing, eLightTooLow, eLightDestabilizing };  // possible settings for pupil alignment tooLowLight flag

// --------------------------------------------
// local variables for Pupil Tracking

//    pixel indexes into the packed intensity array for pupil tracking
static int leftPix[MAX_PIX_FOR_PUP_TRACK];
static int rightPix[MAX_PIX_FOR_PUP_TRACK];
static int topPix[MAX_PIX_FOR_PUP_TRACK];
static int bottomPix[MAX_PIX_FOR_PUP_TRACK];


// ****************************************************************************
// Local Function Prototypes

static void gpAoHrtCent_Init( void );
static void gpAoHrtCent_ProcCmd( int cmdNdx );

static void gpAoHrtCent_DarkBackStart( void );
static void gpAoHrtCent_DarkBackProcess( unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] );
static void gpAoHrtCent_DarkBackAbort( void );

static void gpAoHrtCent_FlatStart( void );
static void gpAoHrtCent_FlatProcess( unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] );
static void gpAoHrtCent_FlatAbort( void );

static void gpAoHrtCent_ApoCalcStart( short apoNum );
static void gpAoHrtCent_RefCalcStart( char clearRef, int currCmd );
static void gpAoHrtCent_RefCalcProcess( float centData[], short runCmd );
static void gpAoHrtCent_RefCalcAbort( void );

static void gpAoHrtCent_AdjustCrntDarkBackFlat( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] );

static short gpAoHrtCent_ProcessCM( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
                                    short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[],
                                    float *averageIntens);

static short gpAoHrtCent_ProcessQuad( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
                                      short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[],
                                      float *averageIntens);

static void gpAoHrtCent_PupilTrack_Init( void );
static void gpAoHrtCent_PupilTrack( float pupIntensity[], float pupTrackData[] );


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent( void* _arg )
 *
 *  @brief
 *  This unit is the centroid thread mainline.
 *
 *  @b DESCRIPTION:
 *  This unit initializes the centroider thread, waits for new camera data,
 *  does any processing necessary if running a centroiding command, calculates
 *  centroids, and passes those centroids on to the reconstructor.
 *  @par
 *
 *  @b Usage: gpAoHrtCent( (void*)NULL );
 *
 *  @param[in] _arg (void*):  needed for pthread_create(), not currently used
 *
 *  @return
 *  0 :  always 0 (NULL); should never return
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *gpAoHrtCent( void *_arg )
{
    int             refCentsCalOffsetsNdx;
    int             i, currNdx;
    CamBuffType*    camBuffPtr = NULL;
    CentBuffType*   centBuffPtr = NULL;
    static int      overflowCntr = 0, overflowCheckCntr = 0;
#if ( TIMER_TEST == 0 )
    int             testNdx;
#else
    int             tmpTimerNdx;
#endif
    int             dispCntr = 0;

    static int darkCorner_PersCntr = 0;
    static int aoLight_PersCntr    = 0;
    static int pupilLight_PersCntr = 0;

    DataBufferElemStatusType* bufStatPtr;

#if (RTL_SYSTEM == 0)
    cpu_set_t mask;
    int         cpuNum = HRT_CPU_CENT;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCent) - CPU #",  cpuNum );
    if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "(gpAoHrtCent) executing sched_setaffinity() for CPU #",  cpuNum );
    sched_getaffinity( 0, sizeof( mask ), &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCent) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#else
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCent) Cent thread id: ", rtl_pthread_self());
#endif

#if (RTL_SYSTEM != 0)
    printf("    hard gpAoHrtCent() CPU:thread %d:%d\n",
    rtl_getcpuid(),rtl_pthread_self());   // sla debugging hang
#endif

//  initialize variables used by the centroid thread
    gpAoHrtCent_Init();

//  initialize variables for the pupil tracking calculations
    gpAoHrtCent_PupilTrack_Init();

//  initialize darks, backs, and/or flats
    gpAoHrtCent_AdjustCrntDarkBackFlat( NULL );

//  enter an infinite loop
    while (!QuitCentroidThread)
    {
//      wait for someone to tell us a command or data is ready
        if (rtl_sem_trywait( &CentSem ) < 0)
        {
            rtl_usleep(1);
            continue;
        }

#if ( TIMER_TEST != 0 )
        tmpTimerNdx = centFrameID % NUM_TIME_SETS;
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterCentWake]);
#endif

        // if a command is pending, call gpAoHrtCent_ProcCmd() to process it, then restart wait
        if (hrtCentDataPtr->cmdStart != eCmdNone)
        {
            SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCent): new command = ", hrtCentDataPtr->cmdStart );

            gpAoHrtCent_ProcCmd( hrtCentDataPtr->cmdStart );
            hrtCentDataPtr->cmdStart = eCmdNone;
            continue;
        }

        // get the current centroider buffer index
        currNdx = hrtCentDataPtr->centBufNdx;

        // Test if we are getting behind
#if ( TIMER_TEST != 0 )
        if ((camFrameID - centFrameID) > 2)
            overflowCntr++;
#else
        testNdx = hrtWFScamDataPtr->camWriteNdx;
        if ((testNdx - currNdx) < 0 )
            testNdx += NUM_HRT_DATA_BUFFS;    // take care of wrap, if that has occurred
        if ((testNdx - currNdx) > 2 )
            overflowCntr++;
#endif
        if ( ++overflowCheckCntr >= LegalFrameRates[hrt_camFrameRate] )
        {
            overflowCheckCntr = 0;
            if (overflowCntr > 0)
            {
                SysConSendStrngIntParm (eDebugLevelWarn, "WARN (gpAoHrtCent) Overrun cb: >2 bufs from cam (chk 1/sec): ", overflowCntr );
                overflowCntr = 0;
            }
        }

        // Set hrtBufState
        bufStatPtr = &hrtDataFlowControlPtr->bufStat[ currNdx ];    // Set pointer to frame status area
        bufStatPtr->hrtBufState = eBufCentProc;

        // get pointer to current camera and centroider buffers
        camBuffPtr = &hrtWFScamDataPtr->camBuffs[ currNdx ];
        centBuffPtr = &hrtCentDataPtr->centroidBuffs[ currNdx ];

        centBuffPtr->camBuffPtr = camBuffPtr;  // the cam buff  the centroids are calced from (to pass to
                                               // > the tweeter System Matrix calculator (for now))

#if ( TIMER_TEST == 0 )
        hrtCentDataPtr->centroidBuffs[ currNdx ].rdtscFrameStartStamp =  rdtsc();
#endif

        if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
            SysConSendStrngIntParm( eDebugLevelMax, "INFO (cent) - start", currNdx );
        #if ( CENT_DEBUG != 0 )
        {
            HRT_PRINTF( "DEBUG (gpAoHrtCent): About to process centroids - camBufPtr=0x%lx", camBuffPtr );
            HRT_PRINTF( "DEBUG (gpAoHrtCent):  WFScamDataPtr = 0x%lx", hrtWFScamDataPtr );
            HRT_PRINTF( "DEBUG (gpAoHrtCent):  camBuffType size = %d", sizeof(CamBuffType) );
        }
        #endif

//        call the centroid processor to compute centroids
        refCentsCalOffsetsNdx = hrtCentDataPtr->crntRefCentCalOffStoreBufNdx;
        //centBuffPtr->skipFrameFlag = gpAoHrtCent_ProcessQuad( camBuffPtr->usedCamData, refCentsCalOffsetsNdx,
        //                                    //XXXDWP110914
        //                                    &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[MAX_NUM_SUBAPS],
        //                                    centBuffPtr->intens, &centBuffPtr->averageIntensity);
        centBuffPtr->skipFrameFlag = gpAoHrtCent_ProcessCM( camBuffPtr->usedCamData, refCentsCalOffsetsNdx,
                                            //XXXDWP110914
                                            &centBuffPtr->xyCents[0], &centBuffPtr->xyCents[MAX_NUM_SUBAPS],
                                            centBuffPtr->intens, &centBuffPtr->averageIntensity);
        centBuffPtr->refCentCalOffStoreBufNdx = refCentsCalOffsetsNdx;
        centBuffPtr->refCentStoreBufNdx = hrtCentDataPtr->crntRefCentStoreBufNdx;

#if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterCentroid]);
#endif

        // Since we are at the end of a full frame::
        //        if we're running the dark or back command
        //            call gpAoHrtCent_DarkBackProcess() to process the raw data
        //        else if we're running the flat command
        //            call gpAoHrtCent_FlatProcess() to process the centroid data
        //        else if we're running the refcent command
        //            call gpAoHrtCent_RefCalcProcess() to process the centroid data
        switch( runCentCmd)
        {
            case eCmdTakeBacks:
            case eCmdTakeDarks:
                gpAoHrtCent_DarkBackProcess( camBuffPtr->usedCamData );
                break;

            case eCmdTakeFlats:
                gpAoHrtCent_FlatProcess( camBuffPtr->usedCamData );
                break;

            case eCmdStoreApoOffs:
//              gpAoHrtCent_ApoMeasProcess( centBuffPtr->xyCents );
                break;

            case eCmdSaveRefOffs:
            case eCmdSaveRefCents:
                gpAoHrtCent_RefCalcProcess( centBuffPtr->xyCents, runCentCmd );
                break;
        }

#if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterCentMoveUnpack]);
#endif

//      tell the Recon thread that data's ready
        bufStatPtr->hrtBufState = eBufReqRecon;

        if (rtl_sem_post(&ReconSem) < 0)
            SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCent) posting to ReconSem, errno = ", rtl_errno);

//      now that reconstructor is kicked off, calculate the pupil alignment
        gpAoHrtCent_PupilTrack( centBuffPtr->intens, centBuffPtr->pupTrackData );
#if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[tmpTimerNdx][eTimerAfterCentPupTrack]);
#endif

//      also, now that reconstructor is kicked off, adjust darks, backs, and/or flats
        gpAoHrtCent_AdjustCrntDarkBackFlat( camBuffPtr->usedCamData );

//      thirdly, range check values for the TLC error flags
        if ((hrtFileDataShmPtr->tlcErrDarkCornerVal >= hrtFileDataShmPtr->rangesCornerCrct[0]) &&
            (hrtFileDataShmPtr->tlcErrDarkCornerVal <= hrtFileDataShmPtr->rangesCornerCrct[1]))
        {
            darkCorner_PersCntr = 0;
            hrtFileDataShmPtr->tlcErrStatArray[eTlcErrDarkCorner] = eTlcErrNone;
        }
        else if (++darkCorner_PersCntr > DARK_CORNER_PERS_CNT)
        {
            darkCorner_PersCntr = 0;
            if (hrtFileDataShmPtr->tlcErrDarkCornerVal < hrtFileDataShmPtr->rangesCornerCrct[0])
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrDarkCorner] = eTlcErrLow;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrDarkCorner] = eTlcErrLow;
            }
            else
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrDarkCorner] = eTlcErrHigh;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrDarkCorner] = eTlcErrHigh;
            }
        }
        if ((hrtFileDataShmPtr->tlcErrAoLight > 0) &&  // tlcErrAoLight gets set to 0 if the numOverThreshSubaps test doesn't pass
            (hrtFileDataShmPtr->tlcErrAoLight <= hrtFileDataShmPtr->rangesAoLight))
            aoLight_PersCntr = 0;
        else if (++aoLight_PersCntr > AO_LIGHT_PERS_CNT)
        {
            aoLight_PersCntr = 0;
            if (hrtFileDataShmPtr->tlcErrAoLight > 0)
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrAoTooLittleLight] = eTlcErrNone;
            else
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrAoTooLittleLight] = eTlcErrLow;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrAoTooLittleLight] = eTlcErrLow;
            }
            if (hrtFileDataShmPtr->tlcErrAoLight <= hrtFileDataShmPtr->rangesAoLight)
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrAoTooMuchLight] = eTlcErrNone;
            else
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrAoTooMuchLight] = eTlcErrHigh;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrAoTooMuchLight] = eTlcErrHigh;
            }
        }
        if ((hrtFileDataShmPtr->tlcErrPupilLight >= hrtFileDataShmPtr->rangesPupLight[0]) &&
            (hrtFileDataShmPtr->tlcErrPupilLight <= hrtFileDataShmPtr->rangesPupLight[1]))
            pupilLight_PersCntr = 0;
        else if (++pupilLight_PersCntr > PUPIL_LIGHT_PERS_CNT)
        {
            pupilLight_PersCntr = 0;
            if (hrtFileDataShmPtr->tlcErrPupilLight >= hrtFileDataShmPtr->rangesPupLight[0])
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrPupTooLittleLight] = eTlcErrNone;
            else
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrPupTooLittleLight] = eTlcErrLow;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrPupTooLittleLight] = eTlcErrLow;
            }
            if (hrtFileDataShmPtr->tlcErrPupilLight <= hrtFileDataShmPtr->rangesPupLight[0])
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrPupTooMuchLight] = eTlcErrNone;
            else
            {
                hrtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrPupTooMuchLight] = eTlcErrHigh;
                hrtFileDataShmPtr->tlcErrStatArray[eTlcErrPupTooMuchLight] = eTlcErrHigh;
            }
        }

//      and, finally, now that reconstructor is kicked off, move the intensity and centroid data to a display
//      > buffer at 10 fps -- this will get picked up by dispint and/or dispcent, if they are running
        if (++dispCntr >= (LegalFrameRates[hrt_camFrameRate] / 10))
        {
            dispCntr = 0;

            hrtCentDataPtr->centDispWriteBuffNdx ^= 0x1;
            for ( i = 0; i < MAX_NUM_SUBAPS; i++ )
            {
                hrtCentDataPtr->centDispCents[hrtCentDataPtr->centDispWriteBuffNdx][i] = centBuffPtr->xyCents[i];
                hrtCentDataPtr->centDispCents[hrtCentDataPtr->centDispWriteBuffNdx][i + MAX_NUM_SUBAPS] =
                    centBuffPtr->xyCents[i + MAX_NUM_SUBAPS];
                hrtCentDataPtr->centDispIntens[hrtCentDataPtr->centDispWriteBuffNdx][i]    = centBuffPtr->intens[i];
            }
            hrtCentDataPtr->centDispReadBuffNdx = hrtCentDataPtr->centDispWriteBuffNdx;
        }

        if( (currNdx % LegalFrameRates [hrt_camFrameRate]) == 0 )
            SysConSendStrngIntParm( eDebugLevelMax, "INFO (cent) - end", currNdx );

        // We are at end of current frame - Update all camera buffer pointers
        // Incr read index and if past end of buffer - wrap it
#if ( TIMER_TEST != 0 )
        centFrameID++;
#endif
        currNdx++;
        currNdx = currNdx % NUM_HRT_DATA_BUFFS;
        hrtCentDataPtr->centBufNdx = currNdx;

        hrtFileDataShmPtr->watchDogBits |= WatchDog_CentBit;
    }
    return 0;
}

/*!
 *******************************************************************************
 *  @fn gpAoHrtCent_Init( void )
 *
 *  @brief
 *  Initialize variables used by the centroid thread.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the centroid thread.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_Init( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static void gpAoHrtCent_Init( void )
{
int     i, j;
int     retVal;
struct rtl_timespec next;

//  initialize variables
    hrtCentDataPtr->cmdStart = eCmdNone;
    hrtCentDataPtr->cmdCrnt = eCmdNone;

    hrtCentDataPtr->centBufNdx = 0;

    for (i = 0; i < NUM_REFCENT_STORE_BUFS; i++)
    {
        for (j = 0; j < MAX_NUM_CENTROIDS; j++)
        {
            hrtCentDataPtr->refCentStoreBuffers[i][j] = 0.0;
            hrtCentDataPtr->refCentCalOffStoreBuffers[i][j] = 0.0;
        }
    }
    hrtCentDataPtr->crntRefCentStoreBufNdx = 0;
    hrtCentDataPtr->crntRefCentCalOffStoreBufNdx = 0;

    hrtCentDataPtr->centDispWriteBuffNdx = 0;
    hrtCentDataPtr->centDispReadBuffNdx = 1;

//  spin on the OkayToFinishInit flag to let sysCon finish getting file data from the SRT
    printf("          Cent_Init starting to spin on !OkayToFinishInit\n");
    while (!OkayToFinishInit)
    {
        next.tv_sec = 0;
        next.tv_nsec = 100000000;
        if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (gpAoHrtCent_Init) rtl_nanosleep(), retVal = ", retVal);
    }
    printf("          Cent_Init Ending spinning on !OkayToFinishInit\n");

//  note that numCentroids and numSubaps are calculated at system startup in gpAoSrtHostICon_ReadAperture()
    hrt_numSubaps = hrtFileDataShmPtr->numSubaps;         // number of wavefront sensor (WFS) sub-apertures
    hrt_numCentroids = hrtFileDataShmPtr->numCentroids;     // number of centroids

    CentInitDone = true;
}

/**********************************************************************************************
UNIT NAME:  CentRestart()

UNIT DESCRIPTION:
The purpose of this unit is to initialize variables used by the centroid thread when processing
is restarted after the camera has been turned off then back on.

UNIT INPUTS:
    None

UNIT OUTPUTS:
    None

UNIT ERROR CODES:
    None

**********************************************************************************************/
void gpAoHrtCent_Restart( void )
{

//  initialize variables
    hrtCentDataPtr->centBufNdx = 0;
}

/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_ProcCmd( int cmdNdx )
 *
 *  @brief
 *  Process centroid commands.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process commands from the host (via the SRT
 *  side)
 *  @par
 *
 *  @b Usage: gpAoHrtCent_ProcCmd( cmdNdx );
 *
 *  @param[in]  cmdNdx (int):  index into CmdTable[] for the command to be processed
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static void gpAoHrtCent_ProcCmd( int cmdNdx )
{
    int   i;
    int   tmpInt = 0, tmpNdx;
    float *pData;

    SysConSendStrngIntParm(eDebugLevelMax, "INFO (gpAoHrtCent_ProcCmd) Got a Centroid Command, NDX = ", cmdNdx);

//  switch on command ID corresponding to command index
    switch (CmdTable[cmdNdx].idNum)
    {
        case eCmdAbort:          // abort long commands
            switch (runCentCmd)
            {
                case eCmdTakeBacks:
                case eCmdTakeDarks:
                    gpAoHrtCent_DarkBackAbort();
                    break;

                case eCmdTakeFlats:
                    gpAoHrtCent_FlatAbort();
                    break;

                case eCmdSaveRefCents:
                    gpAoHrtCent_RefCalcAbort();
                    break;

                case eCmdSaveRefOffs:
                    gpAoHrtCent_RefCalcAbort();
                    break;

                default:
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCent_ProcCmd): unexpected command abort received by cent");
                    break;
            }
            break;

        case eCmdOffsetRefCents:
            {
                float signVal = ( hrtCentDataPtr->cmdIntParam[0] > 0 ) ? 1.0 : -1.0;                // Determine if we are adding or subtracting the offset

                if( (hrtCentDataPtr->cmdIntParam[1] & 0x2) != 0 )                                    // use host provided offsets
                    for (i = 0; i < hrt_numCentroids; i++)
                        hrt_currRefCentsQ[i] += (signVal * hrtFileDataShmPtr->shm_offCentsQ_offsets[i]);

                if( (hrtCentDataPtr->cmdIntParam[1] & 0x4) != 0 )                                    // use cal re-phase offsets
                    for (i = 0; i < hrt_numCentroids; i++)
                        hrt_currRefCentsQ[i] += (signVal * hrtFileDataShmPtr->shm_offCentsQ_Cal_RePhase[i]);

                if( (hrtCentDataPtr->cmdIntParam[1] & 0x8) != 0 )
                {
                    for (i = 0; i < hrt_numCentroids; i++)
                        hrt_currRefCentsQ[i] += (signVal * hrtFileDataShmPtr->shm_offCentsQ_Cal[i]);
                }

                tmpInt = ( hrtCentDataPtr->cmdIntParam[1] >> 4 ) & 0xF;
                switch( tmpInt )
                {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                        for( i=0; i<hrt_numCentroids; i++)
                            hrt_currRefCentsQ[i] += (signVal * hrtFileDataShmPtr->shm_offCentsQ_Apodizer[tmpInt][i]);

                    // FALL THROUGH!

                    case 0:
                        tmpNdx = hrtCentDataPtr->crntRefCentStoreBufNdx;
                        if (++tmpNdx >= NUM_REFCENT_STORE_BUFS)
                            tmpNdx = 0;
                        pData = hrtCentDataPtr->refCentStoreBuffers[tmpNdx];
                        for (i = 0; i < hrt_numCentroids; i++)  // update current Ref Cents storage buffer (used for diag dump)
                            pData[i] = hrt_currRefCentsQ[i];
                        hrtCentDataPtr->crntRefCentStoreBufNdx = tmpNdx;

                        SysConSendStrngNoParm(eDebugLevelMin, "INFO 'offsetRefCents' complete");
// mrr DM                        AOC_HRT_GMB_WRITE_CMD_STATUS(  hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
                        hrtCentDataPtr->cmdCrnt = eCmdNone;
                        break;

                    default:
                        SysConSendStrngNoParm(eDebugLevelErr, "ERR 'offsetRefCents' error" );
// mrr DM                        AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Unexpected apodizer number" );
                        hrtCentDataPtr->cmdCrnt = eCmdNone;
                        break;
                }
            }
            break;

        case eCmdSetRefCents:
            if( (hrtCentDataPtr->cmdIntParam[0] & 0x1) == 0 )
            {
                for (i = 0; i < hrt_numCentroids; i++)
                    hrt_currRefCentsQ[i] = 0;
            }
            else
            {
                for (i = 0; i < hrt_numCentroids; i++)
                    hrt_currRefCentsQ[i] = hrtFileDataShmPtr->shm_refCentsQ[i];
            }

            if( (hrtCentDataPtr->cmdIntParam[0] & 0x2) != 0 )
            {
                for (i = 0; i < hrt_numCentroids; i++)
                    hrt_currRefCentsQ[i] += hrtFileDataShmPtr->shm_offCentsQ_offsets[i];
            }

            if( (hrtCentDataPtr->cmdIntParam[0] & 0x4) != 0 )
            {
                for (i = 0; i < hrt_numCentroids; i++)
                    hrt_currRefCentsQ[i] += hrtFileDataShmPtr->shm_offCentsQ_Cal_RePhase[i];
            }

            if( (hrtCentDataPtr->cmdIntParam[0] & 0x8) != 0 )
            {
                for (i = 0; i < hrt_numCentroids; i++)
                    hrt_currRefCentsQ[i] += hrtFileDataShmPtr->shm_offCentsQ_Cal[i];
            }

            tmpInt = ( hrtCentDataPtr->cmdIntParam[0] >> 4 ) & 0xF;
            switch( tmpInt )
            {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                    for (i = 0; i < hrt_numCentroids; i++)
                        hrt_currRefCentsQ[i] += hrtFileDataShmPtr->shm_offCentsQ_Apodizer[tmpInt][i];
                    // FALL THROUGH!

                case 0:
                    tmpNdx = hrtCentDataPtr->crntRefCentStoreBufNdx;
                    if (++tmpNdx >= NUM_REFCENT_STORE_BUFS)
                        tmpNdx = 0;
                    pData = hrtCentDataPtr->refCentStoreBuffers[tmpNdx];
                    for (i = 0; i < hrt_numCentroids; i++)  // update current Ref Cents storage buffer (used for diag dump)
                        pData[i] = hrt_currRefCentsQ[i];
                    hrtCentDataPtr->crntRefCentStoreBufNdx = tmpNdx;

                    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'setRefCents' complete");
// mrr DM                    AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
                    hrtCentDataPtr->cmdCrnt = eCmdNone;
                    break;

                default:
                    SysConSendStrngNoParm(eDebugLevelErr, "ERR 'setRefCents' error" );
// mrr DM                    AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Unexpected apodizer value" );
                    hrtCentDataPtr->cmdCrnt = eCmdNone;
                    break;
            }
            break;

        case eCmdSaveRefOffs:  // saves centroid offsets  (long command)
            gpAoHrtCent_RefCalcStart( false, CmdTable[cmdNdx].idNum  );
            break;

        case eCmdSaveRefCents:     // determines and stores new ref cents (long command)
            gpAoHrtCent_RefCalcStart( true, CmdTable[cmdNdx].idNum  );
            break;

        case eCmdTakeBacks:    // determines and stores new wfs BkGrnd file(s) (bkgrnd levels) (long command)
            darkBackMode = eDarkBack_Back;
            gpAoHrtCent_DarkBackStart( );
            break;

        case eCmdTakeDarks:    // determines and stores new wfs Dark file(s) (dark levels) (long command)
            darkBackMode = eDarkBack_Dark;
            gpAoHrtCent_DarkBackStart( );
            break;

        case eCmdTakeFlats:    // determines and stores new wfs BkGrnd file (bkgrnd levels) (long command)
            darkBackMode = eDarkBack_Flat;
            gpAoHrtCent_FlatStart( );
            break;

        case eCmdSetDrkBck:
            hrt_applyBackData = (hrtCentDataPtr->cmdIntParam[0] & 0x2);
            hrt_applyDarkData = (hrtCentDataPtr->cmdIntParam[0] & 0x1);
            gpAoHrtFileData_Put(eFileTypeParms);

// mrr DM            AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'setDarksBacks' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdSetFlats:
            hrt_applyFlatData = (hrtCentDataPtr->cmdIntParam[0] & 0x1);
            gpAoHrtFileData_Put(eFileTypeParms);

// mrr DM   AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'setFlats' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdThresh:  // sets threshold used by centroider on raw data
            hrt_CentThresh = hrtCentDataPtr->cmdIntParam[0];
            gpAoHrtFileData_Put(eFileTypeParms);
            {
                // RESPONSE setCentThresh
                int iarr[MAX_NUM_CMD_PARAMS];
                iarr[0] = hrt_CentThresh;
                diagCmdResponse(CmdRespSetCentThresh, 0, iarr, NULL, NULL);
            }

// mrr DM   AOC_HRT_GMB_WRITE_CMD_STATUS(  hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'set thresh' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        case eCmdCalOnOff:
            switch( hrtCentDataPtr->cmdIntParam[0] )
            {
                case 0:
                case 1:
                case 2:  // 2 is an undocumented feature for testing -- it causes Cal phase and Cal ref cent offs to be stored when they change
                    hrt_applyCalData = hrtCentDataPtr->cmdIntParam[0];
                    hrtFileDataShmPtr->applyCalData = hrtCentDataPtr->cmdIntParam[0];
                    gpAoHrtFileData_Put(eFileTypeParms);

// mrr DM           AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
                    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'set calDataOnOff' complete");
                    break;

                default:
// mrr DM           AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 1, GPI_STATE_ERROR, "Bad select value" );
                    SysConSendStrngIntParm(eDebugLevelErr, "ERR 'set CalDataOnOff' param is incorrect ", hrtCentDataPtr->cmdIntParam[0] );
                    break;
            }
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        default:
            SysConSendStrngIntParm(eDebugLevelErr, "ERR (gpAoHrtCent_ProcCmd) unexpected command received by cent, cmdNdx = ", cmdNdx);
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            hrtCentDataPtr->cmdStart = eCmdNone;
            break;
    }
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (gpAoHrtCent_ProcCmd) Exit Cent_ProcCmd()");
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_DarkBackStart( void )
 *
 *  @brief
 *  Initialize the processing of the takePixelDarks or takePixelBacks command.
 *
 *  @b DESCRIPTION:
 *  Start the processing of the takePixelDarks or takePixelBacks command by
 *  initializing the sums to be used to compute averages and setting counters and
 *  flags necessary to run the command.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_DarkBackStart( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_DarkBackStart( void )
{
    int i, j, k;

//  initialize darkBack sums to zeroes
    for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
    {
        for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
        {
            for (k = 0; k < NUM_PIX_IN_ROW; k++)
                darkBackFlatSums[i][j][k] = 0;
        }
    }

//  initialize sum counter and darkBack state and set flag to indicate that the darkBack command is running
    switch( darkBackMode )
    {
      case eDarkBack_Back:
        runCentCmd = eCmdTakeBacks;
        break;
      case eDarkBack_Dark:
        runCentCmd = eCmdTakeDarks;
        break;
    }

    hrtFileDataShmPtr->doneLongDiskWrite = false;  // this will be set true when the SRT has finished write to disk
    darkBackState = eCmdStateSumDarkBack;
    darkBackCntr = 0;
    return;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_DarkBackProcess( unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
 *
 *  @brief
 *  Process the takePixelDarks or takePixelBacks command
 *
 *  @b DESCRIPTION:
 *  Process the takePixelDarks or takePixelBacks command by receiving BacksAvgN
 *  frames worth of raw camera data, averaging those frames, and writing the result
 *  to a file when done.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_DarkBackProcess( pixData[] );
 *
 *  @b param
 *  @param[in]        rawData        (short[][][])    - pixel data (i.e., raw data for the real image area)
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_DarkBackProcess( unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
{
    int i, j, k;

//  sum or average and store darkBack data, depending on darkBackState
    switch (darkBackState)
    {
        case eCmdStateSumDarkBack:
//          add current raw frame data to darkBack sums
            for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
            {
                for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
                {
                    for (k = 0; k < NUM_PIX_IN_ROW; k++)
                        darkBackFlatSums[i][j][k] += rawData[i][j][k];
                }
            }

//          increment sum counter and change state to average data if we have enough data
            if (++darkBackCntr >= hrt_BacksAvgN)
                darkBackState = eCmdStateAvg;
            break;

        case eCmdStateAvg:
//          average the wfs bkGrnd / dark sums
            for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
            {
                for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
                {
                    for (k = 0; k < NUM_PIX_IN_ROW; k++)
                    {
                        switch( darkBackMode)
                        {
                            case eDarkBack_Back:
                                hrt_wfsBkGrndData[i][j][k] = (darkBackFlatSums[i][j][k] / darkBackCntr);
                                break;
                            case eDarkBack_Dark:
                                hrt_wfsDarkData[i][j][k] = (darkBackFlatSums[i][j][k] / darkBackCntr);
                                break;
                        }
                    }
                }
            }

//          command the SRT to store the Wfs BkGrnd data to disk
            hrtFileDataShmPtr->doneLongDiskWrite = false;
            switch( darkBackMode)
            {
                case eDarkBack_Back:
                    gpAoHrtFileData_Put( eFileTypeWfsBkGrnd_0 + hrt_camFrameRate );
                    break;
                case eDarkBack_Dark:
                    gpAoHrtFileData_Put( eFileTypeWfsDark_0 + hrt_camFrameRate );
                    break;
            }
            darkBackState = eCmdStateWaitWrite;
            break;

//      added to ensure we are done before Notice to host - also allows time for SRT to get frame rate etc, before change
        case eCmdStateWaitWrite:
            if( !hrtFileDataShmPtr->doneLongDiskWrite )
                break;

//          set flags to indicate that command is not running and inform the host
            runCentCmd = eCmdNone;
            darkBackMode = eDarkBack_None;

// mrr DM   AOC_HRT_GMB_WRITE_CMD_STATUS(  hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'takePixel(Dark/Back)' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        default:
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCent_DarkBackProcess): unexpected darkBackState");
            break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_DarkBackAbort( void )
 *
 *  @brief
 *  Abort the measurePixelBacks command
 *
 *  @b DESCRIPTION:
 *  Abort the takePixelDarks or takePixelBacks command by simply setting the command
 *  flags to indicate that no command is executing (since the new background data
 *  is stored in a temporary buffer until the command completes, there is no additional
 *  cleanup to be done here)
 *  @par
 *
 *  @b Usage: gpAoHrtCent_BackAbort( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_DarkBackAbort( void )
{
// mrr DM    AOC_HRT_GMB_WRITE_CMD_STATUS( runCentCmd, 0, GPI_STATE_IDLE, "" );
    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'takePixel(Dark/Back)' aborted");
    runCentCmd = eCmdNone;
    hrtCentDataPtr->cmdCrnt = eCmdNone;
    darkBackMode = eDarkBack_None;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_FlatStart( void )
 *
 *  @brief
 *  Initialize variables and sums for calculations of the pixel flat-fields.
 *
 *  @b DESCRIPTION:
 *  Start the processing of the takePixelFlats command by initializing the sums
 *  to be used to compute averages and setting counters and flags necessary to
 *  run the command.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_FlatStart( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_FlatStart( void )
{
    int i, j, k, setNum;

//  initialize darkBack sums to zeroes
    for( i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
    {
        for( j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
        {
            for( k = 0; k < NUM_PIX_IN_ROW; k++)
                darkBackFlatSums[i][j][k] = 0;
        }
    }

    for( setNum = 0; flatControlSets[setNum].activeSet > 0; setNum++ )
    {
        for( i = 0; i < MAX_NUM_SUBAPS; i++)
            flatPeakAtSet[setNum][i] = 0.0;
    }

//  initialize sum counter and darkBack state and set flag to indicate that the darkBack command is running
    runCentCmd = eCmdTakeFlats;
    hrtFileDataShmPtr->doneLongDiskWrite = false;  // this will be set true when the SRT has finished write to disk
    flatProcessState = eCmdStateFlatWait;
    flatAvgCntr = 0;
    flatWaitCntr = 0;
    flatSetCntr = 0;
    flatTipPosCntr = flatTiltPosCntr = 0;

    return;
}

/*!
 *******************************************************************************
 *
 *  @fn static void gpAoHrtCent_FlatProcess(  unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
 *
 *  @brief
 *  Process data to determine flat-fields.
 *
 *  @b DESCRIPTION:
 *  Process the takePixelFlats command by receiving FlatsAvgN frames worth
 *  of raw camera data, averaging those frames, calculating corresponding pixel
 *  gains, and writing the result to a file when done.
 *  @par
 *  The idea is to raster the tip/tilt stage back and forth to find the brightest
 *  spot on each pixel of each sub-ap and then calculate the 'local' gain of
 *  each sub-ap with respect to that brightest pixel.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_FlatProcess( );
 *
 *  @b param
 *  @param[in]    rawData    (short[][][])    - pixel data (i.e., raw data for the active image area)
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_FlatProcess( unsigned short rawData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
{
    int     i, j, k, thisCentRow, pixRow, pixCol, centNdx, subApNdx;
    float   ttAngle[MAX_TT_ACTS], ttVolts[MAX_TT_ACTS];
    float   tmpPixel;

    //  sum or average and store Flat data, depending on flatProcessState
    switch (flatProcessState)
    {
        case eCmdStateFlatWait:
            if( flatWaitCntr == 0 )    // When (flatWaitCntr==0) update TT position
            {
                flatAvgCntr = 0;  // reset the averaging cntr when doing a TT move

//              if at the start of a set (when tip/tilt position counters equal 0), zero out the peaks
                if( (flatTipPosCntr == 0) && (flatTiltPosCntr == 0) )
                {
                    for( centNdx = 0; centNdx < MAX_NUM_SUBAPS; centNdx++)
                        flatPeakAtSet[flatSetCntr][centNdx] =  0.0;
                }

//              move to the correct tip/tilt position
                ttAngle[0] = flatControlSets[flatSetCntr].tipStartPos +
                                flatTipPosCntr * flatControlSets[flatSetCntr].tipStepSize;  //XXX + hrt_tt_x_offset;
                ttAngle[1] = flatControlSets[flatSetCntr].tiltStartPos +
                                flatTiltPosCntr * flatControlSets[flatSetCntr].tiltStepSize;  //XXX + hrt_tt_y_offset;
                gpAoHrtVolt_ConvTT( ttAngle, ttVolts, NULL );
                gpAoHrtVolt_WriteTTVolts( ttVolts, true );

//XXX                printf("pix: %d, lr: %d, tb: %d, tip: %f, tilt: %f\n", flatSetCntr, flatTipPosCntr, flatTiltPosCntr, ttAngle[0], ttAngle[1]);  //XXXdwp110616 -- TEST ONLY !!
            }

//             if enough settling time has passed (100 msecs), then start to sum the values ('eCmdStateSumFlat')
#if (EXAOTEST != 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
            if (flatWaitCntr++ > ((float)LegalFrameRates [hrt_camFrameRate] / FLAT_PROCESS_FRATE_WAIT_DIVISOR / 10))
//XXX            if (flatWaitCntr++ > ((float)LegalFrameRates [hrt_camFrameRate] / FLAT_PROCESS_FRATE_WAIT_DIVISOR))
#endif
            {
                flatWaitCntr = 0;
                flatProcessState = eCmdStateSumFlat;
            }
            break;

        case eCmdStateSumFlat:
//          sum the current values
            for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
            {
                for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
                {
                    for (k = 0; k < NUM_PIX_IN_ROW; k++)
                        darkBackFlatSums[i][j][k] += rawData[i][j][k];  // sum data (the dark will be subtracted out in one lump)
                }
            }
            if (++flatAvgCntr >= hrt_FlatsAvgN)
            {
                flatProcessState = eCmdStateAvg;
                flatAvgCntr = 0;
            }
            break;

        case eCmdStateAvg:    // average the flat sums and find peak over the group
//          update the peak value for the current 'flatSetCntr' pixel - which defines which pixel we are working on as well
            for (subApNdx = 0; subApNdx < hrt_numSubaps; subApNdx++)
            {
//              adjust row for current centroid row
                thisCentRow = hrtFileDataShmPtr->quadCellDescs[subApNdx].subApRow;
                pixRow = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixRows[flatControlSets[flatSetCntr].pixelRow];
                pixCol = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixCols[flatControlSets[flatSetCntr].pixelCol];

                tmpPixel = (float)((double)darkBackFlatSums[thisCentRow][pixRow][pixCol] / (double)hrt_FlatsAvgN -
                            (double)hrt_wfsDarkData[thisCentRow][pixRow][pixCol]);

//XXXdwp110621 -- start
//              store all the steps for a representative sub-ap
                if ((thisCentRow == flatTestCentRow) && ((pixCol == flatTestPixCol) || (pixCol == (flatTestPixCol + 1))))
                {
                    int tmpPixRow = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixRows[0];
                    int tmpPixCol = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixCols[0];

                    float tmpPixel1 = (float)((double)darkBackFlatSums[thisCentRow][tmpPixRow    ][tmpPixCol    ] / (double)hrt_FlatsAvgN -
                                               (double)hrt_wfsDarkData[thisCentRow][tmpPixRow    ][tmpPixCol    ]);
                    float tmpPixel2 = (float)((double)darkBackFlatSums[thisCentRow][tmpPixRow    ][tmpPixCol + 1] / (double)hrt_FlatsAvgN -
                                               (double)hrt_wfsDarkData[thisCentRow][tmpPixRow    ][tmpPixCol + 1]);
                    float tmpPixel3 = (float)((double)darkBackFlatSums[thisCentRow][tmpPixRow + 1][tmpPixCol    ] / (double)hrt_FlatsAvgN -
                                               (double)hrt_wfsDarkData[thisCentRow][tmpPixRow + 1][tmpPixCol    ]);
                    float tmpPixel4 = (float)((double)darkBackFlatSums[thisCentRow][tmpPixRow + 1][tmpPixCol + 1] / (double)hrt_FlatsAvgN -
                                               (double)hrt_wfsDarkData[thisCentRow][tmpPixRow + 1][tmpPixCol + 1]);

                    int whereLightIsRowOffset = flatControlSets[flatSetCntr].pixelRow * FLATTER_NUM_STEPS * 2;
                    int whereLightIsColOffset = flatControlSets[flatSetCntr].pixelCol * FLATTER_NUM_STEPS * 2;

                    flatTestStepsData[whereLightIsRowOffset +                     flatTiltPosCntr]
                                     [whereLightIsColOffset +                     flatTipPosCntr] = tmpPixel1;
                    flatTestStepsData[whereLightIsRowOffset +                     flatTiltPosCntr]
                                     [whereLightIsColOffset + FLATTER_NUM_STEPS + flatTipPosCntr] = tmpPixel2;
                    flatTestStepsData[whereLightIsRowOffset + FLATTER_NUM_STEPS + flatTiltPosCntr]
                                     [whereLightIsColOffset +                     flatTipPosCntr] = tmpPixel3;
                    flatTestStepsData[whereLightIsRowOffset + FLATTER_NUM_STEPS + flatTiltPosCntr]
                                     [whereLightIsColOffset + FLATTER_NUM_STEPS + flatTipPosCntr] = tmpPixel4;
                }
//XXXdwp110621 -- end

                if (tmpPixel > flatPeakAtSet[flatSetCntr][subApNdx])
                {
                    flatPeakAtSet[flatSetCntr][subApNdx] = tmpPixel;
//                  check whether we're at a boundary; if we are, we probably haven't found a peak (or at least we don't know)
                    if ((flatTipPosCntr > 0) &&
                        (flatTipPosCntr < (flatControlSets[flatSetCntr].numTipSteps - 1)))
                        flatBoundaryErrFlags[flatSetCntr][subApNdx][0] = false;
                    else
                    {
                        flatBoundaryErrFlags[flatSetCntr][subApNdx][0] = true;                   // left or right
                        flatBoundaryErrPositions[flatSetCntr][subApNdx][0] = flatTipPosCntr;
                    }
                    if ((flatTiltPosCntr > 0) &&
                        (flatTiltPosCntr < (flatControlSets[flatSetCntr].numTiltSteps - 1)))
                        flatBoundaryErrFlags[flatSetCntr][subApNdx][1] = false;
                    else
                    {
                        flatBoundaryErrFlags[flatSetCntr][subApNdx][1] = true;                   // top or bottom
                        flatBoundaryErrPositions[flatSetCntr][subApNdx][1] = flatTiltPosCntr;
                    }
                }
            }

//          set darkBack sums to zeroes
            for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
            {
                for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
                {
                    for (k = 0; k < NUM_PIX_IN_ROW; k++)
                        darkBackFlatSums[i][j][k] = 0;
                }
            }

//          move the 'set' position pointers to their next locations, if done with group, both will be set to zero
            if ((++flatTipPosCntr) >= flatControlSets[flatSetCntr].numTipSteps)
            {
                flatTipPosCntr = 0;
                if ((++flatTiltPosCntr) >= flatControlSets[flatSetCntr].numTiltSteps)
                {
                    flatTiltPosCntr = 0;
                    flatProcessState = eCmdStateAvg;
                }
            }

//          increment 'set' counter - if next set is 'active' then restart process, else calc and save gains
            flatProcessState = eCmdStateFlatWait;  // preset the new state to postion and wait for the next step in this pixel

//          if done with current pixel (tip/tilt positions == 0) and done with all pixels (activeSet == 0) then calc gains
            if ((flatTipPosCntr == 0) && (flatTiltPosCntr == 0))
            {
                if (!flatControlSets[++flatSetCntr].activeSet)     // if done, set the state to "calc gain"
                    flatProcessState = eCmdStateFlatCalcGain;
            }
            break;

        case eCmdStateFlatCalcGain:
            {
                boolean flatBoundaryErrFlag = false;
                int flatBoundaryErrCntrsLR[4];      // left or right
                int flatBoundaryErrPosAvgsLR[4];  // left or right
                int flatBoundaryErrCntrsTB[4];      // top or bottom
                int flatBoundaryErrPosAvgsTB[4];  // top or bottom

//              check for boundary errors and flag an error if we have any
                for (flatSetCntr = 0; flatControlSets[flatSetCntr].activeSet > 0; flatSetCntr++)
                {
                    flatBoundaryErrCntrsLR[flatSetCntr] = 0;
                    flatBoundaryErrPosAvgsLR[flatSetCntr] = 0;
                    flatBoundaryErrCntrsTB[flatSetCntr] = 0;
                    flatBoundaryErrPosAvgsTB[flatSetCntr] = 0;
                }
                for(subApNdx = 0; subApNdx < hrt_numSubaps; subApNdx++)
                {
                    for (flatSetCntr = 0; flatControlSets[flatSetCntr].activeSet > 0; flatSetCntr++)
                    {
                        if (flatBoundaryErrFlags[flatSetCntr][subApNdx][0])
                        {
                            flatBoundaryErrFlag = true;
                            flatBoundaryErrCntrsLR[flatSetCntr]++;
                            flatBoundaryErrPosAvgsLR[flatSetCntr] += flatBoundaryErrPositions[flatSetCntr][subApNdx][0];
                        }
                        if (flatBoundaryErrFlags[flatSetCntr][subApNdx][1])
                        {
                            flatBoundaryErrFlag = true;
                            flatBoundaryErrCntrsTB[flatSetCntr]++;
                            flatBoundaryErrPosAvgsTB[flatSetCntr] += flatBoundaryErrPositions[flatSetCntr][subApNdx][1];
                        }
                    }
                }

                if (flatBoundaryErrFlag)
                {
                    char tmpText[DIAG_STRING_MAX_LENGTH];
                    for (flatSetCntr = 0; flatControlSets[flatSetCntr].activeSet > 0; flatSetCntr++)
                    {
                        if (flatBoundaryErrCntrsLR[flatSetCntr] > 0)
                        {
                            flatBoundaryErrPosAvgsLR[flatSetCntr] = flatBoundaryErrPosAvgsLR[flatSetCntr] * 10 /
                                                                        flatBoundaryErrCntrsLR[flatSetCntr];
                            sprintf(tmpText, "ERR (gpAoHrtCent_FlatProcess): flatBoundsErr lrPix: %d, lrCnt: %d, lrAvg: %d",
                                                flatSetCntr, flatBoundaryErrCntrsLR[flatSetCntr], flatBoundaryErrPosAvgsLR[flatSetCntr]);
                            SysConSendStrngNoParm(eDebugLevelErr, tmpText);
                        }
                        if (flatBoundaryErrCntrsTB[flatSetCntr] > 0)
                        {
                            flatBoundaryErrPosAvgsTB[flatSetCntr] = flatBoundaryErrPosAvgsTB[flatSetCntr] * 10 /
                                                                        flatBoundaryErrCntrsTB[flatSetCntr];
                            sprintf(tmpText, "ERR (gpAoHrtCent_FlatProcess): flatBoundsErr tbPix: %d, tbCnt: %d, tbAvg: %d",
                                                flatSetCntr, flatBoundaryErrCntrsTB[flatSetCntr], flatBoundaryErrPosAvgsTB[flatSetCntr]);
                            SysConSendStrngNoParm(eDebugLevelErr, tmpText);
                        }
                    }
                    hrtFileDataShmPtr->doneLongDiskWrite = true;
                    flatProcessState = eCmdStateWaitWrite;
                }
//XXX XXX -- for now, let the file write even if there's a boundary error                else
                {
//                  zero the flat field data
                    for (i = 0; i < MAX_SUBAP_ROWS_PER_FRAME; i++)
                    {
                        for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
                        {
                            for (k = 0; k < NUM_PIX_IN_ROW; k++)
                                hrt_wfsFlatData[i][j][k] = 0.0;
                        }
                    }

                    boolean foundZeroFlag = false;
                    float max;
                    for(subApNdx = 0; subApNdx < hrt_numSubaps; subApNdx++)
                    {
//                      find the peak of all four pixels for each active sub-ap
                        for (max = 0.0, flatSetCntr = 0; flatControlSets[flatSetCntr].activeSet > 0; flatSetCntr++)
                        {
                            if (flatPeakAtSet[flatSetCntr][subApNdx] > max)
                                max = flatPeakAtSet[flatSetCntr][subApNdx];
                        }

//                      find the gain for each pixel, relative to the max for the sub-ap
                        for (flatSetCntr = 0; flatControlSets[flatSetCntr].activeSet > 0; flatSetCntr++)
                        {
                            thisCentRow = hrtFileDataShmPtr->quadCellDescs[subApNdx].subApRow;
                            pixRow = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixRows[flatControlSets[flatSetCntr].pixelRow];
                            pixCol = hrtFileDataShmPtr->quadCellDescs[subApNdx].pixCols[flatControlSets[flatSetCntr].pixelCol];
                            if (flatPeakAtSet[flatSetCntr][subApNdx] > 0.0)
                                hrt_wfsFlatData[thisCentRow][pixRow][pixCol] = max / flatPeakAtSet[flatSetCntr][subApNdx];  // only calced for active sub-aps!
                            else
                            {
                                hrt_wfsFlatData[thisCentRow][pixRow][pixCol] = 1.0;
                                foundZeroFlag = true;
                            }
                        }
                    }
                    if (foundZeroFlag)
                        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (gpAoHrtCent_FlatProcess): found zero pixel -- proceeding anyway");

//                  command the SRT to store the wfs flatfield data to disk
                    hrtFileDataShmPtr->doneLongDiskWrite = false;
                    gpAoHrtFileData_Put( eFileTypeWfsFlat_0 + hrt_camFrameRate );
                    flatProcessState = eCmdStateWaitWrite;
                }
            }
            break;

//      ensure we are done before notice to host -- this also allows time for SRT to get frame rate etc, before change
        case eCmdStateWaitWrite:
            if (!hrtFileDataShmPtr->doneLongDiskWrite)
                break;

//          we're done, so set flags to indicate that command is not running and notify the host
            runCentCmd = eCmdNone;
// mrr DM            AOC_HRT_GMB_WRITE_CMD_STATUS( hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'takePixelFlat' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        default:
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (gpAoHrtCent_FlatProcess) unexpected flatProcessState");
            break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn static void gpAoHrtCent_FlatAbort( void )
 *
 *  @brief
 *  Abort the takePixelFlats command
 *
 *  @b DESCRIPTION:
 *  Abort the takePixelFlats command by simply setting the command flags to
 *  indicate that no command is executing (since the new flat field data is stored
 *  in a temporary buffer until the command completes, there is    no additional
 *  cleanup to be done here).
 *  @par
 *
 *  @b Usage: gpAoHrtCent_FlatAbort( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */
static void gpAoHrtCent_FlatAbort( void )
{
// mrr DM    AOC_HRT_GMB_WRITE_CMD_STATUS( runCentCmd, 0, GPI_STATE_IDLE, "" );
    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'takePixelFlats' aborted");
    runCentCmd = eCmdNone;
    hrtCentDataPtr->cmdCrnt = eCmdNone;
    flatProcessState = eCmdState_None;
}


/*!
 *******************************************************************************
 *
 *  @fn static void gpAoHrtCent_ApoCalcStart( short apoNum )
 *
 *  @brief
 *  Start the measureRefCents command.
 *
 *  @b DESCRIPTION:
 *  Start the processing of the measApoOffsets command by initializing the
 *  cent sums (to be used to compute averages) and setting counters and flags
 *  necessary to run the command.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_ApoCalcStart( );
 *
 *  @param[in]    apoNum    (short)    - which Apo offset is being calculated
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_ApoCalcStart( short apoNum )
{
    int   i;
    int   tmpNdx;
    float *pData;

    //    set the current reference centroids to their base  (so they won't influence determination of the new reference centroids)
//    for (i = 0; i < hrt_numCentroids; i++)
//        hrt_currRefCentsQ[i] = xxxxxxxx0.0;

    tmpNdx = hrtCentDataPtr->crntRefCentStoreBufNdx;
    if (++tmpNdx >= NUM_REFCENT_STORE_BUFS)
        tmpNdx = 0;
    pData = hrtCentDataPtr->refCentStoreBuffers[tmpNdx];
    for (i = 0; i < hrt_numCentroids; i++)  // update current Ref Cents storage buffer (used for diag dump)
        pData[i] = hrt_currRefCentsQ[i];
    hrtCentDataPtr->crntRefCentStoreBufNdx = tmpNdx;

    //    initialize refcent sums to zeroes
//    for (i = 0; i < hrt_numCentroids; i++)
//        centSums[i] = 0.0;

    //    initialize sum counter and refcent state and set flag to indicate that the refcent command is running
    centCntr = 0;
    refcentState = eCmdStateSumCent;
    runCentCmd = eCmdSaveRefCents;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_RefCalcStart( char clearRef, int currCmd )
 *
 *  @brief
 *  Start the measureRefCents command.
 *
 *  @b DESCRIPTION:
 *  Start the processing of the measRefCents command by initializing the
 *  cent sums (to be used to compute averages) and setting counters and flags
 *  necessary to run the command.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_RefCalcStart( (char) clearRef );
 *
 *  @param[in]    clearRef    (char )        - tell the initialize routine to clear the ref so that true ref can be established
 *                                          - >  reference will be set to current 'base' reference to calc the offsets
 *  @param[in]    currCmd        ( int )            - What is current command - used to determine offset or base measurement of references
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_RefCalcStart( char clearRef, int currCmd )
{
    int   i;
    int   tmpNdx;
    float *pData;

//  if necessary to clear, then do so; however do not touch if clearing not requested
    if( clearRef )
    {
//      zero out the current reference centroids (so they won't influence determination of the new reference centroids)
        tmpNdx = hrtCentDataPtr->crntRefCentStoreBufNdx;
        if (++tmpNdx >= NUM_REFCENT_STORE_BUFS)
            tmpNdx = 0;
        pData = hrtCentDataPtr->refCentStoreBuffers[tmpNdx];
        for( i = 0; i < hrt_numCentroids; i++)
        {
            hrt_currRefCentsQ[i] = 0.0;
            pData[i] = 0.0;  // update current Ref Cents storage buffer (used for diag dump)
        }
        hrtCentDataPtr->crntRefCentStoreBufNdx = tmpNdx;
    }

    //    initialize refcent sums to zeroes
    for ( i = 0; i < hrt_numCentroids; i++)
        centSums[i] = 0.0;

    //    initialize sum counter and refcent state and set flag to indicate that the refcent command is running
    centCntr = 0;
    refcentState = eCmdStateSumCent;
    runCentCmd = currCmd;
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_RefCalcProcess( float centData[], short runCmd )
 *
 *  @brief
 *  Process data to determine reference centroids.
 *
 *  @b DESCRIPTION:
 *  Process the measureRefCents command by receiving RefcentAvgN frames worth of
 *  centroid data, averaging that data, and writing the result to a file when done.
 *  @par
 *
 *   @b Usage: gpAoHrtCent_RefCalcProcess( (float) centData, (short)runCmd );
 *
 *  @param[in]    centData    (float[])    - centroid data on a frame-by-frame basis
 *  @param[in]    runCmd        (short)        - which command got us here
 *
 *  @return
 *  None
 *
 *  @b UPDATES Global Variables
 *  RefcentsQ[]
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_RefCalcProcess( float centData[], short runCmd )
{
    int ii;
    double tip=0, tilt=0;

//  initialize, sum, or average and store refcent data, depending on refcentState
    switch (refcentState)
    {
        case eCmdStateSumCent:
            // add current centroid data to refcent sums
            for (ii = 0; ii < hrt_numSubaps; ii++)
            {
                centSums[ii]                 += centData[hrtFileDataShmPtr->subApMap[ii]];
                centSums[ii + hrt_numSubaps] += centData[hrtFileDataShmPtr->subApMap[ii] + MAX_NUM_SUBAPS];
            }

            // increment sum counter and change state to average data if we have enough data
            if (++centCntr >= hrt_centAvgN)
                refcentState = eCmdStateAvg;
            break;

        case eCmdStateAvg:
            //    command the SRT to store the cent data to disk
            switch( runCmd)
            {
                case eCmdSaveRefOffs:
                    // average the refcent sums
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        hrtFileDataShmPtr->shm_offCentsQ_offsets[ii]                 = centSums[ii]                 / centCntr;
                        hrtFileDataShmPtr->shm_offCentsQ_offsets[ii + hrt_numSubaps] = centSums[ii + hrt_numSubaps] / centCntr;
                    }

                    // calculate tip/tilt
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        tip  += hrtFileDataShmPtr->shm_offCentsQ_offsets[ii];
                        tilt += hrtFileDataShmPtr->shm_offCentsQ_offsets[ii + hrt_numSubaps];
                    }
                    tip  /= hrt_numSubaps;
                    tilt /= hrt_numSubaps;

                    // remove tip/tilt
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        hrtFileDataShmPtr->shm_offCentsQ_offsets[ii]                 -= tip;
                        hrtFileDataShmPtr->shm_offCentsQ_offsets[ii + hrt_numSubaps] -= tilt;
                    }

                    gpAoHrtFileData_Put(eFileTypeRefOffs);
                    break;


                case eCmdSaveRefCents:
                    // average the refcent sums
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        hrtFileDataShmPtr->shm_refCentsQ[ii]                 = centSums[ii]                 / centCntr;
                        hrtFileDataShmPtr->shm_refCentsQ[ii + hrt_numSubaps] = centSums[ii + hrt_numSubaps] / centCntr;
                    }

                    // calculate tip/tilt
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        tip  += hrtFileDataShmPtr->shm_refCentsQ[ii];
                        tilt += hrtFileDataShmPtr->shm_refCentsQ[ii + hrt_numSubaps];
                    }
                    tip  /= hrt_numSubaps;
                    tilt /= hrt_numSubaps;

                    // remove tip/tilt
                    for (ii = 0; ii < hrt_numSubaps; ii++)
                    {
                        hrtFileDataShmPtr->shm_refCentsQ[ii]                 -= tip;
                        hrtFileDataShmPtr->shm_refCentsQ[ii + hrt_numSubaps] -= tilt;
                    }

                    gpAoHrtFileData_Put(eFileTypeRefCentQ);
                    break;
            }

            // set flags to indicate that command is not running and Notification the host
            runCentCmd = eCmdNone;
// mrr DM            AOC_HRT_GMB_WRITE_CMD_STATUS(  hrtCentDataPtr->cmdCrnt, 0, GPI_STATE_IDLE, "" );
            SysConSendStrngNoParm(eDebugLevelMin, "INFO 'meas cent process(ref/offs)' complete");
            hrtCentDataPtr->cmdCrnt = eCmdNone;
            break;

        default:
            SysConSendStrngNoParm(eDebugLevelErr, "ERR unexpected refcentState");
            break;
    }
}


/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_RefCalcAbort( void )
 *
 *  @brief
 *  Abort the measureRefCents command
 *
 *  @b DESCRIPTION:
 *  Abort the measureRefCents command by simply setting the command flags to
 *  indicate that no command is executing.  The new refcent data is stored in a
 *  temporary buffer until the command completes.
 *  @par
 *  IMPORTANT: The current reference centroids are zeroed out when this command
 *  begins.  Therefore, if the refcent command is aborted, the reference centroids
 *  need to be restored.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_RefCalcAbort();
 *
 *  @b Param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/

static void
gpAoHrtCent_RefCalcAbort( void )
{
// mrr DM    AOC_HRT_GMB_WRITE_CMD_STATUS( runCentCmd, 0, GPI_STATE_IDLE, "" );
    SysConSendStrngNoParm(eDebugLevelMin, "INFO 'measureRefCents' aborted (issue a readConfig command)");
    runCentCmd = eCmdNone;
    hrtCentDataPtr->cmdCrnt = eCmdNone;
    refcentState = eCmdState_None;
}

/*!
 *******************************************************************************
 *
 *  @fn void gpAoHrtCent_GetCrntDarkBackFlat( int crntCamFrameRateNdx )
 *
 *  @brief
 *  Get the dark, background, and flat field data for the current wfs camera frame rate.
 *
 *  @b DESCRIPTION:
 *  Get the dark, background, and flat field data for the current wfs camera frame rate.
 *  This is done by simply moving the dark, background, and flat field data, that
 *  has previously been loaded from files, corresponding to the current frame rate into
 *  the arrays used by the centroider.
 *
 *  @b Usage: gpAoHrtCent_GetCrntDarkBackFlat(crntCamFrameRateNdx);
 *
 *  @param[in] crntCamFrameRateNdx (int) - current wfs camera frame rate index
 *
 *  @return
 *       None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/

void
gpAoHrtCent_GetCrntDarkBackFlat( int crntCamFrameRateNdx )
{
    int i, j, k;

    for (i = 0; i < NUM_SUBAPS_ROWS_PER_FRAME; i++)
    {
        for (j = 0; j < NUM_CAM_ROWS_PER_SUBAP; j++)
        {
            for (k = 0; k < NUM_PIX_IN_ROW; k++)
            {
                hrt_wfsDarkData[i][j][k]   = hrtFileDataShmPtr->wfsDarkData[crntCamFrameRateNdx][i][j][k];
                hrt_wfsBkGrndData[i][j][k] = hrtFileDataShmPtr->wfsBkGrndData[crntCamFrameRateNdx][i][j][k];
                hrt_wfsFlatData[i][j][k]   = hrtFileDataShmPtr->wfsFlatData[crntCamFrameRateNdx][i][j][k];
            }
        }
    }
}

/*!
 *******************************************************************************
 *
 *  @fn static void gpAoHrtCent_AdjustCrntDarkBackFlat( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
 *
 *  @brief
 *  Adjust the dark or background data in use and pack the current flat field data.
 *
 *  @b DESCRIPTION:
 *  This function adjusts the dark or background currently in use by performing
 *  dark-corner correction.  Dark-corner correction works by temporally smoothing 4 dark
 *  pixels, one near each corner, with boxcar averagers (with N = frame rate), subtracting
 *  the dark/background values (as stored in the dark/background files) corresponding to
 *  the smoothed pixel values from the smoothed pixel values, and averaging those
 *  differences to give a correction value.  That correction value is then be added to all
 *  the dark/background values (as stored in the dark/background files) and the resulting
 *  corrected dark/background values are then subtracted from the current frame as usual
 *  (in gpAoHrtCent_ProcessQuad()).  The dark/background values are corrected as just
 *  described, instead of simply subtracting the correction from the frame directly, so that
 *  the dark/background values can be corrected after the centroids have been passed on,
 *  so as not to impact the closed loop timing.  This means that corrections is actually
 *  one frame delayed.
 *  @par
 *  In addition, while we're at it, the flat field values currently in use are packed
 *  for slightly better cache performance in the centroider.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_AdjustCrntDarkBackFlat( pixData );
 *
 *  @param[in]    pixData        (short[][][])    - pixel data (i.e., raw wfs camera data)
 *
 *  @return
 *       None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static const int darkCornerCorrectPixelLocs[DARK_CORNER_NUM_PIXELS][2] = {{6, 6}, {6, 87}, {87, 6}, {87, 87}};
static         int   darkCornerBoxcarAveData[MAX_FRAME_RATE][DARK_CORNER_NUM_PIXELS];
static         float crntDarkBackPacked[MAX_NUM_SUBAPS][DARK_CORNER_NUM_PIXELS];
static         float crntFlatPacked[MAX_NUM_SUBAPS][DARK_CORNER_NUM_PIXELS];

static void gpAoHrtCent_AdjustCrntDarkBackFlat( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW] )
{
    int i, centNdxPacked, subApRowNdx;
    int pixRow0, pixRow1, pixCol0, pixCol1;
    int *pI16Data;

    darkCornerCorrection = 0.0;
    if (pixData != NULL)
    {
        int tmpInt, tmpFrameRate;
        unsigned short *pU16PixData = &pixData[0][0][0];

//      update dark corner averages (these are kept current whether they're being used now or not)
        if (darkCornerPrevFrameRateNdx != hrt_camFrameRate)
        {
            darkCornerPrevFrameRateNdx = hrt_camFrameRate;
            darkCornerBoxcarIndex = 0;
            darkCornerBoxcarN = 0;
            for ( i = 0; i < DARK_CORNER_NUM_PIXELS; i++ )
                darkCornerBoxcarSums[i] = 0;
        }
        tmpFrameRate = LegalFrameRates[darkCornerPrevFrameRateNdx];
        for ( i = 0; i < DARK_CORNER_NUM_PIXELS; i++ )
        {
            if ( darkCornerBoxcarN >= tmpFrameRate )
                darkCornerBoxcarSums[i] -= darkCornerBoxcarAveData[darkCornerBoxcarIndex][i];
            tmpInt = pU16PixData[darkCornerCorrectPixelLocs[i][0] * NUM_PIX_IN_ROW + darkCornerCorrectPixelLocs[i][1]];
            darkCornerBoxcarAveData[darkCornerBoxcarIndex][i] = tmpInt;
            darkCornerBoxcarSums[i] += tmpInt;
        }
        darkCornerBoxcarIndex = (darkCornerBoxcarIndex + 1) % tmpFrameRate;
        if ( darkCornerBoxcarN < tmpFrameRate )
            darkCornerBoxcarN++;
        for ( i = 0; i < DARK_CORNER_NUM_PIXELS; i++ )
            darkCornerBoxcarAves[i] = (float)((double)darkCornerBoxcarSums[i] / (double)darkCornerBoxcarN);

//      determine dark correction, if necessary
        if (hrt_darkCornerCorrectSwitch != 0)
        {
            if( hrt_applyBackData )
                pI16Data = &hrt_wfsBkGrndData[0][0][0];
            else if ( hrt_applyDarkData )
                pI16Data = &hrt_wfsDarkData[0][0][0];
            if( hrt_applyBackData || hrt_applyDarkData )
            {
                for ( i = 0; i < DARK_CORNER_NUM_PIXELS; i++ )
                {
                    darkCornerCorrection += darkCornerBoxcarAves[i] -
                        (float)pI16Data[darkCornerCorrectPixelLocs[i][0] * NUM_PIX_IN_ROW + darkCornerCorrectPixelLocs[i][1]];
                }
                darkCornerCorrection /= (float)DARK_CORNER_NUM_PIXELS;
            }
        }
    }
    hrtFileDataShmPtr->tlcErrDarkCornerVal = darkCornerCorrection;

//    for each sub-ap row
//        for each active sub-ap in the row
    centNdxPacked = 0;
    for( subApRowNdx = 0; subApRowNdx < NUM_SUBAPS_ROWS_PER_FRAME; subApRowNdx++ )
    {
        while (hrtFileDataShmPtr->quadCellDescs[centNdxPacked].subApRow == subApRowNdx)
        {
            pixRow0 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixRows[0];
            pixRow1 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixRows[1];
            pixCol0 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixCols[0];
            pixCol1 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixCols[1];

//          adjust and pack darks, backgrounds, or neither, depending on how flags are set
            if( hrt_applyBackData )
            {
                crntDarkBackPacked[centNdxPacked][0] = (float)hrt_wfsBkGrndData[subApRowNdx][pixRow0][pixCol0] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][1] = (float)hrt_wfsBkGrndData[subApRowNdx][pixRow0][pixCol1] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][2] = (float)hrt_wfsBkGrndData[subApRowNdx][pixRow1][pixCol0] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][3] = (float)hrt_wfsBkGrndData[subApRowNdx][pixRow1][pixCol1] + darkCornerCorrection;
            }
            else if( hrt_applyDarkData )
            {
                crntDarkBackPacked[centNdxPacked][0] = (float)hrt_wfsDarkData[subApRowNdx][pixRow0][pixCol0] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][1] = (float)hrt_wfsDarkData[subApRowNdx][pixRow0][pixCol1] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][2] = (float)hrt_wfsDarkData[subApRowNdx][pixRow1][pixCol0] + darkCornerCorrection;
                crntDarkBackPacked[centNdxPacked][3] = (float)hrt_wfsDarkData[subApRowNdx][pixRow1][pixCol1] + darkCornerCorrection;
            }
            else
            {
                crntDarkBackPacked[centNdxPacked][0] = 0.0;
                crntDarkBackPacked[centNdxPacked][1] = 0.0;
                crntDarkBackPacked[centNdxPacked][2] = 0.0;
                crntDarkBackPacked[centNdxPacked][3] = 0.0;
            }

//          pack the flat fields (it's a bit of a waste doing this every time, since the flat fields only change if
//          > they're re-calculated, the frame rate changes, or the flat field on/off switch is changed; but this is
//          > done after the hard-real-time centroid calculations are done and passed on; so, it's okay)
            if( hrt_applyFlatData )
            {
                crntFlatPacked[centNdxPacked][0] = (float)hrt_wfsFlatData[subApRowNdx][pixRow0][pixCol0];
                crntFlatPacked[centNdxPacked][1] = (float)hrt_wfsFlatData[subApRowNdx][pixRow0][pixCol1];
                crntFlatPacked[centNdxPacked][2] = (float)hrt_wfsFlatData[subApRowNdx][pixRow1][pixCol0];
                crntFlatPacked[centNdxPacked][3] = (float)hrt_wfsFlatData[subApRowNdx][pixRow1][pixCol1];
            }
            else
            {
                crntFlatPacked[centNdxPacked][0] = 1.0;
                crntFlatPacked[centNdxPacked][1] = 1.0;
                crntFlatPacked[centNdxPacked][2] = 1.0;
                crntFlatPacked[centNdxPacked][3] = 1.0;
            }

            centNdxPacked++;
        }
    }
}

/*!
 *******************************************************************************
 *  @fn static short gpAoHrtCent_ProcessCM( unsigned short pixData[ ][NUM_PIX_IN_ROW],
 *                                            short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[] )
 *  @brief
 *  Compute centroids, using the CM algorithm.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to compute centroids, using the quad-cell algorithm.
 *  @par
 *
 *  @b Usage: int retVal = gpAoHrtCent_ProcessQuad(pixData, refCentsCalOffsetsNdx, xCents, yCents, totalIntens);
 *
 *  @param[in]        pixData                    (short[][][])    - pixel data (i.e., raw wfs camera data)
                                                              This essentially recases the 40x40 to 10x4x40
 *  @param[in]        refCentsCalOffsetsNdx    (short)            - current index for Cal reference centroid offsets
 *  @param[out]        xCents                    (float[])        - calculated X centroids
 *  @param[out]        yCents                    (float[])        - calculated Y centroids
 *  @param[out]        totalIntens                (float[])        - calculated total sub-ap intensities
 *  @param[out]        averageIntens            (*float)        - calculated average sub-ap intensity
 *
 *  @return
 *   skipFrameFlag -- flag indicating that frame should be skipped XXXDWP110914
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static short
gpAoHrtCent_ProcessCM ( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
                        short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[],
                        float *averageIntens)
{
boolean skipFrameFlag;  // flag indicating that frame should be skipped XXXDWP110914
int     i, centNdxPacked, centNdx, subApRowNdx;
int     numOverThreshSubaps;  //XXXdwp110816
float   scaleX, scaleY;
float   *pRefCentCalOffs;
double  totalTotalIntens;
static  int Cntr = 0, CheckCntr = 0;
int     j, pixRow, pixCol;
float   tmpPix [ 4 ] [ 4 ], rows [ 4 ], cols [ 4 ];

    //    make sure centroid and intensity arrays are all zeroes to begin with
    for ( i = 0; i < MAX_NUM_SUBAPS; i++ )
        xCents[i] = yCents[i] = totalIntens[i] = 0.0;

if ( ++CheckCntr >= TEST_INTERVAL * LegalFrameRates[hrt_camFrameRate] )
{
    CheckCntr = 0;
    Cntr = 1;
}
    //    for each sub-ap row
    //        for each active sub-ap in the row
    //            compute a, b, c, d, and the total intensity for this sub-ap
    //            compute the X and Y centroids for this sub-ap
    scaleX = hrt_centroidGainX * MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
    scaleY = hrt_centroidGainY * MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
    pRefCentCalOffs = hrtCentDataPtr->refCentCalOffStoreBuffers[refCentsCalOffsetsNdx];
    centNdxPacked = 0;
    totalTotalIntens = 0.0;
    numOverThreshSubaps = hrt_numSubaps;  //XXXdwp110816
    // mrr currently we have an empty row on top and bottom of the used data
    for( subApRowNdx = 0; subApRowNdx < NUM_SUBAPS_ROWS_PER_FRAME + 2; subApRowNdx++ )
    {    // for every subap row
        while ( centMassDescs_0 [ centNdxPacked ].subApRow == subApRowNdx )
        {    // for each subap that is in the same subap row
            centNdx = hrtFileDataShmPtr->subApMap[centNdxPacked];
            // Get all the data in the subap, adjust for flat, and calculate its total intensity
            totalIntens [ centNdx ] = 0.0;    // hasn't this already been done above?    // mrr
            for ( i = 0; i < 4; i++ )    // for each row in the sub ap
            {
                cols [ i ] = 0.0;
                rows [ i ] = 0.0;

                pixRow = centMassDescs_0 [ centNdxPacked ].pixRows [ i ];    // what row are we on //mrr isn't this just i?
                for ( j = 0; j < 4; j++ )    // for each col in the subap
                {    // mrr for now dow't worry about flats and thresh
                    pixCol = centMassDescs_0 [ centNdxPacked ].pixCols [ j ];    // what col are we on?
                    tmpPix [ i ] [ j ] = (float)( pixData[ subApRowNdx ] [ pixRow ] [ pixCol ]);
                                                //    CflatData [ subApRow ] [ pixRow ] [ pixCol ] ) -
                                                //    CentThresh;
                    totalIntens [ centNdx ] += tmpPix [ i ] [ j ];
                }
            }
            totalTotalIntens += totalIntens[ centNdx ];
            //    Get the row and col sums
            for ( i = 0; i < 4; i++ )    // #defines    // mrr
            {
                for ( j = 0; j < 4; j++ )
                {
                    cols [ i ] += tmpPix [ j ] [ i ];
                    rows [ i ] += tmpPix [ i ] [ j ];
                }
            }

            // calculate the weighted x and y centroid sums
            xCents [ centNdx ] = 0.0;
            yCents [ centNdx ] = 0.0;
            for ( i = 0; i < 4; i++ )
            {
                if ( cols [ i ] > 0.0 )
                    xCents [ centNdx ] += cols [ i ] * centCMWeightsX [ i ];

                if ( rows [ i ] > 0.0 )
                    yCents [ centNdx ] += rows [ i ] * centCMWeightsY [ i ];
            }

            // Calculate centroids, scale in microns and calc total aperture centroids for T/T
            if ( totalIntens [ centNdx ] <= 0.0 )
            {
                xCents [ centNdx ] = 0.0;
                yCents [ centNdx ] = 0.0;
            }
            else
            {    // RefCents are stored x and y
                {    //    Original
                    {
                        xCents [ centNdx ] = scaleX * ( xCents [ centNdx ] / ( totalIntens [ centNdx ]) )
                            - hrt_currRefCentsQ [ centNdx ];    // - Centoffs [ centNdx ];
                        yCents [ centNdx ] = scaleY * ( yCents [ centNdx ] / ( totalIntens [ centNdx ]) )
                            - hrt_currRefCentsQ [ hrt_numSubaps + centNdx ];
                        // CentThresh is added to totalIntens to avoid unrealistically large centroids
                        //    if totalIntens is not zero but is still a small number
                        //xCents [ centNdxPacked ] = xCents [ centNdxPacked ] / ( totalIntens [ centNdxPacked ]
                        //    + CentThresh ) / centCMWeightsX [ 3 ] -
                        //    RefcentsC [ centNdxPacked ] - Centoffs [ centNdxPacked ];
                        //yCents [ centNdxPacked ] = yCents [ centNdxPacked ] / ( totalIntens [ centNdxPacked ]
                        //     + CentThresh ) / centCMWeightsY [ 3 ] -
                        //    // Do this calculation less often
                        //    RefcentsC [ NUM_CENTROIDS_PER_WFS + centNdxPacked ] -
                        //    Centoffs [ NUM_CENTROIDS_PER_WFS + centNdxPacked ];    // 120+

                        xCentsSums += xCents [ centNdx ];
                        yCentsSums += yCents [ centNdx ];
                    }
                }
            }

            ( centNdxPacked )++;
        }    // end while still in the same subap row
    }// end for every subap row

    skipFrameFlag = false;  //XXXDWP110914
    // mrr
    //if (numOverThreshSubaps < (int)((float)hrt_numSubaps * hrt_overThreshFraction))  //XXXdwp110816
    //{                                                                                 //XXXdwp110816
    //    skipFrameFlag = true;  //XXXDWP110914
    //    for ( i = 0; i < MAX_NUM_SUBAPS; i++ )                                         //XXXdwp110816
    //        xCents[i] = yCents[i] = 0.0;                                             //XXXdwp110816
    //}                                                                                 //XXXdwp110816

    *averageIntens = (float)(totalTotalIntens / (double)centNdxPacked);
    if (skipFrameFlag)
        hrtFileDataShmPtr->tlcErrAoLight = 0;
    else
        hrtFileDataShmPtr->tlcErrAoLight = *averageIntens;

    Cntr = 0;
    return skipFrameFlag;  //XXXDWP110914
}

/*!
 *******************************************************************************
 *  @fn static short gpAoHrtCent_ProcessQuad( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
 *                                            short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[] )
 *  @brief
 *  Compute centroids, using the quad-cell algorithm.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to compute centroids, using the quad-cell algorithm.
 *  @par
 *
 *  @b Usage: int retVal = gpAoHrtCent_ProcessQuad(pixData, refCentsCalOffsetsNdx, xCents, yCents, totalIntens);
 *
 *  @param[in]        pixData                    (short[][][])    - pixel data (i.e., raw wfs camera data)
                                                          - This is essentially recast from 40x40 to 10x4x40 (for 8x8)
 *  @param[in]        refCentsCalOffsetsNdx    (short)            - current index for Cal reference centroid offsets
 *  @param[out]        xCents                    (float[])        - calculated X centroids    //centBuffPtr->xyCents[0]
 *  @param[out]        yCents                    (float[])        - calculated Y centroids    //centBuffPtr->xyCents[MAX_CENTROIDS_FRAME]
 *  @param[out]        totalIntens                (float[])        - calculated total sub-ap intensities
 *  @param[out]        averageIntens            (*float)        - calculated average sub-ap intensity
 *
 *  @return
 *   skipFrameFlag -- flag indicating that frame should be skipped XXXDWP110914
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ******************************************************************************* */
static short
gpAoHrtCent_ProcessQuad( unsigned short pixData[][NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW],
                         short refCentsCalOffsetsNdx, float xCents[], float yCents[], float totalIntens[],
                         float *averageIntens)
{
boolean skipFrameFlag;  // flag indicating that frame should be skipped
int     i, centNdxPacked, centNdx, subApRowNdx;
int     pixRow0, pixRow1, pixCol0, pixCol1;
int     numOverThreshSubaps;
float   a, b, c, d, scaleX, scaleY, tmpF;
float   *pRefCentCalOffs;
double  totalTotalIntens;

    //    make sure centroid and intensity arrays are all zeroes to begin with
    for ( i = 0; i < MAX_NUM_SUBAPS; i++ )
        xCents[i] = yCents[i] = totalIntens[i] = 0.0;

    //    for each sub-ap row
    //        for each active sub-ap in the row
    //            compute a, b, c, d, and the total intensity for this sub-ap
    //            compute the X and Y centroids for this sub-ap
    scaleX = hrt_centroidGainX * MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
    scaleY = hrt_centroidGainY * MICRON_SLOPE_PER_SUBAP__PER__CENT_UNIT;
    pRefCentCalOffs = hrtCentDataPtr->refCentCalOffStoreBuffers[refCentsCalOffsetsNdx];
    centNdxPacked = 0;
    totalTotalIntens = 0.0;
    numOverThreshSubaps = hrt_numSubaps;  //XXXdwp110816
    for( subApRowNdx = 0; subApRowNdx < NUM_SUBAPS_ROWS_PER_FRAME; subApRowNdx++ )
    {
        while (hrtFileDataShmPtr->quadCellDescs[centNdxPacked].subApRow == subApRowNdx)
        {
            pixRow0 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixRows[0];
            pixRow1 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixRows[1];
            pixCol0 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixCols[0];
            pixCol1 = hrtFileDataShmPtr->quadCellDescs[centNdxPacked].pixCols[1];

            a = (float)(pixData[subApRowNdx][pixRow0][pixCol0]);
            b = (float)(pixData[subApRowNdx][pixRow0][pixCol1]);
            c = (float)(pixData[subApRowNdx][pixRow1][pixCol0]);
            d = (float)(pixData[subApRowNdx][pixRow1][pixCol1]);

            a -= crntDarkBackPacked[centNdxPacked][0];
            b -= crntDarkBackPacked[centNdxPacked][1];
            c -= crntDarkBackPacked[centNdxPacked][2];
            d -= crntDarkBackPacked[centNdxPacked][3];

            a *= crntFlatPacked[centNdxPacked][0];
            b *= crntFlatPacked[centNdxPacked][1];
            c *= crntFlatPacked[centNdxPacked][2];
            d *= crntFlatPacked[centNdxPacked][3];

            if (a < 0.0) a = 0.0;
            if (b < 0.0) b = 0.0;
            if (c < 0.0) c = 0.0;
            if (d < 0.0) d = 0.0;

            centNdx = hrtFileDataShmPtr->subApMap[centNdxPacked];

            totalIntens[centNdx] = a + b + c + d;
            totalTotalIntens += totalIntens[centNdx];
//XXX            if (totalIntens[centNdx] <= 0.0)
            if (totalIntens[centNdx] <= (float)hrt_CentThresh)
            {
// mrr DM                numOverThreshSubaps--;  //XXXdwp110816
                xCents[centNdx] = 0.0;
                yCents[centNdx] = 0.0;
            }
            else
            {
                tmpF = scaleX / totalIntens[centNdx];
                xCents[centNdx] = ((b + d) - (a + c)) * tmpF - hrt_currRefCentsQ[centNdxPacked];
                tmpF = scaleY / totalIntens[centNdx];
                yCents[centNdx] = ((c + d) - (a + b)) * tmpF - hrt_currRefCentsQ[hrt_numSubaps + centNdxPacked];

                if (hrt_applyCalData != 0)
                {
                    xCents[centNdx] -= pRefCentCalOffs[centNdxPacked];
                    yCents[centNdx] -= pRefCentCalOffs[hrt_numSubaps + centNdxPacked];
                }
            }
            centNdxPacked++;
        }
    }

    skipFrameFlag = false;  //XXXDWP110914q
    //if (numOverThreshSubaps < (int)((float)hrt_numSubaps * hrt_overThreshFraction))  //XXXdwp110816    // mrr DM
    //{                                                                                 //XXXdwp110816
    //    skipFrameFlag = true;  //XXXDWP110914
    //    for ( i = 0; i < MAX_NUM_SUBAPS; i++ )                                         //XXXdwp110816
    //        xCents[i] = yCents[i] = 0.0;                                             //XXXdwp110816
    //}                                                                                 //XXXdwp110816

    *averageIntens = (float)(totalTotalIntens / (double)centNdxPacked);
    //if (skipFrameFlag)    // mrr DM
    //    hrtFileDataShmPtr->tlcErrAoLight = 0;
    //else
        hrtFileDataShmPtr->tlcErrAoLight = *averageIntens;
    return skipFrameFlag;  //XXXDWP110914
}

/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_PupilTrack_Init( void )
 *
 *  @brief
 *  Initialize variables used by the centroid thread.
 *
 *  @b DESCRIPTION:
 *  This unit initializes variables for the pupil tracking calculations.  In
 *  particular, it determines the indexes into the packed intensity array
 *  for left, right, top, and bottom.
 *  @par
 *  IMPORTANT -- This REQUIRES that the pupil have MAX_PIX_FOR_PUP_TRACK/2
 *  leftmost sub-aps, rightmost sub-aps, topmost sub-aps, and bottommost
 *  sub-aps.
 *  @par
 *
 *  @b Usage: gpAoHrtCent_PupilTrack_Init( );
 *
 *  @b param None
 *
 *  @return
 *  None
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static void gpAoHrtCent_PupilTrack_Init( void )
{
int i, j;
int apNdx;
int leftmost = APERTURE_GRID_SIZEROW, rightmost  = 0;
int topmost  = APERTURE_GRID_SIZECOL, bottommost = 0;
int leftNdx = 0, rightNdx = 0, topNdx = 0, bottomNdx = 0;
int tmpApPattern[NUM_ROWS_IN_CAM / NUM_CAM_ROWS_PER_SUBAP][NUM_PIX_IN_ROW / NUM_CAM_ROWS_PER_SUBAP];

//  move the aperture pattern to a temporary location and find leftmost, rightmost, topmost, and bottommost
    for(i = 0; i < APERTURE_GRID_SIZECOL; i++)
    {
        for(j = 0; j < APERTURE_GRID_SIZEROW; j++)
        {
            if (hrtFileDataShmPtr->aperturePattern[i][j] == 0.0)
                tmpApPattern[i][j] = 0;  // make sure temp ap pattern is 0 or 1
            else
            {
                tmpApPattern[i][j] = 1;

                if (j < leftmost)
                    leftmost = j;
                if (j > rightmost)
                    rightmost = j;
                if (i < topmost)
                    topmost = i;
                if (i > bottommost)
                    bottommost = i;
            }
        }
    }

//  go through the aperature pattern again, setting leftmost, rightmost, topmost, and bottommost sub-aps
    for(i = 0; i < APERTURE_GRID_SIZECOL; i++)
    {
        for(j = 0; j < APERTURE_GRID_SIZEROW; j++)
        {
            if (tmpApPattern[i][j] != 0)
            {
                if (j == leftmost)
                {
                    tmpApPattern[i][j]     = 2;
                    tmpApPattern[i][j + 1] = 2;
                }
                if (j == rightmost)
                {
                    tmpApPattern[i][j]     = 3;
                    tmpApPattern[i][j - 1] = 3;
                }
                if (i == topmost)
                {
                    tmpApPattern[i][j]     = 4;
                    tmpApPattern[i + 1][j] = 4;
                }
                if (i == bottommost)
                {
                    tmpApPattern[i][j]     = 5;
                    tmpApPattern[i - 1][j] = 5;
                }
            }
        }
    }

//  go through the aperature pattern one more time to set up the arrays of leftmost,
//  > rightmost, topmost, and bottommost indexes
    apNdx = 0;
    for(i = 0; i < APERTURE_GRID_SIZECOL; i++)
    {
        for(j = 0; j < APERTURE_GRID_SIZEROW; j++)
        {
            if (tmpApPattern[i][j] != 0)
            {
                switch (tmpApPattern[i][j])
                {
                    case 2:
                        leftPix[leftNdx] = apNdx;
                        leftNdx++;
                        break;
                    case 3:
                        rightPix[rightNdx] = apNdx;
                        rightNdx++;
                        break;
                    case 4:
                        topPix[topNdx] = apNdx;
                        topNdx++;
                        break;
                    case 5:
                        bottomPix[bottomNdx] = apNdx;
                        bottomNdx++;
                        break;
                    default:
                        break;
                }
            }
            apNdx++;
        }
    }
}

/*!
 *******************************************************************************
 *
 *  @fn gpAoHrtCent_PupilTrack( float pupIntensity[], float pupTrackData[] );
 *
 *  @brief
 *  This unit performs pupil tracking calculations.
 *
 *  @b DESCRIPTION:
 *  This unit performs the pupil tracking calculations for the pupil tracking
 *  approach described in pupilTrackingNotes_v0.2.doc by D. Palmer.
 *  @par
 *  Notes:
 *  1. we are using the Aoc_PupilAlign display data fields in the GMB, with the
 *      normal display data triple-buffering, to provide once-per-second pupil
 *      alignment errors to the TLC
 *  2. the pupil alignment errors are relative offsets, with respect to fully
 *      aligned; not absolute positions (as in integrated actuator positions)
 *  3. the pupil alignment errors are in mas on the sky
 *  4. the pupil alignment errors are given for x and y, in that order, in the
 *      AO WFS coordinate system (x is parallel to the optical table, with
 *      negative values meaning that the pupil is too far to the left, when
 *      looking at the WFS from the front; y is perpendicular to the table, with
 *      negative values meaning that the pupil is to far down)
 *  5. as part of the calculation, the code will make sure there is adequate light
 *      and set a flag accordingly (Aoc_PupilAlignTooLowLight):
 *      a. eLightOkay --          there has been enough light for at least MOVING_AVG_M frames; filtered data is valid
 *      b. eLightStabilizing --   there has been enough light for less than MOVING_AVG_M frames; filtered data is noisy
 *      c. eLightTooLow --           there has not been enough light for at least MOVING_AVG_M frames; filtered data is zero
 *      d. eLightDestabilizing -- there has not been enough light for less than MOVING_AVG_M frames; filtered data is zero
 *
 *  @b Usage: gpAoHrtCent_PupilTrack( pupIntensityPtr, pupTrackDataPtr )
 *
 *  @param[in]    pupIntensity    (float[]):    pupil intensity data
 *  @param[out] pupTrackData     (float[]):    pupil tracking data
 *
 *  @return
 *  None.
 *
 *  @exception None
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void gpAoHrtCent_PupilTrack( float pupIntensity[], float pupTrackData[] )
{
    static int initCntr = 0;      // counts up to MOVING_AVG_M, from initialization or a low light condition, and then stops
    static int lowLightCntr = 0;  // counts the number of times in a row that we see a low light condition, up to MOVING_AVG_M
    static int movingAveNdx = 0;  // current index into the moving average data

    static float movingAveSumX = 0.0, movingAveSumY = 0.0;
    static float movingAveDataX[MOVING_AVG_M];
    static float movingAveDataY[MOVING_AVG_M];

    int    i;
    double left = 0.0, right = 0.0, top = 0.0, bottom = 0.0;

//  sum up the pixels along the pupil edges in order to calculate pupil centroids
    for (i = 0; i < MAX_PIX_FOR_PUP_TRACK; i++)
    {
        left   += pupIntensity[leftPix[i]];
        right  += pupIntensity[rightPix[i]];
        top    += pupIntensity[topPix[i]];
        bottom += pupIntensity[bottomPix[i]];
    }
    pupTrackData[ePupTrackDataLeft]   = left;
    pupTrackData[ePupTrackDataRight]  = right;
    pupTrackData[ePupTrackDataTop]    = top;
    pupTrackData[ePupTrackDataBottom] = bottom;

//  check whether there's enough light and act accordingly
    if (((left + right) < PUP_LOW_LIGHT_LIMIT) || ((top + bottom) < PUP_LOW_LIGHT_LIMIT))  // there's not enough light this time
    {
        if (lowLightCntr < MOVING_AVG_M)
        {
            pupTrackData[ePupTrackLightLevelFlag] = eLightDestabilizing;
            lowLightCntr++;
        }
        else
        {
            pupTrackData[ePupTrackLightLevelFlag] = eLightTooLow;
            initCntr = 0;
            movingAveNdx = 0;
            movingAveSumX = 0.0;
            movingAveSumY = 0.0;
        }

//      set return data to zeroes
        pupTrackData[ePupTrackDataUnfiltX] = 0.0;
        pupTrackData[ePupTrackDataUnfiltY] = 0.0;
        pupTrackData[ePupTrackDataFiltX] = 0.0;
        pupTrackData[ePupTrackDataFiltY] = 0.0;
    }
    else  // there is enough light this time
    {
        lowLightCntr = 0;

//      do the centroiding, converting to fractions of the pupil
        pupTrackData[ePupTrackDataUnfiltX] = (float)((left - right) / (left + right) * PUPILS_PER_PUP_TRACK_CENT_UNIT);
        pupTrackData[ePupTrackDataUnfiltY] = (float)((top - bottom) / (top + bottom) * PUPILS_PER_PUP_TRACK_CENT_UNIT);

        if (initCntr < MOVING_AVG_M)
        {
            pupTrackData[ePupTrackLightLevelFlag] = eLightStabilizing;

//          do the averaging with unfull buffers
            movingAveSumX += pupTrackData[ePupTrackDataUnfiltX];
            movingAveSumY += pupTrackData[ePupTrackDataUnfiltY];
            movingAveDataX[initCntr] = pupTrackData[ePupTrackDataUnfiltX];
            movingAveDataY[initCntr] = pupTrackData[ePupTrackDataUnfiltY];

            initCntr++;

            pupTrackData[ePupTrackDataFiltX] = movingAveSumX / initCntr;
            pupTrackData[ePupTrackDataFiltY] = movingAveSumY / initCntr;
        }
        else
        {
            float tmpX = movingAveDataX[movingAveNdx];
            float tmpY = movingAveDataY[movingAveNdx];

            pupTrackData[ePupTrackLightLevelFlag] = eLightOkay;

            movingAveDataX[movingAveNdx] = pupTrackData[ePupTrackDataUnfiltX];
            movingAveDataY[movingAveNdx] = pupTrackData[ePupTrackDataUnfiltY];

            if (++movingAveNdx < MOVING_AVG_M)
            {
//              use box car averaging
                movingAveSumX += pupTrackData[ePupTrackDataUnfiltX] - tmpX;
                movingAveSumY += pupTrackData[ePupTrackDataUnfiltY] - tmpY;
            }
            else
            {
                movingAveNdx = 0;

//              sum the whole arrays to guard against cumulative truncation error
                movingAveSumX = 0.0;
                movingAveSumY = 0.0;
                for (i = 0; i < MOVING_AVG_M; i++)
                {
                    movingAveSumX += movingAveDataX[i];
                    movingAveSumY += movingAveDataY[i];
                }
            }
            pupTrackData[ePupTrackDataFiltX] = movingAveSumX / MOVING_AVG_M;
            pupTrackData[ePupTrackDataFiltY] = movingAveSumY / MOVING_AVG_M;
        }
    }
    hrtFileDataShmPtr->tlcErrPupilLight = left + right + top + bottom;
}

//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
