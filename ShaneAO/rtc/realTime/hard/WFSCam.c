/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *  @file WFScam.c
 *
 *  @brief
 *  This component contains functions to interface to the SciMeasure WFS camera.
 *
 *  @b DESCRIPTION:
 *  This component is the Hard Real Time (HRT) interface to the AO WFS camera.
 *  It performs the RT camera initialization, communicates with the SRT CamMngr
 *  module to issue commands (because the camera drivers are not strictly RTLinux
 *  compatible), and handles real-time processing for the camera.
 *
 *  @note The Cam() thread is included in this component.
 *
 *  @b AUTHOR:
 *  DWP & SMJ       DATE: 11/25/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c WFScam()
 *  -# @c WFScam_Init()
 *  -# @c WFScam_IntrInit()
 *  -# @c WFScam_IntrHandler()
 *  -# @c WFScam_ProcCmd()
 *  -# @c WFScam_SendCmd()
 *  -# @c WFScam_LoadDataBuffer()
 *  -# @c WFScam_DeIntlv128x86()
 *  -# @c WFScam_DeIntlv128x128()
 *  -# @c WFScam_AutoParms()
 *  -# @c WFScam_DrvrWatch()
 *  -# @c WFScam_LoadSimWfsData()
 *  -# @c WFScam_LoadSimTTData()
 *  -# @c WFScam_LoadSimPupilMoveData()
 *  -# @c WFScam_LoadSimZernikes()
 *  -# @c WFScam_ComputeZernikes()
 *
 *  @see soft/WFScamMngr.C
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>

#if (RTL_SYSTEM == 0)
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif /* RTL_SYSTEM */


#include "WFSCam.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"

// Debugging

#ifdef VERBOSE_DEBUGGING
#define dbprintf(arglist)   printf arglist
#else   /* VERBOSE_DEBUGGING */
#define dbprintf(arglist)   /* as nothing */
#endif  /* VERBOSE_DEBUGGING */

#if (CAM_TYPE == WFS_REAL_GPI)
#if (RTL_SYSTEM != 0)
// real time linux does not follow EDT vendor instructions for include
// ... instead ...
// these next would have come from /opt/EDTpdv/edt_os_lnx.h
#define HANDLE int
typedef unsigned int uint_t;
typedef unsigned char uchar_t;
typedef unsigned short ushort_t;
// this would have come from some linux include, but beware the length
typedef unsigned long int uint64_t;
typedef pthread_t thread_t;
// this next would have come from <asm/posix_types*.h>
typedef long long               __kernel_loff_t;
// these next would have come from <sys/types.h>
typedef unsigned char           u_char;
typedef unsigned short          u_short;
typedef unsigned int            u_int;
typedef unsigned long           u_long;
typedef __kernel_loff_t         loff_t;
// this next is our hacked version of /opt/EDTpdv/module/edt_lnx_kernel.h
#include "edt_lnx_kernel_Local.h"
#else   /* RTL_SYSTEM */
// vanilla linux follows EDT vendor instructions and does this include
#include "edtinc.h"
#endif  /* RTL_SYSTEM */
#endif  /* CAM_TYPE */

// Defines
/* stringify a macro */
#define xstr(s) str(s)
/* stringify a token */
#define str(s) #s

// Global Variables
#if ( TIMER_TEST != 0 )
// a sequential counter incremented every time a valid frame is sent to the centroider
long            camFrameID = -1;
int             timerNdx = 0;
// the next two moved from gpAoHrtRecon.c to allow parallel instances for TTcam
long            reconFrameID = -1;
struct timespec timerTimes[NUM_TIME_SETS][eTimerMaxTimes];
#endif  /* TIMER_TEST */

short   hrt_useWfsSimData;      // flag indicating whether to use simulated wfs data ( 0: no, 1: yes )

short   hrt_camFrameRate;       // camera frame rate INDEX (NOT the actual frame rate!)
short   hrt_camGain;            // camera gain (0, 1, 2, or 3)

short   CamFrameRateNdx = 0;    // index for current camera rate into LegalFrameRates[]

int     WFS_irq = -1;

#if ( RTL_SYSTEM != 0 )
// WARNING: LegalFrameRates[] is defined in both WFScam.c (for the HRT) and gpAoSrtCamMngr.c (for the SRT)
/* WARNING:  The code below ASSUMES that the LegalFrameRates array is ordered. */
#if ( NUM_ROWS_IN_CAM_ACTUAL == 86 )
// IMPORTANT: note that 2000 fps isn't actually that fast in 128x86 mode, it's just a REP of 0 for testing
// IMPORTANT: DO NOT collect 10 seconds of diag data at 2000 fps -- there aren't enough buffers (collect up to 7 secs)!
// const short  LegalFrameRates[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!    // mrr
const short     LegalFrameRates[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator    // mrr
#else
// IMPORTANT: note that 1500 fps isn't actually that fast in 128x128 mode, it's just a REP of 0 for testing
//XXX put this back after testing!  const short    LegalFrameRates[] = { 250, 500, 1000, 1500,   -1, -1 };  // -1 is the terminator
//XXXdwp110615 const short      LegalFrameRates[] = { 250, 500, 1000, 1500, 2000, -1 };  // -1 is the terminator
// const short  LegalFrameRates[] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!
const short     LegalFrameRates[] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator    // mrr
#endif  /* NUM_ROWS_IN_CAM_ACTUAL */
#endif  /* RTL_SYSTEM */

// note that vendor API says this has type PdvDev *, but because of
// wanting to avoid vanilla linux includes for real-time code we
// have eschewed the include of "libpdv.h"
void* WFS_dev = NULL;



// Local Variables
#if ( TIMER_TEST != 0 )
static struct timespec intrTime;
static struct timespec prevIntrTime;
static struct timespec afterWFSWake;
static struct timespec afterEDTIntrProc;
static struct timespec afterWFSIRQEnable = {0,0};
#endif

#if ((RTL_SYSTEM != 0) && (CAM_TYPE == WFS_REAL_GPI))
// These are the kernel pages for the EDT ring buffers in RTL mode;
// camera frames are constructed from these pages.
static u_char   **edtKernelPages[WFS_BUFFERS];
static int      nEdtKernelPages[WFS_BUFFERS];

// the following set of arrays is oversized by RTL_PAGE_SIZE because they
// MUST be big enough for the frame rounded up to an integer number of
// RTL_PAGE_SIZEs
static unsigned short   wfsCamBuffsForRTL[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160 + RTL_PAGE_SIZE];

static unsigned long    oldAffinity;
#endif /* RTL_SYSTEM and CAM_TYPE said we are the real-time code */

// The SRT asks the edt software for WFS_BUFFERS ring buffers
// buf_array is an array with size WFS_BUFFERS
// each of the WFS_BUFFERS elements of buf_array is a pointer to unsigned short
static unsigned short *buf_array[WFS_BUFFERS];      // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other
static unsigned short *buf_array_160[WFS_BUFFERS];  // IMPORTANT: buffers pointed to by buf_array[] may not be contiguous with each other
static unsigned short *buf_array_sim[WFS_BUFFERS];

static unsigned int    edtDoneCount_sim = 0;
static unsigned short  wfsCamBuffsFor128[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];
static unsigned short  wfsCamBuffsForSim[WFS_BUFFERS][NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];

volatile static int     BadCntr             = 0;

volatile static int     WFScamIntrWatchCntr = 0;    // watchdog counter for camera interrupt

volatile static int     WFScamInvSem        = 0;    // watchdog counter
volatile static int     WFScamOvfSem        = 0;    // watchdog counter
volatile static int     WFScamUnkSem        = 0;    // watchdog counter

volatile static int     WFScamIntrWatchReset= 0;    // watchdog counter
volatile static int     WFScamtrywaitCntr   = 0;    // watchdog counter
volatile static int     WFScamnullCDPCntr   = 0;    // watchdog counter
volatile static int     WFScamcmdStartCntr  = 0;    // watchdog counter
volatile static int     WFScamintrIgnoreCntr= 0;    // watchdog counter
volatile static int     WFScamedtBufIniCntr = 0;    // watchdog counter
volatile static int     WFScamedtBufNewCntr = 0;    // watchdog counter
volatile static int     WFScamIntrHandler   = 0;    // watchdog counter
volatile static int     WFScamWaitBefore    = 0;    // watchdog counter
volatile static int     WFScamdoneCntr      = 0;    // watchdog counter
volatile static int     WFScamdc7           = 0;    // watchdog counter
volatile static int     WFScamldb           = 0;    // watchdog counter
static char*    WFScamLastPath      = "none"; // last path taken in while loop
static char*    WFScamLastNonWatchdog = "nada"; // last path taken, not reset by watchdog

static int      interruptWatch = 0;

static int      CheckCntr = 0;
static int      Cntr = 0;

// The following maps are out here so as not to make the stack too big

// map to deinterleave camera data for 128x86
static int deIntlvMap[NUM_ROWS_IN_CAM_FOR_SUBAPS * NUM_PIX_IN_ROW_FOR_SUBAPS];
// temporary map, before removing every fifth column
static int tmpMap[NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];
// temporary map, before removing every fifth column
static int tmpMap160[NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160];
// map to deinterleave camera data for 160x160
static int deIntlvMap160[NUM_ROWS_IN_CAM_ACT_160 * NUM_PIX_IN_ROW_ACT_160];
// temporary destination, before removing every fifth column
static unsigned short tmpDest[ NUM_ROWS_IN_CAM_ACTUAL ][NUM_PIX_IN_ROW_ACTUAL];

// Local Function Prototypes
static void         WFScam_Init( void );
static unsigned int WFScam_IntrHandler(unsigned int irq, struct rtl_frame *regs);
static void         WFScam_ProcCmd( short cmdNdx );
static void         WFScam_SendCmd( short cmd, short param );
static void         WFScam_LoadDataBuffer( int bufNdx );
static void         WFScam_DeIntlv128x86( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]);
static void         WFScam_DeIntlv128x128( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]);
static void         WFScam_LoadSimWfsData( unsigned short buf_array[], int curbuf );
static void         WFScam_LoadSimTTData( unsigned short buf_array[] );
static void         WFScam_LoadSimPupilMoveData( unsigned short buf_array[] );
static void         WFScam_LoadSimZernikes( unsigned short buf_array[] );
static void         WFScam_ComputeZernikes( void );


// Copied from:
//
// http://software.intel.com/en-us/articles/memcpy-performance

static void *sse_memcpy (void *__restrict__ b, const void *__restrict__ a, size_t n){
    char *s1 = b;
    const char *s2 = a;
    for(; 0<n; --n)*s1++ = *s2++;
    return b;
}

/*!
 *******************************************************************************
 *
 *  @fn WFScam( void* arg )
 *
 *  @brief
 *  This unit is the WFS camera thread mainline.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the WFS camera and then wait for commands
 *  from the host or data from the camera (via the camera interrupt service routine) and
 *  then take action as appropriate.
 *  @par
 *
 *  @b Usage: WFScam( arg );
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
WFScam( void *arg )
{
    boolean         waitBeforeIntrEnable;
    int             curbuf = 0;
    static int      doneCount = 0;
    static int      lastDoneIntr = 0;
    unsigned int    sips;

#   if ( TIMER_TEST != 0 )
    int i;
#   endif






    hrtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;
    hrtWFScamDataPtr->intrIgnore = false;


    // print to prove how the C preprocessor macros were defined
    printf("hard WFScam() #RTL_SYSTEM " str(RTL_SYSTEM) " value " xstr(RTL_SYSTEM) "\n");
#   if (RTL_SYSTEM != 0)
    printf("hard WFScam() RTL_SYSTEM != 0\n");
#   else    /* RTL_SYSTEM */
    printf("hard WFScam() RTL_SYSTEM == 0\n");
#   endif   /* RTL_SYSTEM */

    // print to prove how the C preprocessor macros were defined
    printf("hard WFScam() #CAM_TYPE " str(CAM_TYPE) " value " xstr(CAM_TYPE) "\n");
    printf("hard WFScam() #WFS_REAL_GPI " str(WFS_REAL_GPI) " value " xstr(WFS_REAL_GPI) "\n");
#   if (CAM_TYPE == WFS_REAL_GPI)
    printf("hard WFScam() CAM_TYPE == WFS_REAL_GPI\n");
#   else    /* CAM_TYPE */
    printf("hard WFScam() CAM_TYPE != WFS_REAL_GPI\n");
#   endif   /* CAM_TYPE */

#   if (RTL_SYSTEM != 0)
    printf("    hard WFScam() CPU:thread %d:%d\n",
    rtl_getcpuid(),rtl_pthread_self());   // sla debugging hang
#   endif /* RTL_SYSTEM */

    // initialize WFS variables
    // issue Initialize1 command to SRT
    // that requires reading a file, so it must be done by SRT
    // SRT will read WFS camera config file and load it into WFS camera
    // Nobody else can use the camera until until SRT has done this.
    // Also this waits for IntrInit before returning.
    WFScam_Init();
    // If IntrInit succeeded then WFS_dev is not NULL
    // and also the interrupt handler is installed.
    printf("    WFScam(): init 1 done\n");   // sla debugging hang

#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    /*
    With the kernel from Kyle Lanclos rtl_usleep cannot sleep longer
    than 1 s so this delay will not be 2 s.
    Of more concern than that, we do not know why this delay is needed
    at all.
    */
    //rtl_usleep(2000000);
    //rtl_usleep(2000000);    /* add another 2 s which means 1 s */
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
    sips    = 0;
    while(hrtWFScamDataPtr->Init1DrinkMe != 0xC0FFEE8D)
    {
        /*
        Wait until SRT has done gpAoSrtCamMngr_ProcCmd(eWFSInitialize1)
        */
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam() waited %d ds for Init1DrinkMe\n", sips);

    // send commands to initialize the EDT camera interface in user space
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInit2 -- start");
    WFScam_SendCmd (eWFSInitialize2, 0);
    // SRT will reconfigure EDT ring buffers
    // SRT will command camera stop 3 times
    // SRT will command camera program recall
    // SRT will query camera temperature
    // SRT will command camera repcounts
    // SRT will command camera start
    // ==> so interrupts will be happening
    // before SRT finishes eWFSInitialize2 it will have commanded
    // the WFS camera to start aquisition and the pdv API to catch
    // that means that interrupts will be happening
    printf("    WFScam(): init 2 sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInit2 -- end");

    sips    = 0;
    while(hrtWFScamDataPtr->Init2DrinkMe != 0xC0FFEE8D)
    {
        /*
        Wait until SRT has done gpAoSrtCamMngr_ProcCmd(eWFSInitialize2)
        */
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam() waited %d ds for Init2DrinkMe\n", sips);

#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    /*
    We find that rtl_usleep cannot sleep longer than 1 s so this delay
    will not be 2 s.
    Of more concern than that, we do not know why this delay is needed
    at all.
    */
    //rtl_usleep(2000000);
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */

    WFScam_SendCmd (eWFSSetRep, LegalFrameRates[ hrt_camFrameRate ]);
    printf("    WFScam(): SetRep sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInitRep -- end");

    sips    = 0;
    while(hrtWFScamDataPtr->SetRepDrinkMe != 0xC0FFEE8D)
    {
        /*
        Wait until SRT has done gpAoSrtCamMngr_ProcCmd(eWFSSetRep)
        */
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam() waited %d ds for SetRepDrinkMe\n", sips);

#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    /*
    We find that rtl_usleep cannot sleep longer than 1 s so this delay
    will not be 2 s.
    Of more concern than that, we do not know why this delay is needed
    at all.
    */
    //rtl_usleep(2000000);
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */

    WFScam_SendCmd (eWFSSetAtten, (3 - hrt_camGain));
    printf("    WFScam(): SetAtten sent\n");   // sla debugging hang
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInitAtten -- end");

#   if 0 /* original code did not wait here */
    sips    = 0;
    while(hrtWFScamDataPtr->SetAttenDrinkMe != 0xC0FFEE8D)
    {
        /*
        Wait until SRT has done gpAoSrtCamMngr_ProcCmd(eWFSSetAtten)
        */
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam() waited %d ds for SetAttenDrinkMe\n", sips);
#   endif /* 0 */

    WFScamIntrWatchCntr = 0;

#   if (RTL_SYSTEM == 0)
    // this is user space code
    cpu_set_t mask;
    int      cpuNum = HRT_CPU_WFS_CAM;
    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam) CPU requested: ", cpuNum);
    if( sched_setaffinity( 0, sizeof( mask ), &mask ) != 0 )
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFScam) executing sched_setaffinity() for CPU #",  cpuNum);
    sched_getaffinity( 0, sizeof( mask ), &mask );
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam): CPU_ISSET: ", CPU_ISSET( cpuNum, &mask));
#   else   /* RTL_SYSTEM */
    // this is real-time kernel code
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam): Cam thread id: ", rtl_pthread_self());
#   endif  /* RTL_SYSTEM */

#   if 0 /* cannot get here without WFS_dev */
    if (WFS_dev == NULL)
    {
        // suspect that this is a deadly condition
        printf("WFScam(): WFS_dev NULL\n");
    }
#   endif /* 0 */

    dbprintf(("    WFScam(): into infloop\n")); // sla debugging hang

    // enter infinite loop
    while (!QuitCamera_WFS_Thread)
    {
        // This semaphore is currently posted in two places:
        // In sysCon in response to WFS Host commands and in
        // IntrHandler in response to incoming frame data.
        if (rtl_sem_trywait(&WFSSem) < 0)
        {
            // we were not able to lock WFSSem
            // but that is okay and normal unless
            if (rtl_errno != EAGAIN)
            {
                // this printf may be dangerously verbose
                printf("WFScam(): trywait errno %d\n", rtl_errno);
            }
            WFScamtrywaitCntr++;    // bump up watchdog counter
            //WFScamLastPath = "trywait fail";
            rtl_usleep(1);
            // go back to top of while and again try to lock WFSSem
            continue;
        }
        // if we are here then we were able to lock WFSSem
        // this should mean that there is an image ready

#       if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterWFSWake);
#       endif   /* TIMER_TEST */

        waitBeforeIntrEnable = false;

        if(  hrtWFScamDataPtr == NULL )
        {
            /*
            This is global among the HRT files and it should
            have been set by gpAoHrtSysCon.c before creating
            this thread.
            How could we have got here without having dereferenced
            this NULL pointer several times?
            If we are here, is there any remedy?
            */
            WFScamnullCDPCntr++;    // bump up watchdog counter
            //WFScamLastPath = "hrtWFScamDataPtr == NULL";
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (WFScam): hrtWFScamDataPtr is NULL" );
        }
        else if (hrtWFScamDataPtr->cmdStart != eCmdNone)
        {
            WFScamcmdStartCntr++;   // bump up watchdog counter
            //WFScamLastPath = "cmdStart";

            // if a command is pending, call WFScam_ProcCmd() to process it
            WFScam_ProcCmd(hrtWFScamDataPtr->cmdStart);
            hrtWFScamDataPtr->cmdStart = eCmdNone;
        }
#       if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
        else if (hrtWFScamDataPtr->edtBufStat == eEdtBufStatNotInitted)
        {
            // Really, this is ignore interrupts as the camera has been told to stop sending images.
            WFScamedtBufIniCntr++;  // bump up watchdog counter
            //WFScamLastPath = "eEdtBufStatNotInitted";
            dbprintf(("hrtWFScamDataPtr->edtBufStat == eEdtBufStatNotInitted\n"));

            waitBeforeIntrEnable = true;
        }
        else if (hrtWFScamDataPtr->edtBufStat == eEdtBufStatReadNew)
        {
            // Camera has been told to start sending images, get buffer pages.
            int i;

            WFScamedtBufNewCntr++;  // bump up watchdog counter
            //WFScamLastPath = "eEdtBufStatReadNew";
            dbprintf(("hrtWFScamDataPtr->edtBufStat == eEdtBufStatReadNew\n"));

            // This code is based on the rtlinux/testirq.c example from EDT.
            for (i = 0; i < WFS_BUFFERS; i++)
            {
                // get the kernel pages for each ring buffer
                edtKernelPages[i] = (u_char **)edt_get_buf_kbuffer_pages(WFS_dev, i, &nEdtKernelPages[i]);
                SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_Init) npages: ", nEdtKernelPages[i]);
            }
            hrtWFScamDataPtr->edtBufStat = eEdtBufStatOkay;

            waitBeforeIntrEnable = true;
        }
#       endif  /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
        else if (hrtWFScamDataPtr->intrIgnore)
        {
            // ignore the interrupt if we're doing serial communications
            WFScamintrIgnoreCntr++; // bump up watchdog counter
            //WFScamLastPath = "intrIgnore";

#           if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
            edt_intr_process(WFS_dev);      // let EDT do its interrupt processing
            rtl_hard_enable_irq(WFS_irq);   // re-enable the rtl irq
#           endif  /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
        }
        else
        {
            //printf("DEL-cam proc interrupt\n");
            // there was nothing else more important
            // process the interrupt
            WFScamIntrWatchCntr++;  // bump up WFS camera watchdog counter
            //WFScamLastPath = "proc interrupt";

#           if (CAM_TYPE != WFS_REAL_GPI)

            // get the current done counter -- must be simulation mode
            doneCount = edtDoneCount_sim;

#           else    /* CAM_TYPE says we have the real camera */

            // let EDT do its interrupt processing
            edt_intr_process(WFS_dev);

#           if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterEDTIntrProc);
#           endif   /* TIMER_TEST */

            if (hrt_useWfsSimData == 1)
            {
                // get the current done counter -- simulation mode
                doneCount = edtDoneCount_sim;
                edtDoneCount_sim++;
            }
            else
            {
                // get the current EDT done counter
                doneCount = edt_get_done(WFS_dev);
            }

            rtl_hard_enable_irq(WFS_irq);   // re-enable the rtl irq

#           if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &afterWFSIRQEnable);
#           endif   /* TIMER_TEST */
#           endif   /* CAM_TYPE said we have the real camera */

            if (doneCount < 7)
            {
                // doneCount goes to zero when switching modes,
                // let it settle
                lastDoneIntr = doneCount;
                WFScamdc7++;
            }
            else if (lastDoneIntr == doneCount)
            {
                // interrupt could be a serial interrupt or sg ready
                // (not sure what "sg ready" is)
                // note -- not doing anything with BadCntr at present
                BadCntr++;
            }
            else
            {
                // everything's okay, so proceed
#               if ( TIMER_TEST != 0 )
                camFrameID++;
                timerNdx = camFrameID % NUM_TIME_SETS;
                for (i = 0; i < eTimerMaxTimes; i++)
                    timerTimes[timerNdx][i] = intrTime;
                timerTimes[timerNdx][eTimerLastIntr] = prevIntrTime;
                prevIntrTime = intrTime;
                // No need to set eTimerThisIntr, the above loop
                // took care of it.
                // timerTimes[timerNdx][eTimerThisIntr] = intrTime;
                timerTimes[timerNdx][eTimerAfterWFSWake] = afterWFSWake;
                timerTimes[timerNdx][eTimerAfterEDTIntrProc] = afterEDTIntrProc;
                timerTimes[timerNdx][eTimerAfterWFSIRQEnable] = afterWFSIRQEnable;
#               endif   /* TIMER_TEST */

                lastDoneIntr = doneCount;

                // the last DMA buffer is one less than DoneCount
                curbuf = (doneCount - 1) % WFS_BUFFERS;

                // This next step represents the majority of the
                // local processing time.
                WFScam_LoadDataBuffer(curbuf);
                WFScamldb++;
            } /* end if block tests on doneCount */
            WFScamdoneCntr  = doneCount;
        } // end else we actually processed the interrupt

#       if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
        if (waitBeforeIntrEnable)
        {
            WFScamWaitBefore++;
            // Based on the value of hrtWFScamDataPtr->edtBufStat
            // (indicating some change in the buffer layout)
            // we did not handle nor re-enable the interrupt above,
            // so we do it here after suitable delay.
            // the following 2 delays seem to be needed to avoid being inundated with EDT interrupts (that crash the kernel)
            // 6 millisecs -- tried 2, 4, and 5; 2 and 4 were too fast, 5 was okay
            rtl_usleep(6000);


            edt_intr_process(WFS_dev);  // let EDT do its interrupt processing

            rtl_usleep(6000);



            rtl_hard_enable_irq(WFS_irq);   // re-enable the rtl irq
        }
#       endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */

        hrtFileDataShmPtr->watchDogBits |= WatchDog_CamBit;
        interruptWatch = 0;
        WFScamIntrWatchReset++;

    } // end while (!QuitCamera_WFS_Thread)

    // if we are here then we must have been told to quit
    return NULL;
} /* end WFScam() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_Init( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize variables used by the camera thread.
 *  @par
 *
 *  @b Usage: WFScam_Init( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

static void
WFScam_Init( void )
{
    int             i;
    unsigned int    sips;


    // make sure buf_array pointer start out as null
    for (i = 0; i < WFS_BUFFERS; i++)
    {
        buf_array_160[i] = NULL;
    }


    // initialize variables
    hrtWFScamDataPtr->cmdStart = eCmdNone;
    hrtWFScamDataPtr->cmdCrnt  = eCmdNone;
    hrtWFScamDataPtr->stateHRT = 0;
    dbprintf(("state1 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));

    hrtWFScamDataPtr->camWriteNdx   = 0;    // Indexs are incremented at end

    hrtWFScamDataPtr->edtBufStat    = eEdtBufStatNotInitted;
    hrtWFScamDataPtr->intrIgnore    = false;

    // initialize the circular buffer status flags
    for( i=0; i<NUM_HRT_DATA_BUFFS; i++)
    {
        (hrtDataFlowControlPtr->bufStat[i]).hrtBufState     = eBufIdle;
        (hrtDataFlowControlPtr->bufStat[i]).dispDoUpdate    = false;
        (hrtDataFlowControlPtr->bufStat[i]).inDisplayProcess = false;
        (hrtDataFlowControlPtr->bufStat[i]).inSaveProcess   = false;
    }

    hrtWFScamDataPtr->rawDispWriteBuffNdx   = 0;
    hrtWFScamDataPtr->rawDispReadBuffNdx    = 1;


    // SysCon has been starting other threads
    // Let sysCon finish getting file data from the SRT before continuing.
    printf("          WFScam_Init: starting to spin on !OkayToFinishInit\n");
    hrtWFScamDataPtr->stateHRT++;
    dbprintf(("state2 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));
    sips    = 0;
    while (!OkayToFinishInit)
    {
        // Sleep for a tenth of a second.
        sips++;
        rtl_usleep(100000);
    }
    // Getting here means that HRT SysCon has got file data from SRT.
    // Therefore SRT is up and already in its main loop handling commands,
    // and that means soft/WFScamMngr StartCam() has opened the EDT device for WFS.
    printf("    WFScam_Init: Ending spin on !OkayToFinishInit after %d ds\n", sips);
    hrtWFScamDataPtr->stateHRT++;
    dbprintf(("state3 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));


    // send command for SRT to do the first step of EDT camera interface
    // initialization in user space
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInit1 -- start");
    // issue Initialize1 command to SRT
    // SRT will read WFS camera config file and load it into WFS camera
    WFScam_SendCmd (eWFSInitialize1, 0);
    hrtWFScamDataPtr->stateHRT++;
    dbprintf(("state4 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));

    // wait a second for that to happen
    //rtl_usleep(1000000);

    // Actually, nothing else here depends on Initialize1 being done.
    // So instead of sleep just wait until HRT has done WFScam_IntrInit()
    sips    = 0;
    while (hrtWFScamDataPtr->IntrInitDrinkMe   != 0xC0FFEE8D)
    {
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam() waited %d ds for IntrInitDrinkMe\n", sips);

    dbprintf(("state5 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): CCC camInit1 -- end");

#   if (CAM_TYPE == WFS_REAL_GPI)
#   if (RTL_SYSTEM == 0)
    // user-side mode
    // It must be true that HRT main has already called WFScam_IntrInit()
    // if so then we have WFS_dev
    if ((pdv_multibuf(WFS_dev, WFS_BUFFERS)) != 0)
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFScam_Init) failed to configure ring buffers");
    else
    {
        // in user-side mode we use the buffers established
        // by the EDT API after the call to pdv_multibuf()
        // then we do the de-interleave from those
        unsigned char **tmp_array = edt_buffer_addresses(WFS_dev);
        for (i = 0; i < WFS_BUFFERS; i++)
            buf_array_160[i] = (unsigned short *)tmp_array[i];
    }
#   else    /* RTL_SYSTEM */
    // RTLinux mode

    // ... because in RTLinux mode we have our own static image buffers
    // and we use WFScam_LoadDataBuffer() to copy from the EDT kernel pages
    // into those static buffers and then we do the de-interleave from those
    for (i = 0; i < WFS_BUFFERS; i++)
    {
        buf_array_160[i] = wfsCamBuffsForRTL[i];
    }
#   endif  /* RTL_SYSTEM */
#   endif  /* CAM_TYPE */
    hrtWFScamDataPtr->stateHRT++;
    dbprintf(("state6 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));


    // save data for simulated data mode
    for (i = 0; i < WFS_BUFFERS; i++)
    {
        buf_array[i]     = wfsCamBuffsFor128[i];  // added for use with software ROI from 160x160 to 128x128
        buf_array_sim[i] = wfsCamBuffsForSim[i];
    }

    // We have not actually waited for SRT to finish Initialize1,
    // so this setting may be premature, but it should only affect
    // the WFScam_DrvrWatch() watchdog thread, and that seems to be
    // mostly harmless.
    WFScamInitDone = true;

    hrtWFScamDataPtr->stateHRT++;
    dbprintf(("state7 H=%d S=%d\n", hrtWFScamDataPtr->stateHRT, hrtWFScamDataPtr->stateSRT));
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_Init): WFScamInit done");
} /* end WFScam_Init() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_IntrInit( void )
 *
 *  @brief
 *  The purpose of this unit is to initialize the wfs camera interrupt.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the wfs camera interrupt.  This
 *  MUST be separate from the other cam initialization so that it can be called
 *  from SysCon while in RTCore main() initalization mode.  Also note that
 *  gpAoSrtCamMngr_StartCam() (on the SRT side) MUST have been executed by the
 *  time this function is called.
 *  @par
 *
 *  @b Usage: WFScam_IntrInit( void );
 *
 *  @b Param None
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInterruptVectorError
 *  @exception exceptionSleepError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *
 *******************************************************************************
 */

void
WFScam_IntrInit( void )
{
    // this is usually called by the HRT main routine
#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    unsigned long affinity  = 1 << HRT_CPU_WFS_CAM;
    unsigned int    sips    = 0;

    // StartCamDrinkMe should be set by SRT soft/WFScamMngr StartCam()
    while(hrtWFScamDataPtr->StartCamDrinkMe != 0xC0FFEE8D)
    {
        /*
        Wait until SRT has done soft/WFScamMngr StartCam()
        That should have happened already because SysCon initHRT()
        calls this WFScam_IntrInit() routine after waiting for
        hrtFileDataShmPtr->srtIsUpFlag to be set.
        So this double check "sips" should always show 0.
        */
        sips++;
        rtl_usleep(100000);  /* one decisecond */
    }
    printf("WFScam_IntrInit waited %d ds\n", sips);
    // when SRT soft/WFScamMngr StartCam() we know that
    // the EDT module has been asked to open the WFS device

    // get the IRQ# from the EDT driver
    WFS_irq = edt_get_irq_value(WFS_UNIT_NUM);

    if (WFS_irq < 0)
    {
        /*
        This condition is probably deadly.
        It probably makes sense to find a way to signal that we need
        to shutdown.
        */
        printf("WFScam_IntrInit(): WFS_irq < 0\n");
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFScam_IntrInit) failed to get irq value: ",  WFS_irq);
        // should we do this?
        //hrtFileDataShmPtr->hrtShutdown  = true;
    }
    else
    {
        printf("WFScam_IntrInit(): getting WFSdevunit=%d WFS_irq=%d\n", WFS_UNIT_NUM, WFS_irq);
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_IntrInit) WFS camera using irq ", WFS_irq);

        WFS_dev = edt_get_dev_from_irq(WFS_irq);
        edt_take_interrupt(WFS_irq, WFS_dev);

        // establish the real-time interrupt handler for the WFS irq
        if ((rtl_request_irq( WFS_irq, WFScam_IntrHandler )) != 0)
        {
            printf("FAIL WFScam_IntrInit(): rtl_request_irq()\n");
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (WFScam_IntrInit) in rtl_request_irq()");
        }

        // This routine is called from HRT main or initHRT
        // so it will have been running on the SysCon CPU.
        // We want the irq to be handled on the WFScam CPU.
        if (rtl_irq_set_affinity(WFS_irq, &affinity, &oldAffinity) != 0)
        {
            printf("FAIL WFScam_IntrInit(): rtl_irq_set_affinity()\n");
            SysConSendStrngNoParm(eDebugLevelErr, "ERR (WFScam_IntrInit) in rtl_irq_set_affinity()");
        }
    }
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */

    // it might make more sense to require success above before setting this
    hrtWFScamDataPtr->IntrInitDrinkMe   = 0xC0FFEE8D;

} /* end WFScam_IntrInit() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_IntrHandler( unsigned int irq, struct rtl_frame *regs )
 *
 *  @brief
 *  Process commands originating in the HRT Camera control component (File: WFScam.c) .
 *
 *  @b DESCRIPTION:
 *  This routine is called in response to an interrupt generated by the EDT driver upon
 *  receipt of a complete frame of camera data.  It's sole purpose is to check error
 *  conditions and activate a semaphore which triggers the processing of the camera frame data.
 *  @par
 *
 *  @b Usage: WFScam_IntrHandler(  unsigned int irq, struct rtl_frame *regs );
 *
 *  @param[in] irq (int):
 *  @param[in] regs (struct rtl_frame *):
 *
 *  @return 0: None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static unsigned int
WFScam_IntrHandler(unsigned int irq, struct rtl_frame *regs)
{
#   if ( TIMER_TEST != 0 )
    rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#   endif /* TIMER_TEST */

#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    if (++interruptWatch < 1000000)
    {
        WFScamIntrHandler++;
        // unlock WFSSem
        if (rtl_sem_post(&WFSSem) != 0)
        {
            switch (errno)
            {
                case EINVAL:
                    WFScamInvSem++;
                    break;
                case EOVERFLOW:
                    WFScamOvfSem++;
                    break;
                default:
                    WFScamUnkSem++;
                    break;
            }
        }
    } else {
        /*
        Even with a 1500 frame/s camera rate we cannot get here for 15
        minutes or more after failing to handle interrupts.
        That will be long after the system has locked up.
        */
    }
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
    return 0;
} /* end WFScam_IntrHandler() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_ProcCmd( short cmdNdx )
 *
 *  @brief
 *  Process camera commands received from the host.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process camera commands received from the host,
 *  via the SRT side, such as automatically setting the optimal frame rate, forcing
 *  the frame rate to a particular value, etc.
 *  @par
 *
 *  @b Usage:
 *  WFScam_ProcCmd( short cmdNdx );
 *
 *  @param[in] cmdNdx (short): index into CmdTable[] for the command to be processed
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFScam_ProcCmd( short cmdNdx )
{
    unsigned int    sips;

    // switch on command ID corresponding to command index
    switch (CmdTable[cmdNdx].idNum)
    {
    case eCmdAbort:     // abort long commands
        SysConSendStrngNoParm(eDebugLevelErr, "ERR (WFScam_ProcCmd) unexpected command abort received by cam");
        break;


    case eCmdWFScamInit:    // initialize SciMeasure WFS camera
        hrtWFScamDataPtr->Init1DrinkMe  = 0;
        printf("WFScam_ProcCmd eCmdWFScamInit");
        WFScam_SendCmd(eWFSInitialize1, 0);

        // wait until SRT has done WFSInitialize1
        sips    = 0;
        while (hrtWFScamDataPtr->Init1DrinkMe   != 0xC0FFEE8D)
        {
            sips++;
            rtl_usleep(100000);  /* one decisecond */
        }
        printf("WFScam_ProcCmd() waited %d ds for Init1DrinkMe\n", sips);

        hrtWFScamDataPtr->Init2DrinkMe  = 0;
        WFScam_SendCmd(eWFSInitialize2, 0);

        // wait until SRT has done WFSInitialize2
        sips    = 0;
        while (hrtWFScamDataPtr->Init2DrinkMe   != 0xC0FFEE8D)
        {
            sips++;
            rtl_usleep(100000);  /* one decisecond */
        }
        printf("WFScam_ProcCmd() waited %d ds for Init2DrinkMe\n", sips);

        WFScam_SendCmd(eWFSSetRep, LegalFrameRates[ hrt_camFrameRate ]);
        WFScam_SendCmd(eWFSSetAtten, (3 - hrt_camGain));
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamRateAuto:      // do 'autoCamRate' setting
        {
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFScam_ProcCmd) Command is not supported");
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        }
        break;

    case eCmdCamRate:      // set frame rate of the camera (per second)
        {
        int tmpRate = hrtWFScamDataPtr->cmdIntParam[0];
        CamFrameRateNdx = 0;  // find a legal camera rate
        /* WARNING:  The following code ASSUMES that the LegalFrameRates array is ordered. */
        while ((tmpRate > LegalFrameRates[CamFrameRateNdx]) && (LegalFrameRates[CamFrameRateNdx + 1] != -1))
            CamFrameRateNdx ++;
        hrt_camFrameRate = CamFrameRateNdx;
        WFScam_SendCmd( eWFSSetRep, LegalFrameRates[hrt_camFrameRate] );
        gpAoHrtFileData_Put( eFileTypeParms );
        gpAoHrtCent_GetCrntDarkBackFlat( hrt_camFrameRate );  // get dark, back, and flat data for current frame rate
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        }
        break;

    case eCmdCamGain:      // set camera gain (0, 1, 2, or 3)
        hrt_camGain = hrtWFScamDataPtr->cmdIntParam[0];
        WFScam_SendCmd (eWFSSetAtten, (3 - hrt_camGain));
        gpAoHrtFileData_Put(eFileTypeParms);
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamSRate:       // read frame rate from camera
        WFScam_SendCmd (eWFSReadRep, 0);
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        break;

    case eCmdCamSGain:     // read gain from camera
        WFScam_SendCmd (eWFSReadAtten, 0);
        hrtWFScamDataPtr->cmdCrnt = eCmdNone;
        break;


    default:
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFScam_ProcCmd): unexpected command received by cam, cmdNdx = ", cmdNdx);
        break;
    }
} /* end WFScam_ProcCmd() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_SendCmd( short cmd, short cmdParam )
 *
 *  @brief
 *  Send a command and associated parameter (cmd dependent) to the SRT camera manager
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send messages to the SRT side (by using the FIFO diagnostic
 *  message process) to write commands to the camera and/or to change the DMA mapping (to accommodate
 *  changes in frame size).
 *  @par
 *
 *  @b Usage:
 *  WFScam_SendCmd( short cmd, short cmdParam );
 *
 *  @param[in] cmd (int):  Cmd from HRT side that needs to be passed to SRT side manager
 *  @param[in] cmdParam (int): Paramter value (use depends on the command) for this particular command
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileErr
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFScam_SendCmd( short cmd, short param )
{
    static diagFIFOMsgType diagFIFOMsg;

    // set up a diag FIFO message and send it to hostDiag
    diagFIFOMsg.diagID = eDiagWFSCmd;
    diagFIFOMsg.cmdNdx = cmd;
    diagFIFOMsg.intParam[0] = param;

    if (rtl_write(DiagFIFOfd, &diagFIFOMsg, sizeof(diagFIFOMsgType)) < 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFScam_SendCmd): error writing WFScam command to diag FIFO, errno = ", rtl_errno);
}


/*!
 *******************************************************************************
 *
 *  @fn WFScam_LoadDataBuffer( int bufNdx )
 *
 *  @brief
 *  Process frames from the WFS camera.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to process frames from the WFS camera.  This means
 *  finding the next available slot in the camera data circular buffer, de-interleaving
 *  the data from the camera's several amplifiers, and alerting the centroider that
 *  new data is available.
 *  @par
 *
 *  @b Usage:
 *  WFScam_LoadDataBuffer( int bufNdx );
 *
 *  @param[in] bufNdx (int):  index of the SciMeasure camera buffer to process
 *
 *  @return
 *  None.
 *
 *  @exception exceptionInvalidFileCmd
 *  @exception exceptionFileErr
 *
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFScam_LoadDataBuffer(int bufNdx)
{
    int                         i, j;
#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    static const int            pageSizeInUshorts = RTL_PAGE_SIZE / sizeof(unsigned short);
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
    long                        tmpWrtNdx;
    CamBuffType*                camBuffPtr;
    DataBufferElemStatusType*   bufStatPtr;

    static int                  dispCntr = 0;
    static int                  overflowCntr = 0;
    static int                  overflowCheckCntr = 0;


    // Set a temp write index to cameraWriteNdx for validation
    //
    tmpWrtNdx = hrtWFScamDataPtr->camWriteNdx;
    bufStatPtr = &(hrtDataFlowControlPtr->bufStat[ tmpWrtNdx ]);

    // if this buffer is busy, then we have an overrun condition ( diag data dump, processing, (or display??) )
    if ( bufStatPtr->hrtBufState != eBufIdle )
    {
        overflowCntr++;
    }

    if ( ++overflowCheckCntr >= LegalFrameRates[hrt_camFrameRate] )
    {
        // this block happens once a second
        overflowCheckCntr = 0;
        if (overflowCntr > 0)
        {
            SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFScam_LoadDataBuffer) circ. buf overrun: (chk 1/sec), writeNdx = ", tmpWrtNdx);
            printf( "WFScam_LoadDataBuffer: circ. buf overruns %d\n", overflowCntr );
            overflowCntr = 0;
        }
    }

    if ( bufStatPtr->inDisplayProcess != false )    // Highly unexpected
    {
        SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFScam_LoadDataBuffer) display overrun - this frame was not processed for display");
    }

    if ( bufStatPtr->inSaveProcess != false )       // Somewhat expected - particularly at debug time
    {
        SysConSendStrngIntParm(eDebugLevelWarn, "WARN (WFScam_LoadDataBuffer) 'save to file' overrun - this frame is in midst of save to disk - buff Ndx #", tmpWrtNdx );
    }

    // At this point this frame buffer appears to be viable (possibly with Warning(cam_WFS.c)s!)
    //  - set frame state to cam Processing
    //  - update the camWriteNdx
    //  - set Point to buffer
    bufStatPtr->hrtBufState = eBufCamProc;
    camBuffPtr = &(hrtWFScamDataPtr->camBuffs[ tmpWrtNdx ]);

    // determine if the frame will have to be sent to SRT for display
    //  - if so - flag it here - the last HRT process (currently recon) will send FIFO message to SRT based on 'bufStatPtr->dispDoUpdate'
    //
    if( hrtDataFlowControlPtr->dispRate != 0)
    {
        if( ++hrtDataFlowControlPtr->dispCntr >= (float)LegalFrameRates[ hrt_camFrameRate ] / hrtDataFlowControlPtr->dispRate )
        {
            hrtDataFlowControlPtr->dispCntr = 0;
            bufStatPtr->dispDoUpdate = true;
            if( ++(hrtDataFlowControlPtr->nextDispBuf) >= NUM_SRT_DISPLAY_BUFS )
                hrtDataFlowControlPtr->nextDispBuf = 0;
        }
        else
            bufStatPtr->dispDoUpdate = false;
    }

#   if ((CAM_TYPE == WFS_REAL_GPI) && (RTL_SYSTEM != 0))
    // when in RTL mode, construct camera frame from kernel pages
    // (or load sim data, if that's what we're using)
    if (hrt_useWfsSimData == 1)
    {
        WFScam_LoadSimWfsData( buf_array_sim[bufNdx], bufNdx );
    }
    else
    {
        // From testing on rtc.ucolick.org in November 2012:
        // This memcpy() loop is 'slow'. In the best case, it
        // completes in 65 microseconds; in the worst case, it
        // takes more than 400 microseconds. This is unreasonable,
        // since processing a full frame must not take more than
        // 666 microseconds if the camera must run at 1500 Hz.

        // Replacing memcpy() with an SSE-enabled function
        // reduced the best::worst times to roughly 17::60
        // microseconds, a speedup of a factor of four.

        // This code is based on the rtlinux/testirq.c example from EDT.

        // At the previous reconfigure of the ring buffers we
        // saved the number of kernel pages for each ring buffer
        // and we saved the pointer to the array of pointers
        // to kernel pages for each ring buffer.
        for (i = 0; i < nEdtKernelPages[bufNdx]; i++)
        {
            // copy the contents of ith kernel page of the bufNdxth ring buffer
            // from edtKernelPages to buf_array_160
            sse_memcpy(&buf_array_160[bufNdx][i * pageSizeInUshorts],
                edtKernelPages[bufNdx][i],
                RTL_PAGE_SIZE);
        }
#       if ( TIMER_TEST != 0 )
        rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[timerNdx][eTimerAfterWFSBufCopy]);
#       endif   /* TIMER_TEST */
    }
#   endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */

    if (hrt_useWfsSimData == 1)
    {
        // deinterleave from the simulated data to the shared memory buffer
        WFScam_DeIntlv128x128( buf_array_sim[bufNdx], (void *)&camBuffPtr->usedCamData[0][0][0] );
    }
    else
    {
        // deinterleave from our copy of the kernel image pages to the shared memory buffer
        WFScam_DeIntlv128x128( buf_array_160[bufNdx], (void *)&camBuffPtr->usedCamData[0][0][0] );
    }


    //    tell the Centroid thread that data's ready
    bufStatPtr->hrtBufState = eBufReqCent;

    if (rtl_sem_post( &CentSem ) < 0)
    {
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFScam_LoadDataBuffer) posting to CentSem, errno = ", rtl_errno);
        printf( "WFScam_LoadDataBuffer: error posting CentSem\n" );
    }




#   if ( TIMER_TEST != 0 )
    rtl_clock_gettime(RTL_CLOCK_REALTIME, &timerTimes[timerNdx][eTimerAfterCentPost]);
#   endif

    // now that we've sent the data off to the centroider, move the
    // temporary destination matrix from WFScam_DeIntlv128x128()
    // to a raw display buffer at 10 fps -- this will get picked up
    // by dispraw, if dispraw is running
    if (++dispCntr >= (LegalFrameRates[hrt_camFrameRate] / 10))
    {
        dispCntr = 0;

        hrtWFScamDataPtr->rawDispWriteBuffNdx ^= 0x1;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                hrtWFScamDataPtr->rawDispBuffs[hrtWFScamDataPtr->rawDispWriteBuffNdx][i][j] = tmpDest[i][j];
        }
        hrtWFScamDataPtr->rawDispReadBuffNdx = hrtWFScamDataPtr->rawDispWriteBuffNdx;
    }

    if ( tmpWrtNdx % LegalFrameRates[ hrt_camFrameRate ] == 0 )
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_LoadDataBuffer): Buf #", tmpWrtNdx  );

    if( ++tmpWrtNdx >= NUM_HRT_DATA_BUFFS)
    {
        SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_LoadDataBuffer): Normal Buffer Wrap, WriteNdx = ", tmpWrtNdx);
        tmpWrtNdx = 0;
    }
    hrtWFScamDataPtr->camWriteNdx = tmpWrtNdx;

} /* end WFScam_LoadDataBuffer() */


/*!
 *******************************************************************************
 *
 *  @fn WFScam_DeIntlv128x86( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x86 length camera frame into a 96x96 destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x86 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *
 *  @b Usage:
 *  WFScam_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static void
WFScam_DeIntlv128x86( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW])
{
    int i, j, k;
    int colCntr, colSkipper, rowCntr;
    static boolean firstTime = TRUE;

    if (hrt_useWfsSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[NUM_SEGS_IN_CAM_160] = { 12, 8, 4, 0, 2, 6, 10, 14, 15, 11, 7, 3, 1, 5, 9, 13 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_DeIntlv128x86): Setting up de-interleave map");
            firstTime = false;

            // first create a map to deinterleave into 128 element rows
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (NUM_SEGS_IN_CAM / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
                    {
                        tmpMap[i * NUM_PIX_IN_ROW_ACTUAL +
                               j * NUM_PIX_IN_SEG_ROW +
                               k] =
                            i * NUM_PIX_IN_ROW_ACTUAL * 2 + k * NUM_SEGS_IN_CAM + segOrder[j];

                        tmpMap[(NUM_ROWS_IN_CAM_ACTUAL - 1 - i) * NUM_PIX_IN_ROW_ACTUAL +
                               (NUM_SEGS_IN_CAM / 2 - 1 - j) * NUM_PIX_IN_SEG_ROW +
                               (NUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * NUM_PIX_IN_ROW_ACTUAL * 2 + k * NUM_SEGS_IN_CAM + segOrder[j + NUM_SEGS_IN_CAM / 2];
                    }
                }
            }

            // now skip every third column on a per row basis -- this gets us down to the final map
            k = 0;
            for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                {
                    if (((j + 1) % 3) > 0)
                    {
                        deIntlvMap[k] = tmpMap[i * NUM_PIX_IN_ROW_ACTUAL + j];
                        k++;
                    }
                }
            }
        }

        // deinterleave the source array into the destination matrix, offsetting the destination matrix as necessary
        // > (IMPORTANT: it is assumed that the unused part of the destination matrix is never written to; so, it is
        // > not set to zeroes here)
        k = 0;
        rowCntr = 0;                                    // this is for dispraw and can be removed when dispraw isn't needed
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;                                // this is for dispraw and can be removed when dispraw isn't needed
            colSkipper = 0;                             // this is for dispraw and can be removed when dispraw isn't needed
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[deIntlvMap[k]];
                tmpDest[rowCntr][colCntr] = dest[i][j]; // this is for dispraw and can be removed when dispraw isn't needed
                colCntr += 1 + colSkipper;              // this is for dispraw and can be removed when dispraw isn't needed
                colSkipper ^= 0x1;                      // this is for dispraw and can be removed when dispraw isn't needed
                k++;
            }
            rowCntr++;                                  // this is for dispraw and can be removed when dispraw isn't needed
        }
    }
    else
    {
        // save data for dispraw
        k = 0;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
            }
        }

        // don't de-interleave, if we're using simulated data (but skip every third column)
        rowCntr = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)
        {
            colCntr = 0;
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)
            {
                dest[i][j] = src[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr];
                colCntr += 1 + colSkipper;
                colSkipper ^= 0x1;
            }
            rowCntr++;
        }
    }
} /* end WFScam_DeIntlv128x86() */

/*!
 *******************************************************************************
 *
 *  @fn WFScam_DeIntlv128x128( unsigned short *src, unsigned short *dest )
 *
 *  @brief
 *  De-interleave a 128x128 length camera frame into a 96x96 length destination buffer.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to de-interleave a 128x128 length camera frame into
 *  a 96x96 destination buffer.  The reason for this is that the camera data
 *  is transmitted in parallel as it is read from the CCD's several amplifiers,
 *  causing it to be scrambled.  This unit unscrambles it.
 *  @par
 *  This unit also calls a function to display the raw camera data to the monitor
 *  at 20 fps.  For this reason and because de-interleaving is less efficient, this
 *  function is quite a bit slower than WFScam_DeIntlv128x86 and, so,  should only
 *  be used for test purposes.
 *  @par
 *  Quantifying 'slow': in December 2012 on rtc.ucolick.org, in the best
 *  case, this takes 12 microseconds to run; in the worst case, it takes
 *  about 65 microseconds. That's slightly less than 50% of the total
 *  per-frame computation time of the WFS camera thread.
 *  @par
 *
 *  @b Usage:
 *  WFScam_DeIntlv128x86( &src, dest );
 *
 *  @param[in] src (unsigned short*): pointer to the ring buffer
 *  @param[out] dest[][] (unsigned short):  output matrix of size [NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW]
 *
 *  @return
 *  None.
 *
 *  @exception None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- start XXX
// XXX this could definitely be more efficient !! XXX
static void
WFScam_DeIntlv128x128( unsigned short *src, unsigned short dest[NUM_ROWS_IN_CAM][NUM_PIX_IN_ROW])
{
    int i, j, k;
    int rowCntr, rowSkipper, colCntr, colSkipper;
    static boolean firstTime = TRUE;

    if ( ++CheckCntr >= TEST_INTERVAL * LegalFrameRates[hrt_camFrameRate] )
    {
        CheckCntr = 0;
        Cntr = 1;
    }

    if (hrt_useWfsSimData != 1)  // if we're not using simulated data
    {
        // these are the order of the segments in the data array -- so, for example, the top-left pixel is
        // > in the segment inexed by 12, the next pixel to the right is in the segment indexed by 8, etc.
        const int segOrder[NUM_SEGS_IN_CAM_160] = { 3, 13, 0, 10, 4, 14, 1, 11, 2, 12, 17, 7, 16, 6, 19, 9, 15, 5, 18, 8 };

        // if this is the first time executing this function, set up a mapping array
        // > the map is set up such that dest[n] = src[map[n]]
        if (firstTime)
        {
            SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_DeIntlv128x128): Setting up de-interleave map");
            firstTime = false;

            // create a map to deinterleave into 128 element rows
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACT_160 / 2); i++)  // divided by two, because we do 2 rows at a time
            {
                for (j = 0; j < (NUM_SEGS_IN_CAM_160 / 2); j++)  // again, divided by two, because we do 2 rows at a time
                {
                    for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
                    {
                        // map elements for "top" half of CCD
                        tmpMap160[i * NUM_PIX_IN_ROW_ACT_160 +
                                  j * NUM_PIX_IN_SEG_ROW +
                                  k] =
                            i * NUM_PIX_IN_ROW_ACT_160 * 2 + k * NUM_SEGS_IN_CAM_160 + segOrder[j];

                        // map elements for "bottom" half of CCD
                        tmpMap160[(NUM_ROWS_IN_CAM_ACT_160 - 1 - i) * NUM_PIX_IN_ROW_ACT_160 +
                                  (NUM_SEGS_IN_CAM_160 / 2 - 1 - j) * NUM_PIX_IN_SEG_ROW +
                                  (NUM_PIX_IN_SEG_ROW - 1 - k)] =
                            i * NUM_PIX_IN_ROW_ACT_160 * 2 + k * NUM_SEGS_IN_CAM_160 + segOrder[j + NUM_SEGS_IN_CAM_160 / 2];
                    }
                }
            }

            //  rotate the map 180 degrees
            for (i = 0; i < NUM_ROWS_IN_CAM_ACT_160; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACT_160; j++)
                {
                    // actually, the comment above is incorrect
                    // see that no rotation is happening here
                    // this is just a copy from tmpMap160 to deIntlvMap160
                    deIntlvMap160[i * NUM_PIX_IN_ROW_ACT_160 + j]   =
                    tmpMap160    [i * NUM_PIX_IN_ROW_ACT_160 + j];
                }
            }
        } /* end if firstTime */

        //  deinterleave the source array into a temporary destination matrix -- deinterleaving could be done more quickly if
        //  > we didn't go through tmpDest[][] -- but, for now, tmpDest[][] is needed for dispraw
        k = 0;    // mrr DM

        rowCntr = ROWS_OFFSET_160;        // mrr 48
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)    // mrr 64
        {
            colCntr = PIX_IN_ROW_OFFSET_160;    // mrr 48
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)    // mrr 64
            {
                // starting at row 48, col 48 copy 64 pixel for 64 rows.
                // tmpDest now holds the center 64x64 pixels of the 160x160 array
                tmpDest[i][j] = src[deIntlvMap160[rowCntr * NUM_PIX_IN_ROW_ACT_160 + colCntr]] >> 2;    // mrr DM    // mrr 160 // mrr why >> 2? to reduce 16 bits to 14 ???
                //tmpDest[i][j] = k++;
                colCntr++;
            }
            rowCntr++;
        }

        // now skip every fifth column on a per row basis and every fifth row, offsetting the destination matrix
        // > as necessary -- this gets us down to the final frame for processing
        // That is to say, we take the central 40x40 pixels of the 64x64 pixels,
        // but we skip every fifth one so that the dest[][] gets 32x32 pixels,
        // except that we store those offset by 4 rows and columns such that
        // the 32x32 is stored in the middle of dest, which is a 40x40 array.
        rowCntr = 12;    // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)    // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;    // the start in tmpDest
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)    // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {    // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {    // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }    // end of not using sim data
    else
    {    // using sim data
        //    save data for dispraw
        k = 0;
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                tmpDest[i][j] = src[k];
                k++;
                //tmpDest[i][j] = src[i][j];
                if (Cntr == 1)
                {
                //    printf( "%d ", tmpDest[i][j]);
                }
            }
            if (Cntr == 1)
            {
                //printf( "\n ");
            }
        }

        rowCntr = 12;   // the start in tmpDest (64x64)
        rowSkipper = 0;
        for (i = ROWS_OFFSET; i < (ROWS_OFFSET + NUM_ROWS_IN_CAM_FOR_SUBAPS); i++)    // mrr 4, 32    // the positions in dest (40x40)
        {
            colCntr = 12;    // the start in tmpDest
            colSkipper = 0;
            for (j = PIX_IN_ROW_OFFSET; j < (PIX_IN_ROW_OFFSET + NUM_PIX_IN_ROW_FOR_SUBAPS); j++)    // mrr 4, 32 // the positions in dest (40x40)
            {
                dest[i][j] = tmpDest[rowCntr][colCntr];
                if (++colSkipper == 4)
                {    // skip this column in tmpdest
                    colSkipper = 0;
                    colCntr++;
                }
                colCntr++;
            }
            if (++rowSkipper == 4)
            {    // skip this row in tmpdest
                rowSkipper = 0;
                rowCntr++;
            }
            rowCntr++;
        }
    }
    Cntr = 0;
} /* end WFScam_DeIntlv128x128() */
// XXX this is a new 128x128 from that ROIs from 160x160 for the Lick 3m upgrade -- end XXX

/*!
 *******************************************************************************
 *
 *  @fn WFScam_DrvrWatch( void* arg )
 *
 *  @brief
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the wfs camera or load simulated data when in user-side mode.
 *
 *  @b DESCRIPTION:
 *  This is a thread used as a camera watchdog, when in RTL mode, and to control
 *  the wfs camera or load simulated data when in user-side mode.
 *  @par
 *  When in RTL mode, the thread continually sets a 7 second timer and, whenever
 *  it times out, checks a WFS camera frame counter to make sure that frames have
 *  been received.  If no frames have been received, an error condition is raised.
 *  @par
 *
 *  @b Usage:
 *  WFScam_DrvrWatch(void* arg);
 *
 *  @param[in] arg (void*): Required for pthread_create(), not currently used
 *
 *  @return
 *  None.
 *
 *  @exception exceptionCameraTimout  The camera frames have been missing for > 7 sec.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void *
WFScam_DrvrWatch( void *arg )
{
    SysConSendStrngIntParm(eDebugLevelMax, "INFO Camera Type: ", CAM_TYPE);

#   if (RTL_SYSTEM == 0)
    cpu_set_t   mask;
    int         cpuNum = HRT_CPU_WFS_CAM;

    CPU_ZERO( &mask );
    CPU_SET( cpuNum, &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_DrvrWatch) - CPU #",  cpuNum );
    if( sched_setaffinity( 0, sizeof( mask ), &mask) != 0)
        SysConSendStrngIntParm(eDebugLevelErr, "ERR (WFScam_DrvrWatch) executing sched_setaffinity() for CPU #",  cpuNum );
    sched_getaffinity( 0, sizeof( mask ), &mask);
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_DrvrWatch) Affinity is: CPU_ISSET = ", CPU_ISSET( cpuNum, &mask));
#   else   /* RTL_SYSTEM */
    SysConSendStrngIntParm(eDebugLevelMax, "INFO (WFScam_DrvrWatch) CamWatch thread id: ", rtl_pthread_self());
#   endif  /* RTL_SYSTEM */

#   if (RTL_SYSTEM != 0)
    printf("    hard WFScam_DrvrWatch() CPU:thread %d:%d\n",
    rtl_getcpuid(),rtl_pthread_self());   // sla debugging hang
#   endif   /* RTL_SYSTEM */

    //  spin on the InitDone flags for the other threads
    while (!CentInitDone || !ReconInitDone || !WFScamInitDone)
    {
        // Sleep for a tenth of a second.
        rtl_usleep(100000);
    }
    SysConSendStrngNoParm(eDebugLevelMax, "INFO (WFScam_DrvrWatch) Initialization appears to be done");
    printf("    hard WFScam_DrvrWatch() infloop\n");

    while (!QuitWFScamWatchThread)
    {
        boolean postWfsSem;
        postWfsSem = false;

#       if (CAM_TYPE != WFS_REAL_GPI)
        // begin no real camera so use simulated
        {
            // if we don't have a real camera, use the system clock to
            // simulate a frame rate
            if ( LegalFrameRates[ hrt_camFrameRate ] != 0 )
            {
                rtl_usleep(1000000 / LegalFrameRates[ hrt_camFrameRate ]);
            }
            else
            {
                rtl_usleep(1000000);
            }

            hrt_useWfsSimData = 1;  // make sure we're using simulated data
            postWfsSem = true;

#           if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#           endif  /* TIMER_TEST */
        }

        // end no real camera so use simulated
#       else  // (CAM_TYPE == WFS_REAL_GPI)
        // begin real camera
#       if (RTL_SYSTEM == 0)
        // begin user-side mode -- this will only work for the user-side code!!
        {
            static int timeouts, last_timeouts = 0, recovering_timeout = FALSE;

            pdv_wait_image(WFS_dev);       // wait for the last image
#           if ( TIMER_TEST != 0 )
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &intrTime);
#           endif  /* TIMER_TEST */
            pdv_start_images(WFS_dev, 1);  // start the next image

            // check for a camera timeout
            timeouts = pdv_timeouts(WFS_dev);
            if (timeouts > last_timeouts)
            {
                pdv_timeout_restart(WFS_dev, TRUE);
                last_timeouts = timeouts;
                recovering_timeout = TRUE;
                SysConSendStrngIntParm(eDebugLevelWarn, "WARN - SMC WFS camera user mode timeout", timeouts);
            }
            else if (recovering_timeout)
            {
                pdv_timeout_restart(WFS_dev, TRUE);
                recovering_timeout = FALSE;
            }
            else
                postWfsSem = true;
        }
        // end user-side mode -- this will only work for the user-side code!!
#       else /* RTL_SYSTEM */
        // begin RTL mode -- this is just the camera watchdog timer
        {
#           ifdef VERBOSE_DEBUGGING
            static struct timespec      ts;
#           endif /* VERBOSE_DEBUGGING */
            static struct rtl_timespec  next;
            int                         retVal;

            // wait for 7 seconds (increased from 1 sec to accommodate
            // longer delay when switching camera programs)
            //rtl_usleep(7000000);
            next.tv_sec     = 7;
            next.tv_nsec    = 0;
            if ((retVal = rtl_nanosleep(&next, NULL)) < 0)
                printf("WARN (WFScam_DrvrWatch) rtl_nanosleep(), retVal = %d\n", retVal);

#           ifdef VERBOSE_DEBUGGING
            rtl_clock_gettime(RTL_CLOCK_REALTIME, &ts);
#           endif /* VERBOSE_DEBUGGING */

            // make sure we've received camera frames and, if not - inform the user
            if (WFScamIntrWatchCntr == 0)
            {
                // no interrupts were processed during previous watchdog interval
                dbprintf(("%ld bc %d tw %d nc %d cs %d ii %d ebi %d ebn %d "
                "ih=%d wait=%d iw=%d ir=%d is=%d os=%d us=%d dc=%d dc7=%d ldb=%d LP=%s  NW=%s\n",
                ts.tv_sec,
                BadCntr             ,   /* bc */
                WFScamtrywaitCntr   ,   /* tw */
                WFScamnullCDPCntr   ,   /* nc */
                WFScamcmdStartCntr  ,   /* cs */
                WFScamintrIgnoreCntr,   /* ii */
                WFScamedtBufIniCntr ,   /* ebi */
                WFScamedtBufNewCntr,    /* ebn */
                WFScamIntrHandler,      /* ih */
                WFScamWaitBefore,       /* wait */
                interruptWatch,         /* iw */
                WFScamIntrWatchReset,   /* ir */
                WFScamInvSem,           /* is */
                WFScamOvfSem,           /* os */
                WFScamUnkSem,           /* us */
                WFScamdoneCntr,         /* dc */
                WFScamdc7,              /* dc7 */
                WFScamldb,              /* ldb */
                WFScamLastPath,         /* LP */
                WFScamLastNonWatchdog));

                //if (strcmp(WFScamLastPath, "watchdog") != 0) {
                //    WFScamLastNonWatchdog = WFScamLastPath;
                //}

                WFScamtrywaitCntr   = 0;
                //WFScamIntrHandler   = 0;
                //WFScamLastPath = "watchdog";

                SysConSendStrngNoParm(eDebugLevelWarn, "WARN (WFScam_DrvrWatch) SMC WFS camera RTL watchdog timeout");
            }
            else if (WFScamIntrWatchCntr > 0)
            {
                // some interrupts were processed during previous watchdog interval
                dbprintf(("%ld iwc=%d bc=%d tw=%d nc=%d cs=%d ii=%d ebi=%d ebn=%d "
                "ih=%d wait=%d iw=%d ir=%d is=%d os=%d us=%d dc=%d dc7=%d ldb=%d LP=%s  NW=%s\n",
                ts.tv_sec,
                WFScamIntrWatchCntr ,   /* iwc */
                BadCntr             ,   /* bc */
                WFScamtrywaitCntr   ,   /* tw */
                WFScamnullCDPCntr   ,   /* nc */
                WFScamcmdStartCntr  ,   /* cs */
                WFScamintrIgnoreCntr,   /* ii */
                WFScamedtBufIniCntr ,   /* ebi */
                WFScamedtBufNewCntr,    /* ebn */
                WFScamIntrHandler,      /* ih */
                WFScamWaitBefore,       /* wait */
                interruptWatch,         /* iw */
                WFScamIntrWatchReset,   /* ir */
                WFScamInvSem,           /* is */
                WFScamOvfSem,           /* os */
                WFScamUnkSem,           /* us */
                WFScamdoneCntr,         /* dc */
                WFScamdc7,              /* dc7 */
                WFScamldb,              /* ldb */
                WFScamLastPath,         /* LP */
                WFScamLastNonWatchdog));

                WFScamIntrWatchCntr = 0;
            }
        }
        // end RTL mode -- this is just the camera watchdog timer
#       endif  /* RTL_SYSTEM */
        // end real camera
#       endif  /* CAM_TYPE */

#       if ((CAM_TYPE != WFS_REAL_GPI) || (RTL_SYSTEM == 0))
        // if we're using simulated data, load it
        if (hrt_useWfsSimData == 1)
        {
            int curbuf;
            curbuf = edtDoneCount_sim % WFS_BUFFERS;
            WFScam_LoadSimWfsData( buf_array_sim[curbuf], curbuf );
            edtDoneCount_sim++;
        }

        // if we need to post the wfs semaphore, post it
        if (postWfsSem)
        {
            // we are not the RTL system
            // so there was no interupt to unlock the semaphore
            // so we unlock WFSSem
            rtl_sem_post(&WFSSem);
        }
#       endif /* CAM_TYPE and RTL_SYSTEM say we are the real-time code */
    } /* end while (!QuitWFScamWatchThread) loop until quit */
    return NULL;
} /* end WFScam_DrvrWatch() */


/*!
 *******************************************************************************
 *  @fn WFScam_LoadSimWfsData( unsigned short buf_array[], int curbuf )
 *
 *  @brief
 *  This unit loads simulated wfs data into the buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated wfs data into the buffer supplied either from a
 *  file (loaded at system startup) or by generating it in one of several ways,
 *  depending on how flags are set in the code.
 *  @par
 *
 *  @b Usage:
 *  WFScam_LoadSimWfsData(&buf_array, curbuf);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *  @param[in] curbuf:    current wfs buffer (used to choose the right simulated data)
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
enum { eSimFromFile, eSimRamp, eSimTipTilt, eSimPupilMove, eSimZernike, eSimCombo };
//const static int simSelect = eSimZernike;  // choose simulated data    // mrr
const static int simSelect = eSimFromFile;  // choose simulated data
static unsigned short buf_array2[NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL];

static void
WFScam_LoadSimWfsData( unsigned short buf_array[], int curbuf )
{
    static int  testVal = 0;
    int         i, j;

    if (buf_array == NULL)  // make sure the buf_array pointer exists
        return;

    switch (simSelect)
    {
    case eSimFromFile:
    default:
        //    if file-based data is available, use it; otherwise, zero the buffer
        if( hrtWFScamDataPtr->simFileRead  )
        {
            for (i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL); i++)
            {
                for (j = 0; j < (NUM_PIX_IN_ROW_ACTUAL ); j++)
                {
                    // buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = hrtWFScamDataPtr->simDataBuffs[curbuf][i][j];    // mrr
                    buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = hrtWFScamDataPtr->simDataBuffs[0][i][j];    // we only loaded the first buffer // mrr
                }
            }
        }
        else  // in case the file doesn't exist, zero the buffer
        {
            for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
            {
                for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
                    buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = 0.0;
            }
        }
        break;

    case eSimRamp:
        // just loop through sequential data values
        for (i = 0; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_ACTUAL; j++)
            {
                buf_array[i * NUM_PIX_IN_ROW_ACTUAL + j] = testVal;
                if (++testVal > 12345)
                    testVal = 0;
            }
        }
        break;

    case eSimTipTilt:
        WFScam_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the wfs
        break;

    case eSimPupilMove:
        WFScam_LoadSimPupilMoveData( buf_array );  // put low frequency plus high frequency pupil move data, in X and or Y
        break;

    case eSimZernike:
        WFScam_LoadSimZernikes( buf_array );  // put Zernike modes on the wfs
        break;

    case eSimCombo:
        WFScam_LoadSimTTData( buf_array );  // put low frequency plus high frequency tip and/or tilt on the wfs
        WFScam_LoadSimZernikes( buf_array2 );  // put Zernike modes on the wfs
        for ( i = 0; i < NUM_ROWS_IN_CAM_ACTUAL * NUM_PIX_IN_ROW_ACTUAL; i++)
            buf_array[i] = buf_array[i] / 2 + buf_array2[i] / 2;
        break;
    }
}

/*!
 *******************************************************************************
 *  @fn WFScam_LoadSimTTData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency tip and/or tilt
 *  signals into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFScam_LoadSimTTData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
static const boolean makeTip  = true;  // turn tip on or off
static const boolean makeTilt = true;  // turn tilt on or off
static const boolean makeSquare = true;  // make a square wave instead of a sine wave
static const int lowFreqTT  = 1;  // 7;  //  25;  // low frequency TT in Hz  -- these may be approximate, depending on how
static const int highFreqTT = 1;  // 100;  // high frequency TT in Hz    -- > well they divide into the frame rate
static const float lowFreqGainTT  = 0.50;  // gain to be applied to low frequency TT (0.0 means no low freq)
static const float highFreqGainTT = 0.00;  // gain to be applied to high frequency TT (0.0 means no high freq)

static void
WFScam_LoadSimTTData( unsigned short buf_array[] )
{
int         i, j;
int         rowCntr, rowSkipper, colCntr, colSkipper;
int         stepsPerCycleLow, stepsPerCycleHigh;
static int  stepCntrLow = -1, stepCntrHigh = -1;
float       lowLeftBot, lowRightTop, highLeftBot, highRightTop; // these are for low and high frequency -- left/bottom
                                                                // > pixel (for tip/tilt) and right/top pixel

    float tipVector[NUM_PIX_IN_ROW_FOR_SUBAPS], tiltVector[NUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up tip and tilt vectors
    if (lowFreqTT <= 0)
        stepsPerCycleLow = 1;
    else
        stepsPerCycleLow = LegalFrameRates[hrt_camFrameRate] / lowFreqTT;
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;

    if (highFreqTT <= 0)
        stepsPerCycleHigh = 1;
    else
        stepsPerCycleHigh = LegalFrameRates[hrt_camFrameRate] / highFreqTT;
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowLeftBot   = (1.0 + sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    lowRightTop  = (1.0 - sin((float)stepCntrLow  / (float)stepsPerCycleLow  * TWO_PI) * lowFreqGainTT)  * 4000.0;
    highLeftBot  = (1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;
    highRightTop = (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainTT) * 4000.0;

    if (makeSquare)
    {
        if (lowLeftBot < 4000)
            lowLeftBot = 0;
        else
            lowLeftBot = 1000;

        if (lowRightTop < 4000)
            lowRightTop = 0;
        else
            lowRightTop = 1000;

        if (highLeftBot < 4000)
            highLeftBot = 0;
        else
            highLeftBot = 1000;

        if (highRightTop < 4000)
            highRightTop = 0;
        else
            highRightTop = 1000;
    }

    if (!makeTip)
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            tipVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i += 2)
        {
            tipVector[i    ] = lowLeftBot  + highLeftBot;
            tipVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    if (!makeTilt)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            tiltVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
        {
            tiltVector[i    ] = lowLeftBot  + highLeftBot;
            tiltVector[i + 1] = lowRightTop + highRightTop;
        }
    }

    // combine the tip and tilt data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(tiltVector[i] + tipVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFScam_LoadSimPupilMoveData( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated low frequency plus high frequency pupil move data
 *  signals, in X and/or Y into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFScam_LoadSimPupilMoveData(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static const boolean makePupilMoveX = true;  // turn X on or off
static const boolean makePupilMoveY = true;  // turn Y on or off

static const float lowFreqPM  = 0.1;  // low frequency pupil move in Hz  -- these may be approximate, depending on how
static const float highFreqPM = 1.0;  // high frequency pupil move in Hz -- > well they divide into the frame rate

static const float lowFreqGainPM  = 1.0;  // gain to be applied to low frequency PM (0.0 means no low freq)
static const float highFreqGainPM = 1.0;  // gain to be applied to high frequency PM (0.0 means no high freq)

static void
WFScam_LoadSimPupilMoveData( unsigned short buf_array[] )
{
    int         i, j;
    int         rowCntr, rowSkipper, colCntr, colSkipper;
    int         stepsPerCycleLow, stepsPerCycleHigh;
    static int  stepCntrLow = -1, stepCntrHigh = -1;
    float       lowStart, lowIncr, highStart, highIncr;
    float       xVector[NUM_PIX_IN_ROW_FOR_SUBAPS], yVector[NUM_ROWS_IN_CAM_FOR_SUBAPS];

    // set up X and Y vectors
    stepsPerCycleLow = (int)((float)LegalFrameRates[hrt_camFrameRate] / lowFreqPM);
    if (++stepCntrLow >= stepsPerCycleLow)
        stepCntrLow = 0;
    stepsPerCycleHigh = (int)((float)LegalFrameRates[hrt_camFrameRate] / highFreqPM);
    if (++stepCntrHigh >= stepsPerCycleHigh)
        stepCntrHigh = 0;

    lowStart =   (1.0 - sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0;
    lowIncr  = (((1.0 + sin((float)stepCntrLow / (float)stepsPerCycleLow * TWO_PI) * lowFreqGainPM) * 2000.0) - lowStart) /
        NUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS

    highStart =   (1.0 - sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0;
    highIncr  = (((1.0 + sin((float)stepCntrHigh / (float)stepsPerCycleHigh * TWO_PI) * highFreqGainPM) * 2000.0) - highStart) /
        NUM_PIX_IN_ROW_FOR_SUBAPS;  // this will only work right if NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS

    if (!makePupilMoveX)
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_PIX_IN_ROW_FOR_SUBAPS; i++)
            xVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    if (!makePupilMoveY)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = 0.0;
    }
    else
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
            yVector[i] = lowStart + highStart + (float)i * (lowIncr + highIncr);
    }

    // combine the X and Y data into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)(yVector[i] + xVector[j]);
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFScam_LoadSimZernikes( unsigned short buf_array[] )
 *
 *  @brief
 *  This unit loads simulated Zernike modes into the wfs buffer supplied.
 *
 *  @b DESCRIPTION:
 *  This unit loads simulated Zernike modes into the wfs buffer supplied.
 *  @par
 *
 *  @b Usage:
 *  WFScam_LoadSimZernikes(&buf_array);
 *
 *  @param[in] buf_array[]: buffer to load data into
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

#define MAX_Z_COEFFS    66
#define NUM_M1_M2_ZERNS 20

static const float zernFreq =   0.0;  // frequency at which to modulate Zernike modes -- 0.0 means no modulation
static const float zernGain =   0.5;  // gain to be applied to Zernike modes

// Set each of the following to between -1 and 1
static const float
zernSelect[NUM_M1_M2_ZERNS] =
{
     0.0,   // Z0   Piston -- not used, leave it set to zero
     0.0,   // Z1   Tip     M2
     0.0,   // Z2   Tilt    M2
     1.0,   // Z3   Focus   M2
     0.0,   // Z4   Astigmatism (cosine)            M1
     0.0,   // Z5   Astigmatism (sine)              M1
     1.0,   // Z6   Coma x                          M1
     0.0,   // Z7   Coma y                          M1
     0.0,   // Z8   Spherical                       M1
     1.0,   // Z9   Trefoil (cosine)                M1
     0.0,   // Z10  Trefoil (sine)                  M1
     0.0,   // Z11  Secondary astigmatism (cosine)  M1
     1.0,   // Z12  Secondary astigmatism (sine)    M1
     0.0,   // Z13  Secondary coma x                M1
     0.0,   // Z14  Secondary coma y                M1
     0.0,   // Z15  Secondary spherical             M1
     0.0,   // Z16  Tetrafoil x                     M1
     0.0,   // Z17  Tetrafoil y                     M1
     0.0,   // Z18  Secondary trefoil x             M1
     0.0,   // Z19  Secondary trefoil y             M1
};

static float pupilZernikes[MAX_Z_COEFFS][NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];
float zernikes[NUM_PIX_IN_ROW_FOR_SUBAPS][NUM_ROWS_IN_CAM_FOR_SUBAPS];  // shape constructed from selected Zernike modes

static void
WFScam_LoadSimZernikes( unsigned short buf_array[] )
{
    static boolean  firstTimeFlag = true;
    int             i, j, k;
    int             rowCntr, rowSkipper, colCntr, colSkipper;
    float           minVal, maxVal;

    int             stepsPerCycle;
    static int      stepCntr = -1;
    float           freqMultiplier;

    // if it hasn't been done yet, calculate the Zernike modes use to construct the shape
    // > to be put in the wfs buffer
    if (firstTimeFlag)
    {
        firstTimeFlag = false;
        WFScam_ComputeZernikes();
    }

    // now, construct the shape from the selected Zernike modes
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            zernikes[i][j] = 0.0;
    }
    for (k = 0; k < NUM_M1_M2_ZERNS; k++)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
                zernikes[i][j] += pupilZernikes[k][i][j] * zernSelect[k];
        }
    }

    // so the sub-aps look more like slopes, subtract the minimum value from each one (this
    // > will also make all the values >= zero)
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i += 2)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j += 2)
        {
            minVal = (float)1.0e10;
            if (zernikes[i    ][j    ] < minVal) minVal = zernikes[i    ][j    ];
            if (zernikes[i    ][j + 1] < minVal) minVal = zernikes[i    ][j + 1];
            if (zernikes[i + 1][j    ] < minVal) minVal = zernikes[i + 1][j    ];
            if (zernikes[i + 1][j + 1] < minVal) minVal = zernikes[i + 1][j + 1];
            zernikes[i    ][j    ] -= minVal;
            zernikes[i    ][j + 1] -= minVal;
            zernikes[i + 1][j    ] -= minVal;
            zernikes[i + 1][j + 1] -= minVal;
        }
    }

    // calculate the multiplier for this step in the sin wave modulation (the Zernike modes shape can
    // > be temporally modulated to test the M1 and M2 offloading filters)
    if (zernFreq == 0.0)
        freqMultiplier = zernGain;
    else
    {
        stepsPerCycle = (int)((float)LegalFrameRates[hrt_camFrameRate] / zernFreq);
        if (++stepCntr >= stepsPerCycle)
            stepCntr = 0;
        freqMultiplier = sin((float)stepCntr / (float)stepsPerCycle * TWO_PI) * zernGain;  // apply the gain here, too
    }

    // modulate the shape
    maxVal = 0.0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            if (zernikes[i][j] > maxVal)
                maxVal = zernikes[i][j];
        }
    }
    if (maxVal > 0.0)
    {
        for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
        {
            for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
            {
                zernikes[i][j] =  1.0 - zernikes[i][j] / maxVal * 2.0;  // normalize to between -1.0 and 1.0
                zernikes[i][j] =  1.0 - zernikes[i][j] * freqMultiplier;// this will now be between 0.0 and 2.0, centered around 1.0
                zernikes[i][j] *= 8000.0;                               // put into DN-like units (the TT camera is 14-bits)
            }
        }
    }

    // put the zernike modes into the buf array, skipping columns and rows as necessary
    rowCntr = 0;
    rowSkipper = 0;
    for (i = 0; i < NUM_ROWS_IN_CAM_FOR_SUBAPS; i++)
    {
        colCntr = 0;
        colSkipper = 0;
        for (j = 0; j < NUM_PIX_IN_ROW_FOR_SUBAPS; j++)
        {
            buf_array[rowCntr * NUM_PIX_IN_ROW_ACTUAL + colCntr] = (unsigned short)zernikes[i][j];
            colCntr += 1 + colSkipper;
            colSkipper ^= 0x1;
        }
        rowCntr += 1 + rowSkipper;
#       if ( NUM_ROWS_IN_CAM_ACTUAL != 86 )
        rowSkipper ^= 0x1;
#       endif
    }
}


/*!
 *******************************************************************************
 *
 *  @fn WFScam_ComputeZernikes( void )
 *
 *  @brief
 *  This unit calculates values for all Zernike modes.
 *
 *  @b DESCRIPTION:
 *  This unit calculates values for all Zernike modes.
 *  @par
 *
 *  @b Usage:
 *  WFScam_ComputeZernikes();
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

static double rho[NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];
static double theta[NUM_ROWS_IN_CAM_FOR_SUBAPS][NUM_PIX_IN_ROW_FOR_SUBAPS];

static void
WFScam_ComputeZernikes( void )
{
    int i, j, k;
    // IMPORTANT: this will only work right if
    // NUM_PIX_IN_ROW_FOR_SUBAPS == NUM_ROWS_IN_CAM_FOR_SUBAPS
    int xyOffset = NUM_PIX_IN_ROW_FOR_SUBAPS / 2;
    double x, y;
    double rho1, rho2, rho3, rho4, rho5, rho6, rho7, rho8, rho9, rho10;

    for (i = 0; i < MAX_Z_COEFFS; i++)
    {
        for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
        {
            for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
                pupilZernikes[i][j][k] = 0.0;
        }
    }

    // we're putting the zernike modes on a square grid; but, the equations are easier
    // > in polar form; so, compute rho and theta for each point
    for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        x = (double)(j - xyOffset);

        for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            y = (double)(k - xyOffset);

            rho[j][k] = sqrt(x * x + y * y) / (double)xyOffset;  // rho will be between 0 and 1
            theta[j][k] = atan2(y, x);  // theta will be between -pi and pi
        }
    }

    for (j = 0; j < NUM_ROWS_IN_CAM_FOR_SUBAPS; j++)
    {
        for (k = 0; k < NUM_PIX_IN_ROW_FOR_SUBAPS; k++)
        {
            if (rho[j][k] >= 0.0)
            {
                rho1  = rho[j][k];
                rho2  = rho1 * rho[j][k];
                rho3  = rho2 * rho[j][k];
                rho4  = rho3 * rho[j][k];
                rho5  = rho4 * rho[j][k];
                rho6  = rho5 * rho[j][k];
                rho7  = rho6 * rho[j][k];
                rho8  = rho7 * rho[j][k];
                rho9  = rho8 * rho[j][k];
                rho10 = rho9 * rho[j][k];

/*  Z0 */       pupilZernikes[0][j][k] = 1.0;  // 0 - piston

/*  Z1 */       pupilZernikes[1][j][k] = (float)(rho1 * cos(theta[j][k]));  // 2 - tilt
/*  Z2 */       pupilZernikes[2][j][k] = (float)(rho1 * sin(theta[j][k]));  // 1 - tip
/*  Z3 */       pupilZernikes[3][j][k] = (float)(2.0 * rho2 - 1.0);      // 4 - focus

/*  Z4 */       pupilZernikes[4][j][k]  = (float)(rho2 * cos(2.0 * theta[j][k]));                   // 5 - astig with axis at 0 or pi/2
/*  Z5 */       pupilZernikes[5][j][k]  = (float)(rho2 * sin(2.0 * theta[j][k]));                   // 3 - astig with axis at +/- pi/4
/*  Z6 */       pupilZernikes[6][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * cos(theta[j][k]));           // 8 - third order coma along y axis
/*  Z7 */       pupilZernikes[7][j][k]  = (float)((3.0 * rho3 - 2.0 * rho1) * sin(theta[j][k]));           // 7 - third order coma along x axis
/*  Z8 */       pupilZernikes[8][j][k]  = (float)(6.0 * rho4 - 6.0 * rho2 + 1.0);                   // 12 - third order spherical
/*  Z9 */       pupilZernikes[9][j][k]  = (float)(rho3 * cos(3.0 * theta[j][k]));                   // 9 - triangular astig with base on y axis
/* Z10 */       pupilZernikes[10][j][k] = (float)(rho3 * sin(3.0 * theta[j][k]));                   // 6 - triangular astig with base on x axis
/* Z11 */       pupilZernikes[11][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * cos(2.0 * theta[j][k]));       // 13
/* Z12 */       pupilZernikes[12][j][k] = (float)((4.0 * rho4 - 3.0 * rho2) * sin(2.0 * theta[j][k]));       // 11
/* Z13 */       pupilZernikes[13][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * cos(theta[j][k]));  // 18
/* Z14 */       pupilZernikes[14][j][k] = (float)((10.0 * rho5 - 12.0 * rho3 + 3.0 * rho1) * sin(theta[j][k]));  // 17
/* Z15 */       pupilZernikes[15][j][k] = (float)(20.0 * rho6 - 30.0 * rho4 + 12.0 * rho2 - 1.0);           // 24
/* Z16 */       pupilZernikes[16][j][k] = (float)(rho4 * cos(4.0 * theta[j][k]));                   // 14
/* Z17 */       pupilZernikes[17][j][k] = (float)(rho4 * sin(4.0 * theta[j][k]));                   // 10
/* Z18 */       pupilZernikes[18][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3) * cos(3.0 * theta[j][k]));       // 19
/* Z19 */       pupilZernikes[19][j][k] = (float)(( 5.0 * rho5 - 4.0 * rho3) * sin(3.0 * theta[j][k]));       // 16

                pupilZernikes[20][j][k] = (float)(    rho5                 * sin(5.0 * theta[j][k]));  // 15
                pupilZernikes[21][j][k] = (float)(    rho5                 * cos(5.0 * theta[j][k]));     // 20

                pupilZernikes[22][j][k] = (float)(    rho6                  * sin(6.0 * theta[j][k]));  // 21
                pupilZernikes[23][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)          * sin(4.0 * theta[j][k]));  // 22
                pupilZernikes[24][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2) * sin(2.0 * theta[j][k]));  // 23
                pupilZernikes[25][j][k] = (float)((15.0 * rho6 - 20.0 * rho4 +  6.0 * rho2) * cos(2.0 * theta[j][k]));  // 25
                pupilZernikes[26][j][k] = (float)(( 6.0 * rho6 -  5.0 * rho4)          * cos(4.0 * theta[j][k]));  // 26
                pupilZernikes[27][j][k] = (float)(    rho6                  * cos(6.0 * theta[j][k]));  // 27

                pupilZernikes[28][j][k] = (float)(    rho7                       * sin(7.0 * theta[j][k]));  // 28
                pupilZernikes[29][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                   * sin(5.0 * theta[j][k]));  // 29
                pupilZernikes[30][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)           * sin(3.0 * theta[j][k]));  // 30
                pupilZernikes[31][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1) * sin(       theta[j][k]));  // 31
                pupilZernikes[32][j][k] = (float)((35.0 * rho7 - 60.0 * rho5 + 30.0 * rho3 - 4.0 * rho1) * cos(       theta[j][k]));  // 32
                pupilZernikes[33][j][k] = (float)((21.0 * rho7 - 30.0 * rho5 + 10.0 * rho3)           * cos(3.0 * theta[j][k]));  // 33
                pupilZernikes[34][j][k] = (float)(( 7.0 * rho7 -  6.0 * rho5)                   * cos(5.0 * theta[j][k]));  // 34
                pupilZernikes[35][j][k] = (float)(    rho7                       * cos(7.0 * theta[j][k]));  // 35

                pupilZernikes[36][j][k] = (float)(    rho8                         * sin(8.0 * theta[j][k]));  // 36
                pupilZernikes[37][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                 * sin(6.0 * theta[j][k]));  // 37
                pupilZernikes[38][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)         * sin(4.0 * theta[j][k]));  // 38
                pupilZernikes[39][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2) * sin(2.0 * theta[j][k]));  // 39
                pupilZernikes[40][j][k] = (float)( 70.0 * rho8 - 140.0 * rho6 + 90.0 * rho4 - 20.0 * rho2 + 1.0);             // 40
                pupilZernikes[41][j][k] = (float)((56.0 * rho8 - 105.0 * rho6 + 60.0 * rho4 - 10.0 * rho2) * cos(2.0 * theta[j][k]));  // 41
                pupilZernikes[42][j][k] = (float)((28.0 * rho8 -  42.0 * rho6 + 15.0 * rho4)         * cos(4.0 * theta[j][k]));  // 42
                pupilZernikes[43][j][k] = (float)(( 8.0 * rho8 -   7.0 * rho6)                 * cos(6.0 * theta[j][k]));  // 43
                pupilZernikes[44][j][k] = (float)(    rho8                         * cos(8.0 * theta[j][k]));  // 44

                pupilZernikes[45][j][k] = (float)(     rho9                                * sin(9.0 * theta[j][k]));  // 45
                pupilZernikes[46][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                        * sin(7.0 * theta[j][k]));  // 46
                pupilZernikes[47][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                * sin(5.0 * theta[j][k]));  // 47
                pupilZernikes[48][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)        * sin(3.0 * theta[j][k]));  // 48
                pupilZernikes[49][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1) * sin(    theta[j][k]));  // 49
                pupilZernikes[50][j][k] = (float)((126.0 * rho9 - 280.0 * rho7 + 210.0 * rho5 - 60.0 * rho3 + 5.0 * rho1) * cos(    theta[j][k]));  // 50
                pupilZernikes[51][j][k] = (float)(( 84.0 * rho9 - 168.0 * rho7 + 105.0 * rho5 - 20.0 * rho3)        * cos(3.0 * theta[j][k]));  // 51
                pupilZernikes[52][j][k] = (float)(( 36.0 * rho9 -  56.0 * rho7 +  21.0 * rho5)                * cos(5.0 * theta[j][k]));  // 52
                pupilZernikes[53][j][k] = (float)((  9.0 * rho9 -   8.0 * rho7)                        * cos(7.0 * theta[j][k]));  // 53
                pupilZernikes[54][j][k] = (float)(     rho9                                * cos(9.0 * theta[j][k]));  // 54

                pupilZernikes[55][j][k] = (float)(         rho10                                   * sin(10.0 * theta[j][k]));    // 55
                pupilZernikes[56][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                           * sin( 8.0 * theta[j][k]));    // 56
                pupilZernikes[57][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                   * sin( 6.0 * theta[j][k]));    // 57
                pupilZernikes[58][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)           * sin( 4.0 * theta[j][k]));    // 58
                pupilZernikes[59][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * sin( 2.0 * theta[j][k]));  // 59
                pupilZernikes[60][j][k] = (float)( 252.0 * rho10 - 630.0 * rho8 + 560.0 * rho6 - 210.0 * rho4 + 30.0 * rho2 - 1.0);                // 60
                pupilZernikes[61][j][k] = (float)((210.0 * rho10 - 504.0 * rho8 + 420.0 * rho6 - 140.0 * rho4 + 15.0 * rho2) * cos( 2.0 * theta[j][k]));  // 61
                pupilZernikes[62][j][k] = (float)((120.0 * rho10 - 252.0 * rho8 + 168.0 * rho6 -  35.0 * rho4)               * cos( 4.0 * theta[j][k]));    // 62
                pupilZernikes[63][j][k] = (float)(( 45.0 * rho10 -  72.0 * rho8 +  28.0 * rho6)                              * cos( 6.0 * theta[j][k]));    // 63
                pupilZernikes[64][j][k] = (float)(( 10.0 * rho10 -   9.0 * rho8)                           * cos( 8.0 * theta[j][k]));    // 64
                pupilZernikes[65][j][k] = (float)(         rho10                                   * cos(10.0 * theta[j][k]));    // 65
            }
        }
    }
}

//******************************************************************************
// * End of file
//******************************************************************************
