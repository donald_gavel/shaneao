/**********************************************************************************************

COPYRIGHT (C) 2004
LAWRENCE LIVERMORE NATIONAL LABORATORY
ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED

*****

MODULE NAME:        cam_LTT_Mngr

AUTHOR:    DHL & DWP    DATE: 10/7/04

COMPONENT DESCRIPTION:
This component contains functions which interface to the WFS Little Joe Camera camera
for the LickAO control system.  Control originates on the HRT in camLTT but all communication
with the EDT driver happens here.

COMPONENT UNITS:
1.    CamLTTMngr_ProcCmd()
2.    CamLTTMngr_StartCam()
3.    CamLTTMngr_StopCam()
4.    CamLTTMngr_RepCount()
5.    CamLTTMngr_TotalBufferSize()
6.    CamLTTMngr_SendToCam()

REVISIONS:

**********************************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/user.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
//#include <rtl_fifo.h>
//#include <rtl_time.h>

#include "types.h"
#include "constant.h"
//#include "sysCon.h"
#include "cam.h"
#include "centroid.h"
#include "tipTilt.h"
#include "cam_LTT.h"
#include "cam_WFS.h"
#include "cam_LTT_Mngr.h"
#include "cam_WFS_Mngr.h"
#include "recon.h"
#include "fileMngr.h"
#include "fileData.h"
#include "hostICon.h"
#include "hostDiag.h"
//dwp100405 #include "psfest.h"
#include "edtinc.h"


// Defines

#define ACK 6  // acknowledge from the SciMeasure Camera

// Local Variables

static int fd;                         // mmap file descriptor for the LTT shared memory
static unsigned char *lttCamBuf;     // pointer for LTT shared memory buffer
static PdvDev *ltt_pdv_p;             // pointer for the LTT PDV/EDT device

static u_int totalCloseLoopBufSize;     // size of the open-loop DMA ring buffer shared memory region
static u_int totalOpenLoopBufSize;     // size of the closed-loop DMA ring buffer shared memory region

static Dependent *dd_p;
static Edtinfo edtinfo;

//XXX 080513dwp static const LTTCamOffsets[4] = { 405, 400, 405, 400 };  //XXXNOISE
//XXX 120111dwp static const LTTCamOffsets[4] = { 0, 0, 0, 0 };  //XXXNOISE //XXX 080513dwp
static const int LTTCamOffsets[4] = { 200, 200, 200, 200 };  //XXXNOISE //XXX 080513dwp


// Local Function Prototypes

static int CamLTTMngr_RepCount (int Frame_Rate);
static u_int CamLTTMngr_TotalBufferSize (int16 DataSize);
static void CamLTTMngr_SendToCam (char outString[]);


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_ProcCmd()

UNIT DESCRIPTION:
This unit processes commands originating in cam_LTT.

UNIT INPUTS:
1. cmd - the command ID
2. param - an integer parameter

UNIT OUTPUTS:
    None

NOTE: pdv_readcfg() seems to put out a newline to the console (??)

**********************************************************************************************/

void CamLTTMngr_ProcCmd (int16 cmd, int16 param)
{
    char textBuf[32];
    static int crntLoopProg;

//    switch on command ID corresponding to command index
    switch (cmd)
    {
        case eLTTInitialize1:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL1\n");  //dwp100405
                edt_disable_ring_buffers(ltt_pdv_p);                                           // disable ring buffers
#ifdef TEST_60X60
                pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-60x60CL.cfg", dd_p, &edtinfo);  // read the open loop config file  //XXX20
                pdv_initcam(ltt_pdv_p, dd_p, LTT_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-60x60CL.cfg", NULL, 0);  // init the cam  //XXX20
#else
                pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-20x20CL.cfg", dd_p, &edtinfo);  // read the open loop config file  //XXX20
                pdv_initcam(ltt_pdv_p, dd_p, LTT_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-20x20CL.cfg", NULL, 0);  // init the cam  //XXX20
#endif
                printf("traceL2\n");  //dwp100405
            }
            break;

        case eLTTInitialize2:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL3\n");  //dwp100405
                pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalOpenLoopBufSize);    // set the DMA ring buffer sizes

                crntLoopProg = OpenLoopProg;
                sprintf (textBuf, "@PRG %d", OpenLoopProg);
                CamLTTMngr_SendToCam (textBuf);          // set the camera program

                sprintf (textBuf, "@FAM %d", OpenLoopFilt);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                 //XXXNOISE

                sprintf (textBuf, "@OIC #0:%d", LTTCamOffsets[0]);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                        //XXXNOISE

                sprintf (textBuf, "@OIC #1:%d", LTTCamOffsets[1]);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                        //XXXNOISE

                sprintf (textBuf, "@OIC #2:%d", LTTCamOffsets[2]);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                        //XXXNOISE

                sprintf (textBuf, "@OIC #3:%d", LTTCamOffsets[3]);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                        //XXXNOISE
//XXX                CamLTTMngr_SendToCam ("@OAC 0");  // XXX0701 zero all offsets  //XXXNOISE

                pdv_start_images(ltt_pdv_p, 0);          // start the free-run acquistion
                CamLTTMngr_SendToCam ("@SEQ 1");      // start sending data from the camera

                HostDiagSendStrngNoParm("Notification: lttcami complete");  //XXX060630
                printf("traceL4\n");  //dwp100405
            }
            break;

        case eLTTMapOpen:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL5\n");  //dwp100405
                CamLTTMngr_SendToCam ("@SEQ 0");  // stop sending data from the camera
                pdv_start_images(ltt_pdv_p, 1);      // stop the free-run acquistion
                edt_disable_ring_buffers(ltt_pdv_p);                                           // disable ring buffers

#ifdef TEST_60X60
                pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-60x60CL.cfg", dd_p, &edtinfo);  // read the open loop config file  //XXX20
                pdv_initcam(ltt_pdv_p, dd_p, LTT_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-60x60CL.cfg", NULL, 0);  // init the cam  //XXX20
#else
                pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-20x20CL.cfg", dd_p, &edtinfo);  // read the open loop config file  //XXX20
                pdv_initcam(ltt_pdv_p, dd_p, LTT_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-20x20CL.cfg", NULL, 0);  // init the cam  //XXX20
#endif
                pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalOpenLoopBufSize);    // set the DMA ring buffer sizes

                crntLoopProg = OpenLoopProg;
                sprintf (textBuf, "@PRG %d", OpenLoopProg);
                CamLTTMngr_SendToCam (textBuf);   // set the camera program

                sprintf (textBuf, "@FAM %d", OpenLoopFilt);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                 //XXXNOISE

                pdv_start_images(ltt_pdv_p, 0);      // and then re-start the free-run acquistion
                CamLTTMngr_SendToCam ("@SEQ 1");  // and restart the camera data

                HostDiagSendStrngNoParm("Notification: lttopen complete");
                printf("traceL6\n");  //dwp100405
            }
            break;

        case eLTTMapClose:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL7\n");  //dwp100405
                CamLTTMngr_SendToCam ("@SEQ 0");  // stop sending data from the camera
                pdv_start_images(ltt_pdv_p, 1);      // stop the free-run acquistion

                edt_disable_ring_buffers(ltt_pdv_p);                                          // disable ring buffers
                pdv_readcfg("/opt/EDTpdv/camera_config/Lil-Joe-6x6CL.cfg", dd_p, &edtinfo);   // read the closed loop config file
                pdv_initcam(ltt_pdv_p, dd_p, LTT_UNIT_NUM, &edtinfo, "/opt/EDTpdv/camera_config/Lil-Joe-6x6CL.cfg", NULL, 0);  // init the cam
                pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalCloseLoopBufSize);  // change the DMA ring buffer sizes

                crntLoopProg = CloseLoopProg;
                sprintf (textBuf, "@PRG %d", CloseLoopProg);
                CamLTTMngr_SendToCam (textBuf);   // set the camera program

                sprintf (textBuf, "@FAM %d", CloseLoopFilt);  //XXXNOISE
                CamLTTMngr_SendToCam (textBuf);                  //XXXNOISE

                pdv_start_images(ltt_pdv_p, 0);      // and then re-start the free-run acquistion
                CamLTTMngr_SendToCam ("@SEQ 1");  // and restart the camera data
                printf("traceL8\n");  //dwp100405
            }

            g_pReconDataPtr->lttCamCloseDone = true;
            break;

        case eLTTSetRep:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL9\n");  //dwp100405
                CamLTTMngr_SendToCam ("@SEQ 0");   // stop sending data from the camera
                pdv_start_images(ltt_pdv_p, 1);       // stop the free-run acquistion

                sprintf (textBuf, "@REP %d", CamLTTMngr_RepCount(param));  // param is the desired frame rate which is converted into a repetition count
                CamLTTMngr_SendToCam (textBuf);

                if (crntLoopProg == OpenLoopProg)  // re-issuing pdv_multibuf_block() seems to be necessary to get frame transfers to restart
                    pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalOpenLoopBufSize);
                else
                    pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalCloseLoopBufSize);

//XXX                CamLTTMngr_SendToCam ("@OAC?");  // query offsets  //XXXNOISE
//XXX                CamLTTMngr_SendToCam ("@FAM?");  // query filter setting  //XXXNOISE

                pdv_start_images(ltt_pdv_p, 0);       // and then re-start the free-run acquistion
                CamLTTMngr_SendToCam ("@SEQ 1");   // restart the camera data

                HostDiagSendStrngNoParm("Notification: lttrate complete");
                printf("traceL10\n");  //dwp100405
            }
            break;

        case eLTTSetAtten:
            if (ltt_pdv_p != NULL)
            {
                printf("traceL11\n");  //dwp100405
                CamLTTMngr_SendToCam ("@SEQ 0");      // stop sending data from the camera
                pdv_start_images(ltt_pdv_p, 1);          // stop the free-run acquistion

                sprintf (textBuf, "@AAM %d", param);  // param is the attenuation for all modules
                CamLTTMngr_SendToCam (textBuf);

                if (crntLoopProg == OpenLoopProg)      // re-issuing pdv_multibuf_block() seems to be necessary to get frame transfers to restart
                    pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalOpenLoopBufSize);
                else
                    pdv_multibuf_block(ltt_pdv_p, LTTnumbufs, lttCamBuf, totalCloseLoopBufSize);

                pdv_start_images(ltt_pdv_p, 0);          // and then re-start the free-run acquistion
                CamLTTMngr_SendToCam ("@SEQ 1");      // restart the camera data

                HostDiagSendStrngNoParm("Notification: lttgain complete");
                printf("traceL12\n");  //dwp100405
            }
            break;

        default:
            HostDiagSendStrngIntParm("Error: unexpected command in cam_LTT_Mngr: ", cmd);
            break;
    }
}


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_StartCam()

UNIT DESCRIPTION:
The purpose of this unit is to initialize the EDT driver and set up the shared memory for the
DMA ring buffers.

UNIT INPUTS:
    None

UNIT OUTPUTS:
    None

**********************************************************************************************/

void CamLTTMngr_StartCam(void)
{
    char devname[128];

    totalCloseLoopBufSize = CamLTTMngr_TotalBufferSize (LTTClosebufsize);
    totalOpenLoopBufSize = CamLTTMngr_TotalBufferSize (LTTOpenbufsize);

//     the shared memory region is used to transfer either the open or closed loop camera data and
//    > since the totalOpenLoopBufSize is the larger size, that's the size allocated.
    if ((fd = open("/dev/LTT_rt_edt_mem", O_RDWR)) < 0)
    {
        printf("Error: couldn't open shared memory in CamLTT\n");
        return;
    }
    lttCamBuf = mmap(0, totalOpenLoopBufSize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (lttCamBuf == MAP_FAILED)
    {
        printf ("Error: couldn't map shared memory in CamLTT_Mngr\n");
        return;
    }

    memset(lttCamBuf, 0x11, totalOpenLoopBufSize);
    printf("In camLTT, allocated %d bytes for %d buffers\n", totalOpenLoopBufSize, LTTnumbufs);

//    open the edt device (EDT_INTERFACE is defined in edtdef.h (included via edtinc.h))
    strcpy(devname, EDT_INTERFACE);
    printf ("Opening camLTT EDTpdv device: %s, unit: %d\n", devname, LTT_UNIT_NUM);
    if ((ltt_pdv_p = pdv_open(devname, LTT_UNIT_NUM)) == NULL)
    {
        printf("Error: pdv_open(%s,%d) failed \n", devname, LTT_UNIT_NUM);
        return;
    }

    dd_p = pdv_alloc_dependent();
}


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_StopCam()

UNIT DESCRIPTION:
The purpose of this unit is to close the EDT driver and unmap the shared memory for the
DMA ring buffers.

UNIT INPUTS:
    None

UNIT OUTPUTS:
    None

**********************************************************************************************/

void CamLTTMngr_StopCam(void)
{
    if (ltt_pdv_p != NULL)
    {
        CamLTTMngr_SendToCam ("@SEQ 0");  // stop sending data from the camera
        pdv_start_images(ltt_pdv_p, 1);      // stop the free-run acquistion
        pdv_close(ltt_pdv_p);
    }

    munmap(lttCamBuf, totalOpenLoopBufSize);
    close(fd);
}


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_RepCount(int Frame_Rate)

UNIT DESCRIPTION:
The purpose of this unit is to compute a repetition count that yields the desired frame rate.
The values in the case statement assume a base frame rate of 500 fps where each additional
repetition adds 50 microseconds to the frame time.  Currently programs 0 and 2 both have 500 fps
rates but if different programs are selected in the future, these values may have to be adjusted.
The rate verses rep count for any program can be determined empirically by running the Little Joe
and PdvShow utilities.  Adjust the rep count in Little Joe and read the frame rate in PdvShow.

UNIT INPUTS:
1.    Frame_Rate - in frames per second

UNIT OUTPUTS:
1.    the repetition count

UNIT ERROR CODES:
    None

**********************************************************************************************/
static int CamLTTMngr_RepCount (int Frame_Rate)
{
    int Count = 0;

    switch (Frame_Rate)
    {
        case 1:
            Count = 19960;    // used for debugging at low frame rates
            break;
        case 50:
            Count = 360;
            break;
        case 100:
            Count = 160;
            break;
        case 200:
            Count = 60;
            break;
        case 250:
            Count = 40;
            break;
        case 350:
            Count = 17;    // actually yields a rate of 350.85
            break;
        case 500:
            Count = 0;
            break;
        default:
            // this should never happen since the frame rate is constrained in the GUI
            HostDiagSendStrngIntParm("Error: illegal Frame Rate in cam_LTT_Mngr = ", Frame_Rate);
            break;
    }
    return Count;
}


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_TotalBufferSize()

UNIT DESCRIPTION:
The purpose of this unit is to calculate the total DMA buffer size - rounding up to page size and
making sure there's room for deinterleave if needed.

UNIT INPUTS:
    Data size

UNIT OUTPUTS:
    None

UNIT ERROR CODES:
    None

**********************************************************************************************/

static u_int CamLTTMngr_TotalBufferSize (int16 DataSize)
{
    int npages;
    u_int bufsize = DataSize;
    u_int totalsize;

    npages = DataSize / PAGE_SIZE;

    if (DataSize % PAGE_SIZE)
    {
        npages++;
        bufsize = npages * PAGE_SIZE;
    }

    totalsize = bufsize * LTTnumbufs;
    return totalsize;
}


/**********************************************************************************************

UNIT NAME:  CamLTTMngr_SendToCam()

UNIT DESCRIPTION:
The purpose of this unit is to send an ASCII command to the camera and read the response.

UNIT INPUTS:
1.    outString - the command string

UNIT OUTPUTS:
    None

**********************************************************************************************/

static void CamLTTMngr_SendToCam (char outString[])
{
    int  i;  //XXX060227
    int  retVal, inCount;
    char inString[64], errMsg[64];

    struct timespec next;

//    printf ("%s - ", outString);  //XXX060228

//    don't bother doing anything, if there's no camera
    if (ltt_pdv_p == NULL)
        return;

//    printf ("Sending to LTT camera: %s\n", outString);  //XXX

//  indicate that interrrupt should be ignored (because we're doing serial comms)  XXXINTIG
    g_pCamLTTPtr->intrIgnore = true;  //XXXINTIG

    next.tv_sec = 0;
    next.tv_nsec = 100000;  // was 1000
    nanosleep(&next, NULL);

//    printf ("1");  //XXX060228

//    flush any junk from the read buffer and issue command to camera
    retVal = pdv_serial_read(ltt_pdv_p, inString, 32);
    next.tv_sec = 0;
    next.tv_nsec = 1000;
    nanosleep(&next, NULL);

//    printf (" %d 2", retVal);  //XXX060228

    retVal = pdv_serial_command(ltt_pdv_p, outString);
    next.tv_sec = 0;
    next.tv_nsec = 1000;
    nanosleep(&next, NULL);

//    printf ("3");  //XXX060228

//    if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
    if (retVal != 0)
    {
        HostDiagSendStrngIntParm("Error from pdv_serial_command() in CamLTT: ", retVal);
        printf ("Error from pdv_serial_command() in CamLTT: %d\n", retVal);  //XXX
    }
    else
    {
//XXX060227        inCount = pdv_serial_wait(ltt_pdv_p, 500, sizeof(inString) - 1);
        inCount = pdv_serial_wait(ltt_pdv_p, 500, 1);  //XXX060227
        next.tv_sec = 0;
        next.tv_nsec = 300000000;  //XXX new
        nanosleep(&next, NULL);

//        printf ("4");  //XXX060228

        if (inCount == 0)
        {
            HostDiagSendStrngNoParm("Error: no response from CamLTT 1");
            printf ("Error: no response from CamLTT 1\n");  //XXX
        }
        else
        {
            retVal = pdv_serial_read(ltt_pdv_p, inString, inCount);
/*/XXX060227 - start
            printf ("%d %d  ", inCount, retVal);
            for (i = 0; i < inCount; i++)
                printf ("%2X ", inString[i]);
//                printf ("%s%2X ", inString[i], inString[i]);
            printf ("\n");
//XXX060227 - end */

//            printf ("5");  //XXX060228

            if (retVal == 0)
            {
                HostDiagSendStrngNoParm("Error: no response from CamLTT 2");
                printf ("Error: no response from CamLTT 2\n");  //XXX
            }
            else if (retVal != inCount)
            {
                HostDiagSendStrngIntParm("Error from pdv_serial_read() in CamLTT: ", retVal);
                printf ("Error from pdv_serial_read() in CamLTT: %d\n", retVal);  //XXX
            }
            else if (inString[0] != ACK)
            {
                HostDiagSendStrngIntParm("Error: no ACK from CamLTT: ", retVal);
                printf ("Error: no ACK from CamLTT %d\n", retVal);  //XXX
            }
            else if (retVal > 2)
            {
                inString[retVal - 2] = '\0';
                sprintf(errMsg, "Notification: msg from CamLTT: %s", &inString[1]);
                HostDiagSendStrngNoParm(errMsg);
                printf ("%s\n", errMsg);  //XXX
            }
        }
    }

//    indicate that interrupts should no longer be ignored
    g_pCamLTTPtr->intrIgnore = false;  // XXXINTIG

//    printf ("6\n");  //XXX060228
}
