/*******************************************************************************
 *
 *  COPYRIGHT (C) 2008
 *  LAWRENCE LIVERMORE NATIONAL LABORATORY
 *  ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITED
 *
 ******************************************************************************/
/*!
 *
 *  @file WFScamMngr.c
 *
 *  @brief
 *  Interface, via the SRT side, between the HRT and the WFS Camera.
 *
 *  @b DESCRIPTION:
 *  This component contains functions to provide the interface, via the SRT side,
 *  between the HRT and the WFS Camera, including setup, initialization, operating
 *  parameter changes (e.g., frame rate), and status queries.  This indirect interface
 *  needs to be used because the camera driver is not strictly RTLinux compatible.
 *  The complimentary file on the HRT side is 'WFSCam.c'.
 *  @par
 *
 *  NOTE that direct HRT camera control would be preferable and will be used, if it
 *  becomes available.
 *  @par
 *
 *  @b AUTHOR:
 *  SMJ & DWP       DATE: 12/17/2008
 *
 *  @b FUNCTION NAMES:
 *  -# @c WFScamMngr_ProcCmd()
 *  -# @c WFScamMngr_StartCam()
 *  -# @c WFScamMngr_StopCam()
 *  -# @c WFScamMngr_TotalBufferSize()
 *  -# @c WFScamMngr_SendToCam()
 *
 *
 *  @see hard/WFSCam.c
 *
 *  @b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
//dwp100318 #include <sys/user.h>  // this apparently has PAGE_SIZE !!
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "WFSCam.h"

#include "gpAoHrtCent.h"
#include "WFScamMngr.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoHrtParse.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
// #include "gpAoHrtSysCon.h"

#include "fitsio.h"

#include "hostIConCentral.h"

#if ( CAM_TYPE == WFS_REAL_GPI )
#include "edtinc.h"
#endif  /* CAM_TYPE */

// Debugging

#ifdef VERBOSE_DEBUGGING
#define dbprintf(arglist)   printf arglist
#else   /* VERBOSE_DEBUGGING */
#define dbprintf(arglist)   /* as nothing */
#endif  /* VERBOSE_DEBUGGING */

// Defines

#define ACK 6  // acknowledge from the SciMeasure Camera

// Local Variables

#if ( CAM_TYPE == WFS_REAL_GPI )
static PdvDev       *wfs_pdv_p = NULL;  // pointer for the WFS PDV/EDT device
static Dependent    *dd_p = NULL;
static Edtinfo      edtinfo;
#endif  /* CAM_TYPE */

static int          crntFrameRateNdx = 0;  // current index into the following arrays

#define NFRplus1 (NUM_FRAME_RATES+1)

// WARNING: LegalFrameRates[] is defined in both WFSCam.c (for the HRT) and gpAoSrtCamMngr.c (for the SRT)
#if ( NUM_ROWS_IN_CAM_ACTUAL == 86 )
const short     LegalFrameRates[NFRplus1] = { 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator -- 50 fps isn't currently included !!
#else   /* NUM_ROWS_IN_CAM_ACTUAL */
// IMPORTANT: note that 1500 and 2000 fps aren't actually that fast in 128x128 mode, they're just a REP of 0 for testing
//XXX put this back after testing!  const short LegalFrameRates[] = { 250, 500, 1000, 1500,   -1, -1 };  // -1 is the terminator
#if (EXAOTEST == 1)  // for testing on exaotest computer at LLNL -- include EXAO=1 when running Makefile
const short     LegalFrameRates[NFRplus1] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator     // mrr
#else   /* EXAOTEST */
//XXXdwp110615 const short      LegalFrameRates[NFRplus1] = { 250, 500, 1000, 1500, 2000, -1 };  // -1 is the terminator
const short     LegalFrameRates[NFRplus1] = { 50, 100, 250, 500, 1000, 1500, -1 };  // -1 is the terminator     // mrr
#endif  /* EXAOTEST */
#endif  /* NUM_ROWS_IN_CAM_ACTUAL */

/*
The SciMeasure sequences are as follows:
Frame   Frame   Total     Reqd  Min              Addl   Addl    Actual  Actual
Rate    Period  Steps/    Prog  Frame    Integ  Integ   Steps/  Frame            Error
(Hz)    (us)    Frame     No    Steps    Steps  Incs    Frame   Rate(Hz)          (%)
   50   20000   1000000    0    210000  790000  7900    1000000   50.000000      0.000
  100   10000    500000    0    210000  290000  2900     500000  100.000000      0.000
  250    4000    200000    1    105350   94650  1893     200000  250.000000      0.000
  500    2000    100000    2     75852   24148   671     100008  500.000000      0.000
 1000    1000     50000    3     45927    4073   194      50001  999.980000     -0.002
 1500     667     33333    5     32805     528    35      33330 1500.150015     +0.010
*/

#if ( NUM_ROWS_IN_CAM_ACTUAL == 86 )
//                          Frame rates:                                  100    250    500   1000   1500
//static const short          cam_WFS_configProgs[NUM_FRAME_RATES]     = {     0,     1,     2,     3,     5    };  // configuration programs for frame rates        / mrr
//static const unsigned short cam_WFS_configRepCounts[NUM_FRAME_RATES] = {  2900,  1893,   671,   194,    35    };  // and rep counts (or, accumulator values in SMC terminology)      // mrr
static const short          cam_WFS_configProgs[NUM_FRAME_RATES]     = {    0,     0,     1,     2,     3,     5};  // configuration programs for frame rates  // mrr
static const unsigned short cam_WFS_configRepCounts[NUM_FRAME_RATES] = { 7900,  2900,  1893,   671,   194,    35};  // and rep counts (or, accumulator values in SMC terminology)  // mrr
#else   /* NUM_ROWS_IN_CAM_ACTUAL */
static const short          cam_WFS_configProgs[NUM_FRAME_RATES]     = {    0,     0,     1,     2,     3,     5};  // configuration programs for frame rates  // mrr
static const unsigned short cam_WFS_configRepCounts[NUM_FRAME_RATES] = { 7900,  2900,  1893,   671,   194,    35};  // and rep counts (or, accumulator values in SMC terminology)  // mrr
#endif  /* NUM_ROWS_IN_CAM_ACTUAL */

// IMPORTANT: these are just place holders for now!
static const float cam_WFS_fps_offset[NUM_FRAME_RATES] = { 0.0,  0.0,  0.0,  0.0,  0.0,  0.0 };  // offset used in frames/sec calculation      // mrr
static const float cam_WFS_fps_factor[NUM_FRAME_RATES] = { 1.0,  1.0,  1.0,  1.0,  1.0,  1.0 };  // factor used in frames/sec calculation      // mrr

// Local Function Prototypes

static void WFScamMngr_SendToCam (char outString[], char inString[]);

static int readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType );


/*!
 *******************************************************************************
 *
 *  @fn WFScamMngr_ProcCmd( short cmd, int param )
 *
 *  @brief
 *  Process commands originating in the HRT Camera control component (File: WFSCam.c).
 *
 *  @b DESCRIPTION:
 *  Camera commands are under SRT software control because the current RTLinux camera driver
 *  supports the high-speed camera interface; but, it does not support issuing commands.
 *  So, commands are passed from the HRT Camera control component and, then, this unit
 *  communicates the command to the camera hardware.
 *  @par Current list of commands:
 *  @par
 *      - eWFSInitialize1
 *      - eWFSInitialize2
 *      - eWFSSetRep : Set REP.
 *      - eWFSSetAtten : Set ATTEN.
 *      - eWFSReadRep : Read and report the REP as a frame rate.
 *      - eWFSReadAtten : Read and report the attenuation as a gain.
 *
 *
 *  @b Usage: WFScamMngr_ProcCmd( cmd, cmdParam );
 *
 *  @param[in] cmd (short):  Cmd from HRT side that needs to be passed to the h/w thru the SRT
 *  @param[in] param (int): Parameter value (use depends on the command) for this particular command
 *
 *  @return
 *  None.
 *
 *  @exception exceptionFileError
 *
 *  @par    @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void WFScamMngr_ProcCmd (short cmd, int param)
{
#   if ( CAM_TYPE == WFS_REAL_GPI )
    short   i;
    short   Gain;
    int     Prog, Count;
    int     doNotCare, temperatureCode;
    int     retVal, Atten;
    float   ccdTemperature;
    char    textBuf[32], inString[64];
#   endif   /* CAM_TYPE */
    float   FrameRate;

    // switch on command ID corresponding to command index
    switch (cmd) {
    case eWFSInitialize1:

        LogNotice("WFScamMngr_ProcCmd eWFSInitialize1");
        srtWFScamDataPtr->stateSRT = 1;
        //LogNotice("WFSsrtstate1 H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
#       if ( CAM_TYPE == WFS_REAL_GPI )
        if (wfs_pdv_p != NULL) {
#           if ( NUM_ROWS_IN_CAM_ACTUAL == 86 ) // Why is this here? both option look the same?
            // read the config file
            if (pdv_readcfg(EDTCFGDIR "/CCID66_160x160_pe8.cfg", dd_p, &edtinfo) < 0) {
                LogErr("WFScamMngr_ProcCmd eWFSInitialize1 pdv_readcfg a failed");
            }
             // init the cam
            if (pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, EDTCFGDIR "/CCID66_160x160_pe8.cfg", NULL, 0) < 0) {
                LogErr("WFScamMngr_ProcCmd eWFSInitialize1 pdv_initcam a failed");
            }
#           else   /* NUM_ROWS_IN_CAM_ACTUAL */
            // read the config file
            if (pdv_readcfg(EDTCFGDIR "/CCID66_160x160_pe8.cfg", dd_p, &edtinfo) < 0) {
                 LogErr("WFScamMngr_ProcCmd eWFSInitialize1 pdv_readcfg b failed");
            }
            // init the cam
            if (pdv_initcam(wfs_pdv_p, dd_p, WFS_UNIT_NUM, &edtinfo, EDTCFGDIR "/CCID66_160x160_pe8.cfg", NULL, 0) < 0) {
                LogErr("WFScamMngr_ProcCmd eWFSInitialize1 pdv_initcam b failed");
            }
#           endif  /* NUM_ROWS_IN_CAM_ACTUAL */

        }
#       endif  /* CAM_TYPE */
        srtWFScamDataPtr->stateSRT = 2;
        //LogNotice("WFSsrtstate2 H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
        srtWFScamDataPtr->Init1DrinkMe  = 0xC0FFEE8D;
        break;  /* from case eWFSInitialize1 */

    case eWFSInitialize2:
        LogNotice("WFScamMngr_ProcCmd eWFSInitialize2");
        srtWFScamDataPtr->stateSRT = 3;
        //LogNotice("WFSsrtstate3 H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
#       if ( CAM_TYPE == WFS_REAL_GPI )
        if (wfs_pdv_p != NULL) {

#           if (RTL_SYSTEM != 0)
            srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;  f\n"));

            LogNotice("   WFSInit2 ring buffers");
            srtWFScamDataPtr->stateSRT = 4;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);

            // need to do this to make driver happy
            if ((pdv_multibuf(wfs_pdv_p, WFS_BUFFERS)) != 0) {
                printf("srt eWFSInitialize2 failed to configure EDT ring buffers\n");
                HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (WFScamMngr_ProcCmd) failed to configure EDT ring buffers");
                LogErr("WFScamMngr_ProcCmd eWFSInitialize2 pdv_multibuf failed");
            }
            srtWFScamDataPtr->stateSRT = 5;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            sleep(1);

            LogNotice("   WFSInit2 turn off cam 1st");
            srtWFScamDataPtr->stateSRT = 6;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            WFScamMngr_SendToCam ("@SEQ 0", NULL);  // make sure SciMeasure sequencing is off
            srtWFScamDataPtr->stateSRT = 7;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            usleep(10000);

            srtWFScamDataPtr->stateSRT = 8;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 turn off cam 2nd");
            WFScamMngr_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
            srtWFScamDataPtr->stateSRT = 9;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            usleep(10000);

            srtWFScamDataPtr->stateSRT = 10;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 turn off cam 3rd");
            WFScamMngr_SendToCam ("@SEQ 0", NULL);  // camera seems to miss first 2 of these at startup
             srtWFScamDataPtr->stateSRT = 11;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            usleep(10000);

            srtWFScamDataPtr->stateSRT = 12;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 stop images");
            pdv_start_images(wfs_pdv_p, 1);     // stop the free-run acquistion WFSInitialize2
            srtWFScamDataPtr->stateSRT = 13;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            usleep(10000);
#           endif   /* RTL_SYSTEM */

            srtWFScamDataPtr->stateSRT = 14;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 command camera to recall program");
            sprintf (textBuf, "@RCL %d", cam_WFS_configProgs[0]);
            WFScamMngr_SendToCam (textBuf, NULL);   // set the camera program
            srtWFScamDataPtr->stateSRT = 15;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            usleep(1000000);

            srtWFScamDataPtr->stateSRT = 16;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 check temp");
            sprintf (textBuf, "@TMP 69");           // make sure the CCD temperature is -10 C (the setting value is
            WFScamMngr_SendToCam (textBuf, NULL);   // > unitless and was determined empirically)

            srtWFScamDataPtr->stateSRT = 17;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 configRepCounts");
            sprintf (textBuf, "@REP %d", cam_WFS_configRepCounts[0]);  // this will quickly be over-ridden by eWFSSetRep
            WFScamMngr_SendToCam (textBuf, NULL);

            srtWFScamDataPtr->stateSRT = 18;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            LogNotice("   WFSInit2 command camera to start sending images");
            // this probably expects HRT WFSCam() main loop is processing interrupts
            WFScamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera

            srtWFScamDataPtr->stateSRT = 19;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);

#           if (RTL_SYSTEM != 0)
            LogNotice("   WFSInit2 tell pdv API to acquire images");
            // this probably expects HRT WFSCam() main loop is processing interrupts
            pdv_start_images(wfs_pdv_p, 0);     // start the free-run acquistion WFSInitialize2

            // tell HRT to (re)get the kernel pages for the ring buffers
            srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;  a\n"));
#           endif  /* RTL_SYSTEM */


            LogNotice("   WFSInit2 complete");
            HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'set wfscami' complete");
            srtWFScamDataPtr->stateSRT = 20;
            //LogNotice("WFSsrtstate H=%d S=%d", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT);
            dbprintf(("srtstate H=%d S=%d\n", srtWFScamDataPtr->stateHRT, srtWFScamDataPtr->stateSRT));
        }
#       endif  /* CAM_TYPE */
        srtWFScamDataPtr->Init2DrinkMe     = 0xC0FFEE8D;
        break;  /* from case eWFSInitialize2 */

    case eWFSSetRep:
        LogNotice("WFScamMngr_ProcCmd eWFSSetRep");
#       if ( CAM_TYPE != WFS_REAL_GPI )
        // mrr                  AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamRate, 0, GPI_STATE_IDLE, "" );
#       else   /* CAM_TYPE */
        if (wfs_pdv_p != NULL) {
            float ccdTempKelvins = 0.0;

#           if (RTL_SYSTEM != 0)
            srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;  b\n"));

            WFScamMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
            usleep(10000);
            pdv_start_images(wfs_pdv_p, 1);     // stop the free-run acquistion WFSSetRep
            usleep(10000);
#           endif  /* RTL_SYSTEM */

            // find a legal camera rate ('param' is the desired frame rate) and then set camera operating parameters
            crntFrameRateNdx = 0;
            while ((param > LegalFrameRates[crntFrameRateNdx]) && (LegalFrameRates[crntFrameRateNdx + 1] != -1))
                crntFrameRateNdx++;

            sprintf (textBuf, "@RCL %d", cam_WFS_configProgs[crntFrameRateNdx]);
            WFScamMngr_SendToCam (textBuf, NULL);   // set the camera program
            usleep(1000000);

            // set rep counts (or, accumulator values in SMC terminology)
            sprintf (textBuf, "@REP %d", cam_WFS_configRepCounts[crntFrameRateNdx]);
            WFScamMngr_SendToCam (textBuf, NULL);
            usleep(1000000);

            // query the temperature
            WFScamMngr_SendToCam ("@TMP?", inString);
            temperatureCode = -1;
            retVal = sscanf(&inString[1], "@TMP! #0:%d; #1:%d;", &doNotCare, &temperatureCode);
            if ((temperatureCode < 0) || (temperatureCode > 255))
                HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (WFScamMngr_ProcCmd) invalid ccd temperature code: ", temperatureCode);
            else {
                ccdTemperature = ccdTemperatureLUT[temperatureCode];  // ccdTemperature will be in degrees C
                ccdTempKelvins = ccdTemperature + 273.15;
                HostDiagSendStrngFltParm(eDebugLevelMin, "INFO (WFScamMngr_ProcCmd) ccd temperature (degrees C): ", ccdTemperature);
            }

            // test CCD temperature limits for TLC -- most TLC errors have persistence counters; but, because the CCD temp
            //           > should never be out of range (and because it's tested so infrequently), being out of range once is enough
            if (ccdTempKelvins < srtFileDataShmPtr->rangesWfsCcdTemp[1]) {
                if (ccdTempKelvins < srtFileDataShmPtr->rangesWfsCcdTemp[0]) {
                    srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrWfsCCD] = eTlcErrLowLow;
                    srtFileDataShmPtr->tlcErrStatArray[eTlcErrWfsCCD] = eTlcErrLowLow;
                } else {
                    srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrWfsCCD] = eTlcErrLow;
                    srtFileDataShmPtr->tlcErrStatArray[eTlcErrWfsCCD] = eTlcErrLow;
                }
            } else if (ccdTempKelvins > srtFileDataShmPtr->rangesWfsCcdTemp[2]) {
                if (ccdTempKelvins > srtFileDataShmPtr->rangesWfsCcdTemp[3]) {
                    srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrWfsCCD] = eTlcErrHighHigh;
                    srtFileDataShmPtr->tlcErrStatArray[eTlcErrWfsCCD] = eTlcErrHighHigh;
                } else {
                    srtFileDataShmPtr->tlcErrStatArraySticky[eTlcErrWfsCCD] = eTlcErrHigh;
                    srtFileDataShmPtr->tlcErrStatArray[eTlcErrWfsCCD] = eTlcErrHigh;
                }
            } else
                srtFileDataShmPtr->tlcErrStatArray[eTlcErrWfsCCD] = eTlcErrNone;
            srtFileDataShmPtr->tlcErrCCDTempKelvins = ccdTempKelvins;

#           if (RTL_SYSTEM != 0)
            // need to do this to make driver happy
            if ((pdv_multibuf(wfs_pdv_p, WFS_BUFFERS)) != 0)
            {
                printf("srt eWFSSetRep failed to configure EDT ring buffers\n");
                HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (WFScamMngr_ProcCmd) failed to configure EDT ring buffers");
            }
            usleep(10000);

            // this probably expects HRT WFSCam() main loop is processing interrupts
            WFScamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera

            // this probably expects HRT WFSCam() main loop is processing interrupts
            pdv_start_images(wfs_pdv_p, 0);     // start the free-run acquistion WFSSetRep

            // tell HRT to (re)get the kernel pages for the ring buffers
            srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;   c\n"));
#           endif  /* RTL_SYSTEM */

            HostDiagSendStrngIntParm(eDebugLevelMin, "INFO 'setCamRate' to ", LegalFrameRates[crntFrameRateNdx]);
            // mrr    AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamRate, 0, GPI_STATE_IDLE, "" );
        }
#       endif  /* CAM_TYPE */

        // RESPONSE setCamRate SUCCESS - actual rate may not match requested rate and be successful (include CCD temp ?)
        g_pCCentral->queUpdateSetCamRate(CKTLInt::UPDATED, LegalFrameRates[crntFrameRateNdx]);

        srtWFScamDataPtr->SetRepDrinkMe    = 0xC0FFEE8D;

        break;  /* from case eWFSSetRep */

    case eWFSSetAtten:
        LogNotice("WFScamMngr_ProcCmd eWFSSetAtten");
#       if ( CAM_TYPE == WFS_REAL_GPI )
        if (wfs_pdv_p != NULL) {
#           if (RTL_SYSTEM != 0)
            srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatNotInitted;   d\n"));

            WFScamMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
            pdv_start_images(wfs_pdv_p, 1);     // stop the free-run acquistion WFSSetAtten
#           endif  /* RTL_SYSTEM */

            sprintf (textBuf, "@AAM %d", param);    // param is the attenuation for all modules
            WFScamMngr_SendToCam (textBuf, NULL);

            HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'setCamGain' complete");
            // mrr      AOC_SRT_GMB_WRITE_CMD_STATUS( eCmdCamGain, 0, GPI_STATE_IDLE, "" );

#           if (RTL_SYSTEM != 0)
            // need to do this to make driver happy
            if ((pdv_multibuf(wfs_pdv_p, WFS_BUFFERS)) != 0)
            {
                printf("srt eWFSSetAtten failed to configure EDT ring buffers\n");
                HostDiagSendStrngNoParm(eDebugLevelWarn, "WARN (WFScamMngr_ProcCmd) failed to configure EDT ring buffers");
            }
            usleep(10000);

            // this probably expects HRT WFSCam() main loop is processing interrupts
            WFScamMngr_SendToCam ("@SEQ 1", NULL);  // start sending data from the camera

            // this probably expects HRT WFSCam() main loop is processing interrupts
            pdv_start_images(wfs_pdv_p, 0);     // start the free-run acquistion WFSSetAtten

            // tell HRT to (re)get the kernel pages for the ring buffers
            srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;
            dbprintf(("srt set srtWFScamDataPtr->edtBufStat = eEdtBufStatReadNew;    e\n"));

#           endif  /* RTL_SYSTEM */
        }
#       endif  /* CAM_TYPE */
        srtWFScamDataPtr->SetAttenDrinkMe  = 0xC0FFEE8D;
        break;  /* from case eWFSSetAtten */

    // NOTE that neither eWFSReadRep nor eWFSReadAtten are used at present
    case eWFSReadRep:  // read and report the REP as a frame rate
        LogNotice("WFScamMngr_ProcCmd eReadRep");
#       if ( CAM_TYPE == WFS_REAL_GPI )
        if (wfs_pdv_p != NULL) {
            WFScamMngr_SendToCam ("@PRG?", inString);
            retVal = sscanf (&inString[7], "%x", &Prog);  // reply format: "ACK@PRG! n" , where ACK is 0x6 and n is the program setting

            WFScamMngr_SendToCam ("@REP?", inString);
            retVal = sscanf (&inString[10], "%x", &Count);  // reply format: "ACK@REP! $00hhhh" , where ACK is 0x6 and hhhh is the hex rep count

            i = 0;
            while ((Prog != cam_WFS_configProgs[i]) && (LegalFrameRates[i] != -1))
                i++;
            while ((Count != cam_WFS_configRepCounts[i]) && (LegalFrameRates[i] != -1))
                i++;

            if (LegalFrameRates[i] == -1)
                HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (eWFSReadRep) invalid Prog or Count");
            else {
                FrameRate = 1.0 / ((cam_WFS_fps_factor[i] * Count) + cam_WFS_fps_offset[i]);
                HostDiagSendStrngFltParm(eDebugLevelMin, "INFO -- WFS camera rate: ", FrameRate);
                HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamRate' complete");
                // TODO-RESP GetCamRate
            }
        }
#       else  // simulated
        FrameRate = LegalFrameRates[ srtFileDataShmPtr->wfsCamFrameRate ];
        HostDiagSendStrngFltParm(eDebugLevelMin, "INFO -- WFS camera rate: ", FrameRate);
        HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamRate' complete");
#       endif  /* CAM_TYPE */
        break;  /* from case eWFSReadRep */

    // NOTE that neither eWFSReadRep nor eWFSReadAtten are used at present
    case eWFSReadAtten:             // read and report the attenuation as a gain
        LogNotice("WFScamMngr_ProcCmd eWFSReadAtten");
#       if ( CAM_TYPE == WFS_REAL_GPI )
        if (wfs_pdv_p != NULL) {
            WFScamMngr_SendToCam ("@AIM? #0", inString);
            retVal = sscanf (&inString[10], "%d", &Atten);  // reply format: "ACK@AIM! #0:V", where ACK is 0x6 and V is the atten value
            Gain = 3 - Atten;

            HostDiagSendStrngIntParm(eDebugLevelMin, "INFO -- WFS camGain: ", Gain);
            HostDiagSendStrngNoParm(eDebugLevelMin, "INFO 'readCamGain' complete");
        }
#       endif  /* CAM_TYPE */
        break;  /* from case eWFSReadAtten */

    default:
        HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (WFScamMngr_ProcCmd) unexpected command in cam_WFS_Mngr: ", cmd);
        break;  /* from case default */
    }
} /* end WFScamMngr_ProcCmd() */


/*!
 *******************************************************************************
 *
 *  @fn WFScamMngr_StartCam( void )
 *
 *  @brief
 *  Initialize the WFS Camera
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to initialize the WFS Cam driver and set up the shared
 *  memory for the DMA ring buffers.  The memory size is provided from the HRT in
 *  cam_WFS.
 *  @par
 *
 *  - Request shared memory size from HRT Cam module.
 *  - Open/allocate shared memory
 *  - Init WFS Camera Interface
 *  @par
 *
 *  @b Usage:
 *  WFScamMngr_StartCam( );
 *
 *  @b Param None.
 *
 *  @return
 *  0 = fail
 *  1 = success
 *
 *  @exception
 *  Invalid fileCmd or problem opening, reading, shared memory.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

int WFScamMngr_StartCam(void)
{
    // Usually called during startup of SRT
    // by CenterThread(main) in hostIConCentral.C
    // MUST be called before HRT WFSCam_IntrInit() in WFSCam.c
    LogInfo("WFScamMngr_StartCam");
#   if (CAM_TYPE == WFS_REAL_GPI)
    char devname[128];

    // open the edt interface (EDT_INTERFACE is defined in edtdef.h (included via edtinc.h))
    strcpy(devname, EDT_INTERFACE);
    HostDiagSendStrngIntParm(eDebugLevelMin,
    "INFO (WFScamMngr_StartCam) Opening WFScam EDTpdv device, unit: ", WFS_UNIT_NUM);
    dbprintf(("WFScamMngr_StartCam() opening pdv unit %d channel %d\n", WFS_UNIT_NUM, 0));

    wfs_pdv_p   = pdv_open_channel(devname, WFS_UNIT_NUM, 0 /* channel */);

    if (wfs_pdv_p == NULL)
    {
        HostDiagSendStrngIntParm(eDebugLevelErr,
        "ERR (WFScamMngr_StartCam) pdv_open failed, unit:", WFS_UNIT_NUM);
        return 0;
    }
    dd_p = pdv_alloc_dependent();

#   if ( RTL_SYSTEM == 0 )
    WFS_dev = wfs_pdv_p;  // this will only work on the user-side
#   endif   /* RTL_SYSTEM */
#   endif   /* CAM_TYPE */
    return 1;
} /* end WFScamMngr_StartCam() */


/*!
 *******************************************************************************
 *
 *  @fn WFScamMngr_StopCam()
 *
 *  @brief
 *  Initialize the WFS Camera
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to close the EDT driver and unmap the shared
 *  memory for the DMA ring buffers.
 *
 *  @b Usage:
 *  WFScamMngr_StopCam( );
 *
 *  @b Param None.
 *
 *  @return
 *  None.
 *
 *  @exception
 *  Invalid fileCmd or problem opening, reading, shared memory.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

void WFScamMngr_StopCam(void)
{
    LogInfo("WFScamMngr_StopCam");
#   if (CAM_TYPE == WFS_REAL_GPI)
    if (wfs_pdv_p != NULL)
    {
        WFScamMngr_SendToCam ("@SEQ 0", NULL);  // stop sending data from the camera
#       if (RTL_SYSTEM != 0)
        pdv_start_images(wfs_pdv_p, 1);         // stop the free-run acquistion
#       endif  /* RTL_SYSTEM */
        if (pdv_close(wfs_pdv_p) != 0)
        {
            LogErr("WFScamMngr_StopCam cannot close WFS camera");
        }
    }
#   endif  /* CAM_TYPE */
}


/*!
 *******************************************************************************
 *
 *  @fn WFScamMngr_SendToCam( char outString[], char inString[] )
 *
 *  @brief
 *  Send an ASCII command to the camera and read the response.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to send an ASCII command to the camera and read the response.
 *
 *  @b Usage:
 *  WFScamMngr_SendToCam( outString, inString )
 *
 *  @param[in] inString (char[]): The command string to send to camera
 *  @param[out] outString (char[]): A char array to put the camera response into, if any, or NULL
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 *******************************************************************************
 */

#define LOCAL_IN_STRING_LENGTH 256

static void WFScamMngr_SendToCam (char outString[], char inString[])
{
    LogInfo("WFScamMngr_SendToCam");
#   if ( CAM_TYPE == WFS_REAL_GPI )
    int    retVal, inCount;
    char   localInString[LOCAL_IN_STRING_LENGTH], errMsg[128];
    char   tmpText[DIAG_STRING_MAX_LENGTH];
    struct timespec next;

    //  don't bother doing anything, if there's no EDT board or the camera isn't on
    //XXX looks like pdv_cl_camera_connected() isn't working
    //XXXdwp101124  if ((wfs_pdv_p == NULL) || (pdv_cl_camera_connected(wfs_pdv_p) == 0))
    if (wfs_pdv_p == NULL)
    {
        sprintf(tmpText, "INFO (WFScamMngr_SendToCam) wfs_pdv_p is NULL, not sending: %s", outString);
        HostDiagSendStrngNoParm(eDebugLevelMin, tmpText);
        return;
    }

    sprintf(tmpText, "INFO (WFScamMngr_SendToCam) sending to WFS camera: %s", outString);
    HostDiagSendStrngNoParm(eDebugLevelMin, tmpText);
    printf("srt %s\n",tmpText);

    //  set flag to tell HRT that interrupt should be ignored
    // (because we're doing serial comms)
    srtWFScamDataPtr->intrIgnore = true;
    dbprintf(("srt WFScamMngr_SendToCam intrIgnore = true\n"));

    //  make sure the string to be returned is null terminated
    if (inString != NULL)
        inString[0] = '\0';

    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);     /* wait 0.1 s after setting flag */

    //  flush any junk from the read buffer and issue command to camera
    if ((inCount = pdv_serial_wait(wfs_pdv_p, 1, sizeof(localInString) - 1)) != 0)
    {
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator
        pdv_serial_read(wfs_pdv_p, localInString, inCount);
    }
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);     /* wait 0.1 s after flushing buffer */

    retVal = pdv_serial_command(wfs_pdv_p, outString);
    next.tv_sec = 0;
    next.tv_nsec = 100000000;
    nanosleep(&next, NULL);     /* wait 0.1 s after sending serial command */

    //  if an error occurred, report it; otherwise, read the camera response and make sure we got an ACK
    if (retVal != 0)
        HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (WFScamMngr_SendToCam) from pdv_serial_command(): ", retVal);
    else
    {
        if (inString == NULL)
        {
            /* wait up to 1 ms for response to serial command */
            inCount = pdv_serial_wait(wfs_pdv_p, 1000, 1);
        }
        else
        {
            /* wait up to 2 ms for response to serial command */
            inCount = pdv_serial_wait(wfs_pdv_p, 2000, LOCAL_IN_STRING_LENGTH - 1);  // the '- 1' is to accommodate a null terminator
        }
        if (inCount > (LOCAL_IN_STRING_LENGTH - 1))  // this seems to happen when the camera has been turned off
            inCount = LOCAL_IN_STRING_LENGTH - 1;    // the '- 1' is to accommodate a null terminator

        next.tv_sec = 0;
        next.tv_nsec = 300000000;
        nanosleep(&next, NULL);     /* wait 0.3 s after notice of serial response */

        if (inCount == 0)
            HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (WFScamMngr_SendToCam) no response from CamWFS 1");
        else
        {
            retVal = pdv_serial_read(wfs_pdv_p, localInString, inCount);
            if (retVal == 0)
                HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (WFScamMngr_SendToCam) no response from CamWFS 2");
            else if (retVal != inCount)
                HostDiagSendStrngIntParm(eDebugLevelErr, "ERR (WFScamMngr_SendToCam) from pdv_serial_read(): ", retVal);
            else if (localInString[0] != ACK)
                HostDiagSendStrngNoParm(eDebugLevelErr, "ERR (WFScamMngr_SendToCam) no ACK from CamWFS");
            else if (retVal > 2)
            {
                localInString[retVal - 2] = '\0';
                sprintf(errMsg, "INFO (WFScamMngr_SendToCam) msg of lngth %d from CamWFS: %s", retVal, &localInString[1]);
                HostDiagSendStrngNoParm(eDebugLevelMin, errMsg);
                if (inString != NULL)
                    strcpy(inString, localInString);
            }
        }
    }

    //  set flag to tell HRT that interrupt should NOT be ignored
    srtWFScamDataPtr->intrIgnore = false;
    dbprintf(("srt WFScamMngr_SendToCam  intrIgnore = false\n"));

#   endif  // ( CAM_TYPE == WFS_REAL_GPI )
} /* end WFScamMngr_SendToCam() */


/*!
 *******************************************************************************
 *
 *  @fn WFScamMngr_ReadSimWfsData( char *filename )
 *
 *  @brief
 *  The purpose of this unit is to read a simulated wfs data fits file into shared memory.
 *
 *  @b DESCRIPTION:
 *  The purpose of this unit is to read a simulated wfs data fits file into shared memory
 *  for use in sim mode.
 *
 *  @b Usage:
 *  WFScamMngr_ReadSimWfsData( filename )
 *
 *  @param[in] filename (char[]): name of fits file containing simulated data
 *
 *  @return
 *  None.
 *
 *  @exception
 *  None.
 *
 *  @par @b PRIOR REQUIREMENTS:
 *  None.
 *
 *  @callgraph
 ********************************************************************************/
int
WFScamMngr_ReadSimWfsData( char *fileName )
{
    LogInfo("WFScamMngr_ReadSimWfsData");
//  int         nDim = 3;      // mrr
    int         nDim = 2;
    long        dimSize[10];
    int         dataType = TUSHORT;

    dimSize[0] = NUM_ROWS_IN_CAM_ACTUAL;
    dimSize[1] = NUM_PIX_IN_ROW_ACTUAL;
    dimSize[2] = WFS_BUFFERS;

    printf ( " (WFScamMngr_ReadSimWfsData) attempting to read simulation data from: %s, nDim = %d\n", fileName, nDim);

    if( readFits( fileName, &srtWFScamDataPtr->simDataBuffs[0][0][0], &nDim, dimSize, &dataType )) {
        srtWFScamDataPtr->simFileRead = true;
        printf (" (WFScamMngr_ReadSimWfsData) successful read of simulation data\n");
    } else {
        printf("WARN (WFScamMngr_ReadSimWfsData) File read failed for simulation data\n");
        srtWFScamDataPtr->simFileRead = false;
        return 0;
    }
    return 1;
}


/**********************************************************************************************

UNIT NAME:  readFits()

UNIT DESCRIPTION:
This unit reads a fits file from disk.

UNIT INPUTS:
    filename - name of fits file to read
    data - array into which data should be read (must be preallocated)

UNIT OUTPUTS:
    data - data that was read

UNIT ERROR CODES:
    Returns 1 if OKAY
    Returns 0 if error


**********************************************************************************************/

static int readFits( char *filename, void *pData, int* pNDim, long* pDimSize, int* pDataType )
{
    fitsfile    *fptr;
    int         bits_per_pixel;
    int         status = 0;
    int         nDim = 0;
    long        dimSize[10];
    long        nElem = 0L;
    int         ii;
    long        fpixel[10];

    // check for data
    if( pData == NULL)
        return 0;

    LogNotice(" (WFScamMngr readFits) attempting to open %s. pNDim = %d", filename, *pNDim);

    //  open the file
    fits_open_file(&fptr, filename, READONLY, &status);
    if( status != 0)
    {
        LogWarn("**** %s  (WFScamMngr readFits) failed to open. status = %d", filename, status);
        printf("\n**** %s  (WFScamMngr readFits) failed to open. status = %d\n", filename, status);
        pData = NULL;
        return 0;
    }

    // Not allocating - so check that input values same as file specs.
    fits_get_img_param( fptr, 3, &bits_per_pixel, &nDim, &dimSize[0], &status );
    if( status != 0)
    {
        LogWarn("**** %s  (WFScamMngr readFits) failed param status = %d", filename, status);
        printf("\n********************** %s  (WFScamMngr readFits) failed param status = %d\n\n", filename, status);
        pData = NULL;
        return 0;
    }
    LogNotice(" (WFScamMngr readFits)  sim data bits-per-pixel = %d", bits_per_pixel);
    if( nDim != *pNDim )
    {
        LogWarn("**** %s  (WFScamMngr readFits) failed nDim = %d    pNDim = %d", filename, nDim,  *pNDim);
        printf("\n********************** %s  (WFScamMngr readFits) failed nDim = %d    pNDim = %d\n\n", filename, nDim,  *pNDim);
        pData = NULL;
        fits_close_file( fptr, &status );
        return 0;
    }

    nElem = 1;
    for( ii=0; ii< nDim; ii++)
    {
        fpixel[ii] = 1;
        nElem *= dimSize[ii];
        if( dimSize[ii] != pDimSize[ii])
        {
            LogWarn("**** %s  (WFScamMngr readFits) failed DimSize[ii] = %ld    pDimSize[ii] = %ld    ii = %d", filename, dimSize[ii],  pDimSize[ii], ii );
            printf("\n********************** %s  (WFScamMngr readFits) failed DimSize[ii] = %ld    pDimSize[ii] = %ld    ii = %d\n\n",
                   filename, dimSize[ii],  pDimSize[ii], ii );
            pData = NULL;
            fits_close_file( fptr, &status );
            return 0;
        }
    }

    // All seems happy so read file
    fits_read_pix( fptr, *pDataType, fpixel, nElem, NULL, pData, NULL, &status);

    //  close the file
    fits_close_file(fptr, &status);

    return 1;
}


//**********************************************************************************************************
// * End of file
//**********************************************************************************************************
