/*******************************************************************************
 *
 *      COPYRIGHT (C) 2010
 *      LAWRENCE LIVERMORE NATIONAL LABORATORY
 *      ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *      @file gpAoSrtDispRawCam.c
 *
 *      @version
 *
 *      @brief
 *      This component is a standalone program to display raw camera frames.
 *
 *      @b DESCRIPTION:
 *      This component contains code to display raw camera frames to the monitor.  The
 *      graywin.c example code in the SDL/test directory served as its basis.
 *      @par
 *
 *      @b AUTHOR:
 *      DWP     DATE: 4/27/2010
 *
 *      @b FUNCTION NAMES:
 *      -# @c gpAoSrtDispRawCamCreateScreen()
 *      -# @c gpAoSrtDispRawCamDraw()
 *
 *      @b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h>

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "WFSCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "WFScamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoShmMap1.h"

#include <SDL/SDL.h>

// Configuration Variables

#define NUM_COLORS      256

/**/
#define NUM_ROWS_IN_CAM_TO_DISPLAY NUM_ROWS_IN_CAM_ACTUAL  // displays around the center -- must be the same as NUM_PIX_IN_ROW_TO_DISPLAY (for now)
#define NUM_PIX_IN_ROW_TO_DISPLAY  NUM_PIX_IN_ROW_ACTUAL   // displays around the center -- must be the same as NUM_ROWS_IN_CAM_TO_DISPLAY (for now)
#define PIX_SIZE                   7
/*
#define NUM_ROWS_IN_CAM_TO_DISPLAY NUM_ROWS_IN_CAM_ACTUAL / 4
#define NUM_PIX_IN_ROW_TO_DISPLAY  NUM_PIX_IN_ROW_ACTUAL  / 4
#define PIX_SIZE                                   7 * 4
*/

static const long secDelay = 0;                    // 0 secs
static const long nsecDelay = 100000000L;  // 100 msecs, for a 10 Hz update rate

static const int dispRowStart = (NUM_ROWS_IN_CAM_ACTUAL - NUM_ROWS_IN_CAM_TO_DISPLAY) / 2;
static const int dispRowEnd   = (NUM_ROWS_IN_CAM_ACTUAL - NUM_ROWS_IN_CAM_TO_DISPLAY) / 2 + NUM_ROWS_IN_CAM_TO_DISPLAY;
static const int dispColStart = (NUM_PIX_IN_ROW_ACTUAL  - NUM_PIX_IN_ROW_TO_DISPLAY)  / 2;
static const int dispColEnd   = (NUM_PIX_IN_ROW_ACTUAL  - NUM_PIX_IN_ROW_TO_DISPLAY)  / 2 + NUM_PIX_IN_ROW_TO_DISPLAY;


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;                                                             // mmap file descriptor
static ShmMap1Type *shmMap1Ptr = 0;  // pointer to start of shared memory

static FileDataShmType*         fileDataShmPtr;         // pointer to file data shared memory
static WFScamDataType*          WFScamDataPtr;             // pointer to camera shared memory data
static CentDataType*            centDataPtr;            // pointer to centroid shared memory data
static HrtDataFlowControlType*  dataFlowControlPtr;     // pointer to Data Flow Control variables


// Local Function Prototypes

static void termHandler(int sigNum);


static SDL_Surface *gpAoSrtDispRawCamCreateScreen(Uint16 w, Uint16 h, Uint8 bpp, Uint32 flags)  // mrr
{
	SDL_Surface *screen;
	int i;
	SDL_Color palette[NUM_COLORS];

//      set the video mode
	screen = SDL_SetVideoMode(w, h, bpp, flags);
	if ( screen == NULL )
	{
		printf("Couldn't set display mode: %s\n", SDL_GetError());
		return(NULL);
	}
	printf("Screen is in %s mode\n", (screen->flags & SDL_FULLSCREEN) ? "fullscreen" : "windowed");

//      set a gray colormap from black to white
	for ( i = 0; i < NUM_COLORS; i++ )
	{
		palette[i].r = i * (256 / NUM_COLORS);
		palette[i].g = i * (256 / NUM_COLORS);
		palette[i].b = i * (256 / NUM_COLORS);
	}
	SDL_SetColors(screen, palette, 0, NUM_COLORS);

	return(screen);
}


int main(void)
{
	int   i, j, k, m, n;
	Uint8 tmpData;
	Uint8 *buffer;

	int crntReadBuffNdx;

	struct timespec next, remaining;

	const int        width          = NUM_PIX_IN_ROW_TO_DISPLAY  * PIX_SIZE;
	const int        height         = NUM_ROWS_IN_CAM_TO_DISPLAY * PIX_SIZE;
	const int        bpp            = 8;  // bits per pixel -- any more than 8 makes this really slow
	const Uint32 videoflags = SDL_SWSURFACE;

	static int satFlashCntr = 0, satFlashVal = 0;
	static SDL_Surface *screen = NULL;
	static SDL_Event event;

	unsigned short tmpRaw[NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL];

//      set up signal handlers
	signal(SIGTERM, termHandler);

	printf("\n=======================\nNew dispraw\n=======================\n");
	printf("spAoDispRawCam: Attempting to open shared memory:: Size = %ld = 0x%lx\n", sizeof(ShmMap1Type), sizeof(ShmMap1Type) );
	printf("spAoDispRawCam: shmDataSize: %lx, WFScamDataSize: %lx, centDataSize: %lx, dataControlSize: %lx\n",
		sizeof(FileDataShmType), sizeof(WFScamDataType), sizeof(CentDataType), sizeof(HrtDataFlowControlType));

//      open the shared memory region
//      IMPORTANT: reconData is no longer included in AOC_mmap -- dispRaw doesn't need reconData, so the pointer to
//      > it is not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispRawCamDraw: waiting for mmap open: /dev/AOC_mmap\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispRawCamDraw: waiting for mmap open: ./AOC_mmap\n");
		sleep(1);
	}
#endif

//      get the pointer to the shared memory region
	while (!quit && ((shmMap1Ptr = (ShmMap1Type *)mmap(0, sizeof(ShmMap1Type), PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispRawCamDraw: waiting for mmap: %lx\n", sizeof(ShmMap1Type));
		sleep(1);
	}
	printf( "spAoDispRawCam: Shm start Ptr = %p, sizeof(ShmMap1Type) = 0x%lx\n", shmMap1Ptr, sizeof(ShmMap1Type) );

//      set up the pointers to the global variables
	fileDataShmPtr          = &(shmMap1Ptr->FileDataShm);
	WFScamDataPtr           = &(shmMap1Ptr->WFScamDataShm);
	centDataPtr             = &(shmMap1Ptr->CentDataShm);
	dataFlowControlPtr      = &(shmMap1Ptr->DataFlowControlShm);

//      initialize SDL and exit if we can't
//      set the video mode
	if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
	{
		printf("gpAoSrtDispRawCamDraw: couldn't initialize SDL: %s\n", SDL_GetError());
		return -1;
	}
	else
	{
		screen = gpAoSrtDispRawCamCreateScreen(width, height, bpp, videoflags);
		if ( screen == NULL )
		{
			printf("gpAoSrtDispRawCamDraw: couldn't set video mode\n");
			return -2;
		}
	}
	printf("bytesPerPix: %d, doubleBuffered: %d\n", screen->format->BytesPerPixel, screen->flags & SDL_DOUBLEBUF);

	if ( SDL_LockSurface(screen) < 0 )      // mrr
	{
		printf("Couldn't lock display surface: %s\n", SDL_GetError());
		return -3;
	}

	unsigned short minVals[NUM_SEGS_IN_CAM];
	unsigned short maxVals[NUM_SEGS_IN_CAM];
	float scaleFactor[NUM_SEGS_IN_CAM];

//      enter a loop, sleeping for nsecDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

		for (i = 0; i < NUM_SEGS_IN_CAM ; i++)
		{
			minVals[i] = USHRT_MAX;
			maxVals[i] = 0;
		}

//              move data out of the rawDispBuffs[][][] quickly, finding mins and maxes to auto-scale by segment
		crntReadBuffNdx = WFScamDataPtr->rawDispReadBuffNdx;
		for ( i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL / 2 ); i++)    // mrr 64
		{
			for ( j = 0; j < (NUM_SEGS_IN_CAM / 2); j++ )   // mrr 8
			{
				for ( k = 0; k < NUM_PIX_IN_SEG_ROW; k++ )      // mrr 16
				{
					int tmpNdx = j * NUM_PIX_IN_SEG_ROW + k;        // mrr 16
					tmpRaw [ i ] [ tmpNdx ] = WFScamDataPtr->rawDispBuffs [ crntReadBuffNdx ] [ i ] [ tmpNdx ];
					if (( tmpRaw [ i ] [ tmpNdx ] > 0 ) && ( tmpRaw [ i ] [ tmpNdx ] < minVals [ j ] ))
						minVals [ j ] = tmpRaw [ i ] [ tmpNdx ];
					if ( tmpRaw [ i ] [ tmpNdx ] > maxVals [ j ] )
						maxVals [ j ] = tmpRaw [ i ] [ tmpNdx ];
				}
			}
		}
		for ( i = NUM_ROWS_IN_CAM_ACTUAL / 2; i < NUM_ROWS_IN_CAM_ACTUAL; i++ )
		{
			for ( j = 0; j < (NUM_SEGS_IN_CAM / 2); j++ )
			{
				for ( k = 0; k < NUM_PIX_IN_SEG_ROW; k++ )
				{
					int tmpNdx = j * NUM_PIX_IN_SEG_ROW + k;
					tmpRaw [ i ] [ tmpNdx ] = WFScamDataPtr->rawDispBuffs [ crntReadBuffNdx ] [ i ] [ tmpNdx ];
					if (( tmpRaw [ i ] [ tmpNdx ] > 0 ) && ( tmpRaw [ i ] [ tmpNdx ] < minVals [ j + ( NUM_SEGS_IN_CAM / 2 ) ] ))
						minVals [ j + ( NUM_SEGS_IN_CAM / 2 ) ] = tmpRaw [ i ] [ tmpNdx ];
					if ( tmpRaw [ i ] [ tmpNdx ] > maxVals [ j + ( NUM_SEGS_IN_CAM / 2 ) ] )
						maxVals [ j + ( NUM_SEGS_IN_CAM / 2 ) ] = tmpRaw [ i ] [ tmpNdx] ;
				}
			}
		}

//              calculate scale factors
		for (i = 0; i < NUM_SEGS_IN_CAM; i++)
		{
			if (minVals[i] == USHRT_MAX)
				minVals[i] = 0;
			if (maxVals[i] == minVals[i])
				scaleFactor[i] = 0.0;
			else
				scaleFactor[i] = 255.0 / ((float)maxVals[i] - (float)minVals[i]);
		}

//              the following is for auto-scaling by frame instead of by segment
		unsigned short minAll = USHRT_MAX;
		unsigned short maxAll = 0;
		for (i = 0; i < NUM_SEGS_IN_CAM; i++)
		{
			if (minVals[i] < minAll)
				minAll = minVals[i];
			if (maxVals[i] > maxAll)
				maxAll = maxVals[i];
		}

	/*  if this is commented out, you're auto-scaling by segment -- start
	//  if it's not commented out, you're auto-scaling by frame
		float scaleAll;
		if (maxAll == minAll)
			scaleAll = 0.0;
		else
			scaleAll = 255.0 / ((float)maxAll - (float)minAll);

		for (i = 0; i < NUM_SEGS_IN_CAM; i++)
		{
			minVals[i] = minAll;
			maxVals[i] = maxAll;
			scaleFactor[i] = scaleAll;
		}
	// if this is commented out, you're auto-scaling by segment -- end */

	/*/ this causes no auto-scale at all -- start
		for (i = 0; i < NUM_SEGS_IN_CAM; i++)
		{
			minVals[i] = 0;
			scaleFactor[i] = 255.0 / 16383.0;
		}
	// this causes no auto-scale at all -- end */

//              this is to flash saturated pixels
		if (++satFlashCntr >= 5)
		{
			satFlashCntr = 0;
			if (satFlashVal == 0)
				satFlashVal = 255;
			else
				satFlashVal = 0;

//                      we're also using satFlashCntr to print out min and max, instead of having another counter
			printf("RRR Frame min: %d, max: %d\n", minAll, maxAll);
		}

//              auto-scale
		int jkNdx;
		Uint8 tmpBuffer[NUM_ROWS_IN_CAM_ACTUAL][NUM_PIX_IN_ROW_ACTUAL];
		for (i = 0; i < (NUM_ROWS_IN_CAM_ACTUAL / 2); i++)
		{
			for (j = 0; j < (NUM_SEGS_IN_CAM / 2); j++)
			{
				for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
				{
					jkNdx = j * NUM_PIX_IN_SEG_ROW + k;
					if (tmpRaw[i][jkNdx] >= 0x3FFF)
						tmpBuffer[i][jkNdx] = satFlashVal;
					else if (tmpRaw[i][jkNdx] == 0)
						tmpBuffer[i][jkNdx] = 0;
					else
						tmpBuffer[i][jkNdx] = (Uint8)((float)(tmpRaw[i][jkNdx] - minVals[j]) * scaleFactor[j]);
				}
			}
		}
		for (i = NUM_ROWS_IN_CAM_ACTUAL / 2; i < NUM_ROWS_IN_CAM_ACTUAL; i++)
		{
			for (j = 0; j < (NUM_SEGS_IN_CAM / 2); j++)
			{
				for (k = 0; k < NUM_PIX_IN_SEG_ROW; k++)
				{
					jkNdx = j * NUM_PIX_IN_SEG_ROW + k;
					if (tmpRaw[i][jkNdx] >= 0x3FFF)
						tmpBuffer[i][jkNdx] = satFlashVal;
					else if (tmpRaw[i][jkNdx] == 0)
						tmpBuffer[i][jkNdx] = 0;
					else
						tmpBuffer[i][jkNdx] = (Uint8)((float)(tmpRaw[i][jkNdx] - minVals[j + (NUM_SEGS_IN_CAM / 2)]) * scaleFactor[j + (NUM_SEGS_IN_CAM / 2)]);
				}
			}
		}

//              spread the data over the display pixels
		buffer = (Uint8 *)screen->pixels;
		int ii = 0, jj;
		for (i = dispRowStart; i < dispRowEnd; i++)
		{
			jj = 0;
			for (j = dispColStart; j < dispColEnd; j++)
			{
				tmpData = tmpBuffer[i][j];
				for (m = 0; m < PIX_SIZE; m++)
				{
					for (n = 0; n < PIX_SIZE; n++)
						buffer[((ii * PIX_SIZE) + m) * (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE) + (jj * PIX_SIZE) + n] = tmpData;
				}
				jj++;
			}
			ii++;
		}

		//      draw in sub-ap grid lines: Note the display is rotated from the data
		//      First draw the rows
		for (i = 1 * PIX_SIZE; i < (NUM_ROWS_IN_CAM_TO_DISPLAY * PIX_SIZE); i += 5 * PIX_SIZE)  // mrr DM       // mrr 64
		{
			for (j = 0 * PIX_SIZE; j < (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE); j++)
				buffer[i * (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE) + j] = 255;
		}
		for (i = 2 * PIX_SIZE; i < (NUM_ROWS_IN_CAM_TO_DISPLAY * PIX_SIZE); i += 5 * PIX_SIZE)  // mrr DM
		{
			for (j = 0 * PIX_SIZE; j < (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE); j++)
				buffer[i * (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE) + j] = 255;
		}

		//      Now draw the columns
		for (i = 0 * PIX_SIZE; i < (NUM_ROWS_IN_CAM_TO_DISPLAY * PIX_SIZE); i++)
		{
			for (j = 1 * PIX_SIZE; j < (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE); j += 5 * PIX_SIZE)   // mrr DM
				buffer[i * (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE) + j] = 255;
		}
		for (i = 0 * PIX_SIZE; i < (NUM_ROWS_IN_CAM_TO_DISPLAY * PIX_SIZE); i++)
		{
			for (j = 2 * PIX_SIZE; j < (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE); j += 5 * PIX_SIZE)   // mrr DM
				buffer[i * (NUM_PIX_IN_ROW_TO_DISPLAY * PIX_SIZE) + j] = 255;
		}

		SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);

//              check for quit
		while(!quit && SDL_PollEvent(&event))   // mrr
		{
			if (event.type == SDL_QUIT)
			{
				printf("dispraw quitting due to SDL term: %d\n", event.type);
				quit = true;
			}
		}
	}

	if (mmfd > 0)
	{
		munmap(shmMap1Ptr, sizeof(ShmMap1Type));
		close(mmfd);
	}
}


static void termHandler(int sigNum)
{
	printf("dispraw quitting due to OS term: %d\n", sigNum);
	quit = true;
}
