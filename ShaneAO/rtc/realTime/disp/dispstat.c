/*******************************************************************************
 *
 *      COPYRIGHT (C) 2011
 *      LAWRENCE LIVERMORE NATIONAL LABORATORY
 *      ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *      @file gpAoDispReconStats.c
 *
 *      @version
 *
 *      @brief
 *      This component is a standalone program to display (print) AO statistics.
 *
 *      @b DESCRIPTION:
 *      This component contains code to display (print) AO statistics to the monitor.
 *      @par
 *
 *      @b AUTHOR:
 *      DWP     DATE: 6/14/2011
 *
 *      @b FUNCTION NAMES:
 *      -# @c gpAoSrtDispReconStatsDraw()
 *
 *      @b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h>

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "WFSCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "WFScamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoShmMap2.h"

// Configuration Variables


static const long secDelay = 1;    // 1 secs
static const long nsecDelay = 0L;  // 0 msecs, for a 1 Hz update rate


// Global Variables


// Local Variables

static boolean quit = false;
static int mmfd = -1;                                                              // mmap file descriptor
static ShmMap2Type *shmMap2Ptr = 0;  // pointer to start of shared memory

static ReconDataType *reconDataPtr;  // pointer to recon shared memory data


// Local Function Prototypes

static void termHandler(int sigNum);

int main(void)
{
	int   i;
	int   crntReadBuffNdx, printCntr = 0;
	float tmpStats[MAX_DISP_STATS];

	struct timespec next, remaining;

//      set up signal handlers
	signal(SIGTERM, termHandler);
	signal(SIGINT, termHandler);

//      determine sizes of global variables
	printf("spAoDispReconStats: Attempting to open shared memory:: Size = %lld = 0x%llx\n", sizeof(ShmMap2Type), sizeof(ShmMap2Type) );

//      open the shared memory region
//      IMPORTANT: reconData is now in its own AOC_mmap2 -- dispftr doesn't need the others, so the pointers to
//      > them are not set up in this file
#if ( RTL_SYSTEM != 0 )
	while (!quit && ((mmfd = open("/dev/AOC_mmap2", O_RDWR)) < 0))
	{
		printf("gpAoSrtDispReconStatsDraw: waiting for mmap open: /dev/AOC_mmap2\n");
		sleep(1);
	}
#else
	while (!quit && ((mmfd = shm_open("./AOC_mmap2", O_RDWR, S_IRUSR | S_IWUSR)) < 0))
	{
		printf("gpAoSrtDispReconStatsDraw: waiting for mmap open: ./AOC_mmap2\n");
		sleep(1);
	}
#endif

//      get the pointer to the shared memory region
	while (!quit && ((shmMap2Ptr = (ShmMap2Type *)mmap(0, sizeof(ShmMap2Type), PROT_READ | PROT_WRITE, MAP_SHARED, mmfd, 0)) == MAP_FAILED))
	{
		printf("gpAoSrtDispReconStatsDraw: waiting for mmap: %llx\n", sizeof(ShmMap2Type));
		sleep(1);
	}
	printf( "gpAoSrtDispReconStatsDraw: Shm start Ptr = %p, sizeof(ShmMap2Type) = 0x%llx\n", shmMap2Ptr, sizeof(ShmMap2Type) );

//      set up the pointers to the global variables
	reconDataPtr = &(shmMap2Ptr->ReconDataShm);

//      enter a loop, sleeping for secDelay each time, until a signal is received (presumably a terminate)
	while (!quit)
	{
		char *allOpen = "OOOO";
		char loopStats[5];

		next.tv_sec = secDelay;
		next.tv_nsec = nsecDelay;
		while( nanosleep( &next, &remaining ) < 0 )
			memcpy( &next, &remaining, sizeof( next ) );

//              move data out of the reconDispStats[] quickly and print it
		crntReadBuffNdx = reconDataPtr->reconDispStatsReadBuffNdx;
		for( i = 0; i < MAX_DISP_STATS; i++)
			tmpStats[i] = reconDataPtr->reconDispStats[crntReadBuffNdx][i];

		strcpy(loopStats, allOpen);
		for( i = 0; i < 4; i++)
		{
			if (tmpStats[i] == eAOLoopClosed)
				loopStats[i] = 'C';
		}

		printf("SSS %5d  Tw/Wf/TT/OFC: %s, Tip(mas): %8.2f, Tilt(mas): %8.2f, avgIntens(dn): %7.1f, wfsSNR: %6.2f, wfsRMS(mas): %8.2f, r0(cm): %5.1f\n",
				printCntr, loopStats, tmpStats[4], tmpStats[5], tmpStats[6], tmpStats[7], tmpStats[8], tmpStats[9]);

		printCntr = (printCntr + 1) % 10000;
	}

	if (mmfd > 0)
	{
		munmap(shmMap2Ptr, sizeof(ShmMap2Type));
		close(mmfd);
	}
}


static void termHandler(int sigNum)
{
	printf("dispstat quitting due to OS term: %d\n", sigNum);
	quit = true;
}
