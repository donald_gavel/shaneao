/*******************************************************************************
 *
 *	COPYRIGHT (C) 2010
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpAoSrtDpio2Drivers.c
 *
 * 	@version
 *
 *	@brief 
 *	This component contains the interface to the user-side version of the dpio2 drivers. 
 *
 *	@b DESCRIPTION:
 *	This component contains the interface to the user-side version of the dpio2 drivers.  
 *	Two dpio2 boards are used in GPI, one to drive the woofer (including the T/T 
 *	stage) and the other to drive the tweeter.  As would be expected, both boards 
 *	are driven with the same driver software.  
 *	@par
 *
 *	This code was intended to be temporary as the AOC is transitioned to GPI hardware,
 *	but before it is transitioned to RTLinux.  But, the code will be left available, 
 *	under compiler switch, to be able to build an all user-side version of the code.
 *	@par
 *
 *	The examples in dpio2-drv-x86-linux-user-guide.pdf were used as the beginning
 *	for this code.  Please see that document for additional information.  
 *
 *	@b AUTHOR:
 *	DWP	DATE: 03/03/2010
 *
 *	@b FUNCTION NAMES:
 *	-# @c gpAoSrtDpio2Init()
 *	-# @c gpAoSrtDpio2LockBuffer()
 *	-# @c gpAoSrtDpio2ConfigureDmaChain()
 *	-# @c gpAoSrtDpio2StartDmaTransfer()
 *	-# @c gpAoSrtDpio2UnlockBuffer()
 *	-# @c gpAoSrtDpio2Cleanup()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#include <stdio.h>      /* printf, ... */
#include <stdlib.h>     /* malloc,.. */
#include <pthread.h>    /* pthread_attr_init ,pthread_create, .... */ 
#include <time.h>       /* nanosleep */
#include <unistd.h>     /* sleep, close, ... */
#include <malloc.h>     /* memalign */
#include <semaphore.h>
#include <time.h>
#include <sys/time.h>

#include "gpAoConstant.h"	// Pick up values for TWT_*, WFR_*, etc...

#if ((TWT_TYPE == TWT_BMC_4K) || (WFR_TYPE == WFR_GPI_121))

#if (CAM_TYPE == WFS_REAL_GPI)
#include "edtinc.h"	// defines PAGE_SIZE
#undef BOOL		// both edtinc.h and dpio2.h define BOOL and UINT
#undef UINT
#endif

#define  VMOSA
#define  VM_LINUX
#include "vmosa.h"
#include "dpio2.h"

#endif

// Don't include gpAoConstant.h until after vmosa.h, as they both define
// compatible but different TRUE and FALSE constants.
#include "gpAoConstant.h"

#include "gpAoStubs.h"
#include "gpAoConstant.h"
#include "WFSCam.h"
#include "gpAoHrtCent.h"
#include "gpAoHrtRecon.h"
#include "gpAoHrtTipTilt.h"
#include "gpAoSrtCamMngr.h"
#include "gpAoSrtFileMngr.h"
#include "gpAoHrtFileData.h"
#include "gpAoSrtHostDiag.h"
#include "gpAoSrtHostICon.h"
#include "gpAoHrtSysCon.h"
#include "gpAoHrtVolts.h"
#include "gpAoSrtDpio2Drivers.h"

// Defines


// Global Variables

unsigned short *dpio2BufferWFR[2];
unsigned short *dpio2BufferTWT[2];


// stubs for global functions -- start
#if ((TWT_TYPE != TWT_BMC_4K) && (WFR_TYPE != WFR_GPI_121))
int  gpAoSrtDpio2Init ( void )
{
	return -1;
}
int  gpAoSrtDpio2StartDmaTransfer (int dpio2DeviceNumber, int desc_num, int buffNdx)
{
	return -1;
}
void gpAoSrtDpio2Cleanup ( void )
{
}
// stubs for global functions -- end
#else

// Local Variables

static DPIO2_INFO_T info;

static DPIO2_DMA dmaInfoWFR;
static DPIO2_DMA dmaInfoTWT;
static DPIO2_DMA *pPageInfoWFR = &dmaInfoWFR;
static DPIO2_DMA *pPageInfoTWT = &dmaInfoTWT;

static sem_t dmaDoneSemaphoreWFR;
static sem_t dmaDoneSemaphoreTWT;


// Local Function Prototypes

static void gpAoSrtDpio2DmaDonerWFR (long pDmaDoneSemaphore);
static void gpAoSrtDpio2DmaDoneTWT (long pDmaDoneSemaphore);
static int gpAoSrtDpio2LockBuffer(int dpio2DeviceNumber, unsigned int lengthInBytes, unsigned short **buffer, DPIO2_DMA **ppPageInfo);
static int gpAoSrtDpio2ConfigureDmaChain(int dpio2DeviceNumber, DPIO2_DMA* pPageInfo, int desc_num);
static void gpAoSrtDpio2UnlockBuffer(int dpio2DeviceNumber, DPIO2_DMA* pPageInfo, unsigned short *buffer);


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2DmaDoneWFR( long pDmaDoneSemaphore )
 *	@fn gpAoSrtDpio2DmaDoneTWT( long pDmaDoneSemaphore )
 *
 *	@brief
 *  These units are invoked by the kernel when a DMA interrupt occurs.
 *
 *	@b DESCRIPTION:
 *  These units are invoked by the kernel when a DMA interrupt occurs.  They 
 *	simply do some post-DMA cleanup.  
 *
 *	@b Usage: N/A, the system calls this, application code should not
 * 
 *	@param[in]	pDmaDoneSemaphore	(long *):	pointer to semaphore
 *
 *	@return 
 *	 None
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	Must be attached with dpio2Ioctl( , DPIO2_CMD_INTERRUPT_CALLBACK_ATTACH, )
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoSrtDpio2DmaDoneWFR (long pDmaDoneSemaphore)
{
	sem_post ((sem_t*)pDmaDoneSemaphore);
}

static void gpAoSrtDpio2DmaDoneTWT (long pDmaDoneSemaphore)
{
	sem_post ((sem_t*)pDmaDoneSemaphore);
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2Init( void )
 *
 *	@brief
 *  This unit initializes the two dpio2 boards.
 *
 *	@b DESCRIPTION:
 *  This unit initializes the two dpio2 boards.  If any error occurs, the 
 *	initialization aborts and neither board can be assumed to be initialized.  
 *
 *	@b Usage: gpAoSrtDpio2Init();
 * 
 *	@param[in] None
 *
 *	@return 
 *	 0 : no error
 *  <0 : any of several errors (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	Two dpio2 boards must be installed in the system.
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoSrtDpio2Init ( void )
{
//	scan for dpio2 devices
	int numOfDpio2Devices = dpio2Scan (&info);
	if (numOfDpio2Devices == ERROR)
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed scan for devices");
		return -1;
	}
	if (numOfDpio2Devices < 2) 
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to find both devices");
		return -2;
	}

	int devno;
	for (devno = 1; devno <= 2; devno++)
	{
//		open the dpio2 device for output
		if (dpio2Open(devno, DPIO2_OUTPUT) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to open, dev ", devno);
			return -(devno * 100 + 0);
		}

//		configure PIO strobes (used for handshaking with Cambridge Innovations box)
		STATUS status = dpio2Ioctl (devno, DPIO2_CMD_PIO1_DIRECTION_SELECT, 1);  /* enable PIO1 as output */
		if (status == OK)
			status = dpio2Ioctl (devno, DPIO2_CMD_PIO1_OUTPUT_VALUE_SET, 1);  /* and set PIO1 to 1 */
		if (status == OK)
			status = dpio2Ioctl (devno, DPIO2_CMD_PIO2_DIRECTION_SELECT, 1);  /* enable PIO2 as output */
		if (status == OK)
			status = dpio2Ioctl (devno, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, 1);  /* and set PIO2 to 1 */
		if (status != OK) 
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to configure PIO strobes, dev ", devno);
			return -(devno * 100 + 1);
		}

//		set up DMA done interrrupts
		DPIO2_INTERRUPT_CALLBACK callbackSetting;
		callbackSetting.condition = DPIO2_INT_COND_DMA_DONE_CHAIN;
		if (devno == dpio2DevnoWFR)
		{
			callbackSetting.pCallbackFunction = (FUNCPTR)gpAoSrtDpio2DmaDoneWFR;
			sem_init (&dmaDoneSemaphoreWFR, 0, 0);
			callbackSetting.argument = (long)&dmaDoneSemaphoreWFR;
		}
		else  // devno == dpio2DevnoTWT
		{
			callbackSetting.pCallbackFunction = (FUNCPTR)gpAoSrtDpio2DmaDoneTWT;
			sem_init (&dmaDoneSemaphoreTWT, 0, 0);
			callbackSetting.argument = (long)&dmaDoneSemaphoreTWT;
		}
		if (dpio2Ioctl(devno, DPIO2_CMD_INTERRUPT_CALLBACK_ATTACH, (long)&callbackSetting) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to attach callback action, dev ", devno);
			return -(devno * 100 + 2);
		}
		if (dpio2Ioctl(devno, DPIO2_CMD_INTERRUPT_ENABLE, DPIO2_INT_COND_DMA_DONE_CHAIN) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to enable interrupt, dev ", devno);
			return -(devno * 100 + 3);
		}

//		enable strobe generation
		int resultingFrequency = dpio2StrobeFreq;
		if (dpio2Ioctl(devno, DPIO2_CMD_STROBE_FREQUENCY_SET, (long)&resultingFrequency) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to program strobe frequency, dev ", devno);
			return -(devno * 100 + 4);
		}
		if (dpio2Ioctl(devno, DPIO2_CMD_STROBE_GENERATION_ENABLE, DPIO2_PROGRAMMABLE_OSCILLATOR) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to enable strobe generation, dev ", devno);
			return -(devno * 100 + 5);
		}

//		make sure the FIFO is flushed
		if (dpio2Ioctl(devno, DPIO2_CMD_FIFO_FLUSH, 0) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to flush buffer, dev ", devno);
			return -(devno * 100 + 7);
		}

//		set for no swap (endian-wise)
		if (dpio2Ioctl(devno, DPIO2_CMD_DATA_SWAP_MODE_SELECT, DPIO2_NO_SWAP) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to set for no swap, dev ", devno);
			return -(devno * 100 + 8);
		}

//		activate the FPDP interface
		if (dpio2Ioctl(devno, DPIO2_CMD_FPDP_ACTIVATION_SELECT, TRUE) != OK)
		{
			SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to activate the FPDP interface, dev ", devno);
			return -(devno * 100 + 9);
		}
	}

//	allocate and align DMA buffers (2 each for the woofer and tweeter)
	dpio2BufferWFR[0] = memalign(0x100, (size_t)sizeInBytesWFR);
	dpio2BufferWFR[1] = memalign(0x100, (size_t)sizeInBytesWFR);
	dpio2BufferTWT[0] = memalign(0x100, (size_t)sizeInBytesTWT);
	dpio2BufferTWT[1] = memalign(0x100, (size_t)sizeInBytesTWT);

	if ((dpio2BufferWFR[0] == NULL) || (dpio2BufferWFR[1] == NULL) || 
		(dpio2BufferTWT[0] == NULL) || (dpio2BufferTWT[1] == NULL))
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to allocate DM buffers");
		return -(devno * 100 + 10);
	}

	memset(dpio2BufferWFR[0], 0, (size_t)sizeInBytesWFR);
	memset(dpio2BufferWFR[1], 0, (size_t)sizeInBytesWFR);
	memset(dpio2BufferTWT[0], 0, (size_t)sizeInBytesTWT);
	memset(dpio2BufferTWT[1], 0, (size_t)sizeInBytesTWT);

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2LockBuffer(int dpio2DeviceNumber, unsigned int lengthInBytes, unsigned short **buffer, DPIO2_DMA **ppPageInfo)
 *
 *	@brief
 *  This unit allocates and locks buffers for dpio2 DMA.
 *
 *	@b DESCRIPTION:
 *  This unit locks buffers for dpio2 DMA.
 *
 *	@b Usage: gpAoSrtDpio2LockBuffer(devno, lengthInBytes, &pBuffer, &pPageInfo);
 * 
 *	@param[in]	dpio2DeviceNumber	(int):					dpio2 device number (1 or 2)
 *	@param[in]	lengthInBytes		(unsigned int):			length of buffer to be allocated in bytes
 *	@param[in]	buffer				(unsigned short **):	pointer to pointer for buffer
 *	@param[in]	ppPageInfo			(DPIO2_DMA **):			pointer to pointer for buffer descriptor table
 *
 *	@return 
 *	 0 : no error
 *  -1 : error (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int gpAoSrtDpio2LockBuffer (int dpio2DeviceNumber, unsigned int lengthInBytes, unsigned short **buffer, DPIO2_DMA **ppPageInfo)
{
	int maxNumPages;

//	determine the maximum number of pages the buffer can be mapped to (one is added at the end of 
//	> the computation to account for the fact that the buffer may not be aligned to a page boundary)
	maxNumPages = ((lengthInBytes + PAGE_SIZE - 1) / PAGE_SIZE) + 1;

//	allocate structure to store information about all the physical pages the buffer maps to
	*ppPageInfo = malloc (sizeof(DPIO2_DMA) + maxNumPages * sizeof(DPIO2_DMA_PAGE));
	if (*ppPageInfo == NULL) 
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to allocate Page Information structure");
		return -1;
	}

//	lock the buffer into physical memory
	(*ppPageInfo)->pUserAddr = *buffer;
	(*ppPageInfo)->dwBytes = lengthInBytes;
	(*ppPageInfo)->dwPages = maxNumPages;
	if (dpio2DMALock (dpio2DeviceNumber, *ppPageInfo) != OK)
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to lock buffer into physical memory");
		free (*ppPageInfo);
		return -1;
	}

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2ConfigureDmaChain(dpio2DeviceNumber, &pPageInfo, desc_num)
 *
 *	@brief
 *  This unit sets up a dpio2 DMA chain.
 *
 *	@b DESCRIPTION:
 *  This unit sets up a dpio2 DMA chain.
 *
 *	@b Usage: gpAoSrtDpio2ConfigureDmaChain(dpio2DeviceNumber, &pPageInfo, desc_num);
 * 
 *	@param[in]	dpio2DeviceNumber	(int):			dpio2 device number (1 or 2)
 *	@param[in]	ppPageInfo			(DPIO2_DMA **):	pointer to pointer for buffer descriptor table
 *	@param[in]	desc_num			(int):			descriptor number (always 0)
 *
 *	@return 
 *	 0 : no error
 *  -1 : error (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static int gpAoSrtDpio2ConfigureDmaChain(int dpio2DeviceNumber, DPIO2_DMA* pPageInfo, int desc_num)
{
	unsigned int   iPage;
	DPIO2_DMA_DESC dmaDescriptor;

	if ((pPageInfo->dwBytes & 0x00000003 ) != 0)
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 DMA page length must be multiple of 4 bytes");
		return -1;
	} 

	if (( pPageInfo->Page[0].pPhysicalAddr & 0x00000003) != 0)
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 PCI address must be aligned to 4 bytes boundary");
		return -1;
	}

	for (iPage = 0; iPage < (pPageInfo->dwPages - 1); iPage++)
	{
		dmaDescriptor.descriptorId        = iPage + desc_num;
		dmaDescriptor.nextDescriptorId    = iPage + desc_num + 1;
		dmaDescriptor.pciAddress       	  = (unsigned int) pPageInfo->Page[iPage].pPhysicalAddr;
		dmaDescriptor.blockSizeInBytes 	  = pPageInfo->Page[iPage].dwBytes;
		dmaDescriptor.lastBlockInChain    = FALSE;
		dmaDescriptor.useD64              = TRUE;
		dmaDescriptor.notEndOfFrame       = FALSE;
		dmaDescriptor.endOfBlockInterrupt = FALSE;
		if (dpio2Ioctl(dpio2DeviceNumber, DPIO2_CMD_DMA_SET_DESC, (long)&dmaDescriptor) != OK)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to set DMA descriptor 1");
			return -1;
		}
	}

	dmaDescriptor.descriptorId        = iPage + desc_num;
	dmaDescriptor.nextDescriptorId    = 0;
	dmaDescriptor.pciAddress          = (unsigned int) pPageInfo->Page[iPage].pPhysicalAddr;
	dmaDescriptor.blockSizeInBytes    = pPageInfo->Page[iPage].dwBytes;
	dmaDescriptor.lastBlockInChain    = TRUE;
	dmaDescriptor.useD64              = TRUE;
	dmaDescriptor.notEndOfFrame       = FALSE;
	dmaDescriptor.endOfBlockInterrupt = TRUE;
	if (dpio2Ioctl(dpio2DeviceNumber, DPIO2_CMD_DMA_SET_DESC, (long)&dmaDescriptor) != OK)
	{
		SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to set DMA descriptor 1");
		return -1;
	}

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2StartDmaTransfer(int dpio2DeviceNumber, int desc_num, int buffNdx)
 *
 *	@brief
 *  This unit initiates a dpio2 DMA transfer.
 *
 *	@b DESCRIPTION:
 *  This unit initiates a dpio2 DMA transfer.  The transfer is always out and is
 *	from the buffer previously associated with the indicated dpio2DeviceNumber.  
 *
 *	@b Usage: gpAoSrtDpio2StartDmaTransfer(dpio2DeviceNumber, desc_num, buffNdx);
 * 
 *	@param[in]	dpio2DeviceNumber	(int):	dpio2 device number (1 or 2)
 *	@param[in]	desc_num			(int):	descriptor number (always 0)
 *	@param[in]	buffNdx				(int):	the index number for the current double buffer (0 or 1)
 *
 *	@return 
 *	 0 : no error
 *  -1 : error (see code)
 * 
 *	@exception See code
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	DMA buffers must be set up.  
 *
 *	@callgraph
 *******************************************************************************
 */

int gpAoSrtDpio2StartDmaTransfer (int dpio2DeviceNumber, int desc_num, int buffNdx)
{
	if (dpio2DeviceNumber == dpio2DevnoWFR)
	{
		if (gpAoSrtDpio2LockBuffer(dpio2DevnoWFR, sizeInBytesWFR, &dpio2BufferWFR[buffNdx], &pPageInfoWFR) < 0)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to allocate and lock woofer buffer");
			return -1;
		}
		if (gpAoSrtDpio2ConfigureDmaChain (dpio2DevnoWFR, pPageInfoWFR, 0) < 0)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to configure woofer DMA chain");
			gpAoSrtDpio2UnlockBuffer (dpio2DeviceNumber, pPageInfoWFR, dpio2BufferWFR[buffNdx]);
			return -1;
		}

// 		lower PIO2 strobe (PIO1 gets left alone)
		if (dpio2Ioctl(dpio2DevnoWFR, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, 0) != OK)  // set PIO2 to 0
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to lower woofer PIO2 strobe");
			return -1;
		}
	}
	else  // dpio2DeviceNumber == dpio2DevnoTWT
	{
		if (gpAoSrtDpio2LockBuffer(dpio2DevnoTWT, sizeInBytesTWT, &dpio2BufferTWT[buffNdx], &pPageInfoTWT) < 0)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to allocate and lock tweeter buffer");
			return -1;
		}
		if (gpAoSrtDpio2ConfigureDmaChain (dpio2DevnoTWT, pPageInfoTWT, 0) < 0)
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to configure tweeter DMA chain");
			gpAoSrtDpio2UnlockBuffer (dpio2DeviceNumber, pPageInfoTWT, dpio2BufferTWT[buffNdx]);
			return -1;
		}

// 		lower PIO2 strobe (PIO1 gets left alone)
		if (dpio2Ioctl(dpio2DevnoTWT, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, 0) != OK)  // set PIO2 to 0
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to lower tweeter PIO2 strobe");
			return -1;
		}
	}

	if (dpio2Ioctl(dpio2DeviceNumber, DPIO2_CMD_DMA_SET_START_DESCRIPTOR, desc_num) != OK)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to set start descriptor, dev ", dpio2DeviceNumber);
		return -1;
	}
	if (dpio2Ioctl(dpio2DeviceNumber, DPIO2_CMD_DMA_START, 0) != OK)
	{
		SysConSendStrngIntParm(eDebugLevelErr, "DPIO2 failed to start DMA transfer, dev ", dpio2DeviceNumber);
		return -1;
	}

//	Note -- this is VERY inefficient, but is okay for user-side code
	if (dpio2DeviceNumber == dpio2DevnoWFR)
	{
		sem_wait (&dmaDoneSemaphoreWFR);

//		raise PIO2 strobe (PIO1 gets left alone) and unlock DMA buffer
		if (dpio2Ioctl(dpio2DevnoWFR, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, 1) != OK)  // set PIO2 to 1
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to raise woofer PIO2 strobe");
			return -1;
		}
		gpAoSrtDpio2UnlockBuffer (dpio2DevnoWFR, pPageInfoWFR, dpio2BufferWFR[buffNdx]);
	}
	else
	{
		sem_wait (&dmaDoneSemaphoreTWT);

//		raise PIO2 strobe (PIO1 gets left alone) and unlock DMA buffer
		if (dpio2Ioctl(dpio2DevnoTWT, DPIO2_CMD_PIO2_OUTPUT_VALUE_SET, 1) != OK)  // set PIO2 to 1
		{
			SysConSendStrngNoParm(eDebugLevelErr, "DPIO2 failed to raise tweeter PIO2 strobe");
			return -1;
		}
		gpAoSrtDpio2UnlockBuffer (dpio2DevnoTWT, pPageInfoTWT, dpio2BufferTWT[buffNdx]);
	}

	return 0;
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2UnlockBuffer(int dpio2DeviceNumber, DPIO2_DMA* pPageInfo, unsigned short *buffer)
 *
 *	@brief
 *  This unit unlocks buffers for dpio2 DMA.
 *
 *	@b DESCRIPTION:
 *  This unit unlocks buffers for dpio2 DMA.
 *
 *	@b Usage: gpAoSrtDpio2UnlockBuffer(devno, &pageInfo, &buffer);
 * 
 *	@param[in]	dpio2DeviceNumber	(int):					dpio2 device number (1 or 2)
 *	@param[in]	pPageInfo	(DPIO2_DMA *):		pointer for buffer descriptor table
 *	@param[in]	buffer		(unsigned short *):	pointer for buffer
 *
 *	@return 
 *	None.
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

static void gpAoSrtDpio2UnlockBuffer (int dpio2DeviceNumber, DPIO2_DMA* pPageInfo, unsigned short *buffer)
{
//	unlock the pages of physical memory that the buffer maps
	dpio2DMAUnlock (dpio2DeviceNumber, pPageInfo);

//	free the page information structure and the buffer
	free (pPageInfo);
}


/*!
 *******************************************************************************
 *
 *	@fn gpAoSrtDpio2Cleanup( void )
 *
 *	@brief
 *  This unit cleans up the two dpio2 boards.
 *
 *	@b DESCRIPTION:
 *  This unit cleans up the two dpio2 boards.
 *
 *	@b Usage: gpAoSrtDpio2Cleanup();
 * 
 *	@param[in] None
 *
 *	@return 
 *	None.
 * 
 *	@exception None
 *
 *	@par	@b PRIOR REQUIREMENTS:
 *	None.
 *
 *	@callgraph
 *******************************************************************************
 */

void gpAoSrtDpio2Cleanup ( void )
{
//	free buffers
	free (dpio2BufferWFR[0]);
	free (dpio2BufferWFR[1]);
	free (dpio2BufferTWT[0]);
	free (dpio2BufferTWT[1]);

//	disable strobes
	dpio2Ioctl (dpio2DevnoWFR, DPIO2_CMD_CLOCKING_ON_BOTH_STROBE_EDGES_SELECT, FALSE);
	dpio2Ioctl (dpio2DevnoTWT, DPIO2_CMD_CLOCKING_ON_BOTH_STROBE_EDGES_SELECT, FALSE);

//	close device   */
	dpio2Close(dpio2DevnoWFR);
	dpio2Close(dpio2DevnoTWT);
}
#endif  // ((TWT_TYPE != TWT_BMC_4K) && (WFR_TYPE != WFR_GPI_121))

