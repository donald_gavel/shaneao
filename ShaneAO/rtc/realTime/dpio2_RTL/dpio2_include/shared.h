/*
 * DPIO2 RT-Linux Shared Header
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#define FRAME_SIZE 4096

#define PACKETSIZE_1024	1024
#define PACKETSIZE_4096 4096

#define RUN_CMD		0
#define STOP_CMD	1
#define POKE_CMD	2
#define MODE_CMD	3
#define CLEAR_CMD	4
#define LOAD_CMD	5
#define TEST_CMD	6
#define RATE_CMD	7
#define STROBE_CMD	8
#define TERM_CMD	9
#define STEP_CMD	10
#define HERR_CMD	11
