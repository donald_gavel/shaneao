/*
 * DPIO2 RT-Linux Header
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#ifndef VMOSA_LINUX_INCLUDED
#define VMOSA_LINUX_INCLUDED

#ifdef __cplusplus
extern "C" {
#endif

#define OK      (0)
#define ERROR   (-1)
#define TRUE    (1)
#define FALSE   (0)
#ifndef NULL
#define NULL  (0)
#endif

/* Typedefs
 */
#ifndef VM_KERNEL /*meh*/
#ifndef BOOL
typedef  int             BOOL;
#endif
#ifndef STATUS
typedef  int             STATUS;
#endif
#ifndef PWOID
typedef  void*           PWOID;
#endif
#ifndef UINT
typedef  unsigned int    UINT;
#endif
#ifndef UINT32
typedef  unsigned int    UINT32;
#endif
#ifndef DWORD
typedef  unsigned long   DWORD;
#endif
#ifndef UINT16
typedef  unsigned short  UINT16;
#endif
#ifndef UINT8
typedef  unsigned char   UINT8;
#endif
#endif

#ifdef __cplusplus 
typedef  int (*FUNCPTR)(...);
#else
typedef  int (*FUNCPTR)(void*);
#endif

#ifdef __cplusplus
}
#endif

#endif /* VMOSA_LINUX_INCLUDED */
