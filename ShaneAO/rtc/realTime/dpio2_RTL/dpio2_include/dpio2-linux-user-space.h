/*
 * DPIO2 RT-Linux Header
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#ifndef DPIO2_LINUX_USER_SPACE_INCLUDED
#define DPIO2_LINUX_USER_SPACE_INCLUDED

#include "vmosa-linux.h"
#include "dpio2-linux.h"

#include "stdio.h"      /* FILE, printf, ... */
#include "pthread.h"    /* pthread_attr_init ,pthread_create, .... */ 
#include "asm/page.h"   /* PAGE_SIZE */
#include "stdlib.h"     /* malloc,.. */
#include "unistd.h"     /* close, ... */
#include "sys/ioctl.h"  /* ioctl */
#include "sys/types.h"  /* socket, open, .. */
#include "sys/stat.h"   /* open, .. */
#include "fcntl.h"      /* open, .. */
#include "sys/mman.h"   /* mmap, munmap, ... */
#include "errno.h"      /* errno */
#include "string.h"     /* memset,.. */
#include "time.h"       /* nanosleep */

#ifdef __cplusplus
extern "C" {
#endif

#define MARK printf ("%s %d  Was Here\n", __FILE__, __LINE__); \
             vmosa_mdelay (100);

#define DB(args) \
       printf ("DB: %s %d:\n", __FILE__, __LINE__); \
       printf args; \
       vmosa_mdelay (100);

#define DPIO2_ERR_MSG(args) \
       printf ("DPIO2: ERROR: %s %d\n", __FILE__, __LINE__); \
       printf ("DPIO2: ERROR: "); \
       printf args;

#define DPIO2_DEVICE_OPEN   1
#define DPIO2_DEVICE_FREE   0

typedef struct dpio2_isr_t
{
  pthread_t           pthread;
  pthread_attr_t      attribute;
  struct sched_param  pri;
  int                 exit_flag;
} dpio2_isr_t;

typedef struct dpio2_pci_adrs_t
{
  void    *ctrl_base;
  size_t  ctrl_mmap_size;
  void    *fifo_base;
  size_t  fifo_mmap_size;

} dpio2_pci_adrs_t;

typedef struct dpio2_callback_t
{
  FUNCPTR func_ptr;
  void    *arg;
} dpio2_callback_t;

typedef struct dpio2_handle_t
{
  int                   devno;
  int                   fd;
  dpio2_pci_adrs_t      user;
  dpio2_callback_t      callback;
  dpio2_pci_info_t      pci;
  dpio2_isr_t           isr;

} dpio2_handle_t;

typedef dpio2_handle_t*  HANDLE;

#ifdef __cplusplus
}
#endif

#endif /* DPIO2_LINUX_USER_SPACE_INCLUDED */
