/*
 * DPIO2 RT-Linux Header
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#ifndef DPIO2_LINUX_INCLUDED
#define DPIO2_LINUX_INCLUDED

#include "vmosa-linux.h"

#if 1
#ifdef __cplusplus
extern "C" {
#endif
#endif

#if 1
#define NO_READ_AFTER_WRITE
#endif

#define IOCTL_CODE  ( 0xAAAA0000 )

#define VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC 		(  1 + IOCTL_CODE )
#define VMOSA_REG_32_BIT_TOGGLE_ATOMIC        		(  2 + IOCTL_CODE )
#define VMOSA_PCI_CONFIG_8_READ               		( 10 + IOCTL_CODE )
#define VMOSA_PCI_CONFIG_8_WRITE              		( 11 + IOCTL_CODE )
#define VMOSA_PCI_CONFIG_16_READ              		( 12 + IOCTL_CODE )
#define DPIO2_SCAN                      		( 20 + IOCTL_CODE )
#define DPIO2_GET_PCI_CONFIG_INFO       		( 21 + IOCTL_CODE )
#define DPIO2_GET_HW_INFO               		( 22 + IOCTL_CODE )
#define DPIO2_DISABLE_ROUTE_INTR_PCI_SAVE_ATOMIC  	( 30 + IOCTL_CODE )
#define DPIO2_ENABLE_ROUTE_INTR_PCI_SAVE          	( 31 + IOCTL_CODE )
#define DPIO2_INTERRUPT_CONNECT         		( 40 + IOCTL_CODE )
#define DPIO2_INTERRUPT_DISCONNECT      		( 41 + IOCTL_CODE )
#define DPIO2_WAIT_FOR_INTERRUPT        		( 50 + IOCTL_CODE )
#define DPIO2_GIVE_ISR_SEMAPHORE        		( 51 + IOCTL_CODE )
#define DPIO2_MMAP_SELECT         			( 61 + IOCTL_CODE )
#define DPIO2_MMAP_CTRL_BASE   				(0)
#define DPIO2_MMAP_FIFO_BASE   				(1)
#define SET_INTEL_31154_P2P_PREFETCH_POLICY             ( 81 + IOCTL_CODE )
#define DPIO2_DMA_START					( 94 + IOCTL_CODE )
#define DPIO2_DMA_END					( 95 + IOCTL_CODE )  //dwp100505

#define DPIO2_DEVICE_NAME       "dpio2"
#define DPIO2_DEVICE_PATH_NAME  "/dev/dpio2/"

#define DPIO2_MANAGER_NAME      "manager"
#define DPIO2_DEVICE_MANAGER    "/dev/dpio2/manager"

#define DPIO2_PCI_CTRL_WINDOW_SIZE 0x00800000
#define DPIO2_PCI_FIFO_WINDOW_SIZE 0x00800000

#define MAX_DPIO2_CARDS     (8)
#define MAX_DPIO2_DEVICE    (MAX_DPIO2_CARDS + 1)
#define MAX_DEVICE_IRQ_CMP  (1024)

typedef struct dpio2_kio_t
{
  int     devno;
//dwp100505  UINT32  offset;
  unsigned long  offset;  //dwp100505
  UINT32  bit_mask;
  int     bit_number;
  union {
    UINT8   byte;
    UINT16  word;
    UINT32  dword;
  } u;
} dpio2_kio_t;

typedef struct dpio2_pci_info_t
{
  UINT16  vendor_id;
  UINT16  device_id;
  UINT8   revision_id;
  UINT8   device_number;
  UINT8   bus_number;
} dpio2_pci_info_t;

typedef struct dpio2_dma_page_t
{
  UINT32 phys_adrs;
  int    size;        
} dpio2_dma_page_t;

typedef struct dpio2_user_dma_page_t
{
  void              *handle;               /* kiovec */
  void              *user_dma_buffer_adrs; /* start adrs to user dma buffer */
  int               user_dma_buffer_size;  /* size of dma buffer in bytes */
  int               struct_size;         /* size of allocated structs */
  int               nr_pages;            /* number of pages (max and used) */
  int               rw;                  /* read or write */
#ifdef LINUX_KERNEL_VERSION_2_4
  void              *page_table_adrs;    /* adrs to pags[] */
#endif
  dpio2_dma_page_t  page[1];            /* PCI adrs and size to each pages */
} dpio2_user_dma_page_t;


typedef struct dpio2_kio_dma_t
{
  void  *dma_page_struct_user_adrs;
  int   dma_page_struct_size;
} dpio2_kio_dma_t;

typedef struct dpio2_hw_info_t {
  int     devno;
  UINT16  deviceId;
  UINT16  vendorId;
  UINT8   revisionId;
  UINT8   moduleType;
  UINT8   pciBusNumber;
  UINT8   pciDeviceNumber;
  UINT8   pciFpgaVersion;
  UINT8   frontEndFpgaVersion;
} dpio2_hw_info_t;

#if 1
#ifdef __cplusplus
}
#endif
#endif
#endif /* DPIO2_LINUX_INCLUDED */
