/*
 * DPIO2 RT-Linux Kernel Mode Driver
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#define __RTCORE_POLLUTED_APP__
#define __KERNEL__
#include <linux/pci.h>
#include "linux/pagemap.h" /* page_cache_release, ... */
#include <linux/ioport.h>
#include <rtl_pthread.h>
#include <rtl_sched.h>
#include <rtl_time.h>
#include <rtl_fifo.h>
//#include <rtl_profile.h>
#include <rtl_conf.h>
#include <rtl_core.h>
#include <rtl_sync.h>
#include <rtl_posixio.h>
#include <rtl_unistd.h>
#include <rtl_core.h>
#include <rtl_stdio.h>
#include <rtl_pthread.h>
#include <rtl_unistd.h>
#include <rtl_fcntl.h>
#include <rtl_posixio.h>
#include <rtl_semaphore.h>

#define MDL_DEBUG  //dwp100506
#define MDL_INFO

#include "dpio2-linux-kernel.h"
#include "dpio2-linux.h"
#include "Dpio2Defs.h"

#undef wake_up
#define wake_up(x) rtl_sem_post(x)

#undef wake_up_interruptible
#define wake_up_interruptible(x) rtl_sem_post(x)

#undef wait_event
#define wait_event(x,y) rtl_sem_wait(&(x))

#undef wait_event_interruptible
#define wait_event_interruptible(x,y) rtl_sem_wait(&(x))

#undef init_waitqueue_head
#define init_waitqueue_head(x) rtl_sem_init(x,0,0)

#undef sema_init
#define sema_init(x,y) rtl_sem_init(x,y,y)

#undef up
#define up(x) rtl_sem_post(x)

#undef down_interruptible
#define down_interruptible(x) rtl_sem_wait(x)

#undef down
#define down(x) rtl_sem_wait(x)

#undef down_trylock
#define down_trylock(x) rtl_sem_trywait(x)

/* memcpy returns dest; this imitates copy_(to|from)_user */
static inline int my_copy_tofrom_user(void *dest, const void *src, size_t n)
{
   rtl_memcpy(dest, src, n);
   return 0;
}
#define copy_to_user     my_copy_tofrom_user
#define copy_from_user   my_copy_tofrom_user

// Forward declaration 
static int 	dpio2_init_pci 		( dpio2_device_t  *device );
static void dpio2_exit			( void );
static int 	rtl_dpio2_open 		( struct rtl_file *filp );
static int 	rtl_dpio2_release	( struct rtl_file *filp );
static int 	rtl_dpio2_ioctl 	( struct rtl_file *filp, unsigned int, unsigned long);
static int 	rtl_dpio2_mmap 		( struct rtl_file *filp, void *start, rtl_size_t length, int prot,
		    					int flags, rtl_off_t offset, rtl_caddr_t * result );
// Globals 
static int 	dpio2_major 		= 0;
dpio2_device_t  dpio2_device		[MAX_DPIO2_CARDS];
dpio2_device_t* dpio2_device_irq_cmp	[MAX_DEVICE_IRQ_CMP];
int 		dpio2_number_of_cards 	= 0;
static int 	dpio2_bus_numbers	[MAX_DPIO2_DEVICE];
dpio2_hw_info_t dpio2_hw_info		[MAX_DPIO2_DEVICE];
UINT32 		last_dma_size           = 0;

static unsigned int phys_DMA_adrs[MAX_DPIO2_DEVICE];  //dwp100505
static int		    phys_DMA_size[MAX_DPIO2_DEVICE];  //dwp100505


// driver entry points
static struct rtl_file_operations rtl_dpio2_fops =
{
    open:    rtl_dpio2_open,
    release: rtl_dpio2_release,
    ioctl:   rtl_dpio2_ioctl,
    mmap:    rtl_dpio2_mmap
};

// device open
static int rtl_dpio2_open(struct rtl_file *filp)
{
  int status = OK;
  // Get minor number
  int minor = rtl_inodes[filp->devs_index].priv;

#ifdef MDL_DEBUG
//dwp100506  printk("rtl_dpio2_open minor %d\n", minor);
  printk("rtl_dpio2_open minor %d, devs_index: %d\n", minor, filp->devs_index);  //dwp100506
#endif
  // Set filp->private_data 
//dwp100505  filp->f_priv = (int) &dpio2_device[minor];
  filp->f_priv = (unsigned long)&dpio2_device[minor];  //dwp100505

  // /dev/dpio2/x (x = 1..8) ??? 
#ifdef MDL_DEBUG
  printk("rtl_dpio2_open devno %d\n", dpio2_device[minor].devno); 
#endif
  if (dpio2_device[minor].devno >= 0 )
  {
#ifdef MDL_DEBUG
    printk("device->phys_ctrl_base 0x%x\n",(int)dpio2_device[minor].phys_ctrl_base);
#endif
    // Mutex down. Prevent other threads or process to open the dpio2 device 
    status = down_trylock ( &dpio2_device[minor].open_mutex );
    if ( status != OK )
    {  
      ERROR_MSG (( "DPIO2 Device #%d is busy, status = %d\n",
		   dpio2_device [minor].devno, status ));
      return -RTL_EINVAL; 
    }
  }
  last_dma_size = 0;
  return OK;
}

// device clode (release)
static int rtl_dpio2_release(struct rtl_file *filp)
{
  dpio2_device_t  *device;
#ifdef MDL_DEBUG
  printk("rtl_dpio2_release\n");
#endif
  if ( !filp->f_priv)
  {  
    ERROR_MSG (("filp->private_data = NULL pointer\n" ));
    return -RTL_EINVAL; 
  }
  else
    device = (dpio2_device_t *) filp->f_priv;
  
  // /dev/dpio2/x  (x = 1..8)  ???
  if ( device->devno >= 0 ) {
    // Unmap DPIO2 user control base
    if ( device->virt_ctrl_map )
    {  
#ifdef MDL_DEBUG
      printk ("DPIO2: iounmap DPIO2 PCI user control base for device %d\n", device->devno);
#endif
      iounmap ( device->virt_ctrl_map );
      device->virt_ctrl_map = 0;      
    }

    // Unmap DPIO2 user fifo base
    if ( device->virt_fifo_map )
    {  
#ifdef MDL_DEBUG
      printk ("DPIO2: iounmap DPIO2 PCI user FIFO base for device %d\n", device->devno);
#endif
      iounmap ( device->virt_fifo_map );
      device->virt_fifo_map = 0;      
    }

    // Mutex up. 
    // Permit outer threads or process to open the dpio2 device 
#ifdef MDL_DEBUG
    printk("rtl_dpio2_release open_mutex\n");
#endif
    up ( &device->open_mutex );
  }
  return OK;
}

// memory map entry point: map control registers and FIFO
static int rtl_dpio2_mmap (struct rtl_file *filp, void *start, rtl_size_t length, int prot,
		    int flags, rtl_off_t offset, rtl_caddr_t * result)
{
  unsigned long physical;
  unsigned long phys_size;
  unsigned long base;
  char *addr = 0;

  dpio2_device_t  *device;
#ifdef MDL_DEBUG
  printk("rtl_dpio2_mmap\n");
#endif

  if ( !filp->f_priv)
  {  
    ERROR_MSG (("filp->private_data = NULL pointer\n" ));
    return -RTL_EINVAL; 
  }
  else
    device = (dpio2_device_t *) filp->f_priv;

  // check calling context
  if (rtl_in_interrupt() || !rtl_rt_system_is_idle())
  {
    printk("Calling context error\n");
    return -RTL_EACCES;
  }

  if (!length)
  {
    return -RTL_EINVAL;
  }

  if ( device->mmap_select == DPIO2_MMAP_CTRL_BASE )
  {
    physical = (unsigned long) device->phys_ctrl_base;
    phys_size = (unsigned long) device->phys_ctrl_size;
    // for simplicity, we allow mapping only once
    if (device->virt_ctrl_map)
    {
      printk("CONTROL already mapped\n");
      return -RTL_EINVAL;
    }
  } 
  else 
  {
    physical = (unsigned long) device->phys_fifo_base;
    phys_size = (unsigned long) device->phys_fifo_size;
    // for simplicity, we allow mapping only once
    if (device->virt_fifo_map)
    {
      printk("FIFO already mapped\n");    
      return -RTL_EINVAL;
    }
  }

  base = physical + offset;

  if (offset+length > phys_size)
  {
    printk(("vsize > phys_size. Span too high\n"));
    return -RTL_EINVAL;
  }

  if (!(addr = ioremap(base, length)))
  {
    printk("PCI: Ioremap returned NULL for 0x%x \n", (int) base);
    return -RTL_EACCES;
  }

  if ( device->mmap_select == DPIO2_MMAP_CTRL_BASE )
  {
    device->virt_ctrl_map = addr;
#ifdef MDL_DEBUG
    printk("PCI: Ioremap returned virt_ctrl_map = %lX\n", (unsigned long)addr);  //dwp100506
#endif
  } 
  else 
  {
    device->virt_fifo_map = addr;
#ifdef MDL_DEBUG
    printk("PCI: Ioremap returned virt_fifo_map = %lX\n", (unsigned long)addr);  //dwp100506
#endif
  }

  *result = addr;
  return OK;
}

// driver load initialization
int init_module(void)
{
  //char buf[64];
  int status;
  int num_device = 0;
  int count;

  struct pci_dev  *pcidev = NULL;
  struct pci_dev  *p2p_dev = NULL;
  struct pci_dev  *p2p_dev_any = NULL;

  unsigned short  prefetch_policy;
  unsigned char   cache_line_size;
  unsigned char   primary_latency_timer;
  unsigned char   primary_bus_number;
  unsigned char   secondary_bus_number;

#ifdef MDL_INFO
  printk ("DPIO2: RTL DPIO2 Device Driver\n");
  printk ("DPIO2: Release: 1.4\n");
  printk ("DPIO2: Creation date: %s\n", __DATE__ ", " __TIME__ );
#endif

  // Register DPIO2 char device driver. Get major number dynamical
  //sprintf(buf, "%s%d", DPIO2_DEVICE_PATH_NAME, 0);
  status = rtl_register_dev( "/dev/dpio2/0", &rtl_dpio2_fops, 0 /* priv passed to fops */);
  if (status == 0)																			//dwp100506
	  status = rtl_register_dev( "/dev/dpio2/1", &rtl_dpio2_fops, 1 /* priv passed to fops */);  //dwp100506
 
  if (status != 0)
  {
    ERROR_MSG (("rtl_register_dev failed, status %d\n", status));
    return status;
  }
  else
  {
    dpio2_major = status;
#ifdef MDL_DEBUG
    printk ("DPIO2: Register device driver.\n"
	    "DPIO2: Dynamical allocated major = %d\n", dpio2_major); 
#endif
  }
    
  // Clear dpio2_device struct
  memset ( (void*) &dpio2_device[0], 0, MAX_DPIO2_CARDS * sizeof (dpio2_device_t) );

  // Clear irq line compare arry
  memset ( (void*) &dpio2_device_irq_cmp[0], 0, MAX_DEVICE_IRQ_CMP * sizeof (dpio2_device_t*) );

  // Clear dpio2_hw_info
  memset ( (void*) &dpio2_hw_info[0], 0, MAX_DPIO2_DEVICE * sizeof (dpio2_hw_info_t) );

  // Init dpoi2 bus number table
  for (count = 0; count < MAX_DPIO2_CARDS; count++) 
    dpio2_bus_numbers [count] = -1;

#ifdef MDL_DEBUG
  printk ("DPIO2: Scan for DPIO2 hardware devices:\n");
#endif

  // Check for dpio2 33MHz
  while ( ( pcidev = pci_get_device ( PCI_VENDOR_ID_VMETRO,
				       PCI_DEVICE_ID_DPIO2,
				       pcidev ) ) )
  {
    // Add number of dpio2 modules found

#ifdef MDL_INFO
    printk ("DPIO2: Found Device #%d: DPIO2_33MHz\n", num_device );
#endif

    if ( num_device > MAX_DPIO2_CARDS-1 )
    {
      ERROR_MSG (("pci_enable_device failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL;  
    }

    // Set device data
    dpio2_device [num_device].devno = num_device;
    dpio2_device [num_device].pcidev = pcidev;
    
    // Map PCI resourches. Get HW info.
    status = dpio2_init_pci ( &dpio2_device [num_device] );

    if ( status != OK )
    {
      ERROR_MSG (("dpio2_init_pci failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL;    
    }

    num_device++;
  }

  // Check for dpio2 66MHz
  pcidev = NULL;
  
  while ( ( pcidev = pci_get_device ( PCI_VENDOR_ID_VMETRO,
				       PCI_DEVICE_ID_DPIO2_66MHZ,
				       pcidev ) ) )
  {
    // Add number of dpio2 modules found
#ifdef MDL_INFO
    printk ("DPIO2: Found Device #%d: DPIO2_66MHz\n", num_device );
#endif
    
    if ( num_device > MAX_DPIO2_CARDS-1 )
    {
      ERROR_MSG (("pci_enable_device failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL; 
    }

    // Set device data
    dpio2_device [num_device].devno = num_device;
    dpio2_device [num_device].pcidev = pcidev;
    
    // Map PCI resourches. Get HW info.
    status = dpio2_init_pci ( &dpio2_device [num_device] );

    if ( status != OK )
    {
      ERROR_MSG (("dpio2_init_pci failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL;
    }
    num_device++;
  }

  // Check for Intel 31154 P2P Brigde
  // Set prefetch registers to optimal PCI <--> PCI-X performance.
  while ( ( p2p_dev = pci_get_device ( PCI_VENDOR_ID_INTEL_31154,
					PCI_DEVICE_ID_INTEL_31154,
					p2p_dev ) ) )
  {
    // Enable PCI device
    status = pci_enable_device ( p2p_dev );

    if (status != 0)
    {  
      ERROR_MSG (("pci_enable_device failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL;
    }

    // Set prefetch policy
    pci_read_config_word ( p2p_dev, 0x52, &prefetch_policy );
    prefetch_policy |= 0x03ff;
    pci_write_config_word ( p2p_dev, 0x52, prefetch_policy );
    pci_read_config_word ( p2p_dev, 0x52, &prefetch_policy );
 
    // Set cache line size
    pci_write_config_byte ( p2p_dev, 0x0c, 0x10 );
    pci_read_config_byte ( p2p_dev, 0x0c, &cache_line_size );

    /* Set primary latency timer
     */
    pci_write_config_byte ( p2p_dev, 0x0d, 0xff );
    pci_read_config_byte ( p2p_dev, 0x0d, &primary_latency_timer );
         
    // Get bus number
    pci_read_config_byte ( p2p_dev, 0x18, &primary_bus_number );
    pci_read_config_byte ( p2p_dev, 0x19, &secondary_bus_number );

    // Set bus number
    for ( count = 0; count < MAX_DPIO2_CARDS; count++ )
    {  
      if ( dpio2_bus_numbers[count] == -1 )
	break;
      
      if ( dpio2_bus_numbers[count] == secondary_bus_number )
	dpio2_bus_numbers[count] = primary_bus_number;
    }

#ifdef MDL_DEBUG
    printk ("DPIO2: Found Intel 31154 P2P Brigde: "
	    "Bus (Primary #%d, Secondary #%d)\n"
	    "       Prefetch policy 0x%x. Cacheline size 0x%x. "
	    "Primary latency timer 0x%x.\n",
	    (int)primary_bus_number, (int)secondary_bus_number,
	    (int)prefetch_policy, (int)cache_line_size, 
	    (int)primary_latency_timer );
#endif
  }

  // Check for other P2P Brigde
  while ( ( p2p_dev_any = pci_get_class ( 0x60400, p2p_dev_any ) ) )
  {   
    unsigned short vendor_id;
    unsigned short device_id;
    unsigned char  cacheline_size;
    unsigned char  pri_latency_timer;
    unsigned char  sec_latency_timer;

    // Enable PCI device
    status = pci_enable_device ( p2p_dev_any );
    if (status != 0)
    {
      ERROR_MSG (("pci_enable_device failed\n"));
      dpio2_exit ();
      return -RTL_EINVAL;    
    }

    pci_read_config_word ( p2p_dev_any, 0x00, &vendor_id );
    pci_read_config_word ( p2p_dev_any, 0x02, &device_id );
    pci_read_config_byte ( p2p_dev_any, 0x18, &primary_bus_number );
    pci_read_config_byte ( p2p_dev_any, 0x19, &secondary_bus_number );
    pci_read_config_byte ( p2p_dev_any, 0x0c, &cacheline_size );
    pci_read_config_byte ( p2p_dev_any, 0x0d, &pri_latency_timer );
    pci_read_config_byte ( p2p_dev_any, 0x1b, &sec_latency_timer );

#ifdef MDL_DEBUG
    printk ("DPIO2: Found P2P: VendorID 0x%x, DeviceID 0x%x, "
	    "Bus (pri #%d, sec #%d)\n"
	    "       cacheline size 0x%x, latency timer (pri 0x%x, sec 0x%x)\n",
	    vendor_id, device_id, primary_bus_number, secondary_bus_number,
	    cacheline_size, pri_latency_timer, sec_latency_timer );
#endif

#if 0
    for ( count = 0; count < MAX_DPIO2_CARDS-1; count++ )
    {
      if ( dpio2_bus_numbers[count] == -1 )
	break;
 
      if ( dpio2_bus_numbers[count] == secondary_bus_number )
      {
	// Set primary latency timer to 0xff
	pci_write_config_byte ( p2p_dev_any, 0x0d, 0xf8 );
	pci_read_config_byte ( p2p_dev_any, 0x0d, &u8 );
	printk ("primary latency timer 0x%x\n",(int)u8);
	pci_write_config_byte ( p2p_dev_any, 0x0c, 0x10 );
	pci_read_config_byte ( p2p_dev_any, 0x0c, &u8 );
	printk ("cache line size 0x%x\n",(int)u8);
      }
    }
#endif
  }

  // Initialize spin lock 
  for ( count = 0; count < num_device; count++ )    
    rtl_spin_lock_init ( &dpio2_device [count].irq_lock );
  
  // Initialize mutex
  for ( count = 0; count < num_device; count++ )
    sema_init ( &dpio2_device [count].open_mutex, 1 ); /* free */

  // Set global variable
  dpio2_number_of_cards = num_device;
#ifdef MDL_INFO
  printk("Number of DPIO2 cards = %d\n",  dpio2_number_of_cards);
#endif
  return 0;
}

// driver unload cleanup
void cleanup_module(void)
{
  dpio2_exit();
}

// init PCI device when found
int dpio2_init_pci ( dpio2_device_t  *device )
{  
  int status = OK;
  int devno = device->devno;

  // Enable PCI device
  status = pci_enable_device ( device->pcidev );
  if ( status != OK )
  {      
    ERROR_MSG (("pci_enable_device failed\n"));
    return -RTL_EINVAL;    
  }

  // Enable PCIbus master 
  pci_set_master ( device->pcidev );
  if (pci_set_dma_mask (device->pcidev, 0xffffffff ))
  {
    ERROR_MSG (("pci_set_dma_mask failed\n"));
    return -RTL_EINVAL;
  }

  // Set cache line size latency timer to max
  pci_write_config_byte ( device->pcidev, 0x0c, 0x00 );
  pci_write_config_byte ( device->pcidev, 0x0d, 0xff );
  pci_set_master ( device->pcidev );
  pci_write_config_word ( device->pcidev, 0x04, 0x0106 );
   
  // Get bus number
  dpio2_bus_numbers [devno] = device->pcidev->bus->number;

  // Get phys PCI base adrs info
  device->phys_ctrl_base = pci_resource_start ( device->pcidev, 0);
  device->phys_ctrl_end  = pci_resource_end ( device->pcidev, 0);
  device->phys_ctrl_size = device->phys_ctrl_end - device->phys_ctrl_base + 1;
  device->phys_fifo_base = pci_resource_start ( device->pcidev, 1);
  device->phys_fifo_end  = pci_resource_end ( device->pcidev, 1);
  device->phys_fifo_size = device->phys_fifo_end - device->phys_fifo_base + 1;
  device->virt_ctrl_map = NULL;
  device->virt_fifo_map = NULL;
  
#ifdef MDL_DEBUG
  printk ("device->phys_ctrl_base 0x%x\n",(int)device->phys_ctrl_base);
  printk ("device->phys_fifo_base 0x%x\n",(int)device->phys_fifo_base);
#endif
  
  // Map PCI control base to virtual adrs 
  device->ctrl_base = ioremap_nocache ( device->phys_ctrl_base,
					device->phys_ctrl_size );

  // Map PCI FIFO base to virtual adrs
  device->fifo_base = ioremap_nocache ( device->phys_fifo_base,
                                        device->phys_fifo_size );

  // Get dpio2 info (pci, fpga,...)
  dpio2_hw_info [devno].devno = devno;
  pci_read_config_word ( device->pcidev, 0x00, &dpio2_hw_info [devno].vendorId );
  pci_read_config_word ( device->pcidev, 0x02, &dpio2_hw_info [devno].deviceId );
  pci_read_config_byte ( device->pcidev, 0x08, &dpio2_hw_info [devno].revisionId );


  /* Set primary latency timer
  */
  //unsigned char plt;
  //pci_write_config_byte ( device->pcidev, 0x0d, 0x40 );
  //pci_read_config_byte ( device->pcidev, 0x0d, &plt );
  //printk("plt = %x\n", plt);
  
  dpio2_hw_info [devno].pciBusNumber = device->pcidev->bus->number;
  dpio2_hw_info [devno].pciDeviceNumber = PCI_SLOT ( device->pcidev->devfn );
  dpio2_hw_info [devno].moduleType = 
    readb ( device->ctrl_base + DPIO2_FRONT_END_PERSONALITY_ID_REGISTER );
  dpio2_hw_info [devno].pciFpgaVersion = 
    readb ( device->ctrl_base +  DPIO2_PCI_VERSION_REGISTER );
  dpio2_hw_info [devno].frontEndFpgaVersion = 
    readb ( device->ctrl_base + DPIO2_FRONT_END_VERSION_REGISTER );

  return OK;
}

// driver unload cleanup processing
static void dpio2_exit(void)
{ 
  int i;
  dpio2_device_t  *device;
  
  for ( i = 0; i < dpio2_number_of_cards; i++ )
  {  
    device = &dpio2_device[i];

    // Free interrupt handler
    if ( device->interrupt_line )
    {
      printk ("DPIO2: ERROR attempting to remove IRQ (%d) handle for device %d\n",  //XXX
	      	  device->pcidev->irq, device->devno);									//XXX
/*XXX start -- No interrupt is assigned and the build is choking on free_irq() 
      free_irq ( device->interrupt_line, device );
      dpio2_device_irq_cmp [ device->interrupt_line ] = 0;
      device->interrupt_line = 0;
XXX end*/
#ifdef MDL_DEBUG
      printk ("DPIO2: Removing IRQ (%d) handle for device %d\n",
	      device->pcidev->irq, device->devno);
#endif
    }

    // Unmap DPIO2 control base
    if ( device->ctrl_base )
    {  
#ifdef MDL_DEBUG
      printk ("DPIO2: iounmap DPIO2 PCI control base for device %d\n", device->devno);
#endif
      iounmap ( device->ctrl_base );
      device->ctrl_base = 0;      
    }

    // Unmap DPIO2 control base
    if ( device->fifo_base )
    {
#ifdef MDL_DEBUG
      printk ("DPIO2: iounmap DPIO2 PCI FIFO base for device %d\n", device->devno);
#endif
      iounmap ( device->fifo_base );
      device->fifo_base = 0;
    }
  }

  // Unregister DPIO2 char device driver
#ifdef MDL_DEBUG
  printk ( "DPIO2: Unregister device driver\n" );
#endif
//dwp100506  rtl_unregister_dev( "dpio2" );
  rtl_unregister_dev( "/dev/dpio2/0" );  //dwp100506
  rtl_unregister_dev( "/dev/dpio2/1" );  //dwp100506

  // Clear dpio2_device struct
  memset ( (void*) &dpio2_device[0], 0, MAX_DPIO2_DEVICE * sizeof (dpio2_device_t) );
#ifdef MDL_DEBUG
  printk ("DPIO2: Exit\n");
#endif
  return;
}

// device IOCTL entry point
static int rtl_dpio2_ioctl(struct rtl_file *filp,
			 unsigned int request, unsigned long l)
{
  unsigned int cmd = request; 
  unsigned long arg = l;
  int  status;
  unsigned long  input; 
  unsigned long  output; 
  dpio2_device_t *device;
  dpio2_kio_t  kio;

  if ( !filp->f_priv)
  {  
    ERROR_MSG (("filp->private_data = NULL pointer\n" ));
    return -RTL_EINVAL; 
  }
  else
    device = (dpio2_device_t *) filp->f_priv;

  // Find and execute command
  switch (cmd)
  {
  // function used for setting/clearing register bits
  case VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC:
    // Copy user buffer
    status = copy_from_user ( (void*) &kio, 
			      (void*) arg,
			      sizeof (dpio2_kio_t) );
    if ( status != OK )
    {
      //ERROR_MSG (("copy_from_user failed\n"));
      //printk("Error 3\n");
      return -RTL_EACCES;
    }

    // Spin and irq lock 
    rtl_spin_lock_irqsave ( &device->irq_lock, device->irq_save_flag );
    
    //  Read modify write
//	if (kio.offset == DPIO2_FRONT_END_OUTPUT_VALUE_REGISTER)  //dwp100504
//		printk("S");										  //dwp100504
    input = readl ( device->ctrl_base +  kio.offset );
    
    output = ( ( input & ~kio.bit_mask ) | 
	       ( kio.u.dword & kio.bit_mask ));  
   
//dwp100504 -- start
	if (kio.offset == DPIO2_FRONT_END_OUTPUT_ENABLE_REGISTER)
    {
    	printk("E base: %lX, offset: %lX, mask: %X, data: %X, input: %lX, output: %lX\n",
    			(unsigned long)device->ctrl_base, kio.offset, kio.bit_mask, kio.u.dword, input, output);
	}
	static int printCntr = 0, printNext = 0;
	if ((kio.offset == DPIO2_FRONT_END_OUTPUT_VALUE_REGISTER) && (printCntr < 50))
    {
		if (printNext == 1)
		{
			printCntr++;
			printNext = 0;
	    	printk("1 base: %lX, offset: %lX, mask: %X, data: %X, input: %lX, output: %lX\n",
	    			(unsigned long)device->ctrl_base, kio.offset, kio.bit_mask, kio.u.dword, input, output);
		}
		if (kio.u.dword == 0)
		{
			printCntr++;
			printNext = 1;
	    	printk("0 base: %lX, offset: %lX, mask: %X, data: %X, input: %lX, output: %lX\n",
	    			(unsigned long)device->ctrl_base, kio.offset, kio.bit_mask, kio.u.dword, input, output);
		}
	}
//dwp100504 -- end
 
    writel ( output, device->ctrl_base + kio.offset );
    
#ifdef NO_READ_AFTER_WRITE
  wmb();
#else
    // Read the same register to be sure that the write operation has been done
    readl ( device->ctrl_base +  kio.offset );
#endif
   
    // spin and irq unlock 
    rtl_spin_unlock_irqrestore ( &device->irq_lock, device->irq_save_flag );
    return OK;

  case VMOSA_REG_32_BIT_TOGGLE_ATOMIC:
    // Copy user buffer
    status = copy_from_user ( (void*) &kio, (void*) arg, sizeof (dpio2_kio_t) );
    if ( status != OK )
    {
      ERROR_MSG (("copy_from_user failed\n"));
      return ERROR;
    }
   
    // Spin and irq lock 
    rtl_spin_lock_irqsave ( &device->irq_lock, device->irq_save_flag );
 
    // Toggle bit
    input = readl ( device->ctrl_base +  kio.offset );
    
    if ( input & kio.bit_mask)
      output = input & ~kio.bit_mask;
    else 
      output = input | kio.bit_mask;
    
    writel ( output, device->ctrl_base +  kio.offset );
    
#ifdef NO_READ_AFTER_WRITE
  wmb();
#else
    // Read the same register to be sure that the write operation has been done
    readl ( device->ctrl_base +  kio.offset );
#endif
    
    // spin and irq unlock 
    rtl_spin_unlock_irqrestore ( &device->irq_lock, device->irq_save_flag );
    return OK;
    
  case VMOSA_PCI_CONFIG_8_READ:
    {
      UINT8  u8;
      pci_read_config_byte ( device->pcidev, arg, &u8 );
      return u8;
    }

  case VMOSA_PCI_CONFIG_16_READ:
    {
      UINT16  u16;
      pci_read_config_word ( device->pcidev, arg, &u16 );
      return u16;
    }

  case VMOSA_PCI_CONFIG_8_WRITE:
    {
      UINT8  u8;
      // Write to register
      pci_write_config_byte ( device->pcidev, (arg >> 8), (UINT8)(arg & 0x00ff) );
      // Read the same register to be sure that the write operation has been done
      pci_read_config_byte ( device->pcidev, (arg >> 8), &u8 );
      return OK;
    }

  case DPIO2_SCAN:
    // Copy pci info to user
    status = copy_to_user ( (void*) arg, 
			    (void*) &dpio2_hw_info[0],
			    MAX_DPIO2_DEVICE * sizeof (dpio2_hw_info_t) );
    if ( status != OK )
    {
      ERROR_MSG (("copy_to_user failed\n"));
      return ERROR;
    }      
    return dpio2_number_of_cards;

  case DPIO2_MMAP_SELECT:
    if ( arg == DPIO2_MMAP_CTRL_BASE )
    {
      device->mmap_select = DPIO2_MMAP_CTRL_BASE;
    }
    else 
    {
      if ( arg == DPIO2_MMAP_FIFO_BASE )
      {
        device->mmap_select = DPIO2_MMAP_FIFO_BASE;
      }
      else
      {
        ERROR_MSG (("DPIO2_MMAP_SELECT: invalid arg = %d\n", (int) arg));
        return ERROR;
      }
    }
    return OK;

  case DPIO2_GET_HW_INFO: 
    {      
      // Copy pci info to user
      status = copy_to_user ( (void*) arg, 
			      (void*) &dpio2_hw_info[0],
			      MAX_DPIO2_DEVICE * sizeof (dpio2_hw_info_t) );
      if ( status != OK )
      {
	ERROR_MSG (("copy_to_user failed\n"));
	return ERROR;
      }      
      return OK;
    }

  case DPIO2_GET_PCI_CONFIG_INFO: 
    {      
      dpio2_pci_info_t   pci_info;
	
      // Set PCI info
      pci_info.vendor_id = device->pcidev->vendor;
      pci_info.device_id = device->pcidev->device;
      pci_read_config_byte ( device->pcidev, PCI_REVISION_ID, &pci_info.revision_id );
      pci_info.device_number = PCI_SLOT ( device->pcidev->devfn );
      pci_info.bus_number = device->pcidev->bus->number;

      // Copy pci info to user
      status = copy_to_user ( (void*) arg, (void*) &pci_info, sizeof (dpio2_pci_info_t) );
      
      if ( status != OK )
      {
	ERROR_MSG (("copy_to_user failed\n"));
	return ERROR;
      }      
      return OK;
    }

  case DPIO2_DMA_START:
      {
      unsigned long ulReg = 0;

      // Copy user buffer
      status = copy_from_user ( (void*) &kio, 
                              (void*) arg,
                              sizeof (dpio2_kio_t) );
      if ( status != OK )
      {
        //ERROR_MSG (("copy_from_user failed\n"));
        //printk("Error 3\n");
        return -RTL_EACCES;
      }

      // Spin and irq lock
      rtl_spin_lock_irqsave ( &device->irq_lock, device->irq_save_flag );

      if (last_dma_size != 0)
      {
         UINT32 ulBytesX = readl(device->ctrl_base + DPIO2_DMA_TRANSFERRED_BYTE_COUNT_REGISTER);
         UINT32 ulStatus = readl(device->ctrl_base + DPIO2_DMA_STATUS_REGISTER);

         //printk("Byte Xfrd in Last DMA (DMA_CSR7) = %08x\n", ulBytesX);
         //printk("DMA Status            (DMA_CSR5) = %08x\n", ulStatus);

         if ((last_dma_size != ulBytesX) || ((ulStatus & 0x03000206)!=0x03000206))
         {
            //printk("Previous DMA failed\n");
      	    rtl_spin_unlock_irqrestore ( &device->irq_lock, device->irq_save_flag );
            return -RTL_EACCES;
         }
         //else
            //printk("Previous DMA completed OK\n");
      }

//dwp100505      int *cpu_addr = (int *)kio.offset;
      void *cpu_addr = (int *)kio.offset;
//dwp100505      UINT32 bus_addr = virt_to_bus (cpu_addr);
      UINT32 dma_size = kio.bit_mask;
//dwp100505 -- start
//	  void *kPtr = kmap(cpu_addr);  // not sure if you need kmap() here ??
//	  dma_addr_t tmpDmaAddr = pci_map_single ( device->pcidev, kPtr, dma_size, PCI_DMA_TODEVICE );
	  dma_addr_t tmpDmaAddr = pci_map_single ( device->pcidev, cpu_addr, dma_size, PCI_DMA_TODEVICE );
	  UINT32 bus_addr = (UINT32)tmpDmaAddr;

	  int devno = device->devno;
  	  phys_DMA_adrs[devno] = bus_addr; 
  	  phys_DMA_size[devno] = dma_size;

	  static int printCntr = 0;										 //dwp100505
	  if (printCntr < 10)										     //dwp100505
	  {																 //dwp100505
		printCntr++;												 //dwp100505
		rtl_printf("%d DMA start -- tmpDmaAddr: %lX\n", printCntr, (unsigned long)tmpDmaAddr);  //dwp100505
		int i;
		for (i = 0; i < dma_size; i++)
			rtl_printf("%d ", ((unsigned short*)cpu_addr)[i]);  //dwp100505
		rtl_printf("\n");  //dwp100505
   	  }															     //dwp100505
//dwp100505 -- end

      //printk("DMA size = %d\n", dma_size);
      //printk("DMA buffer virt=%08x bus=%08x b2v=%08x v2b=%08x\n", cpu_addr, bus_addr, bus_to_virt(bus_addr), virt_to_bus(cpu_addr));

      DWORD dwDmaOptions = 1<<DPIO2_BIT_NUM_ENABLE_D64_ACCESSES;
      writel (   0x80000000, device->ctrl_base + DPIO2_DMA_NEXT_DESCRIPTOR_ADDRESS_REGISTER  );
      writel ( dwDmaOptions, device->ctrl_base + DPIO2_DMA_D64_REGISTER );

      writel (dma_size, device->ctrl_base + DPIO2_DMA_TRANSFER_SIZE_REGISTER  );
      writel (bus_addr, device->ctrl_base + DPIO2_DMA_PCI_ADDRESS_REGISTER );
      last_dma_size = dma_size;

      // set PCI Rd/Wr commmands Rd=C, Wr=7, start DMA
      ulReg = 0x0000007c;
      writel (   ulReg, device->ctrl_base + DPIO2_DMA_CONTROL_REGISTER  );
      ulReg = 0x8000007c;
      writel (   ulReg, device->ctrl_base + DPIO2_DMA_CONTROL_REGISTER  );

      //ulReg = readl(device->ctrl_base + DPIO2_DMA_NEXT_DESCRIPTOR_ADDRESS_REGISTER); 	printk("a DMA_CSR0 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_PCI_ADDRESS_REGISTER); 		printk("a DMA_CSR1 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_D64_REGISTER); 			printk("a DMA_CSR2 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_TRANSFER_SIZE_REGISTER); 		printk("a DMA_CSR3 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_CONTROL_REGISTER); 			printk("a DMA_CSR4 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_STATUS_REGISTER); 			printk("a DMA_CSR5 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_BYTE_COUNT_REGISTER); 		printk("a DMA_CSR6 = %08x\n", ulReg);
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_TRANSFERRED_BYTE_COUNT_REGISTER); 	printk("a DMA_CSR7 = %08x\n", ulReg);
      //ulReg = 0;
      //while (!ulReg)
      //ulReg = readl(device->ctrl_base + DPIO2_DMA_STATUS_REGISTER) & 0x00000004;

      // spin and irq unlock
      rtl_spin_unlock_irqrestore ( &device->irq_lock, device->irq_save_flag );
      }
      return OK;

//dwp100505 -- start
  case DPIO2_DMA_END:
	  {
	  int devno = device->devno;
  	  pci_unmap_single ( device->pcidev, (dma_addr_t)phys_DMA_adrs[devno], phys_DMA_size[devno], PCI_DMA_TODEVICE );

	  static int printCntr = 0;										 //dwp100505
	  if (printCntr < 10)										     //dwp100505
	  {																 //dwp100505
		printCntr++;												 //dwp100505
		rtl_printf("%d DMA end -- phys_DMA_adrs: %lX\n", printCntr, (unsigned long)phys_DMA_adrs[devno]);  //dwp100505
   	  }															     //dwp100505
	  }
      return OK;
//dwp100505 -- end

  default:
    ERROR_MSG (( "Ilegal ioctl() command\n" ));
    return -EINVAL;

  } /* switch */
  return OK;

}

