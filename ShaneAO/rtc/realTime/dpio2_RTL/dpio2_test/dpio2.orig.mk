include rtl.mk

CFLAGS := $(CFLAGS) -I../dpio2_include
TARGET_RTL := rt_dpio2_test.rtl
LIBRARY_OBJS := rt_dpio2_funcs.o rt_dpio2_osc.o 
LDFLAGS_$(TARGET_RTL) := $(LIBRARY_OBJS)

all: $(TARGET_RTL) $(HOME)/rt_dpio2_test.rtl

$(HOME)/rt_dpio2_test.rtl: rt_dpio2_test.rtl
	cp rt_dpio2_test.rtl $(HOME)

$(TARGET_RTL): $(LIBRARY_OBJS) ../dpio2_include/shared.h

rt_dpio2_funcs.o: rt_dpio2_funcs.c
	$(CC) $(CFLAGS) -I../dpio2_include -c $<

rt_dpio2_osc.o: rt_dpio2_osc.c
	$(CC) $(CFLAGS) -I../dpio2_include -c $<

clean:
	rm -rf *.o *.rtl *.debug

include	Rules.make

