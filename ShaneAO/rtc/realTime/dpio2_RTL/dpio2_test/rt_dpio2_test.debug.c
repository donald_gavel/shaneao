/*
 * DPIO2 RT-Linux Test Program Entry Point and RT Thread
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef CONFIG_RTL_BSD
#include <sys/rtl_ioctl.h>
#include <rtl_fcntl.h>
#endif
#include <string.h>
#include "shared.h"

#define VMOSA
#include "vmosa-linux.h"
#include "dpio2-linux.h"
#include "dpio2-linux-user-space.h"

#include "dpio2.h"
#include "Dpio2Defs.h"

#define MDL_DEBUG  //dwp100209
#define NSTICK 1000000000
#define MMAP_SIZE (FRAME_SIZE * sizeof(unsigned short int) * 2) // double buffered

// see rt_dpio2_funcs.c
extern HANDLE dpio2_Open			(int iDevno);
extern STATUS dpio2_Close			(HANDLE hHandle);
extern STATUS dpio2_ConfigurePioStrobes		(HANDLE hHandle, int iValue);
extern STATUS dpio2_ControlPioStrobes		(HANDLE hHandle, int iValue);
extern STATUS dpio2_InitializeForOutput		(HANDLE hHandle);
extern STATUS dpio2_EnableStrobeGeneration	(HANDLE hHandle, int iMode, unsigned int dwWantedFrequency, unsigned int *pdwResultantFrequency);
extern STATUS dpio2_ActivateFpdpInterface	(HANDLE hHandle);
extern STATUS dpio2_DmaTransfer			(HANDLE hHandle, UINT32 dwSizeInBytes, unsigned char *pData);

static pthread_t	thread		= (pthread_t) NULL;
static int 		fd1 		= -1;
static int 		fdp 		= -1;
static HANDLE 		hDpio2 		= NULL;
static int 		iTick 		= NSTICK / 100;
static unsigned long 	FrameNum 	= 0;
static int		dma_fd		= -1;
static unsigned char 	*dma_base	= NULL;
static unsigned short 	*Data[2]	= {NULL}; // double buffer reference into dma_base
static int 		Run 		= 0;
static int 		Step		= 0;
static int 		Term		= 0;
static int 		iLength 	= PACKETSIZE_1024;
static long 		Rate 		= 1200;
static short 		Strobe		= 10;
static short 		Fixed     	= 0;
static unsigned int 	dwStrobe 	= 10*1000000;
static int 		iDataMode 	= 0;
static short 		PokeIndex 	= 0;
static short 		PokeValue 	= 0;
static unsigned int 	dwResultStrobe 	= 0;
static unsigned int     dwDmaSkipCount  = 0;
static int              idx		= 0; // double buffer index
static int              iErrorMode      = 0;

// The following is called from the real time thread loop.
// 1) Control Strobes
// 2) Receive commands from RTCLI via FIFO
// 3) Generate Data
// 4) Send Data
// 5) Toggle LPT strobes for diagnostics 
//
static void rt_timer_callback(void)
{
	int i =0, j = 0;
	int iRet=0;
	unsigned char CmdByte;
	STATUS status = OK;

	// raise PIO strobe(s)
        if (dpio2_ControlPioStrobes(hDpio2, 1) != OK)
        	Run = 0;

	// get the command code
	if (read(fd1, &CmdByte, sizeof(CmdByte)) == sizeof(CmdByte))
	{
		switch (CmdByte)
		{
		// stop main loop
		case STOP_CMD:
			#ifdef MDL_DEBUG
			printf("STOP\n");
			#endif
			Run = 0;
                        if (dwDmaSkipCount)
                           printf("DMA overrun: %d frames\n", dwDmaSkipCount);
			dwDmaSkipCount = 0;
			break;

		// start main loop
		case RUN_CMD:
			#ifdef MDL_DEBUG
			printf("RUN\n");
			#endif
			Run = 1;
                        dwDmaSkipCount = 0;
			break;

                // step main loop
                case STEP_CMD:
                        #ifdef MDL_DEBUG
                        printf("STEP\n");
                        #endif
                        Step = 1;
                        break;

		// clear all actuator values to 0x0000
		case CLEAR_CMD:
			#ifdef MDL_DEBUG
			printf("CLEAR\n");
			#endif
			iDataMode = 0;
			for( i=0; i<FRAME_SIZE; i++ )
                                Data[0][i] = Data[1][i] = (unsigned short int) 0x0000;
			break;

		// load all actuators from a file
		case LOAD_CMD:
			#ifdef MDL_DEBUG
			printf("LOAD\n");
			#endif
			if (iDataMode != 1)
			{
                        	for( i=0; i<FRAME_SIZE; i++ )
                                	Data[0][i] = Data[1][i] = (unsigned short int) 0x0000;
			}
			iDataMode = 1;
			// PokeIndex used to get count of words to come
			while (read(fd1, &PokeIndex, sizeof(PokeIndex)) != sizeof(PokeIndex));
			for (i=0; i<PokeIndex; i++)
			{
 				while (read(fd1, &PokeValue, sizeof(PokeValue)) != sizeof(PokeValue));
                                Data[0][i] = Data[1][i] = PokeValue;
			}
			break;

		// setup framing test pattern
		case TEST_CMD:
			#ifdef MDL_DEBUG
			printf("TEST\n");
			#endif
			iDataMode = 3;
			break;

                // specify halt on error
                case HERR_CMD:
                        #ifdef MDL_DEBUG
                        printf("HERR ");
                        #endif
                        while (read(fd1, &CmdByte, sizeof(CmdByte)) != sizeof(CmdByte));
                        iErrorMode = CmdByte ? 1 : 0;
                        #ifdef MDL_DEBUG
                        printf("%d\n", iErrorMode);
                        #endif
                        break;

		// poke a single actuator
		case POKE_CMD:
			#ifdef MDL_DEBUG
			printf("POKE\n");
			#endif
                        if (iDataMode != 2)
                        {
                                for( i=0; i<FRAME_SIZE; i++ )
                                        Data[0][i] = Data[1][i] = (unsigned short int) 0x0000;
                        }
                        iDataMode = 2;
			while (read(fd1, &PokeIndex, sizeof(PokeIndex)) != sizeof(PokeIndex));
			while (read(fd1, &PokeValue, sizeof(PokeValue)) != sizeof(PokeValue));
                        #ifdef MDL_DEBUG
                        printf("POKE act %d with %04x\n", PokeIndex, PokeValue);
                        #endif
			if (PokeIndex >=0 && PokeIndex < iLength)
				Data[0][PokeIndex] = Data[1][PokeIndex] = PokeValue;
			break;

		// set the frame size (mode 1024 actuators, mode 4096 actuators)
		case MODE_CMD:
			#ifdef MDL_DEBUG
			printf("MODE ");
			#endif
 			while (read(fd1, &CmdByte, sizeof(CmdByte)) != sizeof(CmdByte));
			if (CmdByte == 0)
				iLength = PACKETSIZE_1024;
			else
			if (CmdByte == 1)
				iLength = PACKETSIZE_4096;
                        else
                        if (CmdByte == 2)
                                iLength = PACKETSIZE_1024;

			#ifdef MDL_DEBUG
			printf("%d\n", iLength);
			#endif
			break;

		// set frame rate via reprogramming the timer
		case RATE_CMD:
			#ifdef MDL_DEBUG
			printf("RATE\n");
			#endif
			while (read(fd1, &Rate, sizeof(Rate)) != sizeof(Rate));
			iTick = (NSTICK) / (int) Rate;
			break;

                // set frame rate via reprogramming the timer
                case STROBE_CMD:
                        #ifdef MDL_DEBUG
                        printf("STROBE\n");
                        #endif
                        while (read(fd1, &Strobe, sizeof(Strobe)) != sizeof(Strobe));
                        while (read(fd1, &Fixed,  sizeof(Fixed))  != sizeof(Fixed));
                        dwStrobe = (unsigned int) (Strobe*1000000);
			{
			// use DPIO2_FIXED_OSCILLATOR or DPIO2_PROGRAMMABLE_OSCILLATOR
        		status = dpio2_EnableStrobeGeneration(hDpio2,
				 Fixed ? DPIO2_FIXED_OSCILLATOR : DPIO2_PROGRAMMABLE_OSCILLATOR,
				 Fixed ? dwStrobe : dwStrobe/2, &dwResultStrobe);

        		if (status != OK)
                		printf("dpio2_EnableStrobeGeneration failed\n");
			#ifdef MDL_DEBUG
			else
				printf("%s dpio2 strobe set to %d\n", Fixed ? "FIXED" : "PROG", dwResultStrobe);
			#endif
			}
                        break;

		// rtcli termination, just stop framing
		case TERM_CMD:
			#ifdef MDL_DEBUG	
			printf("TERM\n");
			#endif
			Term = 1;
			break;
		}
	}

	// if we are framing
	if ((Run||Step) && Term==0)
	{
		// assert sync pulse at printer port
                if (fdp != -1)
                {
                	unsigned long ulMask[2] = {0x00000000, 0x00000080};
                	iRet = ioctl(fdp, RTL_CLEARSETBITMASK, ulMask);
                }

		// the only dynamic pattern is the test pattern (iDataMode = 3)
		switch(iDataMode)
		{
		case 0:
		case 1:
		case 2:
 			break;
		case 3:
		{
			unsigned short int iTopNibble; 
			unsigned short int iShiftPat = 1;
 			FrameNum++;
                        iTopNibble = (FrameNum%16)<<12;
			for( j=0; j<iLength; j++ )
			{
                                Data[idx][j] = (unsigned short int) (iTopNibble | iShiftPat);
				iShiftPat = iShiftPat<<1;
				if (iShiftPat & 0x1000)
					iShiftPat = 1;
			}
		}
			break;
		}

                // assert sync pulse at printer port
                if (fdp != -1)
                {
                        unsigned long ulMask[2] = {0x00000080, 0x00000040};
                        iRet = ioctl(fdp, RTL_CLEARSETBITMASK, ulMask);
                }

		// lower PIO strobe(s)
                if (dpio2_ControlPioStrobes(hDpio2, 0) != OK)
                        Run = 0;

		// transfer the frame buffer into the DPIO2 FIFO
 		status = dpio2_DmaTransfer(hDpio2, (UINT32) (iLength*2), (char *) Data[idx]); 
                if (status != OK)
		{
		   if (iErrorMode)
                   {
                      printf("DMA overrun: loop halted.\n");
		      printf("Lower the loop frequency or use \"halterr\" command to override.\n");
		      Run = 0;
                   }
                   else
  	              // just count skipped frames id iErrorMode=0
		      dwDmaSkipCount++;
		}
		else
		   // toggle the ping/pong buffer if the DMA was succesful
 		   idx = idx?0:1;

		// de-assert sync pulse at printer port
          	if (fdp != -1)	
		{
                	unsigned long ulMask[2] = {0xffffffff, 0x00000000};
                	iRet = ioctl(fdp, RTL_CLEARSETBITMASK, ulMask);
        	}

		Step = 0;
	}
}

// RT-Linux Thread
// Execute worker function at regular intervals (set by iTick through "freq" command)
// Protect against loop hang by checking current now+5us time and avoid calling clock_nanosleep()
// if we are past the time slot; run while Term!=0
void *thread_code(void *t)
{
	struct timespec next;
        struct timespec now;
	#ifdef MDL_DEBUG
		printf("RTL Thread start\n");
	#endif

	/* get the current time and setup for the first tick */
	clock_gettime( CLOCK_REALTIME, &next);

	while (Term == 0)
	{
		/* set the period for the loop */
		timespec_add_ns( &next, iTick);
		/* sleep */
                clock_gettime( CLOCK_REALTIME, &now);

                timespec_add_ns( &now, 5000);

		if (timespec_gt(&next, &now))
			clock_nanosleep( CLOCK_REALTIME, TIMER_ABSTIME, &next, NULL);

		rt_timer_callback();
	}

	#ifdef MDL_DEBUG
		printf("RTL Thread exit\n");
	#endif
	return NULL;
}

// RT-Linux Main Entry Point: laucnhed by rtcli
// Performs initialization before launching real-time thread
int main(void)
{
	int iRet = 0;
	STATUS status;
	#ifdef MDL_DEBUG
		printf("RTL Main start\n");
	#endif

	// open LPT for diagnostic strobes
	fdp = open("/dev/lpt0", O_WRONLY);
	if (fdp == -1) {
		printf("parport0 open failed %d\n", errno);
	}
	else
	{
		// reset LPT bits
		unsigned long ulMask[2] = {0xffffffff, 0x00000000};
		iRet = ioctl(fdp, RTL_CLEARSETBITMASK, ulMask);
		if (iRet == -1) 
        		printf("ioctl lp0 failed\n");
	}

	// allocate memory here
        rtl_shm_unlink("/tmp/shm_dma_test");
	// designate a PHYSICAL allocation
        dma_fd = rtl_shm_open("/tmp/shm_dma_test", RTL_O_CREAT|RTL_O_PHYS, 0777);
        if (dma_fd == -1) {
                rtl_printf("shm_open failed on /tmp/shm_dma_test (%d)\n",
                        rtl_errno);
                return -1;
        }
        // now get a good chunk of DMAable memory
        if (rtl_ftruncate(dma_fd, MMAP_SIZE) == -1) {
                rtl_printf("ftruncate should have succeeded "
                        "on /tmp/shm_dma_test (%d)\n",rtl_errno);
                return -1;
        }
	// map to get a user handle
        dma_base = (unsigned char *)rtl_mmap(0,MMAP_SIZE,RTL_PROT_READ|RTL_PROT_WRITE,
                        RTL_MAP_SHARED,dma_fd,0);
        if (dma_base == RTL_MAP_FAILED) {
                rtl_printf("Failed DMA mmap() (%d)\n",rtl_errno);
                return -1;
        }

#ifdef MDL_DEBUG
        rtl_printf("dma_base/size = %08x/%d\n", dma_base, MMAP_SIZE);
#endif
	// clear the data buffer
	// assign the double ping/pong buffer pointers
        rtl_memset(dma_base, 0, MMAP_SIZE);
	Data[0] = (unsigned short int *)  dma_base;
        Data[1] = (unsigned short int *) (dma_base+(MMAP_SIZE/2));

#ifdef MDL_DEBUG
        rtl_printf("buf0 = %08x, buf1 = %08x\n", Data[0], Data[1]);
#endif

        // remove the FIFO (in case previous program crash left it)
        unlink( "/tmp/mdlfifo" );

	// make a the FIFO that is available to RTLinux and Linux applications
	iRet = mkfifo( "/tmp/mdlfifo", 0777);
	if (iRet) {
		printf("mkfifo failed %d\n", errno);
		return 0;
	}
		
#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test1\n");  //dwp100209
#endif					    //dwp100209
	// open the FIFOs
	fd1 = open( "/tmp/mdlfifo", O_RDONLY | O_NONBLOCK );
	if (fd1 == -1) {
		printf("open failed %d\n", errno);
		return 0;
	}

#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test2\n");  //dwp100209
#endif					    //dwp100209
	// make the FIFO larger - 16kbytes 
	iRet = ioctl(fd1, RTF_SETSIZE, 16<<10);
	if (iRet == -1) {
		printf("ioctl failed\n");
		return 0;
	}

        // open the dpio2
        hDpio2 = dpio2_Open (0);
        if (hDpio2 == NULL) 
	{
        	printf("dpio2_Open failed\n");
                return 0;
        }
//6	printf("*** Returning prematurely 6 ***\n");  //XXXdwp100209 -- for testing -- remove it !!
//6	return 0;								    //XXXdwp100209 -- for testing -- remove it !!

#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test3\n");  //dwp100209
#endif					    //dwp100209

 	// configure PIO strobes	
	status = dpio2_ConfigurePioStrobes(hDpio2, 1);
//7	printf("*** Returning prematurely 7 ***\n");  //XXXdwp100209 -- for testing -- remove it !! -- oops is occurring in 
//7	return 0;								    //XXXdwp100209 -- for testing -- remove it !! -- dpio2_ConfigurePioStrobes()
	if (status != OK)
	{
		printf("dpio2_ConfigurePioStrobes failed\n");
		return 0;
	}
#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test4\n");  //dwp100209
#endif					    //dwp100209
	// set DPIO2 hardware for output operation
	status = dpio2_InitializeForOutput(hDpio2);
        if (status != OK)
        {
                printf("dpio2_InitializeForOutput failed\n");
                return 0;
        }
#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test5\n");  //dwp100209
#endif					    //dwp100209
	// acivate front panel interface
	status = dpio2_ActivateFpdpInterface(hDpio2);
        if (status != OK)
        {
                printf("dpio2_ActivateFpdpInterface failed\n");
                return 0;
        }
#ifdef MDL_DEBUG		    //dwp100209					
    rtl_printf("Test6\n");  //dwp100209
#endif					    //dwp100209
	// create the real-time thread
	rtl_pthread_create( &thread, NULL, thread_code, (void *)0 );
        // wait for the thread to exit or an asynchronous signal to stop us
        rtl_pthread_join( thread, NULL );
	#ifdef MDL_DEBUG
		printf("RTL Main Cleanup\n");
	#endif

	// close the FIFO
	close( fd1 );
	// close the LPT port
	close ( fdp );
	// close mapped memory
        rtl_munmap(dma_base, MMAP_SIZE);
        rtl_close(dma_fd);
        rtl_shm_unlink("/tmp/shm_dma_test");
	// set strobes low, turn off
        status = dpio2_ConfigurePioStrobes(hDpio2, 0);
	// close the dpio2
	dpio2_Close(hDpio2);
	// remove the FIFO 
	unlink( "/tmp/mdlfifo" );
	return 0;
}
