/*
 * DPIO2 RT-Linux PLL Oscillator Interface for Strobe Generation
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#ifdef CONFIG_RTL_BSD
#include <sys/rtl_ioctl.h>
#include <rtl_fcntl.h>
#endif
#include <string.h>
#include "shared.h"

#define VMOSA
#include "vmosa-linux.h"
#include "dpio2-linux.h"
#include "dpio2-linux-user-space.h"

#include "dpio2.h"
#include "Dpio2Defs.h"

//#define MDL_DEBUG
#define DPIO2_ERROR_MSG printf

const int   NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD = 10;
UINT32 m_referenceFrequency = 10*1000*1000;
UINT32 m_programmedFrequency = 0;

// forward declarations (this file)
static void ProgramClockBit(HANDLE hHandle, int iValue);
static void ProgramDataBit(HANDLE hHandle, int iValue);
static void SetControlWord(HANDLE hHandle, UINT32 word);
static void SetProgramWord(HANDLE hHandle, UINT32 word);
static STATUS SetFrequency(HANDLE hHandle,
                    UINT32 counterP,
                    UINT32 dutyCycleAdjustUp,
                    UINT32 mux,
                    UINT32 counterQ,
                    UINT32 index);

// called from rt_dpio2_funcs.c
STATUS dpio2SetFrequency(HANDLE hHandle,
                         UINT32 requestedFrequency, 
                         UINT32 *presultingFrequency)
{
  const UINT32   MIN_VCO_FREQUENCY = (50*1000*1000);
  const UINT32   INDEX_LIMIT_VCO_FREQUENCY = (80*1000*1000);
  const UINT32   MAX_VCO_FREQUENCY = (150*1000*1000);
  const UINT32   MIN_REFERENCE_FREQUENCY_SCALED_BY_Q = (200 * 1000);
  const UINT32   MAX_REFERENCE_FREQUENCY_SCALED_BY_Q = (1 * 1000 * 1000);
  const UINT32   MIN_OUT_FREQUENCY = 3125;
  const UINT32   MAX_OUT_FREQUENCY = (100*1000*1000);
  const UINT32   MIN_Q_COUNTER_VALUE = 1;
  const UINT32   MAX_Q_COUNTER_VALUE = 127;
  const UINT32   MIN_P_COUNTER_VALUE = 1;
  const UINT32   MAX_P_COUNTER_VALUE = 127;
  const UINT32   MAX_MUX_VALUE = 7;

  STATUS   status;
  UINT32   counterP;
  UINT32   counterQ;
  UINT32   mux;
  UINT32   maxCounterQ;
  UINT32   minCounterQ;
  UINT32   bestCounterP = 0;
  UINT32   bestCounterQ = 0;
  UINT32   bestMux = 0;
  UINT32   bestIndex = 0;
  UINT32   vcoFrequency;
  UINT32   generatedFrequency;
  UINT32   deviation;
  UINT32   bestDeviation;
  UINT32   bestResultingFrequency = 0;

  status = OK;

  // Check that the parameters are valid
  if ( requestedFrequency < MIN_OUT_FREQUENCY ) 
  {
    DPIO2_ERROR_MSG("Requested frequency (%d Hz) is lower than the minimum (%d Hz).\n",
               (int) requestedFrequency, (int) MIN_OUT_FREQUENCY);
    status = ERROR;
  }

  if ( requestedFrequency > MAX_OUT_FREQUENCY ) 
  {
    DPIO2_ERROR_MSG("Requested frequency (%d Hz) is higher than the maximum (%d Hz).\n",
               (int) requestedFrequency, (int) MAX_OUT_FREQUENCY);
    status = ERROR;
  }

  if ( status == OK ) 
  {
    // Find the lowest valid VCO frequency (in the range between 50MHz and 150MHz).
    mux = 0;
    vcoFrequency = requestedFrequency;
    while ( (vcoFrequency < MIN_VCO_FREQUENCY) && (mux <= MAX_MUX_VALUE) )
    {
      vcoFrequency *= 2;
      mux++;
    }
#ifdef MDL_DEBUG
    printf("Min. VCO frequency = %d MHz\n", (int) vcoFrequency);
#endif

    // Set best deviation to the requested frequency to be sure that
    // all computed values will be better.
    bestDeviation = requestedFrequency;

    // Try to compute parameters for all applicable VCO frequencies.
    while ( vcoFrequency <= MAX_VCO_FREQUENCY )
    {
      // Compute the maximum and minimum values that can be used for Q counter.
      maxCounterQ = m_referenceFrequency / MIN_REFERENCE_FREQUENCY_SCALED_BY_Q;
      if ( maxCounterQ > MAX_Q_COUNTER_VALUE )
      {
        maxCounterQ = MAX_Q_COUNTER_VALUE;
      }

      minCounterQ = ( (m_referenceFrequency + (MAX_REFERENCE_FREQUENCY_SCALED_BY_Q - 1)) 
                      / MAX_REFERENCE_FREQUENCY_SCALED_BY_Q);
      if ( minCounterQ < MIN_Q_COUNTER_VALUE )
      {
        minCounterQ = MIN_Q_COUNTER_VALUE;
      }

      // Go through all valid values for Q counter and compute the value for P counter
      // that results in the VCO frequency closest to the desired frequency.
      for (counterQ = minCounterQ; counterQ <= maxCounterQ; counterQ++)
      {
        /* When computing the value for P counter, the reference frequency is added
         * in the nominator in order to round the result.
         * To avoid overflow the follwing formula is used:
         *
         *   ((A * B) + C) / D = A * (B / D) + ((A * (B % D) + C) / D)
         */
        counterP = 
          ((counterQ + 2) * (vcoFrequency / (2 * m_referenceFrequency)))
          + (((counterQ + 2) * (vcoFrequency % (2 * m_referenceFrequency)) + m_referenceFrequency)
             / (2 * m_referenceFrequency)) - 3;

        if ( counterP < MIN_P_COUNTER_VALUE )
	{
          counterP = MIN_P_COUNTER_VALUE;
        }
        else if ( counterP > MAX_P_COUNTER_VALUE )
        {
          counterP = MAX_P_COUNTER_VALUE;
        }
      
        /* Compute the frequency that will be generated and the deviation
         * from the requested frequency.
         * To avoid overflow the follwing formula is used:
         *
         *   (A * B) / C = A * (B / C) + (A * (B % C)) / C
         */
        generatedFrequency = 
          ((counterP + 3) * ((2 * m_referenceFrequency) / ((counterQ + 2) * (1 << mux))))
          + (((counterP + 3) * ((2 * m_referenceFrequency) % ((counterQ + 2) * (1 << mux))))
             / ((counterQ + 2) * (1 << mux)));

        if ( generatedFrequency < requestedFrequency )
	{
          deviation = requestedFrequency - generatedFrequency;
        }
	else
	{
          deviation = generatedFrequency - requestedFrequency;
        }

        if ( deviation < bestDeviation )
	{
          bestMux = mux;
          bestCounterQ = counterQ;
          bestCounterP = counterP;
          bestIndex = (vcoFrequency <= INDEX_LIMIT_VCO_FREQUENCY) ? 0 : 8;
          bestDeviation = deviation;
          bestResultingFrequency = generatedFrequency;
#ifdef MDL_DEBUG
          printf ("Q=%3d, P=%3d, M=%d --> %8dHz\n", 
                     (int) (counterQ + 2), 
                     (int) (counterP + 3), 
                     (int) mux, 
                     (int) generatedFrequency);
#endif
        }
      }
      vcoFrequency *= 2;
      mux++;
    }

#ifdef MDL_DEBUG
    printf("Best P Counter Value: %d\n", (int) (bestCounterP + 3));
    printf("Best Q Counter Value: %d\n", (int) (bestCounterQ + 2));
    printf("Best Mux value      : %d\n", (int) bestMux);
    printf("Best Index value    : %d\n", (int) bestIndex);
    printf("Deviation           : %dHz\n", (int) bestDeviation);
#endif

    SetFrequency(hHandle, bestCounterP, 1, bestMux, bestCounterQ, bestIndex);
    // Return and store the resulting frequency.
    m_programmedFrequency = bestResultingFrequency;
    *presultingFrequency = bestResultingFrequency;
  }

  /* Update the attribute for strobe frequency range, and update the configuration
   * of FIFO frequency and PLL if strobe generation is enabled.
   */

  DPIO2_STROBE_FREQUENCY_RANGE Range;

  if ( bestResultingFrequency <= (5*1000*1000) ) {
    Range = BELOW_OR_EQUAL_TO_5MHZ;
  } else if ( bestResultingFrequency <= (10*1000*1000) ) {
    Range = ABOVE_5MHZ_BELOW_OR_EQUAL_TO_10MHZ;
  } else  if ( bestResultingFrequency <= (25*1000*1000) ) {
    Range = ABOVE_10MHZ_BELOW_OR_EQUAL_TO_25MHZ;
  } else  if ( bestResultingFrequency <= (50*1000*1000) ) {
    Range = ABOVE_25MHZ_BELOW_OR_EQUAL_TO_50MHZ;
  } else  if ( bestResultingFrequency <= (100*1000*1000) ) {
    Range = ABOVE_50MHZ_BELOW_OR_EQUAL_TO_100MHZ;
  } else {
    Range = ABOVE_100MHZ;
  }

  BOOL strobeFrequencyIsBelow10Mhz = (Range == BELOW_OR_EQUAL_TO_5MHZ) || (Range == ABOVE_5MHZ_BELOW_OR_EQUAL_TO_10MHZ);

  dpio2_kio_t ioc;
  ioc.offset   = DPIO2_CONFIGURATION_REGISTER;
  ioc.bit_mask = (1 << DPIO2_BIT_NUM_ENABLE_PLL)   | (1 << DPIO2_BIT_NUM_PLL_FS_MID)    | (1 << DPIO2_BIT_NUM_PLL_FS_HIGH)  |
		 (1 << DPIO2_BIT_NUM_PLL_OPT1_MID) | (1 << DPIO2_BIT_NUM_PLL_OPT1_HIGH) | (1 << DPIO2_BIT_NUM_PLL_OPT0_MID) |
                 (1 << DPIO2_BIT_NUM_PLL_OPT0_HIGH);

  ioc.u.dword  = 0;

  if (!strobeFrequencyIsBelow10Mhz)
  {
    ioc.u.dword |= (1 << DPIO2_BIT_NUM_ENABLE_PLL);

    // Determine how to set the Frequency Range of the PLL.
    switch (Range)
    {
    case BELOW_OR_EQUAL_TO_5MHZ:
    case ABOVE_5MHZ_BELOW_OR_EQUAL_TO_10MHZ:
    case ABOVE_10MHZ_BELOW_OR_EQUAL_TO_25MHZ:
      // DPIO2_BIT_NUM_PLL_FS_MID  = 0
      // DPIO2_BIT_NUM_PLL_FS_HIGH = 0
      break;

    case ABOVE_25MHZ_BELOW_OR_EQUAL_TO_50MHZ:
      // DPIO2_BIT_NUM_PLL_FS_MID = 1;
      ioc.u.dword |= (1 << DPIO2_BIT_NUM_PLL_FS_MID);
      break;

    case ABOVE_50MHZ_BELOW_OR_EQUAL_TO_100MHZ:
      // DPIO2_BIT_NUM_PLL_FS_MID  = 0
      // DPIO2_BIT_NUM_PLL_FS_HIGH = 1
      ioc.u.dword |= (1 << DPIO2_BIT_NUM_PLL_FS_HIGH);
      break;

    case ABOVE_100MHZ:
      DPIO2_ERROR_MSG("Warning: PLL is not designed for strobe frequency above 100MHz\n");
      // DPIO2_BIT_NUM_PLL_FS_MID  = 0
      // DPIO2_BIT_NUM_PLL_FS_HIGH = 1
      ioc.u.dword |= (1 << DPIO2_BIT_NUM_PLL_FS_HIGH);
      break;

    default:
      DPIO2_ERROR_MSG("Warning: Unknown frequency range (%d)\n", Range);
      break;
    }

    status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );
    if ( status != OK ) {
      DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
      return ERROR;
    }
  }

  // Delay 1ms to allow the PLL to lock properly.
  struct timespec next;
  clock_gettime( CLOCK_REALTIME, &next);
  timespec_add_ns( &next, 1000000);
  /* sleep */
  clock_nanosleep( CLOCK_REALTIME, TIMER_ABSTIME, &next, NULL);

  return (status);
}

static STATUS SetFrequency(HANDLE hHandle, UINT32 counterP,
                    UINT32 dutyCycleAdjustUp,
                    UINT32 mux,
                    UINT32 counterQ,
                    UINT32 index)
{

  STATUS   status;
  UINT32   programmingWord;

  // Check that the parameters are valid.
  status = OK;
  if ( (counterP < 1) || (counterP > 127) ) {
    DPIO2_ERROR_MSG("Invalid P Counter value (%d)\n", (int) counterP);
    status = ERROR;
  }

  if ( (dutyCycleAdjustUp != 0) && (dutyCycleAdjustUp != 1) ) {
    DPIO2_ERROR_MSG("Invalid Duty Cycle Adjust Up value (%d)\n", (int) dutyCycleAdjustUp);
    status = ERROR;
  }
              
  if ( mux > 7 ) {
    DPIO2_ERROR_MSG("Invalid Mux value (%d)\n", (int) mux);
    status = ERROR;
  }

  if ( (counterQ < 1) || (counterQ > 127) ) {
    DPIO2_ERROR_MSG("Invalid Q Counter value (%d)\n", (int) counterQ);
    status = ERROR;
  }

  if ( (index != 0) && (index != 8) ) {
    DPIO2_ERROR_MSG("Invalid Index value (%d)\n", (int) index);
    status = ERROR;
  }

  // Program frequency if all parameters specifying it are valid.
  if ( status == OK ) {
    /* Load the Control Register to enable MUXREF and enable loading
     * of the Program Register. This will set the output to the reference
     * frequency. The transition is guaranteed to be glitch-free.
     */
    SetControlWord(hHandle, 0x00000005);

    /* Shift in the desired output frequency value computed 
     * via a 22-bit data word, plus any bit-stuffs.
     */
    programmingWord 
      = (counterP << 15) | (dutyCycleAdjustUp << 14) | (mux << 11) | (counterQ << 4) | index;

    SetProgramWord(hHandle, programmingWord);

    /* Load the Control Register to enable MUXREF and disable
     * loading of the Program register. This loads the Program
     * word bits into the Program register and keeps the output
     * set to the reference frequency while the new frequency
     * settles.
     */
    SetControlWord(hHandle, 0x00000004);

    /* Wait for VCO to settle in the new state.
     */
    struct timespec next;
    clock_gettime( CLOCK_REALTIME, &next);
    timespec_add_ns( &next, 100*1000*1000);
    /* sleep */
    clock_nanosleep( CLOCK_REALTIME, TIMER_ABSTIME, &next, NULL);

    /* Load the Control register to enable new frequency output.
     * The transition is guaranteed to be glitch-free.
     */
    SetControlWord(hHandle, 0x00000000);
  }
  return (status);
}

static void SetControlWord(HANDLE hHandle, UINT32 word)
{
  int   cBit;
  int   i;

  // Add the Protocol Word.
  word &= 0x000000FF;
  word |= 0x00001E00;

  // Shift out the word, starting with LSB.
#ifdef MDL_DEBUG
  printf("Shifting out control word: ");
#endif
  for (cBit = 0; cBit < 14; cBit++) {
    // Set the serial data bit.
    if ( (word & 0x00000001) == 0x00000001 ) {
      ProgramDataBit(hHandle, 1);
#ifdef MDL_DEBUG
      printf("1");
#endif
    } else {
      ProgramDataBit(hHandle, 0);
#ifdef MDL_DEBUG
      printf("0");
#endif
    }

    /* Make a low-to-high transition on the serial clock bit.
     * Repeat both clearing and setting to ensure that the clock
     * signal is low for at least 450ns and high for at least 450ns.
     */
    for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
      ProgramClockBit(hHandle, 0);
    }
    for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
      ProgramClockBit(hHandle, 1);
    }
    word >>= 1;
  }
#ifdef MDL_DEBUG
  printf("\n");
#endif
}

static void SetProgramWord(HANDLE hHandle, UINT32 word)
{
  int   cBit;
  int   numSequentialOnes;
  int   i;
  numSequentialOnes = 0;

  // Shift out the word, starting with LSB.
#ifdef MDL_DEBUG
  printf("Shifting out program word: ");
#endif
  for (cBit = 0; cBit < 22; cBit++) {
    /* Set the serial data bit.
     */
    if ( (word & 0x00000001) == 0x00000001 ) {
      ProgramDataBit(hHandle, 1);
      numSequentialOnes++;
#ifdef MDL_DEBUG
      printf("1");
#endif
    } else {
      ProgramDataBit(hHandle, 0);
      numSequentialOnes = 0;
#ifdef MDL_DEBUG
      printf("0");
#endif
    }

    /* Make a low-to-high transition on the serial clock bit.
     * Repeat both clearing and setting to ensure that the clock
     * signal is low for at least 450ns and high for at least 450ns.
     */
    for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
      ProgramClockBit(hHandle, 0);
    }
    for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
      ProgramClockBit(hHandle, 1);
    }

    // If three sequential 1s have been sent, add a stuff-bit.
    if ( numSequentialOnes == 3 ) {
      ProgramDataBit(hHandle, 0);
     
#ifdef MDL_DEBUG 
      printf("0");
#endif
      /* Make a low-to-high transition on the serial clock bit.
       * Repeat both clearing and setting to ensure that the clock
       * signal is low for at least 450ns and high for at least 450ns.
       */
      for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
        ProgramClockBit(hHandle, 0);
      }
      for (i = 0; i < NUM_REPETIONS_TO_ENSURE_MINIMUM_CLOCK_PERIOD; i++) {
        ProgramClockBit(hHandle, 1);
      }

      numSequentialOnes = 0;
    }
    word >>= 1;
  }
#ifdef MDL_DEBUG
  printf("\n");
#endif
}

static void ProgramClockBit(HANDLE hHandle, int iValue)
{
  STATUS status = OK;
  dpio2_kio_t ioc;
  ioc.offset   = DPIO2_CONFIGURATION_REGISTER;
  ioc.bit_mask = (1 << DPIO2_BIT_NUM_CLOCK_PROGRAMMABLE_OSCILLATOR);
  ioc.u.dword  = iValue ? ioc.bit_mask : 0 ;
  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return;
  }
}

static void ProgramDataBit(HANDLE hHandle, int iValue)
{
  STATUS status = OK;
  dpio2_kio_t ioc;
  ioc.offset   = DPIO2_CONFIGURATION_REGISTER;
  ioc.bit_mask = (1 << DPIO2_BIT_NUM_DATA_PROGRAMMABLE_OSCILLATOR);
  ioc.u.dword  = iValue ? ioc.bit_mask : 0 ;
  status = ioctl ( hHandle->fd, VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC, &ioc );

  if ( status != OK ) {
    DPIO2_ERR_MSG (("VMOSA_REG_32_READ_MODIFY_WRITE_ATOMIC failed! status = %d, errno =%d\n", status, errno));
    return;
  }
}
