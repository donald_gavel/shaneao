all:	rtcli mkdata data1024.txt data4096.txt
##	cp rtcli $(HOME)
##	cp data1024.txt $(HOME)
##	cp data4096.txt $(HOME)

rtcli:	rtcli.c ../dpio2_include/shared.h
	cc -o rtcli -I../dpio2_include rtcli.c

mkdata: mkdata.c
	cc -o mkdata -I../dpio2_include mkdata.c

data1024.txt: mkdata
	rm -f data1024.txt
	rm -f data4096.txt
	./mkdata

clean:
	rm -f rtcli
	rm -f mkdata
	rm -f data1024.txt
	rm -f data4096.txt
