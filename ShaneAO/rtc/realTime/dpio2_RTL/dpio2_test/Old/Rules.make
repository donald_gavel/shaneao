#
# Copyright (c) 1995-2006, 2007, Wind River Systems, Inc.
# 
# The right to copy, distribute, modify, or otherwise make use
# of this software may be licensed only pursuant to the terms
# of an applicable Wind River license agreement. 
# # 

ifneq ($(wildcard $(RTL_DIR)/.config),)
include $(RTL_DIR)/.config
endif

ORIG_PATH	:= $(PATH)
PATH	:= $(DEVKIT_DIR)bin:$(PATH)
LOCAL_SYMBOLS	:= -Lmain -L__rtcore_app
ifeq ($(CONFIG_RTL_NETBSD),y)
LOCAL_SYMBOLS	+= -Lrtl_main_init_module -Lrtl_main_cleanup_module
endif

%.kext: %.o
	@sudo rm -rf $@
	mkdir -p $@/Contents/MacOS
	$(CC) $< -arch ppc -static -nostdlib -r -lcc_kext -o $@/Contents/MacOS/$*
	cp $(RTL_DIR)/include/gpos_bridge/darwin/plist $@/Contents/Info.plist
	@sed 's/BUNDLEID/com.windriver.'$*'/g' $@/Contents/Info.plist > tmp
	@mv -f tmp $@/Contents/Info.plist
	@sed 's/EXEC/'$*'/g' $@/Contents/Info.plist > tmp
	@mv -f tmp $@/Contents/Info.plist
	@sed 's/VERSION/'$(RTL_VERSION_MAJOR).$(RTL_VERSION_MINOR).`echo	$(RTL_VERSION_EXTRA)|sed 's/-.*//g'`'/g' $@/Contents/Info.plist > tmp
	@mv -f tmp $@/Contents/Info.plist
	sudo chown -R root:wheel $@
	sudo chmod -R 644 $@

%.rtl: %.o
	$(CC) $(CFLAGS_RTCORE_NAMES) $(CFLAGS_$@) -DKMOD=RTL_MODULE\($(shell basename $@|sed -e s/\\\.rtl//)\) \
		-c $(RTL_DIR)/mainhelper/main_crt0.c -o $@.crt0.o
	$(LD) -r -o $<.mainmod $< $(RTL_LIBS_DIR)/rtl_crt0.o $@.crt0.o $(LDFLAGS_$@) $(LDFLAGS) 
	$(OBJCOPY) $(LOCAL_SYMBOLS) $<.mainmod $<.mainmod
ifeq ($(CONFIG_RTL_FREEBSD),y)
ifeq ($(CONFIG_RTL_FREEBSD_VERSION5),y)
	ld -d -warn-common -r -d -o $<.kld $<.mainmod
	touch $<.export_syms
	awk -f $(GPOS_DIR)/conf/kmod_syms.awk $<.kld $<.export_syms |  xargs -J% objcopy % $<.kld
	ld -Bshareable  -d -warn-common -o $<.ko $<.kld
	objcopy --strip-debug $<.ko
	rm -f $<.kld $<.export_syms
else
	@mv -f $<.mainmod $<.elf
	gensetdefs $<.elf
	$(CC) $(CFLAGS) -c setdef0.c
	$(CC) $(CFLAGS) -c setdef1.c
	$(LD) -Bshareable -o $<.ko setdef0.o $<.elf setdef1.o
	@rm -f setdef*.[cho]
endif
	@mv -f $<.ko $<.mainmod
endif
	cp $< $*.o.debug
ifeq ($(CONFIG_RTL_LINUX26),y)
	$(CC) -c -D__KERNEL__ -DMODULE $(CFLAGS) -DKBUILD_MODNAME=$(shell basename $@|sed -e s/\\\.rtl//) $(RTL_DIR)/mainhelper/module_stub.c -o $<.stub.o
	@mv -f $<.mainmod $<.elf
	$(LD) -r -o $<.mainmod $<.elf $<.stub.o $(LDFLAGS)
	cp $<.mainmod $*.o.debug
endif
	$(OBJCOPY) --strip-debug $<.mainmod
	@rm -f $<.mod $@.crt0.o $<.elf
	$(OBJCOPY) $(OBJCOPY_ARGS) \
		--add-section=.image=$<.mainmod \
		$(RTL_LIBS_DIR)/user.o $@.user.o
	@rm -f $<.mainmod
	$(LD) -r $(RTL_DIR)/mainhelper/ld.user $@.user.o -o $@.final.o
	$(CC) -o $@ $@.final.o
	@rm -f $@.final.o $@.user.o
	$(STRIP) $@

%.o: %.S
	$(CC) $(CFLAGS) $(CFLAGS_$@) -c -o $@ $<

%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(CXXFLAGS_$@) -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) $(CFLAGS_$@) -o $@ -c $<

ifeq ($(CONFIG_RTL_FREEBSD),y)
%.ko: %.o
ifeq ($(CONFIG_RTL_FREEBSD_VERSION5),y)
	ld -d -warn-common -r -d -o $<.kld $<
	touch $<.export_syms
	awk -f $(GPOS_DIR)/conf/kmod_syms.awk $<.kld $<.export_syms |  xargs -J% objcopy % $<.kld
	ld -Bshareable  -d -warn-common -o $@ $<.kld
	objcopy --strip-debug $@
	rm -f $<.kld $<.export_syms
else
	gensetdefs $<
	$(CC) $(CFLAGS) -c setdef0.c
	$(CC) $(CFLAGS) -c setdef1.c
	$(LD) -Bshareable  -o $@ setdef0.o $< setdef1.o
	rm -f setdef0.c setdef1.c setdefs.h setdef0.o setdef1.o symb.tmp tmp.o
endif
else
ifeq  ($(CONFIG_RTL_LINUX26),y)
%.ko: %.o
	$(CC) -c -D__KERNEL__ -DMODULE $(CFLAGS) -DKBUILD_MODNAME=$* $(RTL_DIR)/mainhelper/module_stub.c -o $<.stub.o
	$(LD) -r -o $@ $< $<.stub.o $(LDFLAGS_$@) $(LDFLAGS)
else
%.ko: %.o
	cp -f $< $@
endif
endif

%.o: %.psddmod.c
	$(CC) $(CFLAGS) $(CFLAGS_$@) -o $@ -c $<

%: %.psdd.c
	$(USER_CC) $(USER_CFLAGS) $(CFLAGS_$@) -static -o $@ $< \
		-L$(RTL_LIBS_DIR) -lpsdd -lm

#
# Don't allow .o's to be deleted if they were created so we could
# build a .ko file.  This allows us to just target .ko and get the
# .o built for us -- Cort <cort@fsmlabs.com>
#		
.PRECIOUS: %.o

%: %.psddmod.c
	$(USER_CC) $(USER_CFLAGS) -static -o $@ \
		$(RTL_LIBS_DIR)/psddmain.o $< -L$(RTL_LIBS_DIR) -lpsdd -lm

%: %.c
	$(USER_CC) $(USER_CFLAGS) -o $@ $<

.DELETE_ON_ERROR:

.d.%.cpp: %.cpp
	set -e; $(CXX) -M $(CXXFLAGS) $(CXXFLAGS_$@) $< \
		| sed 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@; \
		[ -s $@ ] || rm -f $@

.d.%.c: %.c
	@set -e; $(CC) -M $(CFLAGS) $(CFLAGS_$@) $< \
		| sed 's/\($*\)\.o[ :]*/\1.o $@ : /g' > $@; \
		[ -s $@ ] || rm -f $@

ifneq ($(wildcard .d.*),)
include $(wildcard .d.*)
endif
