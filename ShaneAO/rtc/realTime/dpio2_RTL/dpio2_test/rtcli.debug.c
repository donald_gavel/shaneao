/*
 * DPIO2 RT-Linux Command Line Interface (RTCLI)
 *
 * Author: Michael A. DeLuca
 * MDL Enterprises
 * m.deluca@comcast.net
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <math.h>
#include <unistd.h>
#include <string.h>

#include "shared.h"			// shared command codes with RT task
#include "mapdata.h"

#define	STATE_RUN	0
#define STATE_STOP	1

static int   end     = 0;		// termination flag	
static int   iMode   = 1024;		// startup mode is 1024 actuators
static int   iLL     = 0;		// Low Latency mode for 1K off
static int   iState  = STATE_STOP;	// default value for start/stop is stopped
static long  iRate   = 1200;		// default frame rate frequency is 1200 Hz
static short iStrobe = 50;		// default strobe rate frequency is 50 MHz
static short iFixed  = 0;		// default strobe is programmable

#define MAX_COMMAND_ARGS 32		// command line processing
#define COMMAND_LENGTH 128
#define MDL_DEBUG  //dwp100209

short sActValue[FRAME_SIZE];

// termination function for SIGINT callback
static void endme(int dummy) { end = 1;}
int FindActuatorIndex(int *Map, int iCount, int iActuator);

// help function prints this menu
void PrintMenu()
{
	printf("RT-Linux DPIO2 Command line Interface\n");
        printf("Version 1.4, %s\n", __DATE__ ", " __TIME__); 
	if (iFixed)
           printf("Mode: %d%s (%s), Rate: %d Hz, Fixed Strobe: 13.8 MHz\n\r", 
			iMode, iLL?"LL":"", iState?"Stopped":"Running", iRate);
	else
           printf("Mode: %d%s (%s), Rate: %d Hz, Programmable Strobe: %d MHz\n\r",
                        iMode, iLL?"LL":"", iState?"Stopped":"Running", iRate, iStrobe);

        printf("---------------------------------------------------\n\r");
        printf("Command      Meaning\n\r");
        printf("---------------------------------------------------\n\r");
        printf("run           Run framing loop\n\r");
        printf("stop          Stop framing loop\n\r");
	printf("step          Step framing loop\n\r");
        printf("mode <1|L|4>  Select 1K, 1KLL, or 4K actuator mode\n\r");
        printf("freq <Hz>     Select frame rate (10 - 70000) Hz\n\r");
	printf("pstrobe <MHz> Select programmable strobe rate (10 - 50) MHz\n\r");
	printf("fstrobe       Select fixed strobe rate (13.8 MHz)\n\r");
        printf("poke <N> <V>  Poke actuator <N> with Value <V>\n\r");
        printf("clear         Clear all actuators to default values\n\r");
        printf("load <file>   Load all actuators from an ASCII file\n\r");
        printf("test          Load all actuators with a test pattern\n\r");
        printf("halterr <0|1> Enable/disable halt on loop error\n\r");
        printf("quit          Quit the program\n\r");
        printf("help          Print this menu\n\r");
}

// main entry (no args used in command line)
int main(int argc,char *argv[])
{
    char SendByte;
    char Commandline[COMMAND_LENGTH], LastCommand[COMMAND_LENGTH];
    int cmd= -1, i = 0, j = 0, ArgCount = 0, Length = 0;
    char *Arg[MAX_COMMAND_ARGS];
   
    // launch the RT-Linux companion program
    pid_t pid;
    int err;
    char *spawnedArgs[] = {"./rt_dpio2_test.rtl" ,NULL};
    char *spawnedEnv[] = {NULL};

    //printf("Parent process id=%ld\n", getpid());
    if( (err=posix_spawn(&pid, spawnedArgs[0], NULL, NULL,
         spawnedArgs, spawnedEnv)) !=0 )
    printf("posix_spawn() error=%d\n",err), exit(-1);
    //printf("Child process  id=%ld\n", pid);

    // wait for launch
    sleep(2);

    // clear out command lines
    memset(Commandline, ' ', COMMAND_LENGTH);
    memset(LastCommand, ' ', COMMAND_LENGTH);

    // initialize last command to default command
    strcpy(LastCommand, "stop");

    // register interrupt signal to terminate the main loop
    signal(SIGINT, endme);

    // open RT FIFO to communicate to the real-time task
    if ((cmd = open("/tmp/mdlfifo", O_WRONLY)) < 0)
    {
	fprintf(stderr, "Error opening /tmp/mdlfifo\n");
	exit(1);
    }

    // send stop
    iState = STATE_STOP;
    SendByte = STOP_CMD;
    write(cmd, &SendByte, 1);

    // send mode = 1024
    iMode = 1024;
    SendByte = MODE_CMD;
    write(cmd, &SendByte, 1);
    SendByte = iMode;
    write(cmd, &SendByte, 1);

    // send error mode = 1
    SendByte = HERR_CMD;
    write(cmd, &SendByte, 1);
    SendByte = 1;
    write(cmd, &SendByte, 1);

    // send frequency = 1200 Hz
    iRate = (long) 1200;
    SendByte = RATE_CMD;
    write(cmd, &SendByte, 1);
    write(cmd, &iRate, sizeof(long));

    // send strobe = 50MHz
    iStrobe = (short) 50;
    SendByte = STROBE_CMD;
    write(cmd, &SendByte, 1);
    write(cmd, &iStrobe, sizeof(short));
    iFixed = 0; // programmable
    write(cmd, &iFixed, sizeof(short));

    // print the help menu on startup
    PrintMenu();

    // start main loop
    while(!end)
    {
	// prompt
	printf("\r>>>"); fflush(stdout);

	// get the user command
	fgets(Commandline, COMMAND_LENGTH-4, stdin);
	Length = strlen(Commandline);

	// convert command line to lower case
	for (i=0; i<Length; i++)
	{
		Commandline[i] = tolower(Commandline[i]);
		if (Commandline[i] == '\n')
			Commandline[i] = '\0';
	}

	// get length of the command line
	Length = strlen(Commandline);

	if (Length == 0)
	{
		// empty command <CR>: just repeat the last command
		strcpy(Commandline, LastCommand);
		printf(">>>%s\n\r", Commandline);
	}
	else
		// save the new command
		strcpy(LastCommand, Commandline);

	#ifdef MDL_DEBUG
		for (i=0; i<strlen(Commandline); i++)
		{
			printf("Commandline[%d] = %c (%d)\n\r", i, Commandline[i], Commandline[i]);
			fflush(stdout);
		}
		printf("Commandline = %s\n\r", Commandline);fflush(stdout);
	#endif

	// get the args
	ArgCount = 0;
	Arg[ArgCount] = strtok( Commandline, " " );
    	while( Arg[ArgCount++] != NULL )
    	{
		if (ArgCount > MAX_COMMAND_ARGS)
			break;

		Arg[ArgCount] = strtok( NULL, " " );
    	}

	ArgCount--;
	Length = strlen(Arg[0]);

	#ifdef MDL_DEBUG
		printf("Arg Count =  %d\n\r", ArgCount);
		printf("Arg0 Length =  %d\n\r", Length);
		for (i=0; i<ArgCount; i++)
		{
			printf("Arg %d = %s\n\r", i, Arg[i]);
		}
	#endif

	// Arg 0 is the command
	Length = strlen(Arg[0]);

        //////////////////////////////////////////////////////
        // HELP command: print menu
        //
        if (strncmp(Arg[0], "help", Length) == 0)
        {
                PrintMenu();
        }
        else
	//////////////////////////////////////////////////////
	// QUIT command: just quit this program
	//
	if (strncmp(Arg[0], "quit", Length) == 0)
	{
		end = 1;
	}
	else
	//////////////////////////////////////////////////////
	// RUN command: run the frame loop
	//
	if (strncmp(Arg[0], "run", Length) == 0)
	{
		iState = STATE_RUN;
		SendByte = RUN_CMD;
		write(cmd, &SendByte, 1);
	}
	else
	//////////////////////////////////////////////////////
	// STOP command: stop the frame loop
	//
	if (strncmp(Arg[0], "stop", Length) == 0)
	{
		iState = STATE_STOP;
		SendByte = STOP_CMD;
		write(cmd, &SendByte, 1);
	}
	else
        //////////////////////////////////////////////////////
        // STEP command: step the frame loop
        //
        if (strncmp(Arg[0], "step", Length) == 0)
        {
		if (iState == STATE_STOP)
		{
                	SendByte = STEP_CMD;
                	write(cmd, &SendByte, 1);
		}
		else
			printf("Can not step while running - issue stop command first...\n\r");
		
        }
        else
	//////////////////////////////////////////////////////
	// MODE command: change the mode, 1024 or 4096
	//
	if (strncmp(Arg[0], "mode", Length) == 0)
	{
		if (ArgCount != 2)
		{
			printf((const char *) "Wrong number of arguments - try again...\n\r");
			fflush(stdout);
		}
		else
		if (strcmp(Arg[1], "1") == 0)
		{
			//{printf((const char *) "SENDING MODE 1024\n\r"); fflush(stdout);}
			iMode = 1024;
			iLL = 0;
			SendByte = MODE_CMD;
			write(cmd, &SendByte, 1);
			SendByte = 0;
			write(cmd, &SendByte, 1);
		}
		else
                if (strcmp(Arg[1], "l") == 0)
                {
                        //{printf((const char *) "SENDING MODE 1024\n\r"); fflush(stdout);}
                        iMode = 1024;
                        iLL = 1;
                        SendByte = MODE_CMD;
                        write(cmd, &SendByte, 1);
                        SendByte = 0;
                        write(cmd, &SendByte, 1);
                }
                else
		if (strcmp(Arg[1], "4") == 0)
		{
			//{printf((const char *) "SENDING MODE 4096\n\r"); fflush(stdout);}
			iMode = 4096;
			iLL = 0;
			SendByte = MODE_CMD;
			write(cmd, &SendByte, 1);
			SendByte = 1;
			write(cmd, &SendByte, 1);
		}
		else
		{
			printf((const char *) "Bad parameter %s - try again...\n\r", Arg[1]);
			fflush(stdout);
		}
	}
	else
	//////////////////////////////////////////////////////
	// FREQUENCY command: change the frequency of the main loop
	//
	if (strncmp(Arg[0], "freq", Length) == 0)
	{
		int rate = 0;
                if (iState == STATE_RUN)
                        printf("Can not change frame rate while running - issue stop command first...\n\r");
		else
		if (ArgCount != 2)
		{
			printf((const char *) "Wrong number of arguments - try again...\n\r");
			fflush(stdout);
		}
		else
		{
		rate = atoi(Arg[1]);

		if (rate < 10 || rate > 70000)
		{
			printf((const char *) "Bad parameter %s or out of range - try again...\n\r",
				Arg[1]);
			fflush(stdout);
		}
		else
		{	
			iRate = (long) rate;
			SendByte = RATE_CMD;
			write(cmd, &SendByte, 1);
			write(cmd, &iRate, sizeof(long));
		}
		}
	}
	else
        //////////////////////////////////////////////////////
        // PSTROBE command: change the frequency of the strobe (programmable)
        //
        if (strncmp(Arg[0], "pstrobe", Length) == 0)
        {
                int strobe = 0;
                if (iState == STATE_RUN)
                        printf("Can not change strobe while running - issue stop command first...\n\r");
                else
                if (ArgCount != 2)
                {
                        printf((const char *) "Wrong number of arguments - try again...\n\r");
                        fflush(stdout);
                }
                else
		{
                strobe = atoi(Arg[1]);

                if (strobe < 10 || strobe > 50)
                {
                        printf((const char *) "Bad parameter %s or out of range - try again...\n\r",
                                Arg[1]);
                        fflush(stdout);
                }
                else
                {
                        iStrobe = (short) strobe;
                        SendByte = STROBE_CMD;
                        write(cmd, &SendByte, 1);
                        write(cmd, &iStrobe, sizeof(short));
    			iFixed = 0; // programmable
    			write(cmd, &iFixed, sizeof(short));
                }
		}
        }
        else
        //////////////////////////////////////////////////////
        // FSTROBE command: change the frequency of the strobe (fixed)
        //
        if (strncmp(Arg[0], "fstrobe", Length) == 0)
        {
                if (iState == STATE_RUN)
                        printf("Can not change strobe while running - issue stop command first...\n\r");
                else
		{
                int strobe = 50; // use highest
                iStrobe = (short) strobe;
                SendByte = STROBE_CMD;
                write(cmd, &SendByte, 1);
                write(cmd, &iStrobe, sizeof(short));
                iFixed = 1; // programmable
                write(cmd, &iFixed, sizeof(short));
		}
        }
        else
	//////////////////////////////////////////////////////
	// CLEAR command: clear all actuator values
	//
	if (strncmp(Arg[0], "clear", Length) == 0)
	{
		SendByte = CLEAR_CMD;
		write(cmd, &SendByte, 1);
	}
	else
	//////////////////////////////////////////////////////
	// TEST command: setup test pattern
	//
	if (strncmp(Arg[0], "test", Length) == 0)
	{
		SendByte = TEST_CMD;
		write(cmd, &SendByte, 1);
	}
	else
        //////////////////////////////////////////////////////
        // HALTERR command: specify halt on error behavior
        //
        if (strncmp(Arg[0], "halterr", Length) == 0)
        {
                if (ArgCount != 2)
                {
                        printf((const char *) "Wrong number of arguments - try again...\n\r");
                        fflush(stdout);
                }
                else
                if (strcmp(Arg[1], "0") == 0)
                {
                        SendByte = HERR_CMD;
                        write(cmd, &SendByte, 1);
                        SendByte = 0;
                        write(cmd, &SendByte, 1);
                }
                else
                if (strcmp(Arg[1], "l") == 0)
                {
                        SendByte = HERR_CMD;
                        write(cmd, &SendByte, 1);
                        SendByte = 1;
                        write(cmd, &SendByte, 1);
                }
                else
                {
                        printf((const char *) "Bad parameter %s - try again...\n\r", Arg[1]);
                        fflush(stdout);
                }

        }
        else
	//////////////////////////////////////////////////////
	// LOAD command: load actuators from file
	//
	if (strncmp(Arg[0], "load", Length) == 0)
	{
		int i;
		short s;
		int Ret, iActValue;
		char ScanLine[32];
		FILE *fp = NULL;
		int Len = 0;
                if (ArgCount != 2)
                {
                        printf((const char *) "Wrong number of arguments - try again...\n\r");
                        fflush(stdout);
                }
                else
		{
		fp = fopen(Arg[1], "rt");
		if (fp == NULL)
		{
			printf((const char *) "Error opening file %s\n\r", Arg[1]);
		}
		else
		{
		int *pMap = NULL;

                if (iMode == 1024)
                {
                	if (iLL == 1)
                                pMap = g_i1kLLMap;
                        else
                                pMap = g_i1kMap;
                }
                else
                        pMap = g_i4kMap;

		s = 0;
		while(fgets(ScanLine, 12, fp) && s < ((iMode == 4096) ? PACKETSIZE_4096 : PACKETSIZE_1024))
		{
			int Index = 0;
			Len = strlen(ScanLine);
			for (i=0; i<Len; i++)
			{
				if (ScanLine[i] == '\n')
					ScanLine[i] = '\0';
			}
			Ret = sscanf(ScanLine, "%x", &iActValue);
			Index = FindActuatorIndex(pMap, (iMode == 4096) ? PACKETSIZE_4096 : PACKETSIZE_1024, s++);
			sActValue[Index] = (short) iActValue;
		}
		fclose(fp);
		printf((const char *) "Loading %d actuator values...\n\r", s);
		fflush(stdout);

		SendByte = LOAD_CMD;
		write(cmd, &SendByte, 1);
		write(cmd, &s, 2);
		write(cmd, sActValue, s*sizeof(short));
		}
		}
	}
	else
	//////////////////////////////////////////////////////
	// POKE command: poke a single actuator with a value
	//
	if (strncmp(Arg[0], "poke", Length) == 0)
	{
                if (ArgCount != 3)
                {
                        printf((const char *) "Wrong number of arguments - try again...\n\r");
                        fflush(stdout);
                }
                else
                {
		unsigned short Index = 0;
		long Actuator = (long) strtol(Arg[1], (char **) NULL, 10);
		unsigned short Value    = (unsigned short) strtol(Arg[2], (char **) NULL, 16);
		long iMax  = (iMode == 4096) ? NUM_ACTUATORS_4096 : NUM_ACTUATORS_1024;
		
		if (Actuator < 1 || Actuator > iMax)
		{
			printf((const char *) "Actuator %d out of range - try again...\n\r", Actuator);
			fflush(stdout);
		}
		else
		{
			Actuator--;
			if (iMode == 1024)
			{
				if (iLL == 1)
					Index = FindActuatorIndex(g_i1kLLMap, iMax, Actuator);
				else
					Index = FindActuatorIndex(g_i1kMap, iMax, Actuator);
			}
			else
				Index = FindActuatorIndex(g_i4kMap, iMax, Actuator);

			printf((const char *) "Poke actuator %d (index %d) with 0x%04x\n\r", Actuator, Index, (unsigned) Value);

			fflush(stdout);
			SendByte = POKE_CMD;
			write(cmd, &SendByte, 1);
			write(cmd, &Index, sizeof(short));
			write(cmd, &Value, sizeof(short));
		}
		}
	}
	else
	{
		printf((const char *) "Unknown command: %s - try again...\n\r", Arg[0]);
		fflush(stdout);
	}
    }

    SendByte = TERM_CMD;	// send the TERM command to the thread	
    write(cmd, &SendByte, 1);	// write it to the FIFO
    (void) wait(NULL);		// wait for spawned processes to finish
    close(cmd);			// close the FIFO
    exit(0);
}


// helper function to look-up the alpha-numeric actuator in the provided list
// and return the index to support the POKE function
int FindActuatorIndex(int *Map, int iCount, int iActuator)
{
        int i, Index = -1;
	#ifdef MDL_DEBUG
	printf("look for %d in array of size %d\n", iActuator, iCount);
	#endif

        for (i=0; i<iCount; i++)
        {
                // compare the search string with the current actuator
                if (Map[i] == iActuator)
                {
                        // we found it, so record the index for return and break
			#ifdef MDL_DEBUG
	                printf((const char *) "Found actuator %d at index %d\n\r", iActuator, i);
                        fflush(stdout);
			#endif
                        Index = i;
                        break;
                }
        }
        return Index;
}



