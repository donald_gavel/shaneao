/*
* UpdateHookInterface.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.lang.*;
import javax.swing.JPanel;

public interface UpdaterInterface extends MsgReqInterface, ControllerInterface
{
	public static final int MAX_NUM_TYPES = AoGuiStructure.NUM_PANEL_TYPES;
	
	public void startServerConnection();
	
	public boolean writeServerCommand( String str );
	
	public void setDisplayUpdateRate( );
	public boolean registerListener( UpdateHookInterface who, int[] data_type ) throws NullPointerException, IllegalArgumentException;
	public boolean registerListener( UpdateHookInterface who, int data_type ) throws NullPointerException, IllegalArgumentException;
	public void removeListener( UpdateHookInterface who );
	public void kill( );
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************