//package MainFrame;

import java.awt.*;
import java.awt.image.*;
import javax.swing.*;
import java.util.*;
import java.text.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import javax.swing.border.*;

import java.awt.event.*;
import javax.swing.event.*;

import java.awt.geom.*;

public class
ColorBar extends JPanel implements ActionListener, MsgReqInterface
{

	//
	//
	// Note this list MUST agree with the combo box 'string' list
	// Note this list MUST agree with the combo box 'string' list
	//
	//
	public final static int GRAY = 0;
	public final static int REV_GRAY = 1;
	public final static int RED_BLUE = 2;
	public final static int BLUE_BLACK = 3;
	public final static int BLUE_WHITE = 4;
	public final static int RED_WHITE = 5;
	public final static int SPECTRUM = 6;
	public final static int REV_SPECTRUM = 7;
	//
	// Note this list MUST agree with the combo box 'string' list
	// Note this list MUST agree with the combo box 'string' list
	//
	//

	private static String thisClassName = "ColorBar";
	
	private AoPanel myParentPanel = null;
	
	private static MsgHandler msg = null;

	private final static int SAMPLE_WIDTH = 128;
	private final static int SAMPLE_HEIGHT = 10;

	private Image image;

	IndexColorModel indColorModel;

	private static final int BYTE_SCALE_SIZE = 256;
	public static final int SATURATION_LEVEL = 250;
	public static final int BRIGHTNESS_START_LEVEL = SATURATION_LEVEL - 1;
	public static final int BACKGROUND_START_LEVEL = 3;

	private byte [] redArray = new byte[ BYTE_SCALE_SIZE ];
	private byte [] greenArray = new byte[ BYTE_SCALE_SIZE ];
	private byte [] blueArray = new byte[ BYTE_SCALE_SIZE ];

	private int bkgdLevel = BACKGROUND_START_LEVEL;
	private int brightLevel = BRIGHTNESS_START_LEVEL;

	JTextField brightTextField = null;
	JTextField bkgdTextField = null;
	
	private JButton colorBarDisplayPanel = null;

	private int currentType = -1;

	boolean visible = false;
	boolean showBrtBkgdSettingPanel = true;
	
	// ============================================================================================================================
	// MsgReqInterface Implementation
	// ============================================================================================================================
		public boolean getDebugState() { return myParentPanel.getDebugState(); }
		public String getRequestorTitle() { return thisClassName; }
		public void setDebugState( boolean state ) { System.out.println( "Warning : Change debug in the parent"); }
		

	// ============================================================================================================================
	// Start - ColorBar Constructors
	// ============================================================================================================================

		ColorBar( AoPanel _this, int type, boolean showBB_Flag )
		{
			myParentPanel = _this;
			msg = new MsgHandler( this );
			showBrtBkgdSettingPanel = showBB_Flag;
			setColorBar( type );
		}

		JFrame getFramedColorBar( AoPanel _this, boolean setVisible )
		{
			if( _this == null)
				return null;
				
			String cbName = _this.getPanelTitle();
			JFrame framedColorBar = new JFrame( cbName + " ColorBar" );
			framedColorBar.setSize( 300, 300);
			framedColorBar.getContentPane().add( this );
			msg.infoMsg( "Added ColorBar to frame" );
			framedColorBar.setVisible( setVisible );
			return framedColorBar;
		}
		 
	// ============================================================================================================================
	// End - ColorBar Constructors
	// ============================================================================================================================

	// ============================================================================================================================
	// ============================================================================================================================
	
	// ============================================================================================================================
	// Public Methods
	// ============================================================================================================================

	public int getBackGround( )
		{ return this.bkgdLevel; }
	
	public void setBackGround( int bkgd )
	{
		this.bkgdLevel = Math.min( bkgd, this.brightLevel );
		this.bkgdLevel = Math.max( this.bkgdLevel, 0 );
		msg.debugMsg( " BkGrdLevel = " + this.bkgdLevel);
		setType( currentType );
		bkgdTextField.setText( String.valueOf( this.bkgdLevel ) );
	}
	
	public int getBrightness(  )
		{ return this.brightLevel; }
	
	public void setBrightness( int brt )
	{
		this.brightLevel = Math.min( brt, SATURATION_LEVEL-1 );
		this.brightLevel = Math.max( bkgdLevel, this.brightLevel );
		msg.debugMsg( " Brightness Level = " + this.brightLevel);
		setType( currentType );
		brightTextField.setText( String.valueOf( this.brightLevel ) );
	}
	
	
	public Dimension getSize()
		{ return ( new Dimension( super.getSize().width+20, super.getSize().height+20 )); }

	public void isVisible( boolean flag )
		{ setVisible( visible = flag ); }

	public IndexColorModel getIndexColorModel()
		{ return indColorModel; }

	public Color getColor( int ndx )
	{
		return (new Color( (int)(redArray[ ndx ] & 0xff), (int)(greenArray[ ndx ] & 0xff), (int)(blueArray[ ndx ] & 0xff) ));
	}

	public void setType( int type )
	{
		currentType = type;

		byte pix [] = new byte[ SAMPLE_WIDTH*SAMPLE_HEIGHT ];

		switch( type )
		{
			case GRAY:
			{
				msg.debugMsg( " Gray ColorBar" );
				createColor( redArray, 1.0f, 0.0f, (byte )255 );
				createColor( greenArray, 1.0f, 0.0f, (byte )0 );
				createColor( blueArray, 1.0f, 0.0f, (byte )0 );
				break;
			}

			case RED_BLUE:
			{
				msg.debugMsg( " RED_BLUE ColorBar" );
				createColor( redArray, -1.0f, 255.0f, (byte )0 );
				createColor( greenArray, 0.0f, 0.0f, (byte )255 );
				createColor( blueArray, 1.0f, 0.0f, (byte )0 );
				break;
			}

			case BLUE_BLACK:
			{
				msg.infoMsg( " BLUE_BLACK ColorBar" );
				createColor( redArray, 0.0f, 0.0f, (byte )255 );
				createColor( greenArray, 0.0f, 0.0f, (byte )0 );
				createColor( blueArray, 1.0f, 0.0f, (byte )0 );
				break;
			}

			case BLUE_WHITE:
			{
				msg.debugMsg( " BLUE_WHITE ColorBar" );
				createColor( redArray, -1.0f, 255.0f, (byte )255 );
				createColor( greenArray, -1.0f, 255.0f, (byte )0 );
				createColor( blueArray, 0.0f, 255.0f, (byte )0 );
				break;
			}

			case RED_WHITE:
			{
				msg.debugMsg( " RED_WHITE ColorBar" );
				createColor( redArray, 0.0f, 255.0f, (byte )0 );
				createColor( greenArray, -1.0f, 255.0f, (byte )0 );
				createColor( blueArray, -1.0f, 255.0f, (byte )0 );
				break;
			}

			case REV_GRAY:
			{
				msg.debugMsg( " Rev_Gray ColorBar" );
				createColor( redArray, -1.0f, 255.0f, (byte )255 );
				createColor( greenArray, -1.0f, 255.0f, (byte )0 );
				createColor( blueArray, -1.0f, 255.0f, (byte )0 );
				break;
			}
			
			case SPECTRUM:
			{
				msg.debugMsg( " Rev_Gray ColorBar" );
				createSpectrum( redArray, greenArray, blueArray, false );
				break;
			}
			
			 case REV_SPECTRUM:
			{
				msg.debugMsg( " Rev_Gray ColorBar" );
				createSpectrum( redArray, greenArray, blueArray, true );
				break;
			}
		}

		indColorModel = new IndexColorModel( 8, 256, redArray, greenArray, blueArray );

		for( int jj= 0; jj<SAMPLE_WIDTH; jj++ )
			for( int ii= 0; ii<SAMPLE_HEIGHT; ii++ )
			{
				float mult = ( float )255/SAMPLE_WIDTH;
				try
				{
					pix[ ii*SAMPLE_WIDTH+jj ] = ( byte )( jj*mult+0.5 );
				}
				catch(Exception e)
				{
					msg.errorMsg( "Exception:: " + e );
					msg.errorMsg( " " + (ii*SAMPLE_WIDTH+jj) );
				}
			}

		image = createImage( new MemoryImageSource( SAMPLE_WIDTH, SAMPLE_HEIGHT, indColorModel, pix, 0, SAMPLE_WIDTH ) );
		colorBarDisplayPanel.setIcon( new ImageIcon( image ) );

		repaint();
	}

	public void actionPerformed( ActionEvent e )
	{
	   setType( ( (JComboBox )e.getSource()).getSelectedIndex() );
	}
	
	//=============================================================================================================
	//  Start - Private helper functions for "ColorBar"
	//=============================================================================================================
	
	private void createSpectrum( byte [] redColor, byte [] greenColor, byte [] blueColor, boolean revFlag )
	{
	
		// Set Saturation Level
		for( int ii=0; ii<bkgdLevel; ii++ )
		{
				int value = (revFlag) ? 255 : 0;
				blueArray[ii] = (byte)(value);
				redArray[ii] = (byte)(value);
				greenArray[ii] = (byte)(value);
		}
		
		for( int ii=bkgdLevel; ii<brightLevel; ii++ )
		{
			if( revFlag )
			{
				blueArray[ii] = (byte)( (ii-bkgdLevel) * 255 / (brightLevel-bkgdLevel) );
				
				redArray[ii] = (byte)( (brightLevel - ii) * 255 / (brightLevel-bkgdLevel)  );
				
				if( ii <= bkgdLevel + (brightLevel-bkgdLevel)/2)
					greenArray[ii] = (byte)((ii-bkgdLevel) * 255 / ((brightLevel-bkgdLevel)/2));
				else 
					greenArray[ii] = (byte)( (brightLevel-ii) * 255 /  ((brightLevel-bkgdLevel)/2)  );
			}
			else
			{
				blueArray[ii] = (byte)( (brightLevel - ii) * 255 / (brightLevel-bkgdLevel) );
				
				redArray[ii] = (byte)( (ii-bkgdLevel) * 255 / (brightLevel-bkgdLevel)  );
				
				if( ii <= bkgdLevel + (brightLevel-bkgdLevel)/2)
					greenArray[ii] = (byte)((ii-bkgdLevel) * 255 / ((brightLevel-bkgdLevel)/2));
				else 
					greenArray[ii] = (byte)( (brightLevel-ii) * 255 /  ((brightLevel-bkgdLevel)/2)  );
			}
		}
			
		for( int ii=brightLevel; ii<255; ii++ )
		{
			int value = (revFlag) ? 0 : 255;
			blueArray[ii] = (byte)(value);
			redArray[ii] = (byte)(value);
			greenArray[ii] = (byte)(value);
		}
	}
	
	private void createColor( byte [] color, float colorScaler, float colorOffset, byte satColor )
	{
		float scaler = ( float )BYTE_SCALE_SIZE/( float )( brightLevel-bkgdLevel );

		// Set Background area
		for( int ii= 0; ii<=bkgdLevel; ii++ )
			color[ ii ] = ( byte )colorOffset;

		int jj = 0;

		for( int ii= bkgdLevel; ii<=brightLevel; ii++ )
			color[ ii ] = ( byte )( ( int )( jj++*scaler )*colorScaler+colorOffset );

		// Set Bright area
		for( int ii= brightLevel; ii<SATURATION_LEVEL; ii++ )
			color[ ii ] = ( byte )( (int)( BYTE_SCALE_SIZE-1 )*colorScaler+colorOffset );

		// Set Saturation Level
		for( int ii=SATURATION_LEVEL; ii<( int )BYTE_SCALE_SIZE; ii++ )
			color[ ii ] = satColor;
	}

	private void setColorBar( int type )
	{
		setBounds( 5, 5, 185, 185 );
		setBorder( BorderFactory.createTitledBorder( new LineBorder( Color.blue ), "Color Map" ) );
		setMinimumSize( new Dimension( 190, 190 ) );
		setPreferredSize( new Dimension( 190, 190 ) );
		setSize( new Dimension( 190, 190 ) );

		// Setup the ColorBar display window
		{
			colorBarDisplayPanel = new JButton();
			colorBarDisplayPanel.setPreferredSize( new Dimension( 170, 43 ) );
			colorBarDisplayPanel.setMaximumSize( new Dimension( 170, 43 ) );
			colorBarDisplayPanel.setMinimumSize( new Dimension( 170, 43 ) );
			colorBarDisplayPanel.setBorder( BorderFactory.createTitledBorder( new LineBorder( Color.blue ), "Current Map" ) );
			
			add( colorBarDisplayPanel );
		}

		setType( type ); // This has to be done after the colorBarDisplayPanel is setup

		// Setup selection window for type of color bar
		{
			String [] colorStrings =
			{
				"Gray",
				"Rev Gray",
				"Red / Blue",
				"Blk / Blue",
				"Blue / Wht",
				"Red / Wht",
				"Spectrum",
				"Rev Spectrum"
			};

			//Create the combo box, select item at index 4.
			JComboBox colorList = new JComboBox( colorStrings );
			colorList.setVisible(true);
			colorList.setFont( new Font( null, Font.PLAIN, 12) ); 

			colorList.setSelectedIndex( 7 );
			colorList.addActionListener( this );
			colorList.setVisible(true);

			add( colorList );
		}

		// Setup brightness and background subtract values
		JPanel limitSetPanel = new JPanel();

		{
			limitSetPanel.setBorder( BorderFactory.createTitledBorder( new LineBorder( Color.blue ), "Brt - BkgdSub " ) );
			limitSetPanel.setPreferredSize( new Dimension( 160, 77 ) );

			bkgdTextField = new JTextField();
			bkgdTextField.setText( String.valueOf(  bkgdLevel ) );
			bkgdTextField.setFont( new Font( null, Font.PLAIN, 10) ); 
			bkgdTextField.setToolTipText( "bkgd level for display" );
			bkgdTextField.setMinimumSize( new Dimension( 4, 30 ) );
			bkgdTextField.addActionListener( new ActionListener()
				{
					public void
					actionPerformed( ActionEvent evt )
					{
						String text = new String( bkgdTextField.getText() );

						try
						{
							int reqBkgdLevel = Integer.parseInt( text );
							setBackGround( reqBkgdLevel );
						}
						catch(NumberFormatException e)
						{
							msg.errorMsg( "action Listener for bkgdTextField", "Only INTS are acceptable." );
						}
						catch(Exception ee)
						{
							msg.errorMsg( "action Listener for bkgdTextField",
									  new String( " "+ee+"--- The text field can't handle an action." ) );
						}
					}
				});

			brightTextField = new javax.swing.JTextField();
			brightTextField.setText( String.valueOf( brightLevel ) );
			brightTextField.setFont( new Font( null, Font.PLAIN, 10) ); 
			brightTextField.setToolTipText( "brightness Level for display" );
			brightTextField.setMinimumSize( new Dimension( 4, 30 ) );
			brightTextField.addActionListener( new ActionListener()
				{
					public void
					actionPerformed( ActionEvent evt )
					{
						String text = new String( brightTextField.getText() );

						try
						{
							int reqBrtLevel = Integer.parseInt( text );
							setBrightness( reqBrtLevel );
						}
						catch(NumberFormatException e)
						{
							msg.errorMsg( "action Listener for brightTextField", "Only INTS are acceptable." );
						}
						catch(Exception ee)
						{
							msg.errorMsg( "action Listener for brightTextField",
									  new String( " "+ee+"--- The text field can't handle an action." ) );
						}
					}
				});

			JLabel tmpLabel1 = new JLabel( "Bright Display Level"  );
			tmpLabel1.setFont( new Font( null, Font.PLAIN, 12) ); 
			limitSetPanel.add( tmpLabel1 );
			limitSetPanel.add( brightTextField );
			
			JLabel tmpLabel2 = new JLabel( "Bkgd Display Level"  );
			tmpLabel2.setFont( new Font( null, Font.PLAIN, 12) ); 
			limitSetPanel.add( tmpLabel2 );
			limitSetPanel.add( bkgdTextField );

			limitSetPanel.setVisible(showBrtBkgdSettingPanel);
			
			add( limitSetPanel );
		}

		msg.debugMsg( "setColorBar Done." );
	}
	//=============================================================================================================
	//  End - Private helper functions for "ColorBar"
	//=============================================================================================================

}
// =================================================================================================
// End of file
// =================================================================================================
