/*
* WfsImagePanel.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

import java.util.*;

import java.lang.*;

import java.io.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


//
//	|-----------------------------------------------|   
//	|												| <-----JPanel ( "this : WfsImagePanel" ) in WfsImagePanel's contentPane
//	|												|   
//	|	 |--------------------------|				|   
//	|	 |							|				|   
//	|	 |							|<--------------+---------------- displayPanel ( Inner Class: DisplayPanel)
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |							|				|   
//	|	 |--------------------------|				|   
//	|												|   
//	|	 |-----|	|-----|	|-----|<----------------+--------------- misc Buttons and Status
//	|	 |-----|	|-----|	|-----|					|   
//	|												|   
//	|-----------------------------------------------|   
//

public class WfsImagePanel extends AoPanel
{

	private boolean				displayRefCircle = false;
	private boolean				displayRefSq = true;
	private boolean				displayPeakPixel = false;
	private boolean				displayPeakOverArea = false;
	
	protected static boolean	startDebug = false;
	
	private float				displayZoomPwr = 0.5f;
	public static final String	thisClassName = "WfsImagePanel";

	private static final int	NUM_PIXELS_PER_ROW = 96;
	private static final int	NUM_PIXELS_PER_COL = 96;
	private static final int	PIXEL_OFFSET = 0;

	
	private static NumberFormat	nf = NumberFormat.getInstance();
	
	ColorBar					myColorBar = null;
	
	private float[]				refGrid = null;			// Full grid - typically square
	private float[]				refAperture = null;		// pupil points - typically a circle
	
	private long				centValue[] = null;
	
	private int					sqrBaseSize = 2; // size in camera pixels, NOT display			//TODO - read this in!!

	private int[][]				myData = null;
	
	protected Image				image;

	protected int				imgXSize = -1;
	protected int				imgYSize = -1;

	byte[]						scaledData = null;
	int							displaySaturationLevel = 250;
	
	JTextField					bkgdTextField = null;
	JTextField					brightTextField = null;

	protected int				old_xSize = -1;
	protected int				old_ySize = -1;
	protected float				old_zoomPwr = 0.0f;

	protected static JFrame		aFrame = null;

	protected DisplayPanel		displayPanel = null;
	protected JPanel			displayBrightnessPanel = null;

	protected PanelMenuBar		imagePanelMenu = null;

	protected JPanel			pkValPanel = null;
	protected JTextField		pkValTextField = null;
	protected JTextField		avgPkValTextField = null;

	protected JPanel			saveImagePanel = null;
	protected boolean			doOneImageSave = false;
	protected String			saveImageFileName = null;
	protected JTextField		ss_fName_field = null;

	protected String			defaultBkGdSubBasePath = ".\\etc\\config\\Data";
	protected String			defaultBkGdSubFileName = "\\bk_grnd_data.aod";
	protected String			defaultBkGdSubZeroFileName = "\\bk_grnd_data_zero.aod";
	
	protected MemoryImageSource locImgSrc = null;
	
	private int					updateCnt = 0;
	
	
	// ============================================================================================================================
	// Constructor - WfsImagePanel
	// ============================================================================================================================
	
	protected WfsImagePanel( AoGuiStructure aoGuiSetup )
	{
		super( thisClassName );
		
		msg = new MsgHandler( this );
		setPanelTitle( thisClassName );
		thisPanel = this;
	
		this.debug = startDebug ;
	
		panelType = AoGuiStructure.PANEL_WFS;
	
		refGrid = SystemPanel.getRefGrid();
		refAperture = SystemPanel.getRefAperture();

		String MethodName =  "WfsImagePanel(  )";
		msg.debugMsg( MethodName, "Start" );
		
		nf.setMinimumIntegerDigits( 3 );		
		
		doImagePanelSetup( "Image Display Panel w/Ref" );
		imagePanelMenu = new PanelMenuBar( );
		inputCount = 0;

		displaySaturationLevel = (aoGuiSetup.getSatLimits( AoGuiStructure.PANEL_WFS ))[1];

		msg.debugMsg( MethodName, "WFS Constructor:: register for Update!!");
		int[] types = new int[1];
		types[0] = AoGuiStructure.DATA_WFS;

		SystemPanel.theUpdater.registerListener( this, types );
	}
	
	
	// ============================================================================================================================
	// Internal Class - ImageDisplayJFrame
	// ============================================================================================================================
	
	public static AOPanelInterface getFramedPanel( AoGuiStructure aoGuiSetup  )
	{

		aFrame = new JFrame();
		
		final AOPanelInterface myPanel = new WfsImagePanel( aoGuiSetup );

		aFrame.setTitle( thisClassName );
		myPanel.setEnclosingFrame( aFrame );

		aFrame.setJMenuBar( myPanel.getMyMenuBar() );
		aFrame.getContentPane().add( (JPanel)myPanel );

		// Set the size of the Frame, based on the image panel size
		Dimension panelSize = ((JPanel)myPanel).getSize();
		aFrame.setSize( new Dimension( panelSize.width+55, panelSize.height + 160 ) );
		aFrame.setResizable( true );

		aFrame.addWindowListener( new java.awt.event.WindowAdapter()
			{
				public void windowClosed(WindowEvent e) { MsgHandler.genericInfoMsg( thisClassName, " Frame Closed2 "); }

				public void windowClosing( java.awt.event.WindowEvent evt )
				{
					MsgHandler.genericInfoMsg( thisClassName, " Frame Closing1 ");
					myPanel.destroyPanel();
				}
			});

		aFrame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		aFrame.setLocation( 0, 350);
		aFrame.setVisible( true );
		
		return myPanel;
	}
	
	
	// ============================================================================================================================
	// Internal Class - DisplayPanel
	// ============================================================================================================================
	
	private class DisplayPanel extends JPanel
	{
		
		DisplayPanel( int xsize, int ysize, float[] ref  )
		{
			this.setBorder( BorderFactory.createTitledBorder 
							( new LineBorder( Color.white, 3 ), null, TitledBorder.CENTER, TitledBorder.CENTER, 
							  new Font( null, Font.BOLD, 10) ) 
						  );
			this.setSize( new Dimension( xsize, ysize ) );
			this.setPreferredSize( new Dimension( xsize, ysize ) );

			if( ref==null )
			{
				msg.errorMsg( " Unable to construct DisplayPanel :: 'ref' is NULL" );
				return;
			}
			msg.debugMsg( " - Got Reference Points" );
			
			setRef( ref );
			setRefCircleDisplay( displayRefCircle );
			setRefSqDisplay( displayRefSq );

			this.setVisible( true );
			displayPanel = this;
		}
		
		boolean getRefCircleDisplay( )
		{
			return displayRefCircle;
		}

		void setRefCircleDisplay( boolean flag )
		{
			displayRefCircle = flag;
		}

		boolean getRefSqDisplay( )
		{
			return displayRefSq;
		}
		
		void setRefSqDisplay( boolean flag )
		{
			displayRefSq = flag;
		}

		boolean getPeakPixelDisplay( )
		{
			return displayPeakPixel;
		}

		void setPeakPixelDisplay( boolean flag )
		{
			displayPeakPixel = flag;
		}

		boolean getPeakOverAreaDisplay( )
		{
			return displayPeakOverArea;
		}
		
		void setPeakOverAreaDisplay( boolean flag)
		{
			displayPeakOverArea = flag;
		}
		
		void setImgMag( float val )
		{
			displayZoomPwr = val;
			super.repaint();
			repaint();
		}

		void setRefSide( int size )
		{
			sqrBaseSize = size;
		}

		// This routine allows the Reference positions to be changed.
		//	  Normally these are set up as part of Construction.		
		void setRef( float [] val )
		{
			if( val==null )
			{
				refGrid = null;
				displayRefCircle = false;
				displayRefSq = false;
				displayRefSq = false;
			}

			int len = val.length;
			refGrid = new float[ len ];

			for( int xx= 0; xx<len; xx++ )
				refGrid[ xx ] = val[ xx ];
		}


		public void paintComponent( Graphics g )
		{
			msg.debugMsg( "\t\t\t\t\t\t\t ***************************** Start Doing Paint !!!!!!!!!!!!!!!!!!!!!!!!");
		
			msg.debugMsg( "DisplayPanel.paintComponent( Graphics)", "Test if image is NULL" );
			
			if( image==null )
			{
				msg.debugMsg( " image during rePaint == null" );
				return;
			}

			msg.debugMsg( "DisplayPanel.paintComponent( Graphics)", "drawImage" );
			g.drawImage( image, 0, 0, this );

			// Check and update the image with the reference points, if they are available.
			msg.debugMsg( "DisplayPanel.paintComponent( Graphics)", "Draw Ref Circles/Squares/Peaks" );
			displayReferenceOnImage( g );
			
			msg.debugMsg( "DisplayPanel.paintComponent( Graphics)", "DONE - Draw Ref Circles/Squares/Peaks" );
			
			msg.debugMsg( "\t\t\t\t\t ******************************  Update Done!!!!!!!!!!!!!!!!!! ");
			msg.debugMsg( "\t\t\t\t\t ******************************  Update Done!!!!!!!!!!!!!!!!!! ");
			msg.debugMsg( "\t\t\t\t\t ******************************  Update Done!!!!!!!!!!!!!!!!!! ");
			
			updateInProgress = false;
			
			return;
		}

		private void displayReferenceOnImage( Graphics g )
		{
			if( (displayRefCircle!=true) && (displayRefSq!=true) && (displayPeakPixel!=true) )
				return;
				
			// Do setup if data arrays not already set up
			if( centValue == null )
			{
				// read the reference points
				if( refGrid == null )
					return;
					
				centValue = new long[ refGrid.length / 2 ];
			}

			int numRef = refGrid.length/2;
			int sqrSize = (int)(sqrBaseSize * displayZoomPwr);

			for( int ii= 0; ii<numRef; ii++ )
			{
				// The additional "+ AoGuiInterface.NUM_PIXELS_PER_CENT / 2" is required because the reference points are to the upper left hand corner
				double centerX = (refGrid[ ii ] + AoGuiInterface.NUM_PIXELS_PER_CENT / 2 ) * displayZoomPwr;
				double centerY = ( refGrid[ ii + numRef ] + AoGuiInterface.NUM_PIXELS_PER_CENT / 2 ) * displayZoomPwr;

				if( displayRefSq == true )
				{				
					if( refAperture[ii] != 0 )
						g.setColor( Color.blue );
					else
						g.setColor( Color.orange );
					g.drawRect( (int)(centerX-sqrSize/2.0-0.5), (int)(centerY-sqrSize/2.0-0.5),
								sqrSize, sqrSize );
				}
				
				if( displayRefCircle == true )
				{				
					if( refAperture[ii] != 0 )
						g.setColor( Color.blue );
					else
						g.setColor( Color.orange );
					g.drawOval( (int )( centerX+0.5-4 ), (int )( centerY+0.5-4 ), (int )( 8 ), (int )( 8 ) );
				}
				
				if( (displayPeakPixel == true) && (centValue!=null) )
				{
					g.setColor( Color.red );
					g.setFont( new Font( null, Font.PLAIN, 9) ); 
					g.drawString( String.valueOf( (int)(centValue[ii]) ), (int)(centerX-sqrSize/2.0+1), (int)(centerY+sqrSize/2.0-1) );
				}
			}
		}
	}

	private int row = -1;
	
	public void myUpdateHook( float[] f_data, int type ) { msg.debugMsg( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][] f_data, int[] types ) { msg.debugMsg( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][][] ff_data, int[] types ) { msg.debugMsg( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( String s_data ) 
	{
		if( updateInProgress )
		{
			msg.infoMsg( "myUpdateHook - Called but being ignored (Update is already in progress)" );
			msg.infoMsg( " Input string stub for WFS is: " + s_data.substring( 0, 20 ) );
			return;
		}
		
		String inputLine = new String( s_data );
		
		msg.debugMsg( "myUpdateHook - Requesting update processing" );
		
		int dataType = -1;
		int numElem = -1;
		String[] strValues = null;
		
		msg.debugMsg("\n (	public void myUpdateHook( String s_data ) ):: Attempting to parse (WFS)");
		
		dataType = (int)( inputLine.charAt(3) - '0');
		msg.debugMsg( "\n\tData Type: " + dataType );

		switch( dataType )
		{
			case 1:	// WFS
				break;
				
			default:
			case 2:	// CENT
			case 3:	// INTEN
			case 4:	// ACT_1
			case 5:	// TT
			case 6:	// ACT_2
				return;			// Do nothing if it is the wrong type
		}

		msg.debugMsg( " Input string stub for WFS is: " + inputLine.substring( 0, 20 ) );

		if( inputLine.indexOf( "UR" ) > 0 )
		{
			String[] tmpS = null;
			tmpS = inputLine.substring( inputLine.indexOf( "UR" )+2 ).split( " " ); 
			msg.debugMsg( " After parsing for UR string stub for WFS is: " + tmpS[0] );
			row = Integer.parseInt( tmpS[0] );
			msg.debugMsg( " For unused rows - got row: " + row );

			inputLine = inputLine.substring( inputLine.indexOf( " ", inputLine.indexOf( "UR", 4 ) )+1, inputLine.length()-3 );
			strValues = inputLine.split( " ", 500);
		}
		else
		{
			int indexCharS = inputLine.indexOf( "S", 4 );
			int indexCharR = inputLine.indexOf( "R", 4 );
			if( indexCharS < 0 || indexCharR < 0 )
				return;
				
			msg.debugMsg( " Index of 's' is: " + indexCharS + " and Index of 'r' is: " + indexCharR );
		
			// Read the current sub-aperture row and the sub-aperture index
			String subApNumStr = inputLine.substring( indexCharS + 1, indexCharR-1 );
			int subApNum = Integer.parseInt( subApNumStr );

			String subApRowStr = inputLine.substring( indexCharR + 1, inputLine.indexOf( " ", indexCharR ) );
			int subApRow = Integer.parseInt( subApRowStr );
			
			row = AoGuiInterface.WFS_ROWS_SKIPPED + subApNum * AoGuiInterface.NUM_PIXELS_PER_CENT + subApRow;
			
			inputLine = inputLine.substring( inputLine.indexOf( " ", indexCharR )+1, inputLine.length()-3 );

			strValues = inputLine.split( " ", 500);
		}
			
		numElem = strValues.length;
		msg.debugMsg( "(wfs-MyUpdateHook): Number entries is: " + numElem );

		// Create data save location if it doesn't already exist
		if( myData == null )
			myData = new int[ NUM_PIXELS_PER_COL ][ NUM_PIXELS_PER_ROW ];
			
		msg.debugMsg( "(wfs-myUpdateHook): Data row #" + row );



		int iVal = 0;
		int ii = 0;
		try
		{
			for( ii=0; ii<NUM_PIXELS_PER_ROW; ii++ )
			{
				iVal = Integer.parseInt( strValues[ ii ] );
				myData[ row ][ ii] = iVal;

				if( iVal < 0 ) 
				{
					System.out.println( "\n INPUTVALUES < 0");
					System.out.println( "\n Input String: " + strValues[ii+PIXEL_OFFSET] );
				} 
				myData[ row ][ ii] = iVal;
				
				if ( debug && ( ii < 20 ) )
					System.out.print( "  " + iVal );
			}
		}
		catch( Exception e )
		{
			System.out.println( "\n\n (WFS::myUpdateHook(string): ii =  " + ii + "\n\n Got Exception: " + e );
			System.out.println( " (WFS callback error) : row #" + row
					+ ", ii = " + ii + ", iVal = " + iVal + ", strValues[ ii ] " + strValues[ii] );
			System.out.println( "\n		input line: " + inputLine );
			System.out.println( " (WFS::myUpdateHook(string):  Stack Trace::");
			e.printStackTrace();
			msg.errorMsg( "Exception caught - See console screen");
		}
		msg.debugMsg( "\n <WFS>222 Moved one row of data into mData[" + row + "][*]" );

		// Test if we have the entire image
		
			// return if not!!
				
		// TODO - Fix this so update is not done on every row!!
		
		if( row == (NUM_PIXELS_PER_COL-1) )		// TODO - fix hard code
		{	
			System.out.println(" CALL - doUpdate ");
			msg.debugMsg( " CALL - doUpdate ");
			doUpdate( );
		}
	}
	
	
		
	// ============================================================================================================================
	// Internal Class - PanelMenuBar
	// ============================================================================================================================
	
	public JMenuBar getMyMenuBar( )
	{
		return (JMenuBar)(new PanelMenuBar());
	}

	private class PanelMenuBar extends JMenuBar
	{
		
		PanelMenuBar(  )
		{
		
			myColorBar = new ColorBar( thisPanel, ColorBar.REV_GRAY, false );
			
			final JMenuItem setColorMapItem = new JMenuItem( "Color Map" );
			{
				setColorMapItem.setFont( new Font( null, Font.PLAIN, 10));
				setColorMapItem.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							msg.debugMsg( "You pushed the 'Select Color Map ' button" );
							JFrame myColorBarFrame = myColorBar.getFramedColorBar( thisPanel,  true );
						}
					});
				msg.debugMsg( "Color Map Menu Item setup completed" );
			}		

			final JMenuItem setSatLevelItem = new JMenuItem( "Saturation Level" );
			{
				setSatLevelItem.setFont( new Font( null, Font.PLAIN, 10));
				setSatLevelItem.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							String s = JOptionPane.showInputDialog(
											(Object) "What is the new Saturation Level? ", 
											(Object) (new Integer(displaySaturationLevel)) );
							
							//If a string(s) was returned, update param
							if ((s != null) && (s.length() > 0)) 
								displaySaturationLevel = Integer.parseInt( s );
						}
					});
				msg.debugMsg( "Saturation Level Menu Item setup completed" );
			}
					
			final JCheckBoxMenuItem refSqrsOnItem = new JCheckBoxMenuItem( "Ref Squares On" );
			{
				refSqrsOnItem.setFont( new Font( null, Font.PLAIN, 10));
				if( displayPanel == null)
					msg.errorMsg( "display Panel is NULL" );
					
				refSqrsOnItem.setState( displayPanel.getRefSqDisplay()  );
			
				refSqrsOnItem.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							boolean currState = refSqrsOnItem.getState();
							msg.infoMsg( "Current State of Ref Sqs: " + currState );
							displayPanel.setRefSqDisplay( currState );
						}
					});
				msg.debugMsg( "Ref Sqr On/Off Menu Item setup completed" );
			}
			
			final JCheckBoxMenuItem refCirclesOnItem = new JCheckBoxMenuItem( "Ref Circles On" );
			{
				refCirclesOnItem.setFont( new Font( null, Font.PLAIN, 10));
				if( displayPanel == null)
					msg.errorMsg( "display Panel is NULL" );
					
				refCirclesOnItem.setState( displayPanel.getRefCircleDisplay()  );
			
				refCirclesOnItem.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							boolean currState = refCirclesOnItem.getState();
							msg.infoMsg( "Current State: " + currState );
							displayPanel.setRefCircleDisplay( currState );
						}
					});
				msg.debugMsg( "Ref Circle On/Off Menu Item setup completed" );
			}

					
			JMenu configMenu = new JMenu( "Config");
			{
				configMenu.setFont( new Font( null, Font.BOLD, 10));
				configMenu.add( setColorMapItem );
				configMenu.add( setSatLevelItem );
				configMenu.addSeparator();
				configMenu.add( refSqrsOnItem );
				configMenu.add( refCirclesOnItem );
				msg.debugMsg( "Configuration Menu Item  completed" );
			}

			
			final JCheckBoxMenuItem displayPeaksOnItem = new JCheckBoxMenuItem( "Display Centroid Peaks On" );
			{
				displayPeaksOnItem.setFont( new Font( null, Font.PLAIN, 10));
				if( displayPanel == null)
					msg.errorMsg( "display Panel is NULL" );
					
				displayPeaksOnItem.setState( displayPanel.getRefCircleDisplay()  );
			
				displayPeaksOnItem.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							boolean currState = displayPeaksOnItem.getState();
							msg.infoMsg( "Current State: " + currState );
							displayPanel.setPeakPixelDisplay( currState );
						}
					});
				msg.debugMsg( "Display Peak On/Off Menu Item setup completed" );
			}

			final JCheckBoxMenuItem displayPeaksOverArea = new JCheckBoxMenuItem( "Display Peaks over Centroid Area" );
			{
				displayPeaksOverArea.setFont( new Font( null, Font.PLAIN, 10));
				if( displayPanel == null)
					msg.errorMsg( "display Panel is NULL" );
					
				displayPeaksOverArea.setState( displayPanel.getRefCircleDisplay()  );
			
				displayPeaksOverArea.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							boolean currState = displayPeaksOverArea.getState();
							msg.infoMsg( "Current State: " + currState );
							displayPanel.setPeakOverAreaDisplay( currState );
						}
					});
				msg.debugMsg( "Display Peak On/Off Menu Item setup completed" );
			}

			JMenu displayMenu = new JMenu( "Display");
			{
				displayMenu.setFont( new Font( null, Font.BOLD, 10));
				displayMenu.add( setColorMapItem );
				displayMenu.add( displayPeaksOnItem );
				displayMenu.add( displayPeaksOverArea );
				msg.debugMsg( "Display Menu Item  completed" );
			}

			
			// Create a menu to set the zoom values
			MenuRadioButtonGroup zoomMenu = null;
			{
				String [] zoomButtons = {"Zoom x0.5", "Zoom x1.0", "Zoom x2.0", "Zoom x4.0", "Zoom x8.0", "Zoom x12.0", "Zoom x16.0"};
				zoomMenu = new MenuRadioButtonGroup( zoomButtons, "ImageZoom", 0, this );
				zoomMenu.addHandler( 0, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 0.5f ); }	});
				zoomMenu.addHandler( 1, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 1.0f ); } 	});
				zoomMenu.addHandler( 2, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 2.0f ); }	});
				zoomMenu.addHandler( 3, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 4.0f ); }	});
				zoomMenu.addHandler( 4, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 8.0f ); }	});
				zoomMenu.addHandler( 5, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 12.0f ); }	});
				zoomMenu.addHandler( 6, new ButtonHookInterface() { public void myHook() { displayPanel.setImgMag( 16.0f ); }	});
				zoomMenu.pushButton( 3 );
				zoomMenu.setFont( new Font( null, Font.BOLD, 10));

				msg.debugMsg( "Image Zoom Menu Item  completed" );
			}
					
			
			// Debug Menu
			JMenu debugMenu = new JMenu( "Debug");
			{			
				final JMenuItem debugOn = new JMenuItem( "Debug On" );
				{
					debugOn.setFont( new Font( null, Font.PLAIN, 10));
					debugOn.addActionListener( new ActionListener()
						{ public void actionPerformed( ActionEvent evt ) { setDebugFlag( true ); } });
				}

				final JMenuItem debugOff = new JMenuItem( "Debug Off" );
				{
					debugOff.setFont( new Font( null, Font.PLAIN, 10));
					debugOff.addActionListener( new ActionListener()
						{ public void actionPerformed( ActionEvent evt ) { setDebugFlag( false );	} });
				}
				
				{
					debugMenu.setFont( new Font( null, Font.BOLD, 10));
					debugMenu.add( debugOn );
					debugMenu.add( debugOff );
					msg.debugMsg( "WfsImagePanel::PanelMenuBar::doUpdate", "Save Menu completed" );
				}
			}
			
			this.add( configMenu );
			this.add( displayMenu );
			this.add( zoomMenu );
			this.add( debugMenu );
			msg.debugMsg( "Image Menu Bar completed" );
		}
	}
	
	// ============================================================================================================================
	// AOPanelInterface Implementation
	// ============================================================================================================================

	
	public int getUpdCount()
	{
		return inputCount;
	}

	public void destroyPanel()
	{
		MsgHandler.genericInfoMsg( " *** REMOVING WFS PANEL - Calling 'theUpdater.removeListener()'" );
	
		SystemPanel.theUpdater.removeListener( this );
		setVisible( false );
		thisFrame.dispose();
	}

	// ============================================================================================================================
	// Base Class Methods
	// ============================================================================================================================
	//
	// Note that these are private defines for internal program use
	//
	private final static int BORDER = 10;

	private long	pkVal = 0;
	private	long	avgPkVal = 0;

	private boolean setPanelSize( int xSize, int ySize, float zoom  )
	{
		// Set the image size based on the input data and the zoomFactor
		msg.debugMsg( "setPanelSize( int, int, float)", " Input data xSize / ySize :" + xSize + " / " + ySize  );
		int locXSize = (int )( xSize * zoom );
		int locYSize = (int )( ySize * zoom );
	
		msg.debugMsg( "setPanelSize( int, int)", "Start" );

		if( ( locXSize==imgXSize ) && ( locYSize==imgYSize ) )
		{
			msg.debugMsg( "setPanelSize( int, int, float)", " image area is already set up :: Returning (Done) " );
			return true;
		}

		msg.debugMsg( "setPanelSize( int, int)", " Creating New Image  (x,y) = "+new String( " (" + locXSize + ", " + locYSize + " )" ) );
		{
			msg.debugMsg( "setPanelSize( int, int, float)", "setSizes" );
			
			int width = locXSize+20;
			this.setSize( new Dimension( width, locYSize+150) );
			this.setPreferredSize( new Dimension( width, locYSize+150) );
			
			displayPanel.setSize( new Dimension( locXSize, locYSize)   );
			displayPanel.setPreferredSize( new Dimension( locXSize, locYSize)   );
			if( thisFrame != null)
			{
				this.thisFrame.setSize( new Dimension( locXSize+40, locYSize+170) );
				this.thisFrame.pack();
			}
		}

		msg.debugMsg( "setPanelSize( int, int)", "Attempt to repaint the image" );
		
		this.thisFrame.repaint();
		this.repaint();
		displayPanel.repaint();

		imgXSize = locXSize;
		imgYSize = locYSize;
		return true;
	}


	//**********************************************************************************************************
	//
	// doUpdate - This routine is called to update the image with new information
	//
	//  Causes a redraw to occur
	// @returns boolean true:ok, false:failed
	// 
	// This routine is primary update routine.
	// Purpose in life : To rescale and move the data into the java - 'image' and repaint the display.
	//
	//**********************************************************************************************************
	//
	private void doUpdate(  )
	{
		boolean updateImgSize = false;
		int	ii, jj, centNum;
		int totCent = refGrid.length/2;
		long pixelTmp;
		
		if( updateInProgress )
			return;
		
		updateInProgress = true;							// only do one at a time!
		
		try
		{		
			int inXSize = myData[0].length;
			int inYSize = myData.length;
			
   			if( (old_xSize != inXSize) || (old_ySize != inYSize) || (old_zoomPwr != displayZoomPwr) )
			{
				setPanelSize( inXSize, inYSize, displayZoomPwr );
				old_xSize = inXSize;
				old_ySize = inYSize;
				old_zoomPwr = displayZoomPwr;
				updateImgSize = true;
			}
			
			long[] inData = new long[ inXSize * inYSize ];
			
			for( int yy=0; yy<inYSize; yy++ )
				for( int xx=0; xx<inXSize; xx++ )
					inData[ yy * inXSize + xx ] = myData[ yy ][ xx ];

			if( debug )
			{

				System.out.println("\nData Dump for current frame: " );
				for( jj=20; jj<25; jj++)
				{
					for( ii=20; ii<25; ii++ )
						System.out.println( " " + inData[ ii + jj * inXSize ] );
					System.out.println( "");
				}
					
			}
						
			if(centValue!=null)
			{
				msg.debugMsg( " sqrBaseSize = " + sqrBaseSize );
				msg.debugMsg( " totCent = " + totCent );
				
				for( centNum=0; centNum<totCent; centNum++ )
				{
					long lTmp = 0;

					int xPos = (int)(refGrid[ centNum ] + AoGuiInterface.WFS_ROWS_SKIPPED + AoGuiInterface.NUM_PIXELS_PER_CENT/2  );
					int yPos = (int)(refGrid[ centNum + totCent ] + AoGuiInterface.WFS_COLS_SKIPPED + AoGuiInterface.NUM_PIXELS_PER_CENT/2);

					//	 If doing average level				
					pixelTmp = 0;
					int jjFactor = 0;
					ii = 0;
					jj = 0;
					try
					{
						for( jj=(yPos-sqrBaseSize/2); jj<(yPos+sqrBaseSize/2); jj++ ) 
						{
							jjFactor = jj * inXSize + xPos;
							for( ii=(jjFactor - sqrBaseSize/2); ii<(jjFactor + sqrBaseSize/2); ii++ ) 
								pixelTmp += inData[ ii ];
						}
					} catch( Exception e ) {
						msg.errorMsg( " Exception caught : ii, jj, inXSize, inYSize, jjFactor, sqrBaseSize, xPos, yPos :: " 
														+ ii + ", "  + jj + ", " + inXSize + ", " + inYSize + ", " 
														+ jjFactor + ", " + sqrBaseSize + ", " + xPos + ", " + yPos );
					}	
					centValue[ centNum ] = (int)(pixelTmp / (sqrBaseSize * sqrBaseSize));

					//	 If doing peak level
					for( jj=(yPos-sqrBaseSize/2); jj<(yPos+sqrBaseSize/2); jj++ ) 
					{
						jjFactor = jj * inXSize + xPos;
						for( ii=(jjFactor - sqrBaseSize/2); ii<(jjFactor + sqrBaseSize/2); ii++ ) 
						{
							pixelTmp = inData[ ii ];
							if( lTmp < pixelTmp )
								lTmp = pixelTmp;
						}
					}
					
					centValue[ centNum ] = lTmp;

					
					if( displayPeakOverArea )
					{
						for( jj=-sqrBaseSize/2; jj<sqrBaseSize/2; jj++ ) 
							for( ii=-sqrBaseSize/2; ii<sqrBaseSize/2; ii++ ) 
								inData[ (xPos+ii) + (yPos+jj)*inXSize ] = lTmp;
					}
				}
			}

			if(centValue!=null)
			{	
				int len = centValue.length;
				long thisCentPeak = 0;
				for( ii=0; ii<len; ii++ )
				{
					thisCentPeak = centValue[ ii ];
					pkVal = ( pkVal < thisCentPeak ) ? thisCentPeak : pkVal;
					avgPkVal += thisCentPeak;
				}
				avgPkVal /= len;
			}

//			System.out.println( "\n Peak centroid value is " + pkVal + "// String is: " + Long.toString( pkVal ) );
//			System.out.println( "\n Avg Peak centroid value is " + avgPkVal + "// String is: " + Long.toString( avgPkVal )  );


			int imgLen = inXSize * inYSize;
			byte[] imgData = new byte[ imgLen ];
			for( int xx=0; xx<imgLen; xx++ )
				imgData[xx] = (byte)(( inData[xx] ) & 0xFF);
				
			pkValTextField.setText( Long.toString( pkVal ) );
			avgPkValTextField.setText( Long.toString( avgPkVal ) );
			
			msg.debugMsg( " Start Update - Mark Saturation"  );
			
		   // Output the peak value to the display
			if( pkVal < displaySaturationLevel )
			{
				pkValTextField.setBackground( Color.WHITE );
				pkValTextField.setForeground( Color.BLACK );
			}
			else
			{
				pkValTextField.setBackground( Color.RED );
				pkValTextField.setForeground( Color.WHITE );
			}

			msg.debugMsg( "updateImage( Integer[] ): ", " Start " );
			
			if( (locImgSrc == null) || (image == null) || updateImgSize )
			{
				locImgSrc = new MemoryImageSource( inXSize, inYSize, myColorBar.getIndexColorModel(), imgData, 0, inXSize );
				locImgSrc.setAnimated( true);
				image = this.createImage( locImgSrc ).getScaledInstance( (int)(inXSize * displayZoomPwr), (int)(inYSize * displayZoomPwr), Image.SCALE_FAST );
			}
			else
			{
				locImgSrc.newPixels( imgData, myColorBar.getIndexColorModel(), 0, inXSize );
			}
		}
		catch( Exception e )
		{
			e.printStackTrace();
			updateInProgress = false;
			msg.debugMsg( "\n\n\n\n\t\t\t  EEEEEERRRRRRRRRROOOOOOOOOORRRRRRRR  ;-( " ) ;
			msg.errorMsg( "Exception caught - See console screen");
			
		}		

		msg.debugMsg( "\t\t\t\t\t\t\t ***************************** Start Repaint !!!!!!!!!!!!!!!!!!!!!!!!");
		this.repaint();
		displayPanel.repaint();
		msg.debugMsg( "updateImage( Integer[]): ", " Done " );
		return;
	}


// ===============================================================================================================================
// private Methods
// ===============================================================================================================================

	private void doImagePanelSetup( String title )
	{
		// Setup the Image Display panel
		msg.infoMsg(" Open DisplayPanel " );
		
		displayPanel = new DisplayPanel( 256, 256, refGrid );

		// Set up the "Peak Value Display"
		{
			pkValPanel = new JPanel();
			pkValPanel.setBorder( BorderFactory.createTitledBorder
						( 	new LineBorder( Color.blue ), 
							"Cent Avgs",
							TitledBorder.CENTER, 
							TitledBorder.CENTER, 
							new Font( null, Font.BOLD, 9) 
						) 
					);

			pkValPanel.setPreferredSize( new Dimension( 180, 45 ) );

			pkValTextField = new JTextField();
			pkValTextField.setText( String.valueOf(  999999 ) );
			pkValTextField.setFont( new Font( null, Font.PLAIN, 9));
			pkValTextField.setToolTipText( "Max Peak Pixel Value over all centroids" );
			pkValTextField.setMinimumSize( new Dimension( 30, 18 ) );
			pkValTextField.setPreferredSize( new Dimension( 30, 18 ) );

			avgPkValTextField = new JTextField();
			avgPkValTextField.setText( String.valueOf(  999999 ) );
			avgPkValTextField.setFont( new Font( null, Font.PLAIN, 9));
			avgPkValTextField.setToolTipText( "Avg Peak Pixel Value over all centroids" );
			avgPkValTextField.setMinimumSize( new Dimension( 35, 18 ) );
			avgPkValTextField.setPreferredSize( new Dimension( 35, 18 ) );
			
			JLabel tmpLabel1 = new JLabel( "Max" );
			tmpLabel1.setFont( new Font( null, Font.PLAIN, 9));
			pkValPanel.add( tmpLabel1 );
			
			pkValPanel.add( pkValTextField );
			
			JLabel tmpLabel2 = new JLabel( "Avg" );
			tmpLabel2.setFont( new Font( null, Font.PLAIN, 9));
			pkValPanel.add( tmpLabel2 );
			
			pkValPanel.add( avgPkValTextField );
		}

		// Set up the "Brightness and BkgdSubtraction Display"
		{
			displayBrightnessPanel = new JPanel();
			displayBrightnessPanel.setBorder( BorderFactory.createTitledBorder
						( 	new LineBorder( Color.blue ), 
							"Brt/Bkgd Setting",
							TitledBorder.CENTER, 
							TitledBorder.CENTER, 
							new Font( null, Font.BOLD, 9) 
						) 
					);
			displayBrightnessPanel.setPreferredSize( new Dimension( 140, 45 ) );

			nf.setMinimumIntegerDigits( 1 );		
			nf.setGroupingUsed( false );
			
			bkgdTextField = new JTextField();
			bkgdTextField.setMinimumSize( new Dimension( 35, 18 ) );
			bkgdTextField.setPreferredSize( new Dimension( 35, 18 ) );
			bkgdTextField.setSize( new Dimension( 35, 18 ) );
			bkgdTextField.setText( nf.format( ColorBar.BACKGROUND_START_LEVEL  ) ); 
			bkgdTextField.setFont( new Font( null, Font.PLAIN, 10) ); 
			bkgdTextField.setToolTipText( "bkgd level for display" );
			bkgdTextField.addActionListener( new ActionListener()
				{
					public void
					actionPerformed( ActionEvent evt )
					{
						String text = new String( bkgdTextField.getText() );

						try
						{
							int reqBkgdLevel = Integer.parseInt( text );
							myColorBar.setBackGround( reqBkgdLevel );
						}
						catch(NumberFormatException e)
						{
							msg.errorMsg( "action Listener for bkgdTextField", "Only INTS are acceptable." );
						}
						catch(Exception ee)
						{
							msg.errorMsg( "action Listener for bkgdTextField",
									  new String( " "+ee+"--- The text field can't handle an action." ) );
						}
					}
				});

			brightTextField = new javax.swing.JTextField();
			brightTextField.setMinimumSize( new Dimension( 35, 18 ) );
			brightTextField.setSize( new Dimension( 35, 18 ) );
			brightTextField.setPreferredSize( new Dimension( 35, 18 ) );
			brightTextField.setText( nf.format( ColorBar.BRIGHTNESS_START_LEVEL  ) ); 
			brightTextField.setFont( new Font( null, Font.PLAIN, 10) ); 
			brightTextField.setToolTipText( "brightness Level for display" );
			brightTextField.addActionListener( new ActionListener()
				{
					public void
					actionPerformed( ActionEvent evt )
					{
						String text = new String( brightTextField.getText() );

						try
						{
							int reqBrtLevel = Integer.parseInt( text );
							myColorBar.setBrightness( reqBrtLevel );
						}
						catch(NumberFormatException e)
						{
							msg.errorMsg( "action Listener for brightTextField", "Only INTS are acceptable." );
						}
						catch(Exception ee)
						{
							msg.errorMsg( "action Listener for brightTextField",
									  new String( " "+ee+"--- The text field can't handle an action." ) );
						}
					}
				});

			JLabel tmpLabel1 = new JLabel( "Brt"  );
			tmpLabel1.setFont( new Font( null, Font.PLAIN, 10) ); 
			displayBrightnessPanel.add( tmpLabel1 );
			displayBrightnessPanel.add( brightTextField );
			
			JLabel tmpLabel2 = new JLabel( "Bkgd"  );
			tmpLabel2.setFont( new Font( null, Font.PLAIN, 10) ); 
			displayBrightnessPanel.add( tmpLabel2 );
			displayBrightnessPanel.add( bkgdTextField );
			
		}
		
		this.add( displayPanel );
		this.add( pkValPanel );
		this.add( displayBrightnessPanel );
		this.setBorder( new LineBorder( java.awt.Color.black, 3 ) );

		super.setSize( new Dimension( 300, 480 ) );
		this.setSize( new Dimension( 300, 480 ) );
		this.setPreferredSize( new Dimension( 300, 480 ) );
		this.setVisible( true );
	}
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
