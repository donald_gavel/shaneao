/*
* ActuatorPanel.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

import java.util.*;

import java.lang.*;

import java.io.*;
import java.text.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


import java.lang.IllegalArgumentException;
import java.lang.Exception;
import java.lang.InstantiationException;

import java.text.NumberFormat;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;

public class ActuatorPanel extends AoPanel
{
	//
	// Private DEFINES
	// Note that these are private defines for internal program use
	//
	private static final int PANEL_PIXEL_XSIZE = 800;
	private static final int PANEL_PIXEL_YSIZE = 800;

	private static final int MAX_ACT_HEIGHT = 1;			// TODO - read this for each type DM!! 
	private static final int MIN_ACT_HEIGHT = -1;			// TODO - read this for each type DM!!

	//
	// LOCAL PRIVATE
	//
	private ActuatorDisplayPanel displayPanel = null;
	private ColorBar myColorBar = null;
	
	protected static boolean startDebug = false;
	
	private	float[] myData;
	
	private JMenu[] myMenuList = null;

	private static final File defaultSavePath = new File( "./JavaSave/", "Actpos" );
	
	private static final String thisClassName = "ActuatorPanel";

	private NumberFormat nf = NumberFormat.getInstance();

	private final int imageXSize = 10;		/* in pixels, size of Actuator image, if rectangular, else diameter*/
	private final int imageYSize = 10;
 
	private float[] dataRange;	/* Modulator Limits */ 

	// TODO ( read this! )
	private float offsetHeight = -1;								// -1 is default - changed after value read from modulator
	private float scaleHeight = (float )Math.abs( 250/( MAX_ACT_HEIGHT - MIN_ACT_HEIGHT) );

	//
	// LOCAL PROTECTED
	//
	protected String flatPathName = null;

	protected String myDmName = "dm1";
	protected int myIntegratorObjInd = 2;
	protected int myDmObjInd = 3;

	public JFrame getFrame()
	{ 
		return null; 
	}

	public void destroyPanel()
	{
		SystemPanel.theUpdater.removeListener( this );
		setVisible( false );
		thisFrame.dispose();
	}

	float getScaleHeight() { return (scaleHeight ); }

	float getOffsetHeight() { return (offsetHeight ); }

	float[] getDataRange() { return (dataRange ); }


	// ============================================================================================================================
	// Internal Class - ImageDisplayJFrame
	// ============================================================================================================================
	
	public static AOPanelInterface getFramedPanel( AoGuiStructure aoGuiSetup, int integratorObjIndex, int dmObjIndex, String dmName, String thisTitle )  throws IOException,InstantiationException
	{
		MsgHandler.genericInfoMsg( thisClassName, "Get a Framed Actuator Panel" );
		
		JFrame aFrame = new JFrame();

		final AOPanelInterface myPanel = new ActuatorPanel( aoGuiSetup, integratorObjIndex, dmObjIndex, dmName );
		
		myPanel.setEnclosingFrame( aFrame );
		
		MsgHandler.genericDebugMsg( "getFramedPanel(): Setting Title");
		aFrame.setTitle( thisClassName );
		
		MsgHandler.genericDebugMsg( "getFramedPanel(): Setting Menu");
		aFrame.setJMenuBar( myPanel.getMyMenuBar() );
		MsgHandler.genericDebugMsg( "getFramedPanel(): Adding panel to frame");
		aFrame.getContentPane().add( (JPanel)myPanel );

		MsgHandler.genericDebugMsg( "getFramedPanel(): Setting Size based on the inside panel size");
		Dimension panelSize = ((JPanel)myPanel).getSize();
		aFrame.setSize( new Dimension( panelSize.width + 20, panelSize.height + 320 ) );
		aFrame.setResizable( true );

		MsgHandler.genericDebugMsg( "getFramedPanel(): Add window Listener");
		aFrame.addWindowListener( new WindowAdapter()
			{
				public void	windowClosed( WindowEvent e ) { MsgHandler.genericInfoMsg( thisClassName, " Frame Closed2 "); }

				public void	windowClosing( WindowEvent evt )
				{
					MsgHandler.genericInfoMsg( thisClassName, " Frame Closing1 ");
					myPanel.destroyPanel();
				}
			});

		aFrame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		aFrame.setLocation( ((integratorObjIndex<4)? 420 : 680), 0 );
		aFrame.pack();
		aFrame.setVisible( true );
			
		MsgHandler.genericInfoMsg( "getFramedPanel(): Done");
		return myPanel;
	}

	public void myUpdateHook( float[] f_data, int type ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][] f_data, int[] types ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][][] ff_data, int[] types ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( String s_data ) 
	{
		if( updateInProgress )
		{
			msg.debugMsg( "myUpdateHook - Called but being ignored (Update in progress)" );
			return;
		}
	
		String inputLine = new String( s_data );			// Do copy and allow s_data to go away!
		msg.debugMsg( "myUpdateHook - Requesting update processing" );
		
		int dataType = -1;
		int numElem = -1;
		String[] strValues = null;
		
		System.out.println("\n( myUpdateHook( String s_data ) ): Attempting to parse");
		
		dataType = (int)( inputLine.charAt(3) - '0');
		System.out.println( "\n\tData Type: " + dataType );

		switch( dataType )
		{
			default:
			case 1:	// WFS
			case 2:	// CENT
			case 3:	// INTEN
			case 5:	// TT
				System.out.println( "\n\tSkipping this packet" );
				return;			// Do nothing if it is the wrong type
				
			case 4:	// ACT_1
				if( !myDmName.equals( "dm1") )
					return;		// Not correct data - ignore
				strValues = inputLine.substring( 4, inputLine.length()-4).trim().split( " ", 5000);
				numElem = strValues.length;
				System.out.println( "\n\tNumber entries is: " + numElem );
				break;
			
			case 6:	// ACT_2
				if( !myDmName.equals( "dm2") )
					return;		// Not correct data - ignore
				strValues = inputLine.substring( 4, inputLine.length()-4).trim().split( " ", 5000);
				numElem = strValues.length;
				System.out.println( "\n\tNumber entries is: " + numElem );
				break;
		}

		// Create data save location if it doesn't already exist
		if( myData == null )
			myData = new float[ numElem ];
			
		System.out.print( "\n Data dump: \n " );
		for( int ii=0; ii<numElem; ii++ )
		{
			myData[ ii] = Float.parseFloat( strValues[ ii ] );
			if( ii < 10 )
				System.out.print( " " + myData[ii] );
		}
		System.out.println( " " );
		
		doUpdate( );
	}
	
	
	/**
	 * Set the Actuator positions
	 * Causes a redraw to occur
	 * @returns boolean true:ok, false:failed
	 */
	public void doUpdate( )
	{

		if( tmpStopUpdate || updateInProgress  )
			return;						// Flag for individual panels to either display or perform local update 


		if( myData == null || (myData.length != displayPanel.getLength()) )
		{
			return;
		}
		
		updateInProgress = true;		// Flag for updates under normal circumstances
		
		for( int jj=0; jj<10; jj++ )
			System.out.println( " Actuator height[" + jj + "] : " + myData[jj] );
		
		displayPanel.setHeight( myData );

		msg.debugMsg( " doUpdate - Done " );

		return;
	}
	
	public ActuatorPanel( AoGuiStructure aoGuiSetup, int myIntegratorObjInd, int myDmObjInd, String myDmName ) throws IOException,InstantiationException
	{
		super( thisClassName );
		thisPanel = this;
		msg = new MsgHandler( this );
		setPanelTitle( thisClassName );
		thisPanel = this;
		
		String MethodName = new String( "ActuatorPanel.Constructor( int, int, String)" );

		if( displayPanel != null )
			throw new IllegalArgumentException( "Setup already Done" );

		inputCount = 0;		// initialize frame counter
		
		msg.debugMsg( MethodName, "Started" );
		
		dataRange = new float[2];
		dataRange[0] = MIN_ACT_HEIGHT;				// TODO Fix this
		dataRange[1] = MAX_ACT_HEIGHT;				// TODO Fix this

		offsetHeight = dataRange[0];
		scaleHeight = ( float )Math.abs( 250/( dataRange[ 1 ]-dataRange[ 0 ] ) );

		this.myDmName = myDmName.toLowerCase();
		this.myDmObjInd = myDmObjInd;
		this.myIntegratorObjInd = myIntegratorObjInd;

		myMenuList = createMenuList();
		if( myMenuList == null )
			throw new InstantiationException( "Failed to initialize Menu List" );

		msg.debugMsg( MethodName, "Init Display Components" );

		if( !initComponents() )
			throw new IllegalArgumentException( "Failed to initialize display components" );

		msg.debugMsg( MethodName, "Init Display Components - Done" );

		panelType = ( myDmName.equals( "dm1") ) ? AoGuiStructure.PANEL_ACT_1 : AoGuiStructure.PANEL_ACT_2;
		int reqType = ( myDmName.equals( "dm1") ) ? AoGuiStructure.DATA_ACT_1 : AoGuiStructure.DATA_ACT_2;
   		SystemPanel.theUpdater.registerListener( this, reqType );

		msg.debugMsg( MethodName, "Done" );
		repaint();
	}

	private class Actuator
	{
		
		private FltPoint	myRawCenter = null;		// Raw x-y positions for each actuator
		private FltPoint	myRelCenter = null;		// Rel x-y positions for each actuator ( 0.0 to 1.0 )
		
		private float		myHeight;				// input height
		
		private int			heightIndex = 0;		// calculated from myHeight -  This is an index into the color table
		private FltPoint	ULC = null;				// Position of the UpperLeftCorner of the actuator image


		Actuator( FltPoint rawCenter, FltPoint relCenter, float height )
		{
			myHeight = height;
			myRawCenter = new FltPoint( rawCenter );
			myRelCenter = new FltPoint( relCenter );
			ULC = new FltPoint( (float )( relCenter.x * PANEL_PIXEL_XSIZE - imageXSize / 2.0f ),
								(float )( relCenter.y * PANEL_PIXEL_YSIZE - imageYSize / 2.0f ) );
		}

		float getActuatorHeight( )
			{ return myHeight; }
				
		void setHeight( float height )
		{
			myHeight = height;
			
			// Limit Checking on input Value
			myHeight = Math.max( myHeight, MIN_ACT_HEIGHT );		//TODO  read this
			myHeight = Math.min( myHeight, MAX_ACT_HEIGHT );		//TODO  read this
			
			heightIndex = ( int )( ( myHeight - offsetHeight ) * scaleHeight );
			
			// Error Checking
			if( heightIndex < 0 )
			{
				msg.debugMsg( "ActuatorImage.setHeight()",
					 "scaleHeight / offsetHeight / myHeight = " + scaleHeight + " / " + offsetHeight + " / " + myHeight );
				msg.debugMsg( "ActuatorImage.setHeight()", "heightIndex = " + heightIndex );
				heightIndex = 0;
			}

			// Error Checking
			if( heightIndex > 254 )
			{
				msg.debugMsg( "ActuatorImage.setHeight()",
					 "scaleHeight / offsetHeight / myHeight = " + scaleHeight + " / " + offsetHeight + " / " + myHeight );
				msg.debugMsg( "ActuatorImage.setHeight()", "heightIndex = " + heightIndex );
				heightIndex = 255;
			}
			return;
		}
		
		public void paint( Graphics g )
		{
			g.setColor( myColorBar.getColor( this.heightIndex ) );
			g.fillOval( (int )(this.ULC.x), (int)(this.ULC.y), imageXSize, imageYSize );

			g.setFont( new Font( null, Font.PLAIN, 8) );
			g.setColor( Color.black );
			g.drawString( nf.format( this.myHeight ), (int )(this.ULC.x), (int )((this.ULC.y) + imageYSize + 6) );
		}
	}	
	
	
	private class ActuatorDisplayPanel extends JPanel
	{
		private Actuator[] actuators = null;
		int len = 0;
		
		int getLength() { return len; }
				
		public void paint( Graphics g )
		{
			super.paint( g );
			for( int ii=0; ii<len; ii++ )
				actuators[ii].paint( g );
				
			updateInProgress = false;		// Updated all actuators
		}
	
		ActuatorDisplayPanel( ) throws IOException
		{
			String MethodName = new String( "ActuatorDisplayPanel Constructor()" );

			// The actuator center positions are passed in in single array
			// - first the x centers are given and then the y centers are given
			// - which means the array is 2X the number of actual newCenters
			// 
			MsgHandler.genericDebugMsg( "<getActPhysicalPos(String myDmName )> :", "Start" );
		
			float[] xyCenters = getActuatorGeom();
			
			len = xyCenters.length / 2;
			
			FltPoint[] rawCenter = new FltPoint[ len];
			FltPoint relCenter = new FltPoint();

			msg.debugMsg( MethodName, new String( "Number of Actuator Pos =" + len ) );

			// Set up centers
			float minX = Float.MAX_VALUE;
			float minY = Float.MAX_VALUE;
			float maxX = Float.MIN_VALUE;
			float maxY = Float.MIN_VALUE;

			float tmpX;
			float tmpY;
			
			for( int ii= 0; ii<len; ii++ )
			{
				tmpX = xyCenters[ ii ];
				tmpY = xyCenters[ ii + len ];
				
				rawCenter[ii] = new FltPoint( tmpX, tmpY );
				
				if( tmpX > maxX )
					maxX = tmpX;
				if( tmpX < minX )
					minX = tmpX;

				if( tmpY > maxY )
					maxY = tmpY;
				if( tmpY < minY )
					minY = tmpY;
			}

			System.out.println( " During setup of Actuator panel display, max_x / min_x / max_y / min_y = " +  maxX  + " / " + minX   + " / " +  maxY  + " / " + minY );

			// add a little breathing room on each side of display	
			minX = minX - ( float )Math.abs( maxX * 0.1f );
			minY = minY - ( float )Math.abs( maxY * 0.1f );
			maxX = maxX + ( float )Math.abs( maxX * 0.1f );
			maxY = maxY + ( float )Math.abs( maxY * 0.1f );

			float tmpX1 = 1.0f / (maxX-minX);
			float tmpX2 = minX / (maxX-minX);
			
			float tmpY1 = 1.0f / (maxY-minY);
			float tmpY2 = minY / (maxY-minY);
			
			System.out.println( " After fudging ::  max_x / min_x / max_y / min_y = " +  maxX  + " / " + minX  + " / " +  maxY  + " / " + minY );
			System.out.println( "                   tmpX1 / tmpX2 / tmpY1 / tmpY2 = " +  tmpX1 + " / " + tmpX2 + " / " +  tmpY1 + " / " + tmpY2 );

			actuators = new Actuator[ len];
			for( int ii= 0; ii<len; ii++ )
			{
				relCenter = new FltPoint( (rawCenter[ ii ].x * tmpX1 - tmpX2) , (rawCenter[ ii ].y * tmpY1 - tmpY2) );
				actuators[ii] = new Actuator( rawCenter[ii], relCenter, 0.0f /*height*/ );
				if( ii< 40 )
					System.out.println( "relCenter [" + ii + "] = " + relCenter + " //  RawCenter[] = " + rawCenter[ii] + "\n" );
			}
		}		

		boolean setHeight( float[] height )
		{
			if( height.length != len )
				return false;
				
			for( int ii= 0; ii<len; ii++ )
			{
				if( ii<10)
					System.out.println( " Setting actuator height[" + ii + "] : " + height[ii] );
				actuators[ii].setHeight( height[ii] );
			}
				
			repaint();
			return true;
		}
	}

	private boolean initComponents()
	{
		String MethodName = new String( "initComponents()" );
		msg.debugMsg( MethodName, "Started" );

		nf.setMaximumFractionDigits( 1 );

		myColorBar = new ColorBar( thisPanel, ColorBar.RED_WHITE, true );
		
		msg.debugMsg( MethodName, "Color Bar Done " );
		try
		{ 
			displayPanel = new ActuatorDisplayPanel( ); 
		  }
		  catch( IOException ioe ) 
		  {
			System.out.println( "\n Caught IO Exception : " + ioe.toString() );
			return false;
		}

		displayPanel.setBorder( new LineBorder( Color.red ) );
		displayPanel.setMinimumSize( new Dimension( PANEL_PIXEL_XSIZE, PANEL_PIXEL_YSIZE ) );
		displayPanel.setPreferredSize( new Dimension( PANEL_PIXEL_XSIZE, PANEL_PIXEL_YSIZE ) );

		Dimension currentSize = displayPanel.getSize(); 
		setSize( new Dimension( (currentSize.width + 15), (currentSize.height + 15) ) );

		setLayout( new java.awt.FlowLayout() );
		add( displayPanel, BorderLayout.NORTH );
		
		repaint();
		setVisible( true );
		msg.debugMsg( MethodName, "Done" );
		
		return true;
	}

	//GEN-END:initComponents

	
// ============================================================================================================================
// ============================================================================================================================
//  Menu Setup Stuff!
// ============================================================================================================================
// ============================================================================================================================
	
// ============================================================================================================================
// Start Internal Class - PanelMenuBar
// -----------------------------------
	public JMenuBar getMyMenuBar( )
	{
		JMenuBar aMenuBar = new JMenuBar();
		if( aMenuBar == null )
			msg.errorMsg(" Unable to get new JMenuBar()");
		if( myMenuList == null )
			msg.errorMsg(" My Menu List is NULL");
		for( int ii=0; ii<myMenuList.length; ii++)
			if( myMenuList[ii] != null )
				aMenuBar.add( myMenuList[ii] );
			
		return aMenuBar;
	}
	
	static final int numMenuItems = 2;
	
	JMenu[] createMenuList()
	{
		String MethodName = thisClassName + ".createMenuList()";
		
		JMenu theList[] = new JMenu[ numMenuItems ];

		// View Menu
		JMenu viewMenu = new JMenu( "Display");
		{
			final JMenuItem setColorMapItem = new JMenuItem( "Color Map" );
			{
				setColorMapItem.setFont( new Font( null, Font.PLAIN, 10));
				setColorMapItem.addActionListener( new ActionListener()
					{
						public void
						actionPerformed( ActionEvent evt )
						{
							msg.infoMsg( "You pushed the 'Select Color Map ' button" );
							JFrame myColorBarFrame = myColorBar.getFramedColorBar( thisPanel,  true );
						}
					});
				msg.debugMsg( "Color Map Menu Item setup completed" );
			}
			
			viewMenu.setFont( new Font( null, Font.BOLD, 10));
			viewMenu.add(setColorMapItem );
			msg.debugMsg( MethodName, "View Menu completed" );
		}

		// Debug Menu
		JMenu debugMenu = new JMenu( "Debug");
		{			
			final JMenuItem debugOn = new JMenuItem( "Debug On" );
			{
				debugOn.setFont( new Font( null, Font.PLAIN, 10));
				debugOn.addActionListener( new ActionListener()
					{ public void actionPerformed( ActionEvent evt ) { setDebugFlag( true ); } });
			}

			final JMenuItem debugOff = new JMenuItem( "Debug Off" );
			{
				debugOff.setFont( new Font( null, Font.PLAIN, 10));
				debugOff.addActionListener( new ActionListener()
					{ public void actionPerformed( ActionEvent evt ) { setDebugFlag( false ); } });
			}
			
			debugMenu.setFont( new Font( null, Font.BOLD, 10));
			debugMenu.add( debugOn );
			debugMenu.add( debugOff );
			msg.debugMsg( MethodName, "Save Menu completed" );
		}
			
		theList[0] = viewMenu;
		theList[1] = debugMenu;
			
		msg.debugMsg( MethodName, "Actuator Menu Bar completed" );
		
		return theList;
	}
	// -----------------------------
	// End Internal Class - PanelMenuBar
	// ============================================================================================================================

	
	float[] getActuatorGeom( )
	{
		String		geomFileName = null;
		float[]		dmGeomData = null;
		float[][]	tmpData = null;
		FitsReader	fitsReader = null;

 		try
		{
			// read the x part
			geomFileName = new String( SystemPanel.getGuiConfigBase() + "/" + myDmName + DEFAULT_FNAME_ACTPOS_X ); 
			System.out.println( "\nAttempt to open actuator position File(): " + geomFileName );
			fitsReader = new FitsReader( geomFileName );

			tmpData = (float[][])(fitsReader.getDataObject( 0 ));

			dmGeomData = new float[ 2 * fitsReader.dimSize[0] * fitsReader.dimSize[1] ];

			for( int j=0; j<fitsReader.dimSize[1]; j++ )
				for( int i=0; i<fitsReader.dimSize[0]; i++ )
					dmGeomData[ i + j * fitsReader.dimSize[0]  ] = tmpData[j][i];
			
			// Read the y part 
			geomFileName = new String( SystemPanel.getGuiConfigBase() + "/" + myDmName + DEFAULT_FNAME_ACTPOS_Y ); 
			System.out.println( "\nAttempt to open actuator position File(): " + geomFileName );
			fitsReader = new FitsReader( geomFileName );
		
			tmpData = (float[][])(fitsReader.getDataObject( 0 ));

			for( int j=0; j<fitsReader.dimSize[1]; j++ )
				for( int i=0; i<fitsReader.dimSize[0]; i++ )
					dmGeomData[ i + j * fitsReader.dimSize[0] +  fitsReader.dimSize[0] * fitsReader.dimSize[1] ] = tmpData[j][i];
			
		}
		catch( Exception e )
		{
			msg.errorMsg( "Unable to get actuator locations for DM, from file: " + geomFileName ); 
			return null;
		}

		return dmGeomData;
	}

}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
