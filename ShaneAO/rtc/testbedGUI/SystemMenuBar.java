/*
* SystemMenuBar.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.lang.*;
import java.io.*;

import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;

import java.util.*;

import javax.swing.*;
import javax.swing.event.*;

class SystemMenuBar extends JMenuBar
{
	SystemPanel myParent;
	
	private static boolean debug = false;

	private static final String thisClassName = "SystemMenuBar";
	

	public boolean getDebugState()
	{
		return debug;
	}

	public SystemMenuBar( SystemPanel parent )
	{
		myParent = parent;

		setMaximumSize( new Dimension( 400, 25 ) );
		setPreferredSize( new Dimension( 400, 25 ) );
		setSize(new Dimension( 400, 25 ) );
		
		// Config Menu
		JMenu configMenu = new JMenu( "Config");
		{			
			final JMenuItem setUpdateRate = new JMenuItem( "Update Rate" );
			{
				setUpdateRate.setFont( new Font( null, Font.PLAIN, 10));
				setUpdateRate.addActionListener(   
						new ActionListener() { public void actionPerformed( ActionEvent evt ) { myParent.theUpdater.setDisplayUpdateRate( ); } }
					);
			}

			configMenu.setFont( new Font( null, Font.BOLD, 10));
			configMenu.add( setUpdateRate );
		}
		
		// Debug Menu
		JMenu debugMenu = new JMenu( "Debug");
		{			
			final JMenuItem debugToggle = new JMenuItem( "Debug Toggle" );
			{
				debugToggle.setFont( new Font( null, Font.PLAIN, 10));
				debugToggle.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{ myParent.setDebugFlag( !myParent.getDebugFlag() ); }
					});
			}

			debugMenu.setFont( new Font( null, Font.BOLD, 10));
			debugMenu.add( debugToggle );
			
		}

		add( configMenu );
		add( debugMenu );
		setVisible( true);
	}

}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************