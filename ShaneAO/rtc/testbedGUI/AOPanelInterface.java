//******************************************************************************
// AOPanelInterface.java:	
//
// Version: 1.01
//
//
//******************************************************************************

import javax.swing.*;

public interface AOPanelInterface extends UpdateHookInterface, MsgReqInterface, ControllerInterface
{

	public JFrame getEnclosingFrame();
	
	public String getPanelTitle();
	
	public AoPanel getPanel();
	
	public void setEnclosingFrame( JFrame theFrame);
	
	public void destroyPanel();

	public JMenuBar getMyMenuBar( );
	
	public MsgHandler getMsgHandler();
	
	public int getPanelType();

}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************