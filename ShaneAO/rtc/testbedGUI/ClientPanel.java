import java.io.*;

import java.awt.*;
import java.awt.event.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import java.util.*;
import java.net.*;

import javax.swing.*;
import javax.swing.JDesktopPane;
import javax.swing.JPanel;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.plaf.basic.BasicFileChooserUI;

class ClientPanel extends SystemPanel
{
	static private final Dimension bigPanelSize = new Dimension( 450, 670 );
	
	
	private boolean autoUpdateEnabled = false;
	private boolean showUpdate = true;

	private int updateRatePerSecond = 1;
	
	private int autoUpdateCount	= 0;
	
	private static final String thisClassName = "ClientPanel[SystemPanel]";

	public ClientPanel()
	{
		super( );
		msg = new MsgHandler( this );
		
		System.out.println( "Initialize the GUI" );
		initPanel();
	}

	public static void main( String [] arg )
	{
		try
		{
			UIManager.LookAndFeelInfo [] look = new UIManager.LookAndFeelInfo[ 10 ];
			look = UIManager.getInstalledLookAndFeels();

			for( int ii= 0; ii<2; ii++ )
				MsgHandler.genericInfoMsg( "<ClientPanel.main> : ", look[ ii ].toString() );

			MsgHandler.genericInfoMsg( "<ClientPanel.main> : ", new String( UIManager.getSystemLookAndFeelClassName() ) );
			UIManager.setLookAndFeel( "javax.swing.plaf.metal.MetalLookAndFeel" );
		}
		catch(Exception e)
		{
			MsgHandler.genericInfoMsg( "<ClientPanel.main> : ", "Exception thrown my UIManager.setLookAndFeel(String className)" );
		}
		
		// Start the mess up
		ClientPanel sp = new ClientPanel();

		return;
	}		
 
	public void	myExit()
	{
		if( SystemPanel.theUpdater != null )
			SystemPanel.theUpdater.kill();

		System.exit( 0 );
	}

	private boolean initPanel()
	{
		System.out.println( " Start: InitPanel()");
		this.setTitle( "TestBed GUI" );
		this.setLocation( 400, 400 );

		if( bigPanel==null)
		{
			msg.errorMsg( " *** ERROR - Big Panel is NULL -- exiting");
			System.exit( 1 );
		}
	
		bigPanel.setPreferredSize( bigPanelSize);
		bigPanel.setMinimumSize( bigPanelSize);
		bigPanel.setSize( bigPanelSize);
		
		this.addWindowListener( new java.awt.event.WindowAdapter()
			{ public void windowClosing( java.awt.event.WindowEvent evt ) { myExit(); } });

		this.pack();
		this.setVisible(true);
		
		System.out.println( " Done: InitPanel()");
		
		repaint();
		
		return true;
	}
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
