/*
* AoGuiStructure.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.io.*;

import java.awt.*;
import java.awt.event.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import java.util.*;

class AoGuiStructure implements AoGuiInterface
{

	int numHostDms;
	int numPanels;
	AoPanelDef panels[] = null;

	static AoGuiStructure currAoGuiStructure = null;

	// Constructor currently sets the decault values and then reads the config file in on top of these
	private AoGuiStructure()
	{ 
		numPanels = 3;
	
		numHostDms = 2;
		
		// Currently hard coding until reader works
		panels = new AoPanelDef[10];
		
		panels[0] = new AoPanelDef();
		panels[0].type = PANEL_WFS;
		panels[0].menuName = "Wfs";
		panels[0].panelName = "Wave Front Sensor Image";
		panels[0].dataType = new int[1];
		panels[0].dataType[0] = DATA_WFS;
		panels[0].posX = 100;
		panels[0].posY = 0;
		panels[0].defaultOn = true;
		panels[0].satLimits = new int[2];
		panels[0].satLimits[0] = 0;
		panels[0].satLimits[1] = 4000;
		
		panels[1] = new AoPanelDef();
		panels[1].type = PANEL_CENT;
		panels[1].menuName = "Cent";
		panels[1].panelName = "Centroid Offsets";
		panels[1].dataType = new int[2];
		panels[1].dataType[0] = DATA_CENT;
		panels[1].dataType[1] = DATA_INTEN;
		panels[1].posX = 0;
		panels[1].posY = 200;
		panels[1].defaultOn = true;
		panels[1].satLimits = new int[2];
		panels[1].satLimits[0] = -2;
		panels[1].satLimits[1] = +2;

		panels[3] = new AoPanelDef();
		panels[3].type = PANEL_ACT_1;
		panels[3].menuName = "Act 1";
		panels[3].panelName = "DM #1 Actuators";
		panels[3].dataType = new int[1];
		panels[3].dataType[0] = DATA_ACT_1;
		panels[3].posX = 100;
		panels[3].posY = 200;
		panels[3].defaultOn = false;
		panels[3].satLimits = new int[2];
		panels[3].satLimits[0] = -1;
		panels[3].satLimits[1] = +1;

		panels[4] = new AoPanelDef();
		panels[4].type = PANEL_ACT_2;
		panels[4].menuName = "Act 2";
		panels[4].panelName = "DM #2 Actuators";
		panels[4].dataType = new int[1];
		panels[4].dataType[0] = DATA_ACT_2;
		panels[4].posX = 300;
		panels[4].posY = 200;
		panels[4].defaultOn = false;
		panels[4].satLimits = new int[2];
		panels[4].satLimits[0] = -10;
		panels[4].satLimits[1] = +10;

		panels[2] = null;
		panels[5] = null;
		panels[6] = null;
		panels[7] = null;
		panels[8] = null;
		panels[9] = null;
	
	}
	
	static AoGuiStructure getAoGuiStructure()
	{
		if( currAoGuiStructure != null)
			return currAoGuiStructure;
		
		return ( currAoGuiStructure = new AoGuiStructure() );
	}	
	
	// local class
	private class AoPanelDef
	{
		int		type;					// 0: WFS, 1: REF, 2: CENT, etc . . .
		String	subType;				// Panel dependent - Current ACT used "DM1","DM2", etc.
		String	menuName;				// Selection Menu Display name
		String	panelName;				// Panel display name
		int		dataType[];				// List of input datas required
		int		posX;					// PanelPos - X
		int		posY;					// PanelPos - Y
		boolean	defaultOn;				// Panel Start at program start???
		int[]	satLimits;				// Input value range [indx 0]:Min - [indx 1]:Max
		
		// Panel dependency file use:
		//
		//		WFS: 1-ref pos, 2,3-unused
		//		CENT: 1-ref pos, 2,3-unused
		//		REF: 1-ref posactuator positions, 2-unused, 3- unused
		//		ACT: 1-act pos, 2,3-unused
		//
		File	file1;					// Panel dependent input setup files (1,2,3)
		boolean file1_remote;			// Loction of file (gui, server)
		
		File	file2;
		boolean file2_remote;			// Loction of file (gui, server)
		
		File	file3;
		boolean file3_remote;			// Loction of file (gui, server)
	}

	static public String getHostAddr()
	{
		return hostAddress;
	}
	
	public int getNumHostDms()
	{
		return numHostDms;
	}

	public int getNumPanels()
	{
		return numPanels;
	}
	
	public String getMenuName( int panelNum)
	{
		return panels[panelNum].menuName;
	}

	public String getpanelName( int panelNum)
	{
		return panels[panelNum].menuName;
	}

	public int getPanelType( int panelNum )
	{
		return panels[panelNum].type;
	}
	
	public String	getServerDataAccess()
	{
		return serverDataAccess;
	}
	
	public int[] getSatLimits( int panelNum ) 
	{
		return panels[panelNum].satLimits;
	}
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
