/*
* CentroidPanel.java
*
* Created on June 18, 2002, 9:51 AM
* 
*/

//package MainFrame;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;

import java.util.*;
import java.text.*;

import java.io.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;


/**
 *
 * @author  sjones
 * 
 */

public class CentroidPanel extends AoPanel
{
	// Note that these are public defines for external program use
	public final static int			CENTER_OFF = 0;
	public final static int			CENTER_DOT = 1;
	public final static int			CENTER_CROSS = 2;
	public final static int			CENTER_CIRCLE = 3;
	private int						centerType = CENTER_CROSS;

	public final static int			DISPLAY_LINE = 1;
	private int						offsetDisplayType = DISPLAY_LINE;

	public final static int			DISPLAY_TILT = 0;
	public final static int			DISPLAY_NO_TILT = 1;

	private int						tiltDisplayType = DISPLAY_NO_TILT;

	private static final int		SINGLE_CENTROID_XSIZE = 14;					// BIG_PANEL
	private static final int		SINGLE_CENTROID_YSIZE = 14;					// BIG_PANEL

	private static final int		PANEL_PIXEL_XSIZE = SINGLE_CENTROID_XSIZE * (AoGuiInterface.NUM_CENT_PER_ROW + 1);
	private static final int		PANEL_PIXEL_YSIZE = SINGLE_CENTROID_YSIZE * (AoGuiInterface.NUM_CENT_PER_COL + 1);

	private final static float		NOMINAL_OFFSET = 0.2f;



	private StripChart				myRmsChart = null;				// RMS ( no Tilt )	

	// Variables declaration - do not modify
	private CentroidDisplayPanel	myDisplayPanel = null;
	
	private static final String		thisClassName = "CentroidPanel";
	
	private JPanel					zoomPanel = null;
	private JTextField				zoomTextField = null;

	private JTextField				xTiltMean_TextField = null;
	private JTextField				yTiltMean_TextField = null;

	private JMenu[]					myMenuList = null;

	private JSliderFloat			myJSlider = null;
	
	private static NumberFormat		nf = NumberFormat.getInstance();

	private FltPoint[]				relativeCenters = null;		// relative positions of centroids on display ( range 0.0 - 1.0 )

	private FltPoint[]				offsets = null;				// These define the 'err' that will be displayed 

	private FltPoint				tilt = null;

	private float[]					myData = null;
	
	private int[]					myActivePos = null;

	private float					offsetScaleFactor; /* camera to canvas? */
	private CentImage[]				centImageList = null;

	Color							backGround = Color.LIGHT_GRAY;
	Color							foreGround = Color.red;
	Color							centerColor = Color.blue;

	int								imgXSize = SINGLE_CENTROID_XSIZE; /* in pixels, size of centroid image*/
	int								imgYSize = SINGLE_CENTROID_YSIZE; /* in pixels, size of centroid image*/

	// ============================================================================================================================
	// AOPanelInterface Implementation
	// ============================================================================================================================

	public int getUpdCount()
	{
		return inputCount;
	}

	// ============================================================================================================================
	// Internal Class - ImageDisplayJFrame
	// ============================================================================================================================
	
	public static AOPanelInterface getFramedPanel( AoGuiStructure aoGuiSetup )
	{
		JFrame aFrame = new JFrame();
		aFrame.setTitle( thisClassName );
	
		MsgHandler.genericDebugMsg( " Create the Centroid Panel");
			
   		final AOPanelInterface myPanel = new CentroidPanel( aoGuiSetup );
		myPanel.setEnclosingFrame( aFrame );
		
		aFrame.setJMenuBar( myPanel.getMyMenuBar() );
		aFrame.getContentPane().add( (JPanel)myPanel );
		
		// Set the size of the Frame, based on the inside panel size
		Dimension panelSize = ((JPanel)myPanel).getSize();
		aFrame.setSize( new Dimension( panelSize.width + 220, panelSize.height + 285 ) );
		aFrame.setResizable( true );
		
		aFrame.addWindowListener( new java.awt.event.WindowAdapter()
			{
				public void windowClosed(WindowEvent e) 
				{
					MsgHandler.genericInfoMsg( thisClassName, " Frame Closed2 ");
				}

				public void	windowClosing( java.awt.event.WindowEvent evt )
				{
					MsgHandler.genericInfoMsg( thisClassName, " Frame Closing1 ");
					myPanel.destroyPanel();
				}
			});

		aFrame.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE );
		aFrame.setLocation( 500, 260 );

		aFrame.pack();
		aFrame.setVisible( true );

		return myPanel;
	}
	
	public void destroyPanel()
	{
		msg.infoMsg("CentroidPanel.destroyPanel()", "Kill the Display Updater");
		SystemPanel.theUpdater.removeListener( this );
		setVisible( false );
		thisFrame.dispose();
	}

	private int subApertureEnergyHandle = -1;
	
	
	// ============================================================================================================================
	// ============================================================================================================================
	// ============================================================================================================================
	// CentroidPanel Class
	// ============================================================================================================================
	// ============================================================================================================================
	// ============================================================================================================================
	
	public CentroidPanel( AoGuiStructure aoGuiSetup )
	{
		super( thisClassName );
		msg = new MsgHandler( this );
		setPanelTitle( thisClassName );
		setDebugFlag(false);
		thisPanel = this;
		panelType = AoGuiStructure.PANEL_CENT;
		
		String MethodName =  "CentroidPanel( )";

		float[] gridCentersXY = SystemPanel.getRefGrid();
		
		if( gridCentersXY == null )
			return;
	
		int len = gridCentersXY.length/2;
		FltPoint[] lcenters = new FltPoint[ len ];

		for( int ii= 0; ii<len; ii++ )
			lcenters[ ii ] = new FltPoint( gridCentersXY[ ii ], gridCentersXY[ ii+len ] );

		constructCentroidPanel( lcenters );
		
		msg.debugMsg( MethodName, "Centroid Constructor:: register for Update!!");
		
		int[] types = new int[2];

		types[0] = AoGuiStructure.DATA_CENT;
		types[1] = AoGuiStructure.DATA_INTEN;

   		SystemPanel.theUpdater.registerListener( this, types );
	}


	private void constructCentroidPanel( Point2D.Float [] rawCenterPos )
	{
		inputCount = 0;

		String MethodName = new String( "constructCentroidPanel( Point2D.Float[] )" );

		tilt = new FltPoint( 0.0, 0.0 );

		nf.setMaximumFractionDigits( 2 );

		if( relativeCenters!=null )
			throw new IllegalArgumentException( "centers already set up" );

		if( rawCenterPos==null )
			throw new IllegalArgumentException( "Parameter 'center position array' is null" );

		int len = rawCenterPos.length;
		msg.debugMsg( MethodName, new String( "Number of Centroid Positions ="+len ) );

		// Set up centers
		relativeCenters = new FltPoint[ len ];
		float minX = Float.MAX_VALUE;
		float minY = Float.MAX_VALUE;
		float maxX = Float.MIN_VALUE;
		float maxY = Float.MIN_VALUE;

		for( int ii= 0; ii<len; ii++ )
		{
			if( rawCenterPos[ ii ].x>maxX )
				maxX = rawCenterPos[ ii ].x;

			if( rawCenterPos[ ii ].x<minX )
				minX = rawCenterPos[ ii ].x;

			if( rawCenterPos[ ii ].y>maxY )
				maxY = rawCenterPos[ ii ].y;

			if( rawCenterPos[ ii ].y<minY )
				minY = rawCenterPos[ ii ].y;
				
			if( ii<10 )
				msg.debugMsg( MethodName, new String( " rawCenterPos : " + rawCenterPos[ ii ].x + 
													  " / " + rawCenterPos[ ii ].y ) );
		}

		msg.debugMsg( MethodName, new String( " MinX / MinY / MaxX / MaxY = "+minX+" / "+minY+" / "+maxX+" / "+maxY ) );
		minX = minX-( float )Math.abs( minX*0.1f );
		minY = minY-( float )Math.abs( minY*0.1f );
		maxX = maxX+( float )Math.abs( maxX*0.1f );
		maxY = maxY+( float )Math.abs( maxY*0.1f );
		msg.debugMsg( MethodName, new String( " MinX / MinY / MaxX / MaxY = "+minX+" / "+minY+" / "+maxX+" / "+maxY ) );

		for( int ii= 0; ii<len; ii++ )
		{
			relativeCenters[ ii ] = new FltPoint( ( (float)rawCenterPos[ ii ].x-minX )/( (float)maxX-minX ), ( (float)rawCenterPos[ ii ].y-minY )
																								   /( (float)maxY-minY ) );
			if( ii<10 )
				msg.debugMsg( MethodName, new String( " relativeCenter : "+relativeCenters[ ii ].x+" / "+relativeCenters[ ii ].y ) );
		}

		// Zero the offsets
		offsets = new FltPoint[ len ];

		for( int ii= 0; ii<len; ii++ )
			offsets[ ii ] = new FltPoint();

		if( createCentroidPanel( rawCenterPos )==false )
			throw new IllegalArgumentException( "Failed to create Centroid Panel" );

		myMenuList = createMenuList( );

		msg.debugMsg( MethodName, "RePaint" );
		repaint();

		this.setLayout( new java.awt.FlowLayout() );

		Dimension tmpDim = new Dimension( PANEL_PIXEL_YSIZE + 65, PANEL_PIXEL_YSIZE + 210 );

		this.setPreferredSize( tmpDim );
		this.setSize( tmpDim );

		this.setBorder( new LineBorder( Color.black, 3 ) );

		setVisible(true);
	}

	class CentroidDisplayPanel extends JPanel
	{
		public void paint( Graphics g )
		{
			tmpStopUpdate = true;

			int len = centImageList.length;
			msg.debugMsg( "<paint(Graphics g)>", "3 f: " + foreGround + ", b: " + backGround + ", c: " + centerColor );

			for( int ii=0; ii<len; ii++ )
				centImageList[ ii ].paint( g );

			msg.debugMsg( "<paint(Graphics g)>", "Repainted ALL centroids, len = " + len );
				
			tmpStopUpdate = false;
		}
	}

	private boolean createCentroidPanel( Point2D.Float[] rawCenterPos )
	{
		String MethodName = new String( "<createCentroidPanel()>" );
		msg.debugMsg( MethodName, "Started" );

		initComponents();

		offsetScaleFactor = myJSlider.getFloatValue();

		int len = relativeCenters.length;
		centImageList = new CentImage[ len ];
		
		myActivePos = new int[ len ];
		for( int jj=0; jj<len; jj++)
			myActivePos[ jj] = 1;			// TODO

		float[] refAperture = SystemPanel.getRefAperture();

		for( int ii= 0; ii<len; ii++ )
			centImageList[ ii ] = new CentImage( relativeCenters[ ii ], (refAperture[ii] != 0.0) );

		repaint();

		msg.debugMsg( MethodName, "Stopped" );
		return true;
	}

	// ===============================================================
	// Start of inner Class "CentImage"
	// ===============================================================
	
		class CentImage
		{
			
			FltPoint myOffset = null;

			float xMin = 0.0f;	// Max and Min positions for this particular centroid image
			float xMax = 0.0f;
			float yMin = 0.0f;
			float yMax = 0.0f;

			private FltPoint myCenter = null;
			private FltPoint myRelativeCenter = null;

			private boolean myActiveFlag = false;
			private boolean	myUsedFlag = false;

			float displayScaleX = 1.0f;
			float displayScaleY = 1.0f;

			boolean setOffset( FltPoint _offset, int currEnergy )
			{
				myActiveFlag = ( currEnergy == 0 ) ? false : true;
				myOffset = _offset;
				repaint();
				return true;
			}

			CentImage( FltPoint relativeCenter, boolean usedFlag )
			{
				float centXsize = imgXSize;
				float centYsize = imgYSize;

				myOffset = new FltPoint( 0.0f, 0.0f );

				xMin = -centXsize/2.0f;
				xMax = +centXsize/2.0f;
				yMin = -centYsize/2.0f;
				yMax = +centYsize/2.0f;

				myRelativeCenter = new FltPoint( relativeCenter );
				myCenter = new FltPoint( (relativeCenter.x + 0.05) * PANEL_PIXEL_XSIZE, (relativeCenter.y + 0.05) * PANEL_PIXEL_YSIZE );

				myUsedFlag = usedFlag;
				displayScaleX = centXsize / 4 / NOMINAL_OFFSET;
				displayScaleY = centYsize / 4 / NOMINAL_OFFSET;
			}

			public void paint( Graphics g )
			{
				FltPoint ULCorner = new FltPoint( myCenter.x+xMin, myCenter.y+yMin );

				// The following will redraw the rectangle and center unless it is a multidot type presentation
				g.setColor( backGround );

				g.drawRect( (int )ULCorner.x, (int )ULCorner.y, imgXSize, imgYSize );

				if( myUsedFlag )
					g.setColor( centerColor );
				else
					g.setColor( backGround );

				switch( centerType )
				{
				case CENTER_DOT:
					{
						// draw dot
						g.drawLine( (int )myCenter.getX(), (int )myCenter.getY(), (int )myCenter.getX(), (int )myCenter.getY() );
					}
					break;

				case CENTER_CROSS:
					{
						// draw horizontal then vertical lines of cross
						g.drawLine( (int )( myCenter.getX()-2 ), (int )( myCenter.getY()), (int )( myCenter.getX()+2 ),
									(int )( myCenter.getY()) );
						g.drawLine( (int )( myCenter.getX()), (int )( myCenter.getY()-2 ), (int )( myCenter.getX()),
									(int )( myCenter.getY()+2 ) );
					}
					break;

				case CENTER_CIRCLE:
					{
						// draw circle
						int circleSize = imgXSize/2;
						g.drawOval( (int )( myCenter.getX()-circleSize/2 ), (int )( myCenter.getY()-circleSize/2 ), circleSize, circleSize );
					}
					break;

				default:
					break;
				}

				g.setColor( foreGround );

				// if "myActiveFlag" == false, then assume that it is a bad point and put a cross there
				if( myActiveFlag == false )
				{
					g.drawLine( (int )ULCorner.x, (int )ULCorner.y, (int )( ULCorner.x + imgXSize ), (int )( ULCorner.y + imgYSize ) );
					g.drawLine( (int )( ULCorner.x + imgXSize ), (int )ULCorner.y, (int )ULCorner.x, (int )( ULCorner.y + imgYSize ) );
				}
				else
				{
					FltPoint displayOffset = scaleDisplayPosition( myOffset );
					g.drawLine( (int)(myCenter.x), (int)(myCenter.y), 
								(int)( myCenter.x + displayOffset.x), (int)( myCenter.y + displayOffset.y) );
				}
			}

			private FltPoint scaleDisplayPosition( FltPoint orig )
			{
				float x = 0.0f;
				float y = 0.0f;

				if( myActiveFlag == true  )
				{
					if( tiltDisplayType!=DISPLAY_TILT )
					{
						x = ( orig.x - tilt.x ) * offsetScaleFactor * displayScaleX;
						y = ( orig.y - tilt.y ) * offsetScaleFactor * displayScaleY;
					}
					else
					{
						x = ( orig.x ) * offsetScaleFactor * displayScaleX;
						y = ( orig.y ) * offsetScaleFactor * displayScaleY;
					}
				}			

				// Make sure the offset stays within the box!
				//
				float xRatio = 1.0f;
				float yRatio = 1.0f;

				if( x>xMax )
					xRatio = ( float )( x/xMax );

				if( x<xMin )
					xRatio = ( float )( x/xMin );

				if( y>yMax )
					yRatio = ( float )( y/yMax );

				if( y<yMin )
					yRatio = ( float )( y/yMin );

				if( xRatio>yRatio )
					yRatio = xRatio;
				else
					xRatio = yRatio;

				x /= xRatio;
				y /= yRatio;

				return ( new FltPoint( x, y ));
			}
		}
	
	// ===============================================================
	// End of inner Class "CentImage"
	// ===============================================================


	public void myUpdateHook( float[] f_data, int type ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][] f_data, int[] types ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( float[][][] ff_data, int[] types ) { System.out.println( "Dummy 'myUpdateHook( f[])'"); }
	public void myUpdateHook( String s_data ) 
	{

		if( updateInProgress )
		{
			msg.debugMsg( "myUpdateHook - Called but being ignored (Update in progress)" );
			return;
		}
		
		String inputLine = new String( s_data );			// Do copy and allow s_data to go away!
		msg.debugMsg( "myUpdateHook - Requesting update processing" );
		
		int dataType = -1;
		int numElem = -1;
		String[] strValues = null;
		
		msg.debugMsg("\n Attempting to parse");
		System.out.println("\n Attempting to parse");
		
		dataType = (int)( inputLine.charAt(3) - '0');
		msg.debugMsg( "\n\tData Type: " + dataType );

		switch( dataType )
		{
				
			case 2:	// CENT
				break;
				
			default:
			case 1:	// WFS
			case 3:	// INTEN
			case 4:	// ACT_1
			case 5:	// TT
			case 6:	// ACT_2
				return;			// Do nothing if it is the wrong type
		}

		strValues = inputLine.substring(4, inputLine.length()-4).trim().split( " ", 100*100 );
		numElem = strValues.length;
		msg.debugMsg( "\n\tNumber entries is: " + numElem );
		System.out.println( "\n\tNumber entries is: " + numElem );

		// Create data save location if it doesn't already exist
		if( myData == null )
			myData = new float[ numElem ];
		

		msg.debugMsg( " Data dump: \t" );
		for( int ii=0; ii<numElem; ii++ )
		{
			myData[ ii] = Float.parseFloat( strValues[ ii ] );

			if( (ii<10) )
				msg.debugMsg( "  " + myData[ ii] );
		}
		msg.debugMsg("");


		// Test if we have the entire image
		
			// return if not!!
				
		// TODO - Fix this so update is not done on every row!!
				
		doUpdate( );
	}
	

	/**
	 * Set the centroid offsets
	 * Causes a redraw to occur
	 * @returns boolean true:ok, false:failed
	 */
	public void doUpdate( )
	{
	
		msg.debugMsg( "\n ***** TRY TO START 'DOUPDATE' ");
		
		if( tmpStopUpdate || updateInProgress  )
		{
			System.out.println( "\n ***** UPDATE ALREADY IN PROGRESS -- RETURNING");
			return;										// Flag for individual panels to either display or perform local update 
		}


		if( myData == null )
		{
			System.out.println( "\n ***** INPUT DATA WAS NULL");
			return;
		}
		
		float[] newOffsets = myData;
		
		String MethodName = new String( "<CentroidPanel.doUpdate( )>" );
		
		int len = newOffsets.length / 2;
	
		msg.debugMsg( MethodName, "Start Centroid Update");
		
		if( offsets.length != len )
		{
			msg.errorMsg( "Input offsets array size not equal to current array size" );
			return;
		}

		inputCount++;
		
		updateInProgress = true;		// Flag for updates under normal circumstances

		msg.debugMsg( "********  Start Update for CENTROIDS" );

		// First Update the Centroid display window and  RMS
		{
			// In this case the newOffsets are passed in in single array
			// - first the x offsets are given and then the y offsets are given
			// - which means the array is 2X the number of actual newOffsets
			// 
			
			msg.debugMsg( MethodName, " Start - Centroid Display update" );

			int numAct = 0;
			tilt = new FltPoint( 0.0, 0.0 );
			double d_rms = 0.0;
			
			int ii = 0;
			try
			{
				msg.debugMsg( MethodName, "  Start Transfer into 'offsets' " );

				for( ii=0; ii<len ; ii++ )
				{
					offsets[ ii ] = new FltPoint( newOffsets[ ii ], newOffsets[ ii+len ] );
				
					if( myActivePos[ ii] != 0  )
					{
						numAct++;
						tilt.x += offsets[ ii ].x;
						tilt.y += offsets[ ii ].y;
					}
				}
				
				msg.debugMsg( MethodName, new String( " ### NUM ACTIVE ACTUATORS = " + numAct ) );
			
				if( numAct > 0 )
				{
					tilt.x /= numAct;
					tilt.y /= numAct;
				}

				xTiltMean_TextField.setText( nf.format( tilt.x ) );
				yTiltMean_TextField.setText( nf.format( tilt.y ) );

				for( ii=0; ii<len ; ii++ )
				{
					if( myActivePos[ii] != 0 )
						d_rms += ( offsets[ ii ].distanceSq( tilt ));
						
					if( debug && ( ii < (len>>5) ) )
					{
						msg.debugMsg( MethodName, new String( "Update 'myLittleCentroid' # " + ii ) );
						msg.debugMsg( MethodName, new String( "	  Offsets = " + offsets [ii] ) );
					}
						
					centImageList[ ii ].setOffset( offsets[ ii ], myActivePos[ii] );
				}


				msg.debugMsg( MethodName + " Start RMS Update");
				
				float rms = (numAct > 1) ? (float)Math.sqrt( d_rms / (numAct-1) ) : 0.0f;
					
				msg.debugMsg( "RMS = " + rms );

				myRmsChart.doUpdate( rms );

				msg.debugMsg( MethodName + " Done RMS Update");
				
			}
			catch(Exception e)
			{
				msg.errorMsg( MethodName, "Exception : "+e );
				msg.debugMsg( MethodName, new String( "II / len = " + ii + " / "+len ) );
				msg.debugMsg( MethodName, new String( "newOffsets[ii] (x/y) = " + newOffsets[ ii ] + " / " + newOffsets[ ii+len ] ) );
				msg.debugMsg( MethodName, new String( "offsets = " + offsets[ ii ] ) );
				return;
			}
		}

		// super.repaint();
		
		updateInProgress = false;						// Flag that update is done

		msg.debugMsg( MethodName, " Done" );

		return;
	}

	private void initComponents()
	{
		//GEN-BEGIN:initComponents
		myDisplayPanel = new CentroidDisplayPanel();

		Dimension tmpDim = new Dimension( PANEL_PIXEL_XSIZE, PANEL_PIXEL_YSIZE );

		myDisplayPanel.setBorder( new LineBorder( Color.red ) );
		myDisplayPanel.setMinimumSize( tmpDim );

		myDisplayPanel.setPreferredSize( tmpDim );
		myDisplayPanel.setSize( tmpDim );
		myDisplayPanel.setLayout( new java.awt.FlowLayout() );

		// Construct the zoom panel
		{
			myJSlider = new JSliderFloat( JSliderFloat.SCALE_TYPE_LOG, 0.01f, 100f, 1f, 3, 60 );
			zoomPanel = new JPanel();
			zoomTextField = new JTextField();
			zoomPanel.setBorder( BorderFactory.createTitledBorder
											( 	new LineBorder( Color.blue ), 
												"Centroid Zoom",
												TitledBorder.CENTER, 
												TitledBorder.CENTER, 
												new Font( null, Font.BOLD, 9) 
											) 
										);

			zoomPanel.setLayout( new java.awt.GridBagLayout() );

			myJSlider.addChangeListener( new SliderListener() );

			zoomTextField.setHorizontalAlignment( JSlider.CENTER );
			zoomTextField.setText( nf.format( myJSlider.getFloatValue() ) );
			zoomTextField.setToolTipText( "Enter Zoom Value" );
			zoomTextField.setMinimumSize( new java.awt.Dimension( 45, 20 ) );
			zoomTextField.setPreferredSize( new java.awt.Dimension( 45, 20 ) );

			zoomTextField.addActionListener( new ActionListener()
				{
					public void
					actionPerformed( ActionEvent evt )
					{
						zoomTextFieldActionPerformed( evt );
					}
				});
				
			zoomPanel.add( myJSlider );
			zoomPanel.add( zoomTextField );
		}

		JPanel tiltInfoPanel = new JPanel();
		// Set up the Tilt INFO Panel
		{
			tiltInfoPanel.setBorder( BorderFactory.createTitledBorder
											( 	new LineBorder( Color.blue ), 
												"Mean Tilt (microns_phase_slope)",
												TitledBorder.CENTER, 
												TitledBorder.CENTER, 
												new Font( null, Font.BOLD, 9) 
											) 
										);
			tiltInfoPanel.setPreferredSize( new Dimension( 145, 55 ) );

			JLabel xTiltMean_Label = new JLabel( "X:" );
			xTiltMean_Label.setFont( new Font( null, Font.PLAIN, 9));

			JLabel yTiltMean_Label = new JLabel( "Y:" );
			yTiltMean_Label.setFont( new Font( null, Font.PLAIN, 9));

			xTiltMean_TextField = new JTextField();
			xTiltMean_TextField.setToolTipText( "Mean X Tilt in Pixels (read only)" );
			xTiltMean_TextField.setSize( new java.awt.Dimension( 30, 20 ) );
			xTiltMean_TextField.setPreferredSize( new java.awt.Dimension( 30, 20 ) );

			xTiltMean_TextField.setFont( new Font( null, Font.PLAIN, 9));
			xTiltMean_TextField.setText( nf.format(  -0.00001 ) );

			yTiltMean_TextField = new JTextField();
			yTiltMean_TextField.setToolTipText( "Mean Y Tilt in Pixels (read only)" );
			yTiltMean_TextField.setSize( new java.awt.Dimension( 30, 20 ) );
			yTiltMean_TextField.setPreferredSize( new java.awt.Dimension( 30, 20 ) );
			yTiltMean_TextField.setFont( new Font( null, Font.PLAIN, 10));
			yTiltMean_TextField.setText( nf.format( -0.00001  ) );

			tiltInfoPanel.add( xTiltMean_Label );
			tiltInfoPanel.add( xTiltMean_TextField );

			tiltInfoPanel.add( yTiltMean_Label );
			tiltInfoPanel.add( yTiltMean_TextField );
		}

		myRmsChart = new StripChart( msg, "", 180, 40, 100, true, 0.0f, (float)( NOMINAL_OFFSET )  );
		myRmsChart.setBorder( BorderFactory.createTitledBorder
											( 	new LineBorder( Color.DARK_GRAY ), 
												"Cent RMS Values (um_phase_slope)",
												TitledBorder.CENTER, 
												TitledBorder.CENTER, 
												new Font( null, Font.BOLD, 9) 
											) 
										);
										
		myRmsChart.setPreferredSize( new Dimension( 220, 80 ) );
		myRmsChart.setMinimumSize( new Dimension( 220, 80 ) );
		
		JPanel biggerPanel = new JPanel();
		biggerPanel.add( zoomPanel );
		biggerPanel.add( myRmsChart );
		biggerPanel.add( tiltInfoPanel );
		
		biggerPanel.setBorder( BorderFactory.createLineBorder(Color.white));

		tmpDim = myDisplayPanel.getSize( );
		tmpDim = new Dimension( tmpDim.width + 55, tmpDim.height + 160 );

		biggerPanel.setPreferredSize( tmpDim );
		biggerPanel.setMaximumSize( tmpDim );
		biggerPanel.setSize( tmpDim );

		add( myDisplayPanel, BorderLayout.WEST );
		add( biggerPanel, BorderLayout.EAST );

		setVisible( true );
	}

	private void zoomTextFieldActionPerformed( ActionEvent evt )
	{
		// Add your handling code here:
		String text = new String( zoomTextField.getText() );

		try
		{
			myJSlider.setFloatValue( Float.parseFloat( text ) );
		}
		catch(NumberFormatException e)
		{
			msg.errorMsg( "zoomTextFieldActionPerformed(ActionEvent)", "Only floats are acceptable." );
		}
		catch(Exception ee)
		{
			msg.errorMsg( "zoomTextFieldActionPerformed(ActionEvent)",
					  new String( " " + ee + "--- The text field can't handle an action." ) );
		}
	}

	class SliderListener implements ChangeListener
	{
		public void
		stateChanged( ChangeEvent e )
		{
			float SliderPositionFloat = 0f;
			int SliderPositionInt = 0;
			String sliderPositionString = null;

			JSliderFloat source = (JSliderFloat )e.getSource();

			if( !source.getValueIsAdjusting() )
			{
				float sliderValue = source.getFloatValue();
				msg.debugMsg( "SliderListener.stateChanged(ChangeEvent e)", new String( "Slider Value = " + sliderValue ) );
				offsetScaleFactor = sliderValue;
				zoomTextField.setText( nf.format( sliderValue ) );
			}
		}
	}

	

// ============================================================================================================================
// ============================================================================================================================
// Menu Setup Stuff
// ============================================================================================================================
// ============================================================================================================================

	// ============================================================================================================================
	// Internal Class - PanelMenuBar
	// ============================================================================================================================
	
	public JMenuBar getMyMenuBar( )
	{
		JMenuBar aMenuBar = new JMenuBar();
		for( int ii=0; ii<myMenuList.length; ii++)
			if( myMenuList[ii] != null )
				aMenuBar.add( myMenuList[ii] );
			
		return aMenuBar;
	}

	static final int numMenuItems = 4;
	
	JMenu[] createMenuList()
	{
	
		String MethodName = "ActuatorPanel.createMenuList()";
		
		JMenu theList[] = new JMenu[ numMenuItems ];

		// Config Menu
		JMenu configMenu = new JMenu( "Config");
		{			

			final JCheckBoxMenuItem tiltDisplayOnOffMenu = new JCheckBoxMenuItem( "Tilt Display OFF" );
			{
				tiltDisplayOnOffMenu.setFont( new Font( null, Font.PLAIN, 10));
				tiltDisplayOnOffMenu.setState( true );
				tiltDisplayOnOffMenu.addActionListener( new ActionListener()
					{
						public void actionPerformed( ActionEvent evt )
						{
							tiltDisplayType = (tiltDisplayOnOffMenu.isSelected())? DISPLAY_NO_TILT  : DISPLAY_TILT ;
						}
					});
				msg.debugMsg( MethodName, "Tilt Display On/OFF  Menu Item  completed" );
			}
					
			{
				configMenu.setFont( new Font( null, Font.BOLD, 10));
				configMenu.add( tiltDisplayOnOffMenu );
				msg.debugMsg( MethodName, "Configuration Menu completed" );
			}
		}
		
		// View Menu
		JMenu viewMenu = new JMenu( "Disp. View");
		{			
			// Create a menu to set the display center  Types
			MenuRadioButtonGroup centerTypeMenu = null;
			{
				String [] typeOffsetButtons = { "Cross", "Dot", "Circle" };
				centerTypeMenu = new MenuRadioButtonGroup( typeOffsetButtons, " Center Display Type", 0, this );
				centerTypeMenu.addHandler( 0, new ButtonHookInterface() { public void myHook() { centerType = CENTER_CROSS; }});
				centerTypeMenu.addHandler( 1, new ButtonHookInterface() { public void myHook() { centerType = CENTER_DOT; }});
				centerTypeMenu.addHandler( 2, new ButtonHookInterface() { public void myHook() { centerType = CENTER_CIRCLE; }});
				centerTypeMenu.pushButton( 0 );
				msg.debugMsg( MethodName, "Offset Display Selection type Menu Item  completed" );
			}
					
			{
				viewMenu.setFont( new Font( null, Font.BOLD, 10));
				viewMenu.add( centerTypeMenu );
				msg.debugMsg( MethodName, "View Menu completed" );
			}
		}
		
		// Config Menu
		JMenu saveMenu = new JMenu( "Save");
		{	
/*		
			final JMenuItem saveCentValuesMenu = new JMenuItem( "Save Cent Values" );
			{
				saveCentValuesMenu.setFont( new Font( null, Font.PLAIN, 10));
				saveCentValuesMenu.addActionListener( new ActionListener()
					{
						public void	actionPerformed( ActionEvent evt )
						{
							File savDir = new File( "./JavaSave" );
							if( !( savDir.exists()) )
								savDir.mkdir();

							JPanel ssPanel = new SaveSnapShotData( thisPanel,  "Centroid Values", 
												new File("./JavaSave", "CentValues" +  + UtilDateFormat.getNumericDateTime()) );
							JFrame centSaveFrame = new JFrame( "Save Centroid Data");
							centSaveFrame.getContentPane().add( ssPanel);
							centSaveFrame.setSize( ssPanel.getSize() );
							centSaveFrame.pack();
							centSaveFrame.setVisible( true );
						}
					});
				msg.debugMsg( MethodName, "Save CentData Values completed" );
			}


			final JMenuItem saveRmsValuesMenu = new JMenuItem( "Save RMS Values" );
			{

				saveRmsValuesMenu.setFont( new Font( null, Font.PLAIN, 10));
				saveRmsValuesMenu.addActionListener( new ActionListener()
					{
						public void	actionPerformed( ActionEvent evt )
						{
							File savDir = new File( "./JavaSave" );
							if( !( savDir.exists()) )
								savDir.mkdir();

							JPanel ssPanel = new SaveSnapShotData( myRmsChart, "Centroid RMS Values", 
											           new File("./JavaSave", "RmsNoTilt" + UtilDateFormat.getNumericDateTime()) );
							JFrame rmsSaveFrame = new JFrame( "Save RMS Data");
							rmsSaveFrame.getContentPane().add( ssPanel);
							rmsSaveFrame.setSize( ssPanel.getSize() );
							rmsSaveFrame.pack();
							rmsSaveFrame.setVisible( true );
						}
					});
				msg.debugMsg( MethodName, "Save RMS Values completed" );
			}
 			
			{
				saveMenu.setFont( new Font( null, Font.BOLD, 10));
				saveMenu.add( saveCentValuesMenu );
				msg.debugMsg( MethodName, "Save Menu completed" );
			}
*/
		}
			
		// Debug Menu
		JMenu debugMenu = new JMenu( "Debug");
		{			
			final JMenuItem debugOn = new JMenuItem( "Debug On" );
			{
				debugOn.setFont( new Font( null, Font.PLAIN, 10));
				debugOn.addActionListener( new ActionListener()
					{
						public void
						actionPerformed( ActionEvent evt )
						{
							setDebugFlag( true );
						}
					});
			}

			final JMenuItem debugOff = new JMenuItem( "Debug Off" );
			{
				debugOff.setFont( new Font( null, Font.PLAIN, 10));
				debugOff.addActionListener( new ActionListener()
					{
						public void
						actionPerformed( ActionEvent evt )
						{
							setDebugFlag( false );
						}
					});
			}
			
			{
				debugMenu.setFont( new Font( null, Font.BOLD, 10));
				debugMenu.add( debugOn );
				debugMenu.add( debugOff );
				msg.debugMsg( MethodName, "Save Menu completed" );
			}
		}
			
		theList[ 0 ] = configMenu;
		theList[ 1 ] = viewMenu;
		theList[ 2 ] = saveMenu;
		theList[ 3 ] = debugMenu;

		return theList;
	}
	
	
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
