import java.io.*;

import java.awt.*;
import java.awt.event.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import java.util.*;

import javax.swing.*;
import javax.swing.JDesktopPane;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.filechooser.*;
import javax.swing.plaf.basic.BasicFileChooserUI;

class SystemPanel extends JFrame implements MsgReqInterface, ControllerInterface, UpdateHookInterface
{
	public static UpdaterInterface theUpdater = null;

	JTextField serverMsgTextField = null;

	protected int	panelType = AoGuiStructure.PANEL_INVALID;

	static private final Dimension smallPanelSize = new Dimension( 600, 150 );
	static private final Dimension scrollPaneSize = new Dimension( 600, 450 );
	static private final Dimension bigPanelSize = new Dimension( 650, 620 );
	static private final Dimension thisFrameSize = new Dimension( 780, 650 );
	
	protected boolean aoSystemConnected = false;
	
	AoGuiStructure aoGuiSetup = null;
	
	JPanel offsetPanel = null;
	
	private static int MAX_LOOP_PANEL = 4;
	
	protected JDesktopPane jDesktopPane1;
	protected SystemMenuBar myMenuBar;

	private static final String thisClassName = "SystemPanel";

	JPanel selectDisplayPanel = null;
	
	JPanel bigPanel = null;
	
	JTextArea textArea = null;

	protected static MsgHandler msg = null;

	protected static boolean debug = true;
	
	private AOPanelInterface cp1_Frame = null;
	private AOPanelInterface ip1_Frame = null;
	private AOPanelInterface rp1_Frame = null;
	private AOPanelInterface ap1_Frame = null;
	private AOPanelInterface ap2_Frame = null;

	// =====================================================================
	// MsgReqInterface Implementation (Part of)
	// =====================================================================
		public boolean getDebugState() { return debug; }
		public String getRequestorTitle() { return thisClassName; }
		public void setDebugState( boolean state ) { debug = state; }

	// =====================================================================
	// UpdateHookInterface
	// =====================================================================

		public void myUpdateHook( float[] f_data, int type )
		{
			System.out.println( "Dummy 'myUpdateHook( f[])'");
		}

		public void myUpdateHook( float[][] f_data, int[] types )
		{
			System.out.println( "Dummy 'myUpdateHook( f[])'");
		}

		public void myUpdateHook( float[][][] ff_data, int[] types )
		{
			System.out.println( "Dummy 'myUpdateHook( f[])'");
		}

		public void myUpdateHook( String s_data ) 
		{
			// Do copy and allow s_data to go away!
			String inputLine = new String( s_data );

			msg.debugMsg( "myUpdateHook - Requesting update processing" );
			
			int dataType = -1;
			int numElem = -1;
			String[] strValues = null;
			
			dataType = (int)( inputLine.charAt(3) - '0');

			switch( dataType )
			{
				case 0:
					break;

				default:
					return;	// Wrong type; do nothing.
			}

			String formatted = "<" + inputLine.substring( 4, inputLine.length()-3 ) + ">";

			if( inputLine.length() > 4 )
			{
				System.out.println( formatted );
				textArea.append( formatted + '\n' );
			}
			else
			{
				System.out.println( inputLine );
			}

			int textLen = textArea.getText().length();
			textArea.setCaretPosition( textLen );

		}
		
		public int getPanelType() { return panelType; }

	// =====================================================================
	// Message Presentation
	// =====================================================================
		public boolean getDebugFlag( ) { return debug; }
		public static void setDebugFlag( boolean flag ) { debug = flag; }


	public	SystemPanel( )
	{
		this.setTitle( "System Window" );
		
		msg = new MsgHandler( this );
		
		msg.debugMsg( " Get AoGuiStructure" );
		aoGuiSetup = AoGuiStructure.getAoGuiStructure();

		msg.debugMsg( " Get UpdateTestbedPanel() Started" );
		switch( AoGuiInterface.guiType )
		{
			case TYPE_TESTBED:
				theUpdater = new UpdateTestBedPanels( );
				break;
			case TYPE_AODLL:
			default:
				throw new IllegalArgumentException( "Updater Type is incorrect" );
		}
		
		//==============================================================
		// INITIALIZE THE PANEL 
		//
		msg.debugMsg( " <SYS PANEL> : initPanel" );

		initPanel();
		
   		theUpdater.registerListener( this, AoGuiStructure.DATA_MSG );

		msg.infoMsg( "UI Panel Setup Done" );
		
		setDebugFlag( false );
		
		theUpdater.startServerConnection();
		return;
	}

	public static void main( String [] arg )
	{
		try
		{
			UIManager.LookAndFeelInfo [] look = new UIManager.LookAndFeelInfo[ 10 ];
			look = UIManager.getInstalledLookAndFeels();

			for( int ii= 0; ii<2; ii++ )
				msg.infoMsg( "<SystemPanel.main> : ", look[ ii ].toString() );

			MsgHandler.genericInfoMsg( "<SystemPanel.main> : ", new String( UIManager.getSystemLookAndFeelClassName() ) );
			UIManager.setLookAndFeel( "javax.swing.plaf.metal.MetalLookAndFeel" );
		}
		catch(Exception e)
		{ MsgHandler.genericInfoMsg( "<SystemPanel.main> : ", "Exception thrown my UIManager.setLookAndFeel(String className)" ); }
		
		SystemPanel sp = new SystemPanel( );
	}		
 
	private boolean initPanel()
	{
		msg.debugMsg(" SystemPanel.initPanel() - Start ");

		panelType = AoGuiStructure.PANEL_SYS;

		//
		//=============================================================================================
		// Setup the MENU bar
		//
		myMenuBar = new SystemMenuBar( this );

		this.setJMenuBar( myMenuBar );

		msg.debugMsg(" SystemPanel.initPanel() - Get 'bigPanel' ");
		bigPanel = new JPanel();
		bigPanel.setSize( bigPanelSize );
		bigPanel.setLayout( new BoxLayout( bigPanel, BoxLayout.PAGE_AXIS ));

		JPanel smallPanel = new JPanel();
		smallPanel.setSize( smallPanelSize );
		smallPanel.setBorder( new LineBorder( Color.orange, 2 ) );
		smallPanel.setLayout( new FlowLayout( FlowLayout.CENTER ));

		msg.debugMsg(" SystemPanel.initPanel() - Get 'selectDisplayPanel' ");
		selectDisplayPanel = new JPanel();
		{
			selectDisplayPanel.setPreferredSize( new Dimension( 320, 50 ) );
			selectDisplayPanel.setSize( 300, 50 );
			
			selectDisplayPanel.setBorder
			( 
				BorderFactory.createTitledBorder ( new LineBorder( Color.blue, 1 ), "Displays ",
							 TitledBorder.LEFT, TitledBorder.CENTER, new Font( null, Font.BOLD, 9) ) 
			);

			final JCheckBox wfsWindow = new JCheckBox("wfs");
			wfsWindow.addActionListener( new SysPanelListener( ip1_Frame, wfsWindow ) );

			final JCheckBox centWindow = new JCheckBox("cent");
			centWindow.addActionListener( new SysPanelListener( cp1_Frame, centWindow ) );

			final JCheckBox act_1_Window = new JCheckBox("act_1");
			act_1_Window.addActionListener( new SysPanelListener( ap1_Frame, act_1_Window ) );

			final JCheckBox act_2_Window = new JCheckBox("act_2");
			act_2_Window.addActionListener( new SysPanelListener( ap2_Frame, act_2_Window ) );


			selectDisplayPanel.add( wfsWindow );
			selectDisplayPanel.add( centWindow );
			selectDisplayPanel.add( act_1_Window );
			selectDisplayPanel.add( act_2_Window );
			
			selectDisplayPanel.setVisible( true );
		}
		
		msg.debugMsg(" SystemPanel.initPanel() - Add Panel Selection Items to 'bigPanel' ");
		smallPanel.add( selectDisplayPanel );
		
		msg.debugMsg( " Found number of DM =  " + aoGuiSetup.getNumHostDms() );
		
		msg.debugMsg(" SystemPanel.initPanel() - Get 'dm1ModePanel' ");
		JPanel dm1ModePanel = new JPanel();
		// Setup the  "Loop Control" Buttons
		{
			dm1ModePanel.setPreferredSize( new Dimension( 260, 60 ) );
			dm1ModePanel.setSize( new Dimension( 260, 60 ) );
			
			String [] buttons = { "Closed", "Hold", "Flat/Idle" };
			RadioButtonGroup dm1Select = new RadioButtonGroup( buttons, "Tweeter Mode Select", 2, this );
			dm1Select.addHandler( 0, new ButtonHookInterface(){ public void myHook() { dm1ModeClosed( );} });
			dm1Select.addHandler( 1, new ButtonHookInterface(){ public void myHook() { dm1ModeHold( );} });
			dm1Select.addHandler( 2, new ButtonHookInterface(){ public void myHook() { dm1ModeIdle( );} });
			dm1Select.pushButton( 2 );
			dm1ModePanel.add( dm1Select );
			dm1ModePanel.setVisible(true);
		}
		
		JPanel dm2ModePanel = null;
		if( aoGuiSetup.getNumHostDms() > 1 )
		{
			msg.debugMsg(" SystemPanel.initPanel() - Get 'dm2ModePanel' ");
			dm2ModePanel = new JPanel();
			// Setup the  "Loop Control" Buttons
			{
				dm2ModePanel.setPreferredSize( new Dimension( 260, 60 ) );
				dm2ModePanel.setSize( new Dimension( 260, 60 ) );
			
				String [] buttons = { "Closed", "Hold", "Flat/Idle" };
				RadioButtonGroup dm2Select = new RadioButtonGroup( buttons, "Woofer Mode Select", 2, dm2ModePanel );
				dm2Select.addHandler( 0, new ButtonHookInterface(){ public void myHook() { dm2ModeClosed( );} });
				dm2Select.addHandler( 1, new ButtonHookInterface(){ public void myHook() { dm2ModeHold( );} });
				dm2Select.addHandler( 2, new ButtonHookInterface(){ public void myHook() { dm2ModeIdle( );} });
				dm2Select.pushButton( 2 );
				dm2ModePanel.add( dm2Select );
				dm2ModePanel.setVisible(true);
			}
		}

		smallPanel.add( getServerMsgField() );
		smallPanel.add( dm1ModePanel );

		if( aoGuiSetup.getNumHostDms() > 1 )
			smallPanel.add( dm2ModePanel );
		
		msg.debugMsg(" SystemPanel.initPanel() - Set 'text window' ");
		// Setup the  Text screen
		JScrollPane scrollPane = null;
		{
			textArea = new JTextArea( 4, 140 );
			textArea.setLineWrap( true );
			textArea.setEditable( false);
			textArea.setWrapStyleWord( true);
			textArea.setBorder( new LineBorder( Color.red, 2 ) );

			scrollPane = new JScrollPane( textArea );
			scrollPane.setSize( scrollPaneSize );
			textArea.setVisible( true );
			scrollPane.setVisible( true );
		}

		smallPanel.setVisible( true );

		bigPanel.add( smallPanel );
		bigPanel.add( scrollPane );
		bigPanel.setVisible(true);

		this.getContentPane().add( bigPanel );
		
		msg.debugMsg(" SystemPanel.initPanel() - Add 'bigPanel'  to SystemPanel ");

		this.addWindowListener( new WindowAdapter()	{ public void windowClosing( WindowEvent evt ) { myExit(); } });

  		this.setSize( thisFrameSize );
	
		this.pack();
		this.setVisible(true);

		return true;
	}

	static public String getGuiConfigBase()
	{
		String	baseName = null;
		try
		{
			if( (baseName = System.getenv( "GPI_GUI_CONFIG")) == null )
			{
				msg.errorMsg( "Unable to read 'reference data' ::\n Env var: 'GPI_GUI_CONFIG' not set" );
				return null;
			}
		}
		catch( Exception e)
			{ System.out.println( "Exception trying to get 'GPI_GUI_CONFIG' environemnt variable:: \n " + e ); }			
		
		return baseName;
	}

	static public float[] getRefGrid( )
	{
		float[][][] tmpAoRef = null;
		float[]		aoRef = null;

		String refFullName = getGuiConfigBase();
		
		refFullName += "/" + ControllerInterface.DEFAULT_FNAME_REF;
		
		FitsReader fitsReader = null;
		try
		{
			tmpAoRef = null;
			System.out.println( "\nAttempt to open Ref File: " + refFullName );
			fitsReader = new FitsReader( refFullName );
		
			tmpAoRef = (float[][][])(fitsReader.getDataObject( 0 ));

			System.out.println("\nNumDim (dimSize) = " + fitsReader.numDim);
			for( int i=0; i<fitsReader.numDim; i++)
				System.out.println(	"\n\tdim #" + i + ": " + fitsReader.dimSize[i]  );

			System.out.println("\nNum Headers (dimSize) = " + fitsReader.numHDU);

			if( fitsReader.numDim != 3 )
			{
				System.out.println("\nERR - format is unexpectedly changed -- expected size 3, got size " + fitsReader.numDim );
				return null;
			}
		}
		catch( Exception e )
		{
			System.out.println( "Unable to get AOData for ref info - exception : " + e);
			tmpAoRef = null;
		}

		if( tmpAoRef == null )
			msg.errorMsg( " Did Not get Reference Data from:"  + refFullName );
	
		aoRef = new float[  fitsReader.dimSize[1] * fitsReader.dimSize[2] * 2 ];

		int tmpOff = fitsReader.dimSize[1] * fitsReader.dimSize[2];
		for( int j=0; j<fitsReader.dimSize[1]; j++ )
			for( int i=0; i<fitsReader.dimSize[2]; i++ )
			{
				aoRef[ i + j * fitsReader.dimSize[2] ] = tmpAoRef[0][j][i]; 	
				aoRef[ i + j * fitsReader.dimSize[2] + tmpOff ] = tmpAoRef[1][j][i]; 	
			}

		return aoRef;
	}
	
	static public float[] getRefAperture( )
	{
		float[][][] 	tmpData = null;
		float[]			aoAperture = null;

		FitsReader		fitsReader = null;
		String			refFullName = getGuiConfigBase();
		
		refFullName += "/" + ControllerInterface.DEFAULT_FNAME_REF;
		
		try
		{
			tmpData = null;
			System.out.println( "\nAttempt to open Ref File: " + refFullName );
			fitsReader = new FitsReader( refFullName );
		
			tmpData = (float[][][])(fitsReader.getDataObject( 0 ));

			System.out.println("\nNumDim (dimSize) = " + fitsReader.numDim);
			for( int i=0; i<fitsReader.numDim; i++)
				System.out.println(	"\n\tdim #" + i + ": " + fitsReader.dimSize[i]  );

			System.out.println("\nNum Headers (dimSize) = " + fitsReader.numHDU);

			if( fitsReader.numDim != 3 )
			{
				System.out.println("\nERR - format is unexpectedly changed -- expected size 3, got size " + fitsReader.numDim );
				return null;
			}
		}
		catch( Exception e )
		{
			System.out.println( "Unable to get AOData for ref info - exception : " + e);
			tmpData = null;
		}

		if( tmpData == null )
			msg.errorMsg( " Did not get Ref Data from:"  + refFullName );
	
		aoAperture = new float[  fitsReader.dimSize[1] * fitsReader.dimSize[2] ];

		for( int j=0; j<fitsReader.dimSize[1]; j++ )
			for( int i=0; i<fitsReader.dimSize[2]; i++ )
				aoAperture[ i + j * fitsReader.dimSize[2] ] = tmpData[2][j][i]; 	

		return aoAperture;
	}

	private void sendServerCommand( java.awt.event.ActionEvent evt )
		{ theUpdater.writeServerCommand( serverMsgTextField.getText() );  }
	

	void dm1ModeClosed( )
	{ 
		System.out.println( " Set System Dm1 to Closed" ); 
		theUpdater.writeServerCommand( "closeOpenLoops 1 -1 -1" ); 
	}

	void dm1ModeHold( )
	{ 
		System.out.println( " Set System Dm1 to Hold" ); 
		theUpdater.writeServerCommand( "closeOpenLoops 0 -1 -1" ); 
	}

	void dm1ModeIdle( )
	{ 
		System.out.println( " Set System Dm1 to Idle" );
		theUpdater.writeServerCommand( "closeOpenLoops 0 -1 -1" );
		theUpdater.writeServerCommand( "setShapes 0x1" ); 
	}
	
	void dm2ModeClosed( )
	{ 
		System.out.println( " Set System Dm2 to Closed" ); 
		theUpdater.writeServerCommand( "closeOpenLoops -1 1 -1" ); 
	}

	void dm2ModeHold( )
	{ 
		System.out.println( " Set System Dm2 to Hold" ); 
		theUpdater.writeServerCommand( "closeOpenLoops -1 1 -1" );
   	}

	void dm2ModeIdle( )
	{ 
		System.out.println( " Set System Dm2 to Idle" ); 
		theUpdater.writeServerCommand( "closeOpenLoops -1 0 -1" ); 
		theUpdater.writeServerCommand( "setShapes 0x10" ); 
	}
	
	public void myExit()
		{ dm1ModeIdle(); dm2ModeIdle(); System.exit( 0 ); }


	JTextField getServerMsgField( )
	{
		serverMsgTextField = new javax.swing.JTextField();
		serverMsgTextField.setBorder( BorderFactory.createTitledBorder
											( 	new LineBorder( Color.blue ), 
												"Server Commands",
												TitledBorder.CENTER, 
												TitledBorder.CENTER, 
												new Font( null, Font.BOLD, 9) 
											) 
										);

		serverMsgTextField.setHorizontalAlignment( JSlider.LEFT );
		serverMsgTextField.setText( "" );
		serverMsgTextField.setToolTipText( "Enter Server Commands then press <CR>" );
		serverMsgTextField.setMinimumSize( new java.awt.Dimension( 250, 40 ) );
		serverMsgTextField.setPreferredSize( new java.awt.Dimension( 250, 40 ) );
			

		serverMsgTextField.addActionListener( new java.awt.event.ActionListener()
			{

				public void actionPerformed( java.awt.event.KeyEvent ke )
				{     
					System.out.println("Got 'key Event' ");
					int keyCode = ke.getKeyCode();     
					switch( keyCode ) 
					{
						case KeyEvent.VK_UP:				// handle up
							System.out.println("Got 'up' arrow");
					  		break;
					   	case KeyEvent.VK_DOWN:			// handle down
							System.out.println("Got 'down' arrow");
							break;
						case KeyEvent.VK_LEFT:			// handle left
							System.out.println("Got 'left' arrow");
						  	break;
					   	case KeyEvent.VK_RIGHT :			// handle right
							System.out.println("Got 'right' arrow");
						  	break;      
					}
			   }

				public void actionPerformed( java.awt.event.ActionEvent evt )
				{ 
					sendServerCommand( evt ); 
					serverMsgTextField.setText( "" );
				}
			});
			
		serverMsgTextField.setVisible( true );
		return serverMsgTextField;
	}

	// Provides ActionListener Service when a new panel is selected from the 'SystemPanel'
	// 	- called from "initPanel"
	class SysPanelListener implements ActionListener
	{
		private AOPanelInterface frame;
		private JCheckBox box;

		public SysPanelListener( AOPanelInterface _frame, JCheckBox _box )
		{
			super();
			frame =_frame;
			box = _box;

			box.setFont( new Font( null, Font.PLAIN, 9));
			box.setPreferredSize( new Dimension( 55, 20 ) );
		}

		public void actionPerformed( ActionEvent evt )
		{
			System.out.println( " Action Event toString : " + evt );

			if( box.isSelected( ) )
			{
				msg.debugMsg( "You 'checked' the bottun: " + box.getText() );

				if( frame != null )
				{
					frame.destroyPanel();
					frame = null;
				}

				try
				{
					if( box.getText().matches( "wfs" ) )
						frame = WfsImagePanel.getFramedPanel( aoGuiSetup );
					if( box.getText().matches( "cent" ) )
						frame = CentroidPanel.getFramedPanel( aoGuiSetup );
					if( box.getText().matches( "act_1" ) )
						frame = ActuatorPanel.getFramedPanel( aoGuiSetup, 2, 3, "dm1", "Actuator Positions DM#1 (uM)");
					if( box.getText().matches( "act_2" ) )
						frame = ActuatorPanel.getFramedPanel( aoGuiSetup, 4, 5, "dm2", "Actuator Positions DM#2 (uM)");
				}
				catch( Exception e )
				{
					System.out.println( "Unable to get Frame - Exception:\n" + e );
					e.printStackTrace();
					return;
				}

				msg.debugMsg( " Got Frame: " + box.getText() );
			}
			else
			{
				msg.debugMsg( "You 'unchecked' the bottun: " + box.getText() );

				frame.destroyPanel();
				frame = null;
					
				msg.debugMsg( " Removed frame: " + box.getText() );
			}
		}
	}

}

//******************************************************************************
// End of file
//******************************************************************************
