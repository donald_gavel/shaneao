/*
* AoGuiInterface.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.io.*;

import java.awt.*;
import java.awt.event.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import java.util.*;

public interface AoGuiInterface
{
	static final int PANEL_WFS = 0;
	static final int PANEL_REF = 1;
	static final int PANEL_CENT = 2;
	static final int PANEL_ACT_1 = 3;
	static final int PANEL_ACT_2 = 4;
	static final int PANEL_SYS = 5;
	static final int PANEL_INVALID = -1;
	static final int NUM_PANEL_TYPES = 10;

	static final int HOST_BASE_PORT = 15000;

	static final int NUM_PIXELS_PER_CENT = 2;

	static final int NUM_CENT_PER_ROW = 48;
	static final int NUM_CENT_PER_COL = 48;

  	static final int WFS_ROWS_SKIPPED = 0;
  	static final int WFS_COLS_SKIPPED = 0;

//*************************************************************************************************************
//**************** START SPECIAL SECTION
//
//*****************  for each 'DATA_' in the first section - you need an equivalent 'SERVER_DATA_' in the second section
//
	static final int DATA_MSG = 0;
	static final int DATA_WFS = 1;
	static final int DATA_CENT = 2;
	static final int DATA_INTEN = 3;
	static final int DATA_ACT_1 = 4;
	static final int DATA_TT = 5;
	static final int DATA_ACT_2 = 6;
	static final int DATA_NONE = -1;
	static final int NUM_DATA_TYPES = 7;

	static final int SERVER_DATA_MSG	= 0x00;
	static final int SERVER_DATA_WFS	= 0x01;
	static final int SERVER_DATA_CENT	= 0x02;
	static final int SERVER_DATA_INTEN	= 0x04;
	static final int SERVER_DATA_ACT_1	= 0x08;
	static final int SERVER_DATA_TT		= 0x10;
	static final int SERVER_DATA_ACT_2	= 0x20;

//*****************  END OF SPECIAL SECTION
//*************************************************************************************************************


	static String		hostAddress = "rtc.ucolick.org";
		
	static String		serverDataAccess = "aoServer@127.0.0.1:./etc/config";		// Not used at this point - but would like to be 
																					// able to access files through standard programs like unix : SCP

	static int			guiType = ControllerInterface.TYPE_TESTBED;

}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
