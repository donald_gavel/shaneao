/*
* ControllerInterface.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.lang.Object;
import java.util.*;
import java.text.*;
import java.nio.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import java.io.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


/**
 *
 * @author  sjones
 * 
 */

public interface ControllerInterface
{

	public static final int TYPE_AODLL = 0;
	public static final int TYPE_TESTBED = 1;

	public final static String DEFAULT_FNAME_REF = "WFS_CAL.fits";
	
	public final static String DEFAULT_FNAME_ACTPOS_X = "/actlocs_x.fits";
	public final static String DEFAULT_FNAME_ACTPOS_Y = "/actlocs_y.fits";
	
	public final static String DEFAULT_PNAME_DM_DATA_BASE = "/data/";
	public final static String DEFAULT_FNAME_DM_FLAT =	"/flat.aod";
	public final static String DEFAULT_FNAME_DM_IIR =	"/iir_2-Tap.aod";
	public final static String DEFAULT_FNAME_DM_SHAPE = "/shape/shape.aod";
	public final static String DEFAULT_FNAME_DM_ZERO =	"/zeroInput.aod";
	public final static String DEFAULT_BACKUP_PATH =	"/backup/";

	public final static int MODE_RUN = 0;
	public final static int MODE_HOLD = 1;
	public final static int MODE_FLAT = 2;
	public final static int MODE_STOP = 3;
	public final static int MODE_SET_DM_SHAPE = 4;
	public final static int MODE_INIT = 5;
	public final static int MODE_IDLE = 6;

	public final static int MODE_ARCH_ON = 10;
	public final static int MODE_ARCH_OFF = 11;
	public final static int MODE_ARCH_RUN_TIME = 12;


}
//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************
