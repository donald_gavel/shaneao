/*
* UpdateHookInterface.java
*
* Created on Dec 18, 2003, 9:51 AM
* 
*/

import java.lang.*;

public interface UpdateHookInterface
{
	public void myUpdateHook( float[] f_data, int type );
	public void myUpdateHook( String s_data );
	public void myUpdateHook( float[][] f_data, int[] types );
	public void myUpdateHook( float[][][] ff_data, int[] types );
	public int getPanelType();
}

//******************************************************************************************************************************
// End of file
//******************************************************************************************************************************