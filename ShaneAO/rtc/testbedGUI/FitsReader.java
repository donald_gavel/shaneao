//package xyzzy;

import nom.tam.util.*;
import nom.tam.fits.*;

import java.lang.reflect.*;

class FitsReader
{

	Fits fHandle = null;
	BasicHDU[] bHDU = null;

	public int 		numHDU = 0;
	public int		numDim = 0;
	public int[]	dimSize = null;
	public Object	theData = null;

	public BasicHDU getBasicHDU( int index )
	{
		System.out.println( " Header Count = " + numHDU );
		if( index < numHDU )
			return bHDU[ index ];
		else
			return null;
	}

	boolean isOpen()
	{ return( fHandle != null ); }

	public FitsReader( )
	{
		fHandle = null;
		bHDU = null;
		numHDU = 0;
	}

	public FitsReader( String filename )
	{
		
		if( !openFile( filename ) )
		{
			System.out.println(" FITS File Open -- failed on filename: " + filename );
			System.exit( 1 );
		}
		
		try
		{
			bHDU = new BasicHDU[20];
			for( int i=0; i<20; i++)
			{
				bHDU[i] = fHandle.readHDU();
				if( bHDU[i] == null)
					break;
				numHDU++;

				this.hdrInfo( bHDU[i] );

				this.listRecords( bHDU[i] );
			}
		  } catch (Exception e) {
			System.out.println( "Caught Exception: " + e );
		}

	}

	boolean openFile( String filename )
	{
		try
		{ 
			fHandle = new nom.tam.fits.Fits( filename ); 
		  } catch( Exception e ) {
			System.out.println( " Caught EXCEPTION trying to open file: " + filename + "\n    Exception description: " + e );	
		}

		return( fHandle != null );
	}

	public static void listRecords( BasicHDU thisHDU )
	{
		listRecords( thisHDU.getHeader() );
	}

	public static void listRecords( Header thisHeader )
	{
		System.out.println( "\n --- All Records --- " );
		Cursor iter = thisHeader.iterator();
		int cnt = 0;
		while( iter.hasNext() )
		{
			HeaderCard hc = (HeaderCard) iter.next();
			System.out.println("HeaderCard #" + ++cnt );; 
			System.out.println( "     Comment: " + hc.getComment() );
			System.out.println( "     Key: " + hc.getKey() );
			System.out.println( "     Value: " + hc.getValue() );
		}
		return;
	}

	public Object getDataObject( int whichHDU )
	{
		return getDataObject( getBasicHDU( whichHDU ) );
	}

	public Object getDataObject( BasicHDU thisHDU )
	{
		if( thisHDU == null )
		{
			System.out.println( " *** Bad header??? (input header is NULL)");
			return null;
		}

		try
		{
			dimSize = thisHDU.getAxes();
			numDim = dimSize.length;

			for( int ll=0; ll<numDim; ll++)
				System.out.println( " Axis #" + ll + " dimension:  " + dimSize[ll] ); 
		  } catch (Exception e) {
			System.out.println( "Unable to get the Axis values (dimensions) - Exception: " + e );
		}

		System.out.println ("----- Attempt to get data ----");

		Object testObject = null;
		try
		{
			testObject =  thisHDU.getData().getData();
		  } catch (Exception e ) {
			System.out.println( "Unable to get the DATA - Exception: " + e );
		}

		// Hard coded in this case as we know there are 3 axis
		int i,j,k;
		
		switch( numDim )
		{
			case 1:	
				{
					int dumpLen =  ((dimSize[0] < 20) ? dimSize[0] : 20);
					System.out.print ( "\n Current values [0:" + dumpLen + "]:");
					for( i=0; i<dumpLen; i++ )
						System.out.print( "  " + ((float[])testObject)[i] );
				}
				System.out.println("");
				break;
			case 2:
				{
					int dumpLen0=  ((dimSize[0] < 20) ? dimSize[0] : 20);
					int dumpLen1=  ((dimSize[1] < 20) ? dimSize[1] : 20);
					for( j=0; j<dumpLen0; j++ )
					{
						System.out.print("\n\n row # " + j + ": ");
						for( i=0; i<dumpLen1; i++ )
							System.out.print( "  " + ((float[][])testObject)[j][i] );
					}
				}
				System.out.println("");
				break;
			case 3:
				{
					int dumpLen0=  ((dimSize[0] < 5) ? dimSize[0] : 5);
					int dumpLen1=  ((dimSize[1] < 20) ? dimSize[1] : 20);
					int dumpLen2=  ((dimSize[2] < 20) ? dimSize[2] : 20);


					for( k=0; k<dumpLen0; k++)
					{
						System.out.println("\n\n group " + k);

						for( j=0; j<dumpLen1; j++ )
						{
							System.out.print("\n\n row # " + j + ": ");

							for( i=0; i<dumpLen2; i++ )
								System.out.print( "  " + ((float[][][])testObject)[k][j][i] );
						}
					}
				}
				System.out.println("");
				break;

			default:
				System.out.println("\n Unable to handle more than 3 dimensions at this point ");
				break;
		}
		return testObject;
	}

	public static void hdrInfo( BasicHDU thisHDU )
	{
		System.out.println( "\n HEADER INFO" );
		System.out.println( "      Size in bytes = " + thisHDU.getSize());
		System.out.println( "      Author = " + thisHDU.getAuthor() );
	
		// This prints out Header type, data array sizes and data type
		thisHDU.info();

		Header h = thisHDU.getHeader();

		try
		{
			System.out.println( " getBUnit returns string: " + thisHDU.getBUnit() );
			System.out.println( " getBitPix returns string: " + thisHDU.getBitPix() );
		  } catch( Exception e ) {
			System.out.println( "  --- BUnit or BitPix information not available \n Exception info: " + e );
		}

		try 
		{
			int[] myAxis = thisHDU.getAxes();
			for( int an=0; an< myAxis.length; an++)
				System.out.println( " Axis[" + an + "] : " + myAxis[an] );
		  } catch( Exception e ) {
			System.out.println( "  --- No axis information available\n Exception info: " + e );
		}
	}

	public static void main( String args[] )
	{
		if( args.length <= 0 )
		{
			System.out.println( "\n No file was specified - cowardly going to exit!");
			System.exit(1);
		}

		for( int ii=0; ii<args.length; ii++ )
			System.out.println( "\t\tArgument #" + ii + ": " + args[ii] );

		System.out.println( " ----- \n");

		FitsReader fitsReader = new FitsReader( args[0] );

		System.out.println( " ***** Attempt to get Data ***** ");
		fitsReader.getDataObject( 0 );
		System.out.println( " ***** Done attempting to get Data *****" );
		
		System.out.println( "Exit - no error reported");
	}

}

// ****************************************************************************************************************************
//
// ****************************************************************************************************************************
//
