/*
* AoPanel.java
*
* Created on June 18, 2002, 9:51 AM
* 
*/

//package MainFrame;
import java.awt.*;
import java.awt.image.*;
import java.awt.event.*;
import java.awt.geom.*;

import java.util.*;
import java.text.*;

import java.io.*;

import java.lang.IllegalArgumentException;
import java.lang.Exception;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*;

/**
 *
 * @author  sjones
 * 
 */

public abstract class AoPanel extends JPanel implements AOPanelInterface
{
	protected boolean				debug = false;
	
	private String					thisClassName = "AOPanel(Abstract)";
	
	protected MsgHandler			msg = null;

	protected JFrame				thisFrame = null;
	protected AoPanel				thisPanel = null;
	
	protected int					inputCount; 
	
	protected String				panelTitle = "AoPanel(abstract)";

	protected int					panelType = AoGuiStructure.PANEL_INVALID;
	
	//
	// Typically updateInProgress must be false and tmpStopUpdate must be false for panel to actually do an update
	//
	protected boolean updateInProgress;		// Flag for updates under normal circumstances
	protected boolean tmpStopUpdate;		// Flag for individual panels to either display or perform local update 
	
	AoPanel( String myClassName )
	{
		thisClassName = myClassName;
		
		msg = new MsgHandler( this );
		
		updateInProgress = false;				// Flag for updates under normal circumstances
		tmpStopUpdate = false;					// Local hold for special update or display in single panel
	}
	
	// ============================================================================================================================
	// AOPanelInterface Implementation
	// ============================================================================================================================

		public MsgHandler getMsgHandler() { return this.msg; }
		public String getPanelTitle() { return this.panelTitle; }
		public AoPanel getPanel() { return (AoPanel)this; }
		public void setPanelTitle( String _title) { this.panelTitle = _title; }
		public void setEnclosingFrame( JFrame theFrame) { this.thisFrame = theFrame; }
		public int getUpdCount() { return this.inputCount; }
		public boolean getDebugFlag( ) { return debug; }
		public void setDebugFlag( boolean _flag ) { debug = _flag; }
		public int getPanelType() { return panelType; }
		
		// May return NULL if this panel is not framed!	
		public JFrame getEnclosingFrame() { return this.thisFrame; }
		
		public String getBackupPath() 
		{ 
			File bkupPath = new File( ControllerInterface.DEFAULT_BACKUP_PATH );
			if( !(bkupPath.exists()) )
			bkupPath.mkdir();
			return( ControllerInterface.DEFAULT_BACKUP_PATH ); 
		}
	
	
	// =====================================================================
	// MsgReqInterface Implementation (Part of)
	// =====================================================================
		public boolean getDebugState() { return this.debug; }
		public String getRequestorTitle() { return this.thisClassName; }
		public void setDebugState( boolean state ) { this.debug = state; }

}

//******************************************************************************
// End of file
//******************************************************************************
