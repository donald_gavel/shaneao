/*
 * UpdateTestBedPanels.java
 *
 * Created on Aug 8, 2008
 * 
 */

//package MainFrame;

import java.lang.*;
import java.util.*;
import java.text.*;
import java.nio.*;
import java.io.*;
import java.net.*;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author  sjones
 * 
 */
public class UpdateTestBedPanels implements UpdaterInterface
{

	private static final String		thisClassName = "UpdateTestBedPanels";
	private ServerConnection		myServer = null;
	protected static boolean		debug = false;
	protected MsgHandler			msg = null;


	private UpdateClient[]			clientList;						// ClientList By Panel Type
	private Vector<UpdateClient>[]	registeredListenerByType;

	private int					updateRate = 1;
	
	// ============================================================================================================================
	// MsgReqInterface Implementation (Part of)
	// ============================================================================================================================
		public boolean getDebugState() { return debug; }
		public String getRequestorTitle() { return thisClassName; }
		public void setDebugState( boolean state) { debug = state; }

	
// ********************************************************************************************************************
// ********************************************************************************************************************
//
// **************          Start of Internal Class: UpdateClient
//
	// This class provides the requirements for the diplay panels.
	// The class members include what types of telemetry data are used in panel update
	//		and an array of the current data to be sent to the panel.  The data structure is provided to allow all
	//		data componenents to be collected once and send to all appropriate listeners.
	//
	private class UpdateClient
	{
		private String					thisClassName = "UpdateClient";
		
		private UpdateHookInterface		who;				// This is used for the callback of data
		private int[]					typeList = null;

		//Constructor
		public UpdateClient( UpdateHookInterface _who, int[] _types ) throws NullPointerException
		{
			if( _who == null )
				throw new NullPointerException( "UpdateHookInterface parent is NULL" );
				
			who = _who;
			typeList = _types;
			
			if( typeList == null )
				throw new NullPointerException( );			// TODO - Give better Exception 		
			
			// Increment all the element types that will be sent
			// - This information is kept so that we can turn off the update at the server if data is no longer needed
			//
			for( int typeIdx=0; typeIdx<typeList.length; typeIdx++ )
			{
				if( this == null )
					msg.errorMsg( "( Update client) 'this' == null" );
					
				registeredListenerByType[ typeList[typeIdx] ].add( (UpdateClient)this );
			}
		}

		void setData( String iStr )
		{
			int tmpLen = 0;

			tmpLen = ( iStr.length() < 20 ) ? iStr.length() : 20;
			msg.debugMsg( "(UpdateClient:setData()): Attempt to call HOOK for type: " + iStr.substring( 0, tmpLen) );
			
			// Send Data
			msg.debugMsg( " Try to do an update! " );
			who.myUpdateHook( iStr );		
		}
					
		void clean()
		{
			if( typeList == null )
				return;
				
			for( int typeIdx=0; typeIdx<typeList.length; typeIdx++ )
				registeredListenerByType[ typeList[typeIdx] ].remove( this );
		}
	}	// End of class

	// *************		End of Internal Class : UpdateClient
	// *********************************************************************



// *****************************************************************************
// **************		Start of Internal Class : ServerConnection

	private class ServerConnection extends Thread
	{
		private boolean done;
		private String inputLine = null;
		private BufferedReader inBufRdr = null;
		private PrintWriter outPrtWrt = null;
		
		private String thisClassName = "ServerConnection"; 

		private Socket socket = null;
		
		ServerConnection( int _listenerPort ) throws Exception
		{
			msg.debugMsg( thisClassName + ".contructor()", "Connect to Server from client on Port" + _listenerPort  );

			try
			{
				msg.genericInfoMsg( thisClassName + ".contructor()", "Attempting to connect to host: " + AoGuiStructure.getHostAddr() );
				socket = new Socket( AoGuiStructure.getHostAddr(), _listenerPort ); 
				System.out.println( "Socket = " + socket );
			  } catch( Exception e ) {
				throw e;
			}
			
			msg.debugMsg( " Got a socket open - port: " +  _listenerPort );


			try
			{
				done = false;
				inBufRdr = new BufferedReader(	new InputStreamReader( socket.getInputStream()));
				outPrtWrt = new PrintWriter(socket.getOutputStream(), true);
				inputLine = null;
			  } catch( Exception e) {
				msg.errorMsg("Exception occurred" + e);
				e.printStackTrace();
			}

			msg.debugMsg( "Waiting for Server input");
			System.out.println( "Waiting for Server input");
					
			this.start();
		}


		public void kill()
		{
			try
			{
				msg.debugMsg(" Got 'KILL' - Closing Buffered Reader and Input Socket\n");
				
				if( outPrtWrt != null)
				{
					outPrtWrt.close();
					outPrtWrt = null;
				}

				if( inBufRdr != null )
				{
					inBufRdr.close();
					inBufRdr = null;
				}

				done = true;
				if( socket != null)
				{
					socket.close();
					socket = null;
				}
			  } catch( Exception e) {
				msg.debugMsg("Exception occurred");
				e.printStackTrace();
			}
			return;
		}
		
		public void writeStr( String str)
		{
			if( (socket == null) || (outPrtWrt == null) )
			{
				msg.errorMsg( " Please connect to the myServer before sending the telem value" );
				return;
			}
			msg.debugMsg( "Sending to Server: <" + str + ">" );
			outPrtWrt.print( str + "\000" );
			outPrtWrt.flush();
			return;
		}
		
		public void run()
		{
			int	tmpLen = 0;

			while (!done)
			{
				try 
				{
					// Short circuit the done flag
					if( inBufRdr == null )
						break;

					inputLine = inBufRdr.readLine();
					
					msg.debugMsg( "Read a line from the Server ");
					if( (inputLine == null) )
					{
						msg.errorMsg( " Server Socket Closed unexpectedly - Should be closed with 'BYE' \n Closing Buffered Reader and Input Socket\n");
						this.kill();
						break;
					}

					if( inputLine.equals("\n") || inputLine.equals("\r") || inputLine.equals("") )
						System.out.println("\n");

					if( inputLine.equals("BYE") )
					{
						msg.debugMsg(" Got 'BYE' - Closing Buffered Reader and Input Socket\n");
						this.kill();
						break;
					}
				  } catch (IOException ioe) {
					msg.errorMsg("<UpdateTestbedPanels:Run()>IOException occurred getting server data: " + ioe);
					ioe.printStackTrace();
					continue;
				  } catch( Exception e) {
					msg.errorMsg("<UpdateTestbedPanels:Run()> Exception occurred getting server data: " + e);
					e.printStackTrace();
				}

				try 
				{
					// Print stub of line for diagnostic purposes
					tmpLen = ( inputLine.length() < 20 ) ? inputLine.length() : 20;					
					msg.debugMsg( " InputLine: " + inputLine.substring( 0, tmpLen)  );
					
					if( !inputLine.toLowerCase().startsWith( "~s~" ) )
						continue;
				  } catch( Exception e) {
					msg.errorMsg("<UpdateTestbedPanels:Run()> Exception occurred processing data: " + e);
					e.printStackTrace();
				}
				
				try
				{
					int dataType = -1;
					
					dataType = (int)( inputLine.charAt(3) - '0');

					msg.debugMsg( "(ServerConnection[thread]:run()): Number of clients that want type " + " data: " +  registeredListenerByType[dataType].size() );
					for( int i=0; i<registeredListenerByType[dataType].size(); i++ )
					{
//						if( (dataType == 1) && (inputLine.indexOf( "UR" ) > 0) )
//								break;
						msg.debugMsg( "(ServerConnection[thread]:run()): Attempt to update client for type: " + dataType );
						((UpdateClient)(registeredListenerByType[dataType].get( i ))).setData( inputLine );
					}
				} catch( Exception e) {
					msg.debugMsg( "\n\t\t input string:: " + inputLine );
					e.printStackTrace();
					msg.errorMsg("<UpdateTestbedPanels:Run()> Exception occurred Sending data to clients: " + e);
				}
			} // while
		}	// run	
	}	// class		

	// *********************************************************************
	// *************		End of Internal Class : ServerConnection
	// *********************************************************************







//******************************************************************************
//***************		Constructor: UpdateTestBedPanels
//******************************************************************************

	// Constructor for this class
	public UpdateTestBedPanels(  )
	{
		msg = new MsgHandler( this );
		if( msg == null )
			msg.errorMsg("The msg Handler was null " );
			
		clientList = new UpdateClient[ AoGuiStructure.NUM_PANEL_TYPES ];	// Create array to hold client 'list' - index is panel type
		registeredListenerByType = new Vector[ AoGuiStructure.NUM_DATA_TYPES ];	// Create vector to hold clients by data type required
		for( int indx=0; indx< AoGuiStructure.NUM_DATA_TYPES; indx++)
			registeredListenerByType[ indx ] = new Vector<UpdateClient>();

	}

	public void startServerConnection()
	{
		int portRange = 0;
		int hostPort = AoGuiStructure.HOST_BASE_PORT;

		while( portRange++ < 10 )
		{
			try
			{ 
				myServer = new ServerConnection( hostPort ); 
				msg.infoMsg( thisClassName + "******** Started ServerConnection Thread", " ");
				return;		// Got one!
			}
			catch( Exception e )
			{ 
				hostPort++;	
				msg.infoMsg( thisClassName + " - - Attempt to connect to port # " + hostPort,  "");
			}
		}

		msg.errorMsg( thisClassName + ".contructor()", "\nUnable to connect to server -- Might try: " +
						"\n\t\t\t\t1) starting server \n\t\t\t\t2) Verify Host address \n\t\t\t\t3) Check connection (ping) " );
		System.exit( 1 );
	}	

// =============================================================================
//			Public Methods : UpdateTestbedPanels
// =============================================================================
	
	public boolean registerListener( UpdateHookInterface _who, int _type ) 
			throws NullPointerException, IllegalArgumentException
	{
		int[] data_type = new int[1];
		data_type[0] = _type;
		msg.debugMsg( " Register Listener - Type = " + _type  );
		return registerListener( _who, data_type );
	}
	
	public boolean registerListener( UpdateHookInterface _who, int[] data_type ) 
			throws NullPointerException, IllegalArgumentException
	{
		if( _who == null )
			throw new NullPointerException( "The UpdateHookInterface Parent is NULL" );
			
		if( data_type.length > MAX_NUM_TYPES )
			throw new IllegalArgumentException( "Types array length is greater than: " + MAX_NUM_TYPES ); 
			
		msg.infoMsg(  thisClassName + ".registerListener( who, int[]", "Added client" );
		clientList[ _who.getPanelType() ] = new UpdateClient( _who, data_type );
		
		requestTelem();
		
		return true;
	} 

	public void removeListener( UpdateHookInterface _who )
	{
		msg.infoMsg( thisClassName + ".removeListener(UpdateHookInterface _who)", "Attempt to clean client" );
		if( clientList[ _who.getPanelType() ] != null )
		{
			clientList[ _who.getPanelType() ].clean();
		}		
		clientList[ _who.getPanelType() ] = null;
		requestTelem();
	}


	
	//
	// Handle the update request and input ( this should be used in the System panel )
	//
	public void setDisplayUpdateRate( )
	{
		String s = JOptionPane.showInputDialog(
					(Object) "What is the new Update Rate (1..16) per sec.?", (Object) (new Integer( updateRate ) )
			);

		//If a string (s) was returned, update param
		if ((s == null) || (s.length() <= 0)) 
			return;

		try
		{ 
			int uu = Integer.parseInt( s );
			if( uu>0 && uu<=16 ) 
				updateRate = uu;
			else throw new NumberFormatException();
			
			msg.debugMsg( thisClassName + ".setDisplayUpdateRate()" , "Update Rate Menu Item setup completed" );
		}
		catch( java.lang.NumberFormatException nfe)
		{
			JOptionPane.showInputDialog( (Object) "Invalid timeout entry", (Object) null );
		}
	}
	
	public void kill()
	{
		msg.infoMsg( thisClassName + ".kill()", "Called" );
		if( myServer != null )
			myServer.kill();
	}
	
	private void requestTelem( )
	{
		int telemValue = 0x00;
		// Go through the current clientList and set all the required data types for update
		for( int idx=0; idx<AoGuiStructure.NUM_DATA_TYPES; idx++ )
		{
			if( registeredListenerByType[ idx ].size() > 0 )
				switch( idx )
				{
					case AoGuiStructure.DATA_WFS:
						msg.debugMsg( "Request WFS Data");
						telemValue += AoGuiStructure.SERVER_DATA_WFS;
						break;
					case AoGuiStructure.DATA_CENT:
						msg.debugMsg( "Request CENT Data");
						telemValue += AoGuiStructure.SERVER_DATA_CENT;
						break;
					case AoGuiStructure.DATA_INTEN:
						msg.debugMsg( "Request INTEN Data");
						telemValue += AoGuiStructure.SERVER_DATA_INTEN;
						break;
					case AoGuiStructure.DATA_ACT_1:
						msg.debugMsg( "Request ACT_1 Data");
						telemValue += AoGuiStructure.SERVER_DATA_ACT_1;
						break;
					case AoGuiStructure.DATA_ACT_2:
						msg.debugMsg( "Request ACT_2 Data");
						telemValue += AoGuiStructure.SERVER_DATA_ACT_2;
						break;
				}
		}
			
		msg.debugMsg( "'************* telem' Value is to be set to : " + telemValue );
		writeServerCommand( new String( "telem " + telemValue ) );
		
		return;
	}
	
	public boolean writeServerCommand( String str )
	{
		if( myServer == null )
			return false;
			
		myServer.writeStr( str );
		return true;
	}

}

//******************************************************************************
// End of file
//******************************************************************************
