/*******************************************************************************
 *
 *  Copyright (c) 2009 Jet Propulsion Laboratory.
 *  All rights reserved.
 *
 *  These commodities, technology or software were exported from
 *  the United States in accordance with the Export Administration Regulations.
 *  Diversion contrary to U.S. law is prohibited.
 *
 ******************************************************************************/
/**
 ************************************************************************
 *
 * @file gpCalHowfs.h
 *
 * @brief
 *  Function headers for HOWFS reconstruction
 *
 *  Not implemented in this delivery.
 *
 * @author
 *  John Angione
 *
 * @b REVISIONS:
 * $Log: gpCalHowfs.h,v $
 * Revision 1.1  2012/09/12 19:53:44  will
 * *** empty log message ***
 *
 * Revision 1.11  2010/04/22 13:56:41  jangione
 * Update HOWFS Reconstruction Algorithm.
 *
 * Revision 1.10  2010/03/18 20:25:55  jangione
 * Change ROI to fully include both pupils.
 *
 * Revision 1.9  2010/03/18 17:25:22  jangione
 * Changes MAX_BINS from 8 to 4.
 *
 * Revision 1.8  2010/03/18 05:25:49  jangione
 * Add second pupil.
 *
 * Revision 1.7  2010/02/22 17:55:43  jangione
 * Add rephase angle.
 *
 * Revision 1.6  2009/12/31 17:45:45  jangione
 * Add compile #defines.
 *
 * Revision 1.5  2009/11/04 20:57:25  jangione
 * Change dimensions from 38x38 to 44x44.
 *
 * Revision 1.4  2009/08/10 23:57:32  jangione
 * Documentation updates.
 *
 * Revision 1.3  2009/08/07 17:53:18  jangione
 * Add support for framegrabber region-of-interest readout.
 *
 * Revision 1.2  2009/08/01 20:29:56  li
 * added CVS log tag and modified copyright header
 *
 *
 ************************************************************************
 */

#ifndef GP_CAL_HOWFS_H
#define GP_CAL_HOWFS_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <sys/select.h>  // for struct timeval

#include "gpCalImage.h"
#include "gpCalError.h"

//=====================================================================
// Defines

/**
 * @brief
 * Width of the HOWFS region of interest.
 *
 * Width of the HOWFS region of interest.
 */
#define GP_CAL_HOWFS_WIDTH   174

/**
 * @brief
 * Height of the HOWFS region of interest.
 *
 * Height of the HOWFS region of interest.
 */
#define GP_CAL_HOWFS_HEIGHT  50

/**
 * @brief
 * Width of the HOWFS pupil
 *
 * Width of the HOWFS pupil
 */
#define GP_CAL_PUPIL_WIDTH   (44)

/**
 * @brief
 * Height of the HOWFS pupil
 *
 * Height of the HOWFS pupil
 */
#define GP_CAL_PUPIL_HEIGHT  (44)

/**
 * @brief
 * Width of the merge region of interest.
 *
 * Width of the merge region of interest.
 */
#define GP_CAL_MERGE_WIDTH   GP_CAL_PUPIL_WIDTH

/**
 * @brief
 * Height of the merge region of interest.
 *
 * Height of the merge region of interest.
 */
#define GP_CAL_MERGE_HEIGHT  GP_CAL_PUPIL_HEIGHT

/**
 * @brief
 * Number of pixels in the "raw frame" to the left of the region of interest.
 *
 * Number of pixels in the "raw frame" to the left of the region of interest.
 */
#define GP_CAL_HOWFS_LEFT_MARGIN   2

/**
 * @brief
 * Number of pixels in the "raw frame" to the right of the region of interest.
 *
 * Number of pixels in the "raw frame" to the right of the region of interest.
 */
#define GP_CAL_HOWFS_RIGHT_MARGIN  2

/**
 * @brief
 * Number of pixels in the "raw frame" to the top of the region of interest.
 *
 * Number of pixels in the "raw frame" to the top of the region of interest.
 */
#define GP_CAL_HOWFS_TOP_MARGIN    2

/**
 * @brief
 * Number of pixels in the "raw frame" to the bottom of the region of interest.
 *
 * Number of pixels in the "raw frame" to the bottom of the region of interest.
 */
#define GP_CAL_HOWFS_BOTTOM_MARGIN 2

/**
 * @brief
 * Total width of a HOWFS "raw frame".
 *
 * Total width of a HOWFS "raw frame".
 */
#define GP_CAL_HOWFS_RAW_WIDTH     (GP_CAL_HOWFS_WIDTH + GP_CAL_HOWFS_LEFT_MARGIN + GP_CAL_HOWFS_RIGHT_MARGIN)

/**
 * @brief
 * Total height of a HOWFS "raw frame".
 *
 * Total height of a HOWFS "raw frame".
 */
#define GP_CAL_HOWFS_RAW_HEIGHT    (GP_CAL_HOWFS_HEIGHT + GP_CAL_HOWFS_TOP_MARGIN + GP_CAL_HOWFS_BOTTOM_MARGIN)


/**
 * @brief
 * Maximum number of HOWFS bins (A, B, C, D, etc.).
 * Typically only 4 are used. However, some special scanning patterns may use
 * as many as 8.
 *
 * Maximum number of HOWFS bins (A, B, C, D, etc.).
 * Typically only 4 are used. However, some special scanning patterns may use
 * as many as 8.
 */
#define GP_CAL_HOWFS_MAX_BINS    4

//=====================================================================
// Typedefs

/**
 * @brief
 *  HOWFS Raw Image.
 *
 *  HOWFS raw image. This is used to store the raw camera data from the HOWFS
 *  plus a margin around the edges.
 */
typedef short gpCalHowfsRawImage_t[GP_CAL_HOWFS_RAW_WIDTH][GP_CAL_HOWFS_RAW_HEIGHT];

/**
 * @brief
 *  HOWFS Image.
 *
 *  HOWFS image. This is used to store the dark-subtracted, flat-fielded data
 *  from the HOWFS.
 */
typedef float gpCalHowfsImage_t[GP_CAL_HOWFS_WIDTH][GP_CAL_HOWFS_HEIGHT];

/**
 * @brief
 *  Merge Image.
 *
 *  Merge image. This is used to store the merged LOWFS and HOWFS images.
 */
typedef float gpCalMergeImage_t[GP_CAL_MERGE_WIDTH][GP_CAL_MERGE_HEIGHT];

/**
 * @brief
 *  HOWFS frame.
 *
 *  HOWFS frame. This stores a single HOWFS frame, along with ancilliary data.
 */

typedef struct gpCalHowfsFrame  {
    gpCalHowfsRawImage_t rawFrame;     /**< Raw data from the HOWFS */
    gpCalHowfsImage_t processedFrame;  /**< Dark-subtracted, flat-fielded HOWFS ROI frames */
    int frameNumber;                   /**< HOWFS frame number from the framegrabber */
    struct timeval timeTag;            /**< Time which the frame was transferred via DMA */
    float frameRate;                   /**< HOWFS frame rate */
    int step;                          /**< Current step (0..#GP_CAL_HOWFS_MAX_BINS) */
    float pztCoarsePosition;           /**< Position of the coarse phasing stage */
    float pztFinePosition;             /**< Position of the fine phasing stage */
    float pztTip;                      /**< Tip of the fine PZTs */
    float pztTilt;                     /**< Tilt of the fine PZTs */
} gpCalHowfsFrame_t;


//=====================================================================
// Global Function Prototypes

gpCalErrorCode_t gpCalHowfsReconstruct (const gpCalImage_t *howfs_bins,
                                        gpCalImage_t* amplitude_map,
                                        gpCalImage_t *phase_map,
                                        float *rephase_angle);

gpCalErrorCode_t gpCalHowfsLowfsMerge (const gpCalImage_t *lowfs_amplitude,
                                       const gpCalImage_t *lowfs_phase,
                                       const gpCalImage_t *howfs_amplitude,
                                       const gpCalImage_t *howfs_phase,
                                       gpCalImage_t *merge_amplitude,
                                       gpCalImage_t *merge_phase);

gpCalErrorCode_t gpCalHowfsGetPupil (const gpCalImage_t *raw,
                                     gpCalImage_t *pupil,
                                     int num);

gpCalErrorCode_t gpCalHowfsSetPupil (gpCalImage_t *raw,
                                     const gpCalImage_t *pupil,
                                     int num);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* GP_CAL_HOWFS_H */
