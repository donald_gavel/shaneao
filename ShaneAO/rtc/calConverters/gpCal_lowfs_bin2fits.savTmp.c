/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpCal_lowfs_bin2fits.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to convert Cal subsystem lowfs data
 *	files from binary format to fits format. 
 *
 *	@b DESCRIPTION:
 *	This component is a standalone program to convert Cal subsystem lowfs data
 *	files from binary format to fits format. 
 *	@par
 *	NOTE that this program is relatively temporary for testing purposes.  As such
 *	it uses local versions of the Cal header files.  If it ever gets integrated into
 *	the permanent Cal subsystem, it should use the deliverable Cal header files.  
 *	@par
 *
 *	@b AUTHOR:
 *	DWP	DATE: 7/21/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c lowfsBin2fits()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

// VERY IMPORTANT -- LONGLONG is defined incorrectly in gpCalTypes.h -- dwp changed a local 
//					 copy of that file for bin2fits !!
#include "gpCalLowfs.h"

#include "fitsio.h"

typedef	signed short boolean;
#define false 0
#define FALSE 0
#define true  1
#define TRUE  1

// VERY IMPORTANT -- this is gpCalLowfsFrame_t from gpCalLowfs.h, but padded by 16 bytes; the 
//					 padding appears different between the Cal and AOC, can't see why
typedef struct gpCalLowfsFrame_PAD  {
    gpCalLowfsRawImage_t rawFrame;    /**< Raw data from the LOWFS. */
    gpCalLowfsImage_t processedFrame; /**< Dark-subtracted, flat-fielded
                                       *   LOWFS ROI frames. */
    int frameNumber;                  /**< LOWFS frame number from the
                                       *   framegrabber. */
	char  padding1[4];				  // PAD
    struct timeval timeTag;           /**< Time which the frame was 
                                       *   transferred via DMA. */
    float frameRate;                  /**< LOWFS frame rate. */
	char  padding2[12];				  // PAD
} gpCalLowfsFrame_PAD_t;    


// Configuration Variables

#define MAX_IMAGES 10000  // maximum images per file


// Global Variables


// Local Variables

static boolean quit = false;

static gpCalLowfsFrame_PAD_t lowfsData[MAX_IMAGES];

static short lowfsRawImages[MAX_IMAGES][GP_CAL_LOWFS_RAW_HEIGHT][GP_CAL_LOWFS_RAW_WIDTH];  // NOTE that width and height are flipped from binary
static float lowfsImages[MAX_IMAGES][GP_CAL_LOWFS_HEIGHT][GP_CAL_LOWFS_WIDTH];			   // NOTE that width and height are flipped from binary
static int   lowfsStats[MAX_IMAGES][4];  // [3] is for frameNumber, tv_sec, and tv_usec


// Local Function Prototypes

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, void *data );
static void termHandler(int sigNum);


int main(int argc, char *argv[])
{
    FILE *fp;
	char path[128], fname[128], fullFilename[256];
	int	 i, j, k;
    int	 numLowfsData;
	long dimSize[4];  // will handle up to 4 dimensions

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	set defaults and then parse command line
	path[0] = '\0';
	strcpy(fname, "lowfs");
    --argc;
    ++argv;
    while (argc && (argv[0][0] == '-'))
    {
		switch (argv[0][1])
		{
			case 'p':		// path
			    ++argv;
			    --argc;
			    strcpy(path, argv[0]);
			    break;

			case 'f':		// file name
			    ++argv;
			    --argc;
			    strcpy(fname, argv[0]);
			    break;

			case '?':
			case 'h':
			    printf("\nDump raw wfs camera data for GPI wfs\n");
			    printf("usage:\n");
			    printf("  -p pathName   path for input and output files\n");
			    printf("  -f fileName   file name for input and output files (don't include an extension)\n");
			    printf("  -h            print this help message\n");
			    exit(0);
			    break;

			default:
			    printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			    break;
		}
		argc--;
		argv++;
    }

//	read LOWFS data
	strcpy(fullFilename, path);
	strcat(fullFilename, fname);
	strcat(fullFilename, ".data");
    if((fp = fopen(fullFilename, "r")) == NULL)
    {
        printf ("lowfs_bin2fits: error opening input file: %s\n", fullFilename);
		exit(-1);
	}
    fread(&numLowfsData, sizeof(int), 1, fp);
	if (numLowfsData > MAX_IMAGES)
	{
        printf ("lowfs_bin2fits: max number of images exceeded, only first %d being converted\n", MAX_IMAGES);
		numLowfsData = MAX_IMAGES;
	}
    fread(lowfsData, sizeof(gpCalLowfsFrame_t), numLowfsData, fp);
    fclose (fp);

//	if we're not quitting, store the lowfs raw frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_raw.fits");

		for (i = 0; i < numLowfsData; i++)
		{
			for (j = 0; j < GP_CAL_LOWFS_RAW_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_LOWFS_RAW_WIDTH; k++)
					lowfsRawImages[i][j][k] = lowfsData[i].rawFrame[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_LOWFS_RAW_WIDTH);
		dimSize[1] = (long)(GP_CAL_LOWFS_RAW_HEIGHT);
		dimSize[2] = (long)numLowfsData;
		if (writeFitsFile(fullFilename, dimSize, 3, TSHORT, lowfsRawImages) == 0)
		{
	        printf ("lowfs_bin2fits: error writing lowfs raw image file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the lowfs processed frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_image.fits");

		for (i = 0; i < numLowfsData; i++)
		{
			for (j = 0; j < GP_CAL_LOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_LOWFS_WIDTH; k++)
					lowfsImages[i][j][k] = lowfsData[i].processedFrame[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_LOWFS_WIDTH);
		dimSize[1] = (long)(GP_CAL_LOWFS_HEIGHT);
		dimSize[2] = (long)numLowfsData;
		if (writeFitsFile(fullFilename, dimSize, 3, TFLOAT, lowfsImages) == 0)
		{
	        printf ("lowfs_bin2fits: error writing lowfs image file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the lowfs frame stats
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_stats.fits");

		for (i = 0; i < numLowfsData; i++)
		{
			lowfsStats[i][0] = lowfsData[i].frameNumber;
			lowfsStats[i][1] = lowfsData[i].timeTag.tv_sec;
			lowfsStats[i][2] = lowfsData[i].timeTag.tv_usec;
			lowfsStats[i][3] = (int)lowfsData[i].frameRate;
		}

		dimSize[0] = 4L;
		dimSize[1] = (long)numLowfsData;
		if (writeFitsFile(fullFilename, dimSize, 2, TLONG, lowfsStats) == 0)
	        printf ("lowfs_bin2fits: error writing lowfs stats file\n");
	}

	printf("numFrames: %d; sizes -- struct: %d, raw: %d, image: %d, frameNum: %d, sec: %d, usec: %d, rate: %d\n", 
			numLowfsData, sizeof(gpCalLowfsFrame_t), sizeof(gpCalLowfsRawImage_t), sizeof(gpCalLowfsImage_t), 
			sizeof(lowfsData[0].frameNumber), sizeof(lowfsData[0].timeTag.tv_sec), sizeof(lowfsData[0].timeTag.tv_usec), 
			sizeof(lowfsData[0].frameRate));
	printf("rawW: %d, rawH: %d, imageW: %d, imageH: %d\n", GP_CAL_LOWFS_RAW_WIDTH, GP_CAL_LOWFS_RAW_HEIGHT, 
														   GP_CAL_LOWFS_WIDTH, GP_CAL_LOWFS_HEIGHT);
	printf("lowfs_bin2fits: file conversion done\n");
}


/**********************************************************************************************

UNIT NAME:  writeFitsFile()

UNIT DESCRIPTION:
The purpose of this unit is to write a FITS file from a possibly multi-dimensional array.  

UNIT INPUTS:
1.    filename (parameter) (name of the fits file to write)
2.    dimSize (parameter) (the dimensions of the array to be written)
3.    numDim (parameter) (the number or dimensions of the array)
4.    dataType (parameter) (data type, using cfitsio data type designators)
5.    data (parameter) (points to the array to be written)

UNIT OUTPUTS:
1.    returns 1 if okay, 0 if error
2.    a fits file named 'filename'

**********************************************************************************************/

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, void *data )
{
	char      tmpBuf[512];
	int       i;
	fitsfile  *fptr;
	char      fitsFilename[256];
	int       status = 0;
	long      dataAxes[4];  // will handle up to 4 dimensions
	long      fpixel[4];    // will handle up to 4 dimensions
	long long size;

//	make sure the filename begins with '!' so fitsio will create the file and then open the it
	strcpy( fitsFilename, "!");
	strcat( fitsFilename, filename);
	fits_create_file( &fptr, fitsFilename, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	for( i = 0; i < 4; i++)
	{
		dataAxes[i] = 0;
		fpixel[i] = 1;
	}

//	create an image, write header keywords, and write out the data
	size = 1;
	for( i = 0; i < numDim; i++)
	{
		dataAxes[i] = dimSize[i];
		size *= dimSize[i];
	}
	switch (dataType)
	{
		case TBYTE:
			fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status );
			break;
		case TDOUBLE:
			fits_create_img( fptr, DOUBLE_IMG, numDim, dataAxes, &status );
			break;
		case TLONG:
			fits_create_img( fptr, LONG_IMG, numDim, dataAxes, &status );
			break;
		case TULONG:
			fits_create_img( fptr, ULONG_IMG, numDim, dataAxes, &status );
			break;
		case TSHORT:
			fits_create_img( fptr, SHORT_IMG, numDim, dataAxes, &status );
			break;
		case TFLOAT:
			fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status );
			break;
		default:
			printf( "Error: fits file type not allowed: %d\n", dataType );
			break;
	}
	if (status != 0)
	{
		printf( "Error: fits image could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_date( fptr, &status );
	if (status != 0)
	{
		printf( "Error: fits file date could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_comment( fptr, "lowfs data", &status );
	if (status != 0)
	{
		printf( "Error: fits file comment could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_pix( fptr, dataType, fpixel, size, data, &status );
	if (status != 0)
	{
		printf( "Error: fits file data could not be writen: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

//	close the file
	fits_close_file( fptr, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be closed for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	return 1;
}


static void termHandler(int sigNum)
{
	printf("lowfs_bin2fits quitting due to OS term: %d\n", sigNum);
	quit = true;
}
