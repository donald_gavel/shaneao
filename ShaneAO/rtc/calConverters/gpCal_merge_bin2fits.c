/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpCal_merge_bin2fits.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to convert Cal subsystem merge data
 *	files from binary format to fits format. 
 *
 *	@b DESCRIPTION:
 *	This component is a standalone program to convert Cal subsystem merge data
 *	files from binary format to fits format. 
 *	@par
 *	NOTE that this program is relatively temporary for testing purposes.  As such
 *	it uses local versions of the Cal header files.  If it ever gets integrated into
 *	the permanent Cal subsystem, it should use the deliverable Cal header files.  
 *	@par
 *
 *	@b AUTHOR:
 *	AGC	DATE: 8/31/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c mergeBin2fits()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

// VERY IMPORTANT -- LONGLONG is defined incorrectly in gpCalTypes.h -- dwp changed a local 
//					 copy of that file for bin2fits !!
#include "gpCalLowfs.h"
#include "gpCalHowfs.h"
// GP_CAL_GMB_MAX_ZERNIKES is defined in gpCalGmb.h, which #includes gpCalLowfs.h and 
// gpCalHowfs.h as well as other header files outside the immediate source tree.
#define GP_CAL_GMB_MAX_ZERNIKES 30

#include "fitsio.h"

typedef	signed short boolean;
#define false 0
#define FALSE 0
#define true  1
#define TRUE  1

typedef struct gpCalGmbCalMeasurement  {
	/*
	 *  LOWFS measurements
	 */
	gpCalLowfsArray_t lowfsXCent;     /**< X centroid for each lenslet */
	gpCalLowfsArray_t lowfsYCent;     /**< Y centroid for each lenslet */
	gpCalLowfsArray_t lowfsFlux;      /**< Flux for each lenslet */
	gpCalMergeImage_t lowfsPhase;     /**< LOWFS phase, pre-merge */
	gpCalMergeImage_t lowfsAmplitude; /**< LOWFS amplitude, pre-merge */
	float lowfsTotalFlux;             /**< Total flux over all lenslets */
	int lastLowfsFrame;               /**< LOWFS frame number used in the merge. */
    
	/*
	 *  HOWFS measurements
	 */
	int howfsNumBins;                 /**< Number of HOWFS bins */
	gpCalHowfsImage_t howfsA;
	gpCalHowfsImage_t howfsB;
	gpCalHowfsImage_t howfsC;
	gpCalHowfsImage_t howfsD;
	gpCalHowfsImage_t howfsPhase;     /**< HOWFS phase, pre-merge */
	gpCalHowfsImage_t howfsAmplitude; /**< HOWFS amplitude, pre-merge */
	int lastHowfsFrame;               /**< HOWFS frame number used in the merge. */
    
	/*
	 *  Merged data
	 */
	float calRmsWavefront;          /**< RMS wavefront for each merged frame */
	float calTip;                   /**< Tip for each merged frame */
	float calTilt;                  /**< Tilt for each merged frame */
	float calFocus;                 /**< Focus for each merged frame */
	gpCalMergeImage_t calPhase;     /**< Phase for each merged frame */
	gpCalMergeImage_t calAmplitude; /**< Amplitude for each merged frame */
	int calDataNumber;              /**< Incremented every data set */
	float calUpdateRate;            /**< Rate at which measurements are made */
	float zernikes[GP_CAL_GMB_MAX_ZERNIKES];
	int num_zernikes;
	float snr;
} gpCalGmbCalMeasurement_t;

// Configuration Variables

#define MAX_IMAGES 1000  // maximum processed frames per file


// Global Variables


// Local Variables

static boolean quit = false;

static gpCalGmbCalMeasurement_t mergeData[MAX_IMAGES];

/*  !!!!! ===== NOTE that in all these arrays, Height and Width are flipped from binary. ==== !!!!!  */
// Stores lowfs X and Y centroids, and flux, in that order.
// See definition of gpCalLowfsArray_t in gpCalLowfs.h
static float lowfsArray[MAX_IMAGES][3][GP_CAL_LOWFS_LENSLET_ARRAY_SIZE][GP_CAL_LOWFS_LENSLET_ARRAY_SIZE];
// Stores lowfs phase and amplitude and cal phase and amplitude, in that order.
// See definition of gpCalMergeImage_t in gpCalHowfs.h
static float mergeImage[MAX_IMAGES][4][GP_CAL_MERGE_HEIGHT][GP_CAL_MERGE_WIDTH];
// Stores howfs A, B, C, D, phase and amplitude, in that order.
// See definition of gpCalHowfsImage_t in gpCalHowfs.h
static float howfsImage[MAX_IMAGES][6][GP_CAL_HOWFS_HEIGHT][GP_CAL_HOWFS_WIDTH];
// Stores the float and int (which are cast to float) statistics in the
// gpCalGmbCalMeasurement_t structure, in the following order:
// First the ints (cast to float): lastLowfsFrame, howfsNumBins, lastHowfsFrame,
//     calDataNumber, num_zernikes
// Then the floats: lowfsTotalFlux, calRmsWavefront, calTip, calTilt, calFocus,
//     calUpdateRate, snr, zernikes[GP_CAL_GMB_MAX_ZERNIKES] (which happens to be 30)
static float mergeStats[MAX_IMAGES][12 + GP_CAL_GMB_MAX_ZERNIKES]; 


// Local Function Prototypes

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, char *comment, void *data );
static void termHandler(int sigNum);


int main(int argc, char *argv[])
{
	FILE *fp;
	char path[128], fname[128], fullFilename[256];
	int	 i, j, k, q, fcounter;
	int	 numMergeData;
	long dimSize[4];  // will handle up to 4 dimensions

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	set defaults and then parse command line
	path[0] = '\0';
	strcpy(fname, "merge");
	--argc;
	++argv;
	while (argc && (argv[0][0] == '-'))
	{
		switch (argv[0][1])
		{
		case 'p':		// path
			++argv;
			--argc;
			strcpy(path, argv[0]);
			break;

		case 'f':		// file name
			++argv;
			--argc;
			strcpy(fname, argv[0]);
			break;

		case '?':
		case 'h':
			printf("\nCal merge data bin2fits convertor\n");
			printf("usage:\n");
			printf("  -p pathName   path for input and output files\n");
			printf("  -f fileName   file name for input and output files (don't include an extension)\n");
			printf("  -h            print this help message\n");
			exit(0);
			break;

		default:
			printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			break;
		}
		argc--;
		argv++;
	}

	// Open the binary file
	strcpy(fullFilename, path);
	strcat(fullFilename, fname);
	strcat(fullFilename, ".data");
	if((fp = fopen(fullFilename, "r")) == NULL)
	{
		printf ("merge_bin2fits: error opening input file: %s\n", fullFilename);
		exit(-1);
	}

	// Pull out the number of records contained therein
	fread(&numMergeData, sizeof(int), 1, fp);
	if (numMergeData > MAX_IMAGES)
	{
		printf ("merge_bin2fits: max number of images exceeded, only first %d being converted\n", MAX_IMAGES);
		numMergeData = MAX_IMAGES;
	}

	// Read all into memory and close the binary file
	fread(mergeData, sizeof(gpCalGmbCalMeasurement_t), numMergeData, fp);
	fclose(fp);

//	if we're not quitting, store the lowfsArray frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_lowfsCentroidsFlux.fits");

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; j++)
			{
				for (k = 0; k < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; k++)
					lowfsArray[i][0][j][k] = mergeData[i].lowfsXCent[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; j++)
			{
				for (k = 0; k < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; k++)
					lowfsArray[i][1][j][k] = mergeData[i].lowfsYCent[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; j++)
			{
				for (k = 0; k < GP_CAL_LOWFS_LENSLET_ARRAY_SIZE; k++)
					lowfsArray[i][2][j][k] = mergeData[i].lowfsFlux[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_LOWFS_LENSLET_ARRAY_SIZE);
		dimSize[1] = (long)(GP_CAL_LOWFS_LENSLET_ARRAY_SIZE);
		dimSize[2] = 3l;
		dimSize[3] = (long)numMergeData;
		if (writeFitsFile(fullFilename, dimSize, 4, TFLOAT, "lowfs X, Y centroids, flux", lowfsArray) == 0)
		{
			printf ("merge_bin2fits: error writing merged cal lowfsArray (lowfs X and Y centroids, lowfs flux) file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the mergeImage frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_phaseAmplitude.fits");

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_MERGE_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_MERGE_WIDTH; k++)
					mergeImage[i][0][j][k] = mergeData[i].lowfsPhase[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_MERGE_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_MERGE_WIDTH; k++)
					mergeImage[i][1][j][k] = mergeData[i].lowfsAmplitude[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_MERGE_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_MERGE_WIDTH; k++)
					mergeImage[i][2][j][k] = mergeData[i].calPhase[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_MERGE_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_MERGE_WIDTH; k++)
					mergeImage[i][3][j][k] = mergeData[i].calAmplitude[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_MERGE_WIDTH);
		dimSize[1] = (long)(GP_CAL_MERGE_HEIGHT);
		dimSize[2] = 4l;
		dimSize[3] = (long)numMergeData;
		if (writeFitsFile(fullFilename, dimSize, 4, TFLOAT, "lowfs and cal phase and amplitude", mergeImage) == 0)
		{
			printf ("merge_bin2fits: error writing merged cal mergeImage (lowfs and cal phase and amplitude) file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the howfsImage frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_howfsImage.fits");

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][0][j][k] = mergeData[i].howfsA[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][1][j][k] = mergeData[i].howfsB[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][2][j][k] = mergeData[i].howfsC[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][3][j][k] = mergeData[i].howfsD[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][4][j][k] = mergeData[i].howfsPhase[j][k];
			}
		}

		for (i = 0; i < numMergeData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImage[i][5][j][k] = mergeData[i].howfsAmplitude[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_HOWFS_WIDTH);
		dimSize[1] = (long)(GP_CAL_HOWFS_HEIGHT);
		dimSize[2] = 6l;
		dimSize[3] = (long)numMergeData;
		if (writeFitsFile(fullFilename, dimSize, 4, TFLOAT, "howfs A, B, C, D, phase, amplitude", howfsImage) == 0)
		{
			printf ("merge_bin2fits: error writing merged cal howfsImage (howfs A, B, C, D, phase, and amplitude) file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the merge frame stats
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_stats.fits");

		for (i = 0; i < numMergeData; i++)
		{
			mergeStats[i][0] = (float)mergeData[i].lastLowfsFrame;
			mergeStats[i][1] = (float)mergeData[i].howfsNumBins;
			mergeStats[i][2] = (float)mergeData[i].lastHowfsFrame;
			mergeStats[i][3] = (float)mergeData[i].calDataNumber;
			mergeStats[i][4] = (float)mergeData[i].num_zernikes;
			mergeStats[i][5] = mergeData[i].lowfsTotalFlux;
			mergeStats[i][6] = mergeData[i].calRmsWavefront;
			mergeStats[i][7] = mergeData[i].calTip;
			mergeStats[i][8] = mergeData[i].calTilt;
			mergeStats[i][9] = mergeData[i].calFocus;
			mergeStats[i][10] = mergeData[i].calUpdateRate;
			mergeStats[i][11] = mergeData[i].snr;
			for (q = 0; q < GP_CAL_GMB_MAX_ZERNIKES; q++)
			{
				mergeStats[i][q + 12] = mergeData[i].zernikes[q];
			}
		}

		dimSize[0] = GP_CAL_GMB_MAX_ZERNIKES + 12;
		dimSize[1] = (long)numMergeData;
		if (writeFitsFile(fullFilename, dimSize, 2, TFLOAT, "stats data", mergeStats) == 0)
			printf ("merge_bin2fits: error writing lowfs stats file\n");
	}

	printf("numFrames: %d; sizes -- merge structure %d, howfs image %d, merge image %d\n",
	       numMergeData, sizeof(gpCalGmbCalMeasurement_t), sizeof(gpCalHowfsImage_t), sizeof(gpCalMergeImage_t));
	printf("mergeW: %d, mergeH: %d, max zernikes: %d\n", GP_CAL_MERGE_WIDTH, GP_CAL_MERGE_HEIGHT, 
	       GP_CAL_GMB_MAX_ZERNIKES);
	printf("merge_bin2fits: file conversion done\n");
}



/**********************************************************************************************

UNIT NAME:  writeFitsFile()

UNIT DESCRIPTION:
The purpose of this unit is to write a FITS file from a possibly multi-dimensional array.  

UNIT INPUTS:
1.    filename (parameter) (name of the fits file to write)
2.    dimSize (parameter) (the dimensions of the array to be written)
3.    numDim (parameter) (the number or dimensions of the array)
4.    dataType (parameter) (data type, using cfitsio data type designators)
5.    comment (parameter) (short comment describing the data)
5.    data (parameter) (points to the array to be written)

UNIT OUTPUTS:
1.    returns 1 if okay, 0 if error
2.    a fits file named 'filename'

**********************************************************************************************/

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, char *comment, void *data )
{
	char      tmpBuf[512];
	int       i;
	fitsfile  *fptr;
	char      fitsFilename[256];
	int       status = 0;
	long      dataAxes[4];  // will handle up to 4 dimensions
	long      fpixel[4];    // will handle up to 4 dimensions
	long long size;

//	make sure the filename begins with '!' so fitsio will create the file and then open the it
	strcpy( fitsFilename, "!");
	strcat( fitsFilename, filename);
	fits_create_file( &fptr, fitsFilename, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	for( i = 0; i < 4; i++)
	{
		dataAxes[i] = 0;
		fpixel[i] = 1;
	}

//	create an image, write header keywords, and write out the data
	size = 1;
	for( i = 0; i < numDim; i++)
	{
		dataAxes[i] = dimSize[i];
		size *= dimSize[i];
	}
	switch (dataType)
	{
	case TBYTE:
		fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status );
		break;
	case TDOUBLE:
		fits_create_img( fptr, DOUBLE_IMG, numDim, dataAxes, &status );
		break;
	case TLONG:
		fits_create_img( fptr, LONG_IMG, numDim, dataAxes, &status );
		break;
	case TULONG:
		fits_create_img( fptr, ULONG_IMG, numDim, dataAxes, &status );
		break;
	case TSHORT:
		fits_create_img( fptr, SHORT_IMG, numDim, dataAxes, &status );
		break;
	case TFLOAT:
		fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status );
		break;
	default:
		printf( "Error: fits file type not allowed: %d\n", dataType );
		break;
	}
	if (status != 0)
	{
		printf( "Error: fits image could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_date( fptr, &status );
	if (status != 0)
	{
		printf( "Error: fits file date could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_comment( fptr, comment, &status );
	if (status != 0)
	{
		printf( "Error: fits file comment could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_pix( fptr, dataType, fpixel, size, data, &status );
	if (status != 0)
	{
		printf( "Error: fits file data could not be writen: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

//	close the file
	fits_close_file( fptr, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be closed for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	return 1;
}


static void termHandler(int sigNum)
{
	printf("merge_bin2fits quitting due to OS term: %d\n", sigNum);
	quit = true;
}
