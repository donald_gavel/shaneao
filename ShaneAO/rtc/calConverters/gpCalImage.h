/*******************************************************************************
 *
 *  Copyright (c) 2009 Jet Propulsion Laboratory.
 *  All rights reserved.
 *
 *  These commodities, technology or software were exported from
 *  the United States in accordance with the Export Administration Regulations.
 *  Diversion contrary to U.S. law is prohibited.
 *
 ******************************************************************************/
/**
 ************************************************************************
 *
 * @file gpCalImage.h
 *
 * @brief
 *  Functions for image processing.
 *
 *  Contains functions used for processing either the current frame of an image
 *  or the entire image.  Functions range from arithmetic operations to more
 *  complicated operations such as creating a phase map.
 *
 * @author
 *  John Angione
 *
 * @b REVISIONS:
 * $Log: gpCalImage.h,v $
 * Revision 1.1  2012/09/12 19:53:44  will
 * *** empty log message ***
 *
 * Revision 1.13  2010/04/22 15:53:40  jangione
 * New Flat Routine.
 *
 * Revision 1.12  2010/04/22 13:56:41  jangione
 * Update HOWFS Reconstruction Algorithm.
 *
 * Revision 1.11  2010/04/16 04:51:21  jangione
 * Calculate RMS.
 *
 * Revision 1.10  2010/03/18 05:25:49  jangione
 * Add second pupil.
 *
 * Revision 1.9  2010/03/09 20:45:35  gpiops
 * Changed gpCalImageFree's input parameter to void *
 *
 * Revision 1.8  2010/02/22 17:57:05  jangione
 * Miscellaneous mods to support HOWFS offsets.
 *
 * Revision 1.7  2010/02/05 22:06:30  jangione
 * Add canned data.
 *
 * Revision 1.6  2010/01/28 21:45:17  gpiops
 * Added gpCalImageFree function to release all image memory including frame memory.
 *
 * Revision 1.5  2009/11/02 16:47:32  jangione
 * Updated to use global parameter structure.
 *
 * Revision 1.4  2009/08/10 23:57:32  jangione
 * Documentation updates.
 *
 *
 ************************************************************************
 */

#ifndef GP_CAL_IMAGE_H
#define GP_CAL_IMAGE_H

#ifdef __cplusplus
extern "C" {
#endif


#include <time.h>
#include "gpCalTypes.h"
#include "cfitsio/fitsio.h"
#include "gpCalError.h"


//=====================================================================
// Typedefs

/**
 * @brief
 *  Structure to represent a data cube.
 *
 *  Structure to represent a data cube.
 */
typedef struct gpCalImage  {
    int wid;         /**< Width of each frame */
    int ht;          /**< Height of each frame */
    int cur;         /**< Index of the current frame */
    int num;         /**< Total number of frames in the image */
    float **images;  /**< Raw image data */
} gpCalImage_t;


//=====================================================================
// Global Function Prototypes

gpCalImage_t *gpCalImageCreate (int width,
                                int height,
                                int num_images,
                                float initial_value);

gpCalImage_t *gpCalImageCreateFromFile (const char *filename);

gpCalErrorCode_t gpCalImageReadFits (gpCalImage_t *image,
                                     const char *filename);

gpCalErrorCode_t gpCalImageSaveFits (gpCalImage_t *image,
                                     const char *filename,
                                     fitsfile **fptrptr );

void gpCalImageFree (void *image);

gpCalErrorCode_t gpCalImageCloseFits (fitsfile *fptr);

gpCalErrorCode_t gpCalImageAppend (gpCalImage_t *image, float *frame);

gpCalErrorCode_t gpCalImageAppendFrame (gpCalImage_t *image,
                                        const unsigned short *frame,
                                        int start_col,
                                        int start_row,
                                        int cols,
                                        int rows);

gpCalErrorCode_t gpCalImageProcess (gpCalImage_t *image,
                                    const gpCalImage_t *dark,
                                    const gpCalImage_t *flat);

gpCalErrorCode_t gpCalImageCopyImageToCurrent (gpCalImage_t *image,
                                               const gpCalImage_t *source,
                                               int image_num);

gpCalErrorCode_t gpCalImageCopyImageToImage (gpCalImage_t *image,
                                             const gpCalImage_t *source,
                                             int src_num,
                                             int dest_num);

gpCalErrorCode_t gpCalImageGetPixel (const gpCalImage_t *image,
                                     int image_num,
                                     int x,
                                     int y,
                                     float *val);

gpCalErrorCode_t gpCalImageNext (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageZero (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageZeroNext (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageZeroAll (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageCreatePhaseMap (gpCalImage_t *image,
                                           const float *zernikes,
                                           int num_zern,
                                           float obs_rad);

gpCalErrorCode_t gpCalImageAdd (gpCalImage_t *augend,
                                const gpCalImage_t *addend,
                                int image_num);

gpCalErrorCode_t gpCalImageAddFloat (gpCalImage_t *augend,
                                     float addend);

gpCalErrorCode_t gpCalImageSubtract (gpCalImage_t *minuend,
                                     const gpCalImage_t *subtrahend,
                                     int image_num);

gpCalErrorCode_t gpCalImageSubtractFloat (gpCalImage_t *minuend,
                                          float subtrahend);

gpCalErrorCode_t gpCalImageMultiply (gpCalImage_t *multiplicand,
                                     const gpCalImage_t *multiplier,
                                     int image_num);

gpCalErrorCode_t gpCalImageMultiplyByFloat (gpCalImage_t *multiplicand,
                                            float multiplier);

gpCalErrorCode_t gpCalImageDivide (gpCalImage_t *dividend,
                                   const gpCalImage_t *divisor,
                                   int image_num);

gpCalErrorCode_t gpCalImageDivideByFloat (gpCalImage_t *dividend,
                                          float divisor);

gpCalErrorCode_t gpCalImageFlipLeftRight (gpCalImage_t *flipped,
                                          const gpCalImage_t *source);

gpCalErrorCode_t gpCalImageSum (const gpCalImage_t *image,
                                float *sum);

gpCalErrorCode_t gpCalImageMean (const gpCalImage_t *image,
                                 gpCalImage_t *mean);

gpCalErrorCode_t gpCalImageMeanValues (const gpCalImage_t *image,
                                       float *mean_values,
                                       int array_size);

gpCalErrorCode_t gpCalImageNonZeroMeanValues (const gpCalImage_t *image,
                                              float *mean_values,
                                              int array_size);

gpCalErrorCode_t gpCalImageRMS (const gpCalImage_t *image,
                                float *rms_values,
                                int array_size);

gpCalErrorCode_t gpCalImageSine (gpCalImage_t *sine,
                                 const gpCalImage_t *image);

gpCalErrorCode_t gpCalImageCosine (gpCalImage_t *cosine,
                                   const gpCalImage_t *image);

gpCalErrorCode_t gpCalImageRotate (gpCalImage_t *x_prime,
                                   gpCalImage_t *y_prime,
                                   const gpCalImage_t *x,
                                   const gpCalImage_t *y,
                                   const gpCalImage_t *angles);

gpCalErrorCode_t gpCalImageNormalize (const gpCalImage_t *image);

gpCalErrorCode_t gpCalImageInverseNormalize (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageInverse (gpCalImage_t *image, float div_threshold, float replace_value);

gpCalErrorCode_t gpCalImageSquareRoot (gpCalImage_t *image);

gpCalErrorCode_t gpCalImageCentroid (const gpCalImage_t *image,
                                     int col,
                                     int row,
                                     int size,
                                     float min_flux,
                                     float *x_cent,
                                     float *y_cent,
                                     float *sum);

gpCalErrorCode_t gpCalImageGetROI (const gpCalImage_t *image,
                                   gpCalImage_t *roi,
                                   int start_col,
                                   int start_row);

gpCalErrorCode_t gpCalImageSetROI (gpCalImage_t *image,
                                   const gpCalImage_t *roi,
                                   int start_col,
                                   int start_row);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*  GP_CAL_IMAGE_H  */

