/*******************************************************************************
 *
 *	COPYRIGHT (C) 2011
 *	LAWRENCE LIVERMORE NATIONAL LABORATORY
 *	ALL RIGHTS RESERVED, ANY UNAUTHORIZED DUPLICATION PROHIBITTED
 *
 ******************************************************************************/
/*!
 *
 *	@file gpCal_howfs_bin2fits.c
 *
 * 	@version
 *
 *	@brief 
 *	This component is a standalone program to convert Cal subsystem howfs data
 *	files from binary format to fits format. 
 *
 *	@b DESCRIPTION:
 *	This component is a standalone program to convert Cal subsystem howfs data
 *	files from binary format to fits format. 
 *	@par
 *	NOTE that this program is relatively temporary for testing purposes.  As such
 *	it uses local versions of the Cal header files.  If it ever gets integrated into
 *	the permanent Cal subsystem, it should use the deliverable Cal header files.  
 *	@par
 *
 *	@b AUTHOR:
 *	AGC	DATE: 9/1/2011
 *
 *	@b FUNCTION NAMES:
 *	-# @c howfsBin2fits()
 *
 *	@b REVISIONS:
 *
 *******************************************************************************
 */

#define _GNU_SOURCE

#include <time.h>
#include <signal.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <sched.h>
#include <unistd.h>
#include <limits.h>
#include <linux/unistd.h> 

// VERY IMPORTANT -- LONGLONG is defined incorrectly in gpCalTypes.h -- dwp changed a local 
//					 copy of that file for bin2fits !!
#include "gpCalHowfs.h"

#include "fitsio.h"

typedef	signed short boolean;
#define false 0
#define FALSE 0
#define true  1
#define TRUE  1

// VERY IMPORTANT -- this is gpCalHowfsFrame_t from gpCalHowfs.h, but padded by 12 bytes; the 
//					 padding appears different between the Cal and AOC, can't see why

typedef struct gpCalHowfsFrame_PAD  {
	gpCalHowfsRawImage_t rawFrame;     /**< Raw data from the HOWFS */
	gpCalHowfsImage_t processedFrame;  /**< Dark-subtracted, flat-fielded HOWFS ROI frames */
	int frameNumber;                   /**< HOWFS frame number from the framegrabber */
	char  padding1[4];                 /**< PAD XXX */
	int  tv_sec;           /**< Time which the frame was */ //XXX
	char padding2[4];				  // PAD //XXX
	int  tv_usec;          /*   transferred via DMA. */     //XXX
	char  padding3[4];                 /**< PAD XXX */
	float frameRate;                   /**< HOWFS frame rate */
	int step;                          /**< Current step (0..#GP_CAL_HOWFS_MAX_BINS) */
	float pztCoarsePosition;           /**< Position of the coarse phasing stage */
	float pztFinePosition;             /**< Position of the fine phasing stage */
	float pztTip;                      /**< Tip of the fine PZTs */
	float pztTilt;                     /**< Tilt of the fine PZTs */
} gpCalHowfsFrame_PAD_t;



// Configuration Variables

#define MAX_IMAGES 10000  // maximum images per file


// Global Variables


// Local Variables

static boolean quit = false;

static gpCalHowfsFrame_PAD_t howfsData[MAX_IMAGES];

static short howfsRawImages[MAX_IMAGES][GP_CAL_HOWFS_RAW_HEIGHT][GP_CAL_HOWFS_RAW_WIDTH];  // NOTE that width and height are flipped from binary
static float howfsImages[MAX_IMAGES][GP_CAL_HOWFS_HEIGHT][GP_CAL_HOWFS_WIDTH];			   // NOTE that width and height are flipped from binary
static long  howfsStats[MAX_IMAGES][9];  


// Local Function Prototypes

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, void *data );
static void termHandler(int sigNum);


int main(int argc, char *argv[])
{
	FILE *fp;
	char path[128], fname[128], fullFilename[256];
	int	 i, j, k;
	int	 numHowfsData;
	long dimSize[4];  // will handle up to 4 dimensions

//	set up signal handlers
	signal(SIGTERM, termHandler);

//	set defaults and then parse command line
	path[0] = '\0';
	strcpy(fname, "howfs");
	--argc;
	++argv;
	while (argc && (argv[0][0] == '-'))
	{
		switch (argv[0][1])
		{
		case 'p':		// path
			++argv;
			--argc;
			strcpy(path, argv[0]);
			break;

		case 'f':		// file name
			++argv;
			--argc;
			strcpy(fname, argv[0]);
			break;

		case '?':
		case 'h':
			printf("\nCal howfs data bin2fits convertor\n");
			printf("usage:\n");
			printf("  -p pathName   path for input and output files\n");
			printf("  -f fileName   file name for input and output files (don't include an extension)\n");
			printf("  -h            print this help message\n");
			exit(0);
			break;

		default:
			printf("gpAoDumpRawCam: unknown command flag -'%c'\n", argv[0][1]);
			break;
		}
		argc--;
		argv++;
	}

//	read HOWFS data
	strcpy(fullFilename, path);
	strcat(fullFilename, fname);
	strcat(fullFilename, ".data");
	if((fp = fopen(fullFilename, "r")) == NULL)
	{
		printf ("howfs_bin2fits: error opening input file: %s\n", fullFilename);
		exit(-1);
	}
	fread(&numHowfsData, sizeof(int), 1, fp);
	if (numHowfsData > MAX_IMAGES)
	{
		printf ("howfs_bin2fits: max number of images exceeded, only first %d being converted\n", MAX_IMAGES);
		numHowfsData = MAX_IMAGES;
	}
	fread(howfsData, sizeof(gpCalHowfsFrame_PAD_t), numHowfsData, fp);
	fclose (fp);

//	if we're not quitting, store the howfs raw frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_raw.fits");

		for (i = 0; i < numHowfsData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_RAW_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_RAW_WIDTH; k++)
					howfsRawImages[i][j][k] = howfsData[i].rawFrame[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_HOWFS_RAW_WIDTH);
		dimSize[1] = (long)(GP_CAL_HOWFS_RAW_HEIGHT);
		dimSize[2] = (long)numHowfsData;
		if (writeFitsFile(fullFilename, dimSize, 3, TSHORT, howfsRawImages) == 0)
		{
			printf ("howfs_bin2fits: error writing howfs raw image file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the howfs processed frames
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_image.fits");

		for (i = 0; i < numHowfsData; i++)
		{
			for (j = 0; j < GP_CAL_HOWFS_HEIGHT; j++)
			{
				for (k = 0; k < GP_CAL_HOWFS_WIDTH; k++)
					howfsImages[i][j][k] = howfsData[i].processedFrame[j][k];
			}
		}

		dimSize[0] = (long)(GP_CAL_HOWFS_WIDTH);
		dimSize[1] = (long)(GP_CAL_HOWFS_HEIGHT);
		dimSize[2] = (long)numHowfsData;
		if (writeFitsFile(fullFilename, dimSize, 3, TFLOAT, howfsImages) == 0)
		{
			printf ("howfs_bin2fits: error writing howfs image file\n");
			quit = true;
		}
	}

//	if we're not quitting, store the howfs frame stats
	if (!quit)
	{
		strcpy(fullFilename, path);
		strcat(fullFilename, fname);
		strcat(fullFilename, "_stats.fits");

		for (i = 0; i < numHowfsData; i++)
		{
			howfsStats[i][0] = howfsData[i].frameNumber;
			howfsStats[i][1] = howfsData[i].tv_sec;
			howfsStats[i][2] = howfsData[i].tv_usec;
			howfsStats[i][3] = (int)howfsData[i].frameRate;
			howfsStats[i][4] = howfsData[i].step;
			howfsStats[i][5] = (int)howfsData[i].pztCoarsePosition;
			howfsStats[i][6] = (int)howfsData[i].pztFinePosition;
			howfsStats[i][7] = (int)howfsData[i].pztTip;
			howfsStats[i][8] = (int)howfsData[i].pztTilt;
		}

		dimSize[0] = 9L;
		dimSize[1] = (long)numHowfsData;
		if (writeFitsFile(fullFilename, dimSize, 2, TLONG, howfsStats) == 0)
			printf ("howfs_bin2fits: error writing howfs stats file\n");
	}

	printf("numFrames: %d; sizes -- struct: %d, raw: %d, image: %d, struct timeval: %d, short: %d\n",
	       numHowfsData, sizeof(gpCalHowfsFrame_PAD_t), sizeof(gpCalHowfsRawImage_t), 
	       sizeof(gpCalHowfsImage_t), sizeof(struct timeval), sizeof(short));
	printf("rawW: %d, rawH: %d, imageW: %d, imageH: %d\n", 
	       GP_CAL_HOWFS_RAW_WIDTH, GP_CAL_HOWFS_RAW_HEIGHT, 
	       GP_CAL_HOWFS_WIDTH, GP_CAL_HOWFS_HEIGHT);
	printf("howfs_bin2fits: file conversion done\n");
}


/**********************************************************************************************

UNIT NAME:  writeFitsFile()

UNIT DESCRIPTION:
The purpose of this unit is to write a FITS file from a possibly multi-dimensional array.  

UNIT INPUTS:
1.    filename (parameter) (name of the fits file to write)
2.    dimSize (parameter) (the dimensions of the array to be written)
3.    numDim (parameter) (the number or dimensions of the array)
4.    dataType (parameter) (data type, using cfitsio data type designators)
5.    data (parameter) (points to the array to be written)

UNIT OUTPUTS:
1.    returns 1 if okay, 0 if error
2.    a fits file named 'filename'

**********************************************************************************************/

int writeFitsFile( char *filename, long *dimSize, int numDim, int dataType, void *data )
{
	char      tmpBuf[512];
	int       i;
	fitsfile  *fptr;
	char      fitsFilename[256];
	int       status = 0;
	long      dataAxes[4];  // will handle up to 4 dimensions
	long      fpixel[4];    // will handle up to 4 dimensions
	long long size;

//	make sure the filename begins with '!' so fitsio will create the file and then open the it
	strcpy( fitsFilename, "!");
	strcat( fitsFilename, filename);
	fits_create_file( &fptr, fitsFilename, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	for( i = 0; i < 4; i++)
	{
		dataAxes[i] = 0;
		fpixel[i] = 1;
	}

//	create an image, write header keywords, and write out the data
	size = 1;
	for( i = 0; i < numDim; i++)
	{
		dataAxes[i] = dimSize[i];
		size *= dimSize[i];
	}
	switch (dataType)
	{
	case TBYTE:
		fits_create_img( fptr, BYTE_IMG, numDim, dataAxes, &status );
		break;
	case TDOUBLE:
		fits_create_img( fptr, DOUBLE_IMG, numDim, dataAxes, &status );
		break;
	case TLONG:
		fits_create_img( fptr, LONG_IMG, numDim, dataAxes, &status );
		break;
	case TULONG:
		fits_create_img( fptr, ULONG_IMG, numDim, dataAxes, &status );
		break;
	case TSHORT:
		fits_create_img( fptr, SHORT_IMG, numDim, dataAxes, &status );
		break;
	case TFLOAT:
		fits_create_img( fptr, FLOAT_IMG, numDim, dataAxes, &status );
		break;
	default:
		printf( "Error: fits file type not allowed: %d\n", dataType );
		break;
	}
	if (status != 0)
	{
		printf( "Error: fits image could not be created for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_date( fptr, &status );
	if (status != 0)
	{
		printf( "Error: fits file date could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_comment( fptr, "howfs data", &status );
	if (status != 0)
	{
		printf( "Error: fits file comment could not be written: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}
	fits_write_pix( fptr, dataType, fpixel, size, data, &status );
	if (status != 0)
	{
		printf( "Error: fits file data could not be writen: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

//	close the file
	fits_close_file( fptr, &status);
	if (status != 0)
	{
		printf( "Error: fits file could not be closed for write: %s, error: %d\n", fitsFilename, status );
		fits_get_errstatus( status, tmpBuf );
		printf( "Fits error message: %s\n", tmpBuf );
		return 0;
	}

	return 1;
}


static void termHandler(int sigNum)
{
	printf("howfs_bin2fits quitting due to OS term: %d\n", sigNum);
	quit = true;
}
