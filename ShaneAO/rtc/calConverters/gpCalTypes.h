/*******************************************************************************
 *
 *  Copyright (c) 2009 Jet Propulsion Laboratory.
 *  All rights reserved.
 *
 *  These commodities, technology or software were exported from
 *  the United States in accordance with the Export Administration Regulations.
 *  Diversion contrary to U.S. law is prohibited.
 *
 ******************************************************************************/
/**
 ************************************************************************
 *
 * @file gpCalTypes.h
 *
 * @brief
 *  Header file for types used throughout gpCal
 *
 *  Header file for types used throughout gpCal
 *
 * @author
 *  S. F. Fregoso
 *
 *
 ************************************************************************
 */

#ifndef GP_CAL_TYPES_H
#define GP_CAL_TYPES_H

#ifdef __cplusplus
extern "C" {
#endif


/*
 *  These next two lines are needed for fitsio
 *  There is an assumption here that a long int is 64-bits.
 */
#ifndef LONGLONG_TYPE
#define LONGLONG_TYPE

//XXXdwp110729 typedef long int LONGLONG;
typedef long long LONGLONG;  //XXXdwp110729

#ifndef NRANSI
#define NRANSI

#endif
#endif





#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /*  GP_CAL_TYPES_H  */
