/*******************************************************************************
 *
 *  Copyright (c) 2009 Jet Propulsion Laboratory.
 *  All rights reserved.
 *
 *  These commodities, technology or software were exported from
 *  the United States in accordance with the Export Administration Regulations.
 *  Diversion contrary to U.S. law is prohibited.
 *
 ******************************************************************************/
/**
 ************************************************************************
 *
 * @file gpCalError.h
 *
 * @brief
 *  Library for error reporting.
 *
 *  This library contains all of the error codes that are used throughout
 *  the Cal system software, as well as functions to print error messages.
 *
 * @author
 *  John Angione
 *
 * @b REVISIONS:
 * $Log: gpCalError.h,v $
 * Revision 1.1  2012/09/12 19:53:44  will
 * *** empty log message ***
 *
 * Revision 1.4  2009/08/07 17:39:19  jangione
 * Updated documentation.
 *
 * Revision 1.3  2009/08/05 03:14:24  jangione
 * Additional documentation added.
 *
 * Revision 1.2  2009/08/01 20:29:56  li
 * added CVS log tag and modified copyright header
 *
 *
 ************************************************************************
 */

#ifndef GP_CAL_ERROR_H
#define GP_CAL_ERROR_H

#ifdef __cplusplus
extern "C" {
#endif
    
#include <stdio.h>

//=====================================================================
// Defines

/**
 * @brief
 *  Maximum length of an error message.
*
 *  Maximum length of an error message.
 */
#define GP_CL_ERROR_MSG_LEN  256
    
//=====================================================================
// Typedefs

/**
 * @brief
 *  Error returns from functions.
 *
 *  Error returns from functions. This is a complete list of all errors
 *  returned from functions in the CAL System.
 */

typedef enum gpCalErrorCode {
    /**
     * The fuction completed successfully.
     */
    gpCalSuccess,

    /**
     * The function was passed a @a NULL pointer.
     */
    gpCalErrorNullPointer = 3000,

    /**
     * The function was passed an invalid value.
     */
    gpCalErrorValueOutOfRange,

    /**
     * An attempt to allocate memory failed.
     */
    gpCalErrorOutOfMemory,
    
    /**
     * The specified structure or device could not be properly initialized.
     */
    gpCalErrorCannotInitialize,

    /**
     * The specified structure or device has not been initialized.
     */
    gpCalErrorNotInitialized,

    /**
     * The specified device which had previously been successfully initialized
     * is no longer responding.
     */
    gpCalErrorDeviceNotResponding,

    /**
     * The specified device returned an unexpected response.
     */
    gpCalErrorDeviceCommandError,
    
    /**
     * An algorithm failed to converge.
     */
    gpCalErrorDidNotConverge,
    
    /**
     * The specified filename is invalid.
     */
    gpCalErrorInvalidFileName,

    /**
     * The specified file could not be opened.
     */
    gpCalErrorCannotOpenFile,
    
    /**
     * The specified file could not be saved.
     */
    gpCalErrorCannotSaveFile,
    
    /**
     * The specified file is not in the correct format.
     */
    gpCalErrorIncorrectFileFormat,
    
    /**
     * The specified image has incorrect dimensions.
     */
    gpCalErrorIncorrectImageFormat,
    
    /**
     * An attempt to read an image timed out.
     */
    gpCalErrorCameraTimeout,

    /**
     * An error occurred during a network operation.
     */
    gpCalErrorNetwork
    
} gpCalErrorCode_t;

//=====================================================================
// Global Function Prototypes

const char *gpCalErrorStrError (gpCalErrorCode_t code);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* GP_CAL_ERROR_H */
