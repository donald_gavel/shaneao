/*******************************************************************************
 *
 *  Copyright (c) 2009 Jet Propulsion Laboratory.
 *  All rights reserved.
 *
 *  These commodities, technology or software were exported from
 *  the United States in accordance with the Export Administration Regulations.
 *  Diversion contrary to U.S. law is prohibited.
 *
 ******************************************************************************/
/**
 ************************************************************************
 *
 * @file gpCalLowfs.h
 *
 * @brief
 *  Function headers for LOWFS reconstruction.
 *
 *
 * @author
 *  John Angione
 *
 * @b REVISIONS:
 * $Log: gpCalLowfs.h,v $
 * Revision 1.1  2012/09/12 19:53:44  will
 * *** empty log message ***
 *
 * Revision 1.9  2010/02/23 00:50:59  gpiops
 * deleted a line
 *
 * Revision 1.8  2010/02/05 22:06:30  jangione
 * Add canned data.
 *
 * Revision 1.7  2010/01/22 21:48:49  sfregoso
 * Added #defines and function prototype for computation of signal-to-noise ration of LOWFS.
 *
 * Revision 1.6  2009/12/31 17:27:09  jangione
 * Add centroid threshold.
 *
 * Revision 1.5  2009/08/10 23:57:32  jangione
 * Documentation updates.
 *
 * Revision 1.4  2009/08/07 17:53:18  jangione
 * Add support for framegrabber region-of-interest readout.
 *
 * Revision 1.3  2009/08/01 20:29:56  li
 * added CVS log tag and modified copyright header
 *
 *
 ************************************************************************
 */

#ifndef GP_CAL_LOWFS_H
#define GP_CAL_LOWFS_H

#ifdef __cplusplus
extern "C" {
#endif
   
#include <sys/select.h>  // for struct timeval

#include "gpCalImage.h"
#include "gpCalError.h"

//=====================================================================
// Defines

/**
 * @brief
 * Number of lenslets on a side.
 *
 * Number of lenslets on a side.
 */
#define GP_CAL_LOWFS_LENSLET_ARRAY_SIZE     7

/**
 * @brief
 * Total number of lenslets.
 *
 * Total number of lenslets, which is simply the square of 
 * #GP_CAL_LOWFS_LENSLET_ARRAY_SIZE.
 */
#define GP_CAL_LOWFS_NUM_LENSLETS           (GP_CAL_LOWFS_LENSLET_ARRAY_SIZE * GP_CAL_LOWFS_LENSLET_ARRAY_SIZE)

/**
 * @brief
 * Width in pixels over which each centroid is computed.
 *
 * Width in pixels over which each centroid is computed.
 */
#define GP_CAL_LOWFS_CENTROID_SIZE          4

/**
 * @brief
 * Width in pixels of the buffer between each centroid region.
 *
 * Width in pixels of the buffer between each centroid region.
 */
#define GP_CAL_LOWFS_LENSLET_BUFFER_SIZE    1

/**
 * @brief
 * Total width in pixels of each centroid region.
 *
 * Total width in pixels of each centroid region.
 */
#define GP_CAL_LOWFS_LENSLET_SIZE  (GP_CAL_LOWFS_CENTROID_SIZE + GP_CAL_LOWFS_LENSLET_BUFFER_SIZE)

/**
 * @brief
 * Width of the region of interest.
 *
 * Width of the region of interest.
 */
#define GP_CAL_LOWFS_WIDTH         (GP_CAL_LOWFS_LENSLET_ARRAY_SIZE * GP_CAL_LOWFS_LENSLET_SIZE - 1)

/**
 * @brief
 * Height of the region of interest.
 *
 * Height of the region of interest.
 */
#define GP_CAL_LOWFS_HEIGHT        (GP_CAL_LOWFS_LENSLET_ARRAY_SIZE * GP_CAL_LOWFS_LENSLET_SIZE - 1)

/**
 * @brief
 * Number of pixels in the "raw frame" to the left of the region of interest.
 *
 * Number of pixels in the "raw frame" to the left of the region of interest.
 */
#define GP_CAL_LOWFS_LEFT_MARGIN   2

/**
 * @brief
 * Number of pixels in the "raw frame" to the right of the region of interest.
 *
 * Number of pixels in the "raw frame" to the right of the region of interest.
 */
#define GP_CAL_LOWFS_RIGHT_MARGIN  2

/**
 * @brief
 * Number of pixels in the "raw frame" to the top of the region of interest.
 *
 * Number of pixels in the "raw frame" to the top of the region of interest.
 */
#define GP_CAL_LOWFS_TOP_MARGIN    2

/**
 * @brief
 * Number of pixels in the "raw frame" to the bottom of the region of interest.
 *
 * Number of pixels in the "raw frame" to the bottom of the region of interest.
 */
#define GP_CAL_LOWFS_BOTTOM_MARGIN 2

/**
 * @brief
 * Total width of a LOWFS "raw frame".
 *
 * Total width of a LOWFS "raw frame".
 */
#define GP_CAL_LOWFS_RAW_WIDTH     (GP_CAL_LOWFS_WIDTH + GP_CAL_LOWFS_LEFT_MARGIN + GP_CAL_LOWFS_RIGHT_MARGIN)

/**
 * @brief
 * Total height of a LOWFS "raw frame".
 *
 * Total height of a LOWFS "raw frame".
 */
#define GP_CAL_LOWFS_RAW_HEIGHT    (GP_CAL_LOWFS_HEIGHT + GP_CAL_LOWFS_TOP_MARGIN + GP_CAL_LOWFS_BOTTOM_MARGIN)

/**
 * @brief
 *  LOWFS default frame rate.
 *
 *  LOWFS default frame rate.
 */
#define GP_CAL_LOWFS_DEFAULT_FRAME_RATE     60

/**
 * @brief
 * Column of region to compute noise for SNR.
 *
 * Column of region to compute noise for SNR.
 */
#define GP_CAL_LOWFS_SNR_NOISE_ROI_COL    0

/**
 * @brief
 * Row of region to compute noise for SNR.
 *
 * Row of region to compute noise for SNR.
 */
#define GP_CAL_LOWFS_SNR_NOISE_ROI_ROW    0

/**
 * @brief
 * Number of pixels on a side of noise region for SNR computation.
 *
 * Number of pixels on a side of noise region for SNR computation.
 * The noise region is #GP_CAL_SNR_NOISE_ROI_SIZE by GP_CAL_SNR_NOISE_ROI_SIZE
 */
#define GP_CAL_LOWFS_SNR_NOISE_ROI_SIZE   4


//=====================================================================
// Typedefs

/**
 * @brief
 *  LOWFS Raw Image.
 *
 *  LOWFS raw image. This is used to store the raw camera data from the LOWFS
 *  plus a margin around the edges.
 */
typedef short gpCalLowfsRawImage_t[GP_CAL_LOWFS_RAW_WIDTH][GP_CAL_LOWFS_RAW_HEIGHT];

/**
 * @brief
 *  LOWFS Image.
 *
 *  LOWFS image. This is used to store the dark-subtracted, flat-fielded data
 *  from the LOWFS.
 */
typedef float gpCalLowfsImage_t[GP_CAL_LOWFS_WIDTH][GP_CAL_LOWFS_HEIGHT];

/**
 * @brief
 *  LOWFS lenslet array.
 *
 *  LOWFS lenslet array. This is used to store data for each lenslet.
 */
typedef float gpCalLowfsArray_t[GP_CAL_LOWFS_LENSLET_ARRAY_SIZE][GP_CAL_LOWFS_LENSLET_ARRAY_SIZE];


/**
 * @brief
 *  LOWFS frame.
 *
 *  LOWFS frame. This stores a single LOWFS frame, along with ancilliary data.
 */

typedef struct gpCalLowfsFrame  {
    gpCalLowfsRawImage_t rawFrame;    /**< Raw data from the LOWFS. */
    gpCalLowfsImage_t processedFrame; /**< Dark-subtracted, flat-fielded
                                       *   LOWFS ROI frames. */
    int frameNumber;                  /**< LOWFS frame number from the
                                       *   framegrabber. */
    struct timeval timeTag;           /**< Time which the frame was 
                                       *   transferred via DMA. */
    float frameRate;                  /**< LOWFS frame rate. */
} gpCalLowfsFrame_t;    


/**
 * @brief
 *  Structure to represent the LOWFS Reconstructor.
 *
 *  Structure to represent the LOWFS Reconstructor.
 */

typedef struct gpCalLowfsReconstructor  {
    char reconstructorFileName[FILENAME_MAX]; /**< Filename of the
                                               *   reconstructor */
    int numZernikes;        /**< Number of Zernikes to be calculated by the 
                             *   reconstructor */
    int numUsefulLenslets;  /**< Number of lenslets  which contribute to the
                             *   reconstruction */
    int *lenslets;          /**< List of useful lenslets */
    float **reconstructor;  /**< Reconstructor matrix */
} gpCalLowfsReconstructor_t;

    
//=====================================================================
// Global Function Prototypes

gpCalLowfsReconstructor_t *gpCalLowfsInit ();

gpCalErrorCode_t gpCalLowfsExit (gpCalLowfsReconstructor_t *l);

gpCalErrorCode_t gpCalLowfsLoadReconstructor (gpCalLowfsReconstructor_t *l,
                                              const char *filename);

gpCalErrorCode_t gpCalLowfsCentroids (const gpCalImage_t *image,
                                      float min_flux,
                                      float *x_cents,
                                      float *y_cents,
                                      float *total_flux);

gpCalErrorCode_t gpCalLowfsReconstruct (const gpCalLowfsReconstructor_t *l,
                                        float *x_cents,
                                        float *y_cents,
                                        float *zernike_coefficients);

gpCalErrorCode_t gpCalLowfsWriteFitsHeader (gpCalLowfsReconstructor_t *l,
                                            fitsfile *fptr);

gpCalErrorCode_t gpCalLowfsSnr (const gpCalLowfsReconstructor_t *l, gpCalImage_t *image, float *snr);


#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* GP_CAL_LOWFS_H */

