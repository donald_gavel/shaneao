#
#
# The following is the configuration file for the AOC make files
# It is included in all Makefiles in all the directories.
#
# CUSTOMIZATION REQUIRED:
#	Typically, only the following:
#	1) PATHS: GPI_ROOT, JDK_PATH, verify the TLC_ROOT
#	2) Platform format ( GPI, Simulated )
#
#######################################
#
# paths
#
ifndef GPI_ROOT
	GPI_ROOT = $(HOME)/gpi_root
endif

ifndef TLC_ROOT
	TLC_ROOT = /home/aocuser/tlc
endif

ifndef TLC_BINPATH
	TLC_BINPATH = $(TLC_ROOT)/bin/linux64
endif

ifndef AOC_ROOT
	AOC_ROOT = $(GPI_ROOT)/aoc
endif

ifndef AOC_BINPATH
	AOC_BINPATH = $(AOC_ROOT)/bin
endif
 
ifndef JDK_PATH
	JDK_PATH = /usr/java/default
endif

ifndef JAVA_HOME
	JAVA_HOME = $(JDK_PATH)
endif

ifndef MY_JAVA_JARS
	MY_JAVA_JARS = $(AOC_BINPATH)/jars
endif

ifndef CLASSPATH
	CLASSPATH = .:$(MY_JAVA_JARS)/sjJavaUtil.jar:$(MY_JAVA_JARS)/fits.jar
endif

#######################################
#
# System configuration
# IMPORTANT: when RTL_SYSTEM is set to 1, 
# > STANDALONE_FFT MUST be set to 1 !!
#
#######################################

# RTL_SYSTEM On/Off flag
# IMPORTANT: this is overridden by the Makefile command line RTL flag, for now
#RTL_SYSTEM = 0
#RTL_SYSTEM = 1

# Standalone FFT On/Off flag -- 1 means use standalone, 0 means use fftw
#STANDALONE_FFT = 0
STANDALONE_FFT = 1

###############################################################################
#                                                                             #
## Platfom type and characteristics`
##	ONLY enable one define for each 'type'
#
# Although the 3 hardware types are left separate for flexibility, they can 
# be set together by setting REAL_HARDWARE to 1 if there is real hardware 
# or 0 if there isn't
# 
# NOTE!  The strings below must be defined in the ./h/gpAoConstant.h file
#

REAL_HARDWARE = 0

ifeq (0, ${REAL_HARDWARE})
CAM_TYPE = 'WFS_SIM_48X48'
WFR_TYPE = 'WFR_SIM_121'
TWT_TYPE = 'TWT_SIM_4K'
else
CAM_TYPE = 'WFS_REAL_GPI'
WFR_TYPE = 'WFR_GPI_121'
TWT_TYPE = 'TWT_BMC_4K'
endif

SYNCHROBUS_BOARD = 0
#SYNCHROBUS_BOARD = 1

#                                                                             #
###############################################################################


#######################################
#
# DEBUG OPTIONS (set to 1 to turn on)
# IMPORTANT: when RTL_SYSTEM is set to 1, 
# > all _DEBUG and _DUMP MUST be set to 0 !!
#
#######################################

# Turn on - Timer Tests
#TIMER_TEST = 0
TIMER_TEST = 1

CENT_DEBUG = 0
#CENT_DEBUG = 1

RECON_DEBUG = 0
#RECON_DEBUG = 1

RECON_DEBUG_DUMP = 0
#RECON_DEBUG_DUMP = 1

RECONOPT_DEBUG = 0
#RECONOPT_DEBUG = 1

#RECONOPT_DEBUG_DUMP = 0
RECONOPT_DEBUG_DUMP = 1

PARSE_DEBUG = 0
#PARSE_DEBUG = 1

PARSE_DEBUG_DUMP = 0
#PARSE_DEBUG_DUMP = 1

# Turn on - ReconOpt Test
#RECONOPT_TEST = 0
RECONOPT_TEST = 1

## Turn on running average for RECON OPT ( don't calculate over data set )
#RECONOPT_DO_BLOCK_AVG = 0
RECONOPT_DO_BLOCK_AVG = 1
#
# Turn on 'KL_VALID' checking (turned off for checking against IDL
#RECONOPT_TURN_OFF_KL_VALID = 0
RECONOPT_TURN_OFF_KL_VALID = 1

# USED IN RECONOPT_TEST mode
TEST_GAIN = 0.30

#######################################
# 
GP_GMBLOCK_AOC = 1

#######################################
# 
# define GMB_SIM = 0 to use real GMB area - dophin board
# define GMB_SIM = 1 to use dophin simulation software
GMB_SIM = 1

#
#######################################################################################################################
################## ITEMS below this line are seldom modified ##########################################################
#
include ${TLC_ROOT}/config/CONFIG.Deps
include ${AOC_ROOT}/AOC_RELEASE_VERSION

#  This is only required if you have the Dolphin libraries and going to run with
#  the real boards
FIXED_PATH_SCI_INCLUDE = ${TLC_ROOT}/external/h
FIXED_PATH_SCI_LIB = ${TLC_ROOT}/external/lib
SCI_LIB_OBJS = ${TLC_ROOT}/external/obj/*.o

#  This is only required if you have the Dolphin libraries and going to run with
#  the real boards, then change the following uncommented line to be:
#  DEPLIBS_SISCI	= -lsisci
#  
DEPLIBS_SISCI	= 

#  This is only required if you have the Dolphin libraries and going to run with
#  the real boards, then change the following uncommented line to be:
#  
DEPLIBS_GIAPI	= -lgiapi-glue-cc


#####################################
# Location of basics
TAR        = /bin/tar
COMPRESS   = gzip
LS         = /bin/ls
MORE       = /bin/more
RM         = /bin/rm -f
####################
# SUBDIR_FILE is the file that contains a line for each directory
# name that there is a make file in.

SUBDIR_FILE = Makefile.subdirs

####################
# Standard macros
#
CC = gcc
CFLAGS = -c -g -Wall 
# This one is only used for the rpcgen code that is generated to
# avoid all the warnings from the variables not used.
CFLAGS_XDR = -c -g 
CFLAGS_STAND = -g -Wall 

CFLAGS_PIC = -fPIC
CFLAGS_SHARED = ${CFLAGS_PIC} -shared

CXX = g++
CXXFLAGS = -g -c -Wall
CXXFLAGS_STAND = -g -Wall

JAVA = ${JDK_PATH}/bin/java
JAVAC = ${JDK_PATH}/bin/javac
JAVAH = ${JDK_PATH}/bin/javah
JAVAHFLAGS = -jni


#######################################################################################################################
# End of File
#######################################################################################################################

