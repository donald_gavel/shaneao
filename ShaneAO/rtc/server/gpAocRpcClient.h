/************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2006                         (c) 2006
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
*/
/*!
 *
 *  @file gpAocRpcClient.h
 *
 *  @b DESCRIPTION:
 *  Contains include stuff for gpAocRpcClientTester.c
 *
 *
 *  @b AUTHOR:
 *	jsd	DATE: 8/31/08
 *
 *
 *
 *  @b REVISIONS:
 * $Log: gpAocRpcClient.h,v $
 * Revision 1.1  2012/09/12 19:53:45  will
 * *** empty log message ***
 *
 *
 ************************************************************************
*/

#ifndef TLC_AOC_CLIENT_H
#define TLC_AOC_CLIENT_H

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * Includes
 */

#include <string.h>
#include <stdlib.h>

#include <pthread.h>


/*
 *  Maximum number of arguments that you can pass into this rpc client
 */
#define AOC_MAX_NUM_ARGS	12

/*!
 *  @b Detailed_Description
 *  gpAocRpcCloseRpc():
 *   Closes an RPC Connection
 *
 * @param[in] debugLevel (int) Current debug level.
 * @param[mod] pClient (CLIENT **) TCPIP client transport ptr.
 *
 * @return : void 
 */ 
void gpAocRpcCloseRpc( int debugLevel, CLIENT ** pClient );

/*!
 *  @b Detailed_Description
 *  gpAocRpcOpenRpc():
 *   Opens an RPC Connection
 *
 * @param[in] debugLevel (int) debugLevel for printing
 * @param[in] destinHost (const char *) Destination host. 
 * @param[mod] pClient (CLIENT **) TCPIP client transport ptr.
 *
 * @return (int) Success=0 or failure=-1.
 */
int gpAocRpcOpenRpc( int debugLevel, const char * destinHost, 
	CLIENT ** pClient );

/*!
 *  @b Detailed_Description
 *  gpAocRpcRunRpc():
 *   Opens an RPC Connection
 *
 * @param[in] debugLevel (int) debugLevel for printing
 * @param[in] command (int) Command to send (from gpAdcAssRpc.h)
 * @param[mod] pClient (CLIENT *) TCPIP client transport ptr.
 * @param[in] inArgs (char [][GPI_MAX_STRING_LEN]) Input arguments
 * @param[in] numArgs (int) Number of Arguments
 * @param[in] printIt (int) If 1, then print the reply
 * @param[out] reply (GPI_ASSEMBLY_RPC_CMD *) Passed out reply structure
 *
 * @return : (int) Success=0 or failure=-1.
 */
int gpAocRpcRunRpc(  int debugLevel, int command, CLIENT * pClient, 
    char inArgs[][GPI_MAX_STRING_LEN], int numArgs, int printIt, 
	GPI_SUBSERV_RPC_CMD * reply );

#ifdef __cplusplus
}
#endif

#endif /* TLC_AOC_CLIENT_H */
