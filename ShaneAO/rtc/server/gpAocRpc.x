/* 
 ************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2006                         (c) 2006
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 * 					
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
 *
 * FILENAME: 
 *  src/gpAocRpc.x
 *
 * PURPOSE: 
 *  File used to create AOC RPC Server commands.  
 * 
 * DESCRIPTION: 
 *  Use rpcgen to create XDR function file gpAocRpc_xdr.c 
 *  from gpAocRpc.x (creates gpAocRpc.h as well).
 *
 *INDENT-OFF*
 * $Log: gpAocRpc.x,v $
 * Revision 1.1  2012/09/12 19:53:45  will
 * *** empty log message ***
 *
 * Revision 1.13  2010/06/16 05:20:05  dunn
 * updated messages
 *
 * Revision 1.12  2010/04/30 22:14:03  dunn
 * update for AOC
 *
 * Revision 1.11  2010/04/23 21:45:17  syrowikb
 * updated commands to comply with ICD v0.16
 *
 * Revision 1.10  2010/04/16 18:28:42  dunn
 * *** empty log message ***
 *
 * Revision 1.9  2010/02/04 06:45:47  dunn
 * Changes to handle new commands.
 *
 * Revision 1.8  2009/07/04 06:10:35  dunn
 * changes to cmds
 *
 * Revision 1.7  2009/04/15 20:05:11  dunn
 * Changes for new gpSubserver library
 *
 * Revision 1.6  2009/02/26 04:49:04  dunn
 * Changed r0Estimate to double, and added comments.
 *
 * Revision 1.5  2008/12/11 20:22:15  dunn
 *  updates on directive, and on reporting completion.
 *
 * Revision 1.4  2008/10/03 18:34:18  dunn
 * Updated ObservConfig to include appropriate inputs
 *
 * Revision 1.3  2008/09/24 21:30:11  dunn
 * Updated command set names, changed index to datum, errorMessage to
 * errMsg.
 *
 * Revision 1.2  2008/08/27 17:20:35  dunn
 * rewrite
 *
 * Revision 1.1  2008/04/17 04:00:53  dunn
 * Initial revision
 *
 * Revision 1.1  2008/02/07 18:56:59  dunn
 * Initial revision
 *
 *
 *INDENT-ON*
 *
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 ************************************************************************
*/


%#include <gpUtRpcCommon.h>

/*
 *  WARNING, if Command order changes in gpAocRpc.x
 *  then this must FirstWaitTimes array in gpAocCustom.C
 */

enum GP_AOC_SRVR_CMDS
    {
        MT_AOC_GLOBAL,
	MT_AOC_CAL_ON_OFF,
	MT_AOC_CAMERA_RATE,
        MT_AOC_CLOSE_OPEN_LOOP,
	MT_AOC_CYCLE_ACTUATORS,
	MT_AOC_DISPLAY,
	MT_AOC_DUMP,
	MT_AOC_ENV_CONFIG,
	MT_AOC_M1_M2_ON_OFF,
	MT_AOC_OBSERV_CONFIG,
	MT_AOC_OPTIMIZE,
	MT_AOC_PERFORM,
	MT_AOC_RECV_SET_REF_CENT,
	MT_AOC_REF_CENTROIDS,
	MT_AOC_SET_SETTINGS,
	MT_AOC_SHAPES,
	MT_AOC_STAT,
	MT_AOC_STATISTICS,
	MT_AOC_STORE_APO,
	MT_AOC_TAKE_EXP,
	MT_AOC_TAKE_PIXELS,
	MT_AOC_TT,
	MT_AOC_IN_FUNC_GAINS,
	MT_AOC_STATS_LOG,
	MT_AOC_GAINS
    } ;

/********************************************************/

/*
 global		A(int)	all open.
*/
struct mtAocGlobal
    {
	GPI_RPC_CMD_COMMON c;
        int    mode;		/* Mode: init, datum, etc.		*/
        int    level;		/* Debug or sim level.			*/
        int    stop;		/* .			*/
    } ;


/*
calDataOnOff	A=0(off),1(on)	Anytime
*/
struct mtAocCalOnOff
    {
	GPI_RPC_CMD_COMMON c;
        int    on;		/* Set=On or off.			*/
    } ;


/*
autoCamRate	None	all control loops are open 
setCamRate		A(int)		all loops open
*/
struct mtAocCameraRate
    {
	GPI_RPC_CMD_COMMON c;
        int    autoCamRate;	/* Set=Auto or manual.			*/
        int    setCamRate;	/* Yes or no.				*/
    } ;


/*
closeOpenLoops	A,B,C 
*/
struct mtAocCloseOpenLoop
    {
	GPI_RPC_CMD_COMMON c;
        int    tweeter;		/* 0=open 1=close -1=don't change	*/
        int    wooffer;		/* 0=open 1=close -1=don't change	*/
        int    tipTilt;		/* 0=open 1=close -1=don't change	*/
    } ;

/*
cycleActuator	A=#	all control loops are open
stopCycling		None		anytime 
*/
struct mtAocCycleAct
    {
	GPI_RPC_CMD_COMMON c;
	int    actuator;         /* -1: Round robin through all actuators
				0 to 4095: (tweeter actuator),
				5000 to 5121: (woofer actuator),
				6000 to 6001 (TT axis)*/
        int    stop;		/* Set=Stop cycling else start.		*/
    } ;


/*
displayData	A(int)		
*/
struct mtAocDisplay
    {
	GPI_RPC_CMD_COMMON c;
        int    type;		/* Type					*/
        char    filename[STR_LEN];/* Noop				*/
	int	stop;		/* Start/Stop displaying data.		*/
    } ;


/*
dumpData	A, B, C
*/
struct mtAocDump
{
	GPI_RPC_CMD_COMMON c;
        int    type;		/* Type, bit indicates what diag data is 
                                 gathered */
	/*	0x000001 = raw WFS camera data
		0x000002 = centroid data
		0x000004 = subap intensities
		0x000008 = output of FTR
		0x000020 = output of FTR, tweeter
		0x000040 = output of FTR, woofer
		0x000100 = closed loop temporal power spectra
		0x000200 = derived open loop temporal power spectra
		0x000400 = modal gains
		0x000800 = actuator settings, tweeter
		0x001000 = actuator settings, woofer
		0x002000 = TT values (readings and mirror settings)
		0x004000 = M1/M2 offload data
		0x008000 = calculate and store statistics
		0x010000 = PSF data
		0x020000 = telescope pupil misalignment data
		0x040000 = Cal reported SNR and corresponding gain
		0x080000 = Cal reference centroid offsets
		0x100000 = current reference centroids
      */

	float  seconds;		/* Number of seconds of data to store		*/
        int    decimation;	/* decimation value: 1 means all data is stored, 
				2 means every other data field is stored, 
				10 means every 10th field is stored, etc. */
        char    filename[STR_LEN];/* Noop				*/
	int	stop;		/* Start/Stop temperature regulation.	*/
    } ;

/*
setEngConfiguration	A-Q		all loops open
*/
struct mtAocEnvConfig
{
	GPI_RPC_CMD_COMMON c;
        char    darkFileBaseName[STR_LEN];/* Base filename for current
					     pixel darks*/
        char    skyFileBaseName[STR_LEN];/* Base filename for current
					     pixel darks*/
        char    flatFileBaseName[STR_LEN];/* Base filename for current
					     pixel darks*/
        int    useFlatFields;		/*   0=No(all 1's), 1=yes */
        char    refCentFileBaseName[STR_LEN];/* Centroid Filename
					     pixel darks*/
/*//set ref cent - done right - REF_CENTROIDS */
        int    rate;		/* Rate					*/
        int    samples;		/* Samples				*/
        int    location;	/* Location				*/
        char    filename[STR_LEN];/* Noop				*/
	int	stop;		/* Start/Stop temperature regulation.	*/
    } ;

/* WARNING: check the ICD, think I am missing a cmd...
setEnvConfiguration	A-Q		all loops open
*/

/*
offloadOnOff	A,B (int)	anytime 
*/

struct mtAocM1M2OnOff
    {
	GPI_RPC_CMD_COMMON c;
	int	m1;		/* STOP=0 | START=1 | -1=NO CHANGE	*/
	int	m2;		/* STOP=0 | START=1 | -1=NO CHANGE	*/
    };


/*
setObservationConfiguration
r0Estimate		(double)	Optional (-1 means not provided)
starMag			(double)	Star I Mag
camRate			(int)		Camera Frame rate
darkSelect		(int) 		0 | 1 | 2
	    				0 = use neither (all zeros), 
					1 = use darks, 2 = use backs
refSelect		(int)		0x0, 1, 2, 4, 8, 10
	    				0x0 = use zeroes, no offsets
					0x01 = use base ref cents
					0x02 = use host-provided offsets
					0x04 = use Cal re-phase offsets
					0x08 = use most recent Cal offsets
					0x10 to 0x80 = use apodizer (PPM) 
						offsets 1 though 8
					
*/
struct mtAocObservConfig
    {
	GPI_RPC_CMD_COMMON c;
	double	r0Estimate; 		/* r0 Estimate.			*/
	double	starMag;		/* Star I Mag			*/
	int	camRate;		/* Camera Frame rate		*/
	int	darkSelect;		/* 0 = use neither (all zeros), 
					1 = use darks, 2 = use backs 	*/
	int	refSelect;		/* 0x0 = use zeroes, no offsets
					0x01 = use base ref cents
					0x02 = use host-provided offsets
					0x04 = use Cal re-phase offsets
					0x08 = use most recent Cal offsets
					0x10 to 0x80 = use apodizer (PPM) 
						offsets 1 though 8	*/
    };


/*
closeOpenOFC		A(int)			action to take for tweeter loop
*/
struct mtAocOptimize
    {
	GPI_RPC_CMD_COMMON c;
	char   choiceFilename[128];
	char   valueFilename[128];
	int    useOptimized;		/* Select optimizer.	*/
    };


/*
perform cmd, varType1-5, var1-5
*/
struct mtAocPerform
    {
	GPI_RPC_CMD_COMMON c;
	char	cmdName[STR_LEN];/* Name of func. to execute.		*/
	char	var1[STR_LEN];		/* Input variable.		*/
	char	var2[STR_LEN];		/* Input variable.		*/
	char	var3[STR_LEN];		/* Input variable.		*/
	char	var4[STR_LEN];		/* Input variable.		*/
	char	var5[STR_LEN];		/* Input variable.		*/
	char	var6[STR_LEN];		/* Input variable.		*/
	char	var7[STR_LEN];		/* Input variable.		*/
	char	var8[STR_LEN];		/* Input variable.		*/
	char	var9[STR_LEN];		/* Input variable.		*/
	char	var10[STR_LEN];		/* Input variable.		*/
    };

/*
receiveRefCents		A(filename)	all loops open
receiveRefCentsCal	A(filename)	anytime
receiveRefCentsMap	A(filename)	anytime
setRefCents		A(int)		anytime
*/
struct mtAocRecvSetRefCentroids
    {
	GPI_RPC_CMD_COMMON c;
	int    set;			/* Set(1) or receive(0) 	*/
	int    type;			/* Cal(0), Map(1), RefCent(2)	*/
	char   filename[128];
    } ;

/*
measureRefCents	       None		all loops open
measureRefOffsets      None		all loops open
offsetRefCents	       A,B (int)	anytime
*/

struct mtAocRefCentroids
    {
	GPI_RPC_CMD_COMMON c;
	int    measure;		/* measure, otherwise offset. */
	int    offset1;		/* offset1			*/
	int    offset2;		/* offset2 		*/
    } ;

/*
selectDarksBacks	A(int)		all loops open
selectFlatField		A(int)		all loops open
setCentThresh		A(int)		all loops open
*/
struct mtAocSetSettings
    {
	GPI_RPC_CMD_COMMON c;
	int darkBacks;	/* <0=ignore, else set darkBakcks	*/
	int flatField;	/* <0=ignore, else set flatField	*/
	int threshold;	/* <0=ignore, else set centroid threshold */
    } ;


/*
setShapes		A(int)		all loops open
setTweeterOffset	A(filename)	all loops open
setWooferOffset		A(filename)	all loops open
setTweeterShape		A(filename)	all control loops open
setWooferShape		A(filename)	all control loops open
storeShapes		A(int)		anytime
*/

struct mtAocShapes
    {
	GPI_RPC_CMD_COMMON c;
	int    set;			/* Set or store.		*/
	int    type;			/* tweeter, woofer, shapes	*/
	int    bits;			/* bits for store shapes only	*/
         			/* 0x01 = store tweeter laboratory flat */
				/* 0x02 = store tweeter latest stored flat */
				/* 0x04 = store tweeter latest stored shape */
				/* 0x10 = store woofer laboratory flat */
				/* 0x20 = store woofer latest flat */
				/* 0x40 = store woofer latest stored shape */
	char   filename[128];
    } ;

/*
Status 
*/
struct mtAocStat
    {
	GPI_RPC_CMD_COMMON c;
    } ;

/*
statsLog		A(int)		anytime
*/
struct mtAocStatistics
    {
	GPI_RPC_CMD_COMMON c;
	int    type;			/* TBD				*/
	char   filename[128];
    } ;

/*
storeApoRCentOffs	A(int)		all AO & CAL loops open
receiveApoRCentOffs     A(int) 
*/
struct mtAocStoreApoRefCent
    {
	GPI_RPC_CMD_COMMON c;
	int    store;			/* 1=store, 0=recieve		*/
	int    apodizer;		/* select apodizer (1-8)	*/
	char   filename[128];		/* filename			*/
    } ;


/*
save a single exposure	A(filename)
*/
struct mtAocTakeExp
    {
	GPI_RPC_CMD_COMMON c;
	int	intTime;	/* Integration Time in msec 
       				    3 (MCDS) | 4 (UTR).			*/
	int	location;	/* Destionation 1(FILE)|2(DHS).		*/
	char	filename[STR_LEN];/* Filename to save to .		*/
    };


/*
takePixelBacks		None		all control loops open
takePixelDarks		None		all loops open
takePixelFlats		None		all loops open
*/
struct mtAocTakePixels
    {
	GPI_RPC_CMD_COMMON c;
	int    type;		/* back=2, flat=1, dark=0 */
	char   filename[STR_LEN];   /* Not used!!!! */
    };
/*
setTT			A,B,C		all loops open
*/
struct mtAocTt
    {
	GPI_RPC_CMD_COMMON c;
	int	angles;		/* If set, then in angles, else voltages. */
	float	setTTXAxis;
	float	setTTYAxis;
    };

/*
makeInFuncGains	None	A(int),B(int),C(float) 	all loops open
receiveInFuncGains	A(filename)		all loops open
selectInFuncGains	A(int)			all loops open
*/
struct mtAocInFuncGains
    {
	GPI_RPC_CMD_COMMON c;
	int    action;		/* 0=make, 1=receive, 2=select	*/
	int    gainXPos;	/* X Mode position for make	*/
	int    gainYPos;	/* Y Mode position for make 	*/
	float  gainAmplitude;	/* Amplitued for make	 	*/
	int    select;		/* which gains to use		*/
	char   filename[128];	/* filename for receive		*/
    } ; 

/*
statsLog		A(int)			any time
*/
struct mtAocStatsLog
    {
        GPI_RPC_CMD_COMMON c;
	int statsLog;			/* logging on or off	*/
    } ;

/*
selectGainSource	A(int)		all loops open
setGainFactors		A(float)	all loops open
setGainSourceChoices	A(int)		all loops open
setGainValues		A(filename)	any time
*/
struct mtAocGains
    {
        GPI_RPC_CMD_COMMON c;
        int command;		/* 0=gainSrc|1=gainFactors|2=gainSrcChoices|3=gainVals */
	int source;		/* input parameter for gainSrc and gainSrcChoices */
	float factors[6];	/* gain maximums and tweak factors */
	char filename[128];
    } ;


/*
 *  This is what is called when a rpc request is initiated.
 */

union GP_AOC_REQUEST switch (enum GP_AOC_SRVR_CMDS command)
    {
        case MT_AOC_GLOBAL:
            mtAocGlobal mtGlobal_;
	case MT_AOC_CAL_ON_OFF:
            mtAocCalOnOff mtCalOnOff_;
	case MT_AOC_CAMERA_RATE:
            mtAocCameraRate mtCameraRate_;
        case MT_AOC_CLOSE_OPEN_LOOP:
	    mtAocCloseOpenLoop mtCloseOpenLoop_;
	case MT_AOC_CYCLE_ACTUATORS:
            mtAocCycleAct mtCycleAct_;
	case MT_AOC_DISPLAY:
            mtAocDisplay mtDisplay_;
	case MT_AOC_DUMP:
            mtAocDump mtDump_;
	case MT_AOC_ENV_CONFIG:
            mtAocEnvConfig mtEnvConfig_;
	case MT_AOC_M1_M2_ON_OFF:
            mtAocM1M2OnOff mtM1M2OnOff_;
	case MT_AOC_OBSERV_CONFIG:
            mtAocObservConfig mtObservConfig_;
	case MT_AOC_OPTIMIZE:
            mtAocOptimize mtOptimize_;
	case MT_AOC_PERFORM:
            mtAocPerform mtPerform_;
	case MT_AOC_RECV_SET_REF_CENT:
            mtAocRecvSetRefCentroids mtRecvSetRefCentroids_;
	case MT_AOC_REF_CENTROIDS:
            mtAocRefCentroids mtRefCentroids_;
	case MT_AOC_SET_SETTINGS:
            mtAocSetSettings mtSetSettings_;
	case MT_AOC_SHAPES:
            mtAocShapes mtShapes_;
	case MT_AOC_STAT:
            mtAocStat mtStat_;
	case MT_AOC_STATISTICS:
            mtAocStatistics mtStatistics_;
	case MT_AOC_STORE_APO:
            mtAocStoreApoRefCent mtStoreApoRefCent_;
	case MT_AOC_TAKE_EXP:
            mtAocTakeExp mtTakeExp_;
	case MT_AOC_TAKE_PIXELS:
            mtAocTakePixels mtTakePixels_;
	case MT_AOC_TT:
            mtAocTt mtTt_;
	case MT_AOC_IN_FUNC_GAINS:
            mtAocInFuncGains mtInFuncGains_;
	case MT_AOC_STATS_LOG:
            mtAocStatsLog mtStatsLog_;
	case MT_AOC_GAINS:
            mtAocGains mtGains_;
        default:
            void;  /* Error occurred, nothing to return. */
    } ;

/*
 *  This is what is used for the response from a request.
 */

struct GP_AOC_REPLY
    {
	GPI_RPC_REPLY_COMMON r;

	/* Required for GLOBAL */
	int datumState;
	int datumId;
	int datumAck;
	
	int initState;
	int initId;
	int initAck;
    } ;



