/************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2009                         (c) 2009
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                  
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
*/
/*!
 *
 *  @file gpAocMain.cpp
 *
 *  @brief 
 *  AOC Main entrance to the AOC RPC server.
 *
 *  @b DESCRIPTION:
 *  Main entry program.
 *
 *  @b AUTHOR:
 *  jsd DATE: 11/27/07
 *
 *  @b METHOD_NAME(S):
 *  @li @c main      - Main entrance to the command event handler.
 *
 ************************************************************************
*/

/*!
 * @dir src
 * This is the AOC RPC Server directory.
 * The AOC RPC Server accepts commands from the Instrument Sequencer.
 * It is the glue between the TLC and the AOC.
 *
 */

/*!
 *  Includes
 */

#include <iostream>
#include <map>
#include <list>

using namespace std;

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <rpc/rpc.h>
#include <rpc/pmap_clnt.h>

#include <gpUtGeneral.h>
#include <gpUtDebug.h>

#include <gpAocCustom.h>

/*!
 *  Prototypes
 */
int main( int, char * [] );

/*!
 ************************************************************************
 *
 * @fn int main(int , char *[]) 
 *
 * @brief
 *   Main program to run the AOC RPC Server
 *
 * @b Description: 
 *    Main entrance for the gpAocServer 
 *
 * @b Usage: 
 *  gpAocServer
 * 
 * @param[in] argc (int) The number of command line arguments.
 * @param[in] argv (char *[]) This list of command line arguments.
 *
 * @return 
 *    (int) 0=Success, !=0 =Fail.
 * 
 * @exception 
 *    None.
 *
 * @par @b PRIOR_REQUIREMENTS:
 *   None.
 * @callgraph
 ************************************************************************
-*/
int main 
( 
    int argc, 
    char* argv[]
)
{
    int status = GPI_ERR_NONE; // Status.

    try
    {
        /*
         * Parse cmd line. Init GMB and init variables. Start RPC server.
         */
        CHECKFNSTAT ( cAocSubserver::start(argc,argv), status, return(status));
    }
    catch ( int s )
    {
        //
        //  Handle error.
        //

        cout << "Critical Error occured starting AOC RPC Server"  << endl;
        return( -1 );
    }

    if ( status == 0  )
    {
        cout << "Exiting AOC RPC Server with Status ok"  << endl;
        return ( 0 );
    }
    else
    {
        cout << "ERROR, Exiting AOC RPC Server with Status not ok"  << endl;
        return ( -1 );
    }

} /* End of main() */
