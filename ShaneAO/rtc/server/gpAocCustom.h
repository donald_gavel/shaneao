/************************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2009                         (c) 2009
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                  
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
*/
/*!
 *
 *  @file gpAocCustom.h
 *
 *  @b DESCRIPTION:
 *  Contains include stuff for gpAocCustom.C
 *
 *  @b AUTHOR:
 *  jsd DATE: 8/31/06
 *
 *  @b CLASS_NAME(S):
 *  @li @c cAocSubserver             - main class
 *  @li @c cAocCmd_GLOBAL          - Init, index, etc.     
 *  @li @c cAocCmd_CAL_ON_OFF      - 
 *  @li @c cAocCmd_CAMERA_RATE     - 
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP - 
 *  @li @c cAocCmd_CYCLE_ACTUATORS - 
 *  @li @c cAocCmd_DISPLAY         - 
 *  @li @c cAocCmd_DUMP            - 
 *  @li @c cAocCmd_ENV_CONFIG      - 
 *  @li @c cAocCmd_M1_M2_ON_OFF    - 
 *  @li @c cAocCmd_OBSERV_CONFIG   - Set observation config
 *  @li @c cAocCmd_OPTIMIZE        - 
 *  @li @c cAocCmd_PERFORM         - Perform generic command. 
 *  @li @c cAocCmd_RECV_SET_REF_CENT - 
 *  @li @c cAocCmd_REF_CENTROIDS   - 
 *  @li @c cAocCmd_SET_SETTINGS    - 
 *  @li @c cAocCmd_SHAPES          - 
 *  @li @c cAocCmd_STAT            - Status return (for debugging)
 *  @li @c cAocCmd_STATISTICS      - 
 *  @li @c cAocCmd_STORE_APO       - 
 *  @li @c cAocCmd_TAKE_EXP        - Take exposure.
 *  @li @c cAocCmd_TAKE_PIXELS     - 
 *  @li @c cAocCmd_TT              - 
 *  @li @c cAocCmd_IN_FUNC_GAINS   - 
 *  @li @c cAocCmd_STATS_LOG       - 
 *  @li @c cAocCmd_GAINS           - 
 *
 ************************************************************************
*/

#ifndef TLC_AOC_CMDS_H
#define TLC_AOC_CMDS_H

/*
 * Defines
 */

/*
 * This is maximum camera rate
 * WARNING, set to a real value when we know what that is.
 */
#define MAX_CAM_RATE 4000

/*
 *  Cal Receive Centroid types.
 */
#define CAL_REPHASE_REF_CENT 	0
#define PHASE_MAP_REF_CENT	1
#define FUNC_GAINS_REF_CENT	-1
#define RECEIVED_REF_CENT	2

/*
 *  AOC Gains Choices.
 */
#define SELECT_GAIN_SOURCE	0
#define SET_GAIN_FACTORS	1
#define SET_GAIN_SRC_CHOICES	2
#define SET_GAIN_VALUES		3

/*
 *  AOC In Function gain actions
 */
#define MAKE_IN_FUNC_GAINS	0
#define RECV_IN_FUNC_GAINS	1
#define SELECT_IN_FUNC_GAINS	2

#define USE_ALL_ONES		0
#define RECV_FROM_HOST		2

#define MIN_GAIN_MODE_POSITION	0
#define MAX_GAIN_MODE_POSITION  31
/*
 *  AOC Command Strings.
 */

/*
 *  AOC Server Cmd structure
 */
/*
typedef struct
{
    char cmdStr[GPI_MAX_STRING_LEN];
    char format[GPI_MAX_STRING_LEN];
} GP_AO_CMD_LAYOUT;


static GP_AO_CMD_LAYOUT ao[] = 
{
	{ 
*/

#define	 AOCCMDFMT_QUIT	"quit"
	//, {eCmdParamNone,   eCmdParamNone,   eCmdParamNone  
#define  AOCCMDFMT_RESTART	"restart"
	//, {eCmdParamNone,   eCmdParamNone,   eCmdParamNone  
#define  AOCCMDFMT_SHUTDOWN	"shutdown"	
	//, {eCmdParamNone,   eCmdParamNone,   eCmdParamNone  
#define  AOCCMDFMT_SHUTDOWN	"shutdown"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  
#define  AOCCMDFMT_SETDEBUG	"setDebug %s"
	//{eCmdParamString,   eCmdParamNone,   eCmdParamNone  
#define AOCCMDFMT_DUMPDATA	"dumpData %d %f %d"
	//, {eCmdParamInt,    eCmdParamFloat,  eCmdParamInt
#define AOCCMDFMT_GLOBALINIT "globalInit"	
	// {eCmdParamNone,   eCmdParamNone,   eCmdParamNone 
#define AOCCMDFMT_GLOBALINDEX "globalIndex"
	//eCmdParamNone,   eCmdParamNone,   eCmdParamNone
#define AOCCMDFMT_GLOBALPARK "globalPark"
	//eCmdParamNone,   eCmdParamNone,   eCmdParamNone  
#define AOCCMDFMT_GLOBALREBOOT "globalReboot"
	//eCmdParamNone,   eCmdParamNone,   eCmdParamNone 
#define AOCCMDFMT_IMAGES       "images"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_DATA  "data"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TELEM "telem"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TRATE "trate %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_INITWFSCAM "initWfsCam"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_AUTOCAMRATE "autoCamRate %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETCAMRATE "setCamRate %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_CAMGAIN "camGain %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_GETCAMRATE "getCamRate"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_GETCAMGAIN "getCamGain"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_RECEIVEREFCENTS "receiveRefCents %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_RECEIVEREFCENTSCAL "receiveRefCentsCal %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_RECEIVEREFCENTSMAP "receiveRefCentsMap %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETREFCENTS "setRefCents %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_OFFSETREFCENTS "offsetRefCents %d %d"
	//{eCmdParamInt,    eCmdParamInt,    eCmdParamNone  }
#define AOCCMDFMT_RECEIVEAPORCENTOFFS "receiveApoRCentOffs %d %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_STOREAPORCENTOFFS "storeApoRCentOffs %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_MEASUREREFOFFSETS "measureRefOffsets"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_MEASUREREFCENTS "measureRefCents"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TAKEPIXELBACKS "takePixelBacks"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TAKEPIXELDARKS "takePixelDarks"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TAKEPIXELFLATS "takePixelFlats"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SELECTDARKBACKS "selectDarksBacks %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SELECTFLATFIELD "selectFlatField %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETCENTTHRESH "setCentThresh %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SM_TWT "sm_twt"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_CLOSEOPENLOOPS "closeOpenLoops %d %d %d"
	//{eCmdParamInt,    eCmdParamInt,    eCmdParamInt   }
#define AOCCMDFMT_CLOSEOPENOFC "closeOpenOFC %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_OFFLOADONOFF "offloadOnOff %d %d"
	//{eCmdParamInt,    eCmdParamInt,    eCmdParamNone  }
#define AOCCMDFMT_TGAIN "tGain %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TINT "tInt %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TCWT1 "tCwt1 %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TCWT2 "tCwt2 %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_WGAIN "wGain %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_WINT "wInt %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_WCWT1 "wCwt1 %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_WCWT2 "wCwt2 %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETTWEETERSHAPE "setTweeterShape %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETWOOFERSHAPE "setWooferShape %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETTWEETEROFFSET "setTweeterOffset %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETWOOFEROFFSET "setWooferOffset %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_STOREMODALGAINS "storeModalGains %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETGAINVALUES "setGainValues %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SELECTGAINSRC "selectGainSrc %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETGAINSRCCHOICES "setGainSrcChoices %s"
	//{eCmdParamStrng,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETSHAPES "setShapes %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_STORESHAPES "storeShapes %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_MAKEINFUNCGAINS "makeInFuncGains %d %d %f"
	//{eCmdParamInt,    eCmdParamInt,   eCmdParamFloat  }
#define AOCCMDFMT_SELECTINFUNCGAINS "selectInFuncGains %d"
	//{eCmdParamInt,    eCmdParamNone,  eCmdParamNone   }
#define AOCCMDFMT_CYCLEACTUATOR "cycleActuator %d"
	//{eCmdParamInt,    eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_STOPCYCLING "stopCycling"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TTGAIN "ttgain %f"
	//{eCmdParamFloat,  eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_SETTT "setTT %d %f %f"
	//{eCmdParamFloat,  eCmdParamFloat,  eCmdParamInt   }
#define AOCCMDFMT_ABORT "abort"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_READCONFIG "readConfig"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TESTN "testn"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_TESTI "testi %d %d %d"
	//{eCmdParamInt,    eCmdParamInt,    eCmdParamInt   }
#define AOCCMDFMT_TESTF "testf %f %f %f"
	//{eCmdParamFloat,  eCmdParamFloat,  eCmdParamFloat }
#define AOCCMDFMT_TESTX  "tests %s %s %s"
	//{eCmdParamStrng,  eCmdParamStrng,  eCmdParamStrng }
#define AOCCMDFMT_HELP "help"
	//{eCmdParamNone,   eCmdParamNone,   eCmdParamNone  }
#define AOCCMDFMT_CALDATAONOFF "calDataOnOff %d"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_RECEIVEINFUNCGAINS "receiveInFuncGains %s"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_SELECTGAINSOURCE "selectGainSource %d"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_SETENGCONFIGURATION "setEngConfiguration"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_SETGAINFACTORS "setGainFactors %f"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_SETOBSERVATIONCONFIGURATION "setObservationConfiguration"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_SETGAINSOURCECHOICES "setGainSourceChoices %d"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }
#define AOCCMDFMT_STATSLOG "statsLog %d"
	//{eCmdParamInt,    eCmdParamNone,    eCmdParamNone  }


/*!
 * Includes
 */

#include <string.h>
#include <stdlib.h>
#include <pthread.h>

#include "gpSubserver.h"
#include "gpAocRpc.h"

/*!
 * Prototypes
 */

/*!
 * Function Prototype
 */

/* 
 *  @b Detailed_Description
 *  aocSubServer()
 *  Function called when an RPC Command is received.
 *          Create a custom class ptr.  It clears the reply structure
 *  Get the RPC request.  
 *  and this isn't an init or reboot command then we need
 *  to error out.
 *  Now react to the request.
 *
 * @param[in] rqstp (struct svc_req  *) Xdr routine to decode args
 * @param[in] xprt (SVCXPRT *) RPC transport handle.
 *
 * @return : void
 */
void aocSubServer ( struct svc_req * rqstp, SVCXPRT * xprt);

/*!
 ************************************************************************
 *
 * @class cAocSubserver
 *
 * @brief
 *   Main AOC Server Command Event class 
 *
 * @b Public_Methods:
 * @li @c cAocSubserver   - Constructor
 * @li @c ~cAocSubserver  - Virtual Deconstructor.
 * @li @c clearRpcReplyStructure- Clear the reply structure.
 * @li @c decodeLineFunc- Function to decode each line in config file
 * @li @c getRequest    - Get the command request information
 * @li @c handleRequest - Handle the command request.
 * @li @c parseCmdLine  - Parse the command line.
 * @li @c readConfig    - Read the configuration file.
 * @li @c setRpcReplyStructure  - Set the reply structure.
 *
 * Inline functions:
 * @li @c commandPtr    - Return the ptr to the command structure.
 * @li @c replyStat - Return the reply status.
 * @li @c replyPtr  - Return the ptr to reply structure.
 *  
 * @par @b Public_Data:
 *   cSubserverBase contains virtual function that this class must implement
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 *   cSubCommands   cmdPtr  - Command Ptr.
 *   int        cmdNum  - Saving command number
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocSubserver : public cSubserverBase
{
public:

    /*!
     * Max IP String length
     */
    #ifndef IPMAX_STR_LEN
    #define IP_MAX_STR_LEN 16
    #endif

    /*!
     * IP Address of AOC 
     */
    static char aocIpStr[ IP_MAX_STR_LEN ];

    /*
     *  AOC Port number.
     */

    static int aocPortNum;

    /**
     *  @b Detailed_Description
     *  cAocSubserver class constructor 
     *  This will get called when the AOC RPC Server first starts ONLY.
     *  So it does the one-of things, like:
     *    connect to GMB or shared memory (depending on how we are running)
     *    clear the reboot flag, and the GMB area
     *    not sure why, but clearning the rpc reply structure
     *
     * @param : void 
     *
     * @return : N/A 
     */
    inline cAocSubserver( void ) { };

    /**
     *  @b Detailed_Description
     *  cAocSubserver class constructor 
     *  This will be called each time a command is received.
     *  It will create the reply structure that will be returned and
     *  will store the reply information 
     *
     * @param[in] ptr (cSubCmdResult *) : Pointer to result structure
     *
     * @return : N/A 
     */
    cAocSubserver( cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *  ~cAocSubserver(): 
     *  This INLINE virtual deconstructor is here to stop
     *   warnings showing up in the newer version of g++.
     *
     * @param : void 
     *
     * @return : N/A 
     */
    virtual ~cAocSubserver( void ) { }

    /**
     *  @b Detailed_Description
     *  clearRpcReplyStructure()
     *  Clear the rpc reply structure
     *
     * @param : void
     *
     * @return : void
     */
    void clearRpcReplyStructure( void );

    /**
     *  @b Detailed_Description
     *   decodeLineFunc():
     *   Calling function gpUtReadConfigFile() passes the
     *   data type, field name and value for evaluation where:
     *      Field  == TBD
     *      value  == value of the field
     *   Nothing is read for now.
     *
     * @param[in] keyword (char *) The Parameter keyword. 
     * @param[in] tokens (char **) The rest of the line in tokens.
     * @param[in] numWords ( int ) Number of tokens in the line.
     * @param[in] classPtr (void *) Pointer to class with information.
     * @param[out] status (int *) Return status
     *
     * @return : ( int )
     */
    static int decodeLineFunc( char *, char **, int, void * const, int* );

    /**
     *  @b Detailed_Description
     *  getRequest():
     *  Get the RPC command request. 
     *  svc_getargs decodes arguments of the RPC request associated
     *  with RPC transport handle (xprt).
     *  Uses the function xdr_GP_CAL_ASS_REPLY (in the 
     *  generated .c file) to decode the arguments passed in.
     *  Decoded args are placed in request structure.
     *
     * @param[in] xprt ( * SVCXPRT) RPC transport handle. 
     *
     * @return : ( int ) Success or failure.
     */
    int getRequest( SVCXPRT * );

    /**
     *  @b Detailed_Description
     *  handleRequest():
     *  Save the command number.
     *  Based on the command - create the template class
     *  that will handle it (eg, for a "global" command, then 
     *  cAocCmd_GLOBAL will be created
     *  Check that we did get a valid command, then
     *  Check the dependencies (checkDependencies()) and makeItBusy()
     *  if it was successful, otherwise makeItUnBusy()
     *
     * @param : void
     *
     * @return : ( int ) Success or failure.
     */
    int handleRequest( void );

    /**
     *  @b Detailed_Description
     *  start():
     *  Parses the cmd line parameters. Reads config file.
     *  connect to GMB or shared memory (depending on how we are running)
     *  clear the reboot flag, and the GMB area
     *  If using AO code, then connect to aoc socket.
     *  Starts the RPC server.
     *
     * @param[in] argc ( int ) Number of arguments
     * @param[in] argv (char**)  Input arguments
     *
     * @return : ( int )
     */
    static int start( int argc, char * argv[] );

    /**
     *  @b Detailed_Description
     *  parseCmdLine:
     *  Parse the command line.   Expect nothing for now.
     *  But can take in a help flag, a debug flag, and a configuration
     *  filename.
     *
     * @param[in] argc ( int ) Number of arguments
     * @param[in] argv (char**)  Input arguments
     *
     * @return : ( int ) Success or failure.
     */
    static int parseCmdLine( int argc, char * argv[] );

    /**
     *  @b Detailed_Description
     *  readConfig():
     *  Read the config file, if the strlen > 0
     *  For now, there should be no config file to read
     *
     * @param[in] configName (char *) Configuration file name.
     *
     * @return : ( int ) Success or failure.
     */
    static int readConfig( char * );

    /**
     *  @b Detailed_Description
     *  setRpcReplyStructure()
     *  Set the rpc reply structure
     *
     * @param[in] status ( int ) function status
     *
     * @return : void
     */
    void setRpcReplyStructure( int );

    /**
     *  @b Detailed_Description
     *  replyPtr():
     *  Returns the reply structure.
     *
     * @param : void
     *
     * @return : (GP_AOC_REPLY *) Pointer to reply structure
     */
    inline GP_AOC_REPLY * replyPtr( void ) { return(&reply); };

private:

    /*!
     *  This is the request structure that is specific to the AOC commands.
     */
   GP_AOC_REQUEST request;

    /*!
     *  This is the reply structure that is specific to the AOC commands.
     */
    GP_AOC_REPLY reply;

}; /* End of cAocSubserver() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_GLOBAL
 *
 * @brief
 *   Handles the Global Command, implements virtual methods from cSubCommands
 *
 * @b Public_Methods:
 * @li @c cAocCmd_GLOBAL    - Constructor
 * @li @c canBeDoneNow      - can the command be done now.
 * @li @c checkDependencies - check Dependencies
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute           - Clear the rpc reply structure
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c executeInit       - Execute an init command.
 * @li @c executeDatum      - Execute an Datum command.
 * @li @c executePark       - Execute an Park command.
 * @li @c executeReboot     - Execute a reboot command.
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_GLOBAL: public cSubCommands
{
public:

    /*
     *  These are the input attributes for the GLOBAL command.
     *  They are stored locally.
     *  level: is simulation or debug level
     *  mode: this is the type of global command: init, index, debug, park,
     *        sim, test, reboot.
     *  stop: indicates if we are to stop this command.
     */
    int mode;
    int level;
    int stop;

    /**
     *  @b Detailed_Description
     *  cAocCmd_GLOBAL class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_GLOBAL(GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );
    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  Overrides a virtual function
     *  This command checks to see if the command can be completed.
     *  For all commands the aoc_observationInProgress, aoc_ExposureInProgress 
     *  and aoc_globalBusy must not be set.
     *  For a Debug cmd: check to make sure it is within limits
     *  For a Sim cmd: check that level is within limits
     *  Otherwise, good to go.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  Overrides a virtual function
     *  This command checks to see if the command is done.
     *  Increment the counter - can do this a maximum of MAX_WAIT_TIME
     *
     * @param[in]
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  Overrides a virtual function
     *  This command does the execution of the command.
     *  Depending on the mode execute the command.
     *  For init, index, and reboot call the appropriate function.
     *  Park is noop
     *  Change debug or simulate level in this function.
     *  Set counter to 1.  This counts number of times trying to 
     *  execute this command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail 
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  executeInit():
     *  Overrides a virtual function
     *  This command performs an init command.
     *  Execute the following:
     *    aoc_pressure_startPolling
     *    aoc_temperature_startPolling
     *    aoc_pressure_startLogging
     *    aoc_temperature_startLogging 
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int executeInit( void );

    /**
     *  @b Detailed_Description
     *  executeDatum():
     *  Overrides a virtual function
     *  This command performs an index command.
     *  Execute the following:
     *  aoc_pressure_startPolling
     *  aoc_temperature_startPolling
     *  aoc_pressure_startLogging
     *  aoc_temperature_startLogging 
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int executeDatum( void );

    /**
     *  @b Detailed_Description
     *  executePark():
     *  Overrides a virtual function
     *  This command performs a park command.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int executePark( void );


    /**
     *  @b Detailed_Description
     *  executeReboot():
     *  Overrides a virtual function
     *  This command performs an Reboot command.
     *  Execute the following:
     *  aoc_observation_abortExposure
     *  aoc_pupilviewing_abortExposure
     *  aoc_pressure_stopPolling
     *  aoc_temperature_stopPolling
     *  aoc_pressure_stopLogging
     *  aoc_temperature_stopLogging
     *  aoc_temperature_stopTemperatureRegulation
     *  kill the rpc server
     *
     * @param : void 
     *
     * @return : void 
     */
    void executeReboot( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  Overrides a virtual function
     *  This command does what it needs to make a command appear busy.
     *  Set aoc_global_status to BUSY,
     *  Clear aoc_global_errMsg
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  Overrides a virtual function
     *  Set aoc_global_status to ERR or IDLE, depending on state of cmd.
     *  Fill  aoc_global_errMsg, if required.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

}; /* end of cAocCmd_GLOBAL() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_CAL_ON_OFF
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_CAL_ON_OFF - Constructor
 * @li @c canBeDoneNow       - Can the cmd be done now
 * @li @c checkDependencies  - check Dependencies
 * @li @c checkIfDone        - Check if the command is complete
 * @li @c makeItBusy         - Make the command busy
 * @li @c makeItUnBusy       - Make the command Unbusy
 * @li @c execute            - Clear the rpc reply structure
 *  
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY reply     - Reply structure.
 * 
 * @callgraph
 ************************************************************************
-*/

class cAocCmd_CAL_ON_OFF: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_CAL_ON_OFF class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_CAL_ON_OFF(GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );
    
    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );
   
    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

    /*
     *  Set on or off.
     */
    int on;
   
private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_CAL_ON_OFF() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_CAMERA_RATE
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_CAMERA_RATE - Constructor
 * @li @c canBeDoneNow        - Can the cmd be done now
 * @li @c checkDependencies   - check Dependencies
 * @li @c checkIfDone         - Check if the command is complete
 * @li @c makeItBusy          - Make the command busy
 * @li @c makeItUnBusy        - Make the command Unbusy
 * @li @c execute             - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_CAMERA_RATE: public cSubCommands
{
public:

    /*!
     *  Request information stored locally:
     *  autoCamRate: Set camera rate to automatic or manual.
     *  setCamRate:  The actual rate
     */
    int autoCamRate;
    int setCamRate;

    /**
     *  @b Detailed_Description
     *  cAocCmd_CAMERA_RATE class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_CAMERA_RATE(GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_CAMERA_RATE() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_CLOSE_OPEN_LOOP
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_CLOSE_OPEN_LOOP       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_CLOSE_OPEN_LOOP: public cSubCommands
{
public:

    /*
     */
    int tweeter;
    int wooffer;
    int tipTilt;
    int ofc;


    /**
     *  @b Detailed_Description
     *  cAocCmd_CLOSE_OPEN_LOOP class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_CLOSE_OPEN_LOOP( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : int 
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_CLOSE_OPEN_LOOP() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_CYCLE_ACTUATORS
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_CYCLE_ACTUATORS       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_CYCLE_ACTUATORS: public cSubCommands
{
public:

    /*
     *  actuator: -1: Round robin through all actuators
     *  0 to 4095: (tweeter actuator),
     *  5000 to 5121: (woofer actuator),
     *  6000 to 6001 (TT axis)
     */
    int actuator;


    /**
     *  @b Detailed_Description
     *  cAocCmd_CYCLE_ACTUATORS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_CYCLE_ACTUATORS( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_CYCLE_ACTUATORS() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_DISPLAY
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_DISPLAY       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_DISPLAY: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_DISPLAY class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     * @param[in] cmdStatics (GP_STATIC_CMD_VARS *) : Pointer to statics
     *
     * @return : N/A 
     */
    cAocCmd_DISPLAY( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : int
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_DISPLAY() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_DUMP
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_DUMP      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_DUMP: public cSubCommands
{
public:

    /*
     *  Local data to store from the request.
     */
    int type;        /* Type, bit indicates what diag data is gathered */
    float seconds;     /* Number of seconds of data to store       */
    int decimation;  /* decimation value: 1 means all data is stored, 
                        2 means every other data field is stored, 
                        10 means every 10th field is stored, etc. */
   
    char filename[GPI_MAX_STRING_LEN]; 

    /**
     *  @b Detailed_Description
     *  cAocCmd_DUMP class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A
     */
    cAocCmd_DUMP( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_DUMP() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_ENV_CONFIG
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_ENV_CONFIG        - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_ENV_CONFIG: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_ENV_CONFIG class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_ENV_CONFIG( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_ENV_CONFIG() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_M1_M2_ON_OFF
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_M1_M2_ON_OFF      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_M1_M2_ON_OFF: public cSubCommands
{
public:

    /*
     *  m1: 0=stop, 1=start
     *  This will start or stop the loop to offload zernikes.
     */

     int m1;

    /*
     *  m12 0=stop, 1=start
     *  This will start or stop the loop to offload m2 data
     *  to the syncrobus by AOC
     */

     int m2;

    /**
     *  @b Detailed_Description
     *  cAocCmd_M1_M2_ON_OFF class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_M1_M2_ON_OFF( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_M1_M2_ON_OFF() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_OBSERV_CONFIG
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_OBSERV_CONFIG     - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_OBSERV_CONFIG: public cSubCommands
{
public:

   /*
    *  Local variables.
    */
    double magnitudeI;  /* I Magnitude estimate.        */
    double r0Estimate;  /* r0Estimate, -1 means none.       */
    int useLastVals;    /* start with last AO vals when closing */
    int camRate;    /* Camera Rate              */
    int darkSelect;     /* 0 = use neither (all zeros), 
                1 = use darks, 2 = use backs    */
    int refSelect;      /* 0x0 = use zeroes, no offsets
                0x01 = use base ref cents
                0x02 = use host-provided offsets
                0x04 = use Cal re-phase offsets
                0x08 = use most recent Cal offsets
                0x10 to 0x80 = use apodizer (PPM) 
                    offsets 1 though 8  */

    /**
     *  @b Detailed_Description
     *  cAocCmd_OBSERV_CONFIG class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_OBSERV_CONFIG( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_OBSERV_CONFIG() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_OPTIMIZE
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_OPTIMIZE      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_OPTIMIZE: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_OPTIMIZE class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A
     */
    cAocCmd_OPTIMIZE( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

    /*
     * Local variables:
     * choiceFilename char[128];    Optimize choices filename
     * valueFilename  char[128];    Optimize values  filename
     * useOptimized   int ;     1=Select optimizer, otherwise not.  
     */
    char choiceFilename[GPI_MAX_STRING_LEN];
    char valueFilename[GPI_MAX_STRING_LEN];
    int useOptimized;        /* Select optimizer.    */

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_OPTIMIZE() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_PERFORM
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_PERFORM       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_PERFORM: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_PERFORM class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A
     */
    cAocCmd_PERFORM( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_PERFORM() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_RECV_SET_REF_CENT
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_RECV_SET_REF_CENT     - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_RECV_SET_REF_CENT: public cSubCommands
{
public:

    /*
     *  Set or receive ref centroids.
     */
    int    set;

    /*
     * Cal, Map, etc...
     */
	int    type;

    /*
     *  Filename to save in.
     */

    char   filename[GPI_MAX_STRING_LEN];

    /**
     *  @b Detailed_Description
     *  cAocCmd_RECV_SET_REF_CENT class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A
     */
    cAocCmd_RECV_SET_REF_CENT( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_RECV_SET_REF_CENT() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_REF_CENTROIDS
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_REF_CENTROIDS     - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_REF_CENTROIDS: public cSubCommands
{
public:

    /*
     *  measure, otherwise offset
     */
    int measure;

    /*
     * offset1
     */
    int offset1;
    

    /*
     * offset2
     */
    int offset2;
    

    /**
     *  @b Detailed_Description
     *  cAocCmd_REF_CENTROIDS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_REF_CENTROIDS( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return: (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_REF_CENTROIDS() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_SET_SETTINGS
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_SET_SETTINGS      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_SET_SETTINGS: public cSubCommands
{
public:

    /*
     *  selection
     */

    int darkBacks;
    int flatField;
    int threshold;

    /**
     *  @b Detailed_Description
     *  cAocCmd_SET_SETTINGS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_SET_SETTINGS( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );


    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_SET_SETTINGS() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_SHAPES
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_SHAPES        - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_SHAPES: public cSubCommands
{
public:
/*
setShapes		A(int)		all loops open
setTweeterOffset	A(filename)	all loops open
setWooferOffset		A(filename)	all loops open
setTweeterShape		A(filename)	all control loops open
setWoofferShape		A(filename)	all control loops open
storeShapes		A(int)		anytime
*/
    /*
     *  Set or store
     */
    int    set;

    int    type;
    
    /*
     * bits for storeShapes and setShapes
     */
    int bits;

    /*
     *  Filename to save in.
     */

    char   filename[GPI_MAX_STRING_LEN];


    /**
     *  @b Detailed_Description
     *  cAocCmd_SHAPES class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     * @param[in] cmdStatics (GP_STATIC_CMD_VARS *) : Pointer to statics
     *
     * @return : void
     */
    cAocCmd_SHAPES( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_SHAPES() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_STAT
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_STAT      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_STAT: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_STAT class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_STAT( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_STAT() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_STATISTICS
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_STATISTICS        - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_STATISTICS: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_STATISTICS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_STATISTICS( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_STATISTICS() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_STORE_APO
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_STORE_APO     - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_STORE_APO: public cSubCommands
{
public:

    /*
     * TBD.
     */
    int    store;
    int    apodizer;
    char   filename[GPI_MAX_STRING_LEN]; 


    /**
     *  @b Detailed_Description
     *  cAocCmd_STORE_APO class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_STORE_APO( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );
    
    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_STORE_APO() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_TAKE_EXP
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_TAKE_EXP      - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_TAKE_EXP: public cSubCommands
{
public:

    /**
     *  @b Detailed_Description
     *  cAocCmd_TAKE_EXP class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_TAKE_EXP( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param: void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_TAKE_EXP() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_TAKE_PIXELS
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_TAKE_PIXELS       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_TAKE_PIXELS: public cSubCommands
{
public:

    /*
     * type: DARK, FLAT or BACK
     */
    int type;


    /**
     *  @b Detailed_Description
     *  cAocCmd_TAKE_PIXELS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_TAKE_PIXELS( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param: void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_TAKE_PIXELS() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_TT
 *
 * @brief
 *   Handles the setting of configuration parameters.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_TT        - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_TT: public cSubCommands
{
public:

    /*
     *  
     */

    int		angles;		/* If set, then in angles, else voltages. */
    float	setTTXAxis;
    float	setTTYAxis;

    /**
     *  @b Detailed_Description
     *  cAocCmd_TT class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : void
     */
    cAocCmd_TT( GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdResult * ); 

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void
     *
     * @return : (int) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : (int) Success=0, Fail!
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void
     *
     * @return : (int) Success=GPI_ERR_NONE, else fail
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : (int) Success=0, Fail!
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_TT() */


/*!
 ************************************************************************
 *
 * @class cAocCmd_IN_FUNC_GAINS
 *
 * @brief
 *	Handle all *InFuncGains commands.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_IN_FUNC_GAINS       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_IN_FUNC_GAINS: public cSubCommands
{
public:

    /*
     */
    int    action;		/* 0=make, 1=receive, 2=select	*/
    int    gainXPos;		/* X Mode position for make	*/
    int    gainYPos;		/* Y Mode position for make 	*/
    float  gainAmplitude;	/* Amplitued for make	 	*/
    int    select;		/* which gains to use		*/
    char   filename[GPI_MAX_STRING_LEN];	/* filename for receive		*/

    /**
     *  @b Detailed_Description
     *  cAocCmd_IN_FUNC_GAINS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_IN_FUNC_GAINS( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : int 
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_IN_FUNC_GAINS() */


/*!
 ************************************************************************
 *
 * @class cAocCmd_STATS_LOG
 *
 * @brief
 *	Handle all *InFuncGains commands.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_STATS_LOG       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_STATS_LOG: public cSubCommands
{
public:

    /*
     */
    int    statsLog;		/* turn statistics logging on or off */

    /**
     *  @b Detailed_Description
     *  cAocCmd_STATS_LOG class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_STATS_LOG( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : int 
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_STATS_LOG() */

/*!
 ************************************************************************
 *
 * @class cAocCmd_GAINS
 *
 * @brief
 *	Handle all *InFuncGains commands.
 *
 * @b Public_Methods:
 * @li @c cAocCmd_GAINS       - Constructor
 * @li @c canBeDoneNow      - Can the cmd be done now
 * @li @c checkDependencies - check Dependencies
 * @li @c checkIfDone       - Check if the command is complete
 * @li @c makeItBusy        - Make the command busy
 * @li @c makeItUnBusy      - Make the command Unbusy
 * @li @c execute       - Clear the rpc reply structure
 *  
 * @par @b Public_Data:
 *   None.
 * @par @b Private_Data:
 *   GP_AOC_REQUEST request - Request structure.
 *   GP_AOC_REPLY   reply   - Reply structure.
 * .
 * 
 * @callgraph
 ************************************************************************
-*/
class cAocCmd_GAINS: public cSubCommands
{
public:

    /*
     */
    int    command;	/*0=gainSrc|1=gainFactors|2=gainSrcChoices|3=gainVals*/
    int    source;      /* input parameter for gainSrc and gainSrcChoices */
    float  factors[6];  /* gain maximums and tweak factors */
    char   filename[GPI_MAX_STRING_LEN];

    /**
     *  @b Detailed_Description
     *  cAocCmd_GAINS class constructor 
     *  This will store the reply and request structure.
     *
     * @param[in] rqst (GP_AOC_REQUEST *) Request structure
     * @param[in] rply (GP_AOC_REPLY *) Reply structure
     * @param[in] result (cSubCmdResult *) Result structure
     *
     * @return : N/A 
     */
    cAocCmd_GAINS( GP_AOC_REQUEST *, GP_AOC_REPLY *, 
        cSubCmdResult * );

    /**
     *  @b Detailed_Description
     *   canBeDoneNow():
     *   If the mode is Debug, Sim, or Park, then can be completed immed.
     *   otherwise it cannot.
     *
     * @param : void 
     *
     * @return : ( int ) 0=Can NOT be completed now, 1=Can be completed now.
     */
    int canBeDoneNow( void );

    /**
     *  @b Detailed_Description
     *  checkDependencies():
     *  This command checks to see if the command can be completed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkDependencies( void );

    /**
     *  @b Detailed_Description
     *  checkIfDone():
     *  This command checks to see if the command is done.
     *  This custom function knows what GMB val to look for to determine if
     *  this command is done, and whether or not it was successful.
     *
     * @param[in] numTimes ( int ) Number of times this can been called.
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int checkIfDone( int );

    /**
     *  @b Detailed_Description
     *  execute():
     *  This command does the execution of the command.
     *
     * @param : void 
     *
     * @return : int 
     */
    int execute( void );

    /**
     *  @b Detailed_Description
     *  makeItBusy():
     *  This command does what it needs to make a command appear busy.
     *
     * @param : void 
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItBusy( void );

    /**
     *  @b Detailed_Description
     *  makeItUnBusy():
     *  This command does what it needs to make a command not be busy anymore.
     *  This would occur if we tried to start a command by making it appear
     *  busy, then tried to execute the command it that failed.
     *
     * @param : void
     *
     * @return : ( int ) Success=0, Fail!=0
     */
    int makeItUnBusy( void );

private:

    GP_AOC_REPLY * reply;
    GP_AOC_REQUEST * request;
    int counter;

}; /* end of cAocCmd_GAINS() */



#endif /* TLC_AOC_CMDS_H */
