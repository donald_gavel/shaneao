/***********************************************************************
 ****      D A O   I N S T R U M E N T A T I O N   G R O U P        *****
 *
 * (c) 2009                         (c) 2009
 * National Research Council        Conseil national de recherches
 * Ottawa, Canada, K1A 0R6          Ottawa, Canada, K1A 0R6
 * All rights reserved              Tous droits reserves
 *                  
 * NRC disclaims any warranties,    Le CNRC denie toute garantie
 * expressed, implied, or statu-    enoncee, implicite ou legale,
 * tory, of any kind with respect   de quelque nature que se soit,
 * to the software, including       concernant le logiciel, y com-
 * without limitation any war-      pris sans restriction toute
 * ranty of merchantability or      garantie de valeur marchande
 * fitness for a particular pur-    ou de pertinence pour un usage
 * pose.  NRC shall not be liable   particulier.  Le CNRC ne
 * in any event for any damages,    pourra en aucun cas etre tenu
 * whether direct or indirect,      responsable de tout dommage,
 * special or general, consequen-   direct ou indirect, particul-
 * tial or incidental, arising      ier ou general, accessoire ou
 * from the use of the software.    fortuit, resultant de l'utili-
 *                                  sation du logiciel.
 *
 ************************************************************************
*/
/*!
 *
 *  @file gpAocCustom.cpp
 *
 *  @brief 
 *  Custom class to handle the AOC commands.
 *
 *  @b DESCRIPTION:
 *  Contains the classes & methods to handle all functions for a custom 
 *  sub system : AOC
 *
 *  @b AUTHOR:
 *  jsd DATE: 10/31/06
 *
 *  @b METHOD_NAME(S):
 *  @li @c cAocSubserver::cAocSubserver             - Constructor 
 *  @li @c cAocSubserver::clearRpcReplyStructure  - Clear the RPC reply 
 *                                                  structure
 *  @li @c cAocSubserver::decodeLineFunc  - Decode line function.
 *  @li @c cAocSubserver::getRequest      - Get the command request.
 *  @li @c cAocSubserver::handleRequest   - Handle the command request.
 *  @li @c cAocSubserver::parseCmdLine    - Parse the command line
 *  @li @c cAocSubserver::readConfig      - Read the config file.
 *  @li @c cAocSubserver::setRpcReplyStructure- Set the RPC reply structure
 *
 *  @li @c cAocCmd_GLOBAL::cAocCmd_GLOBAL   - Constructor
 *  @li @c cAocCmd_GLOBAL::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_GLOBAL::checkDependencies- Can cmd be done?
 *  @li @c cAocCmd_GLOBAL::checkIfDone      - Check to see if cmd is done.
 *  @li @c cAocCmd_GLOBAL::execute          - Execute the command
 *  @li @c cAocCmd_GLOBAL::executeInit      - Execute the init command
 *  @li @c cAocCmd_GLOBAL::executeDatum     - Execute the datum command
 *  @li @c cAocCmd_GLOBAL::executeReboot    - Execute the reboot command
 *  @li @c cAocCmd_GLOBAL::makeItBusy       - Make the command busy.
 *  @li @c cAocCmd_GLOBAL::makeItUnBusy     - Make the command unbusy.
 *
 *  @li @c cAocCmd_CAL_ON_OFF::cAocCmd_CAL_ON_OFF   - Constructor
 *  @li @c cAocCmd_CAL_ON_OFF::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_CAL_ON_OFF::checkDependencies- Can cmd be done?
 *  @li @c cAocCmd_CAL_ON_OFF::checkIfDone  - Check to see if cmd is done.
 *  @li @c cAocCmd_CAL_ON_OFF::execute      - Execute the command
 *  @li @c cAocCmd_CAL_ON_OFF::makeItBusy   - Make the command busy.
 *  @li @c cAocCmd_CAL_ON_OFF::makeItUnBusy - Make the command unbusy.
 *
 *  @li @c cAocCmd_CAMERA_RATE::cAocCmd_CAMERA_RATE - Constructor
 *  @li @c cAocCmd_CAMERA_RATE::canBeDoneNow    - Can cmd be done now?
 *  @li @c cAocCmd_CAMERA_RATE::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_CAMERA_RATE::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_CAMERA_RATE::execute     - Execute the command
 *  @li @c cAocCmd_CAMERA_RATE::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_CAMERA_RATE::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::cAocCmd_CLOSE_OPEN_LOOP - Constructor
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::canBeDoneNow    - Can cmd be done now?
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::execute     - Execute the command
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_CLOSE_OPEN_LOOP::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_CYCLE_ACTUATORS::cAocCmd_CYCLE_ACTUATORS - Constructor
 *  @li @c cAocCmd_CYCLE_ACTUATORS::canBeDoneNow    - Can cmd be done now?
 *  @li @c cAocCmd_CYCLE_ACTUATORS::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_CYCLE_ACTUATORS::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_CYCLE_ACTUATORS::execute     - Execute the command
 *  @li @c cAocCmd_CYCLE_ACTUATORS::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_CYCLE_ACTUATORS::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_DISPLAY::cAocCmd_DISPLAY - Constructor
 *  @li @c cAocCmd_DISPLAY::canBeDoneNow    - Can cmd be done now?
 *  @li @c cAocCmd_DISPLAY::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_DISPLAY::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_DISPLAY::execute     - Execute the command
 *  @li @c cAocCmd_DISPLAY::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_DISPLAY::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_DUMP::cAocCmd_DUMP   - Constructor
 *  @li @c cAocCmd_DUMP::canBeDoneNow   - Can cmd be done now?
 *  @li @c cAocCmd_DUMP::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_DUMP::checkIfDone    - Check to see if cmd is done.
 *  @li @c cAocCmd_DUMP::execute        - Execute the command
 *  @li @c cAocCmd_DUMP::makeItBusy     - Make the command busy.
 *  @li @c cAocCmd_DUMP::makeItUnBusy   - Make the command unbusy.
 *
 *  @li @c cAocCmd_ENV_CONFIG::cAocCmd_ENV_CONFIG   - Constructor
 *  @li @c cAocCmd_ENV_CONFIG::canBeDoneNow         - Can cmd be done now?
 *  @li @c cAocCmd_ENV_CONFIG::checkDependencies    - Can cmd be done?
 *  @li @c cAocCmd_ENV_CONFIG::checkIfDone  - Check to see if cmd is done.
 *  @li @c cAocCmd_ENV_CONFIG::execute      - Execute the command
 *  @li @c cAocCmd_ENV_CONFIG::makeItBusy   - Make the command busy.
 *  @li @c cAocCmd_ENV_CONFIG::makeItUnBusy - Make the command unbusy.
 *
 *  @li @c cAocCmd_M1_M2_ON_OFF::cAocCmd_M1_M2_ON_OFF - Constructor
 *  @li @c cAocCmd_M1_M2_ON_OFF::canBeDoneNow       - Can cmd be done now?
 *  @li @c cAocCmd_M1_M2_ON_OFF::checkDependencies  - Can cmd be done?
 *  @li @c cAocCmd_M1_M2_ON_OFF::checkIfDone- Check to see if cmd is done.
 *  @li @c cAocCmd_M1_M2_ON_OFF::execute    - Execute the command
 *  @li @c cAocCmd_M1_M2_ON_OFF::makeItBusy - Make the command busy.
 *  @li @c cAocCmd_M1_M2_ON_OFF::makeItUnBusy - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_OBSERV_CONFIG::cAocCmd_OBSERV_CONFIG - Constructor
 *  @li @c cAocCmd_OBSERV_CONFIG::canBeDoneNow      - Can cmd be done now?
 *  @li @c cAocCmd_OBSERV_CONFIG::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_OBSERV_CONFIG::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_OBSERV_CONFIG::execute       - Execute the command
 *  @li @c cAocCmd_OBSERV_CONFIG::makeItBusy    - Make the command busy.
 *  @li @c cAocCmd_OBSERV_CONFIG::makeItUnBusy  - Make the command unbusy.
 *
 *  @li @c cAocCmd_OPTIMIZE::cAocCmd_OPTIMIZE   - Constructor
 *  @li @c cAocCmd_OPTIMIZE::canBeDoneNow       - Can cmd be done now?
 *  @li @c cAocCmd_OPTIMIZE::checkDependencies  - Can cmd be done?
 *  @li @c cAocCmd_OPTIMIZE::checkIfDone    - Check to see if cmd is done.
 *  @li @c cAocCmd_OPTIMIZE::execute        - Execute the command
 *  @li @c cAocCmd_OPTIMIZE::makeItBusy     - Make the command busy.
 *  @li @c cAocCmd_OPTIMIZE::makeItUnBusy   - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_PERFORM::cAocCmd_PERFORM     - Constructor
 *  @li @c cAocCmd_PERFORM::canBeDoneNow        - Can cmd be done now?
 *  @li @c cAocCmd_PERFORM::checkDependencies   - Can cmd be done?
 *  @li @c cAocCmd_PERFORM::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_PERFORM::execute     - Execute the command
 *  @li @c cAocCmd_PERFORM::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_PERFORM::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_RECV_SET_REF_CENT::cAocCmd_RECV_SET_REF_CENT - Constructor
 *  @li @c cAocCmd_RECV_SET_REF_CENT::canBeDoneNow - Can cmd be done now?
 *  @li @c cAocCmd_RECV_SET_REF_CENT::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_RECV_SET_REF_CENT::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_RECV_SET_REF_CENT::execute       - Execute the command
 *  @li @c cAocCmd_RECV_SET_REF_CENT::makeItBusy    - Make the command busy.
 *  @li @c cAocCmd_RECV_SET_REF_CENT::makeItUnBusy  - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_REF_CENTROIDS::cAocCmd_REF_CENTROIDS - Constructor
 *  @li @c cAocCmd_REF_CENTROIDS::canBeDoneNow      - Can cmd be done now?
 *  @li @c cAocCmd_REF_CENTROIDS::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_REF_CENTROIDS::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_REF_CENTROIDS::execute       - Execute the command
 *  @li @c cAocCmd_REF_CENTROIDS::makeItBusy    - Make the command busy.
 *  @li @c cAocCmd_REF_CENTROIDS::makeItUnBusy  - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_SET_SETTINGS::cAocCmd_SET_SETTINGS   - Constructor
 *  @li @c cAocCmd_SET_SETTINGS::canBeDoneNow       - Can cmd be done now?
 *  @li @c cAocCmd_SET_SETTINGS::checkDependencies  - Can cmd be done?
 *  @li @c cAocCmd_SET_SETTINGS::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_SET_SETTINGS::execute            - Execute the command
 *  @li @c cAocCmd_SET_SETTINGS::makeItBusy         - Make the command busy.
 *  @li @c cAocCmd_SET_SETTINGS::makeItUnBusy   - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_SHAPES::cAocCmd_SHAPES   - Constructor
 *  @li @c cAocCmd_SHAPES::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_SHAPES::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_SHAPES::checkIfDone  - Check to see if cmd is done.
 *  @li @c cAocCmd_SHAPES::execute      - Execute the command
 *  @li @c cAocCmd_SHAPES::makeItBusy   - Make the command busy.
 *  @li @c cAocCmd_SHAPES::makeItUnBusy - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_STAT::cAocCmd_STAT       - Constructor
 *  @li @c cAocCmd_STAT::canBeDoneNow       - Can cmd be done now?
 *  @li @c cAocCmd_STAT::checkDependencies  - Can cmd be done?
 *  @li @c cAocCmd_STAT::checkIfDone        - Check to see if cmd is done.
 *  @li @c cAocCmd_STAT::execute            - Execute the command
 *  @li @c cAocCmd_STAT::makeItBusy         - Make the command busy.
 *  @li @c cAocCmd_STAT::makeItUnBusy       - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_STATISTICS::cAocCmd_STATISTICS   - Constructor
 *  @li @c cAocCmd_STATISTICS::canBeDoneNow         - Can cmd be done now?
 *  @li @c cAocCmd_STATISTICS::checkDependencies    - Can cmd be done?
 *  @li @c cAocCmd_STATISTICS::checkIfDone  - Check to see if cmd is done.
 *  @li @c cAocCmd_STATISTICS::execute      - Execute the command
 *  @li @c cAocCmd_STATISTICS::makeItBusy   - Make the command busy.
 *  @li @c cAocCmd_STATISTICS::makeItUnBusy - Make the command unbusy.
 *
 *
 *  @li @c cAocCmd_STORE_APO::cAocCmd_STORE_APO - Constructor
 *  @li @c cAocCmd_STORE_APO::canBeDoneNow      - Can cmd be done now?
 *  @li @c cAocCmd_STORE_APO::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_STORE_APO::checkIfDone   - Check to see if cmd is done.
 *  @li @c cAocCmd_STORE_APO::execute       - Execute the command
 *  @li @c cAocCmd_STORE_APO::makeItBusy    - Make the command busy.
 *  @li @c cAocCmd_STORE_APO::makeItUnBusy  - Make the command unbusy.
 *
 *  @li @c cAocCmd_TAKE_EXP::cAocCmd_TAKE_EXP   - Constructor
 *  @li @c cAocCmd_TAKE_EXP::canBeDoneNow       - Can cmd be done now?
 *  @li @c cAocCmd_TAKE_EXP::checkDependencies  - Can cmd be done?
 *  @li @c cAocCmd_TAKE_EXP::checkIfDone    - Check to see if cmd is done.
 *  @li @c cAocCmd_TAKE_EXP::execute        - Execute the command
 *  @li @c cAocCmd_TAKE_EXP::makeItBusy     - Make the command busy.
 *  @li @c cAocCmd_TAKE_EXP::makeItUnBusy   - Make the command unbusy.
 *
 *  @li @c cAocCmd_TAKE_PIXELS::cAocCmd_TAKE_PIXELS - Constructor
 *  @li @c cAocCmd_TAKE_PIXELS::canBeDoneNow        - Can cmd be done now?
 *  @li @c cAocCmd_TAKE_PIXELS::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_TAKE_PIXELS::checkIfDone - Check to see if cmd is done.
 *  @li @c cAocCmd_TAKE_PIXELS::execute     - Execute the command
 *  @li @c cAocCmd_TAKE_PIXELS::makeItBusy  - Make the command busy.
 *  @li @c cAocCmd_TAKE_PIXELS::makeItUnBusy- Make the command unbusy.
 *
 *  @li @c cAocCmd_TT::cAocCmd_TT       - Constructor
 *  @li @c cAocCmd_TT::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_TT::checkDependencies- Can cmd be done?
 *  @li @c cAocCmd_TT::checkIfDone      - Check to see if cmd is done.
 *  @li @c cAocCmd_TT::execute          - Execute the command
 *  @li @c cAocCmd_TT::makeItBusy       - Make the command busy.
 *  @li @c cAocCmd_TT::makeItUnBusy     - Make the command unbusy.
 *
 *  @li @c cAocCmd_IN_FUNC_GAINS::cAocCmd_IN_FUNC_GAINS - Constructor
 *  @li @c cAocCmd_IN_FUNC_GAINS::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_IN_FUNC_GAINS::checkDependencies- Can cmd be done?
 *  @li @c cAocCmd_IN_FUNC_GAINS::checkIfDone      - Check to see if cmd is done.
 *  @li @c cAocCmd_IN_FUNC_GAINS::execute          - Execute the command
 *  @li @c cAocCmd_IN_FUNC_GAINS::makeItBusy       - Make the command busy.
 *  @li @c cAocCmd_IN_FUNC_GAINS::makeItUnBusy     - Make the command unbusy.
 *
 *  @li @c cAocCmd_STATS_LOG::cAocCmd_STATS_LOG- Constructor
 *  @li @c cAocCmd_STATS_LOG::canBeDoneNow     - Can cmd be done now?
 *  @li @c cAocCmd_STATS_LOG::checkDependencies- Can cmd be done?
 *  @li @c cAocCmd_STATS_LOG::checkIfDone      - Check to see if cmd is done.
 *  @li @c cAocCmd_STATS_LOG::execute          - Execute the command
 *  @li @c cAocCmd_STATS_LOG::makeItBusy       - Make the command busy.
 *  @li @c cAocCmd_STATS_LOG::makeItUnBusy     - Make the command unbusy.
 *
 *  @li @c cAocCmd_GAINS::cAocCmd_GAINS     - Constructor
 *  @li @c cAocCmd_GAINS::canBeDoneNow      - Can cmd be done now?
 *  @li @c cAocCmd_GAINS::checkDependencies - Can cmd be done?
 *  @li @c cAocCmd_GAINS::checkIfDone       - Check to see if cmd is done.
 *  @li @c cAocCmd_GAINS::execute           - Execute the command
 *  @li @c cAocCmd_GAINS::makeItBusy        - Make the command busy.
 *  @li @c cAocCmd_GAINS::makeItUnBusy      - Make the command unbusy.
 *
 *  @li @c aocSubServer             - The RPC server calling function
 *  @li @c gpAocSockExecute         - The cmd that sends to the AOC system.
 *
 ************************************************************************
*/

/*!
 *  Defines
 */
/*****************************************************
************** WARNING, ******************************
*********     CONFIGURATION REQUIRED *****************
*********These need to be set to reasonable values 
*********when someone knows what those are!!!!!!!!!!!!
******************************************************/

#define GPI_AOC_MAX_DUMP_DEC    100
#define GPI_AOC_MIN_DUMP_DEC    1

#define GPI_AOC_MAX_REFSELECT   0x80

#define GPI_AOC_MAX_DUMP_SECONDS    30.0
#define GPI_AOC_MIN_DUMP_SECONDS    0.1

#define GPI_AOC_MAX_DUMP_DEC    100
#define GPI_AOC_MIN_DUMP_DEC    1

/***************************************************************/



#define PORTMAP                         /* For RPC purposes.    */

/*
 *  Global connection to GMB area
 *  Done as a global so that the part from Main will 
 *  be able to init (think about changing this to here).
 */
#define GP_GMBLOCK_AOC

/*!
 *  Includes
 */

#include <map>
#include <list>
#include <string>
#include <iostream>

using namespace std;

/* #include <rpc/rpc.h>  <- imported through gpAocCustom.h */
#include <rpc/pmap_clnt.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h> 			/* strerror */

#include "gpUtGeneral.h"
#include "gpUtDebug.h"
#include "gpUtCLArg.h"
#include "gpUtPort.h"
/* #include "gpAocRpc.h" <- imported through gpAocCustom.h */
/* #include "gpSubserver.h" <- imported through gpAocCustom.h */
#include "gpAocCustom.h"
#include "gpUtGmb.h"

/*!
 *  Typedefs
 */

/*
 *  STEVE:
 *  Remove these function prototype when you want to compilet with AOC.
 */

int aocAutoCamRate();
int aocSetCamRate(int);
int aocCheckIfDumpIsDone();
int aocPerformTheDumpOfData(int, int, int);
int aocSaveObservConfigVals(float, float, int, int, int );

/*!
 *  Variables
 */

/*
 *  gmb: used to find ptr to AOC section of GMB.
 *  gmbAoc: Pointer to AOC section of GMB.
 *  err: globally used in gmb calls to extract errors.
 */
extern gpUtGmb_h gmb;
static GMB_ATTRIB_AOC gpUtGmbAoc * gmbAoc;
static GMB_ATTRIB_TLC gpUtGmbTlc * gmbTlc;
extern gmb_err_t err;


/*!
 * Simulating the AOC or not.  Is the AOC there?  This is the default
 * condition, and can be changed on the fly.  Depends on how it is compiled.
 *
 * IF THERE IS AOC CODE THAT YOU CAN COMPILE WITH, THEN IN THE MAKEFILE IN
 * SRC, FIND "gpAocCustom.o:", and add to the compile line:
 *  -DINCLUDING_AOC_CODE
 * then re-compile.  This will include the AOC Code socket bits
 */
#ifdef INCLUDING_AOC_CODE
static int aocSimVal = 0;
#include <stddef.h>			/* NULL */
#include <sys/types.h>
#include <sys/socket.h>  	/* recv, send on socket */
#include <sys/time.h>  		/* timeval struct */
#include <resolv.h>  		/* sockaddr_in */
#include <netinet/tcp.h>	/* TCP_NODELAY */
#include <arpa/inet.h>		/* inet_addr */
#include <fcntl.h>			/* O_NONBLOCK, F_GETFL, F_SETFL	*/
#include <unistd.h>			/* close */

static struct sockaddr_in *address;


/*!
 *  This is the IPC socket to communicate to the AOC
 */

static int rt_ClientSocket=-1;


/*!
 *  cmdTableTranslate: This table holds the string translation between
 *  the AOC RPC Command and the AOC SRT Server.
 */


typedef struct 
{
    const char * aocString;		/*  AOC Command string name. */
    const int  aocRpcCmdNum;
} GP_CMD_RPC_TO_AOC;


/* NOT USED ...
static GP_CMD_RPC_TO_AOC info[] = 
{ 
    {"shutdown", MT_AOC_GLOBAL},
    {"globalInit", MT_AOC_GLOBAL},
    {"globalIndex", MT_AOC_GLOBAL},
    {"globalPark", MT_AOC_GLOBAL},
    {"globalReboot", MT_AOC_GLOBAL},
    {"autoCamRate", MT_AOC_GLOBAL}
};
*/


#else
static int aocSimVal = 1;
#endif

/*!
 *  CONFIGURATION REQUIRED, WARNING, if Command order changes in gpAocRpc.x
 *  then this must change.
 */
int FirstWaitTimes [24] = 
{ 
    10,     /*MT_AOC_GLOBAL, */     
    5,      /*MT_AOC_CAL_ON_OFF, */ 
    5,      /*MT_AOC_CAMERA_RATE, */        
    5,      /*MT_AOC_CLOSE_OPEN_LOOP, */        
    5,      /*MT_AOC_CYCLE_ACTUATORS, */    
    10,     /*MT_AOC_DISPLAY, */        
    10,     /*MT_AOC_DUMP */        
    5,      /*MT_AOC_ENV_CONFIG, */
    5,      /*MT_AOC_M1_M2_ON_OFF, */
    5,      /*MT_AOC_OBSERV_CONFIG, */
    5,      /*MT_AOC_OPTIMIZE, */
    5,      /*MT_AOC_PERFORM, */
    5,      /*MT_AOC_RECV_SET_REF_CENTROIDS, */
    5,      /*MT_AOC_REF_CENTROIDS, */
    5,      /*MT_AOC_SET_SETTINGS, */
    5,      /*MT_AOC_SHAPES, */
    5,      /*MT_AOC_STATS, */
    5,      /*MT_AOC_STORE_APO_REF_CENT, */
    5,      /*MT_AOC_TAKE_EXP, */
    5,      /*MT_AOC_TAKE_PIXELS */
    5,      /*MT_AOC_TT, */
    5,      /*MT_AOC_IN_FUNC_GAINS */
    5,      /*MT_AOC_STATS_LOG */
    5       /*MT_AOC_GAINS */
};


/*! 
 * Defining the debug level for the macros.
 * The Debug Level for this main process. 
 */
#define GPI_LOCAL_DEBUG_LEVEL gpAocSubLocalDebugLevel
int gpAocSubLocalDebugLevel = GPI_DEBUG_MIN;

/*
 * Initialize static variables.
 */
char cAocSubserver::aocIpStr[IP_MAX_STR_LEN]; 
int cAocSubserver::aocPortNum = -1;

int dummyCounter=0; /* This is temporary, remove when real aoc is there.*/

/*!
 * Local Function Prototypes
 */
int currentlyCorrectingAoc( void );

int gpAocSockConnect ();
int gpAocSockExecute (GPI_SUBSERV_RPC_CMD *ptrToCmd, const char * format, ... );

/*!
 ************************************************************************
 *
 * @fn int gpAocSockConnect ()
 *
 * @brief
 *   Connect to AOC Socket
 *
 * @callgraph
 ************************************************************************
-*/
int gpAocSockConnect
(
)
{
    int sockFd=-1;
#ifdef INCLUDING_AOC_CODE

    /*
     * create socket.
     */

    sockFd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if ( sockFd < 0 ) 
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "gpSock: Could not connect to AOC server, is it running:"
            "error.\n" );
    
        return -1;
    }

    /*
     *  Make remote connection.
     */

    address = (struct sockaddr_in *)malloc( sizeof(struct sockaddr_in *));
    address->sin_family = AF_INET;
    int tmp = inet_pton( AF_INET, cAocSubserver::aocIpStr, (void *)( &( address->sin_addr.s_addr)));

    if( tmp < 0)    
    {
	GPI_DEBUG( GPI_DEBUG_NONE,  
	    "gpAocSockConnect:: Can't set remote->sin_addr.s_addr.\n");
	return -1;
    }
    else if( tmp == 0)
    {
	GPI_DEBUG( GPI_DEBUG_NONE,  
	    "gpAocSockConnect:: %s is not a valid IP address\n", cAocSubserver::aocIpStr);
	return -1;
    }

    address->sin_port = htons( cAocSubserver::aocPortNum );
    GPI_DEBUG( GPI_DEBUG_NONE,  
	"gpAocSockConnect:: Actually connect to AOC RT socket.\n");

    if( connect( sockFd, (struct sockaddr *)address, sizeof( struct sockaddr)) < 0 )
    {
	GPI_DEBUG( GPI_DEBUG_NONE,  
	    "gpAocSockConnect:: Connection failed.\n");
	    return -1;
    }
    /*
     * Set socket options
     */
    int flag;
    struct timeval timer;
    if ( setsockopt( sockFd, IPPROTO_TCP, TCP_NODELAY, &flag,
                sizeof(flag) ) < 0 )
        {
            GPI_DEBUG( GPI_DEBUG_NONE,
                        "setSocketOptions: ERROR, unable to set TCP no dely\n" );
        }
    else
    {
        timer.tv_sec = (int) 1;
        timer.tv_usec = (int) ( 1.0e6 * (1 - timer.tv_sec) );

        if ( setsockopt( sockFd, SOL_SOCKET, SO_RCVTIMEO, &timer,
                sizeof(timer) ) < 0 )
	{
            GPI_DEBUG( GPI_DEBUG_NONE,
                        "setSocketOptions: ERROR, unable to set timeout\n" );
	}
      } 


        free( address);

#endif

    return sockFd;
} /* End of gpAocSockConnect() */

/*!
 ************************************************************************
 *
 * @fn int gpAocSockExecute (GPI_SUBSERV_RPC_CMD *ptrToCmd, const char *aocCmdString)
 *
 * @brief
 *   Use AOC socket to Execute a command function
 *
 * @callgraph
 ************************************************************************
-*/

int gpAocSockExecute
(
    GPI_SUBSERV_RPC_CMD * ptrToCmd, 
    const char * format, 
    ...
)
{
    char message[GPI_MAX_STRING_LEN];
    va_list ap;
 
    va_start (ap, format);
    vsnprintf (message, GPI_MAX_STRING_LEN, format, ap);
    va_end (ap);

    GPI_DEBUG_ENTER;

    GPI_DEBUG( GPI_DEBUG_NONE, 
        "gpAocSockExecute: Execute %s\"%s\" to AOC, hostIp<%s>, port<%d>.\n", 
        (ptrToCmd->activityDir==CANCEL)?"<<CANCEL>>":"", message,
        cAocSubserver::aocIpStr, cAocSubserver::aocPortNum );

#ifdef INCLUDING_AOC_CODE
    GPI_DEBUG( GPI_DEBUG_NONE,
	"gpAocSockExecute: Opening AOC Socket.\n");

    if ( rt_ClientSocket < 0 && 
    	( rt_ClientSocket = gpAocSockConnect( ) ) < 0 )
    {

	/*
	 *  Failed to create a socket.
	 */

	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Socket open for AOC failed, is it running.\n");
	strncpy ( ptrToCmd->stateErrMsg, 
	    "Socket open for AOC failed, check to see if AOC is "
	    "running.", GPI_MAX_STRING_LEN );
	ptrToCmd->state =  GPI_STATE_ERROR;
	return -1;
    }

    int iResult;
    char recvBuf[2048];

	// While there is stuff in buffer - flush it out
    while ( (iResult = recv( rt_ClientSocket, (char*)recvBuf, sizeof(recvBuf), MSG_PEEK) > 0 ) )
    {
    	/*
	 */
	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Warning, it appears to we have something to received in "
	    "the recvBuf<%s>, getting rid of it before send.\n",
	    recvBuf );

	recv( rt_ClientSocket, (char*)recvBuf, sizeof(recvBuf), 0);
	}

    /*
     *  Send the command string.
     *  
     */

    int x;
    x=send( rt_ClientSocket, (char *)message, (int) (strlen( message)+1), 0 );
	
    if ( x <= 0 )
    {
	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Sending msg<%s> to AOC failed, is it running:"
	    "x: %d.\n", message, x);
    
	strncpy ( ptrToCmd->stateErrMsg, 
	    "Failed to send msg to AOC failed, check to see if AOC is "
	    "running.", GPI_MAX_STRING_LEN );
	ptrToCmd->state =  GPI_STATE_ERROR;
	return -1;
    }

	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Msg sent: <%s>g:"
	    "x: %d.\n", message, x);

    /*
     *  Receive something?
     */

    iResult = recv( rt_ClientSocket, (char*)recvBuf, sizeof(recvBuf), 0);
    if( iResult == 0 )
    {
	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Received nothing.\n");

    }
    else if ( iResult < 0 )
    {
	GPI_DEBUG( GPI_DEBUG_NONE,
	    "gpAocSockExecute: Sending msg<%s> to AOC failed, Received:%d.",
	    message, iResult);
    
	strncpy ( ptrToCmd->stateErrMsg, 
	    "Got failure returned from AOC, check AOC", GPI_MAX_STRING_LEN );
	ptrToCmd->state =  GPI_STATE_ERROR;
	return -1;
    }
    else if( ( iResult != 1) ||( recvBuf[0] != 0) )
    {
	/*
	 * check form CMD_ERROR or CMD_OK
	 */
	if ( strstr( recvBuf, "CMD_ERROR" ) != NULL )
	{
	    GPI_DEBUG( GPI_DEBUG_NONE,
		"gpAocSockExecute: ERROR, Sent<%s>, got msg back<%s>\n",
		message, recvBuf );
		strncpy ( ptrToCmd->stateErrMsg, 
		    "Got failure returned from AOC, check AOC", GPI_MAX_STRING_LEN );
		ptrToCmd->state =  GPI_STATE_ERROR;
		return -1;
	}
	else if ( strstr ( recvBuf, "CMD_OK" ) != NULL )
	{
	    GPI_DEBUG( GPI_DEBUG_NONE,
		"gpAocSockExecute: Sent<%s>, think it's ok, got msg back<%s>\n",
		message, recvBuf );
	}
	else
	{
	    GPI_DEBUG( GPI_DEBUG_NONE,
		"gpAocSockExecute: WEIRD, Sent<%s>, didnt get CMD_ERROR or CMD_OK: got msg back<%s>\n",
		message, recvBuf );
	}
    }


    close (rt_ClientSocket);
    GPI_DEBUG( GPI_DEBUG_NONE, 
	"cAocSockExecute:: client socket sent.\n");
#endif

   return( GPI_ERR_NONE );

} /* End of gpAocSockExecute() */


/*!
 ************************************************************************
 *
 * @fn cAocSubserver::cAocSubserver ( cSubCmdResult * , int )
 *
 * @brief
 *   Constructor for every command cAocSubserver, clears reply structure
 *
 * @callgraph
 ************************************************************************
-*/
cAocSubserver::cAocSubserver
(
    cSubCmdResult * ptr     /* Ptr to result structure  */
) : cSubserverBase
( 
    ptr, 
    &(reply.r),
    (int *) &(gmbAoc->aocRpc.stat.shutdownRequest) 
)
{
    GPI_DEBUG_ENTER;

    /*
     *  This is called everytime a command is received.
     *  Clear the RPC reply structure.
     */

    clearRpcReplyStructure();

} /* end of cAocSubserver::cAocSubserver() */

/*!
 ************************************************************************
 *
 * @fn void cAocSubserver::clearRpcReplyStructure(int)
 *
 * @brief
 *   Clear the reply structure
 * 
 * @callgraph
 ************************************************************************
-*/
void cAocSubserver::clearRpcReplyStructure
(
    void
)
{
    GPI_DEBUG_ENTER;

    cSubserverBase::clearRpcReplyStructure();

    reply.initState =  GPI_STATE_IDLE;
    reply.initId    =  -1;
    reply.initAck   =  -1;

    reply.datumState    =  GPI_STATE_IDLE;
    reply.datumId   =  -1;
    reply.datumAck  =  -1;

    return;

} /* End of cAocSubserver::clearRpcReplyStructure() */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::decodeLineFunc(char *, char **, int, void * const, 
 *      int *); 
 *
 * @brief
 *   Decode configuration file line.  This will be decoded for the entire 
 *      system.
 *
 * @callgraph
 ************************************************************************
-*/

int cAocSubserver::decodeLineFunc
(
    char * keyword,        /* (in)  Parameter keyword.         */
    char ** tokens,        /* (in)  value of the parameter.    */
    int numWords,          /* (in)  Number of keywords.        */
    void * const classPtr, /* (mod) Class to store info.       */
    int * status           /* (mod) return status.             */
)
{
    *status = GPI_ERR_NONE;
    return( GPI_ERR_NONE );

} /* end of cAocSubserver::decodeLineFunc */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::getRequest (SVCXPRT *xprt)
 *
 * @brief
 *   Get the command request.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocSubserver::getRequest
(
    SVCXPRT * xprt /* (in) RPC transport handle. */
)
{
    GPI_DEBUG_ENTER;

   /*
    *  Get the RPC request.
    *
    *  svc_getargs decodes arguments of the RPC request associated
    *  with RPC transport handle (xprt).
    *
    *  Uses the function xdr_GP_AOC_REPLY (in the generated .c file) to
    *  decode the arguments passed in.
    *
    *  Decoded args are placed in request structure.
    */
    if (!svc_getargs(xprt, (xdrproc_t)xdr_GP_AOC_REQUEST,  
        (char *)&request))
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver:getRequest: ERROR getting command failed.\n" );

        svcerr_decode (xprt);
        return( GPI_ERR_ERROR );
    }

    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocSubserver::getRequest: Got the request, cmd<%d>.\n", 
        request.command );

    return( GPI_ERR_NONE );

} /* end of int cAocSubserver::getRequest() */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::handleRequest ( void )
 *
 * @brief
 *   Handle the request made.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocSubserver::handleRequest
(
  void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

   /*
    *  Save the command number, status already cleared prior to starting.
    */

    cmdNum = request.command;
    cSubCommands * cmdPtr = NULL;
    
    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocSubserver::handleRequest: Received cmd: %d.\n", cmdNum );

    /*
     *  Based on the command sent in - create the template class
     *  that will handle it.
     *  Create object and save to class variable.
     */
  
    switch (cmdNum)
    {
    case MT_AOC_GLOBAL:     /* Init, datum, etc.        */
        GPI_DEBUG( GPI_DEBUG_FULL,
            "cAocSubserver::handleRequest: Received <<GLOBAL>> cmd.\n");

        cmdPtr = new cAocCmd_GLOBAL(&request, &reply, resPtr );

        break;

    case MT_AOC_CAL_ON_OFF: /* Take CAL data or not.    */
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocSubserver::handleRequest: Received <<CAL_ON_OFF>> .\n");
        
        cmdPtr = new cAocCmd_CAL_ON_OFF( &request, &reply, resPtr );
        break;
    case MT_AOC_CAMERA_RATE:    /* CAMERA_RATE.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<CAMERA_RATE>> .\n");
        
        cmdPtr = new cAocCmd_CAMERA_RATE( &request, &reply, resPtr );
        break;

    case MT_AOC_CLOSE_OPEN_LOOP:    /* CLOSE_OPEN_LOOP.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<CLOSE_OPEN_LOOP>> .\n");
        
        cmdPtr = new cAocCmd_CLOSE_OPEN_LOOP( &request, &reply, resPtr );
        break;

    case MT_AOC_CYCLE_ACTUATORS:    /* CYCLE_ACTUATORS.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<CYCLE_ACTUATORS>> .\n");
        
        cmdPtr = new cAocCmd_CYCLE_ACTUATORS( &request, &reply, resPtr );
        break;

    case MT_AOC_DISPLAY:    /* DISPLAY.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<DISPLAY>> .\n");
        
        cmdPtr = new cAocCmd_DISPLAY( &request, &reply, resPtr );
        break;

    case MT_AOC_DUMP:   /* DUMP.    */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<DUMP>> .\n");
        
        cmdPtr = new cAocCmd_DUMP( &request, &reply, resPtr );
        break;

    case MT_AOC_ENV_CONFIG: /* ENV_CONFIG.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<ENV_CONFIG>> .\n");
        
        cmdPtr = new cAocCmd_ENV_CONFIG( &request, &reply, resPtr );
        break;

    case MT_AOC_M1_M2_ON_OFF:   /* M1_M2_ON_OFF.    */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<M1_M2_ON_OFF>> .\n");
        
        cmdPtr = new cAocCmd_M1_M2_ON_OFF( &request, &reply, resPtr );
        break;

    case MT_AOC_OBSERV_CONFIG:  /* Set Observation config.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<OBSERV_CONFIG>>.\n");
        
        cmdPtr = new cAocCmd_OBSERV_CONFIG( &request, &reply, resPtr );
        break;

    case MT_AOC_OPTIMIZE:   /* OPTIMIZE.    */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<OPTIMIZE>> .\n");
        
        cmdPtr = new cAocCmd_OPTIMIZE( &request, &reply, resPtr );
        break;

    case MT_AOC_PERFORM:        /* Perform generic cmd.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<PERFORM>>.\n");
        
        cmdPtr = new cAocCmd_PERFORM( &request, &reply, resPtr );
        break;

    case MT_AOC_RECV_SET_REF_CENT:  /* RECV_SET_REF_CENTRIODS.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<RECV_SET_REF_CENT>>.\n");
        
        cmdPtr = new cAocCmd_RECV_SET_REF_CENT( &request, &reply, resPtr );
        break;

    case MT_AOC_REF_CENTROIDS:  /* REF_CENTRIODS.   */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<REF_CENTRODIS>> .\n");
        
        cmdPtr = new cAocCmd_REF_CENTROIDS( &request, &reply, resPtr );
        break;

    case MT_AOC_SET_SETTINGS:   /* SET_SETTINGS.    */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<SET_SETTINGS>> .\n");
        
        cmdPtr = new cAocCmd_SET_SETTINGS( &request, &reply, resPtr );
        break;

    case MT_AOC_SHAPES: /* SHAPES.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<SHAPES>> .\n");
        
        cmdPtr = new cAocCmd_SHAPES( &request, &reply, resPtr );
        break;

    case MT_AOC_STAT:       /* Just return status.      */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<STATUS>> request.\n");
        
        cmdPtr = new cAocCmd_STAT( &request, &reply, resPtr );
        break;

    case MT_AOC_STATISTICS:     /* Start/stop statistics.   */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<STATISTICS>> "
              "request.\n");
        
        cmdPtr = new cAocCmd_STATISTICS( &request, &reply, resPtr );
        break;

    case MT_AOC_STORE_APO : /* STORE_APO_REF_CENT   */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<STORE_APO>>.\n");
        
        cmdPtr = new cAocCmd_STORE_APO( &request, &reply, resPtr );
        break;

    case MT_AOC_TAKE_EXP:   /* Take an exposure.        */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<TAKE EXP>>.\n");
        
        cmdPtr = new cAocCmd_TAKE_EXP( &request, &reply, resPtr );
        break;

    case MT_AOC_TAKE_PIXELS:    /* TAKE_PIXELS.     */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<TAKE_PIXELS>> .\n");
        
        cmdPtr = new cAocCmd_TAKE_PIXELS( &request, &reply, resPtr );
        break;

    case MT_AOC_TT: /* TT.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<TT>> .\n");
        
        cmdPtr = new cAocCmd_TT( &request, &reply, resPtr );
        break;

    case MT_AOC_IN_FUNC_GAINS: /* IN_FUNC_GAINS.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<IN_FUNC_GAINS>> .\n");
        
        cmdPtr = new cAocCmd_IN_FUNC_GAINS( &request, &reply, resPtr );
        break;

    case MT_AOC_STATS_LOG: /* STATS_LOG.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<STATS_LOG>> .\n");
        
        cmdPtr = new cAocCmd_STATS_LOG( &request, &reply, resPtr );
        break;

    case MT_AOC_GAINS: /* GAINS.  */
        GPI_DEBUG( GPI_DEBUG_MIN,
              "cAocSubserver::handleRequest: Received <<GAINS>> .\n");
        
        cmdPtr = new cAocCmd_GAINS( &request, &reply, resPtr );
        break;

    default:
        status =  GPI_ERR_ERROR;

        GPI_DEBUG( GPI_DEBUG_NONE,
          "cAocSubserver::handleRequest: Invalid Cmd Num received:%d.\n", 
          cmdNum );

        GPI_SET_ERROR( status, reply.r.ackErrMsg, (&reply.r),
            "Invalid Command Number Request" );
        
        break;

    }/*End of switch on command */

    /*
     *  If a valid command then Check the dependencies
     *  
     */
    if ( cmdPtr == NULL && status == GPI_ERR_NONE )
    {
        status = GPI_ERR_ERROR;

        GPI_SET_ERROR( status, reply.r.ackErrMsg, (&reply.r), 
            "Error, command pointer null" );

        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::handleRequest: Error, command pointer is "
            "null.\n");
    }
    else if ( status == GPI_ERR_NONE  )
    {
        setCmdPtr( cmdPtr );
        setCompletionFlag( cmdPtr->canBeDoneNow() );

        /*
         * Just return if this is a cancel command.
         */

        if ( cmdPtr->activityDir == CANCEL )
        {
            setActivityInGmb();
            GPI_DEBUG( GPI_DEBUG_MAX,
                "cAocSubserver::handleRequest: CANCEL directive!!.\n");
        }

        /*
         * Otherwise it is a PRESET or PRESET_START or START,
         * Check the command dependencies
         */

        else if ( ( status = cmdPtr->checkDependencies()
                  ) == GPI_ERR_NONE )
        {
            /*
             *  Succcessful, If the directive is NOT preset 
             *  (meaning it's a START, PRESET_START, or CANCEL) 
             *  set the DIR & ID in the GMB, and then makeItBusy()
             */

            GPI_DEBUG( GPI_DEBUG_MAX,
                "cAocSubserver::handleRequest: Success on checkDependencies.\n");

            if ( cmdPtr->activityDir != PRESET )
            {
                setActivityInGmb();
                status = cmdPtr->makeItBusy();
		/*
		 * Special case, need to set reboot flag here so that main can exit.
		 * after sending response back.
		 */

		if ( cmdNum == MT_AOC_GLOBAL && request.GP_AOC_REQUEST_u.mtGlobal_.mode == 
		    GPI_GLOBAL_REBOOT  && cmdPtr->activityDir != CANCEL ) 
		{
		    gmbAoc->aocRpc.stat.shutdownRequest = 1;

		    GPI_DEBUG( GPI_DEBUG_MAX,
			"cAocSubserver::handleRequest: Reboot flag set.\n");
		}
            }
            
            if ( status == GPI_ERR_NONE )
            {
                GPI_DEBUG( GPI_DEBUG_MAX,
                    "cAocSubserver::handleRequest: Success, dir<%s><%d>.\n",
                    gpActivityStrings[cmdPtr->activityDir], 
                    cmdPtr->activityDir );
            }

        } /* if checkDependencies ok */
        else
        {
            /*
             *  Else, failed in checkDependencies.  Already set the GMB.
             */

            GPI_DEBUG( GPI_DEBUG_NONE,
                "cAocSubserver::handleRequest: Error, failed  dependency "
                "check<%d>.\n", status);
        }
    }/* If creating cmd successful */

    return(status);

} /* End of cAocSubserver::handleRequest() */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::start ( int argc, char * argv[] )
 *
 * @brief
 *  Checks uniqueness, parses the cmd line, reads config file, initializes 
 *  variables, pointers, and GMB. Start the RPC server.
 *
 * @callgraph
 ************************************************************************
-*/
int cAocSubserver::start
(
    int argc, 
    char * argv[]
)
{
    GPI_DEBUG_ENTER;

    int status;

    /*
     *  This is called only once from MAIN and it clears and sets
     *  everything up.
     */

    /*
     *  Check to see if there are other processes running with
     *  the same Mcd number.   Error out if there are? for now...
     */

    if ( gpUtCheckForOtherProcs( "gpAocServer" ) > 0 )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
        "cAocSubserver::start: EXITING - please stop the processes "
        "listed first.\n");
        exit( GPI_ERR_NONE );
    }

    status = parseCmdLine( argc, argv );
    if (status != GPI_ERR_NONE)
    {
         GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::start: ERROR, parsing cmd line.\n" );
        return GPI_ERR_ERROR;
    }

    status = readConfig("");
    if (status != GPI_ERR_NONE)
    {
         GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::start: ERROR, reading config file.\n" );
        return GPI_ERR_ERROR;
    }

    /*
     *  Initialize GMB as the AOC
     *  WARNING, never disable TLC!!!! Otherwise TLC won't be able
     *  to see changes made here.
     *  This will fail if the TLC is not started.
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocSubserver::start: Connecting to GMB, sim val <%d>.\n", 
        cAocSubserver::simGmb);

    CHECKSTATSTR_NRX( (gpUtGmbDisableSystem(GMB_ID_IFS)), 
        "disabling AOC, ignoring..." );
    CHECKSTATSTR_NRX( (gpUtGmbDisableSystem(GMB_ID_CAL)), 
        "disabling AOC, ignoring..." );

    gmb = gpUtGmbConnect(GMB_ID_AOC, cAocSubserver::simGmb, &err);

    CHECK_GMB_ERR( err, "ERROR, initializing GMB, is the "
        "gpUtGmbServiceAOC running...", return(-1) );


    /*
     *  Open a connection to AOC's & TLC's gmb only.
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocSubserver::start: Getting pointer to AOC GMB <%d>.\n", 
        cAocSubserver::simGmb);

    gmbAoc = gpUtGmbGetAoc(gmb);
    if ( gmbAoc == NULL)
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::start: ERROR, Getting AOC ptr, Exiting..\n");
        return(GPI_ERR_ERROR);
    }
    
    gmbTlc = gpUtGmbGetTlc(gmb);
    if ( gmbTlc == NULL)
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::start: ERROR, Getting TLC ptr, Exiting..\n");
        return(GPI_ERR_ERROR);
    }

    /* set simulation level based off command line param */
    gmbAoc->aocRpc.stat.simulate = aocSimVal;

    /*
     *  init the base class.  This initializes the fields in the
     *  gmbAoc->calRpc.stat.* also, and then does a broadcast.
     */
    cSubserverBase::init( (GPI_SUBSERVER_STAT *) &(gmbAoc->aocRpc.stat),
        &gpAocSubLocalDebugLevel );

    /*
     *  Clear the AOC RPC Server GMB area
     */
    /* set by PERFORM */
    gmbAoc->aocRpc.performCmdName = 0;      
    gmbAoc->aocRpc.performVal1[0]='\0';
    gmbAoc->aocRpc.performVal2[0]='\0';
    gmbAoc->aocRpc.performVal3[0]='\0';
    gmbAoc->aocRpc.performVal4[0]='\0';
    gmbAoc->aocRpc.performVal5[0]='\0';
    gmbAoc->aocRpc.performVal6[0]='\0';
    gmbAoc->aocRpc.performVal7[0]='\0';
    gmbAoc->aocRpc.performVal8[0]='\0';
    gmbAoc->aocRpc.performVal9[0]='\0';
    gmbAoc->aocRpc.performVal10[0]='\0';
    /* ??? */
    gmbAoc->aocRpc.observing = 0;       	/* Not observing.*/
    gmbAoc->aocRpc.guiding = 0;         	/* Not guiding.*/
    gmbAoc->aocRpc.useLastVals = 0;		/* not really used ... */
    /* set by CAL_DATA_ON_OFF */
    gmbAoc->aocRpc.aoReadCalData = 0;       	/* No reading cal data. */
    /* set by CAMERA_RATE */
    gmbAoc->aocRpc.cameraAutoModeSet = 1;   	/* Auto mode. */
    gmbAoc->aocRpc.cameraManualRate = 250;  	/* Manual min set. */
    /* set by CLOSE_OPEN_LOOP */
    gmbAoc->aocRpc.loopRequest = 0;     	/* open loops */
    gmbAoc->aocRpc.tweeterLoop = 0;     	/* open loops */
    gmbAoc->aocRpc.wooferLoop = 0;     	/* open loops */
    gmbAoc->aocRpc.tipTiltLoop = 0;     	/* open loops */
    /* set by CYCLE_ACTUATORS */
    gmbAoc->aocRpc.cycleActuatorNum = 1;    	/* Actuator to cycle */
    gmbAoc->aocRpc.stopCycling = 1;		/* Don't cycle acts */
    /* set by DUMP */
    gmbAoc->aocRpc.dumpType = 0;        	/* No dumping. */
    gmbAoc->aocRpc.dumpSeconds = 0.0;     	/* Num sec's to dump */
    gmbAoc->aocRpc.dumpDecimation = 0;     	/* Dump decimation */ 
    /* set by ENV_CONFIG */
    gmbAoc->aocRpc.type = 0;
    gmbAoc->aocRpc.rate = 0;
    gmbAoc->aocRpc.samples = 0;
    gmbAoc->aocRpc.location = 0;
    /* set by OBSERV_CONFIG */
    gmbAoc->aocRpc.magnitudeI = 0.0;
    gmbAoc->aocRpc.magnitudeR = 0.0;
    gmbAoc->aocRpc.r0Estimate = 0.0;
    gmbAoc->aocRpc.camRate = 0;
    gmbAoc->aocRpc.darkSelect = 0;
    gmbAoc->aocRpc.refSelect = 0;
    /* set by OPTIMIZE */
    gmbAoc->aocRpc.optimize = 0;
    gmbAoc->aocRpc.optChoiceFilename[0]='\0';
    gmbAoc->aocRpc.optValueFilename[0]='\0';
    /* set by RECV_SET_REF_CENTROIDS */
    gmbAoc->aocRpc.setRefCentroids = 0;
    gmbAoc->aocRpc.typeRefCentroids = 0;
    gmbAoc->aocRpc.refCentSetting = 0;
    gmbAoc->aocRpc.refCentroidsFilename[0]='\0';
    /* set by REF_CENTROIDS */
    gmbAoc->aocRpc.measure = 0;
    gmbAoc->aocRpc.addSub = 0;
    gmbAoc->aocRpc.refCentOffset = 0;
    /* set by SET_SETTINGS */
    gmbAoc->aocRpc.darkBacks = 0;
    gmbAoc->aocRpc.flatField = 0;
    gmbAoc->aocRpc.threshold = 0;
    /* set by IN_FUNC_GAINS */
    gmbAoc->aocRpc.whichGains = 0;
    gmbAoc->aocRpc.gainXpos = 0;
    gmbAoc->aocRpc.gainYpos = 0;
    gmbAoc->aocRpc.gainAmp = 0.0;
    /* set by STORE_APO_REF_CENT */
    gmbAoc->aocRpc.apodizer = 0;
    /* set by M1_M2_ON_OFF */
    gmbAoc->aocRpc.m1action = 0;
    gmbAoc->aocRpc.m2action = 0;
    /* set by SHAPES */
    gmbAoc->aocRpc.setShapes = 0;
    gmbAoc->aocRpc.storeShapes = 0;
    /* set by STATS_LOG */
    gmbAoc->aocRpc.statsLog = 0;
    /* set by GAINS */
    gmbAoc->aocRpc.gainFactors[0] = 0.0;
    gmbAoc->aocRpc.gainFactors[1] = 0.0;
    gmbAoc->aocRpc.gainFactors[2] = 0.0;
    gmbAoc->aocRpc.gainFactors[3] = 0.0;
    gmbAoc->aocRpc.gainFactors[4] = 0.0;
    gmbAoc->aocRpc.gainFactors[5] = 0.0;
    gmbAoc->aocRpc.gainSource = 0;
    gmbAoc->aocRpc.gainSourceChoices = 0;
    /* set by SET_TT */
    gmbAoc->aocRpc.angle = 0;
    gmbAoc->aocRpc.TTXaxis = 0;
    gmbAoc->aocRpc.TTYaxis = 0;


    err = gpUtGmbBroadcast (gmb, (void *) &(gmbAoc->aocRpc),
        sizeof(gmbAoc->aocRpc) );

    /*!
     *  GPI_SUBSERV_CLEAR_GMB_RPC_CMD : Macro to zero out GPI_SUBSERV_RPC_CMD.
     */
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_GLOBAL, "global" ,
        "ERROR, broadcasting aocCmd_GLOBAL" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_GLOBAL_INIT, "init" ,
        "ERROR, broadcasting aocCmd_GLOBAL_INIT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_GLOBAL_DATUM, "datum" ,
        "ERROR, broadcasting aocCmd_GLOBAL_DATUM" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_GLOBAL_REBOOT, "reboot" ,
        "ERROR, broadcasting aocCmd_GLOBAL_REBOOT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_CAL_ON_OFF, "calOnOff" ,
        "ERROR, broadcasting aocCmd_CAL_ON_OFF" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_CAMERA_RATE, "cameraRate" ,
        "ERROR, broadcasting aocCmd_CAMERA_RATE" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_CLOSE_OPEN_LOOP, 
        "closeOpenLoop" , "ERROR, broadcasting aocCmd_CLOSE_OPEN_LOOP" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_CYCLE_ACTUATORS, 
        "cycleAct" , "ERROR, broadcasting aocCmd_CYCLE_ACT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_DISPLAY, "display" ,
        "ERROR, broadcasting aocCmd_DISPLAY" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_DUMP, "dump" ,
        "ERROR, broadcasting aocCmd_DUMP" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_ENV_CONFIG, "envConfig" ,
        "ERROR, broadcasting aocCmd_ENV_CONFIG" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_M1_M2_ON_OFF, "m1m2OnOff" ,
        "ERROR, broadcasting aocCmd_M1M2_ONOFF" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_OBSERV_CONFIG, 
        "observConfig" , "ERROR, broadcasting aocCmd_OBSERV_CONFIG" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_OPTIMIZE, "optimize" ,
        "ERROR, broadcasting aocCmd_OPTIMIZE" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_PERFORM, "perform" ,
        "ERROR, broadcasting aocCmd_PERFORM" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_RECV_SET_REF_CENT, 
        "rcvSetCent" , "ERROR, broadcasting aocCmd_RCV_SET_CENT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_REF_CENTROIDS, 
        "refCentroids" , "ERROR, broadcasting aocCmd_REF_CENTR" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_SET_SETTINGS, 
        "setSettings" , "ERROR, broadcasting aocCmd_SET_SETTINGS" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_SHAPES, "shapes" ,
        "ERROR, broadcasting aocCmd_SHAPES" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_STAT, "stat" ,
        "ERROR, broadcasting aocCmd_STAT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_STATISTICS, "statistics" ,
        "ERROR, broadcasting aocCmd_STATS" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_STORE_APO, 
        "storeApoRefCent" , "ERROR, broadcasting aocCmd_STORE_AO_REF" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_TAKE_EXP, "takeExp" ,
        "ERROR, broadcasting aocCmd_TAKE_EXP" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_TT, "tt" ,
        "ERROR, broadcasting aocCmd_TT" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_TAKE_PIXELS, "takePixels" ,
        "ERROR, broadcasting aocCmd_TAKE_PIXELS" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_IN_FUNC_GAINS, "inFuncGains" ,
        "ERROR, broadcasting aocCmd_IN_FUNC_GAINS" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_STATS_LOG, "statsLog" ,
        "ERROR, broadcasting aocCmd_STATS_LOG" );
    GPI_SUBSERV_CLEAR_GMB_RPC_CMD( gmbAoc->aocCmd_GAINS, "gains" ,
        "ERROR, broadcasting aocCmd_GAINS" );

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocSubserver::start: Done broadcasting.\n" );

    /*
     *  This is called only once from MAIN and it clears and sets
     *  everything up.
     */
#ifdef INCLUDING_AOC_CODE
    GPI_DEBUG(GPI_DEBUG_NONE, 
        "cAocSubserver::start: Connected to AO.\n");

/**************
 * WARNING, need to monitor AOC heartbeat when it exists - and if it stops/starts
 * then we need to re-connect to socket.
 **************/

#endif //INCLUDING_AOC_CODE

    status = startRpcServer( aocSubServer, GP_AOC_RPC_XID, GP_AOC_RPC_PORT );

    return( status ); 

} /* end of cAocSubserver::start() */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::parseCmdLine ( int, char *argv[] )
 *
 * @brief
 *   Parse the Command line
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocSubserver::parseCmdLine
(
    int argc, 
    char *argv[]
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /*
     * Create flags so we can tell what we got.
     */

    int helpFlag = 0;       // Need help
    int debugVal = gpAocSubLocalDebugLevel;
    int debugFlag = 0;      // Have Debuglevel
    int portFlag   = 0;     // AOC Subsystem Port num
    char filename[GPI_MAX_STRING_LEN];
    int daemonFlag;
    char * errStr;

    filename[0]='\0';

    /*
     * set defualt IP address to local host 
     */
    strcpy( aocIpStr, "127.0.0.1" );


    /*
     *  This structure contains all the info about command line
     *  arguments.
     */

    static GP_UT_CLARG_STRUCT cmdLineArgs[] =
    {    
        { "-aoc", "# AOC SUBSYSTEM ipAddress(optional, default 127.0.0.1)",
            1u, 1u, GP_UT_CLARG_TYPE_STRING, &aocIpStr, sizeof(aocIpStr), 
            NULL }, 

        { "-port", "# Port number for AOC Subsystem",
            1u, 1u, GP_UT_CLARG_TYPE_INT, &aocPortNum, sizeof(aocPortNum), 
	    &portFlag },

	{ "-daemon", "#  Run as daemon (optional)",
            0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &daemonFlag },

        { "-debug", "#  Debug level (optional)",
            1u, 1u, GP_UT_CLARG_TYPE_INT, &debugVal, sizeof(debugVal), 
            &debugFlag },

        { "-d", "#  Debug level (optional)",
            1u, 1u, GP_UT_CLARG_TYPE_INT, &debugVal, sizeof(debugVal), 
            &debugFlag },

        { "-help", "#  Display this help",
            0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &helpFlag },

        { "-h", "#  Display this help",
            0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &helpFlag },

        { "-simGmb", "#  Simulate dolphin reflective memory hardware "
            "(optional)", 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, 
            &cAocSubserver::simGmb },

        { "-simAoc", "#  Simulate the calls to AOC (optional)",
            0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, &aocSimVal },

        { NULL, NULL, 0u, 0u, GP_UT_CLARG_TYPE_NONE, NULL, 0, NULL }
    };

    /*
     *  Get all command line arguments.
     */

    if ( argc <= 1 )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,  
            "cAocSubserver::parseCmdLine: No cmd line params <%d>, "
            "need at least the aocRpcHost name!!!\n", argc );
        
        gpUtCLArgHelp(cmdLineArgs);
        exit ( status );
    }
    else if ( ( status = gpUtCLArgAutoProcess(cmdLineArgs,
        sizeof( cmdLineArgs ), argc, (const char **)argv ) ) != GPI_ERR_NONE )
    {
        gpUtCLArgHelp(cmdLineArgs);
        exit ( status );
    }

    if (daemonFlag != 0)
    {
        gpUtDebugLogToService = 1;
        gpUtDebugLogToStdout = 0;

        GPI_DEBUG(GPI_DEBUG_MIN, " : Starting daemon.\n");
        if (daemon(0,0) != 0)
        {
            //errStr = strerror(errno);
            GPI_DEBUG(GPI_DEBUG_NONE, 
            " : Could not start daemon!\n%x : %s\n", errno, errStr );
        }
    }

    /*
     *  Must have aoc host name.
     */

    if (helpFlag != 0    || portFlag == 0      || 
        aocIpStr == NULL || sizeof(aocIpStr) <= 0 )
    {
        gpUtCLArgHelp(cmdLineArgs);
        status = GPI_ERR_NONE;
        exit(1);
    }

    if (aocSimVal != 0 )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,  
            "cAocSubserver::parseCmdLine: Setting AOC simulation mode:<%d>\n",
            aocSimVal );
    }

    GPI_DEBUG( GPI_DEBUG_NONE,  
	"cAocSubserver::parseCmdLine: AOC Subsystem Port#<%d>, IP#<%s>\n",
	cAocSubserver::aocPortNum, cAocSubserver::aocIpStr );

    if (debugFlag != 0 )
    {
        if ( debugVal < GPI_DEBUG_NONE || debugVal > GPI_DEBUG_MAX )
        {
            GPI_DEBUG( GPI_DEBUG_NONE,
                "cAocSubserver:: Invalid Debug level, using %d, needs "
                "to be between 0 and 3.\n", gpAocSubLocalDebugLevel);
            status =  GPI_ERR_ERROR;

        }
        else
        {
            gpAocSubLocalDebugLevel = debugVal;
        }
    }

    return( status );

} /* end of cAocSubserver::parseCmdLine() */

/*!
 ************************************************************************
 *
 * @fn int cAocSubserver::readConfig ( char * )
 *
 * @brief
 *   Read the configuration file.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocSubserver::readConfig
(
    char * configFile
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

   /* For now, ignore the passed in configFile, 
    * instead take the global confilename that we got
    * from the cmd line - that way we can use it later
    * for a init command.
    */

    if ( strlen( configFileName ) > 0 )
    {
        GPI_DEBUG( GPI_DEBUG_MAX,  
            "cAocSubserver::readConfigFile: Config file name: <%s>\n", 
            configFileName );
        
        status = gpUtReadConfigFile(configFileName, gpAocSubLocalDebugLevel, 
            NULL, &decodeLineFunc);
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,  
            "cAocSubserver::readConfigFile: No Config file to read.\n");
    }
    return(status);

} /* end of cAocSubserver::readConfig() */

/*!
 ************************************************************************
 *
 * @fn void cAocSubserver::setRpcReplyStructure(int)
 *
 * @brief
 *   Set the reply structure
 * 
 * @callgraph
 ************************************************************************
-*/
void cAocSubserver::setRpcReplyStructure
(
    int status
)
{
    GPI_DEBUG_ENTER;

    cSubserverBase::setRpcReplyStructure( status );

    reply.initState      = gmbAoc->aocCmd_GLOBAL_INIT.state;
    reply.initId         = gmbAoc->aocCmd_GLOBAL_INIT.activityId;
    reply.initAck        = gmbAoc->aocCmd_GLOBAL_INIT.ack;

    reply.datumState     = gmbAoc->aocCmd_GLOBAL_DATUM.state;
    reply.datumId        = gmbAoc->aocCmd_GLOBAL_DATUM.activityId;
    reply.datumAck       = gmbAoc->aocCmd_GLOBAL_DATUM.ack;

    return;

} /* End of cAocSubserver::setRpcReplyStructure() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_GLOBAL::cAocCmd_GLOBAL(GP_AOC_REQUEST *, GP_AOC_REPLY *, cSubCmdRestul *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_GLOBAL::cAocCmd_GLOBAL
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result,
    &(rqst->GP_AOC_REQUEST_u.mtGlobal_.c),
    (rqst->GP_AOC_REQUEST_u.mtGlobal_.mode == (int)GPI_GLOBAL_INIT) ?
        (GPI_SUBSERV_RPC_CMD *) & gmbAoc->aocCmd_GLOBAL_INIT :
    (rqst->GP_AOC_REQUEST_u.mtGlobal_.mode == (int)GPI_GLOBAL_DATUM) ?
        (GPI_SUBSERV_RPC_CMD *) & gmbAoc->aocCmd_GLOBAL_DATUM :
    (rqst->GP_AOC_REQUEST_u.mtGlobal_.mode == (int)GPI_GLOBAL_REBOOT) ?
        (GPI_SUBSERV_RPC_CMD *) & gmbAoc->aocCmd_GLOBAL_REBOOT :
        (GPI_SUBSERV_RPC_CMD *) & gmbAoc->aocCmd_GLOBAL,
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    GPI_DEBUG_ENTER;

    /*
     *  Save local command information: dataset Name.
     */ 
    mode = rqst->GP_AOC_REQUEST_u.mtGlobal_.mode;
    level = rqst->GP_AOC_REQUEST_u.mtGlobal_.level; 
    stop = rqst->GP_AOC_REQUEST_u.mtGlobal_.stop; 

    /*
     *  A GLOBAL cmd is unique because it can execute different commands
     *  based on the mode.  The init is reported in 
     *  gmbAoc->aocCmd_GLOBAL_INIT,
     *  datum in gmbAoc->aocCmd_GLOBAL_DATUM, reboot in 
     *  gmbAoc->aocCmd_GLOBAL_REBOOT, and all the rest in
     *  gmbAoc->aocCmd_GLOBAL.
     */
    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocCmd_GLOBAL::cAocCmd_GLOBAL: Received GLOBAL <%s><%d> cmd, "
        "activityId=%d, <%s><%d>.\n", 
        gpGlobalModeStrings[mode], mode, gmbAoc->aocRpc.stat.activityId,
        gpActivityStrings[gmbAoc->aocRpc.stat.activityDir], 
        gmbAoc->aocRpc.stat.activityDir);

} /* end of cAocCmd_GLOBAL::cAocCmd_GLOBAL() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::canBeDoneNow
(
    void
)
{
    int doneNow;

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.  When clear (canBeDoneNow=0), this
     *  will make the execute() be done in a separate thread and a reply
     *  of BUSY will be sent back.  The command will complete in the 
     *  separate thread and the calling program can check the GMB to 
     *  determine when the command is complete.
     *
     *  Can be completed right away if mode is DEBUG, SIM, TEST, or PARK
     */
    if ( mode == GPI_GLOBAL_DEBUG || mode == GPI_GLOBAL_SIM  || 
         mode == GPI_GLOBAL_TEST  || mode == GPI_GLOBAL_PARK )
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_GLOBAL:canBeDoneNow: Cmd can be completed "
            "immediately.\n");
        doneNow = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_GLOBAL:canBeDoneNow: Cmd CAN NOT be completed "
            "immediately.\n");
        doneNow = 0;
    }

    return( doneNow );

} /* End of cAocCmd_GLOBAL::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::checkDependencies( void )
 *
 * @brief
 *   Check the dependencies for this command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_GLOBAL::checkDependencies: Mode=<%s><%d>. Level<%d>.\n",
        gpGlobalModeStrings[mode], mode, level );

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_GLOBAL::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    
    /*
     *  Check that the mode is within range.
     */
    else if (  mode <  GPI_GLOBAL_DEBUG  || mode > GPI_GLOBAL_REBOOT )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL:checkDependencies: Error, GLOBAL mode is out"
            "of range <%d>, needs to be >=%d or <=%d\n", 
            mode, GPI_GLOBAL_DEBUG, GPI_GLOBAL_REBOOT);

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, GLOBAL mode out of range" );
    }

    /*
     *  Check debug level.
     */
    else if (  mode == GPI_GLOBAL_DEBUG  &&
        ( level < GPI_DEBUG_NONE || level > GPI_DEBUG_MAX ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL:checkDependencies: Error, debug level out "
            "of range, <%d>, needs to be >=%d or <=%d\n", 
            level, GPI_DEBUG_NONE, GPI_DEBUG_MAX);

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, debug level out of range" );
    }

    /*
     *  Check simulation level.
     */
    else if ( mode == GPI_GLOBAL_SIM &&
        ( level < GPI_SIM_NONE || level > GPI_SIM_FULL ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL:checkDependencies: Error, simulation level out "
            "of range, <%d>, needs to be >=0 or <=1\n", level);

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, simulation level out of range" );
    }

    /*
     *  That we aren't already busy.
     */
    else if ( ( mode == GPI_GLOBAL_INIT && 
            gmbAoc->aocCmd_GLOBAL_INIT.state == GPI_STATE_BUSY ) ||
        ( mode == GPI_GLOBAL_DATUM && 
            gmbAoc->aocCmd_GLOBAL_DATUM.state == GPI_STATE_BUSY ) )
    {
        /*
         *  WARNING: need to decide if it is OK or not to execute
         *  more than one of the GLOBAL commands at once.....
         *  FOR NOW: not ok for init and datum, but OK for reboot.
         */

        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_GLOBAL:checkDependencies: Error, already busy "
            "doing cmd %d, must stop it first.\n", mode);

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, already executing that command" );
    }
    /*
     *  Can't datum or init while doing anything else.
     */
    else if ( ( mode == GPI_GLOBAL_DATUM && 
            gmbAoc->aocRpc.stat.globalBusy == GPI_STATE_BUSY ) ||
        ( mode == GPI_GLOBAL_INIT && 
            gmbAoc->aocRpc.stat.globalBusy == GPI_STATE_BUSY ) ) 
    {
        /*
         *  WARNING: need to decide if it is OK to FAIL if busy
         *  when asked to do a datum or init.
         */

        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_GLOBAL:checkDependencies: Error, already busy: "
            "INIT<%s>, DATUM<%s>, can't do this cmd <%d>\n",
            gpStateStrings[gmbAoc->aocCmd_GLOBAL_INIT.state],
            gpStateStrings[gmbAoc->aocCmd_GLOBAL_DATUM.state], mode);

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, already executing another cmd" );
    }

    /*
     *  Else, OK to go.  Set interesting info in GMB.
     */
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_GLOBAL:checkDependencies: Success, can execute "
            "<%s><%d> cmd.\n", gpGlobalModeStrings[mode], mode);
    }

    /*
     * Set returned ACK value, and set status for display only.
     * Broadcast results
     */
    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_GLOBAL::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::checkIfDone( void )
 *
 * @brief
 *   Check to see if the command is complete.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::checkIfDone
(
    int numTimes
)
{
    int done;

    GPI_DEBUG_ENTER;

    /*
     *  This method is called by the function that is monitoring
     *  that the command completes correctly.  It will strictly check
     *  the fields in the GMB that are toggled by the functions
     *  makeItBusy/makeItUnBusy.  Don't care if we are in simulate 
     *  mode or not because the execute portion does the simulate.
     *
     *  Check to see if it has completed, and return if it has.
     *  The calling function will keep calling this one a MAX number
     *  of times before declaring that the command failed to complete.
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
            "cAocCmd_GLOBAL::checkIfDone: On count: <%d> for GLOBAL cmd "
            "<%s><%d>\n", counter, gpGlobalModeStrings[mode], mode );

    if ( gmbAoc->aocRpc.stat.simulate &&
    ( ( mode == GPI_GLOBAL_INIT && gmbAoc->aocCmd_GLOBAL_INIT.done  )   ||
         ( mode == GPI_GLOBAL_DATUM  && gmbAoc->aocCmd_GLOBAL_DATUM.done ) ||
         ( mode == GPI_GLOBAL_REBOOT && gmbAoc->aocCmd_GLOBAL_REBOOT.done )||
         ( mode == GPI_GLOBAL_DEBUG || mode == GPI_GLOBAL_SIM || 
           mode == GPI_GLOBAL_TEST || mode == GPI_GLOBAL_PARK ) ) )
    {
        done = 1;
    }
#ifdef INCLUDING_AOC_CODE
    else if ( ( mode == GPI_GLOBAL_INIT &&
                    gmbAoc->aocCmd_GLOBAL_INIT.state != GPI_STATE_BUSY  )  ||
              ( mode == GPI_GLOBAL_DATUM  && 
                    gmbAoc->aocCmd_GLOBAL_DATUM.state != GPI_STATE_BUSY )  ||
              ( mode == GPI_GLOBAL_REBOOT && 
                    gmbAoc->aocCmd_GLOBAL_REBOOT.state != GPI_STATE_BUSY ) ||
              ( mode == GPI_GLOBAL_PARK && 
                    gmbAoc->aocCmd_GLOBAL.state != GPI_STATE_BUSY ) ||
              ( mode == GPI_GLOBAL_DEBUG || mode == GPI_GLOBAL_SIM  || 
                mode == GPI_GLOBAL_TEST ))

#else
    else if ( ( mode == GPI_GLOBAL_INIT   
                    && gmbAoc->aocCmd_GLOBAL_INIT.done==1  ) ||
              ( mode == GPI_GLOBAL_DATUM  
                    && gmbAoc->aocCmd_GLOBAL_DATUM.done )    ||
              ( mode == GPI_GLOBAL_REBOOT 
                    && gmbAoc->aocCmd_GLOBAL_REBOOT.done )   ||
              ( mode == GPI_GLOBAL_PARK 
                    && gmbAoc->aocCmd_GLOBAL.done )   ||
              ( mode == GPI_GLOBAL_DEBUG || mode == GPI_GLOBAL_SIM || 
                mode == GPI_GLOBAL_TEST ))
#endif
    {
    	done = 1;
    }
    else
    {
        done = 0;
    }

    if ( done )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,  
            "cAocCmd_GLOBAL::checkIfDone: Done GLOBAL cmd <%s><%d>, "
            "monitor shutting down.\n", gpGlobalModeStrings[mode], mode );
    }
    else
    {
        /*
         *  Note to self: counter is strictly used for debugging, it is
         *  not used to count down how many times this is called.
         *  This is because the monitor function in gpSubsserver does it.
         */
        GPI_DEBUG( GPI_DEBUG_FULL,  
            "cAocCmd_GLOBAL::checkIfDone: Waiting for <%s> "
            "finish...<%d>....\n", gpGlobalModeStrings[mode], numTimes );
    }

    return( done );

}  /* end of cAocCmd_GLOBAL::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::execute(void)
 *
 * @brief
 *   Execute the "global" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GLOBAL::execute
(
    void
)
{
    int localCmdStatus = GPI_ERR_NONE;      /* Init status. */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_GLOBAL:execute: <%s> state set to <%s><%d>, id <%d>, "
        "dir<%s - %d>.\n",
        subCmdGmb->name, gpStateStrings[subCmdGmb->state], 
        subCmdGmb->state, subCmdGmb->activityId, 
        gpActivityStrings[subCmdGmb->activityDir], subCmdGmb->activityDir);

    /*
     *  Depending on the mode, execute the command.
     */

    switch ( mode )
    {
    case ( GPI_GLOBAL_DEBUG ) :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<DEBUG>> request, setting to <%d>\n",
             level );
        
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL, break );
        localCmdStatus = cSubserverBase::setDebugLevel( level );
        break;

    case ( GPI_GLOBAL_INIT ) :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<INIT>> request.\n");
        
        localCmdStatus = executeInit();
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL_INIT, break );
        break;

    case ( GPI_GLOBAL_DATUM ) :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<DATUM>> request.\n");
        
        localCmdStatus = executeDatum();
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL_DATUM, break );
        break;

    case ( GPI_GLOBAL_PARK ) :
        /*
         *  No-op
         */
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<PARK>> request, is NOOP.\n");
        
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL, break );
        break;

    case ( GPI_GLOBAL_SIM ) :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<SIM>> request, level=<%d>\n",
             level );
        
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL, break );
        gmbAoc->aocRpc.stat.simulate = level;
        break;

    case ( GPI_GLOBAL_TEST ) :
        /*
         *  No-op
         */
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<TEST>> request, is NOOP.\n");
        
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL, break );
        break;

    case ( GPI_GLOBAL_REBOOT ) :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: <<REBOOT>> request.\n");
        
        CHECK_BACKDOOR_ERROR( gmbAoc->aocCmd_GLOBAL_REBOOT, break );
        executeReboot();
        break;

    default:
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:execute: Invalid mode: %d, SHOULD "
            "NEVER BE HERE, needs to be between 1-7.\n", mode);

        localCmdStatus = GPI_ERR_ERROR;
        GPI_SET_STATE_GMB_ERROR( (GPI_STATUS) localCmdStatus, 
           gmbAoc->aocCmd_GLOBAL,
           "Invalid mode for GLOBAL cmd - shouldnt be here");
        break;
    };

    /*
     *  Check that command stateErrCode matches the localCmdStatus, 
     *  otherwise set.
     */
    if ( subCmdGmb->stateErrCode != localCmdStatus )
    {
        GPI_DEBUG( GPI_DEBUG_NONE, 
            "cAocCmd_GLOBAL:execute: !!!!localCmdStatus<%d> doesn't match "
            "gmb stateErrCode <%d> for <%s>, setting!\n", 
            localCmdStatus, subCmdGmb->stateErrCode, subCmdGmb->name );
    }

    GPI_DEBUG( GPI_DEBUG_MAX,  
        "cAocCmd_GLOBAL::execute: Done execute, cmdStatus = <%d> .\n", 
        localCmdStatus );

    return localCmdStatus;

} /* End of cAocCmd_GLOBAL::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::executeInit( void )
 *
 * @brief
 *   Init.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::executeInit
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;


#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an INIT command so we need to send INIT to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<globalInit>> command...., .\n");
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GLOBAL_INIT,
	    AOCCMDFMT_GLOBALINIT );
    }
    else
#endif
    {
        /*
         * WARNING: Simulating Initialize.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL::executeInit: Sim: Going to sleep for 20 to "
            "sim init.\n");

        sleep(20);

    }

    return( status );

} /* End of cAocCmd_GLOBAL::executeInit() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::executeDatum( void )
 *
 * @brief
 *   Datum.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GLOBAL::executeDatum
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an DATUM command so we need to send DATUM to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<globalIndex>> command...., .\n");
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GLOBAL_DATUM,
	    AOCCMDFMT_GLOBALINDEX );

    }
    else
#endif
    {
        /*
         * WARNING: Simulating Datum.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL::executeDatum: Sim: Going to sleep for 10 to "
            "sim datum.\n");

        sleep(10);

    }

    return( status );

} /* End of cAocCmd_GLOBAL::executeDatum() */


/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::executePark( void )
 *
 * @brief
 *   Park.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GLOBAL::executePark
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an Park command so we need to send Park to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<globalPark>> command...., .\n");
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GLOBAL,
	    AOCCMDFMT_GLOBALPARK );

    }
    else
#endif
    {
        /*
         * WARNING: Simulating Park.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL::executePark: Sim: Going to sleep for 10 to "
            "sim Park.\n");

        sleep(10);

    }

    return( status );

} /* End of cAocCmd_GLOBAL::executePark() */

/*!
 ************************************************************************
 *
 * @fn void cAocCmd_GLOBAL::executeReboot( void )
 *
 * @brief
 *   Prepare for a reboot.
 * 
 * @callgraph
 ************************************************************************
-*/
void cAocCmd_GLOBAL::executeReboot
(
    void
)
{

    GPI_DEBUG_ENTER;

#ifdef INCLUDING_AOC_CODE
    int status = GPI_ERR_NONE;
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an REBOOT command so we need to send REBOOT to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<globalReboot>> command...., .\n");
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GLOBAL_REBOOT,
	    AOCCMDFMT_GLOBALREBOOT );
    }
#endif
    /* Regardless, do the following */
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL::executeReboot: Going to execute REBOOT AOC "
            "commands.\n");
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GLOBAL::executeReboot: FOR NOW, NOT CALLING AOC "
            "SUBSYSTEM.\n");

        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_GLOBAL::executeReboot: Exitting............\n");

        /*
         *  Set the shutdownRequest flag - for no reason
         *  Destroy the shared memory area.
         */

        gmbAoc->aocRpc.stat.shutdownRequest = 1;

        exit(0);
    }
 
    return;

} /* End of cAocCmd_GLOBAL::executeReboot() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /*
     *  Call parent makeItBusy to set values in GMB 
     */
    cSubCommands::makeItBusyBroadcast();

    /*
     *  Make the appropriate part busy.  Set the gmbCmdPtr to the correct
     *  structure so that the reply can pick up the correct results.
     */
    switch ( mode )
    {
    case ( GPI_GLOBAL_INIT ) :
        CHECK_GMB_ERR( err, "ERROR, broadcasting aocCmd_GLOBAL_INIT", break );
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:makeItBusy: INIT state:<%s><%d>, id:<%d>\n", 
            gpStateStrings[gmbAoc->aocCmd_GLOBAL_INIT.state], 
            gmbAoc->aocCmd_GLOBAL_INIT.state, 
            gmbAoc->aocCmd_GLOBAL_INIT.activityId );
        break;

    case ( GPI_GLOBAL_DATUM ) :
        CHECK_GMB_ERR( err, "ERROR, broadcasting aocCmd_GLOBAL_DATUM", break );
        break;

    case ( GPI_GLOBAL_REBOOT ) :
        CHECK_GMB_ERR(err, "ERROR, broadcasting aocCmd_GLOBAL_REBOOT", break );
        break;

    case ( GPI_GLOBAL_DEBUG ) :
    case ( GPI_GLOBAL_PARK )  :
    case ( GPI_GLOBAL_TEST )  :
    case ( GPI_GLOBAL_SIM )   :
        GPI_DEBUG( GPI_DEBUG_FULL,
            "cAocCmd_GLOBAL:makeItBusy: state set to <%s><%d>, id <%d>.\n",
            gpStateStrings[gmbAoc->aocCmd_GLOBAL.state], 
            gmbAoc->aocCmd_GLOBAL.state, 
            gmbAoc->aocRpc.stat.activityId ); 
        CHECK_GMB_ERR( err, "ERROR, broadcasting aocCmd_GLOBAL", break );
        break;

    default            :
        GPI_DEBUG( GPI_DEBUG_MIN, 
            "cAocCmd_GLOBAL:makeItBusy: Invalid mode: %d, SHOULD NEVER BE "
            "HERE.\n", mode);

            subCmdGmb = (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_GLOBAL);

        status = GPI_ERR_ERROR;
        GPI_SET_STATE_GMB_ERROR( (GPI_STATUS)status, 
            gmbAoc->aocCmd_GLOBAL,
            "Error, GLOBAL mode invalid, shouldnt be here" );

        err = gpUtGmbBroadcast (gmb, 
            (void *) &(gmbAoc->aocCmd_GLOBAL),
            sizeof(gmbAoc->aocCmd_GLOBAL) );

        CHECK_GMB_ERR( err, "ERROR, broadcasting aocCmd_GLOBAL", break );
        break;
    };


    /*
     * Set the overall state and cmd counter.
     * WARNING: need to move this to gpSubserver lib.
     */

    if ( status == GPI_ERR_NONE )
    {
        gmbAoc->aocRpc.stat.globalBusy = GPI_STATE_BUSY;
        gmbAoc->aocCmd_GLOBAL.done++;
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_GLOBAL::makeItBusy: Success, made it busy.\n");

        err = gpUtGmbBroadcast (gmb, (void *) &(gmbAoc->aocRpc),
            sizeof(gmbAoc->aocRpc) );

        CHECK_GMB_ERR( err, "ERROR, broadcasting aocCmdRpc",
            return( status ) );
    }

    return( status );

} /* End of cAocCmd_GLOBAL::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GLOBAL::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GLOBAL::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* 
     * call parent makeItUnBusy to set values in GMB 
     */
    cSubCommands::makeItUnBusyBroadcast();

    /*
     * Set the overall state (globalBusy)
     * WARNING: this is not done properly, need to include health depending
     * on state, and set if state is ERROR.
     */

    if ( gmbAoc->aocCmd_GLOBAL_INIT.state   != GPI_STATE_BUSY &&
         gmbAoc->aocCmd_GLOBAL_REBOOT.state != GPI_STATE_BUSY &&
         gmbAoc->aocCmd_GLOBAL_DATUM.state  != GPI_STATE_BUSY )
    {
        gmbAoc->aocRpc.stat.globalBusy = GPI_STATE_IDLE;
    }
    else
    {
        gmbAoc->aocRpc.stat.globalBusy = GPI_STATE_BUSY;
    }

    GPI_DEBUG( GPI_DEBUG_MAX, 
        "cAocCmd_GLOBAL:makeItUnBusy: Setting GLOBAL header state "
        "to:<%s><%d>\n",
        gpStateStrings[gmbAoc->aocRpc.stat.globalBusy], 
        gmbAoc->aocRpc.stat.globalBusy );

    gmbAoc->aocCmd_GLOBAL.done--;
    gmbAoc->aocRpc.stat.overallState = (gmbAoc->aocCmd_GLOBAL.done <= 0 ) ? 
        GPI_STATE_IDLE : GPI_STATE_BUSY;

    GPI_DEBUG( GPI_DEBUG_MAX, 
        "cAocCmd_GLOBAL:makeItUnBusy: Setting overall header state "
        "to:<%s><%d>\n",
        gpStateStrings[gmbAoc->aocRpc.stat.overallState], 
        gmbAoc->aocRpc.stat.overallState );

    err = gpUtGmbBroadcast (gmb, (void *) &(gmbAoc->aocRpc),
        sizeof(gmbAoc->aocRpc) );

    err = gpUtGmbBroadcast(gmb, subCmdGmb, sizeof(GPI_SUBSERV_RPC_CMD) );
    CHECK_GMB_ERR( err, "ERROR, broadcasting to GMB", 
	return(GPI_ERR_ERROR) );

    return( status );

} /* End of cAocCmd_GLOBAL::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_CAL_ON_OFF::cAocCmd_CAL_ON_OFF(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_CAL_ON_OFF::cAocCmd_CAL_ON_OFF
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtCalOnOff_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_CAL_ON_OFF),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    on = rqst->GP_AOC_REQUEST_u.mtCalOnOff_.on;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_CAL_ON_OFF:cAocCmd_CAL_ON_OFF: CAL_ON_OFF on <%d>, "
        "state:<%d>, dir<%s>.\n", on, gmbAoc->aocCmd_CAL_ON_OFF.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_CAL_ON_OFF::cAocCmd_CAL_ON_OFF() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAL_ON_OFF::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is expected
     *  to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_CAL_ON_OFF::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "CAL_ON_OFF" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAL_ON_OFF::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_CAL_ON_OFF::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( on != 0 && on != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CAL_ON_OFF:checkDependencies: Error, Invalid "
            "setting <%d>, can on set on<1> or off<0>. \n",
            on );

        status = GPI_ERR_ERROR;
        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, on is out of range, needs to be 1 or 0" );
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_CAL_ON_OFF::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAL_ON_OFF::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */
#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_CAL_ON_OFF.done != 0 &&
           gmbAoc->aocCmd_CAL_ON_OFF.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_CAL_ON_OFF.done ))
#else
    if ( canBeDoneNow() || gmbAoc->aocCmd_CAL_ON_OFF.done != 0 )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_CAL_ON_OFF:checkIfDone: Waiting for finish..<%d>...\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_CAL_ON_OFF::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::execute(void)
 *
 * @brief
 *   Execute the "CAL_ON_OFF" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CAL_ON_OFF::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  execute a calDataOnOff command,
	 *  WARNING, don't know if this is the correct string!!!
         */

	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<calDataOnOff>> command, "
	    	"<%s>...., .\n", (on==1)?"ON":"OFF" );
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CAL_ON_OFF,
	    AOCCMDFMT_CALDATAONOFF, on );

    }
    else
#endif

    {
        /*
         * WARNING: Simulating CAL_ON_OFF
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CAL_ON_OFF::execute: Sim: Going to sleep for 1 to "
            "sim.\n");

        sleep(1);

    }/*else simulating.. */
    
    if (status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        gmbAoc->aocRpc.aoReadCalData = on;
    }

    return( status );

} /* end of cAocCmd_CAL_ON_OFF::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAL_ON_OFF::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();


    return( status );

} /* end of cAocCmd_CAL_ON_OFF::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAL_ON_OFF::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAL_ON_OFF::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();


    return( status );

}  /* end of cAocCmd_CAL_ON_OFF::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_CAMERA_RATE::cAocCmd_CAMERA_RATE(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_CAMERA_RATE::cAocCmd_CAMERA_RATE
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtCameraRate_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_CAMERA_RATE),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    /*
     *  Save local command information:
     */

    autoCamRate = rqst->GP_AOC_REQUEST_u.mtCameraRate_.autoCamRate;
    setCamRate = rqst->GP_AOC_REQUEST_u.mtCameraRate_.setCamRate;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_CAMERA_RATE:cAocCmd_CAMERA_RATE: CAMERA_RATE state:<%d>, "
	"dir<%s>.\n",
        gmbAoc->aocCmd_CAMERA_RATE.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_CAMERA_RATE::cAocCmd_CAMERA_RATE() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAMERA_RATE::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_CAMERA_RATE::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "CAMERA_RATE" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAMERA_RATE::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    if ( gmbAoc->aocRpc.stat.activityDir == CANCEL )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cIfsCmd_CAMERA_RATE:checkDependencies: CANCEL directive "
            "is a NOOP.\n");
    }


    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_CAMERA_RATE::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
   /*
    * Add checking of the camera rate here.
    */

    else if ( autoCamRate != 0 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CAMERA_RATE:checkDependencies: Error, autoCamRate out "
            "of range, <%d>, needs to be 0=Manual, Auto is not supported.\n", 
            autoCamRate );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Only support Manual setting." );
    }
   /*
    * In manual mode, then check that rate is 500,1000,1500, or 2000.
    */

    else if ( autoCamRate == 0 && 
        ( setCamRate != GPI_AOC_CAMERA_RATE_250 &&
          setCamRate != GPI_AOC_CAMERA_RATE_500 &&
          setCamRate != GPI_AOC_CAMERA_RATE_1000 &&
          setCamRate != GPI_AOC_CAMERA_RATE_1500 &&
          setCamRate != GPI_AOC_CAMERA_RATE_2000 )  )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CAMERA_RATE:checkDependencies: Error, in manual mode "
            "the camera rate is out of range, "
            "<%d>, needs to be 250, 500, 1000, 1500, or 2000.\n",
            setCamRate );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Manual camera rate is not 250,500,1000,1500,or 2000." );
    }
    /*
     *  Cannot be currently correcting to change camera rate.
     */
    else if ( (status = currentlyCorrectingAoc() ) )
    {
        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, currently correcting, loop closed, with Aoc.." );
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_CAMERA_RATE::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAMERA_RATE::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_CAMERA_RATE.done != 0 &&
           gmbAoc->aocCmd_CAMERA_RATE.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_CAMERA_RATE.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done != 0 )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_CAMERA_RATE:checkIfDone: Waiting for finish..<%d>..\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_CAMERA_RATE::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::execute(void)
 *
 * @brief
 *   Execute the "CAMERA_RATE" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CAMERA_RATE::execute
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    /*
     * Call only aocSetCamRate() , because "aocAutoCamRate()" not implemented.
     * Then update GMB.
     */

    gmbAoc->aocRpc.cameraAutoModeSet = autoCamRate; /* Set GMB */


    #ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
    /*
     *  Send cmd to AOC Subsystem
     *
     *  Uncomment this if autoCamRate is implemented...
     *
    GPI_DEBUG( GPI_DEBUG_MIN,
	":: Going to execute AOC <<%s>> command....,rate<%d> .\n",
	AOCCMDFMT_AUTOCAMRATE, (autoCamRate)?1:0 );

    status = gpAocSockExecute ( 
	(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CAMERA_RATE,
	AOCCMDFMT_AUTOCAMRATE, (autoCamRate)?1:0 );
     */

        if ( status == GPI_ERR_NONE && autoCamRate ==0 )
        {
	    GPI_DEBUG( GPI_DEBUG_MIN,
	        ":: Going to execute AOC <<%s>> command....,rate<%d> .\n",
		AOCCMDFMT_SETCAMRATE, setCamRate);
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CAMERA_RATE,
	        AOCCMDFMT_SETCAMRATE, setCamRate );
        }
    }
    else
    #endif
    {
        /*
         * Simulating, just sleep for a few seconds.
         */
        GPI_DEBUG( GPI_DEBUG_MIN,
		"cAocCmd_CAMERA_RATE:execute: Simulating, sleeping for 3.\n");

	sleep(3);
    }

    if ( status == GPI_ERR_NONE && activityDir != CANCEL )
    {
	gmbAoc->aocRpc.cameraAutoModeSet = autoCamRate;/* Set GMB */
	if (autoCamRate == 0 )
	{
	    gmbAoc->aocRpc.cameraManualRate = setCamRate;/* Set GMB */
	}
    }

    GPI_DEBUG( GPI_DEBUG_MIN,
        "cAocCmd_CAMERA_RATE:execute: <<CAMERA_RATE>> set mode to<%s>, "
        "rate<%d>, dir<%s>.\n", (autoCamRate)?"AUTO":"MANUAL", 
        gmbAoc->aocRpc.cameraManualRate , 
	gpActivityStrings[activityDir] );

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_CAMERA_RATE::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAMERA_RATE::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_CAMERA_RATE::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CAMERA_RATE::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CAMERA_RATE::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_CAMERA_RATE::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_CLOSE_OPEN_LOOP::cAocCmd_CLOSE_OPEN_LOOP(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_CLOSE_OPEN_LOOP::cAocCmd_CLOSE_OPEN_LOOP
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtCloseOpenLoop_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_CLOSE_OPEN_LOOP),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: .
     */

    tweeter = rqst->GP_AOC_REQUEST_u.mtCloseOpenLoop_.tweeter;
    wooffer = rqst->GP_AOC_REQUEST_u.mtCloseOpenLoop_.wooffer;
    tipTilt = rqst->GP_AOC_REQUEST_u.mtCloseOpenLoop_.tipTilt;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_CLOSE_OPEN_LOOP:cAocCmd_CLOSE_OPEN_LOOP: CLOSE_OPEN_LOOP"
        "state:<%d>, dir<%s>.\n", gmbAoc->aocCmd_CLOSE_OPEN_LOOP.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_CLOSE_OPEN_LOOP::cAocCmd_CLOSE_OPEN_LOOP() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CLOSE_OPEN_LOOP::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* No can be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_CLOSE_OPEN_LOOP::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "CLOSE_OPEN_LOOP" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CLOSE_OPEN_LOOP::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_CLOSE_OPEN_LOOP::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( tweeter != 0 && tweeter != -1 && tweeter != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CLOSE_OPEN_LOOP:checkDependencies: Error, invalid value in "
	    "tweeter value<%d>, needs to be 0, 1, or -1.\n",
            tweeter );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, wooffer value invalid, needs to be 0, 1, or -1." );
    }
    else if ( wooffer != 0 && wooffer != -1 && wooffer != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CLOSE_OPEN_LOOP:checkDependencies: Error, invalid value in "
	    "wooffer value<%d>, needs to be 0, 1, or -1.\n",
            wooffer );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, wooffer value invalid, needs to be 0, 1, or -1." );
    }
    else if ( tipTilt != 0 && tipTilt != -1 && tipTilt != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CLOSE_OPEN_LOOP:checkDependencies: Error, invalid value in "
	    "tipTilt value<%d>, needs to be 0, 1, or -1.\n",
            tipTilt );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, tipTilt value invalid, needs to be 0, 1, or -1." );
    }


    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_CLOSE_OPEN_LOOP::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CLOSE_OPEN_LOOP::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_CLOSE_OPEN_LOOP.done != 0 &&
           gmbAoc->aocCmd_CLOSE_OPEN_LOOP.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_CLOSE_OPEN_LOOP.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_CLOSE_OPEN_LOOP:checkIfDone: Is done." );
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_CLOSE_OPEN_LOOP:checkIfDone: Waiting for finish."
            "<%d>...\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_CLOSE_OPEN_LOOP::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::execute(void)
 *
 * @brief
 *   Execute the "CLOSE_OPEN_LOOP" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CLOSE_OPEN_LOOP::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
    int status = GPI_ERR_NONE;
    
    /* Open all loops on cancel */
    if ( gmbAoc->aocRpc.stat.activityDir == CANCEL )
    {
        tweeter = 0;
	wooffer  = 0;
	tipTilt = 0;
    }

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an CLOSE_OPEN command 
         *  This is not a command that can be completed immediately.
	 *  Determine which ones to do based on passed in val's.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<closeOpenLoops>> command....,"
	    "tweet<%d>, woof<%d>, tt<%d> .\n",
	    tweeter, wooffer, tipTilt);
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CLOSE_OPEN_LOOP,
	    AOCCMDFMT_CLOSEOPENLOOPS, tweeter, wooffer, tipTilt );

        /* if status is ok, then set the GMB */
        if( status == GPI_ERR_NONE )
        {
	    gmbAoc->aocRpc.tweeterLoop = tweeter;
	    gmbAoc->aocRpc.wooferLoop  = wooffer;
	    gmbAoc->aocRpc.tipTiltLoop = tipTilt;
        }
    }
    else
#endif

    {
        /*
         * WARNING: Simulating CLOSE_OPEN_LOOP
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CLOSE_OPEN_LOOP::execute: Sim: Going to sleep "
            "for 1 to sim.\n");


        sleep(1);

    }/*else simulating.. */

    /*
     * if status is ok, then set the GMB 
     */
    if( status == GPI_ERR_NONE )
    {
	gmbAoc->aocRpc.tweeterLoop = tweeter;
	gmbAoc->aocRpc.wooferLoop  = wooffer;
	gmbAoc->aocRpc.tipTiltLoop = tipTilt;
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CLOSE_OPEN_LOOP::execute: Successful, setting "
	    " tweeter<%d>, wooffer<%d>, tiptilt<%d>, isBlocking<%d>.\n",
	    tweeter, wooffer, tipTilt,isBlocking );
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;


    return( status);

} /* end of cAocCmd_CLOSE_OPEN_LOOP::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CLOSE_OPEN_LOOP::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_CLOSE_OPEN_LOOP::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CLOSE_OPEN_LOOP::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CLOSE_OPEN_LOOP::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_CLOSE_OPEN_LOOP:cAocCmd_CLOSE_OPEN_LOOP:makeItUnBusy.\n");

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();


    return( status );

}  /* end of cAocCmd_CLOSE_OPEN_LOOP::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_CYCLE_ACTUATORS::cAocCmd_CYCLE_ACTUATORS(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_CYCLE_ACTUATORS::cAocCmd_CYCLE_ACTUATORS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtCycleAct_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_CYCLE_ACTUATORS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    actuator = rqst->GP_AOC_REQUEST_u.mtCycleAct_.actuator;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_CYCLE_ACTUATORS:cAocCmd_CYCLE_ACTUATORS: "
        "CYCLE_ACTUATORS state:<%d>, actuator<%d>, dir<%s>.\n", 
	gmbAoc->aocCmd_CYCLE_ACTUATORS.state,
	actuator,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_CYCLE_ACTUATORS::cAocCmd_CYCLE_ACTUATORS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CYCLE_ACTUATORS::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_CYCLE_ACTUATORS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "CYCLE_ACTUATORS" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CYCLE_ACTUATORS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
    
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_CYCLE_ACTUATORS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( gmbAoc->aocRpc.stat.activityDir != CANCEL && ( actuator < -4
    	&& actuator > 4095 ) && (actuator < 5000 && actuator > 5121 ) &&
	(actuator != 6000 && actuator != 60001) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CYCLE_ACTUATORS:checkDependencies: Error, invalid value for "
	    "actuator<%d>, needs to be -4to4095, 5000to5121, 6000, or 6001.\n",
            actuator );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, "Error, bad actuator "
		"value, should be -4to4095, 5000to5121, 6000 or 6001.");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_CYCLE_ACTUATORS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CYCLE_ACTUATORS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_CYCLE_ACTUATORS.done != 0 &&
           gmbAoc->aocCmd_CYCLE_ACTUATORS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_CYCLE_ACTUATORS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_CYCLE_ACTUATORS:checkIfDone: Waiting for "
            "finish...<%d>....\n", numTimes );
    }
    return( done );

} /* end of cAocCmd_CYCLE_ACTUATORS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::execute(void)
 *
 * @brief
 *   Execute the "CYCLE_ACTUATORS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_CYCLE_ACTUATORS::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  Execute CYCLE_ACTUATORS command,
         *  This is not a command that can be completed immediately.
         */
        GPI_DEBUG( GPI_DEBUG_NONE, 
            "cAocCmd_CYCLE_ACTUATORS::execute : About to do "
            "CYCLE_ACTUATORS command.\n");
	if ( activityDir == CANCEL )
	{
	    /*
	     *  WARNING, this may not work when we do cancel
	     */

	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CYCLE_ACTUATORS,
		AOCCMDFMT_STOPCYCLING );
	}
	else
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_CYCLE_ACTUATORS,
		AOCCMDFMT_CYCLEACTUATOR, actuator );
	}

    }
    else
#endif
    {
        /*
         * WARNING: Simulating CYCLE_ACTUATORS
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_CYCLE_ACTUATORS::execute: Sim: Going to sleep "
            "for 1 to sim.\n");

        sleep(1);

    }/*else simulating.. */
    
    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE  && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
	/* if stopCycling command */
        if ( activityDir == CANCEL )
	    gmbAoc->aocRpc.stopCycling = 1;
	else /* else, a cycleActuator command */
	{
	    gmbAoc->aocRpc.stopCycling = 0;
	    gmbAoc->aocRpc.cycleActuatorNum = actuator;
	}
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status);

} /* end of cAocCmd_CYCLE_ACTUATORS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CYCLE_ACTUATORS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_CYCLE_ACTUATORS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_CYCLE_ACTUATORS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_CYCLE_ACTUATORS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_CYCLE_ACTUATORS::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_DISPLAY::cAocCmd_DISPLAY(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_DISPLAY::cAocCmd_DISPLAY
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtDisplay_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_DISPLAY),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_DISPLAY:cAocCmd_DISPLAY: DISPLAY state:<%d>,"
	"dir<%s>.\n",
        gmbAoc->aocCmd_DISPLAY.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_DISPLAY::cAocCmd_DISPLAY() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DISPLAY::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_DISPLAY::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "DISPLAY" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DISPLAY::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_DISPLAY::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_DISPLAY::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_DISPLAY::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;


    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_DISPLAY.done != 0 &&
           gmbAoc->aocCmd_DISPLAY.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_DISPLAY.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_DISPLAY:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_DISPLAY::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::execute(void)
 *
 * @brief
 *   Execute the "DISPLAY" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_DISPLAY::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an DISPLAY command so we need to send DISPLAY to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<image>> command...., .\n");
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_DISPLAY,
	    AOCCMDFMT_IMAGES );

    }
    else
#endif
    {
        /*
         * WARNING: Simulating DISPLAY
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DISPLAY::execute: Sim: Going to sleep "
            "for 1 to sim.\n");

        sleep(1);

    }/*else simulating.. */

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_DISPLAY::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DISPLAY::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();


    return( status );

} /* end of cAocCmd_DISPLAY::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DISPLAY::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DISPLAY::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_DISPLAY::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_DUMP::cAocCmd_DUMP(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_DUMP::cAocCmd_DUMP
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtDump_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_DUMP),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    /*
     *  Save local command information: 
     */

    type = rqst->GP_AOC_REQUEST_u.mtDump_.type;
    seconds = rqst->GP_AOC_REQUEST_u.mtDump_.seconds;
    decimation = rqst->GP_AOC_REQUEST_u.mtDump_.decimation;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_DUMP:cAocCmd_DUMP: DUMP state:<%d>, dir<%s>.\n",
        gmbAoc->aocCmd_DUMP.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_DUMP::cAocCmd_DUMP() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DUMP::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* NO cannot be done now.       */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_DUMP::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "DUMP" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DUMP::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_DUMP::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

   /*
    *  Cancel's are not allowed - for now.
    */

    else if ( gmbAoc->aocRpc.stat.activityDir == CANCEL )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DUMP:checkDependencies: Error, can't cancel.\n");

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, can't cancel." );
    }

   /*
    *  If already busy, then fail.
    */

    else if ( gmbAoc->aocCmd_DUMP.state == GPI_STATE_BUSY )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DUMP:checkDependencies: Error, already busy.\n");

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Already dumping." );
    }

   /*
    * Check the type of dump.
    */

    else if ( type < 0 || type > 134217728 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DUMP:checkDependencies: Error, type of data request "
            "out of range, <%d>, needs to be >0 or < 134217728 .\n", 
            type );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Data Type request out of range." );
    }

   /*
    * Else if number of seconds out of range.
    */
    else if ( seconds < GPI_AOC_MIN_DUMP_SECONDS || 
            seconds > GPI_AOC_MAX_DUMP_SECONDS )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DUMP:checkDependencies: Error, number of seconds out "
            "of range, <%d>, needs to be >%d or <%d .\n", 
            seconds, GPI_AOC_MIN_DUMP_SECONDS, GPI_AOC_MAX_DUMP_SECONDS );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Number of seconds for dump request out of range." );
    }

   /*
    * Else Check that decimation value is within range.
    */
    else if ( decimation < GPI_AOC_MIN_DUMP_DEC || 
            decimation > GPI_AOC_MAX_DUMP_DEC )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_DUMP:checkDependencies: Error, number of decimation "
            " out of range, <%d>, needs to be >%d or <%d .\n", 
            seconds, GPI_AOC_MIN_DUMP_DEC, GPI_AOC_MAX_DUMP_DEC );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Number of decimation for dump request out of range." );
    }
   
    if ( status == GPI_ERR_NONE && 
        gmbAoc->aocRpc.stat.activityDir != PRESET )
    {
        /*
         *  Remove this when real aoc is there.
         *  This is a counter to fake the dumping of data.
         */

        dummyCounter = 0;
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_DUMP::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DUMP::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;


    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_DUMP.done != 0 &&
           gmbAoc->aocCmd_DUMP.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_DUMP.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        if ( aocCheckIfDumpIsDone()  )
            done = 1;
    }
    else
    {
    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocCmd_:checkIfDone: Waiting for finish...<%d>.."
        "dummy<%d>..\n",
        numTimes , dummyCounter );
    }

    return( done );

} /* end of cAocCmd_DUMP::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::execute(void)
 *
 * @brief
 *   Execute the "DUMP" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_DUMP::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
    
    int status = GPI_ERR_NONE;

    if ( gmbAoc->aocRpc.stat.activityDir != CANCEL) 
    {
	GPI_DEBUG( GPI_DEBUG_MIN,
	    "cAocCmd_DUMP:execute: <<DUMP>>, type<%d>, second<%d>, "
	    "decimation<%d>, about to start...\n", 
	    type, seconds, decimation );

#ifdef INCLUDING_AOC_CODE
	if ( !gmbAoc->aocRpc.stat.simulate )
	{
	    GPI_DEBUG( GPI_DEBUG_MIN,
		":: Going to execute AOC dumpData command...., .\n");
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_DUMP,
		AOCCMDFMT_DUMPDATA, type, seconds, decimation );
		//"dumpData 32767");
	}
	else
#endif
	{
	    /*
	     * Simulating, just sleep for 20 seconds.
	     */

	    GPI_DEBUG( GPI_DEBUG_MAX,
		"cAocCmd_DUMP:execute: Simulating, sleeping for 20.\n");
	    sleep(20);
	}

    }

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE  && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        gmbAoc->aocRpc.dumpType = type;
        gmbAoc->aocRpc.dumpSeconds = seconds;
        gmbAoc->aocRpc.dumpDecimation = decimation;
        err = gpUtGmbBroadcast (gmb, (void *) &(gmbAoc->aocRpc),
	    sizeof(gmbAoc->aocRpc) );
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_DUMP::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DUMP::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_DUMP::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_DUMP::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_DUMP::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_DUMP::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_ENV_CONFIG::cAocCmd_ENV_CONFIG(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_ENV_CONFIG::cAocCmd_ENV_CONFIG
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtEnvConfig_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_ENV_CONFIG),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_ENV_CONFIG:cAocCmd_ENV_CONFIG: ENV_CONFIG "
	"state:<%d>, dir<%s>.\n",
        gmbAoc->aocCmd_ENV_CONFIG.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_ENV_CONFIG::cAocCmd_ENV_CONFIG() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_ENV_CONFIG::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_ENV_CONFIG::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "ENV_CONFIG" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_ENV_CONFIG::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_ENV_CONFIG::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_ENV_CONFIG::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_ENV_CONFIG::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_ENV_CONFIG.done != 0 &&
           gmbAoc->aocCmd_ENV_CONFIG.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_ENV_CONFIG.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_ENV_CONFIG::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::execute(void)
 *
 * @brief
 *   Execute the "ENV_CONFIG" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_ENV_CONFIG::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;

    /* update GMB if status is ok, and activityDir is not CANCEL */
/*    if( status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        gmbAoc->aocRpc.type = type;
        gmbAoc->aocRpc.rate = rate;
        gmbAoc->aocRpc.samples = samples;
        gmbAoc->aocRpc.location = location;
    }
*/
#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
/*    AT THE MOMENT THERE IS NO ENV_CONFIG COMMAND IN THE ICD! */
        /*
         *  This is an ENV CONFIG command so we need to send ENV CONFIG to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: WARNING, NOT IMPLEMENTED!!!!!!! \n");

    }
    else
#endif
    {
        /*
         * WARNING: Simulating ENV_CONFIG
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_ENV_CONFIG::execute: Sim: Going to sleep "
            "for 1 to sim.\n");

        sleep(1);

    }/*else simulating.. */

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_ENV_CONFIG::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_ENV_CONFIG::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_ENV_CONFIG::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_ENV_CONFIG::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_ENV_CONFIG::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_ENV_CONFIG::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_M1_M2_ON_OFF::cAocCmd_M1_M2_ON_OFF(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_M1_M2_ON_OFF::cAocCmd_M1_M2_ON_OFF
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtM1M2OnOff_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_M1_M2_ON_OFF),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    m1 = rqst->GP_AOC_REQUEST_u.mtM1M2OnOff_.m1;
    m2 = rqst->GP_AOC_REQUEST_u.mtM1M2OnOff_.m2;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_M1_M2_ON_OFF:cAocCmd_M1_M2_ON_OFF: M1_M2_ON_OFF "
        "state:<%d>, m1<%d>, m2<%d>, dir<%s>.\n", 
	gmbAoc->aocCmd_M1_M2_ON_OFF.state,
	m1, m2,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_M1_M2_ON_OFF::cAocCmd_M1_M2_ON_OFF() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_M1_M2_ON_OFF::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* Yes can be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_M1_M2_ON_OFF::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "M1_M2_ON_OFF" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_M1_M2_ON_OFF::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_M1_M2_ON_OFF::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( m1 != -1 && m1 != 0 && m1 != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_M1_M2_ON_OFF:checkDependencies: Error, invalid value for "
	    "m1<%d>, needs to be 0, 1, or -1.\n", m1 );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, m1 value invalid, needs to be 0, 1, or -1." );
    }
    else if ( m2 != -1 && m2 != 0 && m2 != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_M1_M2_ON_OFF:checkDependencies: Error, invalid value for "
	    "m2<%d>, needs to be 0, 1, or -1.\n", m2);

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, m2 value invalid, needs to be 0, 1, or -1." );
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_M1_M2_ON_OFF::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_M1_M2_ON_OFF::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_M1_M2_ON_OFF.done != 0 &&
           gmbAoc->aocCmd_M1_M2_ON_OFF.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_M1_M2_ON_OFF.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
    done = 1;
    }
    else
    {
    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
        numTimes );
    }


    return( done );

} /* end of cAocCmd_M1_M2_ON_OFF::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::execute(void)
 *
 * @brief
 *   Execute the "M1_M2_ON_OFF" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_M1_M2_ON_OFF::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an M1 M2 On off command so we need to send offload to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
            "::cAocCmd_M1_M2_ON_OFF::execute: Going to execute AOC "
	    "<<offload>> command, m1<%d>m2<%d>.... .\n", 
	    m1, m2);

	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_M1_M2_ON_OFF,
	    AOCCMDFMT_OFFLOADONOFF, m1, m2 );
    }
    else
#endif
    {
        /*
         * WARNING: Simulating Initialize.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_M1_M2_ON_OFF::execute: Sim: Going to sleep for 20 to "
            "sim .\n");

        sleep(20);

    }

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE  && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        gmbAoc->aocRpc.m1action = m1;
        gmbAoc->aocRpc.m2action = m2;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_M1_M2_ON_OFF::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_M1_M2_ON_OFF::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_M1_M2_ON_OFF::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_M1_M2_ON_OFF::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_M1_M2_ON_OFF::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_M1_M2_ON_OFF::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_OBSERV_CONFIG::cAocCmd_OBSERV_CONFIG(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_OBSERV_CONFIG::cAocCmd_OBSERV_CONFIG
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtObservConfig_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_OBSERV_CONFIG),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    magnitudeI  = rqst->GP_AOC_REQUEST_u.mtObservConfig_.starMag;
    r0Estimate  = rqst->GP_AOC_REQUEST_u.mtObservConfig_.r0Estimate;
    camRate     = rqst->GP_AOC_REQUEST_u.mtObservConfig_.camRate;
    darkSelect  = rqst->GP_AOC_REQUEST_u.mtObservConfig_.darkSelect;
    refSelect   = rqst->GP_AOC_REQUEST_u.mtObservConfig_.refSelect;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_OBSERV_CONFIG:cAocCmd_OBSERV_CONFIG: OBSERV_CONFIG "
        "state:<%d>, dir<%s>.\n", 
	gmbAoc->aocCmd_OBSERV_CONFIG.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_OBSERV_CONFIG::cAocCmd_OBSERV_CONFIG() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_OBSERV_CONFIG::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_OBSERV_CONFIG::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "OBSERV_CONFIG" command.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_OBSERV_CONFIG::checkDependencies
(
    void
)
{
/*
 *  Putting this here so that you can see what they are easily.
 *  move to global place later.
 */


    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_OBSERV_CONFIG::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    /*
     * If directive is CANCEL, then it's a NOOP
     */

    else if ( gmbAoc->aocRpc.stat.activityDir == CANCEL )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: CANCEL directive "
            "is a NOOP.\n");
    }
    /*
     *  If correcting then can't change these parameters.
     */

    else if ( (status = currentlyCorrectingAoc() ) )
    {
        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, currently correcting, loop closed, with Aoc.." );
    }

    /*
     *  WARNING, put check in here when we know ranges.
     *
     *  Check the range of magnitude R & I
     */

    else if ( magnitudeI < 0.0 || magnitudeI >  GPI_AOC_MAX_STAR_MAG )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: Error, OBSERV_CONFIG "
            "can't be set because star magnitude I<%f> is below "
            "zero or ><%f>.\n",
            magnitudeI, GPI_AOC_MAX_STAR_MAG );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, star magnitude I is out of range" );
    }   
    else if ( r0Estimate < 0.0 && r0Estimate > GPI_AOC_MAX_R0_ESIMATE )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: Error, OBSERV_CONFIG "
            "can't be set because r0Estimate<%f> is below zero, and "
            "more than <%f>.\n",
            r0Estimate, GPI_AOC_MAX_R0_ESIMATE  );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, r0Estimate is less than 0" );
    }   
    else if ( camRate != GPI_AOC_CAMERA_RATE_250 &&
          camRate != GPI_AOC_CAMERA_RATE_500 &&
          camRate != GPI_AOC_CAMERA_RATE_1000 &&
          camRate != GPI_AOC_CAMERA_RATE_1500 &&
          camRate != GPI_AOC_CAMERA_RATE_2000 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: Error, "
            "camera rate is out of range, "
            "<%d>, needs to be 250, 500, 1000, 1500, or 2000.\n",
            camRate );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, Camera rate is out of range." );
    }
    else if ( darkSelect < 0 || darkSelect > 2 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: Error, OBSERV_CONFIG "
            "can't be set because darkSelect<%d> is below zero, or > 2.\n",
            darkSelect );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, darkSelect is out of range, 0-2" );
    }   
    else if ( refSelect < 0 || refSelect > GPI_AOC_MAX_REFSELECT )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:checkDependencies: Error, OBSERV_CONFIG "
            "can't be set because refSelect<%d> is below zero.\n",
            refSelect );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, refSelect is out of range, less than 0 or > 0x80" );
    }   

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_OBSERV_CONFIG::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_OBSERV_CONFIG::checkIfDone
(
    int numTimes
)
{
    int done = 1;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_OBSERV_CONFIG.done != 0 &&
           gmbAoc->aocCmd_OBSERV_CONFIG.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_OBSERV_CONFIG.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_OBSERV_CONFIG::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::execute(void)
 *
 * @brief
 *   Execute the "OBSERV_CONFIG" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OBSERV_CONFIG::execute
(
    void
)
{ 
    int status = GPI_ERR_NONE;
 
    GPI_DEBUG_ENTER;
 
    if ( gmbAoc->aocRpc.stat.activityDir != CANCEL) 
    {
        /*
         *  Save the  information to GMB - 
         *      don't care if it's simulate or not.
         */
        gmbAoc->aocRpc.magnitudeI= magnitudeI;
        gmbAoc->aocRpc.r0Estimate= r0Estimate;
        gmbAoc->aocRpc.camRate   = camRate;
        gmbAoc->aocRpc.darkSelect= darkSelect;
        gmbAoc->aocRpc.refSelect = refSelect;

	/*
	 *  Now saw to the AOC porition of it.
	 */
	gmbAoc->ObservationConfigParms.R0_estimate = r0Estimate;
	gmbAoc->ObservationConfigParms.Star_I_magnitude = magnitudeI;
	gmbAoc->ObservationConfigParms.cameraRate = camRate;
	gmbAoc->ObservationConfigParms.useDarkOrBack = darkSelect;
	gmbAoc->ObservationConfigParms.setRefCentValue = refSelect;


        err = gpUtGmbBroadcast (gmb, (void *) &(gmbAoc->aocRpc),
            sizeof(gmbAoc->aocRpc) );
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OBSERV_CONFIG:execute: <<OBSERV_CONFIG>>, ro<%f>, "
            "mag<%f>, camRate<%d>, darkSelect<%d>, refSelect<%d>.\n",
            r0Estimate, magnitudeI, camRate, darkSelect, refSelect );

#ifdef INCLUDING_AOC_CODE
	if ( ! gmbAoc->aocRpc.stat.simulate )
	{
	    /*
	     *  This is an OBSERV CONFIG command 
	     *      AOC Subsystem
	     *  This is not a command that can be completed immediately.
	     */
	    GPI_DEBUG( GPI_DEBUG_MIN,
		"::cAocCmd_OBSERV_CONFIG::execute: Going to execute AOC "
		"<<observ config>> command >.... .\n" );
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_OBSERV_CONFIG,
		AOCCMDFMT_SETOBSERVATIONCONFIGURATION );
	}
#endif
    }
    else
    {
	GPI_DEBUG( GPI_DEBUG_MIN,
	    "::cAocCmd_OBSERV_CONFIG::execute: Invalid, can't cancel.\n");
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_OBSERV_CONFIG::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OBSERV_CONFIG::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_OBSERV_CONFIG::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OBSERV_CONFIG::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OBSERV_CONFIG::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_OBSERV_CONFIG::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_OPTIMIZE::cAocCmd_OPTIMIZE(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_OPTIMIZE::cAocCmd_OPTIMIZE
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtOptimize_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_OPTIMIZE),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    useOptimized = rqst->GP_AOC_REQUEST_u.mtOptimize_.useOptimized;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_OPTIMIZE:cAocCmd_OPTIMIZE: OPTIMIZE state:<%d>,dir<%s>.\n",
        gmbAoc->aocCmd_OPTIMIZE.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_OPTIMIZE::cAocCmd_OPTIMIZE() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_OPTIMIZE::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "OPTIMIZE" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_OPTIMIZE::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    /*
     * Valid values are: Off, On
     */

    else if ( useOptimized != 0 && useOptimized != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_OPTIMIZED:checkDependencies: WARNING: useOptimized "
            "not 1 or 0, setting to 0\n");

        useOptimized = 0;
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_OPTIMIZE::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_OPTIMIZE.done != 0 &&
           gmbAoc->aocCmd_OPTIMIZE.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_OPTIMIZE.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
    done = 1;
    }
    else
    {
    GPI_DEBUG( GPI_DEBUG_MAX,
        "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
        numTimes );
    }

    return( done );

} /* end of cAocCmd_OPTIMIZE::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::execute(void)
 *
 * @brief
 *   Execute the "OPTIMIZE" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;
    /*
     *  Set optimized.
     */
    gmbAoc->aocRpc.optimize = useOptimized;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an Optimize command 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
            "::cAocCmd_OPTIMIZE::execute: Going to execute AOC "
	    "<<closeOpenOFS>> command, .... .\n" );
	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_OPTIMIZE,
	    AOCCMDFMT_CLOSEOPENOFC, useOptimized );
    }
    else
#endif
    {
        /*
         * WARNING: Simulating optimized.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_OPTIMIZE::execute: Sim: Going to sleep for 20 to "
            "sim .\n");

        sleep(20);

    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_OPTIMIZE::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_OPTIMIZE::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_OPTIMIZE::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_OPTIMIZE::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_OPTIMIZE::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_PERFORM::cAocCmd_PERFORM(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_PERFORM::cAocCmd_PERFORM
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtPerform_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_PERFORM),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_PERFORM:cAocCmd_PERFORM: PERFORM state:<%d>, dir<%s>.\n",
        gmbAoc->aocCmd_PERFORM.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_PERFORM::cAocCmd_PERFORM() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_PERFORM::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "PERFORM" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_PERFORM::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_PERFORM::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_PERFORM.done != 0 &&
           gmbAoc->aocCmd_PERFORM.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_PERFORM.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done != 0 )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_PERFORM::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::execute(void)
 *
 * @brief
 *   Execute the "PERFORM" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::execute
(
    void
)
{

    GPI_DEBUG_ENTER;
 

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = GPI_ERR_NONE;

    return GPI_ERR_NONE;

} /* end of cAocCmd_PERFORM::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_PERFORM::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_PERFORM::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_PERFORM::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_PERFORM::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_RECV_SET_REF_CENT::cAocCmd_RECV_SET_REF_CENT(
 *      GP_AOC_REQUEST *, GP_AOC_REQUEST * )
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_RECV_SET_REF_CENT::cAocCmd_RECV_SET_REF_CENT
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtRecvSetRefCentroids_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_RECV_SET_REF_CENT),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    set = rqst->GP_AOC_REQUEST_u.mtRecvSetRefCentroids_.set;
    type = rqst->GP_AOC_REQUEST_u.mtRecvSetRefCentroids_.type;
    strncpy( filename, 
	rqst->GP_AOC_REQUEST_u.mtRecvSetRefCentroids_.filename,
	GPI_MAX_NAME_LEN );

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_RECV_SET_REF_CENT:cAocCmd_RECV_SET_REF_CENT: "
        "RECV_SET_REF_CENT state:<%d>, dir<%s>.\n",
        gmbAoc->aocCmd_RECV_SET_REF_CENT.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_RECV_SET_REF_CENT::cAocCmd_RECV_SET_REF_CENT() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_RECV_SET_REF_CENT::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_RECV_SET_REF_CENT::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "RECV_SET_REF_CENT" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_RECV_SET_REF_CENT::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_RECV_SET_REF_CENT::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    /* Should we check to make sure setRefCent inputs are valid ??? */

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_RECV_SET_REF_CENT::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_RECV_SET_REF_CENT::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_RECV_SET_REF_CENT.done != 0 &&
           gmbAoc->aocCmd_RECV_SET_REF_CENT.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_RECV_SET_REF_CENT.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_RECV_SET_REF_CENT::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::execute(void)
 *
 * @brief
 *   Execute the "RECV_SET_REF_CENT" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_RECV_SET_REF_CENT::execute
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an RECV_SET_ref command
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
            "::cAocCmd_RECV_SET_REF_CENT::execute: Going to execute AOC "
	    "<<RECV_SET_REF_CENT>> command.... .\n" );
	/*
	 *	    receiveRefCents		A(filename)	all loops open
	 *	    receiveRefCentsCal	A(filename)	anytime
	 *	    receiveRefCentsMap	A(filename)	anytime
	 *	    setRefCents		A(int)		anytime
	 */
	 
	 if ( !set && type == CAL_REPHASE_REF_CENT )
	 {
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_RECV_SET_REF_CENT),
		AOCCMDFMT_RECEIVEREFCENTSCAL, filename );
	 }
	 else if ( !set && type == PHASE_MAP_REF_CENT )
	 {
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_RECV_SET_REF_CENT),
		AOCCMDFMT_RECEIVEREFCENTSMAP, filename );
	 }
	else if ( !set && type == RECEIVED_REF_CENT )
	 {
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_RECV_SET_REF_CENT),
		AOCCMDFMT_RECEIVEREFCENTS, filename );
	 }
	 else if ( set )
	 {
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_RECV_SET_REF_CENT),
		AOCCMDFMT_SETREFCENTS, type );
	 }
    }
    else
#endif
    {
        /*
         * WARNING: Simulating Initialize.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_RECV_SET_REF_CENT::execute: Sim: Going to sleep for 20 to "
            "sim .\n");

        sleep(20);

    }

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
    	if ( set )
            gmbAoc->aocRpc.refCentSetting = type;
	else if ( !set )
	{
	    gmbAoc->aocRpc.typeRefCentroids = type;
            strncpy( (char *)gmbAoc->aocRpc.refCentroidsFilename, filename,
	            GPI_MAX_NAME_LEN );
	}
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_RECV_SET_REF_CENT::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_RECV_SET_REF_CENT::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_RECV_SET_REF_CENT::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_RECV_SET_REF_CENT::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_RECV_SET_REF_CENT::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_RECV_SET_REF_CENT::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_REF_CENTROIDS::cAocCmd_REF_CENTROIDS(GP_AOC_REQUEST *, 
 *      GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_REF_CENTROIDS::cAocCmd_REF_CENTROIDS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtRefCentroids_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_REF_CENTROIDS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    measure = rqst->GP_AOC_REQUEST_u.mtRefCentroids_.measure;
    offset1 = rqst->GP_AOC_REQUEST_u.mtRefCentroids_.offset1;
    offset2 = rqst->GP_AOC_REQUEST_u.mtRefCentroids_.offset2;



    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_REF_CENTROIDS:cAocCmd_REF_CENTROIDS: REF_CENTROIDS "
        "state:<%d>, measure<%d>, dir<%s>..\n",
	gmbAoc->aocCmd_REF_CENTROIDS.state, measure,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_REF_CENTROIDS::cAocCmd_REF_CENTROIDS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* NO can not be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_REF_CENTROIDS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "REF_CENTROIDS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_REF_CENTROIDS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( measure == 0 && ( offset1 != 1 && offset1 != -1 ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_REF_CENTROIDS:checkDependencies: Error, invalid value in "
	    "add/subtract<%d>, needs to be 1 or -1.\n", offset1 );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, add/subtract value invalid, needs to be 1 or -1." );
    }
    /*
     *  WARNING, no idea what the values should be.
     */
    GPI_DEBUG( GPI_DEBUG_NONE,
	"cAocCmd_REF_CENTROIDS::checkDependencies: Warning, dont know "
	"what the values should be - fix when we know.\n");

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_REF_CENTROIDS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_REF_CENTROIDS.done != 0 &&
           gmbAoc->aocCmd_REF_CENTROIDS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_REF_CENTROIDS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done == 1 )
#endif
    {
        done = 1;
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: DONE!!!..XXXXXXXXXXXXXXXXXXXXXXXX.....\n" );
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>...<done=%d><subDone=%d><state=%s><block=%d>.\n",
            numTimes,  gmbAoc->aocCmd_REF_CENTROIDS.done,
	    	subCmdGmb->done,
	      gpStateStrings[gmbAoc->aocCmd_REF_CENTROIDS.state] ,
	      isBlocking );
    }

    return( done );

} /* end of cAocCmd_REF_CENTROIDS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::execute(void)
 *
 * @brief
 *   Execute the "REF_CENTROIDS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::execute
(
    void
)
{

    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;
    int add    = offset1;
    int offset = offset2;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  execute a ref centrioods command,
	 *    measureRefCents	None	"
	 *    offsetRefCents	Nothing? (int)	anytime	
         */

	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<REF_CENTROIDS>> command...., .\n");
	if ( measure == 1 )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_REF_CENTROIDS,
		AOCCMDFMT_MEASUREREFCENTS );
	}
	else if ( measure == 0 )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_REF_CENTROIDS,
		AOCCMDFMT_OFFSETREFCENTS, add, offset );
	}
	else
	{
	    status = gpAocSockExecute (
	        (GPI_SUBSERV_RPC_CMD * ) &gmbAoc->aocCmd_REF_CENTROIDS,
		AOCCMDFMT_MEASUREREFOFFSETS );
	}
    }
    else
#endif

    {
        /*
         * WARNING: Simulating reference centroids
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_REF_CENTROIDS::execute: Sim: Going to sleep for 10 to "
            "sim.\n");

        sleep(10);

    }/*else simulating.. */
    
    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        gmbAoc->aocRpc.measure = measure;
	if ( measure == 0 )
	{
            gmbAoc->aocRpc.addSub = add;
            gmbAoc->aocRpc.refCentOffset = offset;
	}
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    GPI_DEBUG( GPI_DEBUG_MIN,
	"cAocCmd_REF_CENTROIDS::execute: Done<sub:%d, cmd:%d>,isBlocking<%d>, status<%d>\n",
	subCmdGmb->done, gmbAoc->aocCmd_REF_CENTROIDS.done, isBlocking, status);

    return( status );

} /* end of cAocCmd_REF_CENTROIDS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_REF_CENTROIDS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_REF_CENTROIDS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_REF_CENTROIDS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_REF_CENTROIDS::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_SET_SETTINGS::cAocCmd_SET_SETTINGS(GP_AOC_REQUEST *, 
 *      GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_SET_SETTINGS::cAocCmd_SET_SETTINGS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
(   
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtSetSettings_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_SET_SETTINGS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    darkBacks = rqst->GP_AOC_REQUEST_u.mtSetSettings_.darkBacks;
    flatField = rqst->GP_AOC_REQUEST_u.mtSetSettings_.flatField;
    threshold = rqst->GP_AOC_REQUEST_u.mtSetSettings_.threshold;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_SET_SETTINGS:cAocCmd_SET_SETTINGS: SET_SETTINGS "
        "state:<%d>, dir<%s>.\n", gmbAoc->aocCmd_SET_SETTINGS.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_SET_SETTINGS::cAocCmd_SET_SETTINGS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_SET_SETTINGS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "SET_SETTINGS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_SET_SETTINGS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( darkBacks != -1 && darkBacks!=0 && darkBacks!=1 && darkBacks!=2 ) 
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_SETTINGS:checkDependencies: Error, invalid value in "
	    "darkBacks value<%d>, needs to be 0, 1, or 2.\n", darkBacks );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, darkBacks value invalid, needs to be 0, 1, or 2." );
    }
    else if ( flatField != -1 && flatField != 0 && flatField != 1 ) 
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_SETTINGS:checkDependencies: Error, invalid value in "
	    "flatField value<%d>, needs to be 0 or 1.\n", flatField );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, flatField value invalid, needs to be 0 or 1." );
    }
    /* Should check the threshold value too. */

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_SET_SETTINGS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_SET_SETTINGS.done != 0 &&
           gmbAoc->aocCmd_SET_SETTINGS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_SET_SETTINGS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_SET_SETTINGS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::execute(void)
 *
 * @brief
 *   Execute the "SET_SETTINGS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
 
    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  execute either of :
	 *  selectDarksBacks	A(int)		all loops open
	 *  selectFlatField	A(int)		all loops open
	 */

	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<>> command...\n");
	if ( darkBacks >= 0 )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SET_SETTINGS,
		AOCCMDFMT_SELECTDARKBACKS, darkBacks );
	}
	else if (flatField >= 0 )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SET_SETTINGS,
		AOCCMDFMT_SELECTFLATFIELD, flatField );
	}
	else if (threshold >= 0 )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SET_SETTINGS,
		AOCCMDFMT_SETCENTTHRESH, threshold );
	}
    }
    else
#endif

    {
        /*
         * WARNING: Simulating SET_SETTINGS
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_SETTINGS::execute: Sim: Going to sleep for 1 to "
            "sim.\n");

        sleep(1);

    }/*else simulating.. */

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        if ( darkBacks >= 0 )
	    gmbAoc->aocRpc.darkBacks = darkBacks;
	else if ( flatField >= 0 )
	    gmbAoc->aocRpc.flatField = flatField;
	else if ( threshold >= 0 )
	    gmbAoc->aocRpc.threshold = threshold;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return ( status );

} /* end of cAocCmd_SET_SETTINGS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_SET_SETTINGS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SET_SETTINGS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SET_SETTINGS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_SET_SETTINGS::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_SHAPES::cAocCmd_SHAPES(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_SHAPES::cAocCmd_SHAPES
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtShapes_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_SHAPES),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{
    set = rqst->GP_AOC_REQUEST_u.mtShapes_.set;
    type = rqst->GP_AOC_REQUEST_u.mtShapes_.type;
    strncpy( filename, rqst->GP_AOC_REQUEST_u.mtShapes_.filename,
	GPI_MAX_NAME_LEN );
    bits = rqst->GP_AOC_REQUEST_u.mtShapes_.bits;
    
    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_SHAPES:cAocCmd_SHAPES: SHAPES state:<%d>,"
	"dir<%s>.\n",
        gmbAoc->aocCmd_SHAPES.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_SHAPES::cAocCmd_SHAPES() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* Yes can be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_SHAPES::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "SHAPES" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
    
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_SHAPES::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( type == GPI_STORE_SHAPES && (bits < 1 || bits > 119) ) 
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_SHAPES:checkDependencies: Error, invalid value in "
	    "bits value<%d>, needs to be between 1 and 119.\n", bits );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, bits value invalid, needs to be between 1 and 119." );
    }
    else if( type == GPI_SET_SHAPES && (bits < 1 || bits > 887))
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_SHAPES:checkDependencies: Error, invalid value in "
	    "bits<%d>, needs to be between 1 and 887.\n", bits);

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, "Error, bits value "
	    "invalid, needs to be between 1 and 887." );
    }


    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_SHAPES::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_SHAPES.done != 0 &&
           gmbAoc->aocCmd_SHAPES.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_SHAPES.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_SHAPES::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::execute(void)
 *
 * @brief
 *   Execute the "SHAPES" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is a SHAPES command
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
            "::cAocCmd_SHAPES::execute: Going to execute AOC "
	    "<<>> command.... .\n"  );
        int status = GPI_ERR_NONE;

	if ( set && type == GPI_TWEETER )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_SETTWEETERSHAPE, filename );
	}
	else if ( set && type == GPI_WOOFER )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_SETWOOFERSHAPE, filename );
	}
	else if ( !set && type == GPI_WOOFER )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_SETWOOFEROFFSET, filename );
	}
	else if ( !set && type == GPI_TWEETER )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_SETTWEETEROFFSET, filename );
	}
	else if ( type == GPI_STORE_SHAPES )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_STORESHAPES, bits );
	}
	else if ( type == GPI_SET_SHAPES )
	{
	    status = gpAocSockExecute (
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_SHAPES,
		AOCCMDFMT_SETSHAPES, bits );
	}
    }
    else
#endif
    {
        /*
         * WARNING: Simulating Initialize.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SHAPES::execute: Sim: Going to sleep for 20 to "
            "sim .\n");

        sleep(20);

    }

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        if ( type == GPI_STORE_SHAPES )
	    gmbAoc->aocRpc.storeShapes = bits;
        else if ( type == GPI_SET_SHAPES )
	    gmbAoc->aocRpc.setShapes = bits;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_SHAPES::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_SHAPES::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_SHAPES::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_SHAPES::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_SHAPES::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_STAT::cAocCmd_STAT(GP_AOC_REQUEST *, GP_AOC_REPLY *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_STAT::cAocCmd_STAT
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtStat_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_STAT),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    GPI_DEBUG( GPI_DEBUG_FULL,
      "cAocCmd_STAT:cAocCmd_STAT: STAT state: <%d><%s>,"
      "dir<%s>.\n",
      gmbAoc->aocCmd_STAT.state, 
      gpStateStrings[gmbAoc->aocCmd_STAT.state],
      gpActivityStrings[activityDir] );

} /* end of cAocCmd_STAT::cAocCmd_STAT() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::canBeDoneNow( void )
 *
 * @brief
 *   Can always be done now
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STAT::canBeDoneNow
(
    void
)
{
    int doneNow = 1;    /* Yes can be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_STAT::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::checkDependencies( void )
 *
 * @brief
 *   Check the dependencies for "STAT"  command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STAT::checkDependencies
(
    void
)
{

    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_STAT::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_STAT::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STAT::checkIfDone
(
    int numTimes
)
{
    GPI_DEBUG_ENTER;

    int done = 0;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_STAT.done != 0 &&
           gmbAoc->aocCmd_STAT.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_STAT.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_STAT::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::execute(void)
 *
 * @brief
 *   Execute the "STAT" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STAT::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = GPI_ERR_NONE;
 
    return GPI_ERR_NONE;

} /* end of cAocCmd_STAT::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_STAT::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_STAT::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STAT::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STAT::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_STAT::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_STATISTICS::cAocCmd_STATISTICS(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_STATISTICS::cAocCmd_STATISTICS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtStatistics_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_STATISTICS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_STATISTICS:cAocCmd_STATISTICS: STATISTICS "
        "state:<%d>, dir<%s>.\n", gmbAoc->aocCmd_STATISTICS.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_STATISTICS::cAocCmd_STATISTICS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_STATISTICS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "STATISTICS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_STATISTICS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_STATISTICS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( gmbAoc->aocCmd_STATISTICS.done != 0 &&
         gmbAoc->aocCmd_STATISTICS.state != GPI_STATE_BUSY )
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_STATISTICS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::execute(void)
 *
 * @brief
 *   Execute the "STATISTICS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::execute
(
    void
)
{

    GPI_DEBUG_ENTER;

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = GPI_ERR_NONE;
 
    return GPI_ERR_NONE;

} /* end of cAocCmd_STATISTICS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_STATISTICS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATISTICS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATISTICS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_STATISTICS::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_STORE_APO::cAocCmd_STORE_APO(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_STORE_APO::cAocCmd_STORE_APO
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtStoreApoRefCent_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_STORE_APO),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */
    store = rqst->GP_AOC_REQUEST_u.mtStoreApoRefCent_.store;
    apodizer = rqst->GP_AOC_REQUEST_u.mtStoreApoRefCent_.apodizer;
    strncpy( filename, 
	rqst->GP_AOC_REQUEST_u.mtStoreApoRefCent_.filename,
	GPI_MAX_NAME_LEN );


    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_STORE_APO:cAocCmd_STORE_APO: STORE_APO state:<%d>, "
	"dir<%s>.\n",
        gmbAoc->aocCmd_STORE_APO.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_STORE_APO::cAocCmd_STORE_APO() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_STORE_APO::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "STORE_APO" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_STORE_APO::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( apodizer < 1 || apodizer > 8 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_STORE_APO:checkDependencies: Error, invalid value for "
	    "apodizer<%d>, needs to be 1 to 8.\n",
            apodizer );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, apodizer value invalid, needs to be 1 to 8." );
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_STORE_APO::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_STORE_APO.done != 0 &&
           gmbAoc->aocCmd_STORE_APO.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_STORE_APO.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_STORE_APO::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::execute(void)
 *
 * @brief
 *   Execute the "STORE_APO" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
 
    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  execute a calDataOnOff command,
	 *  WARNING, don't know if this is the correct string!!!
         */

        if ( store == 1 )
        {
	    GPI_DEBUG( GPI_DEBUG_MIN,
	        ":: Going to execute AOC <<storeApo>> command...\n");
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_STORE_APO,
	        AOCCMDFMT_STOREAPORCENTOFFS, apodizer );
	}
	else if ( store == 0 )
	{
	    GPI_DEBUG( GPI_DEBUG_MIN,
	        ":: Going to execute AOC <<receiveApo>> command... apodizer=%d,"
		" filename=%s\n",apodizer,filename);
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_STORE_APO,
	        AOCCMDFMT_RECEIVEAPORCENTOFFS, apodizer, filename );
	}	
    }
    else
#endif

    {
        /*
         * WARNING: Simulating STORE_APO
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_STORE_APO::execute: Sim: Going to sleep for 1 to "
            "sim.\n");

        sleep(1);

    }/*else simulating.. */

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
	gmbAoc->aocRpc.apodizer = apodizer;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return ( status );

} /* end of cAocCmd_STORE_APO::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_STORE_APO::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STORE_APO::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STORE_APO::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_STORE_APO::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_TAKE_EXP::cAocCmd_TAKE_EXP(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_TAKE_EXP::cAocCmd_TAKE_EXP
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtTakeExp_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_TAKE_EXP),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_TAKE_EXP:cAocCmd_TAKE_EXP: TAKE_EXP state:<%d>,"
	"dir<%s>.\n",
        gmbAoc->aocCmd_TAKE_EXP.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_TAKE_EXP::cAocCmd_TAKE_EXP() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_TAKE_EXP::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "TAKE_EXP" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_TAKE_EXP::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_TAKE_EXP::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_TAKE_EXP.done != 0 &&
           gmbAoc->aocCmd_TAKE_EXP.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_TAKE_EXP.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_TAKE_EXP::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::execute(void)
 *
 * @brief
 *   Execute the "TAKE_EXP" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    GPI_DEBUG( GPI_DEBUG_NONE,
        "cAocCmd_TAKE_EXP:cAocCmd_TAKE_EXP: Take exposure not implemented.\n" );

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = GPI_ERR_NONE;
 
    return GPI_ERR_NONE;

} /* end of cAocCmd_TAKE_EXP::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_TAKE_EXP::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_EXP::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_EXP::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_TAKE_EXP::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_TAKE_PIXELS::cAocCmd_TAKE_PIXELS(GP_AOC_REQUEST *, 
 *      GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_TAKE_PIXELS::cAocCmd_TAKE_PIXELS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtTakePixels_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_TAKE_PIXELS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    type = rqst->GP_AOC_REQUEST_u.mtTakePixels_.type;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_TAKE_PIXELS:cAocCmd_TAKE_PIXELS: TAKE_PIXELS "
        "state:<%d>, type<%d><%s>, dir<%s>.\n", gmbAoc->aocCmd_TAKE_PIXELS.state,
	type, (type==BACK)?"BACK":(type==FLAT)?"FLAT": "DARK",
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_TAKE_PIXELS::cAocCmd_TAKE_PIXELS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_TAKE_PIXELS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "TAKE_PIXELS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_TAKE_PIXELS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    /*
     *  Cannot be currently correcting to take back/dark/flat.
     */
    else if ( (status = currentlyCorrectingAoc() ) )
    {
        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
            "Error, currently correcting, loop closed, with Aoc.." );
    }
    /*
     *  Check type.
     */

    if ( type != DARK && type != FLAT && type != BACK )
    {
        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, invalid type request" );

        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocSubserver::handleRequest: Error, invalid pixel type request<%d>"
	    " needs to be DARK<%d>, FLAT<%d> or BACK<%d>.\n",
	    type, DARK, FLAT, BACK );
    }
    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_TAKE_PIXELS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_TAKE_PIXELS.done != 0 &&
           gmbAoc->aocCmd_TAKE_PIXELS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_TAKE_PIXELS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_TAKE_PIXELS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::execute(void)
 *
 * @brief
 *   Execute the "TAKE_PIXELS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
    
    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  execute a take pixel command,
	 *  WARNING, don't know if this is the correct string!!!
         */

	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<%d>> command...., .\n",type);

	if ( type == BACK )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_TAKE_PIXELS,
		AOCCMDFMT_TAKEPIXELBACKS);
	}
	else if ( type == FLAT )
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_TAKE_PIXELS,
		AOCCMDFMT_TAKEPIXELFLATS);
	}
	else
	{
	    status = gpAocSockExecute ( 
		(GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_TAKE_PIXELS,
		AOCCMDFMT_TAKEPIXELDARKS);
	}
	    
    }
    else
#endif

    {
        /*
         * WARNING: Simulating TAKE_PIXELS
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_TAKE_PIXELS::execute: Sim: Going to sleep for 10 to "
            "sim.\n");

        sleep(10);

    }/*else simulating.. */

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_TAKE_PIXELS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_TAKE_PIXELS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TAKE_PIXELS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TAKE_PIXELS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_TAKE_PIXELS::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_TT::cAocCmd_TT(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/
cAocCmd_TT::cAocCmd_TT
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtTt_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_TT),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: 
     */

    angles = rqst->GP_AOC_REQUEST_u.mtTt_.angles;
    setTTXAxis = rqst->GP_AOC_REQUEST_u.mtTt_.setTTXAxis;
    setTTYAxis = rqst->GP_AOC_REQUEST_u.mtTt_.setTTYAxis;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_TT:cAocCmd_TT: TT state:<%d>, "
	"dir<%s>.\n", gmbAoc->aocCmd_TT.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_TT::cAocCmd_TT() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_TT::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "TT" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_TT::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( angles != 0 && angles != 1 )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_TT:checkDependencies: Error, invalid value for "
	    "angles<%d>, needs to be 0, or 1.\n", angles );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, angles value invalid, needs to be 0 or 1." );
    }
    else if ( angles == 1 && ( setTTXAxis < -2.1 || setTTXAxis > 2.1 ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_TT:checkDependencies: Error, invalid value for "
	    "x axis<%f>, needs to be -2.1 to 2.1\n", angles );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, x axis value invalid, needs to be -2.1to2.1." );
    }
    else if ( angles == 1 && ( setTTYAxis < -2.1 || setTTYAxis > 2.1 ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_TT:checkDependencies: Error, invalid value for "
	    "y axis<%f>, needs to be -2.1 to 2.1\n", angles );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, y axis value invalid, needs to be -2.1to2.1." );
    }
    /* Should check to ensure voltages are valid, when valid values are known. */

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_TT::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_TT.done != 0 &&
           gmbAoc->aocCmd_TT.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_TT.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_:checkIfDone: Waiting for finish...<%d>....\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_TT::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::execute(void)
 *
 * @brief
 *   Execute the "TT" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::execute
(
    void
)
{
    GPI_DEBUG_ENTER;

    int status = GPI_ERR_NONE;

#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         *  This is an M1 M2 On off command so we need to send offload to the 
         *      AOC Subsystem
         *  This is not a command that can be completed immediately.
         */
	GPI_DEBUG( GPI_DEBUG_NONE,
            "::cAocCmd_TT::execute: Going to execute AOC "
	    "<<TT>> command>, angles=%d. Xaxis=%f, Yaxis=%f\n",
	    angles, setTTXAxis, setTTYAxis);

	status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_TT),
	    AOCCMDFMT_SETTT, angles, setTTXAxis, setTTYAxis );
    }
    else
#endif
    {
        /*
         * WARNING: Simulating Initialize.
         * Removed sleep when AOC exists.
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_SET_TT::execute: Sim: Going to sleep for 20 to "
            "sim .\n");

        sleep(20);

    }

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE  && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
	gmbAoc->aocRpc.angle = angles;
	gmbAoc->aocRpc.TTXaxis = setTTXAxis;
	gmbAoc->aocRpc.TTYaxis = setTTYAxis;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_TT::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_TT::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_TT::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_TT::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();

    return( status );

}  /* end of cAocCmd_TT::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_IN_FUNC_GAINS::cAocCmd_IN_FUNC_GAINS(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_IN_FUNC_GAINS::cAocCmd_IN_FUNC_GAINS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtInFuncGains_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_IN_FUNC_GAINS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: .
     */

    action   = rqst->GP_AOC_REQUEST_u.mtInFuncGains_.action;
    gainXPos = rqst->GP_AOC_REQUEST_u.mtInFuncGains_.gainXPos;
    gainYPos = rqst->GP_AOC_REQUEST_u.mtInFuncGains_.gainYPos;
    gainAmplitude = rqst->GP_AOC_REQUEST_u.mtInFuncGains_.gainAmplitude;
    select   = rqst->GP_AOC_REQUEST_u.mtInFuncGains_.select;
    strncpy( filename, rqst->GP_AOC_REQUEST_u.mtInFuncGains_.filename,
	GPI_MAX_NAME_LEN );

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_IN_FUNC_GAINS:cAocCmd_IN_FUNC_GAINS: IN_FUNC_GAINS"
        "state:<%d>, dir<%s>.\n", gmbAoc->aocCmd_IN_FUNC_GAINS.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_IN_FUNC_GAINS::cAocCmd_IN_FUNC_GAINS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_IN_FUNC_GAINS::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* No can not be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_IN_FUNC_GAINS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "IN_FUNC_GAINS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_IN_FUNC_GAINS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_IN_FUNC_GAINS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( action == MAKE_IN_FUNC_GAINS && 
    	      ( gainXPos < MIN_GAIN_MODE_POSITION ||
	       gainXPos > MAX_GAIN_MODE_POSITION ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_IN_FUNC_GAINS:checkDependencies: Error, invalid value in "
	    "x mode position value<%d>, needs to be %d-%d.\n", gainXPos,
	    MIN_GAIN_MODE_POSITION, MAX_GAIN_MODE_POSITION );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, gainXPos value invalid." );
    }
    else if ( action == MAKE_IN_FUNC_GAINS && 
    	      gainYPos < MIN_GAIN_MODE_POSITION && 
	      gainYPos > MAX_GAIN_MODE_POSITION )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_IN_FUNC_GAINS:checkDependencies: Error, invalid value in "
	    "y mode position value<%d>, needs to be %d-%d.\n", gainYPos,
	    MIN_GAIN_MODE_POSITION, MAX_GAIN_MODE_POSITION );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, gainYPos value invalid." );
    }
    else if ( action == SELECT_IN_FUNC_GAINS && 
    		( select != USE_ALL_ONES && select != RECV_FROM_HOST ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_IN_FUNC_GAINS:checkDependencies: Error, invalid value for "
	    "influence function gains<%d>, needs to be %d or %d.\n", select,
	    USE_ALL_ONES, RECV_FROM_HOST);

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, source value invalid." );
    }
    else if ( action == RECV_IN_FUNC_GAINS && 
    		( strlen(filename) == 0 || 
		strncmp( filename, "NULL", 4)== 0 ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_IN_FUNC_GAINS:checkDependencies: Error, invalid value for "
	    "RECV_IN_FUNC_GAINS, needs a filename.\n");

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, for RECV_IN_FUNC_GAINS need a filename." );
    }
    /*
     * WARNING: not checking amplitude for a MakeInFuncGains, don't know
     * range.
     */

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_IN_FUNC_GAINS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_IN_FUNC_GAINS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_IN_FUNC_GAINS.done != 0 &&
           gmbAoc->aocCmd_IN_FUNC_GAINS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_IN_FUNC_GAINS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_IN_FUNC_GAINS:checkIfDone: Waiting for finish."
            "<%d>...\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_IN_FUNC_GAINS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::execute(void)
 *
 * @brief
 *   Execute the "IN_FUNC_GAINS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_IN_FUNC_GAINS::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
 
    int status = GPI_ERR_NONE;
 
#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
        /*
         */
	GPI_DEBUG( GPI_DEBUG_MIN,
	    ":: Going to execute AOC <<inFuncGains>> command....,"
	    "action<%d>, gainX<%d>, gainY<%d>, gainAmplitude<%d>, select<%d>, filename<%s>.\n",
	    action, gainXPos, gainYPos, gainAmplitude, select, filename);
	if ( action == MAKE_IN_FUNC_GAINS )
	{
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_IN_FUNC_GAINS,
	        AOCCMDFMT_MAKEINFUNCGAINS, gainXPos, gainYPos, gainAmplitude);
	}
	else if ( action == RECV_IN_FUNC_GAINS )
	{
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_IN_FUNC_GAINS,
	        AOCCMDFMT_RECEIVEINFUNCGAINS, filename);
	}
	else if ( action == SELECT_IN_FUNC_GAINS )
	{
	    status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_IN_FUNC_GAINS,
	        AOCCMDFMT_SELECTINFUNCGAINS, select);
	}
    }
    else
#endif

    {
        /*
         * WARNING: Simulating IN_FUNC_GAINS
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_IN_FUNC_GAINS::execute: Sim: Going to sleep "
            "for 12 to sim.\n");

        sleep(12);

    }/*else simulating.. */

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
        if ( action == MAKE_IN_FUNC_GAINS )
	{
	    gmbAoc->aocRpc.gainXpos = gainXPos;
	    gmbAoc->aocRpc.gainYpos = gainYPos;
	    gmbAoc->aocRpc.gainAmp = gainAmplitude;
	}
	else if ( action == SELECT_IN_FUNC_GAINS )
	    gmbAoc->aocRpc.whichGains = select;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_IN_FUNC_GAINS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_IN_FUNC_GAINS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_IN_FUNC_GAINS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_IN_FUNC_GAINS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_IN_FUNC_GAINS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();


    return( status );

}  /* end of cAocCmd_IN_FUNC_GAINS::makeItUnBusy() */

/*!
 ************************************************************************
 *
 * @fn cAocCmd_STATS_LOG::cAocCmd_STATS_LOG(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_STATS_LOG::cAocCmd_STATS_LOG
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtStatsLog_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_STATS_LOG),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: .
     */
    statsLog = rqst->GP_AOC_REQUEST_u.mtStatsLog_.statsLog;

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_STATS_LOG:cAocCmd_STATS_LOG: STATS_LOG"
        "state:<%d>, dir<%s>.\n", gmbAoc->aocCmd_STATS_LOG.state,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_STATS_LOG::cAocCmd_STATS_LOG() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATS_LOG::canBeDoneNow
(
    void
)
{
    int doneNow = 0;    /* No can not be done now.     */

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_STATS_LOG::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "STATS_LOG" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATS_LOG::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_STATS_LOG::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    else if ( statsLog != 1 && statsLog != 0 )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_STATS_LOG::checkDependencies: ERROR, invalid value for"
	    " statsLog<%d>, needs to be 0 or 1.\n", statsLog);

	status = GPI_ERR_ERROR;
        
	GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg,
	    "Error, statsLog value invalid, must be 0 or 1.");
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_STATS_LOG::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATS_LOG::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_STATS_LOG.done != 0 &&
           gmbAoc->aocCmd_STATS_LOG.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_STATS_LOG.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_STATS_LOG:checkIfDone: Waiting for finish."
            "<%d>...\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_STATS_LOG::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::execute(void)
 *
 * @brief
 *   Execute the "STATS_LOG" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_STATS_LOG::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
 
    int status = GPI_ERR_NONE;
 
#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
	GPI_DEBUG( GPI_DEBUG_MIN, ":: Going to execute AOC <<statsLog>>"
	    " command...., statsLog<%d>.\n", statsLog);
        status = gpAocSockExecute ( 
	    (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_STATS_LOG,
	    AOCCMDFMT_STATSLOG, statsLog);
    }
    else
#endif

    {
        /*
         * WARNING: Simulating STATS_LOG
         */

        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_STATS_LOG::execute: Sim: Going to sleep "
            "for 12 to sim.\n");

        sleep(12);

    }/*else simulating.. */

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE )
    {
	    gmbAoc->aocRpc.statsLog = statsLog;
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_STATS_LOG::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_STATS_LOG::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_STATS_LOG::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_STATS_LOG::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_STATS_LOG::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();


    return( status );

}  /* end of cAocCmd_STATS_LOG::makeItUnBusy() */


/*!
 ************************************************************************
 *
 * @fn cAocCmd_GAINS::cAocCmd_GAINS(GP_AOC_REQUEST *, GP_AOC_REQUEST *)
 *
 * @brief
 *   Constructor
 * 
 * @callgraph
 ************************************************************************
-*/

cAocCmd_GAINS::cAocCmd_GAINS
(
    GP_AOC_REQUEST *rqst,
    GP_AOC_REPLY   *rply,
    cSubCmdResult  *result     /* Ptr to result structure */
) : cSubCommands
( 
    result, 
    &(rqst->GP_AOC_REQUEST_u.mtGains_.c),
    (GPI_SUBSERV_RPC_CMD *) &(gmbAoc->aocCmd_GAINS),
    (int *) &(gmbAoc->aocRpc.stat.simulate),
    (int *) &(gmbTlc->iLock.interlock) 
)
{

    /*
     *  Save local command information: .
     */
    command = rqst->GP_AOC_REQUEST_u.mtGains_.command;
    source = rqst->GP_AOC_REQUEST_u.mtGains_.source;
    factors[0] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[0];
    factors[1] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[1];
    factors[2] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[2];
    factors[3] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[3];
    factors[4] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[4];
    factors[5] = rqst->GP_AOC_REQUEST_u.mtGains_.factors[5];
    strncpy( filename, rqst->GP_AOC_REQUEST_u.mtGains_.filename,
    		GPI_MAX_NAME_LEN );

    GPI_DEBUG( GPI_DEBUG_FULL,
        "cAocCmd_GAINS:cAocCmd_GAINS: GAINS"
        "state:<%d>, filename<%s>, dir<%s>.\n", gmbAoc->aocCmd_GAINS.state,
	filename,
	gpActivityStrings[activityDir] );

} /* end of cAocCmd_GAINS::cAocCmd_GAINS() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::canBeDoneNow( void )
 *
 * @brief
 *   Indicated if cmd can be completed (passed to parent class)
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GAINS::canBeDoneNow
(
    void
)
{
#ifdef INCLUDING_AOC_CODE
    int doneNow = 0;    /* No can not be done now.     */
#else
    int doneNow = 1;    /* Yes can be done now.     */
#endif //INCLUDING_AOC_CODE

    GPI_DEBUG_ENTER;

    /*
     *  Need to determine if this function can be completed right away
     *  Setting this bit (canBeDoneNow=1) will make it so that 
     *  in the execute()  function will be called immediately and is 
     *  expected to return immediately.
     */

    return( doneNow );

} /* End of cAocCmd_GAINS::canBeDoneNow() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::checkDependencies(void)
 *
 * @brief
 *   Check dependencies on the "GAINS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GAINS::checkDependencies
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;
 
    status = cSubCommands::checkDependencies();
    if ( status != GPI_ERR_NONE )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "cAocCmd_GAINS::checkDependencies: ERROR, failed cSubCommands::"
            "checkDependencies().\n");
    }
    /*
     *  For selection gain source and set gain src choices the source must be set
     *  to 0x04: use host supplied value from `setGainValues' command
     *  0x08: use `initial Values' as set by the `OPT_INIT_GAINS' in the params file 
     */

    else if ((command == SELECT_GAIN_SOURCE || command == SET_GAIN_SRC_CHOICES) && 
                         (source != 4 && source != 8))
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GAINS:checkDependencies: Error, invalid value in "
	    "source value<%d>, needs to be 4 or 8.\n", source );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, source value invalid, needs to be 4 or 8." );
    }
    else if ( command == SET_GAIN_FACTORS && ( 
    		factors[0] < 0.0 || factors[0] > 10.0 ||
    		factors[1] < 0.0 || factors[1] > 10.0 ||
    		factors[2] < 0.0 || factors[2] > 10.0 ||
    		factors[3] < 0.0 || factors[3] > 10.0 ||
    		factors[4] < 0.0 || factors[4] > 10.0 ||
    		factors[5] < 0.0 || factors[5] > 10.0 
	    ) )
    {
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GAINS:checkDependencies: Error, invalid value for cmd SET_GAIN_FACTORS in "
	    "factors <%f><%f><%f><%f><%f><%f>, needs to be between 0.0 and 10.0\n", 
	    factors[0], factors[1], factors[2], factors[3], factors[4], factors[5],
	    factors[6] );

        status = GPI_ERR_ERROR;

        GPI_SET_GMB_ERROR_PTR( subCmdGmb->ackErrMsg, 
            "Error, factors value invalid, needs to be 0.0-10.0" );
    }

    setAcceptCmdInGmb(status);

    return( status );

} /* End of cAocCmd_GAINS::checkDependencies() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::checkIfDone( int )
 *
 * @brief
 *   Check to see if the command is done, no longer BUSY.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GAINS::checkIfDone
(
    int numTimes
)
{
    int done = 0;

    GPI_DEBUG_ENTER;

    /*
     * If it can be done now, then it's done.
     * otherwise, check if the "done" is set(set when execute finishes).
     */

#ifdef INCLUDING_AOC_CODE
    if ( ( ! gmbAoc->aocRpc.stat.simulate  &&
	   gmbAoc->aocCmd_GAINS.done != 0 &&
           gmbAoc->aocCmd_GAINS.state != GPI_STATE_BUSY  ) ||
	 (  gmbAoc->aocRpc.stat.simulate &&  gmbAoc->aocCmd_GAINS.done ))
#else
    if ( canBeDoneNow() || subCmdGmb->done  )
#endif
    {
        done = 1;
    }
    else
    {
        GPI_DEBUG( GPI_DEBUG_MAX,
            "cAocCmd_GAINS:checkIfDone: Waiting for finish."
            "<%d>...\n",
            numTimes );
    }

    return( done );

} /* end of cAocCmd_GAINS::checkIfDone() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::execute(void)
 *
 * @brief
 *   Execute the "GAINS" command.
 * 
 * @callgraph
 ************************************************************************
-*/
int cAocCmd_GAINS::execute
(
    void
)
{
    GPI_DEBUG_ENTER;
 
    int status = GPI_ERR_NONE;
 
#ifdef INCLUDING_AOC_CODE
    if ( ! gmbAoc->aocRpc.stat.simulate )
    {
	if ( command == SELECT_GAIN_SOURCE ) /* 0 then selectGainSource */
	{
	    GPI_DEBUG( GPI_DEBUG_MIN, ":: Going to execute AOC <<SELECT_GAIN_SOURCE>>"
	        " command... command<%d>, source<%d>, factors<%f>,"
		" filename<%s>.\n", command, source, factors, filename);
            status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GAINS,
	        AOCCMDFMT_SELECTGAINSOURCE, source);
        }
	else if ( command == SET_GAIN_FACTORS )
	{
	    GPI_DEBUG( GPI_DEBUG_MIN, ":: Going to execute AOC <<SET_GAIN_FACTORS>>"
	        " command... command<%d>, source<%d>, factors<%f>,"
		" filename<%s>.\n", command, source, factors, filename);
/* setGainFacotrs is supposed to accept an array of floats. What to do here???
            status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GAINS,
	        AOCCMDFMT_SETGAINFACTORS, factors);
*/	}
	else if ( command == SET_GAIN_SRC_CHOICES )
	{
	    GPI_DEBUG( GPI_DEBUG_MIN, ":: Going to execute AOC <<SET_GAIN_SRC_CHOICES>>"
	        " command... command<%d>, source<%d>, factors<%f>,"
		" filename<%s>.\n", command, source, factors, filename);
            status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GAINS,
	        AOCCMDFMT_SETGAINSOURCECHOICES, source);
	}
	else if ( command == SET_GAIN_VALUES )
	{
	    GPI_DEBUG( GPI_DEBUG_MIN, ":: Going to execute AOC <<set_gain_values>>"
	        " command... command<%d>, source<%d>, factors<%f>,"
		" filename<%s>.\n", command, source, factors, filename);
            status = gpAocSockExecute ( 
	        (GPI_SUBSERV_RPC_CMD *) &gmbAoc->aocCmd_GAINS,
	        AOCCMDFMT_SETGAINVALUES, filename);
	}
	else
	{
	    GPI_DEBUG(GPI_DEBUG_NONE,"Invalid command number<%d>\n", command);
	}
    }
    else
#endif
    {
        /*
         * WARNING: Simulating GAINS
         */
        GPI_DEBUG( GPI_DEBUG_MIN,
            "cAocCmd_GAINS::execute: Sim: Going to sleep "
            "for 1 to sim.\n");

        sleep(1);

    }/*else simulating.. */

    /* update GMB if status is ok, and activityDir is not CANCEL */
    if(status == GPI_ERR_NONE && gmbAoc->aocRpc.stat.activityDir != CANCEL)
    {
	    if ( command == SELECT_GAIN_SOURCE )
	    {
	        gmbAoc->aocRpc.gainSource = source;
	    }
	    else if ( command == SET_GAIN_FACTORS )
	    {
	        gmbAoc->aocRpc.gainFactors[0] = factors[0];
	        gmbAoc->aocRpc.gainFactors[1] = factors[1];
	        gmbAoc->aocRpc.gainFactors[2] = factors[2];
	        gmbAoc->aocRpc.gainFactors[3] = factors[3];
	        gmbAoc->aocRpc.gainFactors[4] = factors[4];
	        gmbAoc->aocRpc.gainFactors[5] = factors[5];
	    }
	    else if ( command == SET_GAIN_SRC_CHOICES )
	    {
	        gmbAoc->aocRpc.gainSourceChoices = source;
	    }
	    /* WARNING, should probably save command too... */
    }

    /*
     * Set the done flag.  Used by checkIfDone to determine if done.
     */
    subCmdGmb->done = 1;
    subCmdGmb->stateErrCode = status;

    return( status );

} /* end of cAocCmd_GAINS::execute() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::makeItBusy( void )
 *
 * @brief
 *   Make the command appear busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GAINS::makeItBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItBusy to set values in GMB */
    cSubCommands::makeItBusyBroadcast();

    return( status );

} /* end of cAocCmd_GAINS::makeItBusy() */

/*!
 ************************************************************************
 *
 * @fn int cAocCmd_GAINS::makeItUnBusy( void )
 *
 * @brief
 *   Make the command appear not busy.
 * 
 * @callgraph
 ************************************************************************
-*/

int cAocCmd_GAINS::makeItUnBusy
(
    void
)
{
    int status = GPI_ERR_NONE;

    GPI_DEBUG_ENTER;

    /* call parent makeItUnBusy to set values in GMB */
    cSubCommands::makeItUnBusyBroadcast();


    return( status );

}  /* end of cAocCmd_GAINS::makeItUnBusy() */




/*!
 ************************************************************************
 *
 * @fn void aocSubServer(struct svc_req*, SVCXPRT* )
 *
 * @brief
 *   Handle the RPC server.
 * 
 * @callgraph
 ************************************************************************
-*/
void aocSubServer
(
    struct svc_req  *rqstp,     /* (in) XDR routine to decode args */
    SVCXPRT         *xprt       /* (in) RPC transport handle.      */
)
{
    int status = GPI_ERR_NONE;/* Return function status.  */

    GPI_DEBUG_ENTER;

   /*
    *  Create a custom class ptr.  It clears the reply structure
    */

   cSubCmdResult * result = new cSubCmdResult();
   cAocSubserver * aoc = new cAocSubserver( result );

   /*
    *  WARNING: Need to check that our health is good - and if it isn't
    *  and this isn't an init or reboot command then we need
    *  to error out.
    */
   

   /*
    *  Get the RPC request.  
    */

   if ( aoc->getRequest(xprt) != GPI_ERR_NONE )
   {
       GPI_DEBUG( GPI_DEBUG_NONE, 
           "aocSubServer: Error, failed to get the xprt request.\n");
       delete result;
       delete aoc;
       return;
   }


   /*
    *  Now react to the request.
    */
   if ( (status = aoc->handleRequest()) == GPI_ERR_NONE  && 
       aoc->getCmdPtr()->activityDir != PRESET ) 
   {
       /*
    *  Start the monitor, if it started ok .
    */

    if ( (status = aoc->startMonitor(aoc->getCmdPtr(), 
        FirstWaitTimes[aoc->cmdNum])) != GPI_ERR_NONE ) 
       {
       /*
        *  If this failed, then makeItUnBusy
        */

       aoc->getCmdPtr()->makeItUnBusy();

       GPI_DEBUG( GPI_DEBUG_NONE, 
           "aocSubServer: failed in startMonitor=%d.\n", status);
       }
   } /* end of handleRequest */
   
    /*
     *  Done.  
     *  Filled in reply structure in.
     */
    aoc->setRpcReplyStructure( status );
    GPI_DEBUG( GPI_DEBUG_MIN, "aocsSubServer: Done, status=<%d>.\n",
        status);

   /*
    *  Update the rest of the GMB.
    */
   err = gpUtGmbBroadcast (gmb, (void *) & (gmbAoc->aocRpc),
           sizeof(gmbAoc->aocRpc) );

   /*
    *
    *  WARNING: fill in the rest of the reply structure.
    */
   aoc->setRpcReplyStructure( status );
   GPI_DEBUG( GPI_DEBUG_FULL, "aocSubServer: Done, status=<%d>.\n",
           status );


   /*
    *  Send the reply back.  Pass in xdr function to encode the reply and
    *  pass in the address of the structure that it is saved in.
    *  svc_sendreply() returns one if it succeeds, zero otherwise.
    */

   if ( (svc_sendreply ( xprt, 
        (xdrproc_t) xdr_GP_AOC_REPLY,
        (caddr_t) aoc->replyPtr()     ) ) == 0)
   {
       /*
        *  svc_sendreply failed!
        *
        *  Free any spaced used from the svc_getargs function.
        */

       (void) svc_freeargs( xprt, 
            (xdrproc_t)xdr_GP_AOC_REPLY, 
            (char *)aoc->replyPtr() );

       /*
        *  Kill the RPC server and exit.
        */

       GPI_DEBUG( GPI_DEBUG_NONE,
            "aocSubServer: Error, Couldn't reply to RPC call. Killing RPC "
            "server\n");
       svc_destroy (xprt);
       delete result;
       delete aoc;
       exit (GPI_ERR_ERROR);
   }

   if ( gmbAoc->aocRpc.stat.shutdownRequest )
   {
       GPI_DEBUG( GPI_DEBUG_MIN, 
            "aocSubServer: Reboot flag set - kill server and exiting.\n");
       svc_destroy (xprt);
       delete result;
       delete aoc;
       exit (0);
   }

   GPI_DEBUG( GPI_DEBUG_FULL, 
        "aocSubServer: Finished handling the command request.\n");

   return;
} /* end of aocSubServer() */

/*!
 ************************************************************************
 *
 * @fn int currentlyCorrectingAoc()
 *
 * @brief
 *   Check to see if we are currently observing.
 *   Not using yet!
 * 
 * @callgraph
 ************************************************************************
-*/

int currentlyCorrectingAoc
(
    void
)
{
    int status = GPI_ERR_NONE;
    GPI_DEBUG_ENTER;

    if ( gmbAoc->aocRpc.guiding == 1 ||
        gmbAoc->aocCmd_CLOSE_OPEN_LOOP.state == GPI_STATE_BUSY )
    {
        GPI_DEBUG( GPI_DEBUG_NONE,
            "currentlyCorrectingAoc: Error, already correcting<%d><%s>  or "
            "taking an exposure.\n",
            gmbAoc->aocRpc.guiding,  
            gpStateStrings[gmbAoc->aocCmd_CLOSE_OPEN_LOOP.state] );

        status = GPI_ERR_ERROR;
    }

    return( status );

} /* End of currentlyCorrectingAoc */
/****************************************************************************************************
 * 
 *  The following are dummy aoc calls.
 *  Need to replace all of them.
 *
 ****************************************************************************************************/

int aocAutoCamRate()
{ 
    sleep (3);
    return 0;
}


int aocSetCamRate( int rate )
{ 
    sleep (5);
    return 0;
}


int aocCheckIfDumpIsDone( )
{ 
    if ( dummyCounter++ > 10 )
        return 1;
    else
        return 0;
}

int aocPerformTheDumpOfData(int type, int seconds, int decimation )
{
    sleep (10);
    return 0;
}

int aocSaveObservConfigVals(float mag, float r0, int camRate, int darkSelect, int refSelect)
{
    sleep (1);
    return 0;
}

