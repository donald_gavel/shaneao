; rd_zygo.pro
; Courtesy of David L. Windt <windt@astro.columbia.edu>
; (part of his Topo package--google it if you want to find it)
; Modified by Katie Morzinski    <ktmorz@ucolick.org>    10 Mar 2005

; This procedure reads a ZYGO data file (*.dat) taken with a ZYGO
; interferometer into an array in IDL.
pro rd_zygo,x,y,image, $
      file=file, $
      tilt_sub=tilt_sub, $
      sphere_sub=sphere_sub,hist_equal=hist_equal, $
      swap_endian=swap_endian

on_error,2
if n_params() eq 0 then message,'usage: rd_zygo,x,y,z'

if keyword_set(file) eq 0 then begin
    if (!d.flags and 65536) ne 0 then begin
        ;file=pickfile(title='Read which ZYGO data file?')
        ;if file eq '' then return
        file=' '  ; Define filename as string
        read,file,prompt='Read which ZYGO data file?'  ; Read file
        if file eq '' then return
    endif else begin
        file=' '
        read,'Enter the name of the ZYGO data-file: ',file
    endelse
endif

openr,lun,file,/get_lun

dum=bytarr(834)
readu,lun,dum

int_width=fix(dum,52)
int_height=fix(dum,54)

phase_width=fix(dum,68)
phase_height=fix(dum,70)

scale_factor=float(dum,164)
lambda=float(dum,168)
obliquity_factor=float(dum,176)
camera_res=float(dum,184)
phase_res=fix(dum,218)

pixel_width=float(dum,486)
pixel_height=float(dum,490)

if keyword_set(swap_endian) then begin
    byteorder,int_width,/sswap
    byteorder,int_height,/sswap

    byteorder,phase_width,/sswap
    byteorder,phase_height,/sswap

    byteorder,scale_factor,/lswap
    byteorder,lambda,/lswap
    byteorder,obliquity_factor,/lswap
    byteorder,camera_res,/lswap

    byteorder,phase_res,/sswap

    byteorder,pixel_width,/lswap
    byteorder,pixel_height,/lswap
endif

if phase_res then res=32768. else res=4096.

;; intensity data is actually unsigned 16-bit data,
;; so intarr is not correct; but we don't need intensity
;; data anyway.
int_arr=intarr(int_width,int_height)
phase_arr=lonarr(phase_width,phase_height)

readu,lun,int_arr
readu,lun,phase_arr

if keyword_set(swap_endian) then byteorder,phase_arr,/lswap

free_lun,lun
close,lun

;; remove bad pixels:
wh=where(phase_arr eq 2147483640.,count)
if count gt 0 then phase_arr(wh)=0.

;; make image in angstroms:
image=phase_arr*lambda*scale_factor*obliquity_factor/res*1.e10

;; make x and y in angstroms:
x=findgen(phase_width)*camera_res*1.e10
y=findgen(phase_height)*camera_res*1.e10

if keyword_set(sphere_sub) then image=image-sfit(image,2) else $
  if keyword_set(tilt_sub) then image=image-sfit(simage,1)

if keyword_set(hist_equal) then image=hist_equal(image)

return
end

