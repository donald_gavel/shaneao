
namespace eval custom {
#
# These are custom procedures to override or add additional
# procedures to the usual galildisp procs.
#
# To override a standard procedure: create the proc in the
# global namespace.
#
# To provide a new procedure that you do not want to accidentally
# override a standard procedure: create the proc in this "custom"
# namespace.

# -------------------------------------------------------------------------
#
# Galildisp runs this proc before ktldisp::start is called.
# The default ::customInitPreKtldisp procedure is a no-op.
#
proc ::customInitPreKtldisp {} {

    ktldisp::confset saomot \
	-modify { MEMSPOWER custom::modify_memspower }
    ktldisp::confset saomot \
	-modify { MEMSPULSE custom::modify_memspulse }

    ktldisp::confset saomot \
	-noautolog { MEMSPULSE modify,modifydone,bcast,data }

    # Don't record modify requests for MEMSPULSE in a 'request key':
    ktldisp::confset saomot \
	-reqkey { MEMSPULSE "" }

}

# -------------------------------------------------------------------------
#
# Galildisp runs a proc of this name just after ktldisp::start is called.
# The default ::customInitPostKtldisp procedure is a no-op.
#
proc ::customInitPostKtldisp {} {

    # Do not set MEMSPOWER; it is tied to the intput bit that
    # reports AC outlet power state.

    setIfChanged ::KeyVal MEMSPULSE 0

    #
    # Start tracing the 'feedback pulse monitor' counter,
    # which is in Galil variable ZAB.
    #
    trace add variable ::X(B,ZA) write custom::traceHeartbeatMissing
    trace add variable ::X(A,ZA) write custom::traceMemsHeartbeatCount
    trace add variable ::KeyVal(MEMSPOWER) write custom::traceMemsPower
}

# -------------------------------------------------------------------------
#
proc modify_memspower {completionvar svc kwd val args} {

    if {[llength $::State(commsOK)]} {
	# Don't have ok comms with Galil
	set $completionvar $::State(commsOK)
	return
    }

    # Expected response:
    # o  ":" from previous command;
    # o  ":" from memsHB=-1;
    # o  number from TC0.
    #
    #
    set resetbit $CGC::Dout::bitnum(MEMS_POWER_RESET_PULSE)

    if {$val == $Map::bin($kwd,ENABLED)} {
	#
	# Enable AC: make MEMS_POWER_RESET_PULSE active.
	# The Galil code is responsible for clearing the reset pulse.
	# Expected response:
	# o  ":" from previous command;
	# o  ":" from ZAA=0;
	# o  ":" from SB/CB;
	# o  number from TC0.
	#

	if {$CGC::Dout::ActiveLev(MEMS_POWER_RESET_PULSE)} {
	    # Active high.
	    set cmd "ZAA=0;SB $resetbit"
	} else {
	    # Active low.
	    set cmd "ZAA=0;CB $resetbit"
	}
	LogInfo "Enabling MEMS AC power with this command: $cmd"

	DComm::addSendQueue $::dcommPri(normal) other \
	    "$cmd;TC0" \
		{^([:?]){3,3} *(\d+)} regexp \
		    [list custom::check_memsHBrequest $completionvar] ""
    } else {
	#
	# Disable: set memsHB=-1, and ensure that the reset line is not active.
	#
	# Expected response:
	# o  ":" from previous command;
	# o  ":" from memsHB=-1;
	# o  ":" from SB/CB of reset line.
	# o  number from TC0.
	#
	if {$CGC::Dout::ActiveLev(MEMS_POWER_RESET_PULSE)} {
	    # Active high
	    set reset "CB $resetbit"	; # ensure reset line deactived
	} else {
	    set reset "SB $resetbit"	; # ensure reset line deactived
	}
	DComm::addSendQueue $::dcommPri(normal) other \
	    "memsHB=-1;$reset;TC0" \
		{^([:?]){3,3} *(\d+)} regexp \
		    [list custom::check_memsHBrequest $completionvar] ""
    }
}

# -------------------------------------------------------------------------
#
proc modify_memspulse {completionvar svc kwd val args} {

    if {[llength $::State(commsOK)]} {
	# Don't have ok comms with Galil
	set $completionvar $::State(commsOK)
	return
    }

    # MEMSPOWER is tied to MEMS_POWER_OUTLET_STATE.  We require (assume)
    # that the configuration for this mapping uses the default rule,
    # which is that the active [inactive] state of MEMS_POWER_OUTLET_STATE
    # is mapped to MEMSPOWER = 1 [0], ie to ENABLED [DISABLED].
    #
    if {$::KeyVal(MEMSPOWER) == $Map::bin(MEMSPOWER,ENABLED)} {
	# Report actual count of write requests.
	incr ::KeyVal(MEMSPULSE)
    } else {
	# Report the negative of the actual count of write requests.
	incr ::KeyVal(MEMSPULSE) -1
    }
    ## LogInfo "MEMSPULSE now $::KeyVal(MEMSPULSE)"

    # Expected response:
    # o  ":" from previous command;
    # o  ":" from memsHB=$count;
    # o  number from TC0.
    #
    # We use count=10 because (see the UserThread block in
    # ../configs/saomot_dispatch_0.conf) the user thread is
    # issuing pulses at about 20 Hz, so this will
    # cause it to run for 1/2 sec before timing out:
    #
    set count 10

    DComm::addSendQueue $::dcommPri(normal) other \
    	"memsHB=$count{N};TC0" \
	    {^([:?]){2,2} *(\d+)} regexp \
		[list custom::check_memsHBrequest $completionvar] ""
}

# -------------------------------------------------------------------------
#
# Callback to be invoked upon getting the response to our
# Galil code for triggering the oneshot.
#
proc check_memsHBrequest {completionvar sendtext response etcinfo} {
    # LogInfo "memsHBrequest send=$sendtext; response=$response"
    if {$completionvar ne ""} {
	set $completionvar [list REQUEST_COMPLETE ""]
    }
}

# -------------------------------------------------------------------------
#
# The XML for bit MEMS_POWER_OUTLET_STATE ties it to keyword MEMSPOWER:
# that is, each time MEMS_POWER_OUTLET_STATE changes, the new state
# is written to keyword MEMSPOWER.  This routine should be set up as
# a trace on MEMSPOWER, and thus on MEMS_POWER_OUTLET_STATE.
#
proc traceMemsPower {args} {

    # MEMSPOWER has changed.
    # Reset the MEMSPULSE counter.
    setIfChanged ::KeyVal MEMSPULSE 0
}


# -------------------------------------------------------------------------
#
#
proc traceHeartbeatMissing {args} {
    # Number of times the heartbeats have gone missing, as counted
    # by the #ININT routine:
    setIfChanged ::KeyVal MEMS_HB_MISS $::X(B,ZA)
}
# -------------------------------------------------------------------------
#
#
proc traceMemsHeartbeatCount {args} {
    # Number of times that we've generated heartbeats:
    setIfChanged ::KeyVal MEMS_HB_GEN $::X(A,ZA)
}

}	; # end namespace custom
