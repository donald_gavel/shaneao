<?xml version="1.0" ?>
<bundle name="$(STAGE)" service="$(KTLSERVICE)" type="MotorStage" xmlns:xsi="http://www.w3.org/2001/XML-Schema-instance">
	<dispatcher>
		<name>$(KTLSERVICE)_dispatch_$(DISPATCHER_NUMBER)</name>
	</dispatcher>
	<keyword>
		<name>$(STAGE)CAL</name>
		<type>boolean</type>
		<longname>$(STAGE_LOWERCASE)-calibration</longname>
		<comment>Calibration state. Setting CAL=reset forces stage to be unhomed. Setting CAL=homed forces a home sequence.</comment>
		<values>
			<entry>
				<key>0</key>
				<value>reset</value>
			</entry>
			<entry>
				<key>1</key>
				<value>homed</value>
			</entry>
		</values>
	</keyword>
        <keyword>
                <name>$(STAGE)CED</name>
                <type>float</type>
                <units>$(ENC_UNITS)</units>
		<range>
		    <minimum>$(ENC_MINIMUM)</minimum>
		    <maximum>$(ENC_MAXIMUM)</maximum>
		</range>
                <tolerance>$(CED_TOLERANCE)</tolerance>
                <longname>$(STAGE_LOWERCASE)-actual_ENC-expected_ENC-difference</longname>
                <comment>Difference between actual ENC value and expected value (ENE, derived from RAW).</comment>
                <format>%.0f</format>
                <related>$(STAGE)ENC</related>
                <related>$(STAGE)ENE</related>
                <related>$(STAGE)RAW</related>
                <capability type="write">False</capability>
        </keyword>
	<keyword>
		<name>$(STAGE)CMT</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-comment</longname>
		<comment>Arbitrary comment. Not written by the service; for use by clients of the service to exchange service-related info. See also TAG.</comment>
	</keyword>
	<keyword>
		<name>$(STAGE)DCC</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-disconnected-cables</longname>
		<comment>Disconnected cables message. Blank if all cables are connected. Contains 'motion disabled' if the disconnected cable is required for stage motion.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)ENC</name>
		<type>integer</type>
		<units>$(ENC_UNITS)</units>
		<range>
		    <minimum>$(ENC_MINIMUM)</minimum>
		    <maximum>$(ENC_MAXIMUM)</maximum>
		</range>
		<longname>$(STAGE_LOWERCASE)-motor-encoder-position</longname>
		<comment>Position as measured by the stage's secondary encoder. If there is a load encoder, this is typically the motor encoder.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)ENE</name>
		<type>double</type>
		<units>$(ENC_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-motor-encoder-expected</longname>
		<comment>Expected ENC value, as derived from RAW value. Allows comparison of RAW and ENC values. When written, tells stage to move RAW such that ENE has this value.</comment>
		<format>%.0f</format>
		<related>$(STAGE)RAW</related>
	</keyword>
	<keyword>
		<name>$(STAGE)ERR</name>
		<type>integer</type>
		<longname>$(STAGE_LOWERCASE)-modify-error-number</longname>
		<comment>Last error number -- it is the last KTL status value. Usually, closely related to MSG.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)GST</name>
		<type>mask</type>
		<longname>$(STAGE_LOWERCASE)-axis-status</longname>
		<comment>Stage axis status bits, as reported by Galil.</comment>
		<capability type="write">False</capability>
		<mask>
			<entry>
				<bit>None</bit>
				<value>None</value>
			</entry>
			<entry>
				<bit>0</bit>
				<value>MOTOR_OFF</value>
			</entry>
			<entry>
				<bit>1</bit>
				<value>HM_PHASE3</value>
			</entry>
			<entry>
				<bit>2</bit>
				<value>LATCH_ARMED</value>
			</entry>
			<entry>
				<bit>3</bit>
				<value>FINAL_DECEL</value>
			</entry>
			<entry>
				<bit>4</bit>
				<value>STOP_BY_ST_OR_LIMIT</value>
			</entry>
			<entry>
				<bit>5</bit>
				<value>SLEWING</value>
			</entry>
			<entry>
				<bit>6</bit>
				<value>CONTOUR</value>
			</entry>
			<entry>
				<bit>7</bit>
				<value>NEG_MOVE</value>
			</entry>
			<entry>
				<bit>8</bit>
				<value>MODE_COORD</value>
			</entry>
			<entry>
				<bit>9</bit>
				<value>HM_PHASE2_OR_FI</value>
			</entry>
			<entry>
				<bit>10</bit>
				<value>HM_PHASE1</value>
			</entry>
			<entry>
				<bit>11</bit>
				<value>HM_IN_PROGRESS</value>
			</entry>
			<entry>
				<bit>12</bit>
				<value>FE_IN_PROGRESS</value>
			</entry>
			<entry>
				<bit>13</bit>
				<value>MODE_PA_NOT_PR</value>
			</entry>
			<entry>
				<bit>14</bit>
				<value>MODE_PA_OR_PR</value>
			</entry>
			<entry>
				<bit>15</bit>
				<value>MOVE_IN_PROGRESS</value>
			</entry>
		</mask>
	</keyword>
	<keyword>
		<name>$(STAGE)GSW</name>
		<type>mask</type>
		<longname>$(STAGE_LOWERCASE)-axis-switches</longname>
		<comment>Stage axis switch bits, as reported by Galil.</comment>
		<capability type="write">False</capability>
		<mask>
			<entry>
				<bit>None</bit>
				<value>None</value>
			</entry>
			<entry>
				<bit>0</bit>
				<value>STEPPER_MODE</value>
			</entry>
			<entry>
				<bit>1</bit>
				<value>HOME_INPUT</value>
			</entry>
			<entry>
				<bit>2</bit>
				<value>REV_LIM_INACTIVE</value>
			</entry>
			<entry>
				<bit>3</bit>
				<value>FWD_LIM_INACTIVE</value>
			</entry>
			<entry>
				<bit>6</bit>
				<value>LATCH_INPUT</value>
			</entry>
			<entry>
				<bit>7</bit>
				<value>LATCH_OCCURRED</value>
			</entry>
		</mask>
	</keyword>
	<keyword>
		<name>$(STAGE)GTE</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-galil-position-error</longname>
		<comment>Current motor positioning error, as reported by the Galil's TE command.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)LCK</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-motion-keyword-lockout</longname>
		<comment>Software lock switch. If set to 'unlocked', motion keywords can be modified; otherwise, they cannot. Normally set by keyword, but is automatically locked whenever local control mode is entered.</comment>
	</keyword>
	<keyword>
		<name>$(STAGE)LIM</name>
		<type>mask</type>
		<longname>$(STAGE_LOWERCASE)-limit-switch-status</longname>
		<comment>Stage limit status.</comment>
		<capability type="write">False</capability>
		<mask>
			<entry>
				<bit>None</bit>
				<value>Not in a limit</value>
			</entry>
			<entry>
				<bit>0</bit>
				<value>Check soft limits</value>
			</entry>
			<entry>
				<bit>1</bit>
				<value>Conflicting active signals</value>
			</entry>
			<entry>
				<bit>2</bit>
				<value>In limit</value>
			</entry>
			<entry>
				<bit>3</bit>
				<value>Soft_Rev</value>
			</entry>
			<entry>
				<bit>4</bit>
				<value>Primary_Rev</value>
			</entry>
			<entry>
				<bit>5</bit>
				<value>Secondary_Rev</value>
			</entry>
			<entry>
				<bit>6</bit>
				<value>Soft_Fwd</value>
			</entry>
			<entry>
				<bit>7</bit>
				<value>Primary_Fwd</value>
			</entry>
			<entry>
				<bit>8</bit>
				<value>Secondary_Fwd</value>
			</entry>
			<entry>
				<bit>9</bit>
				<value>Secondary</value>
			</entry>
		</mask>
	</keyword>
	<keyword>
		<name>$(STAGE)MAP</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-map-units</longname>
		<comment>Maps units.  Use: modify fromval fromsuffix tosuffix. The next show will return mapping of the fromsuffix fromval to the tosuffix toval.</comment>
	</keyword>
	<keyword>
		<name>$(STAGE)MOD</name>
		<type>enumerated</type>
		<longname>$(STAGE_LOWERCASE)-motor-control-mode</longname>
		<comment>Stage control mode</comment>
		<values>
			<entry>
				<key>0</key>
				<value>Halt</value>
			</entry>
			<entry>
				<key>1</key>
				<value>Pos</value>
			</entry>
			<entry>
				<key>2</key>
				<value>Jog</value>
			</entry>
			<entry>
				<key>3</key>
				<value>Track</value>
			</entry>
			<entry>
				<key>4</key>
				<value>Pos ON</value>
			</entry>
			<entry>
				<key>5</key>
				<value>Pos OFF</value>
			</entry>
			<entry>
				<key>6</key>
				<value>Jog ENGR</value>
			</entry>
			<entry>
				<key>7</key>
				<value>Pos ENGR</value>
			</entry>
		</values>
	</keyword>
	<keyword>
		<name>$(STAGE)MOO</name>
		<type>boolean</type>
		<longname>$(STAGE_LOWERCASE)-motor-on-off</longname>
		<comment>Reports whether the motor for this stage is on or off. A value of 'on' can only be written while in 'Pos' mode, which will force the stage to mode 'Pos ON'. A value of 'off' can be written at any time, and will have different effects depending on the state of the stage, but will always result in the motor being turned off as soon as possible.</comment>
		<values>
			<entry>
				<key>0</key>
				<value>Off</value>
			</entry>
			<entry>
				<key>1</key>
				<value>On</value>
			</entry>
		</values>
	</keyword>
	<keyword>
		<name>$(STAGE)MSG</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-lasterror-message</longname>
		<comment>Message. MSG is tied closely to ERR: it is an onto, but not one-to-one, function of ERR, which is essentially the last KTL status value.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)NAM</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-named-position</longname>
		<comment>Name of the current ordinal position.</comment>
		<related>$(STAGE)ORD</related>
		<related>$(STAGE)RAW</related>
		<mapping to="$(STAGE)ORD">
#ifdef RON_LIST
		    #include INSERT_NAM=1 Mapping/RON.xml
#endif
		</mapping>
	</keyword>
	<keyword>
		<name>$(STAGE)ORD</name>
		<type>integer</type>
		<longname>$(STAGE_LOWERCASE)-ordinal-position</longname>
		<comment>Ordinal position, mapping between NAM and RAW values.</comment>
		<related>$(STAGE)RAW</related>
		<related>$(STAGE)NAM</related>
		<mapping to="$(STAGE)RAW">
#ifdef RON_LIST
		    #include INSERT_ORD=1 Mapping/RON.xml
#endif
		</mapping>
	</keyword>
	<keyword>
		<name>$(STAGE)RAW</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<range>
		    <minimum>$(RAW_MINIMUM)</minimum>
		    <maximum>$(RAW_MAXIMUM)</maximum>
		</range>
		<tolerance>$(RAW_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-raw-position</longname>
		<comment>Position as measured by the stage's primary encoder. If available, this is typically the load encoder.</comment>
		<related>$(STAGE)ENE</related>
		<related>$(STAGE)TVA</related>
		<related>$(STAGE)VAL</related>
		<related>$(STAGE)ORD</related>
		<related>$(STAGE)NAM</related>
	</keyword>
	<keyword>
		<name>$(STAGE)RCD</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-remote-control-disablers</longname>
		<comment>Explanation for disabled remote control. Blank if remote control can be enabled; else a terse explanation.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)REL</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-raw-relative-move</longname>
	<comment>Relative move of RAW keyword. On read: return integrated motion since last zeroed. Zeroed when: new pos cmd given (POS mode); or transition from TSP=0 (JOG mode); or any track update (TRACK mode).</comment>
	</keyword>
	<keyword>
		<name>$(STAGE)RON</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-raw-ord-nam-map</longname>
		<comment>RAW-&gt;ORD-&gt;NAM keyword mapping. This is a separated list of RAW/ORD/NAM values; it may be updated on the fly to reflect physical changes in a stage, such as updates to filters in a filter wheel. The first character of the keyword is the separator between the values.</comment>
	</keyword>
	<keyword>
		<name>$(STAGE)SPD</name>
		<type>integer</type>
		<units>$(RAW_UNITS)/s</units>
		<range>
		    <minimum>-$(SPD_MAXIMUM)</minimum>
		    <maximum>$(SPD_MAXIMUM)</maximum>
		</range>
		<tolerance>$(SPD_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-raw-velocity</longname>
		<comment>Stage velocity, in $(RAW_UNITS)/s.</comment>
		<related>$(STAGE)VEL</related>
	</keyword>
	<keyword>
		<name>$(STAGE)STA</name>
		<type>enumerated</type>
		<longname>$(STAGE_LOWERCASE)-overall-status</longname>
		<comment>Control status.</comment>
		<capability type="write">False</capability>
		<values>
			<entry>
				<key>0</key>
				<value>Locked</value>
			</entry>
			<entry>
				<key>1</key>
				<value>Calibrating</value>
			</entry>
			<entry>
				<key>2</key>
				<value>Not Calibrated</value>
			</entry>
			<entry>
				<key>3</key>
				<value>Halted</value>
			</entry>
			<entry>
				<key>4</key>
				<value>Ready</value>
			</entry>
			<entry>
				<key>5</key>
				<value>Stopping</value>
			</entry>
			<entry>
				<key>6</key>
				<value>Moving</value>
			</entry>
			<entry>
				<key>7</key>
				<value>Jogging</value>
			</entry>
			<entry>
				<key>8</key>
				<value>Slewing</value>
			</entry>
			<entry>
				<key>9</key>
				<value>Acquiring</value>
			</entry>
			<entry>
				<key>10</key>
				<value>Tracking</value>
			</entry>
			<entry>
				<key>11</key>
				<value>At Max</value>
			</entry>
			<entry>
				<key>12</key>
				<value>At Min</value>
			</entry>
			<entry>
				<key>13</key>
				<value>Local/Stopped</value>
			</entry>
			<entry>
				<key>14</key>
				<value>Local/Jogging</value>
			</entry>
			<entry>
				<key>15</key>
				<value>Fault</value>
			</entry>
			<entry>
				<key>16</key>
				<value>Disabled</value>
			</entry>
		</values>
	</keyword>
	<keyword>
		<name>$(STAGE)STP</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-stop-request</longname>
		<comment>Request stop. When written: stops stage (but leaves stage mode unchanged). When read: returns the last value written.</comment>
	</keyword>
#if $(HAS_TRACK_MODE)
	<keyword>
		<name>$(STAGE)SVA</name>
		<type>double</type>
		<units>$(VAL_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-servo-val-tracking-error</longname>
	<comment>Motor tracking error, in $(VAL_UNITS). Difference between requested and actual value of VAL, similar to SVR keyword.</comment>
		<format>%.3f</format>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)SVR</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-servo-raw-tracking-error</longname>
		<comment>Load tracking error -- the difference between the requested and actual RAW values.</comment>
		<capability type="write">False</capability>
	</keyword>
#endif
	<keyword>
		<name>$(STAGE)TAG</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-tag</longname>
		<comment>Tag. Not written by the service; for use by clients of the service to use as they wish. Like CMT, but TAG lets scripts tag logged data, while retaining CMT for inter-script or inter-user communication.</comment>
	</keyword>
#if $(HAS_TRACK_MODE)
	<keyword>
		<name>$(STAGE)TIM</name>
		<type>double</type>
		<longname>$(STAGE_LOWERCASE)-track-timestamp</longname>
		<comment>Timestamp, in seconds since 1970.0  When MOD=TRACK, the dispatcher shall endeavor to make the stage match the demand (RAW,SPD,TIM). The demand is updated each time TIM is written.</comment>
		<format>%.6f</format>
	</keyword>
#endif
#if $(HAS_TEMPERATURE)
	<keyword>
		<name>$(STAGE)TMC</name>
		<type>double</type>
		<units>$(TMC_UNITS)</units>
		<range>
		    <minimum>$(TMC_MINIMUM)</minimum>
		    <maximum>$(TMC_MAXIMUM)</maximum>
		</range>
		<tolerance>$(TMC_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-motor-temperature-celsius</longname>
		<comment>$(STAGE) motor temperature in degrees Celcius</comment>
		<format>$(TMC_FORMAT)</format>
		<related>$(STAGE)TMR</related>
		<capability type="write">False</capability>
		#include temperature-Conversion.xml
	</keyword>
	<keyword>
		<name>$(STAGE)TMR</name>
		<type>double</type>
		<units>$(TMR_UNITS)</units>
		<range>
		    <minimum>$(TMR_MINIMUM)</minimum>
		    <maximum>$(TMR_MAXIMUM)</maximum>
		</range>
		<tolerance>$(TMR_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-motor-temperature-raw</longname>
		<comment>$(STAGE) motor temperature raw voltage reading</comment>
		<format>$(TMR_FORMAT)</format>
		<related>$(STAGE)TMC</related>
		<capability type="write">False</capability>
	</keyword>
#endif
	<keyword>
		<name>$(STAGE)TNM</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-named-position-target</longname>
		<comment>NAM value for the current TRD value.</comment>
		<related>$(STAGE)TRD</related>
		<related>$(STAGE)TRG</related>
		<capability type="write">False</capability>
	</keyword>
#if $(IS_SERVO)
	<keyword>
		<name>$(STAGE)TOR</name>
		<type>double</type>
		<tolerance>0.001</tolerance>
		<longname>$(STAGE_LOWERCASE)-motor-torque</longname>
	<comment>Motor torque, measured as voltage output to a current amplifier for a servomotor. Note: the dispatcher won't update the torque keyword until the torque changes by the tolerance level.</comment>
		<format>%.3f</format>
		<capability type="write">False</capability>
	</keyword>
#endif
	<keyword>
		<name>$(STAGE)TRD</name>
		<type>integer</type>
		<longname>$(STAGE_LOWERCASE)-ordinal-position-target</longname>
		<comment>Ordinal position of the current target position, TRG.</comment>
		<related>$(STAGE)TRG</related>
		<related>$(STAGE)TNM</related>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)TRG</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-raw-position-target</longname>
		<comment>RAW position target, in $(RAW_UNITS).</comment>
		<related>$(STAGE)TRD</related>
		<related>$(STAGE)TNM</related>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)TRL</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-raw-relative-move-target</longname>
	<comment>Relative move of RAW keyword, target position. Contains distance to target from start of move (POS mode); else 0.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)TSP</name>
		<type>integer</type>
		<units>$(RAW_UNITS)/s</units>
		<longname>$(STAGE_LOWERCASE)-raw-velocity-target</longname>
		<comment>Target speed (SPD). This indicates the speed that the controller is attempting to drive the stage at, while SPD is a measure of the instantaneous velocity of the load encoder.</comment>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)TVA</name>
		<type>double</type>
		<units>$(VAL_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-position-target</longname>
		<comment>VAL target position.</comment>
		<format>%.3f</format>
		<related>$(STAGE)RAW</related>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)VAL</name>
#ifndef VAL_TYPE
		<type>double</type>
#else
		<type>$(VAL_TYPE)</type>
#endif
		<units>$(VAL_UNITS)</units>
		<range>
		    <minimum>$(VAL_MINIMUM)</minimum>
		    <maximum>$(VAL_MAXIMUM)</maximum>
		</range>
		<tolerance>$(VAL_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-position</longname>
#ifndef VAL_FORMAT
		<format>%.2f</format>
#else
		<format>$(VAL_FORMAT)</format>
#endif
		<comment>Position, in $(VAL_UNITS), as derived from the RAW value.</comment>
		<related>$(STAGE)RAW</related>
#if $(HAS_VAX)
		<related>$(STAGE)VAX</related>
#endif
	</keyword>
	<keyword>
		<name>$(STAGE)VEL</name>
		<type>double</type>
		<units>$(VAL_UNITS)/s</units>
		<range>
		    <minimum>-$(VEL_MAXIMUM)</minimum>
		    <maximum>$(VEL_MAXIMUM)</maximum>
		</range>
		<tolerance>$(VEL_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-velocity</longname>
		<comment>Velocity, in $(VAL_UNITS)/s, as derived from the SPD value.</comment>
		<format>%.3f</format>
		<related>$(STAGE)SPD</related>
#if $(HAS_VAX)
		<related>$(STAGE)VEX</related>
#endif
	</keyword>
#if $(HAS_VAX)
	<keyword>
		<name>$(STAGE)VAX</name>
		<type>double</type>
#ifndef VAX_BINARY_UNITS
		<units>$(VAX_UNITS)</units>
#else
		<units format="ascii">
			<label>$(VAX_UNITS)</label>
#ifdef VAX_ASC_TO_BIN_INCLUDE
		#include $(VAX_ASC_TO_BIN_INCLUDE)
#endif
		</units>
		<units format="binary">
			<label>$(VAX_BINARY_UNITS)</label>
#ifdef VAX_BIN_TO_ASC_INCLUDE
		#include $(VAX_BIN_TO_ASC_INCLUDE)
#endif
		</units>
#endif
		<range>
#ifdef VAX_MINIMUM
		    <minimum>$(VAX_MINIMUM)</minimum>
#endif
#ifdef VAX_MAXIMUM
		    <maximum>$(VAX_MAXIMUM)</maximum>
#endif
		</range>
#ifdef VAX_TOLERANCE
		<tolerance>$(VAX_TOLERANCE)</tolerance>
#endif
		<longname>$(STAGE_LOWERCASE)-position-$(VAX_UNITS)</longname>
		<comment>$(STAGE_LOWERCASE), in $(VAX_UNITS), as derived from the VAL value.</comment>
		<format>%.1f</format>
		<related>$(STAGE)VAL</related>
		<related>$(STAGE)TVX</related>
	</keyword>
	<keyword>
		<name>$(STAGE)SVX</name>
		<type>double</type>
		<units>$(VAX_UNITS)</units>
		<longname>$(STAGE_LOWERCASE)-servo-vax-tracking-error</longname>
		 <comment>Motor tracking error, in $(VAX_UNITS). Difference between requested and actual value of VAX, similar to SVA keyword.</comment>
		<format>%.3f</format>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)VEX</name>
		<type>double</type>
		<units>$(VAX_UNITS)/s</units>
		<range>
#ifdef VEX_MINIMUM
		    <minimum>$(VEX_MINIMUM)</minimum>
#endif
#ifdef VEX_MAXIMUM
		    <maximum>$(VEX_MAXIMUM)</maximum>
#endif
		</range>
#ifdef VEX_TOLERANCE
		<tolerance>$(VEX_TOLERANCE)</tolerance>
#endif
		<longname>$(STAGE_LOWERCASE)-rate-vax-units-per-second</longname>
		 <comment>$(STAGE_LOWERCASE) rate of motion, in $(VAX_UNITS)/s, as derived from the VEL value.</comment>
		<format>%.3f</format>
		<related>$(STAGE)VEL</related>
	</keyword>
	<keyword>
		<name>$(STAGE)TVX</name>
		<type>double</type>
#ifndef VAX_BINARY_UNITS
		<units>$(VAX_UNITS)</units>
#else
		<units format="ascii">
			<label>$(VAX_UNITS)</label>
#ifdef VAX_ASC_TO_BIN_INCLUDE
		#include $(VAX_ASC_TO_BIN_INCLUDE)
#endif
		</units>
		<units format="binary">
			<label>$(VAX_BINARY_UNITS)</label>
#ifdef VAX_BIN_TO_ASC_INCLUDE
		#include $(VAX_BIN_TO_ASC_INCLUDE)
#endif
		</units>
#endif

		<range>
#ifdef VAX_MINIMUM
		    <minimum>$(VAX_MINIMUM)</minimum>
#endif
#ifdef VAX_MAXIMUM
		    <maximum>$(VAX_MAXIMUM)</maximum>
#endif
		</range>
#ifdef VAX_TOLERANCE
		<tolerance>$(VAX_TOLERANCE)</tolerance>
#endif
		<longname>$(STAGE_LOWERCASE)-position-target-vax</longname>
		<comment>VAX target position.</comment>
		<format>%.3f</format>
		<related>$(STAGE)RAW</related>
		<related>$(STAGE)VAX</related>
	</keyword>
#endif
	<keyword>
		<name>$(STAGE)XMV</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-cannot-move-reasons</longname>
		<comment>Shows all the reasons that this stage's status is disabled.</comment>
		<related>$(STAGE)STA</related>
		<capability type="write">False</capability>
	</keyword>
	<keyword>
		<name>$(STAGE)NPX</name>
		<type>string</type>
		<longname>$(STAGE_LOWERCASE)-named-position-prefix</longname>
		<related>$(STAGE)RON</related>
	</keyword>
	<keyword>
		<name>$(STAGE)SWT</name>
		<type>boolean</type>
		<longname>$(STAGE_LOWERCASE)-stopped-within-tolerance</longname>
		<comment>value is inPosition if stage completed a move and is within tolerance</comment>
		<capability type="write">False</capability>
		<values>
			<entry>
				<key>0</key>
				<value>false</value>
			</entry>
			<entry>
				<key>1</key>
				<value>true</value>
			</entry>
		</values>
	</keyword>
	<keyword>
		<name>$(STAGE)ZPX</name>
		<type>integer</type>
		<units>$(RAW_UNITS)</units>
		<tolerance>$(RAW_TOLERANCE)</tolerance>
		<longname>$(STAGE_LOWERCASE)-assign-position-and-assert-homed</longname>
		<related>$(STAGE)RAW</related>
	</keyword>
</bundle>
