;restore,'albedo.sav'
albjun = readfits('/u2/gavel/guidestar/titan/albedojun.fits')
albsept = readfits('/u2/gavel/guidestar/titan/albsept.fits')
cx = 40
cy = 43
pixelsize = 0.02
d = 180./300.
cfact = 1.1
;
;  september data
;
sizefac = 1.15
angdiam = 0.83/sizefac
rx = (angdiam/2.) / pixelsize
ry = rx
subEarthLat = -5.20
subEarthLong = 125.02
subSunLat = -4.43
subSunLong = 127.12
mask = circle(80,80,cx,cy,cfact*rx,1)
mapSept0 = planetMap(albsept*mask,cx,cy,rx,ry,subEarthLat,0.,d)
mapSept = fltarr(600,300)
mapSept(0:299,*) = mapSept0
mapSept = shift(mapSept,150 - subEarthLong/d)
mapSepts = shift(mapSept,90./d)
;
maskSept0 = planetMap(mask,cx,cy,rx,ry,subEarthLat,0.,d)
maskSept = fltarr(600,300)
maskSept(0:299,*) = maskSept0
maskSept = shift(maskSept,150 - subEarthLong/d)
maskSepts = shift(maskSept,90./d)
;mapa = mapSepts<.7
;
;  june data
;
cx = 40
cy = 40
angdiam = 0.748/sizefac
rx = (angdiam/2.) / pixelsize
ry = rx
subEarthLat = -6.13
subEarthLong = 323.59
subSunLat = -3.37
subSunLong = 329.08
mask = circle(80,80,cx,cy,cfact*rx,1)
mapJun0 = planetMap(albjun*mask,cx,cy,rx,ry,subEarthLat,0.,d)
mapJun = fltarr(600,300)
mapJun(0:299,*) = mapJun0
mapJun = shift(mapJun,150 - subEarthLong/d)
mapJuns = shift(mapJun,90./d)
;
maskJun0 = planetMap(mask,cx,cy,rx,ry,subEarthLat,0.,d)
maskJun = fltarr(600,300)
maskJun(0:299,*) = maskJun0
maskJun = shift(maskJun,150 - subEarthLong/d)
maskJuns = shift(maskJun,90./d)
;
;  combine 2 maps
;
masksep = mapSepts gt 0
maskjun = mapJuns gt 0
;mapa = mapSepts*(masksep and not maskjun) + mapJuns*(maskjun and not masksep) + $
;       (mapSepts + mapJuns)*(masksep and maskjun)/2.
mapa0 = mapSepts*(maskSepts and not maskJuns) + mapJuns*(maskJuns and not maskSepts) + $
       (mapSepts + mapJuns)*(maskSepts and maskJuns)/2.
;
mapa = mapa0 - (.0002)*real(not (maskSepts or maskJuns))
;mapa = mapa>0
;
;   plot the result
;    
;   blank out areas above 60 degrees latitude
mapa(*,150+60/d:299) = 0
mapa(*,0:150-60/d,*) = 0
mapnolines = mapa
;
;   draw lat/long lines
;
m = max(mapa)/2.
for i=0,359,30 do begin & $
  mapa(i/d,*) = m & $
endfor
for i=-60,60,30 do begin & $
  mapa(*,150+i/d) = m & $
endfor
mapa(*,150+60/d:299) = 0
mapa(*,0:150-60/d,*) = 0
;
;   send to graphics device and annotate
;
if (!d.name eq 'X') then window,1,xsize=600,ysize=300
if (!d.name eq 'PS') then $
  device,filename = 'mapalbedo.ps',/color,xsize=20,/encapsulated,ysize=10,bits_per_pixel=8
loadct,3
exptv,mapa,/nobox,/data
;contour,smooth(mapnolines,5),/overplot,levels=[.1,.15],color=0
;tvscl,mapa
;
;   write latitude/longitue labels
;
for i=-180,179,60 do begin & $
  xyouts, ((300-40 + (i+90)*(600./360.)) mod 600)/600., (150-60*(300./180.)-10)/300., string(-i), /normal, color=255 & $
endfor
for i=-60,60,60 do begin & $
  xyouts, (300-30 + 90*(600./360.))/600., (150 + i*(300./180.) + 5)/300., string(i), /normal, color=255 & $
endfor
;
;  title
;
xyouts, .2, .9, '!6Titan Albedo Map at 2.1 microns',/normal, color=255,charsize=2
if (!d.name eq 'PS') then device,/close
