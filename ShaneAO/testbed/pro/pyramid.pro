n = 256
microns = 1.e-6
lambda = 0.633*microns
knife = fltarr(n,n)
knife[0:n/2-1,*]=1
ap = circle(n,n,n/2,n/2,n/8,1.)
ap = ap*knife
i = complex(0,1.)
ph = 0*real(zernike(n,n,n/8,2,0))
wf = ap*exp(i*ph)
fp = ft(wf)
im = abs(fp)^2
; knife edge
edge = fltarr(n,n)
edge[(n/2-40):n/2-1,*] = 1.
;edge = smooth(edge,21)
fp2 = fp*edge
pp = ft(fp2,/inverse)
; pyramid
pyrd = edge*transpose(edge)
;pyrd = smooth(pyrd,21)
pyrc = rotate(pyrd,1)
pyrb = rotate(pyrc,1)
pyra = rotate(pyrb,1)
fpa = fp*pyra
fpb = fp*pyrb
fpc = fp*pyrc
fpd = fp*pyrd
ppa = abs(ft(fpa,/inverse))^2
ppb = abs(ft(fpb,/inverse))^2
ppc = abs(ft(fpc,/inverse))^2
ppd = abs(ft(fpd,/inverse))^2
dispgui,[[ppd,ppc],[ppa,ppb]],'4 pupils'
dispgui,[[(1-ap)*ppd,(1-ap)*ppc],[(1-ap)*ppa,(1-ap)*ppb]],'4 pupils scattered light'
sx = ((ppa+ppd)-(ppb+ppc))/(ppa+ppb+ppc+ppd)
sy = ((ppa+ppb)-(ppd+ppc))/(ppa+ppb+ppc+ppd)
dispgui,[sx*ap,sy*ap],'sensor output'
dispgui,[-ap*(ph-shift(ph,1,0)),ap*(ph-shift(ph,0,1))],'actual phase derivs'
end
