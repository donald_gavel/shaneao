;+
;  makeKeckAp.pro - create the Keck aperture and/or psf
;-

function makeKeckAp,n,lambda=lambda,dx=dx
  arcsec = !pi/(180.*3600.)
  if (n_elements(n) eq 0) then n = 256
  microns = 1e-6
  if (not keyword_set(lambda)) then lambda = 1.6 * microns
  if (not keyword_set(dx)) then dx = 0.02 * arcsec
  du = lambda / (n*dx)
  seg = hex(n,.93/du)
  seg = rotate(seg,30)
  keckAp = keckap(du,.9,seg)
  return,keckAp
end

function makeKeckPsf,keckAp
  psf = abs(ft(keckAp))^2
;  psf = abs(shift(fft(keckAp),128,128))^2
  psf = psf/total(psf)
  return,psf
end

function makeKeckMtf,n,lambda=lambda,dx=dx
  arcsec = !pi/(180.*3600.)
  if (n_elements(n) eq 0) then n = 256
  microns = 1e-6
  if (not keyword_set(lambda)) then lambda = 1.6 * microns
  if (not keyword_set(dx)) then dx = 0.02 * arcsec
  ap = makeKeckAp(n,lambda=lambda,dx=dx)
  psf = makeKeckPsf(ap)
  mtf = ft(psf)
  return,mtf
end
