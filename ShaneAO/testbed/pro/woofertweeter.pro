;
; WooferTweeter.pro
;  solve the associated optimal control problem
;
tau_a = 1. ; atmosphere time constant, e.g. 1 -> 3 m/sec over a 3 m telescope
tau_w = .05 ; woofer time constant (200 Hz rolloff)
Bw = .0001 ; woofer control penalty
Bt = .001 ; tweeter control penalty
Ae = 50. ; residual wavefront penalty
At = 30. ; tweeter state penalty
;
; state vector is: x = [x_w, x_t - x_0]
;   where 0 is the uncontrolled atmosphere
;         w is the woofer state
;         t is the tweeter state
F = [[-1/tau_w, 1, 0, 0       ], $
     [0,        0, 0, 0       ], $
     [0,        0, 0, 0       ], $
     [0,        0, 0, -1/tau_a]]
G = [[0,0],[1,0],[0,1],[0,0]]
H = [-1,0,-1,1]
A = Ae*transpose(H) ## H ; penalty for tracking error
A[2,2] += At ; penalty for tweeter state
B = [[Bw,0],[0,Bt]] ; penalty for control
;
;  solve the algebraic riccati equation
S = fltarr(4,4);[[1,0],[0,1]]
GBG = G ## invert(B) ## transpose(G)
dt = .001
niter = 50
for iter = 0,niter-1 do begin
  dS = -(-S ## F - transpose(F) ## S + S ## GBG ## S - A)*dt ; propagate "backwards"
  S += dS
  S = (1/2.)*(S + transpose(S)) ; keep it symmetric
  print,iter,eigenql(S) & wait,.01
endfor
C = invert(B) ## transpose(G) ## S
; demonstrate performance
hes = elmhes(F - G ## C)
evals = hqr(hes)
print,'eigenvalues:'
print,evals

;seed = 9
sdx = 1.
x = transpose([0,0,0,1])*0
Q = 2./tau_a
sd = sqrt(Q)
tf = 5.0 ; second
dt = .001
niter = fix(tf/dt)
x_save = fltarr(niter+1,4)
x_save[0,*] = x
t = findgen(niter+1)*dt

for iter = 0,niter-1 do begin
  w = randomn(seed)*sd
  dx = (F ## x - G ## (C ## x) + transpose([0,0,0,1])*w)*dt
  x += dx
  x_save[iter+1,*] = x
endfor
z_save = x_save[*,3]-x_save[*,0]-x_save[*,1]

device,decomposed = 1
loadct,5
p_save = !p
!p.background = fsc_color('white')
!p.color = fsc_color('black')
background_c = fsc_color('wt3')
legend_c = fsc_color('wt1')
atmos_c = fsc_color('black')
woofer_c = fsc_color('red')
tweeter_c = fsc_color('black')
total_c = fsc_color('blue')
major_c = fsc_color('orange')
minor_c = fsc_color('pink')

!p.charsize=1.5
!p.thick = 1

range = [0.,0.]
range[0] = min([min(x_save[*,3]),min(x_save[*,0]),min(x_save[*,2]),min(x_save[*,0]+x_save[*,2])])
range[1] = max([max(x_save[*,3]),max(x_save[*,0]),max(x_save[*,2]),max(x_save[*,0]+x_save[*,2])])
!p.background = background_c
window,/free
plot,t,x_save[*,3],yrange=range, linestyle=0, /nodata
;tickgrid,[min(t),max(t),1,.1],[range[0],range[1],.05,.005]
tickgrid,[!x.crange[0],!x.crange[1],1,.1],[!y.crange[0],!y.crange[1],.05,.01],color=[major_c,minor_c]
oplot,t,0*t,thick=2
oplot,t,x_save[*,3],linestyle=0,color=atmos_c,thick=1.
oplot,t,x_save[*,0],linestyle=5,color=woofer_c,thick=1.5
oplot,t,x_save[*,2],linestyle=0,color=tweeter_c,thick=1.5
oplot,t,x_save[*,0]+x_save[*,2],linestyle=2,color=total_c,thick=1.5
!p.background = legend_c
legend,['atmosphere','woofer phase','tweeter phase','total of DM phases'], $
        linestyle=[0,5,0,2], color=[atmos_c,woofer_c,tweeter_c,total_c], thick=[1,1,1,1]*1.5,charsize=1.5,/clear
;
ft0 = abs(ft(x_save[*,3])) ; uncontrolled atmospere
ftw = abs(ft(x_save[*,0])) ; woofer control
ftt = abs(ft(x_save[*,2])) ; tweeter control
df = 1/tf
fn = 1/(2*dt)
fs = findgen(2*fix(fn/df))*df - fn
;

!p.background = background_c
window,/free
range = [min([min(ft0),min(ftw),min(ftt)]),max([max(ft0),max(ftw),max(ftt)])]
;range = [min([min(ftt/ft0),min(ftw/ft0)]),max([max(ftt/ft0),max(ftw/ft0)])]
plot,fs,ftt,xtitle = 'frequency, Hz',yrange=range,xrange=[df,fn],/xlog,/ylog,/nodata ; 
;tickgrid,[df,fn],[!y.crange[0],!y.crange[1],.005,.001],/xlog,color=[major_c,minor_c]
tickgrid,[df,fn],[10^!y.crange[0],10^!y.crange[1]],/xlog,/ylog,color=[major_c,minor_c]
oplot,fs,ft0,thick=1.5 ; solid: uncontrolled atmosphere
oplot,fs,ftt,linestyle = 2,thick=1.5 ; short dashed: tweeter commands
oplot,fs,ftw,linestyle = 5,color=200,thick=1.5 ; dashed red: woofer commands
!p.background = legend_c
legend,['atmosphere','tweeter phase','woofer phase'], $
           linestyle=[0,2,5],color=[!p.color,!p.color,200],thick=[1,1,1]*1.5,charsize=1.5,/clear,/right
;oplot,fs,ftt,color=200

!p = p_save
end

