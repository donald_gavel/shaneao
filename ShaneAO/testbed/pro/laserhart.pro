;
;  laserhart.pro - analyze laser hartmann data
;
; inputs:
;   raw = hartmann image, background-subtracted
;   ref = reference hartmann image, background-subtracted
;   xloc, yloc = pre-computed nominal hartmann positions (centers of ROIs)
;   roisize = size of ROI, in pixels
;   mag = display magnification
;
; outputs:
;   cent = 2x31 array of x,y centroid locations with respect to reference centroid locations
;   dispb = handy 2d display of centroid positions
;
function hartcents,raw,ref,xloc,yloc,roisize,dispb,mag=mag
  roi = fltarr(4,31)
  roi[0,*] = xloc - roisize/2.
  roi[1,*] = xloc + roisize/2.
  roi[2,*] = yloc - roisize/2.
  roi[3,*] = yloc + roisize/2.
  allroi = fltarr(4)  ; calculate the bounding box of rois
  allroi[0] = min(roi[0,*])
  allroi[1] = max(roi[1,*])
  allroi[2] = min(roi[2,*])
  allroi[3] = max(roi[3,*])

  cent = fltarr(2,31)
  for i=0,30 do $
    cent[*,i] = centroid(raw[roi[0,i]:roi[1,i],roi[2,i]:roi[3,i]]) - [roisize,roisize]/2.

  centr = fltarr(2,31)
  for i=0,30 do $
    centr[*,i] = centroid(ref[roi[0,i]:roi[1,i],roi[2,i]:roi[3,i]]) - [roisize,roisize]/2.

  cent = cent-centr

  if n_elements(mag) eq 0 then mag = 1   ;  magnification of centroid offset display
  dispa = raw*0
  for i=0,30 do $
    dispa[xloc[i]+mag*cent[0,i],yloc[i]+mag*cent[1,i]] = 1

  disp0 = raw*0
  for i=0,30 do $
    disp0[roi[0,i]:roi[1,i],roi[2,i]:roi[3,i]] = circle(roisize+1,roisize+1,roisize/2.,roisize/2.,roisize/2.,1.)

  dispb = (dispa-disp0)[allroi[0]:allroi[1],allroi[2]:allroi[3]]
  return,cent

end

;
;  nomhart - compute nominal hartmann locations; defines the ROIs
;            specific to the laser Hartmann 31-subaperture array
;
;  inputs:
;   x0, y0 - nominal location of center Hartmann spot in the data image
;   d - nominal separation of Hartmann spots, center-to-center
;  outputs:
;   xloc, yloc - nominal positions of the 31 Hartmann spots
;
pro nomhart,x0,y0,d,xloc,yloc
  dx = d
  dy = d*sqrt(3.)/2.

  xloc = [          -.5, .5, $
                -2, -1, 0, 1, 2, $
        -2.5, -1.5, -.5, .5, 1.5, 2.5, $
                -2, -1, 0, 1, 2, $
        -2.5, -1.5, -.5, .5, 1.5, 2.5, $
                -2, -1, 0, 1, 2, $
                     -.5, .5  ]

  yloc = [           -3, -3, $
                -2, -2, -2, -2, -2, $
              -1, -1, -1,  -1, -1, -1, $
                 0,  0,  0,  0,  0, $
               1,  1,  1,   1,  1,  1, $
                 2,  2,  2,  2,  2, $
                       3,  3  ]

  xloc = xloc*dx + x0
  yloc = yloc*dy + y0

end

function zernikemodes,x,y,nz
  if (n_elements(nz) eq 0) then nz = 10
  r = sqrt(x^2+y^2)
  theta = atan(y,x)
  modes = fltarr(31,10)
  modes[*,0] = r*cos(theta)/2.		; x/2
  if (nz eq 1) then return,modes
  modes[*,1] = r*sin(theta)/2.		; y/2
  if (nz eq 2) then return,modes
  modes[*,2] = (2*r^2-1)/2.		; x^2 + y^2 - 1/2.
  if (nz eq 3) then return,modes
  modes[*,3] = r^2*cos(2*theta)/2.	; (x^2 - y^2)/2.
  if (nz eq 4) then return,modes
  modes[*,4] = r^2*sin(2*theta)/2.	; x*y
  if (nz eq 5) then return,modes
  modes[*,5] = (3*r^3-2*r)*cos(theta)/2.; (3*x^2 + 3*y^2 - 2)*x/2
  if (nz eq 6) then return,modes
  modes[*,6] = (3*r^3-2*r)*sin(theta)/2.; (3*x^2 + 3*y^2 - 2)*y/2
  if (nz eq 7) then return,modes
  modes[*,7] = r^3*cos(3*theta)/2.	; (x^3 - 3*x*y^2)/2
  if (nz eq 8) then return,modes
  modes[*,8] = r^3*sin(3*theta)/2.	; (3*x^2*y-y^3)/2
  if (nz eq 9) then return,modes
  modes[*,9] = (6*r^4-6*r^2+1)		; 1 - 6*x^2 + 6*x^4 - 6*y^2 + 12*x^2*y^2 + 6*y^4
  if (nz eq 10) then return,modes
  modes[*,10] = (4*r^4 - 3*r^2)*cos(2*theta); 4*(x^4-y^4) - 3*(x^2-y^2)
  if (nz eq 11) then return,modes
  modes[*,11] = (4*r^4 - 3*r^2)*sin(2*theta); 8*x^3*y + 8*x*y^3 - 6*x*y
  if (nz eq 12) then return,modes
  modes[*,12] = r^4*cos(4*theta)        ; x^4-6*x^2*y^2 + y^4
  if (nz eq 13) then return,modes
  modes[*,13] = r^4*sin(4*theta)        ; 4*x^3*y - 4*x*y^3
  if (nz eq 14) then return,modes
  modes[*,14] = (10*r^5 - 12*r^3 + 3*r)*cos(theta)
;		3*x - 12*x^3 + 10*x^5 - 12*x*y^2 + 20*x^3*y^2 + 10*x*y^4
  if (nz eq 15) then return,modes
  modes[*,15] = (10*r^5 - 12*r^3 + 3*r)*sin(theta)
;		3*y - 12*x^2*y + 10*x^4*y - 12*y^3 + 20*x^2*y^3 + 10*y^5
  if (nz eq 16) then return,modes
  modes[*,16] = (5*r^5 - 4*r^3)*cos(3*theta)
;		-4*x^3 + 12*x*y^2 + 5*x^3*(x^2 + y^2) - 15*x*y^2*(x^2 + y^2)
  if (nz eq 17) then return,modes
  modes[*,17] = (5*r^5 - 4*r^3)*sin(3*theta) 
;		-12*x^2*y + 4*y^3 + 15*x^2*y*(x^2 + y^2) - 5*y^3*(x^2 + y^2)
  if (nz eq 18) then return,modes
  modes[*,18] = r^5*cos(5*theta)        ; x^5 - 10*x^3*y^2 + 5*x*y^4
  if (nz eq 19) then return,modes
  modes[*,19] = r^5*sin(5*theta)        ; 5*x^4*y - 10*x^2*y^3 + y^5
;
;   {Cos[theta] -> x/r, Sin[theta] -> y/r}   r->Sqrt[x^2+y^2]
;   Cos[2*theta] -> Cos[theta]^2-Sin[theta]^2
;   Sin[2*theta] -> 2*Sin[theta]*Cos[theta]
;   Cos[3*theta] -> 4*Cos[theta]^3*-3*Cos[theta]
;   Sin[3*theta] -> 3*Sin[theta]-4*Sin[theta]^3
;   Cos[4*theta] -> 8*Cos[theta]^4-8*Cos[theta]^2+1
;   Sin[4*theta] -> 8*Cos[theta]^3]*Sin[theta]-4*Cos[theta]*Sin[theta]
;   Cos[5*theta] -> Cos[theta]^5 - 10*Cos[theta]^3*Sin[theta]^2 + 5*Cos[theta]*Sin[theta]^4
;   sin[5*theta] -> 5*Cos[theta]^4*Sin[theta] - 10*Cos[theta]^2*Sin[theta]^3 + Sin[theta]^5

  return,modes
end

function responsematrix,x,y,nz
  if (n_elements(nz) eq 0) then nz = 10
  n = (size(x))(1)

  hmat = fltarr(n*2,nz)

  hmat[0:n-1,0] = 0.5			; d tip / dx
  hmat[n:2*n-1,0] = 0.			; d tip / dy
  if (nz eq 1) then return,transpose(hmat)

  hmat[0:n-1,1] = 0.			; d tilt / dx
  hmat[n:2*n-1,1] = 0.5			; d tilt / dy
  if (nz eq 2) then return,transpose(hmat)

  hmat[0:n-1,2] = 2.*x		 	; d focus / dx
  hmat[n:2*n-1,2] = 2.*y		; d focus / dx
  if (nz eq 3) then return,transpose(hmat)

  hmat[0:n-1,3] = x	  		; d astigx / dx
  hmat[n:2*n-1,3] = -y		 	; d astigx / dy
  if (nz eq 4) then return,transpose(hmat)

  hmat[0:n-1,4] = y		    	; d astigy / dx
  hmat[n:2*n-1,4] = x		    	; d astigy / dy
  if (nz eq 5) then return,transpose(hmat)

  x2 = x^2
  y2 = y^2
  xy = x*y
;  hmat[0:n-1,5] = (3.*y^2+9.*x^2-2.)/2.	; d comax / dx
  hmat[0:n-1,5] = (3.*y2+9.*x2-2.)/2.	; d comax / dx
;  hmat[n:2*n-1,5] = 3.*x*y		; d comax / dy
  hmat[n:2*n-1,5] = 3.*xy		; d comax / dy
  if (nz eq 6) then return,transpose(hmat)

;  hmat[0:n-1,6] = 3.*x*y			; d comay / dx
  hmat[0:n-1,6] = 3.*xy			; d comay / dx
;  hmat[n:2*n-1,6] = (3.*x^2+9.*y^2-2.)/2.	; d comay / dy
  hmat[n:2*n-1,6] = (3.*x2+9.*y2-2.)/2.	; d comay / dy
  if (nz eq 7) then return,transpose(hmat)

;  hmat[0:n-1,7] = 3.*(x^2 - y^2)/2.	; d tricomax / dx
  hmat[0:n-1,7] = 3.*(x2 - y2)/2.	; d tricomax / dx
;  hmat[n:2*n-1,7] = -3.* x* y		; d tricomax / dy
  hmat[n:2*n-1,7] = -3.* xy		; d tricomax / dy
  if (nz eq 8) then return,transpose(hmat)

;  hmat[0:n-1,8] = 3.*x* y		; d tricomay / dx
  hmat[0:n-1,8] = 3.*xy		; d tricomay / dx
;  hmat[n:2*n-1,8] = 3.*( x^2 -  y^2)/2.	; d tricomay / dy
  hmat[n:2*n-1,8] = 3.*( x2 -  y2)/2.	; d tricomay / dy
  if (nz eq 9) then return,transpose(hmat)

  x3 = x2*x
  y3 = y2*y
;  hmat[0:n-1,9] = -12*x + 24*x^3 + 24*x*y^2	; d sphere / dx
  hmat[0:n-1,9] = -12*x + 24*x3 + 24*x*y2	; d sphere / dx
;  hmat[n:2*n-1,9] = -12*y + 24*y*x^2 + 24*y3	; d sphere / dy
  hmat[n:2*n-1,9] = -12*y + 24*y*x2 + 24*y3	; d sphere / dy
  if (nz eq 10) then return,transpose(hmat)

;  hmat[0:n-1,10] = 16*x^3 - 6*x
  hmat[0:n-1,10] = 16*x3 - 6*x
;  hmat[n:2*n-1,10] = 6*y - 16*y^3
  hmat[n:2*n-1,10] = 6*y - 16*y3
  if (nz eq 11) then return,transpose(hmat)

;  hmat[0:n-1,11] = -6*y + 24*x^2*y + 8*y^3
  hmat[0:n-1,11] = -6*y + 24*x2*y + 8*y3
;  hmat[n:2*n-1,11] = -6*x + 8*x^3 + 24*x*y^2
  hmat[n:2*n-1,11] = -6*x + 8*x3 + 24*x*y2
  if (nz eq 12) then return,transpose(hmat)

;  hmat[0:n-1,12] = 4*x^3 - 12*x*y^2
  hmat[0:n-1,12] = 4*x3 - 12*x*y2
;  hmat[n:2*n-1,12] = -12*x^2*y + 4*y^3
  hmat[n:2*n-1,12] = -12*x2*y + 4*y3
  if (nz eq 13) then return,transpose(hmat)

;  hmat[0:n-1,13] = 12*x^2*y - 4*y^3
  hmat[0:n-1,13] = 12*x2*y - 4*y3
;  hmat[n:2*n-1,13] =  4*x^3 - 12*x*y^2
  hmat[n:2*n-1,13] =  4*x3 - 12*x*y2
  if (nz eq 14) then return,transpose(hmat)

  x4 = x3*x
  y4 = y3*y
;  hmat[0:n-1,14] = 3 - 36*x^2 + 50*x^4 - 12*y^2 + 60*x^2*y^2 + 10*y^4
  hmat[0:n-1,14] = 3 - 36*x2 + 50*x4 - 12*y2 + 60*x2*y2 + 10*y4
;  hmat[n:2*n-1,14] = -24*x*y + 40*x^3*y + 40*x*y^3
  hmat[n:2*n-1,14] = -24*xy + 40*x3*y + 40*x*y3
  if (nz eq 15) then return,transpose(hmat)

;  hmat[0:n-1,15] = -24*x*y + 40*x^3*y + 40*x*y^3
  hmat[0:n-1,15] = -24*xy + 40*x3*y + 40*x*y3
;  hmat[n:2*n-1,15] = 3 - 12*x^2 + 10*x^4 - 36*y^2 + 60*x^2*y^2 + 50*y^4
  hmat[n:2*n-1,15] = 3 - 12*x2 + 10*x4 - 36*y2 + 60*x2*y2 + 50*y4
  if (nz eq 16) then return,transpose(hmat)

  r2 = x2+y2
;  hmat[0:n-1,16] = -12*x^2 + 10*x^4 + 12*y^2 - 30*x^2*y^2 + $
;                    15*x^2*(x^2 + y^2) - 15*y^2*(x^2 + y^2)
  hmat[0:n-1,16] = -12*x2 + 10*x4 + 12*y2 - 30*x2*y2 + $
                    15*x2*r2 - 15*y2*r2
;  hmat[n:2*n-1,16] = 24*x*y + 10*x^3*y - 30*x*y^3 - 30*x*y*(x^2 + y^2)
  hmat[n:2*n-1,16] = 24*xy + 10*x3*y - 30*x*y3 - 30*xy*r2
  if (nz eq 17) then return,transpose(hmat)

;  hmat[0:n-1,17] = -24*x*y + 30*x^3*y - 10*x*y^3 + 30*x*y*(x^2 + y^2)
  hmat[0:n-1,17] = -24*xy + 30*x3*y - 10*x*y3 + 30*xy*r2
;  hmat[n:2*n-1,17] =  -12*x^2 + 12*y^2 + 30*x^2*y^2 - 10*y^4 + $
;                      15*x^2*(x^2 + y^2) - 15*y^2*(x^2 + y^2)
  hmat[n:2*n-1,17] =  -12*x2 + 12*y2 + 30*x2*y2 - 10*y4 + $
                      15*x2*r2 - 15*y2*r2
  if (nz eq 18) then return,transpose(hmat)

;  hmat[0:n-1,18] = 5*x^4 - 30*x^2*y^2 + 5*y^4
  hmat[0:n-1,18] = 5*x4 - 30*x2*y2 + 5*y4
;  hmat[n:2*n-1,18] = -20*x^3*y + 20*x*y^3
  hmat[n:2*n-1,18] = -20*x3*y + 20*x*y3
  if (nz eq 19) then return,transpose(hmat)

;  hmat[0:n-1,19] = 20*x^3*y - 20*x*y^3
  hmat[0:n-1,19] = 20*x3*y - 20*x*y3
;  hmat[n:2*n-1,19] = 5*x^4 - 30*x^2*y^2 + 5*y^4
  hmat[n:2*n-1,19] = 5*x4 - 30*x2*y2 + 5*y4

  return,transpose(hmat)
end

;
;   scalecent is a routine specific to the laser Hartmann array
;   that applies the measured plate scale to scale the Hartmann centroids,
;   as measured in pixels deflection, to canonical slope units
;
function scalecent,cent,d,dtele
;  input: cent, units of pixels
;  return: c, units of waves / radius
  pixels = 10. ; pixels from Hartmann spot center to 1st Airy ring
  firstAiryPeak = 1.63472*(dtele/d)/2.;  = waves / radius
  c = cent*(firstAiryPeak/pixels)
  return,c
end

function zernanal,cent,x,y,nz,r
  n = (size(cent))(2)
  if (n_elements(nz) eq 0) then nz = 10
  s = ['tip','tilt','focus','astigx','astigy', $
       'comax','comay','tricomax','tricomay','sphere', $
       '4 2 x','4 2 y','4 4 x','4 4 y','5 1 x', $
       '5 1 y','5 3 x','5 3 y','5 5 x','5 5 y']
  hmat = responsematrix(x,y,nz)
;  cent must be in canonical units of wavefront slope: waves/radius
;  solve for zernike coefficients.  units = waves
  c = fltarr(62)
  for i=0,n-1 do c[i] = cent[0,i]
  for i=0,n-1 do c[i+31] = cent[1,i]

  svdc,hmat,w,u,v
  z=svsol(u,w,v,c)
  z1 = z*0
  r = fltarr(nz)
  print,' residuals are in canonical slope units: waves/radius'
  for i=0,nz-1 do begin
    z1[i] = z[i]
    e = c - hmat##z1
    r[i] = sqrt(total(e*e)/62.)
    print,'residual after mode ',i,': ',s[i],' removed: ',r[i]
  endfor
  print,'----------------------------------------'
  print,'wavefront analysis - fit to 1st 10 modes:'
  print,'aberration   waves P-V'
  for i=0,nz-1 do begin
    print,i,': ',s[i],' ',z[i]
  endfor
  print,'----------------------------------------'
  return,z
end

;  Disphart displays a n=31 element vector on the Hartmann geometry defined by xloc,yloc
;   vals=value vector, raw=original image (used to determine size of image)

function disphart,vals,raw,xloc,yloc,roisize,dtele
  n = (size(raw))(1)
  m = (size(raw))(2)
  nv = (size(xloc))(1)
  roi = fltarr(4,nv)
  roi[0,*] = xloc - roisize/2.
  roi[1,*] = xloc + roisize/2.
  roi[2,*] = yloc - roisize/2.
  roi[3,*] = yloc + roisize/2.
 
  disp0 = float(raw)*0
  for i=0,nv-1 do $
    disp0[roi[0,i]:roi[1,i],roi[2,i]:roi[3,i]] = circle(roisize+1,roisize+1,roisize/2.,roisize/2.,roisize/2.,vals[i])

  x0 = xloc((nv-1)/2)
  y0 = yloc((nv-1)/2)
  u = circle(n,m,x0,y0,dtele/2,1)
  return,disp0+u-1.
end

;  Dispmode creates an image of a zernike mode on the D-circle

function dispmode,modevec,raw,x0,y0,dtele,nz
  if (n_elements(nz) eq 0) then nz = 10
  n = (size(raw))(1)
  m = (size(raw))(2)
  x = ((findgen(n)-x0)*2/dtele) # (fltarr(m)+1)
  y = ((findgen(m)-y0)*2/dtele) ## (fltarr(n)+1)
  ap = circle(n,m,x0,y0,dtele/2.,1) 
  
  i = 0
  disp0 = float(raw)*0
  disp0 = disp0 + modevec[0] * x/2.
  print,i & i = i+1
  if (nz eq 1) then return,disp0*ap
  disp0 = disp0 + modevec[1] * y/2.
  print,i & i = i+1
  if (nz eq 2) then return,disp0*ap
  x2 = x^2
  y2 = y^2
  disp0 = disp0 + modevec[2] * (x2 + y2 - 1/2.)
  print,i & i = i+1
  if (nz eq 3) then return,disp0*ap
  disp0 = disp0 + modevec[3] * (x2 - y2)/2.
  print,i & i = i+1
  if (nz eq 4) then return,disp0*ap
  xy = x*y
  disp0 = disp0 + modevec[4] * xy
  print,i & i = i+1
  if (nz eq 5) then return,disp0*ap
  disp0 = disp0 + modevec[5] * (3*x2 + 3*y2 - 2)*x/2.
  print,i & i = i+1
  if (nz eq 6) then return,disp0*ap
  disp0 = disp0 + modevec[6] * (3*x2 + 3*y2 - 2)*y/2.
  print,i & i = i+1
  if (nz eq 7) then return,disp0*ap
  x3 = x2*x
  y3 = y2*y
  disp0 = disp0 + modevec[7] * (x3 - 3*x*y2)/2.
  print,i & i = i+1
  if (nz eq 8) then return,disp0*ap
  disp0 = disp0 + modevec[8] * (3*x2*y-y3)/2.
  print,i & i = i+1
  if (nz eq 9) then return,disp0*ap
  x4 = x3*x
  y4 = y3*x
  disp0 = disp0 + modevec[9] * (1 - 6*x2 + 6*x4 - 6*y2 + 12*x2*y2 + 6*y4)
  print,i & i = i+1
  if (nz eq 10) then return,disp0*ap
  disp0 = disp0 + modevec[10] * (4*(x4-y4) - 3*(x2-y2) )
  print,i & i = i+1
  if (nz eq 11) then return,disp0*ap
  disp0 = disp0 + modevec[11] * (8*x3*y + 8*x*y3 - 6*xy)
  print,i & i = i+1
  if (nz eq 12) then return,disp0*ap
  disp0 = disp0 + modevec[12] * (x4-6*x2*y2 + y4)
  print,i & i = i+1
  if (nz eq 13) then return,disp0*ap
  disp0 = disp0 + modevec[13] * (4*x3*y - 4*x*y3)
  print,i & i = i+1
  if (nz eq 14) then return,disp0*ap
  x5 = x4*x
  y5 = y4*y
  disp0 = disp0 + modevec[14] * (3*x - 12*x3 + 10*x5 - 12*x*y2 + 20*x3*y2 + 10*x*y4)
  print,i & i = i+1
  if (nz eq 15) then return,disp0*ap
  disp0 = disp0 + modevec[15] * (3*y - 12*x2*y + 10*x4*y - 12*y3 + 20*x2*y3 + 10*y5)
  print,i & i = i+1
  if (nz eq 16) then return,disp0*ap
  disp0 = disp0 + modevec[16] * (-4*x3 + 12*x*y2 + 5*x^3*(x2 + y2) - 15*x*y2*(x2 + y2))
  print,i & i = i+1
  if (nz eq 17) then return,disp0*ap
  disp0 = disp0 + modevec[17] * (-12*x2*y + 4*y3 + 15*x2*y*(x2 + y2) - 5*y3*(x2 + y2))
  print,i & i = i+1
  if (nz eq 18) then return,disp0*ap
  disp0 = disp0 + modevec[18] * (x5 - 10*x3*y2 + 5*x*y4)
  print,i & i = i+1
  if (nz eq 19) then return,disp0*ap
  disp0 = disp0 + modevec[19] * (5*x4*y - 10*x2*y3 + y5)
  print,i & i = i+1

  return,disp0*ap
end

;   --------- end of function definitions ---------
;   --------- begin example script -----------
;      analysis of laser retro wavefront data
;
dir = '/duck5/lickdata/oct99/laser/Oct23/'
ref = 255-tiff_read(dir + 'hart, ref preamp')
h1 = 255 -tiff_read(dir + 'L3retro, preamp, tape, 1')
h2 = 255 -tiff_read(dir + 'L3retro, preamp, tape, 2')
h3 = 255 -tiff_read(dir + 'L3retro, preamp, tape, 3')
n = (size(ref))(1)
m = (size(ref))(2)

; center of ref: 292,227
x0 = 292 & y0 = 227
refr = (ref>70)-70

; center of h1: 299,176, threshold 70
x0r = 299 & y0r = 176
h1r = shift((h1>70)-70,x0-x0r,y0-y0r)
h2r = shift((h2>70)-70,x0-x0r,y0-y0r)
h3r = shift((h3>70)-70,x0-x0r,y0-y0r)
hr = h1r

d = 322.-x0
roisize = 30.

dtele = d*3.5*sqrt(3.) ;  for 31 lenslet array on laser diagnostic leg
ap = circle(n,m,x0,y0,dtele/2.,1.) ; aperture

nomhart,x0,y0,d,xloc,yloc
x = (xloc-x0)/(dtele/2.)
y = (yloc-y0)/(dtele/2.)

cent = hartcents(h1r,refr,xloc,yloc,roisize)
cents = scalecent(cent,d,dtele)
nz = 10
z1 = zernanal(cents,x,y,nz,resid)
z1r = z1
z1r[0:2] = 0
u1 = dispmode(z1r,refr,x0,y0,dtele,nz)

cent = hartcents(h2r,refr,xloc,yloc,roisize)
cents = scalecent(cent,d,dtele)
z2 = zernanal(cents,x,y,nz)
z2r = z2
z2r[0:2] = 0
u2 = dispmode(z2r,refr,x0,y0,dtele,nz)

cent = hartcents(h3r,refr,xloc,yloc,roisize)
cents = scalecent(cent,d,dtele)
z3 = zernanal(cents,x,y,nz)
z3r = z3
z3r[0:2] = 0
u3 = dispmode(z3r,refr,x0,y0,dtele,nz)

dispgui,u1,'L3retro, preamp, tape, 1'
dispgui,u2,'L3retro, preamp, tape, 2'
dispgui,u3,'L3retro, preamp, tape, 3'
end
