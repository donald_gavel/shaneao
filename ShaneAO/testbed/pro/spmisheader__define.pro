; spmisheader__define.pro
;+
;-------------------------------------------------------------------------------
; spmisheader__define            IDL V5.02
;
; PURPOSE:
;    Photometrics "16-bit" image header structure.
;
;    Adopted for general purpose writing/reading of
;    image files and generated image data within IPSys.
;    See files readpmi.pro, writepmi.pro, ipwin.pro
;
;    Header is typically of fixed length = 172 bytes.
;    IDL uses 2 byte integers and 4 byte longs.
;
; CALLING SEQUENCE:
;                   hdr= {sPmisHeader}
;
; MODIFICATION HISTORY:
;  21-Jul-97 MFeldman Created.
;  30-Oct-97 MFeldman Add comments for struc members.
;
;-------------------------------------------------------------------------------
;-
pro sPmisHeader__define

sPmisHeader={sPmisHeader,$; Raw header
   szID:bytarr(4),       $; file type identifier ('PMIS' for original DOS file)
   nHeadSize:0,          $; length of this header in bytes (= 172)
   nVersion:0,           $; Version # of PMIS software that created the image
   nXorg:0,              $; x origin
   nYorg:0,              $; y origin
   nXsiz:0,              $; number of cols in image
   nYsiz:0,              $; number of rows in image
   nXbin:0,              $; Photometrics binning factor for columns
   nYbin:0,              $; Photometrics binning factor for rows
   szName:bytarr(40),    $; Image title or filename, up to 40 ASCII chars
   szComment:bytarr(100),$; Comment, up to 100 ASCII chars 
   tCreated:0L,          $; Creation time
   tModified:0L,         $; Modification time
   nGain:0,              $;
   nImages:0             $; 
   }

end
