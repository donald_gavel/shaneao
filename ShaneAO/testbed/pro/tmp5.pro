; tmp5 - part of the PIB analysis
;   convolve laser beam profile and a star with the atmosphere
;

pro laserprofile,psflaseratm,psfstaratm,nolens=nolens,short=short,set=set

if n_elements(nolens) eq 0 then nolens = 0
if n_elements(short) eq 0 then short = 0
if n_elements(set) eq 0 then set = '10-17'

print,'  laser beam profile ...'
if set eq '10-17' then begin
  file = '~/llnl/gavel/data/10-17/amp  pointing 7.5w nd1.5'
  highdata = tiff_read(file)
  file = '~/llnl/gavel/data/10-17/amp pointing 7.5w nd1.0'
  lowdata = tiff_read(file)
  center = [463,290]
  shiftr = [5,-3]
  thresh = 254
  scale = 2.4
endif
if set eq '10-18' then begin
  file = '~/llnl/gavel/data/10-18/pointing 17 w nd1.5'
  highdata = 255-tiff_read(file)
  file = '~/llnl/gavel/data/10-18/pointing 17w nd0.6'
  lowdata = 255-tiff_read(file)
  center = [415,344]
  shiftr = [10,0]
  thresh = 254
  scale = 7.9
endif
lowdata = shift(lowdata,shiftr(0),shiftr(1))
resdata = lowdata*(lowdata lt thresh) + scale*highdata*(lowdata ge thresh)
print,'psflaser'
psflaser = resdata(center(0)-128:center(0)+127,center(1)-128:center(1)+127)
print,'mtflaser'
mtflaser = shift(fft(psflaser),128,128)
beamsize = .135
;
print,'  atmosphere mtf ...'
n = 256
arcsec = !pi/(180.*3600.)
lambda = .589e-6
r0 = 0.1
pixels = 10. ; between peaks of diffraction
platescale = ((lambda/beamsize)/pixels)/arcsec
dx = platescale*arcsec
d = beamsize*sqrt(2.0)
print,'psfup, mtfup'
if short then print,'(short exposure)' else print,'(long exposure)'
if short then psfup = atmopsf(n,dx,lambda,r0,d,mtf=mtfup,/short) $
else psfup = atmopsf(n,dx,lambda,r0,d,mtf=mtfup)
d = 0.45 ; lenslet diameter
print,'psfdown, mtfdown'
if short then psfdown = atmopsf(n,dx,lambda,r0,d,mtf=mtfdown,/short) $
else psfdown = atmopsf(n,dx,lambda,r0,d,mtf=mtfdown)
;
print,'   wavefront sensor lens ...'
lensfwhm = .76*arcsec
print,'psflens'
if nolens then print,'-no lens'
psflens = gauss2(n,n/2,n/2,lensfwhm/dx)
print,'mtflens'
if nolens then mtflens = 1 $
else mtflens = shift(fft(psflens),n/2,n/2)
;
print,'mtf & psf of star with atmosphere'
mtfstaratm = mtflens*mtfdown
psfstaratm = fft(mtfstaratm)
s = [128,128] - coords(psfstaratm,(where(psfstaratm eq max(psfstaratm)))(0))
psfstaratm = shift(abs(psfstaratm),s(0),s(1))
print,'mtf & psf of laser with atmosphere'
mtflaseratm = mtflens*mtfdown*mtfup*mtflaser
psflaseratm = shift(fft(mtflaseratm),n/2,n/2)
s = [128,128] - coords(psflaseratm,(where(psflaseratm eq max(psflaseratm)))(0))
psflaseratm = shift(abs(psflaseratm),s(0),s(1))
print,'done'

end

pro doem,dataset,date=set,help = helpq
  if keyword_set(helpq) then begin
    print,'usage: doem,dataset,date=mo-day,/help'
    return
  end
  if n_elements(set) eq 0 then set='10-17'
  beamsize = .135
  arcsec = !pi/(180.*3600.)
  lambda = .589e-6
  pixels = 10. ; between peaks of diffraction
  platescale = ((lambda/beamsize)/pixels)/arcsec

  laserprofile,psflaserwfs,psfstarwfs,nolens=0,short=1,set=set

  psflaserwfs = psflaserwfs/total(psflaserwfs)
  a = (coords(psflaserwfs,(where(psflaserwfs eq max(psflaserwfs)))(0)))
  t = thin(sobel(psflaserwfs gt .5*max(psflaserwfs)))
  b = (coords(psflaserwfs,(where(t gt 1))(0)))
  fwhm = 2.0*sqrt((a-b)##transpose(a-b))
;  dispgui,psflaserwfs,'laser on wfs'
  print,'------------------------'
  print,'fwhm- lgs on wfs = ',fwhm*platescale,' arcsec'
  print,'------------------------'

  psfstarwfs = psfstarwfs/total(psfstarwfs)
  a = (coords(psfstarwfs,(where(psfstarwfs eq max(psfstarwfs)))(0)))
  t = thin(sobel(psfstarwfs gt .5*max(psfstarwfs)))
  b = (coords(psfstarwfs,(where(t gt 1))(0)))
  fwhm = 2.0*sqrt((a-b)##transpose(a-b))
;  dispgui,psfstarwfs,'star on wfs'
  print,'------------------------'
  print,'fwhm- ngs on wfs = ',fwhm*platescale,' arcsec'
  print,'------------------------'

  laserprofile,psflaseratm,psfstaratm,nolens=1,short=0,set=set

  psflaseratm = psflaseratm/total(psflaseratm)
  a = (coords(psflaseratm,(where(psflaseratm eq max(psflaseratm)))(0)))
  t = thin(sobel(psflaseratm gt .5*max(psflaseratm)))
  b = (coords(psflaseratm,(where(t gt 1))(0)))
  fwhm = 2.0*sqrt((a-b)##transpose(a-b))
;  dispgui,psflaseratm,'laser on CCD'
  print,'------------------------'
  print,'fwhm- lgs on CCD = ',fwhm*platescale,' arcsec'
  print,'------------------------'

  psfstaratm = psfstaratm/total(psfstaratm)
  a = (coords(psfstaratm,(where(psfstaratm eq max(psfstaratm)))(0)))
  t = thin(sobel(psfstaratm gt .5*max(psfstaratm)))
  b = (coords(psfstaratm,(where(t gt 1))(0)))
  fwhm = 2.0*sqrt((a-b)##transpose(a-b))
;  dispgui,psfstaratm,'star on CCD'
  print,'------------------------'
  print,'fwhm- ngs on CCD = ',fwhm*platescale,' arcsec'
  print,'------------------------'

  dataset = {lgswfs:psflaserwfs, ngswfs:psfstarwfs, $
         lgs:psflaseratm, ngs: psfstaratm}
  return
end
         


