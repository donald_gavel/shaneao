;+
;  getmirror.pro - get the 61 mirror voltages from the
;                  current offset mirror file
;
;  usage:
;    a = getmirror(file)
;
;  input:
;    file (optional) - name of the offset file. The file is
;                      assumed to be in
;                  /net/lgs6/home/winkerbean/AO/Data/MirrorData
;-
function getmirror,file
  if (n_elements(file) eq 0) then begin
    file = 'offsetMirrorCalc.dat'
  endif
  dir = '/net/lgs6/home/winkerbean/AO/Data/MirrorData/'
  openr, unit,dir+file,/get_lun
  act = fltarr(127)
  for i=0,126 do begin
    readf,unit,dum & act[i] = dum
  endfor
  free_lun, unit
  file = 'activeAct.dat'
  openr, unit,dir+file,/get_lun
  aact = fltarr(2,61)
  for i=0,60 do begin
    readf,unit,dum0,dum1
    aact[0,i] = dum0
    aact[1,i] = dum1
  endfor
  free_lun,unit
  m = fltarr(61)
  for i=0,60 do begin
    m[aact[1,i]] = act[aact[0,i]]
  endfor
  m = m/(409.5*16.);  convert to volts
  return,m
end

