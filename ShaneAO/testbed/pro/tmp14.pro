;  psf.pro - compute the MTF and PSF of partially corrected atmosphere
;
;   zernike modes
;
n = 32
D = float(n/2) ; diameter of aperture
ap = circle(n,n,n/2,n/2,D/2,1.)
z = zernike(n,n,D/2,1,1)*ap
piston = ap/sqrt(total(ap*ap))
w = ap/total(ap)
tip = real(z)
tip = tip/sqrt(total(tip*tip*ap))
tilt = imaginary(z)
tilt = tilt/sqrt(total(tilt*tilt*ap))
;
x = ones(n) ## (findgen(n)-n/2)
y = transpose(x)
r = sqrt(x^2+y^2)/D ; radial coordinate in units of D
Dphi = r^(5./3.)
g = 0.5*real(ftconvolve(Dphi,w))*ap
g0 = 0.5*real(ftconvolve(Dphi,piston))*ap
g1a = 0.5*real(ftconvolve(Dphi,tip))*ap
g1b = 0.5*real(ftconvolve(Dphi,tilt))*ap
a = total(g*w)
a0 = total(g0*piston)
a1a = total(g1a*tip)
a1b = total(g1b*tilt)
tau_atm = exp(-0.5*Dphi)
gz1a = fltarr(n,n,n,n)
zz1a = fltarr(n,n,n,n)
for ix = -n/2,n/2-1 do begin
for iy = -n/2,n/2-1 do begin
  gz1a[*,*,ix+n/2,iy+n/2] = (g1a - shift(g1a,ix,iy))*(tip - shift(tip,ix,iy))*ap*shift(ap,ix,iy)
  zz1a[*,*,ix+n/2,iy+n/2] = -0.5*a1a*(tip - shift(tip,ix,iy))*(tip - shift(tip,ix,iy))*ap*shift(ap,ix,iy)
endfor
endfor
end
