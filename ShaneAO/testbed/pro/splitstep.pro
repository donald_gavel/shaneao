;+
;  splitstep - propagate a coherent source through simulated
;       atmospheric turbulance
;
;  usage:
;    splitstep,screens,wf0,du,h,lambda,wf,[verbose=verbose]
;    wf = splitstep_f(screens,wf0,du,h,lambda,[verbose=verbose])
;
;  inputs:
;    screens - screen 'stack', one per turbulent layer
;    wf0 - initial wavefront (at 'top of atmosphere')
;    du - spatial sampling, meters
;    h - vector of screen heights, meters above the telescope,
;        start with h[0] = 0 then list in ascending order
;    lambda - wavelength, meters
;    verbose - print progress reports during execution
;
;  output:
;    wf - wavefront at the telescope
;-


;  propagate a coherent source from the top of
;  the atmosphere to the telescope

function splitstep_f,screens,wf0,du,h,lambda,verbose = verbose
  if keyword_set(verbose) then print,'print,'<splitstep>'
  nlayers = (size(h))(1)
  wf = wf0
  i = complex(0,1.)
  for j=nlayers-1,0,-1 do begin ; cycle through layers starting from the top
;     1st half-step: add phase screen from turbulant layer
    mag = abs(wf)
    ph = angle(wf)
    ph = ph + screens[*,*,j]
    wf = mag*exp(i*ph)
;     2nd half-step: "vacuum" propagate to next layer
    if (j gt 0) then begin
      L = h[j] - h[j-1]
      wf = fresnel(wf,du,L,lambda)
    endif
    if keyword_set(verbose) then print,format='($,I0," ")',j
  endfor
  if keyword_set(verbose) then print,'<splitstep> done'
  return,wf
end

;  move the phase screens a delta amount corresponding to
;  wind speed.  It is assumed that the phase screens are
;  "long and thin" strips that "barrel-roll" with the wind.
;  The return is a stack of square screens which can be
;  used in the splitstep function

function blowwind,screens,windspeed,winddirection,dt,du,verbose = verbose
  if keyword_set(verbose) then print,'<blowwind>'
  nlayers = (size(windspeed))(1)
  n = (size(screens))(1)
  screenstack = fltarr(n,n,nlayers)
  for i=0,nlayers-1 do begin
    screens[*,*,i] = (shift(screens[*,*,i],windspeed[i]*dt/du,0))
    s = rot(screens[*,0:n-1,i],winddirection[i])
    screenstack[*,*,i] = s
    if keyword_set(verbose) then print,format='($,I0," ")',i
  endfor
  if keyword_set(verbose) then print,'<blowwind> done'
  return,screenstack
end

pro splitstep,screens,wf0,du,h,lambda,wf,verbose=verbose
  if n_params() eq 0 then begin
    doc_library,'splitstep'
    return
  endif
  wf = splitstep_f(screens,wf0,du,h,lambda,wf,verbose=verbose)
end

;  example code
seed = 1
microns = 1.e-6
cm = 1.e-2
km = 1.e3
ms = 1.e-3
arcsec = !pi/(180.*3600.)

lambda0 = .5*microns
lambda = 2.2*microns

dtele = 3.0
n = 64
m = n*4
du = 2*dtele/float(n)

h = [0.,2.,5,10]*km
r0 = [10.,100., 100., 15.]*cm*(lambda/lambda0)^(6./5.)

include,'screengen'
screens = makescreens(n,m,r0,du,seed,/verbose)

windspeed = 60*[5.,1.,5.,5.]  ; wind speed, m/sec
winddirection = [0.,45.,90.,90.]  ; wind direction, degrees

dt = 1*ms
nt = 30
tf = dt*nt
wfg = complexarr(n,n,nt);  wavefronts at the telescope, vs time
ap = circle(n,n,n/2,n/2,dtele/(2*du),1)
wf = complex(ap,0*ap)
for i = 0,nt-1 do begin $
  print,'time = ',i*dt & $
  screenstack = blowwind(screens,windspeed,winddirection,dt,du) & $
  wfg[*,*,i] = splitstep_f(screenstack,wf,du,h,lambda)*ap & $
endfor
print,'time = ',i*dt

im = fltarr(n,n,nt)
for i = 0,nt-1 do begin $
  im[*,*,i] = abs(ft(wfg[*,*,i]))^2 & $
endfor
pixelscale = lambda/(n*du)
print,'pixel scale = ',pixelscale/arcsec

phu = fltarr(n,n,nt)
for i = 0,nt-1 do begin $
  phu[*,*,i] = unwrapphase(angle(wfg[*,*,i]),ap) & $
endfor

end