;+
;  gauss2.pro - form a gaussian intensity profile
;
;  usage: g = gauss2(n,cx,cy,stddev)
;
;  where cx and cy are the center
;   and n is the (square) dimension
;-
function gauss2,n,cx,cy,stddev
  machinfo = machar(/double)
  minponent = alog(machinfo.xmin)
  if ((size(stddev))[0] eq 0) then sd = [1,1]*stddev else sd = stddev
  result = make_array(n,n,/double)
  x = extend(indgen(n),2,n)
  y = transpose(x)
  ponent = ( - (((x-cx)/sd[0])^2 + ((y-cy)/sd[1])^2) )
  result = exp(ponent > minponent)
  zil = where(ponent lt minponent)
  if (size(zil))(0) eq 1 then begin
    result[where(ponent lt minponent)] = 0
  endif
  return,result
end
