; Control.pro
;    various control theory support routines
;
function sampleAndHold,s,T,plot=plot
  r = (1.-exp(-s*T))/(s*T)
  if (keyword_set(plot)) then begin
    window,/free,title='sampleAndHold'
    bodePlot,abs(s)/(2*!pi),r,name='sampleAndHold'
  endif
  return,r
end

function integrator,s,T,intGain,plot=plot
  r = 1./(1.-intGain*exp(-s*T))
  if (keyword_set(plot)) then begin
    window,/free,title = 'integrator'
    bodePlot,abs(s)/(2*!pi),r,name='integrator'
  endif
  return,r
end

function delay,s,tau,plot=plot
  r = exp(-s*tau)
  if (keyword_set(plot)) then begin
    window,/free,title='delay'
    bodePlot,abs(s)/(2*!pi),r,name='delay'
  endif
  return,r
end

function leadlag,s,T,a_lead,a_lag,plot=plot
  r = ((1+a_lead*exp(-s*T))/(1-a_lag*exp(-s*T)))*((1-a_lag)/(1+a_lead))
  if (keyword_set(plot)) then begin
    window,/free,title='leadLag'
    bodePlot,abs(s)/(2*!pi),r,name='leadLag'
  endif
  return,r
end

function loopTransfer,s,T,tau,intGain,leadlag=leadlag,wind_predictor=wind_predictor,plot=plot
  if (n_elements(leadlag) eq 0) then begin
    leadlag=[0,0]
  endif
  if (n_elements(wind_predictor) eq 0) then wind_predictor = 0
  a_lead = leadlag[0]
  a_lag = leadlag[1]
  if (keyword_set(wind_predictor)) then begin
    r=sampleAndHold(s,T)*integrator(s,T,intGain)*delay(s,tau)*leadlag(s,T,a_lead,a_lag)
  endif else begin
    r=sampleAndHold(s,T)^2*integrator(s,T,intGain)*delay(s,tau)*leadlag(s,T,a_lead,a_lag)
  endelse
  if (keyword_set(plot)) then begin
    title = 'loopTransfer'
    if (wind_predictor eq 1) then title = 'loopTransfer with wind predictor'
    window,/free,title=title
    bodePlot,abs(s)/(2*!pi),r,name=title
  endif
  return,r
end

function closedLoopTF,s,T,tau,gain,intGain,leadlag=leadlag,wind_predictor=wind_predictor,plot=plot
  if (n_elements(leadlag) eq 0) then begin
    leadlag=[0,0]
  endif
  if (n_elements(wind_predictor) eq 0) then wind_predictor = 0
  r=1./(1.+gain*loopTransfer(s,T,tau,intGain,leadlag=leadlag,wind_predictor=wind_predictor))
  if (keyword_set(plot)) then begin
    title = 'closedLoopTF'
    if (wind_predictor eq 1) then title = 'closedLoopTF with wind predictor'
    window,/free,title=title
    bodePlot,abs(s)/(2*!pi),r,name=title
  endif
  return,r
end

function openLoopTF,s,T,tau,gain,plot=plot
  r = 1 - gain*sampleAndHold(s,T)^2*delay(s,tau)
  if (keyword_set(plot)) then begin
    window,/free,tilte='openLoopTF'
    bodePlot,abs(s)/(2*!pi),r,name='openLoopTF'
  endif
  return,r
end

pro bodePlot,f,h,h2,name=name
  if (n_elements(name) eq 0) then name = 'Bode Plot'
  psave = !p.multi
  !p.multi = [0,0,2,0,0]
  n = (size(f))[1]
  fp = f[n/2+1:n-1]
  hp = h[n/2+1:n-1]
  plot,fp,abs(hp),/xlog,/ylog,xtitle='frequency, Hz',ytitle='Log Magnitude',title=name,ticklen=.5
  if (n_elements(h2) ne 0) then begin
    hp2 = h2[n/2+1:n-1]
    oplot,fp,abs(hp2),color=150
  endif
  plot,fp,(180./!pi)*unwrap1(arg(hp)),/xlog,xtitle='frequency, Hz',ytitle='Phase, degrees',ticklen=.5
  if (n_elements(h2) ne 0) then begin
    oplot,fp,(180./!pi)*unwrap1(arg(hp2)),color=150
  endif
  oplot,fp,0*fp-180.,linestyle=2
  !p.multi = psave
end

pro nyquistPlot,f,h,color=color,oplot=oplot,_extra=e
  if (n_elements(color) eq 0) then color = 255
  n = (size(f))[1]
  fp = f[n/2+1:n-1]
  hp = h[n/2+1:n-1]
  re = real(hp)
  im = imaginary(hp)
  if (keyword_set(oplot)) then begin
      oplot,re,im,color=color
  endif else begin
      plot,re,im,xrange=[-2,2],yrange=[-2,2],charsize=1.5, $
        xstyle=4,ystyle=4
      oplot,re,im,color=color
      axis,0,0,/xaxis
      axis,0,0,/yaxis
  endelse
end

pro rejectionPlot,f,h,h2,name=name,oplot=oplot,colors=colors,_extra = e
  if (n_elements(name) eq 0) then name = 'Rejection Plot'
  if (n_elements(oplot) eq 0) then oplot = 0
  if (n_elements(colors) eq 0) then colors = [255,255]
  
  n = (size(f))[1]
  fp = f[n/2+1:n-1]
  hp = h[n/2+1:n-1]
  if (oplot eq 1) then begin
    oplot,fp,abs(hp),_extra=e
  endif else begin
    plot,fp,abs(hp),/xlog,/ylog,xtitle='frequency, Hz',ytitle='Log Magnitude',title=name,ticklen=.5,/nodata,_extra = e
    oplot,fp,abs(hp),color=colors[0]
  endelse
  if (n_elements(h2) ne 0) then begin
    hp2 = h2[n/2+1:n-1]
    oplot,fp,abs(hp2),color=colors[1],_extra = e
  endif
end

function phaseMargin,f,h   ; returns the phase margin, in degrees, given a loop transfer function
  n = (size(f))[1]
  phase = 0
  for k=n/2,n-1 do begin
    if (abs(h[k]) le 1.0) then begin
      phase = (arg(h[k])+!pi)*(180./!pi)
      break
    endif
  endfor
  return,phase
end

function KolmogorovSpectrum,f,r0,v,L0,plot=plot
  if (n_elements(L0) ne 0) then begin
    f0 = v/L0
  endif else begin
    f0 = 0
  endelse
  n = n_elements(f)
  S0 = 0.026*(v/r0)^(5./3.)/((abs(f))^(8./3.)+abs(f0)^(8./3.))
  S0[n/2]=0.
  if (keyword_set(plot)) then begin
    df = f[n/2+1]
    lambda = 500; nm
    window,/free,title='Kolmogorov Spectrum'
    rejectionPlot,f,sqrt(S0*df)*(lambda/(2*!pi)),name='Kolmogorov Spectrum, nm/sqrt(Hz)'
  endif
  return,S0
end

!p.charsize = 1.5
j = complex(0.,1.d0)
n = 4*2048;512     ; number of sample points
fs = 750    ; sample frequency
T = 1./fs   ; sample interval, seconds
fn = fs/2.  ; Nyquist frequency
df = fs/double(n) ; frequency sampling
f = (dindgen(n)-n/2)*df
f[n/2]=df/10.   ; small, but non-zero initial frequency
s = 2*!pi*j*f   ; Laplace variable (an array)
tau = T*.01; /2.   ; compute delay

CLgain = 0.2; 0.35 ; gives 60 degrees phase margin with a delay of T
intGain = 0.999
OLgain = 0.9
;gain = 0.52 ; gives 60 degrees phase margin with a delay of T/2
;gain = 1.07 ; gives 60 degrees phase margin with a delay of 0
a_lead_set = [0];[0,-.5]
a_lag_set = [0];[0,.3]

n_trials = (size(a_lead_set))[1]
HCL0 = closedLoopTF(s,T,tau,CLgain,intGain)
for k=0,n_trials-1 do begin

a_lead = -0.5
a_lead = a_lead_set[k]
a_lag = 0.3
a_lag = a_lag_set[k]

;gain = gain_set[k]
HLT = loopTransfer(s,T,tau,intGain,leadlag=[a_lead,a_lag])
HCL = closedLoopTF(s,T,tau,CLgain,intGain,leadlag=[a_lead,a_lag])
HnCL = CLgain*HLT*HCL
HOL = openLoopTF(s,T,tau,OLgain)
HCLw = closedLoopTF(s,T,tau,CLgain,intGain,leadlag=[a_lead,a_lag],/wind_predictor)
print,'phase margin: ',phasemargin(f,CLgain*hlt)

; wind predictor
w = 1.;exp(j*s*T/2)

;window,0
;bodePlot,f,gain*HLT,name='Loop Transfer Bode Plot'
;window,1
oplot = 1
if (k eq 0) then begin
  window,/free
  oplot=0
endif
rejectionPlot,f,HCL,HOL*w,name='Close and Open Loop Rejection',oplot=oplot,linestyle=2,xrange=[.1,1000]
;xyouts,10,3,string(gain)+string(a_lead)+string(a_lag)
rejectionPlot,f,HCLw,/oplot,linestyle=1

wait,.5

endfor

; Compare performance of Closed Loop control to Open Loop control
; Assumptions:
;    * Von-Karman like atmospheric temporal power spectrum
;        wind clearing time of 1 second (f0=1.0)
;    * Closed loop control has an integrator gain of 0.998 (Lick typical) to prevent integrator lock-up
;    * Closed loop control gain of 0.2 (produces a crossover bandwidth of 30 Hz - Lick typical)
;    * Open loop control has a gain of 0.9
;        Meant to simulate a 90% accurate open loop model (demonstrated on sky with Villages)
;    * Compute delay is a whole sample
; Conclusions:
;    * Open loop crossover is >2x higher than closed loop (70 Hz vs 30 Hz)
;    * Open loop controller rejects >2x more of the disturbance power above 1 Hz than the closed loop controller
;    * On a 10m telescope in typical seeing this could mean 0.35 Strehl vs 0.2 Strehl
;    * Wind prediction can reduce mse another 20%, boosting Strehl from 0.35 to 0.4
;

;
;window,/free
;f0 = 1.0
;S0 = 1./((abs(f))^(8./3.)+abs(f0)^(8./3.))
;S0[n/2] = S0[n/2+1]
r0 = 0.2 ; seeing parameter, m
lambda = 500. ; defining wavelength for r0, nm
L0 = 1.0 ; outer scale, m
v = 1.0 ; wind velocity, m/sec (1 m/sec = 2.2 mi/hr)
S0 = KolmogorovSpectrum(f,r0,v,L0)*df*(lambda/(2*!pi))^2 ; nm^2/Hz
window,/free,title = 'Disturbance Rejection'
rejectionPlot,f,sqrt(S0),name = 'Phase disturbance rejection, nm/Sqrt(Hz)'
rejectionPlot,f,abs(HCL)*sqrt(S0),/oplot
rejectionPlot,f,abs(HOL*w)*sqrt(S0),/oplot,linestyle=2,xrange=[.1,1000]

rms0 = sqrt(total(S0*(f gt f0))*df)*lambda/(2*!pi) ; nm rms
rmsCL = sqrt(total(abs(HCL)^2*S0*(f gt f0))*df)*lambda/(2*!pi) ; nm rms
rmsOL = sqrt(total(abs(HOL*w)^2*S0*(f gt f0))*df)*lambda/(2*!pi) ; nm rms

print,rms0,rmsCL,rmsOL,' nm rms'

end
