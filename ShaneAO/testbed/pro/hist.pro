;+
;  hist.pro
;    A "smart" histogram analysis and plotter
;
;  input:
;    a = Data array
;  outputs:
;    c = counts in bins
;    x = bins
;  keyword arguments:
;    plot = automatically plot the data
;    extra_plot = structure of extra plot keywords
;    _extra = extra keywords to the histogram function. common keywords are
;       nbins
;       min
;       max
;-
pro hist,a,c,x,plot=plot,extra_plot=extra_plot,_extra=ex
  ap = finite(a)
  ind = where(ap,count)
  if (count eq 0) then begin
  	print,'<hist> no valid data'
  	return
  endif
  a0 = a[ind]
  n = n_elements(ind)
  c = histogram(a0,locations=x,_extra = ex)
  if (keyword_set(plot)) then begin
  	plot,x,c,psym=10,_extra=extra_plot
  endif
end
