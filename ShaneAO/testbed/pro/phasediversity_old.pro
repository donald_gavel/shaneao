;+
;  phasediversity.pro
;    im = (in-focus data, out-of-focus data)
;
;   needs:
;     .compile zernike
;     .compile partialzern
;-
function phasedsimu,ap,du,rp,f,dz,lambda,zer,ph
  nn = (size(ap))(1)
  nz = (size(zer))(1)
  ztab = ztable(nz+3)
  print,'estimating images'
  ph = ap*0
  cph = 0
  for k = 0,nz-1 do begin
    n = ztab(0,k+3)
    m = ztab(1,k+3)
    s = ztab(2,k+3)
    if (s eq 0) then cph = zernike(nn,nn,rp/du,n,m)
    if (s eq 0) then phs = ap*real(cph)
    if (s eq 1) then phs = ap*imaginary(cph)
    ph = ph + zer(k)*phs/rms(phs,ap)
    print,format='($,I0," ")',k
  endfor
  imhat = do2im(ap,ph,du,f,dz,lambda)
  print,'done'
  return,imhat  
end

pro phasediter,im,ap,du,rp,f,dz,lambda,pzset,alpha,zerhat,imhat
; zer = [focus, astig, astig45, coma, coma45, tri, tri45, spherical]
  nn = (size(ap))(1)
  nz = (size(zerhat))(1)
  imhat = phasedsimu(ap,du,rp,f,dz,lambda,zerhat)
  e = im - imhat
  print,'updating zernike weights'
  for k = 0,nz-1 do begin
    dj = total(e*pzset(*,*,k))
    zerhat(k) = zerhat(k) - alpha*(real(dj)+imaginary(dj))
    print,format='($,I0," ")',k
  endfor
  print,'done'
end

;  use .go to run this initialization script
;  .compile zernike first
n = 256
;du = 7./n
dp = 3.
ds = 0.8
fnum = 28.6
f = dp * fnum
lambda = 0.635e-6
dx = 9.e-6
du = f * lambda / (n * dx)
rp = dp / 2.
rs = ds / 2.
i = complex(0,1)
ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
ztab = ztable(10)
zer = [-0.0137836, 0.709896, 0.937447, 0.524606, 0.557746, -2.63941, -0.0271637]
;f = 40*3.
;dz = 0.010
;l = 1/((1/f)-(1/(f+dz)))
;lambda = 1e-6

end
