;+
;  celtseg.pro - analyze performance of AO system
;           in correcting random celt segment aberrations
;
;------------------------
;  generate celt segments
;
;  usage:
;     ph = celtseg(L,n,m,seed,ap=ap)
;
;  inputs:
;     L - size of edge of segment
;     n,m,p - zernike index (radial, azimuthal, parity)
;     seed - random seed
;
; .compile dm2.pro
;
;------------------------
;-
function celtseg,nx,L,n,m,p,seed,ap=ap
eps = 0
;L = 10
;L = 40
dseg = 2*L ; superscribing circle
dx = L*sqrt(3)
dy = L*(3./2.)
seg = hex(dseg+1,L-eps)
nsegx = 3
nsegy = 3 ; must be odd
nseg = nsegx*nsegy
seggeo = fltarr(nseg,2)
k = 0
yf = dy*(nsegy-1)/2.
y0 = -yf
iy = -(nsegy-1)/2
for y = y0,yf,dy do begin $
  xf = dx*(nsegx-1)/2. & $
  x0 = -xf & $
  if ((iy/2)*2 ne iy) then begin $
    xf = xf - dx/2. & $
    x0 = x0 + dx/2. & $
  endif & $
  for x = x0,xf,dx do begin $
    seggeo[k,0] = x & $
    seggeo[k,1] = y & $
    k = k+1 & $
  endfor & $
  iy = iy + 1 & $
endfor
seggeo = seggeo[0:k-1,*]
nseg = k
;seed = 1
;n = 2
;m = 0  ; focus
;
;  get addresses of all points in the segment image that are in the segment
sdex = where(seg eq 1)
phdex = coords(seg,sdex)
ndex = (size(phdex))[1]/2
phdex = reform(phdex,ndex,2) - ones(ndex) # [L,L] + ones(ndex) # [nx/2,nx/2]
;
;  generate random n,m zernike phase
theZern = zernike(dseg+1,dseg+1,dseg/2,n,m)
if (p eq 0) then begin
  theZern = real(theZern)
endif else begin
  theZern = imaginary(theZern)
endelse

ph = fltarr(nx,nx)
ap = fltarr(nx,nx)
;
;  paste randomly aberrated segments into the big phase screen
for i=0,nseg-1 do begin $
  xi = fix(seggeo(i,0)) & $
  yi = fix(seggeo(i,1)) & $
  randomnumber = randomn(seed) & $
  ph[phdex[*,0]+xi+(phdex[*,1]+yi)*nx] = (seg*randomnumber*theZern)[sdex] & $
  ap[phdex[*,0]+xi+(phdex[*,1]+yi)*nx] = seg[sdex] & $
endfor
return,ph
end

;-------------------------------
;  fit the dm to the aberrations
;  using the Green's function approach
;
function fitcelt,ph,dmgeo,basis,grf,ap,phfit=phfit2
;
act2 = dmfit(ph,dmgeo,grf,/greens)
phfit2 = pushact(act2,dmgeo,basis,ph)
phe2 = ph - phfit2
rat = rms(phe2,ap)/rms(ph,ap)
return,rat
;
end
