;  test the map.pro algorithm with dummy data
psf = gauss2(128,64,64,10)
psf = psf/total(psf)
a = psf*0
a(64,64)=1
a(80,80)=1
b = real(ftconvolve(a,psf))
obj = a
im = b


nph = 2000.
b = b*nph
sig = 1.
seed = 123
pnoise = randomn(seed,128,128)*sqrt(b)
rnoise = randomn(seed,128,128)*sig
im = b + pnoise + rnoise
im = (im>(5*sig))-5*sig
;
  f = real(im)
  g = real(im)
  h = real(psf)
  n = (size(f))(1)
  hn = h/total(h)
    f1 = real(ftconvolve(f,hn))
    f2 = g / f1 - 1.
;
;  psf iteration:
;   given im and obj, find psf
obj = real(obj)
im = real(im)
psf = real(psf)
;  model:  im = obj * psf
;psf = ft( ft(im)/ft(obj) , /inverse )  doesn't work
n = 128
F = ft(obj)
G = ft(im)
den = real(conj(F)*F)
den = den + max(abs(den))*.001
H = conj(F)*G/den
psfest = real(ft(H,/inverse))/float(n)^2
; loop here
 psfestm = psfest > 0
 H = ft(psfestm)*float(n)^2
 E = G - F*H
 er = real(ft(E,/inverse))
 cost = total(er^2)
 dH = conj(F)*E/den
 H = H+dH
 psfest = real(ft(H,/inverse))/float(n)^2
; disp,psfest,'psfest iter 2'
 print,'cost = ',cost
;
disp,psf,'psf'
disp,im,'im'
disp,obj,'obj'
disp,obj*0,'imhat'
disp,obj*0,'e'

imhat = real(ftconvolve(obj,psf))*total(im)/(total(psf)*total(obj))
wset,3 & tvscl,imhat & label,3,'imhat' & print,min(imhat),max(imhat)
e = im - imhat
wset,4 & tvscl,e & label,3,'e' & print,min(e),max(e)
j = total(e^2)
print,'err fcn: ',j
;dpsf = real(ftconvolve(obj,rotate(e,2))) & print,min(psf),max(psf)
psf = (psf - .00001*dpsf)
wset,0 & tvscl,psf & label,0,'psf' & print,min(psf),max(psf)

wdelete,0,1,2,3,4

