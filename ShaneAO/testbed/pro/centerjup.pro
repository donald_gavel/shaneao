;+
;  centerjup.pro - center a jupiter image
;
; USAGE:
;    cjup = centerjup(jup)
;
; INPUT
;       jup - a 900x900 jupiter single frame or image
;
; OUTPUT
;     cjup - jup centered on fitting ellipse center
;
;.compile,enclose_ellipse
;-

function centerjup,jup
  u = rebin(jup,300,300)
  sky = u(11:20,11:20)
  ub = total(sky)/100
  un = (u>ub) - ub
  un = real(un)/real(max(un))
  mask = un lt .14
  maske = erode(mask,bytarr(5,5)+1)
  backg = planefit(u,maske)
;
  maskb = bytarr(300,300) + 1        ;  a mask for bad pixels at the edge
  border = 5
  maskb(0:border,*) = 0
  maskb(300-border:299,*) = 0
  maskb(*,0:border) = 0
  maskb(*,300-border:299) = 0
  j = rebin(jup,300,300)
  jn = maskb * real(j-backg)
  jn = jn/max(jn)
  t1 = .095
  t2 = .105
  a = (38.2/(3*0.063))/2.  ; size of jupiter on speckle camera
  b = a/1.069
  x0 = 150
  y0 = 150
  step = 20
  degrees = !pi/180.
  th = 16 * degrees
  enclose_ellipse,jn,t1,t2,a,b,x0,y0,th,step,/first
  for j = 0,4 do begin
    enclose_ellipse,jn,t1,t2,a,b,x0,y0,th,step
  endfor
  backgf = rebin(backg,900,900)
  jr = shift(jup-backgf,(150-x0)*3,(150-y0)*3)
  return,jr
end

 
