;+
; fresnel.pro - fresnel propagation
;
;    Implement's Fresnel's approximation to Huygens' integral
;    for a complex wavefront propagating paraxially from a plane at
;    z=0 to a plane at z=L.
;    Refernce: Siegman, Ch 16, eqn (79)
;    translated from fresnel.vm
;
; USAGE:
;  wfL = fresnel(wf0,du,L,lambda)
;
; INPUTS:
;  wf0 - complex wavefront at a given optical plane
;  du - spacing on the wavefront grid, in meters
;  L - real length of propagation, in meters
;  lambda - real wavelength of light, in meters
;
; OUTPUT:
;  wfL - complex wavefront at distance L
;
; ALGORITHM:
;   ~wfL = ~wf0 * exp{ + i k_perp^2 * L / 2*k }
;   where ~ indicates Fourier transform
;-
function fresnel,wf0,du,l,lambda
  k = 2*!pi/lambda
  n = (size(wf0))(1)
  df = 1./(n*du)
  dk = 2.*!pi*df
  fwf0 = shift(fft(shift(wf0,n/2,n/2)),n/2,n/2)
  k0 = -(n/2)*dk
  kf = (n/2-1)*dk
  r = findgen(n)*dk + k0
  kx = transpose(r) ## make_array(n,1,/float,value=1)
  ky = transpose(kx)
  kperp2 = kx*kx + ky*ky
  propPhase = kperp2*L/(2*k)
  propMag = 0*kperp2 + 1
  i = complex(0,1)
  prop = propMag*Exp(i*propPhase)
  fwfl = fwf0*prop
  wfl = shift(fft(shift(fwfl,n/2,n/2),/inverse),n/2,n/2)
  return,wfl
end
;
function fresnel1,wf0,du,L,lambda
  i = complex(0,1.)
  k = 2*!pi/lambda
  n = (size(wf0))(1)
  dk = 2*!pi/(float(n)*du)
  kx = ones(n) ## (findgen(n)-n/2)*dk
  ky = transpose(kx)
  kperp2 = kx^2 + ky^2
  kernel = exp(i*kperp2*L/(2*k))
  fwf0 = ft(wf0)
  fwfL = fwf0*kernel
  wfL = ft(fwfL,/inverse)
  return,wfL
end

  