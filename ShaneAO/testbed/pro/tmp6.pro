; tmp6.pro
;
pro class1::print
  print,'data1',self.data1
  print,'data2',self.data2
end

pro class1__define
  struct = { class1, data1:0L, data2:fltarr(10) }
end
