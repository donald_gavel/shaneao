;
;  MCAOdraw.pro
;    depict the MCAO geometry
;
;  requires:
;    h = altitudes of layers
;    theta = angles of guidestars
;    z = altitude of sodium layer
;    dz = thickness of sodium layer
;    D_tele = diameter of telescope
;
;  does:
;    draws the pupil
;    draws the metapupils at the highest layer
;    draws the marginal rays from the guidestars that intersect aperture at y=0
;    draws cylinders depicting guidestars in the sodium layer
;    draws lines at each turbulent layer showing a slice through the turbulence
;
function cylinder,d,l
  n = 100;  this many vertices
  points = fltarr(n,3)
  n2 = n/2
  for k=0,n2-1 do begin
    points[k,*] = [cos(theta),sin(theta),0]
    points[k+n,*] = [cos(theta),sin(theta),1]

  cyl = obj_new('IDLgrPolyline',
end
