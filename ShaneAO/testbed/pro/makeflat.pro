;
;  make a flat frame
;
function makeflat,dataset,badpix
  flatdata = readfits(dataset.datadir + dataset.flatfile)
  flatdark = readfits(dataset.datadir + dataset.flatdarkfile)
  flat = flatdata - flatdark
  flat = flat*(1-badpix) + median(flat,5)*badpix
  tmp = flat(*,100:255)             ; normalize over non-divot area
  nf = total(tmp)/n_elements(tmp)
  flat = flat/nf
  flat(0:2,0:5) = 1                 ; eliminate "start up" pixels
  return,flat
end
