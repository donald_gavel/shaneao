;
; procVillagesData.pro
;    Process Villages Telemetry Data
;
;========== in dome with turbulator March 21, 2008 ======================
;dir='C:\Users\DonGavel\Desktop\AdaptiveOptics\LAO\Villages\VillagesTestData\080320'
;cd,dir
;old_data = 0
;ofileset = ['011'];['005','007','009','011']
;cfileset = ['010'];['004','006','008','010']
;cm = rdview('cm_000',/swap)
;================= on sky November 21, 2007 =============================
;dir='C:\Documents and Settings\Don Gavel\My Documents\Adaptive Optics\LAO\VilLaGEs (VisibleLightLGSAOExperiments)\Data\071120\'
;old_data = 1
;ofileset = ['019']
;cfileset = ['018']
;cm = rdview('cm_000',/swap)
;
;================ testing at telescope March 24, 2008 ===================
;dir = 'C:\Users\DonGavel\Desktop\AdaptiveOptics\LAO\Villages\VillagesTestData\080320'
dir = 'C:\Users\DonGavel\Desktop\AdaptiveOptics\LAO\Villages\Data\Data_080716\'
cmdir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/
dir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/081022/'
cd,dir
old_data = 0
ofileset = ['006']
cfileset = ['006']
cm = rdview(cmdir+'cm_000',/swap)
;========================================================================
loadct,5

zer = reform(fltarr(120),120,1)
cm144 = [[zer],[cm[*,0:9]],[zer],[cm[*,10:129]],[zer],[cm[*,130:139]],[zer]]
;wrview,'cm144_000',cm144
view_cm = reform(transpose(cm144),12,12,120)

read_sm = 0
if (read_sm) then begin
	sm = rdview('sm_000')
	sm = transpose(sm)
	sm144 = [[zer],[sm[*,0:9]],[zer],[sm[*,10:129]],[zer],[sm[*,130:139]],[zer]]
	view_sm = reform(transpose(sm144),12,12,120)
endif

ncases = n_elements(ofileset)

theCase = -1

while (1) do begin
	theCase += 1
	if (theCase ge ncases) then break; theCase = 0

	centsOL = rdview('cent_'+ofileset[theCase])
	centsCL = rdview('cent_'+cfileset[theCase])

	dmCL = rdview('mirror_'+cfileset[theCase])

	ns = 60
	nt = 1024;4096
	t_start = 0

	if (not old_data) then begin

	;   ----- open loop WFS measurements during closed loop operation --------
		centsOLx = transpose(centsCL[ns:2*ns-1,t_start:t_start+nt-1])
		centsOLy = transpose(centsCL[3*ns:4*ns-1,t_start:t_start+nt-1])

	;   ----- closed loop WFS measurements during open loop operation --------
	;	centsOLx = transpose(centsOL[0:ns-1,t_start:t_start+nt-1])
	;	centsOLy = transpose(centsOL[2*ns:3*ns-1,t_start:t_start+nt-1])

	;	----- closed loop WFS measurements during closed loop operation ------
		centsCLx = transpose(centsCL[0:ns-1,t_start:t_start+nt-1])
		centsCLy = transpose(centsCL[2*ns:3*ns-1,t_start:t_start+nt-1])

		centsOL = [[centsOLx],[centsOLy]]
		centsCL = [[centsCLx],[centsCLy]]
	endif else begin
		centsOL = transpose(centsOL[0:2*ns-1,t_start:t_start+nt-1])
		centsCL = transpose(centsCL[0:2*ns-1,t_start:t_start+nt-1])
	endelse

	; remove tip and tilt
	remove_tiptilt = 0

	if (remove_tiptilt) then begin

		tip = total(centsOL[*,0:ns-1],2)/float(ns)
		for t=0,nt-1 do centsOL[t,0:ns-1] -= tip
		tilt = total(centsOL[*,ns:2*ns-1],2)/float(ns)
		for t=0,nt-1 do centsOL[t,ns:2*ns-1] -= tilt

		tip = total(centsCL[*,0:ns-1],2)/float(ns)
		for t=0,nt-1 do centsCL[t,0:ns-1] -= tip
		tilt = total(centsCL[*,ns:2*ns-1],2)/float(ns)
		for t=0,nt-1 do centsCL[t,ns:2*ns-1] -= tilt

	endif

	; --- illuminated actuators ---
	acti = [      15,16, $
	        25,26,27,28,29,30, $
	     36,37,38,39,40,41,42,43, $
	  47,48,49,50,51,52,53,54,55,56, $
	  59,60,61,62,      65,66,67,68, $
	  71,72,73,74,      77,78,79,80, $
	  83,84,85,86,87,88,89,90,91,92, $
	     95,96,97,98,99,100,101,102,103, $
	        109,110,111,112,113,114, $
	              123,124 ]
	acti += 2
	illumMask = fltarr(144)
	illumMask[acti] = 1
	; --- powered actuators ---
	actp = indgen(140)
	actp[0:9] += 1
	actp[10:129] += 2
	actp[130:139] += 3

	modal = 0
	if (modal) then mode_1 = reform(vm4[*,*,0],144,1)

	cma = fltarr(120,144)
	cma[*,actp] = cm
	actsOL = cma ## centsOL
	actsCL = cma ## centsCL

	aveActsOL = total(actsOL,1)/float(nt)
	aveActsCL = total(actsCL,1)/float(nt)
	for t = 0,nt-1 do actsOL[t,*] -= aveActsOL
	for t = 0,nt-1 do actsCL[t,*] -= aveActsCL

	if (modal) then begin
	  actsOL = mode_1 ## actsOL
	  actsCL = mode_1 ## actsCL
	endif

	na = n_elements(acti)
	fs = 1000
	psOL = abs(fft(actsOL[*,acti],dimension=1))^2
	if (not modal) then psOL = total(psOL,2)/float(na)
	ps_plot,psOL,fs,/xlog,/ylog,yrange=[.001,1000.]
	psCL = abs(fft(actsCL[*,acti],dimension=1))^2
	if (not modal) then psCL = total(psCL,2)/float(na)
	ps_plot,psCL,fs,/oplot,color=180
	ps_plot,0.5*psCL/psOL,fs,/oplot,color=120

	xyouts,200,200,ofileset[theCase]+','+cfileset[theCase],/device
	wait,1.

;endfor
endwhile

aveDM = total(dmCL,2)/float(nt)
for k=0,nt-1 do dmCL[*,k] -= aveDM
showDM = fltarr(144,nt)
showDM[actp,*] = dmCL

loadct,0
showActsCL = reform(transpose(actsCL),12,12,nt)
showActsOL = reform(transpose(actsOL),12,12,nt)
;disp,showActsCL,'closed loop reconstructed wavefront'
;disp,showActsOL,'open loop reconstructed wavefront'
;disp,showDM,'closed loop DM commands'
;u = [reform(0.25*showActsOL,12,12*nt),reform(showDM,12,12*nt)]
u = [reform(showActsCL,12,12*nt),reform(showActsOL,12,12*nt),4*reform(showDM,12,12*nt)]
u = reform(u,12*3,12,nt)
disp,u

makeMovie = 0

if (makeMovie) then begin
	print,'making movie..' & wait,.1
	gifdir = dir+'\movie\'
	subsample = 4
	us = smooth(u,[1,1,subsample])
	samples = indgen(nt/subsample)*subsample
	us = us[*,*,samples]
	ub = bytscl(us)
	nframes = (size(us))[3]
	for k=0,nframes-1 do write_gif,gifdir+'acts_'+string(k,format='(I04)')+'.gif',ub[*,*,k]
	print,'done'
endif

end
