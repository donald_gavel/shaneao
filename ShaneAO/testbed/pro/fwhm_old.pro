;+
;  fwhm.pro - make a radial plot of a star from its maximum
;    This routine old. Use radialpsf.pro instead
;-
pro fwhm,a,rmax
  ma = max(a)
  p = coords(a,!c)
  n = (size(a))(1)
  m = (size(a))(2)
  x = (findgen(n)-p(0))#(fltarr(m)+1)
  y = (fltarr(n)+1)#(findgen(m)-p(1))
  r = sqrt(x^2+y^2)
  r1 = reform(r,n*m)
  a1 = reform(a,n*m)
  if (n_elements(rmax) eq 0) then rmax = max(r)
  plot,r1,a1,psym=1,xrange=[0,rmax]
end
