;+
;  lorentzian.pro - psf, similar to atmospheric
;
;-
function lorentzian,n,cx,cy,r0,p
  if (n_elements(p) eq 0) then p = 2.
  x = findgen(n) # (fltarr(n)+1) - n/2
  y = (fltarr(n) + 1) # findgen(n) - n/2
  r = sqrt(x^2 + y^2)>.000001
  ret = 1.0/(r0^2 + r^2)^(p/2.)
  return,ret
end

