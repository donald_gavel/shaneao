;
; variance of tip/tilt/piston removed phase
;   (for actuator saturation studies)
;
; Don Gavel, 8/31/05
;
cm = 0.01
microns = 1.e-6

n = 64
D = 8.; 1. ;30.
r0 = 15.*cm; 11.*cm ; 16.*cm
lambda0 = 0.55*microns
L0 = 30.
du = 0.18; 1.5*D/float(n) ; pixel size
ap = circle(n,n,n/2,n/2,(D/2)/du,1.)

L0set = [1e5]; [1.e5,60.,30.]
nset = (size(L0set))[1]
for theCase=0,nset-1 do begin

	L0 = L0set[theCase]
	f = screengen(n,n,[r0,L0],du)
	dk = 1./(n*du)

	;window,0
	fr = ravg(f^2)/dk^2
	kr = findgen(n/2)*dk
	;plot,kr,fr,xrange=[1,n/2-1]*dk,/xlog,/ylog
	;oplot,kr,(0.023/r0^(5./3.))*kr^(-11./3.),linestyle=2

	kx = (ones(n) ## findgen(n)-n/2)*dk
	ky = transpose(kx)
	k0 = 1./L0
	k = sqrt(kx^2+ky^2+k0^2)
	Sphi = (0.023/r0^(5./3.))*k^(-11./3.)
	Sphi = f^2/dk^2
	Sphi[n/2,n/2]=0
	Dphi = 2*real(ft(Sphi,/inverse)*dk^2)
	Dphi = Dphi[n/2,n/2]-Dphi

	;window,/free
	Dphir = ravg(Dphi)
	rr = findgen(n/2)*du
	;plot,rr,Dphir,xrange=[1,n/2-1]*du,/xlog,/ylog
	;oplot,rr,6.88*(rr/r0)^(5./3.),linestyle=2

	u = (ones(n) ## findgen(n)-n/2)*du
	v = transpose(u)
	r = sqrt(u^2+v^2)
	;Dphi = 6.88*(r/r0)^(5./3.)

	piston = real(zernike(n,n,(D/2)*du,0,0))*ap
	piston = piston/sqrt(total(piston^2*du^2))
	tiptilt = zernike(n,n,(D/2)*du,1,1)*ap
	tip = real(tiptilt)
	tip = tip/sqrt(total(tip^2*du^2))
	tilt = imaginary(tiptilt)
	tilt = tilt/sqrt(total(tilt^2*du^2))

	normp = 1./(total(piston)*du^2)
	norma = 1./(total(ap)*du^2)

	g = fltarr(n,n,3)
	z = fltarr(n,n,3)
	z[*,*,0] = piston
	z[*,*,1] = tip
	z[*,*,2] = tilt
	a = fltarr(3,3)
	term1 = fltarr(n,n)
	term2 = fltarr(n,n)
	nz = 3

	for i=0,nz-1 do begin
	 g[*,*,i] = 0.5*real(ftconvolve(Dphi,z[*,*,i]))*du^2
	 term1 += g[*,*,i]*z[*,*,i]
	 for j=0,nz-1 do begin
	   a[i,j] = total(g[*,*,i]*z[*,*,j])*du^2
	   term2 += a[i,j]*z[*,*,i]*z[*,*,j]
	 endfor
	endfor
	sig = 2*term1 - term2

	; sanity check code...
	sanityCheck = 0
	if (sanityCheck) then begin
		g1 = 0.5*real(ftconvolve(Dphi,piston))*du^2
		a1 = total(g1*piston)*du^2
		u1 = 2*g1*piston - a1*piston^2

		w = piston*normp
		g0 = 0.5*real(ftconvolve(Dphi,w))*du^2*ap
		a0 = total(g*w)*du^2*ap^2
		u0 = 2*g-a
		window,/free,title='sanity check'
		plot,rr/D,ravg(u1)/(6.88*(D/r0)^(5./3.)),xrange = [0,.5]
		oplot,rr/D,ravg(u0)/(6.88*(D/r0)^(5./3.)),linestyle=2
	endif

	if (theCase eq 0) then begin
		colors = [255,200,150]
		window,/free,title='stroke rms vs radius'
		plot,rr/D,sqrt(ravg(sig)/((D/r0)^(5./3.))),xrange=[0,0.5],xtitle='r/D',ytitle='sigma/(D/r0)^(5/6)'
		oplot,rr/D,ones(n/2)*sqrt(.134),linestyle=2 ; Noll coefficient after piston, tip, tilt removed
	endif else begin
		oplot,rr/D,sqrt(ravg(sig)/((D/r0)^(5./3.))),color=colors[theCase]
	endelse
endfor

doTrials = 1
dwoof = 0.5
f0 = 1./(2.*dwoof)
fmask = fix(((abs(kx) gt f0) or (abs(ky) gt f0)))
if (doTrials) then begin
	sDev = fltarr(nset)
	for theCase=0,nset-1 do begin
		L0 = L0set[theCase]
		f = screengen(n,n,[r0,L0],du)*fmask
		ntrials = 10; 1000
		sigest = (lambda0/(2*!pi))*sqrt(0.13*(D/r0)^(5./3.))/microns
		range = [-5*sigest,5*sigest]
		range = [-10,10]
    binsize = 0.2 ; 200 nm
    nbins = (range[1]-range[0])/binsize
;		binsize = (range[1]-range[0])/float(nbins-1)
;		p = findgen(nbins)*binsize+range[0]
		hav = fltarr(nbins)
		count = 0
		for i=0,ntrials-1 do begin
			print,format='($,I0," ")',i
			if ((i mod 50) eq 0) then print,''
			wait,.01
			ph = screengen(f,seed)*(lambda0/(2*!pi))/microns
			ph = depiston(ph,ap)
			ph = detilt(ph,ap)*ap
;     h = histogram(ph[where(ap eq 1)],nbins=nbins,min=range[0],max=range[1])
      h = histogram(ph[where(ap eq 1)],binsize=binsize,min = range[0], max = range[1])
      p = findgen(nbins)*binsize+range[0]
			if ((max(ph) gt range[1]) or (min(ph) lt range[0])) then begin
				count += total(ph gt range[1])+total(ph lt range[0])
			endif
			h = h/total(h)
		;	if (i eq 0) then plot,p,h
		;	if (i ne 0) then oplot,p,h
			hav += h/float(ntrials)
		endfor
		print,''
		fractionOutOfRange = count/(ntrials*total(ap))
		print,'fraction out of range = ',fractionOutOfRange
		sDev[theCase] = sqrt(total(hav*p^2))
		print,'L0 = ',L0,', std dev = ',sDev[theCase],'microns'
		if (theCase eq 0) then begin
			colors = [255,200,150]
			window,/free,title='histogram'
			plot,1000*p,0.5*hav/max(hav),/ylog,charsize=2,ticklen=0.5,xtitle='stroke, surface in nm',ytitle='relative and cumulative frequency',yrange=[1e-4,1],xrange=[-1499.,1499.]
		endif else begin
			oplot,p,hav/max(hav),color=colors[theCase]
		endelse
		cum = hav+reverse(hav)
		cum = cum[nbins/2:nbins-1]
		cum = integral(cum)
		oplot,p[nbins/2:nbins-1],1.-cum,color=colors[theCase]
		if (theCase eq 0) then begin
			cumSet = fltarr((size(cum))[1],2,nset)
		endif
		cumSet[*,0,theCase]=p[nbins/2:nbins-1]
		cumSet[*,1,theCase]=cum
	endfor
endif

end
