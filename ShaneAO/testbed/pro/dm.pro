;+
;  dm.pro - generic dm, rectilinear geometry
;;;
;-----------------------------------------------------------------
;  makebasis - create actuator "basis" functions
;  ---------
;  amatrix = makebasis(dmgeo,d,basis=basis,sigma=sigma)
;
;   inputs
;     d - actuator spacing, pixels
;     sigma - width of gaussian envelope, pixels
;     ap - optional aperture over which to restrict the inner products
;       (and optionally define the phase screen size to be other than 128 x 128)
;   outputs
;     basis - cube of basis functions
;     amatrix - matrix of basis function inner products
;-----------------------------------------------------------------
;  dmfit - fit a wavefront to dm actuator pushes
;  -----
;    actvec = dmfit(wf,basis,ainv,/invert,ap=ap)
;   inputs:
;     wf - wavefront phase data
;     basis - from makebasis
;     ainv - "inverse" of a
;          here is sample inversion code:
;              eval = eigenql(a,eigenvectors=ev)
;              evali = (1/(eval>thresh))*(eval gt thresh)
;              ainv = transpose(ev)##diagonal(evali)##ev
;     invert - whether or not to invert "ainv" (=a); (no threshold)
;     ap - optional aperture over which to best-fit the wavefront
;   output:
;     actvec - a vector of actuator commands, in the same units as wf
;-----------------------------------------------------------------
;  pushact - create a phase screen according to dm actuator pushes
;  -------
;    phase = pushact(act,basis)
;
;   inputs
;      act - na element vector of actuator commands
;      basis - cube of actuator basis functions
;   output
;      phase - resulting phase screen
;-
function rectdmgeom,ap,da
; assume the actuators that circumscribe the dm
;   ap = square array with an aperture mask
;   da = actuator spacing, m
	n = (size(ap))[1]
  xrange = [-(n/2),(n/2-1)]
  nactsacross = round(xrange[1]/da)+1+round(-xrange[0]/da)
  xrange = [-reverse(findgen(nactsacross/2-1))-1,findgen(nactsacross/2.)]*da
  nactsacross = (size(xrange))[1]
  nacts = long(nactsacross)^2
  dmgeo = fltarr(nacts,2)
  k = 0
  for i=0,nactsacross-1 do begin
  	x = xrange[i]
  	ix = x+(n/2)
  	for j=0,nactsacross-1 do begin
  		y = xrange[j]
  		iy = y+(n/2)
  		if (ap[ix,iy]) then begin
  			dmgeo[k,*] = [x,y]
  			k += 1
  		endif
  	endfor
  endfor
  dmgeo = dmgeo[0:k-1,*]
  return,dmgeo
end

function makebasis,dmgeo,d,basis=basis,sigma=sigmaarg, ap=ap
;   (sin(pi x)/(pi x)) exp(-(1/2)(x/sig)^2)
  na = (size(dmgeo))[1]
  if n_elements(ap) eq 0 then begin
    n = 128
    ap = ones(n,n)
  endif else begin
    n = (size(ap))[1]
  endelse
  if n_elements(sigmaarg) eq 0 then begin
    print,'computing sigma internally'
    sigma = d
  endif else begin
    sigma = sigmaarg
  endelse
  x = (findgen(n)-n/2) # ones(n)
  x[n/2,*] = .1
  y = ones(n) # (findgen(n)-n/2)
  y[*,n/2] = .1
  fx = (sin(!pi*x/d)/(!pi*x/d))
  fx[n/2,*] = 1.
  fy = (sin(!pi*y/d)/(!pi*y/d))
  fy[*,n/2] = 1.
  f = fx*fy
  f = f*gauss2(n,n/2,n/2,sigma) ; normalized to 1 at the peak
  basis = fltarr(n,n,na)
  for i=0,na-1 do begin
    xi = dmgeo[i,0]
    yi = dmgeo[i,1]
    basis[*,*,i] = shift(f,xi,yi)*ap
  endfor
  a = fltarr(na,na)
  for i=0,na-1 do begin
    print,format='($,I0," ")',i
    for j=0,na-1 do begin
      a[i,j] = total(basis[*,*,i]*basis[*,*,j])
    endfor
  endfor
  print,'done'
  return, a
end

function showdm,dmgeo,ap
  dm = ap*0
  n = (size(dm))[1]
  na = (size(dmgeo))[1]
  for i=0,na-1 do begin
    xi = dmgeo[i,0]
    yi = dmgeo[i,1]
    dm[xi+n/2,yi+n/2] = 1
  endfor
  return,dm
end

function dmfit,wf,basis,aarg,ap=ap,invert=invert
  if n_elements(ap) eq 0 then begin
    ap = basis[*,*,0]*0 + 1
  endif
  if keyword_set(invert) then begin
    eval = eigenql(aarg,eigenvectors=ev)
    ainv = transpose(ev)##diagonal(1/eval)##ev
  endif else begin
    ainv = aarg
  endelse
  na = (size(basis))[3]
  b = fltarr(na)
  for i=0,na-1 do begin
    b[i] = total(wf*basis[*,*,i]*ap)
  endfor
  act = ainv##b
  return,act
end

function pushact,act,basis
  n = (size(basis))[1]
  na = (size(basis))[3]
  nat = (size(act))[1]
  if nat ne na then begin
    print,'<pushact> number of actuators ',nat,' does not match number of basis fucntions ',na
    return,0
  endif
  dm = fltarr(n,n)
  for i=0,na-1 do begin
    dm = dm + act[i]*basis[*,*,i]
  endfor
  return,dm
end
