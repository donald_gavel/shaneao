;+
;  aoTiltps.pro - find the overall tilt power spectrum for Lick AO wfs data
;
;  USAGE:
;    tiltps = aoTiltps(data,fs)
;
;    data is standard centroid dataset from Lick data, 81xN (N usually = 4096)
;     (81st row is the loop status, 0=open, 1=closed)
;    fs is the sample rate in Hz
;    ps is the power spectrum in units(data)^2/hz
;      ps is the "dual-sided" spectra, i.e. it contains the values for both
;      negative and positive frequency.  Elements 0:N/2 are the positive side.
;
;    showps takes the dual-sided spectrum and plots only one side, doubling it
;     so each frequency bin represents total contributions from both "sides"
;
;    verify proper scaling with Parseval's theorem:
;      sqrt(total(ps(0,*))*df) eq sqrt(total((total(sdata(1:39,*),1)/40.)^2)/float(n))
;      where df=fs/N
;-
function aoTiltps,data,fs,full,tx,ty
  dt = 1./fs
;   These are the fully illuminated subapertures:
  isetillum = $
         [ 3,          4,          5,          6,          8,          9,$
          10,         11,         12,         13,         15,         16,$
          17,         18,         21,         22,         23,         24,$
          26,         27,         28,         29,         30,         31,$
          33,         34,         35,         36]
  isetall = indgen(40)
  iset = isetall
  if (keyword_set(full)) then begin
    iset = isetillum;    <-- change to select either only illuminated or all subaps
    print,'using fully illuminated subapertures only'
  endif
  xdata = data(iset,*)
  ydata = data(iset+40,*)
  n = (size(data))(2)
  m = (size(iset))(1)
  print,'averaging tilts'
  tx = total(xdata,1)/float(m)
  tx = tx - mean(tx)
  ty = total(ydata,1)/float(m)
  ty = ty - mean(ty)
  print,'done'
  print,'computing ffts...'
  ftx = n*fft(tx)*dt
  fty = n*fft(ty)*dt
  psx = ftx*conj(ftx)
  psy = fty*conj(fty)
  print,'done'
;   Average, and scale so result is in units(data)^2/hz
  psx = psx/(float(n)*dt)
  psy = psy/(float(n)*dt)
  ps = fltarr(2,n)
  ps(0,*) = psx
  ps(1,*) = psy
  return,ps
end
