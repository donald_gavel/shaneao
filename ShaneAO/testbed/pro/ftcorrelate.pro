;+
;  ftcorrelate.pro - cross correlate two images, fft method
;
;  usage:
;    res = ftcorrelate(im1,im2)
;-
function ftcorrelate,im1,im2
  return,ftconvolve(im1,shift(reverse(reverse(im2,2)),1,1))
end
