;
; legend.pro
;   a dummy test routine
path = strsplit(!path,";",/extract)
top = strsplit(path[0],"\",/extract)
n = n_elements(top)
pro_precedent = 0
for k = 0,n-1 do if top[k] eq 'pro' then pro_precedent = 1

theory = SIN(2.0*FINDGEN(201)*!PI/25.0)*EXP(-0.02*FINDGEN(201))
observed = theory + RANDOMU(seed,201)*0.4-0.2

if (pro_precedent eq 0) then begin
p1 = plot(observed, NAME='Observed')
; Display the second plot.
p2 = plot(theory, /OVERPLOT, 'r2', NAME='Theory')
; Add the legend.
l = legend(TARGET=[p1,p2], POSITION=[140,0.9], /DATA)
endif

if (pro_precedent eq 1) then begin
window
plot,observed
oplot,theory
legend,['stuff','other stuff'],linestyle=[0,1]
endif
end
