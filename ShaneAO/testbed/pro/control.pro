;+
;
;  control.pro - calculate system response
;
;    system = controller * sampleAndHold * stare
;
;    controller = ( G z / (z-p) )
;    sampleAndHold = ( 1 - (1/z) ) / (T s)
;    stare = ( 1 - (1/z) ) / (T s)
;
;     where G = loop gain
;           p = integrator pole location (1= pure integrator)
;           T = sample period = 1/fs
;           fs = sample rate
;
;  Usage:
;
;   r = closedLoopResponse(g,p,fs,npts=1024)
;   r = openLoopResponse(g,p,fs,npts=1024)
;
;   plotMag,r,fs,/db,color=255,title="magnitude"
;   plotPhase,r,fs,color=255,title="phase"
;   nyquistPlot,r,fs,color=255
;
;   The plot functions all have a /oplot option
;
;-
function openLoopResponse,g,p,fs,npts=npts
  if (n_elements(npts) eq 0) then npts = 1024
  df = fs/float(npts)
  T = 1/float(fs)
  f = findgen(npts)*df
  f[0] = df/100.  ;  to avoid division by zero
  I = complex(0,1)
  s = 2*!pi*I*f
  z = Exp(s*T)
  comp = z/(z-p)
  zoh = (1 - (1/z))/(T*s)
  stare = (1 - (1/z))/(T*s)
  r = G * comp * stare * zoh
  return,r
end

function closedLoopResponse,g,p,fs,npts=npts
  if (n_elements(npts) eq 0) then npts = 1024
  r = openLoopResponse(g,p,fs,npts=npts)
  r = 1 / (1 + r)
  return,r
end

;-----------------------------------
;  example requires powerSpectrumPlot.pro, apPhasePs.pro
pro responseExample
  matdir = '/duck3/gavel/lickdataNov98/lgs6/Matrices/'
  datadir = '/duck3/gavel/lickdataNov98/Nov13/'
  print,'open loop data'
  cent = 'cent_34'
  fs = 500.
  loop = 'open'; 60 And loop, from Nov 98
  data = rdview(datadir+cent)
  cm = rdview(matdir + 'CMNov05_02')
  ps = aoPhasePs(data,cm,fs)
  powerSpectrumPlot,ps,fs
  print,'calculate closed loop response'
  g = .7
  p = .992
  h = closedLoopResponse(g,p,fs,npts=4096)
  powerSpectrumPlot,ps*h*conj(h),fs,/oplot,color=50
end

pro plotMag,r,fs,db=db,color=color,oplot=oplot,title=title
  if (n_elements(color) eq 0) then color = 255
  if (n_elements(title) eq 0) then begin
    if (keyword_set(db)) then begin
      title = "db"
    endif else begin
      title = "mag"
    endelse
  endif
  npts = (size(r))(1)
  df = fs/float(npts)
  f = findgen(npts)*df
  p = abs(r)
  if (keyword_set(oplot)) then begin
      if (keyword_set(db)) then begin
	oplot,f[1:npts/2],20*alog10(p[1:npts/2]),color=color
      endif else begin
	oplot,f[1:npts/2],p[1:npts/2],color=color
      endelse
  endif else begin
      if (keyword_set(db)) then begin
	plot,f[1:npts/2],20*alog10(p[1:npts/2]),/xlog, $
	     xtitle='Frequency, Hz',ytitle=title,charsize=1.5,color=color
      endif else begin
	plot,f[1:npts/2],p[1:npts/2],/xlog,/ylog, $
	     xtitle='Frequency, Hz',ytitle=title,charsize=1.5,color=color
      endelse
  endelse
end

function angle,a
  fuzz = 1e-9
  ra = real(a)
;  ra(where(ra eq 0)) = fuzz
  return, atan(imaginary(a),ra)
end

pro plotPhase,r,fs,color=color,oplot=oplot,title=title
  if (n_elements(color) eq 0) then color = 255
  if (n_elements(title) eq 0) then begin
    title = "arg"
  endif
  npts = (size(r))(1)
  df = fs/float(npts)
  f = findgen(npts)*df
  p = angle(r)*(180./!pi)
  if (keyword_set(oplot)) then begin
      oplot,f[1:npts/2],p[1:npts/2],color=color
  endif else begin
      plot,f[1:npts/2],p[1:npts/2],/xlog, $
	     xtitle='Frequency, Hz',ytitle=title,charsize=1.5,color=color
  endelse
end

pro bodePlot,r,fs,_extra=e,oplot=oplot
  npts = (size(r))(1)
  df = fs/float(npts)
  f = findgen(npts)*df
  f = f[1:npts/2]
  xtitle='Frequency, Hz'
  charsize=2.
  ticklen = 0.04
;
  p = abs(r)
  p = p[1:npts/2]
  plot,f,p,/xlog,/ylog,ystyle=4,xmargin=[10,8],xtitle=xtitle, $
    charsize=charsize,ticklen=ticklen,_extra=e
  axis,yaxis=0,charsize=charsize,ytitle='magnitude',ticklen=ticklen
  oplot,f,ones((size(f))[1])*1.0
;
;
  p = angle(r)*(180./!pi)
  p = p[1:npts/2]
  plot,f,p,/xlog,ystyle=4,xstyle=4,xmargin=[10,8], $
     yrange=[-180,0],/noerase, color=100, charsize=charsize, ticklen=ticklen
  axis,yaxis=1,yrange=[-180,0],charsize=charsize,ytitle='phase',ticklen=ticklen
  oplot,f,ones((size(f))[1])*(-180),color=100
end

pro nyquistPlot,r,fs,color=color,oplot=oplot
  if (n_elements(color) eq 0) then color = 255
  npts = (size(r))(1)
  df = fs/float(npts)
  f = findgen(npts)*df
  I = complex(0,1.)
  z = Exp(2*!pi*I*f/fs)
  re = real(r)
  im = imaginary(r)
  if (keyword_set(oplot)) then begin
      oplot,re[1:npts/2],im[1:npts/2],color=color
  endif else begin
      plot,real(z),imaginary(z),xrange=[-2,2],yrange=[-2,2],charsize=1.5, $
        xstyle=4,ystyle=4
      oplot,re[1:npts/2],im[1:npts/2],color=color
      axis,0,0,/xaxis
      axis,0,0,/yaxis
  endelse
end
;
pro control
	print,'<control> compiled
end
;
   p = 1.0 ;
   g = 1.0 ; gain
   fs = 500
   r = closedLoopResponse(g,p,fs,npts=1024)
   r0 = openLoopResponse(g,p,fs,npts=1024)

   window,/free & plotMag,r0,fs,/db,title="magnitude"
   plotMag,r,fs,/oplot,color=120
   ;window,/free & plotPhase,r,fs,title="phase"
   ;window,/free,xsize=500,ysize=500 & nyquistPlot,r,fs,color=255
end