;
; pwd.pro
;   simple emulation of the unix pwd command
;
pro pwd
	cd, current=dir
	print,dir
end

