function readParm,parm,filename
  line = ""
  openr, unit,filename+'.spr',/get_lun,error=err
  if (err ne 0) then begin
    print,!err_string
    return,info
  endif
  for i=0,9 do readf,unit,line ; initial header info
  readf,unit,line & date = line
  while (not eof(unit)) do begin
    readf,unit,line
    res = strsplit(line,':',/extract)
    if (res[0] eq parm) then begin
      free_lun, unit
      return,strtrim(res[1],2)
    endif
  endwhile
  free_lun,unit
  return,0
end