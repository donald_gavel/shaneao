;+
;  ftcube - take Fourier transform of each frame in a data cube
;
;  ftcube, incube, outcube
;
;-
pro ftcube, a, b
  n = (size(a))(1)
  m = (size(a))(2)
  nf = (size(a))(3)
  if (n_elements(b) eq 0) then b = complexarr(n,m,nf,/nozero)
  for i=0,nf-1 do begin
    print,format='($,I0," ")',i
    b(*,*,i) = float(n)*float(m)*ft(a(*,*,i))
  endfor
  print,'done'
end