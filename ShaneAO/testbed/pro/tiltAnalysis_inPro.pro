;clearwins
;!p.background = 255
;!p.color = 0
!p.charsize = 2

baseDir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/'
;dataSets = [44,5,1]
;date_set = ['080917','081023','090331']

;date_set = replicate('080915',5)
;dataSets = indgen(4)

;date_set = replicate('080916',15)
;dataSets = indgen(15)

;date_set = replicate('080917',49)
;dataSets = indgen(49)

;date_set = replicate('081022',5)
;dataSets = indgen(5)

;date_set = replicate('081023',55)
;dataSets = indgen(55)

;date_set = replicate('081024',5)
;dataSets = indgen(5)

;date_set = replicate('090330',11)
;dataSets = indgen(11)+1

;date_set = replicate('090331',5)
;dataSets = indgen(5)

date_set = replicate('090401',34)
dataSets = indgen(34)

n_sets = (size(dataSets))[1]
dset_set = strarr(n_sets)
for k=0,n_sets-1 do begin
  dset_set[k] = string(dataSets[k],format='(I03)')
endfor

; switches for plot options
ttplots = [1,1]
spectraPlots = [1,1]
rejectionPlot = 1
zoomPlot = 1
yarnBallPlot = 1
residYarnBallPlot = 1

plotToFile = 1
plotFile = '/Users/DonGavel/Desktop/ttplots.ps'

if (plotToFile) then begin
  mydevice = !d.name
  set_plot,'PS'
  device,filename=plotFile,/landscape
endif

wfs = 'open loop' ; 'closed loop' ; <--------------- which wavefront sensor!

for this_set = 0,n_sets-1 do begin
;this_set = 0

dset = dset_set[this_set]; '003'
date = date_set[this_set]
titlefix = date + '/' + dset
dir = baseDir + date + '/'
u = rdview(dir+'tt_'+dset,error = err)
if (err ne 0) then begin
  print,dir+'tt_'+dset,' not available
  continue
endif
n = 1024
fs = 1000
fc = 100 ; roughly 20 hz control bandwidth
dt = 1./fs
df = fs/float(n)
t = findgen(n)*dt
f = findgen(n)*df
pixelsPerVolt = 4.0 ; 5.0
arcsecPerPixel = 2.0

if (wfs eq 'closed loop') then begin
  tiltx = u[0,*]*arcsecPerPixel
  tilty = u[1,*]*arcsecPerPixel
endif
if (wfs eq 'open loop') then begin
  tiltx = u[2,*]*arcsecPerPixel
  tilty = u[3,*]*arcsecPerPixel
endif

print,'========== ',dir,' =========='
print,'========== ',dset,' =========='
print,'rms residual tip/tilts: x:',rms(tiltx),' y:',rms(tilty),' arcsec'
tiltxc = (u[4,*]-average(u[4,*]))*pixelsPerVolt*arcsecPerPixel
tiltyc = (u[5,*]-average(u[5,*]))*pixelsPerVolt*arcsecPerPixel
print,'rms controled tip/tilts: x:',rms(tiltxc),' y:',rms(tiltyc),' arcsec'
print,'rms 2-axis tip/tilt from '+wfs+' wfs:',sqrt(rms(tiltx)^2+rms(tilty)^2),' arcsec'

if (ttplots[0]) then begin
  if (!d.name eq 'X') then window,/free,title='x tip/tilt '+titlefix
  plot,t,tiltxc,xtitle='time, sec',ytitle='arcsec',title='x tip/tilt '+titlefix
  oplot,t,tiltx,color=150
endif
if (ttplots[1]) then begin
  if (!d.name eq 'X') then window,/free,title='y tip/tilt '+titlefix
  plot,t,tiltyc,xtitle='time, sec',ytitle='arcsec',title='y tip/tilt '+titlefix
  oplot,t,tilty,color=150
endif

fx = reform(abs(fft(tiltx)))*n*dt
fy = reform(abs(fft(tilty)))*n*dt
fxc = reform(abs(fft(tiltxc)))*n*dt
fyc = reform(abs(fft(tiltyc)))*n*dt
fxu = reform(abs(fft(tiltx+tiltxc)))*n*dt
fyu = reform(abs(fft(tilty+tiltyc)))*n*dt
tfx = fx/fxu
tfy = fy/fyu
tfm = sqrt(f^2/(fc^2+f^2)) ; model rejection spectrum

; do a power-law fit to the tilt spectra
n1=1
n2=fix(40./df) ; from df to 40 Hz
nfit = n2-n1+1
w = ones(nfit)
fp = alog10(f[n1:n2])
; x axis
sp = alog10(fxc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
abx = invert(aa) ## ay
sbarx = 10^(abx[0])*f^(-abx[1])
sbarx[0] = 0.
; y axis
sp = alog10(fyc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
aby = invert(aa) ## ay
sbary = 10^(aby[0])*f^(-aby[1])
sbary[0] = 0.
; ------- end of power law fits -----

if (spectraPlots[0]) then begin
  if (!d.name eq 'X') then window,/free,title='x tilt spectra '+titlefix
  powerSpectrumPlot,fxc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='x tilt spectra '+titlefix
  powerSpectrumPlot,fx,fs,/oplot,color=150
  powerSpectrumPlot,sbarx,fs,/oplot,color=100
  powerSpectrumPlot,sbarx*tfm,fs,/oplot,color=120
endif
if (spectraPlots[1]) then begin
  if (!d.name eq 'X') then window,/free,title='y tilt spectra '+titlefix
  powerSpectrumPlot,fyc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='y tilt spectra '+titlefix
  powerSpectrumPlot,fy,fs,/oplot,color=150
  powerSpectrumPlot,sbary,fs,/oplot,color=100
  powerSpectrumPlot,sbary*tfm,fs,/oplot,color=120
endif
if (rejectionPlot) then begin
  if (!d.name eq 'X') then window,/free,title='Transfer Functions ' +titlefix
  powerSpectrumPlot,0.5*tfx,fs,/xlog,/ylot,ytitle='transfer function',xtitle='frequency, hz',title='Transfer Functions '+titlefix
  powerSpectrumPlot,0.5*tfy,fs,/oplot,color=150
  powerSpectrumPlot,0.5*tfm,fs,/oplot,color=100
endif

if (zoomPlot) then begin
  if (!d.name eq 'X') then window,/free,title='Zoom of x tilt residuals '+titlefix
  plot,t[100:200],tiltx[100:200],ytitle='arcsec',xtitle='seconds',title='zoom in of x tilt residual '+titlefix
  oplot,t[100:200],tiltx[100:200],color=150
  oplot,t[100:200],tiltxc[100:200]-average(tiltxc[100:200])
endif

if (yarnBallPlot) then begin
  if (!d.name eq 'X') then window,/free,title='tip/tilt yarn ball '+titlefix,xsize=1100,ysize=1000
  plot,tiltxc,tiltyc,title = 'tip/tilt yarn ball '+titlefix,xrange = [-1.,1.],yrange=[-1.,1.]
  oplot,tiltx,tilty,color=150
endif
if (residYarnBallPlot) then begin
  if (!d.name eq 'X') then window,/free,title='tt resid yarn ball '+titlefix,xsize=1100,ysize=1000
  plot,tiltx,tilty,title = 'tt resid yarn ball '+titlefix,xrange=[-.1,.1],yrange=[-.1,.1],/nodata
  oplot,tiltx,tilty,color=150
endif
;
; print statixticsl analysis
print,'total in atmospheric model (x,y):', sqrt(total((sbarx)^2))*df,sqrt(total((sbary)^2))*df,' arcsec'
print,'total in control data (x,y):', sqrt(total(fxc^2))*df,sqrt(total(fyc^2))*df,' arcsec'
print,'total in residual model (x,y):', sqrt(total((sbarx*tfm)^2))*df,sqrt(total((sbary*tfm)^2))*df,' arcsec'
print,'total in residual data (x,y):', sqrt(total(fx^2))*df,sqrt(total(fy^2))*df,' arcsec'

print,'==========================================================================================='

endfor

if (plotToFile) then begin
  device,/close
  set_plot, mydevice
endif

end
