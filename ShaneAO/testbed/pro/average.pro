;
;  average.pro - compute the average of 2d data
;
function average,data,ap
  da = data
  apf = finite(da)
  nan_ind = where(1-apf,count)
  if (count ne 0) then begin
  	da[nan_ind] = 0
  	print,'<average> Warning: there are ',strtrim(n_elements(nan_ind),2),' NaNs in the data'
  endif
  if (n_elements(ap) ne 0) then apf *= ap
  da *= apf
  return,total(da)/float(total(apf))
end
