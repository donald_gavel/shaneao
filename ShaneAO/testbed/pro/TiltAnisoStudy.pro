;
; Tilt Anisoplanatism Study
;

; define the tilt, aperture, and structure functions
n = 256
r_max = 40.; 50.
dx = (r_max+4.)/(float(n))
x = (ones(n) ## findgen(n)-n/2)*dx
r2 = (x^2 + transpose(x)^2)
r = sqrt(r2)
a = circle(n,n,n/2,n/2,1./dx,1.)
b = convolve(a*x,a*x)
bvalid = circle(n,n,n/2,n/2,2./dx,1.)
b *= bvalid
d = 6.88*r2^(5./6.) ; Kolmogorov structure function
; von Karmann structure function
L0 = 30.
k0 = 1/L0
nr = sqrt(2.)*n/2+1
r = findgen(nr)*dx
r[0] = 0.0001*dx
d_vk = 0.022883*4*!pi*(3.*Gamma(11./6.)-5.*k0^(5./6.)*!pi^(5./6)*r^(5./6.)*BesselK(-5./6.,2*!pi*r*k0))/(5.*k0^(5./3.)*Gamma(11./6.))
d_kol = 6.88*r^(5./3.)
;window,/free
;plot,r,d_vk,/xlog,/ylog,ticklen=.5,xrange=[.0001,.1],yrange=[1.e-6,.1]
;oplot,r,d_kol,linestyle=2
r = reform(sqrt(r2),long(n)*long(n))
;
for thecase=0,1 do begin

if (thecase eq 0) then d = reform(interpolate(d_kol,r/dx,cubic=-.5),n,n)
if (thecase eq 1) then d = reform(interpolate(d_vk,r/dx,cubic=-.5),n,n)
;
nvalid = n - 4./dx
valid = zeropad(ones(nvalid,nvalid),n,n)
c = convolve(b,d)
c = c/max(c)
c *= valid
;disp,c,ap=valid,dx=dx*[1,1],x0=-(n/2)*dx*[1,1]
!p.color = 0
!p.background = 2^24-1
!p.charsize=2
!p.thick =2
!p.ticklen=.5
cxx = c[n/2:n/2+nvalid/2-1,n/2]
cyy = c[n/2,n/2:n/2+nvalid/2-1]
s = x[n/2:n/2+nvalid/2-1,n/2]/2.
if (thecase eq 0) then begin
  window,/free
  lun1 = !d.window
  plot,s,cxx,yrange=[-.2,1.],xrange=[0,r_max/4.],title='cxx, cyy for tilt, L0 = {Inf,'+strtrim(L0,2)+'}',xtitle='separation, diameters',ytitle='correlation coefficient'
  oplot,s,cyy,color=120
endif else begin
  wset,lun1
  plot,s,cxx,/noerase,/nodata,yrange=[-.2,1.],xrange=[0,r_max/4]
  oplot,s,cxx,linestyle=2
  oplot,s,cyy,linestyle=2,color=120
endelse
if (thecase eq 0) then begin
  window,/free
  lun2 = !d.window
  plot,s,1-cxx^2,yrange=[0,1.],xrange=[0,r_max/4],title='optimum est err variance',xtitle='separation, diameters'
  oplot,s,1-cyy^2,color=120
endif else begin
  wset,lun2
  plot,s,1-cxx^2,/noerase,/nodata,yrange=[0,1.],xrange=[0,r_max/4]
  oplot,s,1-cxx^2,linestyle=2
  oplot,s,1-cyy^2,linestyle=2,color=120
endelse
if (thecase eq 0) then begin
  window,/free
  lun3 = !d.window
  plot,s,2 - 2*cxx,yrange=[0,2.5],xrange=[0,r_max/4],title='variance of the difference',xtitle='separation, diameters'
  oplot,s,2 - 2*cyy,color=120
endif else begin
  wset,lun3
  plot,s,2 - 2*cxx,/noerase,/nodata,yrange=[0,2.5],xrange=[0,r_max/4]
  oplot,s,2 - 2*cxx,linestyle=2
  oplot,s,2 - 2*cyy,linestyle=2,color=120
endelse
endfor
end
