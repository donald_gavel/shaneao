;+
;  makeLickAp.pro - create the Lick aperture and/or psf
;-

function makeLickAp,n,lambda=lambda,dx=dx,spider=spider
  arcsec = !pi/(180.*3600.)
  if (n_elements(n) eq 0) then n = 900
  microns = 1e-6
  if (not keyword_set(lambda)) then lambda = 0.55 * microns
  if (not keyword_set(dx)) then dx = 0.063 * arcsec
  du = lambda / (n*dx)
  rp = 1.5
  rs = .3
  lickAp = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
  if (keyword_set(spider)) then begin
  	spidermask = ones(n,n)
  	spidermask[n/2,*] = 0
  	spidermask[*,n/2] = 0
  	spidermask = rot(spidermask,45)
  	spidermask = smooth(spidermask,2)
  	spidermask = spidermask eq 1
  	lickAp *= spidermask
  endif
  return,lickAp
end

function makeLickMTF,n,lambda=lambda,dx=dx
  arcsec = !pi/(180.*3600.)
  if (n_elements(n) eq 0) then n = 900
  microns = 1e-6
  if (not keyword_set(lambda)) then lambda = 0.55 * microns
  if (not keyword_set(dx)) then dx = 0.063 * arcsec
  dp = 3.
  du = lambda/(n*dx)
;   first compute a 256x256 generic mtf
  nn = 256
  du2 = 2*dp/nn
  dx2 = lambda/(nn*du2)
  ap = makeLickAp(nn,lambda=lambda,dx=dx2)
  mtf = shift(abs(fft(abs(fft(ap))^2)),nn/2,nn/2)
;   then resample on the desired grid
  nsub = n*du/du2
  mtf = congrid(mtf(nn/2-nsub/2:nn/2+nsub/2-1,nn/2-nsub/2:nn/2+nsub/2-1),n,n)
  return,mtf
end

function makeLickPsf,lickAp
	n = (size(lickAP))[1]
  psf = abs(shift(fft(lickAp),n/2,n/2))^2
  psf = psf/total(psf)
  return,psf
end

ap = makeLickAp(1024,lambda=2.2e-6,dx=.076*arcsec/4,/spider)
psf = makeLickPsf(ap)
psfr = rebin(psf,256,256)
disp,alog10(psfr>1.e-6)
end
