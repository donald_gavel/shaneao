;
;  hartmannpro.pro
;    support for hartmanngui
;
pro cleanimage,im,params
  devstripe,im,imds,[442,469];       background subtract
  imdsg = ((imds>0)/max(imds))^.2;   contrast stretch
  imdsgc = (imdsg>params.thresholds(0))<params.thresholds(1); bound
  imdsgcn = (imdsgc-min(imdsgc))/(max(imdsgc)-min(imdsgc)); normalize
  im = imdsgcn
end

function hartmannref,hr,params
  roimincount = 5
;  cleanimage,hr,params
;
  devstripe,hr,hrds,[442,469];       background subtract
  hrdsg = ((hrds>0)/max(hrds))^.2;   contrast stretch
  hrdsgc = (hrdsg>params.thresholds(0))<params.thresholds(1); bound
  hrdsgcn = (hrdsgc-min(hrdsgc))/(max(hrdsgc)-min(hrdsgc)); normalize
;
  print,'finding reference centroids'
  loc1 = [params.corners(0), params.corners(1)]
  loc2 = [params.corners(2), params.corners(3)]
  del = float([loc2 - loc1]) / float([params.cols-1,params.rows-1])
  if even(params.rows) then $
    del(0) = float(loc2(0) - loc1(0)) / (params.cols - 1.5)
  ref_cents = find_roi(hrdsgcn,loc1,params.roi,del,roimincount,params.rows,params.cols)
  hr = hrdsgcn
  return,ref_cents
end

function hartmannwf,centroids,centroids0,del,gs,limits
  if n_elements(del) eq 0 then del = 30
  print,'determining slopes'
  slopex = transpose([transpose(centroids0(*,2)), $
          transpose(centroids0(*,3)), $
          transpose(centroids(*,0)-centroids0(*,2))])

;  regular_grid,slopex, rx

  slopey = transpose([transpose(centroids0(*,2)), $
          transpose(centroids0(*,3)), $
          transpose(centroids(*,1)-centroids0(*,3))])

;  regular_grid,slopey,ry

; create a phase point list
  phase = phase_points(centroids0(*,2:3),[0,del/sqrt(3.0)])

; form the interaction matrix
  print,'building interaction matrix'
  near3_list = near3(phase,centroids0(*,2:3),del*1.0)
  mat = make_interaction_matrix(phase,centroids0(*,2:3),near3_list)

; singular value decomposition
  print,'doing singular value decomposition and matrix pseudoinverse'
  svdc, mat, matw, matu, matv
  np = (size(phase))(1)
  matsv = fltarr(np,np)
  for k = 0,np-1 do matsv(k,k) = matw(k)
  matsvi = fltarr(np,np)
  for k = 0,np-1 do matsvi(k,k) = 1/matw(k)
  mati = matv ## matsvi ## transpose(matu)

; convert the slopes to one data vector
  nsx = (size(slopex))(1)
  nsy = (size(slopey))(1)
  s = fltarr(nsx + nsy)
  for k = 0, nsx-1 do s(k) = slopex(k,2)
  for k = 0, nsy-1 do s(nsx+k) = slopey(k,2)
  print,'finding phases'
  a = mati ## s

;  --- convert to units of waves ---
  microns = 1.e-6
  mm = 1.e-3
  dx = 408.*microns
  foclen = 52.*mm
  lambda = .589*microns
  for k = 0, np-1 do phase(k,2) = a(k)*((dx/del)/foclen)*dx/sqrt(3.0)*(1./lambda)
  regular_grid,phase,rp,gs,limits
;show3,rp
  print,'removing pistion, tip, and tilt'
  mask = rp ne 0
  pf = planefit(rp,mask)*mask
  rpp = rp - pf

  return,rpp
end

