; median_image.pro - calculate the median pixel by pixel

function median_image,data3d
  n = size(data3d)
  res = make_array(n(1),n(2))
  for i=0,n(1)-1 do begin
    for j=0,n(2)-1 do begin
      res(i,j) = median(data3d(i,j,*))
    endfor
  endfor
  return,res
end
