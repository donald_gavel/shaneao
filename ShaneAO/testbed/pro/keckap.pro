;+
;  keckap.pro - create the Keck aperture
;
;  USAGE:
;    ap = keckap(n,lambda=l,du=d)
;
;  INPUTS:
;    n - size of image in pixels
;    lambda - wavelength, m
;    du - pixel size in aperture image, m
;    Lseg - length of an edge of a segment, m
;    fs - set to 1 to use the Fourier shift method (takes longer)
;
;  OUTPUTS:
;    ap - image of Keck aperture
;
;-
function KeckSegLocs, L
  n_rings = 3
  n_seg = 3*n_rings*(n_rings+1)
  segLoc = fltarr(n_seg,2)
  nacross = n_rings*2 + 1
  dx = sqrt(3.)*L
  dy = 3.0*L/2.0
  k = 0
  for j=0,n_rings do begin
    imax = (nacross-j)/2
    imin = ((j/2)*2 ne j); isodd
    for i=imin,imax do begin
      x = i*dx
      if imin then x = (x-dx/2)
      y = j*dy
      if (i ne 0) or (j ne 0) then begin
      	segLoc[k,*] = [x,y] & k++
      endif
      if (i ne 0) then begin
      	segLoc[k,*] = [-x,y] & k++
      endif
      if (j ne 0) then begin
      	segLoc[k,*] = [x,-y] & k++
      endif
      if (i ne 0) and (j ne 0) then begin
      	segLoc[k,*] = [-x,-y] & k++
      endif
    endfor
  endfor

  return,segLoc
end

function makeKeckAp, du, L, segment,fs=fs
  sl = keckSegLocs(L)
  n_seg = (size(sl))[1]
  n = (size(segment))[1]
  ans = segment*0
  if (keyword_set(fs)) then begin
    for k = 0,n_seg-1 do begin
      ans += fshift(segment,sl[k,0]/du,sl[k,1]/du)
    endfor
  endif else begin
    for k = 0,n_seg-1 do begin
      x = round(sl[k,0]/du)
  	  y = round(sl[k,1]/du)
  	  ans = ans or shift(segment,x,y)
  	endfor
  endelse
  return,ans
end

function KeckAp,n,lambda=lambda,du=du,Lseg=L,fs=fs
  arcsec = !pi/(180.*3600.)
  if (n_elements(n) eq 0) then n = 256
  microns = 1e-6
  if (n_elements(lambda) eq 0) then lambda = 2.2 * microns
  if (n_elements(du) eq 0) then du = lambda/(n*0.02 * arcsec)
  if (n_elements(L) eq 0) then L = 0.9
  dtele = 10.95 ; meters, longest baseline
  if (n*du lt dtele) then print,'<keckap> caution: aperture larger than image'
  seg = hex(n,L*1.1/du)
  ap = makeKeckAp(du,L,seg,fs=fs)
  return,ap
end

; Reference:
; http://www2.keck.hawaii.edu/observing/kecktelgde/ktelinstupdate.pdf
; see also: KeckAperture.xls
;
Lseg = .9
sl = keckseglocs(Lseg)
n = 512
app = fltarr(n,n)
du = 20./float(n)
for k=0,35 do begin
  x = round(sl[k,0]/du) + n/2
  y = round(sl[k,1]/du) + n/2
  app[x,y] = 1
endfor
ap = KeckAp(n,du=du,Lseg=Lseg)
disp,ap,x0=-[1,1]*(n/2)*du,dx=[1,1]*du
end
