;+
;  dm2.pro - generic dm, rectilinear geometry
;    version2: uses sparse array technique, overlapintegral.pro
;              to speed up basis function calculation
;
;-----------------------------------------------------------------
;  makebasis - create actuator "basis" functions
;  ---------
;  amatrix = makebasis(dmgeo,d,basis=basis,sigma=sigma)
;
;   inputs
;     dmgeo - na x 2 array of actuator positions
;     d - actuator spacing, pixels
;     sigma - optional width of gaussian envelope, pixels, default: d
;     size - optional size of the influence function array, default: 6 sigma
;       (and optionally define the phase screen size to be other than 128 x 128)
;   outputs
;     basis - actuator influence function
;     amatrix - matrix of basis function inner products
;-----------------------------------------------------------------
;  dmfit - fit a wavefront to dm actuator pushes
;  -----
;    actvec = dmfit(wf,dmgeo,basis,ainv,/invert,ap=ap)
;   inputs:
;     wf - wavefront phase data
;     dmgeo - na x 2 array of actuator positions
;     basis - actuator influence function from makebasis
;     ainv - "inverse" of a
;          here is sample inversion code:
;              eval = eigenql(a,eigenvectors=ev)
;              evali = (1/(eval>thresh))*(eval gt thresh)
;              ainv = transpose(ev)##diagonal(evali)##ev
;     invert - whether or not to invert "ainv" (=a); (no threshold)
;
;   output:
;     actvec - a vector of actuator commands, in the same units as wf
;
;   note:
;
;     to best-fit the wavefront over an aperture, premultiply wf by ap
;
;-----------------------------------------------------------------
;  pushact - create a phase screen according to dm actuator pushes
;  -------
;    phase = pushact(act,dmgeo,basis)
;
;   inputs
;      act - na element vector of actuator commands
;      dmgeo - na x 2 array of actuator positions
;      basis - actuator influence function, from makebasis
;   output
;      phase - resulting phase screen
;-
function makebasis,dmgeo,d,basis=f,sigma=sigmaarg, size=n
;   (sin(pi x)/(pi x)) exp(-(1/2)(x/sig)^2)
  na = (size(dmgeo))[1]
  if n_elements(sigmaarg) eq 0 then begin
    print,'computing sigma internally'
    sigma = d
  endif else begin
    sigma = sigmaarg
  endelse
  if n_elements(n) eq 0 then begin
    n = 6*sigma
  endif
  if ((n/2)*2 ne n) then n=n+1 ; make n even
  x = (findgen(n)-n/2) # ones(n)
  x[n/2,*] = .1
  y = ones(n) # (findgen(n)-n/2)
  y[*,n/2] = .1
  fx = (sin(!pi*x/d)/(!pi*x/d))
  fx[n/2,*] = 1.
  fy = (sin(!pi*y/d)/(!pi*y/d))
  fy[*,n/2] = 1.
  f = fx*fy
  f = f*gauss2(n,n/2,n/2,sigma) ; normalized to 1 at the peak
  a = fltarr(na,na)
  for i=0,na-1 do begin
    print,format='($,I0," ")',i
    c1 = [dmgeo[i,0],dmgeo[i,1]]
    for j=0,na-1 do begin
      c2 = [dmgeo[j,0],dmgeo[j,1]]
      a[i,j] = overlapintegral(f,f,c1,c2)
    endfor
  endfor
  print,'done'
  return, a      
end

function showdm,dmgeo,ap
  dm = ap*0
  n = (size(dm))[1]
  na = (size(dmgeo))[1]
  for i=0,na-1 do begin
    xi = dmgeo[i,0]
    yi = dmgeo[i,1]
    dm[xi+n/2,yi+n/2] = 1
  endfor
  return,dm
end

function dmfit,wf,dmgeo,basis,aarg,invert=invert,greens=greens
  if keyword_set(invert) then begin
    eval = eigenql(aarg,eigenvectors=ev)
    ainv = transpose(ev)##diagonal(1/eval)##ev
  endif else begin
    if (n_elements(aarg) ne 0) then begin
      ainv = aarg
    endif
  endelse
  na = (size(dmgeo))[1]
  b = fltarr(na)
  for i=0,na-1 do begin
    c = [dmgeo[i,0],dmgeo[i,1]]
    b[i] = overlapintegral(wf,basis,[0,0],c)
  endfor
  if keyword_set(greens) then return,b
  act = ainv##b
  return,transpose(act)
end

function pushact,act,dmgeo,basis,dm0
  ndm = (size(dm0))[1]
  n = (size(basis))[1]
  na = (size(dmgeo))[1]
  nat = (size(act))[1]
  if nat ne na then begin
    print,'<pushact> number of actuators ',nat,' does not match number of basis fucntions ',na
    return,0
  endif
  dm = fltarr(ndm,ndm)
  for i=0,na-1 do begin
    c = fix([dmgeo[i,0],dmgeo[i,1]])
    x1b = 0
    x2b = n-1
    y1b = 0
    y2b = n-1
    x1 = c[0]-n/2+ndm/2
    x2 = c[0]+n/2+ndm/2-1
    y1 = c[1]-n/2+ndm/2
    y2 = c[1]+n/2+ndm/2-1
    if (x1 lt 0) then begin
      x1b = -x1
      x1 = 0
    endif
    if (x2 ge ndm) then begin
      x2b = n-(x2-ndm)-1
      x2 = ndm-1
    endif
    if (y1 lt 0) then begin
      y1b = -y1
      y1 = 0
    endif
    if (y2 ge ndm) then begin
      y2b = n-(y2-ndm)-1
      y2 = ndm-1
    endif
    if ((x2 ge x1) and (y2 ge y1)) then begin
      dm[x1:x2,y1:y2] = dm[x1:x2,y1:y2] + act[i]*basis[x1b:x2b,y1b:y2b]
    endif
  endfor
  return,dm
end
