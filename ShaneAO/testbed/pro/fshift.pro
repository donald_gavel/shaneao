;+
;  fshift - Fractional, or Fourier, shift
;  usage:
;    as = fshift(a,sx,sy)
;  inputs:
;    a - array (preferrably power of two in size)
;    sx - fractional pixel shift in x
;    sy - fractional pixel shift in y
;  output:
;    as - shifted image
;-
function fshift,a,sx,sy
	n = (size(a))[1]
	m = (size(a))[2]
  fa = ft(a)
  i = complex(0,1.)
  dfx = 1./float(n)
  dfy = 1./float(m)
  fx = ((findgen(n)-n/2) # ones(m))*dfx
  fy = (ones(n) # (findgen(m)-m/2))*dfy
  fa *= exp(-2*!pi*i*(sx*fx+sy*fy))
  as = ft(fa,/inverse)
  type = typeof(a)
  if ((type ne 6) and (type ne 9)) then as = real(as)
  return,as
end

; test of operation
; non square and non power of 2 sized images
n=100
m = 2*n
a = fltarr(n,m)
a[n/2,m/2]=1
;a = gauss2(n,n/2,n/2,10)
;a = zeropad(a,n,m)
sx = 2.5
sy = 1.5
as = fshift(a,sx,sy)
end
