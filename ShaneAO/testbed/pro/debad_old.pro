;
;  remove the bad pixels from a datacube
;
pro debad,incube,outcube,badpix
  if n_params() eq 0 then begin
    print,"usage: debad,incube,outcube,badpix"
    return
  end
  s = size(incube)
  n = s(3)
  if n_elements(outcube) eq 0 then outcube = make_array(256,256,n);
  goodpix = 1-badpix
  print,"removing bad pixels..."
  for i=0,n-1 do begin
    outcube(*,*,i) = incube(*,*,i)*goodpix + median(incube(*,*,i),5)*badpix
    print,format='($,I0," ")',i
  endfor
  print,"done"
  return
end
