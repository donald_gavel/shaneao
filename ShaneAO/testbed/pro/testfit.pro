;
; test piston effect on wls solution to fitting
;
;  y = R a
;

n = 100 ; size of y
m = 20 ; size of a
s = float(n)/float(m)
sd = s*10.

R = fltarr(n,m)
Rpr = R
C = fltarr(m,m)
Cpr = C

; create influence functions
for i = 0,m-1 do begin
  R[*,i] = gauss1(n,i*s,sd)
  Rpr[*,i] = R[*,i] - total(R[*,i])/float(n)
endfor

; create the cross-coupling matrix
for i = 0,m-1 do begin
  for j = 0, m-1 do begin
    C[i,j] = total(R[*,i]*R[*,j])
    Cpr[i,j] = total(Rpr[*,i]*Rpr[*,j])
  endfor
endfor

; create the reconstructor
CI = invert(C,status,/double)
CI = pseudoinverse(C,0.15) & status = 0

CprI = invert(Cpr,statuspr,/double)
CprI = pseudoinverse(Cpr,0.15) & statuspr = 0

; generate a function and remove the piston part
;seed = 5
if (status eq 0 and statuspr eq 0) then begin
for trial = 1,20 do begin

y = smooth(randomn(seed,n),10)
y -= total(y)/float(n)

; compute the fitted function

a = CI ## R ## y
yhat = reform(a) ## R

apr = CprI ## Rpr ## y
yhatpr = reform(apr) ## Rpr

; compute the error
e = y-yhat
epr = y-yhatpr
print,'error = ',rms(e),rms(epr),' diff = ',rms(e)-rms(epr),' rms(a,apr) =',rms(a),rms(apr) 

range = [min([y,yhat,yhatpr]),max([y,yhat,yhatpr])]
plot,y,yrange=range
oplot,yhatpr
oplot,yhat,color=150

wait,.1
endfor
endif else begin
  print,'singular matrix'
endelse
end
