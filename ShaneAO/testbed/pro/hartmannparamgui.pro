;
; Auto Save File For ./hartmannparamgui.pro
;
;  Fri Sep 12 00:00:33 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN1

function getvals,fieldlist
  me = {rows:14,cols:13,corners:[130,38,488,400], $
        thresholds:[0.7,0.75],roi:[20,20]}
  widget_control, fieldlist(0), get_value=val
  me.rows = val
  widget_control, fieldlist(1), get_value=val
  me.cols = val
  widget_control, fieldlist(2), get_value=val
  me.corners(0) = val
  widget_control, fieldlist(3), get_value=val
  me.corners(1) = val
  widget_control, fieldlist(4), get_value=val
  me.corners(2) = val
  widget_control, fieldlist(5), get_value=val
  me.corners(3) = val
  widget_control, fieldlist(6), get_value=val
  me.thresholds(0) = val
  widget_control, fieldlist(7), get_value=val
  me.thresholds(1) = val
  widget_control, fieldlist(8), get_value=val
  me.roi(0) = val
  widget_control, fieldlist(9), get_value=val
  me.roi(1) = val

  return,me
end

pro putvals,fieldlist,me
  widget_control, fieldlist(0), set_value = me.rows
  widget_control, fieldlist(1), set_value = me.cols
  widget_control, fieldlist(2), set_value = me.corners(0)
  widget_control, fieldlist(3), set_value = me.corners(1)
  widget_control, fieldlist(4), set_value = me.corners(2)
  widget_control, fieldlist(5), set_value = me.corners(3)
  widget_control, fieldlist(6), set_value = me.thresholds(0)
  widget_control, fieldlist(7), set_value = me.thresholds(1)
  widget_control, fieldlist(8), set_value = me.roi(0)
  widget_control, fieldlist(9), set_value = me.roi(1)
end


PRO MAIN1_Event, Event

  common hartmannparamguicom, me, changed

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  t = size(Ev)
  if (t(0) eq 0) then evname = Ev else evname = Ev.name

;  CASE Ev OF 
  CASE evname OF 

  'FIELD10': BEGIN
      Print, 'Event for #rows'
      END
  'FIELD11': BEGIN
      Print, 'Event for #cols'
      END
  'FIELD24': BEGIN
      Print, 'Event for x:'
      END
  'FIELD25': BEGIN
      Print, 'Event for y:'
      END
  'FIELD36': BEGIN
      Print, 'Event for x:'
      END
  'FIELD37': BEGIN
      Print, 'Event for y:'
      END
  'FIELD48': BEGIN
      Print, 'Event for Lo:'
      END
  'FIELD49': BEGIN
      Print, 'Event for hi:'
      END
  'FIELD54': BEGIN
      Print, 'Event for x:'
      END
  'FIELD55': BEGIN
      Print, 'Event for y:'
      END
  'BUTTON71': BEGIN
      Print, 'Event for Revert to Default'

      base = widget_info(/parent, Event.Id)
      donebutton = widget_info(/sibling,widget_info(/sibling,base))
      widget_control, base, get_uvalue = uv
      paramfilename = uv.filename
      if (paramfilename ne '') then begin
        res = paramin(paramfilename,me)
        widget_control, donebutton, get_uvalue=uv
        fieldlist = uv.fields
        putvals,fieldlist,me
      endif
      END
  'BUTTON72': BEGIN
      Print, 'Event for Set as Default'

      base = widget_info(/parent, Event.Id)
      donebutton = widget_info(/sibling,widget_info(/sibling,base))
      widget_control, base, get_uvalue = uv
      paramfilename = uv.filename
      widget_control, donebutton, get_uvalue = uv
      me = getvals(uv.fields)
      if (paramfilename ne '') then begin
        paramout,paramfilename,me
      endif
      END
  'BUTTON85': BEGIN
      Print, 'Event for Cancel'
      widget_control, event.top, /destroy
      changed = 0     
      END
  'BUTTON84': BEGIN
      Print, 'Event for Done'

      fieldlist = Ev.fields
      me = Ev.vals
      me = getvals(fieldlist)
      changed = 1
      widget_control, event.top, /destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN1
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



function hartmannparamgui, me_arg, GROUP=Group, label=label, file=filename, changeflag=ischanged

  common hartmannparamguicom, me, changed

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0
  if n_elements(label) eq 0 then label='untitled'
  if n_elements(me_arg) eq 0 then $
    me = {rows:14,cols:13,corners:[130,38,488,400], $
              thresholds:[0.7,0.75],roi:[20,20]} $
  else me = me_arg
  if n_elements(filename) eq 0 then filename = ''

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN1 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='MAIN1')

  LABEL3 = WIDGET_LABEL( MAIN1, $
      FONT='-adobe-times-medium-i-normal--18-180-75-75-p-94-iso8859-1', $
      UVALUE='LABEL3', $
      VALUE=label)
;      VALUE='Reference Wavefront')

  BASE4 = WIDGET_BASE(MAIN1, $
      ROW=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='row & column count', $
      UVALUE='BASE4')

  FieldVal11373 = [ $
    '14' ]
;  FIELD10 = CW_FIELD( BASE4,VALUE=FieldVal11373, $
  FIELD10 = CW_FIELD( BASE4,VALUE=me.rows, $
      ROW=1, $
      INTEGER=1, $
      TITLE='Number of Rows:', $
      UVALUE='FIELD10', $
      XSIZE=5)

  FieldVal11375 = [ $
    '13' ]
;  FIELD11 = CW_FIELD( BASE4,VALUE=FieldVal11375, $
  FIELD11 = CW_FIELD( BASE4,VALUE=me.cols, $
      ROW=1, $
      INTEGER=1, $
      TITLE='Number of Columns', $
      UVALUE='FIELD11', $
      XSIZE=5)


  BASE5 = WIDGET_BASE(MAIN1, $
      COLUMN=1, $
      YPAD=10, $
      FRAME=2, $
      MAP=1, $
      TITLE='corners', $
      UVALUE='BASE5')

  LABEL17 = WIDGET_LABEL( BASE5, $
      UVALUE='LABEL17', $
      VALUE='Corners')

  BASE18 = WIDGET_BASE(BASE5, $
      ROW=1, $
      SPACE=30, $
      XPAD=10, $
      MAP=1, $
      TITLE='LL and UL corners', $
      UVALUE='BASE18')

  BASE21 = WIDGET_BASE(BASE18, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='Lower Left', $
      UVALUE='BASE21')

  LABEL22 = WIDGET_LABEL( BASE21, $
      UVALUE='LABEL22', $
      VALUE='Lower Left')

  BASE23 = WIDGET_BASE(BASE21, $
      ROW=1, $
      MAP=1, $
      TITLE='X and Y', $
      UVALUE='BASE23')

  FieldVal11383 = [ $
    '130' ]
;  FIELD24 = CW_FIELD( BASE23,VALUE=FieldVal11383, $
  FIELD24 = CW_FIELD( BASE23,VALUE=me.corners(0), $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD24', $
      XSIZE=4)

  FieldVal11385 = [ $
    '38' ]
;  FIELD25 = CW_FIELD( BASE23,VALUE=FieldVal11385, $
  FIELD25 = CW_FIELD( BASE23,VALUE=me.corners(1), $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD25', $
      XSIZE=4)



  BASE38 = WIDGET_BASE(BASE18, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='Upper Right', $
      UVALUE='BASE38')

  LABEL35 = WIDGET_LABEL( BASE38, $
      UVALUE='LABEL35', $
      VALUE='Upper Right')

  BASE23a = WIDGET_BASE(BASE38, $
      ROW=1, $
      MAP=1, $
      TITLE='X and Y', $
      UVALUE='BASE23a')

  FieldVal11390 = [ $
    '488' ]
;  FIELD36 = CW_FIELD( BASE23,VALUE=FieldVal11390, $
  FIELD36 = CW_FIELD( BASE23a,VALUE=me.corners(2), $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD36', $
      XSIZE=4)

  FieldVal11392 = [ $
    '400' ]
;  FIELD37 = CW_FIELD( BASE23,VALUE=FieldVal11392, $
  FIELD37 = CW_FIELD( BASE23a,VALUE=me.corners(3), $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD37', $
      XSIZE=4)

  BASE46 = WIDGET_BASE(MAIN1, $
      ROW=1, $
      SPACE=20, $
      XPAD=10, $
      MAP=1, $
      TITLE='thresholds and roi', $
      UVALUE='BASE46')

  BASE50 = WIDGET_BASE(BASE46, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='thresholds', $
      UVALUE='BASE50')

  LABEL47 = WIDGET_LABEL( BASE50, $
      UVALUE='LABEL47', $
      VALUE='Thresholds')

  BASE20 = WIDGET_BASE(BASE50, $
      ROW=1, $
      MAP=1, $
      TITLE='thresholds', $
      UVALUE='BASE20')

  FieldVal11398 = [ $
    '0.7' ]
;  FIELD48 = CW_FIELD( BASE20,VALUE=FieldVal11398, $
  FIELD48 = CW_FIELD( BASE20,VALUE=me.thresholds(0), $
      ROW=1, $
      FLOAT=1, $
      TITLE='Lo:', $
      UVALUE='FIELD48', $
      XSIZE=4)

  FieldVal11400 = [ $
    '0.75' ]
;  FIELD49 = CW_FIELD( BASE20,VALUE=FieldVal11400, $
  FIELD49 = CW_FIELD( BASE20,VALUE=me.thresholds(1), $
      ROW=1, $
      FLOAT=1, $
      TITLE='hi:', $
      UVALUE='FIELD49', $
      XSIZE=4)

  BASE51 = WIDGET_BASE(BASE46, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='roi', $
      UVALUE='BASE51')

  LABEL52 = WIDGET_LABEL( BASE51, $
      UVALUE='LABEL52', $
      VALUE='roi size')

  BASE53 = WIDGET_BASE(BASE51, $
      ROW=1, $
      MAP=1, $
      UVALUE='BASE53')

  FieldVal11405 = [ $
    '20' ]
;  FIELD54 = CW_FIELD( BASE53,VALUE=FieldVal11405, $
  FIELD54 = CW_FIELD( BASE53,VALUE=me.roi(0), $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD54', $
      XSIZE=5)

  FieldVal11407 = [ $
    '20' ]
;  FIELD55 = CW_FIELD( BASE53,VALUE=FieldVal11407, $
  FIELD55 = CW_FIELD( BASE53,VALUE=me.roi(1), $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD55', $
      XSIZE=5)




  BASE69 = WIDGET_BASE(MAIN1, $
      ROW=1, $
      SPACE=50, $
      XPAD=50, $
      MAP=1, $
      TITLE='2 bottom buttons', $
      UVALUE={name:'BASE69',filename:filename})

  BUTTON71 = WIDGET_BUTTON( BASE69, $
      UVALUE='BUTTON71', $
      VALUE='Revert to Default', $
      XSIZE=120)

  BUTTON72 = WIDGET_BUTTON( BASE69, $
      UVALUE='BUTTON72', $
      VALUE='Set as Default', $
      XSIZE=100)

  fieldlist = [field10, field11, field24, field25, field36, $
               field37, field48, field49, field54, field55]

  BUTTON85 = WIDGET_BUTTON( MAIN1, $
      UVALUE='BUTTON85', $
      VALUE='Cancel')

  BUTTON84 = WIDGET_BUTTON( MAIN1, $
      UVALUE={name:'BUTTON84',fields:fieldlist, vals: me}, $
      VALUE='Done')

  WIDGET_CONTROL, MAIN1, /REALIZE

  XMANAGER, 'MAIN1', MAIN1, /modal
  me_arg = me
  ischanged = changed
  return,me

END

