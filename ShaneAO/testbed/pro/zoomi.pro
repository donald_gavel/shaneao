;+
;    zoomi - zoom in on an image
;
;   USAGE:
;      z = zoomi(image,factor)
;
;   INPUTS:
;      image - input image
;      factor - zoom factor, must divide the size of the image evenly
;-
function zoomi,image,factor
  n = (size(image))(1)
  m = (size(image))(2)
  n1 = n/2 - (n/factor)/2
  n2 = n1 + n/factor - 1
  m1 = m/2 - (m/factor)/2
  m2 = m1 + m/factor - 1
  z = congrid(image(n1:n2,m1:m2),n,m)
;  z = rebin(image(n1:n2,m1:m2),n,m,/sample)
  return,z
end
