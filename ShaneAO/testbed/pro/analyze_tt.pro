dir = '/u/user/observers/gavel/data/080325/'
dir = '/u/user/observers/gavel/data/080915/'
dir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/080915/'

cd, dir

file = 'tt_003'
tt = rdview(file)
nt = 1024
fs = 1000	;sampling

corr = 1.e6
cx = reform(tt[0,*], nt)
cy = reform(tt[1,*], nt)

ox = reform(tt[2,*],nt)
oy = reform(tt[3,*],nt)


ox -= mean(ox)
oy -= mean(oy)
cx -= mean(cx)
cy -= mean(cy)

oxft = (abs(fft(ox)))^2
oyft = (abs(fft(oy)))^2
cxft = (abs(fft(cx)))^2
cyft = (abs(fft(cy)))^2


oxft*=corr
cxft*=corr

window,/free
powerSpectrumPlot,oxft,fs,/xlog,/ylog,title=file
;window,/free
;powerSpectrumPlot,oyft,fs,/xlog,/ylog

end
