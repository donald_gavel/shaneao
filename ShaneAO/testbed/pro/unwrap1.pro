; unwrap1.pro
;   1-D phase unwrapper
function unwrap1,data
  n = (size(data))[1]
  del = data-shift(data,1)
  del[0] = 0
  ind = where(del gt !pi)
  if ((size(ind))[0] ne 0) then del[ind] = del[ind] - 2*!pi
  ind = where(del lt (-!pi))
  if ((size(ind))[0] ne 0) then del[ind] = del[ind] + 2*!pi
  rdata = data[0] + integral(del)
  return,rdata
end
