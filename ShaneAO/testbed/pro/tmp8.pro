;
;  tmp8.pro - phase diversity
;
;  a2s = data

;          parameters
n = 256; 	 size of data
dp = 3.; 	 diameter of primary, meters
ds = 0.8;	 diameter of secondary, meters
fnum = 28.6;	 f/number into scoring camera
lambda = 0.635e-6; wavelength
arcsec = !pi/(180.*3600.);
dth = 0.016*arcsec; pixel size, arcseconds
dx = dth*dp*fnum;	 pixel size, meters
outsideFocusDistance = .012;	  distance of camera from focus, meters
;
f = dp*fnum;	 focal length
du = f*lambda/(n*dx); sample size on aperture
rp = dp/2.;	 radius of primary
;rs = ds/2.;	 radius of secondary
rs = .5
ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
;ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,133,n/2,rs/du,1)
foc = ap*real(zernike(n,n,rp/du,2,0))
foc = foc/rms(foc,ap)
i = complex(0,1)
;       gather data from hither and thither ...
dir = '/duck3/gavel/lickdataJun99/Jul0199/'
a = rdview(dir+'pm_00')
;       pre-process the data
a2 = a(326-256:325,326-256:325);  extract nxn region
a2s = shift(a2,128-154,128-131);  shift image to center 
a2d = (a2s-109)>0;  		  dark-subtract
;
nph = total(a2d);  		  calculate total light energy
xmag = sqrt(a2d)
outsideFocusWaves = outsideFocusDistance / (4*sqrt(3)*4*fnum^2*lambda)
;outsideFocusWaves = 1.04
focusKernal = exp(i*foc*2*!pi*outsideFocusWaves)
umag = sqrt((ap/total(ap))*nph)
ur = umag
;         --- phase diversity iteration ---
x = ft(ur*focusKernal)*float(n);  forward propagate
xr = xmag*exp(i*angle(x));  replace magnitude with data
u = (ft(xr,/inverse)/focusKernal)/float(n);  reverse propagate
ur = umag*exp(i*angle(u)); replace magnitude with fixed illumination
;

