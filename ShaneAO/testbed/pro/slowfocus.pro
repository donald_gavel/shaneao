;+
;  slowfocus.pro -
;-
function slowfocus,a,a0,c,badpix,thresh,baseroi=roi0,theroi=roi, cor=cor
  fixpix,a,badpix,afix
  fixpix,a0,badpix,a0fix
  x = 0
  y = 1
  n = 64
;  extract subaps from baseline (a0) image
  roi0 = fltarr(n,n,4)
  for i=0,3 do $
    roi0[*,*,i] = a0fix[c[i,x]-n/2:c[i,x]+n/2-1,c[i,y]-n/2:c[i,y]+n/2-1]
;
;  anulus background subtraction
rb = [3*n/8,n/2]
  anulus = circle(n,n,n/2,n/2,rb[1],1.)-circle(n,n,n/2,n/2,rb[0],1.)
  ancnt = total(anulus)
  for i=0,3 do $
    roi0[*,*,i] = roi0[*,*,i] - planefit(roi0[*,*,i],anulus)
;  now extract subaps from the data (a) image
roi = fltarr(n,n,4)
for i=0,3 do $
  roi[*,*,i] = afix[c[i,x]-n/2:c[i,x]+n/2-1,c[i,y]-n/2:c[i,y]+n/2-1]
;
;  anulus background subtraction
rb = [3*n/8,n/2]
anulus = circle(n,n,n/2,n/2,rb[1],1.)-circle(n,n,n/2,n/2,rb[0],1.)
ancnt = total(anulus)
for i=0,3 do $
  roi[*,*,i] = roi[*,*,i] - planefit(roi[*,*,i],anulus)
;
;  calculate positions of centroids using the correlation method
cent = fltarr(2,4)
centf = fltarr(2,4)
cor = fltarr(n,n,4)
apa = fltarr(n,n,4)
for i=0,3 do begin $
  u = real(ftcorrelate(roi[*,*,i],roi0[*,*,i])) & $
  u = median(u,3)
  dum = max(u) & $
  cent[*,i] = coords(u,!c) & $
  ap = circle(n,n,cent[0,i],cent[1,i],5,1.) & $
  cent[*,i] = cent[*,i] - [n/2,n/2] & $
  centf[*,i] = centroid(u*ap) - [n/2,n/2] & $
  cor[*,*,i] = u & $
  apa[*,*,i] = ap & $
endfor
; analyze the tip, tilt, focus, and astigmatism components
tip = total(cent[0,*])/4.
tilt = total(cent[1,*])/4.
zfocus = float([[-1,1],[1,1],[-1,-1],[1,-1]])
focus = total(zfocus*cent)/total(zfocus*zfocus)
zastig = float([[-1,1],[-1,-1],[1,1],[1,-1]])
astig = total(zastig*cent)/total(zastig*zastig)
print,'tip,tilt,focus,astig: ',tip,tilt,focus,astig
focusmm = focus/0.181818
focuscounts = -focusmm*5000
print,'move the WFS stage'
print,focuscounts,' counts'
print,focuscounts/5000.,' mm'
return,focuscounts
end
;
function utdate
  theMonths=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  caldat,systime(1,/julian)+8./24.,imonth,iday,iyear,hour,minute,second
  month = theMonths[imonth-1]
  day = strtrim(string(iday),2)
  year = strtrim(string(iyear),2)
  return,month+day+'_'+year+'ut'
end
;
;  Write a new calibration baseline to disk
;    Creates a new directory with today's date, if necessary.
;    - a0 is an optional OUTPUT variable set equal to float(imgg)
;    - imgg is the latest slow WFS image; it is what is written as
;      the calibration reference.
;    - You are given the chance to cancel the write.  If you name
;      the file anything other than backwfs001.fits it may not work
;      later in the focus measurement operations.
pro newbaseline,a0
  common sensys,imgg,ncg,nrg
  common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
  time = systime(0)
  month = strmid(time,4,3)
  day = strmid(time,8,2)
  year = strmid(time,22,2)
  date0 = month+day+'_'+year
  date0 = utdate()
  dir = 'e:\LaserGuideStar\BackWFSdata\'
  path = dir+date0
  result = findfile(path)
  if (result[0] eq '') then begin
    print,'creating directory '+path
    cmd = 'md '+path
    spawn,cmd
    result = findfile(path)
    if (result[0] eq '') then begin
      print,'cant create directory '+path
      res = dialog_message('cant create directory'+path)
      return
    endif
  endif
  filename = dialog_pickfile(path=path,file='backwfs001.fits',/write)
  if (filename ne '') then begin
    print,'saving to file '+filename
    a0 = float(imgg)
    writefits,filename,a0
  endif else begin
    print,'file not saved'
  endelse
  return
end
;
;  get an old baseline calibration file
;
function oldbaseline
  common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
  dir = 'e:\LaserGuideStar\BackWFSdata\'
  filename = dialog_pickfile(path=dir,/read,get_path=getPath)
  if (filename ne '') then begin
    path = getPath
    k = strlen(path)
    path = strmid(path,0,k-1)
    a0 = readfits(filename)
    msg = ['You may need to set new roi centers.', $
           'To do so, dispgui the baseline image, then', $
           'use setrois /pick']
    r = dialog_message(msg,/information)
  endif else begin
    a0 = 0
  endelse
  return,a0
end
;
function pickrois
  common sensys,imgg,ncg,nrg
  msg = ['Select the 4 Hartmann spot centers', $
         'by clicking the', 'RIGHT', 'mouse button in this order:', $
         '     0     1','     2     3']
  for i=0,5 do print,msg[i]
  r = dialog_message(msg,/information)
  c = fltarr(4,2)
  for i=0,3 do begin
    tvvalue1,float(imgg),x,y
    c[i,*] = [x,y]
  endfor
  return,c
end
;
;  Set the regions of interest - Look at the calibration shot
;       and determine the roi centers.  Edit this procedure and recompile,
;       then run this procedure to load the roicenters into a common block
;     - This step should be performed after doing a calibration shot but
;       before doing the first analysis
pro setrois,pick=pick
  common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
  if keyword_set(pick) then begin
    c = pickrois()
    roicenters = c
    return
  endif
  c = fltarr(4,2)
; May, 2002:
; //////////////// Edit Here /////////////////////////
  c[0,*] = [356,286]; upper left Hartmann spot
  c[1,*] = [484,286]; upper right
  c[2,*] = [355,161]; lower left
  c[3,*] = [484,160]; lower right
; ////////////////////////////////////////////////////
  roicenters = c
end
;
;  Analysis - compares present file with the baseline calibration file
;             saved earlier, then prints out the suggested fast wfs z motion.
pro anal,baseroi=roi0,theroi=roi,cor=cor
  common sensys,imgg,ncg,nrg
  common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
  a = float(imgg)
  if (n_elements(path) eq 0) then begin
    msg = ['You will need to pick a baseline file.', 'Click OK to continue.']
    r = dialog_message(msg,/information)
    a0 = oldbaseline()
  endif
  a0 = readfits(path+'\backwfs001.fits')
  badpix = readfits(dir+'badpix.fits')
  thresh = 80.
  if (n_elements(roicenters) eq 0) then setrois
  counts = slowfocus(a,a0,roicenters,badpix,thresh,baseroi=roi0,theroi=roi,cor=cor)
end
;
;   SaveData - will save the latest image to disk (you name the file)
;
pro savedata
  common sensys,imgg,ncg,nrg
  common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
  if (n_elements(path) eq 0) then begin
    time = systime(0)
    month = strmid(time,4,3)
    day = strmid(time,8,2)
    year = strmid(time,22,2)
    date0 = month+day+'_'+year
    date0 = utdate()
    dir = 'e:\LaserGuideStar\BackWFSdata\'
    path = dir+date0
    result = findfile(path)
    if (result[0] eq '') then begin
      print,'creating directory '+path
      cmd = 'md '+path
      spawn,cmd
      result = findfile(path)
      if (result[0] eq '') then begin
        print,'cant create directory '+path
        res = dialog_message('cant create directory'+path)
        return
      endif
    endif
  endif
  if (n_elements(datasetindex) eq 0) then datasetindex = 2
  indexstring = '000'
  t = strtrim(string(datasetindex),2)
  strput,indexstring,t,3-strlen(t)
  a = float(imgg)
  filename = dialog_pickfile(path=path,file='backwfs'+indexstring+'.fits',/write)
  if (filename ne '') then begin
    print,'saving to file '+filename
    writefits,filename,a
    datasetindex = datasetindex + 1
  endif else begin
    print,'file not saved'
  endelse
  return
end
;
common slowFocusSensorCommon,date0,dir,path,datasetindex,roicenters
;date0 = 'May27_02\'
;dir = "e:\LaserGuideStar\BackWFSdata\"
;a = float(imgg)
;a0 = readfits(dir+date0+'backwfs001.fits')
;badpix = readfits(dir+'badpix.fits')
;c = fltarr(4,2)
; centroid index:
;    0   1
;    2   3
;
;  approximate center locations in baseline.fits
; Nov, 2001:
;c[0,*] = [353,286]
;c[1,*] = [474,286]
;c[2,*] = [353,161]
;c[3,*] = [471,165]
; March, 2002:
;c[0,*] = [347,291]
;c[1,*] = [466,291]
;c[2,*] = [345,172]
;c[3,*] = [467,170]
; May, 2002:
;c[0,*] = [356,286]; upper left Hartmann spot
;c[1,*] = [484,286]; upper right
;c[2,*] = [355,161]; lower left
;c[3,*] = [484,160]; lower right
;roicenters = c
;
;thresh = 80.
;print,slowfocus(a,a0,c,badpix,thresh,baseroi=roi0,theroi=roi,cor=cor)
end
