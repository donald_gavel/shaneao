;
; Test code for tomography wavefront error covariance
;
; Based on:
;   y = Ax     wavefront sensors
;   x_hat = Ky  estimate of turbulent volume
;   y_s = A_s x   science wavefront
;   e = ys-As_hat   error in science wavefront
;   K = PA'[APA'+R]^-1  optimal tomography reconstructor (R is the covariance of the wavefront measurement noise)
;   <ee'> = A_s <x_tilde x_tilde'> A_s' covariance of science wavefront error
;   <x_tilde x_tilde'> = (I-KA)P  a-posteriori covariance of volume estimate
;   
;   g = number of guidestars
;   l = number of layers in volume
;   k = spatial frequency in cycles/m
;
;   This is all done in the frequency domain and assumes infinitely distant guidestars and infinite aperture
;   A is gxl
;   K is lxg
;   P is lxl
;   R is gxg
;
degrees = !pi/180.
arcsec = degrees/3600.
km = 1.e3
percent = 0.01
i = complex(0.,1.)

g = 3
guide_star_radius = 50.*arcsec
eps = !pi/30.
theta = guide_star_radius*[[sin(0.+eps),cos(0.+eps)], $
          [sin(2*!pi/3.+eps), cos(2*!pi/3.+eps)], $
          [sin(4*!pi/3.+eps), cos(4*!pi/3.+eps)]]  ; NGAO 3+1 constellation
l = 5
h = [0, 2., 5., 9., 13.]*km
Cn2 = [40, 20, 20., 10., 10.]*percent
n = 128 ; number of spatial frequencies
n_samples_across = 20. ; woofer dm (for now)
D_tele = 10. ; Keck telescope
k_max = (D_tele/n_samples_across)/2. ; highest spatial frequency cy/m
dk = k_max/float(n/2) ; lowest spatial frequency
kx = ones(n) ## (dindgen(n)-n/2)*dk ; spatial frequency samples
ky = transpose(kx)

k0 = dk/3.
kolmog = dk^2*(kx^2 + ky^2 + k0^2)^(-11./6.)

thetaxh = theta[0,*] ## h
thetayh = theta[1,*] ## h
R = identity(g)*1.d-12;1.d-5 ; for now: noise covariance is flat across spatial frequencies
As = ones(l) ; science target is on center of field of view

Cy_save = complexarr(n,n)
Cy0_save = complexarr(n,n)
for ix = 0,n-1 do begin
  for iy = 0,n-1 do begin
    A = exp(i*2*!pi*( kx[ix,iy]*thetaxh + ky[ix,iy]*thetayh ))
    At = Conj(Transpose(A))
    P = kolmog[ix,iy]*diagonal(Cn2)
    AP = A ## P
    APAt = AP ## At + R
    K = transpose(conj(la_least_squares(APAt,AP,/double,method=2)))
    Cx = (identity(l)-K ## A) ## P ## conj(transpose(identity(l)-K ## A)) + K ## R ## conj(transpose(K))
    Cy = As ## Cx ## Conj(Transpose(As))
    Cy0 = As ## P ## Conj(Transpose(As))
    Cy_save[ix,iy] = Cy[0]
    Cy0_save[ix,iy] = Cy0[0]
  endfor
print,'ix = ',ix,' of ',n & wait,.01
endfor
print,'done'

; diagnostic
disp,real(cy_save)/real(cy0_save)
;print,(theta ## k) ## h
;print,reform(arg(a[n-1,n-1,*,*]))/(2*!pi)
;disp,reform(arg(a),n,n,g*l)

end
