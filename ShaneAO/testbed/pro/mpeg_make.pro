;
;  mpeg_make - make an mpeg file from a data cube
;
;   This uses window 0 to display the data one frame
;   at a time and record to an mpeg file.  The window
;   display enables color and gamma corrections to be
;   picked up in the mpeg recording.
;
;  inputs:
;    cube - the data cube
;    filename - string name of file to record to (*.mpg)
;    slow - a factor by which to slow down the playback
;           by recording each frame multiple times
;    rescale - option to rescale the intensity scale on
;         each frame.  The default is to not rescale
;
pro mpeg_make,cube,filename,slow=slow,rescale=rescale
  s = size(cube)
  ndim = s[0]
  n = s[1]
  m = s[2]
  window,0,xsize=n,ysize=m
  mm = minmax(cube)
  mpegid = mpeg_open([n,m])
  if (n_elements(slow) eq 0) then slow = 1
  nf = s[3]*slow
  for i = 0,nf-1 do begin
    j = i/slow
    data = reverse(cube[*,*,j],2)
    if keyword_set(rescale) then mm = minmax(data)
    bdata = bytscl(data,min=mm[0],max=mm[1])
    tv,bdata
;    mpeg_put,mpegid,image = bytscl(data,min=mm[0],max=mm[1]), frame = i,/color
    mpeg_put,mpegid,window=0, frame = i,/color
    print,j,i
  endfor
  mpeg_save,mpegid,filename = filename
  mpeg_close,mpegid
end
