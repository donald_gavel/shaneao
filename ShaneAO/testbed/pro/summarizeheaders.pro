;+
;  summarizeheaders.pro - list specific header information
;        from a series of fits files
;-
pro summarizeheaders, head, tail, range, item, tofile=tofile, silent=silent
  n = n_elements(item)
  lun = 0
  if (n_elements(tofile)) then openw,lun,tofile,/get_lun
  if (n_elements(silent) eq 0) then silent = 0
  for i= range[0], range[1] do begin
    filename = head + strcompress(string(i),/remove_all) + tail
    r = findfile(filename,count=c)
    if (c eq 0) then begin
      if (not silent) then print,'<summarizeheaders> cannot open file ',filename
      if (lun ne 0) then printf,lun,'<summarizeheaders> cannot open file ',filename
    endif else begin
 
    hdr = headfits(filename)
    filename = nodirname(filename)
    if (not silent) then print,format='($,a,a)',filename,' '
    if (lun ne 0) then printf,lun,format='($,a,a)',filename,' '
    for k = 0,n-1 do begin
      val = getkeyval(hdr,item[k],/string)
      if (not silent) then print,format='($,a)',val
      if (lun ne 0) then printf,lun,format='($,a)',val
    endfor
    if (not silent) then print
    if (lun ne 0) then printf,lun

    endelse
  endfor
  if (lun ne 0) then close,lun
end
