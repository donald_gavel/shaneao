;+
;  Levinson.pro - find the solution to a Toeplitz system using
;                 the Levinson algorithm
;
;  usage:
;     x = levinson(t,y)
;     x = levinson2(tp,tm,y)
;
;  inputs
;     t - n+1 vector of symmetric toeplitz matrix entries
;              t =[t[0],t[1],t[2],...,t[n]]
;     tp - n+1 vector of non-symmetric toeplitz matrix entries
;          in the lower triangle:  tp=[t[0],t[1],...,t[n]]
;     tm - n+1 vector of non-symmetric toeplitz matrix entries
;          in the upper triangle:  tm=[t[0],t[-1],...,t[-n]]
;     y - vector of right-hand-side values y0,y1,...,yn
;
;  output
;     x - vector solution to y = T x, where T[i,j] = t[|i-j|]
;-
function levinson,t,y
  n = (size(t))(1)-1
  a = fltarr(n+1)
  x = fltarr(n+1)
  a[0] = 1
  alpha = t[0]
  x[0] = y[0]/t[0]
  for k=1,n do begin
    a[k]=-(1./alpha)*total( reverse(t[1:k])*a[0:k-1] )
    if (k gt 1) then begin
      a[1:k-1] = a[1:k-1] + a[k]*reverse( a[1:k-1] )
    endif
    alpha = (1 - a[k]^2)*alpha
    gamma = y[k] - total( reverse(t[1:k])*x[0:k-1] )
    x[0:k] = x[0:k] + (gamma/alpha)*reverse(a[0:k])
;    print,'k = ',k
;    print,'a = ',a
;    print,'alpha = ',alpha
;    print,'gamma = ',gamma
;    print,'x=',x[0:k]
  endfor
  return,x
end
;
function levinson2,tp,tm,y
  n = (size(y))(1)-1
  a = fltarr(n+1)
  b = fltarr(n+1)
  x = fltarr(n+1)
  a[0] = 1
  b[0] = 1
  alpha = tp[0]
  x[0] = y[0]/tp[0]
  for k=1,n do begin
    a[k] = -(1./alpha)*total( reverse(tp[1:k])*a[0:k-1] )
    b[k] = -(1./alpha)*total( reverse(tm[1:k])*b[0:k-1] )
    if (k gt 1) then begin
      lasta = a[1:k-1]
      a[1:k-1] = a[1:k-1] + a[k]*reverse( b[1:k-1] )
      b[1:k-1] = b[1:k-1] + b[k]*reverse( lasta )
    endif
    alpha = (1 - a[k]*b[k])*alpha
    gamma = y[k] - total( reverse(tp[1:k])*x[0:k-1] )
    x[0:k] = x[0:k] + (gamma/alpha)*reverse(b[0:k])
;    print,'k = ',k
;    print,'a = ',a
;    print,'b = ',b
;    print,'alpha = ',alpha
;    print,'x=',x[0:k]
  endfor
  return,x
end
