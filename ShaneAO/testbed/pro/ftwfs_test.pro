; test of ftwfs with tilt only

n = 16 ; very small, the GPI LOWFS
r = 3.5
ap = circle(n,n,n/2,n/2,r,1.)
tilt_slope_x = ap*ones(n,n)
tilt_slope_y = ap*0
sx = tilt_slope_x
sy = tilt_slope_y
ph = ftwfs(sx,sy,ap,boundaryFix = 'iterative',niter=99,geometry='Fried')
disp,tilt_slope_x,'slopes x'
disp,ph,'phase'
disp,ph*ap,'phase times ap'
window,/free
plot,(depiston(ph,ap)*ap)[*,n/2]
oplot,(depiston(ph,ap)*ap)[n/2,*]
end
