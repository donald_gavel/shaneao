    t0 = systime(/seconds)
    print,'computing tomography back-prop filters' & wait,.001
    A = dcomplexarr(n_guidestars,n_guidestars,n,n)
    C = dcomplexarr(n_guidestars,n,n)
    i = dcomplex(0.0d0,1.0d0)
    c0 = total(Cn2)
    z = lgs_altitude
    theta = gs_angle
    theta_science = science_angle
    df = double(1./(n*du))
    df_prime = df*((z-h)/z)
    Cn2_scaled = Cn2*(z/(z-h))^(-5./3.)
    percent_complete = 0
    fx = ones(n) ## (findgen(m)-m/2)
    fy = (findgen(n)-n/2) ## ones(m)
    fx = reform(fx,long(n)*long(m))
    fy = reform(fy,long(n)*long(m))
    dthetax = ones(n_guidestars) ## theta[*,0] - theta[*,0] ## ones(n_guidestars)
    dthetay = ones(n_guidestars) ## theta[*,1] - theta[*,1] ## ones(n_guidestars)
    dthetax = reform(dthetax,n_guidestars*n_guidestars)
    dthetay = reform(dthetay,n_guidestars*n_guidestars)
    tx = fx ## dthetax
    ty = fy ## dthetay
    tx = reform(tx,n_guidestars*n_guidestars*long(n)*long(m))
    ty = reform(ty,n_guidestars*n_guidestars*long(n)*long(m))
    tx = tx ## (df_prime*h)
    ty = ty ## (df_prime*h)
    e = exp(-2*!pi*i*(tx+ty))
    aa = (e ## cn2_scaled)/c0
    aa = reform(aa,n_guidestars,n_guidestars,n,m)
    t1 = systime(/seconds)
    print,'elapsed time ',t1-t0,' seconds'
end
