;+
;   besselk.pro - calculate the BesselK function
;
;  USAGE:
;    r = besselk(nu,z)
;
;  INPUTS:
;    nu - order (float)
;    z - argument (float)
;
;  OUTPUTS:
;    r - result:  K  (z)
;                  nu
;-
function besselk,u,zz

  if (size(zz))(0) ne 0 then begin ; allow vector arguments (z)
    n = (size(zz))(1)
    zv = double(zz)
  endif else begin
    n = 1
    zv = dblarr(1)
    zv[0] = double(zz)
  endelse
  rv = dblarr(n)

  nu = double(u)
  fac = 1.e-8

  for iz = 0,n-1 do begin
  
  z = zv[iz]
  if (z gt 8.) then begin;  this selection of "8" seems to work for nu=4/3
    c = sqrt(!pi/(2*z))*exp(-z)
    mu = 4*nu^2
    t = 1.
    k = 1
    r = t
    while (abs(t) gt abs(fac*r)) and (k lt 20) do begin
;      print,k,(2*k-1)^2,t,c*r
      t = t*((mu-(2*k-1)^2)/k)/(8.*z)
      r = r+t
      k = k+1
    endwhile
    r = c*r
  endif else begin
    c = (!pi/2.) / sin(nu*!pi)
    r = 0
    t1 = (z/2.)^(-nu)/gamma(-nu+1)
    t2 = (z/2.)^(nu)/gamma(nu+1)
    t = t1-t2
    k = 1
    r = t
    while abs(t) gt abs(fac*r) do begin
;      print,t,c*r,c*fac*r
      t1 = t1/(k*(-nu+k))*(z/2.)^2
      t2 = t2/(k*(nu+k))*(z/2.)^2
      t = t1-t2
;      t = ((z/2.)^(-nu) / gamma(-nu+k+1.) - $
;           (z/2.)^(nu) / gamma(nu+k+1.) )*(z/2.)^(2.*k) / factorial(k)
      r = r + t
      k = k+1
    endwhile
    r = c*r
  endelse

  rv[iz] = r
  endfor

  if (size(zz))(0) eq 0 then return, r
  return,rv
end
;
;   test
nu = 4/3.
c = (!pi/2) / sin(nu*!pi)
r = 0
t0 = (z/2.)^(-nu)/gamma(-nu+1)  - (z/2.)^(nu)/gamma(nu+1)
t = fltarr(10)
for k=0,9 do t[k] = ((z/2)^(-nu) / gamma(-nu+k+1) - $
   (z/2)^(nu) / gamma(nu+k+1) )*(z/2)^(2*k) / factorial(k)
end
