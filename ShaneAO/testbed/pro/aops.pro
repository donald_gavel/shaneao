;+
;  aops.pro - find average tilt power spectrum for Lick AO data
;
;  USAGE:
;    ps = aops(data,fs)
;    showps,ps,fs
;
;    data is standard centroid dataset from Lick data, 81xN (N usually = 4096)
;     (81st row is the loop status, 0=open, 1=closed)
;    fs is the sample rate in Hz
;    ps is the power spectrum in units(data)^2/hz
;      ps is the "dual-sided" spectra, i.e. it contains the values for both
;      negative and positive frequency.  Elements 0:N/2 are the positive side.
;
;    showps takes the dual-sided spectrum and plots only one side, doubling it
;     so each frequency bin represents total contributions from both "sides"
;
;    verify proper scaling with Parseval's theorem:
;      sqrt(total(ps(0,*))*df) eq sqrt(total(data(0:39,*)^2)/(40.*N))
;      where df=fs/N
;-
function aops,data,fs
  dt = 1./fs
;   These are the fully illuminated subapertures:
  isetillum = $
         [ 3,          4,          5,          6,          8,          9,$
          10,         11,         12,         13,         15,         16,$
          17,         18,         21,         22,         23,         24,$
          26,         27,         28,         29,         30,         31,$
          33,         34,         35,         36]
  isetall = indgen(40)
  iset = isetillum;    <-- change to select either only illuminated or all subaps
  xdata = data(iset,*)
  ydata = data(iset+40,*)
  n = (size(data))(2)
  m = (size(iset))(1)
  print,'computing ffts...'
  fxdata = complexarr(m,n)
  fydata = complexarr(m,n)
  for i=0,m-1 do begin
    fxdata(i,*) = n*fft(xdata(i,*))*dt
    fydata(i,*) = n*fft(ydata(i,*))*dt
    print,format='($,I0," ")',i
  endfor
  print,'done'
  print, 'now averaging power spectra ...'
  psx = fltarr(n)
  psy = fltarr(n)
  for i=0,m-1 do begin
    psx = psx + real(fxdata(i,*)*conj(fxdata(i,*)))
    psy = psy + real(fydata(i,*)*conj(fydata(i,*)))
    print,format='($,I0," ")',i
  endfor
  print,'done'
;   Average, and scale so result is in units(data)^2/hz
  psx = psx/(float(m)*float(n)*dt)
  psy = psy/(float(m)*float(n)*dt)
  ps = fltarr(2,n)
  ps(0,*) = psx
  ps(1,*) = psy
  return,ps
end

pro showps,ps,fs,oplot=oplot,color=color
  if (n_elements(color) eq 0) then color = 255
  ndim = (size(ps))(0)
  if ((ndim eq 2) and ((size(ps))(1) eq 1)) then begin
    showps,transpose(ps),fs
    return
  endif
  if (ndim eq 1) then  n = (size(ps))(1)/2. else n = (size(ps))(2)/2.
  dt = 1./fs
  fn = fs/2.
  f0 = fn/float(n)
  f = (findgen(n)+1)*f0
  if (ndim eq 1) then psx = 2*ps(1:n) else psx = 2*ps(0,1:n)
  if keyword_set(oplot) then begin
    oplot,f,psx,color=color
  endif else begin
    plot,f,psx,/xlog,/ylog,xtitle='Frequency, Hz',ytitle='Power, units^2/hz',color=color
  endelse
  if (ndim eq 2) then begin
    psy = 2*ps(1,1:n)
    oplot,f,psy,color=100
  endif
end
