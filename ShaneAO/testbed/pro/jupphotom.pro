;+
;    jupphotom.pro - photometrically analyze Jupiter images
;-

#refhome = '/lgs2/lick_jul94/jul_20/'
refhome = '/lgs1/lick_jul94/jul_19/jul_19/'
datahome = '/lgs1/lick_jul94/jul_19/'

;             550 nm 
#refset = rdspec(refhome + 'kappaVir_3',40)
refset = rdspec(refhome + 'alpha2lib_4',40)
;im = readfits(datahome + 'jul19j36_image.fits')
imset = rdspec(datahome + 'jupiter_36',40)
im = total(imset,3)

;             700 nm
;refset = rdspec(refhome + 'kappaVir_2',40)
;refset = rdspec(refhome + 'alpha2lib_5',40)
;im = readfits(datahome + 'jul19j31_image.fits')

;             850 nm
;refset = rdspec(refhome + 'kappaVir_1',40)
;im = readfits(datahome + 'jul19j32_image.fits')

;========================
;  set physical constants
;========================

;  units
nm = 10.^(-9)
erg = 10.^(-7); joules
cm = 10.^(-2); meters
micron = 10.^(-6); meters
arcsec = !pi / (180. * 3600.)

;   physical constants
h = 6.62617*10.^(-34);  Plank's constant joules/Hz
c = 3.*10.^8;  speed of light
k = 1.38044*10.^(-23);  Boltzmann constant
au = 1.495979*10.^11;     astronomical unit
flambda0v = 3.64*10.^(-2); standard flux (Vega) joules/m^2 m sec

;   observational parameters
lambda = 550. * nm
lambdav = 550. * nm
deltaLambda = 40 * nm;   filter width
nu = c/lambda
nuv = c/lambdav
dp = 3;  diameter of Lick primary
ds = .8;  diameter of Lick secondary
Atele = (dp^2 - ds^2)*!pi/4;  collection area of telescope
tint = 0.3;  integration time
nrefframes = 40;

;===================================
; calibrate the camera at wavelength
;===================================

ref = total(refset,3)

ctr = coords(ref , where(ref eq max(ref)))
rad = 150
cmask = circle(900,900,ctr(0),ctr(1),rad,1.0)
mask = 1-cmask
maskborder = mask*0 + 1
maskborder(0:5,*) = 0
maskborder(*,0:5) = 0
maskborder(894:899,*) = 0
maskborder(*,894:899) = 0
mask = mask*maskborder
r = planefit(ref,mask)
refr = (ref - r)*maskborder
dn = total(refr)

mv = 2.75;   alpha 2 lib visual magnitude
;mv = 4.19;   kappa Vir visual magnitude
T = 4000.;   kappa Vir temperature

flux = flambda0v * (lambdav/lambda)^5 * (exp(h*nuv/(k*T))-1)/(exp(h*nu/(k*T))-1) * (deltaLambda/(h*nu)) * 10^(-0.4*mv); photons / m^2 sec

mvj = -2.70;  Jupiter visual magnitude (at "opposition")
fluxj = flambda0v * (lambdav/lambda)^5 * (exp(h*nuv/(k*T))-1)/(exp(h*nu/(k*T))-1) * (deltaLambda/(h*nu)) * 10^(-0.4*mvj); photons / m^2 sec

nph = flux * Atele * tint * nrefframes; photon flux from reference star
nphj = fluxj * Atele * tint; photon flux from Jupiter
nphj = nphj * (4.2/5.2)^2;  account for distance (not at opposition on 7/19)
;  also, a Jupiter is a little dimmer due to the black impact spot!

cameraconstant = nph / dn
;   It's about 100 photons/dn, a factor of 10 larger than I thought it was.

dnj = total(im); Jupiter total counts
cameraconstantj = nphj / dnj;  what it would be if Jupiter were a photometric source
;
;   This is about 30% higher than the reference-derived cameraconstant.  Need to
;   load data from a reference taken 7/19 instead of 7/20.  It was somewhat
;   cloudy 7/19.  Still can't account for the factor of 10 greater cameraconstant.
;  ----
;   Refererence data (alpha 2 libra) from 7/19 (500 nm) shows a camera
;   constant of 48.3, quite a bit lower than Jupiter_36 data camera
;   constant of 113.  But the reference is at 1.74 air mass.  Jupiter_36
;   is at 2.67 air mass, so extinction may play a major role.
;   Matching the camera constants accounting for extinction, yields
;   an extinction coefficienct of 0.399 per air mass, and
;   (new, extinction-accounted) camera constant of
;   around 10.  This is consistent with a 8 ph/dn camera having 80% QE and
;   very little optics transmission loss.  Maybe the QE was folded
;   into the 8 ph/dn measurement.  I don't know the filter
;   curves very well either.  Should do this with 700 nm data to
;   see if it is consistent.

;  calculation to find extinction coefficient
degrees = !pi/180.
alt = 35*degrees
am = 1./sin(alt)
altj = 22*degrees
amj = 1./sin(altj)
x = 10^(alog10(cameraconstant/cameraconstantj)/(amj - am))

;  new camera constant
cameraconstant = cameraconstant*x^am

;==============================
; apply calibration to the data
;==============================

sundistance = 5.4167773856 * au
earthdistance = 5.1653890102 * au

imph = im * cameraconstant / x^amj;  converted to photons at the top of the atmosphere
deq = sqrt(dp^2 - ds^2)
areapix = (0.063 * arcsec * earthdistance)^2

imI = (imph/tint) * h * nu / (areapix * (!pi/4) * (deq/earthdistance)^2 )
; Watts / m^2 sr   (this is the I of I/F)

;=====================================
; figure the sun illumination constant
;=====================================

flambdaSun = 2.94 * 10.^(10) * erg / (cm^2 * micron); joules / sr m^2 m sec
Asun = 6.087 * 10.^18;  m^2

piF = flambdaSun * Asun * deltaLambda / sundistance^2; Watts / m^2

F = piF / !pi

;  albedo is I/(F * mu0) where mu0 is cosine of sun illumination angle