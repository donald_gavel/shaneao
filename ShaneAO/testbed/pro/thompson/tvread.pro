	FUNCTION TVREAD,RED,GREEN,BLUE,WINDOW=WINDOW,DISABLE=DISABLE
;+
; Project     : SOHO - CDS
;
; Name        : 
;	TVREAD()
; Purpose     : 
;	Reads contents of an image display screen into an array.
; Explanation : 
;	Reads the contents of an image display screen, or window, into an
;	array.  Uses TVRD.  Reads the entire window.
; Use         : 
;	Result = TVREAD()
;	Result = TVREAD(RED,GREEN,BLUE)
; Inputs      : 
;	None.
; Opt. Inputs : 
;	None.
; Outputs     : 
;	None.
; Opt. Outputs: 
;	RED, GREEN, BLUE = The active color tables in the selected
;		  window/device.
; Keywords    : 
;	WINDOW	= Which window to read from.  If passed, then overrides the
;		  TVSELECT routine.
;	DISABLE = If set, then the current graphics device/window is read.
;		  Otherwise, TVSELECT is called to select the image display
;		  device/window.  Ignored if WINDOW keyword is passed.
; Calls       : 
;	HAVE_WINDOWS, TVSELECT, TVUNSELECT
; Common      : 
;	None.
; Restrictions: 
;	Device must be capable of the TVRD function.  Window must be completely
;	visible, with no portion off the end of the screen.
;
;	In general, the SERTS image display routines use several non-standard
;	system variables.  These system variables are defined in the procedure
;	IMAGELIB.  It is suggested that the command IMAGELIB be placed in the
;	user's IDL_STARTUP file.
;
;	Some routines also require the SERTS graphics devices software,
;	generally found in a parallel directory at the site where this software
;	was obtained.  Those routines have their own special system variables.
;
; Side effects: 
;	None.
; Category    : 
;	None.
; Prev. Hist. : 
;	William Thompson, June 1991.
;	William Thompson, May 1992, added output parameters RED, GREEN, BLUE.
; Written     : 
;	William Thompson, GSFC, June 1991.
; Modified    : 
;	Version 1, William Thompson, GSFC, 11 May 1993.
;		Incorporated into CDS library.
; Version     : 
;	Version 1, 11 May 1993.
;-
;
	ON_ERROR,2
;
;  Select the graphics device or window to read from, if applicable.
;
	IF N_ELEMENTS(WINDOW) EQ 1 THEN BEGIN
		GRAPHICS_WINDOW = !D.WINDOW
		WSET,WINDOW
	END ELSE TVSELECT, DISABLE=DISABLE
;
;  Read in the entire window.  Also get the color tables.
;
	IF HAVE_WINDOWS() THEN BEGIN
		WSHOW,!D.WINDOW
		WAIT,1
	ENDIF
	RESULT = TVRD(0,0,!D.X_SIZE,!D.Y_SIZE)
	TVLCT,RED,GREEN,BLUE,/GET
	PRINT,'Image acquired' + STRING(7B)
;
;  Reset to the previous graphics device or window.
;
	IF N_ELEMENTS(WINDOW) EQ 1 THEN BEGIN
		IF GRAPHICS_WINDOW NE -1 THEN WSET,GRAPHICS_WINDOW
	END ELSE BEGIN
		TVUNSELECT, DISABLE=DISABLE
	ENDELSE
;
	RETURN,RESULT
	END
