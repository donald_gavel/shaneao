;
; Auto Save File For ./pibgui.pro
;
;  Mon Sep 29 22:43:03 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




PRO MAINPIB_Event, Event

  common pibguicom, state

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  widget_control,event.top,get_uvalue=uv
  entry = uv.entries

  zoomval = 3.
  t = size(Ev)
  if (t(0) eq 0) then evname = Ev else evname = Ev.name

;  CASE Ev OF 
  CASE evname OF 

  'DRAW6': BEGIN
      Print, 'Event for DRAW6'
      widget_control,event.id,get_value=index
;      wset, index
      tvdevice, index
      widget_control,event.id,get_uvalue=uval
      if (event.press and 1b) and ((size(uval.dispdata))(0) eq 2) then begin
        tvvalue,uval.dispdata
      endif
      END
  'BUTTON8': BEGIN
      Print, 'Event for Browse - file A'
;      widget_control,widget_info(event.id,/sibling),get_value=filename
;      filename = filename(0)
;      path = dirname(filename)
;      if (filename eq '') then path = '~/guidestar/lickLaser/data'
      print,'path=',!filepath
      filename = pickfile(path=!filepath,get_path=ret)
      print,'new path=',!filepath
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ret
;        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then state.HighData = 255 - tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
          catch,/cancel
        endif else begin
          catch,/cancel
          widget_control,/hourglass
;          im = 255-im
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base4
           widget_info(/parent, $; base7
             Event.id))
          win = $
           widget_info(/sibling, $; draw6
           widget_info(/child, $;  label5
             base))
          widget_control,win,get_value=index
;          wset, index
          tvdevice, index
;          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
;          if ((size(uval.background))(0) eq 2) then $
;            imbr = im - uval.background else imbr = im
;          exptv,imbr,/data,/nobox,/noexact
          imbr = state.HighData - state.HighBackground
          exptv, imbr, /data, /nobox, /noexact
          uval = {name:uval.name, rawdata:0, background:0, $
                  cleandata: 0, dispdata:imbr, origin:[0,0]}
          widget_control,win,set_uvalue=uval
        endelse
      endelse
      END
  'FIELD9': BEGIN
      Print, 'Event for file A:'
      END
  'BUTTON10': BEGIN
      Print, 'Event for Browse - background A'
;      widget_control,widget_info(event.id,/sibling),get_value=filename
;      filename = filename(0)
;      path = dirname(filename)
;      if (filename eq '') then path = '~/guidestar/lickLaser/data'
      print,'path=',!filepath
      filename = pickfile(path=!filepath,get_path=ret)
      print,'new path=',!filepath
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ret
;        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then state.HighBackground = 255 - tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
          catch,/cancel
        endif else begin
          catch,/cancel
          widget_control,/hourglass
;          im = 256-im
;    read the data file
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base4
           widget_info(/parent, $; base7
             Event.id))
          win = $
           widget_info(/sibling, $; draw6
           widget_info(/child, $;  label5
             base))
          widget_control,win,get_value=index
;          wset, index
          tvdevice, index
;          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
;          if ((size(uval.rawdata))(0) eq 2) then $
;            imbr = uval.rawdata - im else imbr = im
;          exptv,imbr,/data,/nobox,/noexact
          imbr = state.HighData - state.HighBackground
          exptv, imbr, /data, /nobox, /noexact
          uval = {name:uval.name, rawdata:0, background:0, $
                  cleandata:0, dispdata: imbr, origin:[0,0]}
          widget_control,win,set_uvalue=uval
        endelse
      endelse
      END
  'FIELD11': BEGIN
      Print, 'Event for bkgd A:'
      END
  'FIELD65': BEGIN
      Print, 'Event for x:'
      END
  'FIELD66': BEGIN
      Print, 'Event for y:'
      END
  'DRAW57': BEGIN
      Print, 'Event for DRAW57'
      widget_control,event.id,get_value=index
;      wset, index
      tvdevice, index
      widget_control,event.id,get_uvalue=uval
      if (event.press and 1b) and ((size(uval.dispdata))(0) eq 2) then begin
        tvvalue,uval.dispdata
      endif
      END
  'BUTTON40': BEGIN
      Print, 'Event for Browse - file B'
;      widget_control,widget_info(event.id,/sibling),get_value=filename
;      filename = filename(0)
;      path = dirname(filename)
;      if (filename eq '') then path = '~/guidestar/lickLaser/data'
;      print,'path=',path
      filename = pickfile(path=!filepath,get_path=ret)
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ret
;        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then state.LowData = 255 - tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
          catch,/cancel
        endif else begin
          catch,/cancel
          widget_control,/hourglass
;          im = 256-im
;    read the data file
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base4
           widget_info(/parent, $; base7
             Event.id))
          win = $
           widget_info(/sibling, $; draw6
           widget_info(/child, $;  label5
             base))
          widget_control,win,get_value=index
;          wset, index
          tvdevice, index
;          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
;          if ((size(uval.background))(0) eq 2) then $
;            imbr = im - uval.background else imbr = im
;          exptv,imbr,/data,/nobox,/noexact
          imbr = state.LowData - state.LowBackground
          exptv, imbr,/data,/nobox,/noexact
          uval = {name:uval.name, rawdata:0, background:0, $
                  cleandata:0, dispdata:imbr, origin:[0,0]}
          widget_control,win,set_uvalue=uval
        endelse
      endelse
      END
  'FIELD41': BEGIN
      Print, 'Event for file B:'
      END
  'BUTTON42': BEGIN
      Print, 'Event for Browse - background B'
;      widget_control,widget_info(event.id,/sibling),get_value=filename
;      filename = filename(0)
;      path = dirname(filename)
;      if (filename eq '') then path = '~/guidestar/lickLaser/data'
;      print,'path=',path
      filename = pickfile(path=!filepath,get_path=ret)
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
;        im = 0
        !filepath = ret
        error_status = 0
        catch, error_status
        if (error_status eq 0) then state.LowBackground = 255 - tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
          catch,/cancel
        endif else begin
          catch,/cancel
          widget_control,/hourglass
;          im = 256-im
;    read the data file
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base4
           widget_info(/parent, $; base7
             Event.id))
          win = $
           widget_info(/sibling, $; draw6
           widget_info(/child, $;  label5
             base))
          widget_control,win,get_value=index
;          wset, index
          tvdevice, index
;          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
;          if ((size(uval.rawdata))(0) eq 2) then $
;            imbr = uval.rawdata - im else imbr = im
          imbr = LowData - LowBackground
          exptv,imbr,/data,/nobox,/noexact
          uval = {name:uval.name, rawdata:0, background:0, $
                  cleandata:0, dispdata: imbr, origin:[0,0]}
          widget_control,win,set_uvalue=uval
        endelse
      endelse
      END
  'FIELD43': BEGIN
      Print, 'Event for bkgd B:'
      END
  'FIELD77': BEGIN
      Print, 'Event for x:'
      END
  'FIELD78': BEGIN
      Print, 'Event for y:'
      END
  'FIELD97': BEGIN
      Print, 'Event for         PIB radius'
      END
  'FIELD99': BEGIN
      Print, 'Event for Total Power Radius'
      END
  'FIELD101': BEGIN
      Print, 'Event for   Scale factor A:B'
      END
  'BUTTON103': BEGIN
      Print, 'Event for Analysis'
      widget_control,/hourglass
      base = $
        widget_info(/sibling, $ ; base3
        widget_info(/child, $ ; label2
          Event.top))
      drawA = $
        widget_info(/sibling, $ ; draw6
        widget_info(/child, $ ; label5
        widget_info(/child, $ ; base4
          base)))
      widget_control, drawA, get_uvalue = uvalA
;      imA = uvalA.cleandata
      imA = state.HighData - state.HighBackground
      centx = $
        widget_info(/child, $ ; field65
        widget_info(/sibling, $ ; base64
        widget_info(/child, $ ; label63
        widget_info(/sibling, $ ; base63
        widget_info(/sibling, $ ; base7
          drawA)))))
      widget_control, centx, get_value = cx
      centy = $
        widget_info(/sibling, $ ; field66
          centx)
      widget_control, centy, get_value = cy
      drawB = $
        widget_info(/sibling, $ ; draw57
        widget_info(/child, $ ; label59
        widget_info(/sibling, $ ; base35
        widget_info(/child, $ ; base4
          base))))
      widget_control, drawB, get_uvalue = uvalB
;      imB = uvalB.cleandata
      imB = state.LowData - state.LowBackground
      shiftx = $
        widget_info(/child, $ ; field77
        widget_info(/sibling, $ ; base76
        widget_info(/child, $ ; label75
        widget_info(/sibling, $ ; base74
        widget_info(/sibling, $ ; base44
          drawB)))))
      widget_control, shiftx, get_value = sx
      shifty = $
        widget_info(/sibling, $ ; field78
          shiftx)
      widget_control, shifty, get_value = sy
      pibrad = $
        widget_info(/child, $ ; field97
        widget_info(/sibling, $ ; base95
          base))
      widget_control, pibrad, get_value = r0
      totalpowerrad = $
        widget_info(/sibling, $ ; field99
          pibrad)
      widget_control, totalpowerrad, get_value = r1
      scalefactor = $
        widget_info(/sibling, $ ; field101
          totalpowerrad)
      widget_control, scalefactor, get_value = sf

      print,'size(imB) = ',size(imB)
      if ((size(imB))(0)) eq 0 then begin
        im = imA
      endif else begin
        mask = shift((imB eq max(imB)),sx,sy)
        im = imA*sf*mask + shift(imB,sx,sy)*(1-mask)
;        imc = im(((cx-r1)>0):((cx+r1)<640),((cy-r1)>0):((cy+r1)<480))
;        window,0,xsize = 256, ysize = 256
;        tvdevice,0
;        exptv,imc,/noexact,/nobox
        print,'calling dispgui'
        dispgui,im,'combined PIB image'
        state.CombinedData = im
      endelse
      print,'calling PIB fraction calculator ...'
      pibfrac = pib(im,cx,cy,[r0,r1])
      print,'========================================'
      print,'      Power in the Bucket: ',pibfrac
      print,'========================================'
      imA = imA(((cx-r1)>0):((cx+r1)<640),((cy-r1)>0):((cy+r1)<480))
      widget_control, drawA, get_value = index
;      wset, index
      tvdevice, index
      exptv, imA*(imA ne max(imA)), origin = [((cx-r1)>0), ((cy-r1)>0)], /data, /nobox, /noexact
      uvalA = {name:uvalA.name, rawdata:0, $
               background:0, cleandata:0, $
               dispdata: imA, origin:[((cx-r1)>0), ((cy-r1)>0)]}
      widget_control, drawA, set_uvalue = uvalA

      if (size(imB))(0) ne 0 then begin
        imB = shift(imB(((cx-r1)>0):((cx+r1)<640),((cy-r1)>0):((cy+r1)<480)),sx,sy)
        widget_control, drawB, get_value = index
;        wset, index
        tvdevice, index
        exptv, imB*(imB ne max(imB)), origin = [((cx-r1)>0), ((cy-r1)>0)], /data, /nobox, /noexact
        uvalB = {name:uvalB.name, rawdata:0, $
                 background:0, cleandata:0, $
                 dispdata: imB, origin:[((cx-r1)>0), ((cy-r1)>0)]}
        widget_control, drawB, set_uvalue = uvalB
      endif
      END
  'BUTTON107': BEGIN
      Print, 'Event for Done'
      widget_control, Event.Top, /destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



pro pibgui,thestate, GROUP=Group, help=helpq

  if keyword_set(helpq) then begin
    print,'usage: pibgui,thestate,/help
    return
  endif

  common pibguicom, state

  if n_elements(thestate) eq 0 then begin
    thestate = {HighData:fltarr(640,480), HighBackground:fltarr(640,480), $
                LowData:fltarr(640,480), LowBackground:fltarr(640,480), $
                CombinedData:fltarr(640,480),center:[320,240],shift:[0,0], $
                scale: 1.0, PowerRadius:50, BucketRadius:8}
  endif
  state = thestate

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }
  zoomval = 3.0

  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      TITLE='PIB', $
      UVALUE={name: 'MAIN13'})

  LABEL2 = WIDGET_LABEL( MAIN13, $
      FONT='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      UVALUE='LABEL2', $
      VALUE='Power in the Bucket Analysis')

  BASE3 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='2 PIB areas', $
      UVALUE='BASE3')

  BASE4 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='high level area', $
      UVALUE='BASE4')

  LABEL5 = WIDGET_LABEL( BASE4, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL5', $
      VALUE='High Level')

  DRAW6 = WIDGET_DRAW( BASE4, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW6', $
              rawdata:0, $
              background:0, $
              cleandata:0, $
              dispdata:state.HighData - state.HighBackground, $
              origin:[0,0]}, $
      XSIZE = 640/zoomval, $
      YSIZE = 480/zoomval)

  BASE7 = WIDGET_BASE(BASE4, $
      ROW=2, $
      FRAME=2, $
      MAP=1, $
      TITLE='file inputs', $
      UVALUE='BASE7')

  BUTTON8 = WIDGET_BUTTON( BASE7, $
      UVALUE='BUTTON8', $
      VALUE='Browse')

  FieldVal10069 = [ $
    '' ]
  FIELD9 = CW_FIELD( BASE7,VALUE=FieldVal10069, $
      ROW=1, $
      STRING=1, $
      TITLE='file A:', $
      UVALUE='FIELD9')

  BUTTON10 = WIDGET_BUTTON( BASE7, $
      UVALUE='BUTTON10', $
      VALUE='Browse')

  FieldVal10072 = [ $
    '' ]
  FIELD11 = CW_FIELD( BASE7,VALUE=FieldVal10072, $
      ROW=1, $
      STRING=1, $
      TITLE='bkgd A:', $
      UVALUE='FIELD11')


  BASE62 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='center x and y', $
      UVALUE='BASE62')

  LABEL63 = WIDGET_LABEL( BASE62, $
      UVALUE='LABEL63', $
      VALUE='center of bucket')

  BASE64 = WIDGET_BASE(BASE62, $
      ROW=1, $
      SPACE=50, $
      XPAD=30, $
      MAP=1, $
      TITLE='x and y 2 fields', $
      UVALUE='BASE64')

  FieldVal10077 = [ $
    '' ]
  FIELD65 = CW_FIELD( BASE64, $;VALUE=FieldVal10077, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      VALUE=state.center(0), $
      UVALUE='FIELD65', $
      XSIZE=5)

  FieldVal10079 = [ $
    '' ]
  FIELD66 = CW_FIELD( BASE64, $;VALUE=FieldVal10079, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      VALUE=state.center(1), $
      UVALUE='FIELD66', $
      XSIZE=5)




  BASE35 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='low level area', $
      UVALUE='BASE35')

  LABEL59 = WIDGET_LABEL( BASE35, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL59', $
      VALUE='Low Level (saturated)')

  DRAW57 = WIDGET_DRAW( BASE35, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW57', $
              rawdata:0, $
              background:0, $
              cleandata:0, $
              dispdata:state.LowData - state.LowBackground, $
              origin:[0,0]}, $
      XSIZE = 640/zoomval, $
      YSIZE = 480/zoomval)

  BASE44 = WIDGET_BASE(BASE35, $
      ROW=2, $
      FRAME=2, $
      MAP=1, $
      TITLE='file inputs', $
      UVALUE='BASE44')

  BUTTON40 = WIDGET_BUTTON( BASE44, $
      UVALUE='BUTTON40', $
      VALUE='Browse')

  FieldVal10086 = [ $
    '' ]
  FIELD41 = CW_FIELD( BASE44,VALUE=FieldVal10086, $
      ROW=1, $
      STRING=1, $
      TITLE='file B:', $
      UVALUE='FIELD41')

  BUTTON42 = WIDGET_BUTTON( BASE44, $
      UVALUE='BUTTON42', $
      VALUE='Browse')

  FieldVal10089 = [ $
    '' ]
  FIELD43 = CW_FIELD( BASE44,VALUE=FieldVal10089, $
      ROW=1, $
      STRING=1, $
      TITLE='bkgd B:', $
      UVALUE='FIELD43')


  BASE74 = WIDGET_BASE(BASE35, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='shift x and y', $
      UVALUE='BASE74')

  LABEL75 = WIDGET_LABEL( BASE74, $
      UVALUE='LABEL75', $
      VALUE='shift B to match A by:')

  BASE76 = WIDGET_BASE(BASE74, $
      ROW=1, $
      SPACE=50, $
      XPAD=30, $
      MAP=1, $
      TITLE='x and y', $
      UVALUE='BASE76')

  FieldVal10094 = [ $
    '' ]
  FIELD77 = CW_FIELD( BASE76, $;VALUE=FieldVal10094, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      VALUE=state.shift(0), $
      UVALUE='FIELD77', $
      XSIZE=5)

  FieldVal10096 = [ $
    '' ]
  FIELD78 = CW_FIELD( BASE76, $;VALUE=FieldVal10096, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      VALUE=state.shift(1), $
      UVALUE='FIELD78', $
      XSIZE=5)

  BASE95 = WIDGET_BASE(MAIN13, $
      ROW=2, $
      FRAME=2, $
      MAP=1, $
      TITLE='pib, power radius, scale factor', $
      UVALUE='BASE95')

  FieldVal10099 = [ $
    '' ]
  FIELD97 = CW_FIELD( BASE95, $;VALUE=FieldVal10099, $
      ROW=1, $
      INTEGER=1, $
      TITLE='        PIB radius', $
      VALUE=state.BucketRadius, $
      UVALUE='FIELD97')

  FieldVal10101 = [ $
    '' ]
  FIELD99 = CW_FIELD( BASE95, $;VALUE=FieldVal10101, $
      ROW=1, $
      INTEGER=1, $
      TITLE='Total Power Radius', $
      VALUE=state.PowerRadius, $
      UVALUE='FIELD99')

  FieldVal10103 = [ $
    '' ]
  FIELD101 = CW_FIELD( BASE95, $;VALUE=FieldVal10103, $
      ROW=1, $
      floating=1, $
      TITLE='  Scale factor A:B', $
      VALUE=state.scale, $
      UVALUE='FIELD101')


  BUTTON103 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON103', $
      VALUE='Analysis')

  BUTTON107 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON107', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN13, /REALIZE

  ; Get drawable window index

  COMMON DRAW6_Comm, DRAW6_Id
  WIDGET_CONTROL, DRAW6, GET_VALUE=DRAW6_Id

  ; Get drawable window index

  COMMON DRAW57_Comm, DRAW57_Id
  WIDGET_CONTROL, DRAW57, GET_VALUE=DRAW57_Id

  entryset = {HighDraw:draw6, HighFile:field9, HighBackground:field11, $
              CenterX: field65, CenterY: field66, LowDraw: draw57, $
              LowFile: field41, LowBackground:field43, ShiftX: field77, $
              ShiftY: field78, PIBradius: field97, PowerRadius: field99, $
              ScaleFactor: field101}

  widget_control, main13, get_uvalue = uv
  uv = {name: uv.name, entries: entryset}
  widget_control, main13, set_uvalue = uv

  widget_control, entryset.HighDraw, get_value=index
  tvdevice,index
  exptv, state.HighData - state.HighBackground, /data, /nobox, /noexact

  widget_control, entryset.LowDraw, get_value=index
  tvdevice,index
  exptv, state.LowData - state.LowBackground, /data, /nobox, /noexact

  XMANAGER, 'MAINPIB', MAIN13
  thestate = state
  return
END

