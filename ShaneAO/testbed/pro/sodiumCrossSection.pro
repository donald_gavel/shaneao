; SodiumCrossSection.pro
; calculate the sodium atom D2a cross-section
;
;  Reference: D. Steck, Sodium D Line Data, 2007
;    http://steck.us/alkalidata
;
ns = 1.d-9 ; nanosecond
mW = 1.d-3 ; milliWatt
cm = 1.d-2 ; cm
nm = 1.d-9 ; nanometers
;
c = 299792458.d0 ; speed of light m/s
h = 6.62606957d-34 ; Plank's constant, J-s
hbar = h/(2*!pi)
e0 = 8.854187817620d-12 ; permittivity of free space, Farads per meter
tau = 16.2492*ns ; decay decay time of Sodium, Steck Table 3
Gamma = 1/tau ; decay rate of excited state
d = 2.11305d-29 ; circ polarized dipole moment |F=2,mF=2> ->|F=3,mF=3> transition (Steck, Table 7), coulomb-meters
;
; reported from Steck, table 7:
;   Isat = 6.2600 mW/cm^2
;   cross-section sigma_0 = 1.6573163925e-9 cm^2
;
; c*e0: (m/s) (F/m) = (m/s) (C/(V m)) = C / (V s)
; d: C m
; c*e0 / (d^2): C / (V s C C m m) = 1 / (V s C)  (1/m^2)
; hbar*Gamma: J s / s = J = W s
; (hbar*Gamma)^2 c e0 / d^2: ((W W s s) / (V s C)) (1/m^2) = ((W s) / (V C))  (W/m^2) = ((J) / (J)) (W/m^2)
;
; Intensity of E field: I = (1/2) c e0 E0^2 ; (m/s) (F/m) (V / m)^2 = (m/s) (C/V) (1/m) (V/m)^2 = (1/s) (C V/ m^2) = (J/s) (1/m^2) = W/m^2
;
Isat = (c*e0*Gamma^2*hbar^2)/(4*d^2) ;
print,'Isat = ',Isat,' W/m^2'
print,'Isat = ',Isat/(mW/cm^2), ' mW/cm^2'
lambda = 589.1583264*nm ; wavelength of the Sodium D2 3S_1/2 -> 3P_3/2 transition, Steck Table 3, m
omega = 2*!pi*c/lambda ; frequency of the light, 1/s
sigma_0 = (hbar*omega*Gamma)/(2*Isat) ; cross-section, m^2
print,'sigma_0 = ',sigma_0,' m^2'
print,'sigma_0 = ',sigma_0/cm^2,' cm^2'
;
;  upper state population rho_ee in the case of on-resonance typical laser intensity
;   (Steck, eqn 47)
;
arcsec = !pi/(180.*3600.)
dp = 20.*cm ; projection aperture diameter
zNa = 90.e3 ; distance to sodium layer
fwhm = zNa*(lambda/dp) ; size of spot in sodium layer
P = 10; power of laser, Watts
I = P/(fwhm^2*(!pi/4.)) ; intensity of beam at sodium layer, W/m^2
print,'I = ',I,' W/m^2 at sodium layer'
E_0 = sqrt(2*I/(c*e0)) ; electric field amplitude, V/m
omega_rabi = d*E_0/hbar ; Rabi frequency, 1/s
rho_ee = (omega_rabi/Gamma)^2 / (1 + 2*(omega_rabi/Gamma)^2) ; upper state population
print,'rho_ee = ',rho_ee
end
