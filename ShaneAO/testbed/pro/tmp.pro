;
;  correlation centoiding
;  tests
;
function ftcorrelate1,sig1,sig2
  return,ftconvolve1(sig1,shift(reverse(sig2),1))
end
;
function ft1,a,inverse=inverse
  n = (size(a))(1)
  if keyword_set(inverse) then $
    r = shift(fft(shift(a,n/2),/inverse),n/2) $
  else $
    r = shift(fft(shift(a,n/2)),n/2)
  return,r
end
;
function ftconvolve1,sig1,sig2
  n = (size(sig1))(1)
  fim1 = ft1(sig1)
  fim2 = ft1(sig2)
  fres = fim1*fim2
  res = ft1(fres,/inverse)*float(n)
return,res
end
;
n = 512
sigma = 50.
a = gauss1(n,n/2,sigma); gaussian spot, max amplitude = 1
;seed = 5
snrlist = [0.2,.3,.4,0.5,.6,.7,.8,1.,1.25,1.5,1.75,2.,2.5,3.,4.,5.,6.,7.,8.,9.,10.]
snrlistn = (size(snrlist))[1]
sd = fltarr(snrlistn)
for k = 0,snrlistn-1 do begin
  print,k
  snr = snrlist[k]
  ntrials = 1000
  centarr = fltarr(ntrials)
  for i = 0,ntrials-1 do begin
    ;noise = (1./snr)*randomn(seed,n); uniform noise
    noise = (1./snr)*randomn(seed,n)*sqrt(a)  ; poisson noise
    b = a + noise                   ; added to signal
    c = real(ftcorrelate1(a,b))
    ;if (i eq 0) then plot,x,c else oplot,x,c
    ;peak = max(c)     ; correlation tracker / matched filter
    peak = max(b)      ; peak tracker
    centarr[i] = !c
  endfor
  sd[k] = stddev(centarr)/sigma
endfor
;plot,snrlist,sd,/xlog,/ylog,psym=6,xtitle = 'SNR',ytitle = 'Centroid Error / Spot Std.Dev'
oplot,snrlist,sd,psym = 2; psym=6
oplot,snrlist,0.13*snrlist^(-1)   ; asymptote to correlation tracker
oplot,snrlist,0.5*snrlist^(-0.5)  ; asymptote to peak tracker
print,transpose(reform([snrlist,sd],snrlistn,2))
;  -- results; 1000 trials, sigma=50, n=512 --
;   Fits: 0.13*snr^-1 in decade snr=[1,10]
;
;     0.200000      1.95772
;     0.300000      1.31745
;     0.400000     0.737241
;     0.500000     0.467677
;     0.600000     0.282438
;     0.700000     0.218068
;     0.800000     0.171875
;      1.00000     0.129956
;      1.25000     0.102586
;      1.50000    0.0875871
;      1.75000    0.0736216
;      2.00000    0.0661234
;      2.50000    0.0515188
;      3.00000    0.0433085
;      4.00000    0.0323367
;      5.00000    0.0267224
;      6.00000    0.0222634
;      7.00000    0.0194518
;      8.00000    0.0172364
;      9.00000    0.0151399
;      10.0000    0.0138344
;
;oplot,snrlist,sd,psym=6,color=100
;
;x = findgen(n)-n/2
;w = 2*!pi/10.
;y = sin(w*x)
;fy = ft1(y)
;plot,abs(fy)
end
