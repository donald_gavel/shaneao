;
;  centroid
;

function centroid,array
  sv = size(array)
  n = sv(1)
  m = sv(2)
  x = extend(indgen(n),2,m)
  y = extend(indgen(m),1,n)
  nf = total(array)
  result = [total(x*array), total(y*array)]/nf
  return, result
end
