;+
;  keckmtf.pro - create the Keck modulation transfer function (MTF)
;
;  USAGE:
;    mtf = keckmtf(n,lambda=l,dx=d)
;
;  INPUTS:
;    n - size of image in pixels
;    lambda - wavelength, m
;    dx (optional) - pixel scale in image domain, radians
;    du (optional) - pixel scale in aperture domain, m
;     use dx or du, but not both
;
;  OUTPUTS:
;    mtf - (complex) modulation transfer function image
;
;-
function KeckMtf,n,lambda=lambda,dx=dx,du=du
  microns = 1e-6
  arcsec = !pi/(180.*3600.)
  if n_elements(n) eq 0 then n = 256
  if n_elements(lambda) eq 0 then lambda = 2.2 * microns
  if n_elements(du) eq 0 then begin
    if n_elements(dx) eq 0 then dx = 0.02 * arcsec
  endif else begin
    dx = lambda/(n*du)
  endelse
  dtele = 11.  ; meters, longest baseline
  nqc = (lambda/dtele)/dx  ; nyquist criterion
  if (nqc lt 2.0) then print,'<keckmtf> caution: mtf is aliased'
  psf = keckpsf(n,lambda=lambda,dx=dx)
  mtf = float(n)^2*ft(psf)
  return,mtf
end
