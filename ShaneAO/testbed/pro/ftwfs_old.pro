; ftwfs.pro
;  implements the Fourier-transform wavefront sensor
;
;  Reference: Poyneer, Gavel, Brase, "Fast wave-front reconstruction
;     in large adaptive optics systems with use of the Fourier transform,"
;     JOSA-A, 19, 10, Oct 2002, pp 2100-2111.
;
;  Inputs:
;     sx - array of x slopes
;     sy - array of y slopes
;     ap - mask of illuminated subapertures (1=illuminated)
;     subapFilter - option switch to divide the answer by the
;              subaperture transfer function assuming that the wavefront
;              sensor averaged the gradient over a square subaperture (bisinc)
;     geometry - Fried (0), Shering (1), or Hudgin (2) geometry.  This tells the algorithm
;            how to half-pixel shift the slope data so that it aligns properly with
;            the phase sample points.  Fried is the default.
;
;  Output:
;     a - array of phases
;
function ftwfs,sx,sy,ap,subapFilter=subapFilter,geometry=geometry,verbose=verbose
;
;  this function solves the inverse problem:
;  s = grad(a) for a given s
;
  nx = (size(sx))[1]
  ny = (size(sy))[1]
  if (nx ne ny) then begin
    print,'<ftwfs> ERROR, size of sensor arrays not equal'
    return,0
  endif
  n = nx
;
; Do the slope extension to handle the masked-aperture boundary problem
;
  if (min(ap) eq 0) then begin
;	  apm = zeropad(ap,n+2,n+2)
;	  sxe = zeropad(sx,n+2,n+2)
;	  sye = zeropad(sy,n+2,n+2)
;	  extend_grid,apm,sxe,sye
;	  sx = sxe[1:n,1:n]
;	  sy = sye[1:n,1:n]
   if (keyword_set(verbose)) then print,'<ftwfs> correcting boundary'
   extend_grid,ap,sx,sy
	endif
;
  FriedGeometry = 0
  ShearingGeometry = 1
  HudginGeometry = 2
  if (n_elements(geometry) eq 0) then geometry = FriedGeometry ; default
  if (type(geometry) eq 7) then begin
    case geometry of
     'Fried': geometry = 0
     'Shearing': geometry = 1
     'Hudgin': geometry = 2
    endcase
  endif
;
  i = dcomplex(0,1.)
  dkx = 2*!pi/double(n)
  kx = (dindgen(n)-n/2) # ones(n)*dkx
  kx[0,*]=0. ; mask out the exact Nyquist to keep divergences real
  kx[*,0]=0.
  ky = transpose(kx)
;  k0 masks out places in the Fourier domain where kx^2+ky^2 = 0
  k0 = kx^2+ky^2 eq 0
;
  fsx = ft(sx)
  fsy = ft(sy)
;
;  half-pixel shifts...
;
;   Shearing sensor geometry - assumes that the slope data sample points are
;         located 1/2 pixel in the negative slope-vector direction from the phase sample points
;
  if (geometry eq ShearingGeometry) then begin
    fsx = fsx*exp(i*kx*.5)
    fsy = fsy*exp(i*ky*.5)
  endif
;
;   Fried geometry - assumes that the slope data sample points are located
;         1/2 pixel diagonal (in the negative, negative sense) from the phase sample points
;
;
  if (geometry eq FriedGeometry) then begin
    fsx = fsx*exp(i*kx*.5)*exp(i*ky*.5)
    fsy = fsy*exp(i*kx*.5)*exp(i*ky*.5)
  endif
;
;   Hudgin geometry - slope sample points overlap phase sample points - no pixel shift

;
;  --- Solve Poisson's Equation in the Fourier Domain ---
;
  fa = (-i*kx*fsx - i*ky*fsy)/(kx^2+ky^2+k0^2)
  fa *= (1-k0)
;
;  -------------------------------------------------------
;
  if (keyword_set(subapFilter)) then begin
    fsax = 2*sin(kx/2)/(kx+(kx eq 0))
    fsax += kx eq 0
    fsay = 2*sin(ky/2)/(ky+(ky eq 0))
    fsay += ky eq 0
    fsa = fsax*fsay
    fa = fa/fsa
  endif
;
  a = real(ft(fa,/inverse))
  return,a
end

function computeSlopes,a,subapFilter=subapFilter,differenceMethod=differenceMethod,geometry=geometry,verbose=verbose
;
;  this function performs the forward computation, i.e. s = grad(a)
;  which is used in iterative algorithms to solve the inverse problem
;
  n = (size(a))[1]
  if (keyword_set(differenceMethod) eq 0) then differenceMethod = 'finiteDifference'
  if (keyword_set(geometry) eq 0) then geometry = 'Fried'
  if (differenceMethod eq 'finiteDifference') then begin
    if (geometry eq 'Shearing') then begin ; straight first difference
      sx = a-shift(a,1,0)
      sy = a-shift(a,0,1)
    endif
    if (geometry eq 'Fried') then begin ; average first difference with neighbor first difference one delta away in orthogonal direction
      sx = a-shift(a,1,0)
      sx = (sx + shift(sx,0,1))/2.
      sy = a-shift(a,0,1)
      sy = (sy + shift(sy,1,0))/2.
    endif
    if (geometry eq 'Hudgin') then begin ; compute as a central difference
      sx = a-shift(a,1,0)
      sx = (sx + shift(sx,-1,0))/2.
      sy = a-shift(a,0,1)
      sy = (sy + shift(sy,0,-1))/2.
    endif
  endif
  if (differenceMethod eq 'Fourier') then begin
      i = dcomplex(0,1.)
      dkx = 2*!pi/double(n)
      kx = (dindgen(n)-n/2) # ones(n)*dkx
      kx[0,*]=0.
      kx[*,0]=0.
      ky = transpose(kx)
      kmask = 1-((kx eq 0) and (ky eq 0))
      if (keyword_set(subapFilter)) then begin
          fsax = 2*sin(kx/2)/(kx+(kx eq 0))
          fsax += kx eq 0
          fsay = 2*sin(ky/2)/(ky+(ky eq 0))
          fsay += ky eq 0
          fsa = fsax*fsay
      endif else begin
          fsa = 1
      endelse
      fa = ft(a)
      fa *= kmask               ; remove piston and Nyquist components
      fsx = i*kx*fa*fsa
      fsy = i*ky*fa*fsa
      if (geometry eq 'Shearing') then begin
          fsx *= exp(-i*kx*0.5)
          fsy *= exp(-i*ky*0.5)
      endif
      if (geometry eq 'Fried') then begin
          fsx *= exp(-i*(kx+ky)*0.5)
          fsy *= exp(-i*(kx+ky)*0.5)
      endif
;     if (geometry eq 'Hudgin') then begin  ; no shift
;     endif
      sx = real(ft(fsx,/inverse))
      sy = real(ft(fsy,/inverse))
  endif
  r = dblarr(n,n,2)
  r[*,*,0] = sx
  r[*,*,1] = sy
  return,r
end

;-----------------------------------------------------
;  test software
;-----------------------------------------------------
do_test1 = 0
do_test2 = 1

seed = 5
n = 64;48
apDiam = 0.5*n
dtele = 8.
du = dtele/double(apDiam)
r00 = 0.2
lambda = 1.6
r0 = r00*(lambda/0.5)^(6./5.)
;
FriedGeometry = 0
ShearingGeometry = 1
HudginGeometry = 2
i = dcomplex(0,1.)
dkx = 2*!pi/double(n)
kx = (dindgen(n)-n/2) # ones(n)*dkx
kx[0,*]=0.
kx[*,0]=0.
ky = transpose(kx)
;
ap = ones(n,n)
f = screengen(n,n,r0,du)
a = screengen(f,seed)*(lambda/(2*!pi))
;a = randomn(seed,n,n)
;g = gauss2(n,n/2,n/2,n/8.)
;a = real(ftconvolve(a,g))
fa = ft(a)
kmask = ones(n,n)
kmask[n/2,n/2] = 0.
kmask[*,0] = 0.
kmask[0,*] = 0.
fa *= kmask  ; remove piston and Nyquist components
a = double(ft(fa,/inverse))

if (do_test1) then begin

;
;  ----- finite backward difference ----------
;
title = 'finite backward difference'
sx = a-shift(a,1,0)
sy = a-shift(a,0,1)
ahat = ftwfs(sx,sy,ap,geometry='Shearing'); ,/subapFilter)
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  finite backward difference with subap filter turned on
;
title = 'finite backward difference, with subap filter'
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Shearing')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fried Geometry averaging ------
;
title = 'finite difference Fried Geometry'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
ahat = ftwfs(sx,sy,ap,geometry='Fried')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fried Geometry averaging with subap filter turned on ------
;
title = 'finite difference Fried Geometry, with subap filter'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Fried')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fourier-derivative with 1/2 pixel shift ------
;    (this one should give zero error, since it is the inverse of ftwfs with subapFilter turned off)
;
title = 'Fourier derivative, Shearing'
fsx = fa*i*kx*exp(-i*kx*0.5)
fsy = fa*i*ky*exp(-i*ky*0.5)
sx = real(ft(fsx,/inverse))
sy = real(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Shearing')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Subap filtered derivative ------
;    Average gradient over a subaperture, computed in Fourier space, shifted 1/2 pixel
;    (this one should give zero error, since it is the inverse of ftwfs with subapFilter turned on)
;
title = 'Fourier deriv, Shearing, with subap filter'
fsax = 2*sin(kx/2)/(kx+(kx eq 0))
fsax += kx eq 0
fsay = 2*sin(ky/2)/(ky+(ky eq 0))
fsay += ky eq 0
fsa = fsax*fsay
fsx = fa*i*kx*fsa*exp(-i*kx*0.5)
fsy = fa*i*ky*fsa*exp(-i*ky*0.5)
sx = real(ft(fsx,/inverse))
sy = real(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Shearing')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fourier derivative, Fried geometry ----
;    (should give zero error)
;
title = 'Fourier derivative, Fried'
fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Fried')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fourier derivative, Fried geometry, with subap filter -----
;
title = 'Fourier deriv, Fried, with subap filter'
fsx = fa*i*kx*fsa*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*fsa*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Fried')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fourier derivative, Hudgin geometry ----
;    (should give zero error)
;
title = 'Fourier derivative, Hudgin'
fsx = fa*i*kx
fsy = fa*i*ky
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Hudgin')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ Fourier derivative, Hudgin geometry, with subap filter -----
;
title = 'Fourier deriv, Hudgin, with subap filter'
fsx = fa*i*kx*fsa
fsy = fa*i*ky*fsa
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Hudgin')
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ use an aperture ------
;
title = 'with aperture, finite difference Fried, no boundary correction'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
;
;fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
;fxy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
;sx = real(ft(fsx,/inverse))
;sy = real(ft(fsy,/inverse))
;
ap = circle(n,n,n/2,n/2,apDiam/2.,1.)
sxa = sx*ap
sya = sy*ap
ap0 = ones(n,n)
ahat = ftwfs(sxa,sya,ap0,geometry='Fried',/subapFilter)
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;
;  ------ aperture with boundary correction ------
;
title = 'with aperture, finite difference Fried, and boundary correction'
ap = circle(n,n,n/2,n/2,apDiam/2.,1.)
sxa = sx*ap
sya = sy*ap
ahat = ftwfs(sxa,sya,ap,geometry='Fried',/subapFilter)
e = depiston(a-ahat,ap)
print,title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=title
;

endif ; do_test1

if (do_test2) then begin

; =========  test the slope computer ==========
title = 'Fourier derivative, Fried'
ap = circle(n,n,n/2,n/2,apDiam/2.,1.); ones(n,n)
fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))*ap
sy = double(ft(fsy,/inverse))*ap
s0 = reform(transpose([transpose(sx),transpose(sy)]),n,n,2)
ap2 = reform(transpose([transpose(ap),transpose(ap)]),n,n,2)
ap0 = ones(n,n) ; ap ; set to all ones to force no boundary compensation

ahat = ftwfs(sx,sy,ap0,geometry='Fried')
shat = computeSlopes(ahat,differenceMethod='Fourier')
print,title+' ',rms(depiston(a-ahat,ap),ap),rms(sx-shat[*,*,0],ap),rms(sy-shat[*,*,1],ap),' rms'
;disp,s0-shat,'s0 - s_hat',ap=ap
iteration = 0
itergain = 1.0
n_iterations = 30

; iterate
esave = dblarr(n,n,4,n_iterations+1)
esave[*,*,0,0] = a-ahat
esave[*,*,1:2,0] = s0-shat
esave[*,*,3,0] = ahat
stat = fltarr(n_iterations+1)
stat[0] = rms((s0-shat)[*,*,0],ap)
for iteration = 1,n_iterations do begin
    es = (s0-shat)*ap2
    ahat2 = ahat + itergain*ftwfs(es[*,*,0]*ap,es[*,*,1]*ap,ap0,geometry='Fried')
    shat2 = computeSlopes(ahat2,differenceMethod='Fourier')
;    disp,s0-shat2,'s0 - s_hat iter '+strtrim(iteration,2),ap=ap
	esave[*,*,0,iteration] = a-ahat2
    esave[*,*,1:2,iteration] = s0-shat2
    esave[*,*,3,iteration] = ahat2
    stat[iteration] = rms((s0-shat2)[*,*,0],ap)
    ahat = ahat2
    shat = shat2
    print,title+' iter '+strtrim(iteration,2)+' ',rms(depiston(a-ahat,ap),ap),rms(s0[*,*,0]-shat[*,*,0],ap),rms(s0[*,*,1]-shat[*,*,1],ap),' rms'
endfor

if (n_elements(plot_exists) eq 0) then begin
  plot,stat,/ylog,/xlog,xrange=[1,n_iterations],xtitle='iteration',ytitle='rms error in x-slope'
  plot_exists = 1
endif else begin
  oplot,stat,linestyle=2
endelse

disp,reform(esave[*,*,0,*],n,n,n_iterations+1)

endif ; do_test2

end
