;
;  doTitanJun.pro
;        process speckle data from June
;
.compile,getjundata
; dsn = 'june780'
.run,titanDataset
  getjundata,dataset,titan,titanDark,titanmdark,avedark
  badpix = readfits("/duck1/gavel/keck/sep96/titan/badpix.fits")
;  badpix = findbad(titan(*,*,0))
  debad,titanmdark,titanmdark,badpix
  skyframe = getjunsky(dataset,badpix,sky,skydark)
  remsky, titanmdark, skyframe, titanmsky
  print,"flat-fielding"
  flat = makeflat(dataset,badpix)
  titanmsf = make_array(256,256,100)
  for i=0,99 do begin & titanmsf(*,*,i) = titanmsky(*,*,i)/(flat>.5) & print,format='($,I0," ")',i & endfor
;  cshiftg, titanmsf, titanmsfsw  ; shifted and windowed
  cshift, titanmsf, titanmsfs  ; shifted 
  devstripe, titanmsfs, titanmsfsds, dataset.backgroundarea_y
  dehstripe, titanmsfsds, titanmsfsds, dataset.backgroundarea_x
  sw = supergauss(256,128,128,55,10);  center=[128,128], width=55, power=10
  for i=0,99 do begin & titanmsfsds(*,*,i) = titanmsfsds(*,*,i)*sw & print,format='($,I0," ")',i & endfor
;
; --- write the speckle file ---
  specklefile = dataset.resultsdir + dataset.basename + ".spec"
  wrspec, specklefile, fix(titanmsfsds>0)
;
;  --- process through the bispectrum code ---
  outputfile = dataset.resultsdir + dataset.basename
  command = 'frame ' + specklefile + ' ' + outputfile
  spawn,command
;
;  --- read in the results ---
  ps = rdview(dataset.resultsdir + dataset.basename + "_ps")
  phase = rdview(dataset.resultsdir + dataset.basename + "_phase")
  mtf = rdview(dataset.mtffile)
;
;  --- reference star data ---
  ref_ps = rdview(dataset.resultsdir + dataset.starname + "_ps")
  ref_phase = rdview(dataset.resultsdir + dataset.starname + "_phase")
  psp = pencil(ps,3.,25.)
  titanimage = spdeconvolve(psp,phase,ref_ps,ref_phase,mtf,0,128,.99)
  imagefile = dataset.resultsdir + dataset.basename + "_image.fits"
  writefits,imagefile,titanimage
