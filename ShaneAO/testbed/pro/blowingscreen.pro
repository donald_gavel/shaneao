;
;  create a blowing screen
;
;  (use flim,screens to view)
;.compile screengen
;
function blowingScreen_init,n,m,r0,du,seed=seed
  f = screengen(m,n,r0,du)
  if (n_elements(seed) eq 0) then seed = 5
  s = screengen(f,seed)
  s = [s,s[0:n-1,*]] ; this will make it loop
  return,s
end
;
;  blowingScreen_get(s,t,v)
;    s = base screen
;    t = time (units of seconds)
;    v = optional 2 vector of velocity (units of pixel/second), multiplies t, defaults to [1.,0.]
;
function blowingScreen_get,s,t,v_arg
  n = (size(s))[2]
  m = (size(s))[1]
  if (n_elements(v_arg) eq 0) then v_arg = 1
  if (n_elements(v_arg) eq 1) then begin
    v = v_arg
    tt = (v*t) mod (m-n)
    if (tt lt 0) then tt += m-n
	  s1 = bilinear(s,indgen(n)+tt,indgen(n))
  endif
  if (n_elements(v_arg) eq 2) then begin
    v = v_arg
    w = sqrt(v[0]^2+v[1]^2)
    if (w ne 0) then a = v/w else a = [1,0]
    x = ones(n) ## findgen(n) - n/2
    y = transpose(x)
    tt = (w*t) mod (m-n)
    if (tt lt 0) then tt += m-n
    xprime = a[0]*x - a[1]*y + tt + n/2
    yprime = a[1]*x + a[0]*y + n/2
    s1 = interpolate(s,xprime,yprime,/cubic)
  endif
  return,s1
end
;
pro blowingScreen
	print,'<blowingScreen> compiled'
end
;
	n = 128
	m = 2*128
	r0 = 1.
	du = 1.
	;.compile screengen
	screen = blowingScreen_init(n,m,r0,du)
	ns = m
	ap = circle(n,n,n/2,n/2,(n/2)*.8,1.)
	screens = fltarr(n,n,ns)
	dt = .1
	v = [1.0,2.0]*10.
	for k=0,ns-1 do begin
	  t = float(k)*dt
	  screens[*,*,k] = blowingScreen_get(screen,t,v)*ap
	endfor
end
