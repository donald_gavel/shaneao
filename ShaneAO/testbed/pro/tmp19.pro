n = 256
mm = .001
microns = .000001
i = complex(0.,1.)

D = 2*mm
screensize = 25*mm
dx = screensize/float(n)
f_number = 10000
f = D*f_number
lambda = 0.6*microns

L = 1.
rayleighRange = D^2/lambda
diffraction = L*lambda/D
screensize = dx*n
print,'rayleighRange: ',rayleighRange,' meters'
print,'diffraction: ',diffraction/mm,' mm'
print,'screensize: ',screensize/mm,' mm'

pupil = circle(n,n,n/2,n/2,D/(2*dx),1.)
wf = shift(pupil,n/4)
wfL = fresnel(wf,dx,L,lambda)

end