;+
;  fitellipse.pro - find the best-fit parameters of an ellipse
;
;  usage:
;     fitellipse,data,cx,cy,a,b,alpha
;
;  input:
;     data - image of ellipse (such as a planet ring)
;            data points > 1.0 are ellipse points
;
;  output:
;     cx,cy - center of ellipse
;     a,b - major and minor axes
;     alpha - orientation angle
;
;  see also:
;     ellipse.pro
;-
pro fitellipse,data,cx,cy,a,b,alpha
end
