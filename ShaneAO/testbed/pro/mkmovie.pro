; make a movie from a 3d array - already in memory

pro mkmovie, array

sizes = size(array)

xdim = sizes[1]
ydim = sizes[2]
zdim = sizes[3]

xinteranimate, set=[xdim, ydim, zdim], /showload

for i=0,zdim-1 do xinteranimate, frame = i, image = bytscl(array[*,*,i])

xinteranimate, /keep_pixmaps
 

end