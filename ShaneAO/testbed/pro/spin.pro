;+
; spin.pro - take the azimuthally symmetric part of a 2-d array
;
;  sa = spin(a)
;
;   a = n x n array
;  sa = n x n array result, which will be azimuthally symmetric
;
;-
function spin,a
  ndim = (size(a))[0]
  if (ndim eq 2) then begin
    n = (size(a))[1]
    pa = topolar(a)
    u = total(pa,2)/float(2*n)
  endif else begin
    n = 2*(size(a))[1]
    u = a
  endelse
  pa = u # ones(n*2)
  sa = torect(pa)
  return,sa
end
