;
; lineout - produce a list of values along a line in an image
;    a: image
;    c: starting point of line (2-vector)
;    th: angle of line (in radians)
;    l: length of line (in pixels)
;   output:
;    a list of pairs: {distance along line (in pixels), value}
;
function lineout, a,c,th,l,view=view
  if (arg_present(view)) then view = a*0
  p0 = c
  pf = c + l*[cos(th),sin(th)]
  n = max(abs(pf-p0))
  dp = (pf-p0)/float(n)
  d = fltarr(n)
  v = fltarr(n)
  p = p0
  for k = 0,n-1 do begin
    d[k] = sqrt(total((p-p0)^2))
    v[k] = a[p[0],p[1]]
    p += dp
    if (arg_present(view)) then view[p[0],p[1]] = 1
  endfor
  r = fltarr(n,2)
  r[*,0] = d
  r[*,1] = v
  return,r
end
