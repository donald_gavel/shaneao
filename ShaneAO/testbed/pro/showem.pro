; showem.pro
;  @showem.pro

;        -- read the data from disk --
dir = '/duck1/gavel/keck/sep96/io/'
io231=mrdfits(dir+'io231/io231_star281_image.fits',0)
io242=mrdfits(dir+'io242/io242_star265_image.fits',0)
io245=mrdfits(dir+'io245/io245_star281_image.fits',0)
;io248=mrdfits(dir+'io248/io248_star281_image.fits',0)
io315=mrdfits(dir+'io315/io315_star281_image.fits',0)

;          -- scale and rotate the images --
magfac = 8                   ; magnification factor
n = 64                        ; number of original pixels to display
xs = 0 & ys = 0               ; images are centered on max pixel plus this shift

ia = -5                    ; Io's polar axis inclination angle w.r.t. earth's
m = max(io231,p) & x = (p mod 256)+xs & y = p/256
io231rs = magrot(reverse(io231(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-5.195+ia,magfac)
io231s = magrot(reverse(io231(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),0,magfac)

m = max(io242,p) & x = (p mod 256)+xs & y = p/256
io242rs = magrot(reverse(io242(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-8.12+ia,magfac)
io242s = magrot(reverse(io242(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),0,magfac)

m = max(io245,p) & x = (p mod 256)+xs & y = p/256
io245rs = magrot(reverse(io245(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-9.93+ia,magfac)
io245s = magrot(reverse(io245(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),0,magfac)

;m = max(io248,p) & x = (p mod 256)+xs & y = p/256
;io248rs = magrot(reverse(io248(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-11.734+ia,magfac)

x = 128 & y = 128
io315rs = magrot(reverse(io315(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-37.994+ia,magfac)
magfac = 4
n = 256/magfac
io315r = magrot(reverse(io315(x-(n/2)+1:x+(n/2),y-(n/2)+1:y+(n/2))),-37.994+ia,magfac)

;         -- display the images --
;disp,io231s
;disp_txt, "io231","",10
;disp,io242s
;disp_txt, "io242","",10
;disp,io245s
;disp_txt,"io245","",10
;disp,io315r
;disp_txt,"io315 sunlit","",10

;          -- compute the median and average image, display --
n = size(io231rs)
u = make_array(n(1),n(2),3)
u(*,*,0) = io231rs
u(*,*,1) = io242rs
u(*,*,2) = io245rs
m = median_image(u)
s = total(u,3)/3

;disp,m
;disp_txt,"median","",10
;disp,rebin(s,256,256)
disp,s
disp_txt,"average","",10

;       -- lineouts --
;disp,s
;window,!d.window+1,xsize=356,ysize=256
;plot,s(*,123) & oplot,s(*,140),color=100
