;+
;  wfsrotation.pro - calculate the "rotation"
;    component in the wavefront centroid data
;-
function wfsrotation,cent,scale=scale
  if (n_elements(scale) eq 0) then scale=3.
  dir = '/net/lgs6/home/winkerbean/AO/Data/RunData/'
  data = rdview(dir + cent)
  c = total(data,2)/4096.
  x = [           -.5,.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
   -3.5,-2.5,-1.5,       1.5,2.5,3.5, $
   -3.5,-2.5,-1.5,       1.5,2.5,3.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
                  -.5,.5            ]
;
  y = [           -3.5,-3.5, $
        -2.5,-2.5,-2.5,-2.5,-2.5,-2.5, $
        -1.5,-1.5,-1.5,-1.5,-1.5,-1.5, $
   -0.5,-0.5,-0.5,          -0.5,-0.5,-0.5, $
    0.5, 0.5, 0.5,           0.5, 0.5, 0.5, $
         1.5, 1.5, 1.5, 1.5, 1.5, 1.5, $
         2.5, 2.5, 2.5, 2.5, 2.5, 2.5, $
                   3.5, 3.5             ]
  illumonly = [3,4,5,6,8,9,10,11,12,13,15,16,17,18, $
               21,22,23,24,26,27,28,29,30,31, $
               33,34,35,36]
;  remove tip/tilt from centoids
  tip = total(c[0:39])/40.
  tilt = total(c[40:79])/40.
  cx = c[0:39] - tip
  cy = c[40:79] - tilt
;  twist angle
  r2 = x*x + y*y
  r2a = (x+cx)*(x+cx)+(y+cy)*(y+cy)
  theta = (-(cx+x)*y + (cy+y)*x)/sqrt(r2*r2a)
;  plot of "velocity" field
  triangulate,x,y,triangles
  rx = trigrid(x,y,cx,triangles,[1,1])
  ry = trigrid(x,y,cy,triangles,[1,1])
  velovect,rx,ry
;  plot,x,-y,psym=6,background=255,color=0
;  oplot,x,-y,psym=6,color=200
;  oplot,x+cx*scale,-y-cy*scale,psym=1,color=0
;
  deg=total(theta)/40.*180./!pi
  print,total(theta)/40.,'radians, ',deg,' degrees'
  print,'approx. 0.35 deg/tic, move approx ',deg/(-0.35), ' tics'
  return,total(theta)/40.
end
