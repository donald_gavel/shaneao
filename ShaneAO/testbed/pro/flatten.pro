;+
;  flatten - apply a flat-field to a data cube
;
; USAGE:
;   flatten,incube,outcube,flat
;
;-
pro flatten,incube,outcube,flat
  if n_params() eq 0 then begin
    print,'usage: flatten, incube, outcube, flat'
    return
  end
  if (n_elements(limfac) eq 0) then limfac = 0.5
  s = size(incube)
  if (s(0) eq 3) then nf = s(3) else nf = 1
  n = s(1)
  m = s(2)
  if n_elements(outcube) eq 0 then outcube = make_array(n,m,nf)
  for i=0,nf-1 do begin
    outcube(*,*,i) = incube(*,*,i)/flat
    print,format='($,I0," ")',i
  endfor
  print
end
