;+
;   rms.pro - find the rms value of an image
;         ans = rms(im,ap)
;-
function rms,im,ap
	ima = im
	apf = finite(ima)
	nan_ind = where(1-apf,count)
	if (count ne 0) then begin
		ima[nan_ind] = 0
		print,'<rms> Warning: there are ',strtrim(n_elements(nan_ind),2),' NaNs in the data'
	endif
  	if (n_elements(ap) ne 0) then begin
    	apf *= ap
  	endif
	ima *= apf
	r = sqrt(total(ima*ima)/total(apf))
    return,r
end
