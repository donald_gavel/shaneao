; alternative post processing after changing science wavelength or constellation
  field_angle = 100.*arcsec / sqrt(2.)
  n_scienceStars = 36
  science_pattern = 'line'
  TomographySphericalWave_Widget_realize, widget_id
  calculateDerivedParameters
  diffractionLimitedImage
  coaddScienceImages,/sample
  printStatistics
  case_results = fltarr(n_scienceStars,4,n_cases)
  case_results(*,1:3,theCase) = strehlResults
  for star=0,n_scienceStars-1 do case_results[star,0,theCase] = sqrt(total(science_angle[star,*]^2))/arcsec
  window,/free & plot,case_results[*,0,theCase],case_results[*,3,theCase],psym=1,xtitle='radial field angle (arc seconds)',ytitle='RMS wavefront error (nm)'
  window,/free & plot,case_results[*,0,theCase],case_results[*,1,theCase],psym=1,xtitle='radial field angle (arc seconds)',ytitle='Strehl at 1.25 microns'
end
