HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	03/15/2007 00:27.28
VERSION 1
END

base_TomogSphereWave BASE 5 5 947 690
REALIZE "TomographySphericalWave_widget_realize"
TLB
CAPTION "TomographySphericalWave Setup"
XPAD = 3
YPAD = 3
SPACE = 3
BEGIN
  button_doSimulation PUSHBUTTON 11 64 137 28
  VALUE "Do Simulation"
  ALIGNCENTER
  ONACTIVATE "TomographySphericalWave_Widget_doSimulation"
  END
  button_CheckConfiguration PUSHBUTTON 11 25 0 0
  VALUE "Check Configuration"
  ALIGNCENTER
  ONACTIVATE "tomographySphericalWave_Widget_checkConfiguration"
  END
  button_forwardPropScienceDirections PUSHBUTTON 11 102 137 29
  VALUE "Prop Science Waves"
  ALIGNCENTER
  ONACTIVATE "TomographySphericalWave_Widget_PropScienceWaves"
  END
  base_WFSGuidestarConstellation BASE 178 25 513 165
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_wfsGuidestarConstellation LABEL 78 2 256 21
    VALUE "Wavefront Sensing Guidestar Constellation"
    ALIGNLEFT
    END
    label_guidestarConstellationRadius LABEL 13 33 0 0
    VALUE "Guidestar Constellation Radius (arcsec)"
    ALIGNLEFT
    END
    textInput_guidestarConstellationRadius TEXT 268 29 91 24
    NUMITEMS = 1
    ITEM "2.5 arcmin"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_numberOfWFSGuidestars LABEL 13 62 0 0
    VALUE "Number of Wavefront Sensing Guidestars"
    ALIGNLEFT
    END
    droplist_constellationPattern DROPLIST 13 121 0 0
    CAPTION "Constellation Pattern"
    NUMITEMS = 4
    ITEM "Circle"
    ITEM "One in middle"
    ITEM "Two in middle"
    ITEM "Three in middle"
    END
    droplist_altitudeOfWFSGuidestars DROPLIST 13 91 0 0
    CAPTION "Altitude of Wavefront Sensing Guidestars"
    NUMITEMS = 2
    ITEM "Sodium Layer"
    ITEM "Infinity"
    END
    textInput_numberOfWFSGuidestars TEXT 267 59 56 24
    NUMITEMS = 1
    ITEM "6"
    EDITABLE
    ALLEVENTS
    ONINSERTCHAR "TomographySphericalWave_Widget_numWFSGS"
    WIDTH = 20
    HEIGHT = 1
    END
    WID_BASE_2 BASE 361 53 0 0
    COLUMNS = 1
    NONEXCLUSIVE
    CAPTION "IDL"
    BEGIN
      button_spatialFilter PUSHBUTTON -1 -1 0 0
      VALUE "spatial filtered wfs"
      ALIGNLEFT
      END
    END
    WID_BASE_3 BASE 364 125 0 0
    COLUMNS = 1
    NONEXCLUSIVE
    CAPTION "IDL"
    BEGIN
      button_labData PUSHBUTTON -1 -1 0 0
      VALUE "lab data"
      ALIGNLEFT
      END
    END
  END
  base_scienceField BASE 332 306 362 154
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_scienceField LABEL 108 6 0 0
    VALUE "Science Field"
    ALIGNLEFT
    END
    droplist_scienceField DROPLIST 7 32 223 24
    CAPTION "Science Field"
    NUMITEMS = 3
    ITEM "IRMOS 5 arcmin"
    ITEM "NFIRAOS 1 arcmin"
    ITEM "10 arcsec"
    END
    textInput_numberOfScienceObjects TEXT 189 65 63 26
    NUMITEMS = 1
    ITEM "3"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_numberOfScienceObjects LABEL 8 69 0 0
    VALUE "Number of Science Objects"
    ALIGNLEFT
    END
    droplist_scienceWavelength DROPLIST 8 96 280 24
    CAPTION "Science Wavelength"
    NUMITEMS = 6
    ITEM "K (2.2 micron)"
    ITEM "H (1.6 micron)"
    ITEM "J (1.2 micron)"
    ITEM "I (0.9 micron)"
    ITEM "R (0.7 micron)"
    ITEM "V (0.55 micron)"
    ONSELECT "tomographySphericalWave_widget_checkDL"
    END
    label_plateScale LABEL 10 128 0 0
    VALUE "Display Platescale (arcsec)"
    ALIGNLEFT
    END
    textInput_plateScale TEXT 184 125 72 24
    NUMITEMS = 1
    ITEM "0.0035"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_diffractionLimit LABEL 260 127 83 18
    VALUE "DL="
    ALIGNLEFT
    END
  END
  base_RunOptions BASE 6 197 323 330
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_runOptions LABEL 111 9 0 0
    VALUE "Run Options"
    ALIGNLEFT
    END
    droplist_iterationMethod DROPLIST 18 91 253 24
    CAPTION "Iteration Method"
    NUMITEMS = 2
    ITEM "Conjugate Gradient"
    ITEM "Linear Feedback"
    END
    label_maxIterations LABEL 38 132 0 0
    VALUE "Max Iterations"
    ALIGNLEFT
    END
    textInput_maxIterations TEXT 126 128 63 24
    NUMITEMS = 1
    ITEM "100"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    button_stop PUSHBUTTON 53 182 205 28
    VALUE "Stop"
    ALIGNCENTER
    ONACTIVATE "TomographySphericalWave_widget_stopButton"
    END
    WID_BASE_1 BASE 14 44 0 0
    COLUMNS = 1
    NONEXCLUSIVE
    CAPTION "IDL"
    BEGIN
      checkButton_tipTiltUnknown_1 PUSHBUTTON -1 -1 0 0
      VALUE "tip/tilt unknown"
      ALIGNLEFT
      END
    END
    label_iteration LABEL 231 130 43 18
    VALUE "0"
    ALIGNCENTER
    END
    label_nTimeSteps LABEL 15 270 0 0
    VALUE "# Time Steps"
    ALIGNLEFT
    END
    label_Time LABEL 184 268 0 0
    VALUE "Time ="
    ALIGNLEFT
    END
    label_TimeValue LABEL 239 268 43 18
    VALUE "0"
    ALIGNCENTER
    END
    textInput_nTimeSteps TEXT 104 265 69 24
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_StopAt LABEL 73 155 0 0
    VALUE "Stop at"
    ALIGNLEFT
    END
    textInput_StopAt TEXT 126 152 72 24
    NUMITEMS = 1
    ITEM "0"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_nmRms LABEL 201 152 0 0
    VALUE "nm rms"
    ALIGNLEFT
    END
    label_rmsErrorReport LABEL 258 152 45 19
    VALUE "0"
    ALIGNLEFT
    END
    label_RealizationValue LABEL 266 231 43 18
    VALUE "0"
    ALIGNCENTER
    END
    label_Realization LABEL 183 232 0 0
    VALUE "Realization"
    ALIGNLEFT
    END
    textInput_nRealizations TEXT 105 229 67 24
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    label_nRealizations LABEL 16 234 0 0
    VALUE "# Realizations"
    ALIGNLEFT
    END
    WID_BASE_0 BASE 156 43 115 29
    COLUMNS = 1
    NONEXCLUSIVE
    CAPTION "IDL"
    BEGIN
      checkButton_warmRestart PUSHBUTTON -1 -1 107 29
      VALUE "warm restart"
      ALIGNLEFT
      ONACTIVATE "tomographySphericalWave_Widget_warmRestart"
      END
    END
    label_deltaTime LABEL 22 300 78 18
    VALUE "delta time"
    ALIGNLEFT
    END
    label_seconds LABEL 186 299 0 0
    VALUE "seconds"
    ALIGNLEFT
    END
    textInput_deltaTime TEXT 101 294 74 24
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    button_reset PUSHBUTTON 275 43 40 28
    VALUE "reset"
    ALIGNCENTER
    ONACTIVATE "tomographySphericalWave_widget_reset"
    END
  END
  base_TomogSphereWave_MBAR MENUBAR 0 0 0 0
  BEGIN
    menu_File PUSHBUTTON 0 0 0 0
    VALUE "File"
    MENU
    BEGIN
      menu_file_read PUSHBUTTON 0 0 0 0
      VALUE "Read Parameter File"
      ONACTIVATE "tomographySphericalWave_widget_readParameterFile"
      END
      menu_file_saveas PUSHBUTTON 0 0 0 0
      VALUE "Save Parameters ..."
      ONACTIVATE "tomographySphericalWave_widget_saveas"
      END
      menu_file_readresults PUSHBUTTON 0 0 0 0
      VALUE "Read Results File"
      SEPARATOR
      ONACTIVATE "tomographySphericalWave_widget_ReadResultsFile"
      END
      menu_file_saveresults PUSHBUTTON 0 0 0 0
      VALUE "Save Results ..."
      ONACTIVATE "tomographySphericalWave_widget_SaveResults"
      END
      menu_file_quit PUSHBUTTON 0 0 0 0
      VALUE "Quit"
      SEPARATOR
      ONACTIVATE "tomographySphericalWave_widget_exit"
      END
    END
    menu_go PUSHBUTTON 0 0 0 0
    VALUE "Go"
    MENU
    BEGIN
      menu_go_check PUSHBUTTON 0 0 0 0
      VALUE "Check Configuration"
      ONACTIVATE "tomographySphericalWave_widget_checkConfiguration"
      END
      menu_go_do PUSHBUTTON 0 0 0 0
      VALUE "Do Simulation"
      ONACTIVATE "tomographySphericalWave_widget_doSimulation"
      END
      menu_go_prop PUSHBUTTON 0 0 0 0
      VALUE "Prop Science Waves"
      ONACTIVATE "tomographySphericalWave_widget_propScienceWaves"
      END
      menu_go_science PUSHBUTTON 0 0 0 0
      VALUE "Prop Guidestar Waves"
      ONACTIVATE "tomographySphericalWave_widget_propGuidestarWaves"
      END
      menu_go_clearWins PUSHBUTTON 0 0 0 0
      VALUE "Clear Windows"
      ONACTIVATE "tomographySphericalWave_widget_Clearwins"
      END
      W_MENU_1 PUSHBUTTON 0 0 0 0
      VALUE "Do Cases 1,2,3,..."
      SEPARATOR
      ONACTIVATE "tomographySphericalWave_Widget_DoCases"
      END
    END
    menu_stop PUSHBUTTON 0 0 0 0
    VALUE "Stop"
    MENU
    BEGIN
      meun_stop_stop PUSHBUTTON 0 0 0 0
      VALUE "Stop"
      ONACTIVATE "TomographySphericalWave_widget_stopButton"
      END
    END
    menu_Display PUSHBUTTON 0 0 0 0
    VALUE "Display"
    MENU
    BEGIN
      menu_display_lastRealization PUSHBUTTON 0 0 0 0
      VALUE "Last Realization"
      ONACTIVATE "tomographySphericalWave_widget_displayLastRealization"
      END
      menu_display_AveragePSFs PUSHBUTTON 0 0 0 0
      VALUE "Average PSFs"
      ONACTIVATE "tomographySphericalWave_widget_displayAveragePSFs"
      END
      menu_display_EncircledEnergy PUSHBUTTON 0 0 0 0
      VALUE "Encircled Energy"
      ONACTIVATE "tomographySphericalWave_widget_displayEncircledEnergy"
      END
      menu_display_diagnosticOutput PUSHBUTTON 0 0 0 0
      VALUE "Diagnostic Output"
      ONACTIVATE "tomographySphericalWave_widget_diagnosticOutput"
      END
    END
    menu_Help PUSHBUTTON 0 0 0 0
    VALUE "Help"
    END
  END
  base_Telescope BASE 703 26 217 147
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_telescope LABEL 75 3 0 0
    VALUE "Telescope"
    ALIGNLEFT
    END
    label_subapsAcross LABEL 6 56 0 0
    VALUE "Subapertures Across"
    ALIGNLEFT
    END
    textInput_subapsAcross TEXT 141 53 56 24
    NUMITEMS = 1
    ITEM "100"
    EDITABLE
    ALLEVENTS
    ONINSERTCHAR "tomographySphericalWave_widget_checkSubaps"
    WIDTH = 20
    HEIGHT = 1
    END
    droplist_subsample DROPLIST 4 80 0 0
    CAPTION "Subsample"
    NUMITEMS = 4
    ITEM "1x"
    ITEM "2x"
    ITEM "3x"
    ITEM "4x"
    ONSELECT "tomographySphericalWave_widget_checkSize"
    END
    button_checkSize PUSHBUTTON 168 113 40 26
    VALUE "check"
    ALIGNCENTER
    TOOLTIP "calculate the fine grid array size"
    ONACTIVATE "tomographySphericalWave_widget_checkSize"
    END
    label_size LABEL 8 117 89 19
    VALUE "Fine Grid Size"
    ALIGNLEFT
    END
    text_size TEXT 101 114 59 24
    WIDTH = 20
    HEIGHT = 1
    END
    droplist_aperture DROPLIST 6 24 197 24
    CAPTION "Aperture"
    NUMITEMS = 3
    ITEM "30 meters"
    ITEM "10 meters"
    ITEM "Keck"
    ONSELECT "tomographySphericalWave_widget_checkDL"
    END
  END
  button_clearWindows PUSHBUTTON 9 143 139 27
  VALUE "Clear Windows"
  ALIGNCENTER
  ONACTIVATE "TomographySphericalWave_Widget_Clearwins"
  END
  label_runID LABEL 392 474 0 0
  VALUE "Run ID"
  ALIGNLEFT
  END
  text_runID TEXT 439 471 52 24
  NUMITEMS = 1
  ITEM "0"
  WIDTH = 20
  HEIGHT = 1
  END
  base_Atmosphere BASE 704 182 216 187
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_atmosphere LABEL 61 3 0 0
    VALUE "Atmosphere"
    ALIGNLEFT
    END
    label_r0 LABEL 11 38 54 18
    VALUE "r_0 (cm)"
    ALIGNLEFT
    END
    droplist_Cn2Profile DROPLIST 8 65 0 0
    CAPTION "Cn2 Profile"
    NUMITEMS = 5
    ITEM "Cerro Pachon"
    ITEM "Bad-Bad"
    ITEM "Test"
    ITEM "Mauna Kea"
    ITEM "CN-MK1"
    END
    textInput_r0 TEXT 73 37 36 24
    NUMITEMS = 1
    ITEM "16"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    seed_label LABEL 29 106 0 0
    VALUE "Seed"
    ALIGNLEFT
    END
    seed_text TEXT 75 101 69 24
    NUMITEMS = 1
    ITEM "5"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    droplist_Wind DROPLIST 6 138 0 0
    CAPTION "Wind Profile"
    NUMITEMS = 2
    ITEM "random 10m/s"
    ITEM ""
    END
  END
  text_messageBox TEXT 503 467 411 63
  SCROLL
  NUMITEMS = 1
  ITEM ""
  WRAP
  WIDTH = 20
  HEIGHT = 1
  END
  label_guiID LABEL 390 515 0 0
  VALUE "GUI ID"
  ALIGNLEFT
  END
  text_guiID TEXT 442 509 47 21
  WIDTH = 20
  HEIGHT = 1
  END
  text_parameterFilename TEXT 133 537 416 24
  WIDTH = 20
  HEIGHT = 1
  END
  label_parameterFile LABEL 24 542 0 0
  VALUE "Parameter File"
  ALIGNLEFT
  END
  label_outputFile LABEL 23 576 0 0
  VALUE "Output File"
  ALIGNLEFT
  END
  text_outputFilename TEXT 132 573 415 24
  WIDTH = 20
  HEIGHT = 1
  END
  button_chooseOutputWorkingDirectory PUSHBUTTON 565 572 0 0
  VALUE "Choose Directory"
  ALIGNCENTER
  ONACTIVATE "tomographySphericalWave_widget_outputDirectoryBrowse"
  END
  base_TipTiltStarConstellation BASE 333 199 358 101
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    label_TipTiltConstellation LABEL 54 4 0 0
    VALUE "Tip Tilt Star Constellation"
    ALIGNLEFT
    END
    label_nTipTiltStars LABEL 164 29 0 0
    VALUE "# Tip Tilt Stars"
    ALIGNLEFT
    END
    textInput_nTipTiltStars TEXT 253 26 53 24
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    droplist_TipTiltConstellation DROPLIST 8 59 299 24
    CAPTION "TipTiltConstellation"
    NUMITEMS = 4
    ITEM "Polygon"
    ITEM "Random, Galactic Pole"
    ITEM "Random, glat = 45"
    ITEM "Random, Galactic Equator"
    END
    WID_BASE_4 BASE 4 23 0 0
    COLUMNS = 1
    NONEXCLUSIVE
    CAPTION "IDL"
    BEGIN
      button_UseTipTilt PUSHBUTTON -1 -1 0 0
      VALUE "Use Tip/Tilt Stars"
      ALIGNLEFT
      END
    END
  END
END
