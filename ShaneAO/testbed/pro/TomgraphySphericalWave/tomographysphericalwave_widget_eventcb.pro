;
; IDL Event Callback Procedures
; TomographySphericalWave_Widget_eventcb
;
; Generated on:	07/22/2005 16:43.04
;
;
; Empty stub procedure used for autoloading.
;
pro TomographySphericalWave_Widget_eventcb
end

; ----------------------------------------------------------------
pro TomographySphericalWave_Widget_Message,top,text
  id = widget_info(top,find_by_uname='text_messageBox')
  s = size(text)
  if (s[0] eq 1) then begin ; array of strings
      nlines = s[1]
      for line = 0,nlines-1 do begin
          widget_control,id,set_value=text+string(13B)
      endfor
  endif else begin
      widget_control,id,set_value=text
  endelse
end

;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_Widget_checkConfiguration, Event
	common programSwitches

	TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
	calculateDerivedParameters,/display
end
;
pro tomographySphericalWave_Widget_updateIteration,top,iter
  id = widget_info(top,find_by_uname='label_iteration')
  widget_control,id,set_value=strtrim(string(iter),2)
end
;
pro tomographySphericalWave_Widget_updateTimeStep,top,timeStep
  id = widget_info(top,find_by_uname='label_TimeValue')
  widget_control,id,set_value=strtrim(string(timeStep),2)
end
;
pro tomographySphericalWave_Widget_updateRealization,top,realization
  id = widget_info(top,find_by_uname='label_RealizationValue')
  widget_control,id,set_value=strtrim(string(realization),2)
end
;
pro tomographySphericalWave_Widget_updateRMSerror,top,rmsError
  id = widget_info(top,find_by_uname='label_rmsErrorReport')
  widget_control,id,set_value=strtrim(string(rmsError),2)
end
;
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_Widget_Clearwins, Event
        common constants
	clearwins
        top = Event.top
	id = widget_info(top,find_by_uname='text_runID')
		widget_control,id,set_value=strtrim(string(run_no),2)
        id = widget_info(Event.top,FIND_BY_UNAME='text_outputFilename')
                TomographySphericalWave_widget_setOutputDirectory, id
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_Widget_doSimulation, Event
	common programSwitches

	TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
	tomographySphericalWave
        id = widget_info(Event.top,find_by_uname='button_forwardPropScienceDirections')
        widget_control,id,/sensitive

end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_Widget_doPostProcessing, Event
	common programSwitches

	TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
	tomographySphericalWave
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_Widget_PropScienceWaves, Event
	common programSwitches

        TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
        diffractionLimitedImage
        coaddScienceImages,/sample
end
;-----------------------------------------------------------------

pro TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
;
; _SynchonizeGUItoProgram reads the GUI and sets program values accordingly
; (see also: _realize)
;
	common constants
	common programSwitches
	common tomography
	common atmosphere
	common imaging

	print,'<TomographySphericalWave_Widget_SynchronizeGUItoProgram> top = ',Event.top
	top = Event.top
        id = widget_info(top, FIND_BY_UNAME='droplist_aperture')
                choice = widget_info(id,/droplist_select)
                widget_control,id,get_value = choices
                aperture = choices[choice]
                pupil_shape = 'circle'
                D_tele = float(aperture)
                if (aperture eq '30 TMT') then D_tele = 30
                if (aperture eq '16 circle') then D_tele = 16
                if (aperture eq '10 circle') then D_tele = 10
                if (aperture eq '10 Keck') then begin
                    D_tele = 10
                    pupil_shape = 'Keck'
                endif
                if (aperture eq '10 hexagon') then begin
                    D_tele = 10
                    pupil_shape = 'Hexagon'
                endif
                if (aperture eq '8 circle') then D_tele = 8
                if (aperture eq '5 Palomar') then D_tele = 5
	id = widget_info(top, FIND_BY_UNAME='textInput_numberOfWFSGuidestars')
		widget_control,id,get_value=text
		ngs = fix(text)
		print,'<TomographySphericalWave_Widget_numWFSGS> ngs = ',ngs
		if (ngs gt 0) then begin
                    n_guidestars = ngs[0]
                endif
        id = widget_info(top, FIND_BY_UNAME='button_spatialFilter')
                if (widget_info(id,/button_set)) then begin
                    wfsType = 'spatialFilter'
                endif else begin
                    wfsType = 'HartmannSubap'
                endelse
        id = widget_info(top, FIND_BY_UNAME='button_labData')
                if (widget_info(id,/button_set)) then begin
                    labData = 1
                endif else begin
                    labData = 0
                endelse
	id = widget_info(top,find_by_uname='droplist_constellationPattern')
		choice = widget_info(id,/droplist_select)
                widget_control,id,get_value = choices
                pattern = choices[choice]
	id = widget_info(top,find_by_uname='textInput_guidestarConstellationRadius')
		widget_control,id,get_value=text
		text = strsplit(text,' ',/extract)
		r = float(text[0])
		units = arcsec
		if (n_elements(text) gt 1) then begin
			if (text[1] eq 'arcsec') then units = arcsec
			if (text[1] eq 'arcmin') then units = arcmin
		endif
		r = r*units
		radius = r
	id = widget_info(top,find_by_uname='droplist_altitudeOfWFSGuidestars')
		choice = widget_info(id,/droplist_select)
		if (choice eq 0) then lgs_altitude = 90*km ; Sodium Layer
		if (choice eq 1) then lgs_altitude = 1.e9	; Infinity
	id = widget_info(top,find_by_uname='droplist_scienceField')
                widget_control,id,get_value = choices
		choice = widget_info(id,/droplist_select)
		if (choice eq 0) then field_angle = 2.5*arcmin
		if (choice eq 1) then field_angle = 1*arcmin
		if (choice eq 2) then field_angle = 30*arcsec
		if (choice eq 3) then field_angle = 20*arcsec
		if (choice eq 4) then field_angle = 10*arcsec
	id = widget_info(top, FIND_BY_UNAME='textInput_numberOfScienceObjects')
		widget_control,id,get_value=text
		ngs = fix(text)
		if (ngs gt 0) then begin
			n_scienceStars = ngs[0]
		endif
        id = widget_info(top,find_by_uname='droplist_scienceWavelength')
                choice = widget_info(id,/droplist_select)
                widget_control,id,get_uvalue = scienceWavelengthChoices
                science_lambda = scienceWavelengthChoices[choice]*microns
;                if (choice eq 0) then science_lambda = 2.2*microns
;                if (choice eq 1) then science_lambda = 1.63*microns
;                if (choice eq 2) then science_lambda = 1.25*microns
;                if (choice eq 3) then science_lambda = 0.81*microns
;                if (choice eq 4) then science_lambda = 0.65*microns
;                if (choice eq 5) then science_lambda = 0.55*microns
;                if (choice eq 6) then the user has set this with an IDL command line. The GUI will only display the set value
        id = widget_info(top,find_by_uname='textInput_plateScale')
                widget_control,id,get_value = text
                plateScale = float(text[0])*arcsec
	id = widget_info(top,find_by_uname='droplist_iterationMethod')
		choice = widget_info(id,/droplist_select)
		if (choice eq 0) then iterationAlgorithm = 'PCG'
		if (choice eq 1) then iterationAlgorithm = 'Fixed'
	id = widget_info(top,find_by_uname='textInput_maxIterations')
		widget_control,id,get_value=text
		nit = fix(text[0])
                if (nit ge 0) then begin
                    n_iterations = nit
                endif else begin
                    n_iterations = 0
                    widget_control,id,set_value='0'
                endelse
        id = widget_info(top,find_by_uname='textInput_StopAt')
                widget_control,id,get_value=text
                nit = float(text[0])
                if (nit ge 0) then begin
                    stopAt = nit*nm ; number of nanometers
                endif else begin
                    stopAt = 0
                    widget_control,id,set_value='0'
                endelse
        id = widget_info(top,find_by_uname='textInput_nRealizations')
                widget_control,id,get_value=text
                nit = fix(text[0])
                if (nit gt 0) then begin
                    n_realizations = nit
                endif else begin
                    n_realizations = 1
                    widget_control,id,set_value='1'
                endelse
        id = widget_info(top,find_by_uname='textInput_nTimeSteps')
                widget_control,id,get_value=text
                nit = fix(text[0])
                if (nit gt 0) then begin
                    n_timeSteps = nit
                endif else begin
                    n_timeSteps = 1
                    widget_control,id,set_value='1'
                endelse
        id = widget_info(top,find_by_uname='textInput_deltaTime')
                widget_control,id,get_value=text
                nit = float(text[0])
                if (nit gt 0) then begin
                    delta_time = nit
                endif else begin
                    delta_time = 0
                    widget_control,id,set_value='0'
                endelse
	id = widget_info(top,find_by_uname='textInput_subapsAcross')
		widget_control,id,get_value=value
		nsa = fix(value[0])
		if ((nsa gt 0) and (nsa lt 500)) then begin
                    subapsAcross = nsa
                    D_subap = D_tele/float(subapsAcross)
		endif
	id = widget_info(top,find_by_uname='droplist_subsample')
		choice = widget_info(id,/droplist_select)
                subSampleSelections = [1,2,4]
                subSample = subSampleSelections[choice]
                du_tomog = D_subap
                du_atmos = du_tomog/float(subSample)
	id = widget_info(top,find_by_uname='checkButton_tipTiltUnknown_1')
		tipTiltUncertain = widget_info(id,/button_set)
	id = widget_info(top,find_by_uname='checkButton_warmRestart')
		warm_restart = widget_info(id,/button_set)
        id = widget_info(top,find_by_uname='textInput_r0')
                widget_control,id,get_value=text
                r0 = float(text[0])*cm
	id = widget_info(top,find_by_uname='droplist_Cn2Profile')
                widget_control,id,get_value = choices
		choice = widget_info(id,/droplist_select)
                model = choices[choice]
	id = widget_info(top,find_by_uname='seed_text')
		widget_control,id,get_value=text
		sd = fix(text[0])
		if (sd gt 0) then seed = sd
        id = widget_info(top,find_by_uname='droplist_Wind')
                widget_control,id,get_value = choices
                choice = widget_info(id,/droplist_select)
                windModel = choices[choice]
	id = widget_info(top,find_by_uname='text_runID')
		widget_control,id,set_value=strtrim(string(run_no),2)
        id = widget_info(Event.top,FIND_BY_UNAME='text_outputFilename')
                TomographySphericalWave_widget_setOutputDirectory, id
end

;-----------------------------------------------------------------
; Text Insert Character Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TEXT_CH, ID:0L, TOP:0L, HANDLER:0L, TYPE:0, OFFSET:0L,
;       CH:0B }

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   OFFSET is the (zero-based) insertion position that will result
;       after the character is inserted. CH is the ASCII value of the
;       character.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_Widget_numWFSGS, Event
	common constants
	common programSwitches
	common tomography
	common atmosphere
	common imaging
	common visualization

	id = Event.id
	widget_control,id,get_value=text
	ngs = fix(text)
	print,'<TomographySphericalWave_Widget_numWFSGS> n = ',ngs
	if (ngs ne 0) then begin
		n_guidestars = ngs[0]
	endif
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_widget_stopButton, Event
	common programSwitches

	print,'<TomographySphericalWave_widget_stopButton>'
	quitFlag = 1
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_exit, Event
	id = Event.top
	widget_Control,id,/destroy
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_checkSize, Event

  TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
  n_fine = calcFineGridSize()
  id = widget_info(Event.top,FIND_BY_UNAME='text_size')
  widget_control,id,set_value=strtrim(string(n_fine))
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_readParameterFile, Event
  common defaultDirectories

  if (n_elements(inputWorkingDirectory) eq 0) then inputWorkingDirectory = ''
  file = dialog_pickfile(path=inputWorkingDirectory,filter='*.sav',get_path=path,/read)
  if (file ne '') then inputWorkingDirectory = path
  info = file_info(file)
  if ((file ne '') and info.exists) then begin
      restoreDefaults,file
      tomographySphericalWave_widget_realize,Event.top
      id = widget_info(Event.top,find_by_uname='text_parameterFilename')
      widget_control,id,set_value = file
  endif
end
;
pro tomographySphericalWave_widget_readResultsFile, Event
  common defaultDirectories

  if (n_elements(inputWorkingDirectory) eq 0) then inputWorkingDirectory = ''
  file = dialog_pickfile(path=inputWorkingDirectory,filter='*.sav',get_path=path,/read)
  if (file ne '') then inputWorkingDirectory = path
  info = file_info(file)
  if ((file ne '') and info.exists) then loadResults,file
end
;
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_save, Event

end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_saveas, Event
  common defaultDirectories

  if (n_elements(inputWorkingDirectory) eq 0) then inputWorkingDirectory = ''
  TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
  file = dialog_pickfile(path=inputWorkingDirectory,filter='*.sav',get_path=path,/write)
  if (file ne '') then inputWorkingDirectory = path
  info = file_info(file)
  if ((file ne '')) then begin
      saveDefaults,file
      id = widget_info(Event.top,find_by_uname='text_parameterFilename')
      widget_control,id,set_value = file
  endif
end
;
pro tomographySphericalWave_widget_saveResults, Event
  common defaultDirectories

  if (n_elements(inputWorkingDirectory) eq 0) then inputWorkingDirectory = ''
  TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
  file = dialog_pickfile(path=inputWorkingDirectory,filter='*.sav',get_path=path,/write)
  if (file ne '') then inputWorkingDirectory = path
  info = file_info(file)
  if ((file ne '')) then saveResults,file
end
;
;-----------------------------------------------------------------
; Notify Realize Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_widget_realize, wWidget
;
; _realize sets up the GUI according to values set in the program
; (see also: _SynchronizeGUItoProgram)
;
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging

  top = wWidget
  print,'<TomographySphericalWave_widget_realize> top = ',top
  tomographySphericalWave_Widget_Message,top,versionText

  id = widget_info(wWidget, find_by_uname='button_forwardPropScienceDirections')
  widget_control,id,sensitive=0

  id = widget_info(top, FIND_BY_UNAME='droplist_aperture')
    choices = ['30 TMT', $
    	       '16 circle', $
               '10 circle', $
               '10 Keck', $
               '10 hexagon', $
               '8 circle', $
               '5 Palomar', $
               'other' ]
    widget_control,id,set_value = choices
    defaultChoice = where(choices eq 'other') ; default
    choice = defaultChoice
    if (D_tele eq 30) then choice = where(choices eq '30 TMT')
    if (D_tele eq 16) then choice = where(choices eq '16 circle')
    if (D_tele eq 10) then choice = where(choices eq '10 circle')
    if (pupil_shape eq 'Keck') then choice = where(choices eq '10 Keck')
    if (pupil_shape eq 'Hexagon') then choice = where(choices eq '10 hexagon')
    if (D_tele eq 8) then choice = where(choices eq '8 circle')
    if (D_tele eq 5) then choice = where(choices eq '5 Palomar')
    if (choice eq defaultChoice) then begin
        choices[choice] = strtrim(D_tele,2)
        widget_control,id,set_value = choices
    endif
    widget_control,id,set_droplist_select=choice

  id = widget_info(top, FIND_BY_UNAME='textInput_numberOfWFSGuidestars')
    widget_control,id,set_value=strtrim(string(n_guidestars),2)
  id = widget_info(top, FIND_BY_UNAME='droplist_constellationPattern')
    choices = ['circle', $
               'one in middle', $
               'two in middle', $
               'two circles', $
               'K-NGAO 10', $
               'K-NGAO 5', $
               'testbed-4', $
               'testbed-5']
    widget_control,id,set_value = choices
    choice = (where(pattern eq choices))[0]
    widget_control,id,set_droplist_select=choice

  id = widget_info(top, FIND_BY_UNAME='textInput_guidestarConstellationRadius')
    r = radius/arcsec
    widget_control,id,set_value=strtrim(string(float(r)),2)+' arcsec'

  id = widget_info(top, FIND_BY_UNAME='droplist_altitudeOfWFSGuidestars')
    if (lgs_altitude eq 90*km) then choice = 0
    if (lgs_altitude gt 90*km) then choice = 1
    widget_control,id,set_droplist_select=choice
  id = widget_info(top, FIND_BY_UNAME='droplist_scienceField')
    scienceFieldSelections = ['IRMOS 2.5 arcmin', $
                              '1 arcmin', $
                              'NFIRAOS 30 arcsec', $
                              '20 arcsec', $
                              '10 arcsec', $
                              'other']
    widget_control,id,set_value = scienceFieldSelections
    choice = 5 ; default choice, 'other'
    if (field_angle eq 2.5*arcmin) then choice=0
    if (field_angle eq 1*arcmin) then choice=1
    if (field_angle eq 30*arcsec) then choice=2
    if (field_angle eq 20*arcsec) then choice=3
    if (field_angle eq 10*arcsec) then choice=4
    widget_control,id,set_droplist_select=choice
  id = widget_info(top, FIND_BY_UNAME='textInput_numberOfScienceObjects')
    widget_control,id,set_value=strtrim(string(n_scienceStars),2)
  id = widget_info(top, FIND_BY_UNAME='button_spatialFilter')
    if (wfsType eq 'spatialFilter') then begin
        widget_control,id,/set_button
    endif else begin
        widget_control,id,set_button=0
    endelse
  id = widget_info(top, FIND_BY_UNAME='droplist_scienceWavelength')
    scienceWavelengthChoices = [2.2,1.63,1.25,0.81,0.65,0.55,0.]
    widget_control,id,set_uvalue = scienceWavelengthChoices
    scienceWavelengthSelections = ['K (2.2 microns)', $
                                   'H (1.63 microns)', $
                                   'J (1.25 microns)', $
                                   'I (0.81 microns)', $
                                   'R (0.65 microns)', $
                                   'V (0.55 microns)', $
                                   'other']
    choice = where(scienceWavelengthChoices eq science_lambda/microns)
;    if (science_lambda eq (2.2*microns)) then choice = 0
;    if (science_lambda eq (1.63*microns)) then choice = 1
;    if (science_lambda eq (1.25*microns)) then choice = 2
;    if (science_lambda eq (0.81*microns)) then choice = 3
;    if (science_lambda eq (0.65*microns)) then choice = 4
;    if (science_lambda eq (0.55*microns)) then choice = 5
    if (choice eq (-1)) then begin
        choice = 6
        scienceWavelengthSelections[6] = strtrim(string(format='(F6.2)',science_lambda/microns),2)+' microns'
        scienceWavelengthChoices[6] = science_lambda/microns
    endif
    widget_control,id,set_uvalue = scienceWavelengthChoices
    widget_control,id,set_value = scienceWavelengthSelections
    widget_control,id,set_droplist_select=choice
  id = widget_info(top, FIND_BY_UNAME='textInput_plateScale')
    widget_control,id,set_value=strtrim(string(float(plateScale/arcsec)),2)
  id = widget_info(top, FIND_BY_UNAME='label_diffractionLimit')
    dl_scale = strtrim(string((science_lambda/D_tele)/arcsec),2)
    widget_control,id,set_value='DL='+dl_scale
  id = widget_info(top,find_by_uname='droplist_iterationMethod')
    if (iterationAlgorithm eq 'PCG') then choice=0
    if (iterationAlgorithm eq 'Fixed') then choice=1
    widget_control,id,set_droplist_select=choice
  id = widget_info(top,find_by_uname='textInput_maxIterations')
    widget_control,id,set_value=strtrim(string(n_iterations),2)
  id = widget_info(top,find_by_uname='textInput_StopAt')
    widget_control,id,set_value=strtrim(string(stopAt/nm),2)
  id = widget_info(top,find_by_uname='textInput_subapsAcross')
    widget_control,id,set_value=strtrim(string(subapsAcross),2)
  id = widget_info(top,find_by_uname='droplist_subsample')
    subSampleSelections = ['1x','2x','4x']
    widget_control,id,set_value = subSampleSelections
    choice = 1 ; default choice
    if (subSample eq 1) then choice=0
    if (subSample eq 2) then choice=1
    if (subSample eq 4) then choice=2
    widget_control,id,set_droplist_select=choice
  id = widget_info(top,find_by_uname='checkButton_tipTiltUnknown_1')
    if (tipTiltUncertain) then begin
        widget_control,id,/set_button
    endif else begin
        widget_control,id,set_button=0
    endelse
  id = widget_info(top,find_by_uname='checkButton_warmRestart')
    if (warm_restart) then begin
        widget_control,id,/set_button
    endif else begin
        widget_control,id,set_button=0
    endelse
  id = widget_info(top,find_by_uname='textInput_r0')
    widget_control,id,set_value=strtrim(string(r0/cm),2)
  id = widget_info(top,find_by_uname='droplist_Cn2Profile')
    choices = ['Cerro Pachon','Bad-Bad','Test','Mauna Kea','CN-MK1','KPAO (Neyman)','NGAO (Flicker)','Phantom','PhantomGauss','Laag3Layer','other']
    widget_control,id,set_value = choices
    choice = (where(model eq choices))[0]
    widget_control,id,set_droplist_select=choice
  id = widget_info(top,find_by_uname='seed_text')
    widget_control,id,set_value=strtrim(string(fix(seed)),2)
  id = widget_info(top,find_by_uname='droplist_Wind')
    choices = ['random 10m/s','KPAO model']
    widget_control,id,set_value = choices
    choice = (where(windModel eq choices))[0]
    widget_control,id,set_droplist_select=choice
;    no more realization/time simulation buttons
;  id = widget_info(top,find_by_uname='button_Independent')
;    widget_control,id,set_button=1
  id = widget_info(top,find_by_uname='textInput_nRealizations')
    widget_control,id,set_value=strtrim(string(n_realizations),2)
  id = widget_info(top,find_by_uname='textInput_nTimeSteps')
    widget_control,id,set_value=strtrim(string(n_timeSteps),2)
  id = widget_info(top,find_by_uname='textInput_deltaTime')
    widget_control,id,set_value=strtrim(string(delta_time),2)
;  id = widget_info(top,find_by_uname='button_TimeSim')
;    widget_control,id,set_button=0
;  id = widget_info(top,find_by_uname='label_nTimeSteps')
;    widget_control,id,set_value='# realizations'
;  id = widget_info(top,find_by_uname='label_Time')
;    widget_control,id,set_value='Iter ='
  id = widget_info(top,find_by_uname='text_outputFilename')
  TomographySphericalWave_widget_setOutputDirectory, id
  TomographySphericalWave_widget_updateIteration,top,0
  id = widget_info(top, FIND_BY_UNAME='text_guiID')
    widget_control,id,set_value=strtrim(string(top),2)
    widget_id = top
  tomographySphericalWave_Widget_checkSize, {widget_button, id:top, top:top, handler:0L, select:0}
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro TomographySphericalWave_widget_outputDirectoryBrowse, Event
  forward_function TomographySphericalWave_widget_setOutputDirectory
  common constants
  common defaultDirectories
  if (n_elements(outputWorkingDirectory) eq 0) then begin
      cd,current=outputWorkingDirectory
  endif
  outputWorkingDirectory = dialog_pickFile(path=outputWorkingDirectory,/directory)
  id = widget_info(Event.top,FIND_BY_UNAME='text_outputFilename')
  TomographySphericalWave_widget_setOutputDirectory, id
end
;-----------------------------------------------------------------
pro TomographySphericalWave_widget_setOutputDirectory, id
  forward_function TomographySphericalWave_widget_generateOutputFilename, TomographySphericalWave_widget_dateStamp
  common constants
  common defaultDirectories
  if (n_elements(outputWorkingDirectory) eq 0) then begin
      cd,current=outputWorkingDirectory
      outputWorkingDirectory += path_sep()
  endif
  if (n_elements(run_no) eq 0) then run_no = 0
  filename = TomographySphericalWave_widget_generateOutputFilename(run_no)
  outputFilename = outputWorkingDirectory+filename
  widget_control,id,set_value=outputFilename
end
;-----------------------------------------------------------------
function TomographySphericalWave_widget_generateOutputFilename,run_no
	forward_function TomographySphericalWave_widget_dateStamp
  dateStamp = TomographySphericalWave_widget_dateStamp()
  filename = dateStamp+'run'+string(run_no,format='(i03)')
  return,filename
end
; -----------------------------------------------------------------
function TomographySphericalWave_widget_dateStamp
  date = bin_date(systime())
  year = string(date[0],format='(i4)')
  month = string(date[1],format='(i02)')
  day = string(date[2],format='(i02)')
  dateStamp = year+'_'+month+'_'+day+'_'
  return,dateStamp
end
;-----------------------------------------------------------------
; Text Insert Character Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TEXT_CH, ID:0L, TOP:0L, HANDLER:0L, TYPE:0, OFFSET:0L,
;       CH:0B }

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   OFFSET is the (zero-based) insertion position that will result
;       after the character is inserted. CH is the ASCII value of the
;       character.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_checkSubaps, Event
  if (Event.ch eq 10) then begin
      tomographySphericalWave_widget_checkSize, Event
  endif
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
;  we've eliminated the switch between independent realizations and
;  time simulations (they're now nested). So there is no such button
;pro tomographySphericalWave_Widget_IndependentScreens, Event
;  top = Event.top
;  id = Event.id
;  widget_control,id,set_button=1
;  id = widget_info(top,find_by_uname='button_TimeSim')
;    widget_control,id,set_button=0
;  id = widget_info(top,find_by_uname='label_nTimeSteps')
;    widget_control,id,set_value='# realizations'
;  id = widget_info(top,find_by_uname='label_Time')
;    widget_control,id,set_value='Iter ='
;end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
; we've eliminated the button-switch between independent realizations
; and time simulation (they are now nested).
;
;pro tomographySphericalWave_Widget_TimeSimulation, Event
;  top = Event.top
;  id = Event.id
;  widget_control,id,set_button=1
;  id = widget_info(top,find_by_uname='button_Independent')
;    widget_control,id,set_button=0
;  id = widget_info(top,find_by_uname='label_nTimeSteps')
;    widget_control,id,set_value='# time steps'
;  id = widget_info(top,find_by_uname='label_Time')
;    widget_control,id,set_value='Time ='
;end

;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_Widget_DoCases, Event
  go_cases
end
;-----------------------------------------------------------------
; Droplist Select Item Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_DROPLIST, ID:0L, TOP:0L, HANDLER:0L, INDEX:0L }
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   INDEX returns the index of the selected item. This can be used to
;       index the array of names originally used to set the widget's
;       value.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_checkDL, Event
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging

  top = event.top

  id = widget_info(top, FIND_BY_UNAME='droplist_aperture')
  choice = widget_info(id,/droplist_select)
  if (choice eq 0) then D_tele = 30
  if (choice eq 1) then D_tele = 10
  if (choice eq 2) then begin
      D_tele = 10
      pupil_shape = 'Keck'
  endif
  if (choice eq 3) then D_tele = 5

  id = widget_info(top,find_by_uname='droplist_scienceWavelength')
  choice = widget_info(id,/droplist_select)
  if (choice eq 0) then science_lambda = 2.2*microns
  if (choice eq 1) then science_lambda = 1.6*microns
  if (choice eq 2) then science_lambda = 1.2*microns
  if (choice eq 3) then science_lambda = 0.9*microns
  if (choice eq 4) then science_lambda = 0.7*microns
  if (choice eq 5) then science_lambda = 0.55*microns

  id = widget_info(top, FIND_BY_UNAME='label_diffractionLimit')
  dl_scale = strtrim(string((science_lambda/D_tele)/arcsec),2)
  widget_control,id,set_value='DL='+dl_scale
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_displayLastRealization, Event
;  TomographySphericalWave_Widget_SynchronizeGUItoProgram, Event
  displayDiagnostics
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_displayAveragePSFs, Event
  displayAveragePSFs
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_displayEncircledEnergy, Event
  displayEEplots
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_Widget_warmRestart, Event
  common programSwitches
  id = widget_info(Event.top,find_by_uname='checkButton_warmRestart')
  warm_restart = widget_info(id,/button_set)
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_reset, Event
  simulationReset
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_diagnosticOutput, Event
  saveTimeData,/display
  saveRealizationData,/display
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro tomographySphericalWave_widget_propGuidestarWaves, Event
  common constants
  common tomography
  common atmosphere

      phase_target = forwardPropagateGuideStars(screen_h)
      phest = forwardPropagateGuideStars(volume_est)
      pherr = phase_target-phest
      rr = 0
      for gs = 0,n_guidestars-1 do rr += (rms(pherr[*,*,gs],pupil_tomog)/microns)^2
      rr = sqrt(rr/float(n_guidestars))
      print,'<tomographySphericalWave_widget_propGuidestarWaves> rms meas phase error ',rr,' microns'
      if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateRMSerror,widget_id,rr*microns/nm

end
