; Case runs for Model Validation
;  --- first: Compile and run TomographySphericalWave.pro ---
;
common constants
common programSwitches
common tomography
common atmosphere
common imaging
common simulationState
common visualization
common diagnostics

D_tele = 10
pupil_shape = 'Hexagon'
n_guidestars = 4
pattern = 'one in middle' ; Flicker's "4c" constellation
radius = 35.0*arcsec ; 7,13,21,28,35, from Table 2
lgs_altitude = 90.*km
field_angle = 100*arcsec
science_pattern = 'line'
n_scienceStars = 36 ; evenly spaced in a quadrant of the field
science_lambda = 1.25*microns
plateScale = 0.01*arcsec
iterationAlgorithm = 'PCG'
stopAt = 1*nm
subSample = 1
tipTiltUncertain = 0
warm_restart = 1
r0 = 15.6*cm
model = 'NGAO (Flicker)'
seed = 5
windModel = 'KPAO model'
n_iterations = 12
n_realizations = 1
n_timeSteps = 6
delta_time = 0.
usePostFilter = 1
controlGain = 1.0

radius_set = reform(ones(3) ## [7.5,13,21,28,35],5*3)
n_guidestars_set = reform([4,5,7] ## ones(5),5*3)
n_cases = 15

dir = 'C:\Documents and Settings\Don Gavel\Desktop\TomographySphericalWaveFolder\'
resultsFile = dir + 'results.txt'
openw, unit,resultsFile,/get_lun,/append
printf,unit,'TomographySphericalWave results 4/2/2007 - model validation runs'
printf,unit,'Radial_Field_Angle/arcsec ...  Strehl ... RMS_WFE/nm ... sqrt(-ln(Strehl))/nm'
close,unit & free_lun,unit

case_results = fltarr(n_scienceStars,4)
for theCase = 0,n_cases-1 do begin

    radius = radius_set[theCase]*arcsec
    n_guidestars = n_guidestars_set[theCase]
    seed = 5
    simulationReset
    TomographySphericalWave_Widget_realize, widget_id
    TomographySphericalWave_Widget_doSimulation, {top: widget_id}
    printStatistics
    case_results(*,1:3) = strehlResults
    for star=0,n_scienceStars-1 do case_results[star,0] = sqrt(total(science_angle[star,*]^2))/arcsec
    openw,unit,resultsFile,/get_lun,/append
    printf,unit,'run case: ',theCase
    printf,unit,'radius = ',radius/arcsec
    printf,unit,'n_guidestars = ',n_guidestars
    printf,unit,'measurement consistency = ',consistency_rms/nm,' nm RMS'
    printf,unit,transpose(case_results)
    close,unit & free_lun,unit

endfor

end

