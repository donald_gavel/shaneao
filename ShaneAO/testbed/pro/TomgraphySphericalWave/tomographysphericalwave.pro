;+
;  tomographySphericalWave_vx.x.pro
;    simulate multi-guidestar tomography
;
;    Author: D.Gavel, UCSC
;    version 0 - spherical wave propagation, conformal map cones to columns
;    version 1 - callable sub-functions
;    version 2 -
;       a. Handle different Cn2 for reconstructor and "real" simulation parts
;       b. Add the overlap factor for finite aperture frequency
;       correlation
;    version 3
;    version 3a - fixed stretch factor scale error bug in forward
;                 science light propagation
;    version 4 - generalize the conformal mapping, add tip/tilt stars
;    version 4.1 - add PCG iteration
;    version 4.2 - added GUI front end
;    version 5.0 - structures replace common blocks
;                  divide "real world" simulation from the reconstructor
;                  get the warm restart to run correctly
;                  allow for wind and time-simulation
;                  assign min. aniso. correction to DMs
;                  incorporate tip/tilt/focus/astig stars (I hope)
;      release 1 - Fixed problem with specifying a single guidestar 9/1/06
;      release 2 - Fixed intermittant bug with subsampling 9/1/06
;      release 3 - Fixed problem with metapupil not being computed on cone
;                  Smarter algorithm for placing science objects on a grid instead of a line
;                  Put number-labels on the guidestars and science targets in the constellation display
;                  Fixed inconsistency errors with telescope menu selection, added a hexagon pupil shape option
;                  Removed display of log-scaled images, since disp now has that option
;                  Added an option to select LAO testbed constellation patterns
;                  Added KPAO and NGAO model Cn2 profiles and wind profiles
;                  Working on ability to read wavefront data from disk
;                  (work in progress)
;      release 6 - Fixed miscellaneous small bugs:
;                  Crash on single guidestar with subsampling
;                  Center of displayed psf not 0,0
;      release 7 - Post-Filtering to find the minimum variance
;                  solution (prior versions found the least-squares
;                  solution only)
;                  
;
;    principles of simulation:
;         - set up the problem (optionally with the help of a GUI)
;         - reset the positions of the wind screens and state of the controller
;         - Time simulation loop:
;            begin at time=0, let time progress with a uniform time step
;            take new data and process it in one of two types of algorithms:
;              - In open-loop control, take new measusrements and fit the
;                model to it as best as possible, possibly with old
;                estimates ar a starting point
;                      solves y = Ax where y = wfs measurement and x(0) = prior estimate
;              - In closed-loop control, (pseudo-open-loop method), do
;                as in open loop control but add to it the difference
;                between what is actually measured and what is modeled
;                to be measured
;                      solves y = e+Dx(0) = Ax where e = wfs
;                      measurement, D = Projections to DMs and x(0) = prior estimate
;            - Iteration loop: either fixed or conjugate gradient iteration
;            - Collect and store diagnostic data
;            - Write system state to disk on a clock-time periodic
;              basis (allows restart after crash)
;            - Accumlate science images
;          - Post-process
;            - Display relevant data and statistics
;            - Store run information, results, final state (allow for restart), and
;              optionally diagnostics to disk
;
;-
; ============= Basic Constants =====================
pro setConstants
  common constants, widget_id,run_no,versionText,cm,microns,nm,km,degrees,arcsec,arcmin,i
  if (n_elements(run_no) eq 0) then run_no = 0
  widget_id = 0L
  versionText = ['TomographySphericalWave','version 5.5r7','Don Gavel, UCSC']

  cm = 1.d-2
  microns = 1.d-6
  nm = 1.d-9
  km = 1.d3
  degrees = !pi/double(180)
  arcsec = !pi/double(180*3600.)
  arcmin = 60*arcsec
  i = dcomplex(0.d0,1.d0)
end
;
; ============== message to GUI ==================
function tomographySphericalWave_message,text  ; this can be a string or an array of strings
  common constants
  if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_Widget_Message,widget_id,text
end

; ============== Program Switches ===================
pro initTomography
  common constants
  common defaultDirectories, inputWorkingDirectory, outputWorkingDirectory, $
      parameterFileName, outputFilename
  common programSwitches, AO_mode, draw_pattern, compute_filters, apply_filters, $
      finite_aperture, generate_screens, display_volumes, display_psfs, display_eeplots, $
      print_statistics, analyze_variance, Cn2_unknown, usePostFilter, $
      matrix_degenerate, low_freq_matrix_degenerate, f_low_freq, doPreInversions, $
      regularization, low_freq_regularization, n_realizations, n_timeSteps, n_iterations, stopAt, $
      delta_time, iteration_gain, test_pattern, fourier_resample, doPhaseExtension, $
      noPiston, tipTiltUncertain, iterationAlgorithm, quitFlag, useOverlapFactors, $
      warm_restart, normalizeByIllumination, modelSubapertures, useConformalMap, zoomPsfs, $
      pupil_shape, tipTiltStars, simulateNoise, starList, labData
  common tomography, n_guidestars, radius, pattern, offset_angle, gs_angle, gs_separation, $
      tts_angle, tts_pattern, tipTiltCorrection, D_tele, $
      wfsType, subapsAcross, subSample, D_subap, phaseNoiseStdDev, noiseEquivalentAngle, noiseModel, $
      pupil_tomog, screenSize, du_tomog, dh, ngs_altitude, lgs_altitude, $
      n_tomog, ndm, dm_heights, dm_weights, dm_phase, A, C, postFilter, phase_wfs, phase_ft, filt_phase_ft, filt_phase, pherr, $
      hanning_window,volume_est,metapupil,fc, Cn2_tomog, snr
  common controller, phase_wfs_integral, controlGain
  common atmosphere, model, nlayers, seed, r0, Cn2, h, theta_0, lambda0, zenithAngle, $
      screen_h, du_atmos, n_atmos, m_atmos, pupil_atmos, phase, windModel, wind, base_screen, phasePlateFile, $
      benchScaleFactor, phasePlatePixelScale
  common imaging, n_scienceStars, field_angle,science_angle, science_pattern, science_lambda, uncorrected_phase, psf_open, $
      corrected_phase, psf_mcao, glao_phase, psf_glao, $
      scao_phase, psf_scao, psf_dl, average_psf_uncorrected, average_psf_corrected, $
      plateScale
  common simulationState, realization, time, timeStep, iteration, converged, phase0, forceColdRestart
  common visualization, metapupil_science
  common diagnostics, runstr,rms_log, strehlResults, consistency_rms
  common postProcessing, post_volume, post_volume_est, post_psf_mcao, post_plateScale, post_lambda, n_detector

  if ((size(science_angle))[0] eq 1) then science_angle = reform(science_angle,1,2)
;  screenSize = 2*max(h)*radius+D_tele ; actually done in createAtmosphere
;  n_tomog = nextpow2(fix(screenSize/du_tomog))
;  n_atmos = nextpow2(fix(screenSize/du_atmos)) ; prior calculated in createAtmosphere
  n_tomog = n_atmos/subSample
  n = n_tomog ; used locally
  fc = n*fc
  hanning_window = hanning(n,n,/double,alpha=0.5) ; Gibbs: high inital ring but fast die-down
  ndm = (size(dm_heights))[1]

  if (n_elements(volume_est) ne 0) then begin
      s = size(volume_est)
      if ((s[1] ne n) or (s[2] ne n) or (s[3] ne nlayers)) then volume_est = dblarr(n,n,nlayers)
  endif else begin
      volume_est = dblarr(n,n,nlayers)
  endelse

  createGuideStarConstellation,n_guidestars,pattern,radius,gs_angle,gs_separation,offset_angle

end
; ============= Default Parameters =================
pro defaultTomographyParameters
  common constants
  common programSwitches
  common tomography
  common controller
  common atmosphere
  common imaging
  common simulationState
  common visualization

  setConstants
  AO_mode = 'MOAO'
  draw_pattern = 1
  compute_filters = 1
  apply_filters = 1
  finite_aperture = 1
  generate_screens = 1
  display_volumes = 1
  display_psfs = 1
  display_eeplots = 0
  print_statistics = 1
  analyze_variance = 0
  Cn2_unknown = 0
  usePostFilter = 0
  doPhaseExtension = 0
  normalizeByIllumination = 0
  useOverlapFactors = 1
  seed = 5
  warm_restart = 0
  matrix_degenerate = 1.d-2       ; .01; 4.e-2;  1/condition at which matrix A is degenerated by la_least_squares
  low_freq_matrix_degenerate = 1.e-12 ; use this 1/condition at low frequencies - gives a better low freq fit
  f_low_freq = 20.5               ; 50.5; 4.5;  frequency below which to use low_freq_matrix_degenerate, in units of df (i.e. Fourier domain pixels)
  regularization = 5.e-3; 1.d-2          ;     Reqularlize A inverse by inverting (A+I*reqularization) instead
  low_freq_regularization = 5.e-3; 1.d-2
  doPreInversions = 1
  fc = 1.
  snr = 10.  ; signal to noise ratio for tomography PCG regularization
  delta_time = 1.
  n_realizations = 1
  n_timeSteps = 1
  n_iterations = 1
  stopAt = 1*nm
  iteration_gain = 0.25
  iterationAlgorithm = 'PCG' ; 'Fixed'
  test_pattern = 0
  fourier_resample = 1
  n_guidestars = 8
  radius = 2.5*arcmin ; 40*arcsec ; circular pattern of guidestars
  field_angle = 2.5*arcmin ; science field
  pattern = 'two circles' ; 'one_in_middle' or 'circle' or 'two in middle' or 'two circles'
  offset_angle = 10.*degrees
  simulateNoise = 0
  phaseNoiseStdDev = 0.3*microns
  noiseEquivalentAngle = 0.22*arcsec
  noiseModel = 'slope' ; 'slope' or 'phase'
  controlGain = 0.1
  tipTiltStars = 0
  tts_pattern = '4 TT Stars';'3 TT Stars' ; '1 TTFA Star'
  n_scienceStars = 3
  starList = 0
  science_angle = 0
  science_pattern = 'box'
  science_lambda = 1.238*microns; center of J band; 2.0*microns center of K band
  D_tele = 30.d0
  r0 = 20.*cm
  zenithAngle = 0
  wfsType = 'HartmannSupap' ; 'spatialFilter'
  subapsAcross = 64
  noiseStdDev = 0
  subSample = 1 ; fine grid pixels per subap
;  screenSize = 1.2*D_tele - don't know this yet until we find out the
;                            highest altitude screen in the model

  D_subap = D_tele/float(subapsAcross) ; these are derived parameters
  du_atmos = D_subap/float(subSample)
  du_tomog = du_atmos; D_subap

;
  pupil_shape = 'circle' ; 'square'; 'Keck'
  dh = du_tomog/radius ; this is the minimum resolvable screen altitude-spacing
  model = 'Cerro Pachon' ; 'Mauna Kea' or 'Cerro Pachon' or 'Test'
  windModel = 'default' ; some generic case. we'll add more later
  ngs_altitude = 1.e9 ; effectively infinity
  lgs_altitude = 90.*km;  altitude of laser guide star
  dm_heights = [0, 2.5, 5.0, 6.0]*km
  noPiston = 1
  tipTiltUncertain = 0
  quitFlag = 0
  modelSubapertures = 1
  useConformalMap = 0
  plateScale = .0035*arcsec ; 1/2 xDL for 30 meter at 1 micron

  forceColdRestart = 1

end
;
pro restoreDefaults,file
  restore,file
  if (n_elements(tomogDefaults) ne 0) then unpackParams,tomogDefaults
  if (n_elements(params) ne 0) then unpackParams,params
end
;
pro unpackParams,tomogDefaults
  common constants
  common programSwitches
  common tomography
  common controller
  common atmosphere
  common imaging
  common simulationState
  common visualization

  tags = tag_names(tomogDefaults)

  if total(strupcase('AO_mode') eq tags) then AO_mode = tomogDefaults.AO_mode
  if total(strupcase('draw_pattern') eq tags) then draw_pattern = tomogDefaults.draw_pattern
  if total(strupcase('compute_filters') eq tags) then compute_filters = tomogDefaults.compute_filters
  if total(strupcase('apply_filters') eq tags) then apply_filters = tomogDefaults.apply_filters
  if total(strupcase('finite_aperture') eq tags) then finite_aperture = tomogDefaults.finite_aperture
  if total(strupcase('generate_screens') eq tags) then generate_screens = tomogDefaults.generate_screens
  if total(strupcase('display_volumes') eq tags) then display_volumes = tomogDefaults.display_volumes
  if total(strupcase('display_psfs') eq tags) then display_psfs = tomogDefaults.display_psfs
  if total(strupcase('display_eeplots') eq tags) then display_eeplots = tomogDefaults.display_eeplots
  if total(strupcase('print_statistics') eq tags) then print_statistics = tomogDefaults.print_statistics
  if total(strupcase('analyze_variance') eq tags) then analyze_variance = tomogDefaults.analyze_variance
  if total(strupcase('Cn2_unknown') eq tags) then Cn2_unknown = tomogDefaults.Cn2_unknown
  if total(strupcase('usePostFilter') eq tags) then usePostFilter = tomogDefaults.usePostFilter
  if total(strupcase('doPhaseExtension') eq tags) then doPhaseExtension = tomogDefaults.doPhaseExtension
  if total(strupcase('normalizeByIllumination') eq tags) then normalizeByIllumination = tomogDefaults.normalizeByIllumination
  if total(strupcase('useOverlapFactors') eq tags) then useOverlapFactors = tomogDefaults.useOverlapFactors
  if total(strupcase('seed') eq tags) then seed = tomogDefaults.seed
  if total(strupcase('warm_restart') eq tags) then warm_restart = tomogDefaults.warm_restart
  if total(strupcase('matrix_degenerate') eq tags) then matrix_degenerate = tomogDefaults.matrix_degenerate
  if total(strupcase('low_freq_matrix_degenerate') eq tags) then low_freq_matrix_degenerate = tomogDefaults.low_freq_matrix_degenerate
  if total(strupcase('f_low_freq') eq tags) then f_low_freq = tomogDefaults.f_low_freq
  if total(strupcase('regularization') eq tags) then regularization = tomogDefaults.regularization
  if total(strupcase('low_freq_regularization') eq tags) then low_freq_regularization = tomogDefaults.low_freq_regularization
  if total(strupcase('doPreInversions') eq tags) then doPreInversions = tomogDefaults.doPreInversions
  if total(strupcase('fc') eq tags) then fc = tomogDefaults.fc
  if total(strupcase('delta_time') eq tags) then delta_time = tomogDefaults.delta_time
  if total(strupcase('n_realizations') eq tags) then n_realizations = tomogDefaults.n_realizations
  if total(strupcase('n_timeSteps') eq tags) then n_timeSteps = tomogDefaults.n_timeSteps
  if total(strupcase('n_iterations') eq tags) then n_iterations = tomogDefaults.n_iterations
  if total(strupcase('iteration_gain') eq tags) then iteration_gain = tomogDefaults.iteration_gain
  if total(strupcase('iterationAlgorithm') eq tags) then iterationAlgorithm = tomogDefaults.iterationAlgorithm
  if total(strupcase('test_pattern') eq tags) then test_pattern = tomogDefaults.test_pattern
  if total(strupcase('fourier_resample') eq tags) then fourier_resample = tomogDefaults.fourier_resample
  if total(strupcase('n_guidestars') eq tags) then n_guidestars = tomogDefaults.n_guidestars
  if total(strupcase('radius') eq tags) then radius = tomogDefaults.radius
  if total(strupcase('pattern') eq tags) then pattern = tomogDefaults.pattern
  if total(strupcase('simulateNoise') eq tags) then simulateNoise = tomogDefaults.simulateNoise
  if total(strupcase('phaseNoiseStdDev') eq tags) then phaseNoiseStdDev = tomogDefaults.phaseNoiseStdDev
  if total(strupcase('noiseEquivalentAngle') eq tags) then noiseEquivalentAngle = tomogDefaults.noiseEquivalentAngle
  if total(strupcase('noiseModel') eq tags) then noiseModel = tomogDefaults.noiseModel
  if total(strupcase('controlGain') eq tags) then controlGain = tomogDefaults.controlGain
  if total(strupcase('n_scienceStars') eq tags) then n_scienceStars = tomogDefaults.n_scienceStars
  if total(strupcase('field_angle') eq tags) then field_angle = tomogDefaults.field_angle
  if total(strupcase('offset_angle') eq tags) then offset_angle = tomogDefaults.offset_angle
  if total(strupcase('science_angle') eq tags) then science_angle = tomogDefaults.science_angle
  if total(strupcase('science_pattern') eq tags) then science_pattern = tomogDefaults.science_pattern
  if total(strupcase('science_lambda') eq tags) then science_lambda = tomogDefaults.science_lambda
  if total(strupcase('tipTiltStars') eq tags) then tipTiltStars = tomogDefaults.tipTiltStars
  if total(strupcase('tts_pattern') eq tags) then tts_pattern = tomogDefaults.tts_pattern
  if total(strupcase('D_tele') eq tags) then D_tele = tomogDefaults.D_tele
  if total(strupcase('r0') eq tags) then r0 = tomogDefaults.r0
  if total(strupcase('zenithAngle') eq tags) then zenithAngle = tomogDefaults.zenithAngle
  if total(strupcase('wfsType') eq tags) then wfsType = tomogDefaults.wfsType
  if total(strupcase('subapsAcross') eq tags) then subapsAcross = tomogDefaults.subapsAcross
  if total(strupcase('subSample') eq tags) then subSample = tomogDefaults.subSample
  if total(strupcase('pupil_shape') eq tags) then pupil_shape = tomogDefaults.pupil_shape
  if total(strupcase('model') eq tags) then model = tomogDefaults.model
  if total(strupcase('windModel') eq tags) then windModel = tomogDefaults.windModel
  if total(strupcase('ngs_altitude') eq tags) then ngs_altitude = tomogDefaults.ngs_altitude
  if total(strupcase('lgs_altitude') eq tags) then lgs_altitude = tomogDefaults.lgs_altitude
  if total(strupcase('dm_heights') eq tags) then dm_heights = tomogDefaults.dm_heights
  if total(strupcase('noPiston') eq tags) then noPiston = tomogDefaults.noPiston
  if total(strupcase('tipTiltUncertain') eq tags) then tipTiltUncertain = tomogDefaults.tipTiltUncertain
  if total(strupcase('quitFlag') eq tags) then quitFlag = tomogDefaults.quitFlag
  if total(strupcase('modelSubapertures') eq tags) then modelSubapertures = tomogDefaults.modelSubapertures
  if total(strupcase('useConformalMap') eq tags) then useConformalMap = tomogDefaults.useConformalMap
  if total(strupcase('plateScale') eq tags) then plateScale = tomogDefaults.plateScale

  forceColdRestart = 1
end
;
pro saveDefaults,file
  common constants
  common defaultDirectories
  common programSwitches
  common tomography
  common controller
  common atmosphere
  common imaging
  forward_function packParams

  tomogDefaults = packParams()
  save,tomogDefaults,filename=file
end
;
function packParams
  common constants
  common defaultDirectories
  common programSwitches
  common tomography
  common controller
  common atmosphere
  common imaging

  tomogDefaults = { $
  AO_mode : AO_mode, $
  draw_pattern : draw_pattern, $
  compute_filters : compute_filters, $
  apply_filters : apply_filters, $
  finite_aperture : finite_aperture, $
  generate_screens : generate_screens, $
  display_volumes : display_volumes, $
  display_psfs : display_psfs, $
  display_eeplots : display_eeplots, $
  print_statistics : print_statistics, $
  analyze_variance : analyze_variance, $
  Cn2_unknown : Cn2_unknown, $
  usePostFilter : usePostFilter, $
  doPhaseExtension : doPhaseExtension, $
  normalizeByIllumination : normalizeByIllumination, $
  useOverlapFactors : useOverlapFactors, $
  seed : seed, $
  warm_restart : warm_restart, $
  matrix_degenerate : matrix_degenerate , $
  low_freq_matrix_degenerate : low_freq_matrix_degenerate , $
  f_low_freq : f_low_freq   , $
  regularization : regularization , $
  low_freq_regularization : low_freq_regularization, $
  doPreInversions : doPreInversions, $
  fc : fc, $
  delta_time : delta_time, $
  n_realizations : n_realizations, $
  n_timeSteps : n_timeSteps, $
  n_iterations : n_iterations, $
  iteration_gain : iteration_gain, $
  iterationAlgorithm : iterationAlgorithm, $
  test_pattern : test_pattern, $
  fourier_resample : fourier_resample, $
  n_guidestars : n_guidestars, $
  radius : radius , $
  pattern : pattern, $
  n_scienceStars : n_scienceStars, $
  field_angle : field_angle , $
  offset_angle : offset_angle, $
  simulateNoise : simulateNoise, $
  phaseNoiseStdDev : phaseNoiseStdDev, $
  noiseEquivalentAngle : noiseEquivalentAngle, $
  noiseModel : noiseModel, $
  controlGain : controlGain, $
  science_angle : science_angle , $
  science_pattern : science_pattern, $
  science_lambda : science_lambda, $
  tipTiltStars : tipTiltStars, $
  tts_pattern : tts_pattern, $
  D_tele : D_tele, $
  r0 : r0, $
  zenithAngle : zenithAngle, $
  wfsType : wfsType, $
  subapsAcross : subapsAcross, $
  subSample : subSample, $
  pupil_shape : pupil_shape, $
  model : model, $
  windModel : windModel, $
  ngs_altitude : ngs_altitude , $
  lgs_altitude : lgs_altitude , $
  dm_heights : dm_heights, $
  noPiston : noPiston, $
  tipTiltUncertain : tipTiltUncertain, $
  quitFlag : quitFlag, $
  modelSubapertures : modelSubapertures, $
  useConformalMap : useConformalMap, $
  plateScale : plateScale }

  return,tomogDefaults
end

; ===========================================================
pro drawGuideStarConstellation, gs_angle,science_angle,field_angle,tts_angle,tipTiltStars=tipTiltStars
  common constants
  scienceColor = 180 ; a redish hue in colortable 5, grey in colortable 1
  window,/free,xsize=500,ysize=500,title='constellation [run '+strtrim(string(run_no),2)+']'
  fov = 1.5*max([max(science_angle),max(gs_angle)])/arcsec
  plot,-gs_angle[*,0]/arcsec,-gs_angle[*,1]/arcsec,psym=2, $
       xrange=[-fov,fov],yrange=[-fov,fov],xtitle='arcsec',ytitle='arcsec'
  oplot,-[science_angle[*,0]/arcsec],-[science_angle[*,1]/arcsec],psym=6,color=scienceColor
  ngs = (size(gs_angle))[1]
  for gs = 0,ngs-1 do begin
    xyouts,-gs_angle[gs,0]/arcsec,-gs_angle[gs,1]/arcsec,strtrim(gs,2)
  endfor
  n_scienceStars = (size(science_angle))[1]
  for s = 0,n_scienceStars-1 do begin
    xyouts,-science_angle[s,0]/arcsec,-science_angle[s,1]/arcsec,strtrim(s,2),color=scienceColor
  endfor
  if (tipTiltStars) then begin
    oplot,-[tts_angle[*,0]/arcsec],-[tts_angle[*,1]/arcsec],psym=1
    ntts = (size(tts_angle))[1]
    for ts = 0,ntts-1 do begin
      xyouts,-tts_angle[ts,0]/arcsec,-tts_angle[ts,0]/arcsec,strtrim(ts,2)
    endfor
  endif
  npts = 20
  if (n_elements(field_angle) eq 0) then return
  x = fltarr(npts+1)
  y = fltarr(npts+1)
  th = findgen(npts+1)*(2*!pi/float(npts))
  r = field_angle/arcsec
  x = r*cos(th)
  y = r*sin(th)
  oplot,x,y,color=scienceColor
end
; ===========================================================
pro createGuideStarConstellation,n_guidestars,pattern,radius,gs_angle,gs_separation,offset_angle
  common constants
  common programSwitches

  gs_angle = dblarr(n_guidestars,2)
  small_angle = offset_angle; 22.5*degrees; 1.d-2

  if (pattern eq 'two circles') then begin
  	r = (sqrt(3.)/4.)*radius
  	r = (1./2.)*radius
  	for gs = 0,2 do begin
  		az = 2*!pi*gs/float(3)+small_angle
  		gs_angle[gs,0] = r*cos(az)
  		gs_angle[gs,1] = r*sin(az)
  	endfor
  	for gs = 3,n_guidestars-1 do begin
  		az = 2*!pi*(gs+0.5)/float(n_guidestars-3)+small_angle
  		gs_angle[gs,0] = radius*cos(az)
  		gs_angle[gs,1] = radius*sin(az)
  	endfor
  endif

	if (pattern eq 'two in middle') then begin
  	r = (sqrt(2.)/4.)*radius
  	for gs = 0,1 do begin
  		az = 2*!pi*gs/float(2)+small_angle
  		gs_angle[gs,0] = r*cos(az)
  		gs_angle[gs,1] = r*sin(az)
  	endfor
  	for gs = 2,n_guidestars-1 do begin
  		az = 2*!pi*(gs+0.5)/float(n_guidestars-2)+small_angle
  		gs_angle[gs,0] = radius*cos(az)
  		gs_angle[gs,1] = radius*sin(az)
  	endfor
	endif

  if (pattern eq 'random') then begin
    gs_angle[0,0] = 0.
    gs_angle[0,1] = 0.
    for gs = 1,n_guidestars-1 do begin
      az = 2*!pi*gs/float(n_guidestars-1)+small_angle
      gs_angle[gs,0] = radius*cos(az)+(2*randomu(seed)-1.)*radius*.3
      gs_angle[gs,1] = radius*sin(az)+(2*randomu(seed)-1.)*radius*.3
    endfor
  endif

  if ( (pattern eq 'one in middle') or (pattern eq 'circle')) then begin
	  if (pattern eq 'one in middle') then begin
	     n_in_ring = n_guidestars - 1
	     gs = n_guidestars - 1
	     gs_angle[gs,0] = 0
	     gs_angle[gs,1] = 0
	  endif else begin
	     n_in_ring = n_guidestars
	  endelse
	  for gs = 0,n_in_ring-1 do begin
	    az = 2*!pi*gs/float(n_in_ring)+small_angle
	    gs_angle[gs,0] = radius*cos(az)
	    gs_angle[gs,1] = radius*sin(az)
	  endfor
   endif

   if (pattern eq 'testbed-5') then begin
	  n_in_ring = n_guidestars - 1
	  gs = n_guidestars - 1
	  gs_angle[gs,0] = 0
	  gs_angle[gs,1] = 0
	  for gs = 0,n_in_ring-1 do begin
	    az = 2*!pi*gs/float(n_in_ring)-45*degrees; small_angle... modified for testbed data
	    gs_angle[gs,0] = radius*cos(az)
	    gs_angle[gs,1] = radius*sin(az)
	  endfor
	  ; reshuffle the guide star positions [UL,UR,LL,LR,C] from [UL,LL,LR,UR,C]
	  gs_angle = gs_angle[[0,3,1,2,4],*]
   endif

   if (pattern eq 'testbed-4') then begin
	  n_in_ring = n_guidestars
	  for gs = 0,n_in_ring-1 do begin
	    az = 2*!pi*gs/float(n_in_ring)-45*degrees; small_angle... modified for testbed data
	    gs_angle[gs,0] = radius*cos(az)
	    gs_angle[gs,1] = radius*sin(az)
	  endfor
	  ; reshuffle the guide star positions [UL,UR,LL,LR] from [UL,LL,LR,UR]
	  gs_angle = gs_angle[[0,3,1,2],*]
   endif

   if (pattern eq 'K-NGAO 5') then begin
       n_guidestars = 5
       gs_angle = dblarr(n_guidestars,2)
       gs_angle[0,*] = [0,0]
       for gs = 1,n_guidestars-1 do begin
           az = (!pi/2)*gs+small_angle
           gs_angle[gs,*] = radius*[cos(az),sin(az)]
       endfor
   endif

   if (pattern eq 'K-NGAO 10') then begin
       n_guidestars = 10
       gs_angle = dblarr(n_guidestars,2)
       for gs = 0,3 do begin
           az = !pi/4+(!pi/2)*gs+small_angle
           gs_angle[gs,*] = (radius*.4)*[cos(az),sin(az)]
       endfor
       for gs = 4,9 do begin
           az = 2*!pi*(gs-4)/6.+small_angle
           gs_angle[gs,*] = radius*[cos(az),sin(az)]
       endfor
   endif

  ; determine the smallest separation
  if (n_guidestars gt 1) then begin ; determine the smallest separation
      dif = fltarr((n_guidestars*(n_guidestars-1))/2)
      k = 0
      for gs1 = 0,n_guidestars-1 do begin
          for gs2 = gs1+1,n_guidestars-1 do begin
              dif[k] = sqrt((gs_angle[gs1,0]-gs_angle[gs2,0])^2+(gs_angle[gs1,1]-gs_angle[gs2,1])^2)
              k = k+1
          endfor
      endfor
      gs_separation = min(dif)
  endif else begin
      gs_separation = 1.0
  endelse
end
;
; =========================================================
pro createTipTiltStarConstellation
    common constants
    common tomography
    common imaging

    if (tts_pattern eq '3 TT Stars') then begin
        tts_angle = dblarr(3,2)
        r = (2./3.)*radius
        for gs = 0,2 do begin
            az = 2*!pi*(gs+.25)/float(3)+offset_angle
            tts_angle(gs,0) = r*cos(az)
            tts_angle(gs,1) = r*sin(az)
        endfor
    endif
    if (tts_pattern eq '4 TT Stars') then begin
        tts_angle = dblarr(4,2)
        r = (2./3.)*radius
        for gs = 0,2 do begin
            az = 2*!pi*(gs+.25)/float(3)+offset_angle
            tts_angle(gs,0) = r*cos(az)
            tts_angle(gs,1) = r*sin(az)
        endfor
        tts_angle(gs,0) = 0.
        tts_angle(gs,1) = 0.
    endif
    if (tts_pattern eq '1 TTFA Star') then begin
        tts_angle = dblarr(1,2)
        tts_angle(gs,0) = 0
        tts_angle(gs,1) = 0
    endif
end
; ======================================================
pro createScienceConstellation
    common constants
    common programSwitches
    common tomography
    common imaging

    if (starList) then return
    n = n_scienceStars
    science_angle = fltarr(n,2)

    if (science_pattern eq 'box') then begin
        nsr = ceil(sqrt(n))
        k = 0
        for ix = 0,nsr-1 do begin
            for iy = 0,nsr-1 do begin
                if (k ge n) then break
                science_angle[k,0] = ix
                science_angle[k,1] = iy
                k++
            endfor
        endfor
        science_angle *= field_angle/float(nsr-1)
    endif

    if (science_pattern eq 'line') then begin
        dangle = field_angle/(float(n)-1)
        azAngle = 0*degrees; 45*degrees
        science_angle = findgen(n)*dangle # [cos(azAngle),sin(azAngle)]
    endif

    ;small_angle = offset_angle
    ;science_angle = fltarr(n_scienceStars,2)
    ;az = 2.*!pi/6.+small_angle
    ;for star = 0,n_scienceStars-1 do begin
    ;    r = (star/float(n_scienceStars-1))*field_angle
    ;    science_angle[star,*] = r*[cos(az),sin(az)]
    ;endfor

end
; ====================================================
pro createAtmosphere
  common constants
  common programSwitches
  common tomography
  common atmosphere

  plateFile = ''

  if ((model eq 'Phantom') or (model eq 'PhantomGauss')) then begin
      layer_height = [0, 3, 6, 9, 12]*km
      percent_atmos = [1,1,1,1,1]/5.
  endif

  if (model eq 'Test') then begin
;    r0 = 20.*cm
    layer_height = [0, 6, 13.3]*km
    percent_atmos =[19.5, 13, 11]
    percent_atmos /= total(percent_atmos)
  endif

  if (model eq 'Laag3Layer') then begin ; This models the LAO MCAO testbed
    r0 = 46.*cm
    layer_height = [0, 4.5, 9.0]*km
    percent_atmos =[650, 172, 25.]/1000.d0
    percent_atmos /= total(percent_atmos)
    phasePlateFile = 'phasePlate_byte.sav'
    benchScaleFactor = 30./.0266 ; Meters of aperture on sky per beam width on testbed
    phasePlatePixelScale = 40.*microns
  endif
;
  if (model eq 'Bad-Bad') then begin
;      r0 = 7.26*cm
      layer_height = [0.,25,50,100,200,400,800,1600,3400,6000,7600,13300,16000]
      percent_atmos = [.1164,.0911,.1290,.1333,.0869,.0545,.0615,.0571,.1687,.0671,.0240,.0065,.0041]
                                ; condense - can't resolve under ~800 m vertically with du=50cm
      layer_height = [0.,800,1600,3400,6000,7600,13300,16000]
      percent_atmos = [.1164+.0911+.1290+.1333+.0869+.0545,.0615,.0571,.1687,.0671,.0240,.0065,.0041]
  endif

  if (model eq 'KPAO (Neyman)') then begin
;      r0 = 18*cm
      layer_height = [0.0, 2.1, 4.1, 6.5, 9.0, 12.0, 14.8]*km
      percent_atmos = [0.4707,0.1839,0.1066,0.0845,0.0383,0.0932,0.0228]
  endif

  if (model eq 'NGAO (Flicker)') then begin
;      r0 = 15.6*cm
      layer_height = [0, 0.5, 1, 2, 4, 8, 16]*km
      percent_atmos = [.67,.05,.062,.051,.078,.051,.045]
  endif

  if (model eq 'Cerro Pachon') then begin
;    r0 = 16.*cm
    layer_height = [0., 2.58, 5.16, 7.73, 12.89, 15.46]*km
    percent_atmos = [652, 172, 55, 25, 74, 22]/1000d0
  endif

  if (model eq 'Cerro Pachon 20') then begin
;    r0 = 20.*cm
    layer_height = [0., 2.58, 5.16, 7.73, 12.89, 15.46]*km
    percent_atmos = [652, 172, 55, 25, 74, 22]/1000d0
  endif

  if (model eq 'Mauna Kea') then begin
;    r0 = 20.*cm
    layer_height = [0.09, 1.826, 2.72, 4.256, 6.269, 8.34, 10.546, 12.275, 14.61, 16.471, 17.028]*km
    percent_atmos = [3, 136, 163, 161, 167, 235, 68, 32, 23, 6, 7]/1000.d0
    layer_height_new = findgen(8)*2.5*km
    u = interpol(percent_atmos,layer_height,layer_height_new,/spline)>0
    percent_atmos = u/total(u)
    layer_height = layer_height_new
  endif

  if (model eq 'CN-MK1') then begin
;      r0 = 18.*cm
      layer_height = [0., 2.1, 4.1, 6.5, 9.0, 12.0, 14.8]*km
      percent_atmos = [47, 18.39, 10.66, 8.45, 3.83, 9.32, 2.35]/100.
  endif

  if (model eq 'other') then begin ; presumes h and Cn2 are pre-set
      layer_height = h
      percent_atmos = Cn2/total(Cn2)
  endif

  layer_height /= cos(zenithAngle)
  lambda0 = 0.5*microns
  k = 2*!pi/lambda0
; r0 := (.423*k^2*integral(Cn2))^(-3/5)
; => Cn2 = r0^(-5./3.)/(.423*k^2)
  c0 = (1./cos(zenithAngle))*r0^(-5./3.)/(.423*k^2)
  Cn2 = c0*percent_atmos

;  c0 = total(Cn2) - do this only in tomography calculations and use Cn2_tomog
;   make sure we are achieving the desired r0 with the atmosphere
;  r0_h = r0*(percent_atmos)^(-3./5.)
;  r0check = (total(r0_h^(-5./3.)))^(-3./5.)

  h = layer_height
  theta_0 = (2.905*k^2*total(Cn2*h^(5./3.)))^(-3./5.)
  nlayers = (size(layer_height))[1]

;  hanning_window = hanning(n,n,/double,alpha=0.5) ; Gibbs: high inital ring but fast die-down
;  hamming_window = hanning(n,n,/double,alpha=0.54); Gibbs: low ringing but slow die-down

  screenSize = 2*max(h)*radius+D_tele
  n_atmos = nextpow2(fix(screenSize/du_atmos))
  if (Cn2_unknown) then begin
      Cn2_tomog = ones(nlayers)*c0/float(nlayers) ; assume evenly distributed
  endif else begin
      Cn2_tomog = Cn2           ; assume we know the Cn2 profile
  endelse

  createWindProfile
end

; =========== calculate the fine grid size =============
function calcFineGridSize
  common atmosphere
  common tomography

  if (n_elements(h) eq 0) then createAtmosphere
  D_subap_local = D_tele/float(subapsAcross)
  du_atmos_local = D_subap_local/float(subSample)
  screenSize_local = 2*max(h)*radius+D_tele
  return, nextpow2(fix(screenSize_local/du_atmos_local))

end
; ============== createWindProfile ==================
pro createWindProfile
  common programSwitches
  common tomography
  common atmosphere
  common imaging

  if (windModel eq 'random 10m/s') then begin
      wind = fltarr(nlayers,2)
      wind = 10*randomn(seed,nlayers,2) ; std deviation of 10 m/sec wind speeds uniformly distributed in direction
  endif

  if (windModel eq 'KPAO model') then begin
      wind = fltarr(7,2)
      wind[*,0] = [6.7, 13.9, 20.8, 29.0, 29.0, 29.0, 29.0]
  endif

;  --- calculate the length of the screens ----
  windSpeed = sqrt([1,1] ## (wind*wind))
  maxwind = max(windSpeed)
  tf = (n_timeSteps-1)*delta_Time
  xf = tf*maxwind
  m_atmos = nextPow2( fix(xf/du_atmos) + n_atmos )

end

; =============== form the Tomography back-propagation filters ===============
pro formBackpropFilters
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  forward_function overlapFactors

  if (finite_aperture and useOverlapFactors) then begin
    tau0 = transpose(overlapFactors())
  endif else begin
    tau0 = dblarr(nlayers,n_guidestars,n_guidestars)+1.0d0
  endelse
  print,'<formBackpropFilters> computing tomography back-prop filters' & wait,.001
  n = n_tomog
  du = du_tomog
  A = dcomplexarr(n_guidestars,n_guidestars,n,n)
  C = dcomplexarr(n_guidestars,n,n)
  i = dcomplex(0.0d0,1.0d0)
  c0 = total(Cn2_tomog)
  z = lgs_altitude
  theta = gs_angle
  theta_science = reform(science_angle[0,*])
  df = double(1./(n*du))
  df_prime = df
  Cn2_scaled = Cn2_tomog
  if (useConformalMap) then begin
      df_prime *= (z/(z-h))
      Cn2_scaled *= ((z-h)/z)^(-5./3.)
      c0 = total(Cn2_scaled)
  endif

  mask = ones(n,n)
  mask[n/2,n/2] = 0.
  mask[0,*] = 0.
  mask[*,0] = 0.

  if (usePostFilter) then begin
      fx = (ones(n) ## dindgen(n) - n/2)*df
      fy = transpose(fx)
      f2 = fx^2+fy^2
      p = 1.2; 0.9; 11./6.
      postFilter = (1./(f2^p*mask + (1-mask)))*mask
  endif else begin
      postFilter = ones(n,n)*mask
  endelse

  percent_complete = 0
  printcount = 0
  for ifx = 0,n-1 do begin
    fx = double(ifx-n/2)*df_prime
    for ify = 0,n-1 do begin
      fy = double(ify-n/2)*df_prime
      for k = 0,n_guidestars-1 do begin
        ftheta = fx*(theta_science[0]-theta[k,0])+fy*(theta_science[1]-theta[k,1])
        C[k,ifx,ify] = total(Cn2_scaled*exp(2*!pi*i*ftheta*h))/c0
        for kp = 0,k do begin ; n_guidestars-1 do begin
          ftheta = fx*(theta[k,0]-theta[kp,0])+fy*(theta[k,1]-theta[kp,1])
          A[k,kp,ifx,ify] = postFilter[ifx,ify]*total(Cn2_scaled*exp(-2*!pi*i*ftheta*h)*tau0[*,k,kp])/c0
          A[kp,k,ifx,ify] = conj(A[k,kp,ifx,ify])
        endfor
      endfor
;
;  do matrix inversions here
      if (doPreInversions) then begin
          f = sqrt((float(ifx-n/2))^2+(float(ify-n/2))^2)
          rc = matrix_degenerate
          reg = regularization
          if (f lt f_low_freq) then begin
              rc = low_freq_matrix_degenerate
              reg = low_freq_regularization
          endif
          bb = complex(identity(n_guidestars))
          aa = reform(conj(A[*,*,ifx,ify]),n_guidestars,n_guidestars)
          ar = aa + (reg+(f/fc)^10)*identity(n_guidestars)
          aa = ar ; ******** test ******
          aa = reform(aa,n_guidestars,n_guidestars)
;          if (ifx ne n/2 or ify ne n/2) then begin  ; DG 3/31/07
          if (mask[ifx,ify]) then begin
              la_choldc,aa,/double,status=status
              aa = reform(aa,n_guidestars,n_guidestars)
              if (status eq 0) then begin
                  A[*,*,ifx,ify] = la_cholsol(aa,bb,/double)
              endif else begin
                  la_choldc,ar,/double,status=status
                  ar = reform(ar,n_guidestars,n_guidestars)
                  A[*,*,ifx,ify] = la_cholsol(ar,bb,/double)
                  if (status ne 0) then begin
                      if (printcount lt 10) then print,'<formBackpropFilters> choldc status = ',status,' [ifx,ify] = ',ifx,ify
                      if (printcount eq 10) then print,'<formBackpropFilters> plus more errors'
                      printcount++
                  endif
                  A[*,*,ifx,ify] = la_least_squares(ar,bb,method=2,rcondition=rc)*exp(-(f/fc)^10) ; ai[*,*,ifx,ify] ## x
              endelse
          endif else begin
              A[*,*,ifx,ify] = 0
          endelse
      endif
;
    endfor

    chr = get_kbrd(0)
    if (strupcase(chr) eq "Q") then begin
    	print,'<formBackpropFilters> ***filter calculations aborted***'
    	quitFlag = 1
    	break
    endif
    if (n_elements(percent_done) eq 0) then percent_done = -1
    new_percent_done = fix((ifx/float(n))*20.)
    if (new_percent_done ne percent_done) then begin
      if (new_percent_done eq 0) then print,format='($,"percent complete: ")'
      print,format='($,I0," ")',new_percent_done*5
      percent_done = new_percent_done
      wait,.001
    endif

  endfor
  if (printcount ne 0) then print,'<formBackpropFilters> ',printcount,' total errors'
  print,' '
  if (quitFlag eq 0) then print,'<formBackpropFilters> filters complete'
  return
end

; ====== calculate a set of overlap factors for pairs of guide star metapupils =====
function overlapFactors
  common programSwitches
  common constants
  common tomography
  common atmosphere
  common visualization
  print,'<overlapFactors> computing overlap factors' & wait,.001
  n = n_tomog
  du = du_tomog
;  pupil = circle(n,n,n/2,n/2,D_tele/(2.*du),1.)
  pupil_f = ft(pupil_tomog)
  normFactor = total(pupil_tomog)
  pupil_s = dblarr(n,n,n_guidestars)
  tau0 = dblarr(n_guidestars,n_guidestars,nlayers)
  z = lgs_altitude
  for layer = 0,nlayers-1 do begin
      du_prime = du
      if (useConformalMap) then du_prime *= ((z-h[layer])/z)
      df_prime = 1/(n*du_prime)
      fx = ones(n) ## (dindgen(n)-n/2)*df_prime
      fy = transpose(fx)
       for gs = 0,n_guidestars-1 do begin
         pupil_f1 = pupil_f*exp(i*2*!pi*(fx*gs_angle[gs,0]+fy*gs_angle[gs,1])*h[layer])
         pupil_s[*,*,gs] = ft(pupil_f1,/inverse)
       endfor
       for k = 0,n_guidestars-1 do begin
         for kp = k,n_guidestars-1 do begin
          tau0[k,kp,layer] = total(pupil_s[*,*,k]*pupil_s[*,*,kp])/normFactor
          tau0[kp,k,layer] = tau0[k,kp,layer]
         endfor
       endfor
    endfor
  return, tau0
end

; =========== create turbulent screens on conformal map ==============
pro createAtmosphericPhaseScreens
  common constants
  common programSwitches
  common tomography
  common atmosphere

  forward_function blowingScreen_init
  blowingScreen
  n = n_atmos
  m = m_atmos
  du = du_atmos
  print,'<createAtmosphericPhaseScreens> creating turbulent phase screens' & wait,.001
  z = lgs_altitude
  base_screen = dblarr(m+n,n,nlayers)
  screen_h = dblarr(n,n,nlayers)
  percent_atmos = Cn2/total(Cn2)
  r0_h = r0*(cos(zenithAngle))^(3./5.)*(percent_atmos)^(-3./5.)
  if (model eq 'Phantom') then begin
    base_screen[0:n-1,*,2] = circle(n,n,n/2,n/2,5,1.)*microns ; put a pattern on a layer
    return
  endif
  if (model eq 'PhantomGauss') then begin
      base_screen[0:n-1,*,2] = gauss2(n,n/2,n/2,5)*microns ; a smoother pattern, better for Hartmann sensor models
      return
  endif
  for layer = 0,nlayers-1 do begin
    base_screen[*,*,layer] = blowingScreen_init(n,m,r0_h[layer],du,seed=seed) * (lambda0/(2*!pi))
  endfor
end

; ========= make metapupil, for visualization purposes =========
pro makeMetapupil
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common visualization

  n = n_tomog
  du = du_tomog
  print,'<makeMetapupil> computing guidestar metapupils' & wait,.001
  if (pupil_shape eq 'circle') then pupil = circle(n,n,n/2,n/2,D_tele/(2.*du),1.)
  if (pupil_shape eq 'square') then pupil = rectangle(n,n,n/2,n/2,D_tele/du,D_tele/du,1.)
  if (pupil_shape eq 'Keck') then pupil = KeckAp(n,du=du)
  if (pupil_shape eq 'Hexagon') then begin
  	L = 10.95/2. ; maximum radius of Keck primary
  	pupil = Hex(n,L/du)
  	pupil = rotate(pupil,1)
  endif
  pupil_f = ft(pupil)
  metapupil = fltarr(n,n,nlayers)
  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  z = lgs_altitude
  i = complex(0.,1.)
  code = 1 ; scheme for labeling each metapupil
  for gs = 0,n_guidestars-1 do begin
    print,format='($,I0," ")',gs & wait,.001
    for layer = 0,nlayers-1 do begin
      shx = h[layer]*gs_angle[gs,0]
      shy = h[layer]*gs_angle[gs,1]
      du_prime = du
      if (useConformalMap) then du_prime *= ((z-h[layer])/z)
      df_prime = 1/(n*du_prime)
      fx = ones(n) ## (dindgen(n)-n/2)*df_prime
      fy = transpose(fx)
;
;     shift the metapupil
;
      if (not useConformalMap) then begin
          D = D_tele*(z-h[layer])/z
;          metapupil_l = pupil ; circle(n,n,n/2,n/2,D/(2.*du),1.)
          du_prime = du*(z-h[layer])/z
          ix = (x/du_prime + n/2)>0<(n-1)
          iy = (y/du_prime + n/2)>0<(n-1)
          metapupil_l = interpolate(pupil,ix,iy)
          metapupil_f = ft(metapupil_l)
      endif else begin
          metapupil_f = pupil_f
      endelse
      metapupilh = ft(metapupil_f*exp(i*2*!pi*(fx*shx+fy*shy)),/inverse)
      metapupilh = round(real(metapupilh))
      metapupil[*,*,layer] = metapupil[*,*,layer] + metapupilh*code
    endfor
    code = 2*code
  endfor
  print,'done'
;     science metapupil
  print,'<makeMetapupil> computing science metapupil' & wait,.001
  metapupil_science = fltarr(n,n,nlayers)
  n_scienceStars = (size(science_angle))[1]
  code = 1 ; scheme for labeling each metapupil
  for gs = 0,n_scienceStars - 1 do begin
    print,format='($,I0," ")',gs & wait,.001
	  for layer = 0,nlayers-1 do begin
	    shx = h[layer]*science_angle[gs,0]
	    shy = h[layer]*science_angle[gs,1]
	    du_prime = du
	    if (useConformalMap) then du_prime *= ((z-h[layer])/z)
	    df_prime = 1./(n*du_prime)
	    fx = ones(n) ## (findgen(n)-n/2)*df_prime
	    fy = transpose(fx)
	    pupilh = ft(pupil_f*exp(i*2*!pi*(fx*shx+fy*shy))*hanning_window,/inverse) ; Fourier-shift the meta pupil
	    if (useConformalMap) then begin
	        x = ones(n) ## (findgen(n)-n/2)*du_prime
	        y = transpose(x)
	        ix = x/du + n/2
	        iy = y/du + n/2
	        pupilh = interpolate(pupilh,ix,iy) ; ,cubic=-.5)
	    endif
	    pupilh = round(real(pupilh))
	    metapupil_science[*,*,layer] = metapupil_science[*,*,layer]+pupilh*code
	  endfor
		code *= 2
  endfor
  pupil_tomog = pupil
end
; ========= propagate spherical (or plane) waves ===========
function forwardPropagate,volume,du,h,pupil,star_angle,z,infinity=stars_at_infinity
  print,format='($,"<forwardPropagateGuideStars> propagating guide stars ")' & wait,.001
  n = (size(volume))[1]
  n_layers = (size(volume))[3]
  n_stars = (size(star_angle))[1]
  phase = dblarr(n,n,n_stars)
  rmserr = dblarr(n_stars)
  i = dcomplex(0.,1.)
  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  cone_beam = 1
  if (keyword_set(stars_at_infinity) or (n_elements(z) eq 0)) then cone_beam = 0

  if (finite_aperture) then begin
    if (noPiston) then begin
      piston = pupil/sqrt(total(pupil^2))
    endif
    if (tipTiltUncertain) then begin
      tip = (ones(n) ## findgen(n)-n/2) ; unit angle, 1 radian of tilt
      tilt = transpose(tip)
      tip = tip*pupil
      tip = tip/sqrt(total(tip^2))
      tilt = tilt*pupil
      tilt = tilt/sqrt(total(tilt^2))
    endif
  endif

  for star = 0,n_stars-1 do begin
    ph = dblarr(n,n)
    print,format='($,I0," ")',s & wait,.001
    for layer = 0,nlayers-1 do begin
      shx = h[layer]*star_angle[star,0]
      shy = h[layer]*star_angle[star,1]
      du_prime = du
      df_prime = 1/(n*du_prime)
      fx = ones(n) ## (dindgen(n)-n/2)*df_prime
      fy = transpose(fx)
;
;     shift the screen
;
      screen_hh = volume[*,*,layer]
      screen_f = ft(screen_hh)*exp(-2*!pi*i*(fx*shx+fy*shy)) ; Fourier-shift
      screen_hh = ft(screen_f,/inverse)
;
      if (cone_beam) then begin ; resample (stretch) the screen
          scale =(z-h[layer])/z
          ix = (x*scale/du + n/2)>0<(n-1)
          iy = (y*scale/du + n/2)>0<(n-1)
          screen_hh = interpolate(screen_hh,ix,iy)
      endif
      ph = ph + screen_hh

    endfor
    rmserr[star] = rms(ph-total(ph*pupil)/total(pupil),pupil)
    if (finite_aperture) then begin
      if (noPiston) then begin
        ph = ph - piston*total(ph*piston)
      endif
      if (tipTiltUncertain) then begin
        ph = ph - tip*total(ph*tip)
        ph = ph - tilt*total(ph*tilt)
      endif
      ph = ph*pupil
    endif
    phase[*,*,star] = ph
  endfor
  print,'done' & wait,.001
  return, phase
end
; ========= propagate spherical waves from guidestars ==========
function forwardPropagateGuideStars,volume,z
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common visualization

  print,format='($,"<forwardPropagateGuideStars> propagating guide stars ")' & wait,.001

  n_vol = (size(volume))[1]
  if (n_vol eq n_atmos) then begin
      n = n_atmos      ; use n as a local variable - restore global value before returning!
      du = du_atmos
      pupil = pupil_atmos
  endif else begin
      n = n_tomog
      du = du_tomog
      pupil = pupil_tomog
  endelse
  if (useConformalMap) then pupil_f = ft(pupil)
  phase_gs = dblarr(n,n,n_guidestars)
  rmserr = dblarr(n_guidestars)
  i = dcomplex(0.,1.)
  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  if (n_elements(z) eq 0) then z = lgs_altitude

  if (finite_aperture) then begin
    if (noPiston) then begin
      piston = pupil/sqrt(total(pupil^2))
    endif
    if (tipTiltUncertain) then begin
      tip = (ones(n) ## findgen(n)-n/2) ; unit angle, 1 radian of tilt
      tilt = transpose(tip)
      tip = tip*pupil
      tip = tip/sqrt(total(tip^2))
      tilt = tilt*pupil
      tilt = tilt/sqrt(total(tilt^2))
    endif
  endif

  for gs = 0,n_guidestars-1 do begin
    ph = dblarr(n,n)
    print,format='($,I0," ")',gs & wait,.001
    for layer = 0,nlayers-1 do begin
      shx = h[layer]*gs_angle[gs,0]
      shy = h[layer]*gs_angle[gs,1]
      du_prime = du
      if (useConformalMap) then du_prime *= ((z-h[layer])/z)
      df_prime = 1/(n*du_prime)
      fx = ones(n) ## (dindgen(n)-n/2)*df_prime
      fy = transpose(fx)
;
;     shift the screen
;
      screen_hh = volume[*,*,layer]
      screen_f = ft(screen_hh)*exp(-2*!pi*i*(fx*shx+fy*shy));*hanning(n,n) ; Fourier-shift
      screen_hh = ft(screen_f,/inverse)
;
      if (not useConformalMap) then begin ; resample (stretch) the screen
          scale =(z-h[layer])/z
          ix = (x*scale/du + n/2)>0<(n-1)
          iy = (y*scale/du + n/2)>0<(n-1)
          screen_hh = interpolate(screen_hh,ix,iy)
      endif
      ph = ph + screen_hh

    endfor
    rmserr[gs] = rms(ph-total(ph*pupil)/total(pupil),pupil)
    if (finite_aperture) then begin
      if (noPiston) then begin
        ph = ph - piston*total(ph*piston)
      endif
      if (tipTiltUncertain) then begin
        ph = ph - tip*total(ph*tip)
        ph = ph - tilt*total(ph*tilt)
      endif
      ph = ph*pupil
    endif
    phase_gs[*,*,gs] = ph
  endfor
  print,'done' & wait,.001
  return, phase_gs
end
; ========== forward propagate tip/tilt stars from infinity =======
function forwardPropagateTipTiltStars,volume
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common visualization

  print,format='($,"<forwardPropagateTipTiltStars> propagating guide stars ")' & wait,.001
  n_vol = (size(volume))[1]
  if (n_vol eq n_atmos) then begin
      n = n_atmos      ; use n as a local variable - restore global value before returning!
      du = du_atmos
      pupil = pupil_atmos
  endif else begin
      n = n_tomog
      du = du_tomog
      pupil = pupil_tomog
  endelse
  ; note: I'm assuming we arn't using the conformal map
  n_tipTiltStars = (size(tts_angle))[1]
  if (finite_aperture and noPiston) then piston = pupil/sqrt(total(pupil^2))
  tip = (ones(n) ## findgen(n)-n/2)*du ; radians of tip
  tilt = transpose(tip)
  if (finite_aperture) then begin
      tip = tip*pupil
      tilt = tilt*pupil
  endif
  NormTip = total(tip^2)
  NormTilt = total(tilt^2)
  df = 1/du
  fx = ones(n) ## (dindgen(n)-n/2)*df
  fy = transpose(fx)
  coefs = dblarr(n_tipTiltStars,2)
  for gs = 0,n_tipTiltStars-1 do begin
      ph = dblarr(n,n)
      print,format='($,I0," ")',gs & wait,.001
      for layer = 0,nlayers-1 do begin
          shx = h[layer]*tts_angle[gs,0]
          shy = h[layer]*tts_angle[gs,1]
          screen_hh = volume[*,*,layer]
          screen_f = ft(screen_hh)*exp(-2*!pi*i*(fx*shx+fy*shy)) ;*hanning(n,n) ; Fourier-shift
          screen_hh = ft(screen_f,/inverse)
          ph = ph + screen_hh
      endfor
      if (finite_aperture and noPiston) then ph = ph-piston*total(ph*piston)
      if (finite_aperture) then ph = ph*pupil
      coefs[gs,0] = total(ph*tip)/NormTip
      coefs[gs,1] = total(ph*tilt)/NormTilt
  endfor
  print,' done'
  return,coefs
end
; ========== forward propagate any stars from infinity =======
function forwardPropagateStars,volume,h,du,pupil,star_angle
  common constants
  common programSwitches
;  common tomography
;  common atmosphere
;  common imaging
;  common visualization

  print,format='($,"<forwardPropagateStars> propagating stars ")' & wait,.001
;  n_vol = (size(volume))[1]
;  if (n_vol eq n_atmos) then begin
;      n = n_atmos      ; use n as a local variable - restore global value before returning!
;      du = du_atmos
;      pupil = pupil_atmos
;  endif else begin
;      n = n_tomog
;      du = du_tomog
;      pupil = pupil_tomog
;  endelse
  n = (size(volume))[1]
  nlayers = (size(volume))[3]
  ; note: I'm assuming we aren't using the conformal map
  n_Stars = (size(star_angle))[1]
  phase_stars = dblarr(n,n,n_Stars)
  if (finite_aperture and noPiston) then piston = pupil/sqrt(total(pupil^2))
  df = 1/(n*du)
  fx = ones(n) ## (dindgen(n)-n/2)*df
  fy = transpose(fx)
  for star = 0,n_Stars-1 do begin
      ph = dblarr(n,n)
      print,format='($,I0," ")',star & wait,.001
      for layer = 0,nlayers-1 do begin
          shx = h[layer]*star_angle[star,0]
          shy = h[layer]*star_angle[star,1]
          screen_hh = volume[*,*,layer]
          screen_f = ft(screen_hh)*exp(-2*!pi*i*(fx*shx+fy*shy)) ;*hanning(n,n) ; Fourier-shift
          screen_hh = ft(screen_f,/inverse)
          ph = ph + screen_hh
      endfor
      if (finite_aperture and noPiston) then ph = ph-piston*total(ph*piston)
      if (finite_aperture) then ph = ph*pupil
      phase_stars[*,*,star] = ph
  endfor
  print,' done'
  return,phase_stars
end

; ========== take slopes of wavefronts ================
pro slopeWavefrontSensor,phase,du,phase_slope_x,phase_slope_y
  s = size(phase)
  n = s[1]
  m = s[2]
  if ((size(phase))[0] eq 3) then n_guidestars = s[3] else n_guidestars = 1
  phase_slope_x = dblarr(n,m,n_guidestars)
  phase_slope_y = dblarr(n,m,n_guidestars)
  for gs = 0,n_guidestars-1 do begin
    phase_slope_x[*,*,gs] = (phase[*,*,gs]-shift(phase[*,*,gs],1,0))/du
    phase_slope_y[*,*,gs] = (phase[*,*,gs]-shift(phase[*,*,gs],0,1))/du
  endfor
end
; ========== project volume estimate onto DMs ===========
function DM_collapse,volume
  common constants
  common programSwitches
  common tomography

  ndm = (size(dm_heights))[1]
  n = (size(volume))[1]
  nlayers = (size(volume_est))[3]
  dm_phase = dblarr(n,n,ndm)
  for dm = 0,ndm-1 do begin
      for layer = 0,nlayers-1 do begin
          dm_phase[*,*,dm] += dm_weights[layer,dm]*volume[*,*,layer]
      endfor
  endfor
  return,dm_phase
end
; ========== deformable mirror model ============
;   simple low-pass filter (sin(x)/x) model of DM actuator response
;   this takes a sparse sample of phases and produces a fine grid of phases
function deformableMirror,phase_commands,n_ph
  n = (size(phase_commands))[1]
  if ((size(phase_commands))[0] eq 3) then n_dm = (size(phase_commands))[3] else n_dm = 1
  phase = dblarr(n_ph,n_ph,n_dm)
  for dm = 0,n_dm-1 do begin
      phase_commands_f = ft(phase_commands[*,*,dm])
      phase_f = zeropad(phase_commands_f,n_ph,n_ph)
      phase[*,*,dm] = real(ft(phase_f,/inverse))
  endfor
  return,phase
end
; =========== wavefront sensor model =============
;   simple low-pass filter (spatial frequency cuttoff or sin(f)/f) model of WFS subaperture response
;   this takes a fine grid of phases a produces a sparse sample
function wavefrontSensor,phase,pupil,samples_per_subap,wfsType

  if (samples_per_subap ne 1) then begin
      n = (size(phase))[1]
      if ((size(phase))[0] eq 3) then n_gs = (size(phase))[3] else n_gs = 1
      n_wfs = (size(pupil))[1]  ; fix(n/samples_per_subap)
;  phase_wfs = fltarr(n,n,n_gs)  ; << switchover >> remove
      phase_wfs = fltarr(n_wfs,n_wfs,n_gs) ;  <<switchover>> add

      if (wfsType eq 'spatialFilter') then begin ; cut off spatial frequencies greater than subap Nyquist
          mask = zeropad(ones(n_wfs,n_wfs),n,n)
          for gs = 0,n_gs-1 do begin
              phase_f = ft(phase[*,*,gs])
;      phase_f *= mask   ; << switchover >> remove
              phase_f = phase_f[n/2-n_wfs/2:n/2+n_wfs/2-1,n/2-n_wfs/2:n/2+n_wfs/2-1] ;    << switchover >> add
              phase_wfs[*,*,gs] = depiston(real(ft(phase_f,/inverse)),pupil)*pupil
          endfor
      endif
      if (wfsType eq 'HartmannSubap') then begin ; replicate spectrum after boxcar average to simulate aliasing
          x = ones(n) ## findgen(n)-n/2
          y = transpose(x)
          u = ((x mod samples_per_subap) eq 0) and ((y mod samples_per_subap) eq 0)
          wu = where(u eq 1)    ; << switchover >> add
          box = zeropad(ones(samples_per_subap,samples_per_subap),n,n)
          if (even(samples_per_subap)) then begin
              box = fshift(box,0.5,0.5)
          endif else begin
              box = shift(box,1,1)
          endelse
          fbox = ft(box)/float(samples_per_subap)^2
          for gs = 0,n_gs-1 do begin
              phase_f = ftconvolve(phase[*,*,gs],fbox,domain=[0,1,0])*u
;      phase_wfs[*,*,gs] = real(ftconvolve(phase_f,fbox,domain=[0,1,0])) ; << switchover >> remove
              phase_wfs[*,*,gs] = depiston(reform(phase_f[wu],n_wfs,n_wfs),pupil)*pupil ; << switchover >> add
          endfor
      endif
      return,phase_wfs
  endif else begin
      return,phase
  endelse

end

function wavefrontSensorNoise, noiseStdDev, ap, d_subap, n_gs, model=model
  common constants

;   add noise to the wavefront measurements

  if (n_elements(model) eq 0) then model = 'phase'
  n = (size(ap))[1]
  noise = fltarr(n,n,n_gs)
  if (model eq 'phase') then begin
      for gs = 0,n_gs-1 do noise[*,*,gs] += randomn(seed,n,n)*ap*noiseStdDev
  endif
  if (model eq 'slope') then begin
      for gs = 0,n_gs-1 do begin
          sx = randomn(seed,n,n)*noiseStdDev/sqrt(2.)*d_subap
          sy = randomn(seed,n,n)*noiseStdDev/sqrt(2.)*d_subap
          sx_ft = ft(sx)
          sy_ft = ft(sy)
          noise[*,*,gs] += ftwfs(sx*ap,sy*ap,ap,geometry=0)*ap ; Fried geometry
      endfor
  endif

  return,noise
end

pro preFilter,measPhase,filtPhase
  common constants
  common programSwitches
  common tomography
  common atmosphere

  forward_function countBits

  n = n_tomog
  du = du_tomog
; Fourier transform the wavefronts
  phase_ft = dcomplexarr(n,n,n_guidestars)
  if (finite_aperture eq 1) then begin
    if (doPhaseExtension) then f = screengen(n,n,r0,du)*(lambda0/(2*!pi))
    for gs = 0,n_guidestars-1 do begin
      ph = measPhase[*,*,gs]*pupil_tomog
      if (doPhaseExtension) then begin
        ph = extendPhase(ph,f,pupil_tomog)
      endif
      phase_ft[*,*,gs] = ft(ph)
    endfor
  endif else begin
    for gs = 0,n_guidestars-1 do phase_ft[*,*,gs] = ft(measPhase[*,*,gs])
  endelse

  if (apply_filters) then begin
                                ; Apply filters
      print,'<preFilter> applying backprop filters' & wait,.001
      filt_phase_ft = dcomplexarr(n,n,n_guidestars)
                                ;  fc = n/2.
      printcount = 0
      for ifx = 0,n-1 do begin
          for ify = 0,n-1 do begin
              f = sqrt((float(ifx-n/2))^2+(float(ify-n/2))^2)
              rc = matrix_degenerate
              reg = regularization
              if (f lt f_low_freq) then begin
                  rc = low_freq_matrix_degenerate
                  reg = low_freq_regularization
              endif
              x = reform(phase_ft[ifx,ify,*],1,n_guidestars)
              aa = reform(conj(a[*,*,ifx,ify]),n_guidestars,n_guidestars)
              if (doPreInversions) then begin
                  filt_phase_ft[ifx,ify,*] = conj(aa) ## x
              endif else begin
                  ar = aa + (reg+(f/fc)^10)*identity(n_guidestars)
                  if (ifx ne n/2 or ify ne n/2) then begin
                      la_choldc,aa,/double,status=status
                      if (status eq 0) then begin
                          filt_phase_ft[ifx,ify,*] = la_cholsol(aa,x,/double)
                      endif else begin
                          la_choldc,ar,/double,status=status
                          if (status ne 0) then begin
                              if (printcount le 10) then print,'<preFilter> choldc status = ',status,' [ifx,ify] = ',ifx,ify
                              if (printcount eq 10) then print,'<preFilter> plus more'
                              printcount++
                          endif
                          filt_phase_ft[ifx,ify,*] = la_cholsol(ar,x,/double); least_squares(ar,x,method=2,rcondition=rc)*exp(-(f/fc)^10) ; ai[*,*,ifx,ify] ## x
                      endelse
                  endif else begin
                      filt_phase_ft[ifx,ify,*] = 0
                  endelse
              endelse           ;
          endfor

          chr = get_kbrd(0)
          if (strupcase(chr) eq "Q") then begin
              print,'<preFilter> ***filter calculations aborted***'
              quitFlag = 1
              break
          endif
          if (n_elements(percent_done) eq 0) then percent_done = -1
          new_percent_done = fix((ifx/float(n))*20.)
          if (new_percent_done ne percent_done) then begin
              if (new_percent_done eq 0) then print,format='($,"percent complete: ")'
              print,format='($,I0," ")',new_percent_done*5
              percent_done = new_percent_done
              wait,.001
          endif

      endfor
      if (printcount ne 0) then print,'<preFilter> ',printcount,' total errors'
      print,'done'
  endif else begin
      filt_phase_ft = phase_ft
  endelse

; Inverse transform the filtered wavefronts
  filtPhase = dblarr(n,n,n_guidestars)
  for gs = 0,n_guidestars-1 do begin
    filtPhase[*,*,gs] = real(ft(filt_phase_ft[*,*,gs],/inverse))
  endfor
end

pro postFilter,dvolume,volume  ; DG 3/31/07
  common constants
  common programSwitches
  common tomography
  common atmosphere

  n = n_tomog
  du = du_tomog

  if (n_elements(volume) eq 0) then begin
      volume = dblarr(n,n,nlayers)
  endif
  Cn2 = Cn2_tomog
  c0 = total(Cn2)

;  the postFilter has been pre-computed by formBackpropFilters
  for layer = 0,nlayers-1 do begin
      if (usePostFilter) then begin
          ph = ftconvolve(dvolume[*,*,layer],postFilter/(float(n)^2),domain=[0,1,0])
          volume[*,*,layer] += (Cn2[layer]/c0)*ph
      endif else begin
          volume[*,*,layer] += Cn2[layer]*dvolume[*,*,layer]/c0
      endelse
  endfor
end

; ============= back propagation of guide star data ===========
pro backPropagate,measPhase,volume, accum=accum
  common constants
  common programSwitches
  common tomography
  common atmosphere

  forward_function countBits

; Back-propagate filtered wavefronts
  print,format='($,"<backPropagate> back-propagating filtered wavefronts ")' & wait,.001

  n = n_tomog
  du = du_tomog

  pupil = pupil_tomog
  if (finite_aperture and normalizeByIllumination) then begin
    gs_count = double(countBits(round(metapupil),n_guidestars)) > 1.
  endif else begin
    gs_count = dblarr(n,n,nlayers)+n_guidestars
  endelse

  if (finite_aperture) then begin
    if (noPiston) then begin
      piston = pupil/sqrt(total(pupil^2))
    endif
    if (tipTiltUncertain) then begin
      tip = (ones(n) ## findgen(n)-n/2)
      tilt = transpose(tip)
      tip = tip*pupil
      tip = tip/sqrt(total(tip^2))
      tilt = tilt*pupil
      tilt = tilt/sqrt(total(tilt^2))
    endif
  endif

  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  z = lgs_altitude
  Cn2_scaled = Cn2_tomog
  if (useConformalMap) then Cn2_scaled *= (z/(z-h))^(-5./3.)
  c0 = total(Cn2_tomog)

;  create the volume estimate region if it doesn't already exist
  if (n_elements(volume) eq 0) then begin
      volume = dblarr(n,n,nlayers)
  endif else begin
      if ( total((size(volume))[0:3]-[3,n,n,nlayers]) ne 0 ) then begin
          volume = dblarr(n,n,nlayers)
      endif else begin
          if (not keyword_set(accum)) then volume = dblarr(n,n,nlayers)
      endelse
  endelse
  ; if (n_elements(filt_phase_ft) eq 0) then ...  -- changed DG 10/19/06
  filt_phase_ft = dcomplexarr(n,n,n_guidestars)
  for gs = 0,n_guidestars-1 do begin
    ph = measPhase[*,*,gs]

    if (finite_aperture) then begin
      if (noPiston) then begin
        ph = ph - piston*total(ph*piston)
      endif
      if (tipTiltUncertain) then begin
        ph = ph - tip*total(ph*tip)
        ph = ph - tilt*total(ph*tilt)
      endif
      ph = ph*pupil
    endif

    if (useConformalMap) then begin
        filt_phase_ft[*,*,gs] = ft(ph)
    endif
    print,format='($,I0," ")',gs & wait,.001
    for layer = 0,nlayers-1 do begin
      if (not useConformalMap) then begin ; resample (shrink) the metapupil
          du_prime = du*(z-h[layer])/z
          ix = (x/du_prime + n/2)>0<(n-1)
          iy = (y/du_prime + n/2)>0<(n-1)
          filt_phase_l = interpolate(ph,ix,iy)
          filt_phase_ft[*,*,gs] = ft(filt_phase_l)
      endif
      shx = h[layer]*gs_angle[gs,0]; shift the metapupil
      shy = h[layer]*gs_angle[gs,1]
      du_prime = du
      if (useConformalMap) then du_prime *= ((z-h[layer])/z)
      df_prime = 1/(n*du_prime)
      fx = ones(n) ## (findgen(n)-n/2)*df_prime
      fy = transpose(fx)
      filt_phase_h = ft(filt_phase_ft[*,*,gs]*exp(2*!pi*i*(fx*shx+fy*shy)),/inverse) ; Fourier-shift
      filt_phase_h = filt_phase_h*(double(n_guidestars)/gs_count[*,*,layer]) ; boost sparsely probed parts of volume
;      volume[*,*,layer] += Cn2_scaled[layer]*filt_phase_h/c0 ; DG 3/31/07
      volume[*,*,layer] += filt_phase_h                        ; DG 3/31/07
    endfor
  endfor
  print,'done'
  return
end
; ============= back propagation of tip tilt star data ===============
pro backPropagateTipTilt,measPhase,volume,accum=accum
  common constants
  common programSwitches
  common tomography
  common atmosphere

  n = n_tomog
  du = du_tomog
; Back-propagate filtered tip tilt stars
  print,format='($,"<backPropagateTipTilt> back-propagating tip tilt stars")' & wait,.001
;  create the volume estimate region if it doesn't already exist
  if (n_elements(volume) eq 0) then begin
      volume = dblarr(n,n,nlayers)
  endif else begin
      if ( total((size(volume))[0:3]-[3,n,n,nlayers]) ne 0 ) then begin
          volume = dblarr(n,n,nlayers)
      endif else begin
          if (not keyword_set(accum)) then volume = dblarr(n,n,nlayers)
      endelse
  endelse
  n_tts = (size(tts_angle))[1]
  df = 1/(n*du)
  if (finite_aperture) then piston = pupil/sqrt(total(pupil^2))
  c0 = total(Cn2)
  for gs = 0,n_tts-1 do begin
    ph = measPhase[*,*,gs]

    if (finite_aperture) then begin
      if (noPiston) then begin
        ph = ph - piston*total(ph*piston)
      endif
      ph = ph*pupil
    endif
; we're assuming we're not using the conformal map
    print,format='($,I0," ")',gs & wait,.001
    ph_ft = ft(ph)
    for layer = 0,nlayers-1 do begin
      shx = h[layer]*tts_angle[gs,0]; shift the metapupil
      shy = h[layer]*tts_angle[gs,1]
      fx = ones(n) ## (findgen(n)-n/2)*df
      fy = transpose(fx)
      filt_phase_h = ft(ph_ft*exp(2*!pi*i*(fx*shx+fy*shy)),/inverse) ; Fourier-shift
;      filt_phase_h = filt_phase_h*(double(n_guidestars)/gs_count[*,*,layer]) ; boost sparsely probed parts of volume
      volume[*,*,layer] = volume[*,*,layer]+Cn2[layer]*filt_phase_h/c0
    endfor
  endfor
  print,'done'
  return

end

; ============= Open loop image through the atmosphere ===============
pro propagateScienceLightUncorrected
  common programSwitches
  common constants
  common tomography
  common atmosphere
  common imaging

  n = n_atmos
  du = du_atmos
  print,'<propagateScienceLightUncorrected> imaging through turbulence' & wait,.001
  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  z = lgs_altitude
  n_scienceStars = (size(science_angle))[1]
  uncorrected_phase = dblarr(n,n,n_scienceStars)
  psf_open = dblarr(n,n,n_scienceStars)
  for gs = 0,n_scienceStars - 1 do begin
	  for layer = 0,nlayers-1 do begin
	    shx = h[layer]*science_angle[gs,0] ; shift the metapupil
	    shy = h[layer]*science_angle[gs,1]
	;
	;   shift the screen
	;
	    du_prime = du
	    if (useConformalMap) then du_prime *= (z-h[layer])/z
	    df_prime = 1/(n*du_prime)
	    fx = ones(n) ## (findgen(n)-n/2)*df_prime
	    fy = transpose(fx)
	    screen_f = ft(screen_h[*,*,layer])*exp(-2*!pi*i*(fx*shx+fy*shy));*hanning_window ; Fourier-shift
	    screen = ft(screen_f,/inverse)
	;
	    if (useConformalMap) then begin ; resample (shrink) the screen
	        ix = (x/du_prime + n/2)>0<(n-1)
	        iy = (y/du_prime + n/2)>0<(n-1)
	        screen = interpolate(screen,ix,iy)
	    endif
	    uncorrected_phase[*,*,gs] = uncorrected_phase[*,*,gs] + double(screen)
  	endfor

  	uncorrected_phase[*,*,gs] = depiston(uncorrected_phase[*,*,gs],pupil_atmos)
	  wf = pupil_atmos*exp(i*uncorrected_phase[*,*,gs]*(2*!pi/science_lambda))
	  nn = n
	  if (zoomPsfs) then begin
	  	nn = 2*n
	  	wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
	  endif
	  wf_fp = ft(wf)*float(nn)
	  psf_open[*,*,gs] = abs(wf_fp)^2

	endfor
  pixelScale = science_lambda/(nn*du)
end

; ========== MCAO: Forward-propagate in science direction ========
pro propagateScienceLightMCAOCorrected
  common programSwitches
  common constants
  common tomography
  common atmosphere
  common imaging

  forward_function spatialFilter
  n = n_tomog
  du = du_tomog
  pupil = pupil_tomog
  if (n_elements(uncorrected_phase) eq 0) then propagateScienceLightUncorrected
  print,'<propagateScienceLightMCAOCorrected> mcao-corrected imaging in science direction' & wait,.001
  x = ones(n) ## (dindgen(n)-n/2)*du
  y = transpose(x)
  z = lgs_altitude
  n_scienceStars = (size(science_angle))[1]
  corrected_phase = dblarr(n_atmos,n_atmos,n_scienceStars)
  dx = pixelScale
  nprime = fix(science_lambda/(dx*du))
  psf_mcao = dblarr(nprime,nprime,n_scienceStars)
  for gs = 0,n_scienceStars - 1 do begin
      sum_phase = dblarr(n,n)
      for layer = 0,nlayers-1 do begin
          shx = h[layer]*science_angle[gs,0] ; shift the metapupil
          shy = h[layer]*science_angle[gs,1]
	;
	;     shift the screen
	;
          volume_est_h = volume_est[*,*,layer]
          du_prime = du
          if (useConformalMap) then du_prime *= (z-h[layer])/z
          df_prime = 1/(n*du_prime)
          fx = ones(n) ## (findgen(n)-n/2)*df_prime
          fy = transpose(fx)
          volume_est_f = ft(volume_est_h)*exp(-2*!pi*i*(fx*shx+fy*shy)) ;*hanning_window;    Fourier-shift
          volume_est_h = ft(volume_est_f,/inverse)
	;
          if (useConformalMap) then begin ; resample (shrink) the screen
              ix = (x/du_prime + n/2)>0<(n-1)
              iy = (y/du_prime + n/2)>0<(n-1)
              volume_est_h = interpolate(volume_est_h,ix,iy)
          endif
          sum_phase = sum_phase + double(volume_est_h)
      endfor
      if (finite_Aperture) then sum_phase = sum_phase*pupil
      if (modelSubapertures) then sum_phase = spatialFilter(sum_phase,d_subap,1.,1./(n*du))
                                ; smooth(corrected_phase[*,*,gs],D_subap/du)
      sum_phase = depiston(sum_phase,pupil)
      corrected_phase[*,*,gs] = deformableMirror(sum_phase,n_atmos) ; << switchover >> add
;      corrected_phase[*,*,gs] = sum_phase   ; << switchover >> remove
      corrected_phase[*,*,gs] = uncorrected_phase[*,*,gs] - corrected_phase[*,*,gs]
      wf = pupil_atmos*exp(i*corrected_phase[*,*,gs]*(2*!pi/science_lambda))
;      n = n_atmos  ; this was a mistake - n should equal n_tomog in this loop
      wf = zeropad(wf,nprime,nprime)
      wf_fp = ft(wf)*float(nprime)
      psf_mcao[*,*,gs] = abs(wf_fp)^2
  endfor
end

; ========= GLAO: average the wavefronts from all guidestars =========
pro GLAO_correction
  common programSwitches
  common constants
  common tomography
  common atmosphere
  common imaging
  forward_function spatialFilter

  print,'<GLAO_correction> glao-correct imaging in science direction 0' & wait,.001
  n = n_atmos
  du = du_atmos
  phase_ave = fltarr(n,n)
  for guidestar = 0, n_guidestars-1 do begin
    phase_ave = phase_ave + phase[*,*,guidestar]
  endfor
  phase_ave = phase_ave/n_guidestars
  if (modelSubapertures) then phase_ave = spatialFilter(phase_ave,d_subap,1.,1./(n*du))
                                ; smooth(corrected_phase[*,*,gs],D_subap/du)
  phase_ave = depiston(phase_ave,pupil)
  glao_phase = uncorrected_phase[*,*,0] - phase_ave
  wf = pupil*exp(i*glao_phase*(2*!pi/science_lambda))
  nn = n
  if (zoomPsfs) then begin
  	nn = 2*n
  	wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
  endif
  wf_fp = ft(wf)*float(nn)
  psf_glao = abs(wf_fp)^2
end

; ======== SCAO: use the wavefront from only the first guidestar ====
pro SCAO_correction
  common programSwitches
  common constants
  common tomography
  common imaging
  forward_function spatialFilter

  print,'<SCAO_correction> single conjugate corrected imaging in science direction 0' & wait,.001
  n = n_atmos
  du = du_atmos
  scao_phase = phase[*,*,0]
  if (modelSubapertures) then scao_phase = spatialFilter(scao_phase,d_subap,1.,1./(n*du))
                                ; smooth(scao_phase*pupil,D_subap/du)
  scao_phase = depiston(uncorrected_phase[*,*,0] - scao_phase,pupil_atmos)
  wf = pupil*exp(i*scao_phase*(2*!pi/science_lambda))
  nn = n
  if (zoomPsfs) then begin
  	nn = 2*n
  	wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
  endif
  wf_fp = ft(wf)*float(nn)
  psf_scao = abs(wf_fp)^2
  pixelScale = science_lambda/(nn*du)
end

; ============= diffraction-limited image ===================
pro diffractionLimitedImage
  common programSwitches
  common tomography
  common atmosphere
  common imaging

  n = n_atmos
  du = du_atmos
  wf = pupil_atmos
  dx = plateScale
  nprime = fix(science_lambda/(dx*du))
  if (nprime gt n) then wf = zeropad(wf,nprime,nprime)
  wf_fp = ft(wf)*float(nprime)
  ff = abs(wf_fp)^2
  if (nprime lt n) then ff = congrid(ff,nprime,nprime)
  psf_dl = ff
end

; ================ spatial filtering due to subapertures ============
function spatialFilter,ph,d,lambda,dtheta
  n = (size(ph))[1]
  nf = (lambda/d)/dtheta
  filter = zeropad(ones(nf,nf),n,n)
  fph = ft(ph)
  fphe = filter*fph
  phe = real(ft(fphe,/inverse))
  return,phe
end

; =============== image ==============
function image_psf,phase,pupil,lambda,du,dx
  i = dcomplex(0.,1.)
  n = (size(phase))[1]
  if ((size(phase))[0] eq 3) then n_stars = (size(phase))[3] else n_stars = 1
  nprime = fix(lambda/(du*dx))
  psf = dblarr(nprime,nprime,n_stars)
  for star = 0,n_stars-1 do begin
      wf = pupil*exp(i*phase[*,*,star]*(2*!pi/lambda))
      if (nprime gt n) then wf = zeropad(wf,nprime,nprime)
      wf_fp = ft(wf)*float(nprime)
      ff = abs(wf_fp)^2
      if (nprime lt n) then ff = congrid(ff,nprime,nprime)
      psf[*,*,star] = ff
  endfor
  return,psf
end

; ================ uncompleted code =============
pro uncompletedCode

  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization

  if (Cn2_unknown) then begin
    if (n_elements(Cn2_update) gt 1) then Cn2 = Cn2_update else Cn2 = (ones(nlayers)/float(nlayers))
  endif

  ; volume est statistics for Cn2-adaptive algorithm
  if (Cn2_unknown) then begin
  vol_var = fltarr(nlayers)
  for layer = 0,nlayers-1 do begin
    vol_var[layer] = rms(volume_est[*,*,layer],metapupil[*,*,layer] ge (n_guidestars-.5))
  endfor
  Cn2_update = vol_var/(total(vol_var))
  endif
end

; ================ statistics =================
pro printStatistics

  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  pupil = pupil_tomog
  n_scienceStars = (size(science_angle))[1]
  s0 = max(psf_dl)
;  da = du*((n/2)/fc)
;  fitting_error = sqrt((da/r0)^(5./3.))*(lambda0/(2*!pi))/nm
  fitting_error = sqrt((D_subap/r0)^(5./3.))*(lambda0/(2*!pi))/nm
  print,'<printStatistics> '+'[fitting error '+strtrim(string(fitting_error),2)+' nm]'
  print,'<printStatistics> '+'[uncorrected rms error '+strtrim(string(rms(uncorrected_phase,pupil)/nm),2) + ' nm]'
  print,'<printStatistics> '+'[wavelength '+string(science_lambda/microns,format='(f6.2)'),' microns]'
  strehlResults = fltarr(n_scienceStars,3)
  for star = 0,n_scienceStars - 1 do begin
      s = max(psf_mcao[*,*,star])/s0
      r1 = rms(corrected_phase[*,*,star],pupil)/nm
      r2 = sqrt((r1^2 - fitting_error^2)>0.)
      if (star eq 0) then r2_0 = r2
      r3 = sqrt(-alog(s))*(science_lambda/(2*!pi))/nm
      r4 = sqrt((r3^2 - fitting_error^2)>0.)
      if (star eq 0) then r4_0 = r4
      print,'<printStatistics> ' + '[Strehl '+strtrim(string(s),2)+']   ' + $
                                   '[rms error '+strtrim(string(r1),2)+' nm]   ' + $
                                   '[Strehl implied rms '+strtrim(string(r3),2)+']'
      strehlResults[star,*] = [s,r1,r3]
  endfor

;  calculate the effective depth of the atmosphere

  deltaK = r0*(r4_0*nm*2*!pi/lambda0)^(6./5.)/gs_separation
  print,'<printStatistics> minimum guidestar separation ',gs_separation/arcsec,' arcsec'
  print,'<printStatistics> effective thickness of atmosphere ',deltaK,' meters'
end
;
; ================== count bits =======================================
function countBits,data,nbits
  result = data*0
  sdata = data
  for k = 0, nbits-1 do begin
    result = result + (sdata and 1)
    sdata = ishft(sdata,-1)
  endfor
  return,result
end
;
; ============== analyze least squares variance ==========================
pro analyzeVariance,err,we
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common covarianceAnalysis, Cphi, Dphi, tau0, tau_unc, tau_cor, $
     psf_0, psf_unc, psf_cor
;
;  J = 1 - 2*conj(transpose(g)) ## c + conj(transpose(g)) ## A ## g
;  g = la_invert(A) ## c
;  if (analyze_variance eq 1) then begin
  print,'<analyzeVariance> analyzing residual variance' & wait,.001
  wait,.001
  err = fltarr(n,m)
  wphi = fltarr(n,m)
  df = double(1/(n*du))

  for ifx = 0,n-1 do begin
  for ify = 0,m-1 do begin
    f = fix(sqrt((float(ifx-n/2))^2+(float(ify-m/2))^2)) > 1 < (n/2-1)
    rc = matrix_degenerate
    reg = regularization
    if (f lt f_low_freq) then begin
      rc = low_freq_matrix_degenerate
      reg = low_freq_regularization
    endif
    wphi[ifx,ify] = (f*df)^(-11./3.)
    aa = reform(conj(a[*,*,ifx,ify]),n_guidestars,n_guidestars)
    aa_mod = aa + (reg+(f/fc)^10)*identity(n_guidestars)
    cc = reform(c[*,ifx,ify],1,n_guidestars)
    if (ifx ne n/2 or ify ne m/2) then begin
      g = conj(la_least_squares(aa_mod,cc,method=2,rcondition=rc))*exp(-(f/fc)^(10))
      err[ifx,ify] = 1 - 2*transpose(cc)##g + transpose(g) ## aa ## conj(g)
    endif else begin
      err[ifx,ify] = 1.
    endelse
  endfor
    chr = get_kbrd(0)
    if (strupcase(chr) eq "Q") then goto,quit1
    if (n_elements(percent_done) eq 0) then percent_done = -1
    new_percent_done = fix((ifx/float(n))*20.)
    if (new_percent_done ne percent_done) then begin
      if (new_percent_done eq 0) then print,format='($,"percent complete: ")'
      print,format='($,I0," ")',new_percent_done*5
      percent_done = new_percent_done
      wait,.001
    endif
  endfor
  wphi[n/2,m/2] = 0
  print,'<analyzeVariance> total residual variance, fraction of uncorrected ',sqrt(total(wphi*err)/total(wphi))

  pu = zeropad(pupil,2*n,2*n)
  tau0 = real(ftconvolve(pu,pu))
  tau0 = tau0/total(tau0)

  zpf = 2

  df = 1/(n*du)
  w0 = 9.69d-3*(2*!pi/lambda0)^2*wphi;  wphi = f^(-11/3)
  w = w0*total(Cn2)
  we = w*(err-min(err)) ; *** temporary - don't know why err < 0 ***

  Cphi_atm = real(ft(w,/inverse))*df^2
  Cphi = real(ft(we,/inverse))*df^2
  Dphi_atm = 2*(Cphi_atm[n/2,n/2]-Cphi_atm)
  Dphi = 2*(Cphi[n/2,n/2]-Cphi)
  tau_atm = exp(-0.5*Dphi_atm)
  tau = exp(-0.5*Dphi)

  n1 = zpf*n/2-n/2
  n2 = zpf*n/2+n/2-1
  if (zpf eq 1) then tau0 = tau0[n1:n2,n1:n2]
  tau_unc = tau0*zeropad(tau_atm,zpf*n,zpf*n)
  tau_cor = tau0*zeropad(tau,zpf*n,zpf*n)
  psf_0 = (abs(ft(tau0)))[n1:n2,n1:n2]
  psf_unc = (abs(ft(tau_unc)))[n1:n2,n1:n2]/max(psf_0)
  psf_cor = (abs(ft(tau_cor)))[n1:n2,n1:n2]/max(psf_0)

  window,/free
  f = findgen(n/2)*df
  sf = 1;
  ll = min(w[where(w ne 0)])*sf
  plot,f,ravg((w*sf))>ll,/ylog,/xlog, xrange = [1,n/2]*df, $
    xtitle = 'Spatial Frequency, m^-1', ytitle = 'Power Spectrum, radians^2/m^-2'
  oplot,f,ravg((we*sf))
  sf = (2*!pi/lambda0) ; convert meters to radians of phase
  oplot,f,ravg(abs(ft(uncorrected_phase*pupil*sf,/inverse)*du^2)^2)*df^2,color=150
  oplot,f,ravg(abs(ft(corrected_phase*pupil*sf,/inverse)*du^2)^2)*df^2,color=150
  print,'<analyzeVariance> window ',!d.window

;  endif
  return
  ;
  quit1: print,'done'
  quitFlag = 1
  return
end
; ================== ensquared energy plot =================
function ensquare,im
  n = (size(im))[1]
  r = fltarr(n/2)
  for k = 1,n/2-1 do begin
      r[k] = total(im[n/2-k:n/2+k,n/2-k:n/2+k])
  endfor
  return,r
end

pro esqeplot,gs
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization

  n = n_atmos
  window,/free,title='esqe '+strtrim(string(gs),2)+' [run '+strtrim(string(run_no),2)+']'
;  ee = ensquare(psf_mcao[*,*,gs])/total(psf_mcao[*,*,gs])
  ee = ensquare(average_psf_corrected[*,*,gs])/total(average_psf_corrected[*,*,gs])
  x = findgen(n/2)*plateScale*1000./arcsec
  plot,x,ee,xtitle='radius, mas',ytitle='% ensquared energy',title = 'Ensquared Energy, science target '+strtrim(string(gs),2)
  eedl = ensquare(psf_dl)/total(psf_dl)
  oplot,x,eedl,linestyle = 2
  ees = ensquare(psf_open[*,*,gs])/total(psf_open[*,*,gs])
  oplot,x,ees,linestyle = 2
  oplot,[25,25],[0,1]
  oplot,[min(x),max(x)],[0.5,0.5]
end

; ================= encircled energy plot ===============
pro eeplot,gs
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization

  n = n_atmos
  window,/free,title='ee '+strtrim(string(gs),2)+' [run '+strtrim(string(run_no),2)+']'
;  psf = psf_mcao[*,*,gs]/max(psf_dl)
  psf = average_psf_corrected[*,*,gs]/max(psf_dl)
  ee = encirc(psf,n/2,n/2)
  x = ee[0,*]*plateScale*1000./arcsec
  y = ee[1,*]/total(psf)
  plot,x,y,xtitle='radius, mas',ytitle='% encirc energy',title = 'Encircled Energy, science target '+strtrim(string(gs),2)
;  plot,ee[0,*]*plateScale*1000./arcsec,ee[1,*]/total(psf),xtitle='radius, mas',ytitle='% encirc energy'
  eedl = encirc(psf_dl,n/2,n/2)
  oplot,eedl[0,*]*plateScale*1000./arcsec,eedl[1,*]/total(psf_dl),linestyle = 2
  psf = psf_open[*,*,gs]/max(psf_open[*,*,gs])
  ees = encirc(psf,n/2,n/2)
  oplot,ees[0,*]*plateScale*1000./arcsec,ees[1,*]/total(psf),linestyle = 2
end
;
;================== pcg support: x = x + alpha*p  ===============
pro pcgScaleAdd1,x,alpha,p
  if (type(x,/text) eq 'structure') then begin
      x.highOrder += alpha*p.highOrder
      x.lowOrder += alpha*p.lowOrder
  endif else begin
      x += alpha*p
  endelse
end
;================== pcg support: p = z + beta*p ===============
pro pcgScaleAdd2,z,beta,p
  if (type(z,/text) eq 'structure') then begin
      p.highOrder = z.highOrder + beta*p.highOrder
      p.lowOrder  = z.lowOrder  + beta*p.lowOrder
  endif else begin
      p = z + beta*p
  endelse
end
; ================== pcg sum product: rho = total(r*z) ==============
function pcgSumProduct,r,z
  if (type(r,/text) eq 'structure') then begin
      rho = total(r.highOrder*z.highOrder)
      rho += total(r.lowOrder*z.lowOrder)
  endif else begin
      rho = total(r*z)
  endelse
  return,rho
end
; ================== calculate derived parameters =============
pro calculateDerivedParameters,display=display
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  D_subap = D_tele/float(subapsAcross) ; these are derived parameters
  du_atmos = D_subap/float(subSample)
  du_tomog = D_subap

  createScienceConstellation
  createAtmosphere
  initTomography
  if (tipTiltStars) then createTipTiltStarConstellation
;  if (quitFlag) then return ; allow quick exit during long calculations
  makeMetapupil
  pupil_atmos = rebin(pupil_tomog,n_atmos,n_atmos)

  runstr = '[run '+strtrim(string(run_no),2)+']'
  if (keyword_set(display)) then begin ;      diagnostic display
      drawGuideStarConstellation,gs_angle,science_angle,field_angle,tts_angle,tipTiltStars=tipTiltStars
      disp,metapupil,'metapupil'+runstr,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
      disp,metapupil_science,'science metapupil'+runstr,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
  endif

end

; =============== simulationReseet ==============
pro simulationReset
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

; this forces a cold_restart even when warm_restart is set
  forceColdRestart = 1
end

; =================== tomographySphericalWave ===========================
pro tomographySphericalWave

  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

; -- front matter for program run --

  if (n_elements(arcsec) eq 0) then begin ; in case run parameters haven't been initialized yet
      go
      return
  end
  if (n_elements(warm_restart) eq 0) then warm_restart = 0
  if (n_elements(n_guidestars) eq 0) then n_guidestars = 8

  quitFlag = 0
  runstr = '[run '+strtrim(string(run_no),2)+']'

  print,''
  print,'<tomographySphericalWave:main> '+ $
'------------------ START --'+runstr+'-- START -------------------------'

  if (forceColdRestart or (warm_restart eq 0)) then begin
      calculateDerivedParameters
      if (quitFlag) then goto,quit

      psf_open = 0
      psf_mcao = 0
      average_psf_uncorrected = 0
      average_psf_corrected = 0

      diffractionLimitedImage
      formBackpropFilters
  endif

;  n_scienceStars = (size(science_angle))[1]

  if (quitFlag) then goto,quit

;  ------- loop for statistical realizations ------------

  for realization = 0,n_realizations-1 do begin
      psf_open = 0
      psf_mcao = 0

      doRealization
      saveRealizationData

      if (quitFlag) then break
  endfor
  if (quitFlag) then goto,quit

;  final diagnostic displays
;  displayDiagnostics
;  if (display_eeplots) then begin
;      for gs = 0,n_scienceStars - 1 do begin
;	  eeplot,gs
;          esqeplot,gs
;      endfor
;  endif
  if analyze_variance then analyzeVariance,err,we


  quit: print,'<tomographySphericalWave:main> run '+runstr+' done'
  print,'<tomographySphericalWave:main> '+ $
'--------------------- END --'+runstr+'-- END -------------------------'
  print,''
  quitFlag = 0
  run_no++

end

; ===================== doRealization =======================
pro doRealization
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateRealization,widget_id,realization

  print,'<tomographySphericalWave:doRealization> ********** realization ',realization,' *************'
  if (forceColdRestart or (warm_restart eq 0)) then begin
      createAtmosphericPhaseScreens
      volume_est *= 0
  endif

;  --------------- loop over simulated time -------------
  for timeStep = 0,n_timeSteps-1 do begin
      time = timeStep*delta_time
      doTimeStep
      saveTimeData
      if (quitFlag) then break
  endfor
;  ------------------------------------------------------

  average_psf_uncorrected += psf_open/float(n_realizations)
  average_psf_corrected += psf_mcao/float(n_realizations)

  forceColdRestart = 0
end

; ===================== doTimeStep =========================

pro doTimeStep
  common constants
  common programSwitches
  common tomography
  common controller
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateTimeStep,widget_id,timeStep

  print,'<tomographySphericalWave:doTimeStep> --- time = ',time,' time step ',timeStep,' of ',n_timeSteps

  blowScreens
; ------ do wavefront measurements --------
  phase = forwardPropagateGuideStars(screen_h)
  if (labData) then begin
;    cd,'C:\Documents and Settings\Don Gavel\My Documents\Adaptive Optics\Papers2007\OSA_TopicalMeetingOnAO\Data'
    cd,'C:\Documents and Settings\Don Gavel\Desktop'
    file = 'wfData.sav'
    restore,file
    ph_meas = wf3a
    phase = ph_meas*nm
  endif
  phase_wfs = wavefrontSensor(phase,pupil_tomog,subSample,wfsType) ; this is an open-loop WFS measurement

  if (simulateNoise) then begin
      if (noiseModel eq 'slope') then phase_wfs += wavefrontSensorNoise(noiseEquivalentAngle,pupil_tomog,D_subap,n_guidestars,model=noiseModel)
      if (noiseModel eq 'phase') then phase_wfs += wavefrontSensorNoise(phaseNoiseStdDev,pupil_tomog,D_subap,n_guidestars,model=noiseModel)
  endif

  if (tipTiltStars) then tt_coefs = forwardPropagateTipTiltStars(screen_h)
  if ((warm_restart eq 0) or (forceColdRestart)) then volume_est = 0*volume_est

; -------- do AO control work ------------

  if (forceColdRestart) then phase_wfs_integral = phase_wfs ; 0
  phase_wfs_integral = (1-controlGain)*phase_wfs_integral + controlGain*phase_wfs

; ----- loop over tomography iterations -------
  clocktime0 = systime(1)
  for iteration = 0, n_iterations-1 do begin
      doIteration,phase_wfs_integral
      if (quitFlag or converged) then break
  endfor
  clocktime = systime(1)-clocktime0
  doIteration,phase_wfs_integral,/last
; -------------------------------------------

  if (iteration ne 0) then begin
      print,'<tomographySphericalWave:doTimeStep> Time:',clocktime,' seconds. Time per iteration:',clocktime/iteration,' seconds'
  endif

;  add in separately measured tip/tilt to ground layer
;  if (tipTiltUncertain) then volume_est[*,*,0] += tipTiltCorrection

; -------- make the science observation --------
  coaddScienceImages,/sample
  savePostProcessingData

  time += delta_time
  if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateTimeStep,widget_id,timeStep+1

  forceColdRestart = 0
end

; ===================== doIteration =====================
pro doIteration,phase_target,last=last
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  common pcgState, x,r,p,rho_last

  ev = widget_event(/nowait)    ; check the GUI Stop button
  if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateIteration,widget_id,iteration

  print,'<tomographySphericalWave:doIteration> --- iteration ',iteration,' --'

  if (keyword_set(last)) then begin
      if (iterationAlgorithm eq 'PCG') then begin
;          backPropagate,x,volume_est,/accum         ; DG 3/31/07
          backPropagate,x,delta_volume_est           ; DG 3/31/07
          postFilter,delta_volume_est,volume_est     ; DG 3/31/07
      endif
      phest = forwardPropagateGuideStars(volume_est)
      pherr = phase_target-phest
      rr = 0.
      for gs = 0,n_guidestars-1 do rr += (rms(pherr[*,*,gs],pupil_tomog)/microns)^2
      rr = sqrt(rr/float(n_guidestars))
      consistency_rms = rr*microns
      print,'<tomographySphericalWave:doIteration> rms meas phase error ',rr,' microns'
      if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateRMSerror,widget_id,rr*microns/nm
     return
  endif

  converged = 0

  key = get_kbrd(0)
  if ((key eq 'q') or (quitFlag)) then begin
      quitFlag = 1
      print,'<tomographySphericalWave:main> interrupted at iteration ',iteration
      return
  endif

  if (iterationAlgorithm eq 'Fixed') then begin
      phest = forwardPropagateGuideStars(volume_est)
      pherr = phase_target-phest

      rr = 0.
      for gs = 0,n_guidestars-1 do rr += (rms(pherr[*,*,gs],pupil_tomog)/microns)^2
      rr = sqrt(rr/float(n_guidestars))
      consistency_rms = rr*microns
      print,'<tomographySphericalWave:doIteration> rms meas phase error ',rr,' microns'
      if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateRMSerror,widget_id,rr*microns/nm
      if (rr*microns le stopAt) then begin
          converged = 1
          return
      endif

      preFilter,pherr,filtPherr
;      backPropagate,iteration_gain*filtPherr,volume_est,/accum ; DG 3/31/07
      backPropagate,iteration_gain*filtPherr,delta_volume_est   ; DG 3/31/07
      postFilter,delta_volume_est,volume_est                    ; DG 3/31/07
  endif
  if (iterationAlgorithm eq 'PCG') then begin ; PCG needs a PCG state
      if (iteration eq 0) then begin
          x = phase_target*0
          phest = forwardPropagateGuideStars(volume_est)
          r = phase_target - phest
      endif

      aux_volume = volume_est
;      backPropagate,x,aux_volume,/accum        ; DG 3/31/07
      backPropagate,x,delta_volume_est          ; DG 3/31/07
      postFilter,delta_volume_est,aux_volume    ; DG 3/31/07
      phest = forwardPropagateGuideStars(aux_volume)
      r = phase_target - phest

      rr = 0.
      for gs = 0,n_guidestars-1 do rr += (rms(r[*,*,gs],pupil_tomog)/microns)^2
      rr = sqrt(rr/float(n_guidestars))
      print,'<tomographySphericalWave:doIteration> rms meas phase error ',rr,' microns'
      if (widget_info(widget_id,/valid_id)) then tomographySphericalWave_widget_updateRMSerror,widget_id,rr*microns/nm
      if (rr*microns le stopAt) then begin
          converged = 1
          return
      endif

      preFilter,r,z
      rho = pcgSumProduct(r,z)  ; rho = total(r*z)
      if (iteration eq 0) then begin
          p = z
      endif else begin
          beta = rho/rho_last
          pcgScaleAdd2,z,beta,p ; p = z + beta*p
      endelse
      rho_last = rho
;      backPropagate,p,aux_volume            ; DG 3/31/07
      backPropagate,p,delta_volume_est       ; DG 3/31/07
      postFilter,delta_volume_est,aux_volume ; DG 3/31/07
      q = forwardPropagateGuideStars(aux_volume)
      q += p/snr ;  *** experimental: add a normalization proportional to noise to signal ratio ***
      pq = pcgSumProduct(p,q)   ; pq = total(p*q)
      alpha = rho/pq
      pcgScaleAdd1,x,alpha,p    ; x = x+alpha*p
      pcgScaleAdd1,r,-alpha,q   ; r = r-alpha*q
  endif

end

; ================ blowScreens ==================
pro blowScreens
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState

  n = n_atmos
  m = m_atmos
  x = (ones(n) ## findgen(n))-n/2
  y = transpose(x)
  for layer = 0, nlayers-1 do begin
      th = atan(wind[layer,1],wind[layer,0])
      vt = sqrt(wind[layer,1]^2+wind[layer,0]^2)*time
      xprime = cos(th)*x + sin(-th)*y + ((vt/du_atmos) mod m) + n/2
      yprime = sin(th)*x + cos(th)*y + n/2
      screen_h[*,*,layer] = interpolate(base_screen[*,*,layer],xprime,yprime,/cubic)
  endfor
end

; =================== coaddScienceImages ==================
pro coaddScienceImages,sample=sample
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  uncorrected_phase = forwardPropagateStars(screen_h,h,du_atmos,pupil_atmos,science_angle)

  if (AO_mode eq 'MOAO') then begin
      corrected_phase = forwardPropagateStars(volume_est,h,du_tomog,pupil_tomog,science_angle)
  endif
  if (AO_mode eq 'MCAO') then begin
      dm_phase = DM_collapse(volume_est)
      corrected_phase = forwardPropagateStars(dm_phase,dm_heights,du_tomog,pupil_tomog,science_angle)
  endif
  corrected_phase = deformableMirror(corrected_phase,n_atmos)
  if (not labData) then corrected_phase = uncorrected_phase - corrected_phase

  if (keyword_set(sample)) then begin
      psf_open = image_psf(uncorrected_phase,pupil_atmos,science_lambda,du_atmos,plateScale)
      psf_mcao = image_psf(corrected_phase,pupil_atmos,science_lambda,du_atmos,plateScale)
  endif else begin
      psf_open += image_psf(uncorrected_phase,pupil_atmos,science_lambda,du_atmos,plateScale)
      psf_mcao += image_psf(corrected_phase,pupil_atmos,science_lambda,du_atmos,plateScale)
  endelse
end

; ================== displayDiagnostics ===============
pro displayDiagnostics
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  txt = 'corrected science wavefront'
  if (labData) then txt = 'science wavefront correction'
  disp,corrected_phase/microns,txt+runstr,ap=pupil_atmos,dx=[1,1]*du_atmos,x0=-[1,1]*(n_atmos/2)*du_atmos
;  if (display_volumes) then $   ; ignore the display_volumes switch
  disp,pherr/microns,'corrected guide star wavefronts'+runstr,ap=pupil_tomog,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
  disp,volume_est*(metapupil gt .5)/microns,'volume estimate'+runstr,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
  if (not labData) then disp,(screen_h*(rebin(metapupil,n_atmos,n_atmos,nlayers) gt .5))/microns,'atmosphere'+runstr,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
  mas = arcsec/1000.
  dtheta = plateScale/arcsec
  nprime = (size(psf_mcao))[1]; (size(average_psf_corrected))[1]
  if (not labData) then begin
    printStatistics
    disp,(psf_mcao/max(psf_dl)), $
           strtrim(string(science_lambda/microns,format='(f6.2)'),2)+' micron psf (Strehl units)'+runstr, $
           dx=[1,1]*dtheta,x0=-[1,1]*(nprime/2)*dtheta
  endif

end
; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ User-editable routines for diagnostic output stream ~~~~~~~~~~~~~~~~~~~~~~~~~~~
; =============== saveTimeData ===============
pro saveTimeData,display=display
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics
  common timeDiagnostics, psf_vs_t, volume_est_vs_t, screen_vs_t

  if (keyword_set(display)) then begin   ;  --- final display ----
      if (not labData) then disp,reform(transpose(screen_vs_t,[0,2,1,3]),n_atmos*nlayers,n_atmos,n_timesteps)/microns,'atmosphere'
      disp,reform(transpose(volume_est_vs_t,[0,2,1,3]),n_tomog*nlayers,n_tomog,n_timesteps)/microns,'volume est'
      if (not labData) then begin
	      if (n_elements(psf_vs_t) gt 0) then begin
	        n = (size(psf_mcao))[1]
	        disp,reform(transpose(psf_vs_t,[0,2,1,3]),n*n_scienceStars,n,n_timeSteps),'psf'
	      endif
	  endif
     return
  endif

  if (timeStep eq 0) then begin          ; --- initialization ---
      print,'<tomographySphericalWave:saveTimeData> initializing...'
      n = (size(psf_mcao))[1]
      psf_vs_t = reform(fltarr(n,n,n_scienceStars,n_timeSteps),n,n,n_scienceStars,n_timeSteps)
      volume_est_vs_t = reform(fltarr(n_tomog,n_tomog,nlayers,n_timeSteps),n_tomog,n_tomog,nlayers,n_timeSteps)
      screen_vs_t = reform(fltarr(n_atmos,n_atmos,nlayers,n_timeSteps),n_atmos,n_atmos,nlayers,n_timeSteps)
  endif

                                          ;  --- data stream to memory ---
  psf_vs_t[*,*,*,timeStep] = (psf_mcao/max(psf_dl))
  volume_est_vs_t[*,*,*,timeStep] = volume_est*(metapupil gt .5)
  screen_vs_t[*,*,*,timeStep] = screen_h*(rebin(metapupil,n_atmos,n_atmos,nlayers) gt .5)


end
;

; =============== saveRealizationData ==========
pro saveRealizationData,display=display
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics
  common realizationDiagnostics, dummy_vs_r

end
;
;
;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
; ============== savePostProcessingData ==========
pro savePostProcessingData
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics
  common postProcessing

  if ((timeStep eq 0) and (realization eq 0)) then begin          ; --- initialization ---
      print,'<tomographySphericalWave:savePostProcessingData> initializing...'
      post_volume = fltarr(n_atmos,n_atmos,nlayers,n_realizations,n_timeSteps)
      post_volume_est = fltarr(n_tomog,n_tomog,nlayers,n_realizations,n_timeSteps)
  endif

                             ;  --- post processing data stream to memory ---
  post_volume[*,*,*,realization,timeStep] = screen_h
  post_volume_est[*,*,*,realization,timeStep] = volume_est

end
;
; ============== store results to file for later post-processing ============
;
pro saveResults,file
  common postProcessing

  params = packParams()
  save,params,post_volume,post_volume_est,filename=file
end
;
; ============== load results from file for post-processing =============
;
pro loadResults,file
  common constants
  common programSwitches
  common atmosphere
  common tomography
  common simulationState
  common postProcessing
  common timeDiagnostics

  restore,file
  unpackParams,params

  calculateDerivedParameters
  post_volume = reform(post_volume,n_atmos,n_atmos,nlayers,n_realizations,n_timeSteps)
  post_volume_est = reform(post_volume_est,n_atmos,n_atmos,nlayers,n_realizations,n_timeSteps)

  screen_vs_t = post_volume[*,*,*,n_realizations-1,*]
  screen_vs_t = reform(screen_vs_t,n_atmos,n_atmos,nlayers,n_timeSteps)
  volume_est_vs_t = post_volume_est[*,*,*,n_realizations-1,*]
  volume_est_vs_t = reform(volume_est_vs_t,n_atmos,n_atmos,nlayers,n_timeSteps)

  screen_h = post_volume[*,*,*,n_realizations-1,n_timeSteps-1]
  volume_est = post_volume_est[*,*,*,n_realizations-1,n_timeSteps-1]

  if (widget_info(widget_id,/valid_id)) then begin
    tomographySphericalWave_Widget_Realize,widget_id
    id = widget_info(widget_id,find_by_uname='button_forwardPropScienceDirections')
    widget_control,id,/sensitive
  endif
end
;
; ============== post process ==============
;
pro postProcess,wavelengths,science_angles_arg,display=display,stretch=stretch,avgRealizations=avgRealizations
  common constants
  common programSwitches
  common postProcessing
  common tomography
  common atmosphere
  common imaging

  if (keyword_set(display)) then begin
      if (n_elements(stretch) eq 0) then stretch = 'linear' ; 'log' 'sqrt'
      s = size(post_psf_mcao)
      n = s[1]
      if (s[0] eq 3) then n_scienceStars = s[3] else n_scienceStars = 1
;      n_timeSteps = s[4]
      n_realizations = s[4]
      n_wavelengths = s[5]
;      disp, sqrt(reform(transpose(post_psf_mcao,[0,5, 1,2, 3,4]),n*n_wavelengths,n*n_scienceStars,n_timeSteps*n_realizations))
;      disp, sqrt(total(reform(transpose(post_psf_mcao,[0,5, 1,2, 3,4]),n*n_wavelengths,n*n_scienceStars,n_timeSteps*n_realizations),3)/(n_timeSteps*n_realizations))
      u = reform(transpose(post_psf_mcao,[0,4, 1,2, 3]),n*n_wavelengths,n*n_scienceStars,n_realizations)
      if (keyword_set(avgRealizations)) then u = total(u,3)/float(n_realizations)
      if (stretch eq 'linear') then disp,u
      if (stretch eq 'log') then disp,alog10(u>1e-7)
      if (stretch eq 'sqrt') then disp,sqrt(u>0)
      return
  endif

  setconstants
  calculateDerivedParameters
  if (n_elements(wavelengths) eq 0) then wavelengths = [0.55, 0.7, 0.9, 1.2, 1.6, 2.2]*microns
  if (n_elements(science_angles_arg) ne 0) then science_angle = science_angles_arg

  s = size(post_volume)
;  n_realizations = s[4]
;  n_timeSteps = s[5]
  xdl = 4 ; times Nyquist over-sample
  pixelSizes = wavelengths/(xdl*D_tele)
  print,'<postProcess> pixelSizes = ',pixelSizes/arcsec,' arcsec'
  field = 1*arcsec
  n_detector = fix(field/min(pixelSizes))
  n_wavelengths = (size(wavelengths))[1]
  post_psf_mcao = fltarr(n_detector,n_detector,n_scienceStars,n_realizations,n_wavelengths)
  quitFlag = 0
  post_lambda = wavelengths
  post_plateScale = fltarr(n_wavelengths)
  for lambda = 0,n_wavelengths-1 do begin
      science_lambda = wavelengths[lambda]
      plateScale = science_lambda/(du_atmos*n_detector)
      post_plateScale[lambda] = plateScale
      print,'<postProcess> plate scale = ',plateScale/arcsec,' arcsec per pixel'
      diffractionLimitedImage
      strehl_units = max(psf_dl)
      for realization = 0,n_realizations-1 do begin
          for timeStep = 0,n_timeSteps-1 do begin
              print,'wavelength ',science_lambda/microns,' realization ',realization,' timeStep ',timeStep
              screen_h = post_volume[*,*,*,realization,timeStep]
              volume_est = post_volume_est[*,*,*,realization,timeStep]
              coaddScienceImages,/sample
              for star = 0,n_scienceStars-1 do begin
                  post_psf_mcao[*,*,star,realization,lambda] += crop(psf_mcao[*,*,star],n_detector,n_detector)/strehl_units
              endfor

              key = get_kbrd(0)
              if ((key eq 'q') or (quitFlag)) then begin
                  quitFlag = 1
                  print,'<tomographySphericalWave:postProcess> interrupted'
                  break
              endif

          endfor
          if quitFlag then break
      endfor
      if quitFlag then break
  endfor
  post_psf_mcao /= float(n_timeSteps)
end
;
; =============== display Average PSFs =============
pro displayAveragePSFs
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

;  disp,corrected_phase/microns,'corrected science wavefront'+runstr,ap=pupil_atmos,dx=[1,1]*du_atmos,x0=-[1,1]*(n_atmos/2)*du_atmos
;  if (display_volumes) then disp,volume_est/microns,'volume estimate'+runstr,dx=[1,1]*du_tomog,x0=-[1,1]*(n_tomog/2)*du_tomog
;  disp,pherr/microns,'corrected guide star wavefronts'+runstr,ap=pupil_atmos,dx=[1,1]*du_atmos,x0=-[1,1]*(n_atmos/2)*du_atmos

  mas = arcsec/1000.
  dtheta = plateScale/arcsec
  nprime = (size(average_psf_corrected))[1]
  printStatistics
  disp,(average_psf_corrected/max(psf_dl)), $
           strtrim(string(science_lambda/microns,format='(f6.2)'),2)+' micron psf (Strehl units)'+runstr, $
           dx=[1,1]*dtheta,x0=-[1,1]*(nprime/2)*dtheta
;  disp,alog10(average_psf_corrected/max(psf_dl)), $
;           strtrim(string(science_lambda/microns,format='(f6.2)'),2)+' micron psf (Strehl units, log stretch)'+runstr, $
;           dx=[1,1]*dtheta,x0=-[1,1]*(nprime/2)*dtheta

end

; =============== display EE plots ============
pro displayEEplots
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  for gs = 0,n_scienceStars - 1 do begin
;      eeplot,gs
      esqeplot,gs
  endfor

end

; ====================== tip tilt blur ================
function tipTiltBlur,psf,rmsTilt
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  n = (size(psf))[1]
  if ((size(psf))[0] eq 3) then n_objs = (size(psf))[3] else n_objs = 1
  psfb = psf*0
  blur = gauss2(n,n/2,n/2,rmsTilt/pixelScale)
  for obj = 0,n_objs-1 do begin
      psfb[*,*,obj] = ftconvolve(psf[*,*,obj],blur)
  endfor
  return,psfb
end
;
;=======================================================================
pro go

common constants
common programSwitches
common tomography
common atmosphere
common imaging
common simulationState
common visualization
common diagnostics

useGUI = 1

defaultTomographyParameters

if (useGUI) then begin
	tomographySphericalWave_Widget
endif else begin
	tomographySphericalWave
endelse

end
;====== do several simulations, with various parameters =======
pro go_cases
  common constants
  common programSwitches
  common tomography
  common atmosphere
  common imaging
  common simulationState
  common visualization
  common diagnostics

  useGUI = 1

  defaultTomographyParameters
;  >>>>>>>>>>>>>>>>>>>>     put your parameter sets here    <<<<<<<<<<<<<<<<<<<<<<<<<<<<
  n_cases = 3
  science_lambda_set = [.5, 1., 2.2]*microns
  n_iterations = 15 ; overrides the default
;
  for cas = 0,n_cases-1 do begin
      science_lambda = science_lambda_set[cas]
      tomographySphericalWave
  endfor

end
; ======== code that is run in IDL top environment and by the virtual machine  ============
common defaultDirectories
common constants
common programSwitches
common tomography
common controller
common atmosphere
common imaging
common simulationState
common visualization
common diagnostics
common timeDiagnostics
common realizationDiagnostics
common postProcessing

go

end
