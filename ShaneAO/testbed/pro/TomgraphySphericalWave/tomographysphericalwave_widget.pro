; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	03/15/2007 00:28.28
; 
pro base_TomogSphereWave_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)


  wWidget =  Event.top

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='base_TomogSphereWave'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_doSimulation'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        TomographySphericalWave_Widget_doSimulation, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_CheckConfiguration'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_Widget_checkConfiguration, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_forwardPropScienceDirections'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        TomographySphericalWave_Widget_PropScienceWaves, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='textInput_numberOfWFSGuidestars'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_CH' )then $
        TomographySphericalWave_Widget_numWFSGS, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='droplist_scienceWavelength'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DROPLIST' )then $
        tomographySphericalWave_widget_checkDL, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_stop'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        TomographySphericalWave_widget_stopButton, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='checkButton_warmRestart'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_Widget_warmRestart, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_reset'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_reset, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_file_read'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_readParameterFile, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_file_saveas'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_saveas, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_file_readresults'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_ReadResultsFile, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_file_saveresults'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_SaveResults, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_file_quit'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_exit, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_go_check'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_checkConfiguration, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_go_do'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_doSimulation, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_go_prop'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_propScienceWaves, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_go_science'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_propGuidestarWaves, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_go_clearWins'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_Clearwins, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_1'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_Widget_DoCases, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='meun_stop_stop'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        TomographySphericalWave_widget_stopButton, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_display_lastRealization'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_displayLastRealization, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_display_AveragePSFs'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_displayAveragePSFs, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_display_EncircledEnergy'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_displayEncircledEnergy, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='menu_display_diagnosticOutput'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_diagnosticOutput, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='textInput_subapsAcross'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_CH' )then $
        tomographySphericalWave_widget_checkSubaps, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='droplist_subsample'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DROPLIST' )then $
        tomographySphericalWave_widget_checkSize, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_checkSize'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_checkSize, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='droplist_aperture'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DROPLIST' )then $
        tomographySphericalWave_widget_checkDL, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_clearWindows'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        TomographySphericalWave_Widget_Clearwins, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='button_chooseOutputWorkingDirectory'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        tomographySphericalWave_widget_outputDirectoryBrowse, Event
    end
    else:
  endcase

end

pro base_TomogSphereWave, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_

  Resolve_Routine, 'TomographySphericalWave_Widget_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  inputWidth = 10
  fileInputWidth = 50
  
  base_TomogSphereWave = Widget_Base( GROUP_LEADER=wGroup,  $
      UNAME='base_TomogSphereWave' ,XOFFSET=5 ,YOFFSET=5  $
      ,SCR_XSIZE=947 ,SCR_YSIZE=690  $
      ,NOTIFY_REALIZE='TomographySphericalWave_widget_realize'  $
      ,TITLE='TomographySphericalWave Setup' ,SPACE=3 ,XPAD=3 ,YPAD=3  $
      ,MBAR=base_TomogSphereWave_MBAR)

  
  button_doSimulation = Widget_Button(base_TomogSphereWave,  $
      UNAME='button_doSimulation' ,XOFFSET=11 ,YOFFSET=64  $
      ,SCR_XSIZE=137 ,SCR_YSIZE=28 ,/ALIGN_CENTER ,VALUE='Do'+ $
      ' Simulation')

  
  button_CheckConfiguration = Widget_Button(base_TomogSphereWave,  $
      UNAME='button_CheckConfiguration' ,XOFFSET=11 ,YOFFSET=25  $
      ,/ALIGN_CENTER ,VALUE='Check Configuration')

  
  button_forwardPropScienceDirections =  $
      Widget_Button(base_TomogSphereWave,  $
      UNAME='button_forwardPropScienceDirections' ,XOFFSET=11  $
      ,YOFFSET=102 ,SCR_XSIZE=137 ,SCR_YSIZE=29 ,/ALIGN_CENTER  $
      ,VALUE='Prop Science Waves')

  
  base_WFSGuidestarConstellation = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_WFSGuidestarConstellation' ,FRAME=1 ,XOFFSET=178  $
      ,YOFFSET=25 ,SCR_XSIZE=513 ,SCR_YSIZE=165 ,TITLE='IDL' ,SPACE=3  $
      ,XPAD=3 ,YPAD=3)

  
  label_wfsGuidestarConstellation =  $
      Widget_Label(base_WFSGuidestarConstellation,  $
      UNAME='label_wfsGuidestarConstellation' ,XOFFSET=78 ,YOFFSET=2  $
      ,SCR_XSIZE=256 ,SCR_YSIZE=21 ,/ALIGN_LEFT ,VALUE='Wavefront'+ $
      ' Sensing Guidestar Constellation')

  
  label_guidestarConstellationRadius =  $
      Widget_Label(base_WFSGuidestarConstellation,  $
      UNAME='label_guidestarConstellationRadius' ,XOFFSET=13  $
      ,YOFFSET=33 ,/ALIGN_LEFT ,VALUE='Guidestar Constellation Radius'+ $
      ' (arcsec)')

  
  textInput_guidestarConstellationRadius =  $
      Widget_Text(base_WFSGuidestarConstellation,  $
      UNAME='textInput_guidestarConstellationRadius' ,XOFFSET=268  $
      ,YOFFSET=29 ,/EDITABLE ,VALUE=[  $
      '2.5 arcmin' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  label_numberOfWFSGuidestars =  $
      Widget_Label(base_WFSGuidestarConstellation,  $
      UNAME='label_numberOfWFSGuidestars' ,XOFFSET=13 ,YOFFSET=62  $
      ,/ALIGN_LEFT ,VALUE='Number of Wavefront Sensing Guidestars')

  
  droplist_constellationPattern =  $
      Widget_Droplist(base_WFSGuidestarConstellation,  $
      UNAME='droplist_constellationPattern' ,XOFFSET=13 ,YOFFSET=121  $
      ,TITLE='Constellation Pattern' ,VALUE=[ 'Circle', 'One in'+ $
      ' middle', 'Two in middle', 'Three in middle' ])

  
  droplist_altitudeOfWFSGuidestars =  $
      Widget_Droplist(base_WFSGuidestarConstellation,  $
      UNAME='droplist_altitudeOfWFSGuidestars' ,XOFFSET=13  $
      ,YOFFSET=91 ,TITLE='Altitude of Wavefront Sensing Guidestars'  $
      ,VALUE=[ 'Sodium Layer', 'Infinity' ])

  
  textInput_numberOfWFSGuidestars =  $
      Widget_Text(base_WFSGuidestarConstellation,  $
      UNAME='textInput_numberOfWFSGuidestars' ,XOFFSET=267  $
      ,YOFFSET=59 ,/EDITABLE ,/ALL_EVENTS  $
      ,VALUE=[ '6' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  WID_BASE_2 = Widget_Base(base_WFSGuidestarConstellation,  $
      UNAME='WID_BASE_2' ,XOFFSET=361 ,YOFFSET=53 ,TITLE='IDL'  $
      ,COLUMN=1 ,/NONEXCLUSIVE)

  
  button_spatialFilter = Widget_Button(WID_BASE_2,  $
      UNAME='button_spatialFilter' ,/ALIGN_LEFT ,VALUE='spatial'+ $
      ' filtered wfs')

  
  WID_BASE_3 = Widget_Base(base_WFSGuidestarConstellation,  $
      UNAME='WID_BASE_3' ,XOFFSET=364 ,YOFFSET=125 ,TITLE='IDL'  $
      ,COLUMN=1 ,/NONEXCLUSIVE)

  
  button_labData = Widget_Button(WID_BASE_3, UNAME='button_labData'  $
      ,/ALIGN_LEFT ,VALUE='lab data')

  
  base_scienceField = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_scienceField' ,FRAME=1 ,XOFFSET=332 ,YOFFSET=306  $
      ,SCR_XSIZE=362 ,SCR_YSIZE=154 ,TITLE='IDL' ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  label_scienceField = Widget_Label(base_scienceField,  $
      UNAME='label_scienceField' ,XOFFSET=108 ,YOFFSET=6 ,/ALIGN_LEFT  $
      ,VALUE='Science Field')

  
  droplist_scienceField = Widget_Droplist(base_scienceField,  $
      UNAME='droplist_scienceField' ,XOFFSET=7 ,YOFFSET=32  $
      ,SCR_XSIZE=223 ,SCR_YSIZE=24 ,TITLE='Science Field' ,VALUE=[  $
      'IRMOS 5 arcmin', 'NFIRAOS 1 arcmin', '10 arcsec' ])

  
  textInput_numberOfScienceObjects = Widget_Text(base_scienceField,  $
      UNAME='textInput_numberOfScienceObjects' ,XOFFSET=189  $
      ,YOFFSET=65 ,/EDITABLE ,VALUE=[ '3'  $
      ] ,XSIZE=inputWidth ,YSIZE=1)

  
  label_numberOfScienceObjects = Widget_Label(base_scienceField,  $
      UNAME='label_numberOfScienceObjects' ,XOFFSET=8 ,YOFFSET=69  $
      ,/ALIGN_LEFT ,VALUE='Number of Science Objects')

  
  droplist_scienceWavelength = Widget_Droplist(base_scienceField,  $
      UNAME='droplist_scienceWavelength' ,XOFFSET=8 ,YOFFSET=96  $
      ,SCR_XSIZE=280 ,SCR_YSIZE=24 ,TITLE='Science Wavelength'  $
      ,VALUE=[ 'K (2.2 micron)', 'H (1.6 micron)', 'J (1.2 micron)',  $
      'I (0.9 micron)', 'R (0.7 micron)', 'V (0.55 micron)' ])

  
  label_plateScale = Widget_Label(base_scienceField,  $
      UNAME='label_plateScale' ,XOFFSET=10 ,YOFFSET=128 ,/ALIGN_LEFT  $
      ,VALUE='Display Platescale (arcsec)')

  
  textInput_plateScale = Widget_Text(base_scienceField,  $
      UNAME='textInput_plateScale' ,XOFFSET=184 ,YOFFSET=125  $
       ,/EDITABLE ,VALUE=[ '0.0035' ]  $
      ,XSIZE=inputWidth ,YSIZE=1)

  
  label_diffractionLimit = Widget_Label(base_scienceField,  $
      UNAME='label_diffractionLimit' ,XOFFSET=260 ,YOFFSET=127  $
      ,SCR_XSIZE=83 ,SCR_YSIZE=18 ,/ALIGN_LEFT ,VALUE='DL=')

  
  base_RunOptions = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_RunOptions' ,FRAME=1 ,XOFFSET=6 ,YOFFSET=197  $
      ,SCR_XSIZE=323 ,SCR_YSIZE=330 ,TITLE='IDL' ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  label_runOptions = Widget_Label(base_RunOptions,  $
      UNAME='label_runOptions' ,XOFFSET=111 ,YOFFSET=9 ,/ALIGN_LEFT  $
      ,VALUE='Run Options')

  
  droplist_iterationMethod = Widget_Droplist(base_RunOptions,  $
      UNAME='droplist_iterationMethod' ,XOFFSET=18 ,YOFFSET=91  $
      ,SCR_XSIZE=253 ,SCR_YSIZE=24 ,TITLE='Iteration Method' ,VALUE=[  $
      'Conjugate Gradient', 'Linear Feedback' ])

  
  label_maxIterations = Widget_Label(base_RunOptions,  $
      UNAME='label_maxIterations' ,XOFFSET=38 ,YOFFSET=132,  $
      /ALIGN_LEFT ,VALUE='Max Iterations')

  
  textInput_maxIterations = Widget_Text(base_RunOptions,  $
      UNAME='textInput_maxIterations' ,XOFFSET=126 ,YOFFSET=128  $
       ,/EDITABLE ,VALUE=[ '100' ]  $
      ,XSIZE=inputWidth ,YSIZE=1)

  
  button_stop = Widget_Button(base_RunOptions, UNAME='button_stop'  $
      ,XOFFSET=53 ,YOFFSET=182 ,SCR_XSIZE=205 ,SCR_YSIZE=28  $
      ,/ALIGN_CENTER ,VALUE='Stop')

  
  WID_BASE_1 = Widget_Base(base_RunOptions, UNAME='WID_BASE_1'  $
      ,XOFFSET=14 ,YOFFSET=44 ,TITLE='IDL' ,COLUMN=1 ,/NONEXCLUSIVE)

  
  checkButton_tipTiltUnknown_1 = Widget_Button(WID_BASE_1,  $
      UNAME='checkButton_tipTiltUnknown_1' ,/ALIGN_LEFT  $
      ,VALUE='tip/tilt unknown')

  
  label_iteration = Widget_Label(base_RunOptions,  $
      UNAME='label_iteration' ,XOFFSET=231 ,YOFFSET=130 ,SCR_XSIZE=43  $
      ,SCR_YSIZE=18 ,/ALIGN_CENTER ,VALUE='0')

  
  label_nTimeSteps = Widget_Label(base_RunOptions,  $
      UNAME='label_nTimeSteps' ,XOFFSET=15 ,YOFFSET=270 ,/ALIGN_LEFT  $
      ,VALUE='# Time Steps')

  
  label_Time = Widget_Label(base_RunOptions, UNAME='label_Time'  $
      ,XOFFSET=184 ,YOFFSET=268 ,/ALIGN_LEFT ,VALUE='Time =')

  
  label_TimeValue = Widget_Label(base_RunOptions,  $
      UNAME='label_TimeValue' ,XOFFSET=239 ,YOFFSET=268 ,SCR_XSIZE=43  $
      ,SCR_YSIZE=18 ,/ALIGN_CENTER ,VALUE='0')

  
  textInput_nTimeSteps = Widget_Text(base_RunOptions,  $
      UNAME='textInput_nTimeSteps' ,XOFFSET=104 ,YOFFSET=265  $
      ,/EDITABLE ,XSIZE=inputWidth ,YSIZE=1)

  
  label_StopAt = Widget_Label(base_RunOptions, UNAME='label_StopAt'  $
      ,XOFFSET=73 ,YOFFSET=155 ,/ALIGN_LEFT ,VALUE='Stop at')

  
  textInput_StopAt = Widget_Text(base_RunOptions,  $
      UNAME='textInput_StopAt' ,XOFFSET=126 ,YOFFSET=152  $
       ,/EDITABLE ,VALUE=[ '0' ] ,XSIZE=inputWidth  $
      ,YSIZE=1)

  
  label_nmRms = Widget_Label(base_RunOptions, UNAME='label_nmRms'  $
      ,XOFFSET=201 ,YOFFSET=152 ,/ALIGN_LEFT ,VALUE='nm rms')

  
  label_rmsErrorReport = Widget_Label(base_RunOptions,  $
      UNAME='label_rmsErrorReport' ,XOFFSET=258 ,YOFFSET=152  $
      ,SCR_XSIZE=45 ,SCR_YSIZE=19 ,/ALIGN_LEFT ,VALUE='0')

  
  label_RealizationValue = Widget_Label(base_RunOptions,  $
      UNAME='label_RealizationValue' ,XOFFSET=266 ,YOFFSET=231  $
      ,SCR_XSIZE=43 ,SCR_YSIZE=18 ,/ALIGN_CENTER ,VALUE='0')

  
  label_Realization = Widget_Label(base_RunOptions,  $
      UNAME='label_Realization' ,XOFFSET=183 ,YOFFSET=232  $
      ,/ALIGN_LEFT ,VALUE='Realization')

  
  textInput_nRealizations = Widget_Text(base_RunOptions,  $
      UNAME='textInput_nRealizations' ,XOFFSET=105 ,YOFFSET=229  $
       ,/EDITABLE ,XSIZE=inputWidth ,YSIZE=1)

  
  label_nRealizations = Widget_Label(base_RunOptions,  $
      UNAME='label_nRealizations' ,XOFFSET=16 ,YOFFSET=234  $
      ,/ALIGN_LEFT ,VALUE='# Realizations')

  
  WID_BASE_0 = Widget_Base(base_RunOptions, UNAME='WID_BASE_0'  $
      ,XOFFSET=156 ,YOFFSET=43 ,SCR_XSIZE=115 ,SCR_YSIZE=29  $
      ,TITLE='IDL' ,COLUMN=1 ,/NONEXCLUSIVE)

  
  checkButton_warmRestart = Widget_Button(WID_BASE_0,  $
      UNAME='checkButton_warmRestart' ,SCR_XSIZE=107 ,SCR_YSIZE=29  $
      ,/ALIGN_LEFT ,VALUE='warm restart')

  
  label_deltaTime = Widget_Label(base_RunOptions,  $
      UNAME='label_deltaTime' ,XOFFSET=22 ,YOFFSET=300 ,SCR_XSIZE=78  $
      ,SCR_YSIZE=18 ,/ALIGN_LEFT ,VALUE='delta time')

  
  label_seconds = Widget_Label(base_RunOptions, UNAME='label_seconds'  $
      ,XOFFSET=186 ,YOFFSET=299 ,/ALIGN_LEFT ,VALUE='seconds')

  
  textInput_deltaTime = Widget_Text(base_RunOptions,  $
      UNAME='textInput_deltaTime' ,XOFFSET=101 ,YOFFSET=294  $
      ,/EDITABLE ,XSIZE=inputWidth ,YSIZE=1)

  
  button_reset = Widget_Button(base_RunOptions, UNAME='button_reset'  $
      ,XOFFSET=275 ,YOFFSET=43 ,SCR_XSIZE=40 ,SCR_YSIZE=28  $
      ,/ALIGN_CENTER ,VALUE='reset')

  
  menu_File = Widget_Button(base_TomogSphereWave_MBAR,  $
      UNAME='menu_File' ,/MENU ,VALUE='File')

  
  menu_file_read = Widget_Button(menu_File, UNAME='menu_file_read'  $
      ,VALUE='Read Parameter File')

  
  menu_file_saveas = Widget_Button(menu_File,  $
      UNAME='menu_file_saveas' ,VALUE='Save Parameters ...')

  
  menu_file_readresults = Widget_Button(menu_File,  $
      UNAME='menu_file_readresults' ,/SEPARATOR ,VALUE='Read Results'+ $
      ' File')

  
  menu_file_saveresults = Widget_Button(menu_File,  $
      UNAME='menu_file_saveresults' ,VALUE='Save Results ...')

  
  menu_file_quit = Widget_Button(menu_File, UNAME='menu_file_quit'  $
      ,/SEPARATOR ,VALUE='Quit')

  
  menu_go = Widget_Button(base_TomogSphereWave_MBAR, UNAME='menu_go'  $
      ,/MENU ,VALUE='Go')

  
  menu_go_check = Widget_Button(menu_go, UNAME='menu_go_check'  $
      ,VALUE='Check Configuration')

  
  menu_go_do = Widget_Button(menu_go, UNAME='menu_go_do' ,VALUE='Do'+ $
      ' Simulation')

  
  menu_go_prop = Widget_Button(menu_go, UNAME='menu_go_prop'  $
      ,VALUE='Prop Science Waves')

  
  menu_go_science = Widget_Button(menu_go, UNAME='menu_go_science'  $
      ,VALUE='Prop Guidestar Waves')

  
  menu_go_clearWins = Widget_Button(menu_go,  $
      UNAME='menu_go_clearWins' ,VALUE='Clear Windows')

  
  W_MENU_1 = Widget_Button(menu_go, UNAME='W_MENU_1' ,/SEPARATOR  $
      ,VALUE='Do Cases 1,2,3,...')

  
  menu_stop = Widget_Button(base_TomogSphereWave_MBAR,  $
      UNAME='menu_stop' ,/MENU ,VALUE='Stop')

  
  meun_stop_stop = Widget_Button(menu_stop, UNAME='meun_stop_stop'  $
      ,VALUE='Stop')

  
  menu_Display = Widget_Button(base_TomogSphereWave_MBAR,  $
      UNAME='menu_Display' ,/MENU ,VALUE='Display')

  
  menu_display_lastRealization = Widget_Button(menu_Display,  $
      UNAME='menu_display_lastRealization' ,VALUE='Last Realization')

  
  menu_display_AveragePSFs = Widget_Button(menu_Display,  $
      UNAME='menu_display_AveragePSFs' ,VALUE='Average PSFs')

  
  menu_display_EncircledEnergy = Widget_Button(menu_Display,  $
      UNAME='menu_display_EncircledEnergy' ,VALUE='Encircled Energy')

  
  menu_display_diagnosticOutput = Widget_Button(menu_Display,  $
      UNAME='menu_display_diagnosticOutput' ,VALUE='Diagnostic'+ $
      ' Output')

  
  menu_Help = Widget_Button(base_TomogSphereWave_MBAR,  $
      UNAME='menu_Help' ,VALUE='Help')

  
  base_Telescope = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_Telescope' ,FRAME=1 ,XOFFSET=703 ,YOFFSET=26  $
      ,SCR_XSIZE=217 ,SCR_YSIZE=147 ,TITLE='IDL' ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  label_telescope = Widget_Label(base_Telescope,  $
      UNAME='label_telescope' ,XOFFSET=75 ,YOFFSET=3 ,/ALIGN_LEFT  $
      ,VALUE='Telescope')

  
  label_subapsAcross = Widget_Label(base_Telescope,  $
      UNAME='label_subapsAcross' ,XOFFSET=6 ,YOFFSET=56 ,/ALIGN_LEFT  $
      ,VALUE='Subapertures Across')

  
  textInput_subapsAcross = Widget_Text(base_Telescope,  $
      UNAME='textInput_subapsAcross' ,XOFFSET=141 ,YOFFSET=53  $
       ,/EDITABLE ,/ALL_EVENTS ,VALUE=[  $
      '100' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  droplist_subsample = Widget_Droplist(base_Telescope,  $
      UNAME='droplist_subsample' ,XOFFSET=4 ,YOFFSET=80  $
      ,TITLE='Subsample' ,VALUE=[ '1x', '2x', '3x', '4x' ])

  
  button_checkSize = Widget_Button(base_Telescope,  $
      UNAME='button_checkSize' ,XOFFSET=168 ,YOFFSET=113  $
      ,SCR_XSIZE=40 ,SCR_YSIZE=26 ,/ALIGN_CENTER ,TOOLTIP='calculate'+ $
      ' the fine grid array size' ,VALUE='check')

  
  label_size = Widget_Label(base_Telescope, UNAME='label_size'  $
      ,XOFFSET=8 ,YOFFSET=117 ,SCR_XSIZE=89 ,SCR_YSIZE=19  $
      ,/ALIGN_LEFT ,VALUE='Fine Grid Size')

  
  text_size = Widget_Text(base_Telescope, UNAME='text_size'  $
      ,XOFFSET=101 ,YOFFSET=114  ,XSIZE=inputWidth  $
      ,YSIZE=1)

  
  droplist_aperture = Widget_Droplist(base_Telescope,  $
      UNAME='droplist_aperture' ,XOFFSET=6 ,YOFFSET=24 ,SCR_XSIZE=197  $
      ,SCR_YSIZE=24 ,TITLE='Aperture' ,VALUE=[ '30 meters', '10'+ $
      ' meters', 'Keck' ])

  
  button_clearWindows = Widget_Button(base_TomogSphereWave,  $
      UNAME='button_clearWindows' ,XOFFSET=9 ,YOFFSET=143  $
      ,SCR_XSIZE=139 ,SCR_YSIZE=27 ,/ALIGN_CENTER ,VALUE='Clear'+ $
      ' Windows')

  
  label_runID = Widget_Label(base_TomogSphereWave,  $
      UNAME='label_runID' ,XOFFSET=392 ,YOFFSET=474 ,/ALIGN_LEFT  $
      ,VALUE='Run ID')

  
  text_runID = Widget_Text(base_TomogSphereWave, UNAME='text_runID'  $
      ,XOFFSET=439 ,YOFFSET=471  ,VALUE=[  $
      '0' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  base_Atmosphere = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_Atmosphere' ,FRAME=1 ,XOFFSET=704 ,YOFFSET=182  $
      ,SCR_XSIZE=216 ,SCR_YSIZE=187 ,TITLE='IDL' ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  label_atmosphere = Widget_Label(base_Atmosphere,  $
      UNAME='label_atmosphere' ,XOFFSET=61 ,YOFFSET=3 ,/ALIGN_LEFT  $
      ,VALUE='Atmosphere')

  
  label_r0 = Widget_Label(base_Atmosphere, UNAME='label_r0'  $
      ,XOFFSET=11 ,YOFFSET=38 ,SCR_XSIZE=54 ,SCR_YSIZE=18  $
      ,/ALIGN_LEFT ,VALUE='r_0 (cm)')

  
  droplist_Cn2Profile = Widget_Droplist(base_Atmosphere,  $
      UNAME='droplist_Cn2Profile' ,XOFFSET=8 ,YOFFSET=65 ,TITLE='Cn2'+ $
      ' Profile' ,VALUE=[ 'Cerro Pachon', 'Bad-Bad', 'Test', 'Mauna'+ $
      ' Kea', 'CN-MK1' ])

  
  textInput_r0 = Widget_Text(base_Atmosphere, UNAME='textInput_r0'  $
      ,XOFFSET=73 ,YOFFSET=37 ,/EDITABLE  $
      ,VALUE=[ '16' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  seed_label = Widget_Label(base_Atmosphere, UNAME='seed_label'  $
      ,XOFFSET=29 ,YOFFSET=106 ,/ALIGN_LEFT ,VALUE='Seed')

  
  seed_text = Widget_Text(base_Atmosphere, UNAME='seed_text'  $
      ,XOFFSET=75 ,YOFFSET=101 ,/EDITABLE  $
      ,VALUE=[ '5' ] ,XSIZE=inputWidth ,YSIZE=1)

  
  droplist_Wind = Widget_Droplist(base_Atmosphere,  $
      UNAME='droplist_Wind' ,XOFFSET=6 ,YOFFSET=138 ,TITLE='Wind'+ $
      ' Profile' ,VALUE=[ 'random 10m/s', '' ])

  
  text_messageBox = Widget_Text(base_TomogSphereWave,  $
      UNAME='text_messageBox' ,XOFFSET=523 ,YOFFSET=467  $
       ,/SCROLL ,/WRAP ,VALUE=[ '' ]  $
      ,XSIZE=fileInputWidth ,YSIZE=5)

  
  label_guiID = Widget_Label(base_TomogSphereWave,  $
      UNAME='label_guiID' ,XOFFSET=390 ,YOFFSET=515 ,/ALIGN_LEFT  $
      ,VALUE='GUI ID')

  
  text_guiID = Widget_Text(base_TomogSphereWave, UNAME='text_guiID'  $
      ,XOFFSET=442 ,YOFFSET=509  ,XSIZE=inputWidth  $
      ,YSIZE=1)

  
  text_parameterFilename = Widget_Text(base_TomogSphereWave,  $
      UNAME='text_parameterFilename' ,XOFFSET=133 ,YOFFSET=537  $
       ,XSIZE=fileInputWidth ,YSIZE=1)

  
  label_parameterFile = Widget_Label(base_TomogSphereWave,  $
      UNAME='label_parameterFile' ,XOFFSET=24 ,YOFFSET=542  $
      ,/ALIGN_LEFT ,VALUE='Parameter File')

  
  label_outputFile = Widget_Label(base_TomogSphereWave,  $
      UNAME='label_outputFile' ,XOFFSET=23 ,YOFFSET=576 ,/ALIGN_LEFT  $
      ,VALUE='Output File')

  
  text_outputFilename = Widget_Text(base_TomogSphereWave,  $
      UNAME='text_outputFilename' ,XOFFSET=132 ,YOFFSET=573  $
      ,XSIZE=fileInputWidth ,YSIZE=1)

  
  button_chooseOutputWorkingDirectory =  $
      Widget_Button(base_TomogSphereWave,  $
      UNAME='button_chooseOutputWorkingDirectory' ,XOFFSET=565  $
      ,YOFFSET=572 ,/ALIGN_CENTER ,VALUE='Choose Directory')

  
  base_TipTiltStarConstellation = Widget_Base(base_TomogSphereWave,  $
      UNAME='base_TipTiltStarConstellation' ,FRAME=1 ,XOFFSET=333  $
      ,YOFFSET=199 ,SCR_XSIZE=358 ,SCR_YSIZE=101 ,TITLE='IDL'  $
      ,SPACE=3 ,XPAD=3 ,YPAD=3)

  
  label_TipTiltConstellation =  $
      Widget_Label(base_TipTiltStarConstellation,  $
      UNAME='label_TipTiltConstellation' ,XOFFSET=54 ,YOFFSET=4  $
      ,/ALIGN_LEFT ,VALUE='Tip Tilt Star Constellation')

  
  label_nTipTiltStars = Widget_Label(base_TipTiltStarConstellation,  $
      UNAME='label_nTipTiltStars' ,XOFFSET=164 ,YOFFSET=29  $
      ,/ALIGN_LEFT ,VALUE='# Tip Tilt Stars')

  
  textInput_nTipTiltStars =  $
      Widget_Text(base_TipTiltStarConstellation,  $
      UNAME='textInput_nTipTiltStars' ,XOFFSET=253 ,YOFFSET=26  $
       ,/EDITABLE ,XSIZE=inputWidth ,YSIZE=1)

  
  droplist_TipTiltConstellation =  $
      Widget_Droplist(base_TipTiltStarConstellation,  $
      UNAME='droplist_TipTiltConstellation' ,XOFFSET=8 ,YOFFSET=59  $
      ,SCR_XSIZE=299 ,SCR_YSIZE=24 ,TITLE='TipTiltConstellation'  $
      ,VALUE=[ 'Polygon', 'Random, Galactic Pole', 'Random, glat ='+ $
      ' 45', 'Random, Galactic Equator' ])

  
  WID_BASE_4 = Widget_Base(base_TipTiltStarConstellation,  $
      UNAME='WID_BASE_4' ,XOFFSET=4 ,YOFFSET=23 ,TITLE='IDL'  $
      ,COLUMN=1 ,/NONEXCLUSIVE)

  
  button_UseTipTilt = Widget_Button(WID_BASE_4,  $
      UNAME='button_UseTipTilt' ,/ALIGN_LEFT ,VALUE='Use Tip/Tilt'+ $
      ' Stars')

  Widget_Control, /REALIZE, base_TomogSphereWave

  XManager, 'base_TomogSphereWave', base_TomogSphereWave, /NO_BLOCK  

end
; 
; Empty stub procedure used for autoloading.
; 
pro TomographySphericalWave_Widget, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  base_TomogSphereWave, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
