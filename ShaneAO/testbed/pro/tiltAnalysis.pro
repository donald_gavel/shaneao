if (n_elements(wno) eq 0) then begin
  wno = 0
endif
;clearwins
;wno = 0
;!p.background = 255
;!p.color = 0
!p.charsize = 2
; datasets available from 4/16/08:
;   1,2,3,5,7,8,9,10,11
dset_set= ['000','001','002','003','004']
dset_set = ['003']; ['028'] was a good cancellation at 3 Hz
wfs = 'open loop' ; 'closed loop' ; <--------------- which wavefront sensor!
n_sets = (size(dset_set))[1]

;for this_set = 0,n_sets-1 do begin
this_set = 0

dset = dset_set[this_set]; '003'
;dir = 'C:\Documents and Settings\Don Gavel\Desktop\Data\080416\'
;dir = 'C:\Documents and Settings\Don Gavel\Desktop\Data\080606\'
;dir = 'C:\Documents and Settings\Don Gavel\Desktop\Data\080612\'
;dir = '/u/user/observers/gavel/data/080917/'
dir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/081023/'
dir = 'C:\Users\DonGavel\Desktop\AdaptiveOptics_Vaio\LAO\Villages\Data\081024\'
;dir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/080916/'
u = rdview(dir+'tt_'+dset)
n = 1024
fs = 1000
fc = 100 ; roughly 20 hz control bandwidth
dt = 1./fs
df = fs/float(n)
t = findgen(n)*dt
f = findgen(n)*df
pixelsPerVolt = 4.0 ; 5.0
arcsecPerPixel = 2.0

if (wfs eq 'closed loop') then begin
  tiltx = u[0,*]*arcsecPerPixel
  tilty = u[1,*]*arcsecPerPixel
endif
if (wfs eq 'open loop') then begin
  tiltx = u[2,*]*arcsecPerPixel
  tilty = u[3,*]*arcsecPerPixel
endif

print,'rms residual tip/tilts: x:',rms(tiltx),' y:',rms(tilty),' arcsec'
tiltxc = (u[4,*]-average(u[4,*]))*pixelsPerVolt*arcsecPerPixel
tiltyc = (u[5,*]-average(u[5,*]))*pixelsPerVolt*arcsecPerPixel
print,'rms controled tip/tilts: x:',rms(tiltxc),' y:',rms(tiltyc),' arcsec'
print,'rms 2-axis tip/tilt from '+wfs+' wfs:',sqrt(rms(tiltx)^2+rms(tilty)^2),' arcsec'

window,wno,title='x tip/tilt '+dset & wno++
plot,t,tiltxc,xtitle='time, sec',ytitle='arcsec',title='x tip/tilt '+dset
oplot,t,tiltx,color=150

window,wno,title='y tip/tilt '+dset & wno++
plot,t,tiltyc,xtitle='time, sec',ytitle='arcsec',title='y tip/tilt '+dset
oplot,t,tilty,color=150

fx = reform(abs(fft(tiltx)))*n*dt
fy = reform(abs(fft(tilty)))*n*dt
fxc = reform(abs(fft(tiltxc)))*n*dt
fyc = reform(abs(fft(tiltyc)))*n*dt
fxu = reform(abs(fft(tiltx+tiltxc)))*n*dt
fyu = reform(abs(fft(tilty+tiltyc)))*n*dt
tfx = fx/fxu
tfy = fy/fyu
tfm = sqrt(f^2/(fc^2+f^2)) ; model rejection spectrum

; do a power-law fit to the tilt spectra
n1=1
n2=fix(40./df) ; from df to 40 Hz
nfit = n2-n1+1
w = ones(nfit)
fp = alog10(f[n1:n2])
; x axis
sp = alog10(fxc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
abx = invert(aa) ## ay
sbarx = 10^(abx[0])*f^(-abx[1])
sbarx[0] = 0.
; y axis
sp = alog10(fyc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
aby = invert(aa) ## ay
sbary = 10^(aby[0])*f^(-aby[1])
sbary[0] = 0.
; ------- end of power law fits -----

window,wno,title='x tilt spectra '+dset & wno++
powerSpectrumPlot,fxc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='x tilt spectra '+dset
powerSpectrumPlot,fx,fs,/oplot,color=150
powerSpectrumPlot,sbarx,fs,/oplot,color=100
powerSpectrumPlot,sbarx*tfm,fs,/oplot,color=120
window,wno,title='y tilt spectra '+dset & wno++
powerSpectrumPlot,fyc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='y tilt spectra '+dset
powerSpectrumPlot,fy,fs,/oplot,color=150
powerSpectrumPlot,sbary,fs,/oplot,color=100
powerSpectrumPlot,sbary*tfm,fs,/oplot,color=120
window,wno,title='Transfer Functions ' +dset & wno++
powerSpectrumPlot,0.5*tfx,fs,/xlog,/ylot,ytitle='transfer function',xtitle='frequency, hz',title='Transfer Functions '+dset
powerSpectrumPlot,0.5*tfy,fs,/oplot,color=150
powerSpectrumPlot,0.5*tfm,fs,/oplot,color=100
window,wno,title='Zoom of x tilt residuals '+dset & wno++
plot,t[100:200],tiltx[100:200],ytitle='arcsec',xtitle='seconds',title='zoom in of x tilt residual '+dset
oplot,t[100:200],tiltx[100:200],color=150
oplot,t[100:200],tiltxc[100:200]-average(tiltxc[100:200])

window,wno,title='tip/tilt yarn ball '+dset & wno++
plot,tiltxc,tiltyc,title = 'tip/tilt yarn ball '+dset,xrange = [-1.,1.],yrange=[-1.,1.]
oplot,tiltx,tilty,color=150
window,wno,title='tt resid yarn ball '+dset & wno++
plot,tiltx,tilty,title = 'tt resid yarn ball '+dset,xrange=[-.1,.1],yrange=[-.1,.1],/nodata
oplot,tiltx,tilty,color=150
;
; print statixticsl analysis
print,'total in atmospheric model (x,y):', sqrt(total((sbarx)^2))*df,sqrt(total((sbary)^2))*df,' arcsec'
print,'total in control data (x,y):', sqrt(total(fxc^2))*df,sqrt(total(fyc^2))*df,' arcsec'
print,'total in residual model (x,y):', sqrt(total((sbarx*tfm)^2))*df,sqrt(total((sbary*tfm)^2))*df,' arcsec'
print,'total in residual data (x,y):', sqrt(total(fx^2))*df,sqrt(total(fy^2))*df,' arcsec'

;endfor

end
