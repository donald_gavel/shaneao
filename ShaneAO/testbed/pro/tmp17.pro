n = 256
pupil = circle(n,n,n/2,n/2,n/4,1.)
dx = 8*2*!pi/float(n)
x = (ones(n) ## findgen(n)-n/2)*dx
y = transpose(x)
r2 = .01*(x^2+y^2)
r2s = shift(r2,20,0)+x
ph = r2s+r2
sph = sin(ph)*pupil
end

