;+
; window.pro
;   generate 2-d window functions
;
; see: http://en.wikipedia.org/wiki/Window_function
;-
function window,n,radial=radial,rin=r0
  a = [0.35875, 0.48829, 0.14128, 0.01168] ; Blackman-Harris
  x = ones(n) ## findgen(n)
  y = transpose(x)
  fn1 = float(n)
  if (n_elements(r0) ne 0) then begin
    r = fltarr(n/2)
    np = n/2-r0-1
    r[0:r0-1] = 0.
    r[r0:r0+np] = findgen(np+1)
    fn1 = float(2*np)
    r += np
    f = a[0]-a[1]*cos(2*!pi*r/fn1)+a[2]*cos(4*!pi*r/fn1)-a[3]*cos(6*!pi*r/fn1)
    x -= n/2
    y = transpose(x)
    r = sqrt(x^2+y^2) < n/2
    w = f[r]
    return,w
  endif
  if (keyword_set(radial)) then begin
    r = sqrt((x-n/2)^2+(y-n/2)^2)+n/2
    w = a[0]-a[1]*cos(2*!pi*r/fn1)+a[2]*cos(4*!pi*r/fn1)-a[3]*cos(6*!pi*r/fn1)
    w *= circle(n,n,n/2,n/2,n/2,1)
  endif else begin
    wx = a[0]-a[1]*cos(2*!pi*x/fn1)+a[2]*cos(4*!pi*x/fn1)-a[3]*cos(6*!pi*x/fn1)
    w = wx*transpose(wx)
  endelse
  return,w
end
n = 64
w = window(n)
w1 = window(n,/radial)
wr0 = window(n,rin=n/4)
plot,w[*,n/2]
oplot,w1[*,n/2],linestyle=2,color=150
oplot,wr0[*,n/2],linestyle=1,color=200
disp,w,'separable window'
disp,w1,'radially symmetric window'
disp,wr0,'buffered window'
end
