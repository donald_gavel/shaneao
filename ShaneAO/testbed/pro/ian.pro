;IaN.pro
; Katie Morzinski     ktmorz@ucolick.org     15 Nov 2007
;
; This function inputs an array containing float('NaN') values
; and outputs the same array but with the NaN's replaced by 0's.
; OR: with the keyword_set: /reduce_array, reduces the size of the array
; by the number of NaN's and returns a (smaller) array with only IaN's.
; (IaN stands for "Is a Number", because NaN stands for "Not a Number".)
;
; Input: NaNarr--> An array containing some NaN values.
; Optional input: /reduce_array
; Output: IaNarr--> Same array but with the NaN's set to 0's.
; Optional output: NaN_indices=NaN_indices --> 1-d indices where the NaN's WERE in the old array.
;
; Example 1:
; (IF NOT keyword_set(reduce_array))
; ----------------------------------
; IDL>  a = [0,1,2,float('NaN'),float('NaN'),5]
; IDL>  print,a
; ;     0.00000     1.00000     2.00000         NaN         NaN     5.00000
; IDL>  print,IaN(a,IaN_i=IaN_i,NaN_i=NaN_i)
; ;     0.00000     1.00000     2.00000     0.00000     0.00000     5.00000
; IDL>  print,IaN_i
; ;     0     1     2     5
; IDL>  print,NaN_i
; ;     3     4
;
; Example 2:
; (IF keyword_set(reduce_array))
; ------------------------------
; IDL>  a = [0,1,2,float('NaN'),float('NaN'),5]
; IDL>  print,a
; ;     0.00000     1.00000     2.00000         NaN         NaN     5.00000
; IDL>  print,IaN(a,/reduce_array,IaN_i=IaN_i,NaN_i=NaN_i)
; ;     0.00000     1.00000     2.00000     5.00000
; IDL>  print,IaN_i
; ;     0     1     2     5
; IDL>  print,NaN_i
; ;     3     4
;
; Example 3:
; (If original array has ZERO NaN's to begin with.)
; ------------------------------
; IDL>  a = findgen(6)
; IDL>  print,a
; ;     0.00000     1.00000     2.00000     3.00000     4.00000     5.00000
; IDL>  print,IaN(a,IaN_i=IaN_i,NaN_i=NaN_i)
; ;     0.00000     1.00000     2.00000     3.00000     4.00000     5.00000
; IDL>  print,IaN_i
; ;     0     1     2     3     4     5
; IDL>  print,NaN_i
; ;     -1

function IaN, $;ian.pro
	NaNarr, $;input
	reduce_array=reduce_array, $;optional (keyword) input
	NaN_indices=NaN_indices, IaN_indices=IaN_indices;optional outputs (1-dimensional indices)

	IaNarr = NaNarr
	ndim = (size(NaNarr))[0];number of dimensions in array NaNarr
	nelem = (size(NaNarr))[ndim+2];number of elements in array NaNarr

	; First check to see whether the array has any NaN's at all:
	; (If there are no NaN's at all, we will simply return the original array.)
	count = 0L
	FOR i=0L,nelem-1 DO IF finite(NaNarr[i],/NaN) THEN count=count+1;count = # of NaN's in array
	IF count LT 1 THEN BEGIN;No NaN's in original array.
		IaN_indices = indgen(nelem);ALL of the original elements were IaN
		NaN_indices = -1;returns "-1" as index, if NO elements were NaN (same idea as WHERE returning "-1")
		return,IaNarr
	ENDIF

	; If the array DOES have NaN's, go into one of these 2 loops:
	IF NOT keyword_set(reduce_array) THEN BEGIN
		;In this case we leave the number of elements the same
		; and we simply replace the NaN's with zeroes (place-holders).
		count = 0L
		FOR i=0L,nelem-1 DO BEGIN
			IF finite(NaNarr[i],/NaN) THEN BEGIN
				IaNarr[i] = 0.0
				count = count+1
			ENDIF
		ENDFOR
		IaN_indices = intarr(nelem-count)
		NaN_indices = intarr(count)
		NaN_count = 0L
		IaN_count = 0L
		FOR i=0L,nelem-1 DO BEGIN
			IF finite(NaNarr[i],/NaN) THEN BEGIN
				NaN_indices[NaN_count] = i
				NaN_count = NaN_count+1
			ENDIF ELSE BEGIN
				IaN_indices[IaN_count] = i
				IaN_count = IaN_count+1
			ENDELSE
		ENDFOR
	ENDIF ELSE BEGIN;yes, reduce the array.
		;In this case, array NaNarr is reduced in dimensions,
		; such that the new array contains only the original IaN's,
		; while the elements that USED to be NaN's are completely
		; removed from the array (with no place-holding zeroes used).
		count = 0L
		FOR i=0L,nelem-1 DO IF finite(NaNarr[i],/NaN) THEN count=count+1
		IaNarr = fltarr(nelem-count)
		IaN_indices = intarr(nelem-count)
		NaN_indices = intarr(count)
		NaN_count = 0L
		IaN_count = 0L
		FOR i=0L,nelem-1 DO BEGIN
			IF finite(NaNarr[i],/NaN) THEN BEGIN
				NaN_indices[NaN_count] = i
				NaN_count = NaN_count+1
			ENDIF ELSE BEGIN
				IaN_indices[IaN_count] = i
				IaN_count = IaN_count+1
			ENDELSE
		ENDFOR
		FOR i=0L,nelem-NaN_count-1 DO IaNarr = NaNarr[IaN_indices]
	ENDELSE

	return,IaNarr
end
