; prepspd.pro - prepare for speckle deconvolution step by retrieving
;               psf star and mtf data

pro prepspd,dataset,ps,phase,ref_ps,ref_phase,mtf
  ps = rdview(dataset.resultsdir + dataset.basename + "_ps")
  phase = rdview(dataset.resultsdir + dataset.basename + "_phase")
  stardir = dataset.resultsdir + "../" + dataset.starname + "/"
  ref_ps = rdview(stardir + dataset.starname + "_ps")
  ref_phase = rdview(stardir + dataset.starname + "_phase")
  mtf = rdview(dataset.mtffile)
end
