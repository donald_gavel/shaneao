n = 128
r0 = .1
i = complex(0.,1.)
du = 6./float(n)
f = filtergen(n,n,r0,du)
seed = 5
s = screengen_f(f,seed)
ap = circle(n,n,n/2,n/2,1./du,1)
;
x = (findgen(n)-n/2)*du # ones(n)
y = transpose(x)
px = 10*du
py = 10*du
s = sin(2*!pi*(x/px + y/py))
a0 = abs(ft(ap))^2
a = abs(ft(ap*exp(i*s/1000.)))^2
u = a-a0 + reverse(reverse(a-a0,2))
end
