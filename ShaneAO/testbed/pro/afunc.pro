;+
; afunc.pro - dummy function
;-
pro afunc,r,n
  sw = supergauss(256,128,128,55,10)
  fuzz = 10.^(-8)
  for i=0,n-1 do begin
    r(*,*,i) = (r(*,*,i)>fuzz)*sw
    print,format='($,I0," ")',i
  endfor
end