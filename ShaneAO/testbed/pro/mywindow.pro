pro mywindow,id,_extra=_extra
 print,'<mywindow>'
 defsysv,'!w',exists=exists
 if (not exists) then myScreen
 if (total(window_valid(/list)) eq 0) then begin
   myScreen
 endif
 if (n_elements(id) eq 0) then id = 0
 window,id,xpos=!w.xoffset,ypos=!w.yoffset,_extra=_extra
 ; algorithm for casecading windows
 !w.xoffset -= !w.width/4
 !w.yoffset += !w.height/4
end
