;+
;====================
;   planetcoords.pro - convert from lat,long to x,y,z and vice-versa
;====================
;-
function imagecoords,lat,lon,cx,cy,rx,ry,selat,selon,th ; return x,y,z
  degrees = !pi/180.
  sinlat = sin(lat * degrees)
  coslat = cos(lat * degrees)
  sinlon = -sin((lon-selon) * degrees)
  coslon = cos((lon-selon) * degrees)
  alpha = selat * degrees
  theta = th * degrees
  r = sqrt( (coslat/rx)^2 + (sinlat/ry)^2 )^(-1)
  x = r * coslat * sinlon
  y = r * sinlat
  z = r * coslat * coslon
  xprime = x
  yprime = y*cos(alpha) - z*sin(alpha)
  zprime = y*sin(alpha) + z*cos(alpha)
  xobs = xprime*cos(theta) - yprime*sin(theta)
  yobs = xprime*sin(theta) + yprime*cos(theta)
  zobs = zprime
  xobs = xobs + cx
  yobs = yobs + cy
  return,[xobs,yobs,zobs]
end

function planetcoords,x0,y0,cx,cy,rx,ry,selat,selon,th,x,y,z ; return [lat,lon]
  degrees = !pi/180.
  alpha = selat * degrees
  theta = th * degrees
  xobs = x0 - cx
  yobs = y0 - cy
  xprime = xobs*cos(theta) + yobs*sin(theta)
  yprime = -xobs*sin(theta) + yobs*cos(theta)
  det = 1 - (xprime/rx)^2 - (yprime/ry)^2
  if (det lt 0) then begin
;    print,'<planetcoords> not on ellipse: det < 0'
    return,[-99,-99]
  endif
  zprime = rx * sqrt(det)
  x = xprime
  y = yprime*cos(alpha) + zprime*sin(alpha)
  z = -yprime*sin(alpha) + zprime*cos(alpha)
  r0 = sqrt(x^2 + y^2 + z^2)
  sinlat = y/r0
  lat = asin(sinlat)
  lon = -atan(x,z)
  return,[lat/degrees,(lon/degrees+selon) mod 360.]
end

  