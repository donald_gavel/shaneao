;
;  cshift - centroid shift
;
pro cshift,incube,outcube,threshold = th
  nx = (size(incube))(1)
  ny = (size(incube))(2)
  n = (size(incube))(3)
  if n_elements(outcube) eq 0 then outcube = make_array(nx,ny,n)
  if n_elements(th) eq 0 then th = 40
  if n_elements(gws) eq 0 then gws = 55
  mask = make_array(nx,ny)
  mask(5:(nx-6),50:(ny-5))=1 ; 50 masks off the divot
  print,"centeroid shift ..."
  for i=0,(n-1) do begin
    tmps = smooth(incube(*,*,i),5)
    center = centroid(((tmps>th)-th)*mask)
    outcube(*,*,i) = shift(incube(*,*,i),(nx/2)-center(0),(ny/2)-center(1))
    print,format='($,I0," ")',i
  endfor
  print,"done"
end
