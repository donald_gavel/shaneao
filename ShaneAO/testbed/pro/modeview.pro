;+
;  modeView.pro
;    view the orthonormal modes of a square symmetric matrix
;-
function modeView,a_arg,thresh=thresh,ap=ap,w=w,dewaffle=dewaffle
  a = a_arg
  if (n_elements(thresh) eq 0) then thresh = 0.15
  n = (size(a))[1]
  na = round(sqrt(n))
  if (n_elements(dewaffle) ne 0) then begin
    q = wafflePenalty(na)
    a = a+q*dewaffle
  endif
  if (n_elements(ap) ne 0) then begin
    idx = where(ap)
    am = a[idx,*]
    am = am[*,idx]
    nap = total(ap)
    b = pseudoinverse(am,thresh,modeCount,w=w,u=u,v=v)
    idw = reverse(sort(w))
    w = w[idw]
    v = transpose(v)
    v_modes = fltarr(n,nap)
    for k=0,nap-1 do v_modes[idx,idw[k]] = v[*,k]
    v_modes = reform(v_modes,na,na,nap)
    plot,w,/ylog,psym=3,title='singular values'
    disp,v_modes
    return,v_modes
  endif
  b = pseudoinverse(a,thresh,modeCount,w=w,u=u,v=v)
  idx = reverse(sort(w))
  w = w[idx]
  window,/free
  plot,w,/ylog,psym=3,title='singular values'
  v_modes = reform(transpose(v[idx,*]),na,na,n)
  disp,v_modes,'modes'
  return,v_modes
end
; generate the waffle penalty matrix separately
function wafflePenalty,na
  ker = [[1,-1],[-1,1]]
  n = long(na)*long(na)
  q = fltarr(na,na,n)
  k = 0
  for i = 0,na-2 do begin
    for j = 0,na-2 do begin
      q[i:i+1,j:j+1,k] = ker
      k++
    endfor
  endfor
  q = reform(q,n,n)
  return,q
end
; for reference, depict the "Fourier" modes on a square n x n grid
function fourierModes,na
  n = long(na)*long(na)
  a = fltarr(na,na,na,na)
  for i = 0,na-1 do begin
    for j = 0,na-1 do begin
      for k = 0,na/2-1 do begin
        for l = 0,na/2-1 do begin
          a[i,j,k,l] = cos(2*!pi*i*k/float(na))*cos(2*!pi*j*l/float(na))
        endfor
        for l = na/2,na-1 do begin
          a[i,j,k,l] = cos(2*!pi*i*k/float(na))*sin(2*!pi*j*l/float(na))
        endfor
      endfor
      for k = na/2,na-1 do begin
        for l = 0,na/2-1 do begin
          a[i,j,k,l] = sin(2*!pi*i*k/float(na))*cos(2*!pi*j*l/float(na))
        endfor
        for l = na/2,na-1 do begin
          a[i,j,k,l] = sin(2*!pi*i*k/float(na))*sin(2*!pi*j*l/float(na))
        endfor
      endfor
    endfor
  endfor
  a = reform(a,na,na,n)
  return,a
end