;  gettitandata - read from a Sep 96 Titan dataset

; formerly called: gettitandata
pro darksubtract,dataset,titan,titanDark,titanmdark,aveDark,readdatafromdisk=rfd
  if n_params() eq 0 then begin
    print,"usage: gettitandata, dataset, titan,titanDark, titanmdark, titanmdarkb"
    return
  end
  print,'<darksubtract> -- gettitandata2.pro -- v2, 10/9/97'
  if keyword_set(rfd) then begin
    print,'reading raw data'
    titan = readfits(dataset.datadir + dataset.datafile)
    print,'reading dark file'
    titanDark = readfits(dataset.datadir + dataset.darkfile)
  endif
  n = (size(titan))(3)
  print,'averaging darks'
  aveDark = total(titanDark,3)/n
  print,'subtracting darks'
  titanmdark = make_array(256,256,n)
  for i=0,n-1 do begin
    titanmdark(*,*,i) = titan(*,*,i) - aveDark
    print,format='($,I0," ")',i
  endfor
  print,'done'
end

; formerly called: gettitansky
function makeskyframe,dataset,badpix,sky,skyDark,readdatafromdisk=rfd
  if n_params() eq 0 then begin
    print,"usage: skyframe = gettitansky(dataset,badpix,[sky,skyDark])"
    return,0
  end
  print,'<makeskyframe> -- gettitandata2.pro -- v2, 10/9/97'
  if keyword_set(rfd) then begin
    print,'reading sky data'
    sky = readfits(dataset.datadir + dataset.skyfile)
    skyDark = readfits(dataset.datadir + dataset.skydarkfile)
  end
  n = (size(sky))(3)
  skymdark = sky - skyDark
  print,'averaging frames'
  skyframe = total(skymdark,3)/n;  average over the frames
  print,'removing bad pixels and stripes'
  skyframe = skyframe*(1-badpix) + median(skyframe,5)*badpix
  br = total(skyframe,2)/256;   remove the vertical stripes
  brv = total(br)/256;
  bra = make_array(256,256)
  for i=0,255,1 do bra(*,i) = br
  skyframe = skyframe - bra + brv
  return, skyframe
end

