; Point-spread functions and encircled energy for various
; degrees of correction on a 30 meter telescope
;
; version 1: D. Gavel, 6/9/04
;
microns = 1.e-6
cm = 1.e-2
arcsec = !pi/(180*3600.)
mas = arcsec/1000.
;
n = 1024
Dtele = 30. ; meters, TMT
d = Dtele/20; 100 ; subaperture size
lambda = 1.0*microns
r00 = 20*cm
lambda0 = 0.5*microns
;
fc = 1./(2*d) ; Nyquist frequency of subaperture spacing
ndof = (!pi/4.)*(Dtele/d)^2 ; approximate number of degrees of freedom
r0 = r00*(lambda/lambda0)^(6./5.)
du = 4*Dtele/float(n)
ap = circle(n,n,n/2,n/2,(Dtele/2)/du,1.0)
pixelscale = lambda/(n*du)
df = 1./(n*du)
fx = (ones(n) ## dindgen(n)-n/2)*df
fy = transpose(fx)
f = sqrt(fx^2+fy^2)
Sphi = 0.023*r0^(-5./3.)*f^(-11./3.) ; spectrum of uncorrected Kolmogorov turbulence
Sphi[n/2,n/2] = 0.
Cf = double(abs(fx) lt fc)
Cf = Cf*transpose(Cf)  ;  AO correction filter
Spsi = Sphi*(1.d0-Cf) ; spectrum of corrected phase
Cpsi = real_part(ft(Spsi,/inverse))*df^2 ; ... and covariance
Dpsi = 2*(Cpsi[n/2,n/2] - Cpsi) ; ... and structure function of corrected phase
print,'percent error in the integration: ',abs(max(Cpsi)*(r0*fc)^(5./3.)/(4*.023*0.795386) - 1.0)
;  -- calculate tau --
ux = (ones(n) ## dindgen(n)-n/2)*du/Dtele
uy = transpose(ux)
u = sqrt(ux^2+uy^2)<1.0
tau0 = (2.0/!pi)*(acos(u)-u*sqrt(1.-u^2))
;
taua = exp(-0.5*Dpsi)
tau = tau0*taua
psf = real(ft(tau))
ee = encirc(psf,n/2,n/2)
wset,0
if (n_elements(startPlot) eq 0) then startPlot = 1
if (startPlot eq 1) then begin
  plot,ee[0,*]*pixelscale/mas,ee[1,*],xrange=[0,1000],yrange=[0,1],xtitle='Field Angle, mas',ytitle = 'Encircled Energy Fraction'
  startPlot = 0
endif else begin
  oplot,ee[0,*]*pixelscale/mas,ee[1,*]
endelse
end
