function dateString
  theMonths=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
  caldat,systime(1,/julian),imonth,iday,iyear,hour,minute,second
  month = string(imonth,format='(i2.2)')
  day = string(iday,format='(i2.2)')
  year = string(iyear mod 100,format='(i2.2)')
  hour = string(hour,format='(i2.2)')
  minute = string(minute,format='(i2.2)')
  return,month+day+year+'_'+hour+minute
end

