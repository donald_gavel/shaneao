;+
;  celtseg.pro - analyze performance of AO system
;           in correcting random celt segment aberrations
;-
; .compile dm2.pro
; 
;-------------------
;  generate the dm
;-------------------
nx = 256; fine grid
d = 10; actuator spacing
nax = nx/d + 1; number of actuators across
na = nax*nax
dmradius = 128
dm = circle(nx,nx,nx/2,nx/2,dmradius,1)
dmgeo = fltarr(na,2)
i = 0
for yi = 0,nax-1 do begin $
  for xi = 0,nax-1 do begin $
    x = xi*d - (nax-1)*float(d)/2. & $
    y = yi*d - (nax-1)*float(d)/2. & $
    if (dm[x+nx/2,y+nx/2] eq 1) then begin & $
      dmgeo(i,0) = x & $
      dmgeo(i,1) = y & $
      i = i+1 & $
    endif & $
  endfor & $
endfor
dmgeo = dmgeo[0:i-1,*]
;a = makebasis(dmgeo,d,sigma=1.4*d,basis=basis,ap=ap); 8% undershoot
;            less than 1% ripple inside 1 actuator spacing boundary
; we'll do this later, after defining the aperture with the segments...
;------------------------
;  generate celt segments
;------------------------
eps = 0
L = 10
L = 40
dseg = 2*L ; superscribing circle
dx = L*sqrt(3)
dy = L*(3./2.)
seg = hex(dseg+1,L-eps)
nsegx = 3
nsegy = 3 ; must be odd
nseg = nsegx*nsegy
seggeo = fltarr(nseg,2)
k = 0
yf = dy*(nsegy-1)/2.
y0 = -yf
iy = -(nsegy-1)/2
for y = y0,yf,dy do begin $
  xf = dx*(nsegx-1)/2. & $
  x0 = -xf & $
  if ((iy/2)*2 ne iy) then begin $
    xf = xf - dx/2. & $
    x0 = x0 + dx/2. & $
  endif & $
  for x = x0,xf,dx do begin $
    seggeo[k,0] = x & $
    seggeo[k,1] = y & $
    k = k+1 & $
  endfor & $
  iy = iy + 1 & $
endfor
seggeo = seggeo[0:k-1,*]
nseg = k
seed = 1
n = 2
m = 0  ; focus
;
;  get addresses of all points in the segment image that are in the segment  
sdex = where(seg eq 1)
phdex = coords(seg,sdex)
ndex = (size(phdex))[1]/2
phdex = reform(phdex,ndex,2) - ones(ndex) # [L,L] + ones(ndex) # [nx/2,nx/2]
;
;  generate random n,m zernike phase
theZern = real(zernike(dseg+1,dseg+1,dseg/2,n,m))
ph = fltarr(nx,nx)
ap = fltarr(nx,nx)
;
;  paste randomly aberrated segments into the big phase screen
for i=0,nseg-1 do begin $
  xi = fix(seggeo(i,0)) & $
  yi = fix(seggeo(i,1)) & $
  randomnumber = randomn(seed) & $
  ph[phdex[*,0]+xi+(phdex[*,1]+yi)*nx] = (seg*randomnumber*theZern)[sdex] & $
  ap[phdex[*,0]+xi+(phdex[*,1]+yi)*nx] = seg[sdex] & $
endfor
;-------------------------------
;  fit the dm to the aberrations
;-------------------------------
; generate the basis functions
;a = makebasis(dmgeo,d,sigma=1.4*d,basis=basis,ap=ap)
a = makebasis(dmgeo,d,sigma=1.4*d,basis=basis)
;   8% undershoot and less than 1% ripple inside 1 actuator spacing boundary
;
; invert a
aarg = a
eval = eigenql(aarg,eigenvectors=ev)
evali = 1/eval
evali[where(eval/eval[0] lt .1)] = 0
ainv = transpose(ev)##diagonal(evali)##ev
;
; calculate least squares solution
;act = dmfit(ph,basis,ainv)
act = dmfit(ph,dmgeo,basis,ainv)
;phfit = pushact(act,basis)
phfit = pushact(act,dmgeo,basis,ph)
phe = ph - phfit
rmserr = rms(phe,ap)
u = showdm(dmgeo,ap)

;-----------------------------------------------------------------
; Green's function approach
;
n = 256
a = makebasis(dmgeo,d,sigma=1.4*d,basis=basis)
ai = invert(a) ; for some reason the eigenvalue method fails
act1 = ai[*,274] ; the one nearest the center
grf = pushact(act1,dmgeo,basis,ph) ; this is the Green's function
grf = shift(grf,128-123,128-133) ; shift it into exact center
;
act2 = dmfit(ph,dmgeo,grf,/greens)
phfit2 = pushact(act2,dmgeo,basis,ph)
phe2 = ph - phfit2
rat = rms(phe2,ap)/rms(ph,ap)
print,L,rat
;
;-----------------------------------
; .compile celtseg
nx = 256
L = 15
n = 2
m = 0
p = 0
seed = 1
ph =celtseg(nx,L,n,m,p,seed,ap=ap)
rat = fitcelt(ph,dmgeo,basis,grf,ap)
print,'[  ',L,',    ',rat,' ], $'
;
la = [10.,12,13,14,15,16,18,20,22,25,28,30,35,40,45,50]
nl = (size(la))[1]
dat = fltarr(nl)
for i=0,nl-1 do begin $
  seed = 1 & $
  L = la[i] & $
  ph = celtseg(nx,L,n,m,p,seed,ap=ap) & $
  rat = fitcelt(ph,dmgeo,basis,grf,ap) & $
  print,L,rat & $
  dat[i] = rat & $
endfor
plot,la/d,dat,/xlog,/ylog,psym=1,charsize=1.5
oplot,la/d,dat,color=130
tickgrid,[1,10],[.1,1.],color=80
for i=0,15 do print,'[  ',la[i],',    ',dat[i],' ], $'

;  results: (d=10, focus (n=2,m=0,p=0) seed=1)
;    L      rms correction ratio
data = [ $
  [  10,          0.778154 ], $
  [  12,          0.752988 ], $
  [  13,          0.704588 ], $
  [  14,          0.658010 ], $
  [  15,          0.548352 ], $
  [  16,          0.521387 ], $
  [  18,          0.468882 ], $
  [  20,          0.425154 ], $
  [  22,          0.425546 ], $
  [  25,          0.417959 ], $
  [  28,          0.386901 ], $
  [  30,          0.355803 ], $
  [  35,          0.323779 ], $
  [  40,          0.317866 ], $
  [  45,          0.285788 ], $
  [  50,          0.299714 ] $
]
plot,data[0,*]/d,data[1,*],/xlog,/ylog,psym=1,charsize=1.5
oplot,data[0,*]/d,data[1,*],color=130
tickgrid,[1,10],[.1,1.],color=80

;  results: (d=10, focus (n=2,m=0,p=0) seed=5)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.749141 ], $
[        12.0000,         0.755327 ], $
[        13.0000,         0.710647 ], $
[        14.0000,         0.621360 ], $
[        15.0000,         0.513838 ], $
[        16.0000,         0.489230 ], $
[        18.0000,         0.477476 ], $
[        20.0000,         0.435825 ], $
[        22.0000,         0.415749 ], $
[        25.0000,         0.411564 ], $
[        28.0000,         0.364294 ], $
[        30.0000,         0.353555 ], $
[        35.0000,         0.318841 ], $
[        40.0000,         0.291147 ], $
[        45.0000,         0.276115 ], $
[        50.0000,         0.292970 ] $
]
oplot,data[0,*]/d,data[1,*],color=130

;  results: (d=10, astigmatism (n=2,m=2,p=0) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.922764 ], $
[        12.0000,         0.930647 ], $
[        13.0000,         0.907245 ], $
[        14.0000,         0.841685 ], $
[        15.0000,         0.791314 ], $
[        16.0000,         0.719827 ], $
[        18.0000,         0.626153 ], $
[        20.0000,         0.536675 ], $
[        22.0000,         0.506123 ], $
[        25.0000,         0.531495 ], $
[        28.0000,         0.502017 ], $
[        30.0000,         0.474468 ], $
[        35.0000,         0.407037 ], $
[        40.0000,         0.404126 ], $
[        45.0000,         0.356460 ], $
[        50.0000,         0.373158 ] $
]
oplot,data[0,*]/d,data[1,*],color=130

;  results: (d=10, 45 astigmatism (n=2,m=2, p=1) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.827266 ], $
[        12.0000,         0.623195 ], $
[        13.0000,         0.586832 ], $
[        14.0000,         0.600713 ], $
[        15.0000,         0.610757 ], $
[        16.0000,         0.615496 ], $
[        18.0000,         0.599556 ], $
[        20.0000,         0.585323 ], $
[        22.0000,         0.528100 ], $
[        25.0000,         0.505247 ], $
[        28.0000,         0.474269 ], $
[        30.0000,         0.467161 ], $
[        35.0000,         0.405291 ], $
[        40.0000,         0.414560 ], $
[        45.0000,         0.376905 ], $
[        50.0000,         0.374477 ] $
]
oplot,data[0,*]/d,data[1,*],color=130

;  results: (d=10, coma (n=3,m=1, p=0) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.816724 ], $
[        12.0000,         0.653049 ], $
[        13.0000,         0.605012 ], $
[        14.0000,         0.589647 ], $
[        15.0000,         0.493972 ], $
[        16.0000,         0.492293 ], $
[        18.0000,         0.492746 ], $
[        20.0000,         0.428715 ], $
[        22.0000,         0.442481 ], $
[        25.0000,         0.390942 ], $
[        28.0000,         0.327456 ], $
[        30.0000,         0.297529 ], $
[        35.0000,         0.288008 ], $
[        40.0000,         0.276468 ], $
[        45.0000,         0.234898 ], $
[        50.0000,         0.246059 ] $
]
oplot,data[0,*]/d,data[1,*],color=200

;  results: (d=10, coma (n=3,m=1, p=1) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.821500 ], $
[        12.0000,         0.684458 ], $
[        13.0000,         0.591698 ], $
[        14.0000,         0.568601 ], $
[        15.0000,         0.515617 ], $
[        16.0000,         0.535103 ], $
[        18.0000,         0.454019 ], $
[        20.0000,         0.452790 ], $
[        22.0000,         0.437535 ], $
[        25.0000,         0.371708 ], $
[        28.0000,         0.316744 ], $
[        30.0000,         0.301636 ], $
[        35.0000,         0.289330 ], $
[        40.0000,         0.247776 ], $
[        45.0000,         0.243887 ], $
[        50.0000,         0.246108 ] $
]
oplot,data[0,*]/d,data[1,*],color=200

;  results: (d=10, tricoma (n=3,m=3, p=0) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.967302 ], $
[        12.0000,         0.944902 ], $
[        13.0000,         0.941444 ], $
[        14.0000,         0.913135 ], $
[        15.0000,         0.859307 ], $
[        16.0000,         0.789820 ], $
[        18.0000,         0.743672 ], $
[        20.0000,         0.651441 ], $
[        22.0000,         0.577420 ], $
[        25.0000,         0.558743 ], $
[        28.0000,         0.527151 ], $
[        30.0000,         0.509658 ], $
[        35.0000,         0.452105 ], $
[        40.0000,         0.440395 ], $
[        45.0000,         0.382800 ], $
[        50.0000,         0.400796 ] $
]
oplot,data[0,*]/d,data[1,*],color=100

;  results: (d=10, spherical (n=4,m=0, p=0) seed=1)
;    L      rms correction ratio
data = [ $
[        10.0000,         0.967729 ], $
[        12.0000,         0.976709 ], $
[        13.0000,         0.971846 ], $
[        14.0000,         0.940497 ], $
[        15.0000,         0.896345 ], $
[        16.0000,         0.806651 ], $
[        18.0000,         0.709587 ], $
[        20.0000,         0.606633 ], $
[        22.0000,         0.477264 ], $
[        25.0000,         0.346901 ], $
[        28.0000,         0.330428 ], $
[        30.0000,         0.316338 ], $
[        35.0000,         0.275051 ], $
[        40.0000,         0.246383 ], $
[        45.0000,         0.228063 ], $
[        50.0000,         0.221848 ] $
]
oplot,data[0,*]/d,data[1,*],color=80

