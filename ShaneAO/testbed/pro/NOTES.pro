cm = .01
mm = .001
microns = 1.e-6
arcsec = !pi/(180.*3600.)
i = complex(0,1.)
n = 256
lambda = 2.2*microns
lambda0 = 0.55*microns

dx = .5*mm
x = (findgen(n)-n/2)*dx # (fltarr(n)+1)
y = transpose(x)
r2 = x^2+y^2
fn = 28.6*10
dap = 128*dx
ap = circle(n,n,n/2,n/2,dap/(2*dx),1.)
wf0 = ap
f = dap*fn
dfoc = fn^2*lambda ; depth of focus
rayrange = dap^2/lambda ; Rayleigh range
L = f - 30.*dfoc
A = 1 - L/f & B = L & C = -1/f & D = 1  ; lens and distance
; A = 1 & B = L & C = 0 & D = 1   ; case of f=infinity
; A = 1-L2/f & B = L1+L2-(L1*L2)/f & C = -1/f & D = 1-L1/f  ; L1-lens-L2
;  --- fft method: L,lambda,dx,n,wf0,A,B,C,D
du = B*lambda/(n*dx)
ph = 2*!pi*(A/(2*B*lambda))*r2
dph = (ph-shift(ph,1))*ap
print,max(abs(dph))  ;  check for phase wrapping (must be < pi)
phaseFactor = exp(-i*ph)
wfl = ft(ap*phaseFactor,/inverse)/float(n)
xl = (findgen(n)-n/2)*du # (fltarr(n)+1)
yl = transpose(xl)
rl2 = xl^2 + yl^2
phl = 2*!pi*(D/(2*B*lambda))*rl2
phaseFactorL = exp(-i*phl)
wfl = wfl*phaseFactorL*i
;  --- compare to convolution method (f=infinity => A=1, B=L)
wf1 = fresnel(wf0,dx,L,lambda)
wfl = fresnelft(wf0,dx,A,B,C,D,lambda,ap,/check)
;
; -- check convolution kernal calculation
k = 2*!pi/lambda
w = ft(exp(-i * (k/(2*L)) * r2)) ; explicitly take fft of kernal
dk = 2*!pi/(n*dx)
kx = (findgen(n)-n/2)*dk # (fltarr(n)+1)
ky = transpose(kx)
kperp2 = kx^2+ky^2
propPhase = kperp2*L/(2*k)
prop = exp(i*propPhase)*(-i) ; calculate kernal directly
dispgui,angle(w)-angle(prop) ; they're the same
;
;  test of fresnel propagation through a lens
;
object = fltarr(n,n)
object[n/2,n/2] = 1 ; point source, the longest it can be 
;                     propagated is L1 < n*dx^2/lambda
dx = .5*mm
dap = (n/4)*dx
ap = circle(n,n,n/2,n/2,dap/(2*dx),1.)
f = 14.
L1 = 2*f
L2 = 2*f
wf1 = fresnel(object,dx,L1,lambda)
ph = lens(n,dx,f,lambda,ap,/check) ; the shortest the focal length
;                                    can be is f > dap*dx/lambda
wf2 = wf1*exp(i*ph)
wf3 = fresnel(wf2,dx,L2,lambda)
;
;  constraints in terms of the sampling:
;    dx < f*lambda/dap  ; to sample a lens' wavefront phase at < pi/sample
;     n > L*lambda/dx^2 ; point source underfills the screen of regard
;     L > dx/(lambda/dap) ; point source overfills the aperture
;     n > 2*dap/dx  ; to over sample the PSF
;  solving #1 and #4 to eliminate dx:
;     n > dap^2/(f*lambda)
;
; e.g: dap = 100 mm, f = 3 m, lambda = 2 microns, L = 1 meter
;  dx < 60 microns
;  n > 555
;  n > 1667
;
;  boiling down the constraints even further
;   L*lambda/(2*dap) < dx < L*lambda/dap
;                  n > dap^2/(f*lambda)
;                  n > 2*dap/dx
; strategy: adjust "scale" (distance scaling) to meet constraints
scale = 10.
dap = 71.*mm
f = dap*28.6*scale
L = 2*f
; =>
n = 256
dx = .5*L*lambda/dap
;
print,n,' > ',L*lambda/dx^2
print,n,' > ',dap^2/(f*lambda)
print,n,' > ',2*dap/dx
print,L,' > ',dx/(lambda/dap)
;
ap = circle(n,n,n/2,n/2,dap/(2*dx),1.)
object = fltarr(n,n)
object[n/2,n/2] = 1.
wf1 = fresnel(object,dx,L,lambda)
ph = lens(n,dx,f,lambda,ap,/check)
wf2 = wf1*exp(i*ph)
wf3 = fresnel(wf2,dx,L,lambda)
;
; --- propagate thru Kolmogorov to check scintillation
;
dap = 30.
n = 512
dx = 2*dap/float(n)
r0 = 1.0
L = 4000.
lambda = 2.2*microns
f = filtergen(n,n,r0,dx)
s = screengen_f(f,seed)
wf0 = exp(i*s)