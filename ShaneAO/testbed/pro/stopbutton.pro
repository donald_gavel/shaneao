;+
;  stopButton.pro
;    a simple widget with a stop button
;-
pro stopButton_event, event
  wTarget = event.id
  wWidget = event.top
  case wTarget of
  
  widget_info(wWidget,find_by_uname='Stop'): begin
    print,'<stopButton_event> stop!'
    widget_control,wWidget,set_uvalue=1
  end
  
  else:
  
  endcase
end

function stopButton_pressed,id
  widget_control,id,get_uvalue=pressed
  return,pressed
end

function checkStopButton,id
  if (widget_info(id,/valid_id)) then begin
    event = widget_event(id,/nowait)
    if (event.id) then stopButton_event,event
    if (stopButton_pressed(id)) then begin
      print,'<stopButton:checkStopButton> stop button pressed'
      stopButton_reset,id
      return,1
    endif
    return,0
  endif
  return,0
end

pro stopButton_reset,id
  widget_control,id,set_uvalue=0
end

pro stopButton,id=base
  base = widget_base(column=1,uvalue=0)
  stopButton = widget_button(base,value='Stop',uname='Stop')
  
  widget_control, base, /realize
  xmanager, 'stopButton', base, /no_block
end

stopButton,id=id
print,id
end
