;
; VillagesTelemetryPipeline.pro
;
;  Objectives:
;    Automatically analyze telemetry data sets from the Villages real-time-control RTC system
;    1. Assess which data is available
;    2. Determine:
;       2.1 R0
;       2.2 wind speed, equivalently, tau0
;       2.3 measurement noise (dependent on guide star brightness)
;       2.4 tip/tilt variance
;       2.5 PSF (using a varient of the Veran algorithm)
;    3. Plot:
;       3.1 Spatial Power spectrum of controlled and uncontrolled turbulence (limited spatial frequencies from 1/D to 1/2d)
;           where D is the telescope diameter and d is the subaperture diameter
;       3.2 Temporal Power spectrum of controlled and uncontrolled turbulence (limited temporal frequencies from 1/Tburst to 1/2t_sample)
;           where Tburst is the period of time over which telemetry data is collected (usually 2048 data points at t_sample)
;       3.3 Movies of turbulence
;           3.3.1 Raw Atmosphere (as determined by the open-loop wavefront sensor)
;           3.3.2 Phase on DM (as is driving the DM)
;           3.3.3 The residual (as determined by the closed-loop wavefront sensor)
;       3.4 Plots of tip/tilt performance
;           3.4.1 Tip/tilt residuals, as seen in both the open-loop and closed-loop wavefront sensors, and dthe focal plane tip/tilt sensor
;           3.4.2 Tip/tilt drive to the tip/tilt mirror
;           3.4.3 Telescope offload signals
;           3.4.4 Power spectra of tip/tilt and tip/tilt residuals, and the rejection ratio
;
; --- explanation of file types ---
; rawImage[80,80x1000] = the raw CCD images taken in a 1000 frame burst at full frame rate
; cent[2x2x60,4096] = Hartmann centroids:
;    60 X closed loop WFS
;    60 X open loop WFS
;    60 Y closed loop WFS
;    60 Y open loop WFS
; inten[2x60,4096] = Hartmann spot intensities
;    60 closed loop WFS
;    60 open loop WFS
; disp[12x12,1024] = computed displacements (open-loop WFS)
;    The DM displacements are given on a 12x12 grid
; force[144,1024] = computed plate forces (open-loop WFS)
;    The DM plate forces are given on a 12x12 grid
; mirror[140,1024] = mirror voltages
;    There are 140 actively controlled actuators, all of the 12x12 grid minus the 4 corners
; tt[28,1024] = tip/tilt data
;  in all the above, the last dimension is time index. data are taken in a burst at full frame rate
;
;--------------------------
;  Need to do:
;    - process [closed loop controlled] data to plot phase power spectra and residuals
;      - total phase is reconstructed from the uncontroled WFS reading
;      - residual phase is reconstructed from the controlled WFS reading
;    - process [open loop controled] data to plot phase power spectra and residuals 
;      - total phase is reconstructed from the uncontroled WFS reading
;      - residual phase is reconstructed from the controlled WFS reading
;    - calculate r0 from model fit
;----------------------------

;  Plot switches
plot_hists = 0;1        ; histograms of actuator strokes
plot_phaseps = 1      ; corrected and uncorrected phase power spectra

ttplots = 1*[1,1]       ; time plots of x and y tilts, corrected and uncorrected
spectraPlots = 1*[1,1]  ; spectra of x and y tilts, corrected and uncorrected
rejectionPlot = 1       ; ratio of corrected to uncorrecte phase power spectra
rejectionTTPlot = 1     ; ratio of corrected to uncorrected tip and tilt power spectra
zoomPlot = 1          ; zoom-in to a short time splice of the tilt control signal and residual
yarnBallPlot = 1      ; plot of tip vs tilt in residual and mirror drive signal
residYarnBallPlot = 1 ; plot of residual tip vs tilt (zoomed in scale)

;----------------------------------------------------------
print,'setting directories:' & wait,.01
;baseDir = 'C:\Users\DonGavel\AdaptiveOptics\LAO\Villages\Data\'
;dir = baseDir+'Data_080716\'
;parmDir = baseDir+'Villages_Data\Parms\'
;
; set the base director and the particular data set here
;
;baseDir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/VilLaGEs (VisibleLightLGSAOExperiments)/Data/'
;baseDir = 'C:\Users\DonGavel\AdaptiveOptics\LAO\Villages\Data\'
;baseDir = '/home/user/observers/gavel/data/'
baseDir = '/home/user/villages/home/winkerbean/AO/Data/'
;date = '090401/'
;date = '091112/'
;date = '100514/'
date = '100722/'
;date = '100720\'
dir = baseDir + 'RunData/' + date
parmDir = baseDir + 'Parms/'
;fileNo = '031'
;fileNo = '014'
fileNo = '030' ; open loop controlled
;fileNo = '013' ; losed loop controlled
titlefix = ' '+date+fileNo
cd, dir
print,'---------------'+date+fileNo+'---------------------'
print,'           High Order Wavefront Analysis'

;-------------------------------------------------
; set physical constants
arcsec = !pi/(180.*3600.)
nm = 1.e-9
microns = 1.e-6
cm = 1.e-2
D_tele = 1.0 ; meters - Nickel Telescope aperture diameter
d = 11*cm ; Hartmann subaperture diameter

;-----------------------------------------------------------
print,'reading files from data set ',fileNo & wait,.01
;
r = rdview('rawImage_'+fileNo,err=err_raw)
cent = rdview('cent_'+fileNo,err=err_cent)
inten = rdview('inten_'+fileNo,err=err_inten)
disp = rdview('Disp_'+fileNo,err=err_disp)
force = rdview('Force_'+fileNo,err=err_force)
mirror = rdview('mirror_'+fileNo,err=err_mirror)
tt = rdview('tt_'+fileNo,err=err_tt)
; aperture
ap = circle(12,12,5.5,5.5,5.5,1)-circle(12,12,5.5,5.5,1.0,1)

;-----------------------------------------------------------
;  read the relevant parametric information from paramter files

print,'reading parametric files' & wait,.01
forward_function rdview_param, read_view_parameters
cm_file = rdview_param('cent_'+fileNo,'Open Loop C Matrix:')
;cm_file = 'cm144_000.sdt' ; ***TEMPORARY***
cmat = rdview(parmDir+cm_file)
phi_file = rdview_param('cent_'+fileNo,'Open Loop Phi Matrix:')
phimat = rdview(parmDir+phi_file)
dmShape_file = rdview_param('cent_'+fileNo,'Unpwrd DM Shape Data:')
dmShape = rdview(parmDir+dmShape_file)
dispGain = rdview_param('cent_'+fileNo,'Displacement Gain:')
dispGain = float(dispGain)
dispBias = rdview_param('cent_'+fileNo,'Displacement Bias:')
dispBias = float(dispBias)
fs = rdview_param('cent_'+fileNo,'Rate:')
fs = float(fs)
controllerState = rdview_param('cent_'+fileNo,'AO Loop:')

;-----------------------------------------------------------
ol_recon_gain = -2. ; use this instead of RTC's dispGain
print,'converting data format' & wait,.01

;-----------------------------------------------------------
; convert mirror data to a 12x12 display grid

nt = (size(mirror))[2]
mask = ones(12,12)
mask[0,0]= 0 & mask[0,11] = 0 & mask[11,0] = 0 & mask[11,11] = 0
ind = where(mask eq 1)
dm = fltarr(12*12,nt)
dm[ind,*] = mirror
dm = reform(dm,12,12,nt)
dm_ave = total(dm,3)/float(nt)
;for k = 0,nt-1 do dm[*,*,k] -= dm_ave
; 
if (~err_disp) then disp = reform(disp,12,12,nt)
if (~err_force) then force = reform(force,12,12,nt)

;-----------------------------------------------------------
; convert the centroid data to 4 11x11 display grids: x closed, x open, y closed, y open

platescale = 2.0 ; arcesconds per pixel (on WFS)
wfs_ap = circle(11,11,5.,5.,5,1)-circle(11,11,5.,5.,1.5,1)
illum = where(wfs_ap eq 1)
ns = 60 ; illuminated subapertures recorded in dataset
nt = (size(cent))[2]
cent2 = fltarr(11*11,nt,4)
cent2[illum,*,0] = cent[0:ns-1,*]    ; x closed
cent2[illum,*,1] = cent[ns:2*ns-1,*] ; x open
cent2[illum,*,2] = cent[2*ns:3*ns-1,*] ; y closed
cent2[illum,*,3] = cent[3*ns:4*ns-1,*] ; y open
cent2 = reform(cent2,11,11,nt,4)

;-----------------------------------------------------------
; do statistical analysis of Hartmann tilts
ave_cent2 = total(cent2,3)/float(nt) ; means
rms_ave_cent = fltarr(4)
for k=0,3 do rms_ave_cent[k] = rms(ave_cent2[*,*,k],wfs_ap)
print,'Time-averaged Hartmann tilts (rms over aperture):',rms_ave_cent[0],rms_ave_cent[1],rms_ave_cent[2],rms_ave_cent[3],' pixels'
cent2d = cent2
for k=0,nt-1 do cent2d[*,*,k,*] -= ave_cent2
rms_tilt = sqrt(total(cent2d^2,3)/float(nt))
rms_tilt_sum = fltarr(4)
for k=0,3 do rms_tilt_sum[k] = rms(rms_tilt[*,*,k],wfs_ap) ; standard deviations
pv_tilt_sum = fltarr(4)
for k=0,3 do pv_tilt_sum[k] = max(cent2d[*,*,*,k])-min(cent2d[*,*,*,k]) ; peak to valley deviations from means
nfs = fltarr(4)
thresh = 1.5 ; pixels - value representative of maximum Hartmann tilt expected
for k=0,3 do begin
  for fr = 0,nt-1 do begin
    if (max(abs(cent2d[*,*,fr,k])) gt thresh) then nfs[k] += 1
  endfor
endfor
print,'Hartmann tilts rms: ',rms_tilt_sum[0],rms_tilt_sum[1],rms_tilt_sum[2],rms_tilt_sum[3],' pixels rms'
print,'Hartmann tilts saturated: ',nfs[0]/float(nt),nfs[1]/float(nt),nfs[2]/float(nt),nfs[3]/float(nt),'..(fraction of frames with saturated (>',thresh,' pixels) Hartmann centroids)'

;-----------------------------------------------------------
; create histograms of Hartmann tilts
nbins = 50
u = (reform(cent2d,11*11,nt,4))[illum,*,*]
hist_tilt = fltarr(nbins,4)
xaxis = fltarr(nbins,4)
for k = 0,3 do begin $
  hist_tilt[*,k] = histogram(u[*,*,k],nbins=nbins,omin=omin,omax=omax) & $
  hist_tilt[*,k] /= max(hist_tilt[*,k]) & $
  xaxis[*,k] = findgen(nbins)*((omax-omin)/float(nbins))+omin & $
endfor
if (plot_hists) then begin
  window,/free,title = 'Histogram of Hartmann Tilts'+date+fileNo
  hist_window = !d.window
  gfitx = exp(-0.5*(xaxis[*,1]/rms_tilt_sum[1])^2)
  gfitx = gfitx/max(gfitx)
  gfity = exp(-0.5*(xaxis[*,3]/rms_tilt_sum[3])^2)
  gfity = gfitx/max(gfitx)
  plot,xaxis[*,1]*platescale,hist_tilt[*,1],charsize=2,xtitle='tilt, arcsec',ytitle='relative frequency'
  oplot,xaxis[*,3]*platescale,hist_tilt[*,3]
  oplot,xaxis[*,1]*platescale,gfitx, linestyle=1
  oplot,xaxis[*,3]*platescale,gfity,linestyle=1
endif

;-----------------------------------------------------------
; make a centroid movie (just x and y open-loop)
cent_movie = fltarr(2*11,11,nt)
for k=0,nt-1 do cent_movie[0:10,*,k] = cent2d[*,*,k,1]
for k=0,nt-1 do cent_movie[11:21,*,k] = cent2d[*,*,k,3]

;-----------------------------------------------------------
; convert the intensity data to an 11x11 display grid

sa1_cl = 0
sa2_cl = 59

sa1_ol = 60
sa2_ol = 119

wfs_ap = circle(11,11,5.,5.,5,1)-circle(11,11,5.,5.,1.5,1)
illum = where(wfs_ap eq 1)
nt = (size(inten))[2]

inten2_cl = fltarr(11*11,nt)
inten2_cl[illum,*] = inten[sa1_cl:sa2_cl,*] ; get the first (closed-loop) WFS pattern
inten2_cl = reform(inten2_cl,11,11,nt) ; this is the movie of subap intensities

inten2_ol = fltarr(11*11,nt)
inten2_ol[illum,*] = inten[sa1_ol:sa2_ol,*] ; get the second (open-loop) WFS pattern
inten2_ol = reform(inten2_ol,11,11,nt) ; this is the movie of subap intensities

; measure the scintillation
spotSize = 1.*arcsec
d = 0.11 ; diameter of Hartmann subaperture

av_inten_cl = average(inten[sa1_cl:sa2_cl,*])
sd_inten_cl = stddev(inten[sa1_cl:sa2_cl,*])
SNR_cl = sqrt(av_inten_cl)
scintillation_cl = (sd_inten_cl/av_inten_cl)*100
print,'Corrected Hartmann subap intensity scintillation is ',scintillation_cl,' %'
print,'Corrected Hartmann subap SNR is ',SNR_cl ; assumes the intensity is in photons
print,'This SNR would produce ~',(spotSize/SNR_cl)*d/nm,' nm rms wavefront error'

av_inten_ol = average(inten[sa1_ol:sa2_ol,*])
sd_inten_ol = stddev(inten[sa1_ol:sa2_ol,*])
SNR_ol = sqrt(av_inten_ol)
scintillation_ol = (sd_inten_ol/av_inten_ol)*100
print,'Uncorrected Hartmann subap intensity scintillation is ',scintillation_ol,' %'
print,'Uncorrected Hartmann subap SNR is ',SNR_ol ; assumes the intensity is in photons
print,'This SNR would produce ~',(spotSize/SNR_ol)*d/nm,' nm rms wavefront error'

;-----------------------------------------------------------
; calculate the absolute phase given the centroids from the Uncorrected WFS

print,'calculating uncorrected displacements' & wait,.01
ntcent = (size(cent))[2]
OLcent = fltarr(120,ntcent)
nc = 60 ; number of Hartmann spots
OLcent[0*nc:0*nc+nc-1,*] = cent[1*nc:1*nc+nc-1,*]
OLcent[1*nc:1*nc+nc-1,*] = cent[3*nc:3*nc+nc-1,*]
; remove average x and y slopes before reconstructing 
remove_slopes = 1
if (remove_slopes) then begin
  for k=0,nt-1 do begin
    OLc_ave_x = total(OLcent[0*nc:0*nc+nc-1,k])/float(nc)
    OLc_ave_y = total(OLcent[1*nc:1*nc+nc-1,k])/float(nc)
    OLcent[0*nc:0*nc+nc-1,k] -= OLc_ave_x
    OLcent[1*nc:1*nc+nc-1,k] -= OLc_ave_y
  endfor
endif
;
disp_c = olcent ## transpose(cmat)
disp_c = reform(disp_c,12,12,ntcent) ; disp_c is the reconstructed absolute wavefront
disp_d = disp_c ; disp_d is to be compared to the diagnostics disp
for k=0,ntcent-1 do disp_c[*,*,k] = disp_c[*,*,k]*ol_recon_gain; dispGain
for k=0,ntcent-1 do disp_d[*,*,k] = disp_d[*,*,k]*ol_recon_gain - dmShape
piston = fltarr(ntcent)
for k=0,ntcent-1 do piston[k] = total(disp_d[*,*,k])/float(12*12)
for k=0,ntcent-1 do disp_d[*,*,k] -= piston[k] - dispBias
;
;  ->-> Bug in diagnostic data taking:
;   It appears that the recorded disp data has the dispBias -subtracted- rather than added.
;   (resulting in all positive recorded displacements, rather than negative as they should be)
;   It is also recorded one step behind the disp data computed from the cent data at this step.
;   To reconcile, the formula is:
    diff = (disp+2*dispBias)[*,*,1:500] - disp_d[*,*,0:499]
;
;   I found out this is because of offsets related to the subsequent look-up process
    DISP_MIN = -3000.0
    DISP_MAX = 0.0
    DISP_MID = -1500.0
    DISP_RANGE = DISP_MAX - DISP_MIN
    D1 = DISP_MID - ( DISP_RANGE / 2.0 )
;
print,'calculating forces' & wait,.01
force_c = reform(disp_d,144,ntcent) ## phimat
force_c = reform(force_c,12,12,ntcent)
;   and then the data is offset into the lookup table, which is then what actually gets recorded
    disp_d -= D1

;-----------------------------------------------------------
; calculate the residual phase given the centroids from the Corrected WFS

print,'calculating residual displacements' & wait,.01
ntcent = (size(cent))[2]
CLcent = fltarr(120,ntcent)
nc = 60 ; number of Hartmann spots
CLcent[0*nc:0*nc+nc-1,*] = cent[0*nc:0*nc+nc-1,*]
CLcent[1*nc:1*nc+nc-1,*] = cent[2*nc:2*nc+nc-1,*]
; remove average x and y slopes before reconstructing 
if (remove_slopes) then begin
  for k=0,nt-1 do begin
    CLc_ave_x = total(CLcent[0*nc:0*nc+nc-1,k])/float(nc)
    CLc_ave_y = total(CLcent[1*nc:1*nc+nc-1,k])/float(nc)
    CLcent[0*nc:0*nc+nc-1,k] -= CLc_ave_x
    CLcent[1*nc:1*nc+nc-1,k] -= CLc_ave_y
  endfor
endif
;
disp_r = CLcent ## transpose(cmat)
disp_r = reform(disp_r,12,12,ntcent)
for k=0,ntcent-1 do disp_r[*,*,k] = disp_r[*,*,k]*ol_recon_gain; dispGain
piston = fltarr(ntcent)
for k=0,ntcent-1 do piston[k] = total(disp_r[*,*,k])/float(12*12)
for k=0,ntcent-1 do disp_r[*,*,k] -= piston[k]
;================ compute the residual rms =============
u = disp_r
ntdisp = (size(disp_r))[3]
for k = 0,ntdisp-1 do u[*,*,k] = ap*depiston(u[*,*,k],ap) ; this is in nm
var_ap = fltarr(12,12)
for k = 0,ntdisp-1 do begin
  var_ap += u[*,*,k]^2/float(ntcent)
endfor
sigma2 = total(ap*var_ap)/total(ap)
print,'Controller state = ',controllerState
print,'rms residual wavefront = ',sqrt(sigma2),' nm'
;======================================================
;-----------------------------------------------------------
; do statistical analysis of displacement (wavefront phase) data

;-----------------------------------------------------------
;  calculate r0 given displacements
;    using the formula: sigma^2 = 0.1( D/r0 )^(5/3) (piston, tip/tilt removed)

print,'calculating rms wfe and r0'
if (controllerState eq 'Controlled by closed loop') then begin
  dmar = dm
  nst = (size(dmar))[3]
  for k = 0,nst-1 do dmar[*,*,k] -= dm_ave
  u = dmar*15 ; roughly 15 nm / volt at mid range of DM
endif else begin
  u = disp_c
endelse
ntdisp = (size(u))[3]

for k = 0,ntdisp-1 do u[*,*,k] = ap*depiston(u[*,*,k],ap) ; this is in nm
var_ap = fltarr(12,12)
for k = 0,ntdisp-1 do begin
  var_ap += u[*,*,k]^2/float(ntcent)
endfor
sigma2 = total(ap*var_ap)/total(ap)
print,'Controller state = ',controllerState
print,'rms uncorrected wavefront = ',sqrt(sigma2),' nm'
lambda0 = 500. ; nm
D_tele = 1.0
sigma2 *= ((2*!pi)/lambda0)^2
r0 = D_tele*(sigma2/0.1)^(-3./5.)
print,'r0 = ',r0/cm,' cm'

;-----------------------------------------------------------
; calculate the uncorrected phase power spectra

u = disp_c
ntdisp = (size(u))[3]
n = ntdisp
dt = 1./fs
df = fs/float(n)
f = findgen(n/2)*df
ind_set = where(erode(ap,ones(3,3)) eq 1)
nind = (size(ind_set))[1]
u = reform(u,12*12,ntdisp)
for k=0,nind-1 do begin
  ps = abs(fft(reform(u[ind_set[k],*])))^2/df
  if (k eq 0) then ps_ave = ps
  if (k gt 0) then ps_ave = ps_ave + ps
endfor
ps_ave /= float(nind)
ps = ps_ave
psUnits = 'nm^2 / hz'

;-----------------------------------------------------------
; calculate the corrected (residual) phase power spectra
;
u = disp_r
ntdisp = (size(u))[3]
n = ntdisp
dt = 1./fs
df = fs/float(n)
f = findgen(n/2)*df
ind_set = where(erode(ap,ones(3,3)) eq 1)
nind = (size(ind_set))[1]
u = reform(u,12*12,ntdisp)
for k=0,nind-1 do begin
  psr = abs(fft(reform(u[ind_set[k],*])))^2/df
  if (k eq 0) then psr_ave = psr
  if (k gt 0) then psr_ave = psr_ave + psr
endfor
psr_ave /= float(nind)
psr = psr_ave
psUnits = 'nm^2 / hz'
noisefloor_r = average(psr[n/2-100:n/2-1])
print,'noise floor (measurement noise), corrected WFS = ',sqrt(noisefloor_r*n*df),' nm, rms'

;-----------------------------------------------------------
;   fit a f^(-8/3) power law to the uncorrected power spectrum curve

n1 = 10./df ; these are the frequencies over which to do the fit to a -8/3 power law
n2 = 20./df
logc = average( alog10( ps[n1:n2]) + (8./3.)*alog10(f[n1:n2]))
c = 10.^logc
powerLawModel = c*f^(-8./3.)
; note: c = (lambda0/(2*!pi))^2*0.077*(v/r0)^(5/3)
windEst = ((2*!pi/lambda0)^2*(c/0.077))^(3./5.)*r0
print,'wind estimate (from Kolmogorov temporal spectrum fit) = ',windEst,' m/s'
;   fit a noise floor
noisefloor = average(ps[n/2-100:n/2-1])
print,'noise floor (measurement noise), uncorrected WFS = ',sqrt(noisefloor*n*df),' nm, rms'
normGain = noiseFloor / noiseFloor_r
;-----------------------------------------------------------
;   estimate the break frequency -> wind velocity

ratio = smooth(ps/powerLawModel,15)
k = 0
while (ratio[k] lt .5) do k = k+1
fb = f[k]
v = fb*D_tele
print,'wind velocity (m/sec) from break frequency seen at ',fb,' Hz = ',v

;-----------------------------------------------------------
;   read or build a model of the piston & tip/tilt removed spectrum

sf = rdview(parmDir+'sf',err=err_sf)
dk = 0.01
n = 4096
if (~err_sf) then begin
  params = read_view_parameters(parmDir+'sf')
  dk = params.dx[0]
  n = params.dim[0]
endif
kx = findgen(n)*dk
kx[0] = dk/100.
if (n_elements(sf) eq 0) then begin
  print,'generating generic Kolmogorov temporal power spectrum...'
  print,'(Only done on this first call)'
  k = sqrt(kx^2 # ones(n) + ones(n) # kx^2)
  s0 = 0.023*k^(-11./3.)
  print,'...piston filter...'
  hp = (2*beselj(!pi*k,1)/(!pi*k))^2
  print,'...tilt filter...'
  ht = 16.*(beselj(!pi*k,2)/(!pi*k))^2
  s = s0*(1.-hp-ht)
  s[0,0] = 0
  sf = (4*total(s,1) - s[*,0])*dk
  sig2 = total(sf)*dk
  print,'done.  Generic spectrum will be retained for subsequent calls'
  print,'This number should be 0.135: ',sig2 ; 0.135
endif
;  convert model from radians^2/m^-1 to nm^2/hz
model = (lambda0/(2*!pi))^2*r0^(-5./3.)*sf*dk/df
fmodel = kx*v/D_tele

;-------------------------------------------------------------
; optionally plot the power spectrum and asymptotic fits
forward_function powerSpectrumPlot
if (plot_phaseps) then begin

  window,/free,title = 'Phase Power Spectrum '+date+fileNo
  phasepsPlot_window = !d.window
  powerSpectrumPlot, ps,fs,yrange=[1e-2,1e4],ytitle=psUnits
  psrn = psr*(noiseFloor/noiseFloor_r)
  powerSpectrumPlot, psrn,fs,/oplot,color=150
  oplot,f,2*powerLawModel,color=150
  oplot,f,2*ones(n/2)*noisefloor,color=150
  print,'open loop phase power: ',sqrt(total(ps-2*ones(n/2)*noisefloor)*df),' nm rms'
  print,'residual phase power: ',sqrt(total(psr*(noiseFloor/noiseFloor_r)-2*ones(n/2)*noisefloor)*df),' nm rms'
  oPlot,fmodel[1:n-1],model[1:n-1],color=120
  if (rejectionPlot) then begin
    window,/free,title='Phase Rejection Function ' +titlefix
    powerSpectrumPlot,0.5*psrn/ps,fs,/xlog,/ylot,ytitle='Power Ratio',xtitle='frequency, hz',title='Phase Rejection'+titlefix
    oplot,[df/10,fs],[1,1]
  endif
endif

;--------------------------------------------------------------
; analyze tip/tilt data
print,'---------------'+date+fileNo+'---------------------'
print,'              Tip/Tilt Analysis'

dset = fileNo
titlefix = date + '/' + dset

fs = rdview_param('tt_'+fileNo,'Rate:')
fs = float(fs)
ttControllerState = rdview_param('tt_'+fileNo,'TT Loop:')

n = (size(tt))[2]
fc = fs/10. ; roughly
dt = 1./fs
df = fs/float(n)
t = findgen(n)*dt
f = findgen(n)*df
pixelsPerVolt = 4.0 ; 5.0
arcsecPerPixel = 2.0

if (ttControllerState eq 'Uncontrolled') then begin
  tiltx = tt[0,*]*0
  tilty = tt[1,*]*0
  print,'WARNING: Tip/Tilt was uncontrolled .. (tt data is BOGUS!!)'
endif
if (ttControllerState eq 'Controlled by closed loop WFS') then begin
  tiltx = tt[0,*]*arcsecPerPixel
  tilty = tt[1,*]*arcsecPerPixel
endif
if (ttControllerState eq 'Controlled by open loop WFS') then begin
  tiltx = tt[2,*]*arcsecPerPixel
  tilty = tt[3,*]*arcsecPerPixel
endif

print,'rms residual tip/tilts: x:',rms(tiltx),' y:',rms(tilty),' arcsec'
tiltxc = (tt[4,*]-average(tt[4,*]))*pixelsPerVolt*arcsecPerPixel
tiltyc = (tt[5,*]-average(tt[5,*]))*pixelsPerVolt*arcsecPerPixel
print,'rms controled tip/tilts: x:',rms(tiltxc),' y:',rms(tiltyc),' arcsec'
print,'rms 2-axis tip/tilt from '+ttControllerState+' ',sqrt(rms(tiltx)^2+rms(tilty)^2),' arcsec'

if (ttplots[0]) then begin
  window,/free,title='x tip/tilt '+titlefix
  plot,t,tiltxc,xtitle='time, sec',ytitle='arcsec',title='x tip/tilt '+titlefix
  oplot,t,tiltx,color=150
endif
if (ttplots[1]) then begin
  if (!d.name eq 'X') then window,/free,title='y tip/tilt '+titlefix
  plot,t,tiltyc,xtitle='time, sec',ytitle='arcsec',title='y tip/tilt '+titlefix
  oplot,t,tilty,color=150
endif

fx = reform(abs(fft(tiltx)))*n*dt
fy = reform(abs(fft(tilty)))*n*dt
fxc = reform(abs(fft(tiltxc)))*n*dt
fyc = reform(abs(fft(tiltyc)))*n*dt
fxu = reform(abs(fft(tiltx+tiltxc)))*n*dt
fyu = reform(abs(fft(tilty+tiltyc)))*n*dt
tfx = fx/fxu
tfy = fy/fyu
tfm = sqrt(f^2/(fc^2+f^2)) ; model rejection spectrum

; do a power-law fit to the tilt spectra
n1=1
n2=fix(40./df) ; from df to 40 Hz
nfit = n2-n1+1
w = ones(nfit)
fp = alog10(f[n1:n2])
; x axis
sp = alog10(fxc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
abx = invert(aa) ## ay
sbarx = 10^(abx[0])*f^(-abx[1])
sbarx[0] = 0.
; y axis
sp = alog10(fyc[n1:n2])
aa00 = total(w^2)+total(w^2*fp^2)
aa10 = -total(w^2*fp)-total(w^2*fp^3)
aa11 = total(w^2*fp^2)+total(w^2*fp^4)
aa = [[aa00,aa10],[aa10,aa11]]
ay0 = total(w^2*sp)+total(w^2*fp^2*sp)
ay1 = -total(w^2*sp*fp)-total(w^2*fp^3*sp)
ay = [ay0,ay1]
aby = invert(aa) ## ay
sbary = 10^(aby[0])*f^(-aby[1])
sbary[0] = 0.

;----------------------------------------------------------------------------------
; draw the tip/tilt plots

if (spectraPlots[0]) then begin
  window,/free,title='x tilt spectra '+titlefix
  powerSpectrumPlot,fxc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='x tilt spectra '+titlefix
  powerSpectrumPlot,fx,fs,/oplot,color=150
  powerSpectrumPlot,sbarx,fs,/oplot,color=100
  powerSpectrumPlot,sbarx*tfm,fs,/oplot,color=120
endif
if (spectraPlots[1]) then begin
  window,/free,title='y tilt spectra '+titlefix
  powerSpectrumPlot,fyc,fs,/xlog,/ylog,ytitle='arcsec/hz',xtitle='frequency, hz',title='y tilt spectra '+titlefix
  powerSpectrumPlot,fy,fs,/oplot,color=150
  powerSpectrumPlot,sbary,fs,/oplot,color=100
  powerSpectrumPlot,sbary*tfm,fs,/oplot,color=120
endif
if (rejectionTTPlot) then begin
  window,/free,title='tt Transfer Functions ' +titlefix
  powerSpectrumPlot,0.5*tfx,fs,/xlog,/ylot,ytitle='transfer function',xtitle='frequency, hz',title='Transfer Functions '+titlefix
  powerSpectrumPlot,0.5*tfy,fs,/oplot,color=150
  powerSpectrumPlot,0.5*tfm,fs,/oplot,color=100
endif

if (zoomPlot) then begin
  window,/free,title='Zoom of x tilt residuals '+titlefix
  plot,t[100:200],tiltx[100:200],ytitle='arcsec',xtitle='seconds',title='zoom in of x tilt residual '+titlefix
  oplot,t[100:200],tiltx[100:200],color=150
  oplot,t[100:200],tiltxc[100:200]-average(tiltxc[100:200])
endif

if (yarnBallPlot) then begin
  window,/free,title='tip/tilt yarn ball '+titlefix,xsize=1100,ysize=1000
  plot,tiltxc,tiltyc,title = 'tip/tilt yarn ball '+titlefix,xrange = [-1.,1.],yrange=[-1.,1.]
  oplot,tiltx,tilty,color=150
endif
if (residYarnBallPlot) then begin
  window,/free,title='tt resid yarn ball '+titlefix,xsize=1100,ysize=1000
  plot,tiltx,tilty,title = 'tt resid yarn ball '+titlefix,xrange=[-.1,.1],yrange=[-.1,.1],/nodata
  oplot,tiltx,tilty,color=150
endif
;
; print statixticsl analysis
print,'total tilt in atmospheric model (x,y):', sqrt(total((sbarx)^2))*df,sqrt(total((sbary)^2))*df,' arcsec'
print,'total tilt in control data (x,y):', sqrt(total(fxc^2))*df,sqrt(total(fyc^2))*df,' arcsec'
print,'total tilt in residual model (x,y):', sqrt(total((sbarx*tfm)^2))*df,sqrt(total((sbary*tfm)^2))*df,' arcsec'
print,'total tilt in residual data (x,y):', sqrt(total(fx^2))*df,sqrt(total(fy^2))*df,' arcsec'

end
