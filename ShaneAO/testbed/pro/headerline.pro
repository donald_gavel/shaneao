;+
;  headerline.pro - write a FITS header line
;    hdrlin = headerline(keywork,value,comment)
;-
function headerline, keyword, value, comment
  type = (size(value))(1)
  line = string(" ",format='(A80)')
  strput,line,keyword
  strput,line,"=",8
  strput,line,"/",31
  strput,line,comment,33
  case type of
  2: begin         ; integer
       strput,line,string(value,format='(I21)'),9
     end
  3: begin         ; long
       strput,line,string(value,format='(I21)'),9
     end
  4: begin         ; float
       strput,line,string(value,format='(E21.12)'),9
     end
  5: begin         ; double
       strput,line,string(value,format='(E21.12)'),9
     end
  7: begin         ; string
       strput,line,string(value,format='(A)'),10
     end
  endcase
  return,line
end