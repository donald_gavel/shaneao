pro bodePlot,f,h,h2,name=name
  if (n_elements(name) eq 0) then name = 'Bode Plot'
  psave = !p.multi
  !p.multi = [0,0,2,0,0]
  n = (size(f))[1]
  fp = f[n/2:n-1]
  hp = h[n/2:n-1]
  plot,fp,abs(hp),/xlog,/ylog,xtitle='frequency, Hz',ytitle='Log Magnitude',title=name,ticklen=.5
  if (n_elements(h2) ne 0) then begin
    hp2 = h2[n/2:n-1]
    oplot,fp,abs(hp2),color=150
  endif
  plot,fp,(180./!pi)*unwrap1(arg(hp)),/xlog,xtitle='frequency, Hz',ytitle='Phase, degrees',ticklen=.5
  if (n_elements(h2) ne 0) then begin
    oplot,fp,(180./!pi)*unwrap1(arg(hp2)),color=150
  endif
  oplot,fp,0*fp-180.,linestyle=2
  !p.multi = psave
end
