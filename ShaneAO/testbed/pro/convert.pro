; readtiff.pro - convert the tiff files to pnm format for reading into idl
;
pro read_tiff, file, data
  command = 'tifftopnm ' + file + ' > tmp.pnm'
  spawn,command
  read_ppm, 'tmp.pnm', data
end
