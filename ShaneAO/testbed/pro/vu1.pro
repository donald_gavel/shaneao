; vu1.pro - display vu-graph #1
;     3 single speckle frames, average speckle frame, average power spectrum
;

dir = '/duck1/gavel/keck/sep96/io/'
io231f=mrdfits(dir+'io231/io231_prep.fits',0)

disp,reverse(rebin(io231f(128-63:128+64,128-63:128+64,1),256,256))
disp_txt, "io231 1","",10

disp,reverse(rebin(io231f(128-63:128+64,128-63:128+64,3),256,256))
disp_txt, "io231 3","",10

disp,reverse(rebin(io231f(128-63:128+64,128-63:128+64,50),256,256))
disp_txt, "io231 50","",10

av = total(io231f,3)/100
disp,reverse(rebin(av(128-63:128+64,128-63:128+64),256,256))
disp_txt, "io231 average","",10

; ps = rdview(dir+'io231/io231_ps.sdt',129,256)
; ps2 = [ps,(reverse(reverse(ps,2)))(1:127,*)]
; writefits,dir+'io231/io231_ps.fits',ps2

ps = mrdfits(dir+'io231/io231_ps.fits',0)
disp,alog10(ps>1e5)
disp_txt, "io231 average power spectrum","",10

;   -- mtf outline --
 mtf = rdview(dir+'io242/keckmtfKp.sdt',256,256)
 mtfo = (mtf gt 10) and (mtf lt 20)
 t=3*mtfo+alog10(ps>1e5)
 disp,t
