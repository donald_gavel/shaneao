function fixps,ps
;bruce's version of Don's pencil filter; designed to remove the
;(clearly artifact) stripes in the power spectra

outps=ps
;first try: calculate the average value in the stripe
;range and subtract it - assumes power spectrum doesn't go all the way
;out to the edges


bgav=(avg(outps(5:20,132:135))+avg(outps(5:20,121:124)))/2
for row=126,130 do begin
   rowav=avg(outps(5:20,row))-bgav
   outps(*,row)=outps(*,row)-rowav
endfor

bgav=(avg(outps(122:124,25:35))+avg(outps(122:124,220:230)))/2
print,bgav
for col=125,128 do begin
   colav=((median(outps(col,220:230))+median(outps(col,25:35)))/2)-bgav
   print,colav
   outps(col,*)=outps(col,*)-colav
endfor

return,outps
end