;
;   remove vertical stripe patterns
;   bmac version - uses median, rejects anything within
;   rejbox pixels of brightest pixel (sort-of - actually
;   rejects anything in the rows within rejbox of the row 
;   containing brightest pixel
pro devstripem,datain,dataout,yspan0,rejbox
  if n_params() eq 0 then begin
    print,"usage: devmstripe,datain,dataout,[yspan],[rejbox]"
    return
  end
  if n_elements(yspan0) eq 0 then yspan0 = [70,255]
  if n_elements(rejbox) eq 0 then rejbox=20.0
  na = yspan0(1) - yspan0(0) + 1
  nx = (size(datain))(1)
  ny = (size(datain))(2)
  n = (size(datain))(3)
  mask = fltarr(nx,ny)
  mask (6:nx-6,6:ny-6)=1.0
  mask = 1.0 - mask
  print,"devstripe..."
;  index=findgen(yspan(1)-yspan(0)+1)+yspan(0)
  if n_elements(dataout) eq 0 then dataout = make_array(nx,ny,n)
  bra = make_array(nx,ny)
  for i=0,(n-1) do begin
    yspan=yspan0
    print,format='($,I0," ")', i
    peak=brightest(median(datain(*,*,i)-mask*1000.0,3)) 
; find peak pixel w. median filter
;    print,format='(2x,I3,1X,I3)',peak
    halfspan=(yspan(0)+yspan(1))/2
    if (peak(1) ge halfspan) then yspan(1)=min([peak(1)-rejbox,yspan(1)])
    if (peak(1) lt halfspan) then yspan(0)=max([peak(1)+rejbox,yspan(0)])
;    print,format='(2x,I3,1X,I3)',yspan
;    mask=where(abs(index - peak(1) ge rejrad))

    br=fltarr(nx)
    for h=0,(nx-1) do begin
     slice=datain(h,yspan(0):yspan(1),i)
;     if (abs(h - peak(0)) le rejrad) then slice=slice(mask)
;    print,format='($,I0," ")', i
;    br = total(datain(0:(nx-1),yspan(0):yspan(1),i),2)/na  ;  vertical stripes
     br(h)=median(slice)
    endfor
    for j=0,(ny-1),1 do bra(*,j) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end
