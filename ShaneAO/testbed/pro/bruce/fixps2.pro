function fixvert,image,col,row0,row1,buffer
;function to fix along a vertical stripe running from row0 to row1
;by linear-fitting the pixels in the buffer
outim=image
slice=outim(col,row0-buffer:row1+buffer)
slicex=findgen(n_elements(slice))+row0-buffer
buffpixs=where(slicex lt row0 or slicex gt row1)
badpixs=where(slicex ge row0 and slicex le row1)
fit=poly_fit(slicex(buffpixs),slice(buffpixs),1)
fixed=slicex(badpixs)*fit(1)+fit(0)

;fit=poly_fit(slicex(buffpixs),slice(buffpixs),2)
;fixed=slicex(badpixs)*fit(1)+fit(0)+fit(2)*slicex(badpixs)*slicex(badpixs)
;I considered fitting a quadratic but decided not to.
;slice(badpixs)=fixed
outim(col,row0:row1)=fixed
return,outim
end


function fixhoriz,image,row,col0,col1,buffer
;function to fix along a horizontal stripe running from col0 to col1
;at the right hand edge
;by linear-fitting the pixels in the buffer
outim=image
slice=outim(col0-buffer:col1,row)
slicex=findgen(n_elements(slice))+col0-buffer
buffpixs=where(slicex lt col0)
badpixs=where(slicex ge col0 and slicex le col1)
fit=poly_fit(slicex(buffpixs),slice(buffpixs),1)
fixed=slicex(badpixs)*fit(1)+fit(0)

;fit=poly_fit(slicex(buffpixs),slice(buffpixs),2)
;fixed=slicex(badpixs)*fit(1)+fit(0)+fit(2)*slicex(badpixs)*slicex(badpixs)
;I considered fitting a quadratic but decided not to.
;slice(badpixs)=fixed


;plot,slicex,slice,psym=-2,title=(row)
;oplot,slicex(badpixs),fixed,psym=-4
;wait,2

outim(col0:col1,row)=fixed
return,outim
end


function fixhoriz2,image,row,col0,col1,buffer
;function to fix along a horizontal stripe running from col0 to col1
;by linear-fitting the pixels in the buffer
outim=image
slice=outim(col0-buffer:col1+buffer,row)
slicex=findgen(n_elements(slice))+col0-buffer
buffpixs=where(slicex lt col0)
badpixs=where(slicex ge col0 and slicex le col1)
fit=poly_fit(slicex(buffpixs),slice(buffpixs),1)
fixed=slicex(badpixs)*fit(1)+fit(0)

;fit=poly_fit(slicex(buffpixs),slice(buffpixs),2)
;fixed=slicex(badpixs)*fit(1)+fit(0)+fit(2)*slicex(badpixs)*slicex(badpixs)
;I considered fitting a quadratic but decided not to.
;slice(badpixs)=fixed


;plot,slicex,slice,psym=-2,title=(row)
;oplot,slicex(badpixs),fixed,psym=-4
;wait,2

outim(col0:col1,row)=fixed
return,outim
end


function fixps2,ps
;bruce's version of Don's pencil filter; designed to remove the
;(clearly artifact) stripes in the power spectra
;not very general-purpose - tailored to the main glitches visible in the
;io data

outps=fixps(ps)
;first, remove constant-value stripes

for col=0,1 do outps=fixvert(outps,col,125,131,5)
;left glitch
;for col=62,66 do outps=fixvert(outps,col,125,131,5)
;center glitch
;for row=116,142
for row=116,124 do outps=fixhoriz2(outps,row,63,65,5)
for row=134,142 do outps=fixhoriz2(outps,row,63,65,5)
for row=125,133 do outps=fixhoriz2(outps,row,61,67,5)


for row=0,16 do outps=fixhoriz(outps,row,122,128,5)
;top glitch
for row=239,255 do outps=fixhoriz(outps,row,122,128,5)
;bottom glitch

for col=114,117 do outps=fixvert(outps,col,220,223,5)

;second try: fit across the remaining spikes




return,outps
end