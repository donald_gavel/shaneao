;
;  remove horizontal stripes
;  modified by bmac the same way devstripe was

pro dehstripem,datain,dataout,xspan0,rejbox
  if n_params() eq 0 then begin
    print,"usage: dehstripe,datain,dataout,[xspan],[rejbox]"
    return
  end
  if n_elements(xspan0) eq 0 then xspan0 = [8,250]
  na = xspan0(1) - xspan0(0) + 1
  nx = (size(datain))(1)
  ny = (size(datain))(2)
  n = (size(datain))(3)
  mask = fltarr(nx,ny)
  mask (6:nx-6,6:ny-6)=1.0
  mask = 1.0 - mask
  print,"dehstripe..."
  if n_elements(dataout) eq 0 then dataout = make_array(nx,ny,n)
  bra = make_array(nx,ny)
  for i=0,(n-1) do begin
    xspan=xspan0
    print,format='($,I0," ")', i
    peak=brightest(median(datain(*,*,i)-mask*1000.0,3)) 
    halfspan=(xspan(0)+xspan(1))/2
    if (peak(0) ge halfspan) then xspan(1)=min([peak(0)-rejbox,xspan(1)])
    if (peak(0) lt halfspan) then xspan(0)=max([peak(0)+rejbox,xspan(0)])
    br=fltarr(ny)
    for h=0,(ny-1) do begin
     slice=datain(xspan(0):xspan(1),h,i)
     br(h)=median(slice)
    endfor
;   br = total(datain(xspan(0):xspan(1),0:(ny-1),i),1)/na  ; horizontal stripes
    for j=0,(nx-1),1 do bra(j,*) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end
