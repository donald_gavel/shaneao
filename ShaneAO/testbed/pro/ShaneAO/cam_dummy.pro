;
; ShaneAO support routine
;   cam_dummy.pro
;  generate dummy WFS data:
;    1 - raw 128x128 frame
;    2 - processed 40x40 frame
;    
;
function shift_spot,spot,x,y ; does sub-pixel shifts. this assumes "spot" is a 5x5 pixel image
  n = 128
  return,(fshift(zeropad(spot,n,n),x,y))[n/2-3:n/2+1,n/2-3:n/2+1]
end

n = 160
ns = 5
nc = 4
spot = gauss2(ns,1.5,1.5,1.4)
modeset = ['30']+' across'; ['8','16','30']+' across'
n_modes = (size(modeset))[1]
save_files = 0
tilt = [.1,.1]*0.
z_value = 0.001  ; size of aberration. Keep this small <<1 to get small spot shifts.
z_n = 2 ; zernike index, following Noll's convention
z_m = 0 ;    focus: n=2, m=0
        ;    astigmatism: n=2, m=2
        ;    coma: n=3, m=1
        ;    tricoma: n=3, m=3
r_value = 0; .001 ; random aberration.  Keep this small <<1 to get small spot shifts.

for modeno = 0,n_modes-1 do begin
  mode = modeset[modeno]
  
  if (mode eq '8 across') then begin
    n_across = 8
    np = 40
    d = 8
    ds = 2 ; secondary
  endif
  
  if (mode eq '16 across') then begin
    n_across = 16
    np = 80
    d = 14
    ds = 4; 6 ; secondary
  endif
  
  if (mode eq '30 across') then begin
    n_across = 32
    np = 160
    d = 30
    ds = 8; 12 ; secondary
  endif
  
  ap = circle(n_across,n_across,n_across/2-.5,n_across/2-.5,d/2,1.) - circle(n_across,n_across,n_across/2-.5,n_across/2-.5,ds/2,1.)
  r = n_across/2-.5
  focus = z_value*real(zernikeslope(n_across,n_across,r,z_n,z_m,center=[1,1]*r)) ; thie is a (2 x n_across) x n_across array of x and y slope components
  focus_x = focus[0:n_across-1,*]
  focus_y = focus[n_across:n_across*2-1,*]
  rand_x = r_value*randomn(seed,n_across,n_across)
  rand_y = r_value*randomn(seed,n_across,n_across)
  xy = coords(ap,where(ap))
  n_illum = size(xy,/dimensions)/2
  xy = transpose(reform(xy,n_illum,2))  
  nx = (size(xy,/dimensions))[1]
  
  ; -- raw frame ---
  offset = [1,1]*(n/2 - (n_across/2)*ns)
  a = fltarr(n,n)
  for k = 0,nx-1 do begin
    ind = offset + xy[*,k]*ns
    aspot = shift_spot(spot,focus_x[xy[0,k],xy[1,k]],focus_y[xy[0,k],xy[1,k]])
    aspot = shift_spot(aspot,tilt[0],tilt[1])
    aspot = shift_spot(aspot,rand_x[xy[0,k],xy[1,k]],rand_y[xy[0,k],xy[1,k]])
    a[ind[0]:ind[0]+ns-1,ind[1]:ind[1]+ns-1] = aspot
  endfor
  
  ; -- decimated / clipped (processed) frame --
  offsetp = [1,1]*(np/2 - (n_across/2)*nc)
  aproc = fltarr(np,np)
  for k = 0,nx-1 do begin
    inda = offset + xy[*,k]*ns
    indap = offsetp + xy[*,k]*nc
    aproc[indap[0]:indap[0]+nc-1,indap[1]:indap[1]+nc-1] = a[inda[0]:inda[0]+nc-1,inda[1]:inda[1]+nc-1]
  endfor
  
  disp,ap,'aperture '+mode
  if (save_files) then writefits,'aperture '+mode+'.fits',ap
  disp,a,'raw frame '+mode
  if (save_files) then writefits,'raw frame '+mode+'.fits',a
  disp,aproc,'processed frame '+mode
  if (save_files) then writefits,'processed frame '+mode+'.fits',aproc
  
endfor ; modeset

end
