;
;  stroke.pro
; calculate the rms phase across an aperture given
;    r0 and aperture diameter
;
function stroke,D,r0,L0,n=n,tiltRemoved=tiltRemoved
  microns = 1.e-6
  if (n_elements(n) eq 0) then n = 256
  lambda0 = 0.55*microns
  du = D*2/long(n)
  ap = circle(n,n,n/2,n/2,(D/2)/du,1.)
  f = screengen(n,n,[r0,L0],du)
  ph = (lambda0/(2*!pi))*screengen(f,seed)
  ph = depiston(ph,ap)
  if (keyword_set(tiltRemoved)) then ph = detilt(ph,ap)
  return,rms(ph,ap)
end

microns = 1.e-6
cm = 1.e-2
r0 = 10.*cm
L0 = 300.
D = 3.
n_trials = 100
r = fltarr(n_trials)
for k=0,n_trials-1 do begin
 r[k] = stroke(D,r0,L0,/tiltRemoved)/microns
endfor
print,average(r),stddev(r)
end

