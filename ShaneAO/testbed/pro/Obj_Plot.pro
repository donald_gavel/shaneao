Pro obj_oplot,mywindow, myview, xdata, ydata
  myplot2 = OBJ_NEW('IDLgrPlot', xdata, ydata, color=[200,0,0])
  mymodel = myview->getByName("mymodel")
  myplot = mymodel->GetByName("mainPlot")
  myplot->GetProperty, XRANGE=xr, YRANGE=yr
  xscale = norm_coord(xr)
  yscale = norm_coord(yr)
  myplot2->SetProperty, xcoord_conv=xscale, ycoord_conv=yscale
  mymodel->Add, myplot2
  mywindow->Draw, myview
end

PRO obj_plot, xdata, ydata, VIEW=myview, MODEL=mymodel, WINDOW=mywindow, $
   CONTAINER=mycontainer, XAXIS=myxaxis, YAXIS=myyaxis, $
   PLOT=myplot, _EXTRA=e

; Next, create the window, view, and model objects:
mycontainer = OBJ_NEW('IDL_Container')
mywindow = OBJ_NEW('IDLgrWindow',retain=2,dimensions=[520,500])
myview = OBJ_NEW('IDLgrView')
mymodel = OBJ_NEW('IDLgrModel',name="mymodel")

; And a font object, specifying Times Roman for
; the font and the default size of 12 points:
myfont = OBJ_NEW('IDLgrFont', 'times')

; Next, create a plot object using data specified
; at the command line:
myplot = OBJ_NEW('IDLgrPlot', xdata, ydata, COLOR=[200,100,200],name="mainPlot")

; Now pass any extra keywords to OBJ_PLOT to the SetProperty
; method of the plot object. Keywords that do not apply to the
; SetProperty method are ignored.
myplot->SetProperty, _EXTRA=e

; Retrieve the data ranges from the plot object,
; and convert to normalized coordinates using the
; NORM_COORD function.
myplot->GetProperty, XRANGE=xr, YRANGE=yr
xscale = norm_coord(xr)
yscale = norm_coord(yr)
myplot->SetProperty, XCOORD_CONV=xscale, YCOORD_CONV=yscale

; Using the range from the plot object, create X- and
; Y-axis objects with appropriate ranges, and convert
; to normalized coordinates. Set the tick lengths to 5% of
; the data range (which is now normalized to 0.0-1.0).

; create axis for all 4 sides of a box
myxaxis = OBJ_NEW('IDLgrAxis', 0, RANGE=[xr[0], xr[1]])
myxaxis2 = obj_new('IDLgrAxis', 0, RANGE=[xr[0], xr[1]],tickdir=1,/notext)
myyaxis = OBJ_NEW('IDLgrAxis', 1, RANGE=[yr[0], yr[1]])
myyaxis2 = obj_new('IDLgrAxis', 1, RANGE=[yr[0], yr[1]],tickdir=1,/notext)

; determine the "natural" size of the axes, compute the scale factors
myxaxis->GetProperty, CRANGE = xcr
myyaxis->GetProperty, CRANGE = ycr

; move the 2nd axes to their appropriate location in scaled space
myxaxis->SetProperty, location=[0,yscale[0]+yscale[1]*ycr[0]]
myxaxis2->SetProperty, location=[0,yscale[0]+yscale[1]*ycr[1]]
myyaxis->SetProperty, location=[xscale[0]+xscale[1]*xcr[0],0]
myyaxis2->SetProperty, location=[xscale[0]+xscale[1]*xcr[1],0]

myxaxis->SetProperty, XCOORD_CONV=xscale
myxaxis2->SetProperty, XCOORD_CONV=xscale
myyaxis->SetProperty, YCOORD_CONV=yscale
myyaxis2->SetProperty, YCOORD_CONV=yscale

; set the tick lengths
tickLength = 0.05
myxaxis->SetProperty, TICKLEN=tickLength
myxaxis2->SetProperty, TICKLEN=tickLength
myyaxis->SetProperty, TICKLEN=tickLength
myyaxis2->SetProperty, TICKLEN=tickLength

; Add the model object to the view object,
; and the plot and axis objects to the model object.
myview->Add, mymodel
mymodel->Add, myplot
mymodel->Add, myxaxis
mymodel->Add, myxaxis2
mymodel->Add, myyaxis
mymodel->Add, myyaxis2

; Use the SET_VIEW routine to set an appropriate viewplane
; rectangle and zclip region for the view.
SET_VIEW, myview, mywindow

; Add a title to the X-axis, using the font object defined above:
xtext = OBJ_NEW('IDLgrText', 'X Title', FONT=myfont)
myxaxis->SetProperty, TITLE=xtext
ytext = OBJ_NEW('IDLgrText', 'Y Title', FONT=myfont)
myyaxis->SetProperty, TITLE=ytext

; Add all objects to the container object.
; Destroying the container destroys all of its contents:
mycontainer->Add, mywindow
mycontainer->Add, myview
mycontainer->Add, myfont
mycontainer->Add, xtext
mycontainer->Add, ytext

; Finally, draw the object tree:
;mywindow->Draw, myview
mywindow->SetProperty,dimensions=[900,500]
mywindow->Draw, myview
END

npts = 100
xdata = findgen(npts)*2*!pi/float(npts)
ydata = 12*sin(xdata)
ydata2 = 12*cos(xdata)
;if (obj_valid(cont)) then obj_destroy,cont
OBJ_PLOT, xdata,ydata, WINDOW=win, VIEW=view, PLOT=plot, XAXIS=xax, YAXIS=yax, CONTAINER=cont
end
