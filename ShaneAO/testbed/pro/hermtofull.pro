;
;  hermtofull.pro - convert the complex hermetian data type to
;                   fully complex type
;
function hermtofull, hmat
  n = (size(hmat))(2)
  res = [hmat, (conj(shift(reverse(reverse(hmat),2),1,1)))(2:(n/2),0:(n-1))]
  return,res
end
