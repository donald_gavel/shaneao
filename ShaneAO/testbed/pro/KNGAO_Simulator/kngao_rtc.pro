;-----------
; kngao_rtc.pro
;  This IDL code is a simulation tool for the massive parallel processor
;  implementation of multi-guidestar laser tomography.
;  Its purpose is to validate the MPP algorithm and processor architecture
;  design at a high level. It can later function as a baseline reference
;  source for dummy input and comparison output data for use during
;  development and testing of the RTC.
;
;  kngao = Keck Next Generation Adaptive Optics
;  rtc = Real-time control
;-----------
;  This is work in progress...
;
;  Version: 1.0
;  Date: 3/16/2009
;  Contact: Don Gavel, UCSC
;
;  Version 1 tests
;     - MPP Fourier-Transform algorithm
;     - forward propagation
;     - back propagation
;     - It also nominally tests the parameter loading process but this approach is more likely subject to change.
;
;  Future version will test
;     - interpolation (needed for cone beam projection and supporting height-dependent voxel sizes)
;     - camera data processing and slope-to-phase wavefront reconstruction
;     - DM control processing (influence function inverse fitting for woofer, open-loop model for MEMS)
;     - pseudo-open-loop / hybrid closed loop / woofer-tweeter offload
;     - split tomography (incorporation of tip/tilt/focus/astigmatism sensor data)
;-----------
;
;        Architecture of Processors:
; one voxel unit contains:
;   the Fourier ccmponents of delta-index
;   the Fourier factors needed to do the transform
;   Cn2 for this layer
;   shift factors for each guidestar
;   the cone beam scale factor for this altitude (assuming 90km sodium guidestars)
;   spatial frequency address (x and y)
;   factor for post-conditioning (k^(11/3))

; one wfs pixel unit contains:
;   the Fourier components of delta-phase
;   the Fourier factors needed to do the transform
;   spatial frequency address (x and y)
;   complex matrix elements for pre-conditioning
;   spatial domain mask for pupil (0 or 1)

; one DM pixel unit contains:
;   the Fourier components of the actuator commands
;   plate-equation Fourier factors
;   actuator 2-entry look-up table
;   alternative to plate equation: Fourier factors for inverting the DM influence function
;
;-------------
;  References
;   1. "Fast hardware implementation of tomography for multi-guidestar adaptive optics,"
;      D. Gavel, M. Reinig and C. Cabrera. In: Astronomical Adaptive Optics Systems and Applications II,
;      Proc. SPIE, Vol. 5903, eds. R.K. Tyson and M. Lloyd-Hart, 138-147, 2005.
;   2. "Tomography for multiconjugate adaptive optics systems using laser guide stars,"
;      D.T. Gavel. In: Advancements in Adaptive Optics, Proc. SPIE, Vol. 5490,
;      eds. D.B. Calia, B.L. Ellerbroek and R. Ragazzoni, 1356-1373, 2004.
;------------


;===========================================================
;  section 1:
;       massively-parallel RTC processor
;===========================================================
pro mpp_transform,voxel_array
  s = size(voxel_array)
  d = s[0]
  n = s[1]
  if (d eq 2) then begin
    sx = [-1,0]
    sy = [0,-1]
  endif else begin
    sx = [-1,0,0]
    sy = [0,-1,0]
  endelse
  ; ====== The mpp Fourier Transform is 4 lines of code ========
  ; ====== done in 2n steps ====================================
  print,'<mpp_transform> doing x transforms' & wait,.01
  for ix = 0,n-1 do begin
    voxel_array.accum1 += voxel_array.phi*voxel_array.c[ix,0]
    voxel_array.phi = shift(voxel_array.phi,sx)
  endfor
  print,'<mpp_transform> doing y transforms' & wait,.01
  for iy = 0,n-1 do begin
    voxel_array.fphi += voxel_array.accum1*voxel_array.c[iy,1]
    voxel_array.accum1 = shift(voxel_array.accum1,sy)
  endfor
  ; ============================================================
  print,'<mpp_transform> done' & wait,.01
end

pro mpp_backpropagate,voxel_array
  s = size(voxel_array)
  dimen = s[0]
  if (dimen eq 2) then n_layers = 1 else n_layers = s[3]
  ; ===== the mpp back propagation is 3 lines of code =====
  ; ===== done in n_layers steps ==========================
  for layer = 0,n_layers-1 do voxel_array[*,*,layer].s = shift(voxel_array[*,*,layer].s,[-layer,0,0])
  voxel_array.accum1 = voxel_array.fwfs
  for layer = 0,n_layers-1 do begin
    voxel_array.fphi += voxel_array.accum1*conj(voxel_array.s[0])*voxel_array.cn2
    voxel_array.accum1 = shift(voxel_array.accum1,[0,0,1])
    voxel_array.s = shift(voxel_array.s,[1,0,0,0])
  endfor
  for layer = 0,n_layers-1 do voxel_array[*,*,layer].s = shift(voxel_array[*,*,layer].s,[layer,0,0])
; ==========================================================
end

pro mpp_forwardpropagate,voxel_array
  s = size(voxel_array)
  dimen = s[0]
  if (dimen eq 2) then n_layers = 1 else n_layers = s[3]
  n_guidestars = (size(voxel_array[0,0,0].s))[1]
  voxel_array.fwfs *= 0
  ; ===== the mpp forward propagation is 3 lines of code =====
  ; ===== done in n_guidestars * n_layers steps ==============
  for guidestar = 0,n_guidestars-1 do begin
    voxel_array.accum1 = voxel_array.fphi*voxel_array.s[guidestar]
    for layer = 0,n_layers-1 do begin
      voxel_array[*,*,guidestar].fwfs += voxel_array[*,*,guidestar].accum1
      voxel_array.accum1 = shift(voxel_array.accum1,[0,0,-1])
    endfor
  endfor
; ==========================================================
end

pro mpp_precondition,voxel_array
;  not implemented yet
end

pro mpp_postcondition,voxel_array
;  not implemented yet
end

pro mpp_loop,voxel_array,wfs
  s = size(voxel_array)
  dimen = s[0]
  if (dimen eq 2) then n_layers = 1 else n_layers = s[3]
  n = s[2]
  n_guidestars = (size(wfs))[3]
  wfs_f = ftcube(wfs) ; convert the data to the spatial frequency domain
  ;  (This is the assumed input to the tomography engine. Later we'll model the wfs mpp.)
  mpp_forwardpropagate,voxel_array
  ; ===== load the voxel array with the data =====
  ; ===== done in n steps ========================
  sh = [1,0,0]
  for k = 0,n-1 do begin
    voxel_array[0,*,0:n_guidestars-1].accum1 = reform(wfs_f[n-1,*,*],1,n,n_guidestars)
    voxel_array.accum1 = shift(voxel_array.accum1,sh)
    wfs_f = shift(wfs_f,sh)
  endfor
  ; ===== implement the summing junction and multiply by feedback gain ==
  ; ===== done in 3 steps ==============================================
  voxel_array.fwfs -= voxel_array.accum1 ; formulate the wfs residual
  voxel_array.fwfs *= -1 ; correct thw residual sign
  voxel_array.fwfs *= voxel_array.gain ; apply the feedback gain
  ; =================================================================
  ; complete the iteration by preconditioning, back-propagating, then postconditioning
  mpp_precondition,voxel_array
  mpp_backpropagate,voxel_array
  mpp_postcondition,voxel_array
end

;=================================================================
;   Section 2
;        simulator for "real" atmosphere and wavefront sensing
;=================================================================
function atmos_forwardpropagate,atmosphere,guidestar_directions,verbose=verbose
  ; this is done in the spatial domain
  ;  atmosphere is forward-propagated from each guidestar direction
  ;  and put in a wfs data cube
  if (n_elements(verbose) eq 0) then verbose = 0
  if (verbose) then begin
    print,'<atmos_forwardpropagate> starting' & wait,.01
  endif
  n_guidestars = (size(guidestar_directions))[2]
  n = (size(atmosphere.phi))[1]
  du = atmosphere.du
  wfs_phase = fltarr(n,n,n_guidestars)
  for gs = 0,n_guidestars-1 do begin
    if (verbose) then begin
      print,'guide star '+strtrim(gs,2) & wait,.01
    endif
    for layer = 0,atmosphere.n_layers-1 do begin
      sx = guidestar_directions[0,gs]*atmosphere.h[layer]/du
      sy = guidestar_directions[1,gs]*atmosphere.h[layer]/du
      wfs_phase[*,*,gs] += fshift(atmosphere.phi[*,*,layer],sx,sy)
    endfor
  endfor
  if (verbose) then begin
    print,'<atmos_forwardpropagate> done' & wait,.01
  endif
  return,wfs_phase
end

; defined constants
cm = 0.01
km = 1000.
microns = 1.0e-6
degrees = !pi/180.
arcsec = degrees/3600.
;
; parameter definitions for simulated atmosphere and guidestar constellation
n = 128
du = 20./float(n) ; 20 meters oversized screen above the 10 meter telescope
n_layers = 5
lambda00 = 0.5*microns
k = 2*!pi/lambda00
r0 = 10*cm
zenith = 0.0*degrees
c0 = r0^(-5./3.)/(0.423*k^2*(1/cos(zenith)))
h = [0,4,8,12,16]*km ; layer altitudes
percent_atmos = [0.517+0.119,0.063,0.061,0.105,0.081+0.054] ; Mauna Kea Ridge Model, Dekany KAON #503 (collapsed to 5 layers -DG)
;percent_atmos must sum to one, this is the normalized Cn2
Cn2 = c0*percent_atmos
wind = fltarr(n_layers,2) ; future feature
n_guidestars = 4 ; number of LGS guidestars
gs_angle = [[1.,1.],[1.,-1],[-1.,-1.],[-1.,1.]]*15.*arcsec/sqrt(2.)

;
; generate a random simulated atmosphere
atmosphere = {atmos, du:du, n:n, n_layers:n_layers, phi:fltarr(n,n,n_layers), Cn2:Cn2, wind:wind, h:h}
seed = 5
f = screengen(n,n,r0,du)
for k=0,n_layers-1 do begin
  atmosphere.phi[*,*,k] = screengen(f*sqrt(Cn2[k]/c0),seed)
endfor
wfs = atmos_forwardpropagate(atmosphere,gs_angle,/verbose)
n_apsample = 1024
ap = keckap(n_apsample,du=du*float(n)/float(n_apsample))
ap = smooth(float(ap),n_apsample/n)
ap = fix(rebin(ap,n,n) > 0.5)
for gs=0,n_guidestars-1 do wfs[*,*,gs] *= ap

;=======================================================
;  Section 3
;        supervisory computer
;=======================================================
; ------- define physical constants ------
i = complex(0.,1.)
arcsec = !pi/(180.*3600.)
km = 1.e3

; ------- define the problem to the RTC ---------
n_layers = 5 ; number of atmospheric layers
h = [0,1.,2.,4.,10.]*km
h = [0,4,8,12,16]*km
percent_atmos = [0.517+0.119,0.063,0.061,0.105,0.081+0.054] ; Mauna Kea Ridge Model, Dekany KAON #503 (collapsed to 5 layers -DG)
n_guidestars = 4 ; number of LGS guidestars
gs_angle = [[1.,1.],[1.,-1],[-1.,-1.],[-1.,1.]]*15.*arcsec/sqrt(2.)
na_across = 64 ; number of actuators across DM
ns_across = 48 ; number of subapertures across WFS
d_tele = 10. ; diameter of telescope, in meters
d_subap = d_tele/ns_across ; diameter of a WFS subaperature, in meters
dx = d_subap
n = nextpow2(2*ns_across) ; number of voxel processors on a layer
feedback_gain = 0.5
; load inital physical parameters, x,y and kx,ky axes
dk = 1./(dx*n)
kx = (findgen(n) - n/2)*dk
ky = kx
x = (findgen(n) - n/2)*dx
y = x
; -------------------------------------------------------------------------

; ---- define the aperture ----
;   the aperture will contain an indication of subap % illumination by pupil
n_apsample = 1024
ap = keckap(n_apsample,du=d_tele/(n_apsample/2))
ap = smooth(float(ap),n_apsample/n)
ap = rebin(ap,n,n)
; -----------------------------

; ------ define a voxel structure ----------
; one voxel unit contains:
;   phi: the delta-index
;   fphi: the Fourier components of delta-index
;   wfs: the phase at the wavefront sensor  (one wfs assigned per layer, assuming n_layers > n_guidestars)
;   fwfs: the Fourier components of phase at the wavefront sensor
;   accum1: a temporary accumulator
;   c: the Fourier factors needed to do the transform (x and y)
;   Cn2: Cn2 for this layer
;   s: complex shift factors for each guidestar: exp(i*2*pi*(sx*kx+sy*ky))
;   sf: the cone beam scale factor for this altitude (assuming 90km sodium guidestars)
;   x,y: spatial address
;   kx,ky: spatial frequency address
;   p: factor for post-conditioning (k^(11/3))
;   gain: feedback gain
;   a: elements of the preconditioner matrix
voxel_definition = {voxel, phi:real(0.), accum1:complex(0.), fphi:complex(0.), wfs:float(0.), fwfs:complex(0.), c: complexarr(n,2), Cn2:float(1.), $
  s: complexarr(n_layers), sf: float(1.), x:float(0.), y:float(0.), kx: float(0.), ky:float(0.), p: float(0.), gain: float(1.), a: fltarr(n_layers)}
voxel_array = replicate({voxel},n,n,n_layers)
; ------------------------------------------

; ------- load the voxel array -----------
; load up
;   1) spatial position and spatial frequency values
;   2) Cn2
;   3) preconditioner elements
;   4) feedback gain
;
print,'loading parameters' & wait,.1
t = systime(1)
voxel_array[*,0,0].x = x
voxel_array[*,0,0].kx = kx
voxel_array[0,*,0].y = reform(y,1,n)
voxel_array[0,*,0].ky = reform(ky,1,n)
voxel_array[0,*,0].gain = feedback_gain
for layer = 0, n_layers-1 do voxel_array[0,*,layer].Cn2 = Cn2[layer]
; distribute over layers
for layer = 1, n_layers-1 do begin
  voxel_array[*,0,layer].x = voxel_array[*,0,layer-1].x
  voxel_array[*,0,layer].kx = voxel_array[*,0,layer-1].kx
  voxel_array[0,*,layer].y = voxel_array[0,*,layer-1].y
  voxel_array[0,*,layer].ky = voxel_array[0,*,layer-1].ky
  voxel_array[0,*,layer].gain = voxel_array[0,*,layer-1].gain
endfor
; distribute across layers
for iy = 1,n-1 do voxel_array[*,iy,*].x = voxel_array[*,iy-1,*].x
for iy = 1,n-1 do voxel_array[*,iy,*].kx = voxel_array[*,iy-1,*].kx
for ix = 1,n-1 do voxel_array[ix,*,*].y = voxel_array[ix-1,*,*].y
for ix = 1,n-1 do voxel_array[ix,*,*].ky = voxel_array[ix-1,*,*].ky
for ix = 1,n-1 do voxel_array[ix,*,*].gain = voxel_array[ix-1,*,*].gain
for ix = 1,n-1 do voxel_array[ix,*,*].Cn2 = voxel_array[ix-1,*,*].Cn2
;
print,'time to load parameters: ',systime(1)-t,' seconds' & wait,.1
print,'Computing Fourier factors' & wait,.1
t = systime(1)
;
; calculate the Fourier factors
;   load at the edge
for ix = 0,n-1 do voxel_array[*,0,0].c[ix,0] = exp(i*2*!pi*x[ix]*kx)
for iy = 0,n-1 do voxel_array[0,*,0].c[iy,1] = reform(exp(i*2*!pi*y[iy]*ky),1,n)
;   distribute across array and over layers
for ix = 1,n-1 do voxel_array[ix,*,0].c[*,1] = voxel_array[ix-1,*,0].c[*,1]
for iy = 1,n-1 do voxel_array[*,iy,0].c[*,0] = voxel_array[*,iy-1,0].c[*,0]
for layer = 1,n_layers-1 do voxel_array[*,*,layer].c = voxel_array[*,*,layer-1].c

print,'time to compute Fourier factors: ',systime(1)-t,' seconds' & wait,.1
print,'loading guidestar shift factors' & wait,.1
t = systime(1)
;
; ---------- load the guidestar shift factors ---------------
for layer = 0,n_layers-1 do begin
  for guidestar = 0,n_guidestars-1 do begin
    voxel_array[*,*,layer].s[guidestar] = exp(i*2*!pi*(voxel_array[*,*,layer].kx*gs_angle[0,guidestar]*h[layer] + voxel_array[*,*,layer].ky*gs_angle[1,guidestar]*h[layer]))
  endfor
endfor
print,'time to load shift factors: ',systime(1)-t,' seconds' & wait,.1
;

; ------------------------------

;=======================================================================================================
;  test the massively parallel processor (mpp) Fourier transform algorithm by transforming the aperture
;-------------------------------------------------------------------------------------------------------
test_fourier = 1 & fourier_method = 'fft'
test_forwardProp = 1
test_backProp = 1

if (not test_fourier) then begin
  test_fourier = 1
  fourier_method = 'fft'
endif

if (test_fourier) then begin
  ; for layer = 0,n_layers-1 do voxel_array[*,*,layer].phi = ap ; load aperture into phase
  voxel_array[*,*,n_layers-1].phi = ap
  print,'starting Fourier test' & wait,.01
  
  if ((fourier_method eq 'mpp') or (fourier_method eq 'both')) then begin
    ; forward transform using mpp algorithm
    t = systime(1)
    voxel_array.accum1 = 0
    voxel_array.fphi = 0
    mpp_transform,voxel_array
    voxel_array.fphi /= float(n)^2 ; scaling for the forward transform
    ; for the inverse transform, we don't scale, and the Fourier coefficient must be pre-conjugated
    print,'mpp ft time elapsed: ',systime(1)-t,' seconds'
  ; disp,abs(voxel_array.fphi),'mpp algorithm'
  endif
  
  if ((fourier_method eq 'fft') or (fourier_method eq 'both')) then begin
    ; forward transform using fft algorithm
    t = systime(1)
    for layer = 0,n_layers-1 do voxel_array[*,*,layer].fphi = ft(voxel_array[*,*,layer].phi)
    print,'fft time elapsed: ',systime(1)-t,' seconds'
  ; disp,abs(voxel_array.fphi),'fft algorithm'
  endif
  
endif  ; if (test_fourier)

if (test_forwardProp) then begin
  ; assumes voxels already have Fourier transform coefficients
  print,'starting forward propagation test' & wait,.01 & t = systime(1)
  mpp_forwardPropagate,voxel_array
  print,'mpp propagation time elapsed: ',systime(1)-t,' seconds' & wait,.01
  ; check the answer by inverse transforming the wfs
  for guidestar = 0,n_guidestars-1 do voxel_array[*,*,guidestar].wfs = ft(voxel_array[*,*,guidestar].fwfs,/inverse)
  disp,voxel_array[*,*,0:n_guidestars-1].wfs,'wfs'
endif  ; if (test_forwardProp)

if (test_backProp) then begin
  voxel_array.fphi *= 0
  voxel_array.wfs *= 0
  voxel_array.fwfs *= 0
  for gs = 0,n_guidestars-1 do voxel_array[*,*,gs].wfs = (gs+1)*ap ; put dummy data in the wfs for testing up-propagation
  voxel_array.fwfs = ftcube(voxel_array.wfs)
  print,'starting back propagation test' & wait,.01 & t = systime(1)
  mpp_backPropagate,voxel_array
  print,'mpp propagation time elapsed: ',systime(1)-t,' seconds' & wait,.01
  ; check the answer by inverse transforming the wfs
  voxel_array.phi = ftcube(voxel_array.fphi,/inverse)
  disp,voxel_array.phi,'phi'
endif

end

