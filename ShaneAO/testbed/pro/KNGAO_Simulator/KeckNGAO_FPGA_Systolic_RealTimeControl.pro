;
;  KeckNGAO_FPGA_Systolic_RealTimeControl.pro
;     Simulates the proposed architecture for the NGAO Real-Time Controller
;
;  Version: 1.0
;  Date: 2/19/2010
;  Contact: Don Gavel, UCSC
;
;  References
;   1. "Fast hardware implementation of tomography for multi-guidestar adaptive optics,"
;      D. Gavel, M. Reinig and C. Cabrera. In: Astronomical Adaptive Optics Systems and Applications II,
;      Proc. SPIE, Vol. 5903, eds. R.K. Tyson and M. Lloyd-Hart, 138-147, 2005.
;   2. "Tomography for multiconjugate adaptive optics systems using laser guide stars,"
;      D.T. Gavel. In: Advancements in Adaptive Optics, Proc. SPIE, Vol. 5490,
;      eds. D.B. Calia, B.L. Ellerbroek and R. Ragazzoni, 1356-1373, 2004.
;

;--------------------------------------------------------------------------
; Definition of operations

pro init
  ; Definitions of physical constants and unit conversion factors
  common physical, km, cm, mm, microns, nm, arcsec, i
  km = 1.e3
  cm = 1.e-2
  mm = 1.e-3
  microns = 1.e-6
  nm = 1.e-9
  arcsec = !pi/(180.*3600.)
  i = complex(0.,1.)
end

pro mpp_transform,voxel_array,fast=fast
  t = systime(1)
  s = size(voxel_array)
  ndimen = s[0]
  if (ndimen eq 2) then n_layers = 1 else n_layers = s[3]
  n = s[1] ; horizontal dimension
  
  if (keyword_set(fast)) then begin      ; fast Fourier transform
    if (ndimen eq 2) then begin
      voxel_array.fphi = ft(voxel_array.phi)
    endif else begin
      for layer = 0,n_layers-1 do begin
        voxel_array[*,*,layer].fphi = ft(voxel_array[*,*,layer].phi)
      endfor
    endelse
    print,'<mpp_transform> time to compute Fourier transform (fast): ',systime(1)-t,' seconds' & wait,.1
    return
  endif
                                          ; mpp Fourier transform
  if (ndimen eq 2) then begin
    sx = [-1,0]
    sy = [0,-1]
  endif else begin
    sx = [-1,0,0]
    sy = [0,-1,0]
  endelse
  ;  The mpp Fourier Transform is 4 lines of code
  print,'<mpp_transform> doing x transforms' & wait,.01
  for ix = 0,n-1 do begin
    voxel_array.accum1 += voxel_array.phi*voxel_array.c[ix,0]
    voxel_array.phi = shift(voxel_array.phi,sx)
  endfor
  print,'<mpp_transform> doing y transforms' & wait,.01
  for iy = 0,n-1 do begin
    voxel_array.fphi += voxel_array.accum1*voxel_array.c[iy,1]
    voxel_array.accum1 = shift(voxel_array.accum1,sy)
  endfor
  print,'<mpp_transform> done' & wait,.01
  print,'<mpp_transform> time to compute Fourier transform (mpp): ',systime(1)-t,' seconds' & wait,.1
  
end

pro mpp_backpropagate,voxel_array
  s = size(voxel_array)
  ndimen = s[0]
  if (ndimen eq 2) then n_layers = 1 else n_layers = s[3]
  ;  the mpp back propagation is 3 lines of code 
  ;  done in n_layers steps 
  for layer = 0,n_layers-1 do voxel_array[*,*,layer].s = shift(voxel_array[*,*,layer].s,[-layer,0,0])
  voxel_array.accum1 = voxel_array.fwfs
  for layer = 0,n_layers-1 do begin
    voxel_array.fphi += voxel_array.accum1*conj(voxel_array.s[0])*voxel_array.cn2
    voxel_array.accum1 = shift(voxel_array.accum1,[0,0,1])
    voxel_array.s = shift(voxel_array.s,[1,0,0,0])
  endfor
  for layer = 0,n_layers-1 do voxel_array[*,*,layer].s = shift(voxel_array[*,*,layer].s,[layer,0,0])
end

pro mpp_forwardpropagate,voxel_array
  s = size(voxel_array)
  ndimen = s[0]
  if (ndimen eq 2) then n_layers = 1 else n_layers = s[3]
  n_guidestars = (size(voxel_array[0,0,0].s))[1]
  voxel_array.fwfs *= 0
  ; the mpp forward propagation is 3 lines of code 
  ;  done in n_guidestars * n_layers steps 
  for guidestar = 0,n_guidestars-1 do begin
    voxel_array.accum1 = voxel_array.fphi*voxel_array.s[guidestar]
    for layer = 0,n_layers-1 do begin
      voxel_array[*,*,guidestar].fwfs += voxel_array[*,*,guidestar].accum1
      voxel_array.accum1 = shift(voxel_array.accum1,[0,0,-1])
    endfor
  endfor
end

pro mpp_precondition,voxel_array
;  not implemented yet
end

pro mpp_postcondition,voxel_array
;  not implemented yet
end

pro mpp_loop,voxel_array,wfs
  s = size(voxel_array)
  dimen = s[0]
  if (dimen eq 2) then n_layers = 1 else n_layers = s[3]
  n = s[2]
  n_guidestars = (size(wfs))[3]
  wfs_f = ftcube(wfs) ; convert the data to the spatial frequency domain
  ;  (This is the assumed input to the tomography engine. Later we'll model the wfs mpp.)
  mpp_forwardpropagate,voxel_array
  ;  load the voxel array with the data 
  ;  done in n steps 
  sh = [1,0,0]
  for k = 0,n-1 do begin
    voxel_array[0,*,0:n_guidestars-1].accum1 = reform(wfs_f[n-1,*,*],1,n,n_guidestars)
    voxel_array.accum1 = shift(voxel_array.accum1,sh)
    wfs_f = shift(wfs_f,sh)
  endfor
  ;  implement the summing junction and multiply by feedback gain 
  ;  done in 3 steps 
  voxel_array.fwfs -= voxel_array.accum1 ; formulate the wfs residual
  voxel_array.fwfs *= -1 ; correct thw residual sign
  voxel_array.fwfs *= voxel_array.gain ; apply the feedback gain
  ; 
  ; complete the iteration by preconditioning, back-propagating, then postconditioning
  mpp_precondition,voxel_array
  mpp_backpropagate,voxel_array
  mpp_postcondition,voxel_array
end

pro mpp_init,voxel_array
  common physical
  
  s = size(voxel_array)
  n_layers = s[3]
  n = s[1]

;  <problem definition parameters
  h = [0,4,8,12,16]*km                                                ; layer heights in model
  Cn2 = [0.517+0.119,0.063,0.061,0.105,0.081+0.054]                   ; Mauna Kea Ridge Model, Dekany KAON #503 (collapsed into 5 layers -DG)
  n_guidestars = 4                                                    ; number of LGS guidestars
  gs_angle = [[1.,1.],[1.,-1],[-1.,-1.],[-1.,1.]]*15.*arcsec/sqrt(2.) ; positions of the guide stars on the sky
  na_across = 64                                                      ; number of actuators across DM
  ns_across = 63
  d_tele = 10.                                                        ; diameter of telescope, in meters
  gain = 0.5                                                          ; iteration feedback gain
;  /problem definition parameters>

  d_subap = d_tele/ns_across                                          ; diameter of a WFS subaperature, in meters
  dx = d_subap                                                        ; spatial sampling used in systolic volume
  x = (findgen(n) - n/2)*dx                                           ; spatial position samples
  y = transpose(x)
  dk = 1./(dx*n)                ; spatial frequency sampline in systolic voluem
  kx = (findgen(n) - n/2)*dk    ; spatial freqency samples
  ky = transpose(kx)

  print,'<mpp_init> loading parameters' & wait,.1
  t = systime(1)
  voxel_array[*,0,0].x = x
  voxel_array[*,0,0].kx = kx
  voxel_array[0,*,0].y = reform(y,1,n)
  voxel_array[0,*,0].ky = reform(ky,1,n)
  voxel_array[0,*,0].gain = gain
  for layer = 0, n_layers-1 do voxel_array[0,*,layer].Cn2 = Cn2[layer]
; distribute over layers
  for layer = 1, n_layers-1 do begin
     voxel_array[*,0,layer].x = voxel_array[*,0,layer-1].x
     voxel_array[*,0,layer].kx = voxel_array[*,0,layer-1].kx
     voxel_array[0,*,layer].y = voxel_array[0,*,layer-1].y
     voxel_array[0,*,layer].ky = voxel_array[0,*,layer-1].ky
     voxel_array[0,*,layer].gain = voxel_array[0,*,layer-1].gain
  endfor
; distribute across layers
  for iy = 1,n-1 do voxel_array[*,iy,*].x = voxel_array[*,iy-1,*].x
  for iy = 1,n-1 do voxel_array[*,iy,*].kx = voxel_array[*,iy-1,*].kx
  for ix = 1,n-1 do voxel_array[ix,*,*].y = voxel_array[ix-1,*,*].y
  for ix = 1,n-1 do voxel_array[ix,*,*].ky = voxel_array[ix-1,*,*].ky
  for ix = 1,n-1 do voxel_array[ix,*,*].gain = voxel_array[ix-1,*,*].gain
  for ix = 1,n-1 do voxel_array[ix,*,*].Cn2 = voxel_array[ix-1,*,*].Cn2
;
  print,'<mpp_init> time to load parameters: ',systime(1)-t,' seconds' & wait,.1
  print,'<mpp_init> Computing Fourier factors' & wait,.1
  t = systime(1)
;
; calculate the Fourier factors
;   load at the edge
  for ix = 0,n-1 do voxel_array[*,0,0].c[ix,0] = exp(i*2*!pi*x[ix]*kx)
  for iy = 0,n-1 do voxel_array[0,*,0].c[iy,1] = reform(exp(i*2*!pi*y[iy]*ky),1,n)
;   distribute across array and over layers
  for ix = 1,n-1 do voxel_array[ix,*,0].c[*,1] = voxel_array[ix-1,*,0].c[*,1]
  for iy = 1,n-1 do voxel_array[*,iy,0].c[*,0] = voxel_array[*,iy-1,0].c[*,0]
  for layer = 1,n_layers-1 do voxel_array[*,*,layer].c = voxel_array[*,*,layer-1].c

  print,'<mpp_init> time to compute Fourier factors: ',systime(1)-t,' seconds' & wait,.1
  print,'<mpp_init> loading guidestar shift factors' & wait,.1
  t = systime(1)
;
;  load the guidestar shift factors 
  for layer = 0,n_layers-1 do begin
     for guidestar = 0,n_guidestars-1 do begin
        voxel_array[*,*,layer].s[guidestar] = exp(i*2*!pi*(voxel_array[*,*,layer].kx*gs_angle[0,guidestar]*h[layer] + voxel_array[*,*,layer].ky*gs_angle[1,guidestar]*h[layer]))
     endfor
  endfor
  print,'<mpp_init> time to load shift factors: ',systime(1)-t,' seconds' & wait,.1
;
end

;-----------------------------------------------------------------------------

; sample parameters
n_layers = 5                ; number of atmospheric layers
ns_across = 63              ; number of subapertures across WFS
n = nextpow2(2*ns_across)   ; number of voxel processors on a layer


; ------------------------------------------------------------------------------
; Definition of Data Structures
aVoxel = {voxel, description: 'one voxel in the RTC systolic array', $
       	 phi: real(0.0), $    	   	 ; delta-index at this spatial point in the tomography volume
         accum1:complex(0.), $           ; local storage
	 fphi:complex(0.), $             ; Fourier coefficient of delta-index at this spatial frequency point in the tomography volume
	 wfs:float(0.), $                ; the phase at this pupil location in this wavefront sensor 
	 fwfs:complex(0.), $             ; the Fourier components of phase at this wavefront sensor
	 c: complexarr(n,2), $           ; Fourier kernel factors
	 Cn2:float(1.), $                ; Cn2 for this layer
	 s: complexarr(n_layers), $      ; shift factors for each guidestar at this layer: exp(i*2*pi*(sx*kx+sy*ky))
	 sf: float(1.), $                ; cone beam scale factor for this altitude
	 x:float(0.), $                  ; spatial address
	 y:float(0.), $
	 kx: float(0.), $                ; spatial frequency address
	 ky:float(0.), $
	 p: float(0.), $                 ; factor for post-conditioning (k^(11/3))
	 gain: float(1.), $              ; feedback gain
	 a: fltarr(n_layers) $           ; elements of the preconditioner matrix, A
	 }
;
voxel_array = replicate({voxel},n,n,n_layers)
init

; Test code
mpp_init,voxel_array
voxel_array[*,*,0].phi = circle(n,n,n/2,n/2,10,1.)
mpp_transform,voxel_array

end
