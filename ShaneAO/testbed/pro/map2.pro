;
;  map2.pro - Maximum-A-Posteriori restoration
;    MAP algorithm modified to take into account statistics of PSF exposure
;    and sky background noise
;
;  written by: Don Gavel, L-258, LLNL, gavel1@llnl.gov, Feb, 1999
;
;  usage:
;    map2, im, psf, bvar, bprimevar, niter, fhat, hhat, bhat, bprimehat
;
;  inputs:
;    im - object data image with zero-mean background
;    psf - psf data image with zero-mean background
;    bvar - variance of background of data image
;    bprimevar - variance of background of psf image
;    niter - number of iterations
;    gamma - gain on psf iteration, suggest << 1
;
;  outputs:
;    f - estimate of object
;    h - estimate of psf
;    b - estimate of object background
;    bprime - estimate of psf background
;
pro map2,im,psf,bvar,bprimevar,niter,gamma,f,h,b,bprime,residual=r,errorvector=errvec
  nx = (size(f))(1)
  ny = (size(f))(2)
  npixels = nx*ny
  f = real(im)>0
  g = real(im)
  d = real(psf)
  N = total(d)
  h = (d/N)>0
  b = bvar
;  b = f*0+bvar
  bprime = bprimevar
;  bprime = h*0+bprimevar
  g = g + b
  d = d + bprime
  lastIter = niter - 1
  print,'iter','err'
  errvec = fltarr(niter)
  for i = 0,lastIter do begin
    ghat = real(ftconvolve(f,h)) + b
    g1 = g/ghat - 1.
    err = total(g1^2)/npixels
    print,i,err
    errvec(i) = err
    f1 = real(ftconvolve(g1,h))
    f = f*exp(f1)
    dhat = N*h + bprime
    d1 = d/dhat - 1
    h1 = gamma*(real(ftconvolve(g1,f))/N + d1)
    h = h*exp(h1)
    dum = max(h)
    c = coords(h,!c)
    h = shift(h,nx/2-c(0),ny/2-c(1))
    h = h/total(h)
    b = b*exp(total(g1)/npixels)
;    b = b*exp(g1)
    bprime = bprime*exp(total(d1)/npixels)
;    bprime = bprime*exp(d1)
  endfor
  r = g-ghat
  print,'done'
end
