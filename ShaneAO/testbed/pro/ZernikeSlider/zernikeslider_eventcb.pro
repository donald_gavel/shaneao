;
; IDL Event Callback Procedures
; zernikeSlider_eventcb
;
; Generated on:	02/04/2006 13:37.43
;
;
; Empty stub procedure used for autoloading.
;
pro zernikeSlider_eventcb
end
;-----------------------------------------------------------------
; Notify Realize Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro zernikeSlider_Realize, wWidget
  print,'<zernikeSlider_Realize> widgitID = ',strtrim(string(wWidget),2)
  nZernikes = 12
  info = {zernikeInfo, n:0, plateScale:0.,norm:1.,pa:ptr_new(),dispID:0,sliderList:intarr(nZernikes)}
  id = widget_info(wWidget,find_by_uname='Textn')
  widget_control,id,get_value=text
  n = fix(text[0])
  id = widget_info(wWidget,find_by_uname='TextPlateScale')
  widget_control,id,get_value=text
  plateScale = float(text[0])
  ap = circle(n,n,n/2,n/2,n*plateScale,1.)
  info.pa = ptr_new(ap)
  im = abs(ft(ap))
  info.norm = max(im)
  im = im/info.norm
  disp,im,'Zernike Slider PSF',x0=[-1,-1]*info.n/2
  id = widget_info(wWidget,find_by_uname='TextStrehlOn')
  widget_control,id,set_value=strtrim(string(1.),2)
  id = widget_info(wWidget,find_by_uname='TextStrehlPeak')
  widget_control,id,set_value=strtrim(string(1.),2)
  id = !disp.windows[!disp.wincount-1]
  info.n = n
  info.plateScale = plateScale
  info.dispID = id

  k = 0
  id = widget_info(wWidget,find_by_uname = 'SliderFocus20')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[2,0,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderAstig22')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[2,2,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderAstig22m')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[2,2,1],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderComa31')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[3,1,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderComa31m')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[3,1,1],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderComa33')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[3,3,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderComa33m')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[3,3,1],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderSpherical40')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[4,0,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderSpherical42')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[4,2,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderSpherical42m')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[4,2,1],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderSpherical44')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[4,4,0],set_value=0
  id = widget_info(wWidget,find_by_uname = 'SliderSpherical44m')
  info.sliderList[k] = id & k++
  widget_control,id,set_uvalue=[4,4,1],set_value=0

  widget_control,wWidget,set_uvalue=info

end
;-----------------------------------------------------------------
; Text Insert String Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TEXT_STR, ID:0L, TOP:0L, HANDLER:0L, TYPE:1, OFFSET:0L,
;       STR:'' }

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   OFFSET is the (zero-based) insertion position that will result
;       after the character is inserted. STR is the string to be
;       inserted.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro zernikeSlider_Redo, Event
  print,'<zernikeSlider_Redo>'
  base = Event.top
  widget_control,base,get_uvalue=info
  nZernikes = (size(info.sliderList))[1]
  id = widget_info(base,find_by_uname='Textn')
  widget_control,id,get_value=text
  n = fix(text[0])
  info.n = n
  id = widget_info(base,find_by_uname='TextPlateScale')
  widget_control,id,get_value=text
  plateScale = float(text[0])
  info.plateScale = plateScale
  ap = circle(n,n,n/2,n/2,n*plateScale,1.)
  im = abs(ft(ap))
  info.norm = max(im)
  im = im/info.norm
  if (ptr_valid(info.pa)) then ptr_free,info.pa
  info.pa = ptr_new(ap)
  if (widget_info(info.dispID,/valid_id)) then widget_control,info.dispID,/destroy
  disp,im,'Zernike Slider PSF',x0=[-1,-1]*info.n/2
  id = widget_info(base,find_by_uname='TextStrehlOn')
  widget_control,id,set_value=strtrim(string(1.),2)
  id = widget_info(base,find_by_uname='TextStrehlPeak')
  widget_control,id,set_value=strtrim(string(1.),2)
  id = !disp.windows[!disp.wincount-1]
  info.dispID = id
  widget_control,base,set_uvalue=info
  zernikeSlider_Adjust,{widget_slider, id:base, top:base, handler:0L, value:0L, drag:0}
end
pro zernikeSlider_Quit, Event
  print,'<zernikeSlider_Quit>'
  base = Event.top
  widget_control,base,/destroy
end
;-----------------------------------------------------------------
; Slider Value Changed Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_SLIDER, ID:0L, TOP:0L, HANDLER:0L, VALUE:0L, DRAG:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   VALUE returns the new value of the slider. DRAG returns integer 1
;       if the slider event was generated as part of a drag
;       operation, or zero if the event was generated when the user
;       had finished positioning the slider.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro zernikeSlider_Adjust, Event
  base = Event.top
  widget_control,base,get_uvalue=info
  nZernikes = (size(info.sliderList))[1]

  if (widget_info(info.dispID,/valid_id) eq 0) then begin
      zernikeSlider_Redo,{widget_slider, id:base, top:base, handler:0L, value:0L, drag:0}
      return
  endif

  ph = fltarr(info.n,info.n)
  for k=0,nZernikes-1 do begin
      widget_control,info.sliderList[k],get_uvalue=ztab,get_value=sliderValue
      coef = float(sliderValue)/100.
      phc = zernike(info.n,info.n,info.n*info.plateScale,ztab[0],ztab[1])
      if (ztab[2] eq 0) then begin
          ph += (real(phc)/rms(real(phc),*info.pa))*coef
      endif else begin
          ph += (imaginary(phc)/rms(imaginary(phc),*info.pa))*coef
      endelse
  endfor
  i = complex(0.,1.)
  wf = (*info.pa)*exp(i*2*!pi*ph)
  im = abs(ft(wf))
  im = im/info.norm
  id = widget_info(base,find_by_uname='TextStrehlOn')
  widget_control,id,set_value=strtrim(string(im[info.n/2,info.n/2]),2)
  id = widget_info(base,find_by_uname='TextStrehlPeak')
  widget_control,id,set_value=strtrim(string(max(im)),2)
  displayBase = widget_info(info.dispID,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  ptr_free,dataInfo.data
  dataInfo.data = ptr_new(im)
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase

end
;-----------------------------------------------------------------
; Text Insert Character Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TEXT_CH, ID:0L, TOP:0L, HANDLER:0L, TYPE:0, OFFSET:0L,
;       CH:0B }

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   OFFSET is the (zero-based) insertion position that will result
;       after the character is inserted. CH is the ASCII value of the
;       character.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro zernikeSlider_CR, Event
  if (event.ch eq 10) then begin
    zernikeSlider_Redo,Event
  endif
end
