; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	04/14/2006 22:18.21
; 
pro WID_BASE_0_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)


  wWidget =  Event.top

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='WID_BASE_0'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='Textn'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_CH' )then $
        zernikeSlider_CR, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_STR' )then $
        zernikeSlider_Redo, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='TextPlateScale'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_CH' )then $
        zernikeSlider_CR, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TEXT_STR' )then $
        zernikeSlider_Redo, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='ButtonRedo'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        zernikeSlider_Redo, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='ButtonQuit'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        zernikeSlider_Quit, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderFocus20'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderAstig22'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderAstig22m'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderComa31'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderComa31m'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderComa33'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderComa33m'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderSpherical40'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderSpherical42'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderSpherical42m'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderSpherical44'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='SliderSpherical44m'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        zernikeSlider_Adjust, Event
    end
    else:
  endcase

end

pro WID_BASE_0, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_

  Resolve_Routine, 'zernikeSlider_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  WID_BASE_0 = Widget_Base( GROUP_LEADER=wGroup, UNAME='WID_BASE_0'  $
      ,XOFFSET=5 ,YOFFSET=5 ,SCR_XSIZE=1118 ,SCR_YSIZE=600  $
      ,NOTIFY_REALIZE='zernikeSlider_Realize' ,TITLE='IDL' ,SPACE=3  $
      ,XPAD=3 ,YPAD=3)

  
  ZernikeTitle = Widget_Label(WID_BASE_0, UNAME='ZernikeTitle'  $
      ,XOFFSET=472 ,YOFFSET=18 ,SCR_XSIZE=149 ,SCR_YSIZE=23  $
      ,/ALIGN_LEFT ,VALUE='Zernike Control Panel')

  
  ToggleBase = Widget_Base(WID_BASE_0, UNAME='ToggleBase' ,FRAME=1  $
      ,XOFFSET=17 ,YOFFSET=55 ,SCR_XSIZE=179 ,SCR_YSIZE=201  $
      ,TITLE='IDL' ,SPACE=3 ,XPAD=3 ,YPAD=3)

  
  Labeln = Widget_Label(ToggleBase, UNAME='Labeln' ,XOFFSET=36  $
      ,YOFFSET=18 ,/ALIGN_LEFT ,VALUE='n')

  
  LabelPlateScale = Widget_Label(ToggleBase, UNAME='LabelPlateScale'  $
      ,XOFFSET=8 ,YOFFSET=47 ,/ALIGN_LEFT ,VALUE='PlateScale')

  
  LabelLamDperPix = Widget_Label(ToggleBase, UNAME='LabelLamDperPix'  $
      ,XOFFSET=8 ,YOFFSET=70 ,/ALIGN_LEFT ,VALUE='(Lambda/D per'+ $
      ' pixel)')

  
  Textn = Widget_Text(ToggleBase, UNAME='Textn' ,XOFFSET=84  $
      ,YOFFSET=12 ,/EDITABLE ,/ALL_EVENTS  $
      ,VALUE=[ '128' ] ,XSIZE=20 ,YSIZE=1)

  
  TextPlateScale = Widget_Text(ToggleBase, UNAME='TextPlateScale'  $
      ,XOFFSET=84 ,YOFFSET=43  ,/EDITABLE  $
      ,/ALL_EVENTS ,VALUE=[ '0.25' ] ,XSIZE=20 ,YSIZE=1)

  
  LabelStrehlOn = Widget_Label(ToggleBase, UNAME='LabelStrehlOn'  $
      ,XOFFSET=5 ,YOFFSET=97 ,/ALIGN_LEFT ,VALUE='On-Axis Strehl')

  
  TextStrehlOn = Widget_Text(ToggleBase, UNAME='TextStrehlOn'  $
      ,XOFFSET=99 ,YOFFSET=94  ,XSIZE=20  $
      ,YSIZE=1)

  
  ButtonRedo = Widget_Button(ToggleBase, UNAME='ButtonRedo'  $
      ,XOFFSET=57 ,YOFFSET=158 ,/ALIGN_CENTER ,VALUE='ReDo')

  ButtonQuit = Widget_Button(ToggleBase, UNAME='ButtonQuit' $
      ,xoffset=107, yoffset=158, /align_center, value='Quit')
  
  LabelStrehlPeak = Widget_Label(ToggleBase, UNAME='LabelStrehlPeak'  $
      ,XOFFSET=5 ,YOFFSET=126 ,/ALIGN_LEFT ,VALUE='Peak Strehl')

  
  TextStrehlPeak = Widget_Text(ToggleBase, UNAME='TextStrehlPeak'  $
      ,XOFFSET=98 ,YOFFSET=122, XSIZE=20, YSIZE=1)

  
  BaseSliders = Widget_Base(WID_BASE_0, UNAME='BaseSliders' ,FRAME=1  $
      ,XOFFSET=203 ,YOFFSET=55 ,SCR_XSIZE=888 ,SCR_YSIZE=495  $
      ,TITLE='IDL' ,SPACE=3 ,XPAD=3 ,YPAD=3)

  
  SliderFocus20 = Widget_Slider(BaseSliders, UNAME='SliderFocus20'  $
      ,XOFFSET=20 ,YOFFSET=32 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Focus (2,0)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderAstig22 = Widget_Slider(BaseSliders, UNAME='SliderAstig22'  $
      ,XOFFSET=20 ,YOFFSET=89 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Astigmatism (2,2,1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderAstig22m = Widget_Slider(BaseSliders, UNAME='SliderAstig22m'  $
      ,XOFFSET=20 ,YOFFSET=145 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Astigmatism (2,2,-1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  LabelAberation = Widget_Label(BaseSliders, UNAME='LabelAberation'  $
      ,XOFFSET=123 ,YOFFSET=6 ,/ALIGN_LEFT ,VALUE='Aberation, Waves'+ $
      ' RMS')

  
  SliderComa31 = Widget_Slider(BaseSliders, UNAME='SliderComa31'  $
      ,XOFFSET=21 ,YOFFSET=208 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Coma (3,1,1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderComa31m = Widget_Slider(BaseSliders, UNAME='SliderComa31m'  $
      ,XOFFSET=21 ,YOFFSET=281 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Coma (3,1,-1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderComa33 = Widget_Slider(BaseSliders, UNAME='SliderComa33'  $
      ,XOFFSET=22 ,YOFFSET=346 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Coma (3,3,1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderComa33m = Widget_Slider(BaseSliders, UNAME='SliderComa33m'  $
      ,XOFFSET=21 ,YOFFSET=419 ,SCR_XSIZE=393 ,SCR_YSIZE=51  $
      ,TITLE='Coma (3,3,-1)' ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderSpherical40 = Widget_Slider(BaseSliders,  $
      UNAME='SliderSpherical40' ,XOFFSET=445 ,YOFFSET=31  $
      ,SCR_XSIZE=393 ,SCR_YSIZE=51 ,TITLE='Spherical (4,0)'  $
      ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderSpherical42 = Widget_Slider(BaseSliders,  $
      UNAME='SliderSpherical42' ,XOFFSET=445 ,YOFFSET=92  $
      ,SCR_XSIZE=393 ,SCR_YSIZE=51 ,TITLE='Spherical (4,2,1)'  $
      ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderSpherical42m = Widget_Slider(BaseSliders,  $
      UNAME='SliderSpherical42m' ,XOFFSET=445 ,YOFFSET=152  $
      ,SCR_XSIZE=393 ,SCR_YSIZE=51 ,TITLE='Spherical (4,2,-1)'  $
      ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderSpherical44 = Widget_Slider(BaseSliders,  $
      UNAME='SliderSpherical44' ,XOFFSET=445 ,YOFFSET=205  $
      ,SCR_XSIZE=393 ,SCR_YSIZE=51 ,TITLE='Spherical (4,4,1)'  $
      ,MINIMUM=-100 ,MAXIMUM=100)

  
  SliderSpherical44m = Widget_Slider(BaseSliders,  $
      UNAME='SliderSpherical44m' ,XOFFSET=445 ,YOFFSET=279  $
      ,SCR_XSIZE=393 ,SCR_YSIZE=51 ,TITLE='Spherical (4,4,-1)'  $
      ,MINIMUM=-100 ,MAXIMUM=100)

  Widget_Control, /REALIZE, WID_BASE_0

  XManager, 'WID_BASE_0', WID_BASE_0, /NO_BLOCK  

end
; 
; Empty stub procedure used for autoloading.
; 
pro zernikeSlider, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  WID_BASE_0, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
