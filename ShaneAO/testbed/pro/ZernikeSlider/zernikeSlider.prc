HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	04/14/2006 22:18.17
VERSION 1
END

WID_BASE_0 BASE 5 5 1118 600
REALIZE "zernikeSlider_Realize"
TLB
CAPTION "IDL"
XPAD = 3
YPAD = 3
SPACE = 3
BEGIN
  ZernikeTitle LABEL 472 18 149 23
  VALUE "Zernike Control Panel"
  ALIGNLEFT
  END
  ToggleBase BASE 17 55 179 201
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    Labeln LABEL 36 18 0 0
    VALUE "n"
    ALIGNLEFT
    END
    LabelPlateScale LABEL 8 47 0 0
    VALUE "PlateScale"
    ALIGNLEFT
    END
    LabelLamDperPix LABEL 8 70 0 0
    VALUE "(Lambda/D per pixel)"
    ALIGNLEFT
    END
    Textn TEXT 84 12 77 24
    NUMITEMS = 1
    ITEM "128"
    EDITABLE
    ALLEVENTS
    ONINSERTCHAR "zernikeSlider_CR"
    ONINSERTSTRING "zernikeSlider_Redo"
    WIDTH = 20
    HEIGHT = 1
    END
    TextPlateScale TEXT 84 43 80 24
    NUMITEMS = 1
    ITEM "0.25"
    EDITABLE
    ALLEVENTS
    ONINSERTCHAR "zernikeSlider_CR"
    ONINSERTSTRING "zernikeSlider_Redo"
    WIDTH = 20
    HEIGHT = 1
    END
    LabelStrehlOn LABEL 5 97 0 0
    VALUE "On-Axis Strehl"
    ALIGNLEFT
    END
    TextStrehlOn TEXT 99 94 69 24
    WIDTH = 20
    HEIGHT = 1
    END
    ButtonRedo PUSHBUTTON 57 158 0 0
    VALUE "ReDo"
    ALIGNCENTER
    ONACTIVATE "zernikeSlider_Redo"
    END
    LabelStrehlPeak LABEL 5 126 0 0
    VALUE "Peak Strehl"
    ALIGNLEFT
    END
    TextStrehlPeak TEXT 98 122 69 24
    WIDTH = 20
    HEIGHT = 1
    END
  END
  BaseSliders BASE 203 55 888 495
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    SliderFocus20 SLIDER 20 32 393 51
    CAPTION "Focus (2,0)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderAstig22 SLIDER 20 89 393 51
    CAPTION "Astigmatism (2,2,1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderAstig22m SLIDER 20 145 393 51
    CAPTION "Astigmatism (2,2,-1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    LabelAberation LABEL 123 6 0 0
    VALUE "Aberation, Waves RMS"
    ALIGNLEFT
    END
    SliderComa31 SLIDER 21 208 393 51
    CAPTION "Coma (3,1,1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderComa31m SLIDER 21 281 393 51
    CAPTION "Coma (3,1,-1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderComa33 SLIDER 22 346 393 51
    CAPTION "Coma (3,3,1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderComa33m SLIDER 21 419 393 51
    CAPTION "Coma (3,3,-1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderSpherical40 SLIDER 445 31 393 51
    CAPTION "Spherical (4,0)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderSpherical42 SLIDER 445 92 393 51
    CAPTION "Spherical (4,2,1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderSpherical42m SLIDER 445 152 393 51
    CAPTION "Spherical (4,2,-1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderSpherical44 SLIDER 445 205 393 51
    CAPTION "Spherical (4,4,1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
    SliderSpherical44m SLIDER 445 279 393 51
    CAPTION "Spherical (4,4,-1)"
    MINIMUM = -100
    MAXIMUM = 100
    ONVALUECHANGED "zernikeSlider_Adjust"
    END
  END
END
