;+
;  psf_mtf_ao.pro
;    calculates mean mtf and psf of AO corrected image
;-

n = 1024; 1024 gives ~17% error, 2048 ~5.5% error in integration
calcGeneric = 1
if (n_elements(startPlot) eq 0) then startPlot = 1

;  f is a normalized spatial frequency.  f=1 at the spatial
;   frequency cut-off (1/2d)
;  u is a normalized pupil coordinate.  u=1 at one sample spacing (d)

;  -- calculate Dpsi --
if (calcGeneric) then begin
umax = 64.; 100. ; max number of subaps across
du = 2*umax/float(n)
df = 1.0/(float(n)*du)
fx = (ones(n) ## findgen(n)-n/2)*df
fy = transpose(fx)
f = sqrt(fx^2+fy^2)
Sphi = 0.023*f^(-11./3.)
Sphi[n/2,n/2] = 0.
Cphi = real(ft(Sphi,/inverse))*df^2
Dphi = 2*(Cphi[n/2,n/2] - Cphi)
F0 = float(f gt 1); float((abs(fx) gt 1) or (abs(fy) gt 1))
Spsi = Sphi*F0
Cpsi = real(ft(Spsi,/inverse))*df^2
Dpsi = 2*(Cpsi[n/2,n/2] - Cpsi)
print,abs(max(Cpsi)/(.023*2*!pi*(3./5.)) - 1.0); percent error in the integration
endif
; -- parameters in physical units --
Dtele = 30.
r0 = 1.0
d = Dtele/umax; 2.0; 1.0; 0.3
fc = 1/(2*d)
;
;  -- calculate tau --
ux = (ones(n) ## findgen(n)-n/2)*du*d/Dtele
uy = transpose(ux)
u = sqrt(ux^2+uy^2)<1.0
tau0 = (2.0/!pi)*(acos(u)-u*sqrt(1.-u^2))
;
;taua0 = exp(-(r0*fc)^(-5./3.)*0.5*Dphi)
taua = exp(-(r0*fc)^(-5./3.)*0.5*Dpsi)
tau = tau0*taua
;
if (startPlot eq 1) then begin
  plot,ravg(u),ravg(tau0),/xlog,xrange=[u[n/2,n/2+1],1.]
  startPlot = 0
endif
oplot,ravg(u),ravg(tau),color=150
;
;  -- calculate psf --
psf = real(ft(tau))
end
