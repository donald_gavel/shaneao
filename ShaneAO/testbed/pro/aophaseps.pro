;+
;  aoPhasePs.pro
;    compute the phase power spectrum given Lick AO system diagnostic data
;
;  usage:
;    ps = aoPhasePs(data,cm,fs,/lgs) ; compute the power spectrum
;    e = ise(ps,fs)  ;  integrate the power spectrum
;
;  inputs:
;    data - centroid diagnostic data (81, dataLength)
;       (note: 81st data point is state of loop)
;    cm - control matrix (80x61)
;    fs - sample rate, Hz
;
;  input flags:
;    lgs -- 1 = laser guide star, 0 = natural guide star
;
;  outputs:
;    ps - phase power spectrum (dataLength), units = microns^2/Hz
;
;  verify proper scaling with Parseval's theorem:
;      sqrt(total(ps(0,*))*df) eq sqrt(total(data(0:39,*)^2)/(40.*N))
;      where df=fs/N
;-
function aoPhasePs, data, cm, fs, lgs=lgs
;
    if (n_elements(lgs) eq 0) then lgs = 0
;
;         k indicates the actuators inside the aperture
    k = [6,7,8,9,12,13,14,15,16,19,20,21,22,23,24,27, $
         28,29,31,32,33,35,36,37,38,39,40,41,44,45,46,47,48,51,52,53,54]
;
;  magicScaleFactor was computed assuming a "hex-tent"
;    influence function and comparing to the measured response function
;  arcsecPerPixel was determined by ray trace analysis of the
;    Hartmann sensor optics
;
    if (keyword_set(lgs)) then begin
      magicScaleFactor = 1.45
      arcsecPerPixel = 1.93
    endif else begin
      magicScaleFactor = 1.45
      arcsecPerPixel = 1.0
    endelse
    cmi = cm[*,k]*(arcsecPerPixel/magicScaleFactor);
    b = transpose(data(0:79,*))
    n = (size(b))(1) ;  number of data points
    m = (size(cmi))(2) ;  number of actuators
;    a = cmi##b
    a = fltarr(n,m)
    print,"computing ",m," phase points"
    for i=0,m-1 do begin
      a(*,i) = cmi[*,i]##b
      print,format='($,I0," ")',i
    endfor
    print,"done"
;    a = a/1e-3;  to scale from microns to nanometers
    ;  take the power spectra of phases
    n = (size(a))(1)
    m = (size(a))(2)
    dt = 1./fs
    df = fs/float(n)
    fa = complexarr(m,n)
    ;  Fourier transform the data
    for i=0,m-1 do begin
      fa(i,*) = n*fft(a(*,i))*dt
    endfor
    ;  Average the squared transforms
    ps = fltarr(n)
    for i=0,m-1 do begin
      ps = ps + real(fa(i,*)*conj(fa(i,*)))
    endfor
    ;  Scale so result is in units(data)^2/hz
    ps = ps/(float(m)*float(n)*dt)
    return, ps
end

function ise,ps,fs
  n = (size(ps))(1)
  df = fs/float(n)
  r = total(ps*df)
  return,r
end

;-----------------------------------
;  example requires powerSpectrumPlot.pro
pro aoPhasePsExample
  matdir = '/duck3/gavel/lickdataNov98/lgs6/Matrices/'
  cm = rdview(matdir + 'CMNov05_02')
  datadir = '/duck3/gavel/lickdataNov98/Nov13/'
  fs = 500.
  cent = 'cent_34'
  print, 'open loop data'
  data = rdview(datadir+cent)
  pso = aoPhasePs(data,cm,fs)
  powerSpectrumPlot, pso,fs,title='Phase, nm^2/Hz'
  print, 'closed loop data'
  cent = 'cent_33'
  data = rdview(datadir+cent)
  ps = aoPhasePs(data,cm,fs)
  powerSpectrumPlot, ps,fs,color=50,/oplot
end
