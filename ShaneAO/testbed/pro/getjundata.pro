;
;  get jundata.pro
;

pro getjundata, dataset,rawdata, darkdata, rawmdark, aveDark
  print,"reading in raw data"
  rawdata = readfits(dataset.datadir + dataset.datafile)
  nx = (size(rawdata))(1)
  ny = (size(rawdata))(2)
  n = (size(rawdata))(3)
  dfn = fix(dataset.darkfile)
  darkdata = make_array(nx,ny,8)
  print,"reading in dark data"
  darkdata(*,*,0) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+0) + ".fits")
  darkdata(*,*,1) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+1) + ".fits")
  darkdata(*,*,2) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+2) + ".fits")
  darkdata(*,*,3) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+3) + ".fits")
  darkdata(*,*,4) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+5) + ".fits")
  darkdata(*,*,5) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+6) + ".fits")
  darkdata(*,*,6) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+7) + ".fits")
  darkdata(*,*,7) = readfits(dataset.datadir + "s00" + string(format='(I3)',dfn+8) + ".fits")
  print,'averaging dark frame'
  aveDark = total(darkdata,3)/8
  print,'subtracting average dark'
  rawmdark = make_array(nx,ny,n)
  for i=0,n-1 do rawmdark(*,*,i) = rawdata(*,*,i) - aveDark
  return
end

function getjunsky,dataset,badpix,sky,skyDark
  print,'reading sky data'
  sky = readfits(dataset.datadir + dataset.skyfile)
  nx = (size(sky))(1)
  ny = (size(sky))(2)
  n = (size(sky))(3)
  dfn = fix(dataset.skydarkfile)
  skyDark = make_array(nx,ny,8)
  print,'reading sky dark data'
  skyDark(*,*,0) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+0) + ".fits")
  skyDark(*,*,1) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+1) + ".fits")
  skyDark(*,*,2) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+2) + ".fits")
  skyDark(*,*,3) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+3) + ".fits")
  skyDark(*,*,4) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+5) + ".fits")
  skyDark(*,*,5) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+6) + ".fits")
  skyDark(*,*,6) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+7) + ".fits")
  skyDark(*,*,7) = readfits(dataset.datadir + 's00' + string(format='(I3)',dfn+8) + ".fits")
  print,'averaging frames'
  aveskydark = total(skyDark,3)/8.
  skyframe = total(sky,3)/100. - aveskydark;  average over the frames
  skyframe = skyframe*(1-badpix) + median(skyframe,5)*badpix
  br = total(skyframe,2)/256;   remove the vertical stripes
  brv = total(br)/256;
  bra = make_array(256,256)
  for i=0,255,1 do bra(*,i) = br
  skyframe = skyframe - bra + brv
  return, skyframe
end
