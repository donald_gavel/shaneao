;Written on 21 November 2001 by Lisa Poyneer at LLNL


;this method, instead of first fixing the spatial periodicity
;issue, fixes the boundaries at zero and sets the
;rest (and square edges) to work....

;@wrview
;@continuity_tester
PRO extend_grid, subapmask, gx, gy



tpath = '/d10/poyneer/WFC-FT/Extend/Temp1/'
dims = size(gx)
N = dims[1]
Nsq = N^2

gx = gx*subapmask
gy = gy*subapmask

xin = subapmask
yin = subapmask
xinsh = shift(xin, 0, -1)
yinsh = shift(yin, -1, 0)

loops1 = xin + yin + xinsh + yinsh

;for every 3 entry, set that gradient based on the others.....

FOR k=0, N-1 DO BEGIN
	FOR l=0, N-1 DO BEGIN
		IF loops1[k,l] EQ 3 THEN BEGIN
			;Print, 'KEG: found a three-loop at: ', k, l
			;find the missing grad and set
			flag = 0
			sum = 0
			IF xin[k,l] EQ 0 THEN BEGIN
				flag = 1
			ENDIF ELSE BEGIN
				sum = sum + gx[k,l]
			ENDELSE

			IF xinsh[k,l] EQ 0 THEN BEGIN
				flag = 2
			ENDIF ELSE BEGIN
				sum = sum - gx[k,l+1]
			ENDELSE
			
			IF yin[k,l] EQ 0 THEN BEGIN
				flag = 3
			ENDIF ELSE BEGIN
				sum = sum - gy[k,l]
			ENDELSE
			
			IF yinsh[k,l] EQ 0 THEN BEGIN
				flag = 4
			ENDIF ELSE BEGIN
				sum = sum + gy[k+1,l]
			ENDELSE
			CASE flag OF
				1: BEGIN
				;Print, 'in extend_center set x grad at', k, l
				gx[k,l] = -sum
				xin[k,l] = 1
				END			
				2: BEGIN
				;Print, 'in extend_center set x grad at', k, l+1
				gx[k,l+1] = sum
				xin[k,l+1] = 1
				END			
				3: BEGIN
				;Print, 'in extend_center set y grad at', k, l
				gy[k,l] = sum
				yin[k,l] = 1
				END			
				4: BEGIN
				;Print, 'in extend_center set y grad at', k+1, l
				gy[k+1,l] = -sum
				yin[k+1, l] = 1
				END			
			ENDCASE
		ENDIF
	ENDFOR
ENDFOR

xout = 1-xin
yout = 1-yin

;now find top, bpt, left, right
xtop0 = xin + shift(xin, 0, 1)
yleft0 = yin + shift(yin, 1, 0)
xbot0 = xin + shift(xin, 0, -1)
yright0 = yin + shift(yin, -1, 0)
xtop = fltarr(N,N)
yleft = fltarr(N,N)
xbot = fltarr(N,N)
yright = fltarr(N,N)

FOR k=0, N-1 DO BEGIN
	;find first 1 in each column (going down) of xtop
	;find first 1 in row (going right) in yleft
	;find last 1 in each column of xbot
	;find last 1 in each rw of yright
		found1 = 0
		found2 = 0
		found3 = 0
		found4 = 0
	FOR l=0, N-1 DO BEGIN

		l2 = N-1-l

		IF found1 EQ 0 THEN BEGIN		
			IF xtop0[k,l] EQ 1 THEN BEGIN
				xtop[k,l] = 1
				found1 = 1
			ENDIF
		ENDIF
		IF found2 EQ 0 THEN BEGIN		
			IF xbot0[k,l2] EQ 1 THEN BEGIN
				xbot[k,l2] = 1
				found2 = 1
			ENDIF
		ENDIF
		IF found3 EQ 0 THEN BEGIN		
			IF yleft0[l,k] EQ 1 THEN BEGIN
				yleft[l,k] = 1
				found3 = 1
			ENDIF
		ENDIF
		IF found4 EQ 0 THEN BEGIN		
			IF yright0[l2,k] EQ 1 THEN BEGIN
				yright[l2,k] = 1
				found4 = 1
			ENDIF
		ENDIF		
	ENDFOR
ENDFOR

;now take thse values and propagate out through whole grid.

FOR k=0, N-1 DO BEGIN
	toploc = where(xtop[k,*] EQ 1, is_top)
	IF is_top EQ 1 THEN BEGIN
		val = gx[k,toploc[0]]
		FOR l=0, toploc[0]-1 DO BEGIN
			gx[k,l] = val
			xin[k,l] = 1
		ENDFOR		
	ENDIF
	botloc = where(xbot[k,*] EQ 1, is_bot)
	IF is_bot EQ 1 THEN BEGIN
		val = gx[k,botloc[0]]
		FOR l=botloc[0]+1, N-1 DO BEGIN
			gx[k,l] = val
			xin[k,l] = 1
		ENDFOR		
	ENDIF
	leftloc = where(yleft[*,k] EQ 1, is_left)
	IF is_left EQ 1 THEN BEGIN
		val = gy[leftloc[0], k]
		FOR l=0, leftloc[0]-1 DO BEGIN
			gy[l,k] = val
			yin[l,k] = 1
		ENDFOR		
	ENDIF
	rightloc = where(yright[*,k] EQ 1, is_right)
	IF is_right EQ 1 THEN BEGIN
		val = gy[rightloc[0], k]
		FOR l=rightloc[0]+1, N-1 DO BEGIN
			gy[l,k] = val
			yin[l,k] = 1
		ENDFOR		
	ENDIF

ENDFOR

;now need to fix the seams due to spatial periodicity....

FOR k = 0, N-1 DO BEGIN
	gx[N-1, k] = -Total(gx[0:N-2, k])
	gy[k,N-1] = -Total(gy[k, 0:N-2])
ENDFOR

END

