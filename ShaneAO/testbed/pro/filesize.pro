;
;  filesize.pro - determine a file's size in bytes
;
function filesize,file
  openr,1,file
  s = fstat(1)
  close,1
  return,s.size
end
