;+
;  NaNreport.pro
;    Report on the positions of NANs and non-finite values
;
;  input: array a of any type and dimension
;  outputs:
;       inda = multidimensional indices of non-finite values
;       indw = one dimensional indices of non-finite values
;-
pro NaNreport,a,inda,indw,verbose=verbose
  u = finite(a)
  indw = where(1-u)
  if (n_elements(indw) eq 1) then begin
  	if (indw eq -1) then begin
  	  if (keyword_set(verbose)) then print,'<NaNreport> all entries are finite'
  	  inda = -1
  	  return
  	endif
  endif
  inda = array_indices(a,indw)
  if (keyword_set(verbose)) then print,'<NaNreport> found ',strtrim(n_elements(indw),2),' non-finite values'
end
