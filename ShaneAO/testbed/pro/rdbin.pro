; rdbin.pro - read a binary file

function read_binary, filename, n
  openr,unit,filename,/get_lun
  t = assoc(unit,make_array(n,/byte))
  result = t(0)
  free_lun, unit
  return,result
end
