;+
;  makeRefPS.pro - create a simulated PSF reference power spectrum
;
;   refps = makeRefPS(n,D/r0,diflim)
;        where diflim = (D/lambda)*n*dtheta
;                   n = size of the image
;              dtheta = sample spacing on image
;              lambda = wavelength
;                   D = telescope diameter
;                  r0 = seeing cell size (at this wavelength)
;                       (Scale as (lambda/lambda0)^(6/5) for
;                       r0 given at lamda0)
;
;-
function makeRefPS,n,DoverR0,diflim
  if (n_params() ne 3) then begin
    print,'usage: makeRefPS, n, DoverR0, diflim'
    return,0
  endif
  r0 = 1/DoverR0
  ny = n
  nx = ny/2 + 1
  x = make_array(1,ny,/float,value=1) ## (indgen(nx) - nx + 1)/diflim
  y = ((indgen(ny) - ny/2)/diflim) ## make_array(nx,1,/float,value=1)
  r = sqrt(x^2+y^2)
  u = r * (r lt 1)
  ui = sqrt(1-u^2)
  uc = complex(u,ui)
  theta = angle(uc)
  mtf0 = (2/!pi)*(theta - u*ui)
  dphise = 3.44*(u/r0)^(5./3.) * (1 - u^(1./3.))
  mtfse = exp(-dphise)
  lo = (mtf0 * mtfse)^2
  fuzz = 1e-5
  hi = mtf0 * r0^2 * (0.435 + 0.278*(u/r0+fuzz)^(-1./3.))
  refps = lo + hi
  return,refps
end

