dir = "c:/dad's/LaserGuideStar/PhaseDiversityTestData/"
b = readfits(dir + 'ircal0095.fits')
a = readfits(dir+'ircal0103.fits')
image = (a-b)[108-64:108+63,130-64:130+63]
image=(image-20000)>0
dp = 3
ds = 0.8
lam = 2.16
dtheta = 0.0756
defocus = -1.4
spider = 3
n = (size(image))[1]
ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
ap[*,(n/2-spider/2):(n/2+spider/2)] = 0
ap[(n/2-spider/2):(n/2+spider/2),*] = 0
ap = rot(ap,45)
ph = phasediversity(image,dp,ds,lam,dtheta,defocus,10,magsim,spider=3)
ph = ph-ph0
i = complex(0,1.)
lamhart = 0.7
wf = float(ap)*exp(i*ph*lam/lamhart)
wf0 = complex(ap)
x = (findgen(n)-n/2)*du # ones(n)
y = transpose(x)
hartspotsize = 0.8*arcsec
wf1 = float(ap)*exp(i*(2*!pi/(lamhart*microns))*x*hartspotsize/10.)
simpixscale = (lamhart*microns)/(32*du)
blursize = hartspotsize/simpixscale
sl = lickwfs()
t0 = hartmannFFT(float(ap),wf0,sl*d/du,d/du,wfs0,blursize=blursize)
;t1 = hartmannFFT(float(ap),wf1,sl*d/du,d/du,wfs1,blursize=blursize)
t = hartmannFFT(float(ap),wf,sl*d/du,d/du,wfs,blursize=blursize)
;c = (t1-t0)*simpixscale/hartspotsize
c = (t-t0)*simpixscale/hartspotsize
wset,0
hartmannDisplay,c,sl,scale=1./max(abs(c))
dispgui,angle(wf)*ap
wset,0
end
