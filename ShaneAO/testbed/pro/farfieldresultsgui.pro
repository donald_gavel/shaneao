;
; Auto Save File For ./farfieldresultsgui.pro
;
;  Mon Sep 29 22:47:36 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN1




PRO MAIN1_Event, Event


  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  CASE Ev OF 

  'DRAW6': BEGIN
      Print, 'Event for DRAW6'
      END
  'DRAW9': BEGIN
      Print, 'Event for DRAW9'
      END
  'BUTTON18': BEGIN
      Print, 'Event for Done'
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN1
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO farfieldresultsgui, GROUP=Group


  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN1 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='MAIN1')

  LABEL2 = WIDGET_LABEL( MAIN1, $
      UVALUE='LABEL2', $
      VALUE='Far Field Results')

  BASE3 = WIDGET_BASE(MAIN1, $
      ROW=1, $
      MAP=1, $
      TITLE='2 images', $
      UVALUE='BASE3')

  BASE4 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      MAP=1, $
      TITLE='far field, no atmos', $
      UVALUE='BASE4')

  LABEL5 = WIDGET_LABEL( BASE4, $
      UVALUE='LABEL5', $
      VALUE='Far Field Image, no atmosphere')

  DRAW6 = WIDGET_DRAW( BASE4, $
      RETAIN=0, $
      UVALUE='DRAW6')


  BASE7 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      MAP=1, $
      TITLE='far field with atmos', $
      UVALUE='BASE7')

  LABEL8 = WIDGET_LABEL( BASE7, $
      UVALUE='LABEL8', $
      VALUE='Far Field, 2-way atmosphere')

  DRAW9 = WIDGET_DRAW( BASE7, $
      RETAIN=0, $
      UVALUE='DRAW9')



  BUTTON18 = WIDGET_BUTTON( MAIN1, $
      UVALUE='BUTTON18', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN1, /REALIZE

  ; Get drawable window index

  COMMON DRAW6_Comm, DRAW6_Id
  WIDGET_CONTROL, DRAW6, GET_VALUE=DRAW6_Id

  ; Get drawable window index

  COMMON DRAW9_Comm, DRAW9_Id
  WIDGET_CONTROL, DRAW9, GET_VALUE=DRAW9_Id

  XMANAGER, 'MAIN1', MAIN1
END
