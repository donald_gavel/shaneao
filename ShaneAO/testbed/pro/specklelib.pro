;+
; specklelib - setup for speckle gui interface to speckle processor
;
;   assumed directory structure, file name and type conventions:
;     !home
;        bin                 -- contains the frame.exe executable code
;        pro                 -- contains IDL codes
;          default.database      -  default gui form entries for each dataset
;     (datadir)
;     (resultsdir)
;        [objectname].spec          (speckle format file)
;        [objectname]_ps.sdt        (view format file)
;        [objectname]_phase.sdt     (view format file)
;        [objectname]_image.fits    (FITS file)
;        [starname].spec            (speckle format file)
;        [starname]_ps.sdt          (view format file)
;        [starname]_phase.sdt       (view format file)
;     (mtffile)                     (FITS file)
;     (badpixelfile)                (FITS file)
;-
devicelib
imagelib
defsysv,'!home','~/'
message,'Added system variable !HOME',/informational
.compile specklegui
.compile specklegui2
.compile specklelistgui
.compile titanDataset2
.compile gettitandata2
.compile debad2
.compile dispgui3
.compile speckleOptionsGUI
