;
;  lucy.pro - Lucy/Richardson restoration
;
;  written by: Don Gavel, L-258, LLNL, gavel1@llnl.gov, Dec, 1998
;    based on Lucy, Astron. J. 79, 1974, p.745
;    translated from the View macro written by Jim Brase, LLNL
;
;  usage:
;   est = lucy(im,psf,n,err)
;  where
;   im -- image
;   psf -- psf
;   n -- number of iterations
;   err -- measurement error signal
;
function lucy,im,psf,niter,err,initial=f0
  if (n_elements(f0) eq 0) then begin
    print,'starting with 1st arg as inital estimate'
    f = real(im)
  endif else begin
    f = f0
  endelse
  g = real(im)
  h = real(psf)
  n = (size(f))(1)
  hn = h/total(h)
  ims = smooth(im,3)
  imhigh = im-ims
  r = moment(imhigh)
  fhm = r[0]
  fhvar = r[1]
  print,'Noise estimate: ',fhvar
  err = fltarr(niter)
  lastIter = niter-1
  for i = 0,lastIter do begin
    f1 = real(ftconvolve(f,hn))
    err(i) = total((f1-im)^2)/n^2
    if (err(i) lt fhvar) then print,'Residual less than  noise variance, suggest stopping'
;    print,format='($,I0," ")',i
    print,'iteration ',i,' error = ',err(i)
;    f2 = g / f1
    f2 = g / (abs(f1)+0.1)
    f3 = real(ftconvolve(f2,hn))
    f = f*f3
    f = f>0 ; force answer to be > 0
  endfor
  print,'done'
  return,f
end
