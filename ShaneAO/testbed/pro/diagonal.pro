;+
;  diagonal - make a diagnoal matrix, or extract the diagonal of
;             a matrix
;
;  USAGE:
;    a = diagonal(v)  or
;    v = diagonal(a)
;
;  INPUTS:
;    a - square matrix
;    v - vector
;
;  OUTPUT:
;    a - square diagonal matrix with elements of v along the diagonal
;    v - vector containing diagonal elements of a
;-
function diagonal,v
  s = size(v)
  dimension = s[0]
  n = s(1)
  if (dimension eq 1) then begin
    type = s[2]
    case type of
    9: a = dcomplexarr(n,n)
    6: a = complexarr(n,n)
    5: a = dblarr(n,n)
    4: a = fltarr(n,n)
    else: a = fltarr(n,n)
    endcase
    for i=0,n-1 do a[i,i] = v[i]
  endif else begin
    type = s[3]
    case type of
    4: a = fltarr(n)
    5: a = dblarr(n)
    6: a = complexarr(n)
    9: a = dcomplexarr(n)
    else: a = fltarr(n)
    endcase
    for i=0,n-1 do a[i] = v[i,i]
  endelse
  return,a
end