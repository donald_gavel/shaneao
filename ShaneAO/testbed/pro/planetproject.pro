;+
;   planetproject.pro - project an image of a planet globe
;         given the data from a latitude-longitude grid
;         (see planetmap.pro).  This is useful for "observing" the
;         planet from another vantage point.
;
;   USAGE:
;     image = planetproject(map,n,cx,cy,rx,ry,subEarthLat,subEarthLong)
;
;   INPUTS:
;     map = a latitude/longitude map of the planet
;     n = desired dimension of the output image
;     cx,cy = center of planet on image  (usually n/2)
;     rx,ry = planet major and minor radii, in pixels
;       (as it would appear on the output image if the subEarthLat were zero)
;     subEarthLat, subEarthLong = desired sub-observer point, in degrees
;
;   OUTPUT:
;     image = the projected image of the planet
;
;   NOTES:
;     Choose n, the desired dimension of the output image, in consideration
;     of the sample spacing on the latitude/longitude map.  The finest
;     latitude and longitude spacing as seen on the output image should be less
;     than or equal to the grid spacing on the lat/long map, otherwise
;     patterns of drop-out points may appear on the output image (this is
;     a bug that should be fixed in a later version).
;
;     See the web page http://www-igpp.llnl.gov/people/gavel/jupiter.html
;     (second image) for an example of a reprojected planet image using this
;     algorithm. It is a 3-color combination of data
;     from the Jupiter/SL-9 impact, recentered on one of the comet
;     fragment impact points.
;
;   AUTHOR:
;     Don Gavel, LLNL
;-
function planetproject, map, n, cx, cy, rx, ry, subEarthLat, subEarthLong
  mlong = (size(map))(1)
  mlat = (size(map))(2)
  image = fltarr(n,n)
  degrees = !pi/180.
  dlat = (180./mlat)*degrees
  dlong = (360./mlong)*degrees
  alpha = subEarthLat * degrees
  beta = subEarthLong * degrees
  long = indgen(mlong)*dlong - !pi
  lat = indgen(mlat)*dlat - !pi/2
  sinlat = sin(lat)
  coslat = cos(lat)
  sinlong = sin(long)
  coslong = cos(long)
  r = sqrt( (coslat/rx)^2 + (sinlat/ry)^2 )^(-1)
  x = transpose(r*coslat)##sinlong
  y = extend(r*sinlat,1,mlong)
  z = transpose(r*coslat)##coslong
  xprime = x
  yprime = y*cos(alpha) - z*sin(alpha)
  zprime = y*sin(alpha) + z*cos(alpha)
  xobs = xprime + cx
  yobs = yprime + cy
  zobs = zprime
  i = where((xobs ge 0) and (xobs lt n) and $
            (yobs ge 0) and (yobs lt n) and $
            (zobs ge 0))
  image(xobs(i),yobs(i)) = (shift(map,fix(-beta/dlong),0))(i)
  return, image
end


  
  