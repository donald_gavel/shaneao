; testfftw.pro
;
n_loop = 100
n = 512
a = (randomn(seed,n,n))

; --------- planning -----------
t0 = systime(1)
print,'planning ',n,' by ',n & wait,.01
dum = a
plan = fftw(dum,/plan)
t1 = systime(1)
print,'took ',t1-t0,' seconds' & wait,.01

; --------- fftw -------------
print,' iterations (',n_loop,')' & wait,.01
t0 = systime(1)
for iter = 0,n_loop-1 do begin
  tf = fftw(a,plan)
;  print,iter+1 & wait,.01
endfor
t1 = systime(1)
print,' iterations of fftw took ',t1-t0,' seconds ',(t1-t0)/n_loop,' per iteration'
dt_fftw = t1-t0

; ----------- ft -----------
;a = float(a)
print,' iterations (',n_loop,')' & wait,.01
t0 = systime(1)
for iter = 0,n_loop-1 do begin
  tf = fft(a)
;  print,iter+1 & wait,.01
endfor
t1 = systime(1)
print,' iterations of ft took ',t1-t0,' seconds ',(t1-t0)/n_loop,' per iteration'
dt_fft = t1-t0

print,' speedup ratio: ',dt_fft/dt_fftw

end
