image = fltarr(128,128)
lam = 2.61
dtheta = 0.0756
dp = 3
ds = 0.8
;
n = (size(image))(1)
microns = 1.0e-6
arcsec = !pi/(180.*3600.)
lambda = lam*microns
dth = dtheta*arcsec
du = lambda/(n*dth);  sample size on aperture
rp = dp/2.;	 radius of primary
rs = ds/2.;	 radius of secondary
ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
;
d = 0.43
sl = lickwfs()
i = complex(0,1.)
wf0 = complex(ap)
x = (findgen(n)-n/2)*!pi/float(n) # ones(n)
y = transpose(x)
wf1 = float(ap)*exp(i*x)
t0 = hartmannFFT(float(ap),wf0,sl*d/du,d/du,wfs0,blursize=10)
t1 = hartmannFFT(float(ap),wf1,sl*d/du,d/du,wfs1,blursize=10)
hartmannDisplay,t1-t0,sl,scale=1.
end
