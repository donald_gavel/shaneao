;+
;  overlapIntegral - integrate the product of two images
;                  in their overlapping area
;
; usage:
;   r = overlapIntegral(a,b,ca,cb)
;
; inputs:
;   a, b - images of any dimension
;   ca, cb - coordinates of the center of a, b respectively (2-vectors)
;
; output:
;   r - integral of a*b
;  
;-
function overlapintegral,a,b,ca,cb
  na = (size(a))(1)
  ma = (size(a))(2)
  xrangea = [ca[0]-na/2,ca[0]-na/2+na-1]
  yrangea = [ca[1]-ma/2,ca[1]-ma/2+ma-1]
  nb = (size(b))(1)
  mb = (size(b))(2)
  xrangeb = [cb[0]-nb/2,cb[0]-nb/2+nb-1]
  yrangeb = [cb[1]-mb/2,cb[1]-mb/2+mb-1]
  xrange = [max([xrangea[0],xrangeb[0]]),min([xrangea[1],xrangeb[1]])]
  yrange = [max([yrangea[0],yrangeb[0]]),min([yrangea[1],yrangeb[1]])]
  if ((xrange[0] gt xrange[1]) or (yrange[0] gt yrange[1])) then return,0
  xlapa = xrange - ca[0] + na/2
  ylapa = yrange - ca[1] + ma/2
  xlapb = xrange - cb[0] + nb/2
  ylapb = yrange - cb[1] + mb/2
  r = total(a[xlapa[0]:xlapa[1],ylapa[0]:ylapa[1]]*b[xlapb[0]:xlapb[1],ylapb[0]:ylapb[1]])
  return,r
end
;
function overlapmask,a,ca,mask
  na = (size(a))(1)
  ma = (size(a))(2)
  xrangea = [ca[0]-na/2,ca[0]-na/2+na-1]
  yrangea = [ca[1]-ma/2,ca[1]-ma/2+ma-1]
  r = a*mask[xrangea[0]:xrangea[1],yrangea[0]:yrangea[1]]
  return,r
end
