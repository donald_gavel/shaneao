;
;  ftconvolve.pro - convolve 2 images using the fast-Fourier-transform
;
;  usage:
;    res = ftconvolve(im1,im2,domain=opt)
;
;  inputs:
;    im1, im2 = input images
;    domain = 3-vector of logicals delineating special cases for domains:
;      opt[0] = im1 is already in the transform domain
;      opt[1] = im2 is already in the transform domain
;      opt[2] = the result is to be returned in the transform domain
;
function ftconvolve,im1,im2,domain=opt
    dim = (size(im1))[0]
    n = (size(im1))[1]
    m = (size(im1))[2]
    if (dim eq 1) then m = 1
    if (n_elements(opt) eq 0) then opt = [0,0,0]
    if ((opt[0] eq 0) and (opt[1] eq 0)) then begin
    	fim1 = ft(im1)
    	fim2 = ft(im2)
    	fres = fim1*fim2
    endif
    if ((opt[0] eq 0) and (opt[1] eq 1)) then begin
    	fim1 = ft(im1)
    	fres = fim1*im2
		endif
    if ((opt[0] eq 1) and (opt[1] eq 0)) then begin
    	fim2 = ft(im2)
    	fres = im1*fim2
		endif
    if ((opt[0] eq 1) and (opt[1] eq 1)) then begin
    	fres = im1*im2
    endif
    if (opt[2] eq 1) then return,fres
    res = ft(fres,/inverse)*float(n)*float(m)
    if (dim eq 1) then res = shift(res,-n/2)
    return,res
end
