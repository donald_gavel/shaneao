;+
;  coords.pro - determine the x,y coordinate of a 2-D dataset
;-

function coords,a,n
  nx = (size(a))[1]
  y = fix(real(n)/real(nx))
  x = n - y*nx
  return,[x,y]
end
