; testfftw.pro
;
n_loop = 250
n = 256
a = dcomplex(randomn(seed,n,n,/double))
t0 = systime(1)
print,'planning' & wait,.01
dum = a
rv = fftw(dum,/plan)
t1 = systime(1)
print,'took ',t1-t0,' seconds' & wait,.01
print,' iterations (',n_loop,')' & wait,.01
t0 = systime(1)
for iter = 0,n_loop-1 do begin
  tf = fftw(a)
;  print,iter+1 & wait,.01
endfor
t1 = systime(1)
print,' iterations took ',t1-t0,' seconds'
end
