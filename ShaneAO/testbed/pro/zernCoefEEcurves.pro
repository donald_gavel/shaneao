;
; Form enclosed energy curves vs zernike mode coefficient
;    supporting Jerry Nelson's analysis
;
;----------

pro showEE,nn,r0,n,m,a,oplot=oplot,psfc=fp,psfz=fpzr,phz=ph
;
;  nn: size of grid
;  n,m: zernike index (Noll's notation)
;  a: amplitude of aberation, in waves
;
;  fp: focal plane image
;  fpzr: focal plane image of zernike only, resampled on atmosphere grid
;  ph: zernike phase at aperture plane
;
  waves = a/(2*!pi)
  subsamp = 4*sqrt(1/waves)
  ;subsamp = 1.
  subsampAtmos = 50
  lambda = 1.0
  D = 1.0
  i = complex(0.,1.)
  if (n_elements(a) gt 1) then begin
    a_set = a
    na = (size(a_set))[1]
    if (arg_present(fp)) then fp = fltarr(nn,nn,na)
    if (arg_present(fpzr)) then fpzr = fltarr(nn,nn,na)
    if (arg_present(ph)) then ph = fltarr(nn,nn,na)
    for k = 0,na-1 do begin
      print,'k = ',strtrim(k,2),' a = ',strtrim(a_set[k],2) & wait,.1
      if (k eq 0) then op = 0 else op = 1
      showEE,nn,r0,n,m,a_set[k],oplot=op,psfc=fp_k,psfz=fpzr_k,phz=ph_k
      empty
      if (arg_present(fp)) then fp[*,*,k] = fp_k
      if (arg_present(fpzr)) then fpzr[*,*,k] = fpzr_k
      if (arg_present(ph)) then ph[*,*,k] = ph_k
    endfor
    return
  endif
    
  ; zernike aberration
  r = nn/(2*subsamp)
  du= D/(2*r)
  ap = circle(nn,nn,nn/2,nn/2,r,1.)
  dtheta = lambda / (nn*du)
  rset = findgen(nn/2)*dtheta
  z = zernike(nn,nn,r,n,m,/noll)
  z /= rms(z,ap)
  ph = a*z*ap
  wf = ap*exp(i*ph)
  fpz = abs(ft(wf))
  
  ; atmosphere aberration
  dthetaa = (lambda / r0) / subsampAtmos ; size of pixel in atmos PSF, units of D
  rseta = findgen(nn/2)*dthetaa
  dua = lambda/(nn*dthetaa) ; size of pixel in atmos MTF, units of lambda/D
  fpA = atmopsf(nn,dthetaa,lambda,r0)
  
  ; convolve the two
  x = (findgen(nn)-nn/2)*(du/dua)+nn/2
  y = x
  fpzr = interpolate(fpz,x,y,/grid,cubic=-0.5)
  mask = circle(nn,nn,nn/2,nn/2,(nn/2)*(dua/du),1)
  fpzr *= mask
  fpzr /= total(fpzr)
  fp = abs(ft(ft(fpzr)*ft(fpa),/inverse)) ; convolve the zernike psf with the atmosphere psf
  fp /= total(fp)
  
  ; compute encircled energy
  eeA = encirc(fpA,nn/2,nn/2,1,/normalize)
  eeA = reform(eeA[1,*])
  ee = encirc(fp,nn/2,nn/2,1,/normalize)
  ee = reform(ee[1,*])
  
  ; plot the result
  old_p = !p
  !p.background = 255
  !p.color = 0
  !p.charsize = 3.
  !p.charthick = 2.
  !p.ticklen = 0.5
  !p.thick = 2
  
  title='Zernike ['+strtrim(n,2)+','+strtrim(m,2)+']; a = '+strtrim(a,2)
  xtitle = 'diameter / !4k!3/D'
  ytitle = 'Encircled Energy'
  if (not keyword_set(oplot)) then begin
    window,/free,title=title
    plot,rseta,ee,title=title,xtitle=xtitle,ytitle=ytitle
    oplot,rseta,eeA,linestyle=2
  endif else begin
    oplot,rseta,ee
  endelse
  
  !p = old_p
  
end


;clearwins


dir = '/Users/DonGavel/Desktop/eeData/'
outFile = 'eeVsZern'
outType = 'csv'
doPlots = 1
doFile = 0
doAtmos = 1
userModes = 1 ; user specifies the zernike modes to calculate
savePSFs = 1 ; save all the computed PSFs in a data cube

i = complex(0.,1.)
cm = 0.01; centimeters
nn = 1024 ; # fine-grid points in aperture domain
nmax = 2 ; highest radial order Zernike
nNollModes = ((nmax+1)*(nmax+2))/2 ; number of "Noll" modes - n,m forming the Pascal triangle
nmodes = (nNollModes + (nmax+2)/2)/2 - 2 ; number of "unique" modes: ignores reflection symmetry (-m modes) and piston and tilt
;nmodes = 25; 25 ; number of Zernike modes to evaluate, starting at focus. 25 modes is up to Noll #45 and through n=8th order
na = 5; 20 ; number of samples of coefficients
coef_max = 10.0; maximum of coefficient, in waves rms
subsamp = 4 ; desired # pixels per lambda/D in the zernike PSF
subsampAtmos = 50 ; desired # of pixels per lambda/r0 in the atmosphere PSF
;drange = 400. ; display range of the PSF focal plane, in lambda/D's
;prange = 800 ; x-axis range of the EE plot, in lambda/D's
D = 1.0 ; telescope diameter
lambda = 1.0 ; wavelength
r0 = 20*cm/30. ; Fried parameter, in units of telescope diameter
pc = 83.866 ; energy percentage to be captured in "ee%% vs A" curves.
; numerically, 83.866% is in a 1.22 encircled energy radius
; analytically, 83.7785% is in a 1.21967 (first zero of the Airy pattern) encircled energy radius
tab_user = [[4,2],[4,4]]

; generate a table of modes
if (userModes) then begin
  tab = tab_user
  nmodes = (size(tab))[2]
endif else begin
  tab = zernike(table=nNollModes,/noll)
; collapse the table somewhat to ignore piston, tip, tilt, and all negative m modes
  tab2 = intarr(2,nmodes)
  k = 0
  for zi = 3,nNollModes-1 do begin
    if (tab[1,zi] ge 0) then begin
      tab2[*,k] = tab[*,zi]
      k++
    endif
  endfor
  tab2 = tab2[*,0:k-1]
  tab = tab2
endelse
nmodes = (size(tab))[2]
;

; generate a table of coefficients
da = coef_max*2*!pi / float(na)
a_set = (findgen(na)+1)*da

; define an aperture
r = nn/(2*subsamp) ; radius of the aperture, in pixels
du = D/(2*r) ; pixel size in aperture domain, in units of telescope Diameter
dtheta = lambda / (nn*du) ; focal plane pixel size, in units of lambda/D
ap = circle(nn,nn,nn/2,nn/2,r,1.)
stop
; compute a baseline PSF and EE curve for diffraction-limited and its encircled energy
rset = findgen(nn/2)*dtheta
fpDL = abs(ft(ap))^2
eeDL = encirc(fpDL,nn/2,nn/2,1,/normalize)  ; calculate ee curve
eeDL = reform(eeDL[1,*])
eepcDL = 2*eePercent(eeDL,pc)*dtheta

;ind = value_locate(eeDL,pc/100.)            ; find "theta80"
;alpha = (pc/100.-eeDL[ind])/(eeDL[ind+1]-eeDL[ind]) ; interpolate
;eepcDL = 2*(ind+alpha)*dtheta ; baseline ee%%

print,'baseline ee'+strtrim(pc,2)+' diameter for diffraction limited: '+strtrim(eepcDL,2)+' lambda/D'

 disp,fpDL,'Diff Lim',x0=-(nn/2)*dtheta*[1,1],dx=dtheta*[1,1]
 window,/free,title='Diff Lim',xpos=0,ypos=0 & plot,rset,eeDL,xrange=[0,eepcDL*2],charsize=2,ytitle=ytitle,xtitle=xtitle

xtitle = 'diameter / !4k!3/D'
ytitle = 'Encircled Energy'

;
if (doFile) then begin
  cd,dir
  hstr = 'diameter / lambda/D'
  hstr += ', '+string(0)
  for ai = 0,na-1 do hstr += ', '+string(a_set[ai]/(2*!pi))
endif

; compute the atmosphere PSF and its encircled energy
;  atmosphere PSF is sampled on a different scale than the zernike PSF
if (doAtmos) then begin
  dthetaa = (lambda / r0) / subsampAtmos ; size of pixel in atmos PSF, units of D
  dua = lambda/(nn*dthetaa) ; size of pixel in atmos MTF, units of lambda/D
  fpA = atmopsf(nn,dthetaa,lambda,r0,mtf=mtfa)
  mtfa = abs(mtfa)
  rseta = findgen(nn/2)*dthetaa
  eeA = encirc(fpA,nn/2,nn/2,1,/normalize)
  eeA = reform(eeA[1,*])
  eepcA = 2*eePercent(eeA,pc)*dthetaa
  
   disp,fpA,'atmosphere',x0=-(nn/2)*dthetaa*[1,1],dx=dthetaa*[1,1]
   window,/free,title='atmosphere',xpos=100,ypos=100 & plot,rseta,eeA,xrange=[0,eepcA*2],charsize=2,ytitle=ytitle,xtitle=xtitle
  
  print,'baseline ee'+strtrim(pc,2)+' diameter for atmosphere: '+strtrim(eepca,2)+' lambda/D'
endif

; diagnostic storage
if (savePSFs) then begin
  fp_save = fltarr(nn,nn,na+2,nmodes)  ; saved PSFs
  fp_save[*,*,0] = fpDL
  if (doAtmos) then fp_save[*,*,1] = fpA else fp_save[*,*,1] = fpDL
  fpz_save = fltarr(nn,nn,na+2,nmodes) ; saved zernike PSFs
  fpz_save[*,*,0] = fpDL
  fpz_save[*,*,1] = fpDL
endif

eepc_save = fltarr(na+2,nmodes)      ; saved theta%%
eepc_save[0,*] = ones(nmodes)*eepcDL ; first one is that of diffraction-limit
if (doAtmos) then eepc_save[1,*] = ones(nmodes)*eepcA else eepc_save[1,*] = ones(nmodes)*eepcDL ; second one is that of atmosphere
nee = (size(eeDL))[1]
ee_save = fltarr(nee,na+2,nmodes)   ; saved ee curves
for zi=0,nmodes-1 do begin           ; first ee curve is diffraction-limit, second is atmosphere with no zernike aberration
                                     ; **careful** these two ee curves are on different scales, rset and rseta respectively ***
  ee_save[*,0,zi] = eeDL
  if (doAtmos) then ee_save[*,1,zi] = eeA else ee_save[*,1,zi] = eeDL
endfor

; cycle through all aberrations and all zernike modes

; cycle through zernike modes
for zi = 0,nmodes-1 do begin

; one file per zernike
  n = tab[0,zi]
  m = tab[1,zi]

  if (doFile) then begin
    atext = 'NoAtmos'
    if (doAtmos) then atext = 'Atmos'
    fileName = outFile+'_'+atext+'_'+strtrim(n,2)+'_'+strtrim(m,2)+'.'+outType
    openw,unit,fileName,/get_lun
    printf,unit,'Encircled Energy vs Diameter for various Zernike aberrations at various amplitudes'
    printf,unit,'Donald Gavel UCSC'
    printf,unit,'Generated '+systime()
    if (doAtmos) then begin
      printf,unit,'Atmosphere, D/r0 =,'+strtrim(1/r0,2)
    endif else begin
      printf, unit,'No Atmosphere'
    endelse
    printf,unit,' '
  endif

  z = zernike(nn,nn,r,n,m,/noll)
  z /= rms(z,ap)
  
  if (doFile) then begin
    printf,unit,'Zernike',', ',n,', ',m
    printf,unit,',DL,','amplitude (waves rms)'
    printf,unit,hstr
  endif
  
  ; cycle through coefficient values
  for ai = 0,na-1 do begin
    a = a_set[ai]
    wf = ap*exp(i*a*z)
    fpz = abs(ft(wf))^2
    fpz /= total(fpz)
    
    if (doAtmos) then begin
      ; resample the zernike psf at the sample points of the atmosphere psf
      x = (findgen(nn)-nn/2)*(du/dua)+nn/2
      y = x
      fpzr = interpolate(fpz,x,y,/grid,cubic=-0.5)
      mask = circle(nn,nn,nn/2,nn/2,(nn/2)*(dua/du),1)
      fpzr *= mask
      fpzr /= total(fpzr)
      fp = abs(ft(ft(fpzr)*ft(fpa),/inverse)) ; convolve the zernike psf with the atmosphere psf
      fp /= total(fp)
      rset = rseta
      dtheta = dthetaa
    endif

    ee = encirc(fp,nn/2,nn/2,1,/normalize)
    ee = reform(ee[1,*])
    ee_save[*,ai+2,zi] = ee
    eepc = 2*eePercent(ee,pc)*dtheta
    
    fp_save[*,*,ai+2,zi] = fp
    fpz_save[*,*,ai+2,zi] = fpzr
    eepc_save[ai+2,zi] = eepc
    print,'A = ',strtrim(a,2),' zern: ',strtrim(n,2),',',strtrim(m,2),' ee'+strtrim(pc,2)+':'+strtrim(eepc,2) & wait,.1
    
    ;
    ; -- this is stuff dealing with the display --
       new_image = fp_save[*,*,ai+2,zi]
       if (ai eq 0 and zi eq 0) then begin
         disp,new_image,x0=-(nn/2)*[1,1]*dtheta,dx=[1,1]*dtheta
         id = !disp.windows[!disp.wincount-1]
       endif
       displayBase = widget_info(id,find_by_uname='displayBase')
       widget_control,displayBase,get_uvalue=dataInfo
       ptr_free,dataInfo.data
       dataInfo.data = ptr_new(new_image)
       widget_control,displayBase,set_uvalue=dataInfo
       dispWidget_redrawDisplay,displayBase
    ;
  endfor
  
  if (doFile) then begin
  
    for k=0,nn/2-1 do begin
      if (doAtmos) then begin
        kdl = fix(k*(du/dua))
        if (kdl ge nn/2) then eedl = 1.0 else eedl = ee0[kdl]
      endif else begin
        eedl = ee0[k]
      endelse
      ostr = string(rset[k]*2)
      ostr += ', '+string(eedl)
      for ai = 0,na do ostr += ', '+string(ee_save[k,ai,zi])
      printf,unit,ostr
    endfor
    printf,unit,' '
  
  endif

  if (doFile) then begin
    close,unit
    free_lun,unit
  endif

endfor

;if (doFile) then close,unit

print,'done' & wait,.1

if (doPlots) then begin
  old_p = !p
  !p.background = 255
  !p.color = 0
  !p.charsize = 3.
  !p.charthick = 2.
  !p.ticklen = 0.
  !p.thick = 2
  
  title = ' steps of '+strtrim(da/(2*!pi),2)+' wave'
  xrange = prange ; ee plot range, in lambda/D
  
  for zi = 0,nmodes-1 do begin
    n = tab[0,zi]
    m = tab[1,zi]
    ztitle = '['+strtrim(n,2)+','+strtrim(m,2)+']'
    window,/free
    plot,2*rset,ee_save[*,1,zi],xrange = [0,xrange],color=100,/nodata,ticklen=0.5
    plot,2*rset,ee_save[*,1,zi],xrange = [0,xrange],title=ztitle+title,ytitle=ytitle,xtitle = xtitle,/noerase ; atmosphere, no aberration
    for k = 2,na+1 do oplot,2*rset,ee_save[*,k,zi]
    if (not doAtmos) then oplot,2*rset,ee0,linestyle = 2
    if (doAtmos) then begin
      oplot,2*rset*(dua/du),eeA,linestyle=2 ; diffraction-limited
    endif
  endfor
  
  for zi = 0,nmodes-1 do begin
    window,/free
    plot,a_set/(2*!pi),eepc_save[*,zi],color=100,/nodata,ticklen=0.5
    plot,a_set/(2*!pi),eepc_save[*,zi],/noerase,ytitle = 'ee'+strtrim(pc,2)+' diameter / !4k!3/D',xtitle='A, waves rms',title = 'Zernike ['+strtrim(tab[0,zi],2)+','+strtrim(tab[1,zi],2)+']'
  endfor
  
  !p = old_p
endif

;fp_save = reform(fp_save,nfp,nfp,(na+1)*(nmodes-2))
end
