PRO Read_OPD, filename, x, y, phase, amp, note=note, quiet=quiet
;
; read from WYKO OPD file
;
IF n_elements(filename) EQ 0 THEN BEGIN
    print, 'Read_OPD, filename, x, y, phase, amp, note=note, /quiet'
    return
ENDIF
nheader = 2
name = bytarr(16)
type = bytarr(2)
lng = bytarr(4)
attr = bytarr(2)
entry = {name:name, type:type, lng:lng, attr:attr}
;
;
;
openr, lun, filename, /get_lun, /SWAP_IF_BIG_ENDIAN
;
; read header infomation
;
header = bytarr(nheader)
readu, lun, header
readu, lun, entry
;
; extract info from dir
;
dir_size =  $
  entry.lng(0)+entry.lng(1)*256.+entry.lng(2)*256.^2+entry.lng(3)*256.^3
entries = dir_size/24.
if(0) then print, string(entry.name), dir_size, ' - ', entries, ' entries'
ctr = dir_size+2
pos_note = fltarr(10)
for i=0, entries-2 do begin
    readu, lun, entry
    lng = entry.lng(0)+entry.lng(1)*256.+entry.lng(2)*256.^2+ $
      entry.lng(3)*256.^3
    if strpos(entry.name, 'RAW')        ne -1 then pos_data = ctr
    if strpos(entry.name, 'Aspect')     ne -1 then pos_aspect = ctr
    if strpos(entry.name, 'Pixel_size') ne -1 then pos_pix = ctr
    if strpos(entry.name, 'Mult')       ne -1 then pos_mult = ctr
    if strpos(entry.name, 'Wavelength') ne -1 then pos_wave = ctr
    if strpos(entry.name, 'Time')       ne -1 then pos_time = ctr
    if strpos(entry.name, 'Date')       ne -1 then pos_date = ctr
    if strpos(entry.name, 'NA')         ne -1 then pos_na = ctr
    if strpos(entry.name, 'Mag')        ne -1 then pos_mag = ctr
    if strpos(entry.name, 'Ima')        ne -1 then pos_img = ctr
    if strpos(entry.name, 'AMPL')       ne -1 then pos_img = ctr
    if strpos(entry.name, 'Title')      ne -1 then pos_title = ctr
    if strpos(entry.name, 'Time')       ne -1 then pos_time = ctr
    if strpos(entry.name, 'Date')       ne -1 then pos_date = ctr
    if strpos(entry.name, 'Note1')      ne -1 then pos_note[0] = ctr
    if strpos(entry.name, 'Note2')      ne -1 then pos_note[1] = ctr
    if strpos(entry.name, 'Note3')      ne -1 then pos_note[2] = ctr
    if strpos(entry.name, 'Note4')      ne -1 then pos_note[3] = ctr
    if strpos(entry.name, 'Note5')      ne -1 then pos_note[4] = ctr
    if strpos(entry.name, 'Note6')      ne -1 then pos_note[5] = ctr
    if strpos(entry.name, 'Note7')      ne -1 then pos_note[6] = ctr
    if strpos(entry.name, 'Note8')      ne -1 then pos_note[7] = ctr
    if strpos(entry.name, 'Note9')      ne -1 then pos_note[8] = ctr
    if strpos(entry.name, 'Note10')     ne -1 then pos_note[9] = ctr
    if strpos(entry.name, 'Object')     ne -1 then pos_obje = ctr
    if strpos(entry.name, 'Wedge')      ne -1 then pos_wedg = ctr
    if strpos(entry.name, 'Subtr')      ne -1 then pos_subt = ctr
    if strpos(entry.name, 'Filt')       ne -1 then pos_filt = ctr
    if(NOT keyword_set(quiet))then begin
        if(string(entry.name) ne '')then begin
            print, string(entry.name), ' ', ctr
        endif
    endif
    ctr = ctr+lng
endfor
;
;
; read header information
;
;
; String Data
;
;
title = bytarr(20)
IF n_elements(pos_title) NE 0 THEN BEGIN
   point_lun, lun, pos_title
   readu, lun, title
ENDIF
;
date = bytarr( 9)
IF n_elements(pos_date) NE 0 THEN BEGIN
   point_lun, lun, pos_date
   readu, lun, date
ENDIF
;
time = bytarr( 9)
IF n_elements(pos_time) NE 0 THEN BEGIN
   point_lun, lun, pos_time
   readu, lun, time
ENDIF
note = strarr(10, 100)  ;was 60
this_note = bytarr(100)
FOR i=0, 9 DO BEGIN
    IF n_elements(pos_note[i]) NE 0 THEN BEGIN
       point_lun, lun, pos_note[i]
       readu, lun, this_note
       note[i, *]= string(this_note)
   ENDIF
ENDFOR
IF n_elements(pos_obje) NE 0 THEN BEGIN
    object = bytarr(60)
    point_lun, lun, pos_obje
    readu, lun, object
    object = string(object)
ENDIF ELSE object = ''
IF n_elements(pos_subt) NE 0 THEN BEGIN
    subtra = bytarr(10)
    point_lun, lun, pos_subt
    readu, lun, subtra
ENDIF ELSE subtra = ''
;
filt = bytarr(40)
IF n_elements(pos_filt) NE 0 THEN BEGIN
   point_lun, lun, pos_filt
   readu, lun, filt
ENDIF
;
; Numerical Data
;
;
IF n_elements(pos_NA) NE 0 THEN BEGIN
    NA = float(0.0)
    point_lun, lun, pos_NA
    readu, lun, NA
ENDIF ELSE NA = ''
IF n_elements(pos_mag) NE 0 THEN BEGIN
    mag = float(0.0)
    point_lun, lun, pos_mag
    readu, lun, mag
ENDIF ELSE mag = ''
pixel_size = float(0.0)
IF n_elements(pos_pix) NE 0 THEN BEGIN
   point_lun, lun, pos_pix
   readu, lun, pixel_size
ENDIF
;
aspect = float(0.0)
IF n_elements(pos_aspect) NE 0 THEN BEGIN
   point_lun, lun, pos_aspect
   readu, lun, aspect
ENDIF
;
IF n_elements(pos_mult) NE 0 THEN BEGIN
    mult = fix  (0  )
    point_lun, lun, pos_mult
    readu, lun, mult
    mult = fix(mult)
ENDIF ELSE mult = 1.
wedge = float(0.0)
IF n_elements(pos_pix) NE 0 THEN BEGIN
   point_lun, lun, pos_wedg
   readu, lun, wedge
ENDIF
;
wave = float(0.0)
point_lun, lun, pos_wave
readu, lun, wave
;check for Phillion specification change
IF (abs(wave) GT 1.e10) OR (abs(wave) LT 1.e-10) THEN BEGIN
    wave = 0.d
    point_lun, lun, pos_wave
    readu, lun, wave
ENDIF
IF NOT keyword_set(quiet) THEN BEGIN
    print, ''
    print, 'Title         = ', string(title)
    print, 'Date          = ', string(date)
    print, 'Time          = ', string(time)
    FOR i=0, 9 DO  $
      print, 'Note'+strcompress(string(i+1), /remove_all)+ $
             '  = ', note[i]
    print, 'Object        = ', object
    print, 'Subtract Ref  = ', string(subtra)
    print, 'Filter Type   = ', string(filt)
    print, ' '
    print, 'NA            = ', NA
    print, 'Magnification = ', mag
    print, 'Pixel_Size    = ', pixel_size
    print, 'Aspect        = ', aspect
    print, 'Multiplier    = ', mult
    print, 'Wedge         = ', wedge
    print, 'Wavelength    = ', wave
ENDIF
;
;
; read phase data
;
point_lun, lun, pos_data
xsize = 0
ysize = 0
element = 0
readu, lun, xsize
readu, lun, ysize
readu, lun, element
nxf=xsize
nyf=ysize
if element eq 4 then phase = fltarr(nyf, nxf) else phase = intarr(nyf, nxf)
readu, lun, phase
if element eq 4 then indx = where(phase gt 1.e30) $
else indx = where(abs(phase) eq 32767)
if indx(0) ne -1 then phase[indx] = 0.0
if element ne 4 then phase = phase/mult
x0 = nxf/2
y0 = nyf/2
x = (findgen(nxf)-x0)*pixel_size
y = (findgen(nyf)-y0)*pixel_size*aspect
phase = phase*wave
;
; read image array
;
IF n_elements(pos_img) NE 0 THEN BEGIN
    point_lun, lun, pos_img
    xsize = fix(0)
    ysize = fix(0)
    element = fix(0)
    readu, lun, xsize
    readu, lun, ysize
    readu, lun, element
    if element eq 2 then amp = intarr(nyf, nxf) else  $
      IF element EQ 4 THEN amp=fltarr(nyf, nxf) ELSE amp=bytarr(nyf, nxf)
    readu, lun, amp
ENDIF
close, lun
free_lun, lun
return
END