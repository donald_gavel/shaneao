;+
;====================
;  dojupiter.pro - process datasets
;====================
;-

function dojupiter,jup,dataset,centeronly=centeronly
;+
;=====================
; USAGE
;
;  im = dojupiter(jup,dataset,centeronly=centeronly)
;
; INPUTS
;  jup - jupiter raw speckle data
;  dataset - defined in jupiterDataset.pro
;  centeronly - a flag to indicate don't do speckle processing,
;               just center the frames and return the centered dataset
;
; OUTPUT
;   speckle-processed image
;  -or-
;   dataset with centered, background subtracted frames
;
; SIDE EFFECTS
;  produces speckle file, _ps and _phase files
;
;====================
;-
  if (n_elements(jup) eq 0) then begin
    print,'reading raw speckle data'
    jup = rdspec(dataset.datadir + dataset.datafile,100)
  endif
  
;
;    calculate background
;
  u=rebin(jup(*,*,0),300,300)
  sky = u(11:20,11:20)       ; hope this is sky
  ub = total(sky)/100
  un = (u>ub) - ub
  un = real(un)/real(max(un))
  mask = un lt .14
  maske = erode(mask,bytarr(5,5)+1)
  backg = planefit(u,maske)
;
;
;    center up on shrunk images
;.compile,enclose_ellipse
  maskb = bytarr(300,300) + 1        ;  a mask for bad pixels at the edge
  border = 5
  maskb(0:border,*) = 0
  maskb(300-border:299,*) = 0
  maskb(*,0:border) = 0
  maskb(*,300-border:299) = 0
  goodset = dataset.goodframes
  nf = dataset.nframes ; (size(goodset))(1)
;
;  print,'sample-down for centroiding'
;  j_good = rebin(jup(*,*,goodset),300,300,nf)
  j_goodn = fltarr(300,300,nf)
;
  print,'normalizing for centroiding'
  for i=0,nf-1 do begin
    j_good = rebin(jup(*,*,goodset(i)),300,300)
    j_goodn(*,*,i) = maskb * real(j_good-backg)
    j_goodn(*,*,i) = j_goodn(*,*,i) / max(j_goodn(*,*,i))
    print,format='($,I0," ")',i
  endfor
  print,'done'
;
  t1 = .095  ; recommended thresholds for normalized data
  t2 = .105
  a = (38.2/(3*0.063))/2.  ; size of jupiter on speckle camera
  b = a/1.069
  x0 = 150
  y0 = 150
  step = 20
  degrees = !pi/180.
  th = dataset.rotang * degrees
  print,'finding raw frame centers'
  center_ellipse_set,j_goodn,t1,t2,a,b,x0,y0,th,step,stat
;
;    extract region of interest
;
  x = dataset.subposx
  y = dataset.subposy
  nx = dataset.subnx
  ny = dataset.subny
  jc = fltarr(nx,ny,nf)
  backgf = rebin(backg,900,900)
  stat(0,*) = (150-stat(0,*))*3 ; need these because of centroid shifts
  stat(1,*) = (150-stat(1,*))*3

  print,'centering frames'
  for i=0,nf-1 do begin
    tmp = shift((jup(*,*,goodset(i))-backgf),stat(0,i),stat(1,i))
    jc(*,*,i) = tmp(x-nx/2:x+nx/2-1,x-nx/2:x+nx/2-1)
    print,format='($,I0," ")',i
  endfor
  print,'done'
;
;   taper the image edges to zero -
;     characteristic smooth size ~20 pixels, which is about the atmospheric
;     blur size, so that maximum data area retained in the image
;     while keeping sharp edges from dominating the speckle reconstruction.
;
  print,'computing taper window'
  g = gauss2(nx,nx/2,nx/2,24.)
  w = fltarr(nx,ny)
  border = 20
  w(border:nx-border,border:ny-border)=1
  wg = convolve(w,g)/total(g);  NOTE: replace with FFT based convolver!
  jcm = intarr(nx,ny,nf)
  print,'tapering edges' 
  for i=0,nf-1 do begin
    jcm(*,*,i) = fix(jc(*,*,i)*wg)
    print,format='($,I0," ")',i
  endfor
  print,'done'
;
;
  if (keyword_set(centeronly)) then begin
    return,jcm
  endif
;
;  do the speckle processing
;
  specklefilename = dataset.basename + '.spec'
  outputfilename = dataset.basename
  wrspec,specklefilename,jcm
  command = 'frame -framesProcess ' + string(nf) + $
                 ' -maxFrames ' + string(nf)  + $
         ' ' + specklefilename + ' ' + outputfilename
  spawn,command
;
  print,'calculating final image'
  ps = rdview(outputfilename + '_ps')
  phase = rdview(outputfilename + '_phase')
  refps = rdview(dataset.starfile + '_ps')
  refphase = refps*0
;  refphase = rdview(dataset.starfile + '_phase')
  mtf = readfits(dataset.mtffile)
  im = spdeconvolve(ps,phase,refps,refphase*0,mtf,0,128,.99)
  return,im
end

;ims = shift(im,-26,43)
;writefits,'j12_image.fits',ims
;disp, ((ims-smooth(ims,10)*.9)>(-20))<300  ; unsharp mask, contrast enhance

function projImage,im,dataset,lat,lon,map,minnaert,lines=lines
;+
;=========================================
;  USAGE
;     pim = projImage(im,dataset,lat,lon,map,minnaert,lines=lines)
;
;  DESCRIPTION
;    
;    project jupiter image to another view angle,
;     also, map the image onto lat-long grid
;
;  INPUTS
;     im - (sub)image of jupiter
;     dataset - created by jupiterDataset.pro
;     lat, lon - desired projection viewpoint
;     minnaert - minnaert parameter ([0,1], ~0.65 works on 550 nm data)
;
;  OUTPUT
;     map - the image projected on a lat-long map
;     pim - projected image
;====================
;-
  if (n_elements(minnaert) eq 0) then minnaert=0.75
  x0 = 450
  y0 = 450
  ;tvscl,e*400 + (ims - smooth(ims,10)*.9)
  sslat = dataset.sslat
  sslon = dataset.sslon
  selat = dataset.selat
  selon = dataset.selon
  th = dataset.rotang
  a = dataset.majoraxis / dataset.pixelsize
  b = dataset.minoraxis / dataset.pixelsize
  mapdim = 1024
; e = check_ellipse(imi,a,b,x0,y0,th)
  nx = (size(im))(1) ;  dataset.subnx
  ny = (size(im))(2) ;  dataset.subny
  x = dataset.subposx
  y = dataset.subposy
  imi = fltarr(900,900)
  imi(x-nx/2:x+nx/2-1,y-ny/2:y+ny/2-1) = im
  print,'mapping image to lat-long grid'
  map = planetmap(imi,x0,y0,a,b,selat,th,180./mapdim,minnaert, $
          sslat,sslon-selon,6.)
  map = shift([map,map*0],mapdim/2)
;
;   annotate with disk, equator lines, and pole points
  if (keyword_set(lines)) then begin
    drawval = max(map)
    map = map + drawval/2. ; to see the disk
    map(*,0:5) = drawval   ; south pole
    map(*,510:515) = drawval ; equator
  endif
;
  print,'projecting to new lat long center'
  pim = planetproject(map,900,450,450,a,b,lat,-(lon-selon))
;  p12s = p12 - smooth(p12,50)*.9
;  p12sc = p12s(450-150:450+149,450-150:450+149)
  return,pim
end

