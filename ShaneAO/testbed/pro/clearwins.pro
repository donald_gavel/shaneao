;+
; clearWins.pro  - clear all windows from display
;
;-
pro clearWins
	win = !D.window
	while (win ne (-1)) do begin
		wdelete,win
		win = !D.window
	endwhile
  defsysv, "!disp", exists = exists
  if (exists eq 0) then return
	dispWidget_destroyAll
end
