;+
;  lickwfs.pro - model the Lick AO wavefront sensor
;
;  usage:
;    sl = lickwfs(illum)
;
;  returns:
;    sl - a 2 x (2ns) array of subap locations (x and y) in units of
;         subap spacing
;
;  optional output:
;    illum - index list of fully illuminated subapertures
;
;  note:
;    specific to the Lick AO system
;
;  see also:
;    hartmannsimu.pro
;-
function lickwfs,illum
;  choice of axes and numbering is appropriate for IRCAL phase diversity
  y = [           -.5,.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
   -3.5,-2.5,-1.5,       1.5,2.5,3.5, $
   -3.5,-2.5,-1.5,       1.5,2.5,3.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
        -2.5,-1.5,-.5,.5,1.5,2.5, $
                  -.5,.5            ]
;
  x = [            3.5, 3.5, $
         2.5, 2.5, 2.5, 2.5, 2.5, 2.5, $
         1.5, 1.5, 1.5, 1.5, 1.5, 1.5, $
    0.5, 0.5, 0.5,           0.5, 0.5, 0.5, $
   -0.5,-0.5,-0.5,          -0.5,-0.5,-0.5, $
        -1.5,-1.5,-1.5,-1.5,-1.5,-1.5, $
        -2.5,-2.5,-2.5,-2.5,-2.5,-2.5, $
                  -3.5,-3.5             ]
  sl = [transpose(x),transpose(y)]
  illum = [3,4,5,6,8,9,10,11,12,13,15,16,17,18, $
           21,22,23,24,26,27,28,29,30,31, $
           33,34,35,36]
  return, sl
end