;+
;  phasescreen.pro - simulate Kolmogorov turbulence
;
;  creates a Gaussian random phase screen with the structure function
;    D(r) = 6.88 (r/r0)^(5/3)
;
;  usage:
;     screen = phasescreen(n,m,dx,r0,filter,seed,NEW=new)
;   where
;       n and m - dimensions of the array
;       dx - the physical size of a pixel (meters)
;       r0 - Fried's parameter (meters)
;       filter - the spatial filter function for the white noise
;           This is generated if the keyword NEW is set
;           Otherwise it assumed to be pre-generated
;           It must be regernerated if any of n,m,dx, or r0 is changed
;       seed - the random number seed
;       new - keyword to trigger a new filter calculation
;-
function phasescreen,n,m,dx,r0,filter,seed,new=new
   dkx = 1./(n*dx)
   dky = 1./(m*dx)
   if keyword_set(new) then begin
     kx = (findgen(n)-n/2) # (1+fltarr(m)) * dkx
     ky = (1+fltarr(n)) # (findgen(m)-m/2) * dky
     ksq = kx^2 + ky^2
     ksq(n/2,m/2) = 1.
     filter = sqrt(0.023 * r0^(-5./3.) * ksq^(-11./6.) )
     filter(n/2,m/2) = 0.
   endif
   w = randomn(seed,n,m) ; white noise
   wf = sqrt(float(n)*float(m))*ft(w)/sqrt(dkx*dky)
   sf = wf*filter
   s = real(ft(sf,/inverse)*dkx*dky)
   return,s
end

function subharmonic,n,m,r0,dx,level,seed
  i = complex(0,1)
  x = (findgen(n)-n/2)*dx
  y = (findgen(m)-m/2)*dx
  dkx = 1./(3.^level*n*dx)
  dky = 1./(3.^level*m*dx)
  p = (1./sqrt(2.))*(randomn(seed,4) + i*randomn(seed,4)) / sqrt(dkx*dky)
  f10 = sqrt(0.023 * r0^(-5./3.) * dkx^(-11./3.))
  f01 = sqrt(0.023 * r0^(-5./3.) * dky^(-11./3.))
  f11 = sqrt(0.023 * r0^(-5./3.) * ( dkx^2 + dkx^2 )^(-11./6.))
  lf0 = 2. * real( p(0) * exp(2*!pi*i*x*dkx) # (fltarr(m)+1) ) * f10
  lf1 = 2. * real( p(1) * (fltarr(n)+1) # exp(2*!pi*i*y*dky) ) * f01
  lf2 = 2. * real( p(2) * exp(2*!pi*x*i*dkx) # exp(2*!pi*i*y*dky) ) * f11
  lf3 = 2. * real( p(3) * exp(2*!pi*x*i*dkx) # exp(-2*!pi*i*y*dky) ) * f11
  lf = (lf0 + lf1 + lf2 + lf3)*dkx*dky
  return,lf
end
