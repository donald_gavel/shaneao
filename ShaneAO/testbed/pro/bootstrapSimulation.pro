; units
  mm = .001 ; millimeters
  microns = .000001 ; micrometers
  nm = 1.0e-9 ; nanometers
  i = complex(0.,1.)

; ---- parameters ----
; pyramid-lenslet wavefront sensor
if (n_elements(initialize) eq 0) then initialize = 1
if (initialize) then begin
  n = 512 ; simulation samples across the quad-lenslet array
  
  lensletSize = 3.6*mm ; overall extent of the simulation grid
  Nccd = 40 ; pixels across wfs CCD
  Npupil = 9 ; pixels to be dedicated to pupil image on CCD
  n2 = nextpow2(Npupil)
  pix = 24*microns ; size of pixel on CCD
  d_lenslet = 500*microns ; individual lenslet diameter
  lambda0 = 0.6328*microns ; central wavelength
  delta_lambda = 150.*nm ; bandwidth
  n_lambdas = 1; number of single lines, to simulate broad spectrum (pick an odd number)
  f_lenslet = 8.4*mm ; focal length of lenslet
  f_number_in = 45 ; f/# into lenslet
  pupil_separation = 23*pix
  pupil_size = Npupil*pix
  ;
  f_focusing_lens = f_lenslet*d_lenslet/(pupil_separation - d_lenslet) ; distance from focusing lens to lenslet
  D_focusing_lens = f_focusing_lens/f_number_in ; size of beam at focusing lens
  lambda = lambda0
  ;
  du = lensletSize/float(n) ; size of fine-sample in simulation
  spotSize = f_number_in*lambda0
  r0 = D_focusing_lens/8. ; atmospheric parameter
  ;
  mask0 = circle(Nccd,Nccd,Nccd/2+.5,Nccd/2+.5,Npupil/2.,1.,/grey)
  s = pupil_separation/pix/2-.5
  mask = shift(mask0,s,s)+shift(mask0,s,-s)+shift(mask0,-s,s)+shift(mask0,-s,-s)
;====================================
;  Propagate calibration light through the PLWFS
;
  lensletArray = genLensletArray(d_lenslet,f_lenslet,du,n)
  ap = circle(n,n,n/2-.5,n/2-.5,D_focusing_lens/du/2,1.,/grey)
  ph_lens = lens(n,du,f_focusing_lens,lambda,ap,/check,offset = -[1,1]*du/2)
  wf = ap*exp(i*ph_lens)
  wfL = fresnel(wf,du,f_focusing_lens,lambda) ; propagate incoming beam to lenslet
  wfL *= lensletarray
  wfP = fresnel(wfL,du,f_lenslet,lambda) ; propagate from lenslet to pupil image
  ;
  x = (findgen(Nccd)-Nccd/2)*pix/du + n/2
  y = x
  ccd = interpolate(abs(wfP)^2,x,y,/grid) ; bin onto the CCD
  ccd0 = ccd ; this is the flat wavefront appearance on the CCD
;===================================
;  Extract the slopes
;  "Hartman-transpose" method
  s = pupil_separation/pix/2-.5
  x1 = Nccd/2-s-Npupil/2.+.5
  x2 = Nccd/2+s-Npupil/2.+.5
  A = ccd[x1:x1+Npupil-1,x1:x1+Npupil-1]
  B = ccd[x2:x2+Npupil-1,x1:x1+Npupil-1]
  C = ccd[x1:x1+Npupil-1,x2:x2+Npupil-1]
  D = ccd[x2:x2+Npupil-1,x2:x2+Npupil-1]
  denom = A+B+C+D
  sx = ((B+D)-(A+C))/denom
  sy = ((C+D)-(A+B))/denom
  mask_ccd = mask0[Nccd/2-Npupil/2.+.5:Nccd/2+Npupil/2.-.5,Nccd/2-Npupil/2.+.5:Nccd/2+Npupil/2.-.5] gt 0.4
  sx0 = sx ; these are the reference centroids
  sy0 = sy
  initialize = 0
endif ; if (initialize)
restart = 1
if (not restart) then begin
;===================================
; start with test pattern on the dm:
  dm = fltarr(n2,n2); fltarr(Npupil,Npupil)
  pattern = 'sin'
  x = (ones(n2) ## (findgen(n2)-n2/2))/Npupil
  y = transpose(x)
  if (pattern eq 'tilt') then begin
    slope = 1.
    dm = slope*y + slope*x
  endif
  if (pattern eq 'focus') then begin
    sag = 1.
    dm = 4*sag*(x^2 + y^2)
  endif
  if (pattern eq 'astig') then begin
    bend = .1
    dm = 4*bend*(x^2-y^2)
  endif
  if (pattern eq 'astig45') then begin
    bend = .1
    dm = 4*bend*(x*y)
  endif
  if (pattern eq 'cross') then begin
    dm[n2/2,*] = 1.
    dm[*,n2/2] = 1.
;    dm[n2/2,n2/2] = 0.
  endif
  if (pattern eq 'sin') then begin
    amp = 4
    freq = 2.0
    dm = amp*sin(2*!pi*freq*x)
  endif
  if (pattern eq 'bump') then begin
    dm[9,7] = 1. ; y
;;    dm[10,5] = 1. ; n
;    dm[4,7] = 1. ; y
;;    dm[6,11] = 1. ; n
;    dm[7,6] = 1. ; y
;    dm[6,9] = 1.
;    dm[9,6] = 1.
;    dm[10,10] = 1.
;    dm /= 10.
  endif
  x = (findgen(n2)-n2/2)*d_focusing_lens/Npupil/du + n/2
  ph_atmos = fltarr(n,n)
  for k=0,n2-1 do for j = 0,n2-1 do ph_atmos[x[j],x[k]] = dm[j,k]
;  blur = gauss2(n,n/2,n/2,d_focusing_lens/Npupil/du)
  x = (ones(n) ## (findgen(n)-n/2))/(d_focusing_lens/Npupil/du)
  y = transpose(x)
  u = ((x ge 0) and (x lt 1) and (y ge 0) and (y lt 1))*(1-x-y+x*y)
  blur = u + rotate(u,1) + rotate(u,2) + rotate(u,3)
  ph_atmos = convolve(ph_atmos,blur)
;   temporary code
;  ph_atmos = sin(2*!pi*2*((x/npupil)+(y/npupil)))
;
  dm0 = dm ; save initial dm
  dm *= 0 ; flatten the dm
  ph_dm = fltarr(n,n) ; initial dm setting
;  ph_atmos *= 0. ; zero it out (flat wavefront)
endif ; not restart
;-----------------------------------------
;  loop here
  loopGain = 0.04
  n_iters = 50
  ph_residual = fltarr(n,n,n_iters+1)
  ccd_t = fltarr(Nccd,Nccd,n_iters+1)
  rms = fltarr(n_iters+1)
  for iter = 0,n_iters do begin
  ;-----------------------------------------
  ;  Propagate aberated wavefront to PLWFS
    wf_lens = ap*exp(i*ph_lens)*exp(i*ph_atmos)*exp(i*ph_dm)
    wf_lenslet = fresnel(wf_lens,du,f_focusing_lens,lambda) ; propagate incoming beam to lenslet
    wf_lenslet *= lensletarray
    wfP = fresnel(wf_lenslet,du,f_lenslet,lambda) ; propagate from lenslet to pupil image
    x = (findgen(Nccd)-Nccd/2)*pix/du + n/2
    y = x
    ccd = interpolate(abs(wfP)^2,x,y,/grid) ; bin onto the CCD
    ; and extract the slopes
    A = ccd[x1:x1+Npupil-1,x1:x1+Npupil-1]
    B = ccd[x2:x2+Npupil-1,x1:x1+Npupil-1]
    C = ccd[x1:x1+Npupil-1,x2:x2+Npupil-1]
    D = ccd[x2:x2+Npupil-1,x2:x2+Npupil-1]
    denom = A+B+C+D
    sx = ((B+D)-(A+C))/denom
    sy = ((C+D)-(A+B))/denom
    sx -= sx0
    sy -= sy0
    n2 = nextpow2(Npupil)
    sx = zeropad(sx,n2,n2) ; accomodate zeropad's shift of one pixel
    sy = zeropad(sy,n2,n2) ; the bottom line is dm is registered to WFS
    apWFS = shift(zeropad(mask_ccd,n2,n2),1,1)
    sx = rotate(sx,2) ; this accounts for the lenslet's flipping of the pupil image
    sy = rotate(sy,2)
    ph_est = ftwfs(sx,sy,apWFS,geometry='Fried',boundaryFix='iterative')
    apDM = circle(n2,n2,n2/2-.5,n2/2-.5,(Npupil+1)/2.,1.)
;    disp,ph_atmos,'ph_atmos'
;    disp,ccd,'ccd'
;    disp,ph_est*apDM,'ph_est'
  ;-------------------------------------
  ;  integrate correction onto dm
    ph_est = fshift(ph_est,.5,.5)
    smoother = gauss2(n2,n2/2,n2/2,1.2)
    ph_est = convolve(ph_est,smoother) ; do not allow high order, which gets aliased
    dm += loopGain*ph_est ; control loop integrator
    x = (findgen(n2)-n2/2)*d_focusing_lens/Npupil/du + n/2
    ph_dm = fltarr(n,n)
    for k=0,n2-1 do for j = 0,n2-1 do ph_dm[x[j],x[k]] = dm[j,k]
    ph_dm = convolve(ph_dm,blur)
;    disp,ph_dm,'ph_dm'
    ph_residual[*,*,iter] = depiston(ph_atmos+ph_dm,ap)
    rms[iter] = rms(ph_residual[*,*,iter],ap)
    ccd_t[*,*,iter] = ccd
    print,'iteration '+strtrim(iter,2),' rms ',strtrim(rms[iter],2)
    wait,.1
  endfor
end
