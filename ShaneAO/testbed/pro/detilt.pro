;+
;  detilt - remove tilt over an aperture
;
;  USAGE:
;    phdt = detilt(ph,ap)
;
;  INPUTS:
;    ph - phase
;    ap - aperture
;
;  OUTPUTS:
;    phdt - phase with tilt removed
;    tx, ty - (optional) tip and tilt coefficients (units: phase/pixel)
;
;-
function detilt,ph,ap,tx,ty
  n = (size(ph))(1)
  m = (size(ph))(2)
  if (n_elements(ap) eq 0) then ap = ones(n,m)
  x = ( (    findgen(n) # (fltarr(m)+1)) - n/2)
  x = x - total(x*ap)/total(ap)
  y = ( ( (fltarr(n)+1) #   findgen(m) ) - m/2)
  y = y - total(y*ap)/total(ap)
  tx = total(ph*x*ap)/total(x*x*ap)
  ty = total(ph*y*ap)/total(y*y*ap)
  phdt = (ph - tx*x - ty*y)
  return, phdt
end