;+
;  ps_plot.pro
;     plot power spectra in a nice manner
;
;  inputs:
;     ps - power spectrum, 1 or 2 rows of n datapoints
;         ps is the "dual-sided" spectra, i.e. it contains the values for both
;         negative and positive frequency.  Elements 0:n/2 are the positive side.
;         psplot takes the dual-sided spectrum and plots only one side,
;         doubling it so each frequency bin represents total contributions
;         from both "sides"
;     fs - sample frequency, Hz
;
;  optional inputs:
;     oplot - plot over a previous plot
;     color - curve color(s)
;     ytitle - text to lable the y axis
;
;-
pro ps_plot, ps,fs,f,xlog=xlog,ylog=ylog, _extra = e,oplot=oplot,color=color,ytitle=ytitle, charsize=charsize
  if (n_elements(color) eq 0) then color = 255
  if (n_elements(color) eq 1) then color2 = 100
  if (n_elements(color) eq 2) then begin
    color2 = color[1]
    color = color[0]
  endif
  if (n_elements(ytitle) eq 0) then ytitle = 'Power, units^2/hz'
  if (n_elements(charsize) eq 0) then charsize = 1.5
  if (n_elements(xlog) eq 0) then xlog=1
  if (n_elements(ylog) eq 0) then ylog=1
  ndim = (size(ps))(0)
  if ((ndim eq 2) and ((size(ps))(1) eq 1)) then begin
    psplot,transpose(ps),fs,color=color,ytitle=ytitle,charsize=charsize,_extra=e
    return
  endif
  if (ndim eq 1) then  n = (size(ps))(1)/2. else n = (size(ps))(2)/2.
  dt = 1./fs
  fn = fs/2.
  f0 = fn/float(n)
  f = (findgen(n)+1)*f0
  if (ndim eq 1) then psx = 2*ps(1:n) else psx = 2*ps(0,1:n)
  if keyword_set(oplot) then begin
    oplot,f,psx,color=color, _extra = e
  endif else begin
    plot,f,psx,xlog=xlog,ylog=ylog,xtitle='Frequency, Hz',ytitle=ytitle,color=color, $
        charsize=charsize,ticklen = 0.04, _extra = e
  endelse
  if (ndim eq 2) then begin
    psy = 2*ps(1,1:n)
    oplot,f,psy,color=color2, _extra = e
  endif
;  oplot,f,ones(n)
end
