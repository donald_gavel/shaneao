;+
;  stars.pro - determine the density of guide stars
;              according to the Bahcall-Soneira model
;
;  usage:
;     n = stars(l,b,m)
;
;  inputs:
;     l,b - Galactic longitude, latitude, respectively, in radians
;      m - star magnitude
;
;  output:
;      number of stars per square degree brighter than magnitude m
;
;-
function stars,l,b,m
  if (m le 12) then begin
    mu = 0.03
    gamma = 0.36
  endif
  if ((m gt 12) and (m lt 20)) then begin
    mu = 0.0075*(m-12)+0.03
    gamma = 0.04*(12-m)+0.36
  endif
  if (m ge 20) then begin
    mu = 0.09
    gamma = 0.04
  endif
  sigma = 1.45-0.20*cos(b)*cos(l)
  c1 = 925.
  c2 = 1050.
  alpha = -0.132
  beta = 0.035
  delta = 3.0
  kappa = -0.180
  eta = 0.087
  lambda = 2.50
  mstar = 15.75
  mdart = 17.5
  n = c1*10.^(beta*(m-mstar))/(1+10^(alpha*(m-mstar)))^delta * $
      1/(sin(b)*(1-mu*(1/tan(b))*cos(l)))^(3-5*gamma) + $
      c2*10^(eta*(m-mdart))/(1+10^(kappa*(m-mdart)))^lambda * $
      1/(1-cos(b)*cos(l))^sigma
  return,n
end
;
;  given magnitude and distance in arcsec, find the
;   number of guide stars
;
degrees = !pi/180.
arcsec = !pi/(180.*3600.)
ma = findgen(20)*.5+10
na = ma
n = (size(ma))(1)
b = 20.*degrees
l = 0.*degrees
;  NGS
for i=0,n-1 do na[i] = (20*arcsec/degrees)^2*stars(l,b,ma[i])
plot,ma,na,/ylog,charsize=1.5,xtitle='Guide star V-magnitude',ytitle='Sky coverage',yrange=[1.e-4,10.]
tickgrid,[10,20.,2,.5],[1.e-4,10.],/ylog
b = 50.*degrees
for i=0,n-1 do na[i] = (20*arcsec/degrees)^2*stars(l,b,ma[i])
oplot,ma,na,linestyle=2
;  LGS
b = 20.*degrees
for i=0,n-1 do na[i] = (120*arcsec/degrees)^2*stars(l,b,ma[i])
oplot,ma,na,color=180
b = 50.*degrees
for i=0,n-1 do na[i] = (120*arcsec/degrees)^2*stars(l,b,ma[i])
oplot,ma,na,color=180,linestyle=2
end
