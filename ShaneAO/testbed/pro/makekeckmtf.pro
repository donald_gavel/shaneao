;+
;  makekeckmtf.pro - make a Keck mtf
;    usage: mtf = makekeckmtf(lambda,dtheta,Nx)
;     where lambda is wavelength, in microns
;           dtheta is platescale, in arcseconds
;           Nx is the size of the image, in pixels
;-
function makekeckmtf,lambda,dtheta,Nx
  arcseconds = !Pi/(180.*3600.)
  microns = 1.e-6
  dth = dtheta*arcseconds
  lam = lambda*microns
  du = lam/(Nx*dth)
  L = 0.9
  ap = keckap(du,L,hex(Nx,(L+.1)/du))
  apf = ft(ap)
  mtf = real(ft(apf*conj(apf),/inverse))
  mtf = rotate(mtf,60)
  return,mtf
end

