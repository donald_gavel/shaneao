function catchKeyboardEvent,k,nk,percent_done
    chr = get_kbrd(0)
    if (strupcase(chr) eq "Q") then return,1
    if (n_elements(percent_done) eq 0) then percent_done = -1
    new_percent_done = fix((k/float(nk))*20.)
    if (new_percent_done ne percent_done) then begin
      if (new_percent_done eq 0) then print,format='($,"percent complete: ")'
      print,format='($,I0," ")',new_percent_done*5
      percent_done = new_percent_done
      wait,.001
    endif
    return,0
end
