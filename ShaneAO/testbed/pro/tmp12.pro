;mask = 1.-circle(n,n,n/2,n/2,5.5,1.)
;u = ft(ft(corrected_phase)*mask,/inverse)
;wf = pupil*exp(i*u*(lambda/science_lambda))
;wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
;wf_fp = ft(wf)
;psf = abs(wf_fp)^2/max(psf_dl)
;print,sqrt(-alog(max(psf)))*(science_lambda/(2*!pi))/nm,' nm'
;
;wf = pupil*exp(i*corrected_phase*(lambda/(1.6*microns)))
;wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
;wf_fp = ft(wf)
;psf_mcaoH = abs(wf_fp)^2/max(psf_dl)
;print,'H Strehl = ',max(psf_mcaoH)
;
;wf = pupil*exp(i*corrected_phase*(lambda/(1.0*microns)))
;wf = congrid(zeropad(wf,2*n,2*n),n,n,/center)
;wf_fp = ft(wf)
;psf_mcaoJ = abs(wf_fp)^2/max(psf_dl)
;print,'J Strehl = ',max(psf_mcaoJ)
;
;u = abs(ft(corrected_phase))^2
;u = ravg(u)
;r = dindgen((size(u))[1])
;u = u*2*!pi*r
;oplot,u,color=220;,xrange=[0,10]
;
pu = zeropad(pupil,2*n,2*n)
tau0 = real(ftconvolve(pu,pu))
tau0 = tau0/total(tau0)

zpf = 2

df = 1/(n*du)
w0 = 9.69d-3*(2*!pi/lambda)^2*wphi;  wphi = f^(-11/3)
w = w0*total(Cn2)
we = w*err

Cphi_atm = real(ft(w,/inverse))*df^2
Cphi = real(ft(we,/inverse))*df^2
Dphi_atm = 2*(Cphi_atm[n/2,n/2]-Cphi_atm)
Dphi = 2*(Cphi[n/2,n/2]-Cphi)
tau_atm = exp(-0.5*Dphi_atm)
tau = exp(-0.5*Dphi)

n1 = zpf*n/2-n/2
n2 = zpf*n/2+n/2-1
if (zpf eq 1) then tau0 = tau0[n1:n2,n1:n2]
tau_unc = tau0*zeropad(tau_atm,zpf*n,zpf*n)
tau_cor = tau0*zeropad(tau,zpf*n,zpf*n)
psf_0 = (abs(ft(tau0)))[n1:n2,n1:n2]
psf_unc = (abs(ft(tau_unc)))[n1:n2,n1:n2]/max(psf_0)
psf_cor = (abs(ft(tau_cor)))[n1:n2,n1:n2]/max(psf_0)

window,1
u = ravg(we)
r = findgen((size(u))[1])
plot,ravg((w))>.01,xrange=[1,50],/ylog,/xlog
oplot,ravg((we))
oplot,ravg(abs(ft(uncorrected_phase*pupil))^2)*long(n)^2*du^2,color=150
oplot,ravg(abs(ft(corrected_phase*pupil))^2)*long(n)^2*du^2,color=150
;
window,2
u = ravg(abs(ft(corrected_phase*pupil))^2)*long(n)^2*du^2
plot,integral(u*r),xrange=[1,50],/xlog
oplot,integral(ravg(we)*r),color=150
end
