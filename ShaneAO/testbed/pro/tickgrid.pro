;+
; minor tick grid for log axes
;
;  inputs:
;    xrange = [x0,xf] for /xlog
;    xrange = [x0,xf,major_dx,minor_dx] for linear
;    color = [major_color, minor_color]
;  
;-
pro tickGrid,xrange,yrange,color=color,xlog=xlog,ylog=ylog

if (n_elements(color) eq 0) then color = [255,100]

if (keyword_set(xlog)) then begin
  x0 = 10.^floor(alog10(xrange[0]))
  x1 = 10.^ceil(alog10(xrange[1]))
  tx = alog10([x0,x1]) + [1,-1]*alog10(xrange[1]/xrange[0])*.02
  tx = 10.^tx
  ttx = alog10([x0,x1]) + [1,-1]*alog10(xrange[1]/xrange[0])*.01
  ttx = 10.^ttx
endif else begin
  x0 = xrange[0]
  x1 = xrange[1]
  dx = xrange[2]
  ddx = xrange[3]
  tx = [x0,x1]+(x1-x0)*[1,-1]*.02
  ttx = [x0,x1]+(x1-x0)*[1,-1]*.01
endelse

if (keyword_set(ylog)) then begin
  y0 = 10.^floor(alog10(yrange[0]))
  y1 = 10.^ceil(alog10(yrange[1]))
  ty = alog10([y0,y1]) + [1,-1]*alog10(yrange[1]/yrange[0])*.02
  ty = 10.^ty
  tty = alog10([y0,y1]) + [1,-1]*alog10(yrange[1]/yrange[0])*.01
  tty = 10.^tty
endif else begin
  y0 = yrange[0]
  y1 = yrange[1]
  dy = yrange[2]
  ddy = yrange[3]
  ty = [y0,y1]+(y1-y0)*[1,-1]*.02
  tty = [y0,y1]+(y1-y0)*[1,-1]*.01
endelse

if (keyword_set(ylog)) then begin
  n = fix(alog10(y1/y0))
  y = y0
  for j=0,n-1 do begin
    if (j gt 0) then begin
      oplot,[x0,x1],y*[1,1],color=color[0]
      oplot,[x0,tx[0]],y*[1,1]
      oplot,[tx[1],x1],y*[1,1]
    endif
    for i=2,9 do begin
      oplot,[x0,x1],y*i*[1,1],color=color[1]
      oplot,[x0,ttx[0]],y*i*[1,1]
      oplot,[ttx[1],x1],y*i*[1,1]
    endfor
    y = y*10.
  endfor
endif else begin
  n = fix((y1-y0)/dy)
  for j=0,n do begin
    y = y0 + j*dy
    oplot,[x0,x1],[y,y]
    m = fix(dy/ddy)
    if (j gt 0) then oplot,[x0,x1],y*[1,1],color=color[0]
    oplot,[x0,tx[0]],y*[1,1]
    oplot,[tx[1],x1],y*[1,1]
    for i=1,m do begin
      oplot,[x0,x1],y+i*ddy*[1,1],color=color[1]
      oplot,[x0,ttx[0]],y+i*ddy*[1,1]
      oplot,[ttx[1],x1],y+i*ddy*[1,1]
    endfor
  endfor
endelse

if (keyword_set(xlog)) then begin
  n = fix(alog10(x1/x0))
  x = x0
  for j=0,n-1 do begin
    if (j gt 0) then begin
      oplot,x*[1,1],[y0,y1],color=color[0]
      oplot,x*[1,1],[y0,ty[0]]
      oplot,x*[1,1],[ty[1],y1]
    endif
    for i=2,9 do begin
      oplot,x*i*[1,1],[y0,y1],color=color[1]
      oplot,x*i*[1,1],[y0,tty[0]]
      oplot,x*i*[1,1],[tty[1],y1]
    endfor
    x = x*10.
  endfor
endif else begin
  n = fix((x1-x0)/dx)
  for j=0,n do begin
    x = x0 + j*dx
    oplot,[x,x],[y0,y1]
    m = fix(dx/ddx)
    if (j gt 0) then oplot,x*[1,1],[y0,y1],color=color[0]
    oplot,x*[1,1],[y0,ty[0]]
    oplot,x*[1,1],[ty[1],y1]
    for i=1,m do begin
      oplot,x+i*ddx*[1,1],[y0,y1],color=color[1]
      oplot,x+i*ddx*[1,1],[y0,tty[0]]
      oplot,x+i*ddx*[1,1],[tty[1],y1]
    endfor
  endfor
endelse

end
