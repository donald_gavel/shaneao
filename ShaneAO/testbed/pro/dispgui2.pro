;
; Auto Save File For dispgui.pro
;
;  Sat Oct  4 21:07:37 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




PRO dispgui_Event, Event


  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  t = size(ev)
  if (t(0) eq 0) then evname = ev else evname = ev.name
  widget_control, event.top, get_uvalue = uv
  field = uv.field
  widget_control, field.draw, get_value = win
  tvdevice, win


  CASE evname OF 

  'slide1': begin
;      Print, 'Event for slide1'
      frame = event.value
;      Print, 'frame = ', frame
      widget_control, field.draw, get_uval = uval
      uval.frame = frame
      x0 = uval.box(0)
      x1 = uval.box(1)
      y0 = uval.box(2)
      y1 = uval.box(3)
      widget_control, field.draw, set_uval = uval
      exptv, uval.data(x0:x1,y0:y1,uval.frame),/noexact, /data, relative = .8
      tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
      tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]

      end
  'DRAW3': BEGIN
      Print, 'Event for DRAW3'
      if (event.press and 1b) then begin
        widget_control, event.id, get_uvalue = uval
        nw = uval.wsize(0)
        mw = uval.wsize(1)
        widget_control, event.id, get_value = windex
        tvdevice, windex
;   determine what to do!
        buttongroup = field.buttongroup
        widget_control, buttongroup, get_value = bindex
        case bindex of
       0: begin    ; Pan
            x = event.x
            y = event.y
            s = size(uval.data)
            n = s(1)
            m = s(2)
;  convert from device to pixel coordinates
            print, 'device coord: [',x,', ',y,']'
;            x = x - round(256*.1)
            x = x - round(nw*.1)
;            y = y - round(256*.1)
            y = y - round(mw*.1)
;  convert from pixel to original image pixel
;            xo = float(x) / (256*.8)
            xo = float(x) / (nw*.8)
;            yo = float(y) / (256*.8)
            yo = float(y) / (mw*.8)
            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
            print, 'original data index: [',x,', ',y,']'
;  pan
            aspect = float(rangey) / float(rangex)

            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1)>0
            endif

            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1)>0
            endif
            uval.box = [x0,x1,y0,y1]
            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            if uval.nframes eq 0 then $
              exptv, uval.data(x0:x1,y0:y1), /noexact, /data, relative = .8
            if uval.nframes gt 0 then $
              exptv, uval.data(x0:x1,y0:y1,uval.frame),/noexact, /data, relative = .8
            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
       1: begin    ; Zoom
            x = event.x
            y = event.y
            s = size(uval.data)
            n = s(1)
            m = s(2)
;  convert from device to pixel coordinates
            print, 'device coord: [',x,', ',y,']'
;            x = x - round(256*.1)
            x = x - round(nw*.1)
;            y = y - round(256*.1)
            y = y - round(mw*.1)
;  convert from pixel to original image pixel
;            xo = float(x) / (256*.8)
            xo = float(x) / (nw*.8)
;            yo = float(y) / (256*.8)
            yo = float(y) / (mw*.8)
            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
            print, 'original data index: [',x,', ',y,']'
;  zoom!
            aspect = float(rangey) / float(rangex)
            rangex = round(rangex/2) > 2
            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1) > 0
            endif
            rangey = round(aspect*rangex)
            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1) > 0
            endif
            uval.box = [x0,x1,y0,y1]
            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            if uval.nframes eq 0 then $
              exptv, uval.data(x0:x1,y0:y1), /noexact, /data, relative = .8
            if uval.nframes gt 0 then $
              exptv, uval.data(x0:x1,y0:y1,uval.frame),/noexact, /data, relative = .8
            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
       2: begin    ; Unzoom
            x = event.x
            y = event.y
            s = size(uval.data)
            n = s(1)
            m = s(2)
;  convert from device to pixel coordinates
            print, 'device coord: [',x,', ',y,']'
;            x = x - round(256*.1)
            x = x - round(nw*.1)
;            y = y - round(256*.1)
            y = y - round(mw*.1)
;  convert from pixel to original image pixel
;            xo = float(x) / (256*.8)
            xo = float(x) / (nw*.8)
;            yo = float(y) / (256*.8)
            yo = float(y) / (mw*.8)
            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
            print, 'original data index: [',x,', ',y,']'
;  unzoom!
            aspect = float(rangey) / float(rangex)
            rangex = round(rangex*2) < n
            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1) > 0
            endif
            rangey = round(aspect*rangex) < m
            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1) > 0
            endif
            uval.box = [x0,x1,y0,y1]
            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            if uval.nframes eq 0 then $
              exptv, uval.data(x0:x1,y0:y1), /noexact, /data, relative = .8
            if uval.nframes gt 0 then $
              exptv, uval.data(x0:x1,y0:y1,uval.frame),/noexact, /data, relative = .8

            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
       3: begin    ; Values
            box = uval.box
            print,'origin: ',[box(0),box(2)]
            tvvalue2, uval.data(box(0):box(1),box(2):box(3),uval.frame), $
                   origin = [box(0),box(2)]
          end
        endcase
      endif
      END
  'BGROUP4': BEGIN
      CASE Event.Value OF
      0: Print,'Button Pan       Pressed'
      1: Print,'Button Zoom      Pressed'
      2: Print,'Button Unzoom  Pressed'
      3: Print,'Button Values Pressed'
      ELSE: Message,'Unknown button pressed'
      ENDCASE
      END
  'button5a': Begin
      Print, 'Event for Revert'
        widget_control, field.draw, get_value = windex
        widget_control, field.draw, get_uvalue = uval
        s = size(uval.data)
        n = s(1)
        m = s(2)
        uval.box = [0,n,0,m]
        widget_control, field.draw, set_uval = uval
        exptv, uval.data(*,*,uval.frame), /noexact, /data, relative = .8
        tvaxis, xaxis = 0, yaxis = 0
        tvaxis, xaxis = 1, yaxis = 1
      end
  'BUTTON5': BEGIN
      Print, 'Event for Color Table'
        xloadct
      END
  'BUTTON6': BEGIN
      Print, 'Event for Done'
        widget_control,event.top,/destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO dispgui, data, name, GROUP=Group, size = wsize

  Print, 'DISPGUI -- v.2 experimenetal'
  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0
  if n_elements(wsize) eq 0 then wsize = [256,256]

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE={name:'MAIN13'})

  LABEL2 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL2', $
;      /DYNAMIC_RESIZE, $
      FONT='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      VALUE='Dispgui')

  s = size(data)
  if s(0) eq 3 then nframes = s(3) else nframes = 0
  frame = 0

  base1 = widget_base( main13, row=1, map=1, uvalue='base1' )

  if nframes gt 1 then begin
    slide1 = widget_slider( base1,maximum = nframes-1, $
           value = 0, title='frame', /vertical, uvalue = 'slide1')
  endif else begin
    slide1 = 0
  endelse

  DRAW3 = WIDGET_DRAW( base1, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW3', data:data, box:[0,s(1)-1,0,s(2)-1], $
              nframes: nframes, frame: frame, wsize:wsize}, $
      /no_copy, $
      XSIZE=wsize(0), $
      YSIZE=wsize(0))

  Btns198 = [ $
    'Pan    ', $
    'Zoom   ', $
    'Unzoom ', $
    'Values' ]
  BGROUP4 = CW_BGROUP( MAIN13, Btns198, $
      ROW=1, $
      EXCLUSIVE=1, $
      set_value = 3, $
      UVALUE='BGROUP4')

  button5a = widget_button( main13, $
      uvalue = 'button5a', $
      value = 'Revert')

  BUTTON5 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON5', $
      VALUE='Color Table')

  BUTTON6 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON6', $
      VALUE='Done')

  field = {slide: slide1, draw: draw3, buttongroup: bgroup4}
  widget_control, main13, get_uvalue = uv
  uv = {name:uv.name, field:field}
  widget_control, main13, set_uvalue = uv

  WIDGET_CONTROL, MAIN13, /REALIZE

  ; Get drawable window index

  COMMON DRAW3_Comm, DRAW3_Id
  WIDGET_CONTROL, DRAW3, GET_VALUE=DRAW3_Id

  if n_elements(name) ne 0 then $
    widget_control, label2, set_value = name
  widget_control, field.draw, get_value = win
  tvdevice, win
  if nframes eq 0 then exptv, data, /noexact, /data, relative = .8
  if nframes gt 0 then exptv, data(*,*,0), /noexact, /data, relative = .8
  tvaxis, xaxis=0, yaxis=0
  tvaxis, xaxis=1, yaxis=1

  XMANAGER, 'dispgui', MAIN13
END
