;+
;  Hartmann mask analysis for the 1 meter
;
;-

cm = .01

dx = 10*cm
n = 16
c = n/2 - 0.5 ; center
r = 50.*cm ; radius
rso = 20.*cm ; radius of secondary obscuration

; generate the 1 meter aperture with its obscuration
ap = circle(n,n,c,c,r/dx,1.) - circle(n,n,c,c,rso/dx,1.)

; simulate a zernike mode
ph = real(zernike(n,n,r/dx,3,1,center=[1,1]*c))

; calculate slopes
sx = (shift(ph,-1,0)-shift(ph,1,0))/2. ; X Hartmann Slopes
sy = (shift(ph,0,-1)-shift(ph,0,1))/2. ; Y Hartmann Slopes

; reconstruct wavefront from slopes
a = ftwfs(sx,sy,ap)

disp,ph,ap=ap,'original phase'
disp,a,ap=ap,'reconstructed phase'
disp,a-ph,ap=ap,'reconstruction error'
end

