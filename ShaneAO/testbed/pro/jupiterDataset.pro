; jupiterDataset.pro - dataset definitions for Jupiter files, July, 1994
;

dataset = {dataset, $
           basename:'', $
           datafile:'', $
           date:'', $
           time:'', $
           wavelength:0.0, $
           filter:'', $
           rotang:0.0, $
           tint:0.0, $
           nframes:0, $
           goodframes:intarr(100), $
           pixelsize:0.0, $
           starfile:'', $
           mtffile:'',  $
           datadir:'',  $
           resultsdir:'', $
           selon:0.0, $
           selat:0.0, $
           sslon:0.0, $
           sslat:0.0, $
           majoraxis:0.0, $
           minoraxis:0.0, $
           subposx:0, $
           subposy:0, $
           subnx:0, $
           subny:0, $
           spotlon:0.0, $
           spotlat:0.0, $
           procdate:''}

if n_elements(dsn) eq 0 then dsn = 'jul17j11g'
print,'including jupiterDataset.pro with dsn = ' + dsn

case dsn of

'jul17j11g': begin
  dataset.basename = 'jul17j11g'
  dataset.datafile = 'jupiter_11'
  dataset.date = '17/7/94'
  dataset.time = '04:46'
  dataset.wavelength = .55e-6
  dataset.filter = '550'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(40)
  dataset.nframes = 40
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = '/lgs3/lick_july94/jul_17/jul_17/alpha2lib_2'
  dataset.mtffile = 'mtf550.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 28.06 + (34.1-28.06)*.6
  dataset.selat = -2.99
  dataset.sslon = 17.42 + (23.46-17.42)*.6
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 750
  dataset.subny = 750
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '13/4/95'
  end

'jul17j14g': begin
  dataset.basename = 'jul17j14g'
  dataset.datafile = 'jupiter_14'
  dataset.date = '17/7/94'
  dataset.time = '05:37'
  dataset.wavelength = .7e-6
  dataset.filter = '700'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(20)
  dataset.nframes = 20
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = '/lgs3/lick_july94/jul_17/jul_17/alpha2lib_2'
  dataset.mtffile = 'mtf700.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 58.28 + (64.32-58.28)*.7
  dataset.selat = -2.99
  dataset.sslon = 47.64 + (53.68-47.64)*.7
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 900
  dataset.subny = 900
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '8/7/97'
  end

'jul17j16g': begin
  dataset.basename = 'jul17j16g'
  dataset.datafile = 'jupiter_16'
  dataset.date = '17/7/94'
  dataset.time = '05:39'
  dataset.wavelength = .85e-6
  dataset.filter = '850'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(20)
  dataset.nframes = 20
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = '/lgs3/lick_july94/jul_17/jul_17/alpha2lib_2'
  dataset.mtffile = 'mtf850.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 58.28 + (64.32-58.28)*.9
  dataset.selat = -2.99
  dataset.sslon = 47.64 + (53.68-47.64)*.9
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 900
  dataset.subny = 900
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '8/7/97'
  end

'jul17j18g': begin
  dataset.basename = 'jul17j18g'
  dataset.datafile = 'jupiter_18'
  dataset.date = '17/7/94'
  dataset.time = '05:42'
  dataset.wavelength = .7e-6
  dataset.filter = '700'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(20)
  dataset.nframes = 20
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps700'
  dataset.mtffile = 'mtf700.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 64.32 + (70.37-64.32)*.2
  dataset.selat = -2.99
  dataset.sslon = 53.68 + (59.73-53.68)*.2
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 642
  dataset.subposy = 303
  dataset.subnx = 256
  dataset.subny = 256
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul17j18': begin
  dataset.basename = 'jul17j18'
  dataset.datafile = 'jupiter_18'
  dataset.date = '17/7/94'
  dataset.time = '05:42'
  dataset.wavelength = .7e-6
  dataset.filter = '700'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(20)
  dataset.nframes = 20
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps700'
  dataset.mtffile = 'mtf700.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 64.32 + (70.37-64.32)*.2
  dataset.selat = -2.99
  dataset.sslon = 53.68 + (59.73-53.68)*.2
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 900
  dataset.subny = 900
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul17j19g': begin
  dataset.basename = 'jul17j19g'
  dataset.datafile = 'jupiter_19'
  dataset.date = '17/7/94'
  dataset.time = '05:44'
  dataset.wavelength = .85e-6
  dataset.filter = '850'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(20)
  dataset.nframes = 20
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps850'
  dataset.mtffile = 'mtf850.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_17/jul_17/'
  dataset.selon = 64.32 + (70.37-64.32)*.4
  dataset.selat = -2.99
  dataset.sslon = 53.68 + (59.73-53.68)*.4
  dataset.sslat = -3.12
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 645
  dataset.subposy = 304
  dataset.subnx = 256
  dataset.subny = 256
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul19j30g': begin
  dataset.basename = 'jul19j30g'
  dataset.datafile = 'jupiter_30'
  dataset.date = '19/7/94'
  dataset.time = '04:56'
  dataset.wavelength = .55e-6
  dataset.filter = '550'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(40)
  dataset.nframes = 40
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps550'
  dataset.mtffile = 'mtf550.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_19/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_19/'
  dataset.selon = 334.32+(340.36-334.32)*.7
  dataset.selat = -2.99
  dataset.sslon = 323.62+(329.67-323.62)*.7
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 360
  dataset.subposy = 222
  dataset.subnx = 256
  dataset.subny = 256
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul19j31': begin
  dataset.basename = 'jul19j31'
  dataset.datafile = 'jupiter_31'
  dataset.date = '19/7/94'
  dataset.time = '05:09'
  dataset.wavelength = .7e-6
  dataset.filter = '700'
  dataset.rotang = 16.
  dataset.tint = .3
  dataset.nframes = 24
  dataset.goodframes = [2,3,4,6,7, 8,9,10,11,12, 13,14,15,16,17, 25,26,27,30,32,34,36,37,38]
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps700'
  dataset.mtffile = 'mtf700.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_19/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_19/'
  dataset.selon =  346.41
  dataset.selat = -2.99
  dataset.sslon =  335.71
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 900
  dataset.subny = 900
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '9/7/97'
  end

'jul19j32': begin
  dataset.basename = 'jul19j32'
  dataset.datafile = 'jupiter_32'
  dataset.date = '19/7/94'
  dataset.time = '05:20'
  dataset.wavelength = .85e-6
  dataset.filter = '850'
  dataset.rotang = 16.
  dataset.tint = .3
  dataset.nframes = 20
  dataset.goodframes = [0,1,3,5,6,7,8,9,12,13,15,16,18,19,20,21,23,25,28,29]
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps850'
  dataset.mtffile = 'mtf850.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_19/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_19/'
  dataset.selon =  352.45
  dataset.selat = -2.99
  dataset.sslon =  341.76
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450
  dataset.subposy = 450
  dataset.subnx = 900
  dataset.subny = 900
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul19j32g': begin
  dataset.basename = 'jul19j32g'
  dataset.datafile = 'jupiter_32'
  dataset.date = '19/7/94'
  dataset.time = '05:20'
  dataset.wavelength = .85e-6
  dataset.filter = '850'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(35)
  dataset.nframes = 35
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps850'
  dataset.mtffile = 'mtf850.fits'
  dataset.datadir = '/lgs3/lick_july94/jul_19/'
  dataset.resultsdir = '/lgs3/lick_july94/jul_19/'
  dataset.selon =  352.45
  dataset.selat = -2.99
  dataset.sslon =  341.76
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 399
  dataset.subposy = 235
  dataset.subnx = 256
  dataset.subny = 256
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

'jul19j36g': begin
  dataset.basename = 'jul19j36g'
  dataset.datafile = 'jupiter_36'
  dataset.date = '19/7/94'
  dataset.time = '05:48'
  dataset.wavelength = .55e-6
  dataset.filter = '550'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(40)
  dataset.nframes = 40
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps550'
  dataset.mtffile = 'mtf550.fits'
  dataset.datadir = '/lgs1/lick_july94/jul_19/'
  dataset.resultsdir = '/lgs1/lick_july94/jul_19/'
  dataset.selon = 10.59
  dataset.selat = -2.99
  dataset.sslon = 359.89
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 375
  dataset.subposy = 375
  dataset.subnx = 750
  dataset.subny = 750
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '10/8/94'
  end

'jul20j20g': begin
  dataset.basename = 'jul20j20g'
  dataset.datafile = 'jupiter_20'
  dataset.date = '20/7/94'
  dataset.time = '02:04'
  dataset.wavelength = .85e-6
  dataset.filter = '850'
  dataset.rotang = 16.
  dataset.tint = .3
  goodset = indgen(40)
  dataset.nframes = 40
  dataset.goodframes(0:dataset.nframes-1) = goodset
  dataset.pixelsize = 0.063
  dataset.starfile = 'refps850'
  dataset.mtffile = 'mtf850.fits'
  dataset.datadir = '/lgs2/lick_july94/jul_20/jul_20'
  dataset.resultsdir = '/lgs2/lick_july94/jul_20/jul_20/'
  dataset.selon =  24.39
  dataset.selat = -2.98
  dataset.sslon =  13.67
  dataset.sslat = -3.13
  dataset.majoraxis = 38.2/2.
  dataset.minoraxis = (38.2/2.)/1.069
  dataset.subposx = 450 ; 508
  dataset.subposy = 450 ; 268
  dataset.subnx = 900 ; 256
  dataset.subny = 900 ; 256
  dataset.spotlon = 23    ; G impact
  dataset.spotlat = -43.59
  dataset.procdate = '23/6/97'
  end

else: print, 'dataset not defined: ', dsn
endcase

end
