;+
;  readtelem.pro - read the Lick AO telemetry files
;
; usage:
;   a = readtelem(filename,recsize,nrecords)
;
; where:
;   filename - text name of file
;   recsize - record size: 40 for centroids, 61 for actuators
;            (default: 40)
;   nrecords - number of records (default: 50)
;
;-
function readtelem,filename,recsize,nrecords
  if (n_elements(recsize) eq 0) then recsize = 40
  if (n_elements(nrecords) eq 0) then nrecords = 50
  openr,unit,filename,/get_lun
  t = assoc(unit,make_array(recsize,nrecords,/float))
  result = t(0)
  return,result
end
