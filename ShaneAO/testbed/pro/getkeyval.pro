function getkeyval,header,key,string=str
;function to take a fits header header and find and return the value
;of keyword key

keyloc=where(strpos(header,key) eq 0)
if (keyloc(0) lt 0) then begin
  return,'(no key:'+ key + ')'
endif
keyline=header(keyloc(0))
keystart=strpos(keyline,'=')
keyend=31; strpos(keyline,'/')
keylen=keyend-keystart
keyval=strmid(keyline,keystart+1,keylen-1)
if (n_elements(str) eq 0) then str = 0
if (str eq 0) then keyval=float(keyval)
return,keyval
end
