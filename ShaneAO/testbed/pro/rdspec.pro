;+
; DESCRIPTION:
;  rdspec - read a speckle format file
;
; USAGE:
;   sf = rdspec(filename, nframes)
;
; INPUTS:
;   filename - speckle file
;   nframes - total number of frames in the file
;-

function rdspec,filename,n
  header = intarr(48)
  openr,unit,filename,/get_lun
  readu, unit, header
  data = intarr(header(2),header(3),n)
  tmp = intarr(header(2),header(3))
  for i=0, n-1 do begin
    if i gt 0 then readu, unit, header
    readu, unit, tmp
    data(*,*,i) = tmp
    print,format='($,I0," ")',i
  endfor
  free_lun, unit
  print,'done'
  return,data
end
