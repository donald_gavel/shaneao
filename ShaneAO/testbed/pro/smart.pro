; Smart.pro
;
;  simulate a "smart" (point diffraction) interferometer
;
n = 256
i = complex(0.,1.)
pointSize = 2
ap = circle(n,n,n/2,n/2,n/4,1)
disp,ap,'aperture'
nz_case = [2,3,4]
mz_case = [0,1,0]
ncases = (size(nz_case))[1]
mag_set = fltarr(n,n,ncases)
ph_set = fltarr(n,n,ncases)
im_set = fltarr(n,n,ncases)
zoomi = 32
for theCase = 0,ncases-1 do begin
 nz = nz_case[theCase]
 mz = mz_case[theCase]
 ph = real(zernike(n,n,n/4,nz,mz))
 ph_set[*,*,theCase] = ph*ap
 wf = ap*exp(i*ph)
 wf_fp = ft(wf)
 point = 1-circle(n,n,n/2,n/2,pointSize,1)
 wf_fp2 = wf_fp*point
 im = abs(wf_fp2)
 imz = im[n/2-n/zoomi:n/2+n/zoomi-1,n/2-n/zoomi:n/2+n/zoomi-1]
 imz = rebin(imz,n,n)
 im_set[*,*,theCase] = imz
 wf_pu = ft(wf_fp2)
 mag_pu = abs(wf_pu)
 mag_set[*,*,theCase] = mag_pu
endfor
disp,reverse(reform(ph_set,n,n*ncases),2),'phase aberration'
disp,reverse(reform(alog10(im_set>1.e-3),n,n*ncases),2),'focal plane x pin block'
disp,reverse(reform(mag_set,n,n*ncases),2),'pinhole measurements'
for theCase = 0,ncases-1 do begin
 nz = nz_case[theCase]
 mz = mz_case[theCase]
 ph = real(zernike(n,n,n/4,nz,mz))
 ph_set[*,*,theCase] = ph*ap
 wf = ap*exp(i*ph)
 wf_fp = ft(wf)
 pointphase = exp(i*!pi/2*circle(n,n,n/2,n/2,pointSize,1))
 wf_fp2 = wf_fp*pointphase
 im = abs(wf_fp2)
 imz = im[n/2-n/zoomi:n/2+n/zoomi-1,n/2-n/zoomi:n/2+n/zoomi-1]
 imz = rebin(imz,n,n)
 im_set[*,*,theCase] = imz
 wf_pu = ft(wf_fp2)
 mag_pu = abs(wf_pu)
 mag_set[*,*,theCase] = mag_pu
endfor
disp,reverse(reform(alog10(im_set>1.e-3),n,n*ncases),2),'focal plane'
disp,reverse(reform(mag_set,n,n*ncases),2),'phase shift measurements'
end
