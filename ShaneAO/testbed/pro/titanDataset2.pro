;+
;  titanDataset2.pro - initialize the Titan dataset for speckle processing
;-

function getDefDatasetList
  list = [ 'june780', '342', '348', '458', '473', '476', '483', $
           '511', '514', '524', '531', '614' ]
  return,list
end

function getTitanDatasetDef,dsn,help=helpa

if (keyword_set(helpa)) then begin
  print,'usage: getTitanDatasetDef(dsn) where dsn is one of:'
  list = getDefDatasetList()
  n = (size(list))(1)
  for i = 0, n-1 do begin
    print,list(i)
  endfor
  return,0
endif

dataset = {datasetname:'', $
           basename:'', $
           datafile:'', $
           darkfile:'', $
           backgroundarea_x:[11,95], $
           backgroundarea_y:[170,231], $
           starname:'', $
           starfile:'', $
           stardarkfile:'', $
           skyfile:'', $
           skydarkfile:'', $
           flatfile:'', $
           flatdarkfile:'', $
           mtffile:'', $
           datadir:'', $
           resultsdir:''}

home = "/duck1/gavel/keck/sep96/"

; --------- set the dataset number here -----------
;if n_elements(dsn) eq 0 then dsn = '458'
;if n_elements(dsn) eq 0 then dsn = 'octKeckDummy458'
if n_elements(dsn) eq 0 then dsn = 'empty'
; -------------------------------------------------

dataset.datasetname = dsn

case dsn of

'empty': begin
  dataset.basename = ''
  dataset.datafile = ''
  dataset.darkfile = ''
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = ''
  dataset.starfile = ''
  dataset.stardarkfile = ''
  dataset.skyfile = ''
  dataset.skydarkfile = ''
  dataset.flatfile = ''
  dataset.flatdarkfile = ''
  dataset.mtffile = ''
  dataset.datadir = ''
  dataset.resultsdir = ''
  end

'octKeckDummy458': begin
  dataset.basename = 'titan458'
  dataset.datafile = 's00458.fits'
  dataset.darkfile = 's00457.fits'
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = 'star490'
  dataset.starfile = 's00490.fits'
  dataset.stardarkfile = 's00489.fits'
  dataset.skyfile = 's00499.fits'
  dataset.skydarkfile = 's00498.fits'
  dataset.flatfile = 's00711.fits'
  dataset.flatdarkfile = 's00710.fits'
  dataset.mtffile = '~/gavel/results/keckmtfKp.fits'
  dataset.datadir = '~/gavel/rawData/'
  dataset.resultsdir = '~/gavel/results/'
  end

'june780': begin
  dataset.basename = "star780"
  dataset.datafile = "s00780.fits"
  dataset.darkfile = '776'
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star749"
  dataset.starfile = "s00749.fits"
  dataset.stardarkfile = '745'
  dataset.skyfile = "s00807.fits"
  dataset.skydarkfile = '803'
  dataset.flatfile = "s00895.fits"
  dataset.flatdarkfile = "s00894.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = "/lgs2/keck_jun96_2/"
  dataset.resultsdir = "/duck1/gavel/keck/jun96/titan/psfstar780/"
  end
'342': begin
  dataset.basename = "doubleStar342"
  dataset.datafile = "s00342.fits"
  dataset.darkfile = "s00341.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star362"
  dataset.starfile = "s00362.fits"
  dataset.stardarkfile = "s00361.fits"
  dataset.skyfile = "s00350.fits"
  dataset.skydarkfile = "s00351.fits"
  dataset.flatfile = "s00689.fits"
  dataset.flatdarkfile = "s00688.fits"
  dataset.mtffile = home + "titan/keckmtfH"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/doubleStar342/"
  end
'348': begin
  dataset.basename = "doubleStar348"
  dataset.datafile = "s00348.fits"
  dataset.darkfile = "s00347.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star362"
  dataset.starfile = "s00362.fits"
  dataset.stardarkfile = "s00361.fits"
  dataset.skyfile = "s00350.fits"
  dataset.skydarkfile = "s00351.fits"
  dataset.flatfile = "s00689.fits"
  dataset.flatdarkfile = "s00688.fits"
  dataset.mtffile = home + "titan/keckmtfH"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/doubleStar342/"
  end
'458': begin
  dataset.basename = "titan458"
  dataset.datafile = "s00458.fits"
  dataset.darkfile = "s00457.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan458/"
  end
'473': begin
  dataset.basename = "titan473"
  dataset.datafile = "s00473.fits"
  dataset.darkfile = "s00472.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan473/"
  end
'476': begin
  dataset.basename = "titan476"
  dataset.datafile = "s00476.fits"
  dataset.darkfile = "s00475.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan476/"
  end
'483': begin
  dataset.basename = "titan483"
  dataset.datafile = "s00483.fits"
  dataset.darkfile = "s00482.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan483/"
  end
'511': begin
  dataset.basename = "star511"
  dataset.datafile = "s00511.fits"
  dataset.darkfile = "s00510.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/star511/"
  end
'514': begin
  dataset.basename = "star514"
  dataset.datafile = "s00514.fits"
  dataset.darkfile = "s00513.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star490"
  dataset.starfile = "s00490.fits"
  dataset.stardarkfile = "s00489.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/star514/"
  end
'524': begin
  dataset.basename = "titan524"
  dataset.datafile = "s00524.fits"
  dataset.darkfile = "s00523.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star517"
  dataset.starfile = "s00517.fits"
  dataset.stardarkfile = "s00516.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan524/"
  end
'531': begin
  dataset.basename = "titan531"
  dataset.datafile = "s00531.fits"
  dataset.darkfile = "s00530.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star517"
  dataset.starfile = "s00517.fits"
  dataset.stardarkfile = "s00516.fits"
  dataset.skyfile = "s00499.fits"
  dataset.skydarkfile = "s00498.fits"
  dataset.flatfile = "s00711.fits"
  dataset.flatdarkfile = "s00710.fits"
  dataset.mtffile = home + "titan/keckmtfKp"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan531/"
  end
'614': begin
  dataset.basename = "titan614"
  dataset.datafile = "s00614.fits"
  dataset.darkfile = "s00613.fits"
  dataset.backgroundarea_x = [11,95]
  dataset.backgroundarea_y = [170,231]
  dataset.starname = "star630"
  dataset.starfile = "s00630.fits"
  dataset.stardarkfile = "s00629.fits"
  dataset.skyfile = "s00595.fits"
  dataset.skydarkfile = "s00594.fits"
  dataset.flatfile = "s00688.fits"
  dataset.flatdarkfile = "s00689.fits"
  dataset.mtffile = home + "titan/keckmtfH"
  dataset.datadir = home + "rawData/"
  dataset.resultsdir = home + "titan/titan614/"
  end
else: begin
  print, "dataset not defined: " + dsn
  return,0
  end
endcase

return,dataset
end
