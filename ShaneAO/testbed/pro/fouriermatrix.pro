;
; FourierMatrix.pro
;    Create a matrix which is the equivalent of the Fourier Transform operator
;
; Usage:
;    F = FourierMatrix(ndim,n)
;
; Inputs:
;    ndim = number of dimensions (1 or 2)
;    n = size of transform
;    shift = 1 if you would like the spatial and frequency domain 0,0 coordinates are assumbed to be at the center, as opposed to at the first element, of the data set
;
function FourierMatrix,ndim,n,shift=shift
  if (ndim ne 2) then begin
    print,'one-diminsional transform not supported yet'
    return,0
  endif
  i = complex(0,1.)
  nn = n*long(n)
  x = ones(n) ## findgen(n)
  if (keyword_set(shift)) then x -= n/2
  y = transpose(x)
  x = reform(x,nn)
  y = reform(y,nn)
  kxx = transpose(x) ## x
  kyy = transpose(y) ## y
  f = exp(-2*!pi*i*(kxx+kyy)/float(n))
  return,f
end
;
; examples
test1 = 0
test2 = 1
test3 = 0
;   experiment with the forward gradient operator
;
if (test1) then begin
  n = 16
  nn = n*long(n)
  ker = [[0,0,0],[-1,1,0],[0,0,0]] ; convolution kernel, backward difference
  ker = [[0,0,0],[-.5,0,.5],[0,0,0]] ; central difference (average of forward and backward differences)
  Gx = fltarr(n,n,n,n)
  Q = fltarr(n,n)
  Q[0:2,0:2] = ker
  Q = shift(Q,-1,-1)
  for y=0,n-1 do begin
    for x=0,n-1 do begin
      Gx[*,*,x,y] = Q
      Q = shift(Q,1,0)
    endfor
    Q = shift(Q,0,1)
  endfor
  disp,reform(gx,n,n,nn),'gx'
  Gx = reform(Gx,nn,nn)
  F = FourierMatrix(2,n)
  Gtilde = F ## Gx ## conj(F) / float(nn)
endif ; test1

if (test2) then begin
  test = '<Fourier Matrix> Test 2: '
  title = 'Div of Pupiled Grad '
  print,test+title+' start' & wait,.01
  n = 16
  m = 16
  nn = long(n)*long(m)
  subapFilter = 1
  geometry = 'Fried'
  
;  set up the frequency domain
  i = dcomplex(0,1.)
  dkx = 2*!dpi/double(n)
  kx = (dindgen(n)-n/2) # ones(m)*dkx
  dky = 2*!dpi/double(m)
  ky = ones(n) # (dindgen(m)-m/2)*dky
  mask = 1-((kx eq 0) and (ky eq 0)) ; removes piston and Nyquist components
  mask[0,*] = 0.
  mask[*,0] = 0.
  if (subapFilter) then begin
      fsax = 2*sin(kx/2)/(kx+(kx eq 0))
      fsax += kx eq 0
      fsay = 2*sin(ky/2)/(ky+(ky eq 0))
      fsay += ky eq 0
      fsa = fsax*fsay
  endif else begin
      fsa = 1
  endelse
      
;      Gradient
  cx = i*kx*fsa*mask
  cy = i*ky*fsa*mask

  if (geometry eq 'Shearing') then begin
      cx *= exp(-i*kx*0.5)
      cy *= exp(-i*ky*0.5)
  endif
  if (geometry eq 'Fried') then begin
      cx *= exp(-i*(kx+ky)*0.5)
      cy *= exp(-i*(kx+ky)*0.5)
  endif

;      Pupil
  apdiam = 0.8*n
  ap = circle(n,n,n/2,n/2,apdiam/2,1.)
;  w = zeropad(window(apdiam,/radial),n,n)
  
;      Transform the derivative matrices (grad and div)
  Gx_tilde = diagonal(reform(cx,nn))
  Gy_tilde = diagonal(reform(cy,nn))
  P = diagonal(reform(ap,nn))
  F = FourierMatrix(2,n)
  Finv = (1.0/nn)*conj(F)
  Gpx = real(P ## Finv ## Gx_tilde ## F)
  Gpy = real(P ## Finv ## Gy_tilde ## F)
  A = transpose(Gpx) ## Gpx + transpose(Gpy) ## Gpy
  A_tilde = F ## A ## Finv
  sigma2 = 0. ; measurement noise covariance
  Q_tilde = sigma2*(mask*(kx^2+ky^2)+(1-mask))^(11./6.) ; regularizer
  C_tilde = diagonal(reform((1./(cx*conj(cx) + cy*conj(cy)+Q_tilde+(1-mask)))*mask,nn))
  
  Gx_inv_tilde = C_tilde ## conj(Gx_tilde) ; these are the reconstructors in the diagonal Fourier domain approximation
  Gy_inv_tilde = C_tilde ## conj(Gy_tilde)
  
  CA = real(Finv ## (C_tilde ## A_tilde) ## F)
  itergain = 1.0
  D = diagonal(ones(nn))-itergain*CA
  
;      Create a poke matix
  ap_act = circle(n,n,n/2,n/2,apdiam/2,1.)
  p_set = where(ap eq 1)
  a_set = where(ap_act eq 1)
  ns = (size(p_set))[1]
  na = (size(a_set))[1]
  Gpx_r = (Gpx[*,p_set])[a_set,*]
  Gpy_r = (Gpy[*,p_set])[a_set,*]
  Gp_r = [[Gpx_r],[Gpy_r]] ; this is the traditional poke matrix
  
;      Invert the poke matrix
  Gp_r_inv = pseudoinverse(gp_r,.01,modecount) ; this is the traditional reconstructor
  Gpx_r_inv = Gp_r_inv[0:ns-1,*]
  Gpy_r_inv = Gp_r_inv[ns:2*ns-1,*]
  
;     Determine the inverse poke matrix's Fourier domain representation  
  u = fltarr(nn,na)
  u[p_set,*] = Gpx_r_inv
  Gpx_inv = fltarr(nn,nn)
  Gpx_inv[*,a_set] = u
  u = fltarr(nn,na)
  u[p_set,*] = Gpy_r_inv
  Gpy_inv = fltarr(nn,nn)
  Gpy_inv[*,a_set] = u
  
  Gpx_inv_tilde = F ## Gpx_inv ## Finv ; These are the traditional reconstructors in the Fourier domain
  Gpy_inv_tilde = F ## Gpy_inv ## Finv

;      Create a test atmospheric screen
  seed = 5
  dtele = 8.
  du = dtele/double(apDiam)
  r00 = 0.2
  lambda = 1.6
  r0 = r00*(lambda/0.5)^(6./5.)
  fKol = screengen(n,n,r0,du)
  a = screengen(fKol,seed)*(lambda/(2*!dpi))
  fa = ft(a)
  fa *= mask  ; remove piston and Nyquist components
  a = double(ft(fa,/inverse)) ; this is the atmospheric phase screen
  fsx = cx*fa ; these are the slopes
  fsy = cy*fa
  sx = ft(fsx,/inverse)
  sy = ft(fsy,/inverse)
  sxp = sx*ap
  syp = sy*ap
  fsxp = ft(sxp)
  fsyp = ft(syp)

;    Do the reconstruction the traditional way
  a_ap = a[a_set]
  s_ap = Gp_r ## a_ap
  a_hat_ap = Gp_r_inv ## s_ap
  a_hat = fltarr(nn)
  a_hat[a_set] = a_hat_ap
  a_hat = reform(a_hat,n,n)
  print,test+title
  print,'max|a-ahat| = ',max(abs(a*ap - a_hat*ap)),'. <- This should be machine precision ~1e-8'
  
  s = Gpx
;      Find the eigenvalues and eigenvectors of the iteration, D
  At = D
  
;  trired,At,Dt,Et,/double
;  triql,Dt,Et,At,/double
  hes = elmhes(d)
  eval = hqr(hes)
  eval = eval[sort(abs(eval))]
;  evec = eigenvec(d,eval)
  
  Sphi = mask*((mask*(kx^2+ky^2)+(1-mask))^(-11./6.))
  dphi = Finv ## diagonal(reform(sphi,nn)) ## F
  de = dphi
  niter = 2
  for k = 0,niter-1 do begin
    de = d ## de ## transpose(d)
  endfor
  def = F ## P # de ## P ## Finv
  Se = abs(reform(diagonal(def),n,n))
  disp,[Sphi,Se],test+title+'Sphi, Se'
  
  window,/free,title=test+title+' eigenvalues' & plot,abs(eval)
;  disp,reform(real(evec),n,n,nn),test+title+' eigenvectors'
  print,test+title+' eigenvalue range: '
  print,min(eval),' ',max(eval)
  fuzz = 1d-9
  print,test+title+' number of eigenvalues > 1: ',total(abs(eval) gt (1+fuzz))
;   
  print,test+title+' done'
  
endif ; test2

end
