;+
;  psf.pro
;    calculates psf of diffraction-limited image
;-

function psf,n,lambda,d,dx,dsec=dsec
  x = ones(n) ## (findgen(n)-n/2)*dx
  y = transpose(x)
  r = sqrt(x^2+y^2)
  rr = r/(lambda/d)
  rr[n/2,n/2] = 0.001*dx
  psf = (rr ne 0)*2*(beselj(!pi*rr,1)/(!pi*rr)) + (rr eq 0)*1.0
  if (n_elements(dsec) ne 0) then begin
    rr = r/(lambda/dsec)
    rr[n/2,n/2] = 0.001*dx
    psf1 = (rr ne 0)*2*(beselj(!pi*rr,1)/(!pi*rr)) + (rr eq 0)*1.0
    psf1 = psf1*(dsec/d)^2
    psf = psf-psf1
  endif
  psf = psf^2
  return,psf
end
;
;  example code
arcsec = !pi/(180.*3600.)
microns = 1.e-6
band = 'laser';'I';'R'
lambda0 = 0.635*microns;0.8059*microns;0.65*microns; 0.55*microns
dlam = 0.1495*microns; 0.1436*microns; 0.0891*microns
d = 1.0
dsec = 0.33
dx = 0.025*arcsec
n = 64.;1024
nlam = 5
p = fltarr(n,n,nlam)
lam = (findgen(nlam)-nlam/2)*dlam/(nlam-1) + lambda0
for k=0,nlam-1 do begin
  p[*,*,k] = psf(n,lam[k],d,dx,dsec=dsec)
  print,k & wait,.01
endfor
;;p = psf(n,lambda,d,dx)
disp,p,'dsec='+strtrim(dsec,2)+' '+band+' band',dx=[1,1]*dx/arcsec,x0=-[1,1]*(n/2)*dx/arcsec
;window,/free
;u = radialavg(total(p,3),n/2,n/2)
;plot,u[0,*]*dx/arcsec,nlam*u[1,*]/max(u[1,*]),xrange = [0,.5],yrange=[0,1]
;for k=0,nlam-1 do begin
;  u = radialavg(p[*,*,k],n/2,n/2)
;;  if (k eq 0) then plot,u[0,*]*dx/arcsec,u[1,*]/max(u[1,*]),xrange=[0,1]
;;  if (k ne 0) then
;   oplot,u[0,*]*dx/arcsec,u[1,*]/max(u[1,*]),color=120
;  wait,.01
;endfor
end
