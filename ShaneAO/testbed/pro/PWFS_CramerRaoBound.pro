;
; PWFS_CramerRaoBound.pro
;   Calculate the CRLB for the PWFS under various conditions
;
function PWFS,U
  n = (size(U))[1]
  
  ; propagate to focal plane x
  V = float(n)*ft(U)
  V = fshift(V,0.5,0.5) ; center the beam on the apex
  H = ones(n,n)
  H[0:n/2,*] = 0 ; knife edge
  V *= H
  
  ; propagate to a pupil image plane w
  W = float(n)*ft(V)
  return,W
end

; constants
microns = 1.e-6
i = complex(0.,1.)

; parameters
n = 256 ; sample grid
dtele = 10.  ; diameter of telescope, meters
atele = !pi*(dtele/2.)^2
du = 2*dtele/float(n)   ;  fine grid pixel size, meters
d = 0.20 ; actuator spacing, meters
nph = 1.*atele/d^2   ; number of photons
r0 = 0.2 ; seeing parameter, meters

; do the CRLB calculations
mask = circle(n,n,n/2,n/2,(dtele/2)/du,1.)
dk = 1/(n*du)
kmax = 1/(2*d)
if (2*kmax/dk lt n) then lpf = zeropad(ones(2*kmax/dk,2*kmax/dk),n,n) else lpf = ones(n,n)
f = screengen(n,n,r0,du)*(1-lpf)
ph0 = screengen(f,seed)

; perturbing unit aberration
ux = ones(n) ## (findgen(n)-n/2)*du
uy = transpose(ux)
fx = 3/dtele
fy = 3/dtele
dph = sin(2*!pi*(fx*ux+fy*uy))
;dph = zernike(n,n,n/2,2,2,l=0) ; astigmatism
dph /= rms(dph,mask)
dph *= 1.

; wavefront at aperture
A = circle(n,n,n/2,n/2,(dtele/2)/du,1.)
A /= sqrt(total(A^2))
A *= sqrt(nph)
U = A*exp(i*(ph0+dph))
U0 = A*exp(i*ph0)

; photon counts at sensor
W = PWFS(U)
W0 = PWFS(U0)

; CRLB for Poisson statistics:
;  lambda is the mean count
;  Fisher information matrix for Poisson is F = Sum ( dlambda^2 / lambda )
;  where dlambda is the partial of lambda with respect to the parameter
;  the parameter is the coefficient of the perturbing unit aberation
;
dlambda = abs(W)^2-abs(W0)^2
lambda = abs(W0)^2

;disp,abs(W)^2-abs(W0)^2,'pupil plane'
lambda_inverse = mask*(1/(mask*lambda+(1-mask)))
F = total(mask*dlambda^2*lambda_inverse)
;print,'F = ',F
print,'sqrt(1/F) = ',sqrt(1./F)
print,'rms of unit perturbation: ',rms(dph,mask)
print,'CRLB = ',sqrt(1./F)*rms(dph,mask),' radians'
end

