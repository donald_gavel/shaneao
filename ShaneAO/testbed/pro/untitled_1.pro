  n_across = 7
  mask = ones(n_across,n_across)
  mask[0,0:1] = 0
  mask[1,0] = 0
  mask[3,3] = 0
  mask = mask * rotate(mask,1) * rotate(mask,2) * rotate(mask,3)

n=7
n2 = nextpow2(n)
poke = fltarr(n,n)
poke[5,3]=1
mask2 = fltarr(n,n)
mask2[0:6,0:6]=mask
;mask2 = ones(n,n)
geom = 'Hudgin'
disp,poke,'poke'
poke2 = zeropad(poke,n2,n2)
s = computeSlopes(poke2,differenceMethod='Fourier',geometry=geom)
disp,s,'slopes '+geom
ph = ftwfs(zeropad(s[*,*,0],n2,n2),zeropad(s[*,*,1],n2,n2),zeropad(mask2,n2,n2),niter=10,boundaryFix='iterative',geometry='Hudgin')
disp,poke+mask2
disp,ph+.5*zeropad(mask2,n2,n2)
end
