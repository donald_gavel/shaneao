;+
;  ellipse.pro - generate a filled ellipse
;
;   result = ellipse(n,m, cx,cy, ax,ay,value)
;-
function ellipse, n,m, cx,cy, ax,ay, value, alpha=alpha
  if n_elements(value) eq 0 then value = 1
  x = (findgen(n) # ones(m)) - cx
  y = (ones(n) # findgen(m)) - cy
  if n_elements(alpha) ne 0 then begin
    xp = x*cos(alpha)+y*sin(alpha)
    yp =-x*sin(alpha)+y*cos(alpha)
    x = xp
    y = yp
  endif
  r = sqrt((x/ax)^2 + (y/ay)^2)
  ret = (r lt 1.0)*value
  return,ret
end
; -- Mathematic Code for ellipse equation coefficients --
;
;xp := ca (x-x0) + sa (y-y0)
;yp := -sa (x-x0) + ca (y-y0)
;Collect[Expand[((xp/a)^2+(yp/b)^2)],{x,y}] // InputForm
;
;  (ca^2/a^2 + sa^2/b^2)*x^2 
;+ ((2*ca*sa)/a^2 - (2*ca*sa)/b^2)*x*y
;+ (ca^2/b^2 + sa^2/a^2)*y^2 
;+ ( ((-2*ca^2)/a^2 - (2*sa^2)/b^2)*x0  + ((-2*ca*sa)/a^2 + (2*ca*sa)/b^2)*y0)*x 
;+ ( ((-2*ca*sa)/a^2 + (2*ca*sa)/b^2)*x0 + ((-2*ca^2)/b^2 - (2*sa^2)/a^2)*y0)*y
;+ (ca^2/a^2 + sa^2/b^2)*x0^2 - (2*ca*sa)/b^2)*x0*y0 + (ca^2/b^2 + sa^2/a^2)*y0^2 + ((2*ca*sa)/a^2 
;
; = mu1*x^2 + mu2*x*y + mu3*y^2 + mu4*x + mu5*y + mu6
;
; 0) least-squares solve for mu1...mu6 given x,y data
; 1) solve for a^2,b^2,ca^2,sa^2=1-sa^2 using mu1,mu2,mu3 nonlinear equations
; 2) solve for x0,y0 using mu4,mu5 linear equations
; 3) check mu6
