;+
;  keckpsf.pro - create the Keck psf
;
;  USAGE:
;     p = keckpsf(n,lambda=l,dx=d)
;
;  INPUTS:
;     n - size of image in pixels
;     lambda - wavelength, m
;     dx - pixel scale, radians
;
;  OUTPUTS:
;     p - image of psf
;-
function keckpsf,n,lambda=lambda,dx=dx,ap=ap, Lseg=L
  microns = 1.e-6
  arcsec = !pi/(180.*3600.)
  if n_elements(dx) eq 0 then dx = 0.02 * arcsec ; LIRC plate scale
  if n_elements(lambda) eq 0 then lambda = 2.2 * microns ; K band
  du = lambda/(n*dx)
  dtele = 11. ; meters, longest baseline
  nqc = (lambda/dtele)/dx
  if (nqc lt 2.0) then print,'<keckpsf> caution: psf is under Nyquist-sampled'
  ap = keckap(n,lambda=lambda,du = du, Lseg=L)
  psf = abs(ft(ap))^2
  psf = psf/total(psf)
  return,psf
end
