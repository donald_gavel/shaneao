;
;  crop.pro
;    crop or zeropad an image
;    to make it a given size, while preserving pixel scale
;
function crop,a,nb,mb
	na = (size(a))[1]
	ma = (size(a))[2]
	typeCode = size(a,/type)
	b = make_array(nb,mb,type=typeCode)
	width = min([na,nb])
	na_range = na/2 + [-1,1]*width/2
	nb_range = nb/2 + [-1,1]*width/2
	height = min([ma,mb])
	ma_range = ma/2 + [-1,1]*height/2
	mb_range = mb/2 + [-1,1]*height/2

	b[nb_range[0]:nb_range[1]-1,mb_range[0]:mb_range[1]-1] = $
	  a[na_range[0]:na_range[1]-1,ma_range[0]:ma_range[1]-1]
	return,b
end
