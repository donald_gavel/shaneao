;+
;  Telescope.pro
;    Parametric data about the various telescopes
;    
;    Fills in the telescope data structure
;-

function telescope_new
t = {telescope, name:'', $
              n:0, $
              Dtele:0., $
              Dsec:0., $
              r00:0., $
              z: 0., $
              lambda0: 0., $
              d: 0., $
              v: 0., $
              snr: 0., $
              wfs_aliased: 0, $
              
              }
return,t


function telescope_define,t,name
@constants.pro

if (name eq 'Keck') then begin
  Dtele = 10.   ; Telescope aperture diameter
  dsec = 1.3   ; Telescope secondary diameter
  r00 = .14     ; Fried's seeing parameter
  z = 30.*degrees ; zenith angle
  lambda0 = 500.*nm  ; seeing parameter wavelength
  lambda = 1.*micron ; science wavelength
  d = 15.*cm    ; subaperture size
  v = 10.       ; wind velocity, m/s
  snr = 10.     ; signal-to-noise of wavefront sensor (depends on guide star brightness)
  wfsAliased = 0 ; (for NGAO, the LGSWFS does not use a spatial filter for anti-aliasing)
endif

if (name eq 'Nickel') then begin
  Dtele = 1
  dsec = 0.3
  r00 = .12
  z = 0.*degrees
  lambda0 = 500.*nm
  lambda = 550.*nm
  d = 11.*cm
  v = 10.
  snr = 10.
  wfsAliased = 1
endif

if (name eq 'Shane') then begin
  Dtele = 3.
  dsec = 0.8
  r00 = .12
  z = 0.*degrees
  lambda0 = 500.*nm
  lambda = 550.*nm
  d = 20.*cm
  v = 10.
  snr = 10.
  wfs_aliased = 0
endif

t.Dtele = Dtele
t.dsec = 1.3
t.r00 = r00
t.z = z

return,t
end

function telescope_deriveParameters,t
@constants.pro

  r0 = r00*(lambda/lambda0)^(6./5.)/cos(z)
  du = 8*Dtele/float(n)
  ux = (ones(n) ## findgen(n) - n/2)*du
  uy = transpose(ux)
  u = sqrt(ux^2+uy^2) ; spatial points
  dk = 1./(n*du) ; 1/(2*Dtele)
  kx = (ones(n) ## findgen(n) - n/2)*dk
  ky = transpose(kx)
  k = sqrt(kx^2+ky^2) ; spatial frequency points
  ; -- telescope MTF --
  if (Telescope eq 'Keck') then begin
    tau0 = abs(keckmtf(n,lambda=lambda,du=du))
    psf0 = abs(ft(tau0))
  endif else begin
    ap = circle(n,n,n/2,n/2,Dtele/2/du,1.) - circle(n,n,n/2,n/2,Dsec/2/du,1.)
    psf0 = abs(ft(ap))^2
    psf0 /= total(psf0)
    tau0 = abs(ft(psf0,/inverse))
  endelse
end

pro telescope
 print,'telescope.pro compiled'
return
