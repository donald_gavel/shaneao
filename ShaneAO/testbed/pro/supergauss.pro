;
;  supergauss.pro - form a supergaussian intensity profile
;

function supergauss,n,cx,cy,width,power
  result = make_array(n,n)
  x = extend(indgen(n),2,n)
  y = transpose(x)
  x = x - cx
  y = y - cy
  r = sqrt(x^2+y^2)
  fuzz = 10.^(-11)
  result = (r/width)^power
  result = exp( - (result<24))
  result = result*(result gt fuzz)
  return,result
end
