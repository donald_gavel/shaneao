;+
;  pyramidLenslet.pro
;    calculate the pupil images and difference data for a pyramid lenslet sensor
;-
function pyramidLenslet,wf,du,lenslet,wavelength=lambda
	microns = 1.e-6
	mm = 1.e-3
	i = complex(0.,1.)
	if (n_elements(lambda) eq 0) then lambda = 0.65*microns
	n = (size(wf))[1]
	f_beam = du^2*float(n)/lambda ; this makes dx = du at lenslet
	dx = du
	; propagate to the focal plane
	wf0 = ft(wf)
	wf0 = fshift(wf0,-.5,-.5)
	if (n_elements(lenslet) eq 0) then lensletArray = genLensletArray(D,du,n)
	wf1 = ft(lensletArray*wf0)
	im = abs(wf1)^2
	return,im
end
;
function genLensletArray,D,f_lenslet,du,n,wavelength=lambda
	microns = 1.e-6
	mm = 1.e-3
	i = complex(0.,1.)
	if (n_elements(lambda) eq 0) then lambda = 0.65*microns
;	f_beam = du^2*float(n)/lambda ; this makes dx = du at lenslet
	if (n_elements(D) eq 0) then D = 500.*microns
	f_number_lenslet = f_lenslet/D ; lenslet f-number

	; define a single lenslet's phase and aperture function
	n_l = fix(D/du)
	lensletAp = fltarr(n,n,4)
	lensletAp[n/2-n_l:n/2-1,n/2:n/2+n_l-1,0] = 1. ; upper left
	lensletAp[n/2:n/2+n_l-1,n/2:n/2+n_l-1,1] = 1. ; upper right
	lensletAp[n/2-n_l:n/2-1,n/2-n_l:n/2-1,2] = 1. ; lower left
	lensletAp[n/2:n/2+n_l-1,n/2-n_l:n/2-1,3] = 1. ; lower right

	a = !pi/(f_lenslet*lambda)
	x = (ones(n) ## (findgen(n)-n/2+0.5))*du
	y = ((findgen(n)-n/2+0.5) ## ones(n))*du

	x0 = [-1,1,-1,1]*(D/2.)
	y0 = [1,1,-1,-1]*(D/2.)
	lensletPhase = fltarr(n,n)
	for lenslet = 0,3 do lensletPhase += lensletAp[*,*,lenslet]*a*((x-x0[lenslet])^2+(y-y0[lenslet])^2)

	lensletArray = total(lensletAp,3)*exp(i*lensletPhase)
	return,lensletArray
end

; units
mm = .001 ; millimeters
microns = .000001 ; micrometers
nm = 1.0e-9 ; nanometers
i = complex(0.,1.)

; ---- parameters ----
n = 512
screensize = 1.2*mm ; overall extent of the calculated region
D = 500*microns ; lenslet size
pupil_diameter = 6.4*mm ; diameter of beam on SLM
lambda0 = 0.6328*microns ; central wavelength
delta_lambda = 150.*nm ; bandwidth
n_lambdas = 1; number of single lines, to simulate broad spectrum (pick an odd number)
f_lenslet = 5.0*mm;8.5*mm;5.0*mm; 7.5*mm ; focal length of lenslet
f_number_lenslet = f_lenslet/D
f_number_beam = 18.7; 24.
test = 'sin-freq';'pattern';'sin';'atmos' ; 'sin-angle' ; 'sin-freq'
n_cases = 1;7
r0 = D/8. ; for the atmos test
sinusoid_frequency = 3 ; for the sin test
amplitude = 0.1*lambda0 ; amplitude of the aberation OPD, for the sin test
sin_freq_min = 3. ; for the sin-freq test
sin_freq_max = 20. ; for the sin-freq test - the Nyquist limit is D/airy_size (~35 for the LAO setup)
do_Hilbert_ests = 0 ; compute, or not, the Hilbert transform wavefront estimates
nPh_per_pixel = 100. ; number of photons per resolution element
; ----------------------

dx = screensize/float(n) ; fine pixel size at lenslet array
lensletArray = genLensletArray(D,f_lenslet,dx,n)
;disp,abs(lensletArray)*unwrap(arg(lensletArray)),'lenslet array',dx=[1,1]*dx/mm,x0=-[1,1]*dx*(n/2-1)/mm

; signum function - for the Hilbert transform reconstructor
sgnx = (ones(n) ## findgen(n)-n/2+0.5)
sgnx = sgnx/(abs(sgnx)>1e-8)
sgny = transpose(sgnx)

focal_im_set = fltarr(n,n,n_cases)
pupil_im_set = fltarr(n,n,n_cases)
xdiff_set = fltarr(n,n,n_cases)
ydiff_set = fltarr(n,n,n_cases)
phestx_set = fltarr(n,n,n_cases)
phesty_set = fltarr(n,n,n_cases)

; cycle over wavelengths
dlambda = delta_lambda/float(n_lambdas)
lambda_set = fltarr(n_lambdas)

for lindex=-n_lambdas/2,n_lambdas/2 do begin

; form original pupil plane
	lambda = lambda0+lindex*dlambda
	print,lindex+n_lambdas/2,' of ',n_lambdas,' lambda = ',lambda/nm,' nm'
	f_beam = f_number_beam*pupil_diameter
	du = f_beam*lambda/(n*dx) ; fine pixel size at pupil (depends on the wavelength!)
	pupil = circle(n,n,n/2,n/2,pupil_diameter/(2.*du),1.)
	nPh = nPh_per_pixel*total(pupil) ; photons
	airy_size = 1.22*f_number_beam*lambda ; size of the focused spot (depends on wavelength!)

;  wavefront aberration
	x = (ones(n) ## (findgen(n)-n/2))*du
	y = ((findgen(n)-n/2) ## ones(n))*du
	r = sqrt(x^2+y^2)
	r = fshift(r,-0.5,-0.5)

	phase_set = fltarr(n,n,n_cases)

	if (test eq 'sin-angle') then begin
		fx_set = findgen(n_cases)*1.
		fy_set = fx_set
		angle_set = (findgen(n_cases)/float(n_cases))*2*!pi
		f = sinusoid_frequency
		fx_set = f*cos(angle_set)
		fy_set = f*sin(angle_set)
		for theCase = 0,n_cases-1 do begin
			fx = fx_set[theCase]
			fy = fy_set[theCase]
			phase = amplitude*sin(2*!pi*(x/(pupil_diameter/fx)+y/(pupil_diameter/fy)))
			phase_set[*,*,theCase] = phase*pupil
		endfor

	endif

	if (test eq 'sin-freq') then begin
		if (n_cases gt 1) then begin
			df = (sin_freq_max - sin_freq_min)/float(n_cases-1)
		endif else begin
			df = 0
		endelse
		fx_set = findgen(n_cases)*df + sin_freq_min
		fy_set = fltarr(n_cases)
		for theCase = 0,n_cases-1 do begin
			fx = fx_set[theCase]
			fy = fy_set[theCase]
			phase = amplitude*sin(2*!pi*(x/(pupil_diameter/fx)+y/(pupil_diameter/fy)))
			phase_set[*,*,theCase] = phase*pupil
		endfor

	endif

	if (test eq 'atmos') then begin
		forward_function blowingScreen_init,blowingScreen_get
		blowingScreen
		screen = blowingScreen_init(n,2*n,r0,du)
		for theCase = 0,n_cases-1 do begin
			t = theCase
			phase_set[*,*,theCase] = blowingScreen_get(screen,t)
		endfor
	endif

	if (test eq 'pattern') then begin
		width_of_line = 26.*microns/dx
		line_spacing = 26.*microns/dx
		phase = x*0
		n_lines = 7
		for line = 0,n_lines-1 do phase[n/2-width_of_line/2:n/2+width_of_line/2,line*line_spacing+n/2-float((n_lines-1)/2.)*line_spacing]=1
		for line = 0,n_lines-1 do phase[line*line_spacing+n/2-float((n_lines-1)/2.)*line_spacing,n/2-width_of_line/2:n/2+width_of_line/2]=1
	;	phase[n/2,*] = 1.
	;	phase[*,n/2] = 1.
	;	phase = rot(phase,45.)

		for theCase = 0,n_cases-1 do begin
		  phase_set[*,*,theCase] = phase*pupil
		endfor
	endif

	for theCase = 0,n_cases-1 do begin
		print,'<pyramidLenslet> ',theCase,' of ',n_cases & wait,.01
		phase = phase_set[*,*,theCase]

		; propagate to focal plane
		wf0 = pupil*exp(i*phase*2*!pi/lambda)
		airy = ft(wf0) ; Fraunhofer propagation
		airy = fshift(airy,-.5,-.5)
		focal_im_set[*,*,theCase] += abs(airy)^2

		; calculate wavefront just after lenslet
		wf = lensletArray*airy

		; propagate the light the focal length of the lenslet (pupil image plane)
		L = f_lenslet

		rayleighRange = D^2/lambda
		diffraction = L*lambda/D
		screensize = du*n
		;print,'rayleighRange: ',rayleighRange,' meters'
		;print,'diffraction: ',diffraction/mm,' mm'
		;print,'screensize: ',screensize/mm,' mm'

		propagator = 'fresnelft'
		du2 = dx
		if (propagator eq 'fresnel') then wfL = fresnel(wf,dx,L,lambda)
		if (propagator eq 'fresnelft') then wfL = fresnelft(wf,dx,1.,L,0.,1.,lambda,dx=du2,/check)

		im = abs(wfL)^2
		im = nPh*im/total(im)
		noise = randomn(seed,n,n)*sqrt(im)
		pupil_im_set[*,*,theCase] += im + noise

	endfor ; cases

endfor ; wavelengths


rms_set = fltarr(2,n_cases) ; one for xdiff, one for ydiff
print,'computing the wavefront estimates'
for theCase = 0,n_cases-1 do begin

	print,theCase,' of ',n_cases & wait,.01
	; calculate the geometric size of the pupil image
	pimage_size = f_lenslet/f_number_beam
	pimage = circle(n,n,n/2,n/2,pimage_size/(2*du2),1.)
	sx = (D/2.)/du2
	sy = sx
	pimageABCD = fshift(pimage,sx,sy)
	pimageABCD += fshift(pimage,sx,-sy)
	pimageABCD += fshift(pimage,-sx,sy)
	pimageABCD += fshift(pimage,-sx,-sy)
	pimageABCD = (pimageABCD gt 0.5) ; make it a binary mask

	; compute the pupil differences
	im = pupil_im_set[*,*,theCase]
	ima = im*pimageABCD
	xdiff = pimage*((fshift(ima,-sx,-sy)+fshift(ima,-sx,sy))-(fshift(ima,sx,-sy)+fshift(ima,sx,sy)))
	ydiff = pimage*((fshift(ima,-sx,-sy)+fshift(ima,sx,-sy))-(fshift(ima,-sx,sy)+fshift(ima,sx,sy)))
	xdiff_set[*,*,theCase] = xdiff
	ydiff_set[*,*,theCase] = ydiff

	; compute the rms by Jess's method
	pimage_in = circle(n,n,n/2,n/2,pimage_size*0.85/(2*du2),1.)
	rms_set[0,theCase] = rms(xdiff,pimage_in)
	rms_set[1,theCase] = rms(ydiff,pimage_in)

	if (do_Hilbert_ests) then begin
		; compute an estimate of the phase using the Hilbert transform
		f_xdif = ft(xdiff_set[*,*,theCase])
		phest = ft(-f_xdif*i*sgnx,/inverse) ; Hilbert transform
		phestx_set[*,*,theCase] = real(phest)

		f_ydif = ft(ydiff_set[*,*,theCase])
		phest = ft(-f_ydif*i*sgny,/inverse)
		phesty_set[*,*,theCase] = real(phest) ; Hilbert transform
	endif

endfor ; cases

disp,focal_im_set,'focal images at lenslet array',dx=[1,1]*dx/mm,x0=-[1,1]*dx*(n/2-1)/mm
disp,pupil_im_set,'pupil images',dx=[1,1]*du2/mm,x0=-[1,1]*du2*(n/2-1)/mm
disp,-xdiff_set,'xdiff',dx=[1,1]*du2,x0=-[1,1]*dx*(n/2)/mm
disp,-ydiff_set,'ydiff',dx=[1,1]*du2,x0=-[1,1]*dx*(n/2)/mm

;disp,phase_set,'phase',ap=pupil,dx=[1,1]*du/mm,x0=-[1,1]*(n/2-1)*du/mm
;disp,phestx_set,'phase estimate (x)',ap=pupil,dx=[1,1]*du/mm,x0=-[1,1]*(n/2-1)*du/mm
;disp,phesty_set,'phase estimate (y)',ap=pupil,dx=[1,1]*du/mm,x0=-[1,1]*(n/2-1)*du/mm
;disp,(phestx_set+phesty_set)/2.,'phase estimate (x,y average)',ap=pupil,dx=[1,1]*du/mm,x0=-[1,1]*(n/2-1)*du/mm

window,/free
m = max(rms_set)
plot,fx_set,rms_set[0,*]/m,charsize=2,title='RMS signal',xtitle='cycles/pupil',ytitle='Arbitrary Units'
oplot,fx_set,rms_set[0,*]/m,psym=1
oplot,fx_set,rms_set[1,*]/m
oplot,fx_set,rms_set[1,*]/m,psym=2
end
