;
; IDL Event Callback Procedures
; disp_eventcb
;
; Generated on:	02/16/2003 15:15.53
;
;----------------------------------------------------------------
;   Modify disp.pro as follows:
;pro disp, arg, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
;  if (n_elements(arg) eq 0) then begin
;    dispBase, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
;  endif else begin
;    dispBase, GROUP_LEADER=wGroup, data=arg, _EXTRA=_VWBExtra_
;  endelse
;end
;-----------------------------------------------------------------
; Post Create Widget Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;   Any keywords passed into the generated widget creation procedure
;       are passed into this procudure.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_createSliderGroup, wWidget, _EXTRA=_VWBExtra_
  parent = widget_info(wWidget,/parent)
  displayBase = widget_info(parent,FIND_BY_UNAME='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  data = dataInfo.data
  dataMin = dataInfo.min
  dataMax = dataInfo.max
  minSlider = cw_fslider(wWidget,title='min',minimum=dataMin,maximum=dataMax, $
       value=dataMin,/edit,uname='minSlider')
  maxSlider = cw_fslider(wWidget,title='max',minimum=dataMin,maximum=dataMax, $
       value=dataMax,/edit,uname='maxSlider')
end
;-----------------------------------------------------------------
; Handle Event Callback Function.
; Argument:
;   Event structure:
;
;   {WIDGET_<type>, ID:0L, TOP:0L, HANDLER:0L,...}
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
function dispWidget_fSliderEvent, Event

  displayBase = widget_info(Event.top,FIND_BY_UNAME='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  widget_control,Event.id,get_value=value
  name = widget_info(Event.id,/uname)

  case name of
    'minSlider': begin
      dataInfo.displayMin = value
    end
    'maxSlider': begin
      dataInfo.displayMax = value
    end
    else: begin
    end
  endcase

  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase,/forceSecondaryDraw

  return, Event ; By Default, return the event.

end
;-----------------------------------------------------------------
; Handle Event Callback Function.
; Argument:
;   Event structure:
;
;   {WIDGET_<type>, ID:0L, TOP:0L, HANDLER:0L,...}
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
function dispWidget_displayEvent, Event

     return, Event ; By Default, return the event.

end
;
; Empty stub procedure used for autoloading.
;
pro dispWidget_eventcb
end
;-----------------------------------------------------------------
; Post Create Widget Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;   Any keywords passed into the generated widget creation procedure
;       are passed into this procudure.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_populateButtonSet, wWidget, _EXTRA=_VWBExtra_

  panButton = widget_button(wWidget,uname='panButton',  $
      tooltip='pan', $
      scr_xsize=22 ,scr_ysize=22, value='pan.bmp' ,/bitmap)
  zoomButton = widget_button(wWidget,uname='zoomButton', $
      tooltip='left: zoom in; right: zoom out', $
      scr_xsize=22 ,scr_ysize=22, value='zoom.bmp' ,/bitmap)
  zoomSelectButton = widget_button(wWidget,uname='zoomSelectButton', $
      tooltip='select zoom region', $
      scr_xsize=22 ,scr_ysize=22, value='rectangl.bmp' ,/bitmap)
  lineoutButton = Widget_Button(wWidget, uname='lineoutButton',  $
      tooltip='lineout, left: horizontal, right: vertical', $
      scr_xsize=22 ,scr_ysize=22, value='line.bmp' ,/bitmap)
  surfaceButton = Widget_Button(wWidget, uname='surfaceButton',  $
      tooltip='shaded surface plot', $
      scr_xsize=22 ,scr_ysize=22,value='surface.bmp' ,/bitmap)
  contourButton = Widget_Button(wWidget, uname='contourButton', $
      tooltip='contour map', $
      scr_xsize=22 ,scr_ysize=22,value='contour.bmp' ,/bitmap)

  widget_control,panButton,/set_button
end
;-----------------------------------------------------------------
; Post Create Widget Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;   Any keywords passed into the generated widget creation procedure
;       are passed into this procudure.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_createDisplay, wWidget, data=data, name=name, dx=dx, x0=x0, demo=demo, ap=ap, baseID=pDispBase,_EXTRA=_VWBExtra_

  dispBase = widget_info(wWidget,/parent)
  if ((n_elements(baseID) eq 0) and (ptr_valid(pDispBase))) then *pDispBase = dispBase
  if (n_elements(demo) eq 0) then demo = 5 ; "demo" is number of frames in a demo

  if (n_elements(name) eq 0) then begin
    name = 'Disp #'+strcompress(string(dispBase))
  endif
  widget_control,dispBase,tlb_set_title=name
 ; print,'<dispWidget_createDisplay> '+name+'('+string(dispBase)+')'

  if (n_elements(x0) eq 0) then x0 = [0,0]
  if (n_elements(dx) eq 0) then dx = [1,1]
  if (n_elements(data) eq 0) then begin ; create "demo" data - Gabor patterns
;    data = ones(32) ## findgen(32)
    n = 32
    x = ones(n) ## findgen(n) & y = transpose(x)
    kx = 0.1
    ky = 0.2
    if (demo eq 1) then begin
      data = sin(2*!pi*(kx*x+ky*y))*gauss2(n,n/2,n/2,n/4.)
    endif else begin
      nframes = demo > 1 < 50
      data = findgen(n,n,nframes)
      for k=0,nframes-1 do begin
        t = !pi*k/float(nframes)
        s = sin(t)
        c = cos(t)
        data[*,*,k] = sin(2*!pi*(kx*s*x + ky*c*y))*gauss2(n,n/2,n/2,n/4.)
      endfor
    endelse
    if (n_elements(ap) eq 0) then begin
      ap = circle(n,n,n/2,n/2,n/2,1.)
    endif
  endif

  pa = 0
  dataMin = 0
  dataMax = 1
  mode = 'pan'
  scale = 'linear'
  external_pointer = 0

;  --------- type checks ----------
  types = ['undefined','byte','int','long','float','double', $
           'complex','string','structure','doubleComplex','pointer']
  type = typeof(data)

;  -- dereferenced types, structure and pointer --
	if (type eq 8) then begin ; structure
	  if (total(tag_names(data) eq ['DATA','DX','X0']) eq 3) then begin
		  pa = data.data
		  dx = data.dx
		  x0 = data.x0
		  type = typeof(*pa)
		  external_pointer = 1
		endif else begin
		  r = dialog_message('<dispWidget_createDisplay> invalid image structure',/error)
			mode = 'invalid'
		endelse
	endif else begin

	  if (type eq 10) then begin ; pointer
	  	pa = data
	  	external_pointer = 1
	  	type = typeof(*pa)
	 	endif else begin
	 		pa = ptr_new(data)
		endelse

	endelse

;  -- direct types --
  if (total(type eq [6,9])) then begin  ; complex types: show abs value
    r=dialog_message('<dispWidget_createDisplay> complex data type - displaying the absolute value')
    adata = abs(*pa)
    if (external_pointer eq 0) then ptr_free,pa
    pa = ptr_new(adata)
    dataMax = max(*pa,/nan) & dataMin = min(*pa,/nan)
  endif

  if (type eq 1) then begin ; byte type
    print,'<dispWidget_createDisplay> byte type - displaying as integer'
    idata = fix(*pa)
    dataMax = max(idata,/nan) & dataMin = min(idata,/nan)
    if (external_pointer eq 0) then ptr_free,pa
    pa = ptr_new(idata)
  endif

  if (total(type eq [2,3,4,5])) then begin ; other valid types
    dataMax = max(*pa,/nan) & dataMin = min(*pa,/nan)
  endif

  if (total(type eq [0,7,8,10])) then begin ; invalid type
    r=dialog_message('<dispWidget_createDisplay> type '+types[type]+' not supported',/error)
    mode = 'invalid'
    if (external_pointer eq 0) then ptr_free,pa
    pa = 0
  endif

;  --------- end of type checks -------
  if (mode ne 'invalid') then begin
  	s = size(*pa)

	  if ((s[0] ne 2) and (s[0] ne 3)) then begin
	    r=dialog_message('<dispWidget_createDisplay> invalid number of dimensions; must be 2 or 3',/error)
	    mode = 'invalid'
	  endif

    if (dataMin ge dataMax) then begin
      result = dialog_message('<dispWidget_createDisplay> Invalid Data (min>max)',/error)
      dataMin = 0.
      dataMax = 1.
      mode = 'invalid'
    endif
  endif

  if (mode ne 'invalid') then begin
    nx = s[1]
    ny = s[2]
    if (s[0] eq 3) then nframes = s[3] else nframes = 1
    box = [0,nx-1,0,ny-1]
    cd,current=dir  ; current working directory

    if (n_elements(ap) eq 0) then ap = ones(nx,ny)
    sa = size(ap)
    if ((sa[1] ne nx) or (sa[2] ne ny)) then begin
      result = dialog_message('<dispWidget_createDisplay> Invalid mask dimensions '+strcompress(string(sa[0])+','+string(sa[1])),/error)
      ap = ones(nx,ny)
    endif

    pap = ptr_new(ap)
  endif else begin
    nx = 1
    ny = 1
    nframes = 1
    pap = 0
    box = [0,0,0,0]
    cd,current = dir
  endelse

  dataInfo = {data:pa, name:name, nx:nx, ny:ny, nframes:nframes, frame:0, $
    max:dataMax, min:dataMin, displayMin:dataMin, displayMax:dataMax, $
    box:box, dx:dx, x0:x0, mode:mode, scale:scale, ap:pap, dir:dir, $
    mouseButtonState:0, mouseButtonStartPosn:[0,0,0,0], ax:30., az:30., external_pointer:external_pointer, movieWidget:0L}
  widget_control,wWidget,set_uvalue = dataInfo
;  print,'<dispWidget_createDisplay> displayBase = ',wWidget

  if (mode eq 'invalid') then return ; allow widgets to realize.  Will later destroy itself on mouse motion

  if (nframes gt 1) then begin
    frameSlider = Widget_Slider(wWidget, /vertical, $
      UNAME='frameSlider',frame=1,ysize=256,scr_xsize=36,scr_ysize=256,minimum=0,maximum=nframes-1,value=0, $
      event_pro='dispWidget_frame')
  endif
end
;
pro dispWidget_frame,Event
  widget_control,Event.id,get_value=frame
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.frame = frame
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase,/forceSecondaryDraw
end
;-----------------------------------------------------------------
; TLB_SIZE_EVENTS Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BASE, ID:0L, TOP:0L, HANDLER:0L, X:0, Y:0 }
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   The X and Y fields return the new width of the base, not
;       including any frame provided by the window manager.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_resizeWindow, Event
;  fudge terms to get the -real- window size:
  windowBorder = [8,45]

  widget_control,Event.top,tlb_get_size = newSize
;  print,'<dispWidget_resizeWindow> newSize = ',Event.top,newSize
  x = Event.x
  y = Event.y
;  print,'<dispWidget_resizeWindow> x, y = ',x,y
  scr_x = x+windowBorder[0]
  scr_y = y+windowBorder[1]

  rightDisplayMargin = 120
  bottomMargin = 105

  displayBase = widget_info(Event.top,FIND_BY_UNAME='displayBase')
  mainDraw = widget_info(Event.top,FIND_BY_UNAME='mainDraw')
  displayMarginFrame = widget_info(Event.top,FIND_BY_UNAME='displayMarginFrame')
  buttonSet = widget_info(Event.top,FIND_BY_UNAME='buttonSet')
  bottomFrame = widget_info(Event.top,FIND_BY_UNAME='bottomFrame')
  minSlider = widget_info(bottomFrame,FIND_BY_UNAME='minSlider')
  maxSlider = widget_info(bottomFrame,FIND_BY_UNAME='maxSlider')
  frameSlider = widget_info(Event.top,FIND_BY_UNAME='frameSlider')

  if (widget_info(frameSlider,/valid_id)) then begin
      geo = widget_info(frameSlider,/geometry)
      frameSliderWidth = geo.xsize
;    frameSliderWidth = 36
  endif else begin
    frameSliderWidth = 0
  endelse

  geo = widget_info(bottomFrame,/geometry)
  bottomMargin = geo.scr_ysize
  geo = widget_info(displayMarginFrame,/geometry)
  rightDisplayMargin = geo.scr_xsize
  frameMargin = 3

  widget_control,Event.top,scr_xsize=scr_x,scr_ysize=scr_y
  widget_control,displayBase,scr_xsize=x-2*frameMargin,scr_ysize=y-bottomMargin-4*frameMargin
  if (widget_info(frameSlider,/valid_id)) then widget_control,frameSlider,scr_ysize=y-bottomMargin-12
  widget_control,mainDraw,scr_xsize=x-rightDisplayMargin-frameSliderWidth-6*frameMargin,scr_ysize=y-bottomMargin-6*frameMargin
  widget_control,displayMarginFrame,scr_ysize=y-bottomMargin-2*frameMargin,scr_xsize=rightDisplayMargin
  widget_control,bottomFrame,scr_xsize=x-2*frameMargin
  widget_control,minSlider,scr_xsize=(x-3)/2-9
  widget_control,maxSlider,scr_xsize=(x-3)/2-9

  dispWidget_redrawDisplay,displayBase

;  widget_control,Event.top,tlb_get_size = newSize
;  print,'<dispWidget_resizeWindow> new(Re)Size = ',Event.top,newSize

end
;-----------------------------------------------------------------
; Notify Realize Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_debug, wWidget
;  geom = widget_info(wWidget,/geometry)
;  print,'<dispWidget_debug> initial geometry'
;  print,'offset ',geom.xoffset,geom.yoffset
;  print,'size ',geom.xsize,geom.ysize
;  print,'scr size ',geom.scr_xsize,geom.scr_ysize
;  print,'draw size ',geom.draw_xsize,geom.draw_ysize
;  print,'margin ',geom.margin
;  print,'pad ',geom.xpad,geom.ypad
;  print,'space ',geom.space
;  widget_control,wWidget,tlb_get_size=winSize
;  print,'<dispWidget_debug> winSize = ',winSize
end
;-----------------------------------------------------------------
; Notify Realize Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_realizeDisplay, wWidget
  DEVICE, Decomposed=0, Bypass_Translation=0
  defsysv, "!image", exists = exists
  if (exists eq 0) then imagelib
  defsysv, "!aspect", exists=exists
  if (exists eq 0) then devicelib
  dispWidget_redrawDisplay, wWidget
  dispBase = widget_info(wWidget,/parent)
  geo = widget_info(dispBase,/geometry)
  x = geo.scr_xsize
  y = geo.scr_ysize
  ; originally:
  ; x = 404
  ; y = 421
  dispWidget_resizeWindow, {id:dispBase, top:dispBase, handler:0L,  x:x, y:y}
  defsysv, "!disp", exists = exists
  if (exists eq 0) then begin
  	defsysv,"!disp",{disp,activeWindow:dispBase,winCount:0,windows:lonarr(100)}
  endif
  !disp.activeWindow = dispBase
  !disp.windows[!disp.winCount] = dispBase
  !disp.winCount++
  !quiet=1
end
;-----------------------------------------------------------------
pro dispWidget_newData,wWidget,data,rescale=rescale
  displayBase = widget_info(wWidget,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  ptr_free,dataInfo.data
  dataInfo.data = ptr_new(data)
  if (keyword_set(rescale)) then begin
    dataInfo.min = min(data)
    dataInfo.max = max(data)
    dataInfo.displayMin = dataInfo.min
    dataInfo.displayMax = dataInfo.max
    minSlider = widget_info(wWidget,find_by_uname='minSlider')
    maxSlider = widget_info(wWidget,find_by_uname='maxSlider')
    widget_control,minSlider,get_value=minSliderValue
    widget_control,maxSlider,get_value=maxSliderValue
    widget_control,minSlider,set_value=[minSliderValue,dataInfo.displayMin,dataInfo.displayMax]
    widget_control,maxSlider,set_value=[maxSliderValue,dataInfo.displayMin,dataInfo.displayMax]
  endif
  widget_control,displayBase,set_uvalue=dataInfo
  if (keyword_set(rescale)) then begin
    dispWidget_redrawDisplay,displayBase
  endif else begin
    dispWidget_redrawDisplay,displayBase,/no_rescale
  endelse
end
;-----------------------------------------------------------------
pro dispWidget_redrawDisplay, wWidget,noerase=noerase,forceSecondaryDraw=forceSecondaryDraw,no_rescale=no_rescale
  widget_control,wWidget,get_uvalue=dataInfo
  if (dataInfo.mode eq 'invalid') then begin
    return
  endif
  box = dataInfo.box
  x0 = dataInfo.x0
  dx = dataInfo.dx
  frame = dataInfo.frame
  ap = (*dataInfo.ap)[box[0]:box[1],box[2]:box[3]]
  scale = dataInfo.scale
  eps = 1.e-16

  zoomedData = (*dataInfo.data)[box[0]:box[1],box[2]:box[3],frame] > dataInfo.displayMin < dataInfo.displayMax

  if (scale eq 'linear') then begin
    dataMin = dataInfo.displayMin
    dataMax = dataInfo.displayMax
  endif
  if (scale eq 'log') then begin
    eps *= max(zoomedData,/nan)
    zoomedData = alog10(zoomedData > eps)
    dataMin = alog10(dataInfo.displayMin > eps)
    dataMax = alog10(dataInfo.displayMax > (2*eps))
  endif
  if (scale eq 'sqrt') then begin
    eps *= max(zoomedData,/nan)
    zoomedData = sqrt(zoomedData > eps)
    dataMin = sqrt(dataInfo.displayMin > eps)
    dataMax = sqrt(dataInfo.displayMax > (2*eps))
  endif
  if (scale eq 'logAbs') then begin
    zoomedData = abs(zoomedData)
    eps *= max(zoomedData,/nan)
    zoomedData = alog10(zoomedData > eps)
    dataMin = alog10(dataInfo.displayMin > eps)
    dataMax = alog10(dataInfo.displayMax > (2*eps))
  endif
  if (scale eq 'sqrtAbs') then begin
    zoomedData = abs(zoomedData)
    zoomedData = sqrt(zoomedData > (eps*max(zoomedData,/nan)))
    dataMin = sqrt(dataInfo.displayMin > eps)
    dataMax = sqrt(dataInfo.displayMax > (2*eps))
  endif

  xrange = box[0:1]+[0,1]
  yrange = box[2:3]+[0,1]

; playing with new idea to display the pixel-scaled range
  xrange = [x0[0],x0[0]] + [box[0],box[1]+1]*dx[0]
  yrange = [x0[1],x0[1]] + [box[2],box[3]+1]*dx[1]
  if (not keyword_set(no_rescale)) then begin
    dataMin = min(zoomedData*ap,/nan) > dataMin; dataInfo.displayMin
    dataMax = max(zoomedData*ap,/nan) < dataMax; dataInfo.displayMax
  endif
  zrange = dataMax-dataMin

  mainDraw = widget_info(wWidget,find_by_uname='mainDraw')
  widget_control,mainDraw,get_value=win
  wset,win

  case dataInfo.mode of
    'contour': begin
      tvdevice,win
      exptv, zoomedData*ap+(1-ap)*dataMin, /noexact, /data, relative = .8,color=255
      tvaxis, xaxis = 0, yaxis = 0, xrange=xrange, yrange=yrange
      tvaxis, xaxis = 1, yaxis = 1, xrange=xrange, yrange=yrange
      contour,zoomedData*ap,nlevels=12,/overplot
    end
    'surface': begin
      if (keyword_set(noerase)) then !p.noerase = 1
      eps = zrange*1.e-3
      shade_surf,zoomedData*ap+(1-ap)*(dataMin-eps), $
        ax=dataInfo.ax,az=dataInfo.az, $
        min_value=dataMin,max_value=dataMax
      !p.noerase = 0
      if (not keyword_set(forceSecondaryDraw)) then return
    end
    else: begin
      tvdevice,win
      exptv, zoomedData*ap+(1-ap)*dataMin, /noexact, /data, relative = .8,color=255
      tvaxis, xaxis = 0, yaxis = 0, xrange=xrange, yrange=yrange
      tvaxis, xaxis = 1, yaxis = 1, xrange=xrange, yrange=yrange
    end
  endcase

  secondaryDraw = widget_info(wWidget,find_by_uname='secondaryDraw')
  widget_control,secondaryDraw,get_value=win
  wset,win
  tvdevice,win
  ap = *dataInfo.ap
  exptv, ((*dataInfo.data)[*,*,frame])*ap+(1-ap)*dataInfo.displayMin > dataInfo.displayMin < dataInfo.displayMax, /noexact, /data, color=255
  boxSize = [box[1]-box[0],box[3]-box[2]]
  boxCenter = [box[0],box[2]]+boxSize/2
  tvDrawBox,boxSize,boxCenter[0],boxCenter[1],/data
end
;-----------------------------------------------------------------
; MOTION_EVENTS Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_DRAW, ID:0L, TOP:0L, HANDLER:0L, TYPE: 0, X:0, Y:0,
;       PRESS:0B, RELEASE:0B, CLICKS:0}

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   TYPE returns a value that describes the type of draw widget
;       interaction that generated an event: 0 - Button Press, 1 -
;       Button Release, 2 - Motion, 3 - Viewport Moved, 4 -
;       Visibility Changed (Expose)


;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_mouseMotion, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  valueLabel = widget_info(Event.top,find_by_uname='valueLabel')
  indexLabel = widget_info(Event.top,find_by_uname='indexLabel')
  positionLabelX = widget_info(Event.top,find_by_uname='positionLabelX')
  positionLabelY = widget_info(Event.top,find_by_uname='positionLabelY')
  widget_control,displayBase,get_uvalue=dataInfo
  data = dataInfo.data
  ap = dataInfo.ap
  frame = dataInfo.frame
  box = dataInfo.box
  dx = dataInfo.dx
  x0 = dataInfo.x0

  if (dataInfo.mode eq 'invalid') then begin
    dispWidget_destroyDisplay,displayBase
    widget_control,Event.top,/destroy
    return
  end
  if (dataInfo.mode eq 'surface') then begin
    if (dataInfo.mouseButtonState ne 1) then return
    x = Event.x - dataInfo.mouseButtonStartPosn[0]
    y = Event.y - dataInfo.mouseButtonStartPosn[1]
    dataInfo.ax = fix(-y*180./256.) + dataInfo.mouseButtonStartPosn[2]
    dataInfo.az = fix(x*360./256.) + dataInfo.mouseButtonStartPosn[3]
    widget_control,displayBase,set_uvalue=dataInfo
    dispWidget_redrawDisplay,displayBase,/noerase
    return
  end

  widget_control,Event.id,get_value=win
  if (event.type eq 2) then begin  ; mouse motion, print values
    tvdevice,win
    get_tv_scale,sx,sy,mx,my,jx,jy
    tvpos,x,y,wait=0
;    x = fix(x)  &  y = fix(y)

    if ((x ge 0) and (y ge 0) and (x lt sx) and (y lt sy)) then begin
      x = x+box[0]
      y = y+box[2]
      z = (*data)[x,y,frame]
      xr = x*dx[0]+x0[0]  ; xr = scaled-pixel x coordinate
      yr = y*dx[1]+x0[1]
      x = fix(x) & y = fix(y)
      str = '['+strcompress(string(x))+','+strcompress(string(y))+']'
      widget_control,indexLabel,set_value=str
      str = 'x = '+strcompress(string(xr),/remove_all)
      widget_control,positionLabelX,set_value=str
      str = 'y = '+strcompress(string(yr),/remove_all)
      widget_control,positionLabelY,set_value=str
      if ((*ap)[x,y]) then begin
        str = strcompress(string(z,format='(g10.3)'))
      endif else begin
        str = ''
      endelse
      widget_control,valueLabel,set_value=str
    endif else begin
      widget_control,valueLabel,set_value=''
      widget_control,indexLabel,set_value=''
    endelse
  endif
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_colorMap, Event
  displayBase = widget_info(Event.top,FIND_BY_UNAME='displayBase')
  xloadct,/use_current,updatecallback = 'dispWidget_colorMapUpdate',updatecbdata=displayBase
end
;
pro dispWidget_colorMapUpdate,data=displayBase
  dispWidget_redrawDisplay,displayBase,/forceSecondaryDraw
end;-----------------------------------------------------------------
; Handle Event Callback Function.
; Argument:
;   Event structure:
;
;   {WIDGET_<type>, ID:0L, TOP:0L, HANDLER:0L,...}
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
function dispWidget_radioButtonEvent, Event

  name=widget_info(Event.id,/uname)
  set =widget_info(Event.id,/button_set)
  if (not set) then return,Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  mainDraw = widget_info(Event.top,find_by_uname='mainDraw')
  widget_control,displayBase,get_uvalue=dataInfo
  if (dataInfo.mode eq 'invalid') then return,Event

  case name of
    'panButton': begin
      dataInfo.mode = 'pan'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    'surfaceButton': begin
      dataInfo.mode = 'surface'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    'zoomButton': begin
      dataInfo.mode = 'zoom'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    'zoomSelectButton': begin
      dataInfo.mode = 'zoomSelect'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    'lineoutButton': begin
      dataInfo.mode = 'lineout'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    'contourButton': begin
      dataInfo.mode = 'contour'
      widget_control,displayBase,set_uvalue=dataInfo
    end
    else:
  endcase

  dispWidget_redrawDisplay,displayBase
  return, Event ; By Default, return the event.

end
;-----------------------------------------------------------------
; Kill Notify Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_destroyDisplay, wWidget
	dispBase = widget_info(wWidget,/parent)
;	print,'<dispWidget_destroyDislay> widget = ',wWidget,' dispBase = ',dispBase
  widget_control,wWidget,get_uvalue=dataInfo
;  if (n_elements(dataInfo) eq 0) then return
  if ((dataInfo.external_pointer eq 0) and ptr_valid(dataInfo.data)) then ptr_free,dataInfo.data
  if (ptr_valid(dataInfo.ap)) then ptr_free,dataInfo.ap
  windows = lonarr(100)
  j = 0
  for k=0,99 do begin
  	if (widget_info(!disp.windows[k],/valid_id) and (!disp.windows[k] ne dispBase)) then begin
  		windows[j] = !disp.windows[k]
  		j++
  	endif
  endfor
  !disp.windows = windows
  !disp.winCount = j
  !disp.activeWindow = !disp.windows[max([0,j-1])]
end

pro dispWidget_destroyAll
  defsysv, "!disp", exists = exists
  if (exists eq 0) then return
	n = !disp.winCount
	windows = !disp.windows
	for k=0,n-1 do begin
		if (widget_info(windows[k],/valid_id)) then begin
;			dispBase = widget_info(windows[k],find_by_uname='displayBase')  don't have to do this since kill window notifies the display
;			dispWidget_destroyDisplay,dispBase
			widget_control,windows[k],/destroy
		endif
	endfor
	!disp.windows = lonarr(100)
	!disp.winCount = 0
	!disp.activeWindow = 0
end

;-----------------------------------------------------------------
; BUTTON_EVENTS Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_DRAW, ID:0L, TOP:0L, HANDLER:0L, TYPE: 0, X:0, Y:0,
;       PRESS:0B, RELEASE:0B, CLICKS:0}

;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   TYPE returns a value that describes the type of draw widget
;       interaction that generated an event: 0 - Button Press, 1 -
;       Button Release, 2 - Motion, 3 - Viewport Moved, 4 -
;       Visibility Changed (Expose)


;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_mouseClick, Event
  displayBase = widget_info(Event.top,FIND_BY_UNAME='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  widget_control,Event.id,get_value=win
  tvdevice,win
  get_tv_scale,sx,sy,mx,my,jx,jy
  tvpos,x,y,wait=0
  x = fix(x)  &  y = fix(y)
  if (((x lt 0) or (y lt 0) or (x ge sx) or (y ge sy)) $
      and (dataInfo.mode ne 'surface')) then return
  box = dataInfo.box
  boxSize = [box[1]-box[0]+1,box[3]-box[2]+1]
  x = x + box[0] & y = y + box[2]
;  print,'<dispWidget_mouseClick> x,y = ',x,y

  case dataInfo.mode of
    'zoom': begin
      if (Event.type ne 0) then return
      case Event.press of
        1: begin  ; left mouse button - zoom in
          if ((boxSize[0] gt 2) and (boxSize[1] gt 2)) then begin
            boxSize = boxSize/2
          endif
        end
        4: begin  ; right mouse button - zoom out
          boxSize = boxSize*2
        end
        else: begin
        end
      endcase
    end
    'pan': begin
      if (Event.type ne 0) then return
    end
    'surface': begin
;      print,'<dispWidget_mouseClick> ',Event.press,Event.release
      dataInfo.mouseButtonState = Event.press
      mainDraw = widget_info(displayBase,find_by_uname='mainDraw')
      widget_control,mainDraw,get_value = win
      wset,win
      if (Event.release) then erase
      dataInfo.mouseButtonStartPosn = [Event.x,Event.y,dataInfo.ax,dataInfo.az]
      widget_control,displayBase,set_uvalue=dataInfo
      dispWidget_redrawDisplay,displayBase
      return
    end
    else: begin
      return
    end
  endcase

  boxSize = boxSize < [dataInfo.nx,dataInfo.ny]
  boxStart = ([x,y]-boxSize/2)>0
  boxEnd = boxStart + boxSize
  if (boxEnd[0] gt dataInfo.nx) then boxStart[0] = dataInfo.nx - boxSize[0]
  if (boxEnd[1] gt dataInfo.ny) then boxStart[1] = dataInfo.ny - boxSize[1]
  boxEnd = boxStart + boxSize - [1,1]
  box = [boxStart[0],boxEnd[0],boxStart[1],boxEnd[1]]
;  print,'<dispWidget_mouseClick> box = ',box
  dataInfo.box = box

  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_help, Event
  cr = string(13b)
  message = ['<<<<<<< Disp >>>>>>>', $
             'usage:', $
             '   Disp, data=yourData [,ap = aperture] [,name=windowTitle]', $
             '   Disp, demo=n   ;runs the program with dummy data, n = number of frames', $
             'Release History:', $
             '   March 2, 2007 ', $
             '   -Added File and Scale menu; allows Log and Sqrt scaling', $
             '   -Modified the MoviePlayer to respect the scaling choice', $
             '<<<<<<<<<<>>>>>>>>>>']
  n = (size(message))[1]
  for k=0,n-1 do print,message[k]
  print,''
  r = dialog_message('Disp release: 3/2/2007'+cr+ $
          'Don Gavel, UCO/Lick Observatory'+cr+ $
          'gavel@ucolick.org'+cr+ $
          'Window ID: '+strtrim(Event.top,2))
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_openFile, Event
  displayBase = widget_info(Event.top,FIND_BY_UNAME='displayBase')
  widget_control,displayBase,get_uvalue = dataInfo

  file = dialog_pickfile(path=dataInfo.dir,get_path=dir,filter=['*.fit*','*.sdt'])
  if (dir ne '') then begin
    dataInfo.dir = dir
    widget_control,displayBase,set_uvalue = dataInfo
  endif

  if (file eq '') then return

  fileType = strlowcase( (strsplit(file,'.',/extract))[1] )

  if (fileType eq 'sdt') then begin
    fileBase = (strsplit(file,'.',/extract))[0]
    data = rdview(fileBase)
  endif else begin
    data = readfits(file)
  endelse

  s = size(data)

  if (s[0] eq 0 and data[0] eq -1) then begin
    r=dialog_message('<dispWidget_openFitsFile> cannot open file '+file,/error)
    return
  endif

  if (s[0] ne 2) and (s[0] ne 3) then begin
    r=dialog_message('<dispWidget_openFitsFile> invalid number of dimensions; must be 2 or 3',/error)
    return
  endif

  types = ['undefined','byte','int','long','float','double', $
           'complex','string','structure','doubleComplex']
  type = typeof(data)
  if (total(type eq [0,7,8])) then begin
    r=dialog_message('<dispWidget_openFitsFile> type '+types[type]+' not supported',/error)
    return
  endif

  if (type eq 1) then begin
    r=dialog_message('<dispWidget_openFitsFile> byte type - displaying as integer')
    data = fix(data)
  endif

  if (total(type eq [6,9])) then begin  ; complex types: show abs value
    r=dialog_message('<dispWidget_openFitsFile> complex data type - displaying the absolute value')
    data = abs(data)
  endif

  nx = s[1]
  ny = s[2]
  nframes = 1
  if (s[0] eq 3) then nframes = s[3]
  dataMin = min(data,/nan) & dataMax = max(data,/nan)
  if (dataMin ge dataMax) then begin
    r=dialog_message('<dispWidget_openFitsFile> invalid data range: min '+ $
       strcompress(string(dataMin))+ ' ge max ' + $
       strcompress(string(dataMin)),/error)
    return
  endif

  if ptr_valid(dataInfo.data) then ptr_free,dataInfo.data
  if ptr_valid(dataInfo.ap) then ptr_free,dataInfo.ap

  box = [0,nx-1,0,ny-1]
  pa = ptr_new(data)
  ap = ones(nx,ny)
  pap = ptr_new(ap)
  mode = 'pan'

;  dataInfo = {data:pa, nx:nx, ny:ny, nframes:nframes, frame:0, $
;    max:dataMax, min:dataMin, displayMin:dataMin, displayMax:dataMax, $
;    box:box, mode:mode, ap:pap, dir:dir, $
;    mouseButtonState:0, mouseButtonStartPosn:[0,0,0,0], ax:30.,
;    az:30., external_pointer:0, movieWidget:0L}
  dataInfo.pa = pa
  dataInfo.nx = nx
  dataInfo.ny = ny
  dataInfo.nframes = nframes
  dataInfo.frame = 0
  dataInfo.max = dataMax
  dataInfo.min = dataMin
  dataInfo.displayMin = dataMin
  dataInfo.displayMax = dataMax
  dataInfo.box = box
  dataInfo.mode = mode
  dataInfo.ap = pap
  dataInfo.dir = dir
  dataInfo.mouseButtonState = 0
  dataInfo.mouseButtonStartPosn = [0,0,0,0]
  dataInfo.ax = 30.
  dataInfo.az = 30.
  dataInfo.external_pointer = 0
  dataInfo.movieWidget = 0L
  widget_control,displayBase,set_uvalue = dataInfo

  panButton = widget_info(Event.top,find_by_uname='panButton')
  widget_control,panButton,/set_button

  frameSlider = widget_info(Event.top,find_by_uname='frameSlider')
  if (widget_info(frameSlider,/valid_id)) then begin
    widget_control,frameSlider,set_value=0,sensitive=0
    if (nframes gt 1) then begin
      widget_control,frameSlider,set_slider_max = nframes-1,/sensitive
    endif
  endif

  minSlider = widget_info(Event.top,find_by_uname='minSlider')
  maxSlider = widget_info(Event.top,find_by_uname='maxSlider')
  widget_control,minSlider,set_value = [dataMin,dataMin,dataMax]
  widget_control,maxSlider,set_value = [dataMax,dataMin,dataMax]

  widget_control,Event.top,tlb_set_title=file
  dispWidget_redrawDisplay,displayBase
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_movie, Event
   id = Event.top
   displayBase = widget_info(Event.top,find_by_uname='displayBase')
   widget_control,displayBase,get_uvalue=dataInfo
   if (dataInfo.nframes eq 1) then return
   movieWidget = dataInfo.movieWidget
   if (widget_info(movieWidget,/valid_id)) then begin
;       widget_control,movieWidget,/iconify
       widget_control,movieWidget,/show
   endif else begin
       moviePlayerWidget,group_leader=id,dispWin=id
       wait,.1
       displayBase = widget_info(Event.top,find_by_uname='displayBase')
       widget_control,displayBase,get_uvalue=dataInfo
       movieWidget = dataInfo.movieWidget
       forwardButton = widget_info(movieWidget,find_by_uname='forwardButton')
       moviePlayer_Forward,{id:forwardButton,top:movieWidget}
   endelse
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_saveFile, Event
  dispWidget_notImplemented,'saveFile'
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_scaleLinear, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.scale = 'linear'
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Linear')
  widget_control,menuItem,set_button=1
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Log')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Sqrt')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_LogAbs')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_SqrtAbs')
  widget_control,menuItem,set_button=0
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
;  dispWidget_notImplemented,'scaleLinear'
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_scaleLog, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.scale = 'log'
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Linear')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Log')
  widget_control,menuItem,set_button=1
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Sqrt')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_LogAbs')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_SqrtAbs')
  widget_control,menuItem,set_button=0
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
;  dispWidget_notImplemented,'scaleLog'
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_scaleSqrt, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.scale = 'sqrt'
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Linear')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Log')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Sqrt')
  widget_control,menuItem,set_button=1
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_LogAbs')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_SqrtAbs')
  widget_control,menuItem,set_button=0
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
;  dispWidget_notImplemented,'scaleSqrt'
end
;
pro dispWidget_notImplemented,routine
  cr = string(13b)
  r = dialog_message('Sorry, '+routine+' is not implemented yet.'+cr+'Bug the developer to get off his tush and get this written')
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_scaleLogAbs, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.scale = 'logAbs'
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Linear')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Log')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Sqrt')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_LogAbs')
  widget_control,menuItem,set_button=1
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_SqrtAbs')
  widget_control,menuItem,set_button=0
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro dispWidget_scaleSqrtAbs, Event
  displayBase = widget_info(Event.top,find_by_uname='displayBase')
  widget_control,displayBase,get_uvalue=dataInfo
  dataInfo.scale = 'sqrtAbs'
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Linear')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Log')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_Sqrt')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_LogAbs')
  widget_control,menuItem,set_button=0
  menuItem = widget_info(Event.top,find_by_uname='scaleMenu_SqrtAbs')
  widget_control,menuItem,set_button=1
  widget_control,displayBase,set_uvalue=dataInfo
  dispWidget_redrawDisplay,displayBase
end
