; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	03/03/2007 01:08.51
; 
pro dispBase_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)

  wWidget =  Event.top
  !disp.activewindow = widget_info(wWidget,find_by_uname='dispBase')
  

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='dispBase'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BASE' )then $
        dispWidget_resizeWindow, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='displayBase'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='mainDraw'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DRAW' )then $
        if( Event.type eq 0 )then $
          dispWidget_mouseClick, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DRAW' )then $
        if( Event.type eq 1 )then $
          dispWidget_mouseClick, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_DRAW' )then $
        if( Event.type eq 2 )then $
          dispWidget_mouseMotion, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='buttonSet'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='colorMapButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_colorMap, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='helpButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_help, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='openButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_openFile, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='movieButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_movie, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='bottomFrame'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='fileMenu_Open'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_openFile, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='fileMenu_Save'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_saveFile, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='scaleMenu_Linear'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_scaleLinear, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='scaleMenu_Log'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_scaleLog, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='scaleMenu_Sqrt'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_scaleSqrt, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='scaleMenu_LogAbs'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_scaleLogAbs, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='scaleMenu_SqrtAbs'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        dispWidget_scaleSqrtAbs, Event
    end
    else:
  endcase

end

;pro dispBase, GROUP_LEADER=wGroup, size=window_size, offset=offset, _EXTRA=_VWBExtra_
pro dispBase, size=window_size, offset=offset, _EXTRA=_VWBExtra_

  Resolve_Routine, 'dispWidget_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  DonsMac = 1
  if (n_elements(offset) eq 0) then begin
    offset = [5,5]
    if (DonsMac) then offset[0] = 5000
  endif
  if (n_elements(window_size) eq 0) then window_size = [404,421]; [395,353]
  
  dispBase = Widget_Base( GROUP_LEADER=wGroup, UNAME='dispBase'  $
      ,XOFFSET=offset[0] ,YOFFSET=offset[1] ,SCR_XSIZE=window_size[0] ,SCR_YSIZE=window_size[1]  $
      ,NOTIFY_REALIZE='dispWidget_debug' ,/TLB_SIZE_EVENTS  $
      ,TITLE='IDL' ,SPACE=3 ,ROW=3 ,MBAR=dispBase_MBAR)

  
  displayBase = Widget_Base(dispBase, UNAME='displayBase' ,FRAME=1  $
      ,YOFFSET=3 ,SCR_XSIZE=385 ,SCR_YSIZE=window_size[1]  $
      ,NOTIFY_REALIZE='dispWidget_realizeDisplay'  $
      ,KILL_NOTIFY='dispWidget_destroyDisplay' ,TITLE='IDL' ,SPACE=3  $
      ,XPAD=3 ,YPAD=3 ,COLUMN=3)

  
  dispWidget_createDisplay, displayBase, _EXTRA=_VWBExtra_

  
  mainDraw = Widget_Draw(displayBase, UNAME='mainDraw' ,XOFFSET=3  $
      ,YOFFSET=3 ,SCR_XSIZE=265 ,SCR_YSIZE=272 ,/BUTTON_EVENTS  $
      ,/MOTION_EVENTS)

  mac_adjust = 25
  displayMarginFrame = Widget_Base(displayBase,  $
      UNAME='displayMarginFrame' ,FRAME=1 ,XOFFSET=271 ,YOFFSET=3  $
      ,SCR_XSIZE=110 ,SCR_YSIZE=296+mac_adjust ,TITLE='IDL' ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  secondaryDraw = Widget_Draw(displayMarginFrame,  $
      UNAME='secondaryDraw' ,XOFFSET=1 ,YOFFSET=1 ,SCR_XSIZE=65  $
      ,SCR_YSIZE=62)

  ; ====================================================
  ;  This is the first column of buttons: Pan, Tilt, etc
  ; ====================================================
  buttonColumnWidth = 50; for the mac; was 29 for pc
  buttonSet = Widget_Base(displayMarginFrame, UNAME='buttonSet'  $
      ,FRAME=1 ,XOFFSET=6 ,YOFFSET=71 ,SCR_XSIZE=buttonColumnWidth ,SCR_YSIZE=147  $
      ,EVENT_FUNC='dispWidget_radioButtonEvent' ,TITLE='IDL' ,ROW=6, /exclusive)
 
  dispWidget_populateButtonSet, buttonSet, _EXTRA=_VWBExtra_

  ; ====================================================
  ;   This is the second column of buttons: File, Color, Movie, etc.
  ; ====================================================
  
  buttonColumnOffset = 59; for the mac; was 39 for the PC

  openButton = Widget_Button(displayMarginFrame,  $
      UNAME='openButton' ,XOFFSET=buttonColumnOffset ,YOFFSET=71 ,SCR_XSIZE=22  $
      ,SCR_YSIZE=22 ,/ALIGN_CENTER ,TOOLTIP='Open FITS or VIEW File'  $
      ,VALUE='open.bmp' ,/BITMAP)
  
  colorMapButton = Widget_Button(displayMarginFrame,  $
      UNAME='colorMapButton' ,XOFFSET=buttonColumnOffset ,YOFFSET=100 ,SCR_XSIZE=22  $
      ,SCR_YSIZE=22 ,/ALIGN_CENTER ,TOOLTIP='change the color map'  $
      ,VALUE='color.bmp' ,/BITMAP)

  movieButton = Widget_Button(displayMarginFrame,  $
      UNAME='movieButton' ,XOFFSET=buttonColumnOffset ,YOFFSET=127 ,SCR_XSIZE=22  $
      ,SCR_YSIZE=22 ,/ALIGN_CENTER ,TOOLTIP='movie mode'  $
      ,VALUE='movieCamera.bmp' ,/BITMAP)

  helpButton = Widget_Button(displayMarginFrame,  $
      UNAME='helpButton' ,XOFFSET=buttonColumnOffset ,YOFFSET=157 ,SCR_XSIZE=22  $
      ,SCR_YSIZE=22 ,/ALIGN_CENTER ,TOOLTIP='help' ,VALUE='help.bmp'  $
      ,/BITMAP)

  ; =====================================================
  ;  Now comes the position and data readout boxes 
  ; ===================================================== 
  indexLabel = Widget_Label(displayMarginFrame, UNAME='indexLabel'  $
      ,FRAME=1 ,XOFFSET=39 ,YOFFSET=198+mac_adjust ,SCR_XSIZE=63 ,SCR_YSIZE=19  $
      ,/ALIGN_CENTER ,VALUE='[0,0]')

  valueLabel = Widget_Label(displayMarginFrame, UNAME='valueLabel'  $
      ,FRAME=1 ,XOFFSET=3 ,YOFFSET=269+mac_adjust ,SCR_XSIZE=102 ,SCR_YSIZE=19  $
      ,/ALIGN_CENTER ,VALUE='0.0')
  
  positionLabelX = Widget_Label(displayMarginFrame,  $
      UNAME='positionLabelX' ,FRAME=1 ,XOFFSET=5 ,YOFFSET=222+mac_adjust  $
      ,SCR_XSIZE=100 ,SCR_YSIZE=18 ,/ALIGN_CENTER ,VALUE='x ='+ $
      ' 0000.0000')
  
  positionLabelY = Widget_Label(displayMarginFrame,  $
      UNAME='positionLabelY' ,FRAME=1 ,XOFFSET=5 ,YOFFSET=245+mac_adjust  $
      ,SCR_XSIZE=98 ,SCR_YSIZE=18 ,/ALIGN_CENTER ,VALUE='y ='+ $
      ' 0000.0000')
  
  ; =======================================================
  ;  Min-Max sliders
  ; =======================================================
  bottomFrame = Widget_Base(dispBase, UNAME='bottomFrame' ,FRAME=1  $
      ,YOFFSET=409 ,SCR_XSIZE=386 ,SCR_YSIZE=81  $
      ,EVENT_FUNC='dispWidget_fSliderEvent' ,TITLE='IDL' ,SPACE=3  $
      ,XPAD=3 ,YPAD=3 ,COLUMN=2)

  
  dispWidget_createSliderGroup, bottomFrame, _EXTRA=_VWBExtra_

  ; =======================================================
  ;  Menu Items
  ; =======================================================
  
  fileMenu = Widget_Button(dispBase_MBAR, UNAME='fileMenu' ,/MENU, VALUE='File')

  fileMenu_Open = Widget_Button(fileMenu, UNAME='fileMenu_Open' ,VALUE='Open')
  
  fileMenu_Save = Widget_Button(fileMenu, UNAME='fileMenu_Save' ,VALUE='Save')
  
  
  scaleMenu = Widget_Button(dispBase_MBAR, UNAME='scaleMenu' ,/MENU, VALUE='Scale')

  scaleMenu_Linear = Widget_Button(scaleMenu, UNAME='scaleMenu_Linear'  $
      ,/CHECKED_MENU ,VALUE='Linear')
  
  scaleMenu_Log = Widget_Button(scaleMenu, UNAME='scaleMenu_Log'  $
      ,/CHECKED_MENU ,VALUE='Log')
  
  scaleMenu_Sqrt = Widget_Button(scaleMenu, UNAME='scaleMenu_Sqrt'  $
      ,/CHECKED_MENU ,VALUE='Sqrt')
  
  scaleMenu_LogAbs = Widget_Button(scaleMenu, UNAME='scaleMenu_LogAbs'  $
      ,/CHECKED_MENU ,VALUE='LogAbs')
  
  scaleMenu_SqrtAbs = Widget_Button(scaleMenu, UNAME='scaleMenu_SqrtAbs' $
      ,/CHECKED_MENU ,VALUE='SqrtAbs')

  Widget_Control, /REALIZE, dispBase

  XManager, 'dispBase', dispBase, /NO_BLOCK  

end
; 
; Empty stub procedure used for autoloading.
; 
pro dispwidget, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  dispBase, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
