;+
;  moviePlayer.pro
;   play data cubes as movies
;
;  usage:
;   moviePlayer,data
;-
pro moviePlayer,data
  moviePlayerWidget,data=data
end
;--------------------
; example usage
;
;a = randomn(seed,20,20,10)
n = 100
x = findgen(n) ## ones(n)
y = transpose(x)
nframes = 10
a = fltarr(n,n,nframes)
f = 1./10.
for k=0,nframes-1 do begin
	a[*,*,k] = sin(2*!pi*(x-k)*f)
endfor
moviePlayer,a
end
