HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	03/26/2006 10:23.21
VERSION 1
END

moviePlayerBase BASE 5 5 416 127
REALIZE "moviePlayer_Realize"
KILLNOTIFY "moviePlayer_Destroy"
ONPOSTCREATE "moviePlayer_Create"
TLB
CAPTION "Movie Player Controls"
XPAD = 3
YPAD = 3
SPACE = 3
BEGIN
  reverseButton PUSHBUTTON 21 32 0 0
  VALUE "C:\RSI\IDL62\resource\bitmaps\shift_left.bmp"
  ALIGNCENTER
  TIMEREVENT "moviePlayer_ReverseTimer"
  BITMAP
  ONACTIVATE "moviePlayer_Reverse"
  END
  forwardButton PUSHBUTTON 83 32 0 0
  VALUE "C:\RSI\IDL62\resource\bitmaps\shift_right.bmp"
  ALIGNCENTER
  TIMEREVENT "moviePlayer_ForwardTimer"
  BITMAP
  ONACTIVATE "moviePlayer_Forward"
  END
  pauseButton PUSHBUTTON 53 32 0 0
  VALUE "C:\RSI\IDL62\resource\bitmaps\pause.bmp"
  ALIGNCENTER
  BITMAP
  ONACTIVATE "moviePlayer_Pause"
  END
  frameRateSlider SLIDER 138 11 251 59
  CAPTION "Frame Rate"
  MINIMUM = 1
  ONVALUECHANGED "moviePlayer_SetFrameRate"
  END
END
