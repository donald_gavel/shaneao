HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	03/03/2007 01:08.46
VERSION 1
END

dispBase BASE 5 5 395 458
REALIZE "dispWidget_debug"
TLB
CAPTION "IDL"
ROWS = 3
SPACE = 3
ONSIZECHANGE "dispWidget_resizeWindow"
BEGIN
  displayBase BASE -1 -1 385 403
  FRAME = 1
  KILLNOTIFY "dispWidget_destroyDisplay"
  REALIZE "dispWidget_realizeDisplay"
  ONPOSTCREATE "dispWidget_createDisplay"
  COLUMNS = 3
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
    mainDraw DRAW -1 -1 265 272
    ONBUTTON "dispWidget_mouseClick"
    ONMOTION "dispWidget_mouseMotion"
    END
    displayMarginFrame BASE -1 -1 110 296
    FRAME = 1
    XPAD = 3
    YPAD = 3
    SPACE = 3
    CAPTION "IDL"
    BEGIN
      secondaryDraw DRAW 1 1 65 62
      END
      buttonSet BASE 6 71 29 147
      FRAME = 1
      ONPOSTCREATE "dispWidget_populateButtonSet"
      ONHANDLEEVENT "dispWidget_radioButtonEvent"
      ROWS = 6
      EXCLUSIVE
      CAPTION "IDL"
      BEGIN
      END
      indexLabel LABEL 39 198 63 19
      VALUE "[0,0]"
      FRAME = 1
      ALIGNCENTER
      END
      valueLabel LABEL 3 269 102 19
      VALUE "0.0"
      FRAME = 1
      ALIGNCENTER
      END
      colorMapButton PUSHBUTTON 39 100 22 22
      VALUE "C:\pro\dispGui\color.bmp"
      ALIGNCENTER
      BITMAP
      TOOLTIP "change the color map"
      ONACTIVATE "dispWidget_colorMap"
      END
      WID_BUTTON_0 PUSHBUTTON 39 157 22 22
      VALUE "C:\RSI\IDL56\resource\bitmaps\help.bmp"
      ALIGNCENTER
      BITMAP
      TOOLTIP "help"
      ONACTIVATE "dispWidget_help"
      END
      WID_BUTTON_1 PUSHBUTTON 39 71 22 22
      VALUE "C:\RSI\IDL56\resource\bitmaps\open.bmp"
      ALIGNCENTER
      BITMAP
      TOOLTIP "Open FITS or VIEW File"
      ONACTIVATE "dispWidget_openFile"
      END
      positionLabelX LABEL 5 222 100 18
      VALUE "x = 0000.0000"
      FRAME = 1
      ALIGNCENTER
      END
      positionLabelY LABEL 5 245 98 18
      VALUE "y = 0000.0000"
      FRAME = 1
      ALIGNCENTER
      END
      movie_button PUSHBUTTON 39 127 22 22
      VALUE "C:\pro\dispGui\movieCamera.bmp"
      ALIGNCENTER
      BITMAP
      TOOLTIP "movie mode"
      ONACTIVATE "dispWidget_movie"
      END
    END
  END
  bottomFrame BASE -1 -1 386 81
  FRAME = 1
  ONPOSTCREATE "dispWidget_createSliderGroup"
  ONHANDLEEVENT "dispWidget_fSliderEvent"
  COLUMNS = 2
  XPAD = 3
  YPAD = 3
  SPACE = 3
  CAPTION "IDL"
  BEGIN
  END
  dispBase_MBAR MENUBAR -1 -1 0 0
  BEGIN
    W_MENU_0 PUSHBUTTON 0 0 0 0
    VALUE "File"
    MENU
    BEGIN
      W_MENU_3 PUSHBUTTON 0 0 0 0
      VALUE "Open"
      ONACTIVATE "dispWidget_openFile"
      END
      W_MENU_4 PUSHBUTTON 0 0 0 0
      VALUE "Save"
      ONACTIVATE "dispWidget_saveFile"
      END
    END
    W_MENU_1 PUSHBUTTON 0 0 0 0
    VALUE "Scale"
    MENU
    BEGIN
      menu_scaleLinear PUSHBUTTON 0 0 0 0
      VALUE "Linear"
      CHECKED_MENU
      ONACTIVATE "dispWidget_scaleLinear"
      END
      menu_scaleLog PUSHBUTTON 0 0 0 0
      VALUE "Log"
      CHECKED_MENU
      ONACTIVATE "dispWidget_scaleLog"
      END
      menu_scaleSqrt PUSHBUTTON 0 0 0 0
      VALUE "Sqrt"
      CHECKED_MENU
      ONACTIVATE "dispWidget_scaleSqrt"
      END
      menu_scaleLogAbs PUSHBUTTON 0 0 0 0
      VALUE "LogAbs"
      CHECKED_MENU
      ONACTIVATE "dispWidget_scaleLogAbs"
      END
      menu_scaleSqrtAbs PUSHBUTTON 0 0 0 0
      VALUE "SqrtAbs"
      CHECKED_MENU
      ONACTIVATE "dispWidget_scaleSqrtAbs"
      END
    END
  END
END
