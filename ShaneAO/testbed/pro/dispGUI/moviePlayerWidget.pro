; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	03/26/2006 10:23.24
; 
pro moviePlayerBase_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)


  wWidget =  Event.top

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='moviePlayerBase'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='reverseButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TIMER' )then $
        moviePlayer_ReverseTimer, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        moviePlayer_Reverse, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='forwardButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_TIMER' )then $
        moviePlayer_ForwardTimer, Event
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        moviePlayer_Forward, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='pauseButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        moviePlayer_Pause, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='frameRateSlider'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_SLIDER' )then $
        moviePlayer_SetFrameRate, Event
    end
    else:
  endcase

end

pro moviePlayerBase, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_

  Resolve_Routine, 'moviePlayerWidget_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  moviePlayerBase = Widget_Base( GROUP_LEADER=wGroup,  $
      UNAME='moviePlayerBase' ,XOFFSET=5 ,YOFFSET=5 ,SCR_XSIZE=416  $
      ,SCR_YSIZE=127 ,NOTIFY_REALIZE='moviePlayer_Realize'  $
      ,KILL_NOTIFY='moviePlayer_Destroy' ,TITLE='Movie Player'+ $
      ' Controls' ,SPACE=3 ,XPAD=3 ,YPAD=3)

  
  moviePlayer_Create, moviePlayerBase, _EXTRA=_VWBExtra_

  
  reverseButton = Widget_Button(moviePlayerBase,  $
      UNAME='reverseButton' ,XOFFSET=21 ,YOFFSET=32 ,/ALIGN_CENTER  $
      ,VALUE='shift_left.bmp' ,/BITMAP)

  
  forwardButton = Widget_Button(moviePlayerBase,  $
      UNAME='forwardButton' ,XOFFSET=83 ,YOFFSET=32 ,/ALIGN_CENTER  $
      ,VALUE='shift_right.bmp' ,/BITMAP)

  
  pauseButton = Widget_Button(moviePlayerBase, UNAME='pauseButton'  $
      ,XOFFSET=53 ,YOFFSET=32 ,/ALIGN_CENTER ,VALUE='pause.bmp'  $
      ,/BITMAP)

  
  frameRateSlider = Widget_Slider(moviePlayerBase,  $
      UNAME='frameRateSlider' ,XOFFSET=138 ,YOFFSET=11 ,SCR_XSIZE=251  $
      ,SCR_YSIZE=59 ,TITLE='Frame Rate' ,MINIMUM=1)

  Widget_Control, /REALIZE, moviePlayerBase

  XManager, 'moviePlayerBase', moviePlayerBase, /NO_BLOCK  ,CLEANUP='moviePlayer_Destroy'  

end
; 
; Empty stub procedure used for autoloading.
; 
pro moviePlayerWidget, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  moviePlayerBase, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
