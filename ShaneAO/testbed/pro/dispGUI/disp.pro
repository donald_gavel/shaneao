;+
;  disp.pro
;     This is the wrapper routine for dispwidget, the GUI-based image displayer
;
;    usage:
;        disp, image, [name], [dx=dx], [x0=x0], [ap=ap]
;        
;    parameters
;        image - the 2-d image to be displayed. Must be real
;        name - a string to display as the window name
;        dx - a 2-vector indicating the pixel size in x and y (default is [1,1]). The position in pixels and the coordinates described by x0 and dx are real time dispayed in the GUI
;        x0 - a 2-vector indicating the position in x and y of the lower left pixel (default is [0,0])
;        ap - an aperture mask. Data outside the mask will not be displayed. It must be the same size as the image, and consist of 1's (display) and 0's (don't display)
;        
;    outputs
;        none
;        
;    additional notes
;      There is a way to get hold of a handle to the display window and the pointer to the data being displayed. This allows external routines to modify the data and update what is displayed
;      
;      Just after starting a gui, the id is available with the command:
;        id = !disp.windows[!disp.wincount-1]
;
;      Then, later one can update the displayed data with new data, new_image, using the following series of commands:
;        displayBase = widget_info(id,find_by_uname='displayBase')
;        widget_control,displayBase,get_uvalue=dataInfo
;        ptr_free,dataInfo.data
;        dataInfo.data = ptr_new(new_image)
;        widget_control,displayBase,set_uvalue=dataInfo
;        dispWidget_redrawDisplay,displayBase
;-

pro disp, data, name, id=id, _EXTRA=_VWBExtra_
  if (n_elements(data) eq 0) then begin
  	if (n_elements(name) eq 0) then begin
  		dispwidget,_EXTRA=_VWBExtra_
  	endif else begin
  		dispwidget,name=name,_EXTRA=_VWBExtra_
  	endelse
  endif else begin
  	if (n_elements(name) eq 0) then begin
   		dispwidget, data=data, _EXTRA=_VWBExtra_
   	endif else begin
   		dispwidget,data=data,name=name, _EXTRA=_VWBExtra_
   	endelse
  endelse
  id = !disp.windows[!disp.wincount-1]
end
