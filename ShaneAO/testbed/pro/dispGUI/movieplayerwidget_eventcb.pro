;
; IDL Event Callback Procedures
; moviePlayerWidget_eventcb
;
; Generated on:	10/30/2005 20:58.21
;
;
; Empty stub procedure used for autoloading.
;
pro moviePlayerWidget_eventcb
end
;-----------------------------------------------------------------
; Notify Realize Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Realize, wWidget, data=data
  widget_control,wWidget,get_uvalue=movieInfo
  dispWin = movieInfo.dispWin
  widget_control,dispWin,tlb_get_offset=offset
  widget_control,wWidget,tlb_get_size=winsize
  widget_control,wWidget,tlb_set_xoffset=offset[0],tlb_set_yoffset=((offset[1]-winsize[1])>0)
  rateSlider = widget_info(wWidget, FIND_BY_UNAME='frameRateSlider')
  widget_control,rateSlider,set_value = 30
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Reverse, Event
  print,'<moviePlayer_Reverse>'

  id = widget_info(Event.top, FIND_BY_UNAME='moviePlayerBase')
  widget_control,id,get_uvalue = movieInfo
  movieInfo.mode = -1
  widget_control,id,set_uvalue = movieInfo
  moviePlayer_ReverseTimer, Event
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Forward, Event
  print,'<moviePlayer_Forward>'

  id = widget_info(Event.top, FIND_BY_UNAME='moviePlayerBase')
  widget_control,id,get_uvalue = movieInfo
  movieInfo.mode = 1
  widget_control,id,set_uvalue = movieInfo
  moviePlayer_ForwardTimer, Event
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Pause, Event
  print,'<moviePlayer_Pause>'
  wait,.1
  id = widget_info(Event.top, FIND_BY_UNAME='moviePlayerBase')
  widget_control,id,get_uvalue = dataInfo
  dataInfo.mode = 0
  dispWin = dataInfo.dispWin
  if (dispWin ne 0) then begin
      displayBase = widget_info(dispWin,FIND_BY_UNAME='displayBase')
      widget_control,displayBase,get_uvalue = winInfo
      winInfo.frame = dataInfo.frame
      widget_control,displayBase,set_uvalue = winInfo
  endif
  widget_control,id,set_uvalue = dataInfo
end
;-----------------------------------------------------------------
; Slider Value Changed Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_SLIDER, ID:0L, TOP:0L, HANDLER:0L, VALUE:0L, DRAG:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   VALUE returns the new value of the slider. DRAG returns integer 1
;       if the slider event was generated as part of a drag
;       operation, or zero if the event was generated when the user
;       had finished positioning the slider.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_SetFrameRate, Event

end
;-----------------------------------------------------------------
; Post Create Widget Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;   Any keywords passed into the generated widget creation procedure
;       are passed into this procudure.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Create, wWidget, data=data, dispWin=dispWin,_EXTRA=_VWBExtra_
  if (keyword_set(dispWin)) then begin
      displayBase = widget_info(dispWin,FIND_BY_UNAME='displayBase')
      widget_control,displayBase,get_uvalue=dataInfo
      name = dataInfo.name
      widget_control,wWidget,tlb_set_title=name+' - movie controls'
      dataInfo.movieWidget = wWidget
      frameSlider = widget_info(dispWin,FIND_BY_UNAME='frameSlider')
      widget_control,frameSlider,get_value=frame
      box = dataInfo.box
      data = (*dataInfo.data)[box[0]:box[1],box[2]:box[3],*] > dataInfo.displayMin < dataInfo.displayMax
      ap = (*dataInfo.ap)[box[0]:box[1],box[2]:box[3]]
      scale = dataInfo.scale
      eps = 1.e-16
      if (scale eq 'log') then begin
        data = alog10(data > (eps*max(data)))
      endif
      if (scale eq 'sqrt') then begin
        data = sqrt(data > (eps*max(data)))
      endif
      if (scale eq 'logAbs') then begin
        data = abs(data)
        data = alog10(data > (eps*max(data)))
      endif
      if (scale eq 'sqrtAbs') then begin
        data = abs(data)
        data = sqrt(data > (eps*max(data)))
      endif
      for k = 0,dataInfo.nframes-1 do data[*,*,k] *= ap
      ;dataMin = min(data) > dataInfo.displayMin
      ;dataMax = max(data) < dataInfo.displayMax
      ;zrange = dataMax-dataMin

      mainDraw = widget_info(dispWin,find_by_uname='mainDraw')
      widget_control,mainDraw,get_value=win
      wset,win
      tvdevice,win
      scale_tv,ap,mx,my,jx,jy,/noexact,relative=.8
      print,'<moviePlayer_Create> win = ',win,' mx,my = ',mx,my,' jx,jy = ',jx,jy,' !d.[x,y]_size ',!d.x_size,!d.y_size
      n = (size(data))[1]
      m = (size(data))[2]
      widget_control,displayBase,set_uvalue=dataInfo
  endif else begin
      dispWin = 0
      frame = 0
      mx = 500
      my = 500
      n = (size(data))[1]
      m = (size(data))[2]
      jx = 0
      jy = 0
  endelse
  nframes = (size(data))[3]
  data_display = fltarr(mx,my,nframes)
  for k=0,nframes-1 do begin
      data_display[*,*,k] = congrid(data[*,*,k],mx,my)
  endfor
  data_display -= min(data_display)
  data_display *= 255./max(data_display)
  if (not keyword_set(dispWin)) then begin
      window,/free,xsize=mx,ysize=my
      win = !d.window
  endif
  tv,data_display[*,*,frame],jx,jy

  pdata_display = ptr_new(data_display)
  movieInfo = {data:pdata_display, window:win, dispWin:dispWin, winOffset:[jx,jy], nframes:nframes, frame:frame, mode:0}
;  modes: 0=pause, 1=forward, -1=reverse
  widget_control,wWidget,set_uvalue = movieInfo
end
;-----------------------------------------------------------------
; Timer Event Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TIMER, ID:0L, TOP:0L, HANDLER:0L}
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_ReverseTimer, Event
;  print,'<moviePlayer_ForwardTimer>'
  id = widget_info(Event.top, FIND_BY_UNAME='moviePlayerBase')
  widget_control,id,get_uvalue = movieInfo
  if (movieInfo.mode ne (-1)) then return
  dispWin = movieInfo.dispWin
  if (dispWin ne 0) then begin
      displayBase = widget_info(dispWin,FIND_BY_UNAME='displayBase')
      widget_control,displayBase,get_uvalue = winInfo
      frameSlider = widget_info(dispWin,FIND_BY_UNAME='frameSlider')
      widget_control,frameSlider,get_value=frame
      movieInfo.frame = frame
  endif

  rateSlider = widget_info(Event.top, FIND_BY_UNAME='frameRateSlider')
  frame = movieInfo.frame
  widget_control,rateSlider,get_value = rate
  nframes = movieInfo.nframes
  frame--
  if (frame lt 0) then frame = nframes-1

  wset,movieInfo.window
  tvdevice,movieInfo.window
  tv,(*movieInfo.data)[*,*,frame],movieInfo.winOffset[0],movieInfo.winOffset[1]

  if (dispWin ne 0) then widget_control,frameSlider,set_value=frame
  movieInfo.frame = frame
  widget_control,id,set_uvalue = movieInfo
  winInfo.frame = movieInfo.frame
  widget_control,displayBase,set_uvalue = winInfo
;  print,'<moviePlayer_Forward> frame ',frame,' timer ',1./rate,' seconds'
  widget_control,Event.id,timer = 1./rate

end
;-----------------------------------------------------------------
; Timer Event Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_TIMER, ID:0L, TOP:0L, HANDLER:0L}
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_ForwardTimer, Event
;  print,'<moviePlayer_ForwardTimer>'
  id = widget_info(Event.top, FIND_BY_UNAME='moviePlayerBase')
  widget_control,id,get_uvalue = movieInfo
  if (movieInfo.mode ne 1) then return
  dispWin = movieInfo.dispWin
  if (dispWin ne 0) then begin
      displayBase = widget_info(dispWin,FIND_BY_UNAME='displayBase')
      widget_control,displayBase,get_uvalue = winInfo
      frameSlider = widget_info(dispWin,FIND_BY_UNAME='frameSlider')
      widget_control,frameSlider,get_value=frame
      movieInfo.frame = frame
  endif

  rateSlider = widget_info(Event.top, FIND_BY_UNAME='frameRateSlider')
  frame = movieInfo.frame
  widget_control,rateSlider,get_value = rate
  nframes = movieInfo.nframes
  frame++
  if (frame eq nframes) then frame = 0

  wset,movieInfo.window
  tvdevice,movieInfo.window
  tv,(*movieInfo.data)[*,*,frame],movieInfo.winOffset[0],movieInfo.winOffset[1]

  if (dispWin ne 0) then widget_control,frameSlider,set_value=frame
  movieInfo.frame = frame
  widget_control,id,set_uvalue = movieInfo
  winInfo.frame = movieInfo.frame
  widget_control,displayBase,set_uvalue = winInfo
;  print,'<moviePlayer_Forward> frame ',frame,' timer ',1./rate,' seconds'
  widget_control,Event.id,timer = 1./rate

end
;-----------------------------------------------------------------
; Kill Notify Callback Procedure.
; Argument:
;   wWidget - ID number of specific widget.
;
;
;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro moviePlayer_Destroy, wWidget
  widget_control,wWidget,get_uvalue = movieInfo
  if (ptr_valid(movieInfo.data)) then ptr_free,movieInfo.data
end
