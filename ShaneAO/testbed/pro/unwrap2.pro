;+
;  unwrap2.pro - experimental phase unwrapper
;
; usage:
;   phu = unwrap2(ph,ap)
;
;  inputs:
;    ph - phase screen (real, radians)
;    ap - aperture mask (1 inside, 0 outside)
;
;  outputs:
;    phu - phase screen with unwrapped phase
;
;-
function unwrapphase,ph,ap
  if n_params() eq 0 then begin
    doc_library,'unwrapphase'
    return,0
  endif
  n = (size(ph))(1)
  m = (size(ph))(2)
  if n_elements(ap) eq 0 then begin
    ap = ph*0 + 1
  endif
  dif = (ph - shift(ph,1,0))*ap
  boundary = ap - shift(ap,1,0)
  difnb = dif * (boundary eq 0)
  difm = dif - 2*!pi*(float(difnb gt !pi)-float(difnb lt -!pi))
  ker = fltarr(n,m)
  ker[n/2:n-1,m/2] = 1
  phu = real(ftconvolve(ker,difm))*ap 

  u = total(phu,1)/(total(ap,1)>.5)
  du = u - shift(u,1)
  dum = - 2*!pi*(float(du gt !pi)-float(du lt -!pi))
  n = (size(dum))(1)
  us = fltarr(n)
  us[0] = 0
  for i=1,n-1 do us[i] = us[i-1] + dum[i]
  uu = ((fltarr(n)+1) # us)*ap
  phu = phu+uu
return,phu
end
