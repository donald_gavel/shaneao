; encirc.pro - encircled energy calculation
;
; encirc returns the encircled energy curve, a 2 x n array
;  the first row is the radius values and the 2nd row is the encircled energy at that radius
;
;  if dr is not specified (not in the argument list) then -all- the possible radius values are used (one per data point)
;  if dr is specified, then only n/2/dr encircled energy points are returned
;

function encircum,data
  n = (size(data))(2)
  ret = data
  ret(0,*) = data(0,*)
  ret(1,0) = data(1,0)
  for i=long(1),n-1 do begin
    ret(1,i) = ret(1,i-1) + data(1,i)
  endfor
  return,ret
end

function radialavg,data,cx,cy
  n = (size(data))(1)
  m = (size(data))(2)
  x = (findgen(n)-cx) # (fltarr(m)+1)
  y = (fltarr(n)+1) # (findgen(m)-cy)
  r = sqrt(x^2+y^2)
  is = sort(r)
  ret = [transpose(r(is)), transpose(data(is))]
  return,ret
end

function eePercent,ee,pc
  ind = value_locate(ee,pc/100.)            ; find "theta80"
  alpha = (pc/100.-ee[ind])/(ee[ind+1]-ee[ind]) ; interpolate
  eepc = (ind+alpha) ; baseline ee%%
  return,eepc
end

function encirc,data,cx,cy,dr,method = meth,normalize = normalize
  if (n_elements(dr) eq 0) then begin
    ret = radialavg(data,cx,cy)
    ret = encircum(ret)
  endif else begin
    if (n_elements(meth) ne 0) then method = meth else method = 1
    n = (size(data))[1]
    m = (size(data))[2]
    x = (ones(n) ## findgen(n) - cx)
    y = (findgen(n) ## ones(m) - cy)
    rmax = min([max(x),max(-x),max(y),max(-y)]) ; assumes you want to compute ee over only the largest enclosed circle
    r = round(sqrt(x^2 + y^2))
    mask = (r lt rmax)
    ri = round((r*mask)/dr)
    nee = (rmax/dr)
    ee = fltarr(2,nee)
    ee[0,*] = findgen(nee)
    if (method eq 1) then begin
      dm = data*mask
      for iy = 0,n-1 do begin
        for ix = 0,n-1 do begin
          k = ri[ix,iy]
          ee[1,k] += dm[ix,iy]
        endfor
      endfor
      ee[1,*] = shift(integral(reform(ee[1,*])),1)
      ee[1,0] = 0
    endif
    if (method eq 2) then begin
      for i=0,nee-1 do begin
        ee[0,i] = i*dr
        ee[1,i] = total(data*(r lt i*dr))
      endfor
    endif
    ret = ee
  endelse
  if (keyword_set(normalize)) then ret[1,*] /= max(ret[1,*])
  return,ret
end
;
;  code test
n = 1024
data = abs(randomn(seed,n,n))
cx = n/2
cy = n/2
ee = encirc(data,cx,cy,1,method = 1,/normalize)
window,0
plot,ee[0,*],ee[1,*],charsize=2
ee2 = encirc(data,cx,cy,1,method = 2,/normalize)
oplot,ee2[0,*],ee2[1,*],psym=1,color = 180
window,2
plot,ee[0,*],ee[1,*]-ee2[1,*],charsize=2
end

