;+
;  windowdata - impose a window on each frame of a data cube
;
;  USAGE:
;    windowdata, incube, outcube, window
;
;-
pro windowdata,incube,outcube,w
  if n_params() eq 0 then begin
    print,'usage: windowdata, incube, outcube, window'
    return
  end
  s = size(incube)
  if (s(0) eq 3) then nf = s(3) else nf = 1
  n = s(1)
  m = s(2)
  if n_elements(outcube) eq 0 then outcube = make_array(n,m,nf)
  for i=0,nf-1 do begin
    outcube(*,*,i) = incube(*,*,i)*w
    print,format='($,I0," ")',i
  endfor
  print
end
