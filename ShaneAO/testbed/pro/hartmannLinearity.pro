;+
; HartmannLinearity.pro -
;   Test the linearity of the Hartmann sensor for the Villages experiment
;
;-
microns = 1.e-6
cm = 1.e-2
arcsec = !pi/(180.*3600.)
i = complex(0.,1.)

pixelSize_set = [1.0,1.25,1.5]*arcsec
n_cases = (size(pixelSize_set))[1]
;r0_set = [7.,11.,15.]*cm
;ncases = (size(r0_set))[1]

for theCase = 0,n_cases-1 do begin

n = 256
Dtele = 1.0
d = Dtele/9.
r0 = 10.*cm; r0_set[theCase]; 15.*cm
lambda = 0.5*microns
pixelSize = pixelSize_set[theCase]; 1.5*arcsec ; per pixel on wfs ccd
dx = pixelSize/float(16.)
du = lambda/(n*dx)
n_finepointsPerSubap = round(d/du)
subap = rectangle(n,n,n/2,n/2,n_finepointsPerSubap,n_finepointsPerSubap,1.0)
screenSize = n*du
print,'screenSize = ',screenSize

f = screengen(n,n,r0,du)
fx = ((findgen(n)-n/2) # ones(n))*du
fy = transpose(fx)
n_binned = n/16

n_realizations = 100
cents = fltarr(2,n_realizations)
trueCents = fltarr(2,n_realizations)
fp_hist = fltarr(n,n,n_realizations)
bfp_hist = fltarr(n_binned,n_binned,n_realizations)
for realization = 0,n_realizations-1 do begin
	ph = screengen(f,seed)

	wf = subap*exp(i*ph)
	wf *= exp(-!pi*i*(fx+fy)) ; 1/2 pixel shift
	fp = abs(ft(wf))^2
	fp_hist[*,*,realization] = fp
	trueCents[*,realization] = (centroid(fp) - n/2+0.5)*dx

	binnedfp = rebin(fp,n_binned,n_binned)
	bfp_hist[*,*,realization] = binnedfp
	fieldStop = rectangle(n_binned,n_binned,n_binned/2,n_binned/2,4,4,1.0)
	cents[*,realization] = (centroid(binnedfp*fieldStop)-n_binned/2+0.5)*pixelSize
	print,'<hartmannLinearity> realization = ',realization
;	print,'<hartmannLinearity> cx = ',cents[0,realization],' true cx = ',truecents[0,realization]
endfor

loadct,3
;plot,trueCents[0,*]/arcsec,xtitle='realization',ytitle='arcsec'
;oplot,trueCents[1,*]/arcsec,color=120
;oplot,cents[0,*]/arcsec,psym=1
;oplot,cents[1,*]/arcsec,psym=1,color=120

window,/free,xsize=500,ysize=500,title='case '+strtrim(theCase,2)+' response curve'
plot,[-1,1],[-1,1],xtitle='true cents',ytitle='centroider cents',title = 'pixel size = '+strtrim(pixelSize/arcsec,2)+' arcsec'
oplot,trueCents[0,*]/arcsec,cents[0,*]/arcsec,psym=1
oplot,trueCents[1,*]/arcsec,cents[1,*]/arcsec,psym=1,color=160
xyouts,-.75,.75,'r0 = '+strtrim(r0,2)

; statistical analyses
; histogram
centdifs = cents - trueCents
nbins = 21
range = [-0.5,0.5]
dc = (range[1]-range[0])/float(nbins)
hx = histogram(centdifs[0,*]/arcsec,max=range[1],min=range[0],nbins=nbins)
hy = histogram(centdifs[1,*]/arcsec,max=range[1],min=range[0],nbins=nbins)
bins = (findgen(nbins)-nbins/2)*dc
window,/free,xsize=500,ysize=500,title='case '+strtrim(theCase,2)+' histogram'
plot,bins,hx,ytitle='counts',xtitle='centroid difference, arcsec',title='Histogram of centroid differences'
oplot,bins,hy,color=160
; standard deviation
if (theCase eq 0) then sd_hist = fltarr(2,n_cases)
sdx = stddev(centdifs[0,*])/arcsec
sdy = stddev(centdifs[1,*])/arcsec
sd_hist[*,theCase] = [sdx,sdy]
print,'<hartmannLinearity> case ',strtrim(theCase,2),' standard dev. centroids: ',strtrim(sdx,2),',',strtrim(sdy,2),' arcsec'
wait,.01
endfor ; cases

print,'<hartmannLinearity> standard deviations in arcsec:'
print, sd_hist
;disp,fp_hist,'fp',dx=[1,1]*dx/arcsec,x0=-[1,1]*(n/2)*dx/arcsec
;disp,bfp_hist,'binned fp',dx=[1,1]*pixelSize/arcsec,x0=-[1,1]*(n_binned/2)*pixelSize/arcsec

end
