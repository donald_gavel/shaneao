;
;  remart.pro - attempt to remove IR array artifacts by Fourier filtering
;
function convolve,a,b
  return, iffts(conj(ffts(a))*ffts(b))
end

function ffts,a
  n = (size(a))(1)
  return, shift(fft(shift(a,n/2,n/2)),n/2,n/2)
end

function iffts,a
  n = (size(a))(1)
  return, shift(fft(shift(a,n/2,n/2)),n/2,n/2)/n^2
end
  
function remart,win
  u = make_array(256,256)
  u(128,*) = 1
  u(127,*) = .5
  u(125,*) = 1
  u(113,*) = 1
  u(110,*) = 1
  u(*,128) = 1
  uu = float(iffts(conj(ffts(u))*win))
  uu = (uu/max(uu))(0:128,*)
  return, uu
end

dir = '/duck1/gavel/keck/jun96/titan/titan709/'
ps = rdview(dir + 'titan709_ps')
ph = rdview(dir + 'titan709_phase')
ref_ps = rdview(dir + '../psfstar749/psfStar749_ps')
ref_ph = rdview(dir + '../psfstar749/psfStar749_phase')
mtf = rdview(dir + '../../../sep96/io/io242/keckmtfKp')
win = supergauss(256,128,128,55,10)
uu = remart(win)
psu = ((1-uu)>0)*ps
r = float(spdeconvolve(psu,ph,ref_ps,ref_ph,mtf,0,100,.99))
; rr = rot(reverse(r),55.37)
; tp=readfits('titan709_prep.fits')
; l = total(tp,3)/100
; lr=rot(reverse(l),55.37)
; rl = rr/(lr>60)
end
