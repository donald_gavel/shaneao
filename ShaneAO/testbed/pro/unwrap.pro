; phase unwrapper, using fourier transform method
;
function unwrap,data,ap,verbose=verbose
  n0 = (size(data))[1]
  m0 = (size(data))[2]
  n = n0
  m = m0

  fixBoundary =1
  if (n_elements(ap) eq 0) then begin
    fixBoundary = 0
    ap = ones(n,m)
  endif

  if (m lt n) then begin
    data2 = data[(n/2)-m/2:(n/2)+m/2-1,*]  ; use a square grid
    ap2 = ap[(n/2)-m/2:(n/2)+m/2-1,*]
    n = m
  endif else begin
    data2 = data[*,(m/2)-n/2:(m/2)+n/2-1]
    ap2 = ap[*,(m/2)-n/2:(m/2)+n/2-1]
    m = n
  endelse
  avedata = mean(data2)
  ind = where(ap2)
  phase0index = ind[0]

  sx = (data2 - shift(data2,1,0))
  sy = (data2 - shift(data2,0,1))

  if (keyword_set(verbose)) then print,'<unwrap> taking gradients' & wait,.001
  sxu = sx
  syu = sy
  ind = where(sx gt !pi)
  if ((size(ind))[0] ne 0) then begin
    sxu[ind] = sx[ind] - 2*!pi
    phase0index = ind[0]
  endif
  ind = where(sx lt (-!pi))
  if ((size(ind))[0] ne 0) then sxu[ind] = sx[ind] + 2*!pi
  ind = where(sy gt !pi)
  if ((size(ind))[0] ne 0) then syu[ind] = sy[ind] - 2*!pi
  ind = where(sy lt (-!pi))
  if ((size(ind))[0] ne 0) then syu[ind] = sy[ind] + 2*!pi

  if (fixBoundary eq 1) then begin
    if (keyword_set(verbose)) then print,'<unwrap> fixing boundary' & wait,.001
    extend_grid,ap2,sxu,syu
  endif

  if (keyword_set(verbose)) then print,'<unwrap> restoring phase' & wait,.001
  i = complex(0,1.)
  dfx = (1./float(n))
  dfy = (1./float(m))
  fx = (ones(m) ## findgen(n)-n/2)*dfx
  fy = (findgen(m) ## ones(n)-m/2)*dfy
  f2 = fx^2 + fy^2
  f2[n/2,m/2] = 1.
  fsx = ft(sxu)*exp(!pi*i*fx) ;  incorporate 1/2 pixel Fourier shift
  fsy = ft(syu)*exp(!pi*i*fy)
  f_phihat = -i*(fx*fsx + fy*fsy)/(2*!pi*f2)
  f_phihat[n/2,m/2] = 0.
  phihat2 = real(ft(f_phihat,/inverse))
  phihat2 = phihat2 - total(phihat2*ap2)/total(ap2)

  n = n0
  m = m0
  phihat = fltarr(n,m)
  if (m lt n) then phihat[(n/2)-m/2:(n/2)+m/2-1,*] = phihat2*ap2
  if (m ge n) then phihat[*,(m/2)-n/2:(m/2)+n/2-1] = phihat2*ap2
  return,phihat
end
