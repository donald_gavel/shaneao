calculate_matrices = 0
n = 32
dtele = n/2
r0 = 1.
du = 1.
seed = 5
f = filtergen(n,n,r0,du)
fmask = fltarr(n,n)
fmask[n/2-8,n/2+4] = 1
fmask[n/2+8,n/2-4] = 1
s = screengen(f*fmask,seed)
ap = circle(n,n,n/2,n/2,dtele/2,1.)
x = ones(n) ## findgen(n)-n/2
y = transpose(x)
r = sqrt(x^2+y^2)
nphi = total(ap)
ind = where(ap ne 0)
ni = (size(ind))[1]

if (calculate_matrices) then begin
Cphi = fltarr(ni,ni)
for i1 = 0,ni-1 do begin
  for i2 = 0,ni-1 do begin
    x1 = [x[ind[i1]],y[ind[i1]]]
    x2 = [x[ind[i2]],y[ind[i2]]]
    r = norm(x1-x2)
    Cphi[i1,i2] = -0.5*(r/r0)^(5./3.) + g_function(norm(x1)) + g_function(norm(x2)) - a_function(0)
  endfor
endfor
outd = where(ap eq 0)
no = (size(outd))[1]
Rphi = fltarr(ni,no)
for i1 = 0,ni-1 do begin
  for i2 = 0,no-1 do begin
    x1 = [x[ind[i1]],y[ind[i1]]]
    x2 = [x[outd[i2]],y[outd[i2]]]
    r = norm(x1-x2)
    Rphi[i1,i2] = -0.5*(r/r0)^(5./3.) + g_function(norm(x1)) + g_function(norm(x2)) - a_function(0)
  endfor
endfor
cphi_inverse = invert(cphi)
endif

spr = s - total(s*ap)/total(ap)  ; remove piston
phi = spr[ind]
phihat = rphi ## cphi_inverse ## phi
phihata = fltarr(n,n)
phihata[outd] = phihat
phihata[ind] = phi
end

