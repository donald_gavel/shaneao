;+
;
pro flim,a,rate,mag=mag,win=win		;Display frames at a given rate.
; a = image cube
; rate = frames per second.
; mag = magnification factor to increase size of image using rebin
; win = window number
;
;  While the flim is running, type:
;      + to flick faster by a factor of 2
;      - to flick slower by a factor of 2
;      R to reverse
;      F to go forward
;      <space> to step one frame at a time
;      G to go again
;      Q to quit
;
; NAME:
;	FLIM
;
; PURPOSE:
;	Display frames from a data cube at a given rate.
;
; CATEGORY:
;	Image display, animation.
;
; CALLING SEQUENCE:
;	FLIM, A, Rate
;
; INPUTS:
;	A:	Byte data cube image, scaled from 0 to 255.
;
; OPTIONAL INPUT PARAMETERS:
;	Rate:	The flicker rate.  The default is 1.0 sec/frame
;
; KEYWORD PARAMETERS:
;	None.
;
; OUTPUTS:
;	No explicit outputs.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	None.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	cycle through data cube, display the frame number in the upper left
;
; MODIFICATION HISTORY:
;	DG 10/4/96 - modeled after IDL routine flip.pro
;-
common colors, r_orig, g_orig, b_orig, r_curr, g_curr, b_curr
on_error,2                        ;Return to caller if an error occurs

if n_elements(rate) eq 0 then rate = 1.0 ;Parameter there?
sfact = 1.5		;Speed steps

if n_elements(win) eq 0 then win = 0

if n_elements(mag) ne 0 then begin
  n = (size(a))[1]
  m = (size(a))[2]
  nframes = (size(a))[3]
  n_new = n*mag
  m_new = m*mag
  a_new = fltarr(n_new,m_new,nframes)
  for i=0,nframes-1 do begin
    a_new[*,*,i] = rebin(a[*,*,i],n_new,m_new,/sample)
  endfor
  flim,a_new,rate,win=win
  return
endif

na = size(a)
a_scaled = a - min(a)
a_scaled = a_scaled*255/max(a_scaled)
;Assume X or Windows
;   window, win, xsize=na[1], ysize=na[2]
    setwin, win, xsize=na[1], ysize=na[2]
;	if !d.window lt 0 then window, 0, xsize=na(1), ysize=na(2)
;    window, win, xsize=na[1], ysize=na[2]
;	cwin = !d.window
;	wset, cwin
;	print,'<flim> starting loop'
	while 1 do begin	;loop infinitely over each chl
;	    print,'<flim> top of infinite loop'
		i = 0
		dir = 1
		first = 0
		last = na(3)-1
		stopmode = 0
		while 1 do begin
;		    print,'<flim> i = ',i
			b = a_scaled(*,*,i)
			tv, b
			disp_txt, "frame " + string(i), "", 10
			wait,1./rate	;This also empties the graphics buffer
			chr = get_kbrd(stopmode) ;Read character
;			print,'<flim>',chr,stopmode
			case strupcase(chr) of
		"" :	stopmode = 0
		"G":	stopmode = 0		;go
		"+":	begin
				  rate = rate*sfact	;Faster
				  stopmode = 0
			    end
		"-": 	begin
				  rate = rate/sfact	;Slower
				  stopmode = 0
			    end
		" ":	stopmode = 1	;step one at a time
		"R":	dir = -1		;Reverse
		"F":	dir = 1			;Forward
		"Q":	goto,done1      ;Quit
		else:	stopmode = 0    ; stop stepping
			endcase
			i = i+dir
			if (dir eq 1) and (i gt last) then i = first
			if (dir eq -1) and (i lt first) then i = last
		endwhile
	endwhile
;
done1:	print,'<flim> done'
        return
;
end
