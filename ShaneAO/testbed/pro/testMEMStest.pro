dir = '/Volumes/My Book 1/IF_with_140_bias_applied'
cd,dir

m=32
g=992;number of pixels in one dimesion on zygo camera

;working aperture or DM corresponding to pixels on zygo
;measured previously and need to change if zygo position changes
;ap = circle(m,m,507,589,375,1);
ap=fltarr(g,g)
ap[230:825,315:910]= 1 
;ap[230:845,315:910]= 1 

rd_zygo, x, y, bias, file='pre_tests_140V_bias1.dat', swap_endian=1
bias = rotate(bias,7)
bias = detilt(depiston(bias,ap),ap)*ap

dm = fltarr(32,32)
n = 100
fileno = indgen(n)+100
mat = fltarr(n,n)
for i = 0,n-1 do begin
  filename = 'data.0'+strtrim(fileno[i],2)+'.dat'
  rd_zygo, x, y, imagei, file = filename, swap_endian = 1
  imagei = rotate(imagei,7)
  imagei = detilt(depiston(imagei,ap),ap)*ap
  imagei = imagei - bias
  x0 = coords(dm,i)
  for j = 0,i do begin
    x1 = coords(dm,j)
    dx = x0-x1
    if ( sqrt(total(dx^2)) lt 3) then begin
      filename = 'data.0'+strtrim(fileno[j],2)+'.dat'
      rd_zygo, x, y, imagej, file = filename, swap_endian = 1
      imagej = rotate(imagej,7)
      imagej = detilt(depiston(imagej,ap),ap)*ap
      imagej = imagej - bias
      mat[i,j] = total(imagei*imagej)/total(ap)
      print,i,j & wait,.001
    endif
  endfor
endfor
mat = (mat + transpose(mat))
for i = 0,n-1 do mat[i,i] = 0.5*mat[i,i]
disp,mat
end

    