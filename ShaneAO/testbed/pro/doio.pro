;  doio.pro - the equivalent of procedure doio from io.pro, but
;        in the main program

  if n_elements(io) eq 0 then getiodata, dataset, io, ioDark, iomdark, avedark
  badpix = mrdfits("/duck1/gavel/keck/sep96/io/badpix.fits",0)
;;  badpix = findbad(io(*,*,0))
  debad,iomdark,iomdark,badpix
  if n_elements(skyframe) eq 0 then skyframe = getiosky(dataset,badpix)
  remsky, iomdark, skyframe, iomsky
  devstripe, iomsky, iomsky
  dehstripe, iomsky, iomsky
;
  print,"flat-fielding"
  if n_elements(flat) eq 0 then flat = makeflat(badpix)
  iomsf = make_array(256,256,100)
  for i=0,99 do iomsf(*,*,i) = iomsky(*,*,i)/(flat>.5)
  cshiftg, iomsf, iomsfsw  ; shifted and windowed
;  analps, iomsfsw, aveps
  prepfile = dataset.basename + "_prep.fits"
  specklefile = dataset.basename + ".speckle"
; writefits,dataset.resultsdir + prepfile,long(iomsfsw)
;  --- at this point, process through the bispectrum code ---
;           fitstospeckle prepfile specklefile 100 256 256
;           frame io.frameparams basename
;  ----------------------------------------------------------
;  prepspd,dataset,ps,phase,ref_ps,mtf
;  ioimage = spdeconvolve(ps>1,phase,ref_ps,mtf(0:129,*),0,90,.99)
;  imagefile = basename + "_image.fits"
; writefits,dataset.resultsdir + imagefile,ioimage
end
