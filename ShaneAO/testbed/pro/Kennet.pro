; Kennet.pro
;   generate a phase screen for putting on the open-loop MEMS
;
n = 512
m = 512
r0 = 1.
du = 1.
f = screengen(n,m,r0,du)  ; generates filter for subsequent calls
s = screengen(f,seed)    ; generates random screen
s0 = s[0:399,0:399]
ap = ones(400,400)
s0 = detilt(depiston(s0,ap),ap)
end
