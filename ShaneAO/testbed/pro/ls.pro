;
; ls.pro
;    simple emulation of the unix ls command
;
pro ls,dir
	if (n_elements(dir) eq 0) then dir = ''
	print,transpose(file_search(dir+'*'))
end
