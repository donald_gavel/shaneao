  nzern = 10
  ztab = ztable(nzern)
  x = geom[0,*]
  y = geom[1,*]
  r = 3.5 ; particular to Lick!!!
  zcoef = fltarr(nzern)
  tip = average(cent[0,*])
  tilt = average(cent[1,*])
  tiptilt = [tip,tilt] # ones(ns)
  for i=1,nzern do begin
    zs = zernikeslope(x,y,r,ztab[0,i],ztab[1,i])
    if ztab[2,i] eq 0 then begin
      zs = real(zs)
    endif else begin
      zs = imaginary(zs)
    endelse
    zcoef[i-1] = total(zs*(cent-tiptilt))
  endfor
end
