;
;  sf.pro - calculate the sample structure function of data
;
;   D_phi = 1/N Integral( [phi(x) - phi(x+r)]^2 dx )
;
;  revision: 12/16/04 DG
;     allow for aperture (ap) and two types of normalization:
;      0: divide by the number of points where both x and x+r are in the aperture
;      1: divide by the number of points where x is in the aperture
;
function sf,phi_arg,ap,normalization=normalization
  s = size(phi_arg)
  n = s[1]
  m = s[2]
  type = s[3]
  if (type eq 5) then begin
    phi = dblarr(2*n,2*m)
    w = dblarr(2*n,2*m)
    fuzz = 1.d-16
  endif else begin
    phi = fltarr(2*n,2*m)
    w = fltarr(2*n,2*m)
    fuzz = 1.e-6
  endelse
  if (n_elements(ap) eq 0) then ap = ones(n,n)
  if (n_elements(normalization) eq 0) then normalization = 0

  phi[n/2:n+n/2-1,m/2:m+m/2-1] = phi_arg*ap
  w[n/2:n+n/2-1,m/2:m+m/2-1] = ap
  phif = ft(phi)
  phi2f = ft(phi^2)
  wf = ft(w)

  nm = 2*long(n)*2*long(m)
  ww = real(ft(wf*conj(wf),/inverse))*nm ; cw = convolution of window with itself
  phiphi = real(ft(phif*conj(phif),/inverse))*nm ; cphi = convolution of windowed phase with itself
  phi2w = real(ft(phi2f*conj(wf),/inverse))*nm ; cphiw = convolution of squared windowed phase with window
  A = total(w)
  s2phi = total(phi^2)/A
  wwmask = ww ge (1-fuzz)

  if (normalization eq 0) then begin
    dphi = 2*(phi2w-phiphi)
    dphi = dphi / (ww*wwmask + (1-wwmask))
    dphi = dphi*wwmask
  endif else begin
    dphi = s2phi + (phi2w-2*phiphi)/A
  endelse
  return,dphi
end
; Using the sf.pro routine.
; This calculates the sample average structure function over point
; pairs only within the specified aperture.
; The radial average structure function with an infinite aperture
; would grow as r^(5/3)
; but an apertured and tip/tilt removed case is more complicated.
;
; The power spectrum S_phi(k) = -(1/2)*D_phi(x) where D_phi(x) is the structure function.
;
;Removing piston has the effect of multiplying the S_phi(k) = k^(-11/3) Kolmogorov power spectrum by
; 1 - P(k) = 1 - (beselj(!pi*D*k,1)/(!pi*D/2*k))^2
;
; and removing Tip/Tilt has the effect of multiplying by
; 1 - T(k) = 1 - 4*(beselj(!pi*D*k,2)/(!pi*D/2*k))^2
;
; where D is the diameter of the circular aperture.
;
; So a model fit of the data power spectrum ( ft(sf(phi,ap))) would
; need to match a curve
; k^(-11/3)*(1-P(k))*(1-T(k)) to conclude it is Kolmogorov within the aperture.
