; magrot.pro - magnify and rotate
;     image - input array
;     r - rotation angle, degrees clockwise
;     m - magnification factor
;     cx,cy - center of rotation - pixel in original image
;

function magrot, image, r, m, cx, cy
  n = size(image)
  x = rebin(image,n(1)*m, n(2)*m, /sample)
  if n_elements(cx) eq 0 then xr = rot(x,r,1) else xr = rot(x,r,1,cx,cy)
  return, xr
end

