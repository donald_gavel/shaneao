;
; Simulation of the vibration noise-canceler
;  closed loop tt version
;
ms = 1.e-3
dt = 1.0*ms
n = 2048;1024
;  --- parameters ---
; noise generator
omega = 2*!pi*43.  ; 40 Hz
tau_phi = 500.*ms
tau_amp = 1000.0*ms
; controller
ttgain = 0.5
; phase lock loop
if (n_elements(pll) eq 0) then pll = {input:0.,Ichan:0.,Qchan:0.,phase:0.,dphase:0.,amp:0.,damp:0.,f:0.,avgTime:0.,phaseGain:0.,ampGain:0.}
pll.f = 43
pll.avgTime = .1  ;   0.01 (10 ms)
pll.phaseGain = 0.1
pll.ampGain = 0.1
; --- initialization ----
if (n_elements(restart) eq 0) then restart = 1
if (restart) then begin
  t=0
  u=0
  amp = 1
  phi = 1
  pll.amp=0
  pll.phase=0
  restart = 0
endif
if (n_elements(t) eq 0) then t = 0
if (n_elements(amp) eq 0) then begin
  amp = 1.
  phi = 1.
endif
t_hist = fltarr(n)
nt_save = fltarr(n,7)
; ----- simulation -----
; make a continuum background
cb = randomn(seed,n)
df = 1/(n*dt)
f = (findgen(n)-n/2)*df
f[n/2] = df/10.
cb_spectrum = abs(f)^(-1.)
cb_spectrum[n/2] = 0.
f_cb = ft(cb)*cb_spectrum
cbf = real(ft(f_cb,/inverse))
rms_cb = 0.3
cbf *= rms_cb/stddev(cbf)
;
if (n_elements(u) eq 0) then u = 0.
for k = 0,n-1 do begin
  if (n_elements(t) eq 0) then t = 0 else t = t + dt
  t_hist[k] = t
; noise generator
  rn = randomn(seed)
  amp = abs(amp+rn*(dt/tau_amp))
  rn = randomn(seed)
  phi = phi+rn*(dt/tau_phi)
  if (phi gt !pi) then phi -= 2*!pi
  if (phi lt -!pi) then phi += 2*!pi
  nt = amp*sin(omega*t+phi) + cbf[k]
; tip/tilt controller
  tterr = nt-u
  u = u + ttgain*tterr
; phase lock loop
  pll.input = tterr
  pll.Ichan = sin(2*!pi*pll.f*t+pll.phase)
  pll.Qchan = cos(2*!pi*pll.f*t+pll.phase)
  pll.dphase = (1-dt/pll.avgTime)*pll.dphase + (dt/pll.avgTime)*(pll.Qchan*pll.input)
  pll.phase = pll.phase + pll.phaseGain*pll.dphase
  pll.damp = (1-dt/pll.avgTime)*pll.damp + (dt/pll.avgTime)*(pll.Ichan*pll.input)
  pll.amp = pll.amp + pll.ampGain*pll.damp
  if (pll.phase gt !pi) then pll.phase -= 2*!pi
  if (pll.phase lt -!pi) then pll.phase += 2*!pi
  u = u + pll.amp*pll.Ichan; append vib control to regular control
;  diagnostics
  nt_save[k,0] = phi
  nt_save[k,1] = amp
  nt_save[k,2] = pll.phase
  nt_save[k,3] = pll.amp
  nt_save[k,4] = nt
  nt_save[k,5] = pll.Ichan
  nt_save[k,6] = tterr
endfor
; residual
resid = nt_save[*,6]
print,'signal rms: ',stddev(nt_save[*,4])
print,'vibration rms: ',stddev(nt_save[*,4]-cbf)
print,'residual rms: ',stddev(resid)

window,0,title='phase'
plot,t_hist,nt_save[*,0],yrange=[-1,1]*!pi
oplot,t_hist,nt_save[*,2],color=150

window,1,title='amp'
plot,t_hist,nt_save[*,1],yrange=[min(nt_save),max(nt_save)]
oplot,t_hist,nt_save[*,3],color=150
oplot,t_hist,nt_save[*,1]

window,2,title='noise, comp'
;wset,2
t0 = min(t_hist)
plot,t_hist,nt_save[*,4],xrange=[t0,t0+n*dt],yrange=[-2,2.]
;oplot,t_hist,nt_save[*,5],color=150
oplot,t_hist,nt_save[*,6],linestyle=1,color=150

; spectrum
window,3,title='noise, comp spectra'
;wset,3
plot,f[n/2+1:n-1],(abs(ft(nt_save[*,4]))^2)[n/2+1:n-1],xrange = [df,500],/xlog,/ylog,/nodata; [df,n/2*df]
;oplot,f[n/2+1:n-1],(abs(ft(nt_save[*,5]))^2)[n/2+1:n-1],color=150
oplot,f[n/2+1:n-1],(abs(ft(nt_save[*,4]))^2)[n/2+1:n-1]
oplot,f[n/2+1:n-1],(abs(ft(nt_save[*,6]))^2)[n/2+1:n-1],color=150
;
end
