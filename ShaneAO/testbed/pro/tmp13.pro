n=256
dx = 1.
df = 1./(n*dx)
fx = (findgen(n)-n/2) ## ones(n)*df
fy = transpose(fx)
h = fltarr(n,n)
h[n/2,n/2]=1.
h[n/2+1,n/2]=.5
h[n/2-1,n/2]=.5
Hf = ft(h)
Hff = ft(Hf,/inverse)
Hfi = 1./(Hf+1.d-7)
hi = ft(Hfi)/float(n)^2
plot,h[*,n/2],xrange=[n/2-10,n/2+10],psym=1,yrange=[-2,2]
oplot,real(hi[*,n/2])/10.,psym=2
oplot,real(u[*,n/2])/max(real(u)),color=100,psym=3
oplot,real(u[*,n/2])/max(real(u)),color=100
end
