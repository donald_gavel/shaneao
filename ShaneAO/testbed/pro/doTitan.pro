;  doTitan.pro - main do-it-all pre-processor for speckle files
;
 .compile,gettitandata
;   --- set up the dataset ---
;  enter the dataset number here
  dsn = '614'
  .run, titanDataset
;
;   --- first, pre-process the raw planet data ---
  gettitandata, dataset, titan, titanDark, titanmdark, avedark
  badpix = readfits("/duck1/gavel/keck/sep96/titan/badpix.fits")
;  badpix = findbad(titan(*,*,0))
  debad,titanmdark,titanmdark,badpix
  maxi = fltarr(2,100)
  for i=0,99 do maxi(*,i) = [min(titanmdark(*,*,i)),max(titanmdark(*,*,i))]
;  plot,maxi(0,*),yrange=[-500,50000] & oplot,maxi(1,*)
  skyframe = gettitansky(dataset,badpix)
  remsky, titanmdark, skyframe, titanmsky
  print,"flat-fielding"
  flat = makeflat(dataset,badpix)
  titanmsf = make_array(256,256,100)
  for i=0,99 do begin & titanmsf(*,*,i) = titanmsky(*,*,i)/(flat>.5) & print,format='($,I0," ")',i & endfor
;  cshiftg, titanmsf, titanmsfsw  ; shifted and windowed
  cshift, titanmsf, titanmsfs  ; shifted 
  devstripe, titanmsfs, titanmsfsds, dataset.backgroundarea_y
  dehstripe, titanmsfsds, titanmsfsds, dataset.backgroundarea_x
  sw = supergauss(256,128,128,55,10);  center=[128,128], width=55, power=10
  for i=0,99 do begin & titanmsfsds(*,*,i) = titanmsfsds(*,*,i)*sw & print,format='($,I0," ")',i & endfor
;
; --- write the speckle file ---
  specklefile = dataset.resultsdir + dataset.basename + ".spec"
  wrspec, specklefile, fix(titanmsfsds>0)
;
;  --- process through the bispectrum code ---
  outputfile = dataset.resultsdir + dataset.basename
  command = 'frame ' + specklefile + ' ' + outputfile
  spawn,command
;
;  --- read in the results ---
  ps = rdview(dataset.resultsdir + dataset.basename + "_ps")
  phase = rdview(dataset.resultsdir + dataset.basename + "_phase")
  mtf = rdview(dataset.mtffile)

;
;  --- pre-process the reference star data ---
  star = readfits(dataset.datadir + dataset.starfile)
  starDark = readfits(dataset.datadir + dataset.stardarkfile)
  aveStarDark = total(starDark,3)/100
  starmdark = make_array(256,256,100)
  for i=0,99 do begin & starmdark(*,*,i) = star(*,*,i) - aveStarDark & print,format='($,I0," ")',i & endfor
  debad,starmdark,starmdark,badpix
  remsky, starmdark, skyframe, starmsky
  starmsf = make_array(256,256,100)
  for i=0,99 do begin & starmsf(*,*,i) = starmsky(*,*,i)/(flat>.5) & print,format='($,I0," ")',i & endfor
  cshift, starmsf, starmsfs
  devstripe, starmsfs, starmsfs
  dehstripe, starmsfs, starmsfs
  starmsfsds = make_array(256,256,100)
  for i=0,99 do begin & starmsfsds(*,*,i) = starmsfs(*,*,i)*sw & print,format='($,I0," ")',i & endfor
  specklefile = dataset.resultsdir + dataset.starname + ".spec"
  wrspec, specklefile, fix(starmsfsds>0)
;
; --- speckle process the star data ---
  outputfile = dataset.resultsdir + dataset.starname
  command = 'frame ' + specklefile + ' ' + outputfile
  spawn, command
;
; --- read in results ---
  ref_ps = rdview(dataset.resultsdir + dataset.starname + "_ps")
  ref_phase = ref_ps*0
;  ref_phase = rdview(dataset.resultsdir + dataset.starname + "_phase")
;
; --- post-process: speckle deconvolution
  psp = pencil(ps,3.,25.); args are pencil width (degrees), untouched circle radius (pixels)
  titanimage = spdeconvolve(psp, phase, ref_ps, ref_phase, mtf, 0, 128, .99)
  imagefile = dataset.resultsdir + dataset.basename + "_image.fits"
  writefits,imagefile,titanimage
;
; --- display ---
  parang = -6.419106367209E+01;  set the telescope parang here
  disp, rot(reverse(zoomi(titanimage,4)),parang)
