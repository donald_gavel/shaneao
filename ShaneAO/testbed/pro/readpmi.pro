; readpmi.pro
;-------------------------------------------------------------------------------
; ReadPmi()                IP I/O Utility
;+
; NAME:   ReadPmi
;
; PURPOSE:   Read a Photometrics PMI 2 byte/pixel "16-bit" external image file
;            (*.pmi) into IDL or read the byte-swapped unix version of a PMI
;            file (*.upmi) into IDL.  Allocates heap storage for image.
;            Returns copy of file header and a heap pointer to the image data
;            thru the function arguments.  Return values are initialized at
;            the end of the procedure.
;
; CALLING SEQUENCE:
;   .run ReadPmi
;   ReadPmi(filename,pHdr,pImage,/keyword)
;
; INPUT PARAMETERS:
;   filename  file to be read.
;
; OUTPUT PARAMETERS:
;    hdr      file header with byte ordering appropriate for current OS.
;   pImage    Heap ptr to integer (2 byte/16 bit per pixel) 2D array with
;             byte ordering appropriate for current operating system.
;
; KEYWORD PARAMETERS:
;   Must set one of these keywords:
;       upmi  assume file to be read is a Unix PMIS file, ie. do not byte
;             swap upon reading in the unix environment; do byte swap upon
;             reading in Windows env.
;        pmi  assume file to be read is a DOS Photometrics PMIS file, ie byte
;             swap upon reading in the unix environment; do not byte swap upon
;             reading in Windows env.
;        u08  assume file to be read is a unix 8-bit image file.  File has a
;             Photometrics file header followed by byte data (1 8-bit
;             byte/pixel).
;             Do not byte swap upon reading in the unix environment; do byte
;             swap in Windows env.
;        w08  assume file to be read is a DOS 8-bit image file.  File has a
;             Photometrics file header followed by byte data (1 8-bit
;             byte/pixel).
;             Do byte swap upon reading in the unix environment; do not byte
;             swap in Windows env.
; RETURNS:
;   0 for success, <0 for error condition.
;
; FILES
;   spmisheader__define.pro  File header structure definition file.
;
; SIDE EFFECTS:
;   Allocates heap memory for header and image.
;
; RESTRICTIONS:
;
; PROCEDURE:
;   Checks IDL system variable VERSION.OS_FAMILY to determine current operating
;   system environment and compares with keyword parameter to determine
;   whether or not the file needs to be byte swapped upon reading.  The returned
;   data is made compatible with the current OS.
;
;  Table of file types, keyword parameters, and expected header field szID:
;
;       File Type          keyword param      hdr.szID
;       Unix PMIS              upmi             'UPMI'
;       DOS/Windows PMIS        pmi             'PMIS' or 'PMI '
;       Unix 8-bit              u08             'U08 '
;       DOS/Windows 8-bit       w08             'W08 '
;
; MODIFICATION HISTORY:
;   23-nov-95 MFeldman Created.
;   04-dec-95 MFeldman Cleaned up.
;   31-dec-95 MFeldman Use ip.h file for structure definitions.
;   15-jan-96 MFeldman Add keyword upmi for reading Unix PMIS files.
;   10-sep-96 MFeldman Checks current OS and file type keyword to determine
;                      whether byte swapping is necessary.
;   24-apr-97 MFeldman Add upm8 and pm8 file types.
;   14-sep-97 MFeldman Introduce heap ptr return arguments. Replace ip.h with
;                      spmisheader__define.pro for file header struc definition.
;   30-oct-97 MFeldman Replace upm8 with u08. Replace pm8 with w08.
;
;-------------------------------------------------------------------------------
function ReadPmi, filename, hdr, pImage, upmi=upmi, pmi=pmi, u08=u08, $
                  w08=w08

; test for keyword
if (not keyword_set(upmi)) and (not keyword_set(pmi)) and $
   (not keyword_set(u08))  and (not keyword_set(w08)) then begin
   print,'ReadPmi(): keyword not set.  File not read.'
   return, -1
endif

RHdr=   {sPmisHeader} ;Photometrics 2 byte/pixel "16-bit" image header structure
RetHdr= {sPmisHeader} ;Processed header with byte swapped entries for Unix

; is byte swapping necessary?
if ( ((!VERSION.OS_FAMILY eq 'unix')    and keyword_set(pmi))  or $
     ((!VERSION.OS_FAMILY eq 'Windows') and keyword_set(upmi)) or $
     ((!VERSION.OS_FAMILY eq 'unix')    and keyword_set(w08)) or $
     ((!VERSION.OS_FAMILY eq 'Windows') and keyword_set(u08))) then $
   swap=1  $       ;flag: perform byteswapping $
else       $
   swap=0          ;flag: do not perform byteswapping

; valid file?
result=findfile(filename,count=count1)
if count1 eq 0 then begin                ; file not found?
   print, 'ReadPmi(): file not found'
   return, -1
endif

; open file, read header
openr,inunit,filename,/get_lun
readu,inunit,RHdr

if swap then $      ;byte swap?
   RetHdr=swap_endian(RHdr) $   ;Swap byte order (Windows<-->unix) in header
else  $
   RetHdr=Rhdr                  ;no swap

print,'ReadPmi():  FILE: ',filename    ; Display filename
help,/structure,RetHdr     ; Display header info
print,'ReadPmi():  ID:      ',string(RetHdr.szID)
print,'ReadPmi():  Name:    ',string(RetHdr.szName)
print,'ReadPmi():  Comment: ',string(RetHdr.szComment)
print,' '

; test for correct keyword and header szID combinations
szID=strupcase(string(RetHdr.szID))
if (keyword_set(pmi)   and ((szID ne 'PMIS') and (szID ne 'PMI ')) or  $
   (keyword_set(upmi)  and  (szID ne 'UPMI'))  or $
   (keyword_set(u08)   and  (szID ne 'U08 '))  or $
   (keyword_set(w08)   and  (szID ne 'W08 ')) )   $
   then begin
   print,'ReadPmi():  keyword mismatches header ID.  Data not read.'
   return, -2
endif

; Read image data directly into IDL image array; close file
if keyword_set(pmi) or keyword_set(upmi) then $       ;2 bytes per pixel
   RetImage=intarr(RetHdr.nXsiz,RetHdr.nYsiz) $
else if keyword_set(w08) or keyword_set(u08) then $   ;1 byte per pixel
   RetImage=bytarr(RetHdr.nXsiz,RetHdr.nYsiz)
readu,inunit,RetImage
close,inunit
print,'ReadPmi():  Unit=',inunit,' freed'
free_lun,inunit

print,"ReadPmi():  File image read. File closed"

if swap then $                ;swap byte order (Windows<-->unix) in data?
   byteorder,RetImage,/SSWAP  ;swap

; return values through function arguments
hdr= RetHdr                          ;return header value thru function argument
;pImage= ptr_new(RetImage, /no_copy)  ;switch to heap storage for return ptr
pImage = RetImage

return,0
end
