;+
;  include.pro
;-

pro include,filename
  catch, error_status
  if error_status ne 0 then begin
    print,'<include>...'
    return
  endif
  resolve_routine,filename
end

