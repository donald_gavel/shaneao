;+
;  combine.pro - combine two images with different ND
;
;  inputs:
;    sat - saturated image (byte)
;    unsat - unsaturated image (byte)
;    nd - nd difference of unsat and sat images (>0)
;    shift - amount to shift unsat image to line up with sat image [x,y]
;-
function combine,sat,unsat,nd,shift=shift
  if n_elements(shift) eq 0 then shift = [0,0]
  sf = 10^nd
  unsatScaled = shift(long(unsat)*sf,shift[0],shift[1])
  satScaled = long(sat)
  masksat = satScaled ge (.9*256)
  maskunsat = unsatScaled ge (.9*256)
  res = unsatScaled*maskunsat + satScaled*(1-maskunsat)
  return,res
end

