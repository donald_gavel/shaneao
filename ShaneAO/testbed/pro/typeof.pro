;+
;   typeof - determine the type of an expression
;
;  USAGE:
;    result = typeof(expr)
;
;  OUTPUT:
;    result = 0-undefined, 1-byte, 2-int, 3-long, 4-float,
;             5-double, 6-complex, 7-string, 8-structure, 9-doubleComplex
;-

function typeof,expr
  s = size(expr)
  n = s(0)
  return,s(n+1)
end
