;
;   subtract sky frame from data
;
pro remsky,datain,skyframe,dataout
  if n_params() lt 2 then begin
    print,"usage: remsky,datain,skyframe,dataout"
    return
  end
  nx = (size(datain))(1)
  ny = (size(datain))(2)
  n = (size(datain))(3)
  print,"remsky: subtracting sky background..."
  if n_elements(dataout) eq 0 then dataout = make_array(nx,ny,n)
  for i=0,(n-1) do begin
    dataout(*,*,i) = datain(*,*,i) - skyframe
    print,format='($,I0," ")',i
  endfor
  print,'done'
end
