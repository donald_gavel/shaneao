;+
; NAME:
;       disp, label
; PURPOSE:
;       display an array in a new window.
;       label an existing window.
; CALLING SEQUENCE:
;
;       disp, array, atitle
;       label, windowNo, title
;
; INPUTS:
;       array = a 2-D array
;       title = string - will be printed in upper left corner of display
;       windowNo = window number to label
;
; HISTORY:
;       written, Don Gavel, 1997
;-

pro disp,array,atitle
  n = size(array)
  window,(!d.window+1),xsize=n(1),ysize=n(2)
  tvscl,array
  if n_elements(atitle) gt 0 then disp_txt, atitle,"", 10
  return
end

pro label,wno,atitle
  wset,wno
  disp_txt, atitle,"", 10
  return
end
