function doit,file
dir = 'F:\Vision\Data\Dec21_01\vision_data_noWaffle\DonNW\'
;file = 'ff_m66_150v_532nm_closeloop.tif'
ff = read_tiff(dir+file)
c=[620,708]
m = 496 ; median(ff0)
a = float(ff[c[0]-128:c[0]+127,c[1]-128:c[1]+127]) - m
airynull = 24.5 ; pixels:  0.22*lam/dtele = 26*pixelscale
lambda = 0.633
dtele = 0.008
arcsec = !pi/(180.*3600.)
microns = 10.^(-6)
pixelscale = (1.22*lambda*microns/dtele)/airynull/arcsec
;print,'pixelscale =',pixelscale*arcsec
s = strehl(a,lambda,pixel=pixelscale,dp=dtele,model=psf0)
psf = a/total(a)
print,file,': ',s
return,psf
end
;
psf = fltarr(256,256,5)
file = 'ff_m66_0v_532nm_closeloop.tif'
psf[*,*,0] = doit(file)
file = 'ff_m66_50v_532nm_closeloop.tif'
psf[*,*,1] = doit(file)
file = 'ff_m66_100v_532nm_closeloop.tif'
psf[*,*,2] = doit(file)
file = 'ff_m66_120v_532nm_closeloop.tif'
psf[*,*,3] = doit(file)
file = 'ff_m66_150v_532nm_closeloop.tif'
psf[*,*,4] = doit(file)
end
;   results
;ff_m66_0v_532nm_closeloop.tif:       1.00114
;ff_m66_50v_532nm_closeloop.tif:       1.03590
;ff_m66_100v_532nm_closeloop.tif:      0.987679
;ff_m66_120v_532nm_closeloop.tif:      0.931527
;ff_m66_150v_532nm_closeloop.tif:      0.639960
