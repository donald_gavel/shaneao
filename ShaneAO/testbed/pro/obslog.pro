;+
; obsLog.pro - create an observation log from the .spr files in a directory
;
;-
dir = 'C:\Documents and Settings\Don Gavel\My Documents\Adaptive Optics\LAO\VilLaGEs (VisibleLightLGSAOExperiments)\Data\080325\'
cd, dir
fileList = file_search(dir+'cent_*.spr')
n = n_elements(fileList)
openw,wunit,dir+'log.csv',/get_lun,width=200
printf,wunit,'file name',',','date',',','mode',',','matrix',',','AOLoop',',','TTLoop',',','centroider',',','rate',',','gain',',','int',',','Cwt1',',','Cwt2',',','TTGain'

for k=0,n-1 do begin
  str = ""
  dum = 0.0
  dim = fltarr(2)
  x0 = fltarr(2)
  dx = fltarr(2)

  openr, unit,fileList[k],/get_lun,error=err
  if (err ne 0) then begin
    print,!err_string
    continue
  endif

  readf,unit,ndim
  readf,unit,dum & dim[0]=dum
  readf,unit,dum & x0[0]=dum
  readf,unit,dum & dx[0]=dum
  readf,unit,dum & dim[1]=dum
  readf,unit,dum & x0[1]=dum
  readf,unit,dum & dx[1]=dum
  readf,unit,type
  readf,unit,format='(%"%s")',str & date=str
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	mode = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	matrix = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	AOLoop = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
    strs = strsplit(str,':',/extract)
  	TTLoop = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	centroider = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	rate = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	gain = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	int = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	Cwt1 = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	Cwt2 = strtrim(strs[1],2)
  readf,unit,format='(%"%s")',str
  	strs = strsplit(str,':',/extract)
  	TTGain = strtrim(strs[1],2)
  free_lun, unit
  printf,wunit,file_basename(fileList[k]),',',date,',',mode,',',matrix,',',AOLoop,',',TTLoop,',',centroider,',',rate,',',gain,',',int,',',Cwt1,',',Cwt2,',',TTGain
  wait,.01
endfor
free_lun, wunit
end
