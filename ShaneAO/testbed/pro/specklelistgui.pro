;
; Auto Save File For specklelist.pro
;
;  Thu Oct  2 03:22:30 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13

function readdatabase,file
  openr,1,file
  line = ''
  readf,1,line
  n = fix(line)
  data = replicate( $
          {datasetname:'', $
           basename:'', $
           datafile:'', $
           darkfile:'', $
           backgroundarea_x:[11,95], $
           backgroundarea_y:[170,231], $
           starname:'', $
           starfile:'', $
           stardarkfile:'', $
           skyfile:'', $
           skydarkfile:'', $
           flatfile:'', $
           flatdarkfile:'', $
           mtffile:'', $
           datadir:'', $
           resultsdir:''}, n)
  for i=0,n-1 do begin
    readf,1,line  ; blank separator line
    for j=0,3 do begin
      readf,1,line
      token = str_sep(strcompress(line),' ')
      data(i).(j) = token(2)
    endfor
    for j=4,5 do begin
      readf,1,line
      token = str_sep(strcompress(line),' ')
      data(i).(j) = [fix(token(2)),fix(token(3))]
    endfor
    for j=6,15 do begin
      readf,1,line
      token = str_sep(strcompress(line),' ')
      data(i).(j) = token(2)
    endfor
  endfor
  close,1
  return, data
end

PRO specklelistgui_Event, Event

  forward_function getTitanDatasetDef, getDefDatasetList

  common specklelistguicom, selection, databasefilename, data

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  t = size(ev)
  if t(0) eq 0 then evname = Ev else evname = Ev.name

  CASE evname OF 

  'LIST2': BEGIN
;      Print, 'Event for list widget'
      i = event.index
;      selection = ev.list(i)
      selection = i
      widget_control,event.top,/destroy
      END
  'BUTTON1': BEGIN
;      Print, 'Event for Other Database...'
      databasefilename = pickfile(path = dirname(databasefilename), $
                                  filter = '*.database')
      if databasefilename ne '' then begin
        labelwid = widget_info(/child, event.top)
        widget_control,labelwid, set_value = nodirname(databasefilename)
        data = readdatabase(databasefilename)
        n = (size(data))(1)
        list = strarr(n)
        for i = 0,n-1 do begin
          list(i) = data(i).basename
        endfor
        listwid = $
           widget_info(/sibling, $
           widget_info(/child, $
             event.top))
        widget_control,listwid,set_value = list
        endif
      END
  'BUTTON2': BEGIN
;      Print, 'Event for Cancel'
      selection = -1
      widget_control,event.top,/destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



function specklelistgui, arg, GROUP=Group

  common specklelistguicom, selection, databasefilename, data

  if n_elements(arg) eq 0 then begin
    arg = getDefDatasetList()
  endif

  if (size(arg))(0) eq 0 then begin ; string => database file
    databasefilename = arg
    data = readdatabase(databasefilename)
    n = (size(data))(1)
    list = strarr(n)
    for i = 0,n-1 do begin
      list(i) = data(i).basename
    endfor
  endif else begin ; list of strings => list of keys into titanDataset2
    data = 0
    databasefilename = 'titan datasets'
    list = arg
  endelse

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      /column, $
      MAP=1, $
      UVALUE={name:'MAIN13'})

  LABEL3 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL3', $
      VALUE=databasefilename)
;      VALUE=databasefilename, /dynamic_resize)

  LIST2 = WIDGET_LIST( MAIN13,VALUE=list, $
      UVALUE={name:'LIST2',list:list}, $
      YSIZE=8)

  BUTTON1 = WIDGET_BUTTON( MAIN13, $
      UVALUE = 'BUTTON1', $
      VALUE = 'Other Database...')

  BUTTON2 = WIDGET_BUTTON( MAIN13, $
      UVALUE = 'BUTTON2', $
      VALUE = 'Cancel')

  WIDGET_CONTROL, MAIN13, /REALIZE

  XMANAGER, 'specklelistgui', MAIN13, /modal

  if selection eq (-1) then return,(-1)

  if (size(data))(0) eq 1 then begin
    return, data(selection)
  endif else begin
    return, getTitanDatasetDef(list(selection))
  endelse

END
