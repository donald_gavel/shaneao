;
;  Kolmogorov.pro
;    create the Kolmogorov power spectrum, OTF and PSF
;

cm = 1.e-2
microns = 1.e-6
arcsec = !Pi/(180.*3600.)

lambda = 0.55*microns
r0_set = [12,15,18,20,25]*cm
n_r0cases = (size(r0_set))[1]
L0_set = [30.] ; meters
n_L0cases = (size(L0_set))[1]
fwhm_set = fltarr(n_r0cases,n_L0cases)
fwhm0_set = fltarr(n_r0cases)

doPlots = 1
doDisps = 0

for L0_case = 0,n_L0cases-1 do begin


for r0_Case = 0,n_r0cases-1 do begin

r0 = r0_set[r0_Case]; 20.*cm
L0 = L0_set[L0_case]; 30.
D = 30.
n = 1024
dr = 2.2*D/double(n)
dk = 1./(double(n)*dr) ; (1./(1.2*D))
sixPointEightEight = Double(8.)*Sqrt(2.)*( 3./5.*Gamma(6./5.) )^(5./6.)
zeroPointZeroTwoThree = Double(2.)*Sqrt(2.)*(5./3.)^(1./6.)*Gamma(6./5.)^(5./6.)*Gamma(11./6.) / $
                        (!Pi^(8./3.)*Gamma(1./6.))
x = (ones(n) ## dindgen(n) - n/2)*dr
y = transpose(x)
r = sqrt(x^2+y^2)
kx = (ones(n) ## dindgen(n) - n/2)*dk
ky = transpose(kx)
k = sqrt(kx^2+ky^2)
k0 = 1./L0
kmask = k lt max(kx)
rmask = r lt max(x)
;
;  structure function and power spectrum of atmospheric aberration
Dphi0 = sixPointEightEight*(r/r0)^(5./3.)
Sphi = zeroPointZeroTwoThree*r0^(-5./3.)*(k^2+k0^2)^(-11./6.)*kmask
Dphi = float(n)^2*abs(ft(Sphi))*dk^2
Dphi = 2*(Dphi[n/2,n/2]-Dphi)
;
;      diagnostic
	k1 = k[n/2+1:n-1,n/2]
	Sphi1 = Sphi[n/2+1:n-1,n/2]
	r1 = r[n/2+1:n-1,n/2]
	Dphi0_1d = Dphi0[n/2+1:n-1,n/2]
	Dphi_1d =  Dphi[n/2+1:n-1,n/2]
	scale = Dphi0_1d[0]/Dphi_1d[0]
	Dphi_1d *= scale
	if (doPlots) then begin
		window,/free
		plot,r1,Dphi0_1d,/xlog,/ylog,xtitle='r',ytitle='Dphi'
		oplot,r1,Dphi_1d
	endif
;Dphi *= scale
;
;  telescope OTF
u = r/D < 1.0
OTF_telescope = (2./!Pi)*(acos(u)-u*sqrt(1.-u^2))
;
;  atmosphere OTF
OTF_atmosphere0 = exp(-0.5*Dphi0)
OTF_atmosphere = exp(-0.5*Dphi)
;
OTF = OTF_telescope * OTF_atmosphere
OTF0 = OTF_telescope * OTF_atmosphere0
PSF = real(ft(OTF)) ; outer scale version
PSF0 = real(ft(OTF0)) ; infinite outer scale version
norm = 1./max(PSF0)
PSF /= max(PSF)
PSF0 /= max(PSF0)
dtheta = lambda/(n*dr)
theta_1d = findgen(n/2)*dtheta
PSF_1d = PSF[n/2:n-1,n/2]
PSF0_1d = PSF0[n/2:n-1,n/2]
;
if (doDisps) then begin
	disp,PSF,dx = [1,1]*dtheta/arcsec, x0=-[1,1]*(n/2)*dtheta/arcsec,'PSF '+strtrim(string(L0),2)+'m outer scale'
	disp,PSF0,dx = [1,1]*dtheta/arcsec, x0=-[1,1]*(n/2)*dtheta/arcsec,'PSF infinite outer scale'
	disp,PSF0-PSF,dx = [1,1]*dtheta/arcsec, x0=-[1,1]*(n/2)*dtheta/arcsec,'PSF difference'
endif
;
if (doPlots) then begin
	window,/free
	plot,theta_1d/arcsec,PSF_1d,title='r0 = '+strtrim(string(r0/cm),2)+' cm; '+'solid: '+strtrim(string(L0),2)+'m outer scale; dashed: infinite outer scale'
	oplot,theta_1d/arcsec,PSF0_1d,linestyle=2
	oplot,theta_1d/arcsec,abs(PSF_1d-PSF0_1d),linestyle=1
endif
;
; look up 1/2 power points
;
index = 0
while (PSF_1d[index] gt 0.5) do index += 1
fwhm = theta_1d[index]*2
index = 0
while (PSF0_1d[index] gt 0.5) do index += 1
fwhm0 = theta_1d[index]*2
print,'full width half max at r0 = ',+strtrim(string(r0/cm),2)+' cm :'
print,strtrim(string(L0),2)+' meter outer scale: ',strtrim(string(fwhm/arcsec),2),' arcsec'
print,'infinite outer scale: ',strtrim(string(fwhm0/arcsec),2),' arcsec'
wait,.1

fwhm_set[r0_Case,L0_case] = fwhm
fwhm0_set[r0_Case] = fwhm0

endfor ; cases

if (L0_case eq 0) then begin
	window,/free
	plot,r0_set, fwhm0_set/arcsec,xtitle='r0',ytitle='fwhm',ticklen=0.5,linestyle=2
endif
oplot,r0_set, fwhm_set[*,L0_case]/arcsec

endfor ; L0_case

end
