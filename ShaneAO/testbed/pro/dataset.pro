; dataset.pro - dataset definitions
;

; --------- set the dataset number here -----------
if n_elements(dsn) eq 0 then dsn = '242'
; -------------------------------------------------

print,'including dataset.pro with dsn = ' + dsn

case dsn of

'231': begin
  dataset.basename = "io231"
  dataset.iofile = "s00231.fits"
  dataset.darkfile = "s00230.fits"
  dataset.starname = "star281"
  dataset.starfile = "s00281.fits"
  dataset.stardarkfile = "s00280.fits"
  dataset.skyfile = "s00305.fits"
  dataset.skydarkfile = "s00304.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io231/"
  end
'242': begin
  dataset.basename = "io242"
  dataset.iofile = "s00242.fits"
  dataset.darkfile = "s00241.fits"
  dataset.starname = "star281"
  dataset.starfile = "s00281.fits"
  dataset.stardarkfile = "s00280.fits"
  dataset.skyfile = "s00305.fits"
  dataset.skydarkfile = "s00304.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io242/"
  end
'242a': begin
  dataset.basename = "io242"
  dataset.iofile = "s00242.fits"
  dataset.darkfile = "s00241.fits"
  dataset.starname = "star265"
  dataset.starfile = "s00265.fits"
  dataset.stardarkfile = "s00264.fits"
  dataset.skyfile = "s00305.fits"
  dataset.skydarkfile = "s00304.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io242/"
  end
'245': begin
  dataset.basename = "io245"
  dataset.iofile = "s00245.fits"
  dataset.darkfile = "s00244.fits"
  dataset.starname = "star281"
  dataset.starfile = "s00281.fits"
  dataset.stardarkfile = "s00280.fits"
  dataset.skyfile = "s00305.fits"
  dataset.skydarkfile = "s00304.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io245/"
  end
'245a': begin
  dataset.basename = "io245"
  dataset.iofile = "s00245.fits"
  dataset.darkfile = "s00244.fits"
  dataset.starname = "star265"
  dataset.starfile = "s00265.fits"
  dataset.stardarkfile = "s00264.fits"
  dataset.skyfile = "s00305.fits"
  dataset.skydarkfile = "s00304.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io245/"
  end
'315': begin
  dataset.basename = "io315"
  dataset.iofile = "s00315.fits"
  dataset.darkfile = "s00314.fits"
  dataset.starname = "star281"
  dataset.starfile = "s00281.fits"
  dataset.stardarkfile = "s00280.fits"
  dataset.skyfile = "s00308.fits"
  dataset.skydarkfile = "s00307.fits"
  dataset.datadir = "/duck1/gavel/keck/sep96/rawData/"
  dataset.resultsdir = "/duck1/gavel/keck/sep96/io/io315/"
  end
else: print, "dataset not defined: " + dsn
endcase
