;
;  dirname.pro - extract directory name from a filename
;
function dirname,filename
  n = rstrpos(filename,'/')
  if n eq (-1) then return,'./'
  s = strmid(filename,0,n)
  s = s + '/'
  return,s
end
