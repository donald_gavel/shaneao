;+
;  spotfactor.pro - calculate a guide star spot factor
;   (result in pixels)
;-
function spotfactor,a
  ma = max(a)
  p = coords(a,!c)
  num = total(a)
  denx = 2*total(a(p(0),*))
  deny = 2*total(a(*,p(1)))
  return,[num/denx,num/deny]
end
