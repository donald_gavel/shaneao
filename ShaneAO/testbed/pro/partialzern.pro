;+
;  partialzern.pro - take partial derivatives of
;        in and out of focus images with respect to zernike
;
;    pz = partialzern(n,m,ap,du,rp,f,dz,lambda)
;
;       INPUTS:
;          n,m - zernike index
;           ap - aperture mask
;           du - pixel size on aperture
;           rp - aperture radius
;            f - focal length
;           dz - out-of-focus shift
;       lambda - wavelength
;
;       OUTPUT:
;           returns, as a complex pair, partials of the 2 images with respect
;              to zernike mode amplitude
;           
;-
function partialzern,n,m,s,ap,du,rp,f,dz,lambda
  nn = (size(ap))(1)
  if (s eq 0) then ph = ap*real(zernike(nn,nn,rp/du,n,m))
  if (s eq 1) then ph = ap*imaginary(zernike(nn,nn,rp/du,n,m))
  ph = ph/rms(ph)
  alpha = 1.
  hat = do2im(ap,alpha*ph,du,f,dz,lambda)
  hat0 = do2im(ap,0*ph,du,f,dz,lambda)
  partialz = (hat - hat0)/alpha
  return,partialz
end

function do2im,ap,ph,du,f,dz,lambda
  n = (size(ap))(1)
  l = 1/((1/f)-(1/(f+dz)))
  i = complex(0,1)
  wf0 = ap * exp(i*ph)
  imL = abs(fresnel(wf0,du,l,lambda))^2
  dxL = du*dz/f
  imf = abs(shift(fft(wf0),n/2,n/2))^2
  dxf = lambda*f/(n*du)
  nf = (dxL/dxf)*n
  imf = congrid(imf((n-nf)/2:(n+nf)/2,(n-nf)/2:(n+nf)/2),n,n)
  return,complex(imf,imL)
end

pro dopartials,pzset
  n = 256
  du = 7./n
  rp = 1.5
  rs = .3
  f = 40*3.
  dz = 0.01
  lambda = 1e-6
  ap = circle(n,n,n/2,n/2,rp/du,1) - circle(n,n,n/2,n/2,rs/du,1)
  tab = ztable(10)
  pzset = complexarr(n,n,7)
  print,'doing partials'
  for k=3,9 do begin ; starts at focus
    pzset(*,*,k-3) = partialzern(tab(0,k),tab(1,k),tab(2,k),ap,du,rp,f,dz,lambda)
    print,format='($,I0," ")',k
  endfor
  print,'done'
end
