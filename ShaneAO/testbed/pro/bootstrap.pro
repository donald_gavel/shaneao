;
; bootstrap.pro
;   simulate bootstrapping the LGS
;

;---------------------------
; Physical Constants
;---------------------------
cm = 1.e-2
microns = 1.e-6
nm = 1.e-9
km = 1.e3
ms = 1.e-3
arcsec = !pi/(180.*3600.)
i = complex(0.,1.)
seed = 7; 9
F0 = 9.e9; flux of zero magnitude star in V band, ph/m^2/s)

;---------------------------
; Problem Parameters
;---------------------------
lambda = 589.*nm ; wavelength of propagation
r0 = 10.*cm      ; seeing parameter
d = 10.*cm       ; spatial filter scale for sensing
v = 20.          ; wind velocity meters/sec
sigma_fit = (lambda/(2*!pi))*sqrt(0.3*(d/r0)^(5./3.)) ; calculate fitting error
dtele = 1.0      ; telescope diameter
dsec = 0.3       ; secondary obscuration
zNa = 90.*km     ; altitude of sodium layer
texp = 1.*ms     ; exposure (sample) time
flux = 100.      ; return efficiency of the laser, photons/s/cm2/W (LGS mode)
m_v = 16.5         ; magnitude of natural star (NGS mode)
power = 5.       ; laser power, Watts
A = (!pi/4)*(dtele^2 - dsec^2) ; calculated area of the telescope
eta = .1         ; generic throughput/efficiency number (for example, on ~1/2 the PWFS light is inside the pupils)
;
n = 256          ; number of fine pixels in simulation
m = 4*n          ; length of blowing screen
du = 4./float(n) ; size of fine pixel
ap = circle(n,n,n/2,n/2,(dtele/2.)/du,1.) - circle(n,n,n/2,n/2,(dsec/2.)/du,1.) ; aperture
df = 1./(n*du)   ; size of frequency domain sample, in m^-1
dtheta = lambda/(n*du) ; size of fine pixel in far field (angle)
dx = zNa*lambda/(n*du) ; size of fine pixel in far field (at the sodium layer)
nf = (1./d)/df   ; number of frequency bins inside AO low-pass filter
ao_filter = zeropad(ones(nf,nf),n,n) ; ao low-pass filter in the spatial frequency domain
!p.charsize = 2

;-----------------------------
; pinhole definition
;-----------------------------
pinhole = circle(n,n,n/2,n/2,(1.22*lambda/dtele)/dtheta/2.,1) ; one Airy core diameter circle

;------------------------------
; generate the atmosphere
;------------------------------
forward_function blowingScreen_init, blowingScreen_get
blowingScreen
s = blowingScreen_init(n,m,r0,du,seed=seed)

;-----------------------------
; initialize the simulation loop
;-----------------------------
ao_ph = fltarr(n,n)   ; initial estimate of phase is zero
doUplink = 1 ; switch to do the uplink AO correction
doDownlink = 1 ; switch to do the downlink AO correction
save_images = 0
mode = 'NGS' ; 'LGS' or 'NGS'
; calculate number of photons per exposure
if (mode eq 'LGS') then nph = flux*(A/cm^2)*texp*power*eta
if (mode eq 'NGS') then nph = F0*A*texp*eta*exp(-m_v/2.5)

t = 0  ; starting time
nsteps = 1000
gain = .5 ; feedback gain
int_gain = .99 ; forgetting factor
;----
; diagnostic streams for simulation
if (save_images) then begin
  ff_laser_set = fltarr(n,n,nsteps)
  fp_lgs_set = fltarr(n,n,nsteps)
  ph_set = fltarr(n,n,nsteps)
endif
wfe_set = fltarr(nsteps)
snr_set = fltarr(nsteps)
phrms_set = fltarr(nsteps)

;---
; start of simulation loop
x_offset = 6.5; 0.78; 3.28 ; meters along screen
trigger = 1
snr = 0.

tset = findgen(nsteps)*texp
tperiod = 200*ms
cycle = sin(2*!pi*tset/tperiod+0.75*2*!pi) gt (-0.9)

for k=0,nsteps-1 do begin

  if (cycle[k] eq 0) then ao_ph = fltarr(n,n) ; turn off
     

  print,'time step ',strtrim(k,2),' t = ',strtrim(t,2) & wait,.1
  ph = blowingScreen_get(s,(v*t+x_offset)/du)
  if (save_images) then ph_set[*,*,k] = ph
  phrms_set[k] = rms(detilt(depiston(ph,ap),ap),ap)*(lambda/(2*!pi))/nm
;  phrms_set[k] = rms(depiston(ph,ap),ap) *(lambda/(2*!pi))/nm
  if (snr lt .2) then ao_ph *=0  ; reset dm if the snr gets bad - to prevent driving DM to oblivion
  
  ; ---
  ;  propagate the laser up from telescope to sodium layer (far-field)
  if (mode eq 'LGS') then begin
    phr = ph
    if (doUplink) then phr -= ao_ph                    ; this is the wavefront residual that the laser sees on uplink path
    phr = detilt(phr,ap)                               ; assume perfect uplink tip/tilt correction
    wf_laser = ap*exp(i*phr)
  endif
  if (mode eq 'NGS') then begin
    wf_laser = ones(n,n)
  endif
  ff_laser = abs(ft(wf_laser))                       ; this is the amplitude in the far-field
  if (save_images) then ff_laser_set[*,*,k] = ff_laser
  ;disp,ff_laser,x0=-(n/2)*[1,1]*dtheta/arcsec,dx=[1,1]*dtheta/arcsec,'ff_laser amplitude '+strtrim(doUplink)+','+strtrim(doDownlink)
  
  ; ---
  ;  create a glowing beacon, and propagate its light back down to the telescope
  
  wf_laser_z = ff_laser*exp(i*2*!pi*randomu(seed,n,n)) ; incoherent emmitters at sodium layer
  wf_laser_1 = ft(wf_laser_z)                          ; propagated to just above ground layer
  wf_laser_0 = wf_laser_1*exp(i*ph)                    ; then to just below ground layer
  if (doDownlink) then wf_laser_0 *= exp(-i*ao_ph)     ; correct the wavefront with the downlink AO
;  ph_0 = depiston(detilt(unwrap(arg(wf_laser_0)),ap),ap) ; compute the phase of the downlink wavefront, and perfectly remove tip/tilt
  ph_0 = depiston( unwrap(arg(wf_laser_0)) ,ap)           ; compute the phase of the downlink wavefront
  psf_amp = abs(ft(ap*exp(i*ph_0)))                    ; compute the amplitude of psf assuming uniform illumination
  fp_lgs = convolve(psf_amp^2,ff_laser^2)              ; convolve with the laser spot
  ; centroid on brightest point in focal plane
;  mv = max(fp_lgs)
;  ij = coords(fp_lgs,!c)
;  fp_lgs = shift(fp_lgs,n/2-ij[0],n/2-ij[1])
  ; end of max-tracker
  snr = sqrt(nph*total(pinhole*fp_lgs*(d^2/A))/total(fp_lgs))  ; figure what percentage of light got through the pinhole and landed on a subaperture sample
  snr_set[k] = snr
  print,'snr = '+strtrim(snr)
  if (save_images) then fp_lgs_set[*,*,k] = fp_lgs
  ;disp,fp_lgs,x0=-(n/2)*[1,1]*dtheta/arcsec,dx=[1,1]*dtheta/arcsec,'lgs focus '+strtrim(doUplink)+','+strtrim(doDownlink)
  
  ; ---
  ;  measure the return wavefront phase
  ph_wfs_f = ft(ph_0)*ao_filter
  ph_wfs = real(ft(ph_wfs_f,/inverse))                 ; low-pass filtered phase measured by the wfs
  noise_f = ft(randomn(seed,n,n))*ao_filter
;  noise = real(ft(noise_f,/inverse))*(d/du)*ap/snr     ; calculate noise according to the brigtness in the pinhole and filtered by the wfs
  noise = real(ft(noise_f,/inverse))*ap
  noise /= rms(noise,ap)
  noise /= snr ; calculate noise according to the brigtness in the pinhole and filtered by the wfs
  kkn = lambda/(2*!pi)/nm
  print,'noise  = '+strtrim(rms(noise,ap)*kkn,2)+' nm rms'+strtrim(doUplink)+','+strtrim(doDownlink)
  print,'pherr = '+strtrim(rms(ph_wfs,ap)*kkn,2)+' nm rms '+strtrim(doUplink)+','+strtrim(doDownlink)
  print,'pherr+noise = '+strtrim(rms(ph_wfs+noise,ap)*kkn,2)+' nm rms'+strtrim(doUplink)+','+strtrim(doDownlink)
  wfe_set[k] = rms(detilt(ph_wfs+noise,ap),ap)*(lambda/(2*!pi))
  
  ao_ph = int_gain*ao_ph + gain*(ph_wfs+noise)                  ; for next iteration
  t = t+texp
  
endfor

if (save_images) then begin
  disp,ff_laser_set,x0=-(n/2)*[1,1]*dtheta/arcsec,dx=[1,1]*dtheta/arcsec,'ff_laser'
  disp,fp_lgs_set,x0=-(n/2)*[1,1]*dtheta/arcsec,dx=[1,1]*dtheta/arcsec,'fp_lgs'
  disp,ph_set,ap=ap,'screen'
endif

window,/free,title='wfe'
plot,wfe_set/nm,xtitle='time, ms',ytitle='phase, nm',yrange=[0,750],thick=3,charsize=3,background=255,color=0
;oplot,sqrt(wfe_set^2-sigma_fit^2)/nm,color=120
oplot,phrms_set,color=100
rmsave = sqrt(0.1*(dtele/r0)^(5./3.))*(lambda/(2*!pi))/nm
oplot,[0,nsteps],[rmsave,rmsave],linestyle = 1,color=0

window,/free,title='snr'
plot,snr_set,xtitle='time, ms',ytitle = 'snr'

window,/free,title='cycle'
plot,cycle,xtitle='time, ms',ytitle='AO on / AO off',yrange=[-1,2],thick=3,charsize=3

end
