;+
;  rdview.pro - read a view format file
;
; usage:
;   a = rdview(basename)
;   val = rdview_param(basename,paramName)
;   info = read_view_parameters(basename)
;
; where
;   basename is the view filename, without the .sdt or .spr extension
;   paramName is the named parameter keyword
;   val is the named parameter value
;   info is a structure containing the basic signal information:
;     {ndim, dim_array[3], x0[3], dx[3], type}  (up to 3 dimensions)
;
; author: Don Gavel, LLNL (gavel1@llnl.gov)
;
;*----------------------------------------------------------------------*/
;*     (c) Copyright 1998 the Regents of the University                 */
;*         of California.  All rights reserved.                         */
;*                                                                      */
;*     Work performed under the auspices of the U.S. Department         */
;*     of Energy by the Lawrence Livermore National Laboratory.         */
;*     The United States Government retains certain rights therin       */
;*----------------------------------------------------------------------*/
;-

function read_view_parameters, filename,error=err
  info = {view_parameter, $
      ndim:1, dim:lonarr(3), x0:fltarr(3), dx:fltarr(3), type:2}
  openr, unit,filename+'.spr',/get_lun,error=err
  if (err ne 0) then begin
    print,!err_string
    return,info
  endif
  readf,unit,dum & info.ndim = dum
  for i=0,info.ndim-1 do begin
    readf,unit,dum & info.dim(i) = dum
    readf,unit,dum & info.x0(i) = dum
    readf,unit,dum & info.dx(i) = dum
  endfor
  readf,unit,dum & info.type = dum
  free_lun, unit
  return, info
end

; rdview_parm reads the parameter given the keyword
;  (works for the keyword header pair lines below the basic dimension data in the .spr file)
function rdview_param, filename,param,error=err
  info = {view_parameter, $
      ndim:1, dim:lonarr(3), x0:fltarr(3), dx:fltarr(3), type:2}
  openr, unit,filename+'.spr',/get_lun,error=err
  if (err ne 0) then begin
    print,!err_string
    return,info
  endif
  readf,unit,dum & info.ndim = dum
  for i=0,info.ndim-1 do begin
    readf,unit,dum & info.dim(i) = dum
    readf,unit,dum & info.x0(i) = dum
    readf,unit,dum & info.dx(i) = dum
  endfor
  readf,unit,dum & info.type = dum
  l = strlen(param)
  str = ''
  v = ''
  while ~eof(unit) do begin
    readf,unit,str
    p = strpos(str,param)
    if (p ne (-1)) then begin
      v = strtrim(strmid(str,p+l),2)
      break
    endif
  endwhile
  free_lun, unit
  return, v
end

function rdview,filename,nx,ny,nz,swap=swap,error=err
  if (n_params() eq 0) then return,0
  finfo = {view_parameter, $
    ndim:1, dim:lonarr(3), x0:fltarr(3), dx:fltarr(3), type:2}
  n = strpos(filename,'.sdt')
  if n gt 0 then begin
    basename = strmid(filename,0,n)
  endif else begin
    basename = filename
  endelse
  dataname = basename + '.sdt'
  if n_elements(nx) eq 0 then begin
    finfo = read_view_parameters(basename,error=err)
    if (err ne 0) then return,0
    nx = finfo.dim(0) & ny = 1 & nz = 1
    if finfo.ndim gt 1 then ny = finfo.dim(1)
    if finfo.ndim gt 2 then nz = finfo.dim(2)
  endif
  openr,unit,dataname,/get_lun,error=err
  if (err ne 0) then begin
    print,!err_string
    return,0
  endif
  case finfo.type of
 0: t = assoc(unit,make_array(nx,ny,nz,/byte))
 1: t = assoc(unit,make_array(nx,ny,nz,/int))
 2: t = assoc(unit,make_array(nx,ny,nz,/long))
 3: t = assoc(unit,make_array(nx,ny,nz,/float))
 4: t = assoc(unit,make_array(nx,ny,nz,/complex))
  endcase
  result = t(0)
  if (keyword_set(swap)) then result = swap_endian(result)
  free_lun, unit
  return,result
end
