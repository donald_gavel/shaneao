;  PowerSpecDemo.pro
; test the generation of power spectra
; and demonstrate the correct units
n = 4096
fs = 1000 ; 1 kHz
dt = 1./fs
t = findgen(n)*dt
df = fs/float(n)
f = findgen(n/2)*df
f0 = 50.
print,'f0 = ',f0,' Hz'
;  select and example by commenting out...
; we can do a sum of sinusoids
d = sin(2*!pi*f0*t) ; this is a sinusoid with a mean-square value of 1/2 nm
d = d + sin(2*!pi*2*f0*t)/4.
; or some random data
;d = smooth(randomn(seed,n),30)
;
window,/free
plot,t,d,xtitle='time, seconds',ytitle = 'nm',charsize=2,xrange=[0,dt*100]
print,'mean square of data: ',average(d^2),' nm^2'
fd = fft(d)
;plot,f,fsint[0:n-1] ; the IDL definition of forward FFT is (1/N)*sum( f(x) exp(-j 2 pi u x / N ) where u is the index in the resulting array
ps = abs(fd[0:n-1])^2/df ; units are nm^2 / hz
window,/free
;plot,f,2*ps[0:n/2-1],xtitle = 'Hz',ytitle = 'nm^2/Hz',charsize=2,xrange=[df,n*df]
powerSpectrumPlot,ps,fs ; this plots a "folded" spectrum = 2 x the positive frequency side of the symmetric power spectrum.
print,'total power in spectrum: ',total(ps)*df,' nm^2'
end
