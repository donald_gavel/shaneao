;+
;  mu.pro - find the illumination angle (mu0) and observer angle (mu)
;-
function mu,x,y,z,rx,ry,selat,selon,sslat,sslon
  degrees = !pi/180.
;  z = rx * sqrt(1 - (x/rx)^2 - (y/ry)^2)
  nhat = [x,y,z] / [rx,ry,rx]^2
  nhat = nhat / norm(nhat)
  alpha = selat*degrees
  beta = sslat*degrees
  lambda = (selon-sslon)*degrees
  obslos = [0,sin(alpha),cos(alpha)]
  sunlos = [sin(lambda)*cos(beta) , sin(beta), cos(lambda)*cos(beta) ]
  mu = obslos ## transpose(nhat)
  mu0 = sunlos ## transpose(nhat)
  return,[mu,mu0]
end
