;+
;  screenadd.pro - add two screens with differing pixel samples
;
; usage:
;  c = screenadd(a,b,p0a,da,p0b,db,p0c,dc, nc,mc) : c = a+b
;  screencoadd,a,b,p0a,da,p0b,dxb                : a = a+b
;
; inputs:
;   a,b - screens
;   p0a, p0b, p0c - 2-vector, positions associated with [0,0] pixel
;   da, db, dc - pixel size of each screen
;   nc,mc - desired size of result array
;
; outputs:
;   c - result screen
;
;-
function screenadd,a,b,p0a,da,p0b,db,p0c,dc,nc,mc,nointerp=nointerp,debug=debug

  interp = 1
  if (keyword_set(nointerp)) then interp = 0
  na = (size(a))[1]
  ma = (size(a))[2]
  nb = (size(b))[1]
  mb = (size(b))[2]
  axrange = [p0a[0],p0a[0]+(na-1)*da]
  ayrange = [p0a[1],p0a[1]+(ma-1)*da]
  bxrange = [p0b[0],p0b[0]+(nb-1)*db]
  byrange = [p0b[1],p0b[1]+(mb-1)*db]
  cxrange = [p0c[0],p0c[0]+(nc-1)*dc]
  cyrange = [p0c[1],p0c[1]+(mc-1)*dc]
;  find the intersecting area of a and c
  overlapacx = [max([axrange[0],cxrange[0]]),min([axrange[1],cxrange[1]])]
  overlapacy = [max([ayrange[0],cyrange[0]]),min([ayrange[1],cyrange[1]])]
;  find the intersecting area of b and c
  overlapbcx = [max([bxrange[0],cxrange[0]]),min([bxrange[1],cxrange[1]])]
  overlapbcy = [max([byrange[0],cyrange[0]]),min([byrange[1],cyrange[1]])]
;  index overlap area into a
  indexax = round([(overlapacx[0]-p0a[0])/da,(overlapacx[1]-p0a[0])/da])
  indexay = round([(overlapacy[0]-p0a[1])/da,(overlapacy[1]-p0a[1])/da])
;  index overlap area into b
  indexbx = round([(overlapbcx[0]-p0b[0])/db,(overlapbcx[1]-p0b[0])/db])
  indexby = round([(overlapbcy[0]-p0b[1])/db,(overlapbcy[1]-p0b[1])/db])
;  index a overlap area into c
  indexacx = round([(overlapacx[0]-p0c[0])/dc,(overlapacx[1]-p0c[0])/dc])
  indexacy = round([(overlapacy[0]-p0c[1])/dc,(overlapacy[1]-p0c[1])/dc])
;  index b overlap area into c
  indexbcx = round([(overlapbcx[0]-p0c[0])/dc,(overlapbcx[1]-p0c[0])/dc])
  indexbcy = round([(overlapbcy[0]-p0c[1])/dc,(overlapbcy[1]-p0c[1])/dc])
;
  if keyword_set(debug) then begin
    print,'<screenadd> a range: x = ',axrange,' y = ',ayrange
    print,'<screenadd> b range: x = ',bxrange,' y = ',byrange
    print,'<screenadd> c range: x = ',cxrange,' y = ',cyrange
    print,'<screenadd>'
    print,'<screenadd> overlapac: x = ',overlapacx,' y = ',overlapacy
    print,'<screenadd> overlapbc: x = ',overlapbcx,' y = ',overlapbcy
    print,'<screenadd>'
    print,'<screenadd> c overlap on a index: x = ',indexax, ' y = ',indexay
    print,'<screenadd> c overlap on b index: x = ',indexbx, ' y = ',indexby
    print,'<screenadd> a overlap on c index: x = ',indexacx, ' y = ',indexacy
    print,'<screenadd> b overlap on c index: x = ',indexbcx, ' y = ',indexbcy
  endif
;
;  form the c grid
  c = fltarr(nc,mc)
;  extract the a overlap area and resample on the c grid
  if ((indexax[1] ge indexax[0]) and (indexay[1] ge indexay[0])) then begin
    overlap = a[indexax[0]:indexax[1],indexay[0]:indexay[1]]
    nx = indexacx[1]-indexacx[0]+1
    ny = indexacy[1]-indexacy[0]+1
    if interp then overlap = congrid(overlap,nx,ny)
    c[indexacx[0]:indexacx[0]+nx-1,indexacy[0]:indexacy[0]+ny-1] = overlap
  endif
;  extract the b overlap area and add resampled on the c grid
  if ((indexbx[1] ge indexbx[0]) and (indexby[1] ge indexby[0])) then begin
    overlap = b[indexbx[0]:indexbx[1],indexby[0]:indexby[1]]
    nx = indexbcx[1]-indexbcx[0]+1
    ny = indexbcy[1]-indexbcy[0]+1
    if interp then overlap = congrid(overlap,nx,ny,/interp)
    c[indexbcx[0]:indexbcx[0]+nx-1,indexbcy[0]:indexbcy[0]+ny-1] = $
      c[indexbcx[0]:indexbcx[0]+nx-1,indexbcy[0]:indexbcy[0]+ny-1] + overlap
  endif
;  
  return,c
end

pro screencoadd,a,b,p0a,dxa,p0b,dxb
end

; tests
a = fltarr(100,100)
a[50-10:50+10,50-10:50+10]=1
p0a = [0,0]
da = 1.
b = fltarr(10,10)
b=b+1
p0b = [45,45]
db = 1.
dc = 1.
c = screenadd(a,b,p0a,da,p0b,db,p0c,dc,100,100)
;
db = 2.
c = screenadd(a,b,p0a,da,p0b,db,p0c,dc,100,100)
;
db = 1.5
c = screenadd(a,b,p0a,da,p0b,db,p0c,dc,100,100)
;
p0b = [85.,85.]
db = 1.5
dc=.75
c = screenadd(a,b,p0a,da,p0b,db,p0c,dc,300,300)
end
