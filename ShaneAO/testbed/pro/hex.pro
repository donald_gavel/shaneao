;+
;  hex.pro  - create a hexagonal pattern
;    inputs:  n-size of image
;             L-length of a side, in pixels
;-
function hex,n,L
  x = abs((findgen(n)#(fltarr(n)+1)-float(n-1)/2.))/L
  y = abs(((fltarr(n)+1)#findgen(n)-float(n-1)/2.))/L
  a = 1 - ((( x/sqrt(3.) +y ) gt 1 ) or ( x gt (sqrt(3.)/2)))
  return,a
end
