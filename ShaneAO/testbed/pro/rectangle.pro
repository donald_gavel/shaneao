;+
;  rectangle.pro - generate a filled rectangle
;
;   result = rectangle(n,m, cx,cy, sx,sy ,value)
;-
function rectangle, n,m, cx,cy, sx,sy, value
  if n_elements(value) eq 0 then value = 1
  ret = fltarr(n,m)
  a = round(cx-sx/2.) >0 < (n-1)
  b = round(cx+sx/2.-1) >0 < (n-1)
  c = round(cy-sy/2.) >0 < (m-1)
  d = round(cy+sy/2.-1) >0 < (m-1)
  ret[a:b,c:d] = value
  return,ret
end
