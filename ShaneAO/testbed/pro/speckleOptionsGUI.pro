;
; Auto Save File For /home/lumahai/nirc2/gavel/pro/speckleOptionsGUI.pro
;
;  Sun Oct 12 21:25:07 HST 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




PRO speckleOptions_Event, Event

  common speckleOptionsCommon, option
  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  CASE Ev OF 

  'BGROUP2': BEGIN
      IF Event.Select THEN Sel = 1 ELSE Sel = 0
      CASE Event.Value OF
      0: Print,'Button debad Turned ', Sel
      1: Print,'Button remove sky Turned ', Sel
      2: Print,'Button flat field Turned ', Sel
      3: Print,'Button centroid shift Turned ', Sel
      4: Print,'Button destripe vertical Turned ', Sel
      5: Print,'Button destripe horizontal Turned ', Sel
      6: Print,'Button supergaussian window Turned ', Sel
      ELSE: Message,'Unknown button pressed'
      ENDCASE
      option.(Event.Value) = Sel
      END
  'BUTTON7': BEGIN
      Print,'Event for Done'
      widget_control,event.top,/destroy
      END
  ENDCASE
  return
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO speckleOptionsGUI, theoption, GROUP=Group

  common speckleOptionsCommon, option

  if n_elements(theoption) eq 0 then begin
    theoption = {debad:1, removeSky: 1, flatField: 1, $
               centroidShift: 1, deVstripe: 1, deHstripe: 1, $
               window: 1}
  endif
  option = theoption

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='MAIN13')

  LABEL6 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL6', $
      VALUE='Options')

  Btns754 = [ $
    'debad', $
    'remove sky', $
    'flat field', $
    'centroid shift', $
    'destripe vertical', $
    'destripe horizontal', $
    'supergaussian window' ]
  BGROUP2 = CW_BGROUP( MAIN13, Btns754, $
      COLUMN=1, $
      NONEXCLUSIVE=1, $
      set_value = [ $
        option.debad, $
        option.removeSky, $
        option.flatField, $
        option.centroidShift, $
        option.deVstripe, $
        option.deHstripe, $
        option.window ], $
      UVALUE='BGROUP2')

  BUTTON7 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON7', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN13, /REALIZE

  XMANAGER, 'speckleOptions', MAIN13, /modal
  theoption = option
END
