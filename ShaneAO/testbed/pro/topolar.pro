;+
; topolar.pro - convert from rectangular to polar coordinates
;   see also: torect.pro
;
;  pa = topolar(a)
;
;   a = n x n array, organized as x,y values of a
;   pa = n/2 x 2*n array, organized as r,theta values of a
;-
function topolar,a
  n = (size(a))[1]
  pa = fltarr(n/2,2*n)
  r = findgen(n/2) # (fltarr(2*n)+1)
  th = (fltarr(n/2)+1) # ((findgen(2*n) - n)*(!pi/float(n)))
  x = round(r*cos(th)) + n/2; floor(r*cos(th)) + n/2 -- changed 4/12/07 DG
  y = round(r*sin(th)) + n/2; floor(r*sin(th)) + n/2
  inda = n*y + x
  pa = a(inda)
  pa = reform(pa,n/2,2*n)
return,pa
end
;
; test pattern
;
n = 64
a = fltarr(n,n)
a[n/2:n-1,n/2] = 1.
pa = topolar(a)
end
