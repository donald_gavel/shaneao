;
;   remove vertical stripe patterns
;
pro devstripe,datain,dataout,yspan
  if n_params() eq 0 then begin
    print,"usage: devstripe,datain,dataout,[yspan]"
    return
  end
  if n_elements(yspan) eq 0 then yspan = [222,255]
  na = yspan(1) - yspan(0) + 1
  nx = (size(datain))(1)
  ny = (size(datain))(2)
  if ((size(datain))(0) eq 2) then n = 1 else n = (size(datain))(3)
  print,"devstripe..."
  if n_elements(dataout) eq 0 then dataout = make_array(nx,ny,n)
  bra = make_array(nx,ny)
  for i=0,(n-1) do begin
    print,format='($,I0," ")', i
    br = total(datain(0:(nx-1),yspan(0):yspan(1),i),2)/na  ;  vertical stripes
    for j=0,(ny-1),1 do bra(*,j) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end
