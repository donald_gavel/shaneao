;+
;  imagestats.pro - find photostatistics
;    imagestats(image, blocksize)
;-

function imagestats,im,n
  ims = smooth(im,n,/edge_truncate)
  imvar = smooth((im - ims)^2,n,/edge_truncate)
  imc = real(ims) / (real(imvar) > .000001);  camera constant
;      electrons per digital number in regions where
;      illumination is above the read noise and
;      away from sharp contrast features (such as peak of star psf)
  return,max(smooth(imc,n))
end

;flat0 = rdspec('flat_0',10)
im = real(refset(*,*,0))*0
ctr = [545,471]
cmask = circle(900,900,ctr(0),ctr(1),rad,1.0)
nframes = 40
stat = fltarr(2,nframes)
for i=0,nframes-1 do begin
  print,format='($,I0," ")',i
  ctr = centroid(cmask*real(refset(*,*,i)))
  im = im + shift(real(refset(*,*,i)),450-ctr(0),450-ctr(1))
;  ims = smooth(im,n,/edge_truncate)
;  imvar = smooth((im-ims)^2,n,/edge_truncate)
;  stat(0,i) = ims(172*3,145*3)
;  stat(1,i) = imvar(172*3,145*3)
endfor
im = im/nframes - bias
print,'done'
end
