; Test workspace for disp guis development
;
pro dispBase_event, Event
 wTarget = (widget_info(Event.id,/name) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)

  wWidget =  Event.top
  !disp.activewindow = widget_info(wWidget,find_by_uname='dispBase')
;  print,'<dispBase_event> wWidget = ',wWidget
end

pro dispWidget_realizeDisplay, wWidget
  DEVICE, Decomposed=0, Bypass_Translation=0
  defsysv, "!image", exists = exists
  if (exists eq 0) then imagelib
  defsysv, "!aspect", exists=exists
  if (exists eq 0) then devicelib
;  dispWidget_redrawDisplay, wWidget
  dispBase = widget_info(wWidget,/parent)
  geo = widget_info(dispBase,/geometry)
  x = geo.scr_xsize
  y = geo.scr_ysize
;  dispWidget_resizeWindow, {id:dispBase, top:dispBase, handler:0L,  x:x, y:y}
  defsysv, "!disp", exists = exists
  if (exists eq 0) then begin
    defsysv,"!disp",{disp,activeWindow:dispBase,winCount:0,windows:lonarr(100)}
  endif
  !disp.activeWindow = dispBase
  !disp.windows[!disp.winCount] = dispBase
  !disp.winCount++
  !quiet=1
end
; --------------------
;  basic display properties
  percent = 0.01
  inch = 100. ; pixels per inch on display (mac desktop)
  inch = 149. ; pixels per inch on laptop (sony vaio)
  mm = inch/25.4 ; pixels per mm on display
  
;  initial size preferences
  space = 3 ; spacing between subwindow "children", pixels
  offset = [5,5] ; entire widget offset from upper left of screen, pixels
  defaultWindow_size = [4.04,4.21]*inch ; size of entire widget
  if (n_elements(window_size) eq 0) then window_size = defaultWindow_size
  minPrimaryDraw_size = [70,70]*mm
  secondaryDraw_size = [25,25]*mm

;  computed sub-element sizes
  border = [15,2] ; between main window and frame border of base window - magic number determined experimentally
  viewSelect_size = [.5,2]*inch
  toolSelect_size = [.5,2]*inch
  buttonGroup_size = [viewSelect_size[0]+toolSelect_size[0] + 40*space,max([viewSelect_size[1],toolSelect_size[1]])]
  valueDisplay_size = [25,5]*mm
  valueGroup_size = [1,4]*valueDisplay_size
  rightGroup_size = [max([secondaryDraw_size[0],buttonGroup_size[0],valueGroup_size[0]]) $
                    ,secondaryDraw_size[1]+buttonGroup_size[1]+valueGroup_size[1]]
  sliderGroup_size = [window_size[0],20*mm]
  frameScroll_size = [1,1]
  
;  shrink or enlarge the primary draw area to fit everything else
  window_size[1] = rightGroup_size[1] + sliderGroup_size[1] ; minimum window height
  display_size = window_size - border
  draw_size = window_size - [rightGroup_size[0],sliderGroup_size[1]] - [5,0]*space
  draw_size[0] = max([minPrimaryDraw_size[0],draw_size[0]])
  draw_size[1] = max([minPrimaryDraw_size[1],draw_size[1],rightGroup_size[1]])
  
; -------------------
  dispBase = Widget_Base( uname='dispBase'  $
      ,xoffset=offset[0] ,yoffset=offset[1] $
      ,scr_xsize=window_size[0] ,scr_ysize=window_size[1]  $
      ,title='IDL' ,space=space ,row=3 $
      ,/tlb_size_events, mbar = dispBase_mbar)
  
  displayBase = Widget_Base(dispBase, uname='displayBase' ,frame=1  $
      ,scr_xsize = display_size[0] ,scr_ysize = display_size[1]  $
      ,notify_realize='dispWidget_realizeDisplay' $
      ,title='IDL' ,column=3)
  
  mainDraw = Widget_Draw(displayBase, uname='mainDraw' $
      ,scr_xsize = draw_size[0] ,scr_ysize = draw_size[1] $
      ,/button_events ,/motion_events)  
  
  displayMarginFrame = Widget_Base(displayBase, uname='displayMarginFrame' ,frame=1 $
      ,scr_xsize = rightGroup_size[0] ,scr_ysize = rightGroup_size[1] $
      ,row = 3)
      
  secondaryDraw = Widget_Draw(displayMarginFrame, uname ='secondaryDraw' $
      ,scr_xsize = secondaryDraw_size[0], scr_ysize = secondaryDraw_size[1])
  
  buttonGroup = Widget_Base(displayMarginFrame, uname = 'buttonGroup', frame = 1 $
      ,scr_xsize = buttonGroup_size[0], scr_ysize = buttonGroup_size[1] $
      ,column = 2)
  
  valueGroup = Widget_Base(displayMarginFrame, uname = 'valueGroup', frame = 1 $
      ,scr_xsize = valueGroup_size[0], scr_ysize = valueGroup_size[1])
  
  viewSelect = Widget_Base(buttonGroup, uname='viewSelect', frame = 1 $
      ,scr_xsize = viewSelect_size[0] , scr_ysize = viewSelect_size[1]  $
      ,ROW = 6, /exclusive)
      
  toolSelect = Widget_Base(buttonGroup, uname = 'toolSelect', frame = 1 $
      ,scr_xsize = toolSelect_size[0], scr_ysize = toolSelect_size[1])
  
  Widget_Control, /realize, dispBase

  XManager, 'dispBase', dispBase, /no_block  
  
;  widget_control,dispBase,/destroy
end
