;+
;  geminiLaser.pro
;    simulate the Rayleigh return from the Gemini laser
;-

function proplaser,laserPower,psubap,gsno,rno,hrange,fpsave,pixelsize=pixelsize
  hmin = hrange[0]
  hmax = hrange[1]
  dh = hrange[2]
  nh = round((hmax-hmin)/dh) + 1
  dh = (hmax-hmin)/float(nh-1)

  microns = 1.0e-6
  cm = 1.0e-2
  ms = 1.0e-3
  km = 1.0e3
  arcsec = !pi/(180.*3600.)

;  receiving telescope
  du = 1.*cm
  n = 256
  d = 50.*cm  ; diameter of subap
  ap = circle(n,n,n/2,n/2,d/(2*du),1.)
  i = complex(0,1.)
  wf = complex(ap,0*ap)
  wf = wf / sqrt(total(abs(wf)^2))  ; normalize to total power = 1
  lambda = 0.589*microns

;  focal plane
  if (n_elements(pixelsize) eq 0) then begin
    dtheta = .5*arcsec
  endif else begin
    dtheta = pixelsize
  endelse

  nf = 256
  p0fp = -(nf/2)*dtheta*[1,1]
  fpa = fltarr(nf,nf)

;  image slices of atmosphere
  f = 1.   ; (quasi) focal length of primary
;          fit to Gardner's lidar measurements: return = cph*exp(h/h0)
  h0 = 4.343*km  ; scale height of atmospheric return
  cph = 10.^5 ; constant: photons/150m/sec/watt at h -> 0

  c = 3.e8
  nu = c/lambda
  hplank = 6.62617e-34

  ph = (laserPower/(hplank*nu))*ms ; photons/ms

  a0 = cph*(laserPower/100.)*(dh/150.) * ms ; total Rayleigh return photons/cm^2/ms
  fpa = fltarr(nf,nf)
  fpsave = fltarr(nf,nf,nh+1)
  k = 0

  for h = hmin, hmax, dh do begin
    print,'<propLaser> h = ',h,' km'
    hprime = 1./(1./f - 1./h)
    dx = ((hprime-f)/hprime)*du
    p0 = -(n/2)*dx*[1,1]
    p0 = p0 + fieldAngle(psubap,gsno,rno,h)
    wfh = fresnel(wf,du,h,lambda)
    a = a0*exp(-h/h0)
    tmp = screenadd(fpa*0,a*abs(wfh)^2*(dtheta/dx)^2,p0fp,dtheta,p0,dx,p0fp,dtheta,nf,nf)
    fpsave[*,*,k] = tmp
    k = k+1
    fpa = fpa+tmp
  endfor
; simulate the sodium return
;  projecting telescope
  h = 90.*km
  sigma = .075 ; 1/e^2 at 300 mm diameter
  pr = gauss2(n,n/2,n/2,sigma/du)
  wfl = complex(pr,0*pr)
  wfna = fresnel(wfl,du,h,lambda)
  gs = abs(wfna)^2
  gs = gs/total(gs)
;  dx is very close to du/h at 90 km so just convolve illum profile with psf

  hprime = 1./(1./f - 1./h) 
  dx = ((hprime-f)/hprime)*du
  p0 = -(n/2)*dx*[1,1]
  p0 = p0 + fieldAngle(psubap,gsno,rno,h)
  wfh = fresnel(wf,du,h,lambda)
  psf = abs(wfh)^2
  gss = real(ft(ft(psf)*ft(gs),/inverse)*float(n)^2)
  a = (5.14e6)*ms*cm^2 ;  photons/ms/cm^2
  tmp = screenadd(fpa*0,a*gss*(dtheta/dx)^2,p0fp,dtheta,p0,dx,p0fp,dtheta,nf,nf)
  fpsave[*,*,k] = tmp

  fpa = fpa+tmp ;   photons/cm^2/ms/pixel
  fpan = fpa / (dtheta/arcsec)^2;  normalize to photons/cm^2/ms/arcsec^2
  return,fpan
end

;+
;   fieldAngle - find the field angle of the rayleigh at height h from
;      one laser while looking at the guidestar of another
;
; inputs
;   psubap - 2-vector center position of the subaperture
;   gsno - guide star number 0-4: [center,+y,+x,-y,-x]
;   rno - Rayleigh number 0-4
;   h - Rayleigh scatter height
;
; output
;   2-vector field angle, in radians
;-
function fieldAngle,psubap,gsno,rno,h
  x = psubap[0]
  y = psubap[1]
  i = rno
  j = gsno
  ngs = 5
  hgs = 90000.
  arcsec = !pi/(180.*3600.)
  thetags = 42.5*arcsec
  d = h*tan(thetags)
  dgs = hgs*tan(thetags)
  v = dblarr(3,ngs)
  v[*,0] = [-x,-y,h]
  v[*,1] = [-x,d-y,h]
  v[*,2] = [d-x,-y,h]
  v[*,3] = [-x,-d-y,h]
  v[*,4] = [-d-x,-y,h]
  vgs = dblarr(3,ngs)
  vgs[*,0] = [-x,-y,hgs]
  vgs[*,1] = [-x,dgs-y,hgs]
  vgs[*,2] = [dgs-x,-y,hgs]
  vgs[*,3] = [-x,-dgs-y,hgs]
  vgs[*,4] = [-dgs-x,-y,hgs]
  cthetaf = ((v[*,i]/norm(v[*,i])) ## transpose(vgs[*,j]/norm(vgs[*,j])))[0]
  thetaf = acos(cthetaf)
;  deltav = v[*,i] - vgs[*,j]
  vhat = v[*,i]/norm(v[*,i])
  vgshat = vgs[*,j]/norm(vgs[*,j])
; pdeltav = deltav - vgshat*(vgshat ## transpose(deltav))[0]
; pdeltav = v[*,i] - vgshat*(vgshat ## transpose(v[*,i]))[0]
; print,pdeltav
; psi = atan(pdeltav[1],pdeltav[0])
;;  psi = atan(v[1,i],v[0,i])
;  print,[v[1,i],v[0,i],psi*180./!pi,thetaf/arcsec,thetaf*cos(psi)/arcsec,thetaf*sin(psi)/arcsec]
; print,'rayleigh: ',i,'  guide star: ',j
; print,'<fieldAngle> thetaf (arcsec) = ',thetaf/arcsec
; print,'<filedAngle> psi(degrees) = ',psi*180./!pi
;;  return,[thetaf*cos(psi),thetaf*sin(psi)]
  alpha = vgshat[2]/vhat[2]
;  th = vgshat - vhat / total(vhat*vgshat)
  th = alpha*vhat - vgshat
  return,[th[0],th[1]]
end
