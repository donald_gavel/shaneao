;
; test code
;
;clearwins
i = complex(0.,1.)
n = 8
m = 2
nn = 4096*2
r = nn/2
a_set = [2,5,10,15]
na = (size(a_set))[1]
nblocks = 4*2
nb = nn/nblocks
fap_save = fltarr(nb,nb,na)
fap_xy = complexarr(nb,nb)
;z_pasteup = fltarr(nn,nn)
;z_pasteup_save = fltarr(nn,nn,na)
for k=0,na-1 do begin
  a = a_set[k]
  for x = 0,nblocks-1 do begin
    for y = 0,nblocks-1 do begin
      print,x,y & wait,.1
      cx = -(x-nblocks/2)*nb
      cy = -(y-nblocks/2)*nb
      ap = circle(nb,nb,cx,cy,r,1.)
      z = zernike(nb,nb,r,n,m,center=[cx,cy],/noll)
      wf = ap*exp(i*2*!pi*a*z)
      ;z_pasteup[x*nb:(x+1)*nb-1,y*nb:(y+1)*nb-1] = real(wf)
      fap_xy += ft(wf)
    endfor
  endfor
  ;z_pasteup_save[*,*,k] = z_pasteup
  fap_save[*,*,k]=abs(fap_xy)^2
  ;
  ee = encirc(fap_save[*,*,k],nb/2,nb/2,1,/normalize)
  print,k & wait,.1
  if (k eq 0) then begin
    window,/free
    plot,ee[0,*],ee[1,*],charsize=1.5
    empty
  endif else begin
    oplot,ee[0,*],ee[1,*]
    empty
  endelse
endfor
disp,fap_save
end
