;
; cmCreate.pro
;   create a theoretical push matrix for Villages
;
; Define Actuator Locations
actLocX = findgen(12) # ones(12) - 5.5
actLocY = transpose(actLocX)
actLocX = -actLocX
actLocX = reform(actLocX,144)
actLocY = reform(actLocY,144)
actLocX = [actLocX[1:10],actLocX[12:131],actLocX[133:142]]
actLocY = [actLocY[1:10],actLocY[12:131],actLocY[133:142]]
na = 140
; Define Hartmann Subaperture Locations
sLocX = findgen(9) # ones(9) - 4
sLocY = transpose(sLocX)
sLocX = reform(sLocX,81)
sLocY = reform(sLocY,81)
sLocX = [sLocX[2:6],sLocX[10:16],sLocX[18:26],sLocX[27:29],sLocX[33:35],sLocX[36:38], $
         sLocX[42:44],sLocX[45:47],sLocX[51:53],sLocX[54:62],sLocX[64:70],sLocX[74:78]]
sLocY = [sLocY[2:6],sLocY[10:16],sLocY[18:26],sLocY[27:29],sLocY[33:35],sLocY[36:38], $
         sLocY[42:44],sLocY[45:47],sLocY[51:53],sLocY[54:62],sLocY[64:70],sLocY[74:78]]
ns = 60
; Build the "Push" matrix based on influence distance
sm0 = fltarr(na,2*ns)
infl = 0.1  ; influence function coefficient for sensors further away from the actuator
for a=0,na-1 do begin
    for s=0,ns-1 do begin
        r = (sLocX[s] - actLocX[a])^2 + (sLocY[s] - actlocY[a])^2
        if (r lt 1.) then begin
            if (sLocX[s] gt actLocX[a]) then sm0[a,s] = 1 else sm0[a,s] = -1
            if (sLocY[s] gt actLocY[a]) then sm0[a,ns+s] = 1 else sm0[a,ns+s] = -1
        endif
        if (r ge 1. and r lt 4.) then begin
            if (sLocX[s] gt actLocX[a]) then sm0[a,s] = infl else sm0[a,s] = -infl
            if (sLocY[s] gt actLocY[a]) then sm0[a,ns+s] = infl else sm0[a,ns+s] = -infl
        endif
    endfor
endfor

; Make a display of the pupil showing the number of sensors and actuators
z = 40
map = fltarr(z*13,z*13)
map[0,0] = 1
f = .6
for x = 1,12 do begin
    for y = 1,12 do begin
       map[z*x,*] = f
       map[*,z*y] = f
    endfor
endfor
map = map + circle(z*13,z*13,z*13./2,z*13./2,z*4.5,f/2.)
map = map - circle(z*13,z*13,z*13./2,z*13./2,z*1.5,f/2.)
window,0,xsize=z*13,ysize=z*13,title='Villages sensor-actuator numbering'
tvscl,1-map
a0 = -5.5
for a=0,na-1 do xyouts,z*(actLocX[a]-a0)+z,z*12-(z*(actLocY[a]-a0)+12)+7,strtrim(a,2),alignment=0.5,/device,charthick=2,color=0

s0 = -4
for s=0,ns-1 do xyouts,z*(sLocX[s]-s0)+z*2.5,z*13-(z*(sLocY[s]-s0)+z*2.5),strtrim(s,2),alignment=0.5,/device,color=0

; display the computed system matrix
disp,sm0,'computed system matrix'

; calculate the corresponding control matrix
forward_function weightingv
forward_function wlscmvr
forward_function waffleSet
dummy = wlscmv()
frac = 0.0
sig = 100
weights = weightingv('illum',frac,sig)
thresh = 0.17
badModes = waffleSet(n=140)
cm0 = wlscmvr(sm0,weights,badModes,0.,thresh=thresh,/verbose)

; display the computed control matrix
disp,cm0,'computed control matrix'

; analyze the matrix singular mode space
svdc, transpose(cm0), w, u, v
zer = reform(fltarr(140),140,1)
vm = [[zer],[v[*,0:9]],[zer],[v[*,10:129]],[zer],[v[*,130:139]],[zer]]
zer = reform(fltarr(144),1,144)
vm2 = [zer,vm[0:9,*],zer,vm[10:129,*],zer,vm[130:139,*],zer]
w2 = [0,w[0:9],0,w[10:129],0,w[130:139],0]
vm3 = transpose(reform(vm2,144,12,12),[1,2,0])
ind = reverse(sort(w2))
vm4 = vm3[*,*,ind]

; display the singular modes and print out the relative singular values
disp, vm4
evals = transpose([[findgen(144)],[w2[ind]/max(w2)]])

;  write the new control matrix to a view file
;wrview,'cm_000',-cm0*1000,/swap
;wrview,'sm_000',sm0
end

