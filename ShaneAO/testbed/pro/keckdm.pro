;+
;  keckdm -  - routines for creating and manipulating the
;       keck deformable mirror
;
;  USAGE
;    al = acts_inap(ap,ds,du) ; find actuators
;    im = keckdm_display(n,d,du,sl) ; display actuators
;    f = actuator_influence(n,p,du,ds)
;    h = calibrate_dm(ap,f,al,sl,ds,du)
;    ph = pushdm(dm,a,f,al,du)
;
;  INPUTS:
;    ap - aperture (11.8 m diameter circle)
;    n - image dimensions
;    ds - inter-actuator spacing, meters
;    du - pixel size, meters
;    al - 2xNA array of x,y locations of actuators, meters
;
;  OUTPUT:
;    im - display image of Keck aperture with the actuator array overlayed
;
;-
function acts_inap,dm,ds,du
  n = (size(dm))(1)
  na = round((n/2)*du/ds)
;   x and y are the positions of the actuators
  x = findgen(na)*ds
  x = [-reverse(x),x(1:na-1)]
  y = x
;   is dm(x,y) eq 1 ?
  ix = round(x/du) + n/2
  ix = ix # (ix*0+1)
  iy = round(y/du) + n/2
  iy = (iy*0+1) # iy
  ind = iy*n + ix
  ti = where(dm[ind] eq 1)
  x = (ix[ti] - n/2)*du
  y = (iy[ti] - n/2)*du
  r = transpose([[x],[y]])
  return, r
end
;
function keckdm_display,n,d,du,al,footprint
  na = (size(al))(2)
  if (n_elements(footprint) eq 0) then footprint = 2
  act = circle(n,n,n/2,n/2,footprint,1.)
  r = fltarr(n,n)
  for i=0,na-1 do begin
    x = al[0,i]/du
    y = al[1,i]/du
    r = r + shift(act,x,y)
  endfor
  return,r
end
;
;
;  actuator_influence - generate a Gaussian X Sin(x)/x influence function
;     with a given inter-actuator influence percentage
;
;  USAGE:
;    f = actuator_influence(n,p,du,ds)
;
;  INPUTS:
;    n - size of screen
;    p - inter-actuator influence percentage (must be p < 0.2)
;         (20% is the influence of a sync function)
;    du - pixel size, meters
;    ds - interactuator spacing, meters
;
;  OUTPUT:
;    f - influence function
;
function actuator_influence,n,p,du,ds
  sigma = (1.5*ds/du) / sqrt(-alog(p/.2))
  g = gauss2(n,n/2,n/2,sigma)
  fuzz = 1.e-9
  u = (findgen(n) # (fltarr(n)+1) - n/2)*(du/ds) + fuzz
  v = transpose(u)
  f = sin(!pi*u)/(!pi*u)*sin(!pi*v)/(!pi*v)
  return, f*g
end
;
;  calibrate_dm
;
;  USAGE:
;    h = calibrate_dm(ap,f,al,sl,ds,du)
;
;  INPUTS:
;    ap - aperture
;    f - influence function
;    al - actuator locations, 2 X na, meters with respect to center
;    sl - subaperture locations, 2 X ns, meters with respect to center
;    ds - lenslet size, meters
;    du - pixel size, meters
;
;  OUTPUT:
;    h - influence matrix
;
;  REQUIRES:
;    hartmannsimu.pro
;
function calibrate_dm,ap,f,al,sl,ds,du
  nn = (size(ap))(1)
  n = (size(f))(1)
  na = (size(al))(2)
  ns2 = (size(sl))(2)*2
  h = fltarr(ns2,na)
  slp = round(sl/du)
  dsp = round(ds/du)
  nnmax = max([max(al/du)-min(al/du) + n,nn])
  ph = fltarr(nnmax,nnmax)
  for i=0,na-1 do begin
    ph = ph*0
    ix = round(al(0,i)/du) + nnmax/2
    iy = round(al(1,i)/du) + nnmax/2
    ph[ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1] = f
    s = hartmannAveGrad(ap, $
          ph[nnmax/2-nn/2:nnmax/2+nn/2-1,nnmax/2-nn/2:nnmax/2+nn/2-1], $
          slp,dsp)
    h(*,i) = reform(transpose(s),ns2)
    print,format='($,I0," ")',i
  endfor
  print
  return,h
end
;
; pushdm - move the dm in accordance with a vector of actuator values
;
;  USAGE:
;    ph = pushdm(dm,a,f,al,du)
;
;  INPUTS:
;    dm - dm aperture
;    a - vector of actuator values
;    f - actuator influence function (see function actuator_influence)
;    al - actuator locations, 2 X na, in meters
;    du - pixel size, in meters
;
;  OUTPUT:
;    ph - phase sheet response of the dm
;
function pushdm,dm,a,f,al,du
  na = (size(a))(1)
  nn = (size(dm))(1)
  n = (size(f))(1)
  nnmax = max([max(al/du)-min(al/du) + n,nn])
  ph = fltarr(nnmax,nnmax)
  for i=0,na-1 do begin
    ix = round(al(0,i)/du) + nnmax/2
    iy = round(al(1,i)/du) + nnmax/2
    ph[ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1] = $
      ph[ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1] + a(i)*f
  endfor
  return, ph[nnmax/2 - nn/2:nnmax/2 + nn/2-1,nnmax/2 - nn/2:nnmax/2 + nn/2-1]
end
;
;   controlmatrix - calculate the pseudo-inverse of the
;        influence matrix and condition it for AO control
;
;  USAGE:
;    m = controlmatrix(h,thresh,/detiltacts,/detiltsens)
;
;  INPUTS:
;    h - influence function matrix, 2*ns X na
;        (where ns = number of subaps, na = number of actuators)
;    thresh - condition number threshold for rejecting
;        nearly unobservable singular modes
;    detiltacts - flag to condition the matrix so that it
;        removes tilts in the output (actuator) space
;    detiltsens - flag to condition the matrix so that it
;        removes tilts in the input (sensor) space
;
;  OUTPUTS:
;    cm - control matrix, 2*ns X na (can be used to directly
;        multiply the slope vector: a = m ## reform(transpose(s),2*ns) )
;
function controlmatrix, h,thresh,w,u,v
  if n_elements(w) eq 0 then begin
    print,'computing singular value decomposition'
    svdc, transpose(h), w,u,v
  endif
  print,'composing control matrix'
  cm = v ## diagonal((1/w)*(w/max(w) gt thresh)) ## transpose(u)
;  identity = cm ## transpose(h)
  return,cm
end
;
pro keckdm
  doc_library,'keckdm'
end
