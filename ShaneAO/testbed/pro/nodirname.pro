;
;  nodirname.pro - remove directory name from a filename
;
function nodirname,filename
  n = rstrpos(filename,'/')
  m = strlen(filename)
  if n eq (-1) then return,filename
  s = strmid(filename,n+1,m)
  return,s

end