;
; Hartmann_GPICAL.pro
;
;  Hartmann analysis of the spot patterns from the GPI CAL LOWFS Hartman sensor
;
dir = '/Users/SONY USER/Documents/AdaptiveOptics/LAO/GPI/GPI_CAL/Data/Data_110901/'
file = 'exp_lowfs.fits'
colStart = 0
rowStart = 0
nSpotsHoriz = 7
nSpotsVert = 7
pitch = 5

cd,dir
im = float(readfits(file))
if (size(im,/n_dimensions) gt 2) then im = im[*,*,0]
nSpots = nSpotsHoriz*nSpotsVert

; calibrator
;im = rot(im,2.,/interp) ; about 2x off (reports 1 degree per 2 degree rotation)
;im = fshift(im,0,0.2)    ; about 2x off (reports 0.5 pixel per 1 pixel shift)

; assign pupil locations to the Hartmann subapertures
;
locs = fltarr(nSpots,2)
k = 0
for y = 0,nSpotsVert-1 do begin
  for x = 0,nSpotsHoriz-1 do begin
    locs[k,0] = pitch*x
    locs[k,1] = pitch*y
    k++
  endfor
endfor
locs[*,0] -= average(locs[*,0])
locs[*,1] -= average(locs[*,1])
;
; compute x and y centroids
;    1) using the quad-cell formula
;    2) using the center of mass formula
;
cents = fltarr(nSpots,2)
k = 0
for y = 0,nSpotsVert-1 do begin
  for x = 0,nSpotsHoriz-1 do begin
    x0 = colStart+pitch*x
    y0 = rowStart+pitch*y
    a = im[x0+1,y0+2]
    b = im[x0+2,y0+2]
    c = im[x0+1,y0+1]
    d = im[x0+2,y0+1]
    inten = a+b+c+d
    cents[k,0] = ((b+d)-(a+c))/inten
    cents[k,1] = ((a+b)-(c+d))/inten
    k++
  endfor
endfor
;
;  analyze the results:
;    1) find the average tilt
;    2) find the average rotation
;    3) find the focus term
;    4) find the astigmatism term
;
r2 = locs[*,0]^2+locs[*,1]^2
reg = fltarr(nSpots)
reg[where(r2 eq 0)] = 1.
tilt = [average(cents[*,0]),average(cents[*,1])]
rota = average(asin((locs[*,0]*cents[*,1] - locs[*,1]*cents[*,0])/(r2+reg)))*180/!pi
focus = total(locs[*,0]*cents[*,0] + locs[*,1]*cents[*,1])/total(r2)
astig = total(locs[*,0]*cents[*,0] - locs[*,1]*cents[*,1])/total(r2)
astig45 = total((locs[*,0]+locs[*,1])*cents[*,0] - (locs[*,0]-locs[*,1])*cents[*,1])/total(r2)/sqrt(2.)
;
; print report:
print,'======================================='
print,file
print,systime()
print,'---------------------------------------'
print,'tilt = [',tilt[0],', ',tilt[1],'] pixels'
print,'rotation = ',rota,' degrees'
print,'focus = ',focus,' pixels/pixel'
print,'astigmatism 0 = ',astig,' pixels/pixel'
print,'astigmatism 45 = ',astig45,' pixels/pixel'
print,'======================================='

end

    
    
    