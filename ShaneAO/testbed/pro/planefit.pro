;+
;  planefit - fit an area of an image to a plane
;
;  USAGE:
;    r = planefit(image,mask,piston=p,tip=t,tilt=l)
;
;  INPUTS:
;    image
;    mask - (optional) set to one in areas to be fit
;
;  OUTPUT:
;    an image the size of the input image with the plane surface
;    optional: keyword parameters piston, tip, tilt set to their
;              computed values
;-
function planefit,image,mask,piston = piston,tip = tip,tilt = tilt
  n = (size(image))(1)
  m = (size(image))(2)
  if (n_elements(mask) eq 0) then mask = fltarr(n,m) + 1
  tmask = total(mask,/double)
  x = real(extend(indgen(n),2,m)) - (n-1)/2.
  x = x - total(x*mask,/double)/tmask
  y = real(extend(indgen(m),1,n)) - (m-1)/2.
  y = y - total(y*mask,/double)/tmask
  piston = total(mask*image,/double)/tmask
  im = image - piston
  tip = total(mask*x*im,/double) / total(mask*x*x,/double)
  im = im - tip*x
  tilt = total(mask*y*im,/double) / total(mask*y*y,/double)
  return, tip*x + tilt*y + piston
end
