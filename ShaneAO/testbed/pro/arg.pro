; arg.pro - return the angle of a complex number
;
function arg,a
  return, atan(imaginary(a),real_part(a))
end
