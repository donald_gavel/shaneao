; hs.pro - analyze the hartman sensor data
;
;
function even, x
  if (x/2)*2 eq x then return,1 else return, 0
end

function find_roi, image, loc1, siz, del, thresh, rowcount, colcount
;
  xmax = (size(image))(1)
  ymax = (size(image))(2)
  if n_params() eq 0 then begin
    print,'usage: find_roi, image, loc1, siz, del, thresh'
    return,0
  endif
  n = siz(0)
  m = siz(1)
  x = loc1(0) - n/2
  y = loc1(1) - m/2
;  list of hartmann spot locations
;  list = make_array(13*15,5)
  list = make_array(colcount*rowcount,5)
  k = 0
 for j = 0,rowcount-1 do begin
  y = loc1(1) - m/2 + j*del(1)
  for i = 0,colcount-1 do begin
    x = loc1(0) - n/2 + i*del(0)
    if not even(j) then x = x + del(0)/2
    roi = image((x>0):(x+n)<(xmax-1),(y>0):(y+m)<(ymax-1)) ;  extract region of interest
;   determine if it has enough light to qualify as a Hartmann spot
    z = total(roi)
;    print,i,j,x+n/2,y+m/2,z
    if z gt thresh then begin
      list(k,0:1) = centroid(roi) + [x,y]
      list(k,2:3) = [x+n/2,y+m/2]
      list(k,4) = z
;      print,k,j,i,list(k,0),list(k,1),z
      k = k+1
    endif
  endfor
 endfor
 listout = fltarr(k,5)
 for i=0,k-1 do listout(i,*) = list(i,*)
return,listout
end

;  find the Hartmann centroids, given reference centers.
;    uses reference centers found by find_roi
function hartmann_centroids, image, list, roi_size, thresh
  xmax = (size(image))(1)
  ymax = (size(image))(2)
  n = (size(list))(1)
  r = fltarr(n,5)
  k = 0
  for i = 0, n-1 do begin
    x = list(i,0) - roi_size(0)/2
    y = list(i,1) - roi_size(1)/2
    roi = image((x>0):(x+roi_size(0))<(xmax-1),(y>0):(y+roi_size(1))<(ymax-1))
;    print,i,total(roi),thresh,total(roi)>thresh
    if total(roi) gt thresh then begin
      r(k,0:1) = centroid(roi) + [x,y]
      r(k,2:3) = list(i,0:1)
      r(k,4) = total(roi)
;      print,k,r(k,*)
      k = k+1
    endif
  endfor
  rout = fltarr(k,5)
  for i=0,k-1 do rout(i,*) = r(i,*)
  return,rout
end

pro draw_roi, list, roi_size
  nr = (size(list))(1)
  n = roi_size(0)
  m = roi_size(1)
  for i=0,nr-1 do begin
   x = list(i,0) - n/2
   y = list(i,1) - m/2
   p = [[x,y], [x,y+m], [x+n,y+m], [x+n,y], [x,y]]
   oplot,p(0,*),p(1,*)
  endfor
end

;  create a regular of the hexagonal data mesh
pro regular_grid, list, r, gs, limits
  triangulate,list(*,0),list(*,1),triangles
  r = trigrid(list(*,0),list(*,1),list(*,2),triangles,gs,limits)
;  surface, r
  return
end

;  display of hartmann centroid positions
pro hartmann_display, centroids, ref_cents
  plot,ref_cents(*,0),ref_cents(*,1),psym=6
  oplot,centroids(*,0),centroids(*,1),psym=1
end

; create a phase point list
function phase_points, ref_cents, del
  n = (size(ref_cents))(1)
  phase = fltarr(n,3)
  phase(*,0) = ref_cents(*,0) + del(0)
  phase(*,1) = ref_cents(*,1) + del(1)
  phase(*,2) = 0
  return, phase
end

; find the nearest 3 Hartmann points to each phase points
function near3, phase, ref_cents, radius
  n = (size(phase))(1)
  list = intarr(n,3)
  list = list - 1
  for i = 0, n-1 do begin
    k = 0
    for j = 0, n-1 do begin
      r2 = (phase(i,0)-ref_cents(j,0))^2 + $
           (phase(i,1)-ref_cents(j,1))^2
      if r2 lt radius^2 then begin
        list(i,k) = j
        k = k+1
      endif
    endfor
;    print, i, list(i,0), list(i,1), list(i,2)
  endfor
  return, list
end

; form the interaction matrix
function make_interaction_matrix, phase, ref_cents, near3_list
  n = (size(phase))(1)
  mat = make_array(n,2*n)
  for i = 0, n - 1 do begin ; i is the phase point index
    for j = 0,2  do begin
      k = near3_list(i,j)  ; k is the index of a nearby Hartmann point
      if k ge 0 then begin
        x = ref_cents(k,0) - phase(i,0)
        y = ref_cents(k,1) - phase(i,1)
        r = sqrt(x^2 + y^2)
        mat(k,i) = x/r
        mat(k,i+n) = y/r
      endif
    endfor
  endfor
  return, mat
end

; invert the interaction matrix
function psinv, mat, w, u, v, thresh
  print,'calling svdc...'
  svdc, mat, w, u, v
  print,'done'
  n = (size(w))(1)
  svi = fltarr(n,n)
  for k=0,n-1 do begin
    if (w(k) ge thresh) then svi(k,k) = 1/w(k)
  endfor
  print,'calculating inverse...'
  minv = v ## svi ## transpose(u)
  print,'done'
  return, minv
end

; convert the 