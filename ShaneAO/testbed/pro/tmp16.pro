function compCost,eww,pupil,phiw,f,p1,phihat,pw
  phihat = real(ft(ft(eww)*f,/inverse))
  pw = (phiw-phihat)*pupil
  cost = sqrt(total(pw^2))/p1
  return,cost
end

function extendPhase,phiw,f,pupil
n = (size(phiw))[1]
;  filter the windowed screen
phiw_f = ft(phiw)
phihat_f = 0*phiw_f
phihat = fltarr(n,n)
eww = fltarr(n,n)
;
gain = .075
adaptGain = 1
maxiter = 100; 60
costStop = .001
p1 = sqrt(total(phiw^2*pupil))
pw = phiw
cost = compCost(eww,pupil,phiw,f,p1,phihat,pw); sqrt(total(pw^2))/p1
print,'initial cost: ',cost
eww_save = eww
gaintrimdown = .8
gaintrimup = 1.08
iter = 0
while (cost gt costStop) do begin
  iter = iter+1
  if (iter gt maxiter) then goto, exit
;  compute direction
  ew_f = ft(pw) / (f>1e-16)
  ew_f[n/2,n/2] = 0
  ew = real(ft(ew_f,/inverse))
;  take a step
  eww_save = eww
  eww = eww + gain*ew*pupil
  oldcost = cost
  cost = compCost(eww,pupil,phiw,f,p1,phihat,pw)
  print,iter,cost & wait,.001
;  pw_save = pw
;  pw = (phi-phihat)*pupil
;  oldcost = cost
;  cost = compCost(eww,pupil,phiw,p1); sqrt(total(pw^2))/p1
;  print,j,cost & wait,.001
  whilecount = 0
  maxwhilecount = 10
  if (adaptGain) then begin
  while (cost gt oldcost) do begin
    print,iter,cost,oldcost,'stepping down to gain ',gain*gaintrimdown
    gain = gain*gaintrimdown
    eww = eww_save + gain*ew*pupil
    cost = compCost(eww,pupil,phiw,f,p1,phihat,pw)
    whilecount = whilecount+1
    if (whilecount gt maxwhilecount) then goto, exit
  endwhile
  gain = gain*gaintrimup
  print,iter,'drifting gain up to ',gain
  endif
;  phihat = real(ft(ft(eww)*f,/inverse))
endwhile
;
exit:
return,phihat
end

;  true screen
f = screengen(n,n,r0,du)*(lambda0/(2*!pi))
phi = screengen(f,5)
;  windowed screen
phiw = pupil*phi
phihat = extendPhase(phiw,f,pupil)
end
