;
;  clearstate.pro
;    clear the "state" structure, used in specklegui, of all handles except
;       the dataset
;
pro clearstate,state
  n = n_tags(state)
  names = tag_names(state)
  for i=1,n-1 do begin
    if (size(state.(i)))(0) eq 0 then begin
      if state.(i) ne 0 then begin
        if handle_info(state.(i),/valid_id) then handle_free,state.(i)
      endif
      state.(i) = 0
    endif
  endfor
end
