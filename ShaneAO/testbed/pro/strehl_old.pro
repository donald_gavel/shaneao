;+
;   strehl.pro - do a "best-fit" calculation of Strehl ratio
;
;    S = strehl(image,wavelength,camera,pixel=pixelscale,d=d,ds=ds,secondary=secondary)
;
;   inputs:
;     image: square image, background-subtracted
;     wavelength, in microns
;
;   optional inputs:
;     camera: 'ircal' - ircal IR camera
;             'pm' - photometrics camera in scoring mode (Bravais lens out)
;             'pmb' - photometrics camera in wide field mode (Bravais lens in)
;     pixelscale: pixel scale in arcseconds.  Required if camera not specified.
;     d: primary mirror diameter in meters. Default 3 meters
;     ds: secondary obscuration in meters. Default 0.8 meters
;     secondary: set this keyword if there is a secondary obscuration
;             (not necessary for 'ircal', which always has a secondary obs)
;-
function strehl,data,lam,cam,pixel=pixelscalearcsec,dp=dp,ds=ds,secondary=secondary,model=psf0
  if keyword_set(secondary) then secondary = 1 else secondary = 0
  if n_elements(ds) ne 0 then secondary = 1
  arcsec = !pi/(180.*3600.)
  microns = 1.e-6
  i = complex(0,1.)
  lambda = lam*microns
  n = (size(data))[1]
  m = (size(data))[2]
  if (n ne m) then begin
  	print,'<strehl> ERROR input must be a square image'
  	return,0
  endif
;
  if (n_elements(pixelscalearcsec) eq 0) then begin
    if cam eq 'ircal' then begin
      pixelscale = 0.076*arcsec
      secondary = 1
    endif
    if cam eq 'pm' then pixelscale = 0.016*arcsec
    if cam eq 'pmb' then pixelscale = 0.28*arcsec
    if cam eq 'villages' then begin
      pixelscale = 0.033*arcsec
      secondary = 1
    endif
  endif else begin
    pixelscale = pixelscalearcsec*arcsec
  endelse
  dx = lambda/(n*pixelscale)
  if (n_elements(cam) ne 0) then begin
	  if (cam eq 'villages') then begin
		  if (n_elements(dp) eq 0) then dp = 1.    ;  Nickel telescope
		  if (n_elements(ds) eq 0) then ds = 0.35
	  endif
  endif
  if (n_elements(dp) eq 0) then dp = 3.    ;  Shane telescope
  if (n_elements(ds) eq 0) then ds = 0.8
  pupil = circle(n,n,n/2,n/2,(dp/2.)/dx,1.)
  if secondary eq 1 then begin
    pupil = pupil - circle(n,n,n/2,n/2,(ds/2.)/dx,1.)
    pupil[n/2,*] = 0
    pupil[*,n/2] = 0
  endif
;
;      shift the peak of the data to the center pixel
  m = max(data)
  c = coords(data,!c)
  datas = shift(data,n/2-c[0],n/2-c[1])
;
;      now, find the sub-pixel centroid of the central core
  x = (findgen(n)-n/2)*dx # (fltarr(n)+1)
  y = (fltarr(n)+1) # (findgen(n)-n/2)*dx
  aperture = circle(n,n,n/2,n/2,1.22*(lambda/dp)/pixelscale,1.) ; the central core
  c = (centroid(datas*aperture) - [n/2,n/2])*pixelscale
;
;      and center the model psf there
  ph = (c[0]*x + c[1]*y)*(2*!pi/lambda)*pupil
  wf = pupil*exp(i*ph)
  psf0 = abs(ft(wf))^2
  psf0 = psf0/total(psf0)
  psf = datas/total(datas)
;
;      strehl is the ratio of energy in brightest pixel to that of model
  strehl = max(psf)/max(psf0)
  return,strehl
end

