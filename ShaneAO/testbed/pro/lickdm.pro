;+
;  lickdm.pro - support routines for modeling
;  ==========
;   the lick deformable mirror
;;;;;;
;  dmgeo - make an array of dm actuator locations
;   (wafefront sensor map orientation, i.e. a0 is lower left, a60 upper right)
;  -----
;    dmgeo = dmgeom()
;;;;;;
;  makebasis - creates the basis functions for mirror fitting
;  ---------
;    a = makebasis(dmgeo,dx,ap=ap,basis=basis,sigma=sigma)
;   input:
;     dmgeo - dm geometry, a (61,2) array of actuator locations, in meters
;     dx - desired pixel size (suggest dx=lambda/(128*platescale))
;     ap (optional) - aperture, on (128,128) grid
;     sigma (optional) - gaussian-width of the influence function, in meters
;   outputs:
;     a - an (nact,nact) basis cross-correlation matrix (the matrix to
;         invert for a least squares fit)
;     basis - an (n,n,nact) array of the actuator influence functions
;         (n=128 pixels, nact = 61 actuators)
;;;;;;;
;  dmfit - fit a wavefront to dm actuator pushes
;  -----
;    actvec = dmfit(wf,basis,ainv,/invert,ap=ap)
;   inputs:
;     wf - wavefront phase data (64,64)
;     basis - from makebasis
;     ainv - "inverse" of a
;          here is sample inversion code:
;              eval = eigenql(a,eigenvectors=ev)
;              evali = (1/(eval>thresh))*(eval gt thresh)
;              ainv = transpose(ev)##diagonal(evali)##ev
;     invert - whether or not to invert "ainv" (=a); (no threshold)
;     ap - optional aperture over which to best-fit the wavefront
;   output:
;     actvec - a vector of 61 actuator commands, in the same units as wf
;-
function makebasis,dmgeo,dx,ap=ap,basis=basis,sigma=sigmaarg,influence=influence
  n = 128
  if n_elements(ap) eq 0 then begin
    dtele = 3./dx
    dsec = 0.8/dx
    ap = circle(n,n,n/2,n/2,dtele/2,1.) - circle(n,n,n/2,n/2,dsec/2,1.)
  endif
  numact = 61
;  dtele = 64
;  dx = 3./dtele
  dsubap = 0.43/dx
  if n_elements(sigmaarg) eq 0 then begin
    print,'computing sigma internally'
    dsubap = 10.5925  ;  43 cm / dx
    sigma = dsubap*0.6/1.414 ; magic number for actuator influence
  endif else begin
    sigma = sigmaarg / dx
  endelse
  if (n_elements(influence) eq 0) then begin
    influence = 'gauss'
  endif
  
  if (influence eq 'circle') then begin
    g = circle(n,n,n/2,n/2,dsubap/2.-1,1.)
  endif
  
  if (influence eq 'gauss') then begin
    print,'sigma = ',sigma
    g = gauss2(n,n/2,n/2,sigma) ; normalized to 1 at the peak
  endif
  
  basis = fltarr(n,n,61)
  for i=0,numact-1 do begin
    x = dmgeo[i,0]/dx
    y = dmgeo[i,1]/dx
    basis[*,*,i] = shift(g,x,y)*ap
  endfor
  a = fltarr(numact,numact)
  for i=0,numact-1 do begin
    print,format='($,I0," ")',i
    for j=0,numact-1 do begin
      a[i,j] = total(basis[*,*,i]*basis[*,*,j])
    endfor
  endfor
  print,'done'
  return, a
end

function dmgeom
  dtele = 3.
  da = dtele/6.
  dy = sqrt(3.)*da/2.
  na = 61
  dmgeo = fltarr(na,2)
  rowsize = [5,6,7,8,9,8,7,6,5]
  ny = 9
  y = (indgen(ny)-(ny-1)/2)*dy
  k = 0
  for i=0,ny-1 do begin
    nx = rowsize[i]
    x = (indgen(nx)-(nx-1.)/2.)*da
    for j=0,nx-1 do begin
      dmgeo[k,0] = x[j]
      dmgeo[k,1] = y[i]
      k = k+1
    endfor
  endfor
  return,dmgeo
end

function dmfit,wf,basis,aarg,ap=ap,invert=invert
  if n_elements(ap) eq 0 then begin
    ap = basis[*,*,0]*0 + 1
  endif
  if keyword_set(invert) then begin
    eval = eigenql(aarg,eigenvectors=ev)
    ainv = transpose(ev)##diagonal(1/eval)##ev
  endif else begin
    ainv = aarg
  endelse
  na = 61
  b = fltarr(na)
  for i=0,na-1 do begin
    b[i] = total(wf*basis[*,*,i]*ap)
  endfor
  act = ainv##b
  return,act
end

function pushact,act,basis
  n = 128
  na = 61
  dm = fltarr(n,n)
  for i=0,na-1 do begin
    dm = dm + act[i]*basis[*,*,i]
  endfor
  return,dm
end

pro showdm,circles=circles
  common showdmcom,basis,A
  if keyword_set(circles) then influence = 'circle'
  n = 128
;  if (n_elements(basis) eq 0) then begin
    print,'<showdm> making basis functions (done on first call only)'
    dmgeo = dmgeom()
    dx = 6./float(n)
    a = makebasis(dmgeo,dx,basis=basis,influence=influence,sigma = .5)
;  endif
;  dir = '/net/lgs6/home/winkerbean/AO/Data/RunData/'
;  a = rdview(dir+file)
  act = total(a,2)/1024.
  ph = pushact(act,basis)
;  ph[0:19,*] = ones(20) # (findgen(n)-n/2+.5)*(10./float(n-1))
  disp,ph
end

function lickdm
  return,0
end

  common showdmcom,basis,A
  showdm,/circles

  n = 128
  dmgeo = dmgeom()
  dx = 6./float(n)
  a = makebasis(dmgeo,dx,basis=basis,influence='gauss',sigma = .45)
  act = total(a,2)/1024.
  ph = pushact(act,basis)
  disp,ph

end