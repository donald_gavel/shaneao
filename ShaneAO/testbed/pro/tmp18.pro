n = 256
mm = .001
microns = .000001
i = complex(0.,1.)

screensize = 10.*mm
D = 800*microns ; lenslet pitch
dx = screensize/float(n)
f_lenslet = 7.5*mm
f_number_lenslet = f_lenslet/D
lambda = 0.6*microns
lenslet_offset = 0.5*D

a = !pi/(f_lenslet*lambda)
x = (ones(n) ## findgen(n)-n/2)*dx
y = transpose(x)
r = sqrt(x^2+y^2)
lensletphase = a*r^2

f_number_beam = 36.
airy_size = f_number_beam*lambda
pupil_diameter = 0.5*screensize
pupil = circle(n,n,n/2,n/2,pupil_diameter/(2*dx),1.)
airy = ft(pupil)

lensletmask = rectangle(n,n,n/2,n/2,D/dx,D/dx,1.)
lensletphase_s = shift(lensletmask*lensletphase,D/(dx*2),D/(dx*2))
wf = exp(i*lensletphase_s)*airy

L = f_lenslet

rayleighRange = D^2/lambda
diffraction = L*lambda/D
screensize = dx*n
print,'rayleighRange: ',rayleighRange,' meters'
print,'diffraction: ',diffraction/mm,' mm'
print,'screensize: ',screensize/mm,' mm'

wfL = fresnel(wf,dx,L,lambda)
end
