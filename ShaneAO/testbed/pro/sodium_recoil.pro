; sodium emission recoil
;
h = 6.62606957d-34 ; planks constant J-s
c = 3.d8 ; speed of light, m/s
h_bar = h/(2*!pi)
lambda = 588.9d-9 ; meters
k = 2*!pi/lambda
p = k*h_bar ; momentum of photon
u = 1.660538921d-27 ; atomic mass unit, kg
m = 22.98976928*u ; mass of sodium atom, kg
v = p/m
print,'v = ',v,'m/s'
f0 = c/lambda
f = (1/(1+v/c))*f0
df = f-f0
print,'df = ',df/1.d3,' khz'
tau = 16.d-9 ; decay time of sodium
delta_nu = 1/tau
print,'delta_nu',delta_nu/1.d3,' kHz'
end
