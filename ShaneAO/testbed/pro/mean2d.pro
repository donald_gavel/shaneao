;+
;   mean - calculate the mean of a dataset
;
;  m = mean(a)
;    if a is 2-dimensional, the mean is calculated along the 2nd dimension
;-
function mean,a
  ndim = (size(a))(0)
  if (ndim gt 2) then begin
    print,'<mean> cannot send array larger than 2 dimensions'
    return,0
  end
  n = (size(a))(ndim)
  m = total(a,ndim)/float(n)
  return,m
end
