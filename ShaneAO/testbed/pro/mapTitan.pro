;+
;  mapTitan.pro - map titan onto a lat-long grid
;
;-
cx = 127.5
cy = 131
pixelsize = 0.02 / 4.; arcsec
d = 180./300.
cfact = .95
sizefac = 1.15
k = .6;  Minnaert parameter
;
; September data
;
print,'reading September data'
angdiam = 0.83/sizefac
rx = (angdiam/2.) / pixelsize
ry = rx
subEarthLat = -5.20
subEarthLong = 125.02
subSunLat = -4.43
subSunLong = 127.12
im458 = readfits('titan458/titan458_image.fits')
im473 = readfits('titan473/titan473_image.fits')
im476 = readfits('titan476/titan476_image.fits')
im483 = readfits('titan483/titan483_image.fits')
im524 = readfits('titan524/titan524_image.fits')
im458z = zoomi(im458,4)
im473z = zoomi(im473,4)
im476z = zoomi(im476,4)
im483z = zoomi(im483,4)
im524z = zoomi(im524,4)
npang = 3.33; angle of north pole of titan
imSeptz = (rot(reverse(im458z), 7.921035468645 + npang) + $
          rot(reverse(im473z), -2.573066550406 + npang) + $
          rot(reverse(im476z), -6.188985854542 + npang) + $
          rot(reverse(im483z), -10.2883 + npang) + $
          rot(reverse(im524z), -38.8096 + npang))/5.
print,'mapping September data'
mask = circle(256,256,cx,cy,cfact*rx,1)
mapSept0 = planetMap(imSeptz*mask,cx,cy,rx,ry,subEarthLat,0.,d,k,subSunLat,subSunLong-subEarthLong,6.,index=isep)
mapSept = fltarr(600,300)
mapSept(0:299,*) = mapSept0
mapSept = shift(mapSept,150 - subEarthLong/d)
;
;  June data
;
print,'reading June data'
angdiam = 0.748/sizefac
rx = (angdiam/2.) / pixelsize
ry = rx
subEarthLat = -6.13
subEarthLong = 323.59
subSunLat = -3.37
subSunLong = 329.08
im709 = readfits('../../jun96/titan/titan709/titan709_image.fits')
im709z = zoomi(im709,4)
parang = 55.36814669872
print,'mapping June data'
mask = circle(256,256,cx,cy,cfact*rx,1)
npang = 3.22
map709 = planetMap(reverse(im709z)*mask,cx,cy,rx,ry,subEarthLat,parang+npang,d,k,subSunLat,subSunLong-subEarthLong,6.,index=ijun)
mapJun = fltarr(600,300)
mapJun(0:299,*) = map709
mapJun = shift(mapJun,150 - subEarthLong/d)
;
;  put together the maps
;
print,'now merging maps'
map = mapSept + mapJun*(mapSept eq 0)
map = shift(map,90./d)
mapSepts = shift(mapSept,90./d)
mapJuns = shift(mapJun,90./d)
msk1 = (mapSepts gt 0) and (mapSepts lt 1500)
msk2 = (mapJuns gt 0) and (mapJuns lt 1500)
mapa = mapSepts*(msk1 and not msk2) + mapJuns*(msk2 and not msk1) + $
    (mapSepts+mapJuns)*(msk1 and msk2)/2.
print,'done'
;
;  annotate the map
;
;   blank out areas above 60 degrees latitude
mapa(*,150+60/d:299) = 0
mapa(*,0:150-60/d,*) = 0
mapnolines=mapa
;   latitude and longitude lines
;m = (600 - 1)/2.
m = max(mapa)/2.
if (n_elements(dolines) eq 0) then dolines=1
if (dolines eq 1) then begin & $
for i=0,359,30 do begin & $
  mapa(i/d,*) = m & $
endfor & $
for i=-60,60,30 do begin & $
  mapa(*,150+i/d) = m & $
endfor & $
endif
;
window,0,xsize=600,ysize=300
;device,filename = 'mapbrightness.ps',/color,xsize=20,ysize=10,/encapsulated,bits_per_pixel=8
loadct,3
;tvscl,mapa
exptv, mapa, /nobox, /data
contour, mapnolines>300<400, /overplot, levels=indgen(13)*50
;   write latitude/longitue labels
for i=-180,179,60 do begin & $
  xyouts, ((300-40 + (i+90)*(600./360.)) mod 600)/600., (150-60*(300./180.)-10)/300., string(-i), /normal, color=255 & $
endfor
for i=-60,60,60 do begin & $
  xyouts, (300-30 + (-150)*(600./360.))/600., (150 + i*(300./180.) + 5)/300., string(i), /normal, color=255 & $
endfor
;device,/close
