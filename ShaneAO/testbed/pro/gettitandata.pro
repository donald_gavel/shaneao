;  gettitandata - read from a Sep 96 Titan dataset

pro gettitandata,dataset,titan,titanDark,titanmdark,aveDark
  if n_params() eq 0 then begin
    print,"usage: gettitandata, dataset, titan,titanDark, titanmdark, titanmdarkb"
    return
  end
  print,'reading raw data'
  titan = readfits(dataset.datadir + dataset.datafile)
  titanDark = readfits(dataset.datadir + dataset.darkfile)
  print,'averaging darks'
  aveDark = total(titanDark,3)/100
  print,'subtracting darks'
  titanmdark = make_array(256,256,100)
  for i=0,99 do begin
    titanmdark(*,*,i) = titan(*,*,i) - aveDark
    print,format='($,I0," ")',i
  endfor
  print,'done'
end

function gettitansky,dataset,badpix,sky,skyDark
  if n_params() eq 0 then begin
    print,"usage: skyframe = gettitansky(dataset,badpix,[sky,skyDark])"
    return,0
  end
  print,'reading sky data'
  sky = readfits(dataset.datadir + dataset.skyfile)
  skyDark = readfits(dataset.datadir + dataset.skydarkfile)
  skymdark = sky - skyDark
  print,'averaging frames'
  skyframe = total(skymdark,3)/100;  average over the frames
  print,'removing bad pixels and stripes'
  skyframe = skyframe*(1-badpix) + median(skyframe,5)*badpix
  br = total(skyframe,2)/256;   remove the vertical stripes
  brv = total(br)/256;
  bra = make_array(256,256)
  for i=0,255,1 do bra(*,i) = br
  skyframe = skyframe - bra + brv
  return, skyframe
end

