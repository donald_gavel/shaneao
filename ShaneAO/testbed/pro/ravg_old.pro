;
;  ravg.pro - find radial average
;
function ravg,a

sizep = size(a)
x0 = sizep(1)/2.
y0 = sizep(2)/2.

xs=sizep(1)
ys=sizep(2)

dists=fltarr(xs,ys)

for i=0,xs-1 do begin
  for j=0,ys-1 do begin
    dists(i,j)=norm([float(i)-x0,float(j)-y0])
  endfor
endfor

dists=floor(dists)

vals=fltarr(xs/2)
for i=0,xs/2-1 do begin
   valid=where(dists eq i)
   if (n_elements(valid) gt 0) and (valid(0) gt (-1)) then vals(i) = avg(a(valid))
endfor
return,vals
end
