;+
;  type.pro - determine the data type of an idl expression
;
; usage:
;   t = type(a,[/text])
;
; input:
;   a - any expression
;
; output:
;   t - the type, in integer form:
;          0 - undefined
;          1 - byte
;          2 - int
;          3 - long int
;          4 - float
;          5 - double
;          6 - complex
;          7 - string
;          8 - structure
;          9 - complex double
;         10 - pointer
;         11 - object
;         12 - unsigned int
;         13 - unsigned long int
;         14 - 64 bit int
;         15 - unsigned 64 bit int
;
; keyword:
;   /text - return the type as a text string instead of integer
;-
function type,a,text=text
  t = (size(a))[(size(size(a)))[1]-2]
  if (keyword_set(text)) then begin
    case t of
       0: t='undefined'
       1: t='byte'
       2: t='integer'
       3: t='long int'
       4: t='float'
       5: t='double'
       6: t='complex'
       7: t='string'
       8: t='structure'
       9: t='complex double'
      10: t='pointer'
      11: t='object'
      12: t='unsigned int'
      13: t='unsigned long int'
      14: t='64 bit int'
      15: t='unsigned 64 bit int'
    endcase
  endif
  return,t
end
