; pib.pro - procedure to analyze power in the bucket measurements
;
function pib,image,cx,cy,r,sx,sy,sr
;
;  image - input image
;   cx,cy - center of spot
;   r - aperture radii: 1st element=area of interest, 2nd=total illum area
;  optional:
;   sx,sy - center of sky area
;    sr - sky area radius
;
  common photomblock, skyave, uall, circall
  n = (size(image))(1)
  m = (size(image))(2)
  print,'pib: calculating power in bucket...'
  circap = circle(n,m,cx,cy,r(0),1)
  uap = total(circap*image)
  print,'pib: done'

  if (size(r))(1) eq 2 then begin
    print,'pib: calculating total power'
    circall = circle(n,m,cx,cy,r(1),1)
    uall = total(circall*image)
    print,'pib: done'
  endif
  if n_params() gt 4 then begin
    circsky = circle(n,m,sx,sy,sr,1)
    skyave = total(circsky*image)/total(circsky)
    uap = uap - skyave*total(circap)
    uall = uall - skyave*total(circall)
  endif
  frac = uap/uall
  return,frac
end

pro pibi,image
  print,'center?'
  cursor,cx,cy,/dev
  print,cx,cy
  print,'aperture radius?'
  cursor,rx,ry,/dev
  r = sqrt((rx-cx)^2 + (ry-cy)^2)
  print,r
  print,'all light radius?'
  cursor,ax,ay,/dev
  ar = sqrt((ax-cx)^2 + (ay-cy)^2)
  print,ar
  print,'sky center?'
  cursor,sx,sy,/dev
  print,sx,sy
  print,'sky radius?'
  cursor,srx,sry,/dev
  sr = sqrt((srx-sx)^2 + (sry-sy)^2)
  print,sr
  print,'calculating fraction of light in aperture ...'
  print,pib(image,cx,cy,[r,ar],sx,sy,sr)
  return
end

function pibc,nosat,sat,gain,clip
  r = (sat lt clip)*sat + gain*(sat ge clip)*nosat
  return,r
end
