;+
; start.pro - start up file for IDL
;  sets up the path to the astronomy routines
;  usage: @start.pro
;-
;!path = expand_path('+/u/lgs/pro')+':'+EXPAND_PATH('+/u/user/observers/llnl/pro')+':'+!path
DEVICE, Decomposed=0, Bypass_Translation=0

