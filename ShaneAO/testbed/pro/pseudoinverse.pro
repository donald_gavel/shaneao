;+
;  pseudoinverse.pro - singular value decomposition and matrix pseudoinverse
;
;  usage:
;    ai = pseudoinverse(a,thresh)
;
;  inputs:
;    a - matrix of size n x m (any dimension, doesn't need to be square)
;    thresh - singular value threshold.  Any singular modes whose singlular
;             value ratio to the largest singular value is smaller than
;             the threshold are not used in the pseudo inverse reconstruction
;
;  output:
;    ai - pseudo inverse matrix, of size m x n
;
;-
function pseudoinverse,a,thresh,modeCount,w=w,u=u,v=v
  svdc, a, w, u, v
  n = (size(w))(1)
  s = fltarr(n,n)
  msv = max(w)
  modeCount = 0
  for i = 0,n-1 do begin
     if ( w[i]/msv gt thresh ) then begin
         s[i,i] = 1./w[i]
         modeCount += 1
     endif
  endfor
  ai = v ## s ## transpose(u)
  return,ai
end
