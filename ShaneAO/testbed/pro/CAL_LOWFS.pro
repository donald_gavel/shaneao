;+
;  LOWFS.pro
;   reconstruct wavefronts from GPI CAL LOWFS data
;
;  Usage:
;    result = lowfs(a,ap=ap,s=s,/nosec)
;    
;  Inputs:
;    a - raw image frame from 34x34 pixel section of the LOWFS camera (assumptions about the pitch and position of Hartmann dots are built in to the code)
;    /nosec - flag to tell the reconstructor to use (or not use) the central Hartmann dot
;    geometry - 'Hugdin' or 'Fried', default is 'Fried'
;  Outputs:
;    result - reconstructed phase, on 7x7 grid (Hudgin geometry) or 8x8 grid (Fried geometry)
;    ap - the valid phase points in the aperture, as assumed by this routine. Useful for subsequent zernike or other wavefront analysis.
;    s - resulting slopes x and y, on 7x7 grid (Hudgin geometry) or 8x8 grid (Fried geometry)
;    
;  Don Gavel, 10/31/11
;    revised:  4/9/12
;  
;-
function lowfs,a,ap=ap,s=s,nosec = nosec, geometry=geometry
  n = (size(a))[1]
  m = (size(a))[2]
  if (n_elements(geometry) eq 0) then geometry = 'Fried'
  
  ; Hartmann reconstruction
  ;  geometry
  n_across = 7
  pitch = 5.
  subap_size = 4
  
  isl = intarr(n_across,n_across,2)
  isl[*,*,0] = ones(n_across) ## indgen(n_across)
  isl[*,*,1] = transpose(isl[*,*,0])
  
  p0 = pitch*(n_across-1)/2
  sl = isl*pitch - p0
  
  mask = ones(n_across,n_across)
  mask[0,0:1] = 0
  mask[1,0] = 0
  if (not keyword_set(nosec)) then mask[3,3] = 0
  mask = mask * rotate(mask,1) * rotate(mask,2) * rotate(mask,3)
  
  ; measure the slopes
  s = fltarr(n_across,n_across,2)
  tip = ones(subap_size) ## (findgen(subap_size) - (subap_size-1)/2.)
  tilt = transpose(tip)
  normfac = total(tip*tip)
  
  for x = 0,n_across-1 do begin
    for y = 0,n_across-1 do begin
      px = isl[x,y,0]*pitch
      py = isl[x,y,1]*pitch
      subap = a[px:px+subap_size-1,py:py+subap_size-1]
      denom = total(subap)
      if (denom gt 0) then subap /= denom
      s[x,y,*] = -[total(tip*subap),total(tilt*subap)]
    endfor
  endfor
  
  ; reconstruct the wavefront
  arcsec = !pi/(3600.*180.)
  nm = 1.e-9
  ;
  ;  the plate scale is set so that reconstructed
  ;  phase for ASU motion of 0.1 mm is equal to
  ;  5.8 microns peak-to-valley:
  ;  ((0.1 mm / (8 m x f#17)) x 8m = 5.8 microns
  ;  So, as emperically determined from 10/27/11 data
  ;  the platescale is:
  platescale = 0.35*arcsec
  
  dtele = 8.0 ; Gemini
  d = dtele/n_across ; subap size
  sl_pupil = (sl/pitch)*d
  
  n2 = nextpow2(n_across)*2
  sx = zeropad(s[*,*,0],n2,n2)
  sy = zeropad(s[*,*,1],n2,n2)
  ap = zeropad(mask,n2,n2)

  ph = ftwfs(sx,sy,ap,boundaryFix='iterative',geometry=geometry,/subapfilter,niter=10,noiseVar=1.)
  ph = ph*d*platescale/nm

  if (geometry eq 'Hudgin') then begin
    ph = (shift(ph,-n2/4,-n2/4))[0:n_across-1,0:n_across-1]
    ap = (shift(ap,-n2/4,-n2/4))[0:n_across-1,0:n_across-1]
  endif
  if (geometry eq 'Fried') then begin
    ph = (shift(ph,-n2/4+1,-n2/4+1))[0:n_across,0:n_across]
    ap = ap or shift(ap,1,0) or shift(ap,0,1) or shift(ap,1,1) ; this activates all the edge actuators
    ap = (shift(ap,-n2/4,-n2/4))[0:n_across,0:n_across]
  endif
  return,ph
end

; ========================================= sample use code =================================
;clearwins

if (!version.os_name eq 'Mac OS X') then begin
  machine = 'mac'
  sep = '/'
  maindir = '/Users/DonGavel/Documents/Adaptive Optics/LAO/Gemini Planet Imager/GPI_CAL/'
endif
if (!version.os_name eq 'Microsoft Windows') then begin
  machine = 'pc'
  sep = '\'
  maindir = 'C:\Users\SONY USER\Documents\AdaptiveOptics\LAO\GPI\GPI_CAL\'
endif

dtele = 8.0 ; Gemini
n_across = 7
d = dtele/n_across ; subap size

;dataset = 'testPoke'
dataset = 'testTilt'
;dataset = '10/27/11'
;dataset = '12/01/11'
;dataset = '12/6/11'

if (dataset eq '12/01/11') then begin
  dir = maindir + 'Data_Dec6,2011'
  cd,dir
  
  geometry = 'Fried'
  p1 = readfits('111201_LOWFS_CalSphere_pinhole.fits')
  p2 = readfits('111201_LOWFS_ASU_pinhole_2.fits')
  ph1 = lowfs(p1,ap=ap,s=s1,/nosec,geometry=geometry)
  ph2 = lowfs(p2,s=s2,/nosec,geometry=geometry)
  
  ph1a = detilt(depiston(ph1,ap),ap)
  ph2a = detilt(depiston(ph2,ap),ap)
  
  disp,ph1a,ap=ap,'CalSphere_pinhole'
  disp,ph2a,ap=ap,'ASU_pinhole_2'
  
  print,'----------- CalSphere_pinhole report ---------------'
  coef = zernikefit(detilt(depiston(ph1a,ap),ap),ap,3.5,20,fit=fit,center=[3,3],/print)
  print,'----------- ASU_pinhole_2 report ---------------'
  coef = zernikefit(detilt(depiston(ph2a,ap),ap),ap,3.5,20,fit=fit,center=[3,3],/print)

;  dir_pc = 'C:\Users\SONY USER\Documents\AdaptiveOptics\LAO\GPI\GPI_CAL\Data_Dec6,2011\'
  dir = maindir+'Data_Dec6,2011'
  cd, dir

  a = readfits('20111206_Focus_0.fits')
  z = lowfs(a,/nosec,geometry=geometry)
  print,'----------- Focus_0 report ---------------'
  coef = zernikefit(detilt(depiston(z,ap),ap),ap,3.5,20,fit=fit,center=[3,3],/print)
  if (geometry eq 'Hudgin') then bap = circle(7,7,3.,3.,2.5,1)
  if (geometry eq 'Fried') then bap = circle(8,8,3.5,3.5,3.,1)
  print,'----------- Focus_0 report (brutal aperture) ---------------'
  coef = zernikefit(detilt(depiston(z,bap),bap),bap,2.5,20,fit=fit,center=[3,3],/print)
  print,'----------- Focus_0 relative to pinhole (brutal aperture) -------'
  coef = zernikefit(detilt(depiston(z-ph1a,bap),bap),bap,2.5,20,fit=fit,center=[3,3],/print)
endif

if (dataset eq 'testTilt') then begin

  geometry = 'Fried'
  a = fltarr(34,34)
  for x=1,31,5 do begin
    for y = 1,31,5 do begin
      a[x:x+1,y:y+1] = ones(2,2)
    endfor
  endfor
  a_nom = a
  a = fshift(a_nom,.2,0)
  ;disp,a-a_nom
  a *= (circle(34,34,16.5,16.5,17,1)-circle(34,34,16.5,16.5,2.5,1))
  ph_test = lowfs(a,ap=ap,s=s,geometry=geometry)
  ap_ph = circle(8,8,3.5,3.5,4,1.)
  ap_s = circle(7,7,3,3,3.5,1.)
  disp,ap_ph*ph_test,'phase'
  disp,ap_s*s[*,*,0],'slope x'
  disp,ap_s*s[*,*,1],'slope y'
endif

if (dataset eq 'testPoke') then begin
    
  geometry = 'Fried'
  ; dummy data test
  a = fltarr(34,34)
  for x=1,31,5 do begin
    for y = 1,31,5 do begin
      a[x:x+1,y:y+1] = ones(2,2)
    endfor
  endfor
  a_nom = a
  if (geometry eq 'Hudgin') then n = 7
  if (geometry eq 'Fried') then n = 8

  corner = [[1,2],[0,1]]
  edge = 2*[[0,1],[0,1]]
  i = 3 & j = 2 ; index of poke
  p = 5 ; pitch of subaps
  
  if (geometry eq 'Hudgin') then n = 7
  if (geometry eq 'Fried') then n = 8
  npokes = 8*8
  ph_test_set = fltarr(n,n,npokes)
  a_test_set = fltarr(34,34,npokes)
  poke = 0

  for i = 0,7 do begin
    for j = 0,7 do begin
      a = a_nom
      if (geometry eq 'Hudgin') then begin
        if ((i gt 0) and (j gt 0)) then a[(i-1)*p+1:(i-1)*p+2,(j-1)*p+1:(j-1)*p+2] = rotate(corner,3)
        if (i gt 0) then a[(i-1)*p+1:(i-1)*p+2,(j)*p+1:(j)*p+2] = rotate(edge,2)
        if ((i gt 0) and (j lt 6)) then a[(i-1)*p+1:(i-1)*p+2,(j+1)*p+1:(j+1)*p+2] = rotate(corner,2)
        if (j lt 6) then a[(i)*p+1:(i)*p+2,(j+1)*p+1:(j+1)*p+2] = rotate(edge,1)
        if ((i lt 6) and (j lt 6)) then a[(i+1)*p+1:(i+1)*p+2,(j+1)*p+1:(j+1)*p+2] = rotate(corner,1)
        if (i lt 6) then a[(i+1)*p+1:(i+1)*p+2,(j)*p+1:(j)*p+2] = rotate(edge,0)
        if ((i lt 6) and (j gt 0)) then a[(i+1)*p+1:(i+1)*p+2,(j-1)*p+1:(j-1)*p+2] = rotate(corner,0)
        if (j gt 0) then a[(i)*p+1:(i)*p+2,(j-1)*p+1:(j-1)*p+2] = rotate(edge,3)
      endif
      
      if (geometry eq 'Fried') then begin
        if ((i gt 0) and (j gt 0)) then a[(i-1)*p+1:(i-1)*p+2,(j-1)*p+1:(j-1)*p+2] = rotate(corner,3)
        if ((i gt 0) and (j lt 6)) then a[(i-1)*p+1:(i-1)*p+2,(j)*p+1:(j)*p+2] = rotate(corner,2)
        if ((i lt 6) and (j lt 6)) then a[(i)*p+1:(i)*p+2,(j)*p+1:(j)*p+2] = rotate(corner,1)
        if ((i lt 6) and (j gt 0)) then a[(i)*p+1:(i)*p+2,(j-1)*p+1:(j-1)*p+2] = rotate(corner,0)
      endif
      
      ; mask out the unilluminated subapertures
      a *= (circle(34,34,16.5,16.5,17,1)-circle(34,34,16.5,16.5,2.5,1))
      a_test = a
      
    ;  disp,a_test,'test poke (raw Hartmann image)'
      ph_test = lowfs(a,ap=ap,s=s,geometry=geometry)
      n2 = (size(ph_test))[1]
    ;  disp,ph_test*ap,'test poke reconstructed phase'
    
      a_test_set[*,*,poke] = a_test
      ph_test_set[*,*,poke] = ph_test
      poke++
      
    endfor
  endfor
  
  disp,a_test_set,'test poke (raw Hartmann images)'
  disp,ph_test_set,'test poke reconstructed phase'
endif

if (dataset eq '10/27/11') then begin
  
  dir = maindir + 'Data2'
  cd, dir
  
  geometry = 'Fried'

  ; focus test (bias-flat)
  file = '2011_10_27_PPM_GP_Mems_Flat.fits'
  a = readfits(file)
  ph_flat = lowfs(a,ap=ap,s=s_flat,geometry=geometry)
  
  file = '2011_10_27_PPM_GP_Mems_Bias.fits'
  a = readfits(file)
  ph_bias = lowfs(a,s=s_bias,geometry=geometry)
  
  disp,ap*(ph_flat-ph_bias),'bias-flat phase difference, nm',x0=-[1,1]*n_across/2*d,dx=[1,1]*d
  
  a0 = readfits('2011_10_27_PPM_GP_Mems_Bias_RefX0.fits')
  ax = readfits('2011_10_27_PPM_GP_Mems_Bias_RefX-0.1.fits')
  ay = readfits('2011_10_27_PPM_GP_Mems_Bias_RefY-0.1.fits')
  
  ph0 = lowfs(a0,s=s_ph0,geometry=geometry)
  phx = lowfs(ax,s=s_phx,geometry=geometry)
  phy = lowfs(ay,s=s_phy,geometry=geometry)
  
  disp,ap*(phx-ph0),'x motion',x0=-[1,1]*n_across/2*d,dx=[1,1]*d
  disp,ap*(phy-ph0),'y motion',x0=-[1,1]*n_across/2*d,dx=[1,1]*d
endif

if (dataset eq '12/6/11') then begin
  dir = maindir+'Data_Dec6,2011'
  cd, dir
  
  nfiles = 17
  aset = fltarr(34,34,nfiles)
  nphs = 17

  k = 0
  j = 0
  geometry = 'Fried'
  if (geometry eq 'Hudgin') then phset = fltarr(7,7,nphs)
  if (geometry eq 'Fried') then phset = fltarr(8,8,nphs)
  
  ; actuator 4,4 poke
  a = readfits('20111206_Poke_2um_Ref.fits') & aset[*,*,k]=a & k++
  z = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z & j++
  a = readfits('20111206_Poke_2um_4_4.fits') & aset[*,*,k]=a & k++ ; 2 microns
  ph = lowfs(a,/nosec,ap=ap,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,'Poke_2um_4_4.fits',ap=ap
  a = readfits('20111206_Poke_4um_4_4.fits') & aset[*,*,k]=a & k++ ; 4 microns
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,'Poke_4um_4_4.fits',ap=ap

  ; actuator 6,6 poke
  a = readfits('20111206_Poke_1um_6_6.fits') & aset[*,*,k]=a & k++ ; 1 micron
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'1um_6_6'
  a = readfits('20111206_Poke_2um_6_6.fits') & aset[*,*,k]=a & k++ ; 2 microns
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'2um_6_6'
  a = readfits('20111206_Poke_Ref_3.fits') & aset[*,*,k]=a & k++
  z = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z & j++
  a = readfits('20111206_Poke_0.5um_6_6.fits') & aset[*,*,k]=a & k++ ; 1/2 micron
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'0.5um_6_6'
  a = readfits('20111206_Poke_0.25um_6_6.fits') & aset[*,*,k]=a & k++ ; 1/4 micron
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'0.25um_6_6'
  
  ; groups of actuators pokes
  a = readfits('20111206_Poke_Ref_TS_1.fits') & aset[*,*,k]=a & k++
  z = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z & j++
  a = readfits('20111206_Poke_4-5_TS.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Poke_4-5_TS'
  a = readfits('20111206_Poke_0.25um_4-5_TS.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Poke_0.25um_4-5_TS'
  a = readfits('20111206_Poke_0.5um_4-5_TS.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Poke_0.5um_4-5_TS'
  a = readfits('20111206_Poke_0.25um_3-5_TS.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Poke_0.25_3-5_TS'
  a = readfits('20111206_Poke_0.5um_3-5_TS.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Poke_0.5um_3-5_TS'
  
  ; woofer focus tests
  a = readfits('20111206_Focus_0.fits') & aset[*,*,k]=a & k++
  z = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z & j++
  a = readfits('20111206_Focus_minus.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Focus_minus'
  print,'----------- Focus_minus report -------------'
  coef = zernikefit(-ph+z,ap,3.5,20,fit=fit,center=[3,3],/print)
  a = readfits('20111206_Focus_plus.fits') & aset[*,*,k]=a & k++
  ph = lowfs(a,/nosec,geometry=geometry) & phset[*,*,j] = z-ph & j++
  disp,z-ph,ap=ap,'Focus_plus'
  print,'----------- Focus_plus report -------------'
  coef = zernikefit(-ph+z,ap,3.5,20,fit=fit,center=[3,3],/print)
  ; analyze the focus_0 data:
  print,'----------- Focus_0 report ---------------'
  if (geometry eq 'Hudgin') then center = [3,3]
  if (geometry eq 'Fried') then center = [3.5,3.5]
  coef = zernikefit(detilt(depiston(z,ap),ap),ap,3.5,20,fit=fit,center=center,/print)
  if (geometry eq 'Hudgin') then fit_focus = coef[3]*zernike(7,7,3.5,center=center,2,0,l=0)
  if (geometry eq 'Fried') then fit_focus = coef[3]*zernike(8,8,4.,center=center,2,0,l=0)
  z1 = zeropad(detilt(depiston(z,ap),ap),9,9)
  fitf = zeropad(detilt(depiston(fit_focus,ap),ap),9,9)
  fit1 = zeropad(detilt(depiston(fit,ap),ap),9,9)
  ap1 = zeropad(ap,9,9)
  disp,[z1,fitf,fit1,z1-fitf,z1-fit1],ap=[ap1,ap1,ap1,ap1,ap1]
  ; [ focus0,  focus fit to focus0,  20 zernike fit to focus 0, residual after focus fit, residual after 20 zernike fit ]
endif
end
