;
;  fit a core-halo model to a psf
;  run strehl2.pro first
;
;  inputs
;    psf
;    psf0
;    strehl
;    r0
;
microns = 1.e-6
arcsec = !pi/(180.*3600.)
lambda = 0.9*microns
pixelscale = 0.027*arcsec
Dtele = 1.
r0 = 0.08*(lambda/(0.5*microns))^(6./5.)
n = (size(psf))[1]
dx = lambda/(n*pixelscale)
x = (ones(n) # (findgen(n)-n/2))*dx
y = transpose(x)
r = sqrt(x^2+y^2)

r0_set = [0.05,0.06,0.07,0.08,0.09,0.1,0.11,0.12]
r0_set = [0.15,0.12,0.1,0.08];[0.01,0.04,0.06,0.08]
alpha_set = [0.0,0.0,0.0,0.0]
;r0_set = 0.08*ones(n_elements(alpha_set))
;r0_set = [0.08,0.09,0.1]
n_cases = n_elements(r0_set)
firstTime = 1
for theCase = 0,n_cases-1 do begin

r0 = r0_set[theCase]*(lambda/(0.5*microns))^(6./5.)
sf = 6.88*(r/r0)^(5./3.) ; atmospheric structure function
rc = r*(r lt Dtele)
alpha = alpha_set[theCase];1.0;0.5;1.0
sf *= (1.-alpha*(rc/Dtele)^0.333) ; tilt-removed
mtf0 = abs(ft(psf0))
mtfa = exp(-0.5*sf)
psfa = abs(ft(mtfa*mtf0))
psfa /= total(psfa)
s = .1;strehl
psf_model = psf0*s + psfa*(1.-s)
;psf_model = psfa
;psf_model = psfa
;disp,psf_model/max(psf_model),'psf_model',x0=-n/2*[1,1]*pixelscale/arcsec,dx=[1,1]*pixelscale/arcsec
;disp,psf/max(psf),'psf',x0=-n/2*[1,1]*pixelscale/arcsec,dx=[1,1]*pixelscale/arcsec
psf_ra = ravg(psf)
psf_model_ra = ravg(psf_model)
nra = n_elements(psf_ra)
rr = findgen(nra)*pixelscale/arcsec
if (firstTime) then begin
  firstTime = 0
  window,/free
  xrange=[0.,1.5];;[0,1.5]
  yrange=[1.e-5,1.e-3];[1.e-6,1.e-2]
  ylog=1
;  plot,reform(r),reform(psf),xtitle='radius, arcsec',ylog=ylog,xrange=xrange,yrange=yrange;*max(psf_ra),psym=3
;  oplot,reform(r),reform(psf),color=120
  plot,rr,ravg(psf),xtitle='radius, arcsec',ylog=ylog,xrange=xrange,yrange=yrange;*max(psf_ra),psym=3
  oplot,rr,ravg(r),color=120
endif
oplot,rr,psf_model_ra

endfor
end
