;
;  map.pro - Maximum-A-Posteriori restoration
;
;  written by: Don Gavel, L-258, LLNL, gavel1@llnl.gov, Dec, 1998
;    based on Hunt, SPIE 1567, p.600
;    translated from the View macro written by Jim Brase, LLNL
;
;  usage:
;   est = map(im,psf,n,err)
;  where
;   im -- image
;   psf -- psf
;   n -- number of iterations
;   err -- measurement error signal
;
function map,im,psf,niter,err
  f = real(im)
  g = real(im)
  h = real(psf)
  n = (size(f))(1)
  hn = h/total(h)
  fs = smooth(f,3)
  fhigh = f-fs
  r = moment(fhigh)
  fhm = r[0]
  fhvar = r[1]
  print,'Noise estimate: ',fhvar
  err = fltarr(niter)
  lastIter = niter-1
  for i = 0,lastIter do begin
    f1 = real(ftconvolve(f,hn))
    err(i) = total((f1-im)^2)/n^2
    if (err(i) lt fhvar) then print,'Residual less than  noise variance, suggest stopping'
;    print,format='($,I0," ")',i
    print,'iteration ',i,' error = ',err(i)
    f2 = g / f1 - 1.
    f3 = real(ftconvolve(f2,hn))
    f4 = exp(f3)
    f = f*f4
  endfor
  print,'done'
  return,f
end
