;+
; WaveOptics.pro
;   General purpose simulator for wave optic propagation
;   This code converts commands in a spreadsheet or text file to IDL commands that
;   invoke calls to Fresnel and FresnelFT to simulate the wave propagation
;-
print,'<WaveOptics.pro> Starting'

verbose = 0

print,'<WaveOptics.pro> Select a propagation definition file'
inputFile = dialog_pickfile(get_path = dir,title = 'Select a propagation definition file',filter=['*.txt','*.csv'])
cd,dir
outputFile = 'WaveOptics_GeneratedCode.pro'

; template is used to read in the command lines from the inputFile
templ =     {version:float(1), $
             datastart:long(0), $
             delimiter:byte(44), $
             missingvalue:0.0, $
             commentsymbol:';', $
             fieldcount:lonarr(1), $
             fieldtypes:lonarr(10), $
             fieldnames:strarr(10), $
             fieldlocations:lonarr(10), $
             fieldgroups:lonarr(10)}
templ.fieldcount = 10
templ.fieldtypes = [7,7,4,4,4,4,4,4,4,4]
templ.fieldnames = ['kind', 'name', 'arg', 'FIELD04', 'FIELD05', 'FIELD06', 'FIELD07', 'FIELD08', 'FIELD09', 'FIELD10']
templ.fieldlocations = [0,12,19,0,0,0,0,0,0,0]
templ.fieldgroups = [0, 1, 2, 2, 2, 2, 2, 2, 2, 2]

r = read_ascii(inputFile,template=templ)
nlines = (size(r.kind))[1]
          
; generate code
print,'<WaveOptics.pro> Generating code...'
micron = 1.e-6
mm = 1.e-3
lambda = 0.5*micron
du = 1.*mm
inf = 1./0. ; infinity

openw,unit,outputFile,/get_lun
leader = '....'

banner = [[";=========== Wave Optics Simulator ================"], $
          ["; (c) 2010, Donald Gavel, UC Santa Cruz            "], $
          [";=================================================="]]
printf,unit,banner & if (verbose) then print,banner
time = systime()
cmd = '; Code Generated on '+time
printf,unit,cmd & if (verbose) then print,cmd

cmd = ';'                           & printf,unit,cmd & if (verbose) then print,cmd
cmd = '; Define constants'          & printf,unit,cmd & if (verbose) then print,cmd
cmd = 'micron = 1.e-6'              & printf,unit,cmd & if (verbose) then print,cmd
cmd = 'mm = 1.e-3'                  & printf,unit,cmd & if (verbose) then print,cmd
cmd = 'i = complex(0.,1.)'          & printf,unit,cmd & if (verbose) then print,cmd
cmd = 'inf = 1./0.  ; infinity'     & printf,unit,cmd & if (verbose) then print,cmd

for line = 0,nlines-1 do begin

  cmd = ';'                         & printf,unit,cmd & if (verbose) then print,cmd

  if (r.name[line] ne '') then begin
    cmd = '; (line '+strtrim(line,2)+') '+leader+r.name[line]+' ('+r.kind[line]+')'+leader & printf,unit,cmd & if (verbose) then print,cmd 
  endif
  
  if (r.kind[line] eq 'Definition') then begin
    name = r.name[line]
    lambda = r.arg[0,line]*micron
    n = fix(r.arg[2,line])
    beamDiam = r.arg[1,line]*mm
    du = 2*beamDiam/float(n)
    roc = inf
    cmd = 'lambda = '+strtrim(lambda,2) & printf,unit,cmd & if (verbose) then print,cmd  ; wavelength
    cmd = 'du = '+strtrim(du,2)         & printf,unit,cmd & if (verbose) then print,cmd  ; sample size on simulation grid
    cmd = 'n = '+strtrim(n,2)           & printf,unit,cmd & if (verbose) then print,cmd  ; # points across simulation grid
    cmd = 'roc = inf'                   & printf,unit,cmd & if (verbose) then print,cmd  ; wavefront radius of curvature
    cmd = 'wf = {du: du, roc: roc, d:beamDiam, wf:complexarr(n,n)}'  & printf,unit,cmd & if (verbose) then print,cmd
  endif

  if (r.kind[line] eq 'PlaneSource') then begin
    roc = inf
    cmd = 'intensity = '+strtrim(r.arg[0,line],2)  & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'wf.roc = inf'                           & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'wf.wf = ones(n,n)*complex(intensity)'   & printf,unit,cmd & if (verbose) then print,cmd
  endif
  
  if (r.kind[line] eq 'Aperture') then begin
    beamDiam = r.arg[0,line]*mm
    cmd = 'beamDiam = '+strtrim(r.arg[0,line],2)+'*mm'  & printf,unit,cmd & if (verbose) then print,cmd 
    cmd = 'ap = circle(n,n,n/2,n/2,beamDiam/2/du,1.)'   & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'wf.d = beamDiam'                             & printf,unit,cmd & if (verbose) then print,cmd ; beam diameter
    cmd = 'wf.wf *= ap'                                 & printf,unit,cmd & if (verbose) then print,cmd
  endif
  
  if (r.kind[line] eq 'Distance') then begin
    L = r.arg[0,line]*mm
    A = 1.
    B = L
    C = 0.
    D = 1.
    rayleighRange = beamDiam^2/lambda
    cmd = '; INFO: L = '+strtrim(L,2)+' meters; rayleighRange = '+strtrim(rayleighRange,2)+' meters' & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'L = '+strtrim(r.arg[0,line],2)+'*mm' & printf,unit,cmd & if (verbose) then print,cmd
    if (L gt 0.05*rayleighRange) then begin
      if (verbose) then print,'<WaveOptics.pro> dph < pi, using fresnelFT'
      cmd = 'A = 1.'                              & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'B = L '                              & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'C = 0.'                              & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'D = 1.'                              & printf,unit,cmd & if (verbose) then print,cmd
      dx = sqrt(L*lambda/float(n))
      cmd = 'dx = sqrt(L*lambda/float(n))'                           & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'x = (findgen(n)-n/2)*(dx/du)+n/2'                       & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'wfr = interpolate(wf.wf,x,x,/grid,cubic=-0.5)'          & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'wfr = fresnelft(wfr,dx,A,B,C,D,lambda)'                 & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'x = (findgen(n)-n/2)*(du/dx)+n/2'                       & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'wf.wf = interpolate(wfr,x,x,/grid,cubic=-0.5)'          & printf,unit,cmd & if (verbose) then print,cmd
    endif else begin
      if (verbose) then print,'<WaveOptics.pro> dph > phi, using fresnel'
      cmd = 'wf.wf = fresnel(wf.wf,du,B,lambda)'              & printf,unit,cmd & if (verbose) then print,cmd
    endelse
    cmd = 'if (finite(wf.roc) then wf.d = wf.d*(wf.roc-L)/wf.roc'
  endif
  
  if (r.kind[line] eq 'Lens') then begin
    wf_end = 'wf'+strtrim(line+1,2)
    f = r.arg[0,line]*mm
    L = r.arg[1,line]*mm
    roc = roc + f
    depth_of_focus = lambda*(beamDiam/roc)^2
    beamDiam = beamDiam*(roc-L)/roc
    cmd = 'f = '+strtrim(r.arg[0,line],2)+'*mm'                    & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'L = '+strtrim(r.arg[1,line],2)+'*mm'                    & printf,unit,cmd & if (verbose) then print,cmd
    cmd = 'roc = roc + f'                                          & printf,unit,cmd & if (verbose) then print,cmd
    if ( (L ge roc-depth_of_focus/2) and (L le roc+depth_of_focus/2)) then begin ; propagation to inside depth of focus
      cmd = wf_end +' = ft('+wf_start+')'                             & printf,unit,cmd & if (verbose) then print,cmd
      du = lambda*L/(n*du)
      cmd = 'du = lambda*L/(n*du)'                                   & printf,unit,cmd & if (verbose) then print,cmd
    endif else begin ; propagation to an intermediate plane outside focus
      Lprime = 1./(1./L - 1./f)
      cmd = 'Lprime = 1./(1./L - 1./f)'                              & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'A = 1.'                                                 & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'B = Lprime '                                            & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'C = 0.'                                                 & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'D = 1.'                                                 & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'dx = sqrt(L*lambda/float(n))'                           & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'x = (findgen(n)-n/2)*(dx/du)+n/2'                       & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'wfr = interpolate('+wf_start+',x,x,/grid,cubic=-0.5)'   & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'wfr = fresnelft(wfr,dx,A,B,C,D,lambda)'                 & printf,unit,cmd & if (verbose) then print,cmd
      cmd = 'x = (findgen(n)-n/2)*(du/dx)+n/2'                       & printf,unit,cmd & if (verbose) then print,cmd
      cmd = wf_end+' = interpolate(wfr,x,x,/grid,cubic=-0.5)'        & printf,unit,cmd & if (verbose) then print,cmd
      du = ((f-L)/f)*du  ; reset the sampling scale as the beam shrinks (expands)
      cmd = 'du = (f/(f-L))*du'                                      & printf,unit,cmd & if (verbose) then print,cmd
      if (du < 0) then begin ; went through focus
         cmd = wf_end+' = reverse(reverse(wf_end,1),2)'              & printf,unit,cmd & if (verbose) then print,cmd
         du = -du
         roc = -roc
         cmd = 'du = -du'                                            & printf,unit,cmd & if (verbose) then print,cmd
         cmd = 'roc =-roc'                                           & printf,unit,cmd & if (verbose) then print,cmd
      endif
    endelse
  endif
  
endfor

printf,unit,'end'

close,unit

print,'<WaveOptics.pro> ... Code Generation Complete'
print,'<WaveOptics.pro>   Now run the generated file ', dir+outputFile, ' to execute the simulation'
print,'<WaveOptics.pro> >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<'

end
