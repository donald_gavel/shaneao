;  dostar.pro - preprocess a reference psf star
;

badpix = mrdfits(badpixfile,0)
print,"subtracting dark"
star = mrdfits(dataset.datadir + dataset.starfile,0)
starDark = mrdfits(dataset.datadir + dataset.stardarkfile,0)
aveStarDark = total(starDark,3)/100
starmdark = make_array(256,256,100)
for i=0,99 do starmdark(*,*,i) = star(*,*,i) - aveStarDark
if n_elements(skyframe) eq 0 then skyframe = getiosky(dataset,badpix)
remsky, starmdark, skyframe, starmsky
devstripe, starmsky, starmsky
dehstripe, starmsky, starmsky
print,"flat-fielding"
if n_elements(flat) eq 0 then flat = makeflat(badpix)
starmsf = make_array(256,256,100)
for i=0,99 do starmsf(*,*,i) = starmsky(*,*,i)/(flat>.5)
cshiftg, starmsf, starmsfsw  ; shifted and windowed
;  analps, iomsfsw, aveps
prepfile = dataset.starname + "_prep.fits"
specklefile = dataset.starname + ".speckle"
; writefits,dataset.resultsdir + prepfile,long(starmsfsw)
end
