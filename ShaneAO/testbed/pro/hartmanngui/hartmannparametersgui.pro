; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	09/26/2006 23:35.30
; 
pro WID_BASE_1_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)


  wWidget =  Event.top

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='WID_BASE_1'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='cancelButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmannparametersgui_cancel, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='applyButton'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmannparametersgui_apply, Event
    end
    else:
  endcase

end

pro WID_BASE_1, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_

; Floating or modal bases must have a Group Leader.

  if(N_Elements(wGroup) eq 0)then $
     Message,'Group leader must be specified for Modal or Floating'+ $
      ' top level bases'

  Resolve_Routine, 'hartmannparametersgui_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  WID_BASE_1 = Widget_Base( GROUP_LEADER=wGroup, UNAME='WID_BASE_1'  $
      ,XOFFSET=5 ,YOFFSET=5 ,SCR_XSIZE=357 ,SCR_YSIZE=304  $
      ,NOTIFY_REALIZE='hartmannparametersgui_onrealize'  $
      ,TITLE='Hartmann sensor parameters' ,SPACE=3 ,XPAD=3 ,YPAD=3  $
      ,/MODAL)

  
  WID_LABEL_2 = Widget_Label(WID_BASE_1, UNAME='WID_LABEL_2'  $
      ,XOFFSET=39 ,YOFFSET=114 ,SCR_XSIZE=88 ,SCR_YSIZE=15  $
      ,/ALIGN_CENTER ,VALUE='Chip Subarray')

  
  WID_LABEL_0 = Widget_Label(WID_BASE_1, UNAME='WID_LABEL_0'  $
      ,XOFFSET=42 ,YOFFSET=7 ,SCR_XSIZE=56 ,SCR_YSIZE=14  $
      ,/ALIGN_CENTER ,VALUE='Scale')

  
  WID_BASE_0 = Widget_Base(WID_BASE_1, UNAME='WID_BASE_0' ,XOFFSET=25  $
      ,YOFFSET=215 ,SCR_XSIZE=315 ,SCR_YSIZE=50 ,SPACE=3 ,XPAD=3  $
      ,YPAD=3)

  
  cancelButton = Widget_Button(WID_BASE_0, UNAME='cancelButton'  $
      ,XOFFSET=181 ,YOFFSET=11 ,SCR_XSIZE=83 ,SCR_YSIZE=28  $
      ,/ALIGN_CENTER ,VALUE='Cancel')

  
  applyButton = Widget_Button(WID_BASE_0, UNAME='applyButton'  $
      ,XOFFSET=48 ,YOFFSET=10 ,SCR_XSIZE=83 ,SCR_YSIZE=29  $
      ,/ALIGN_CENTER ,VALUE='Apply')

  
  WID_BASE_2 = Widget_Base(WID_BASE_1, UNAME='WID_BASE_2' ,FRAME=1  $
      ,XOFFSET=26 ,YOFFSET=123 ,SCR_XSIZE=311 ,SCR_YSIZE=78 ,SPACE=3  $
      ,XPAD=3 ,YPAD=3)

  
  subarrayStartXEntry = Widget_Text(WID_BASE_2,  $
      UNAME='subarrayStartXEntry' ,XOFFSET=133 ,YOFFSET=17  $
      ,SCR_XSIZE=52 ,SCR_YSIZE=18 ,/EDITABLE ,VALUE=[ '0' ] ,XSIZE=20  $
      ,YSIZE=1)

  
  subarrayStartYEntry = Widget_Text(WID_BASE_2,  $
      UNAME='subarrayStartYEntry' ,XOFFSET=193 ,YOFFSET=18  $
      ,SCR_XSIZE=52 ,SCR_YSIZE=18 ,/EDITABLE ,VALUE=[ '0' ] ,XSIZE=20  $
      ,YSIZE=1)

  
  subarraySizeXEntry = Widget_Text(WID_BASE_2,  $
      UNAME='subarraySizeXEntry' ,XOFFSET=133 ,YOFFSET=44  $
      ,SCR_XSIZE=52 ,SCR_YSIZE=18 ,/EDITABLE ,VALUE=[ '768' ]  $
      ,XSIZE=20 ,YSIZE=1)

  
  subarraySizeYEntry = Widget_Text(WID_BASE_2,  $
      UNAME='subarraySizeYEntry' ,XOFFSET=192 ,YOFFSET=44  $
      ,SCR_XSIZE=52 ,SCR_YSIZE=18 ,/EDITABLE ,VALUE=[ '512' ]  $
      ,XSIZE=20 ,YSIZE=1)

  
  WID_LABEL_7 = Widget_Label(WID_BASE_2, UNAME='WID_LABEL_7'  $
      ,XOFFSET=68 ,YOFFSET=45 ,SCR_XSIZE=42 ,SCR_YSIZE=18  $
      ,/ALIGN_LEFT ,VALUE='size')

  
  WID_LABEL_6 = Widget_Label(WID_BASE_2, UNAME='WID_LABEL_6'  $
      ,XOFFSET=68 ,YOFFSET=19 ,SCR_XSIZE=49 ,SCR_YSIZE=18  $
      ,/ALIGN_LEFT ,VALUE='start')

  
  WID_BASE_3 = Widget_Base(WID_BASE_1, UNAME='WID_BASE_3' ,FRAME=1  $
      ,XOFFSET=25 ,YOFFSET=13 ,SCR_XSIZE=310 ,SCR_YSIZE=82 ,SPACE=3  $
      ,XPAD=3 ,YPAD=3)

  
  WID_LABEL_5 = Widget_Label(WID_BASE_3, UNAME='WID_LABEL_5'  $
      ,XOFFSET=10 ,YOFFSET=43 ,SCR_XSIZE=167 ,SCR_YSIZE=18  $
      ,/ALIGN_LEFT ,VALUE='Subap pitch (pixels)')

  
  subapPitchEntry = Widget_Text(WID_BASE_3, UNAME='subapPitchEntry'  $
      ,XOFFSET=183 ,YOFFSET=42 ,SCR_XSIZE=50 ,SCR_YSIZE=18 ,/EDITABLE  $
      ,VALUE=[ '11' ] ,XSIZE=20 ,YSIZE=1)

  
  WID_LABEL_8 = Widget_Label(WID_BASE_3, UNAME='WID_LABEL_8'  $
      ,XOFFSET=10 ,YOFFSET=15 ,SCR_XSIZE=167 ,SCR_YSIZE=18  $
      ,/ALIGN_LEFT ,VALUE='Plate scale (arcsec/pixel)')

  
  plateScaleEntry = Widget_Text(WID_BASE_3, UNAME='plateScaleEntry'  $
      ,XOFFSET=182 ,YOFFSET=15 ,SCR_XSIZE=50 ,SCR_YSIZE=18 ,/EDITABLE  $
      ,VALUE=[ '2.0' ] ,XSIZE=20 ,YSIZE=1)

  Widget_Control, /REALIZE, WID_BASE_1

  XManager, 'WID_BASE_1', WID_BASE_1, /NO_BLOCK  

end
; 
; Empty stub procedure used for autoloading.
; 
pro hartmannparametersgui, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  WID_BASE_1, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
