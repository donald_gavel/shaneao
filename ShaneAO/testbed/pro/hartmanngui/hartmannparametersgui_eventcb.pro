;
; IDL Event Callback Procedures
; hartmannparametersgui_eventcb
;
; Generated on:	08/14/100 18:31.28
;
;
; Empty stub procedure used for autoloading.
;
pro hartmannparametersgui_eventcb
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro hartmannparametersgui_cancel, Event
  widget_control, event.top, /destroy
end
pro hartmannparametersgui_onrealize, id
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir

  widget_control, id, xoffset=50, yoffset=50
  entry = widget_info(id,find_by_uname='plateScaleEntry')
  widget_control,entry,set_value=string(plateScale,format='(f6.2)')
  entry = widget_info(id,find_by_uname='subapPitchEntry')
  widget_control,entry,set_value=string(subap_pitch,format='(f6.2)')
  entry = widget_info(id,find_by_uname='subarrayStartXEntry')
  widget_control,entry,set_value=string(subarray[0])
  entry = widget_info(id,find_by_uname='subarrayStartYEntry')
  widget_control,entry,set_value=string(subarray[1])
  entry = widget_info(id,find_by_uname='subarraySizeXEntry')
  widget_control,entry,set_value=string(subarray[2])
  entry = widget_info(id,find_by_uname='subarraySizeYEntry')
  widget_control,entry,set_value=string(subarray[3])
end
pro hartmannparametersgui_apply, Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir

  entry = widget_info(event.top,find_by_uname='plateScaleEntry')
  widget_control,entry,get_value=text
  platescale = float(text[0])
  entry = widget_info(event.top,find_by_uname='subapPitchEntry')
  widget_control,entry,get_value=text
  subap_pitch = float(text[0])
  entry = widget_info(event.top,find_by_uname='subarrayStartXEntry')
  widget_control,entry,get_value=text
  subarray[0] = float(text[0])
  entry = widget_info(event.top,find_by_uname='subarrayStartYEntry')
  widget_control,entry,get_value=text
  subarray[1] = float(text[0])
  entry = widget_info(event.top,find_by_uname='subarraySizeXEntry')
  widget_control,entry,get_value=text
  subarray[2] = float(text[0])
  entry = widget_info(event.top,find_by_uname='subarraySizeYEntry')
  widget_control,entry,get_value=text
  subarray[3] = float(text[0])

  widget_control, event.top, /destroy
end
