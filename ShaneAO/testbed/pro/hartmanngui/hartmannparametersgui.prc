HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	10/02/100 21:53.25
VERSION 1
END

WID_BASE_1 BASE 5 5 357 304
REALIZE "hartmannparametersgui_onrealize"
TLB
CAPTION "Hartmann sensor parameters"
MODAL
XPAD = 3
YPAD = 3
SPACE = 3
BEGIN
  WID_LABEL_2 LABEL 39 114 88 15
  VALUE "Chip Subarray"
  ALIGNCENTER
  END
  WID_LABEL_0 LABEL 42 7 56 14
  VALUE "Scale"
  ALIGNCENTER
  END
  WID_BASE_0 BASE 25 215 315 50
  XPAD = 3
  YPAD = 3
  SPACE = 3
  BEGIN
    cancelButton PUSHBUTTON 181 11 83 28
    VALUE "Cancel"
    ALIGNCENTER
    ONACTIVATE "hartmannparametersgui_cancel"
    END
    applyButton PUSHBUTTON 48 10 83 29
    VALUE "Apply"
    ALIGNCENTER
    ONACTIVATE "hartmannparametersgui_apply"
    END
  END
  WID_BASE_2 BASE 26 123 311 78
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  BEGIN
    subarrayStartXEntry TEXT 133 17 52 18
    NUMITEMS = 1
    ITEM "0"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    subarrayStartYEntry TEXT 193 18 52 18
    NUMITEMS = 1
    ITEM "0"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    subarraySizeXEntry TEXT 133 44 52 18
    NUMITEMS = 1
    ITEM "768"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    subarraySizeYEntry TEXT 192 44 52 18
    NUMITEMS = 1
    ITEM "512"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    WID_LABEL_7 LABEL 68 45 42 18
    VALUE "size"
    ALIGNLEFT
    END
    WID_LABEL_6 LABEL 68 19 49 18
    VALUE "start"
    ALIGNLEFT
    END
  END
  WID_BASE_3 BASE 25 13 310 82
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  BEGIN
    WID_LABEL_5 LABEL 10 43 167 18
    VALUE "Subap pitch (pixels)"
    ALIGNLEFT
    END
    subapPitchEntry TEXT 183 42 50 18
    NUMITEMS = 1
    ITEM "11"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
    WID_LABEL_8 LABEL 10 15 167 18
    VALUE "Plate scale (arcsec/pixel)"
    ALIGNLEFT
    END
    plateScaleEntry TEXT 182 15 50 18
    NUMITEMS = 1
    ITEM "2.0"
    EDITABLE
    WIDTH = 20
    HEIGHT = 1
    END
  END
END
