  n = subarray[2]
  m = subarray[3]
  n2 = nextpow2(n)
  m2 = nextpow2(m)
  n2 = max(n2,m2)
  dummy = fltarr(n2,n2)
  nsa = round(subap_pitch)
  sa = gauss2(nsa,nsa/2,nsa/2,nsa/2)
  ns = (size(geom))(2)
  for i=0,ns-1 do begin
    x = n2/2 + geom[0,i]*subap_pitch
    y = n2/2 + geom[1,i]*subap_pitch
    x0 = x-nsa/2
    y0 = y-nsa/2
    dummy[x0:x0+nsa-1,y0:y0+nsa-1] = sa
  endfor
  ; pre-process the data image
  x0 = subarray[0]
  y0 = subarray[1]
  im_pp = im_raw[x0:x0+n-1,y0:y0+m-1]
  h = histogram(im_pp,omax=im_max,omin=im_min)
  hmax = max(h)
  imax = !c
  i = imax
  while h[i] gt hmax/2. do i=i+1
  clip = im_min + imax + 3*(i-imax)
  im_pp = (im_pp > clip) - clip
  ; convolve the dummy with the data to find the
  ; absolute spot locations
  im = fltarr(n2,n2)
  im[0:n-1,0:m-1] = im_pp
  imf = ft(im)
  dumf = ft(dummy)
  ccf = imf*dumf
  cc = abs(ft(ccf,/inverse))
  ccmax = max(cc)
  center = coords(cc,!c)
  ; create the rois
  roi = fltarr(2,ns) ; each roi location is subap center
  for i=0,ns-1 do begin
    roi[0,i] = center[0] + geom[0,i]*subap_pitch
    roi[1,i] = center[1] + geom[1,i]*subap_pitch
  endfor
  ; display the rois
  im_roi = fltarr(n,m)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    im_roi[x0:x1,y0] = 1
    im_roi[x0:x1,y1] = 1
    im_roi[x0,y0:y1] = 1
    im_roi[x1,y0:y1] = 1
  endfor
  im_roi = im_roi + im_pp/max(im_pp)
  ; calculate the refcents
  refcent = fltarr(2,ns)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    subap = im_pp[x0:x1,y0:y1]
    refcent[*,i] = centroid(subap) - float(subap_pitch)/2.
  endfor
end
