; 
; IDL Widget Interface Procedures. This Code is automatically 
;     generated and should not be modified.

; 
; Generated on:	09/26/2006 23:35.30
; 
pro WID_BASE_0_event, Event

  wTarget = (widget_info(Event.id,/NAME) eq 'TREE' ?  $
      widget_info(Event.id, /tree_root) : event.id)


  wWidget =  Event.top

  case wTarget of

    Widget_Info(wWidget, FIND_BY_UNAME='WID_BASE_0'): begin
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_28'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_file_openimage, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_29'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_file_editparameters, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_37'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_file_exit, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_39'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_calculate_refcentsnow, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_40'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_calculate_centroidsnow, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_41'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_calculate_trigger, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_43'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_display_image, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_44'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_display_roisonimage, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_45'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_display_dots, Event
    end
    Widget_Info(wWidget, FIND_BY_UNAME='W_MENU_46'): begin
      if( Tag_Names(Event, /STRUCTURE_NAME) eq 'WIDGET_BUTTON' )then $
        hartmanngui_display_arrows, Event
    end
    else:
  endcase

end

pro WID_BASE_0, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_

  Resolve_Routine, 'hartmanngui_eventcb',/COMPILE_FULL_FILE  ; Load event callback routines
  
  WID_BASE_0 = Widget_Base( GROUP_LEADER=wGroup, UNAME='WID_BASE_0'  $
      ,XOFFSET=5 ,YOFFSET=5 ,SCR_XSIZE=300 ,SCR_YSIZE=312  $
      ,NOTIFY_REALIZE='hartmanngui_realize' ,TITLE='Hartmann'+ $
      ' Analysis' ,SPACE=3 ,XPAD=3 ,YPAD=3 ,MBAR=WID_BASE_0_MBAR)

  
  W_MENU_25 = Widget_Button(WID_BASE_0_MBAR, UNAME='W_MENU_25' ,/MENU  $
      ,VALUE='File')

  
  W_MENU_28 = Widget_Button(W_MENU_25, UNAME='W_MENU_28' ,VALUE='Open'+ $
      ' image...')

  
  W_MENU_29 = Widget_Button(W_MENU_25, UNAME='W_MENU_29' ,VALUE='Edit'+ $
      ' parameters...')

  
  W_MENU_37 = Widget_Button(W_MENU_25, UNAME='W_MENU_37' ,/SEPARATOR  $
      ,VALUE='Exit')

  
  W_MENU_26 = Widget_Button(WID_BASE_0_MBAR, UNAME='W_MENU_26' ,/MENU  $
      ,VALUE='Calculate')

  
  W_MENU_39 = Widget_Button(W_MENU_26, UNAME='W_MENU_39'  $
      ,VALUE='Refcents now')

  
  W_MENU_40 = Widget_Button(W_MENU_26, UNAME='W_MENU_40'  $
      ,VALUE='Centroids now')

  
  W_MENU_41 = Widget_Button(W_MENU_26, UNAME='W_MENU_41'  $
      ,VALUE='Trigger on camera [off]')

  
  W_MENU_42 = Widget_Button(WID_BASE_0_MBAR, UNAME='W_MENU_42' ,/MENU  $
      ,VALUE='Display')

  
  W_MENU_43 = Widget_Button(W_MENU_42, UNAME='W_MENU_43'  $
      ,VALUE='Image')

  
  W_MENU_44 = Widget_Button(W_MENU_42, UNAME='W_MENU_44' ,VALUE='ROIs'+ $
      ' on image')

  
  W_MENU_45 = Widget_Button(W_MENU_42, UNAME='W_MENU_45'  $
      ,VALUE='Centroids as dots')

  
  W_MENU_46 = Widget_Button(W_MENU_42, UNAME='W_MENU_46'  $
      ,VALUE='Centroids as arrows')

  
  W_MENU_47 = Widget_Button(W_MENU_42, UNAME='W_MENU_47'  $
      ,VALUE='Wavefront as grayscale')

  
  W_MENU_48 = Widget_Button(W_MENU_42, UNAME='W_MENU_48'  $
      ,VALUE='Wavefront as surface')

  
  W_MENU_50 = Widget_Button(WID_BASE_0_MBAR, UNAME='W_MENU_50' ,/MENU  $
      ,VALUE='Help')

  
  W_MENU_51 = Widget_Button(W_MENU_50, UNAME='W_MENU_51'  $
      ,VALUE='Help')

  
  WID_BASE_1 = Widget_Base(WID_BASE_0, UNAME='WID_BASE_1' ,FRAME=1  $
      ,XOFFSET=5 ,YOFFSET=19 ,SCR_XSIZE=271 ,SCR_YSIZE=236 ,SPACE=3  $
      ,XPAD=3 ,YPAD=3)

  
  WID_LABEL_1 = Widget_Label(WID_BASE_1, UNAME='WID_LABEL_1'  $
      ,XOFFSET=6 ,/ALIGN_LEFT ,VALUE='Zernike List')

  
  zernikelist = Widget_List(WID_BASE_1, UNAME='zernikelist'  $
      ,XOFFSET=9 ,YOFFSET=38 ,SCR_XSIZE=253 ,SCR_YSIZE=179 ,/MULTIPLE  $
      ,VALUE=[ 'Tip 1 1 cos', 'Tilt 1 1 sin', 'Focus 2 0',  $
      'Astigmatism 2 2 cos', 'Astigmatism 2 2 sin', 'Coma 3 1 cos',  $
      'Coma 3 1 sin', 'Coma 3 3 cos', 'Coma 3 3 sin', 'Spherical 4 0'  $
      ] ,XSIZE=11 ,YSIZE=2)

  Widget_Control, /REALIZE, WID_BASE_0

  XManager, 'WID_BASE_0', WID_BASE_0, /NO_BLOCK  

end
; 
; Empty stub procedure used for autoloading.
; 
pro hartmanngui, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
  WID_BASE_0, GROUP_LEADER=wGroup, _EXTRA=_VWBExtra_
end
