;+
;  sllick.pro - Lick subap locations
;
;  usage:
;    sl = sllick()
;    sl = sllick_illum()
;-
function sllick_illum
; only the fully illuminated subaps
  sl0 = sllick()
  illum = [3,4,5,6,8,9,10,11,12,13, $
           15,16,17,18,21,22,23,24, $
           26,27,28,29,30,31, $
           33,34,35,36]
  sl = sl0[*,illum]
  return, sl
end
function sllick,illum=illum
  if keyword_set(illum) then begin
    sl = sllick_illum()
    return,sl
  endif
  sl = fltarr(2,40)
  sl[1,0:1] = 3
  sl[0,0:1] = [-1,0]
  sl[1,2:7] = 2
  sl[0,2:7] = [-3,-2,-1,0,1,2]
  sl[1,8:13] = 1
  sl[0,8:13] = [-3,-2,-1,0,1,2]
  sl[1,14:19] = 0
  sl[0,14:19] = [-4,-3,-2,1,2,3]
  sl[1,20:25] = -1
  sl[0,20:25] = [-4,-3,-2,1,2,3]
  sl[1,26:31] = -2
  sl[0,26:31] = [-3,-2,-1,0,1,2]
  sl[1,32:37] = -3
  sl[0,32:37] = [-3,-2,-1,0,1,2]
  sl[1,38:39] = -4
  sl[0,38:39] = [-1,0]
  sl = sl + 0.5
  return, sl
end
