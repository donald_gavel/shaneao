HEADER
; IDL Visual Widget Builder Resource file. Version 1
; Generated on:	08/23/100 21:40.53
VERSION 1
END

WID_BASE_0 BASE 5 5 300 312
REALIZE "hartmanngui_realize"
TLB
CAPTION "Hartmann Analysis"
XPAD = 3
YPAD = 3
SPACE = 3
BEGIN
  WID_BASE_0_MBAR MENUBAR 0 0 0 0
  BEGIN
    W_MENU_25 PUSHBUTTON 0 0 0 0
    VALUE "File"
    MENU
    BEGIN
      W_MENU_28 PUSHBUTTON 0 0 0 0
      VALUE "Open image..."
      ONACTIVATE "hartmanngui_file_openimage"
      END
      W_MENU_29 PUSHBUTTON 0 0 0 0
      VALUE "Edit parameters..."
      ONACTIVATE "hartmanngui_file_editparameters"
      END
      W_MENU_37 PUSHBUTTON 0 0 0 0
      VALUE "Exit"
      SEPARATOR
      ONACTIVATE "hartmanngui_file_exit"
      END
    END
    W_MENU_26 PUSHBUTTON 0 0 0 0
    VALUE "Calculate"
    MENU
    BEGIN
      W_MENU_39 PUSHBUTTON 0 0 0 0
      VALUE "Refcents now"
      ONACTIVATE "hartmanngui_calculate_refcentsnow"
      END
      W_MENU_40 PUSHBUTTON 0 0 0 0
      VALUE "Centroids now"
      ONACTIVATE "hartmanngui_calculate_centroidsnow"
      END
      W_MENU_41 PUSHBUTTON 0 0 0 0
      VALUE "Trigger on camera [off]"
      ONACTIVATE "hartmanngui_calculate_trigger"
      END
    END
    W_MENU_42 PUSHBUTTON 0 0 0 0
    VALUE "Display"
    MENU
    BEGIN
      W_MENU_43 PUSHBUTTON 0 0 0 0
      VALUE "Image"
      ONACTIVATE "hartmanngui_display_image"
      END
      W_MENU_44 PUSHBUTTON 0 0 0 0
      VALUE "ROIs on image"
      ONACTIVATE "hartmanngui_display_roisonimage"
      END
      W_MENU_45 PUSHBUTTON 0 0 0 0
      VALUE "Centroids as dots"
      ONACTIVATE "hartmanngui_display_dots"
      END
      W_MENU_46 PUSHBUTTON 0 0 0 0
      VALUE "Centroids as arrows"
      ONACTIVATE "hartmanngui_display_arrows"
      END
      W_MENU_47 PUSHBUTTON 0 0 0 0
      VALUE "Wavefront as grayscale"
      END
      W_MENU_48 PUSHBUTTON 0 0 0 0
      VALUE "Wavefront as surface"
      END
    END
    W_MENU_50 PUSHBUTTON 0 0 0 0
    VALUE "Help"
    MENU
    BEGIN
      W_MENU_51 PUSHBUTTON 0 0 0 0
      VALUE "Help"
      END
    END
  END
  WID_BASE_1 BASE 5 19 271 236
  FRAME = 1
  XPAD = 3
  YPAD = 3
  SPACE = 3
  BEGIN
    WID_LABEL_1 LABEL 6 0 0 0
    VALUE "Zernike List"
    ALIGNLEFT
    END
    zernikelist LIST 9 38 253 179
    NUMITEMS = 10
    ITEM "Tip 1 1 cos"
    ITEM "Tilt 1 1 sin"
    ITEM "Focus 2 0"
    ITEM "Astigmatism 2 2 cos"
    ITEM "Astigmatism 2 2 sin"
    ITEM "Coma 3 1 cos"
    ITEM "Coma 3 1 sin"
    ITEM "Coma 3 3 cos"
    ITEM "Coma 3 3 sin"
    ITEM "Spherical 4 0"
    MULTIPLE
    WIDTH = 11
    HEIGHT = 2
    END
  END
END
