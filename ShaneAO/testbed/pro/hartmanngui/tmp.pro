; tmp.pro
common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
          geom_file, refcent_file, $
          subarray,platescale,subap_pitch,center, $
          cent,gain,zcoef,ddir
resolve_routine,'zernike',/is_function
ddir = 'E:\LaserGuidestar\BackWFSdata\'
wdir = 'C:\RSI\IDL53\lib\pro\hartmanngui\'
im_raw = readfits(ddir + 'im6.fits')
geom = sllick(/illum)
geom_file = wdir + 'lickgeom.fits'
subarray = [325,287,100,100]
subap_pitch = 11.
gain = 35.
end

