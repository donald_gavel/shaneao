;
; IDL Event Callback Procedures
; hartmanngui_eventcb
;
; Generated on:	08/14/100 12:49.19
;
;
; Empty stub procedure used for autoloading.
;
pro hartmanngui_eventcb
end
pro hartmanngui_realize,Event
  ; set up all the default initial values
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
          geom_file, refcent_file, $
          subarray,platescale,subap_pitch,center, $
          cent,gain,zcoef,ddir
  resolve_routine,'zernike',/is_function,/compile_full_file
  ddir = 'E:\LaserGuidestar\BackWFSdata\'
  wdir = 'C:\RSI\IDL53\lib\pro\hartmanngui\'
  im_raw = readfits(ddir + 'im6.fits')
  geom = sllick(/illum)
  geom_file = wdir + 'lickgeom.fits'
  subarray = [325,287,100,100]
  platescale = 2.0
  subap_pitch = 11.
  gain = 35.
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro hartmanngui_file_exit, Event
  widget_control,Event.top,/destroy
end
pro hartmanngui_calculate_trigger, Event
  widget_control,Event.id,get_uvalue=state
  if (n_elements(state) eq 0) then state = 0
  if (state eq 0) then begin
    widget_control,Event.id,set_value='Trigger on camera [on]'
    widget_control,Event.id,set_uvalue=1
  endif else begin
    widget_control,Event.id,set_value='Trigger on camera [off]'
    widget_control,Event.id,set_uvalue=0
  endelse
end
pro hartmanngui_file_editparameters,event
  print,'hartmannparametersgui'
  hartmannparametersgui,group_leader=event.top
end
;-----------------------------------------------------------------
; Activate Button Callback Procedure.
; Argument:
;   Event structure:
;
;   {WIDGET_BUTTON, ID:0L, TOP:0L, HANDLER:0L, SELECT:0}
;
;   ID is the widget ID of the component generating the event. TOP is
;       the widget ID of the top level widget containing ID. HANDLER
;       contains the widget ID of the widget associated with the
;       handler routine.

;   SELECT is set to 1 if the button was set, and 0 if released.
;       Normal buttons do not generate events when released, so
;       SELECT will always be 1. However, toggle buttons (created by
;       parenting a button to an exclusive or non-exclusive base)
;       return separate events for the set and release actions.

;   Retrieve the IDs of other widgets in the widget hierarchy using
;       id=widget_info(Event.top, FIND_BY_UNAME=name)

;-----------------------------------------------------------------
pro hartmanngui_file_openimage, Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  if n_elements(ddir) eq 0 then begin
    file = dialog_pickfile(/read,get_path=ddir,filter='*.fits')
  endif else begin
    file = dialog_pickfile(/read,path=ddir,get_path=ddir,filter='*.fits')
  endelse
  if (strlen(file) ne 0) then begin
    im_raw = readfits(file)
;    dispgui,im_raw,file
  endif
end
pro hartmanngui_calculate_refcentsnow, Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  ; read the hartmann geometry file
  if (strlen(geom_file) eq 0) then geom_file = 'lickgeom.fits'
  ;geom = readfits(geom_file)
  ; create a dummy hartmann image
  n = subarray[2]
  m = subarray[3]
  n2 = nextpow2(n)
  m2 = nextpow2(m)
  n2 = max(n2,m2)
  dummy = fltarr(n2,n2)
  nsa = round(subap_pitch)
  sa = gauss2(nsa,nsa/2,nsa/2,nsa/2)
  ns = (size(geom))(2)
  for i=0,ns-1 do begin
    x = n2/2 + geom[0,i]*subap_pitch
    y = n2/2 + geom[1,i]*subap_pitch
    x0 = x-nsa/2
    y0 = y-nsa/2
    dummy[x0:x0+nsa-1,y0:y0+nsa-1] = sa
  endfor
  ; pre-process the data image
  x0 = subarray[0]
  y0 = subarray[1]
  im_pp = im_raw[x0:x0+n-1,y0:y0+m-1]
  h = histogram(im_pp,omax=im_max,omin=im_min)
  hmax = max(h)
  imax = !c
  i = imax
  while h[i] gt hmax/2. do i=i+1
  clip = im_min + imax + 3*(i-imax)
  im_pp = (im_pp > clip) - clip
  ; convolve the dummy with the data to find the
  ; absolute spot locations
  im = fltarr(n2,n2)
  im[0:n-1,0:m-1] = im_pp
  imf = ft(im)
  dumf = ft(dummy)
  ccf = imf*dumf
  cc = abs(ft(ccf,/inverse))
  ccmax = max(cc)
  center = coords(cc,!c)
  ; create the rois
  roi = fltarr(2,ns) ; each roi location is subap center
  for i=0,ns-1 do begin
    roi[0,i] = center[0] + geom[0,i]*subap_pitch
    roi[1,i] = center[1] + geom[1,i]*subap_pitch
  endfor
  ; display the rois
  im_roi = fltarr(n,m)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    im_roi[x0:x1,y0] = 1
    im_roi[x0:x1,y1] = 1
    im_roi[x0,y0:y1] = 1
    im_roi[x1,y0:y1] = 1
  endfor
  im_roi = im_roi + im_pp/max(im_pp)
  ; calculate the refcents
  refcent = fltarr(2,ns)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    subap = im_pp[x0:x1,y0:y1]
    refcent[*,i] = centroid(subap) - float(subap_pitch)/2.
  endfor
end
pro hartmanngui_calculate_centroidsnow, Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  forward_function ztable,zernikeslope
  ; pre-process the data image
  x0 = subarray[0]
  y0 = subarray[1]
  n = subarray[2]
  m = subarray[3]
  im_pp = im_raw[x0:x0+n-1,y0:y0+m-1]
  h = histogram(im_pp,omax=im_max,omin=im_min)
  hmax = max(h)
  imax = !c
  i = imax
  while h[i] gt hmax/2. do i=i+1
  clip = im_min + imax + 3*(i-imax)
  im_pp = (im_pp > clip) - clip
  ; calculate the centroids
  ns = (size(geom))(2)
  cent = fltarr(2,ns)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    subap = im_pp[x0:x1,y0:y1]
    cent[*,i] = centroid(subap) - float(subap_pitch)/2.
  endfor
  cent = cent - refcent
  ; fit to zernikes
  nzern = 10
  ztab = ztable(nzern)
  x = geom[0,*]
  y = geom[1,*]
  r = 3.5 ; particular to Lick!!!
  zcoef = fltarr(nzern)
  for i=1,nzern do begin
    zs = zernikeslope(x,y,r,ztab[0,i],ztab[1,i])
    if ztab[2,i] eq 0 then begin
      zs = real(zs)
    endif else begin
      zs = imaginary(zs)
    endelse
    zcoef[i-1] = total(zs*cent)
  endfor
  ; display the zernike list
  zernikelist = widget_info(Event.top, FIND_BY_UNAME='zernikelist')
  zlist = strarr(nzern)
  zlist[0] = 'tip 1 1 x = ' + string(zcoef[0])
  zlist[1] = 'tilt 1 1 y = ' + string(zcoef[1])
  zlist[2] = 'focus 2 0  = ' + string(zcoef[2])
  zlist[3] = 'astig 2 2 x = ' + string(zcoef[3])
  zlist[4] = 'astig 2 2 y = ' + string(zcoef[4])
  zlist[5] = 'coma 3 1 x = ' + string(zcoef[5])
  zlist[6] = 'coma 3 1 y = ' + string(zcoef[6])
  zlist[7] = 'tricoma 3 3 x = ' + string(zcoef[7])
  zlist[8] = 'tricoma 3 3 y = ' + string(zcoef[8])
  zlist[9] = 'sphere 4 0 = ' + string(zcoef[9])
  widget_control,zernikelist,set_value=zlist
end
pro hartmanngui_display_image,Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  dispgui, im_pp, 'hartmann image'
end
pro hartmanngui_display_roisonimage,Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  ; display the rois
  n = (size(im_pp))(1)
  m = (size(im_pp))(2)
  ns = (size(geom))(2)
  im_roi = fltarr(n,m)
  for i=0,ns-1 do begin
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    im_roi[x0:x1,y0] = 1
    im_roi[x0:x1,y1] = 1
    im_roi[x0,y0:y1] = 1
    im_roi[x1,y0:y1] = 1
  endfor
  im_roi = im_roi + im_pp/max(im_pp)
  dispgui, im_roi, 'rois on image'
end
pro hartmanngui_display_dots,Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  n = (size(im_pp))(1)
  m = (size(im_pp))(2)
  ns = (size(cent))(2)
; remove tip/tilt
  tip = average(cent[0,*])
  tilt = average(cent[1,*])
  dispgui,im_pp*0,'centroids (arrows)',size=500
  for i=0,ns-1 do begin
; display the slope-lines
    x0 = roi[0,i]
    y0 = roi[1,i]
    x1 = roi[0,i] + gain*(cent[0,i]-tip)
    y1 = roi[1,i] + gain*(cent[1,i]-tilt)
    plots,0.8*[x1,x1]/float(n)+0.1,0.8*[y1,y1]/float(m)+0.1,/normal,psym=3
    plots,0.8*[x0,x0]/float(n)+0.1,0.8*[y0,y0]/float(m)+0.1,/normal,psym=1,color=150
    ;plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal,color=255
; display the rois
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    plots,0.8*[x0,x0]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal
    plots,0.8*[x1,x1]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal
    plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y0,y0]/float(m)+0.1,/normal
    plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y1,y1]/float(m)+0.1,/normal
  endfor
end
pro hartmanngui_display_arrows,Event
  common hartmann,im_raw,im_pp,im_roi,roi,refcent,geom, $
            geom_file, refcent_file, $
            subarray,platescale,subap_pitch,center, $
            cent,gain,zcoef,ddir
  n = (size(im_pp))(1)
  m = (size(im_pp))(2)
  ns = (size(cent))(2)
; remove tip/tilt
  tip = average(cent[0,*])
  tilt = average(cent[1,*])
  dispgui,im_pp*0,'centroids (arrows)',size=500
  for i=0,ns-1 do begin
; display the slope-lines
    x0 = roi[0,i]
    y0 = roi[1,i]
    x1 = roi[0,i] + gain*(cent[0,i]-tip)
    y1 = roi[1,i] + gain*(cent[1,i]-tilt)
    plots,0.8*[x1,x1]/float(n)+0.1,0.8*[y1,y1]/float(m)+0.1,/normal,psym=3
    plots,0.8*[x0,x0]/float(n)+0.1,0.8*[y0,y0]/float(m)+0.1,/normal,psym=1,color=150
    plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal,color=255
; display the rois
    x0 = round(roi[0,i] - subap_pitch/2.)
    x1 = round(x0 + subap_pitch)
    y0 = round(roi[1,i] - subap_pitch/2.)
    y1 = round(y0 + subap_pitch)
    plots,0.8*[x0,x0]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal
    plots,0.8*[x1,x1]/float(n)+0.1,0.8*[y0,y1]/float(m)+0.1,/normal
    plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y0,y0]/float(m)+0.1,/normal
    plots,0.8*[x0,x1]/float(n)+0.1,0.8*[y1,y1]/float(m)+0.1,/normal
  endfor
end
