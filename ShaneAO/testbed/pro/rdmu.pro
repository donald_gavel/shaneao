Pro Rdmu, Image, cx,cy,rx,ry,th,selat,selon,sslat,sslon
           		;Read the value of the pixel under the cursor
			;Display x,y and the pixel value under the cursor,
			;planetocentric latitude and longitude,
                        ;mu (observer angle cosine), and mu0 (sun angle cosine)
;+
; NAME:
;	RDMU
;
; PURPOSE:
;	Interactively display the X position, Y position, pixel value 
;       planet latitude, longitude, observer and sun angle for the point
;	of the cursor.
;
; CATEGORY:
;	Image display.
;
; CALLING SEQUENCE:
;	RDMU, Image , cx,cy, rx,ry, th, selat,selon,sslat,sslon
;
; INPUTS:
;	Image:	The array that represents the image being displayed.  This 
;		array may be of any type.  Rather reading pixel values from 
;		the display, they are taken from this parameter, avoiding 
;		scaling difficulties.
;	cx,cy:  Center of planet
;	rx,ry:  Major and minor radii of planet
;	th:     Tilt angle of planet, ccw
;       selat:  sub-earth latitude
;       selon:  sub-earth longitude
;       sslat:  sub-sun latitude
;       sslon:  sub-sun longitude
;
; OUTPUTS:
;	None.
;
; COMMON BLOCKS:
;	None.
;
; SIDE EFFECTS:
;	The X, Y, and value of the pixel under the cursor are continuously 
;	displayed.
;
; RESTRICTIONS:
;	None.
;
; PROCEDURE:
;	Instructions are printed and the pixel values are printed as the 
;	cursor is moved over the image.
;
;	Press the left or center mouse button to create a new line of output,
;	saving the previous line.
;
;	Press the right mouse button to exit the procedure.
;
; EXAMPLE USAGE:
;       j = readfits('/lgs1/lick_jul94/jul_19/jul19j31_image.fits',hdr)
;       j3 = rebin(j,300,300)
;       disp,j3
;       annotatefits,hdr
;       rx = (38.2/(3*0.063))/2.
;       selat = getkeyval(hdr,'SELAT')
;       selon = getkeyval(hdr,'SELON')
;       sslat = getkeyval(hdr,'SSLAT')
;       sslon = getkeyval(hdr,'SSLON')
;       rdmu,j3,150,150,rx,rx*1.069,16.,selat,selon,sslat,sslon
;
; MODIFICATION HISTORY:
;	9/6/97 created using rdpix.pro as a boilerplate
;
;-

on_error,2              ;Return to caller if an error occurs
print,'Press left or center mouse button for new output line."
print,'... right mouse button to exit.'
s = size(image)
if s(0) ne 2 then message, 'Image parameter not 2d.'
s(1) = s(1)-1		;To n-1
s(2) = s(2)-1
!err=0
if n_elements(x0) le 0 then x0 = 0
if n_elements(y0) le 0 then y0 = 0
if s(s(0)+1) ge 4 then form = 'F' else form = 'I'
cr = string("15b)	;this codes a newline
form1="($,'x=',i4,', y=',i4,', value=',"+form+",a)"
form="($,'x=',i4,', y=',i4,', value=',"+form+", ' lat=',f6.2, ' long=',f6.2, ' mu=',f5.2, ' mu0=',f5.2,a)"
while !err ne 4 do begin
	tvrdc,x,y,2,/dev
	if (!err and 3) ne 0 then begin	;New line?
	   print,form="($,a)",string("12b)
	   while (!err ne 0) do begin wait,.1 & tvrdc,x,y,0,/dev & end
	  endif

	x = x-x0 & y = y - y0
	if (x le s(1)) and (y le s(2)) and (x ge 0) and (y ge 0) then begin
	   if (!order eq 1) then yy = s(2) - y else yy = y
	   latlon = planetcoords(x,y,cx,cy,rx,ry,selat,selon,th,x1,y1,z1)
;           print,latlon
	   if ( latlon(0) ne -99 ) then begin
	     mumu = mu(x1,y1,z1,rx,ry,selat,selon,sslat,sslon)
	     print,form = form, x,y,Image(x,yy),latlon(0),latlon(1),mumu(0),mumu(1),cr
	   endif else begin
	     print,form = form, x,y,Image(x,yy),-10000.,-10000.,0.,0.,cr
	   endelse
	endif
endwhile
print,form="(/)"
end

;  example code (compiled as "main")
 j = readfits('/lgs1/lick_jul94/jul_19/jul19j31_image.fits',hdr)
 j3 = rebin(j,300,300)
 disp,j3
 cx = 150
 cy = 150
 rx = (38.2/(3*0.063))/2.
 ry = rx/1.069
 rx = rx*1.05
 ry = ry*1.05
 cx = cx-3
 th = 16.
 selat = getkeyval(hdr,'SELAT')
 selon = getkeyval(hdr,'SELON')
 sslat = getkeyval(hdr,'SSLAT')
 sslon = getkeyval(hdr,'SSLON')
 e = check_ellipse(j3,rx,ry,cx,cy,th)*max(j3)/2
 tvscl,e+j3
 annotatefits,hdr
 rdmu,j3,cx,cy,rx,ry,th,selat,selon,sslat,sslon
end
