;
; Auto Save File For ./specklegui.pro
;
;  Wed Oct  1 23:14:13 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13

pro initspecklegui,entryset,dataset
  widget_control,entryset.datadirectory,set_value = dataset.datadir
  widget_control,entryset.resultsdirectory,set_value = dataset.resultsdir
  widget_control,entryset.objectbasename,set_value = dataset.basename
  widget_control,entryset.objectdatafile,set_value = dataset.datafile
  widget_control,entryset.objectdarkfile ,set_value = dataset.darkfile
  widget_control,entryset.objectfrom ,set_value = 0
  widget_control,entryset.objectto ,set_value = 99
  widget_control,entryset.objectskyfile ,set_value = dataset.skyfile
  widget_control,entryset.objectskydarkfile ,set_value = dataset.skydarkfile
  widget_control,entryset.objectxmin ,set_value = dataset.backgroundarea_x(0)
  widget_control,entryset.objectxmax ,set_value = dataset.backgroundarea_x(1)
  widget_control,entryset.objectymin ,set_value = dataset.backgroundarea_y(0)
  widget_control,entryset.objectymax ,set_value = dataset.backgroundarea_y(1)
  widget_control,entryset.starbasename ,set_value = dataset.starname
  widget_control,entryset.stardatafile ,set_value = dataset.starfile
  widget_control,entryset.stardarkfile ,set_value = dataset.stardarkfile
  widget_control,entryset.starfrom ,set_value = 0
  widget_control,entryset.starto ,set_value = 99
  widget_control,entryset.starskyfile ,set_value = dataset.skyfile
  widget_control,entryset.starskydarkfile ,set_value = dataset.skydarkfile
  widget_control,entryset.starxmin ,set_value = dataset.backgroundarea_x(0)
  widget_control,entryset.starxmax ,set_value = dataset.backgroundarea_x(1)
  widget_control,entryset.starymin ,set_value = dataset.backgroundarea_y(0)
  widget_control,entryset.starymax ,set_value = dataset.backgroundarea_y(1)
  widget_control,entryset.mtffile ,set_value = dataset.mtffile
  widget_control,entryset.badpixfile ,set_value = ''
  widget_control,entryset.flatdatafile ,set_value = dataset.flatfile
  widget_control,entryset.flatdarkfile ,set_value = dataset.flatdarkfile
end

pro readbackspecklegui,entryset,dataset
  widget_control,entryset.datadirectory,get_value = v
  dataset.datadir = v(0)
  widget_control,entryset.resultsdirectory,get_value = v
  dataset.resultsdir = v(0)
  widget_control,entryset.objectbasename,get_value = v
  dataset.basename = v(0)
  widget_control,entryset.objectdatafile,get_value = v
  dataset.datafile = v(0)
  widget_control,entryset.objectdarkfile ,get_value = v
  dataset.darkfile = v(0)
;  widget_control,entryset.objectfrom ,get_value = 0
;  widget_control,entryset.objectto ,get_value = 99
  widget_control,entryset.objectskyfile ,get_value = v
  dataset.skyfile = v(0)
  widget_control,entryset.objectskydarkfile ,get_value = v
  dataset.skydarkfile = v(0)
  widget_control,entryset.objectxmin ,get_value = v
  dataset.backgroundarea_x(0) = v(0)
  widget_control,entryset.objectxmax ,get_value = v
  dataset.backgroundarea_x(1) = v(0)
  widget_control,entryset.objectymin ,get_value = v
  dataset.backgroundarea_y(0) = v(0)
  widget_control,entryset.objectymax ,get_value = v
  dataset.backgroundarea_y(1) = v(0)
  widget_control,entryset.starbasename ,get_value = v
  dataset.starname = v(0)
  widget_control,entryset.stardatafile ,get_value = v
  dataset.starfile = v(0)
  widget_control,entryset.stardarkfile ,get_value = v
  dataset.stardarkfile = v(0)
;  widget_control,entryset.starfrom ,get_value = 0
;  widget_control,entryset.starto ,get_value = 99
  widget_control,entryset.starskyfile ,get_value = v
;  dataset.skyfile = v(0)
  widget_control,entryset.starskydarkfile ,get_value = v
;  dataset.skydarkfile = v(0)
  widget_control,entryset.starxmin ,get_value = v
  dataset.backgroundarea_x(0) = v(0)
  widget_control,entryset.starxmax ,get_value = v
  dataset.backgroundarea_x(1) = v(0)
  widget_control,entryset.starymin ,get_value = v
  dataset.backgroundarea_y(0) = v(0)
  widget_control,entryset.starymax ,get_value = v
  dataset.backgroundarea_y(1) = v(0)
  widget_control,entryset.mtffile ,get_value = v
  dataset.mtffile = v(0)
;  widget_control,entryset.badpixfile ,get_value = ''
  widget_control,entryset.flatdatafile ,get_value = v
  dataset.flatfile = v(0)
  widget_control,entryset.flatdarkfile ,get_value = v
  dataset.flatdarkfile = v(0)
end

;PRO MAIN13_Event, Event
PRO Specklegui_Event, Event

  forward_function getTitanDatasetDef, getDefDatasetList, makeskyframe

  common speckleguicom, state
 
  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  widget_control,Event.Top,Get_uvalue = uv
  state = uv.state
  entry = uv.entry

  CASE Ev OF 

  'FIELD227': BEGIN
;      Print, 'Event for Data directory: '
      END
  'FIELD229': BEGIN
;      Print, 'Event for Results directory:'
      END
; -------------------------------------- OBJECT -----------------------------------------
  'FIELD102': BEGIN
;      Print, 'Event for Basename (object)'
      END
  'FIELD106': BEGIN
;      Print, 'Event for Datafile (object):'
; read data from disk
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.objectdatafile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if fileexists(datadir + datafile) then begin
        if handle_info(state.objdata,/valid_id) then handle_free,state.objdata
        widget_control,/hourglass
        thedata = readfits(datadir + datafile)
        state.objdata = handle_create(value = thedata,/no_copy)
      endif else begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endelse
      END
  'FIELD104': BEGIN
;      Print, 'Event for Darkfile (object):'
; read data from disk
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.objectdarkfile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if fileexists(datadir + datafile) then begin
        if handle_info(state.objdarkdata,/valid_id) then handle_free,state.objdarkdata
        widget_control,/hourglass
        thedata = readfits(datadir + datafile)
        state.objdarkdata = handle_create(value = thedata,/no_copy)
      endif else begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endelse
      END
  'FIELD111': BEGIN
;      Print, 'Event for from:'
      END
  'FIELD113': BEGIN
;      Print, 'Event for to:'
      END
  'FIELD59': BEGIN
;      Print, 'Event for Sky file (object):'
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.objectskyfile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if not fileexists(datadir + datafile) then begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endif
      END
  'FIELD60': BEGIN
;      Print, 'Event for Sky Dark file:'
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.objectskydarkfile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if not fileexists(datadir + datafile) then begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endif
      END
  'FIELD62': BEGIN
;      Print, 'Event for xmin: '
      END
  'FIELD63': BEGIN
;      Print, 'Event for xmax: '
      END
  'FIELD64': BEGIN
;      Print, 'Event for ymin: '
      END
  'FIELD65': BEGIN
;      Print, 'Event for ymax: '
      END
  'BUTTON116a': begin
;      Print, 'Event for reset (object)'
      handle_value,state.dataset,dataset,/no_copy
      if handle_info(state.objdata,/valid_id) then begin
        handle_free,state.objdata
        state.objdata = 0
      end
      if handle_info(state.objdarkdata,/valid_id) then begin
        handle_free,state.objdarkdata
        state.objdarkdata = 0
      end
      if handle_info(state.objmdark,/valid_id) then begin
        handle_free,state.objmdark
        state.objmdark = 0
      end
      if handle_info(state.skyframe,/valid_id) then begin
        handle_free,state.skyframe
        state.skyframe = 0
      end
      handle_value,state.dataset,dataset,/no_copy,/set
      end
  'BUTTON116b' : begin
;      Print, 'Event for options (object)'
      handle_value,state.dataset,dataset,/no_copy
      opts = state.options
      speckleOptionsGUI,opts
      state.options = opts
      handle_value,state.dataset,dataset,/no_copy,/set
      end
  'BUTTON116': BEGIN
;      Print, 'Event for Quicklook'
; build the object dataset from the entries
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset

; if object-dark does not exist, compute it

      if handle_info(state.objmdark,/valid_id) then begin

        handle_value,state.objmdark,objmdark,/no_copy

      endif else begin
; read raw data from disk if necessary
        rfd = 0
        if handle_info(state.objdata,/valid_id) then begin
          handle_value,state.objdata,objdata,/no_copy
        endif else begin
          state.objdata = handle_create()
          rfd = 1
        endelse

        if handle_info(state.objdarkdata,/valid_id) then begin
          handle_value,state.objdarkdata,objdarkdata,/no_copy
        endif else begin
          state.objdarkdata = handle_create()
          rfd = 1
        endelse

        widget_control,/hourglass
        darksubtract,dataset,objdata,objdarkdata,objmdark,readdatafromdisk=rfd
        handle_value,state.objdata,objdata,/no_copy,/set
        handle_value,state.objdarkdata,objdarkdata,/no_copy,/set
 
        state.objmdark = handle_create()

      endelse

;      theshow = objmdark(*,*,1)

; show the dark subtracted object data
;      window,0,xsize=256,ysize=256
;      exptv,theshow,/data,/nobox,/noexact
;      dispgui,theshow,'dark-subtracted ' + dataset.basename
;      dispgui,objmdark,'dark-subtracted ' + dataset.basename

;      h = handle_create(value = objmdark,/no_copy)
;      dispgui,h,'dark-subtracted ' + dataset.basename
;      handle_value,h,objmdark,/no_copy
;      handle_free,h

; stuff the results back into the state
      handle_value,state.objmdark,objmdark,/no_copy,/set
;  and display
      dispgui,state.objmdark,'dark-subtracted ' + dataset.basename
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON120': BEGIN
;      Print, 'Event for Go (object)'
; do all the preprocessing
;   first: check for availability of all data
      option = state.options
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      widget_control,/hourglass

      if handle_info(state.objmdark,/valid_id) eq 0 then begin
        if handle_info(state.objdata,/valid_id) eq 0 then begin
          if fileexists(dataset.datadir + dataset.datafile) then begin
            objdata = readfits(dataset.datadir + dataset.datafile)
            state.objdata = handle_create(value = objdata,/no_copy)
          endif else begin
            res = widget_message(['file not found', $
                        dataset.datadir + dataset.datafile],/error)
            goto, out
          endelse
        endif
        if handle_info(state.objdarkdata,/valid_id) eq 0 then begin
          if fileexists(dataset.datadir + dataset.darkfile) then begin
            objdarkdata = readfits(dataset.datadir + dataset.darkfile)
            state.objdarkdata = handle_create(value = objdarkdata,/no_copy)
          endif else begin
            res = widget_message(['file not found', $
                        dataset.datadir + dataset.darkfile],/error)
            goto, out
          endelse
        endif
        handle_value,state.objdata,objdata,/no_copy
        handle_value,state.objdarkdata,objdarkdata,/no_copy
        darksubtract,dataset,objdata,objdarkdata,objmdark
        handle_value,state.objdata,objdata,/no_copy,/set
        handle_value,state.objdarkdata,objdarkdata,/no_copy,/set
        state.objmdark = handle_create(value = objmdark,/no_copy)
      endif

      if ((option.debad) and (handle_info(state.badpix,/valid_id) eq 0)) then begin
        res = widget_message('need to load badpix',/error)
        goto, out
      endif

      if option.debad then handle_value,state.badpix,badpix,/no_copy

      if ((option.flatField) and (handle_info(state.flatfield,/valid_id) eq 0)) then begin
        print,'flat-fielding'
        if fileexists(dataset.datadir + dataset.flatfile) then begin
          flat = makeflat(dataset,badpix)
        endif else begin
;          res = widget_message('no flat field, continue?',/question)
          res = widget_message('no flat field, will continue anyway',/error)
          flat = badpix*0 + 1.0
        endelse
        state.flatfield = handle_create(value = flat,/no_copy)
      endif

      print, 'option.removeSky = ',option.removeSky, $
          ' state.skyframe valid = ', handle_info(state.skyframe,/valid_id)
      if ((option.removeSky) and (handle_info(state.skyframe,/valid_id) eq 0)) then begin
        skyframe = makeskyframe(dataset,badpix,/readdatafromdisk)
        state.skyframe = handle_create(value = skyframe,/no_copy)
      endif

      handle_value,state.objmdark,objmdark,/no_copy
      if option.removeSky then handle_value,state.skyframe,skyframe,/no_copy
      if option.flatField then handle_value,state.flatfield,flat,/no_copy
;   then: doit
; -------------- main do-it code --------------
      print,'<specklegui> starting the preprocessing - object data'

      if option.debad then debad,objmdark,tmp,badpix else begin
        tmp = objmdark
        print,'NOT doing bad pixel removal'
      endelse
      
      if option.removeSky then remsky, tmp, skyframe, tmp $
        else print,'NOT removing sky'
      
      if option.flatField then begin
        print,'flat-fielding...'
        n = (size(tmp))(3)
        for i=0,n-1 do begin
          tmp(*,*,i) = tmp(*,*,i)/(flat>.5)
          print,format='($,I0," ")',i
        endfor
        print,'done'
      endif else print, 'NOT flat-fielding'

      if option.centroidShift then cshift, tmp, tmp else print, 'NOT centroid shifting'

      if option.deVstripe then devstripe, tmp, tmp, dataset.backgroundarea_y $
        else print,'NOT removing V stripes'
      if option.deHstripe then dehstripe, tmp, tmp, dataset.backgroundarea_x $
        else print,'NOT removing H stripes'

      if option.window then begin
        print,'% supergauss - mod2 DG 10/2/97'
        print,'supergaussian window...'
        sw = supergauss(256,128,128,100,10)
        fuzz = 10.^(-11)
        for i=0,n-1 do begin
          tmp(*,*,i) = (tmp(*,*,i)>fuzz)*sw
          print,format='($,I0," ")',i
        endfor
        print,'done'
      endif else print, 'NOT windowing'

      print,'<specklegui> finished preprocessing object'
; ---------------------------------------------
; put the pre-processed data into the state
      if handle_info(state.speckledata,/valid_id) then $
         handle_free,state.speckledata
      state.speckledata = handle_create(value = tmp,/no_copy)
; return other stuff to the state
      handle_value,state.objmdark,objmdark,/no_copy,/set
      if option.debad then handle_value,state.badpix,badpix,/no_copy,/set
      if option.removeSky then handle_value,state.skyframe,skyframe,/no_copy,/set
      if option.flatField then handle_value,state.flatfield,flat,/no_copy,/set

      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON244': BEGIN
;      Print, 'Event for Look at object specklefile'
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      if handle_info(state.speckledata,/valid_id) eq 0 then begin
;      ---  try to read from the speckle file
        Print,dataset.resultsdir + dataset.basename + '.spec: ', $
            fileexists(dataset.resultsdir + dataset.basename + '.spec')
        if fileexists(dataset.resultsdir + dataset.basename + '.spec') then begin
          res = widget_message('read from disk?',/question)
          if res eq 'Yes' then begin
            sfs = specfilesize(dataset.resultsdir + dataset.basename + '.spec')
            speckledata = rdspec(dataset.resultsdir + dataset.basename + '.spec',sfs(2))
            state.speckledata = handle_create(value = speckledata,/no_copy)
          endif else begin
            goto, out
          endelse
        endif else begin
          res = widget_message('use GO to compute preprocessed speckle data first',/error)
          goto, out
        endelse
      endif
;      handle_value,state.speckledata,speckledata,/no_copy
;      window,0,xsize=256,ysize=256
;      exptv,speckledata(*,*,0),/data,/nobox,/noexact
;      dispgui,speckledata(*,*,0),'pre-processed ' + dataset.basename
;      dispgui,speckledata,'pre-processed ' + dataset.basename

;      h = handle_create(value = speckledata,/no_copy)
;      dispgui,h,'pre-processed ' + dataset.basename
;      handle_value,h,speckledata,/no_copy
;      handle_free,h

;      handle_value,state.speckledata,speckledata,/no_copy,/set
      dispgui,state.speckledata,'pre-processed ' + dataset.basename
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON122': BEGIN
;      Print, 'Event for Save object specklefile'
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      if handle_info(state.speckledata,/valid_id) eq 0 then begin
        res = widget_message('use GO to compute preprocessed speckle data first',/error)
        goto, out
      endif
      handle_value,state.speckledata,speckledata,/no_copy
      specklefile = dataset.resultsdir + dataset.basename + '.spec'
      res = 'Yes'
      if fileexists(specklefile) then begin
        res = widget_message(['overwrite existing file',specklefile+'?'],/question)
      endif
      if res eq 'Yes' then begin
        widget_control,/hourglass
        print,'writing to ',specklefile,'...'
        wrspec, specklefile, fix(speckledata>0)
        print,'done'
      endif
      handle_value,state.speckledata,speckledata,/no_copy,/set
      handle_value,state.dataset,dataset,/no_copy,/set
      END
; -------------------------------------- STAR -----------------------------------------
  'FIELD166': BEGIN
;      Print, 'Event for Basename (star)'
      END
  'FIELD169': BEGIN
;      Print, 'Event for Datafile (star):'
; read data from disk
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.stardatafile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if fileexists(datadir + datafile) then begin
        if handle_info(state.stardata,/valid_id) then handle_free,state.stardata
        widget_control,/hourglass
        thedata = readfits(datadir + datafile)
        state.stardata = handle_create(value = thedata,/no_copy)
      endif else begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endelse
      END
  'FIELD170': BEGIN
;      Print, 'Event for Darkfile (star):'
; read data from disk
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.stardarkfile,get_value = darkfile
      datadir = datadir(0)
      darkfile = darkfile(0)

      if fileexists(datadir + darkfile) then begin
        if handle_info(state.stardarkdata,/valid_id) then handle_free,state.stardarkdata
        widget_control,/hourglass
        thedata = readfits(datadir + darkfile)
        state.stardarkdata = handle_create(value = thedata,/no_copy)
      endif else begin
        res = widget_message(['file not found',datadir + darkfile],/error)
      endelse
      END
  'FIELD172': BEGIN
;      Print, 'Event for from:'
      END
  'FIELD173': BEGIN
;      Print, 'Event for to:'
      END
  'FIELD174': BEGIN
;      Print, 'Event for Sky file (star):'
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.starskyfile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if not fileexists(datadir + datafile) then begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endif
      END
  'FIELD175': BEGIN
;      Print, 'Event for Sky Dark file (star):'
      widget_control,entry.datadirectory,get_value = datadir
      widget_control,entry.starskydarkfile,get_value = datafile
      datadir = datadir(0)
      datafile = datafile(0)

      if not fileexists(datadir + datafile) then begin
        res = widget_message(['file not found',datadir + datafile],/error)
      endif
      END
  'FIELD177': BEGIN
;      Print, 'Event for xmin: '
      END
  'FIELD178': BEGIN
;      Print, 'Event for xmax: '
      END
  'FIELD179': BEGIN
;      Print, 'Event for ymin: '
      END
  'FIELD180': BEGIN
;      Print, 'Event for ymax: '
      END
  'BUTTON181a': begin
;      Print, 'Event for reset (star)'
      handle_value,state.dataset,dataset,/no_copy
      if handle_info(state.stardata,/valid_id) then begin
        handle_free,state.stardata
        state.stardata = 0
      end
      if handle_info(state.stardarkdata,/valid_id) then begin
        handle_free,state.stardarkdata
        state.stardarkdata = 0
      end
      if handle_info(state.starmdark,/valid_id) then begin
        handle_free,state.starmdark
        state.starmdark = 0
      end
      if handle_info(state.starskyframe,/valid_id) then begin
        handle_free,state.starskyframe
        state.starskyframe = 0
      end
      handle_value,state.dataset,dataset,/no_copy,/set
      end
  'BUTTON181b' : begin
;      Print, 'Event for options (star)'
      handle_value,state.dataset,dataset,/no_copy
      opts = state.staroptions
      speckleOptionsGUI,opts
      state.staroptions = opts
      handle_value,state.dataset,dataset,/no_copy,/set
      end
  'BUTTON181': BEGIN
;      Print, 'Event for Quicklook: Star'
; build the star dataset from the entries
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset

; if star-dark does not exist, compute it

      if handle_info(state.starmdark,/valid_id) then begin

        handle_value,state.starmdark,starmdark,/no_copy

      endif else begin
; read raw data from disk if necessary
        rfd = 0
        if handle_info(state.stardata,/valid_id) then begin
          handle_value,state.stardata,stardata,/no_copy
        endif else begin
          state.stardata = handle_create()
          rfd = 1
        endelse

        if handle_info(state.stardarkdata,/valid_id) then begin
          handle_value,state.stardarkdata,stardarkdata,/no_copy
        endif else begin
          state.stardarkdata = handle_create()
          rfd = 1
        endelse

        widget_control,/hourglass
        darksubtract,{datadir:dataset.datadir, $
                      datafile:dataset.starfile, $
                      darkfile: dataset.stardarkfile}, $
                stardata,stardarkdata,starmdark,readdatafromdisk=rfd
        handle_value,state.stardata,stardata,/no_copy,/set
        handle_value,state.stardarkdata,stardarkdata,/no_copy,/set
 
        state.starmdark = handle_create()

      endelse

;      theshow = starmdark(*,*,1)

; show the dark subtracted object data
;      window,0,xsize=256,ysize=256
;      exptv,theshow,/data,/nobox,/noexact
;      dispgui,theshow,'dark-subtracted ' + dataset.starname

;      h = handle_create(value = starmdark,/no_copy)
;      dispgui,h,'dark-subtracted ' + dataset.starname
;      handle_value,h,starmdark,/no_copy
;      handle_free,h

; stuff the results back into the state and display
      handle_value,state.starmdark,starmdark,/no_copy,/set
      dispgui,state.starmdark,'dark-subtracted ' + dataset.starname
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON182': BEGIN
;      Print, 'Event for Go (star)'
; do all the preprocessing
;   first: check for availability of all data
      option = state.staroptions
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      widget_control,/hourglass

      if handle_info(state.starmdark,/valid_id) eq 0 then begin
        if handle_info(state.stardata,/valid_id) eq 0 then begin
          if fileexists(dataset.datadir + dataset.starfile) then begin
            stardata = readfits(dataset.datadir + dataset.starfile)
            state.stardata = handle_create(value = stardata,/no_copy)
          endif else begin
            res = widget_message(['file not found', $
                        dataset.datadir + dataset.starfile],/error)
            goto, out
          endelse
        endif
        if handle_info(state.stardarkdata,/valid_id) eq 0 then begin
          if fileexists(dataset.datadir + dataset.stardarkfile) then begin
            stardarkdata = readfits(dataset.datadir + dataset.stardarkfile)
            state.stardarkdata = handle_create(value = stardarkdata,/no_copy)
          endif else begin
            res = widget_message(['file not found', $
                        dataset.datadir + dataset.stardarkfile],/error)
            goto, out
          endelse
        endif
        handle_value,state.stardata,stardata,/no_copy
        handle_value,state.stardarkdata,stardarkdata,/no_copy
        darksubtract,{datadir:dataset.datadir, $
                      datafile:dataset.starfile, $
                      darkfile: dataset.stardarkfile}, $
                 stardata,stardarkdata,starmdark
        handle_value,state.stardata,stardata,/no_copy,/set
        handle_value,state.stardarkdata,stardarkdata,/no_copy,/set
        state.starmdark = handle_create(value = starmdark,/no_copy)
      endif

      if ((option.debad) and (handle_info(state.badpix,/valid_id) eq 0)) then begin
        res = widget_message('need to load badpix',/error)
        goto, out
      endif

      if option.debad then handle_value,state.badpix,badpix,/no_copy

      if ((option.flatField) and (handle_info(state.flatfield,/valid_id) eq 0)) then begin
        print,'flat-fielding'
        if fileexists(dataset.datadir + dataset.flatfile) then begin
          flat = makeflat(dataset,badpix)
        endif else begin
;          res = widget_message('no flat field, continue?',/question)
          res = widget_message('no flat field, will continue anyway',/error)
          flat = badpix*0 + 1.0
        endelse
        state.flatfield = handle_create(value = flat,/no_copy)
      endif

      if ((option.removeSky) and (handle_info(state.starskyframe,/valid_id) eq 0)) then begin
        skyframe = makeskyframe(dataset,badpix,/readdatafromdisk)
        state.starskyframe = handle_create(value = skyframe,/no_copy)
      endif

      handle_value,state.starmdark,starmdark,/no_copy
      if option.removeSky then handle_value,state.starskyframe,skyframe,/no_copy
      if option.flatField then handle_value,state.flatfield,flat,/no_copy
;   then: doit
; -------------- main do-it code --------------
      print,'<specklegui> starting the preprocessing - star data'

      if option.debad then debad,starmdark,tmp,badpix else begin
        tmp = starmdark
        print, 'NOT dogin bad pixel removal'
      endelse

      if option.removeSky then remsky, tmp, skyframe, tmp $
        else print,'NOT removing sky'

      if option.flatField then begin
        print,'flat-fielding...'
        n = (size(tmp))(3)
        for i=0,n-1 do begin
          tmp(*,*,i) = tmp(*,*,i)/(flat>.5)
          print,format='($,I0," ")',i
        endfor
        print,'done'
      endif else print, 'NOT flat-fielding'

      if option.centroidShift then cshift, tmp, tmp else print, 'NOT centroid shifting'

      if option.deVstripe then devstripe, tmp, tmp, dataset.backgroundarea_y $
        else print,'NOT removing V stripes'
      if option.deHstripe then dehstripe, tmp, tmp, dataset.backgroundarea_x $
        else print,'NOT removing H stripes'

      if option.window then begin
        print,'% supergauss - mod2 DG 10/2/97'
        print,'supergaussian window...'
        sw = supergauss(256,128,128,100,10)
        sw(0:5,*) = 0; TEMPORARY
        sw(251:255,*) = 0; TEMPORARY
        sw(*,0:5) = 0; TEMPORARY
        sw(*,251:255) = 0; TEMPORARY
        for i=0,n-1 do begin
          tmp(*,*,i) = tmp(*,*,i)*sw
          print,format='($,I0," ")',i
        endfor
        print,'done'
      endif else print, 'NOT windowing'

      print,'<specklegui> finished preprocessing star'
; ---------------------------------------------
; put the pre-processed data into the state
      if handle_info(state.specklestar,/valid_id) then $
         handle_free,state.specklestar
      state.specklestar = handle_create(value = tmp,/no_copy)
; return other stuff to the state
      handle_value,state.starmdark,starmdark,/no_copy,/set
      if option.debad then handle_value,state.badpix,badpix,/no_copy,/set
      if option.removeSky then handle_value,state.starskyframe,skyframe,/no_copy,/set
      if option.flatField then handle_value,state.flatfield,flat,/no_copy,/set

      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON246': BEGIN
;      Print, 'Event for Look at star specklefile'
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      if handle_info(state.specklestar,/valid_id) eq 0 then begin
;      ---  try to read from the speckle file
        if fileexists(dataset.resultsdir + dataset.starname + '.spec') then begin
          res = widget_message('read from disk?',/question)
          if res eq 'Yes' then begin
            sfs = specfilesize(dataset.resultsdir + dataset.starname + '.spec')
            specklestar = rdspec(dataset.resultsdir + dataset.starname + '.spec',sfs(2))
            state.specklestar = handle_create(value = specklestar,/no_copy)
          endif
        endif else begin
          res = widget_message('use GO to compute preprocessed speckle data first',/error)
          goto, out
        endelse
      endif
;      handle_value,state.specklestar,specklestar,/no_copy
;      window,0,xsize=256,ysize=256
;      exptv,speckledata(*,*,0),/data,/nobox,/noexact
;      dispgui,specklestar,'pre-processed ' + dataset.starname

;      h = handle_create(value = specklestar,/no_copy)
;      dispgui,h,'pre-processed ' + dataset.starname
;      handle_value,h,specklestar,/no_copy
;      handle_free,h

;      handle_value,state.specklestar,specklestar,/no_copy,/set
      dispgui,state.specklestar,'pre-processed ' + dataset.starname
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON183': BEGIN
;      Print, 'Event for Save star specklefile'
      handle_value,state.dataset,dataset,/no_copy
      if handle_info(state.specklestar,/valid_id) eq 0 then begin
        res = widget_message('use GO to compute preprocessed speckle data first',/error)
        goto, out
      endif
      handle_value,state.specklestar,specklestar,/no_copy
      specklefile = dataset.resultsdir + dataset.starname + '.spec'
      res = 'Yes'
      if fileexists(specklefile) then begin
        res = widget_message(['overwrite existing file',specklefile+'?'],/question)
      endif
      if res eq 'Yes' then begin
        widget_control,/hourglass
        print,'writing to ',specklefile,'...'
        wrspec, specklefile, fix(specklestar>0)
        print,'done'
      endif
      handle_value,state.specklestar,specklestar,/no_copy,/set
      handle_value,state.dataset,dataset,/no_copy,/set
      END
; -------------------------------------------------------------------------------------
  'FIELD188': BEGIN
;      Print, 'Event for mtf file:'
      END
  'FIELD189': BEGIN
;      Print, 'Event for wavelength:'
      END
  'BUTTON190': BEGIN
;      Print, 'Event for Load'
      END
  'BUTTON191': BEGIN
;      Print, 'Event for Save'
      END
  'BUTTON192': BEGIN
;      Print, 'Event for Make'
      END
  'FIELD195': BEGIN
;      Print, 'Event for badpixel file:'
      END
  'BUTTON196': BEGIN
;      Print, 'Event for Load'
; load the bad pixel file
      handle_value,state.dataset,dataset,/no_copy
      filename = pickfile(path = dataset.resultsdir, filter = '*.fits')
      badpix = readfits(filename)
      if (size(badpix))(0) ne 0 then begin
        if handle_info(state.badpix,/valid_id) then handle_free,state.badpix
        state.badpix = handle_create(value = badpix,/no_copy)
      endif
      widget_control,entry.badpixfile,set_value = filename
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON197': BEGIN
      Print, 'Save'
      Print, '--Not implemented yet--'
      END
  'BUTTON198': BEGIN
      Print, 'Make'
      Print, '--Not implemented yet--'
      END
  'FIELD201': BEGIN
;      Print, 'Event for FlatData file:'
      END
  'FIELD202': BEGIN
;      Print, 'Event for FlatDark file:'
      END
  'BUTTON235': BEGIN
      Print, 'Make'
      Print, '--Not implemented yet--'
      END

  'button206a': BEGIN
;      Print, 'Event for Load parameters'
      if fileexists(!home+'pro/default.database') then begin
        ds = specklelistgui(!home+'pro/default.database')
      endif else begin
        res = widget_message(['file not found', $
                     !home+'pro/default.database' ],/error)
        ds = -1
      endelse
      if (size(ds))(0) ne 0 then begin; otherwise, it was a cancel
        dataset = ds
        initspecklegui,entry,dataset
        clearstate,state
        if handle_info(state.dataset,/valid_id) then handle_free,state.dataset
        state.dataset = handle_create(value = dataset,/no_copy)
      endif
      END
  'button206b': BEGIN
;      Print, 'Event for Save'
      handle_value,state.dataset,dataset,/no_copy
      readbackspecklegui,entry,dataset
      printstruct,dataset,/all
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON206': BEGIN
;      Print, 'Event for  -> Speckle Processing'
      specklegui2, state
      END
  'BUTTON207': BEGIN
;      Print, 'Event for Done'
      widget_control,event.top,/destroy
      return
      END
  ENDCASE
 out:
  if n_elements(dataset) ne 0 then $
    handle_value,state.dataset,dataset,/set,/no_copy
  uv.state = state
  widget_control,event.top,set_uvalue = uv

  return

END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO specklegui,thestate, dataset=dataset, GROUP=Group, help=helpa


  common speckleguicom, state

  if (keyword_set(helpa)) then begin
    print,'usage: specklegui,state,dataset=dataset'
    return
  endif

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE={name:'MAIN13'})

  LABEL3 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL3', $
      FONT='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      VALUE='Speckle Imaging')

  LABEL238 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL238', $
      FONT='-adobe-times-medium-i-normal--14-140-75-75-p-74-iso8859-1', $
      VALUE='Setup & Preprocessing')

  BASE225 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='data and results directories', $
      UVALUE='BASE225')

  FieldVal16160 = [ $
    '' ]
  FIELD227 = CW_FIELD( BASE225,VALUE=FieldVal16160, $
      ROW=1, $
      STRING=1, $
      TITLE='Data directory: ', $
      UVALUE='FIELD227', $
      XSIZE=40)

  FieldVal16162 = [ $
    '' ]
  FIELD229 = CW_FIELD( BASE225,VALUE=FieldVal16162, $
      ROW=1, $
      STRING=1, $
      TITLE='Results directory:', $
      UVALUE='FIELD229', $
      XSIZE=40)


  BASE4 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='data and star bases', $
      UVALUE='BASE4')

  BASE5 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=4, $
      MAP=1, $
      TITLE='data', $
      UVALUE='BASE5')

  LABEL46 = WIDGET_LABEL( BASE5, $
      UVALUE='LABEL46', $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      VALUE='Object')

  base5a = widget_base(base5, $
      row = 1, map=1, title='basename and options', uvalue='BASE5a')

  FieldVal16167 = [ $
    '' ]
  FIELD102 = CW_FIELD( BASE5a,VALUE=FieldVal16167, $
      ROW=1, $
      STRING=1, $
      TITLE='Basename', $
      UVALUE='FIELD102')

  Button116b = widget_button( base5a, $
      uvalue='BUTTON116b', $
      value='Preprocessing Options')

  BASE50 = WIDGET_BASE(BASE5, $
      ROW=1, $
      MAP=1, $
      TITLE='data 2 columns', $
      UVALUE='BASE50')

  BASE54 = WIDGET_BASE(BASE50, $
      COLUMN=1, $
      MAP=1, $
      TITLE='data left column', $
      UVALUE='BASE54')

  FieldVal16171 = [ $
    '' ]
  FIELD106 = CW_FIELD( BASE54,VALUE=FieldVal16171, $
      ROW=1, $
      STRING=1, $
      TITLE='Datafile:', $
      UVALUE='FIELD106', $
      XSIZE=11,/return_events)

  FieldVal16173 = [ $
    '' ]
  FIELD104 = CW_FIELD( BASE54,VALUE=FieldVal16173, $
      ROW=1, $
      STRING=1, $
      TITLE='Darkfile:', $
      UVALUE='FIELD104', $
      XSIZE=11,/return_events)

  LABEL145 = WIDGET_LABEL( BASE54, $
      UVALUE='LABEL145', $
      VALUE='Frames: ')

  BASE107 = WIDGET_BASE(BASE54, $
      ROW=1, $
      MAP=1, $
      TITLE='frames from-to', $
      UVALUE='BASE107')

  FieldVal16177 = [ $
    '' ]
  FIELD111 = CW_FIELD( BASE107,VALUE=FieldVal16177, $
      ROW=1, $
      INTEGER=1, $
      TITLE='from:', $
      UVALUE='FIELD111', $
      XSIZE=3)

  FieldVal16179 = [ $
    '' ]
  FIELD113 = CW_FIELD( BASE107,VALUE=FieldVal16179, $
      ROW=1, $
      STRING=1, $
      TITLE='to:', $
      UVALUE='FIELD113', $
      XSIZE=3)



  BASE58 = WIDGET_BASE(BASE50, $
      COLUMN=1, $
      MAP=1, $
      TITLE='data right column', $
      UVALUE='BASE58')

  FieldVal16182 = [ $
    '' ]
  FIELD59 = CW_FIELD( BASE58,VALUE=FieldVal16182, $
      ROW=1, $
      STRING=1, $
      TITLE='Sky file:', $
      UVALUE='FIELD59', $
      XSIZE=11,/return_events)

  FieldVal16184 = [ $
    '' ]
  FIELD60 = CW_FIELD( BASE58,VALUE=FieldVal16184, $
      ROW=1, $
      STRING=1, $
      TITLE='Sky Dark file:', $
      UVALUE='FIELD60', $
      XSIZE=11,/return_events)

  LABEL143 = WIDGET_LABEL( BASE58, $
      UVALUE='LABEL143', $
      VALUE='Background')

  BASE61 = WIDGET_BASE(BASE58, $
      ROW=2, $
      MAP=1, $
      TITLE='background regions 4', $
      UVALUE='BASE61')

  FieldVal16188 = [ $
    '' ]
  FIELD62 = CW_FIELD( BASE61,VALUE=FieldVal16188, $
      ROW=1, $
      INTEGER=1, $
      TITLE='xmin: ', $
      UVALUE='FIELD62', $
      XSIZE=4)

  FieldVal16190 = [ $
    '' ]
  FIELD63 = CW_FIELD( BASE61,VALUE=FieldVal16190, $
      ROW=1, $
      INTEGER=1, $
      TITLE='xmax: ', $
      UVALUE='FIELD63', $
      XSIZE=3)

  FieldVal16192 = [ $
    '' ]
  FIELD64 = CW_FIELD( BASE61,VALUE=FieldVal16192, $
      ROW=1, $
      INTEGER=1, $
      TITLE='ymin: ', $
      UVALUE='FIELD64', $
      XSIZE=3)

  FieldVal16194 = [ $
    '' ]
  FIELD65 = CW_FIELD( BASE61,VALUE=FieldVal16194, $
      ROW=1, $
      INTEGER=1, $
      TITLE='ymax: ', $
      UVALUE='FIELD65', $
      XSIZE=3)




  BASE114 = WIDGET_BASE(BASE5, $
      ROW=1, $
      MAP=1, $
      TITLE='data 3 bottom buttons', $
      UVALUE='BASE114')

  Button116a = widget_button( base114, $
      uvalue='BUTTON116a', $
      value='Reset')

  BUTTON116 = WIDGET_BUTTON( BASE114, $
      UVALUE='BUTTON116', $
      VALUE='Quicklook')

  BUTTON120 = WIDGET_BUTTON( BASE114, $
      UVALUE='BUTTON120', $
      VALUE='Go')

  BUTTON244 = WIDGET_BUTTON( BASE114, $
      UVALUE='BUTTON244', $
      VALUE='Look')

  BUTTON122 = WIDGET_BUTTON( BASE114, $
      UVALUE='BUTTON122', $
      VALUE='Save')



  BASE184 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=4, $
      MAP=1, $
      TITLE='star', $
      UVALUE='BASE184')

  LABEL165 = WIDGET_LABEL( BASE184, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL165', $
      VALUE='PSF Star')

  base184a = widget_base(base184, $
      row = 1, map=1, title='star basename and options', uvalue='BASE184a')

  FieldVal16203 = [ $
    '' ]
  FIELD166 = CW_FIELD( BASE184a,VALUE=FieldVal16203, $
      ROW=1, $
      STRING=1, $
      TITLE='Basename', $
      UVALUE='FIELD166')

  Button181b = widget_button( base184a, $
      uvalue='BUTTON181b', $
      value='Preprocessing Options')

  BASE50 = WIDGET_BASE(BASE184, $
      ROW=1, $
      MAP=1, $
      TITLE='star 2 columns', $
      UVALUE='BASE50')

  BASE54 = WIDGET_BASE(BASE50, $
      COLUMN=1, $
      MAP=1, $
      TITLE='star left column', $
      UVALUE='BASE54')

  FieldVal16207 = [ $
    '' ]
  FIELD169 = CW_FIELD( BASE54,VALUE=FieldVal16207, $
      ROW=1, $
      STRING=1, $
      TITLE='Datafile:', $
      UVALUE='FIELD169', $
      XSIZE=11,/return_events)

  FieldVal16209 = [ $
    '' ]
  FIELD170 = CW_FIELD( BASE54,VALUE=FieldVal16209, $
      ROW=1, $
      STRING=1, $
      TITLE='Darkfile:', $
      UVALUE='FIELD170', $
      XSIZE=11,/return_events)

  LABEL171 = WIDGET_LABEL( BASE54, $
      UVALUE='LABEL171', $
      VALUE='Frames: ')

  BASE107 = WIDGET_BASE(BASE54, $
      ROW=1, $
      MAP=1, $
      TITLE='frames from-to', $
      UVALUE='BASE107')

  FieldVal16213 = [ $
    '' ]
  FIELD172 = CW_FIELD( BASE107,VALUE=FieldVal16213, $
      ROW=1, $
      INTEGER=1, $
      TITLE='from:', $
      UVALUE='FIELD172', $
      XSIZE=3)

  FieldVal16215 = [ $
    '' ]
  FIELD173 = CW_FIELD( BASE107,VALUE=FieldVal16215, $
      ROW=1, $
      STRING=1, $
      TITLE='to:', $
      UVALUE='FIELD173', $
      XSIZE=3)



  BASE58 = WIDGET_BASE(BASE50, $
      COLUMN=1, $
      MAP=1, $
      TITLE='star right column', $
      UVALUE='BASE58')

  FieldVal16218 = [ $
    '' ]
  FIELD174 = CW_FIELD( BASE58,VALUE=FieldVal16218, $
      ROW=1, $
      STRING=1, $
      TITLE='Sky file:', $
      UVALUE='FIELD174', $
      XSIZE=11,/return_events)

  FieldVal16220 = [ $
    '' ]
  FIELD175 = CW_FIELD( BASE58,VALUE=FieldVal16220, $
      ROW=1, $
      STRING=1, $
      TITLE='Sky Dark file:', $
      UVALUE='FIELD175', $
      XSIZE=11,/return_events)

  LABEL176 = WIDGET_LABEL( BASE58, $
      UVALUE='LABEL176', $
      VALUE='Background')

  BASE61 = WIDGET_BASE(BASE58, $
      ROW=2, $
      MAP=1, $
      TITLE='background regions 4', $
      UVALUE='BASE61')

  FieldVal16224 = [ $
    '' ]
  FIELD177 = CW_FIELD( BASE61,VALUE=FieldVal16224, $
      ROW=1, $
      INTEGER=1, $
      TITLE='xmin: ', $
      UVALUE='FIELD177', $
      XSIZE=4)

  FieldVal16226 = [ $
    '' ]
  FIELD178 = CW_FIELD( BASE61,VALUE=FieldVal16226, $
      ROW=1, $
      INTEGER=1, $
      TITLE='xmax: ', $
      UVALUE='FIELD178', $
      XSIZE=3)

  FieldVal16228 = [ $
    '' ]
  FIELD179 = CW_FIELD( BASE61,VALUE=FieldVal16228, $
      ROW=1, $
      INTEGER=1, $
      TITLE='ymin: ', $
      UVALUE='FIELD179', $
      XSIZE=3)

  FieldVal16230 = [ $
    '' ]
  FIELD180 = CW_FIELD( BASE61,VALUE=FieldVal16230, $
      ROW=1, $
      INTEGER=1, $
      TITLE='ymax: ', $
      UVALUE='FIELD180', $
      XSIZE=3)




  BASE114a = WIDGET_BASE(BASE184, $
      ROW=1, $
      MAP=1, $
      TITLE='star 3 bottom buttons', $
      UVALUE='BASE114a')

  Button181a = widget_button( base114a, $
      uvalue='BUTTON181a', $
      value='Reset')

  BUTTON181 = WIDGET_BUTTON( BASE114a, $
      UVALUE='BUTTON181', $
      VALUE='Quicklook')

  BUTTON182 = WIDGET_BUTTON( BASE114a, $
      UVALUE='BUTTON182', $
      VALUE='Go')

  BUTTON246 = WIDGET_BUTTON( BASE114a, $
      UVALUE='BUTTON246', $
      VALUE='Look')

  BUTTON183 = WIDGET_BUTTON( BASE114a, $
      UVALUE='BUTTON183', $
      VALUE='Save')




  BASE186 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='mtf, badpix, flatfield sections', $
      UVALUE='BASE186')

  BASE193 = WIDGET_BASE(BASE186, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='mtf section', $
      UVALUE='BASE193')

  LABEL187 = WIDGET_LABEL( BASE193, $
      UVALUE='LABEL187', $
      VALUE='MTF')

  BASE18 = WIDGET_BASE(BASE193, $
      ROW=1, $
      MAP=1, $
      TITLE='file and wavelength', $
      UVALUE='BASE18')

  FieldVal16241 = [ $
    '' ]
  FIELD188 = CW_FIELD( BASE18,VALUE=FieldVal16241, $
      ROW=1, $
      STRING=1, $
      TITLE='mtf file:', $
      UVALUE='FIELD188', $
      XSIZE=11)

  FieldVal16243 = [ $
    '' ]
  FIELD189 = CW_FIELD( BASE18,VALUE=FieldVal16243, $
      ROW=1, $
      FLOAT=1, $
      TITLE='wavelength:', $
      UVALUE='FIELD189', $
      XSIZE=5)


  BASE22 = WIDGET_BASE(BASE193, $
      ROW=1, $
      MAP=1, $
      TITLE='mtf 3 bottom buttons', $
      UVALUE='BASE22')

  BUTTON190 = WIDGET_BUTTON( BASE22, $
      UVALUE='BUTTON190', $
      VALUE='Load')

  BUTTON191 = WIDGET_BUTTON( BASE22, $
      UVALUE='BUTTON191', $
      VALUE='Save')

  BUTTON192 = WIDGET_BUTTON( BASE22, $
      UVALUE='BUTTON192', $
      VALUE='Make')



  BASE199 = WIDGET_BASE(BASE186, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='Badpix section', $
      UVALUE='BASE199')

  LABEL194 = WIDGET_LABEL( BASE199, $
      UVALUE='LABEL194', $
      VALUE='BADPIX')

  FieldVal16251 = [ $
    '' ]
  FIELD195 = CW_FIELD( BASE199,VALUE=FieldVal16251, $
      ROW=1, $
      STRING=1, $
      TITLE='badpixel file:', $
      UVALUE='FIELD195', $
      XSIZE=11)

  BASE28 = WIDGET_BASE(BASE199, $
      ROW=1, $
      MAP=1, $
      TITLE='badpix 3 bottom buttons', $
      UVALUE='BASE28')

  BUTTON196 = WIDGET_BUTTON( BASE28, $
      UVALUE='BUTTON196', $
      VALUE='Load')

  BUTTON197 = WIDGET_BUTTON( BASE28, $
      UVALUE='BUTTON197', $
      VALUE='Save')

  BUTTON198 = WIDGET_BUTTON( BASE28, $
      UVALUE='BUTTON198', $
      VALUE='Make')



  BASE204 = WIDGET_BASE(BASE186, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='Flatfield section', $
      UVALUE='BASE204')

  LABEL200 = WIDGET_LABEL( BASE204, $
      UVALUE='LABEL200', $
      VALUE='FLATFIELD')

  FieldVal16259 = [ $
    '' ]
  FIELD201 = CW_FIELD( BASE204,VALUE=FieldVal16259, $
      ROW=1, $
      STRING=1, $
      TITLE='FlatData file:', $
      UVALUE='FIELD201', $
      XSIZE=11)

  FieldVal16261 = [ $
    '' ]
  FIELD202 = CW_FIELD( BASE204,VALUE=FieldVal16261, $
      ROW=1, $
      STRING=1, $
      TITLE='FlatDark file:', $
      UVALUE='FIELD202', $
      XSIZE=11)

  BASE234 = WIDGET_BASE(BASE204, $
      ROW=1, $
      MAP=1, $
      TITLE='bottom button(s)', $
      UVALUE='BASE234')

  BUTTON235 = WIDGET_BUTTON( BASE234, $
      UVALUE='BUTTON235', $
      VALUE='Make')




  BASE208 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='bottom buttons', $
      UVALUE='BASE208')

  button206a = widget_button( base208, $
      uvalue = 'button206a', $
      value = 'Load Parameters...')

  button206b = widget_button( base208, $
      uvalue = 'button206b', $
      value = 'Save Parameters')

  BUTTON206 = WIDGET_BUTTON( BASE208, $
      UVALUE='BUTTON206', $
      VALUE=' -> Speckle Processing')

  BUTTON207 = WIDGET_BUTTON( BASE208, $
      UVALUE='BUTTON207', $
      VALUE='Done')

  optionset = {debad:1, removeSky: 1, flatField: 1, $
               centroidShift: 1, deVstripe: 1, deHstripe: 1, $
               window: 1}

  entryset = {datadirectory: field227, $
              resultsdirectory: field229, $
              objectbasename: field102, $
              objectdatafile: field106, $
              objectdarkfile: field104, $
              objectfrom: field111, $
              objectto: field113, $
              objectskyfile: field59, $
              objectskydarkfile: field60, $
              objectxmin: field62, $
              objectxmax: field63, $
              objectymin: field64, $
              objectymax: field65, $
              starbasename: field166, $
              stardatafile: field169, $
              stardarkfile: field170, $
              starfrom: field172, $
              starto: field173, $
              starskyfile: field174, $
              starskydarkfile: field175, $
              starxmin: field177, $
              starxmax: field178, $
              starymin: field179, $
              starymax: field180, $
              mtffile: field188, $
              wavelength: field189, $
              badpixfile: field195, $
              flatdatafile: field201, $
              flatdarkfile: field202}

  if (n_elements(thestate) eq 0) then $
    thestate = {dataset:0, objdata:0, objdarkdata:0, objmdark:0, skyframe:0, $
                stardata:0, stardarkdata:0, starmdark:0, starskyframe:0, $
                badpix:0, mtf:0, flatfield:0, $
                speckledata:0, specklestar:0, image:0, $
                options: optionset, staroptions: optionset}

  if (n_elements(dataset) eq 0) then begin
    if handle_info(thestate.dataset,/valid_id) then begin
      handle_value,thestate.dataset,dataset
    endif else begin
      dataset = getTitanDatasetDef()
    endelse
  endif

  case typeof(dataset) of
     7: begin ; string
          dataset = getTitanDatasetDef(dataset)
        end
     8: begin ; structure
        end
     else: begin
          print,'the dataset is invalid - creating a new one'
          dataset = getTitanDatasetDef()
          return
        end
  endcase

  if handle_info(thestate.dataset,/valid_id) then handle_free,thestate.dataset
  thestate.dataset = Handle_create(value = dataset)

; initialize entries
  initspecklegui,entryset,dataset

  widget_control, main13, get_uvalue = uv
  uv = {name: uv.name, entry: entryset, state: thestate}
  widget_control, main13, set_uvalue = uv

  WIDGET_CONTROL, MAIN13, /REALIZE

  XMANAGER, 'specklegui', MAIN13

  thestate = state;  returns the state, with all its handles intact!
  return

END
