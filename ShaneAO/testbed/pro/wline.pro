;
;  wline - generate a wide line at a given angle and position
;   inputs
;    n,m: size of image
;    w,l: width and length of line (in pixels)
;    th: angle of line, in degrees
;    p0: starting position of line (2-vector)
;   output:
;    returns an image with 1's on the line, 0's elsewhere
;
function wline, n,m,w,l,th,p0
  u = fltarr(n,m)
  u[0:l-1,0:w-1] = 1
  u = shift(u,n/2,m/2)
  u = rot(u,th)
  u = shift(u,p0[0]-n/2,p0[1]-m/2)
  return,u
end
