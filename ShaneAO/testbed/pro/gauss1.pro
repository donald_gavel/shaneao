;+
;  gauss1.pro - form a gaussian profile
;
;  usage: g = gauss1(n,c,stddev)
;
;  where c is the center
;   and n is the (1d) dimension
;-
function gauss1,n,c,stddev
  machinfo = machar(/double)
  minponent = alog(machinfo.xmin)
  result = make_array(n,/double)
  x = findgen(n)
  ponent = ( - (x-c)^2/stddev^2 )
  result = exp(ponent > minponent)
  zil = where(ponent lt minponent)
  if (size(zil))(0) eq 1 then begin
    result[where(ponent lt minponent)] = 0
  endif
  return,result
end
