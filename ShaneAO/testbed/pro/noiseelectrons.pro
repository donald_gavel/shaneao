;+
;  noiseelectrons.pro - reduce AOA wfs "flat" (actually dark) data
;                       to calculate noise electrons
;
; usage:
;    nel = noiseelectrons(data)
;
; input:
;    data = a 64x64x100 array of "cflat" data
;
; output:
;    nel = scalar, number of noise electrons
;
; note:
;    a scale factor of 1.24 electrons/DN is assumed, per B. Macintosh's
;    measurements
;-
function noiseelectrons,data
  meanimage = total(data,3)/100.
  u = data*0
  for i=0,99 do u[*,*,i] = data[*,*,i] - meanimage
  n = 64.*64.*100.
  electronsperdn = 1.24
  nel = sqrt(total(u^2)/n)*electronsperdn
  return,nel
end