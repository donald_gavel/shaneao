;
;  remove the bad pixels from a datacube
;      test version - replace bad pix with neighbor algorithm
;
pro debad,incube,outcube,badpix
  if n_params() eq 0 then begin
    print,"usage: debad,incube,outcube,badpix"
    return
  end
  s = size(incube)
  n = s[1]
  m = s[2]
  if (s(0) eq 3) then nframes = s(3) else nframes = 1
  if n_elements(outcube) eq 0 then outcube = make_array(n,m,nframes);
  u = where(badpix eq 1)
  nbad = (size(u))(1)
  if u(0) eq 0 then u(0) = 1
  print,"removing bad pixels..."
  for i=0,nframes-1 do begin
    dataframe = incube(*,*,i)
    for j=0,nbad-1 do dataframe(u(j)) = dataframe(u(j)-1)
    outcube(*,*,i) = dataframe
    print,format='($,I0," ")',i
  endfor
  print,"done"
  return
end
