;
; uplink.pro
;  simulate the laser propagation to the mesosphere
;

;---------------------------
; Physical Constants
;---------------------------
c = 3.0e8  ; speed of light
cm = 1.e-2
microns = 1.e-6
nm = 1.e-9
km = 1.e3
ms = 1.e-3
h = 6.626e-34 ; J/photon Planck constant
arcsec = !pi/(180.*3600.)
i = complex(0.,1.)
seed = 7; 9

;---------------------------
; Problem Parameters
;---------------------------
lambda = 589.*nm ; wavelength of propagation
nu = c/lambda    ; frequency of the light wave
r0 = 10.*cm      ; seeing parameter
v = 20.          ; wind velocity meters/sec
zNa = 90.*km     ; altitude of sodium layer
texp = 1.*ms     ; exposure (sample) time
power = 5.       ; laser power, Watts
d_launch = .2    ; launch telescope
e2_beam = .2     ; 1/e2 diameter of beam (if Gaussian)
astig = 3.       ; radians peak-to-valley of laser aberration (will be tuned to get desired M^2)
t_atmos = 0.85   ; transmission of atmosphere (one way)
;
n = 256          ; number of fine pixels in simulation
m = 2*n          ; length of blowing screen
du = 1./float(n) ; size of fine pixel
df = 1./(n*du)   ; size of frequency domain sample, in m^-1
dtheta = lambda/(n*du) ; size of fine pixel in far field (angle)
dx = zNa*lambda/(n*du) ; size of fine pixel in far field (at the sodium layer)
!p.charsize = 2
; ---------- unused --------
d = 10.*cm       ; spatial filter scale for sensing
sigma_fit = (lambda/(2*!pi))*sqrt(0.3*(d/r0)^(5./3.)) ; calculate fitting error
dtele = 1.0      ; telescope diameter
dsec = 0.3       ; secondary obscuration
flux = 100.      ; return efficiency of the laser, photons/s/cm2/W
A = (!pi/4)*(dtele^2 - dsec^2) ; calculated area of the telescope
eta = .1         ; generic throughput/efficiency number (for example, on ~1/2 the PWFS light is inside the pupils)
nph = flux*(A/cm^2)*texp*power*eta ; calculated number of photons per exposure
ap = circle(n,n,n/2,n/2,(dtele/2.)/du,1.) - circle(n,n,n/2,n/2,(dsec/2.)/du,1.) ; aperture
nf = (1./d)/df   ; number of frequency bins inside AO low-pass filter
ao_filter = zeropad(ones(nf,nf),n,n) ; ao low-pass filter in the spatial frequency domain

;
nphs = power/(h*nu)
du = sqrt(zNa*lambda/float(n))
dx = du
ap = circle(n,n,n/2,n/2,(d_launch/2.)/du,1.)
mag2 = (ap/total(ap))*nphs
mag = sqrt(mag2)
ph = 0
wf0 = mag*exp(i*ph)

; propagation with zero phase error
wf1_0 = fresnelft(wf0,du,1.,zNa,0.,1.,lambda,ap)
ir0 = abs(wf1_0)^2
disp,ir0,'no aberrations'
u0 = real((ir0/max(ir0)) gt (1/exp(1.))^2)

; propagate with astigmatism only
ph_astig = real(zernike(n,n,(d_launch/2.)/du,2,2))
ph_astig = astig*ph_astig/(max(ph_astig*ap)-min(ph_astig*ap))
wf0 = mag*exp(i*ph_astig)
wf1_astig = fresnelft(wf0,du,1.,zNa,0.,1.,lambda,ap)
ir = abs(wf1_astig)^2
disp,ir,'astigmatism only'
u1 = real((ir/max(ir)) gt (1/exp(1.))^2)
m_squared = (total(u1)/total(u0))
print,'m squared = ',m_squared

; propagate with atmosphere and astigmatism
f = screengen(n,n,r0,du)
ph = depiston(screengen(f,seed),ap)
wf0 = mag*exp(i*(ph+ph_astig))
wf1_atmos = sqrt(t_atmos)*fresnelft(wf0,du,1.,zNa,0.,1.,lambda,ap)
ir = abs(wf1_atmos)^2
disp,ir,'atmosphere and astigmatism'

end

