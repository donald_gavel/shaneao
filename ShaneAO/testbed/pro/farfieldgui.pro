;
; Auto Save File For ./farfieldgui.pro
;
;  Mon Sep 29 22:42:00 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




PRO MAIN13_Event, Event


  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  CASE Ev OF 

  'DRAW6': BEGIN
      Print, 'Event for DRAW6'
      END
  'BUTTON49': BEGIN
      Print, 'Event for Browse'
      END
  'FIELD50': BEGIN
      Print, 'Event for wavefront file:'
      END
  'FIELD121': BEGIN
      Print, 'Event for x:'
      END
  'FIELD122': BEGIN
      Print, 'Event for y:'
      END
  'FIELD123': BEGIN
      Print, 'Event for x:'
      END
  'FIELD124': BEGIN
      Print, 'Event for y:'
      END
  'DRAW9': BEGIN
      Print, 'Event for DRAW9'
      END
  'BUTTON52': BEGIN
      Print, 'Event for Browse'
      END
  'FIELD53': BEGIN
      Print, 'Event for Intensity file:'
      END
  'FIELD126': BEGIN
      Print, 'Event for x:'
      END
  'FIELD127': BEGIN
      Print, 'Event for y:'
      END
  'FIELD128': BEGIN
      Print, 'Event for x:'
      END
  'FIELD129': BEGIN
      Print, 'Event for y:'
      END
  'FIELD72': BEGIN
      Print, 'Event for seeing (arcseconds):'
      END
  'BUTTON69': BEGIN
      Print, 'Event for go'
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO farfieldgui, GROUP=Group


  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='MAIN13')

  LABEL71 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL71', $
      VALUE='Far Field Analysis')

  BASE2 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='wavefront and centering images', $
      UVALUE='BASE2')

  BASE4 = WIDGET_BASE(BASE2, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='wavefront', $
      UVALUE='BASE4')

  LABEL5 = WIDGET_LABEL( BASE4, $
      UVALUE='LABEL5', $
      VALUE='Wavefront')

  DRAW6 = WIDGET_DRAW( BASE4, $
      RETAIN=0, $
      UVALUE='DRAW6')

  BASE51 = WIDGET_BASE(BASE4, $
      ROW=1, $
      MAP=1, $
      TITLE='wavefront file', $
      UVALUE='BASE51')

  BUTTON49 = WIDGET_BUTTON( BASE51, $
      UVALUE='BUTTON49', $
      VALUE='Browse')

  FieldVal9815 = [ $
    '' ]
  FIELD50 = CW_FIELD( BASE51,VALUE=FieldVal9815, $
      ROW=1, $
      STRING=1, $
      TITLE='wavefront file:', $
      UVALUE='FIELD50')


  LABEL133 = WIDGET_LABEL( BASE4, $
      UVALUE='LABEL133', $
      VALUE='Aperture Definition:')

  BASE125 = WIDGET_BASE(BASE4, $
      ROW=2, $
      MAP=1, $
      TITLE='wavefront aperture', $
      UVALUE='BASE125')

  FieldVal9819 = [ $
    '' ]
  FIELD121 = CW_FIELD( BASE125,VALUE=FieldVal9819, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD121', $
      XSIZE=5)

  FieldVal9821 = [ $
    '' ]
  FIELD122 = CW_FIELD( BASE125,VALUE=FieldVal9821, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD122', $
      XSIZE=5)

  FieldVal9823 = [ $
    '' ]
  FIELD123 = CW_FIELD( BASE125,VALUE=FieldVal9823, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD123', $
      XSIZE=5)

  FieldVal9825 = [ $
    '' ]
  FIELD124 = CW_FIELD( BASE125,VALUE=FieldVal9825, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD124', $
      XSIZE=5)



  BASE7 = WIDGET_BASE(BASE2, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='intensity', $
      UVALUE='BASE7')

  LABEL8 = WIDGET_LABEL( BASE7, $
      UVALUE='LABEL8', $
      VALUE='Intensity')

  DRAW9 = WIDGET_DRAW( BASE7, $
      RETAIN=0, $
      UVALUE='DRAW9')

  BASE54 = WIDGET_BASE(BASE7, $
      ROW=1, $
      MAP=1, $
      TITLE='intensity file', $
      UVALUE='BASE54')

  BUTTON52 = WIDGET_BUTTON( BASE54, $
      UVALUE='BUTTON52', $
      VALUE='Browse')

  FieldVal9832 = [ $
    '' ]
  FIELD53 = CW_FIELD( BASE54,VALUE=FieldVal9832, $
      ROW=1, $
      STRING=1, $
      TITLE='Intensity file:', $
      UVALUE='FIELD53')


  LABEL136 = WIDGET_LABEL( BASE7, $
      UVALUE='LABEL136', $
      VALUE='Aperture Definition:')

  BASE130 = WIDGET_BASE(BASE7, $
      ROW=2, $
      MAP=1, $
      TITLE='intensity aperture def', $
      UVALUE='BASE130')

  FieldVal9836 = [ $
    '' ]
  FIELD126 = CW_FIELD( BASE130,VALUE=FieldVal9836, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD126', $
      XSIZE=5)

  FieldVal9838 = [ $
    '' ]
  FIELD127 = CW_FIELD( BASE130,VALUE=FieldVal9838, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD127', $
      XSIZE=5)

  FieldVal9840 = [ $
    '' ]
  FIELD128 = CW_FIELD( BASE130,VALUE=FieldVal9840, $
      ROW=1, $
      INTEGER=1, $
      TITLE='x:', $
      UVALUE='FIELD128', $
      XSIZE=5)

  FieldVal9842 = [ $
    '' ]
  FIELD129 = CW_FIELD( BASE130,VALUE=FieldVal9842, $
      ROW=1, $
      INTEGER=1, $
      TITLE='y:', $
      UVALUE='FIELD129', $
      XSIZE=5)




  BASE68 = WIDGET_BASE(MAIN13, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='parameters', $
      UVALUE='BASE68')

  FieldVal9845 = [ $
    '' ]
  FIELD72 = CW_FIELD( BASE68,VALUE=FieldVal9845, $
      ROW=1, $
      STRING=1, $
      TITLE='seeing (arcseconds):', $
      UVALUE='FIELD72')


  BUTTON69 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON69', $
      VALUE='go')

  BUTTON137 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON137', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN13, /REALIZE

  ; Get drawable window index

  COMMON DRAW6_Comm, DRAW6_Id
  WIDGET_CONTROL, DRAW6, GET_VALUE=DRAW6_Id

  ; Get drawable window index

  COMMON DRAW9_Comm, DRAW9_Id
  WIDGET_CONTROL, DRAW9, GET_VALUE=DRAW9_Id

  XMANAGER, 'MAIN13', MAIN13
END
