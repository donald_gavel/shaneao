; brighest.pro - find the location of the brightest pixel
;
function brightest,array
  sv = size(array)
  n = sv(1)
  m = sv(2)
  ma = max(array,p)
  result = [ p-(p/n)*n, p/n ]
  return,result
end
