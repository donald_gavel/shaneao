;
; pencil.pro - implement the pencil filter
;
function pencil,ps,ndeg,inner,rang
  if (n_elements(ndeg) eq 0) then ndeg = 6
  if (n_elements(rang) eq 0) then rang = 0
  n = (size(ps))(1)
  m = (size(ps))(2)
  pi = 4*atan(1)
  degrees = pi/180.
  y = extend(findgen(m)/(m/2) - 1,1,m)
  x = rotate(y,3)
  ang = atan(y,x)
  ang = rot(ang,rang)
  u=(ang gt (-ndeg*degrees)) and (ang lt (ndeg*degrees))
  u = u or shift(rotate(u,2),0,1)
;  u = u or shift(rotate(u,1),1,0) or shift(rotate(u,2),0,1) or rotate(u,3)
  mask = 1 - u(0:(m/2),*)
  if (n_elements(inner) eq 0) then inner = 7
  cmask = circle(n,m,m/2,m/2,inner,1)
  mask = mask or cmask
  return, mask*ps
end

