;+
; DESCRIPTION:
;  wrspec - write a speckle format file
;
; USAGE:
;   wrspec,filename,data
;
; INPUTS:
;   filename - speckle file name
;   data - n x n x nf data cube *MUST BE INTEGER DATA TYPE*
;-

pro wrspec,filename,data
  header = intarr(48)
  openw,unit,filename,/get_lun
  ndim = (size(data))(0)
  n = (size(data))(1)
  if ndim gt 2 then nf = (size(data))(3) else nf = 1
  header(2) = n
  header(3) = n
  header(4) = n
  header(5) = n
  header(6) = 1
  header(7) = 1
  for i=0, nf-1 do begin
    writeu, unit, header
    writeu, unit, data(*,*,i)
    print,format='($,I0," ")',i
  endfor
  free_lun, unit
  print,'done'
  return
end
