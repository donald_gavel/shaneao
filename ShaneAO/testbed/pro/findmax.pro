; findMax.pro
;   sub-pixel 2-D maxima finder
; inputs:
;   a - data
; optional inputs:
;   sigma - approximate width of data at peak, in pixels [default: 4]
;   niter - number of iterations to take to find minimum [default: 6]
; note on bias:
;   The -center- of the pixel is the integer location
;   So, the image spans from [-.5,-.5] to [n-.5,n-.5]
;
function findMax,a,sigma,niter=niter
  if (n_elements(sigma) eq 0) then sigma = 4
  n = (size(a))[1]
  m = (size(a))[2]
  ; put the array inside a power of 2 array
  n2 = nextPow2(n)
  m2 = nextPow2(m)
  a2 = fltarr(n2,m2)
  a2[0:n-1,0:m-1] = (a - min(a)) ; bring positive
  ; find the maximum pixel
  a_max = max(a2)
  c = coords(a2,!c)
  a2 = shift(a2,n2/2-c[0],m2/2-c[1]) ; shift peak to center
  ; find the sub-pixel peak
  x = (findgen(n2)-n2/2) # ones(m2)
  y = ones(n2) # (findgen(m2)-m2/2)
  ;
  i = complex(0,1.)
  dkx = 2*!pi/float(n2)
  dky = 2*!pi/float(m2)
  kx = (findgen(n2)-n2/2)*dkx # ones(m2)
  ky = ones(n2) # (findgen(m2)-m2/2)*dky
  aperture = circle(n2,m2,m2/2,m2/2,sigma,1.)
  if (n_elements(niter) eq 0) then niter = 6
  iter = 0
  c_frac = [0.,0.]
  while (iter lt niter) do begin
    a3 = a2 - min(a2[where(aperture)])
    c_frac_iter = centroid(a3*aperture) - [n2/2,m2/2]
  ;   fft sub-pixel shift to the peak
    fa = ft(a2)*exp(i*(kx*c_frac_iter[0]+ky*c_frac_iter[1]))
    a2 = real(ft(fa,/inverse))
    c_frac = c_frac + c_frac_iter
  ;  print,iter,c_frac
    iter = iter + 1
  endwhile
  print,'<findmax> error est.=',c_frac_iter ; accuracy estimate
  return,c+c_frac
end
; test
dc = randomn(seed,2)
c = [14.2133,15.7543]+dc
c = [14.5,15.]
print,'truth: ',c
g = gauss2(32,c[0],c[1],3.)
c_hat = findmax(g,niter=6)
print,'estimate:',c_hat
print,'error:',c_hat - c
end
