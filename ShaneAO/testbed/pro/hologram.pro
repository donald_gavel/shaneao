;
; Hologram.pro
;   Simulate holograms
;     - Intensity holograms for now
;
i = complex(0.,1.)
micron = 1.e-6
cm = 1.e-2
n = 256
lambda = 0.5*micron
D = 5*cm
du = 4*D/float(n)
u = (ones(n) ## findgen(n) - n/2)*du
rayleighRange = D^2/lambda
;
L = 0.04*rayleighRange
theta = D / L ; angle that separates beams after L meters
phi0 = 5*real(zernike(n,n,n/2,3,3)) ; object beam assume it's in waves
phi = 2.*phi0
wf = exp(i*phi*2*!pi)
wfr = exp(i*theta*u*2*!pi/lambda)
wfc = wf+wfr
h1 = abs(wfc)^2
;
phi = .1*phi0 ; record a second beam and reference
wf = exp(i*phi*2*!pi)
theta = 0.5 * theta
wfr = exp(i*theta*u*2*!pi/lambda)
wfc = wf+wfr
h2 = abs(wfc)^2
;
h = h1+h2 ; make a hologram
;
; =====================================
; read out the hologram
ap = circle(n,n,n/2,n/2,0.5*D/du,1.)
phi = 2.*phi0
wft = exp(i*2*!pi*phi)
wf1 = ap*h*wft ; read the hologram
;
phi = .2*phi0
wft = exp(i*2*!pi*phi)
wf2 = ap*h*wft
; track the beams
trackbeam = 0
if (trackbeam) then begin
  wf2 = complexarr(n,n,npl)
  wf2r = complexarr(n,n,npl)
  wf2l = complexarr(n,n,npl)
  npl = 5
  dz = 2*L/float(npl)
  for k = 0,npl-1 do begin
    z = k*dz
    if (z lt 0.1*rayleighRange) then wf2[*,*,k] = fresnel(wf1,du,z,lambda)
    if (z ge 0.1*rayleighRange) then wf2[*,*,k] = fresnelft(wf1,du,1.,z,0.,1.,lambda)
    wf2r[*,*,k] = wf2[*,*,k]*wfr ; counter the angle of the eminated beam
    wf2l[*,*,k] = wf2[*,*,k]*conj(wfr)
    print,k & wait,.01
  endfor
endif
; look at far field
ff = ft(wf1)
wfm = ft(ff*ap,/inverse)
ff2 = ft(wf2)

block = ones(n,n)
block[0:n/2+n/10,*] = 0
disp,abs(ff)^2*block,'ff'
disp,abs(ff2)^2*block,'ff2'
disp,(abs(ff2)^2-abs(ff)^2)*block,'ff2 - ff'
;window,0
;plot,(abs(ff)^2)[*,n/2]
;window,1
;plot,(abs(ff2)^2)[*,n/2]
end
