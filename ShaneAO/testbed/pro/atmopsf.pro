;+
;  atmopsf.pro - atmosphere PSF
;    psf = atmopsf(n,dx,lambda,r0,d,mtf=mtf,/short)
;  INPUTS
;     n - size of final output image
;    dx - pixel size in output image (radians)
;   lambda - wavelength of light (meters)
;     r0 - seeing cell size (meters)
;    mtf - return value of mtf
;      d - telescope diameter (meters)
;          (necessary if doing short exposure mtf)
;   short - if set, use short exposure mtf, otherwise long exposure
;-
function atmopsf,n,dx,lambda,r0,d,mtf=mtf,short=short,sphere=sphere
  du = lambda/(n*dx)
  x = make_array(1,n,/float,value=1) ## (real(indgen(n)-n/2)*du)
  y = transpose(real(indgen(n)-n/2)*du) ## make_array(n,1,/float,value=1)
  r = sqrt(x^2+y^2)
  dphi = 6.88*(r/r0)^(5./3.)
  if keyword_set(sphere) then begin
    dphi = (3./8.)*dphi
  endif
  if keyword_set(short) then begin
;    alpha = 1 + 0.433* Exp(-2/(12.5*r0))
    alpha = 0.9
    if n_elements(d) eq 0 then d = n*du
    u = r/d
    mask = circle(n,n,n/2,n/2,d/du,1.0)
    arg = (-0.5 * dphi *(1-alpha*u^(1./3.)))*mask + (1-mask)*(-10)
  endif else begin
    arg = (-0.5*dphi)
  endelse
  mtf = exp(arg > (-11.)) - exp(-11)
  psf = shift(abs(fft(mtf)),n/2,n/2)
  return,psf
end

