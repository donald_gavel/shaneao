; optimal.pro - attempt to formulate the optimal
;               wavefront controller
;
;   phi_k = pupil*phi
;   phi_hat = <phi,phi_k> <phi_k,phi_k>^-1 phi_k
;
n = 256
r0 = 1.0
du = 2.0/float(n)
dk = float(n)/du
sphi = filtergen(n,n,r0,du)
ap = circle(n,n,n/2,n/2,n/16,1.0)
p = ft(ap)
f = ftconvolve(p,ftconvolve(sphi,conj(p)))
x = findgen(n/2)*(2./n)
;plot,x[1:n/2-1],abs(f[n/2,n/2+1:n-1]),/xlog,/ylog
;oplot,x[1:n/2-1],sphi[n/2,n/2+1:n-1],/xlog,/ylog
;oplot,x[1:n/2-1],abs(p[n/2,n/2+1:n-1]),/xlog,/ylog
n = 16
dx = 1.0
dk = float(n)/dx
x = (findgen(n)-n/2)*dx # (fltarr(n)+1)
y = transpose(x)
r2 = x^2 + y^2
x1 = reform(x,n*n) # (fltarr(n*n)+1)
y1 = reform(y,n*n) # (fltarr(n*n)+1)
x2 = transpose(x1)
y2 = transpose(y1)
r2a = (x1-x2)^2 + (y1-y2)^2
kx1 = x1
kx2 = x2
ky1 = y1
ky2 = y2
i = complex(0,1.)
u = exp(i*2*!pi*(x1*kx2 + y1*ky2)/float(n))
rsq = (float(n)/4.)^2
ap = (r2 le rsq)*1.0
b = (r2a le rsq)*1.0
a = u ## invert(b)
;w = circle(n,n,n/2,n/2,n/2,1.0)
w = fltarr(n,n)+1.0
w[n/2,n/2]=0.
fk = filtergen(n,n,r0,dx)*w
fx = real(ft(fk,/inverse))
seed = 5
ex = randomn(seed,n,n)
ek = ft(ex)
phix = real(ft(ek*fk,/inverse))
;phi = screengen_f(fk,seed)
phi = reform(phix,n*n)
alpha = a ## phi
alpha = reform(alpha,n,n)
fink = dk^(-4.)*w/(fk+(1.-w))
finx = real(ft(fink,/inverse))
ehatk = alpha*w/(fink+(1.-w))
ehatx = real(ft(ehatk,/inverse))
phihatx = real(ft(alpha,/inverse))
;  iterate
;ehatx = real(ftconvolve(finx,phix*ap))
;phihatx = real(ftconvolve(fx,ehatx))
;
;  direct calculation in x-space
xp = x(where(r2 le rsq))
yp = y(where(r2 le rsq))
np = (size(xp))[1]
xp1 = xp # (fltarr(np)+1.)
yp1 = yp # (fltarr(np)+1.)
xp2 = transpose(xp1)
yp2 = transpose(yp1)
r = sqrt((xp1-xp2)^2+(yp1-yp2)^2)
Dphix = 6.88*(r/r0)^(5./3.)
Rphi0 = 3.44*(2*sqrt(rsq)/r0)^(5./3.)
Rphix = Rphi0 - 0.5*Dphix
xp1 = (fltarr(n*n)+1.) # xp
yp1 = (fltarr(n*n)+1.) # yp
x2 = reform(x,n*n) # (fltarr(np)+1.)
y2 = reform(y,n*n) # (fltarr(np)+1.)
r = sqrt((xp1-x2)^2+(yp1-y2)^2)
Dphiphikx = 6.88*(r/r0)^(5./3.)
Rphiphikx = Rphi0 - 0.5*Dphiphikx
a = transpose(Rphiphikx) ## invert(Rphix)
phia = phi(where(reform(ap,n*n) eq 1))
phihatx = a ## phia
phihatx = reform(phihatx,n,n)
;
;-------------------------------------
;  Wiener filtering
;
df = 1.
n = 512
f = findgen(n+1)*df
f0 = 2.
p = 8./3.
Sy = (f^2 + f0^2)^(-p/2.) ; Kolmogorov - VonKarmen spectrum
Sn = 1.e-4
Sz = Sy + Sn
; -power spectra plots
wset,0
Plot,f[1:n],Sy[1:n],/xlog,/ylog
oplot,f[1:n],Sz[1:n]
;
Ry = Real((FFT( [ Sy[0:n],Reverse(Sy[1:n-1]) ] , /inverse ))[0:n])
Rz = Real((FFT( [ Sz[0:n],Reverse(Sz[1:n-1]) ] , /inverse ))[0:n])
t1 = findgen(n) # ones(n)
t2 = transpose(t1)
M = Rz[abs(t1-t2)]
choldc,M,pd
lumask = (t1-t2) lt 0
L = lumask*M + diagonal(pd) ; this will have the whitening filter
h = cholsol(M,pd,Ry[0:n-1])*float(2*n) ; h scaled by 2*n = 1/dt, so Ry = Rz*h*dt
;ludc,M,index
;h = lusol(M,index,Ry[0:n-1]) ; optimal filter
Hf = (FFT( [ h,fltarr(n) ] ))
; Plot,f[1:n],Abs(Hf[1:n]),/xlog,/ylog
; Plot,f[1:n],angle(Hf[1:n]),/xlog
;
; -generate some signal and noise-
;
seed = 5
w = sqrt(float(2*n))*randomn(seed,2*n)
nr = sqrt(Sn*float(2*n))*randomn(seed,2*n)
Wf = fft(w)
Nf = fft(nr)
Yf = Wf*sqrt([Sy[0:n],Reverse(Sy[1:n-1])])
y = real(fft(Yf,/inverse))
z = y + nr
; -filter the data through the wiener filter
Zf = fft(z)
Yhatf = Zf*Hf
yhat = real(fft(Yhatf,/inverse))
e = y - yhat
Ef = fft(e)
r = z - yhat
Rf = fft(r)
; -freq domain plots
wset,0
plot,f[1:n],abs(Yf[1:n])^2,/xlog,/ylog
oplot,f[1:n],abs(Zf[1:n])^2,color=150
oplot,f[1:n],abs(Yhatf[1:n])^2,color=100
oplot,f[1:n],abs(Rf[1:n])^2,color=50
oplot,f[1:n],abs(Ef[1:n])^2,color=10
; -time domain plots
wset,1
plot,y
oplot,z,color=1
oplot,y,color=50
oplot,yhat,color=200
; - closed-loop compensator
LTf = Hf/(1.0-Hf)
ltt = real(fft(LTf,/inverse))
wset,0 ; Bode magnitude plot
plot,f[1:n],abs(LTf[1:n]),/xlog,/ylog
oplot,f[1:n],abs(Hf[1:n]),color=100
wset,1 ; Nyquist plot
i=complex(0,1)
u = exp(i*2*!pi*f/n)
plot,real(LTf[1:n]),imaginary(LTf[1:n]),xrange=[-5,1],yrange=[-5,1]
oplot,imaginary(u),real(u)
; -compare to optimal gain integral control
gamma = (10./3.)^(3./8.)*Sn^(-3./8.)
iLTf = gamma/(f0+i*f)
iLTf = [iLTf,conj(reverse(iLTf[1:n-1]))]
ilt = real(fft(iLTf,/inverse))
iEf = Yf/(1.0+iLTf) + Nf*iLTf/(1.0+iLTf)
ie = real(fft(iEf,/inverse))
; plot comparison of loop transfers
wset,0
plot,f[1:n],abs(iLTf[1:n]),/xlog,/ylog
oplot,f[1:n],abs(LTf[1:n]),color=100
; plot comparison of errors
wset,0
plot,f[1:n],abs(iEf[1:n])^2,/xlog,/ylog
oplot,f[1:n],abs(Ef[1:n])^2,color=100
print,stddev(ie),stddev(e)
; models
Se = abs(1.0/(1.0+LTf))^2*Sy + abs(LTf/(1.0+LTf))^2*Sn
Sie = abs(1.0/(1.0+iLTf))^2*Sy + abs(iLTf/(1.0+iLTf))^2*Sn
print,sqrt(total(Sie)*2),sqrt(total(Se)*2) ; 2 because these are n-arrays
;
oplot,f[1:n],Sie[1:n]
oplot,f[1:n],Se[1:n],color=100
; -sample and hold function
;   for sample data systems
dt = 1.0/(2*n*df)
SHf = (1-exp(-i*2*!pi*f*dt))/(2*!pi*dt*(f0+i*f))
SHf = [SHf,conj(reverse(SHf[1:n-1]))]
t = findgen(n-1)*dt
;
; note: scaling of h with Sn
;       f: Sy=Sn,
;       scales on time axis
;       with f, on values axis
;       with 1/f
;   e.g.
; Sn = Sy[fc0]
; plot,t,h
; Sn = Sy[fc]
; oplot,(t/dt)*(fc/fc0),h*(fc0/fc)