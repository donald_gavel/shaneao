;+
;  stddev2 - calculate the standard deviation of a dataset
;
;   s = stddev2(a)
;    a 2-dimensional array, a, can be sent, in which case the
;    standard deviation will be taken along the 2nd dimension
;
;-
function stddev2,a,m
  ndim = (size(a))(0)
  if (ndim gt 2) then begin
    print,'<stddev> cannot send array larger than 2 dimensions'
    return,0
  endif
  n = (size(a))(ndim)
  if (n_elements(m) eq 0) then m = mean(a)
  if (ndim gt 1) then begin
    s = sqrt( total((a - m#make_array(n,/float,value=1.))^2,ndim)/float(n))
  endif else begin
    s = sqrt( total((a - m*make_array(n,/float,value=1.))^2,ndim)/float(n))
  endelse
  return,s
end
