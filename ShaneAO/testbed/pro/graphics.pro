;+
;  graphics.pro
;    How to do various graphics tricks:
;    1. determining the number of windows and whether a window id is valid
;    2. setup for color display
;    3. using disp to show "live" data
;    4. going back to previous graphs and adding data
;
;  see: http://www.idlcoyote.com/ for IDL tips
;-
; --------- screen size on the macintosh in my office -----------
;  see: myscreen.pro
; --------- determine open windows and window validity
;  see: window_valid.pro
forward_function checkStopButton

test = [0,0,1,0,0]
test = [0,0,0,1,1]

; ---------- set up colors properly ---------------
DEVICE, Decomposed=0, Bypass_Translation=0

if (test[0]) then begin
; -------- determine open DISP windows ------------
disp ; disp must be initialized to get the !disp system variable defined
print,!disp.windows ; the list of valid DISP ids
print,!disp.wincount ; the number of open DISPs
print,!disp.activeWindow ; the most recent window that processed and event
endif

if (test[1]) then begin
; --------- using DISP to display "live" data --------
a = randomn(seed,200,200)
disp,a,id=id
for k = 1,10 do begin
  a = randomn(seed,200,200)
  wait,.5
  dispWidget_newData,id,a
endfor
endif

if (test[2]) then begin
; -------- dealing with several plot windows ---------
;  save the plot display characteristics from !p
;  save the plot coordinate conversion system from !x, !y, !z
;  can interleave calls to disp and dispWidget_newData
;

doSines = 0 ; else do slices through the generated screen
resume = 0 ; means resume from before on existing data
frameRate = 10 ; frames per second
if (n_elements(id) eq 0) then id = 0
if (not widget_info(id,/valid_id)) then stopButton,id=id

psave = !p
pd = !p ; pd are the plot parameters for the DISP window
pd.charsize = 1.5
pd.ticklen = .5
pd.background = 255
pd.color = 0

if (n_elements(s) eq 0) then resume = 0

if (not resume) then begin
  
  n = 256 & r0 = 0.2 & du = 20./float(n) & micron = 1.e-6 & lambda = 0.5*micron
  f = screengen(n,n,r0,du)
  s = (lambda/(2*!pi))*screengen(f,seed)/micron
  
  p0 = !p
  p0.charsize = 2
  p0.thick = 3
  p0.background = 255
  p0.color = 0
  p0.ticklen = .5

  p1 = !p
  p1.charsize = 2
  p1.background = 50
    
  if window_valid(0) then wset,0 else mywindow,0
  !p = p0
  
  if (doSines) then begin
    x = findgen(100)*(2*!pi/100.)
    plot,x,sin(x)
  endif else begin
    x = (findgen(n)-n/2)*du
    plot,x,s[*,n/2],title = 'horizontal slice'
  endelse
  x0 = !x & y0 = !y & z0 = !z
  
  !p = pd
  defsysv,'!disp',exists=exists & if (exists) then dispWidget_destroyAll ; get rid of old DISP windows
  disp,s,id=disp_id
  
; window 1 showes a beautified log-log plot with soft background grid lines and nice tick locations
  if window_valid(1) then wset,1 else mywindow,1
  !p = p1
  if (doSines) then begin
    plot,x,20*cos(x)
  endif else begin
    ps = abs(ft(s[*,n/2]))^2
    df = 1./(n*du)
    f = (findgen(n)-n/2)*df
    f = f[n/2+1:n-1]
    ps = ps[n/2+2:n-1]
    yrange = minmax(ps)
    ticks = loglevels(yrange)
    nticks = n_elements(ticks)
    xrange = minmax(f)
    xticks = loglevels(xrange)
    nxticks = n_elements(xticks)
    plot,f,ps,title = 'power spectrum',yrange=yrange,/xlog,/ylog,yticks=nticks-1,ytickv=reverse(ticks),ticklen=.5,color=100,/nodata
    plot,f,ps,title = 'power spectrum',yrange=yrange,/xlog,/ylog,yticks=nticks-1,ytickv=reverse(ticks),/noerase
  endelse
  x1 = !x & y1 = !y & z1 = !z
  
  wset,0
  !p = p0 & !x = x0 & !y = y0 & !z = z0
  
  n = n_elements(x)
  nsteps = 300
  xbar = 5
  dx = 2*!pi/float(n)
  nbar = n/xbar
  ss = s
endif

for k = 0,nsteps-2 do begin
  last_ss = ss
  ss = shift(ss,10,5)
  
  xx = (findgen(nbar)+k*nbar)*dx
  wset,0 & !p = p0 & !x = x0 & !y = y0 & !z = z0
  if (doSines) then begin
    oplot,xx,cos(xx)/2.
  endif else begin
    oplot,x,last_ss[*,n/2],color=!p.background,min_value=!y.crange[0],max_value=!y.crange[1]
    oplot,x,ss[*,n/2],color=!p.color,min_value=!y.crange[0],max_value=!y.crange[1]
  endelse
  wait,0.5/frameRate
  
  !p = pd
  dispWidget_newData,disp_id,ss
  
  wset,1 & !p = p1 & !x = x1 & !y = y1 & !z = z1
  if (doSines) then begin
    oplot,xx,5*sin(xx)
  endif else begin
    last_ps = ps
    ps = abs(ft(ss[*,n/2]))^2
    ps = ps[n/2+2:n-1]
    oplot,f,last_ps,color=!p.background ,min_value=(10^!y.crange[0]),max_value=(10^!y.crange[1])
    oplot,f,ps,color=!p.color ,min_value=(10^!y.crange[0]),max_value=(10^!y.crange[1])
  endelse
  wait,0.5/frameRate
  
  if (checkStopButton(id)) then break
  
endfor

!p = psave
endif

if (test[3]) then begin
; from http://www.astro.ucla.edu/~mperrin/idl/colors.shtml
  device,decomposed=1
  p_save = !p
  !p.multi=[0,2,2]
  loadct,3
  window,/free
  imdisp,dist(200),/axis,color=fsc_color('pink')
  xyouts,50,50,"Data!",color='00BB10'x
  xyouts,150,150,"More Data!",color=fsc_color('turquoise')
  loadct,5
  tvimage,dist(100),/keep_aspect
  plot,sin(findgen(100)/10),color=fsc_color('orange red')
  xyouts,30,0.5,"A Sin Wave",color=fsc_color('Dodger Blue')
  contour,dist(256),color='FF00A0'x,/xs,/ys
  !p = p_save
endif

if (test[4]) then begin
  device,decomposed=1
  p_save = !p
  !p.multi = 0
  !p.background=fsc_color('tan1')
  !p.color = fsc_color('black')
  loadct,0
  data = randomn(seed,1000)
  window,/free
  plot,data,/nodata
  tickgrid,[0,1000,200,50],[-4,4,2,.5]
  c1 = fsc_color('dodgerblue')
  oplot,data,color=c1
  c2 = fsc_color('black')
  !p.color = fsc_color('black')
  !p.background = fsc_color('wt8')
  !p.charthick = 2
  legend,['random','good stuff'],linestyle=[0,1],color=[c1,c2],thick=[1,2],/clear,charsize=1.5
  !p = p_save
endif

end
