;multiD.pro
; Katie Morzinski     14 Dec 2007     ktmorz@ucolick.org

; This procedure (multiD stands for "multi-dimensional")
; returns the multi-d indices of the input array
; (instead of the 1-d indices).
; You can then go on to use the multi-d indices to subscript the array.

; For example:
; IDL>  array = intarr(4,4,2) + 10
; IDL>  print,array
; ;     10     10     10     10
; ;     10     10     10     10
; ;     10     10     10     10
; ;     10     10     10     10
; ;
; ;     10     10     10     10
; ;     10     10     10     10
; ;     10     10     10     10
; ;     10     10     10     10
; IDL>  multiD,array,xind,yind,zind,tind
; IDL>  print,xind
; ;     0     1     2    3
; IDL>  print,yind
; ;     0     1     2    3
; IDL>  print,zind
; ;     0     1
; IDL>  print,tind
; ;    -1
; Here is a futher example to show how you might use this information:
; IDL>  nx = (size(array))[1]
; IDL>  ny = (size(array))[2]
; IDL>  nz = (size(array))[3]
; IDL>  for i=0,nx-1 do begin for j=0,ny-1 do begin for k=0,nz-1 do begin print,array[xind[i],yind[j],zind[k]]

pro multiD, array, xind, yind, zind, tind

	ndim = (size(array))[0];number of dimensions in array
	nelem = (size(array))[ndim+2];number of elements in array
	CASE ndim OF
		0: xind = -1
		1: xind = indgen((size(array))[1])
		2: xind = indgen((size(array))[1])
		3: xind = indgen((size(array))[1])
		4: xind = indgen((size(array))[1])
	ENDCASE
	CASE ndim OF
		0: yind = -1
		1: yind = -1
		2: yind = indgen((size(array))[2])
		3: yind = indgen((size(array))[2])
		4: yind = indgen((size(array))[2])
	ENDCASE
	CASE ndim OF
		0: zind = -1
		1: zind = -1
		2: zind = -1
		3: zind = indgen((size(array))[3])
		4: zind = indgen((size(array))[3])
	ENDCASE
	CASE ndim OF
		0: tind = -1
		1: tind = -1
		2: tind = -1
		3: tind = -1
		4: tind = indgen((size(array))[4])
	ENDCASE

	return
end
