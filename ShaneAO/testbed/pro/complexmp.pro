; complexmp.pro - complex number given magnitude and phase
;
function complexmp,mag,ph
  return, mag*complex(cos(ph),sin(ph))
end
