; Knife.pro
;
;  simulate knife edge test
;
n = 256
i = complex(0.,1.)
ap = circle(n,n,n/2,n/2,n/4,1)
disp,ap,'aperture'
nz_case = [2,3,4]
mz_case = [0,1,0]
ncases = (size(nz_case))[1]
mag_set = fltarr(n,n,2,ncases)
ph_set = fltarr(n,n,ncases)
im_set = fltarr(n,n,2,ncases)
zoomi = 32
for theCase = 0,ncases-1 do begin
 nz = nz_case[theCase]
 mz = mz_case[theCase]
 ph = real(zernike(n,n,n/4,nz,mz))
 ph_set[*,*,theCase] = ph*ap
 wf = ap*exp(i*ph)
 wf_fp = ft(wf)
 knife = fltarr(n,n)
 knife[0:n/2-1,*] = 1
 wf_fp2 = wf_fp*knife
 im = abs(wf_fp2)
 imz = im[n/2-n/zoomi:n/2+n/zoomi-1,n/2-n/zoomi:n/2+n/zoomi-1]
 imz = rebin(imz,n,n)
 im_set[*,*,0,theCase] = imz
 wf_pu = ft(wf_fp2)
 mag_pu = abs(wf_pu)
 mag_set[*,*,0,theCase] = mag_pu
 knife = fltarr(n,n)
 knife[*,0:n/2] = 1
 wf_fp2 = wf_fp*knife
 im = abs(wf_fp2)
 imz = im[n/2-n/zoomi:n/2+n/zoomi-1,n/2-n/zoomi:n/2+n/zoomi-1]
 imz = rebin(imz,n,n)
 im_set[*,*,1,theCase] = imz
 wf_pu = ft(wf_fp2)
 mag_pu = abs(wf_pu)
 mag_set[*,*,1,theCase] = mag_pu
endfor
disp,reverse(reform(ph_set,n,n*ncases),2),'phase aberration'
disp,reverse(reform(transpose(mag_set,[0,2,1,3]),n*2,n*ncases),2),'knife edge measurements'
disp,reverse(reform(transpose(alog10(im_set>1.e-3),[0,2,1,3]),n*2,n*ncases),2),'focal plane x knife'
end