; Compute and display phase power spectrum

pro dispPhasePs, diagFilename, actMapFilename, reconFilename

fs = 600
dt = 1.0 / fs

; Read offset centroids from diagnostics file

rdOffCents, diagFilename, centBuff
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240
Ndm = 349

; Extract active centroids

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
centact = fltarr(2*NactSub,Ndiag)
actCent = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    centact[actCent,*] = centBuff[2*subap,*]
    actCent = actCent + 1
    centact[actCent,*] = centBuff[2*subap+1,*]
    actCent = actCent + 1
  endif
endfor


; Read reconstruction matrix

openr, 2, reconFilename
reconMatrix = fltarr(2*Nsubap,Ndm)
readu, 2, reconMatrix
close, 2


; Calculate phase power spectrum

phasePs, centBuff, reconMatrix, phasePs


; Display the phase power spectrum

psSize = size(phasePs)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]
plot, freq, phasePs[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG

end
