; Compute centroid power spectrum

PRO dispPs, filename, actMapFilename, subapEnergy

sampRate = 600.0
dt = 1.0/sampRate


; Read centroids from diagnostic file

rdOffCents, filename, centBuff
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240


; Separate x and y centroids

extXYcents, centBuff, Xcents, Ycents


; Extract active subaps

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
xcentact = fltarr(NactSub,Ndiag)
ycentact = fltarr(NactSub,Ndiag)
actSub = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    xcentact[actSub,*] = Xcents[subap,*]
    ycentact[actSub,*] = Ycents[subap,*]

    actSub = actSub + 1
  endif
endfor


; Compute the power spectra

avgps = abs(fft(Xcents[0,*]*hanning(Ndiag)))^2
psSize = size(avgps)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]
subapEnergy = fltarr(Nsubap)
subapEnergy[0] = total(avgps)

for subap = 1, Nsubap-1 do begin
  cps = abs(fft(Xcents[subap,*]*hanning(Ndiag)))^2
  subapEnergy[subap] = total(cps)
;  plot, freq, cps[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
;  print, 'subap ', subap, subapEnergy[subap]
;  wait, 1
  avgps = avgps + cps
endfor

!P.MULTI = [0,1,2]
plot, freq, avgps[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
plot, subapEnergy

; Print the power spectra

oldDevice = !D.NAME
set_plot, 'ps'
device, FILENAME='outps.ps'
plot, freq, avgps[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
plot, subapEnergy
device, /close
set_plot, oldDevice
spawn, 'lpr outps.ps'
!P.MULTI = [0,1,1]
END
