; Compute and display and save phase power spectrum

pro getactmap, mrFile, outputViewName

Nsubap = 304
NactSub = 240
Ndm = 349

; Parse the mr file into 2 parts:  actuator map and reconstruction matrix
; Read Actuator map 1st
openr, 2, mrFile
amap = bytarr(Nsubap)
readu, 2, amap
close,2
wrview, outputViewName, amap
end
