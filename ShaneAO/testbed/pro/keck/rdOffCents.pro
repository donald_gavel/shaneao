; Read Keck WFC diagnostics file - offset centroids

PRO rdOffCents, filename, centroids

; Define component sizes in terms of 8 byte doubles
headerLength = LONG(12)

imageNum = 10
pixelSize = 0.25
imageSize = 4096 * pixelSize

DMdiagLength = LONG(1000)
TTdiagLength = 2048
numCentroids = 608
numActuators = 349


; First, read the header into long (4 byte) array
header = lonarr(headerLength*2)
openr, 2, filename
readu, 2, header
close, 2


; Extract the diagnostic flags
diagFlags = header[6:23]


; Compute offset to data
offset = LONG(0)
if diagFlags[0] EQ 1 then offset = offset + imageSize*imageNum
if diagFlags[1] EQ 1 then offset = offset + imageSize*imageNum
if diagFlags[2] EQ 1 then offset = offset + numCentroids*DMdiagLength


; Read centroid data
if diagFlags[3] eq 1 then begin
  bufferLength = headerLength+offset+numCentroids*DMdiagLength
  buffer = dblarr(bufferLength)
  openr, 2, filename
  readu, 2, buffer
  close, 2
  centroidBuffer = buffer[headerLength+offset:bufferLength-1]
  centroids = reform(centroidBuffer,numCentroids,DMdiagLength)
endif else print, 'No offset centroids in ',filename
END
