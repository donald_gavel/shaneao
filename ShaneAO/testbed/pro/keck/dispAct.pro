; Displays centroid and DM time series with rms values

PRO dispAct, filename, actMapFilename, subap, act


; Read centroids from diagnostic file

rdOffCents, filename, centBuff
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240

; Get x and y cent time series for subap

xcindex = 2 * subap
ycindex = xcindex + 1
xc = centBuff[xcindex,*]
yc = centBuff[ycindex,*]

 
; Separate x and y centroids

extXYcents, centBuff, Xcents, Ycents


; Extract active subaps

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
xcentact = fltarr(NactSub,Ndiag)
ycentact = fltarr(NactSub,Ndiag)
actSub = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    xcentact[actSub,*] = Xcents[subap,*]
    ycentact[actSub,*] = Ycents[subap,*]
    actSub = actSub + 1
  endif
endfor


; Compute rms centroid values
xrms = sqrt(total(xcentact*xcentact)/(NactSub*Ndiag))
yrms = sqrt(total(ycentact*ycentact)/(NactSub*Ndiag))
print, xrms, yrms


; Read DM vectors from diagnostic file

rdDm, filename, dmBuff
sizes = size(dmBuff)
Ndiag = sizes[2]


; Get DM time series for act

dmvec = dmBuff[act,*]


; Display the centroid and actuator time series

!P.MULTI = [0,1,3]
plot, xc
plot, yc
plot, dmvec

END
