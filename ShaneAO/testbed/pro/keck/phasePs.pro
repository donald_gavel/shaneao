pro phasePs, centBuff, reconMatrix, ps

; Set parameters

insize = size(centBuff)
Ncent = insize[1]
Nsubap = Ncent / 2
Ndiag = insize[2]

; Reconstruct the phase

phaseBuff = reconMatrix ## transpose(centBuff)


; Compute the power spectra

ps = abs(fft(centBuff[0,*]*hanning(Ndiag)))^2

for cent = 2, Ncent-1,2 do begin
  cps = abs(fft(centBuff[cent,*]*hanning(Ndiag)))^2
  ps = ps + cps
endfor
ps = ps / Nsubap

end
