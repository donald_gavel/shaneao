; Compute and display and save phase power spectrum

pro dispPhasePsNew, diagFilename, mrFile, outputViewName

fs = 670
dt = 1.0 / fs

; Read offset centroids from diagnostics file

readDiag, diagFilename, dg
centBuff = dg.offsetCent
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240
Ndm = 349

; Parse the mr file into 2 parts:  actuator map and reconstruction matrix
; Read Actuator map 1st
openr, 2, mrFile
amap = bytarr(Nsubap)
readu, 2, amap

; Read reconstruction matrix 2nd

reconMatrix = fltarr(2*NactSub,Ndm)
readu, 2, reconMatrix
close, 2

;wrview,"reconmat",reconMatrix

; Extract active centroids

centact = fltarr(2*NactSub,Ndiag)
actCent = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    centact[actCent,*] = centBuff[2*subap,*]
    actCent = actCent + 1
    centact[actCent,*] = centBuff[2*subap+1,*]
    actCent = actCent + 1
  endif
endfor

; Calculate phase power spectrum

phasePsNew, centact, reconMatrix, phasePs, phaseBuff


; Display the phase power spectrum

psSize = size(phasePs)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]
plot, freq, phasePs[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
phasePst=transpose(phasePs)
wrview, outputViewName, phasePst[1:Nps/2], fmin, fmin
phasebuffname=outputViewName+"_phaseBuff"
wrview, phasebuffname, phaseBuff
end
