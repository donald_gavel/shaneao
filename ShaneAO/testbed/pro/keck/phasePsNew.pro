pro phasePsNew, centBuff, reconMatrix, ps, phaseBuff

; Set parameters


; Reconstruct the phase

phaseBuff = reconMatrix ## transpose(centBuff)
insize = size(phaseBuff)
Ndiag = insize[1]
Nphase = insize[2]
print,'Ndiag',Ndiag
print,'Nphase',Nphase
; Compute the power spectra
phaseBuff=transpose(phaseBuff)

;ps = abs(fft(phaseBuff[0,*]*hanning(Ndiag)))^2
ps = abs(fft(phaseBuff[0,*]))^2

for cent = 2, Nphase-1 do begin
;  cps = abs(fft(phaseBuff[cent,*]*hanning(Ndiag)))^2
  cps = abs(fft(phaseBuff[cent,*]))^2
  ps = ps + cps
endfor
ps = ps / Nphase

end
