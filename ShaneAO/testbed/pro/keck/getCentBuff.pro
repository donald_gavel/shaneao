; get  and save the centroid file from the diag.dat file

pro getCentBuff, diagFilename, outputViewName

; Read offset centroids from diagnostics file

readDiag, diagFilename, dg
centBuff = dg.offsetCent
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]

Nsubap = Ncent / 2
Ndm = 349

; Extract active centroids

centact = fltarr(Ncent,Ndiag)
actCent = 0
for subap = 0,Nsubap-1 do begin
    centact[actCent,*] = centBuff[2*subap,*]
    actCent = actCent + 1
    centact[actCent,*] = centBuff[2*subap+1,*]
    actCent = actCent + 1
endfor

wrview, outputViewName, centact
end
