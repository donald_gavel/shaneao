; Extracts x and y centroids into separate arrays

PRO extXYcents, centArray, Xarray, Yarray

sizes = size(centArray)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
Xarray = fltarr(Nsubap,Ndiag)
Yarray = fltarr(Nsubap,Ndiag)

cent = 0
for subap = 0,Nsubap-1 do begin
  Xarray[subap,*] = centArray[cent,*]
  cent = cent + 1
  Yarray[subap,*] = centArray[cent,*]
  cent = cent + 1
endfor

END
