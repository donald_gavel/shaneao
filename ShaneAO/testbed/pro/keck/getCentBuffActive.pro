; get  and save the centroid file from the diag.dat file

pro getCentBuffActive, diagFilename, mrFile, outputViewName

; Read offset centroids from diagnostics file

readDiag, diagFilename, dg
centBuff = dg.offsetCent
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240
Ndm = 349

; Parse the mr file into 2 parts:  actuator map and reconstruction matrix
; Read Actuator map 1st
openr, 2, mrFile
amap = bytarr(Nsubap)
readu, 2, amap

amapname=outputViewName+"_amap"
wrview, amapname, amap

; Read reconstruction matrix 2nd

reconMatrix = fltarr(2*NactSub,Ndm)
readu, 2, reconMatrix
close, 2

; Extract active centroids

centact = fltarr(2*NactSub,Ndiag)
actCent = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    centact[actCent,*] = centBuff[2*subap,*]
    actCent = actCent + 1
    centact[actCent,*] = centBuff[2*subap+1,*]
    actCent = actCent + 1
  endif
endfor

wrview, outputViewName, centact
end
