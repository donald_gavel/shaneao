; Read Keck WFC diagnostics file - raw WFS pixels
; Returns 64x640 array of 10 images (2 bytes/pixel)

PRO rdRawPixels, filename, images

; Define component sizes in terms of 8 byte doubles
headerLength = LONG(12)

imageNum = 10
pixelSize = 0.25
imageSize = 4096 * pixelSize
imagePixels = 64

DMdiagLength = LONG(1000)
TTdiagLength = 2048
numCentroids = 608
numActuators = 349


; First, read the header into long (4 byte) array
header = lonarr(headerLength*2)
openr, 2, filename
readu, 2, header
close, 2

; Extract the diagnostic flags
diagFlags = header[6:23]
for n = 0,17 do print, diagFlags[n]

; Compute offset to data
offset = LONG(0)


; Read raw image data
if diagFlags[0] eq 1 then begin
  buffLength = (headerLength+offset+imageSize*imageNum)/pixelSize
  buffer = intarr(buffLength)
  openr, 2, filename
  readu, 2, buffer
  close, 2
  firstPixel = (headerLength+offset) / pixelSize
  imageBuffer = buffer[firstPixel:buffLength-1]
  images = reform(imageBuffer,imagePixels,imagePixels*imageNum)
endif else print, 'No raw WFS pixels in ', filename
END
