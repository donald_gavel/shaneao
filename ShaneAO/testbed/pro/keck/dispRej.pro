; Compute centroid power spectrum

PRO dispRej, filename1, filename2, actMapFilename

sampRate = 600.0
dt = 1.0/sampRate

Navg = 200


; Read first centroids from diagnostic file

rdOffCents, filename1, centBuff
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240


; Separate x and y centroids

extXYcents, centBuff, Xcents, Ycents


; Extract active subaps

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
xcentact = fltarr(NactSub,Ndiag)
ycentact = fltarr(NactSub,Ndiag)
actSub = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    xcentact[actSub,*] = Xcents[subap,*]
    ycentact[actSub,*] = Ycents[subap,*]

    actSub = actSub + 1
  endif
endfor


; Compute the power spectrum

avgps1 = abs(fft(xcentact[0,*]*hanning(Ndiag)))^2
for subap = 1, Navg-1 do begin
  cps = abs(fft(Xcents[subap,*]*hanning(Ndiag)))^2
  avgps1 = avgps1 + cps
endfor




; Read second centroids from diagnostic file

rdOffCents, filename2, centBuff
sizes = size(centBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240


; Separate x and y centroids

extXYcents, centBuff, Xcents, Ycents


; Extract active subaps

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
xcentact = fltarr(NactSub,Ndiag)
ycentact = fltarr(NactSub,Ndiag)
actSub = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    xcentact[actSub,*] = Xcents[subap,*]
    ycentact[actSub,*] = Ycents[subap,*]

    actSub = actSub + 1
  endif
endfor


; Compute the power spectrum

avgps2 = abs(fft(Xcents[0,*]*hanning(Ndiag)))^2
for subap = 1, Navg-1 do begin
  cps = abs(fft(Xcents[subap,*]*hanning(Ndiag)))^2
  avgps2 = avgps2 + cps
endfor



; Display the power spectra

psSize = size(avgps2)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]
plot, freq, avgps1[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
oplot, freq, avgps2[0,*], linestyle=2
xyouts, 10.0, avgps1[0,0]/5, filename1
xyouts, 10.0, avgps1[0,0]/10, filename2


; Print the power spectra

oldDevice = !D.NAME
set_plot, 'ps'
device, FILENAME='outrej.ps'
plot, freq, avgps1[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
oplot, freq, avgps2[0,*], linestyle=2
xyouts, 10.0, avgps1[0,0]/5, filename1
xyouts, 10.0, avgps1[0,0]/10, filename2
device, /close
set_plot, oldDevice
spawn, 'lpr outrej.ps'
END
