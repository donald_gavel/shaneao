; Compute and display open and closed loop phase power spectrum

pro dispPhaseRej, openFilename, closedFilename, actMapFilename, reconFilename

fs = 600
dt = 1.0 / fs

; Read offset centroids from open diagnostics file

rdOffCents, openFilename, ocentBuff
sizes = size(ocentBuff)
Ncent = sizes[1]
Ndiag = sizes[2]
Nsubap = Ncent / 2
NactSub = 240
Ndm = 349


; Read offset centroids from closed diagnostics file

rdOffCents, closedFilename, ccentBuff


; Extract active centroids

openr, 2, actMapFilename
amap = bytarr(Nsubap)
readu, 2, amap
close, 2
centact = fltarr(2*NactSub,Ndiag)
actCent = 0
for subap = 0,Nsubap-1 do begin
  if amap[subap] GT 0 then begin
    centact[actCent,*] = ocentBuff[2*subap,*]
    actCent = actCent + 1
    centact[actCent,*] = ocentBuff[2*subap+1,*]
    actCent = actCent + 1
  endif
endfor


; Read reconstruction matrix

openr, 2, reconFilename
reconMatrix = fltarr(2*Nsubap,Ndm)
readu, 2, reconMatrix
close, 2


; Calculate phase power spectra

phasePs, ocentBuff, reconMatrix, ophasePs
phasePs, ccentBuff, reconMatrix, cphasePs


; Display the phase power spectra

psSize = size(ophasePs)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]
plot, freq, ophasePs[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
oplot, freq, cphasePs[0,*], linestyle=2
xyouts, 10.0, ophasePs[0,0]/5, openFilename
xyouts, 10.0, ophasePs[0,0]/10, closedFilename


end


