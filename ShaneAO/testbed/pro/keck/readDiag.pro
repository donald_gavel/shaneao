


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; The procedure reads Keck WFC diagnostics file - ENTIRELY
; Returns...
; 

	pro readDiag, fn, dg

;
; Define component sizes in terms of 8 byte doubles
	headerLength = LONG(9)
	imageNum = 10
	rawDim3 = 10
	condDim3 = 10
	pixelSize = 0.25
	imageSize = 4096 * pixelSize
	imagePixels = 64
	rawDim1 = 64
	condDim1 = 64
	DMdiagLength = LONG(1000)
	TTdiagLength = 2048
	numCentroids = 608
	numActuators = 349



	on_ioerror, cleanup


	char=string(replicate(32B,1))
	str1=string(replicate(32B,80))


	diagstr=strarr(50)
	strcount=0
	openr, 2, fn
	while (strmid(str1,0,8) ne 'dtSensor') do begin
		str1=''
		while (char ne string([10B])) do begin
			readu, 2, char 
			str1=str1+char
;;			print, byte(char)
		endwhile
		strcount=strcount+1
		str1=strtrim(str1,2)
		diagstr(strcount)=str1
		char=' '
	endwhile


;;;	print, diagStr



;
; First, read the header into long (4 byte) array
	header = lonarr(headerLength*2)
	
	readu, 2, header
;	close, 2
;
; Extract the diagnostic flags
;	diagFlags = header[6:23]


;
; Adjust the size of matrices to minnimize memory use 
; i.e. if a given data set is not in the file, then make the size of it 
; really small.
	if (header(6) eq 1) then begin 
		rawDim1 = 1
		rawDim3 = 1
	endif else begin
		rawDim1 = imagePixels
		rawDim3 = imageNum
	endelse

	if (header(7) eq 1) then begin 
		condDim1 = 1
		condDim3 = 1
	endif else begin
		condDim1 = imagePixels
		condDim3 = imageNum
	endelse

	if (header(8) eq 1) then begin 
		centDim1 = 1
		centDim2 = 1
	endif 


; define the diagnostics data structure
	dg={dgStruct,$
		 rawImgPres: 0l, $
		 condImgPres: 0l, $
		 centPres: 0l, $
		 OffCentPres: 0l, $
		 ZernPres: 0l, $
		 subapIntPres: 0l, $
		 reconPres: 0l, $
		 rmsWfPres: 0l, $
		 dmVecPres: 0l, $
		 dmZernikePres: 0l, $
		 DTTSensorPres: 0l, $
		 DTTErrorPres: 0l, $
		 DTTIntPres: 0l, $
		 DTTMirrorPres: 0l, $
		 DTTRmsErrPres: 0l, $
		 UTTSensorPres: 0l, $
		 UTTMirrorPresent: 0l, $
		 UTTErrPresent: 0l, $

		 rawImg:intarr(imagePixels,imagePixels,imageNum),$
		 conditionedImg: intarr(imagePixels,imagePixels,imageNum),$
		 cent: dblarr(numCentroids,DMdiagLength ),$
		 offsetCent: dblarr(numCentroids,DMdiagLength) ,$
		 zernikeErr: dblarr(10,DMDiagLength),$
		 subapInt: dblarr(numCentroids,DMDiagLength) ,$
		 reconWf: dblarr(numActuators,DMDiagLength),$
		 rmsWf: dblarr(DMDiagLength) ,$
		 dmVec:dblarr(numActuators,DMdiagLength) ,$
		 dmZernike:dblarr(10,DMDiagLength) ,$
		 dttSensor:dblarr(4,TTDiagLength) ,$
		 dttError: dblarr(2,TTDiagLength),$
		 dttInt:dblarr(TTDiagLength) ,$
		 dttMirror:dblarr(2,TTDiagLength) ,$
		 dttRmsErr:dblarr(2,TTDiagLength) }


; add the UTT lines below to the structure above when and IF UTT comes on line
;		& uttSensor: ,$
;		& uttMirror: ,$
;		& uttRmsErr: ,$
;		& }
;


	on_ioerror, cleanup


; Extract the diagnostic flags
;	diagFlags = header[6:23]


		 dg.rawImgPres=header[0]
		 dg.condImgPres=header[1]
		 dg.centPres=header[2]
		 dg.OffCentPres=header[3]
		 dg.zernPres=header[4]
		 dg.subapIntPres=header[5]
		 dg.reconPres=header[6]
		 dg.rmsWfPres=header[7]
		 dg.dmVecPres=header[8]
		 dg.dmZernikePres=header[9]
		 dg.DTTSensorPres=header[10]
		 dg.DTTErrorPres=header[11]
		 dg.DTTIntPres=header[12]
		 dg.DTTMirrorPres=header[13]
		 dg.DTTRmsErrPres=header[14]
		 dg.UTTSensorPres=header[15]
		 dg.UTTMirrorPresent=header[16]
		 dg.UTTErrPresent=header[17]



; RAW IMAGES ************************************

;
; Read raw image data
	if dg.rawImgPres eq 1 then begin
	  print, 'reading rawImg'
	  buffLength = (imageSize*imageNum)/pixelSize
	  buffer = intarr(buffLength)
	  readu, 2, buffer
	  firstPixel = 0
	  imageBuffer = buffer[firstPixel:buffLength-1]
	  dg.rawImg = reform(imageBuffer,imagePixels,imagePixels,imageNum)
	endif ;else print, '....... rawImg is not available'


; CONDITIONED IMAGES ************************************

;
; Read conditioned image data
	if dg.condImgPres eq 1 then begin
	  print, 'reading conditionedImg'
	  buffLength = (imageSize*imageNum)/pixelSize
	  buffer = intarr(buffLength)
	  readu, 2, buffer
	  firstPixel = 0
	  imageBuffer = buffer[firstPixel:buffLength-1]
	  dg.conditionedImg = reform(imageBuffer,imagePixels,imagePixels,imageNum)
	endif ;else print, '....... conditionedImg is not available'

 
; CENTROIDS***********************************
;
; Read centroid data
	if dg.centPres eq 1 then begin
	  print, 'reading cent'
	  bufferLength = numCentroids*DMdiagLength
	  buffer = dblarr(bufferLength)
	  readu, 2, buffer
	  centroidBuffer = buffer[0:bufferLength-1]
	  dg.cent = reform(centroidBuffer,numCentroids,DMdiagLength)
	endif ;else print, '....... cent is not available '

; OFFSET CENTROIDS***********************************
;
; Read offset centroid data
	if dg.offCentPres eq 1 then begin
	  Print, 'reading offsetCent ',fn	  
	  bufferLength = numCentroids*DMdiagLength
	  buffer = dblarr(bufferLength)
	  readu, 2, buffer
	  centroidBuffer = buffer[0:bufferLength-1]
	  dg.offsetCent = reform(centroidBuffer,numCentroids,DMdiagLength)
	endif ;else print, '....... offsetCent is not available'


; ZERNIKE ERRORS***********************************
;
; Read zernike error data
	if dg.zernPres eq 1 then begin
	  print, 'reading zernikeErr'
	  bufferLength = 10*DMdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.zernikeErr = reform(zBuffer,10,DMdiagLength)
	endif ;else print, '....... zernikeErr is not available'


;  subap intensity***********************************
;
; Read subap intensity data
	if dg.subapIntPres eq 1 then begin
	  print, 'reading subapInt'
	  bufferLength = 10*DMdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.subapInt = reform(zBuffer,10,DMdiagLength)
	endif ;else print, '........subapInt is not available '


;  reconstructed wavefront***********************************
;
; Read reconstructed wavefront data
	if dg.reconPres eq 1 then begin
	  print, 'reading reconWf'
	  bufferLength = 10*DMdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.reconWf = reform(zBuffer,10,DMdiagLength)
	endif ;else print, '....... reconWf is not available'

;  rmsWavefront***********************************
;
; Read rms wavefront data
	if dg.rmsWfPres eq 1 then begin
	  print, 'reading rmsWf'
	  bufferLength = DMdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.rmsWf = reform(zBuffer,1,DMdiagLength)
	endif ;else print, '....... rmsWf is not available '


;  DM vector***********************************
;
	if dg.dmVecPres eq 1 then begin
	  print, 'reading dmVec'
	  bufferLength = numActuators*DMdiagLength
	  dmbuffer = dblarr(bufferLength)
	  readu, 2, dmbuffer
	  dg.dmVec = reform(dmBuffer,numActuators,DMdiagLength)
	endif ;else print, '....... dmVec is not available'


; ZERNIKE DM ERRORS***********************************
;
; Read zernike DM errors
	if dg.dmZernikePres eq 1 then begin
	  print, 'reading dmZernike '
	  bufferLength = 10*DMdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dmZernike = reform(zBuffer,10,DMdiagLength)
	endif ;else print, '....... dmZernike is not available '


; DTT Sensor***********************************
;
; Read dtt sensor data
	if dg.DTTSensorPres eq 1 then begin
	  print, 'reading dttSensor'
	  bufferLength = 4*TTdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dttSensor = reform(zBuffer,4,TTdiagLength)
	endif ;else print, '....... dttSensor is not available'

; DTT error***********************************
;
; Read dttError data
	if dg.DTTErrorPres eq 1 then begin
	  print, 'reading dttError'
	  bufferLength = 2*TTdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dttError = reform(zBuffer,2,TTdiagLength)
	endif ;else print, '....... dttError is not available'

; DTT intensity***********************************
;
; Read DTT intensity data
	if dg.DTTIntPres eq 1 then begin
	  print, 'reading dttInt'
	  bufferLength = TTdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dttInt = reform(zBuffer,1,TTdiagLength)
	endif ;else print, '....... dttInt is not available'

; DTT MIRROR***********************************
;
; Read DTT Mirror data
	if dg.DTTMirrorPres eq 1 then begin
	  print, 'reading dttMirror '
	  bufferLength = 2*TTdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dttMirror = reform(zBuffer,2,TTdiagLength)
	endif ;else print, '....... dttMirror is not available '

; DTT rmsErr***********************************
;
; Read DTT Rms Error data
	if dg.DTTRmsErrPres eq 1 then begin
	  print, 'reading dttRmsErr'
	  bufferLength = 2*TTdiagLength
	  zbuffer = dblarr(bufferLength)
	  readu, 2, zbuffer
	  dg.dttRmsErr = reform(zBuffer,2,TTdiagLength)
	endif ;else print, '....... dttRmsErr is not available'


	print, 'Done reading diagnostics file', fn

cleanup:
	print, 'Closing diagnostics file', fn
	close, 2
	end

 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; This procedure gets the active subap map

    pro getActMap, act_map
	get_lun,lun
	temp=intarr(21,21)
	openr,lun,'/home/pstomski/kroot/kss/ao/wfc/idl/data/act_map.txt'
	readf,lun,temp
	close,lun
	free_lun,lun
	act_map=where(temp eq 1)

    end
