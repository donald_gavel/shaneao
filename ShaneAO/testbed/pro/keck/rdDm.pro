; Read Keck WFC diagnostics file - DM vector

PRO rdDm, filename, dmVectors

; Define component sizes in terms of 8 byte doubles
headerLength = LONG(12)

imageNum = 10
pixelSize = 0.25
imageSize = 4096 * pixelSize

DMdiagLength = LONG(1000)
TTdiagLength = 2048
numCents = 608
numSubs = 304
numActs = 349
numModes = 10


; First, read the header into long (4 byte) array
header = lonarr(headerLength*2)
openr, 2, filename
readu, 2, header
close, 2


; Extract the diagnostic flags
diagFlags = header[6:23]
for n = 0,17 do print, diagFlags[n]


; Compute offset to data
offset = LONG(0)
;skip raw images
if diagFlags[0] EQ 1 then offset = offset + imageSize*imageNum
;skip conditioned images
if diagFlags[1] EQ 1 then offset = offset + imageSize*imageNum
;skip raw centroids
if diagFlags[2] EQ 1 then offset = offset + numCents*DMdiagLength
;skip offset centroids
if diagFlags[3] EQ 1 then offset = offset + numCents*DMdiagLength
;skip error modes
if diagFlags[4] EQ 1 then offset = offset + numModes*DMdiagLength
;skip subap intensities
if diagFlags[5] EQ 1 then offset = offset + numSubs*DMdiagLength
;skip recon vectors
if diagFlags[6] EQ 1 then offset = offset + numActs*DMdiagLength
;skip rms wavefront error
if diagFlags[7] EQ 1 then offset = offset + DMdiagLength


; Read DM vector data
if diagFlags[8] eq 1 then begin
  bufferLength = headerLength+offset+numActs*DMdiagLength
  buffer = dblarr(bufferLength)
  openr, 2, filename
  readu, 2, buffer
  close, 2
  dmBuffer = buffer[headerLength+offset:bufferLength-1]
  dmVectors = reform(dmBuffer,numActs,DMdiagLength)
endif else print, 'No DM vectors in ', filename
END
