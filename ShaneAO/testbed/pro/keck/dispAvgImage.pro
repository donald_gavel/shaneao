; Display averaged WFS image

pro dispAvgImage, filename

; Read raw image data from diagnostics file

rdRawPixels, filename, imageArray
sizeVec = size(imageArray)
xs = sizeVec[1]
ys = sizeVec[2]
Nimages = ys / 64


; Average the images

avgImage = fltarr(xs,xs)
for im = 0, Nimages-1 do $
  avgImage = avgImage + imageArray[0:xs-1,im*xs:(im+1)*xs-1]
avgImage = avgImage / Nimages


; Display the avgeraged image

expand, avgImage, 256, 256, dd
tvscl, dd
end
