pro ps, centBuff, ps

fs = 600
dt = 1.0 / fs
insize = size(centBuff)
Ncent = insize[1]
Nsubap = Ncent / 2
Ndiag = insize[2]

; Compute the power spectra

ps = abs(fft(centBuff[0,*]*hanning(Ndiag)))^2
psSize = size(ps)
Nps = psSize[2]
freq = findgen(Nps/2+1) / (Nps*dt)
fmin = freq[1]
fmax = freq[Nps/2]

for cent = 2, Ncent-1,2 do begin
  cps = abs(fft(centBuff[cent,*]*hanning(Ndiag)))^2
  ps = ps + cps
endfor
ps = ps / Nsubap

plot, freq, ps[0,*], XRANGE=[fmin,fmax], /XLOG, /YLOG
end
