; aveps.pro - average the power spectra of centroid data
pro aveps,d,ps,f
  nc = (size(d))(1)
  nd = (size(d))(2)
  n = 1024
  u = fltarr(n)
  df = 600./n
  f = findgen(n/2-1)*df+df
  ps = fltarr(n/2-1)
  for i=0,nc-1 do begin
    u(0:nd-1) = d(i,*)
    uf = (abs(fft(u)))(1:(n/2-1))^2
    ps = ps + uf
    print,format='($,I0," ")',i
  endfor
  print,'done'
  ps = ps/nc
end
