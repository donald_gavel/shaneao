; deconvolve.pro - deconvolve an image by a PSF,
;       then impose the mft
function deconvolve, image, ref_ps, mtf, div_thresh, div_radius, alpha
  if n_params() eq 0 then begin
    print,"usage: deconvolve(ps,ref_ps,mtf,[div_thresh,div_radius,alpha])
    return,0
  endif
  m = (size(ref_ps))(1)
  n = (size(ref_ps))(2)
  if n_elements(div_radius) eq 0 then div_radius = m
  if n_elements(div_thresh) eq 0 then div_thresh = 1e-6
  if n_elements(alpha) eq 0 then alpha = 1
  image_ft = shift(fft(image),n/2,n/2)
  ps = (abs(image_ft)^2)(0:n/2,0:(n-1))
  ph = (angle(image_ft))(0:n/2,0:(n-1))
  mask = circle(m,n, m-1,m-1, div_radius, 1)
  mtf_norm = mtf(0:n/2,0:(n-1))/max(mtf)
  ref_ps_norm = (ref_ps(0:n/2,0:(n-1))/max(ref_ps)) > div_thresh
  mag = sqrt(abs( (ps*mask) / ((1-alpha) + alpha*ref_ps_norm)))*mtf_norm
  tmp = complexmp(mag,ph)
  a = hermtofull(tmp)
  res = fft(shift(a,n/2,n/2),/inverse)
  return, real(res)
end
