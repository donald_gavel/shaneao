;+
;  Constants.pro
;    various useful physical constants
;    Include with @constants
;-
nm = 1.e-9
micron = 1.e-6
mm = 1.e-3
cm = 1.e-2
km = 1.e3
degrees = !pi/180.
arcmin = !pi/(180.*60.)
arcsec = !pi/(180.*3600.)
i = complex(0.,1.)
e = 1.602176487e-19 ; charge on electron, coulombs
h = 6.626068e-34 ; Plank's constant, m2 kg / s
k = 1.3806504e-23 ; Boltzmann constant, J / K
c = 299792458. ; speed of light, m/s
au = 149.60e9 ; astronomical unit, m
pc = 30.857e15 ; parsec, m
ly = 9.461e15 ; light-year, m
