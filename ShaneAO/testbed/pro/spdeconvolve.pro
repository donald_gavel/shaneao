;+
;  spdeconvolve.pro - deconvolve the results of a bispectrum speckle analysis
;
;    USAGE:
;       im = spdeconvolve(ps, ph, ref_ps, ref_ph, mtf,
;                         div_thresh, div_radius, alpha)
;
;    INPUTS:
;       ps = object power spectrum
;       phase = object phase
;       ref_ps = star power spectrum
;       ref_ph = star phase (usually zero)
;       mtf = telescope modulation transfer function
;       div_thresh = do not divide by ref_ps values less than this number
;       div_radius = use ps and ph data out this radius
;       alpha = regularization parameter, 0<alpha<1
;               closer to one means more trust in ref_ps
;
;    OUTPUT:
;       im = reconstructed image
;-
function spdeconvolve, ps, ph, ref_ps, ref_ph, mtf, div_thresh, div_radius, alpha
  if n_params() eq 0 then begin
    print,"usage: spdeconvolve, ps, ph, ref_ps, mtf, div_thresh, div_radius, alpha"
    return,0
  endif
  m = (size(ps))(1)
  n = (size(ps))(2)
  if (m eq n) then begin
    print,'<spdeconvolve> error: inputs must be 1/2 plane complex'
    return,0
  endif
  mask = circle(m,n, m-1,m-1, div_radius, 1)
  mtf_norm = mtf(0:n/2,0:(n-1))/max(mtf)
  ref_ps_norm = (ref_ps(0:n/2,0:(n-1))/max(ref_ps)) > div_thresh
  mag = sqrt(abs( (ps*mask) / ((1-alpha) + alpha*ref_ps_norm)))*mtf_norm
  phr = ph - ref_ph
  tmp = mag*complex(cos(phr),sin(phr))
  a = hermtofull(tmp)
  res = shift(fft(shift(a,n/2,n/2),/inverse)/n^2,n/2,n/2)
  return,real(res)
end
