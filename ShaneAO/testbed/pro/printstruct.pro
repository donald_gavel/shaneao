;+
;  printstruct.pro
;
;   USAGE:
;     printstruct,structure
;
;-

pro printstruct,s,all=all
  typecodes = ['undefined','byte','integer','long int','float', $
               'double','complex float','string','structure', $
               'complex double']
  n = n_tags(s)
  names = tag_names(s)
  for i=0,n-1 do begin
    thesize = size(s.(i))
    m = (size(thesize))(1)
    thetype = typecodes(thesize(m-2))
    if ((thesize(0) eq 0) or keyword_set(all)) then begin
      print,names(i),' (',thetype,')',':    ',s.(i)
    endif else begin
      print,names(i),' (',thetype,' array)',(size(s.(i)))(1:m-3)
    endelse
  endfor
end
