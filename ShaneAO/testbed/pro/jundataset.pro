; jundataset.pro
;

dataset = {dataset, $
           basename:'', file:'', datadir:'', $
           darkfiles: strarr(8), $
           skyfile:'', skydarkfiles: strarr(8), $
           flatfile:'', flatdarkfile:'', $
           refpsffile:'', mtffile:'', $
           resultsdir:''}

if n_elements(dsn) eq 0 then dsn = '709'

print,'dsn = ' + dsn
home = '/duck1/gavel/keck/jun96/'

case dsn of

'709': begin
  dataset.basename = "titan709"
  dataset.file = "s00709.fits"
;  dataset.datadir = "/net/wolverine/lgs2/keck_jun96_2/" ; originally
  dataset.datadir = home + "titan/titan709/rawdata/"
  dataset.darkfiles = ["s00705.fits", "s00706.fits", $
                       "s00707.fits", "s00708.fits", $
                       "s00710.fits", "s00711.fits", $
                       "s00712.fits", "s00713.fits"]
  dataset.skyfile = "s00812.fits"
  dataset.skydarkfiles = ["s00808.fits", "s00809.fits", $
                         "s00810.fits", "s00811.fits", $
                         "s00813.fits", "s00814.fits", $
                         "s00815.fits", "s00815.fits"]
;   ***note: this is not a mistake: s00816 was destroyed when
;             nirc software crashed
  dataset.flatfile = "s00896.fits"     ; only one flat integration time, 0.5 s
  dataset.flatdarkfile = "s00897.fits"
  dataset.refpsffile = home + "titan/psfstar749/psfStar749"
  dataset.mtffile = home + "../sep96/titan/keckmtfKp"
  dataset.resultsdir = home + "titan/titan709/"
  end
'749': begin
  dataset.basename = "psfStar749"
  dataset.file = "s00749.fits"
  dataset.datadir = home + "titan/titan709/rawdata/"
  dataset.darkfiles = ["s00745.fits", "s00746.fits", $
                       "s00747.fits", "s00748.fits", $
                       "s00750.fits", "s00751.fits", $
                       "s00752.fits", "s00753.fits"]
  dataset.skyfile = "s00812.fits"
  dataset.skydarkfiles = ["s00808.fits", "s00809.fits", $
                          "s00810.fits", "s00811.fits", $
                          "s00813.fits", "s00814.fits", $
                          "s00815.fits", "s00815.fits"]
  dataset.flatfile = "s00896.fits"     ; only one flat integration time, 0.5 s
  dataset.flatdarkfile = "s00897.fits"
  dataset.resultsdir = home + "titan/psfstar749/"
  end
else: print, "dataset not defined: " + dsn
endcase

end
