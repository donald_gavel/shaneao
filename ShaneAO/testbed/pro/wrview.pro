;+
; wrview - write a view format file
;   USAGE:
;    wrview, filename, array, [x0, dx]
;-
pro write_view_parameters, filename, info
  openw, unit, filename+'.spr', /get_lun
  printf, unit, info.ndim, format = '(I0)'
  for i=0,info.ndim-1 do begin
    printf, unit, info.dim(i), format = '(I0)'
    printf, unit, info.x0(i), format = '(G0.4)'
    printf, unit, info.dx(i), format = '(G0.4)'
  endfor
  printf,unit,info.type, format = '(I0)'
  free_lun, unit
  return
end

pro wrview, filename, array, x0, dx, swap=swap
  s = size(array)
  info = {view_parameter, $
    ndim:1, dim:intarr(3), x0:fltarr(3), dx:fltarr(3), type:2}
  info.ndim = s(0)
  if n_elements(x0) eq 0 then x0 = [0,0,0]
  if n_elements(dx) eq 0 then dx = [1,1,1]
  for i=1,info.ndim do begin
    info.dim(i-1) = s(i)
    info.x0(i-1) = x0(i-1)
    info.dx(i-1) = dx(i-1)
  endfor
  case s(info.ndim+1) of
 1: info.type = 0 ; byte
 2: info.type = 1 ; integer
 3: info.type = 2 ; long
 4: info.type = 3 ; float
 6: info.type = 4 ; complex
 else: begin
         print, '<wrview> array type is not supported by View'
         print, ' convert to one of: byte, integer, long, float, complex'
         return
       end
  endcase
  write_view_parameters, filename, info
  openw, unit, filename+'.sdt', /get_lun
  if (keyword_set(swap)) then begin
    writeu, unit, swap_endian(array)
  endif else begin
    writeu, unit, array
  endelse
  free_lun, unit
  return
end

;finfo = {view_parameter, ndim:1, dim:intarr(3), x0:fltarr(3), dx:fltarr(3), type:2}
