;+
;  radialpsf.pro - plot the intensity vs radius from a certain point
;-

function radialpsf,im,cx,cy,rmax
  n = (size(im))(1)
  m = (size(im))(2)
  mask = circle(n,m,cx,cy,rmax,1)
  x = make_array(1,n,/float,value=1.0) ## indgen(m)
  y = indgen(n) ## make_array(m,1,/float,value=1.0)
  r = sqrt((x-cx)^2 + (y-cy)^2)
  iset = where(mask eq 1)
  rset = r(iset)
  vset = im(iset)
  plot,rset,vset,psym=1
  return,transpose([transpose(rset),transpose(vset)])
end

function statpsf,data,nbins
  rset = data(*,0)
  vset = data(*,1)
  rmax = max(rset)
  stats = make_array(nbins,3)
  dr = rmax/nbins
  for i = 0,nbins-1 do begin
    stats(i,0) = i*dr
    is = where((rset ge i*dr) and (rset lt (i+1)*dr))
    tmp = moment(vset(is))
    stats(i,1) = tmp(0);   mean
    stats(i,2) = sqrt(tmp(1));   standard deviation
  endfor
  plot,stats(*,0),stats(*,1)
  oplot,stats(*,0),stats(*,2)
  return,stats
end
