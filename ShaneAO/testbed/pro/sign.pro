;+
;  sign.pro - return the sign of a number or array
;-
function sign,a
	pos = fix(a ge 0)
	neg = -fix(a lt 0)
	return,pos+neg
end