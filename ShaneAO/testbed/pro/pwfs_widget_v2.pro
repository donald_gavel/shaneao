PRO pwfs_widget_event, event 
  
  wTarget = event.id
  wWidget = event.top
  case wTarget of
  
  widget_info(wWidget,find_by_uname='Initialize'): begin
    print,'<pwfs_widget_event> Initialize'
    pwfs_init,/pr
    w = widget_info(wWidget,find_by_uname='FocusCheck')
    widget_control,w,/sensitive
    w = widget_info(wWidget,find_by_uname='Calibrate')
    widget_control,w,/sensitive
  end
  
  widget_info(wWidget,find_by_uname='FocusCheck'): begin
    print,'<pwfs_widget_event> FocusCheck'
    w = widget_info(wWidget,find_by_uname='Active')
    widget_control,w,set_value='<<< STOP >>>',/sensitive
    
    pwfs_FocusCheck & wait,.01
    
    widget_control,w,set_value='   Ready   ',sensitive=0
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,/sensitive,set_value='Disp*'
  end

  widget_info(wWidget,find_by_uname='Calibrate'): begin
    print,'<pwfs_widget_event> Calibrate'
    w = widget_info(wWidget,find_by_uname='Active')
    widget_control,w,set_value='<<< STOP >>>',/sensitive
    
    pwfs_Calibrate & wait,.01
    
    widget_control,w,set_value='   Ready   ',sensitive=0
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,/sensitive,set_value='Disp*'
    w = widget_info(wWidget,find_by_uname='Run')
    widget_control,w,/sensitive
  end
  
  
  widget_info(wWidget,find_by_uname='Run'): begin
    print,'<pwfs_widget_event> Run'
    w = widget_info(wWidget,find_by_uname='Active')
    widget_control,w,set_value='<<< STOP >>>',/sensitive
    
    pwfs_run & wait,.01
    
    widget_control,w,set_value='   Ready   ',sensitive=0
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,/sensitive,set_value='Disp*'
  end
  
  widget_info(wWidget,find_by_uname='Disp'): begin
    print,'<pwfs_widget_event> Disp'
    pwfs_disp
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,set_value='Disp'
  end
  
  widget_info(wWidget,find_by_uname='Reset'): begin
    print,'<pwfs_widget_event> Reset'
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='Run')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='FocusCheck')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='Calibrate')
    widget_control,w,sensitive=0
  end
  
  widget_info(wWidget,find_by_uname='Active'): begin
    print,'<pwfs_widget_event> STOP!'
  end
  
  widget_info(wWidget,find_by_uname='Clear'): begin
    print,'<pwfs_widget_clear> Clear'
    clearwins
  end
  
  widget_info(wWidget,find_by_uname='Quit'): begin
    print,'<pwfs_widget_event> Quit'
    widget_control, event.top, /destroy
  end
  
  else:
  
  endcase
END 
 
PRO pwfs_widget 
 
  device,decomposed=1
  base = WIDGET_BASE(COLUMN=1) 
  print,'<pwfs_widget> base = ',base
  initButton = WIDGET_BUTTON(base, value='Initialize',uname='Initialize')
  dispButton = widget_button(base, value='Calibrate',uname='Calibrate',sensitive=0)
  dispButton = widget_button(base, value='Focus Check',uname='FocusCheck',sensitive=0)
  dispButton = widget_button(base, value='Run',uname='Run',sensitive=0)
  dispButton = widget_button(base, value='Display',uname='Disp',sensitive=0)
  resetButton = widget_button(base, value='Reset',uname='Reset')
  clearButton = widget_button(base, value='Clear',uname='Clear')
  activeLabel = widget_button(base,value='   Ready   ',uname='Active',sensitive=0)
  quitButton = widget_button(base, value='Quit',uname='Quit')
  
  WIDGET_CONTROL, base, /REALIZE 
  XMANAGER, 'pwfs_widget', base, /no_block 
  
END

; pwfs callbacks
pro pwfs_init,pr=pr
  common constants, i, microns, nm, mm, cm, arcsec
  common parameters, fnum_in, fnum_out, lenslet_pitch, f_lenslet, $
    lambda, nph, Dtele, Dsec, r0, pixels_per_spot, spot_size, dx, n, $
    pupilSize, du, ap, seed, lensletArray, L, du2, scaleFactor, feedbackGain
  common results, f, phase_atm, phase_dm, phase_est, psf, airySpot, cam_image, $
    E_pupil, E_lenslet, E_pupilImage, D_pupilImage, noise, E_pupilImage_reference, E_pupilImage_footprint, $
    Epi_set, PupilImageMask, pmask, sx0, sy0, sx, sy, residual, window, phase_hpf
  
  ; constants ;
  i = complex(0,1.)
  microns = 1.e-6
  nm = 1.e-9
  mm = 1.e-3
  cm = 1.e-2
  arcsec = !pi/(180.*3600.)
  
  ; parameters ;
  fnum_in = 20
  fnum_out = 22
  lenslet_pitch = 500.*microns
  f_lenslet = fnum_out * lenslet_pitch ; root 2 to account for diagonal across square lenslet
  L = f_lenslet + 628.*microns ; propagation distance from lenslet plane to pupil image plane
  lambda = 0.6*microns
  nph = 300.
  Dtele = 1.
  Dsec = 0.3
  r0 = 10.*cm
  pixels_per_spot = 4 ; how many pixels across the diffraction spot at the lenslet?
  seed = 11 ; random number generator seed
  du2 = 0. ;  will hold the pixel size at the pupil image plane
  scaleFactor = 12.*nm ; will be calculated during calibration
  feedbackGain = 0.5
  
  ; derived parameters 
  spot_size = 1.22*fnum_in*lambda
  dx = spot_size/pixels_per_spot
  n = nextpow2(2*lenslet_pitch/dx)
  pupilsize = nextpow2(n/pixels_per_spot) 
  du  = Dtele/float(pupilsize)
  ap = circle(n,n,n/2,n/2,(Dtele/2.)/du,1.)-circle(n,n,n/2,n/2,(Dsec/2.)/du,1.)
  lensletArray = genlensletarray(lenslet_pitch,f_lenslet,dx,n,wavelength=lambda)
;  window = supergauss(n,n/2,n/2,0.95*(Dtele/2.)/du,10)
  window = ones(n,n)
  
  ; starting point results
  pwfs_genscreen,kind='Sine'
  phase_atm = detilt(depiston(phase_atm,ap),ap)
  phase_dm = fltarr(n,n)
  residual = phase_atm
  
  if keyword_set(pr) then pwfs_init_print
  
end

pro pwfs_init_print

  print,'--- constants ---'
  varList = scope_varname(common='constants')
  n = (size(varList))[1]
  for k = 0,n-1 do begin
    print,varList[k],' = ',scope_varfetch(varList[k],common='constants')
  endfor
  
  print,'--- parameters ---'
  varList = scope_varname(common='parameters')
  n = (size(varList))[1]
  for k = 0,n-1 do begin
    data = scope_varfetch(varList[k],common='parameters')
    if (n_elements(data) eq 1) then print,varList[k],' = ',data else print, varList[k]
  endfor
  
end

pro pwfs_focusCheck,oneLens = oneLens
  common constants
  common parameters
  common results
  
  rname = '<pwfs_focusCheck>'
  if (n_elements(oneLens) eq 0) then oneLens = 0
  print,rname+'starting propagation' & wait,.01
  E_pupil = ap ; phase set to zero for checking
  E_lenslet = ft(E_pupil) ; propagate to lenslet array
  E_lenslet = fshift(E_lenslet,-0.5,-0.5) ; to get PSF centered on the apex, which is at location n/2,n/2 lower left corner of pixel
  if (oneLens) then  E_lenslet = fshift(E_lenslet,lenslet_pitch/2./dx,lenslet_pitch/2./dx) ; test: put beam in center of a lenslet
  psf = abs(E_lenslet)^2
  lensletMask = fltarr(n,n)
  lensletMask[n/2:n/2+lenslet_pitch/dx,n/2:n/2+lenslet_pitch/dx] = 1.
  lens = lensletArray
  if (oneLens) then  lens = lensletArray * lensletMask  ; test: mask all but one lenslet
  ; run through focus
  print,rname+'starting focus test' & wait,.01
  n_focs = 10
  Epi_set = fltarr(n,n,n_focs)
  delta_focus = .01*L
  foc_set = (findgen(n_focs) - n_focs/2)*delta_focus
  for foc = 0,n_focs-1 do begin
    print,rname+'focus ',foc & wait,.01
    Lp = L + foc_set[foc]
    E_pupilImage = fresnel(E_lenslet*lens,dx,Lp,lambda)
;    E_pupilImage = fresnelft(E_lenslet*lensletArray,dx,1.,Lp,0.,1.,lambda,dx=du2,/check)
    Epi_set[*,*,foc] = abs(E_pupilImage)^2
  endfor
  du2 = dx ; only if using fresnel, not fresnelft
  print,rname+'done' & wait,.01
end

pro pwfs_calibrate
  common constants
  common parameters
  common results
  
  rname = '<pwfs_calibrate> '
  phase_atm_save = phase_atm
  
  print,rname+'determining pupil mask' & wait,.01 ; one representative pupil image, for locating the pupil in the image plane
  E_pupil = ap ; phase set to zero for calibration
  E_lenslet = ft(E_pupil) ; propagate to lenslet array
  E_lenslet = fshift(E_lenslet,-0.5,-0.5) ; to get PSF centered on the apex, which is at location n/2,n/2 lower left corner of pixel
  E_lenslet_shifted = fshift(E_lenslet,lenslet_pitch/2./dx,lenslet_pitch/2./dx) ; test: put beam in center of a lenslet
  lensletMask = fltarr(n,n)
  lensletMask[n/2:n/2+lenslet_pitch/dx,n/2:n/2+lenslet_pitch/dx] = 1.
  E_pupilImage_footprint = fresnel(E_lenslet_shifted*lensletArray*lensletMask,dx,L,lambda)
  
  u = abs(E_pupilImage_footprint)
  mask = float(u gt 0.5*max(u))
  s = round(lenslet_pitch/dx)
  PupilImageMask = fltarr(n,n,4)
  PupilImageMask[*,*,0] = shift(mask,-s,0)
  PupilImageMask[*,*,1] = mask
  PupilImageMask[*,*,2] = shift(mask,-s,-s)
  PupilImageMask[*,*,3] = shift(mask,0,-s)
  pmask = shift(mask,-s/2,-s/2)
  
  print,rname+'determining flat reference' & wait,.01 ; all 4 lenslets for background flat
  E_pupilImage_reference = fresnel(E_lenslet*lensletArray,dx,L,lambda)
  du2 = dx ; only if using fresnel, not fresnelft
  
  pwfs_reconstruct,/reference
  
  print,rname+'determining the scale factor' & wait,.01
  pwfs_genscreen,/calibration
  phase_atm = detilt(depiston(phase_atm,ap),ap)
  
  E_pupil = ap*exp(2*!pi*i*phase_atm/lambda)
  E_lenslet = ft(E_pupil) ; propagate lenslet array
  E_lenslet = fshift(E_lenslet,-0.5,-0.5) ; to get PSF centered on the apex, which is at location n/2,n/2 lower left corner of pixel
  psf = abs(E_lenslet)^2
  E_pupilImage = fresnel(E_lenslet*lensletArray,dx,L,lambda)
  D_pupilImage = abs(E_pupilImage)^2
  du2 = dx ; only if using fresnel, not fresnelft
  
  pwfs_reconstruct
  
  boxSize = round(lenslet_pitch/du2)
  phase_est_insert = phase_est[n/2-boxSize/2:n/2+boxSize/2,n/2-boxSize/2:n/2+boxSize/2]
  boxSize = round(Dtele/du)
  if ((boxSize mod 2) ne 0) then boxSize += 1 ; make it even
  phase_est_insert = congrid(phase_est_insert,boxSize,boxSize,cubic=-0.5)
  phase_est = fltarr(n,n)
  phase_est[n/2-boxSize/2:n/2+boxSize/2-1,n/2-boxSize/2:n/2+boxSize/2-1]=phase_est_insert
  phase_est = detilt(depiston(phase_est,ap),ap)
  
  scaleFactor = total(phase_est*phase_atm*ap)/total(phase_est^2*ap) ; from estimator output to wavefront phase
  print,rname+'scale factor = ',strtrim(scaleFactor,2)
  phase_est *= scaleFactor
  residual = phase_atm - phase_est
  print,rname+'rms residual = ',rms(residual,ap)/microns,' microns'  
  
  phase_atm = phase_atm_save
  print,rname+'done' & wait,.01
end

pro pwfs_run
  common constants
  common parameters
  common results
  
  rname = '<pwfs_run> '
;  pwfs_genscreen
;  phase = detilt(depiston(phase,ap),ap)
  
  print,rname+'starting propagation' & wait,.01
  E_pupil = ap*exp(2*!pi*i*residual/lambda)
  E_lenslet = ft(E_pupil) ; propagate lenslet array
  E_lenslet = fshift(E_lenslet,-0.5,-0.5) ; to get PSF centered on the apex, which is at location n/2,n/2 lower left corner of pixel
  psf = abs(E_lenslet)^2
  E_pupilImage = fresnel(E_lenslet*lensletArray,dx,L,lambda)
  D_pupilImage = abs(E_pupilImage)^2
  D_pupilImage /= total(D_pupilImage)
  D_pupilImage *= nPh
  noise = sqrt(D_pupilImage)*randomn(seed,n,n) ; photon noise
  D_pupilImage += noise
  D_pupilImage = smooth(D_pupilImage,6)
  D_pupilImage = D_pupilImage > 0
  du2 = dx ; only if using fresnel, not fresnelft
  
  pwfs_reconstruct
  
  boxSize = round(lenslet_pitch/du2)
  phase_est_insert = phase_est[n/2-boxSize/2:n/2+boxSize/2,n/2-boxSize/2:n/2+boxSize/2]
  boxSize = round(Dtele/du)
  if ((boxSize mod 2) ne 0) then boxSize += 1 ; make it even
  phase_est_insert = congrid(phase_est_insert,boxSize,boxSize,cubic=-0.5)
  phase_est = fltarr(n,n)
  phase_est[n/2-boxSize/2:n/2+boxSize/2-1,n/2-boxSize/2:n/2+boxSize/2-1]=phase_est_insert
  phase_est = detilt(depiston(phase_est,ap),ap)
  
  phase_est *= scaleFactor
  
  print,rname+'done' & wait,.01
end

pro pwfs_closeLoop,zero=zero
  common constants
  common parameters
  common results
  
  rname = '<pwfs_closeLoop> '
  if (keyword_set(zero)) then begin
    phase_dm *= 0
    residual = phase_atm
  endif
  pwfs_run
  phase_dm = phase_dm + feedbackGain*window*phase_est*ap
  residual = phase_atm-phase_dm
  print,rname+'rms residual = '+strtrim(rms(residual,ap)/microns,2)
end

pro pwfs_compare
  common constants
  common parameters
  common results

  rname = '<pwfs_compare> '
  print,rname+'starting'
  m = round(n*spot_size/lenslet_pitch)
  if ((m mod 2) ne 0) then m += 1
  lowPassFilter = fltarr(n,n)
  lowPassFilter[n/2-m/2:n/2+m/2+1,n/2-m/2:n/2+m/2+1] = 1.
  phase_f = ft(phase_atm)
  phase_f *= lowPassFilter
  phase_lpf = real(ft(phase_f,/inverse))
  phase_hpf = phase_atm - phase_lpf
  print,rname+'rms lpf residual = ',rms(phase_hpf,ap)/microns,' microns'
end

pro pwfs_reconstruct,reference=reference
  common constants
  common parameters
  common results

  rname = '<pwfs_reconstruct> '
  print,rname+'starting slopes calculation'
  s = round(lenslet_pitch/dx/2.)

  if (keyword_set(reference)) then begin
    u0 = abs(E_pupilImage_reference)^2
    a0 = shift(u0*PupilImageMask[*,*,0],s,-s)
    b0 = shift(u0*PupilImageMask[*,*,1],-s,-s)
    c0 = shift(u0*PupilImageMask[*,*,2],s,s)
    d0 = shift(u0*PupilImageMask[*,*,3],-s,s)
    denom0 = a0+b0+c0+d0
    denom0 += (1-pmask)*max(denom0)
    sx0 = pmask*((b0+d0)-(a0+c0))/denom0
    sy0 = pmask*((a0+b0)-(c0+d0))/denom0
  endif else begin
    u = D_pupilImage
    a = shift(u*PupilImageMask[*,*,0],s,-s)
    b = shift(u*PupilImageMask[*,*,1],-s,-s)
    c = shift(u*PupilImageMask[*,*,2],s,s)
    d = shift(u*PupilImageMask[*,*,3],-s,s)
    denom = a+b+c+d
    eps = 0.001*max(denom)
    dmask = real(denom gt eps)
    denom = denom*dmask + eps*(1-dmask)
    sx = dmask*pmask*((b+d)-(a+c))/denom
    sy = dmask*pmask*((a+b)-(c+d))/denom
    print,rname+'starting phase reconstructor'
    phase_est = ftwfs(sx-sx0,sy-sy0,pmask,boundaryFix='noCurl',/verbose)
  endelse
  
  print,rname+'done'
  
end

pro pwfs_genscreen,calibration=calibration,kind=kind
  common parameters
  common results
  
  a = 0.1
  if (n_elements(kind) eq 0) then kind = 'Kolmogorov'
  if (keyword_set(calibration)) then a = 0.1
  if (kind eq 'Kolmogorov') then begin
    f=screengen(n,n,r0,du)
    phase_atm=screengen(f,seed)
    phase_atm *= (lambda/(2*!pi)) ; convert from radians to opd in meters
  endif
  if (kind eq 'Sine') then begin
    x = (ones(n) # findgen(n) - n/2)*du
    y = transpose(x)
    phase_atm = a*(lambda/2*!pi)*sin(2*!pi*((x*8/Dtele)-(y*8/Dtele)))
  endif
end

pro pwfs_prop
  common constants
  common parameters
  common results

  wf = ap*exp(2*!pi*i*phase/lambda)
  psf = abs(ft(wf))^2
end

pro pwfs_disp
  common constants
  common parameters
  common results
  
  if (n_elements(phase_atm) gt 1) then disp,phase_atm*ap/microns,'phase_atm, microns',ap=ap,x0=-(n/2)*du*[1,1],dx=du*[1,1]
  if (n_elements(psf) gt 1) then disp,psf,'psf, photons',x0=-(n/2)*dx*[1,1]/microns,dx=dx*[1,1]/microns
  if (n_elements(lensletArray) gt 1) then disp,arg(lensletArray),'lenslet array phase, radians',x0=-(n/2)*dx*[1,1]/microns,dx=dx*[1,1]/microns
  if (n_elements(E_pupilImage) gt 1) then disp,abs(E_pupilImage)^2,'pupil image',x0=-(n/2)*du2*[1,1]/microns,dx=du2*[1,1]/microns
  if (n_elements(D_pupilImage) gt 1) then disp,D_pupilImage,'noisy pupil image, photons',x0=-(n/2)*du2*[1,1]/microns,dx=du2*[1,1]/microns
  if (n_elements(noise) gt 1) then disp,noise,'noise in pupil image',x0=-(n/2)*du2*[1,1]/microns,dx=du2*[1,1]/microns
  if (n_elements(E_pupilImage_Reference) gt 1) then disp,abs(E_pupilImage_Reference)^2,'pupil image reference, photons',x0=-(n/2)*du2*[1,1]/microns,dx=du2*[1,1]/microns
  if (n_elements(E_pupilImage_Footprint) gt 1) then disp,abs(E_pupilImage_Footprint)^2,'pupil image footprint, photons',x0=-(n/2)*du2*[1,1]/microns,dx=du2*[1,1]/microns
  if (n_elements(phase_dm) gt 1) then disp,phase_dm*ap/microns,'phase_dm, microns',ap=ap,x0=-(n/2)*du*[1,1],dx=du*[1,1]
  if (n_elements(phase_est) gt 1) then disp,phase_est*ap/microns,'phase estimate, microns',ap=ap,x0=-(n/2)*du*[1,1],dx=du*[1,1]
  if (n_elements(residual) gt 1) then disp,residual*ap/microns,'residual, microns',ap=ap,x0=-(n/2)*du*[1,1],dx=du*[1,1]
  if (n_elements(phase_hpf) gt 1) then disp,phase_hpf*ap/microns,'high-pass filtered phase_atm, microns',ap=ap,x0=-(n/2)*du*[1,1],dx=du*[1,1]
end

;  pwfs code
common constants
common parameters
common results

print,'pwfs_widget'
pwfs_widget
end
