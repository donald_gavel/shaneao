;
; Auto Save File For ./hartmanngui.pro
;
;  Thu Sep 11 23:59:55 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13


function dirname,filename
  n = rstrpos(filename,'/')
  s = strmid(filename,0,n) + '/'
  return,s
end

function paramfilename,filename
  path = dirname(filename)
  m = strlen(path)
  n = strlen(filename)
  properfilename = strmid(filename,m,n-m)
  return,path + '.' + properfilename + '.param'
end

pro paramout,filename,me
  openw,1,filename
  printf,1,me.rows,' rows'
  printf,1,me.cols,' cols'
  printf,1,me.corners(0),me.corners(1),me.corners(2),me.corners(3),' corners'
  printf,1,me.thresholds(0),me.thresholds(1),' thresholds'
  printf,1,me.roi(0),me.roi(1),' roi'
  close,1
end

function paramin,filename,me
  openr,1,filename,error=err
  if (err ne 0) then begin
    print,'<paramin> cannot open file:',filename
    close,1
    return,0
  endif
  val = 0
  readf,1,val & me.rows = val
  readf,1,val & me.cols = val
  val = [0,0,0,0]
  readf,1,val & me.corners = val
  val = [0.,0.]
  readf,1,val & me.thresholds = val
  val = [0,0]
  readf,1,val & me.roi = val
  close,1
  return,1
end

function fileexists,filename
  openr,1,filename,error=err
  close,1
  if (err eq 0) then return, 1
  return,0
end

;function typeof,expr
;  s = size(expr)
;  n = s(0)
;  return,s(n+1)
;end

function length,expr
  s = size(expr)
  n = s(0)
  return,s(n+2)
end

;PRO MAIN13_Event, Event
PRO hartmanngui_Event, Event

  common hartmannguicom, invertim
  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev

  zoomval = 3.
  t = size(Ev)
  if (t(0) eq 0) then evname = Ev else evname = Ev.name
  print,'got an event'  
;  CASE Ev OF 
  CASE evname OF 
;======================================
;             Reference
;======================================
  'DRAW88': BEGIN
      Print, 'Event for graphic'
      widget_control,event.id,get_value=index
      wset, index
      tvdevice, index
      x = zoomval*event.x
      y = zoomval*event.y
      widget_control,event.id,get_uvalue=uval
      v = uval.rawdata(x,y)
      if (event.press and 1b) then begin
        print,'Reference: x = ',x, ', y = ', y, ', value = ', v
;      disp_txt,'x = ' + string(x) + ', y = ' + string(y) + ', value = ' + string(v),"",10
      endif
      END
  'BUTTON52': BEGIN
      Print, 'Event for Browse'
      widget_control,widget_info(event.id,/sibling),get_value=filename
      filename = filename(0)
      path = dirname(filename)
      if (filename eq '') then path = '~/guidestar/lickLaser/data'
      print,'path=',path
      filename = pickfile(path=!filepath,get_path=ret)
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ret
        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then im = tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
        endif else begin
          catch,/cancel
          widget_control,/hourglass
          if invertim then im = 255-im
;    read the data file
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base5
           widget_info(/parent, $; base18
             Event.id))
          win = $
           widget_info(/sibling, $; draw88
           widget_info(/child, $;  label6
             base))
          widget_control,win,get_value=index
          wset, index
          tvdevice,index
          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
          uval = {name:uval.name,rawdata:im,cleandata:0}
          widget_control,win,set_uvalue=uval
;    reset centroids
          widget_control,base,get_uvalue = uval
          uval = {name:uval.name, cents:0}
          widget_control,base,set_uvalue = uval

;    read the parameter file
          parambutton = $
            widget_info(/sibling, $
            widget_info(/parent, $
              Event.Id))
          widget_control,parambutton,get_uvalue = uv
          pfn = paramfilename(filename)
;          print,'reading parameter file ',pfn,uv.params
          me = uv.params
          res = paramin(pfn,me)
          if (res eq 1) then begin
            name = uv.name
            uv = {name:name,params:me}
            widget_control, parambutton, set_uvalue=uv
          endif
	endelse
      endelse
      END
  'FIELD20': BEGIN
      Print, 'Event for File:'
      END
  'BUTTON48': BEGIN
      Print, 'Event for Parameters'
      me = Ev.params
      filenamefield = $
        widget_info(/sibling, $; field63
        widget_info(/child, $;  button62
        widget_info(/sibling, $; base18
        widget_info(/sibling, $; draw61
        widget_info(/child, $;  label60
        widget_info(/parent, $; base67
          Event.Id))))))
      widget_control, filenamefield, get_value = filename
      filename = filename(0)
      pfn = paramfilename(filename)
      print,'before hartmannparamgui',me
      newme = hartmannparamgui(me,label='Reference Wavefront',file=pfn,changeflag = changed)
      if (changed eq 1) then begin
        print,'parameters changed'
        Ev = {name:evname,params:newme}
        widget_control, Event.Id, set_uvalue=Ev
        base = widget_info(/parent, Event.Id)
        win = $
          widget_info(/sibling, $
          widget_info(/child, $
            base))
        widget_control, base, get_uvalue = uv
        uv = {name:uv.name, cents:0}
        widget_control, base, set_uvalue = uv
        widget_control, win, get_uvalue = uv
        uv = {name:uv.name, rawdata:uv.rawdata, cleandata:0}
        widget_control, win, set_uvalue = uv
      endif
      END
  'BUTTON50': BEGIN
      Print, 'Event for Hartmann Centroids'
      datawindow = $
        widget_info(/sibling, $; draw61
        widget_info(/child, $; label60
        widget_info(/parent, $; base67
         Event.Id)))
      parambutton = $
        widget_info(/sibling, $; button64
        widget_info(/sibling, $; base18
         datawindow))
      widget_control, parambutton, get_uvalue=uv
      me = uv.params
      widget_control, datawindow, get_uvalue=dwuv
      im = dwuv.rawdata
      widget_control,/hourglass
      refcents = hartmannref(im,me)
      dwuv = {name:dwuv.name, rawdata:dwuv.rawdata, cleandata:im}
      widget_control, datawindow, set_uvalue = dwuv
      roimincount = 5
      refcents = hartmann_centroids(im,refcents,me.roi,roimincount)
      widget_control, datawindow, get_value=index
      wset, index
      tvdevice, index
      exptv, im, /data, /nobox, /noexact
      draw_roi, refcents, me.roi
      base = widget_info(/parent,Event.Id)
      widget_control, base, get_uvalue = uv
      uv = {name:uv.name, cents:refcents}
      widget_control, base, set_uvalue = uv
      END
;  'BUTTON51': BEGIN
;      Print, 'Event for Wavefront'
;
;      END
;======================================
;             Outgoing
;======================================
  'DRAW61': BEGIN
      Print, 'Event for graphic'
      widget_control,event.id,get_value=index
      wset, index
      tvdevice, index
      widget_control,event.id,get_uvalue=uval
      if (event.press and 1b) then begin
        if (n_tags(uval) eq 4) then begin
;         v = uval.phasedata(x,y)
          print,'Units are in WAVES'
          tvvalue,uval.phasedata
        endif else begin
          x = zoomval*event.x
          y = zoomval*event.y
          v = uval.rawdata(x,y)
          print,'Outgoing: x = ',x, ', y = ', y, ', value = ', v
        endelse
      endif
      END
  'BUTTON62': BEGIN
      Print, 'Event for Browse'
      widget_control,widget_info(event.id,/sibling),get_value=filename
      filename = filename(0)
      path = dirname(filename)
      if (filename eq '') then path = '~/guidestar/lickLaser/data'
      print,'path=',path
      filename = pickfile(path=!filepath,get_path=ref)
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ref
        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then im = tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
        endif else begin
          catch,/cancel
          widget_control,/hourglass
          if invertim then im = 255-im
;    read the data file
          widget_control,widget_info(event.id,/sibling),set_value=filename
          base = $
           widget_info(/parent, $; base67
           widget_info(/parent, $; base18b
             Event.id))
          win = $
           widget_info(/sibling, $; draw61
           widget_info(/child, $;  label60
            base))
          widget_control,win,get_value=index
          wset, index
          tvdevice, index
          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
          uval = {name:uval.name, rawdata:im, cleandata:0}
          widget_control,win,set_uvalue=uval
;    reset centroids
          widget_control,base,get_uvalue = uval
          uval = {name:uval.name, cents:0, origcents:0, iterno:0}
          widget_control,base,set_uvalue = uval
;    read the parameter file
          parambutton = $
            widget_info(/sibling, $
            widget_info(/parent, $
              Event.Id))
          widget_control,parambutton,get_uvalue = uv
          pfn = paramfilename(filename)
          me = uv.params
          res = paramin(pfn,me)
          if (res eq 1) then begin
            name = uv.name
            uv = {name:name,params:me}
            widget_control, parambutton, set_uvalue=uv
          endif
	endelse
      endelse
      END
  'FIELD63': BEGIN
      Print, 'Event for File:'
      END
  'BUTTON64': BEGIN
      Print, 'Event for Parameters'
      me = Ev.params
      filenamefield = $
        widget_info(/sibling, $; field63
        widget_info(/child, $;  button62
        widget_info(/sibling, $; base18
        widget_info(/sibling, $; draw61
        widget_info(/child, $;  label60
        widget_info(/parent, $; base67
          Event.Id))))))
      widget_control, filenamefield, get_value = filename
      filename = filename(0)
      pfn = paramfilename(filename)
      print,'before hartmannparamgui',me
      newme = hartmannparamgui(me,label='Outgoing Wavefront',file=pfn, changeflag = changed)
      if (changed eq 1) then begin
        print,'parameters changed'
        Ev = {name:evname,params:newme}
        widget_control, Event.Id, set_uvalue=Ev
        base = widget_info(/parent, Event.Id)
        win = $
          widget_info(/sibling, $
          widget_info(/child, $
            base))
        widget_control, base, get_uvalue = uv
        uv = {name:uv.name, cents:0, origcents:0, iterno:0}
        widget_control, base, set_uvalue = uv
        widget_control, win, get_uvalue = uv
        uv = {name:uv.name, rawdata:uv.rawdata, cleandata:0}
        widget_control, win, set_uvalue = uv
      endif
      END
  'BUTTON65': BEGIN
      Print, 'Event for Hartmann Centroids'
      base = widget_info(/parent, Event.Id); base67
      datawindow = $
        widget_info(/sibling, $; draw61
        widget_info(/child, $; label60
          base))
      parambutton = $
        widget_info(/sibling, $; button64
        widget_info(/sibling, $; base18
         datawindow))
      widget_control, parambutton, get_uvalue=uv
      me = uv.params
      widget_control, datawindow, get_uvalue=dwuv
; get the reference centroids
      refbase = $
        widget_info(/child, $; base5
        widget_info(/parent, $; base4
          base))
      widget_control, refbase, get_uvalue = uv
      print,uv.name
      refcents = uv.cents
;      print, type(refcents)
;      print, refcents
; compute the data centroids
      if ((size(refcents))(0) eq 2) then begin ; refcents exists (array)
        roimincount = 5
        widget_control, base, get_uvalue = buv
        cents = buv.cents
        widget_control,/hourglass
        if ((size(cents))(0) ne 2) then begin ; first time
          print,'finding centroids: first time'
          im = dwuv.rawdata
          cleanimage,im,me
          dwuv = {name:dwuv.name,rawdata:dwuv.rawdata,cleandata:im}
          widget_control, datawindow, set_uvalue = dwuv
          cents = hartmann_centroids(im,refcents,me.roi,roimincount)
;         cents = hartmannref(im,me)
          buv = {name:buv.name, cents:cents, origcents:cents, iterno:1}
        endif else begin ; iterate
          print,'finding centroids: iterating'
          im = dwuv.cleandata
          cents = hartmann_centroids(im,cents,me.roi,roimincount)
          buv = {name:buv.name, cents:cents, origcents:buv.origcents, $
                 iterno:buv.iterno+1}
        endelse
        widget_control, base, set_uvalue = buv
        widget_control, datawindow, get_value=index
        wset, index
        tvdevice, index
        exptv, im, /data, /nobox, /noexact
        draw_roi, cents, me.roi
        disp_txt,'iter:'+string(buv.iterno),"",10
      endif else begin
        print,'do the reference centroids first!'
      endelse
      END
  'BUTTON66': BEGIN
      Print, 'Event for Wavefront'
      base = widget_info(/parent,Event.Id); base67
; get the reference centroids
      refbase = $
        widget_info(/child, $; base5
        widget_info(/parent, $; base4
          base))
      widget_control, refbase, get_uvalue = uv
      refcents = uv.cents
      limits = [min(refcents(*,0)), min(refcents(*,1)), $
                max(refcents(*,0)), max(refcents(*,1))]
      gs = [(limits(2)-limits(0))/50., (limits(3)-limits(1))/50.]
      win = $
        widget_info(/sibling,$
        widget_info(/child,$
          base))
      widget_control, base, get_uvalue = uv
      if ((size(uv.cents))(0) eq 2) then begin
        widget_control,/hourglass
        wf = hartmannwf(uv.cents,uv.origcents,30,gs,limits)
;        print,'wf size: ',size(wf)
        widget_control, win, get_value = index
        wset, index
        tvdevice, index
        exptv, wf, /data, /nobox, /noexact
        widget_control, win, get_uvalue = uv
        uv = {name: uv.name, rawdata:uv.rawdata, cleandata:uv.cleandata, $
              phasedata: wf}
        widget_control, win, set_uvalue = uv
      endif
      END
;======================================
;             Retro
;======================================
  'DRAW76': BEGIN
      Print, 'Event for graphic'
      widget_control,event.id,get_value=index
      wset, index
      tvdevice, index
      widget_control,event.id,get_uvalue=uval
      if (event.press and 1b) then begin
        if (n_tags(uval) eq 4) then begin
;          v = uval.phasedata(x,y)
           print,'Units are in WAVES'
           tvvalue,uval.phasedata
        endif else begin
          x = zoomval*event.x
          y = zoomval*event.y
          v = uval.rawdata(x,y)
          print,'Reference: x = ',x, ', y = ', y, ', value = ', v
        endelse
      endif
      END
  'BUTTON77': BEGIN
      Print, 'Event for Browse'
      widget_control,widget_info(event.id,/sibling),get_value=filename
      filename = filename(0)
      path = dirname(filename)
      if (filename eq '') then path = '~/guidestar/lickLaser/data'
      print,'path=',path
      filename = pickfile(path=!filepath,get_path=ret)
      if (filename eq '') then begin
        print,'cancelled - no file'
      endif else begin
        !filepath = ret
        im = 0
        error_status = 0
        catch, error_status
        if (error_status eq 0) then im = tiff_read(filename)
        if (error_status ne 0) then begin
          print,filename
          msg = widget_message('Not a Tiff file')
        endif else begin
          catch,/cancel
          widget_control,/hourglass
          if invertim then im = 255-im
;    read the data file
          base = $
           widget_info(/parent, $; base5
           widget_info(/parent, $; base18
             Event.Id))
          win = $
           widget_info(/sibling, $; draw88
           widget_info(/child, $;  label6
             base))
          widget_control,win,get_value=index
          wset, index
          tvdevice, index
          tvscl,congrid(im,640/zoomval,480/zoomval)
          widget_control,win,get_uvalue=uval
          uval = {name:uval.name, rawdata:im, cleandata:0}
          widget_control,win,set_uvalue=uval
;    reset centroids
          widget_control,base,get_uvalue = uval
          uval = {name:uval.name, cents:0, origcents:0, iterno:0}
          widget_control,base,set_uvalue = uval
          widget_control,widget_info(event.id,/sibling),set_value=filename
;    read the parameter file
          parambutton = $
            widget_info(/sibling, $
            widget_info(/parent, $
              Event.Id))
          widget_control,parambutton,get_uvalue = uv
          pfn = paramfilename(filename)
          me = uv.params
          res = paramin(pfn,me)
          if (res eq 1) then begin
            name = uv.name
            uv = {name:name,params:me}
            widget_control, parambutton, set_uvalue=uv
          endif
	endelse
      endelse
      END
  'FIELD78': BEGIN
      Print, 'Event for File:'
      END
  'BUTTON79': BEGIN
      Print, 'Event for Parameters'
      me = Ev.params
      filenamefield = $
        widget_info(/sibling, $; field63
        widget_info(/child, $;  button62
        widget_info(/sibling, $; base18
        widget_info(/sibling, $; draw61
        widget_info(/child, $;  label60
        widget_info(/parent, $; base67
          Event.Id))))))
      widget_control, filenamefield, get_value = filename
      filename = filename(0)
      pfn = paramfilename(filename)
      print,'before hartmannparamgui',me
      newme = hartmannparamgui(me,label='Retro Wavefront',file=pfn, changeflag = changed)
      if (changed eq 1) then begin
        print,'parameters changed'
        Ev = {name:evname,params:newme}
        widget_control, Event.Id, set_uvalue=Ev
        base = widget_info(/parent, Event.Id)
        win = $
          widget_info(/sibling, $
          widget_info(/child, $
            base))
        widget_control, base, get_uvalue = uv
        uv = {name:uv.name, cents:0, origcents:0, iterno:0}
        widget_control, base, set_uvalue = uv
        widget_control, win, get_uvalue = uv
        uv = {name:uv.name, rawdata:uv.rawdata, cleandata:0}
        widget_control, win, set_uvalue = uv
      endif
      END
  'BUTTON80': BEGIN
      Print, 'Event for Hartmann Centroids'
      base = widget_info(/parent, Event.Id); base82
      datawindow = $
        widget_info(/sibling, $; draw61
        widget_info(/child, $; label60
         base))
      parambutton = $
        widget_info(/sibling, $; button64
        widget_info(/sibling, $; base18
         datawindow))
      widget_control, parambutton, get_uvalue=uv
      me = uv.params
      widget_control, datawindow, get_uvalue=dwuv
; get the reference centroids
      refbase = $
        widget_info(/child, $; base5
        widget_info(/parent, $; base4
          base))
      widget_control, refbase, get_uvalue = uv
      refcents = uv.cents
; compute the data centroids
      if ((size(refcents))(0) eq 2) then begin ; array
        roimincount = 5
        widget_control, base, get_uvalue = buv
        cents = buv.cents
        widget_control,/hourglass
        if ((size(cents))(0) ne 2) then begin ; first time
          im = dwuv.rawdata
          cleanimage,im,me
          dwuv = {name:dwuv.name,rawdata:dwuv.rawdata,cleandata:im}
          widget_control, datawindow, set_uvalue = dwuv
          cents = hartmann_centroids(im,refcents,me.roi,roimincount)
;         cents = hartmannref(im,me)
          buv = {name:buv.name, cents:cents, origcents:cents, iterno:1}
        endif else begin ; iterate
          im = dwuv.cleandata
          cents = hartmann_centroids(im,cents,me.roi,roimincount)
          buv = {name:buv.name, cents:cents, origcents:buv.origcents, $
                 iterno:buv.iterno+1}
        endelse
        widget_control, base, set_uvalue = buv
        widget_control, datawindow, get_value=index
        wset, index
        tvdevice, index
        exptv, im, /data, /nobox, /noexact
        draw_roi, cents, me.roi
        disp_txt,'iter:'+string(buv.iterno),"",10
      endif else begin
        print,'do the reference centroids first!'
      endelse
      END
  'BUTTON81': BEGIN
      Print, 'Event for Wavefront'
      base = widget_info(/parent,Event.Id)
; get the reference centroids
      refbase = $
        widget_info(/child, $; base5
        widget_info(/parent, $; base4
          base))
      widget_control, refbase, get_uvalue = uv
      refcents = uv.cents
      limits = [min(refcents(*,0)), min(refcents(*,1)), $
                max(refcents(*,0)), max(refcents(*,1))]
      gs = [(limits(2)-limits(0))/50., (limits(3)-limits(1))/50.]
      win = $
        widget_info(/sibling,$
        widget_info(/child,$
          base))
      widget_control, base, get_uvalue = uv
      if ((size(uv.cents))(0) eq 2) then begin
        widget_control,/hourglass
        wf = hartmannwf(uv.cents,uv.origcents,30,gs,limits)
;        print,'wf size: ',size(wf)
        widget_control, win, get_value = index
        wset, index
        tvdevice, index
        exptv, wf, /data, /nobox, /noexact
        widget_control, win, get_uvalue = uv
        uv = {name: uv.name, rawdata:uv.rawdata, cleandata:uv.cleandata, $
              phasedata: wf}
        widget_control, win, set_uvalue = uv
      endif
      END
  'BUTTON49b': BEGIN
      Print, 'Event for telescope component'
      base = $;
        widget_info(/sibling, $; base4
        widget_info(/child, $; label3
          Event.top))
      baseout = $
        widget_info(/sibling, $; base67
        widget_info(/child, $; base5
          base))
      baseretro = widget_info(/sibling, baseout)
      wfout = $
        widget_info(/sibling, $; draw61
        widget_info(/child, $; label60
          baseout))
      wfretro = $
        widget_info(/sibling, $; draw76
        widget_info(/child, $; label75
          baseretro))
      widget_control, wfout, get_uvalue = uvout
      widget_control, wfretro, get_uvalue = uvretro
      if ((n_tags(uvout) eq 4) and (n_tags(uvretro) eq 4)) then begin
        mask = (uvout.phasedata ne 0.0) and (uvretro.phasedata ne 0.0)
        wf = mask*(uvretro.phasedata - uvout.phasedata) / 2.0
        widget_control, base, get_uvalue = uv
;        uv = {name:uv.name, phasedata:wf}
        widget_control, base, set_uvalue = uv
        hartmannwfgui, wf, name = 'Telescope Component'
      endif else begin
        print,'cannot combine, not all wavefronts defined'
      endelse
      END
  'BUTTON49a': BEGIN
      Print, 'Event for combine wavefronts'
      base = $;
        widget_info(/sibling, $; base4
        widget_info(/child, $; label3
          Event.top))
      baseout = $
        widget_info(/sibling, $; base67
        widget_info(/child, $; base5
          base))
      baseretro = widget_info(/sibling, baseout)
      wfout = $
        widget_info(/sibling, $; draw61
        widget_info(/child, $; label60
          baseout))
      wfretro = $
        widget_info(/sibling, $; draw76
        widget_info(/child, $; label75
          baseretro))
      widget_control, wfout, get_uvalue = uvout
      widget_control, wfretro, get_uvalue = uvretro
      if ((n_tags(uvout) eq 4) and (n_tags(uvretro) eq 4)) then begin
        mask = (uvout.phasedata ne 0.0) and (uvretro.phasedata ne 0.0)
        wf = mask*(uvout.phasedata + uvretro.phasedata) / 2.0
        widget_control, base, get_uvalue = uv
        uv = {name:uv.name, phasedata:wf}
        widget_control, base, set_uvalue = uv
        hartmannwfgui, wf, name = 'Combined Wavefront'
      endif else begin
        print,'cannot combine, not all wavefronts defined'
      endelse
      END
  'BUTTON49': BEGIN
      Print, 'Event for Done'
      widget_control, event.top, /destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO hartmanngui, GROUP=Group, invert = invertit, help=helpq

  if keyword_set(helpq) then begin
    print,'usage: hartmanngui,invert=invertit,/help
    return
  endif

  common hartmannguicom, invertim
  if n_elements(invertit) eq 0 then invertit = 0
  invertim = invertit
  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }
  zoomval = 3.

  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='MAIN13')

  LABEL3 = WIDGET_LABEL( MAIN13, $
      FONT='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      UVALUE='LABEL3', $
      VALUE='Hartmann Analysis')

  BASE4 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='3 wavefronts', $
      UVALUE={name:'BASE4'})

  BASE5 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='reference', $
      UVALUE={name:'BASE5',cents:0})

  LABEL6 = WIDGET_LABEL( BASE5, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL6', $
      VALUE='Reference')

  DRAW88 = WIDGET_DRAW( BASE5, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW88',rawdata:fltarr(640,480),cleandata:0}, $
      XSIZE=640/zoomval, $
      YSIZE=480/zoomval)

  BASE18 = WIDGET_BASE(BASE5, $
      ROW=1, $
      MAP=1, $
      TITLE='File selection', $
      UVALUE='BASE18')

  BUTTON52 = WIDGET_BUTTON( BASE18, $
      UVALUE='BUTTON52', $
      VALUE='Browse')

  FieldVal10553 = ''
  FIELD20 = CW_FIELD( BASE18,VALUE=FieldVal10553, $
      ROW=1, $
      STRING=1, $
      TITLE='File:', $
      UVALUE='FIELD20', $
      XSIZE=20)


  BUTTON48 = WIDGET_BUTTON( BASE5, $
      UVALUE={name:'BUTTON48', $
              params:{rows:14,cols:13,corners:[138,36,495,394], $
                      thresholds:[0.6,0.75],roi:[25,25]}}, $
      VALUE='Parameters')

  BUTTON50 = WIDGET_BUTTON( BASE5, $
      UVALUE='BUTTON50', $
      VALUE='Hartmann Centroids')

;  BUTTON51 = WIDGET_BUTTON( BASE5, $
;      UVALUE='BUTTON51', $
;      VALUE='Wavefront')


  BASE67 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='outgoing', $
      UVALUE={name:'BASE67',cents:0,origcents:0,iterno:0})

  LABEL60 = WIDGET_LABEL( BASE67, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL60', $
      VALUE='Outgoing')

  DRAW61 = WIDGET_DRAW( BASE67, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW61',rawdata:fltarr(640,480),cleandata:0}, $
      XSIZE=640/zoomval, $
      YSIZE=480/zoomval)

  BASE18b = WIDGET_BASE(BASE67, $
      ROW=1, $
      MAP=1, $
      TITLE='File selection', $
      UVALUE='BASE18')

  BUTTON62 = WIDGET_BUTTON( BASE18b, $
      UVALUE='BUTTON62', $
      VALUE='Browse')

;  FieldVal10563 = [ $
;    '' ]
  FieldVal10563 = ''
  FIELD63 = CW_FIELD( BASE18b,VALUE=FieldVal10563, $
      ROW=1, $
      STRING=1, $
      TITLE='File:', $
      UVALUE='FIELD63', $
      XSIZE=20)


  BUTTON64 = WIDGET_BUTTON( BASE67, $
      UVALUE={name:'BUTTON64', $
              params:{rows:14,cols:13,corners:[130,38,488,400], $
                      thresholds:[0.7,0.75],roi:[20,20]}}, $
      VALUE='Parameters')

  BUTTON65 = WIDGET_BUTTON( BASE67, $
      UVALUE='BUTTON65', $
      VALUE='Hartmann Centroids')

  BUTTON66 = WIDGET_BUTTON( BASE67, $
      UVALUE='BUTTON66', $
      VALUE='Wavefront')


  BASE82 = WIDGET_BASE(BASE4, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='retro', $
      UVALUE={name:'BASE82',cents:0,origcents:0,iterno:0})

  LABEL75 = WIDGET_LABEL( BASE82, $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      UVALUE='LABEL75', $
      VALUE='Retro')

  DRAW76 = WIDGET_DRAW( BASE82, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW76',rawdata:fltarr(640,480),cleandata:0}, $
      XSIZE=640/zoomval, $
      YSIZE=480/zoomval)

  BASE18a = WIDGET_BASE(BASE82, $
      ROW=1, $
      MAP=1, $
      TITLE='file selection', $
      UVALUE='BASE18a')

  BUTTON77 = WIDGET_BUTTON( BASE18a, $
      UVALUE='BUTTON77', $
      VALUE='Browse')

;  FieldVal10573 = [ $
;    '' ]
  FieldVal10573 = ''
  FIELD78 = CW_FIELD( BASE18a,VALUE=FieldVal10573, $
      ROW=1, $
      STRING=1, $
      TITLE='File:', $
      UVALUE='FIELD78', $
      XSIZE=20)


  BUTTON79 = WIDGET_BUTTON( BASE82, $
      UVALUE={name:'BUTTON79', $
              params:{rows:14,cols:13,corners:[130,38,488,400], $
                      thresholds:[0.7,0.75],roi:[20,20]}}, $
      VALUE='Parameters')

  BUTTON80 = WIDGET_BUTTON( BASE82, $
      UVALUE='BUTTON80', $
      VALUE='Hartmann Centroids')

  BUTTON81 = WIDGET_BUTTON( BASE82, $
      UVALUE='BUTTON81', $
      VALUE='Wavefront')


  BUTTON49a = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON49a', $
      VALUE='Combined Wavefronts: (outgoing + retro)/2')

  BUTTON49b = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON49b', $
      VALUE='Telescope Component: (retro - outgoing)/2')

  BUTTON49 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON49', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN13, /REALIZE

  ; Get drawable window index

  COMMON DRAW88_Comm, DRAW88_Id
  WIDGET_CONTROL, DRAW88, GET_VALUE=DRAW88_Id

  ; Get drawable window index

  COMMON DRAW61_Comm, DRAW61_Id
  WIDGET_CONTROL, DRAW61, GET_VALUE=DRAW61_Id

  ; Get drawable window index

  COMMON DRAW76_Comm, DRAW76_Id
  WIDGET_CONTROL, DRAW76, GET_VALUE=DRAW76_Id
  print,'calling xmanager'
;  XMANAGER, 'MAIN13', MAIN13
  XMANAGER, 'hartmanngui', MAIN13
  print,'exiting xmanager'
END

