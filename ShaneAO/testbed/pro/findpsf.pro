;
;  findpsf.pro - iterative deconvolution via the regularized transform method
;
function findpsf,im,obj,niter
  n = (size(im))(1)
  m = (size(im))(2)
  F = ft(obj)
  G = ft(im)
  den = real(conj(F)*F)
  den = den + max(abs(den))*.001
  H = conj(F)*G/den
  psfest = real(ft(H,/inverse))/(float(n)*float(m))
  psfestm = psfest > 0
  for i=0,niter-1 do begin
    H = ft(psfestm)*float(n)*float(m)
    E = G - F*H
    er = real(ft(E,/inverse))
    cost = total(er^2)
    dH = conj(F)*E/den
    H = H+dH
    psfest = real(ft(H,/inverse))/(float(n)*float(m))
    print,'iteration ',i,' cost = ',cost
    psfestm = psfest > 0
  endfor 
  return,psfestm
end
