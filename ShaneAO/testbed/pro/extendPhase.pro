;+
;  extendPhase.pro
;    estimate the phase outside of an aperture given the phase inside
;
; Usage: phihat = extendPhase(phiw,f,pupil,eww)
;
; Inputs:
;    phiw - the phase on the aperture (2-d real array n x n)
;    f - the square-root of the power spectrum of the phase on an infinite aperture
;       (k^(-11/6) for Kolmogorov)
;    pupil - the aperture (2-d real array n x n, = 1 inside and 0 outside aperure)
;
; Output:
;    phihat - the estimate of phase on whole n x n array
;             closely mathches phase, phiw, inside the pupil if the iteration converged
;    eww - the white noise that, when convolved with f, produces phihat
;
; Method:
;    Search for the solution to
;         phiw*pupil = [filter (convolve) eww]*pupil
;    where filter is the inverse Fourier transform of f
;    The estimate result, phihat = filter (convolve) eww on the whole array
;
;    The solver is fixed iteration:
;               e(k) = e(k-1) + gain*delta_e(k-1)*pupil
;          Phihat(k) = f (convolve) e(k)
;           dPhiw(k) = Phiw - Phihat(k)
;         delta_e(k) = dPhiw(k) (deconvolve) f
;
;       with initialization:
;               e(0) = 0
;         delta_e(0) = Phiw (deconvolve) f
;
;       Convergence is when ||dPhiw(k)|| is less than a target percentaga of ||Phiw||
;         or if a given maximum number of iterations is reached
;-

function compCost,eww,pupil,phiw,f,p1,phihat,pw
  phihat = real(ft(ft(eww)*f,/inverse))
  pw = (phiw-phihat)*pupil
  cost = sqrt(total(pw^2))/p1
  return,cost
end

function extendPhase,phiw,f,pupil,eww,verbose=diagnostics,target=costStop,maxiter=maxiter,gain=gain
n = (size(phiw))[1]
;  filter the windowed screen
phiw_f = ft(phiw)
phihat_f = 0*phiw_f
phihat = fltarr(n,n)
eww = fltarr(n,n)
;
if (n_elements(gain) eq 0) then gain = .07
adaptGain = 0
if (n_elements(diagnostics) eq 0) then diagnostics = 0
if (n_elements(maxiter) eq 0) then maxiter = 100
if (n_elements(costStop) eq 0) then costStop = .02
p1 = sqrt(total(phiw^2*pupil))
pw = phiw
cost = compCost(eww,pupil,phiw,f,p1,phihat,pw); sqrt(total(pw^2))/p1
if (diagnostics) then print,'<extendPhase> initial cost: ',cost
eww_save = eww
gaintrimdown = .8
gaintrimup = 1.08
iter = 0
while (cost gt costStop) do begin
  iter = iter+1
  if (iter gt maxiter) then goto, exit
;  compute direction
  ew_f = ft(pw) / ((f)>1e-30)
  ew_f[n/2,n/2] = 0
  ew = real(ft(ew_f,/inverse))
;  take a step
  eww_save = eww
  eww = eww + gain*ew*pupil
  oldcost = cost
  cost = compCost(eww,pupil,phiw,f,p1,phihat,pw)
  if (cost gt oldcost*1.2) then begin
    phihat = phiw*pupil + phihat*(1-pupil)
    goto, exit2
  endif
  if (not diagnostics) then begin
    if (n_elements(percent_done) eq 0) then percent_done = -1
    new_percent_done = fix((iter/float(maxiter))*20.)
    if (new_percent_done ne percent_done) then begin
      if (new_percent_done eq 0) then print,format='($,"percent complete: ")'
      print,format='($,I0," ")',new_percent_done*5
      percent_done = new_percent_done
      wait,.001
    endif
  endif
  if (diagnostics) then print,'<extendPhase> ',iter,cost & wait,.001
;  pw_save = pw
;  pw = (phi-phihat)*pupil
;  oldcost = cost
;  cost = compCost(eww,pupil,phiw,p1); sqrt(total(pw^2))/p1
;  print,j,cost & wait,.001
  whilecount = 0
  maxwhilecount = 10
  if (adaptGain) then begin
  while (cost gt oldcost) do begin
    if (diagnostics) then print,'<extendPhase> ',iter,cost,oldcost,'stepping down to gain ',gain*gaintrimdown
    gain = gain*gaintrimdown
    eww = eww_save + gain*ew*pupil
    cost = compCost(eww,pupil,phiw,f,p1,phihat,pw)
    whilecount = whilecount+1
    if (whilecount gt maxwhilecount) then goto, exit
  endwhile
  gain = gain*gaintrimup
  if (diagnostics) then print,'<extendPhase> ',iter,'drifting gain up to ',gain
  endif
;  phihat = real(ft(ft(eww)*f,/inverse))
endwhile
if (diagnostics) then print,'<extendPhase> stopping because cost < cost goal of ',strtrim(costStop,2)
print,'done'
;
return,phihat
exit:
print,'done'
print,'<extendPhase> WARNING gave up due to max iteration count exceeded, cost = ',oldcost,cost,'target = ',costStop
return,phihat
exit2:
print,'done'
print,'<extendPhase> WARNING gave up due to cost increasing, cost = ',cost,oldcost,' target = ',costStop
return,phihat
end

;  true screen
microns = 1.0e-6
D_tele = 10.0
n = 128
r0 = 0.12
lambda0 = 0.5*microns
du = D_tele*2./float(n)
pupil = circle(n,n,n/2,n/2,n/4,1.)
f = screengen(n,n,r0,du)*(lambda0/(2*!pi))
if (n_elements(seed) eq 0) then seed = 5
phi = screengen(f,seed)
;  windowed screen
phiw = pupil*phi
phihat = extendPhase(phiw,f,pupil,eww,/verbose,target = .01,maxiter=1000)
;
disp,phi/microns,'phi (microns)'
disp,phi*pupil/microns,ap=pupil,'phi*pupil (microns)'
disp,phihat/microns,'phihat (microns)'
disp,(phi-phihat)/microns,'phi-phihat (microns)'

end
