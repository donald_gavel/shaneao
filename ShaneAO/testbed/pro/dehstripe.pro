;
;  remove horizontal stripes
;

pro dehstripe,datain,dataout,xspan
  if n_params() eq 0 then begin
    print,"usage: dehstripe,datain,dataout,[xspan]"
    return
  end
  if n_elements(xspan) eq 0 then xspan = [0,99]
  na = xspan(1) - xspan(0) + 1
  nx = (size(datain))(1)
  ny = (size(datain))(2)
  if ((size(datain))(0) eq 2) then n = 1 else n = (size(datain))(3)
  print,"dehstripe..."
  if n_elements(dataout) eq 0 then dataout = make_array(nx,ny,n)
  bra = make_array(nx,ny)
  for i=0,(n-1) do begin
    print,format='($,I0," ")', i
    br = total(datain(xspan(0):xspan(1),0:(ny-1),i),1)/na  ; horizontal stripes
    for j=0,(nx-1),1 do bra(j,*) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end
