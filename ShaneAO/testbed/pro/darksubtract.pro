;+
;  darksubtract - remove an averaged dark frame from each element of a
;         speckle dataset
;
;  USAGE:
;    result = darksubtract(data,dark)
;
;    dark can be a data cube, in which case, the frame-average is taken
;      or it can be the (already frame-averaged) dark frame
;-
function darksubtract,data,dark
  if n_params() eq 0 then begin
    print,'usage: result = darksubtract(data,dark)'
    return,0
  end
  s = size(dark)
  if (s(0) eq 3) then begin
    nf = s(3)
    print,format='($,A,I0,A)','<darksubtract> averaging ',nf,' darks...'
    dark1 = total(dark,3)/float(nf)
    print,'done'
  endif
  s = size(data)
  n = s(1)
  m = s(2)
  nf = s(3)
  datamdark = make_array(n,m,nf)
  print,format='($,A,I0,A)','<darksubtract> subtracting from ',nf,' frames '
  for i=0,nf-1 do begin
    datamdark(*,*,i) = data(*,*,i) - dark1
    print,format='($,I0," ")',i
  endfor
  print
  return,datamdark
end
