; angle.pro - return the angle of a complex number
;
function angle,a
  fuzz = 1e-9
  ra = real(a)
;  ra(where(ra eq 0)) = fuzz
  return, atan(imaginary(a),ra)
end
