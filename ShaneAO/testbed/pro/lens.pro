;+
;  lens.pro - create a phase screen equivalent to a lens
;            of a given focal length
;
;  Usage:
;     ph = lens(n,dx,f,lambda,ap,check)
;
;  Inputs:
;     n - dimension of screen
;     dx - sample spacing
;     f - focal length
;     lambda - wavelength
;     ap - (optional) aperture - used only for checking phase wrap
;     check - keyword to check for phase wrapping.  needs ap
;     offset - 2-vector of x,y offset for lens center, in same units as dx (default 0,0)
;
;  Output:
;     ph - phase screen, real n X n
;-
function lens,n,dx,f,lambda,ap,check=check,offset=offset
  if (n_elements(offset) eq 0) then offset = [0.,0.]
  x = (findgen(n)-n/2)*dx # (fltarr(n)+1)
  y = transpose(x)
  r2 = (x-offset[0])^2 + (y-offset[1])^2
  ph = (2*!pi/lambda) * r2 / (2*f)
  if (keyword_set(check)) then begin
    dph = ap*(ph-shift(ph,1))
    print,'<lens> check for phase wrapping (must be < pi) ',max(abs(dph))
  endif
;  if (n_elements(ap) ne 0) then ph = ph*ap
  return,ph
end
