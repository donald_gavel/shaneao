;
; Auto Save File For ./specklegui2.pro
;
;  Wed Oct  1 23:12:44 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




PRO specklegui2_Event, Event

  common specklegui2com, state

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  widget_control,event.top,get_uvalue = uv
  state = uv.state
  entry = uv.entry

  CASE Ev OF 

  'FIELD15': BEGIN
;      Print, 'Event for Directory:'
      END
  'FIELD16': BEGIN
;      Print, 'Event for Basename:'
      END
  'BUTTON17': BEGIN
;      Print, 'Event for Compute Object Bispectrum'
      widget_control, entry.objectdirectory, get_value = dir
      dir = dir(0)
      widget_control, entry.objectbasename, get_value = basename
      basename = basename(0)
      if fileexists(dir + basename + '.spec') then begin
        s = specfilesize(dir + basename + '.spec')
        res = 'Yes'
        if fileexists(dir + basename + '_ps.sdt') then begin
          res = widget_message(['overwrite exiting file', $
                         dir + basename + '_ps and _phase ?'], $
              /question)
        endif
        if res eq 'Yes' then begin
          widget_control,/hourglass
          param = writeframep(basename + '.spec', basename, $
                      dir + '.frameinput', $
                      frames = s(2), size = [s(0),s(1)])
;          home = '~/gavel/'
          spawn,'(cd ' + dir + '; ' + !home + 'bin/frame.exe < .frameinput)'
          print,'done'
        endif
      endif else begin
        res = widget_message(['file ' + dir + basename + '.spec', $
                              'not found'],/error)
      endelse
      END

  'BUTTON18a': BEGIN
;      Print, 'Event for Look at Object Spectrum'
      widget_control, entry.objectdirectory, get_value = dir
      dir = dir(0)
      widget_control, entry.objectbasename, get_value = basename
      basename = basename(0)
      if fileexists(dir + basename + '_ps.sdt') then begin
        ps = rdview(dir + basename + '_ps')
        phase = rdview(dir + basename + '_phase')
        i = complex(0,1.)
        ps_c = ps*exp(i*phase)
        ps_c = hermtofull(ps_c)
        ps = abs(ps_c)
        phase = angle(ps_c)
        minval = abs(min(ps))/10.
        lps = alog10(ps > minval)
        tmp = [lps,phase]
        dispgui,tmp,'Object Spectrum',size=[512,256]
      endif else begin
        res = widget_message(['file ' + dir + basename + '_ps.sdt', $
                              'not found'],/error)
      endelse
      END

  'FIELD23': BEGIN
;      Print, 'Event for Directory: '
      END
  'FIELD24': BEGIN
;      Print, 'Event for Basename:'
      END
  'BUTTON25': BEGIN
;      Print, 'Event for Compute Star Power Spectrum'
      widget_control, entry.stardirectory, get_value = dir
      dir = dir(0)
      widget_control, entry.starbasename, get_value = basename
      basename = basename(0)
      if fileexists(dir + basename + '.spec') then begin
        s = specfilesize(dir + basename + '.spec')
        res = 'Yes'
        if fileexists(dir + basename + '_ps.sdt') then begin
          res = widget_message(['overwrite exiting file', $
                         dir + basename + '_ps ?'], $
              /question)
        endif
        if res eq 'Yes' then begin
          widget_control,/hourglass
          param = writeframep(basename + '.spec', basename, $
                      dir + '.frameinput', $
                      frames = s(2), size = [s(0),s(1)])
;          bindir = '~/gavel/bin/'
          spawn,'(cd ' + dir + '; ' + !home + 'bin/frame.exe < .frameinput)'
          print,'done'
        endif
      endif else begin
        res = widget_message(['file ' + dir + basename + '.spec', $
                              'not found'],/error)
      endelse
      END

  'BUTTON26a': BEGIN
;      Print, 'Event for Look at Star Spectrum'
      widget_control, entry.stardirectory, get_value = dir
      dir = dir(0)
      widget_control, entry.starbasename, get_value = basename
      basename = basename(0)
      if fileexists(dir + basename + '_ps.sdt') then begin
        ps = rdview(dir + basename + '_ps')
        ps_c = complex(ps,0*ps)
        ps_c = hermtofull(ps_c)
        ps = abs(ps_c)
        minval = abs(min(ps))/10.
        lps = alog10(ps > minval)
        dispgui,lps,'Star Spectrum'
      endif else begin
        res = widget_message(['file ' + dir + basename + '_ps.sdt', $
                              'not found'],/error)
      endelse
      END

  'BUTTON28': BEGIN
;      Print, 'Event for Deconvolve'
      handle_value,state.dataset,dataset,/no_copy
      thefile = dataset.resultsdir + dataset.basename + '_ps.sdt'
      sorry = 0
      if (fileexists(thefile)) then begin
        ps = rdview(dataset.resultsdir + dataset.basename + '_ps')
        phase = rdview(dataset.resultsdir + dataset.basename + '_phase')
      endif else begin
        sorry = 1
        res = widget_message(['file '+ thefile,'not found'],/error)
      endelse
      thefile = dataset.resultsdir + dataset.starname + '_ps.sdt'
      if (fileexists(thefile)) then begin
        ref_ps = rdview(dataset.resultsdir + dataset.starname + '_ps')
;        ref_phase = rdview(dataset.resultsdir + dataset.starname + '_phase')
        ref_phase = ref_ps*0
      endif else begin
        sorry = 1
        res = widget_message(['file '+ thefile,'not found'],/error)
      endelse
      if (fileexists(dataset.mtffile)) then begin
        mtf = readfits(dataset.mtffile)
      endif else begin
        sorry = 1
        res = widget_message(['file '+ dataset.mtffile,'not found'],/error)
      endelse

      if not sorry then begin
        widget_control,/hourglass
        image = spdeconvolve(ps, phase, ref_ps, ref_phase, mtf, 0, 128, .99)
        print,'done'
      endif

      if handle_info(state.image,/valid_id) then begin
        handle_free,state.image
        state.image = 0
      endif
      state.image = handle_create(value = image,/no_copy)
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON89': BEGIN
;      Print, 'Event for Look at Deconvolution Result'
      handle_value,state.dataset,dataset,/no_copy
      if handle_info(state.image,/valid_id) then begin
        handle_value,state.image,image,/no_copy
        dispgui,image,dataset.basename
        handle_value,state.image,image,/no_copy,/set
      endif else begin
        res = widget_message('need to do deconvolution first',/error)
      endelse
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON30': BEGIN
;      Print, 'Event for Save'
      handle_value,state.dataset,dataset,/no_copy
      if handle_info(state.image,/valid_id) then begin
        handle_value,state.image,im,/no_copy
        res = 'Yes'
        if fileexists(dataset.resultsdir + dataset.basename + '_image.fits') then begin
        res = widget_message(['overwrite existing file', $
                 dataset.resultsdir + dataset.basename + '_image.fits?'],$
               /question)
        endif
        if res eq 'Yes' then begin
          print,'writing image file...'
          writefits,dataset.resultsdir + dataset.basename + '_image.fits',im
        endif
        handle_value,state.image,im,/no_copy,/set
      endif else begin
        res = widget_message('need to deconvolve first',/error)
      endelse
      handle_value,state.dataset,dataset,/no_copy,/set
      END
  'BUTTON31': BEGIN
;      Print, 'Event for Done'
      widget_control,event.top,/destroy
      return
      END
  ENDCASE

  if n_elements(dataset) ne 0 then $
    handle_value,state.dataset,dataset,/set,/no_copy
  uv.state = state
  widget_control,event.top,set_uvalue = uv

END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO specklegui2, thestate, GROUP=Group

  common specklegui2com, state

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }


  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      UVALUE={name:'MAIN13'})

  LABEL2 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL2', $
      FONT='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      VALUE='Speckle Processing')

  LABEL2a = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL2a', $
      FONT='-adobe-times-medium-i-normal--14-140-75-75-p-74-iso8859-1', $
      VALUE='Bispectrum and Power Spectrum Calculation')

  BASE3 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='data and star 2 columns', $
      UVALUE='BASE3')

  BASE5 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='data', $
      UVALUE='BASE5')

  LABEL80 = WIDGET_LABEL( BASE5, $
      UVALUE='LABEL80', $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      VALUE='Object')

  BASE7 = WIDGET_BASE(BASE5, $
      ROW=1, $
      MAP=1, $
      TITLE='top region', $
      UVALUE='BASE7')

  BASE14 = WIDGET_BASE(BASE7, $
      COLUMN=1, $
      MAP=1, $
      TITLE='directory and name', $
      UVALUE='BASE14')

  FieldVal15434 = [ $
    '' ]
  FIELD15 = CW_FIELD( BASE14,VALUE=FieldVal15434, $
      ROW=1, $
      STRING=1, $
      TITLE='Directory:', $
      UVALUE='FIELD15')

  FieldVal15436 = [ $
    '' ]
  FIELD16 = CW_FIELD( BASE14,VALUE=FieldVal15436, $
      ROW=1, $
      STRING=1, $
      TITLE='Basename:', $
      UVALUE='FIELD16')



  BASE8 = WIDGET_BASE(BASE5, $
      ROW=1, $
      MAP=1, $
      TITLE='2 buttons', $
      UVALUE='BASE8')

  BUTTON17 = WIDGET_BUTTON( BASE8, $
      UVALUE='BUTTON17', $
      VALUE='Compute Bispectrum')

;  BUTTON18 = WIDGET_BUTTON( BASE8, $
;      UVALUE='BUTTON18', $
;      VALUE='or Load...')

  BUTTON18a = widget_button( BASE8, $
      UVALUE = 'BUTTON18a', $
      VALUE = 'Look')

;  BASE9 = WIDGET_BASE(BASE5, $
;      ROW=1, $
;      MAP=1, $
;      TITLE='2 displays', $
;      UVALUE='BASE9')

;  DRAW19 = WIDGET_DRAW( BASE9, $
;      RETAIN=0, $
;      UVALUE='DRAW19')

;  DRAW20 = WIDGET_DRAW( BASE9, $
;      BUTTON_EVENTS=1, $
;      RETAIN=0, $
;      UVALUE='DRAW20')



  BASE6 = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      FRAME=2, $
      MAP=1, $
      TITLE='star', $
      UVALUE='BASE6')

  LABEL82 = WIDGET_LABEL( BASE6, $
      UVALUE='LABEL82', $
      FONT='-adobe-times-medium-r-normal--14-140-75-75-p-74-iso8859-1', $
      VALUE='PSF Star')

  BASE10 = WIDGET_BASE(BASE6, $
      ROW=1, $
      MAP=1, $
      TITLE='star top region', $
      UVALUE='BASE10')

  BASE22 = WIDGET_BASE(BASE10, $
      COLUMN=1, $
      MAP=1, $
      TITLE='directory and name', $
      UVALUE='BASE22')

  FieldVal15448 = [ $
    '' ]
  FIELD23 = CW_FIELD( BASE22,VALUE=FieldVal15448, $
      ROW=1, $
      STRING=1, $
      TITLE='Directory: ', $
      UVALUE='FIELD23')

  FieldVal15450 = [ $
    '' ]
  FIELD24 = CW_FIELD( BASE22,VALUE=FieldVal15450, $
      ROW=1, $
      STRING=1, $
      TITLE='Basename:', $
      UVALUE='FIELD24')



  BASE11 = WIDGET_BASE(BASE6, $
      ROW=1, $
      MAP=1, $
      TITLE='star 2 buttons', $
      UVALUE='BASE11')

  BUTTON25 = WIDGET_BUTTON( BASE11, $
      UVALUE='BUTTON25', $
      VALUE='Compute Power Spectrum')

;  BUTTON26 = WIDGET_BUTTON( BASE11, $
;      UVALUE='BUTTON26', $
;      VALUE='or Load...')

  BUTTON26a = WIDGET_BUTTON( BASE11, $
      UVALUE='BUTTON26a', $
      VALUE='Look')

;  BASE12 = WIDGET_BASE(BASE6, $
;      ROW=1, $
;      MAP=1, $
;      TITLE='star display region', $
;      UVALUE='BASE12')

;  DRAW27 = WIDGET_DRAW( BASE12, $
;      RETAIN=0, $
;      UVALUE='DRAW27')




  BASE4 = WIDGET_BASE(MAIN13, $
      ROW=1, $
      MAP=1, $
      TITLE='bottom buttons', $
      UVALUE='BASE4')

  BUTTON28 = WIDGET_BUTTON( BASE4, $
      UVALUE='BUTTON28', $
      VALUE='Deconvolve')

  BUTTON89 = WIDGET_BUTTON( BASE4, $
      UVALUE='BUTTON89', $
      VALUE='Look')

  BUTTON30 = WIDGET_BUTTON( BASE4, $
      UVALUE='BUTTON30', $
      VALUE='Save...')

  BUTTON31 = WIDGET_BUTTON( BASE4, $
      UVALUE='BUTTON31', $
      VALUE='Done')

  entryset = {objectdirectory: field15, $
              objectbasename: field16, $
              stardirectory: field23, $
              starbasename: field24}

  if (n_elements(thestate) eq 0) then begin
    thestate = {dataset:0,  $
                objdata:0, objdarkdata:0, objmdark:0, skyframe:0, $
                stardata:0, stardarkdata:0, starmdark:0, starskyframe:0, $
                badpix:0, mtf:0, flatfield:0, $
                speckledata:0, specklestar:0, image:0}
    dataset = getTitanDatasetDef()
    thestate.dataset = Handle_create(value = dataset,/no_copy)
  endif

  handle_value,thestate.dataset,dataset,/no_copy
  widget_control,entryset.objectdirectory,set_value = dataset.resultsdir
  widget_control,entryset.objectbasename, set_value = dataset.basename
  widget_control,entryset.stardirectory,  set_value = dataset.resultsdir
  widget_control,entryset.starbasename,   set_value = dataset.starname
  handle_value,thestate.dataset,dataset,/set,/no_copy

  WIDGET_CONTROL, MAIN13, /REALIZE

  widget_control, main13, get_uvalue = uv
  uv = {name:uv.name, entry: entryset, state: thestate}
  widget_control, main13, set_uvalue = uv

  XMANAGER, 'specklegui2', MAIN13, /modal

  thestate = state;
  return  
END
