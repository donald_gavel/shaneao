;
;  hartmannlib.pro - everything needed to processes hartmann data
;
devicelib
imagelib
defsysv,'!filepath','~/llnl/gavel/'
print,'% PIBLIB: Added system variable !FILEPATH
.compile hs.pro
.compile hartmannpro.pro
.compile hartmanngui.pro
.compile hartmannparamgui.pro
.compile hartmannwfgui.pro
.compile writefits.pro
