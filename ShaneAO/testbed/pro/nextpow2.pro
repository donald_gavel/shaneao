;+
;  nextpow2 - find the smallest power of 2 less or equal to a given number
;
;  USAGE:
;    n = nextpow2(n0)
;-
function nextpow2,n0
  if ((size(n0))[0] eq 0) then begin
    n = 2;
    while n lt n0 do n = n*2
  endif else begin
  	count = (size(n0))[1]
  	n = intarr(count)
  	for k=0,count-1 do n[k] = nextpow2(n0[k])
  endelse
  return,n
end
