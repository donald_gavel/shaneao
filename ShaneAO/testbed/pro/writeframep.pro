;
;  writeframep.pro - write a parameter file for the frame (speckle) program
;
;    inputs
;       speckleFileName = usually <basename>.spec
;       outputFileName = usually <basename>; actual output files will be
;                        View format files named *_ps.sdt, *_phase.sdt
;       paramFileName = name of the parameter file (optional, default:
;                          .frameinput)
;    keyword args
;       size  = [fftsizeX, fftsizeY]
;       pruning = [top,left,right,bottom]
;       param = input the dataset (i.e. set all paramters according to it)
;
;    returns a structure with frame parameters
;
;
function writeframep, speckleFileName, outputFileName, paramFileName, $
        size = size, frames = frames, pruning = pruningDim, param = param
  if (n_elements(paramFileName) eq 0) then paramFileName = '.frameinput'
  if (n_elements(size) eq 0) then size = [256,256]
  if (n_elements(frames) eq 0) then frames = 100
  if (n_elements(pruningDim) ne 0) then begin
    pruning = 'y'
  endif else begin
    pruning = 'n'
    pruningDim = [0,0,0,0]
  endelse
  if (n_elements(param) eq 0) then begin
    param = {longExposure:    'n', $ ; 0
             singleFrame:     'y', $
             framesProcess:   frames, $
             computeBispectrum: 'y', $
             psVariance:      'n', $
             bsVariance:      'n', $ ; 5
             numberOfFiles:   1, $
             speckleFile:     speckleFileName, $
             maxFrames:       frames, $
             badFrames:       'done', $
             startFrame:      1, $ ;   10
             headerSize:      96, $
             biasSubtraction: 'n', $
             flatField:       'n', $
             pruning:         pruning, $
             pruningDimension: pruningDim, $ ; 15
             nulling:         'n', $
             fftsize:         [256,256], $
             bispectShift:    [4,4], $
             bsThresh:        1.0e-5, $
             windowing:       'n', $ ;  20
             edgeBoundaryLimit:  1, $
             readNoise:       60, $
             difflim:         128, $
             ccdGain:         3.3, $
             outputFile:      outputFileName  $ ; 25
    }
  endif
  openw,1,paramFileName
  for i=0,14 do begin
    printf,1,param.(i)
  endfor
  if param.pruning eq 'y' then begin
    printf,1,param.pruningDimension(0)
    printf,1,param.pruningDimension(1)
    printf,1,param.pruningDimension(2)
    printf,1,param.pruningDimension(3)
  endif
  printf,1,param.nulling
  printf,1,param.fftsize(0)
  printf,1,param.fftsize(1)
  printf,1,param.bispectShift(0)
  printf,1,param.bispectShift(1)
  for i= 19,25 do begin
    printf,1,param.(i)
  endfor
  close,1
  return,param
end
