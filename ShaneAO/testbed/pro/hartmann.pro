;+
;  Hartmann.pro
;    Routines for reconstructing wavefronts from Hartmann sensor measurements
;    
;-
function hartmann_reconstruct,s,R
  a = R ## s
  return,a
end
;
function hartmann_poke,apa,aps,geometry=geometry
;  poke "calibrator"
  nax = (size(apa))[1]
  nay = (size(apa))[2]
  nsx = (size(aps))[1]
  nsy = (size(aps))[2]
  
  if (n_elements(geometry) eq 0) then geometry = 'Fried'  ; the default geometry
  if (geometry ne 'Fried') then begin
    print,'<hartmann_poke> ERROR sorry, only Fried geometry is supported'
    return,0
  endif
  nsx_ = max([nsx,nax+1])
  nsy_ = max([nsy,nay+1])
  aps_ = fltarr(nsx_,nsy_)
  aps_[0:nsx-1,0:nsy-1]=aps

  Sx = fltarr(nsx_,nsy_,nax,nay)
  Sy = fltarr(nsx_,nsy_,nax,nay)
    
  for i = 0, nax-1 do begin
    for j = 0, nay-1 do begin
    
      Sx[i,j,i,j] = -1.
      if (j gt 0) then Sx[i,j-1,i,j] = -1.
      if (i gt 0) then Sx[i-1,j,i,j] = 1.
      if ((i gt 0) and (j gt 0)) then Sx[i-1,j-1,i,j] = 1.
      
      Sy[i,j,i,j] = -1.
      if (j gt 0) then Sy[i,j-1,i,j] = 1.
      if (i gt 0) then Sy[i-1,j,i,j] = -1.
      if ((i gt 0) and (j gt 0)) then Sy[i-1,j-1,i,j] = 1.
      
    endfor
  endfor
  
  Sx = reform(Sx,nsx_*nsy_,nax*nay)
  Sy = reform(Sy,nsx_*nsy_,nax*nay)
  ida = where(apa)
  na = (size(ida))[1]
  ids = where(aps_)
  ns = (size(ids))[1]
  Sx_reduced = fltarr(ns,na)
  Sy_reduced = fltarr(ns,na)
  for i = 0, ns-1 do begin
    for j = 0, na-1 do begin
      Sx_reduced[i,j] = Sx[ids[i],ida[j]]
      Sy_reduced[i,j] = Sy[ids[i],ida[j]]
    endfor
  endfor
  S = [Sx_reduced,Sy_reduced]
  return,S
end

function hartmann_controlMatrix,S,thresh=thresh
  ; this computes the control matrix given the poke matrix
  ; thresh is the singular value threshold (relative to largest sv)
  if (n_elements(thresh) eq 0) then thresh = 0.15
  n = min((size(S))[1:2])
  C = pseudoinverse(S,thresh,modeCount,w=w,u=u,v=v)
  print,'<Hartmann_controlMatrix> number of modes retained: ',strtrim(modeCount,2),' of ',strtrim(n,2)
  return,C
end

function hartmann_modeView,S,thresh=thresh
  ; display the orthogonal singular modes of the poke matrix
  if (n_elements(thresh) eq 0) then thresh = 0.15
  ss = (size(S))[1:2]
  if (ss[0] gt ss[1]) then begin
    return,transpose(hartmann_modeView(transpose(S),thresh=thresh))
  endif
  n = min(ss)
  m = max(ss)
  na = round(sqrt(n))
  ns = round(sqrt(m/2))
  C = pseudoinverse(S,thresh,modeCount,w=w,u=u,v=v)
  ; assuming S is m x n, m > n, m = #subaps x 2, n = #pokes
  ; w = singular values, n
  ; u = measurement space singular vectors, n m-vectors
  ; v = poke space singular vectors, n n-vectors
  idx = reverse(sort(w))
  window,/free
  plot,w[idx],/ylog,psym=1,title='singular values'
  v_modes = reform(transpose(v[idx,*]),na,na,n)
  u_modes_x = reform(transpose(u[idx,0:m/2-1]),ns,ns,n)
  u_modes_y = reform(transpose(u[idx,m/2:m-1]),ns,ns,n)
  disp,v_modes,'poke space modes'
  disp,u_modes_x,'measurement space modes - X'
  disp,u_modes_y,'measurement space modes - Y'
  return,C
end


;  =============== sample code ==============
;
;  GPI's CAL system LOWFS
;

test_no = 2

if (test_no eq 3) then begin
; Lick 3m case
  aps = circle(8,8,3.5,3.5,3.6,1)
  apa = circle(9,9,4,4,4.5,1)
  aps[3:4,3:4] = 0
  s = hartmann_poke(apa,aps,geometry='Fried')
  disp,s,'Poke Matrix, S'
  c = hartmann_controlMatrix(S,thresh=.15)
  disp,c,'Reconstruction Matrix, C'  
endif

if (test_no eq 1) then begin
  ns = 7
  na = 8
  
  apa = circle(na,na,na/2-.5,na/2-.5,na/2,1)
  aps = circle(ns,ns,ns/2,ns/2,ns/2+.5,1)
  
  s = hartmann_poke(apa,aps,geometry='Fried')
  disp,s,'Poke Matrix, S'
  c = hartmann_controlMatrix(S,thresh=.15)
  disp,c,'Reconstruction Matrix, C'
endif

if (test_no eq 2) then begin
  ; sinc influence functions
  ; 
  ; 1) set up the geometry, aperture and subaperture definitions
  
  p = 40   ; pitch of subapertures
  n_across = 7 ; LOWFS subaps across primary
  dtele = 8. ; Gemini primary diameter, meters
  dx = dtele/(float(n_across)*float(p)) ; fine pixel size in meters
  dy = dx
  n = dtele*1.2/dx
  x = ones(n) # (findgen(n) - n/2)*dx
  y = transpose(x)
  ap = circle(n,n,n/2-0.5,n/2-0.5,dtele/2./dx,1.)
  subap = rectangle(p,p,p/2,p/2,p-2,p-2,1.)
  
  ; 2) display the subaperture locations - just a check
  w = ap*0
  i1 = -3
  i2 = +3
  j1 = -3
  j2 = +3
  for i = i1,i2 do begin
    x = i*p*dx
    ix = (n/2) + x/dx
    for j = j1,j2 do begin
       y = j*p*dy
       jy = (n/2) + y/dx
       w[ix-p/2:ix+p/2-1,jy-p/2:jy+p/2-1] = subap
    endfor
  endfor
  im = {data:ptr_new(w+ap),dx:[1,1]*dx,x0:[1,1]*(-n/2)*dx}
  disp,im
  
  ; 3) create each acuator push and its gradient
  ;
  x = (findgen(n) - n/2 + 0.5)*dx # ones(n)
  y = transpose(x)
  eps = dx/1000.
  x_ = x+eps
  y_ = y+eps
  ds = dx*p
  sinc = (sin(2*!pi*x_/ds)/(2*!pi*x_/ds))*(sin(2*!pi*y_/ds)/(2*!pi*y_/ds))
  dsincx = (cos(2*!pi*x_/ds)/x_ - sin(2*!pi*x_/ds)/(2*!pi*x_^2/ds))*(sin(2*!pi*y_/ds)/(2*!pi*y_/ds))
  dsincy = (sin(2*!pi*x_/ds)/(2*!pi*x_/ds))*(cos(2*!pi*y_/ds)/y_ - sin(2*!pi*y_/ds)/(2*!pi*y_^2/ds))
  
  na_across = 8
  alocx = findgen(na_across)*ds - dtele/2.
  alocy = alocx
  
  pd = fltarr(n,n,na_across,na_across)
  pdx = pd
  pdy = pd
  for i = 0,na_across-1 do begin
    x = alocx[i]
    ix = round(x/dx)
    for j = 0,na_across-1 do begin
      y = alocy[j]
      iy = round(y/dy)
      pd[*,*,i,j] = shift(sinc,ix,iy)
      pdx[*,*,i,j] = shift(dsincx,ix,iy)
      pdy[*,*,i,j] = shift(dsincy,ix,iy)
    endfor
  endfor
  
  ; 4) for every actuator push, determine the average slope in a subaperture and fill in the system push matrix
  ;
  n_across = 7
  slocx = findgen(n_across)*ds - dtele/2.
  slocy = slocx
  
  subap = fltarr(n,n)
  nf = float(p)*float(p)
  sx = fltarr(n_across,n_across,na_across,na_across)
  sy = fltarr(n_across,n_across,na_across,na_across)
  
  for i = 0,na_across-1 do begin
    print,'i = ',strtrim(i,2),' / ',strtrim(na_across,2) & wait,.01
    x = alocx[i]
    for j = 0,na_across-1 do begin
      y = alocy[j]
      for k = 0,n_across-1 do begin
        xs = slocx[k]
        ixs = xs/dx+n/2
        for l = 0,n_across-1 do begin
          ys = slocy[l]
          iys = ys/dx+n/2
          subap *= 0
          subap[ixs:ixs+p-1,iys:iys+p-1] = 1
          sx[k,l,i,j] = total(subap*pdx[*,*,i,j])/nf
          sy[k,l,i,j] = total(subap*pdy[*,*,i,j])/nf
        endfor
      endfor
    endfor
  endfor
  s = [reform(sx,n_across*n_across,na_across*na_across),reform(sy,n_across*n_across,na_across*na_across)]
  disp,s,'Poke Matrix, S'
  c = hartmann_controlMatrix(S,thresh=.15)
  disp,c,'Reconstruction Matrix, C'
      
endif

end

   