;+
;  showdm.pro - display the state of the deformable mirror
;
;   usage:
;     .compile lickdm.pro
;     ph = showdm(basis,file='offsetMirrorCalc_....dat')
;
;   inputs:
;     basis - optional. showdm will compute this the first time
;             showdm is called.  It takes a while to compute basis
;             so it is passed out of the routine, and can be reused
;             on the second and subsequent calls.
;     file - optional. If not given, the present DM flat file,
;             offsetMirrorCalc.dat, is read in.  You can choose any of the
;             other files - the ones with dates appended.
;
;   outputs:
;     ph - the phase map that approximates the "flat" mirror shape.
;
;     showdm prints out the fit of a focus component to the phase map.
;
;-
function showdm,basis,file=file
  forward_function dmgeom,makebasis,pushact,getmirror,zernike,lickdm
  dum = lickdm() ; forces compilation of lickdm.pro
  microns = 1.e-6
  arcsec = !pi/(180.*3600.)
  nm = 1.e-9
  cm = 1.e-2
  n = 128
  lambda = 2.2*microns
  pixelscale = .076*arcsec
  dx = lambda/(n*pixelscale)
  if (n_elements(basis) eq 0) then begin
    dmgeo = dmgeom()
;    dmgeo[*,1] = dmgeo[*,1] - 9*dx ;  this is the apparent mis-registration
;    dmgeo[*,0] = dmgeo[*,0] - dx 
    sigma=35.*cm
    a = makebasis(dmgeo,dx,ap=ap,basis=basis,sigma=sigma)
  endif
  if (n_elements(file) ne 0) then begin
    actv = getmirror(file)
  endif else begin
    actv = getmirror()
  endelse
  actrad = actv*1.75
  ph = pushact(actrad,basis)
  phnm = ph*(lambda/(2*!pi))/nm
;   analyze focus term
  focus = real(zernike(n,n,(3./(2*dx)),2,0))
  if (n_elements(ap) eq 0) then begin
    dtele = 3.0/dx
    dsec = 0.8/dx
    ap = circle(n,n,n/2,n/2,dtele/2,1.) - circle(n,n,n/2,n/2,dsec/2,1.)
  endif
  focusTerm = total(focus*phnm*ap)/total(focus*focus*ap)
  print,'focus term = ',focusTerm,' nm'
  return,phnm
end
