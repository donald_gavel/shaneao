;         --- phase diversity iteration ---
x = ft(ur*focusKernal)*float(n);  forward propagate
xr = xmag*exp(i*angle(x));  replace magnitude with data
u = (ft(xr,/inverse)/focusKernal)/float(n);  reverse propagate
ur = umag*exp(i*angle(u)); replace magnitude with fixed illumination
;
print,sqrt(total(abs(x)^2-abs(xmag)^2))
