;
;  gauss2.pro - form a gaussian intensity profile
;

function gauss2,n,cx,cy,stddev
  result = make_array(n,n)
  x = extend(indgen(n),2,n)
  y = transpose(x)
  result = exp( - ((x-cx)^2 + (y-cy)^2)/stddev^2 )
  return,result
end
