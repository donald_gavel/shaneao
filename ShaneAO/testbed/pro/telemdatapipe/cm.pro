pro cm,smfile,basename,sig

;program to make control matrices from a measured system matrix
;
; Inputs: smfile = system matrix file (including path if necessary)
;	basename = filename base string for output files
;	sig = signal = number of photons in a fully illuminated subap
;
; Outputs:  a number of control matrices with various weighting
;	for NGS and LGS observations
;
; Example: 
;	@/u/lgs/start.pro  
;	.compile wlscm
;	cm,'sm_000','CMApr28_00',1500.


;set variables
thresh=0.15
frac=1.0
frac2=0.5
frac3=0.3

;read in system matrix
sm=rdview(smfile)

;make uniformly weighted matrix
weights = weighting('uniform',frac,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename,cm

;make weighted least squares matrix
weights = weighting('illum',frac2,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename+'wls',cm

;make weighted least squares matrix 3
weights = weighting('illum',frac3,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename+'wls3',cm

;make laser matrix
weights = weighting('laser',frac3,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename+'laser',cm

;make dimlaser matrix
weights = weighting('dimlaser',frac3,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename+'dimlaser',cm

;make laser weighted least squares matrix
weights = weighting('laserwls',frac3,sig)
cm=wlscm(sm,weights,thresh)
wrview,basename+'laserwls',cm

end