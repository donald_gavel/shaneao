;+
;  gainfunc.pro - function to call for fitting the spot gain
;    to the power spectrum
;-
function func,param
  common telem, psol,pscl,fs,loopGain,intGain
  spotGain = param[0]
  tc = param[1]
  n = (size(psol))(1)/2
  fn = fs/2.
  df = fn / float(n)
  T = 1./float(fs)
  f = findgen(n-1)*df+df ; don't include f=0
  i = complex(0.,1.)
  s = 2*!pi*f*i
  z = exp(s*T)
  cwt1 = -0.5296
  cf = z^2 / ((z - intGain)*(z - cwt1))
  sh = (1-z^(-1))/(s*T)
  stare = (1-z^(-1))/(s*T)
;  tc = .0012
  delay = exp(-s*tc)
  ltr = cf*sh*stare*delay
  n1 = fix(fn/250.)
  n2 = fix(fn/2.5)
  err = total(( (pscl/psol) - abs(1./(1.+loopGain*spotGain*ltr))^2 )[n1:n2]^2)/(float(n2)-float(n1)+1.)
  print,'spotGain = ',spotGain,', tc = ',tc,', err = ',err
  return,err
end
; param = [.9,.0012] ; spot gain
; result = amoeba(.01,function_name='func',p0=param,scale=[.1,.0005])
; 