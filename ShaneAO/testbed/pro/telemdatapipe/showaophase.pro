;+
;  showaophase.pro - Show the AO phase power spectra plots, given
;                 real-time WFS data and the control matrix
;
; usage:
;   ps = showAOphase(cent,cm,fs,/oplot,color=color)
;
; inputs:
;   cent - centroid file name, e.g. 'Jun03/cent_01'
;       Assumed directory is /net/lgs6/home/winkerbean/AO/Data/RunData
;   cm - control matrix file name, e.g. 'CMMay31_00'
;       Assumed directory is /net/lgs6/home/winkerbean/AO/Data/Matrices
;
; options:
;   /oplot - over plot curve on a previous plot
;   color - color of curve
;-
function showaophase,cent,cm,fs,_extra=e
  dir = '/net/lgs6/home/winkerbean/AO/Data/RunData/'
  cmdir = '/net/lgs6/home/winkerbean/AO/Data/Matrices/'
  cm = rdview(cmdir + cm)
  data = rdview(dir + cent)
  ps = aophaseps(data,cm,fs)
  psplot,ps,fs,_extra=e
  return,ps
end
