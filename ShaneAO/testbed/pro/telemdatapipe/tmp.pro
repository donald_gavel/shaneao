; ------ data set defn -----
common telem, psol,pscl,fs,loopGain,intGain
ddir = '/duck5/lickdata/may00/lgs6/may21/'
colFile = 'cent_40'
cclFile = 'cent_39'
intenFile = 'inten_39'
mirrorFile = 'mirror_39'
rawFile = 'rawImage_40'
cmFile = '/duck5/lickdata/sep00/lgs6data/sep20ut/CMMay17_00'
imageFile = '/duck5/lickdata/may00/ircal/May22/ircal0325.fits'
;  AO system configuration -
fs = 500.
loopGain = 0.8
intGain = 0.998
camGain = 2
cwt1 = -0.5296

;  seeing conditions -
seeingK = 0.83*arcsec ; get this from an open loop ircal image ...
; then look up r00 using the graph on http://lgspsdi.llnl.gov/r0vsFWHMtable.html
r00 = .10
; -------- pipeline -------
pipeline,ddir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs,loopGain,intGain,r00,psol,pscl
; --------- control loop model -----------
n = 2048
fn = fs/2.
df = fn / float(n)
T = 1./float(fs)
f = findgen(n-1)*df+df ; don't include f=0
i = complex(0.,1.)
s = 2*!pi*f*i
z = exp(s*T)
cwt1 = -0.5296
cf = z^2 / ((z - intGain)*(z - cwt1))
sh = (1-z^(-1))/(s*T)
stare = (1-z^(-1))/(s*T)
tc = .0012
delay = exp(-s*tc)
ltr = cf*sh*stare*delay
plot,f,smooth((pscl/psol)[1:n-1],5),/xlog,/ylog
spotGain = .6
oplot,f,abs(1./(1.+loopGain*spotGain*ltr))^2,color=150
end
