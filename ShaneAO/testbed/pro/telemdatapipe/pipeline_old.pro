;+
; pipeline.pro
;
;-
pro pipeline,datestr,ddir,colFile,cclFile,intenFile,mirrorFile,cmFile, $
  fs,loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
  r0=r00,micronsPerVolt=phaseScaleFactor, r0only=r0only
;
wno = 0 ; first window number, for plots
;---------------------
;  standard constants
;---------------------
nm = 1.e-9
microns = 1.e-6
cm = 1.e-2
arcsec = !pi/(180.*3600.)
;---------------------
; gather some data
;---------------------
if (type(colFile,/text) eq 'string') then col = rdview(ddir + colFile) else col=colFile
if (type(cclFile,/text) eq 'string') then ccl = rdview(ddir + cclFile) else ccl=cclFile
if (type(mirrorFile,/text) eq 'string') then act = rdview(ddir + mirrorFile) else act=mirrorFile
if (type(intenFile,/text) eq 'string') then inten = rdview(ddir + intenFile) else inten=intenFile
if (type(cmFile,/text) eq 'string') then cmat = rdview(cmFile) else cmat=cmFile
lambda = .7*microns ; average wavelength into wfs
lambda0 = .55*microns
;----------------------------------
; determine the Hartmann sensor SNR
;----------------------------------
; average intensity over illuminated subapertures
isetillum = $
         [ 3,          4,          5,          6,          8,          9,$
          10,         11,         12,         13,         15,         16,$
          17,         18,         21,         22,         23,         24,$
          26,         27,         28,         29,         30,         31,$
          33,         34,         35,         36]
nillum = (size(isetillum))(1)
; measure average illumination in photons/subap/sample
case camGain of
 1:  phdn = 2
 2:  phdn = 1
 else: print,'ERROR: camGain not valid'
endcase
nph = average(inten[isetillum])*phdn
readnoise = 6*phdn
SNR = nph / sqrt(nph + readnoise^2)
print,'<pipeline> SNR = ',SNR
;-----------------------------------------
; Compute the control compensator function
;-----------------------------------------
n = 2049
fn = fs/2.
df = fn / float(n)
T = 1./float(fs)
f = findgen(n)*df ;  include f="0" and f_nyquist
f[0] = df/10.
i = complex(0.,1.)
s = 2*!pi*f*i
z = exp(s*T)
cf = z^2 / ((z - intGain)*(z - cwt1)); compensator
sh = (1-z^(-1))/(s*T) ; sample and hold
stare = (1-z^(-1))/(s*T) ; CCD stare
tc = .0012
delay = exp(-s*tc); compute delay
ltr = cf*sh*stare*delay ; loop transfer function
;
;  Atmospheric phase rejection
;  ---------------------------
;  Atmospheric phase rejection, from the science image, is
;  through the filter 1./(1.+loopGain*ltr), as seen in the following diagram
;
;                               meas noise
;            to science image       |       to telemetry
;                        ^          |       ^
;                 +      |          v       |               H: push matrix
;  atmos phase ---->(+)---->[H]--->(+)--------->[K]----     K: reconstructor matrix
;                  - ^          wf-slope              |     KH=I
;                    |                                |
;                    -----------[ cf(z) ]<-------------
;             phase-correction                  delta-phase-correction
;
;
;  Noise amplification
;  -------------------
;     Noise seen by the telemetry, open loop, is basically measurement noise
;     at high frequencies since the Kolmogorov atmosphere spectrum drops off quickly.
;     For white measurement noise,
;     the high freq noise floor on the slope power spectrum is flat
;     with an amplitude equal to its power spectral density.  We have
;     not calibrated the slope data to angle on the sky, so this density is
;     in "wfs units".
;
;     Phase = K*slope, so there is a high freq noise floor on the phase
;     power spectrum from which we can read off the "measurement noise
;     induced phase noise".  This number is calibrated (via the phaseScaleFactor)
;     to nanometers.  The noise relationship is Phase noise = K*slope noise,
;     so the equivalent diagram has "phase noise" entering after K:
;
;                               meas noise induced
;            to science image        phase "noise"  to telemetry
;                        ^                     |    ^
;                 +      |                     V    |       H: push matrix
;  atmos phase ---->(+)---->[H]------->[K]----(+)------     K: reconstructor matrix
;                  - ^          wf-slope                    KH=I
;                    |
;        0 (no correction open loop)
;
;     Since KH=I, we can, equivalently, move the noise source to before H:
;
;                            meas noise induced
;         to science image   phase "noise"        to telemetry
;                        ^   |                    ^
;                 +      |   V                    |         H: push matrix
;  atmos phase ---->(+)-----(+)---->[H]--->[K]---------     K: reconstructor matrix
;                  - ^               wf-slope               KH=I
;                    |
;                   0
;
;  In closed loop, the measurement-induced-phase noise is amplified by the filter
;  loopGain*ltr/(1.+loopGain*ltr), as seen in the following diagram:
;
;                            meas noise induced
;         to science image   phase "noise"        to telemetry
;                        ^   |                    ^
;                 +      |   V                    |         H: push matrix
;  atmos phase ---->(+)-----(+)---->[H]--->[K]---------     K: reconstructor matrix
;                  - ^               wf-slope         |     KH=I
;                    |                                |
;                    -----------[ cf(z) ]<-------------
;             phase-correction                  delta-phase-correction
;
modelRejection = abs(1./(1.+loopGain*ltr))^2
; <<< Plot Bode Plot of controller loop tranfer function >>>
if (keyword_set(r0only) eq 0) then begin
  window,wno,title='Bode Plot'+string(wno)
  bodePlot,ltr,fs
  wno = wno+1
; <<< Rejection plot >>>
  window,wno,title='Rejection plot'+string(wno)
  rejectionwin = wno
  wno = wno+1
  plot,f[1:n-1],modelRejection[1:n-1],/xlog,/ylog,ytitle='Ratio of closed to open transfer fcn',xtitle='Frequency, Hz',charsize=2
  tickgrid,[f[1],max(f)],[min(modelRejection),max(modelRejection)]
endif
;------------------------------
; measure the actuator variance
;------------------------------
;  isetpupil is the set of actuators located inside the pupil
isetpupil = [6,7,8,9,12,13,14,15,16,19,20,21,22,23,24,27, $
         28,29,31,32,33,35,36,37,38,39,40,41,44,45,46,47,48,51,52,53,54]
n = 1024
npupil = (size(isetpupil))(1)
actave = total(act[isetpupil,*],2)/float(n)
a = act[isetpupil,*] - (actave # ones(n))
actsd = sqrt(total(a^2)/(float(n)*npupil))
;--------------------------------------------------------------------------------
; now, we either calibrate to the given r0, or determine r0 given the calibration
;--------------------------------------------------------------------------------
dtele = 3.0
case 1 of
  (n_elements(phaseScaleFactor) eq 0): begin
    if (n_elements(r00) eq 0) then begin
      print,"<pipeline> ERROR: you need to specify either r0 or the phase scale factor"
      return
    endif
    r0 = r00*(lambda/lambda0)^(6./5.)
;   calculate the theoretical, tilt-removed phase variance, in microns
    opdsdTheory = sqrt(0.34*(dtele/r0)^(5./3.))*(lambda/(2*!pi))/microns
;   here's the scale factor for phase (microns/actuator-unit)
    phaseScaleFactor = opdsdTheory / actsd 
    print,'<pipeline> DM actuator scale: ',phaseScaleFactor, ' microns/actuator-unit'
  end
  (n_elements(phaseScaleFactor) ne 0): begin
    opdsd = actsd*phaseScaleFactor*microns
    phasesd = opdsd*2*!pi/lambda
    r0 = (phasesd^2 / 0.34)^(-3./5.)*dtele
    r00 = r0*(lambda/lambda0)^(-6./5.)
    print,'<pipeline> determined from DM voltages: r0 = ',r00/cm,' cm at ',lambda0/microns,' microns wavelength'
  end
  else: print,'<pipeline> ERROR: phase not calibrated'
endcase
; calculate the fitting error
muDM = 0.27 ; magic number for Lick DM
d = dtele/7.; subaperture diameter at Lick
sigmaDM = sqrt(muDM*(d/r0)^(5./3.))*(lambda/(2*!pi))
print,'<pipeline> sigmaDM = ',sigmaDM/nm,' nm rms'
if keyword_set(r0only) then return
;-----------------------------------------
; compute the "measured" residual spectrum
;-----------------------------------------
psol=aophaseps(col,cmat,fs)*phaseScaleFactor^2 ; (microns^2/hz)
pscl=aophaseps(ccl,cmat,fs)*phaseScaleFactor^2
;  <<< Plot - temporal phase power spectra >>>
pswin = wno
window,wno,title='Power spectrum plot'+string(wno)
wno = wno+1
psplot,psol,fs,title = 'WFS Residual Phase, microns^2/hz'
psplot,pscl,fs,/oplot,color=150
tickgrid,[df,max(fs)],[min(psol),max(psol)]
;
; plot a fitting power law f^(-8/3)
n = 2048
fn = fs/2.
df = fn / float(n)
f = (findgen(n)+1)*df
n1 = fix((fs/50.)/df) & n2 = fix((fs/10.)/df)
loga = average( alog10(2*psol[n1:n2]) + (8./3.)*alog10(f[n1:n2]))
a = 10.^loga
print,'<pipeline> power law fit to Kolmogorov phase spectrum: ',a,' *f^(-8/3) microns^2/Hz'
oplot,f,a*f^(-8./3.)
;
; make some statements about the Greenwood model spectrum, given the fitted power law
; a = 0.077 * r0^(-5/3) * v^(5/3)
arad2 = a * ( 2*!pi / (lambda/microns) )^2
windVelocity = r0 * (arad2/0.077)^(3./5.)
print,'<pipeline> model wind velocity: ',windVelocity,' m/sec'
fg = 0.158 * (windVelocity/r00)
print,'<pipeline> model Greenwood frequency: ',fg, ' Hz'
;----------------------------------------
;  compute the controller rejection curve
;----------------------------------------
rejection = pscl/psol
wset,rejectionwin
plot,f[1:n-1],rejection[1:n-1],/xlog,/ylog,ytitle='Ratio',xtitle='Frequency, Hz',charsize=2,nsum=10,title='Disturbance rejection',subtitle='smoothed data and model'
tickgrid,[f[1],max(f)],[min(rejection[1:n-1]),max(rejection[1:n-1])]
oplot,f[1:n-1],modelrejection[1:n-1],color=150
;----------------------------
; fit the effective loop gain
;----------------------------
common loopGainFit, f_com, ltr_com, rejection_com, ncall_com
n = 2049
fn = fs/2.
df = fn / float(n)
T = 1./float(fs)
f = findgen(n)*df ;  include f="0" and f_nyquist
f_com = f
ltr_com = ltr
rejection_com = rejection
ncall_com = 0
result = amoeba(.001,function_name = 'loopGainFitFunc',p0=[loopGain],scale=[.1],ncalls=30)
apparentLoopGain = result[0]
modelrejection = abs(1./(1.+apparentLoopGain*ltr))^2
; find the crossover frequency
i = 1
while (abs(modelrejection[i]) lt 1.0) do i = i+1
fc = f[i]
; revise the rejection plot
oplot,f[1:n-1],modelrejection[1:n-1],color=200
hartmannSpotSize = (loopGain/apparentLoopGain)*referenceSpotSize
print,'<pipeline> Loop Gain is apparently ',apparentLoopGain
print,'<pipeline> Spot Size is apparently ',hartmannSpotSize,' arcseconds'
;
;---------------------------------------------------
; calculate the closed loop noise averaging factor
;---------------------------------------------------
noiseFilter = apparentLoopGain*ltr / (1. + apparentLoopGain*ltr)
;plot,f[1:n-1],abs(noiseFilter[1:n-1]),/xlog,/ylog
loopAverageFactor = sqrt( total( abs(noiseFilter)^2 * df ) / fs )
print,'<pipeline> closed loop control noise averaging factor = ',loopAverageFactor
;-------------------------------------------------------------------------
; determine the sensor noise component of the measured phase power spectra
;-------------------------------------------------------------------------
;    method 1: from the noise floor of the open loop tilt power spectrum
;
;\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
; NOTE: there is an assumption that the noise floor is the last nav (~200) data points!
;\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
;
nav = 100
pss = aops(col,fs)
sigma2tilt = (average(pss[0,2047-nav:2047]) + average(pss[1,2047-nav:2047])) * fs ; wfs slope units^2
print,'<pipeline> rms wfs noise is ',sqrt(sigma2tilt),' wfs units'
print,'<pipeline> this should be close to 1/SNR = ',1/SNR
;
kk = diagonal(cmat ## transpose(cmat))
kk = average(kk[isetpupil])
Snph_1 = (sigma2tilt/fs)*kk*phaseScaleFactor^2 ;   phase noise spectral density, microns^2 / Hz
print,'<pipeline> measurement noise induced phase open loop, based on slope data, is ',sqrt(Snph_1*fs)*microns/nm,' nm rms'
;
;    method 2: from the noise floor of the open loop phase power spectrum.
;        note: method 2 results in a lower rms phase noise I think because the
;              reconstruction suppresses some modes in the noise (both unreconstructable
;              and specifically removed in the SVD).  These modes are
;              not propagated and hence are not seen in the science wavefront
;
sigma2phase = average(psol[2047-200:2047]) * fs
Snph_2 = (sigma2phase/fs) ; phase noise spectral density, microns^2 / Hz
print,'<pipeline> measurement noise induced phase open loop, based on reconstructed phase, is ',sqrt(sigma2phase)*microns/nm,' nm rms'
;
;  we get to "average" (that is, low-pass filter) the phase errors in closed loop,
;  use the loopAveragingFactor
;
print,'<pipeline> closed loop measurement induced phase noise is ',sqrt(sigma2phase)*loopAverageFactor*microns/nm,' nm rms'
;
;-------------------------------------------
;  account for sensor noise in phase spectra
;-------------------------------------------
; Computing the residual phase power spectrum, given the wfs telemetry and wfs noise
;  s = H p + n  ;  s=wfs slope,
;                  p=wavefront phase after closed loop correction, as seen by the science camera
;                  H=push matrix,
;                  n=wfs noise
;
;  Ss(f) = H Sp(f) H' + H Spn(f) + Snp(f) H' + Sn(f) ;  Sx(f)=power spectrum of "x"
;                                                       Sxy(f)=cross spectrum of "x" with "y"
;  Spn(f) = - C(f) K Sn(f) / (1. + C(f))             ;  C(f) = compensator function x loopGain
;
;  let
;  Cl(f) := C(f)/(1.+C(f))
;
;  K Ss(f) K' = Sp(f) - Cl(f) K Sn(f) K' - Cl'(f) K Sn(f) K' + K Sn(f) K'
;    where Cl'(f)=complex conjugate of Cl(f)
;
;   solving for Sp(f):
;     Sp(f) = K Ss(f) K' + Sn(f) K K' ( 2 Re(Cl(f)) - 1 )
;
n = (size(ltr))[1]
cl = apparentLoopGain*ltr/(1.+apparentLoopGain*ltr)
psnoisecl = Snph_2 * (1. - 2*real(cl)) ; noise correction (may be + or -)
psnoisecl = [psnoisecl , fltarr(n-2)]
psnoiseol = Snph_2 * ones(n)
psnoiseol = [psnoiseol , fltarr(n-2)]
;
psclScience = (pscl - psnoisecl); > (min(abs(psol))/100.)
psolScience = (psol - psnoiseol); > (min(abs(pscl))/100.)
;
;  overplot the power spectra with the noise correction factors to be applied
wset,pswin
psplot,psol,fs,title = 'WFS Residual Phase, microns^2/hz'
psplot,pscl,fs,/oplot,color=150
tickgrid,[df,max(fs)],[min(psol),max(psol)]
psplot,abs(psnoisecl)>1.e-10,fs,/oplot
psplot,psnoiseol>1.e-10,fs,/oplot
;
;   plot the science phase spectra
window,wno,title='science phase spectra'+string(wno)
wno = wno + 1
psplot,psolScience,fs,yrange=[min(abs(psol)),max(abs(psol))],title = 'Science Residual Phase, microns^2/hz'
psplot,psclScience,fs,/oplot,color=150
tickgrid,[df,fs],[min(psol),max(psol)]
oplot,f,a*f^(-8./3.)
;
; replot the rejection curve
wset,rejectionwin
plot,f[1:n-1],rejection[1:n-1],/xlog,/ylog,ytitle='Ratio',xtitle='Frequency, Hz',charsize=2,nsum=10,title='Disturbance rejection',subtitle='smoothed data and model'
tickgrid,[f[1],max(f)],[min(rejection[1:n-1]),max(rejection[1:n-1])]
oplot,f[1:n-1],modelrejection[1:n-1],color=150
newrejection = psclScience/psolScience
oplot,f[1:n-1],abs(newrejection[1:n-1]),color=10,nsum=10
;
;---------------------------------------------------
;  calculate the integrated temporal power rejection
;---------------------------------------------------
; Backwards-integrate the open loop spectrum
n = (size(psol))[1]/2
df = float(fs)/4098.
cum_ol_rad = sqrt(reverse(integral(reverse(psolScience[1:n-1]>0)))*df) $
              *microns*(2*!pi/lambda)
cum_cl_rad = sqrt(reverse(integral(reverse(psclScience[1:n-1]>0)))*df) $
              *microns*(2*!pi/lambda)
cum_ol_model = sqrt(reverse(integral(reverse((a*f^(-8./3.))[1:n-1]>0)))*df) $
              *microns*(2*!pi/lambda)
;
;  <<< Plot - cumulative error in phase power spectrum >>>
window,wno,title='Cumulative bandwidth error plot'+string(wno)
wno = wno+1
plot,f[1:n-1],cum_ol_rad>.01,/xlog,/ylog,ticklen=1.,xtitle='Freqency, Hz',ytitle='Cum Error, radians rms',charsize=2.
tickgrid,[f[1],f[n-1]],[min(cum_ol_rad>.01),max(cum_ol_rad>0.1)]
oplot,f[1:n-1],cum_cl_rad>.01,color=180
oplot,f[1:n-1],cum_ol_model>.01,color=180
;
sigma_BW = sqrt(total(psclScience[0:2047])*df)*microns/nm
print,'<pipeline> Integrated closed-loop temporal spectrum:' & $
print,'<pipeline> sigma_BW = ', sigma_BW,' nm rms = ',sigma_BW * nm *(2*!pi/lambda),' radians rms'
;
;  do a "pretty print" summary of the error budget
;
print,'============================================='
print,'Lick 3m error budget'
print,'  ',ddir+cclFile
print,'  ',datestr
print,'---------------------------------------------'
print,'Fitting Error (sigmaDM) ',sigmaDM/nm,' nm'
print,'   d = ',d/cm,' cm'
print,'   r0Hv = ',r00/cm,' cm'
print,'---------------------------------------------'
print,'Servo Error (sigma_BW) ',sigma_BW,' nm'
print,'   fc = ',fc,' Hz'
print,'   fgHv = ',fg,' Hz'
print,'   fs = ',fs,' Hz'
print,'---------------------------------------------'
print,'Measurement Error (sigma2phase) ',sqrt(sigma2phase)*loopAverageFactor*microns/nm, ' nm'
print,'   SNR = ',SNR
print,'   control loop averaging factor = ',loopAverageFactor
print,'   spotSizeFactor = ',hartmannSpotSize,' arcsec'
print,'---------------------------------------------'
print,'           TOTAL:    ',sqrt((sigmaDM/nm)^2+(sigma_BW)^2+sigma2phase*(loopAverageFactor*microns/nm)^2),' nm'
print,'============================================='
end
;
; loopGainFitFunc
;  This routine calculates the rms difference between the rejection
;  curve calculated from the data and the rejection curve derived
;  from the control loop model.  This is used to fit the "effective"
;  control loop gain, and from there ascertain the Hartmann spot size.
;
function loopGainFitFunc,p
  common loopGainFit, f_com, ltr_com, rejection_com, ncall_com
  ncall_com = ncall_com + 1
  rejectionModel = abs(1./(1.+p[0]*ltr_com))^2
  n = (size(rejectionModel))[1]
  residual = (rejection_com - rejectionModel)[1:n-1]
  weights = sqrt(1./f_com[1:n-1])
  rss = total((residual*weights)^2)
  print,'<loopGainFitFunc>',ncall_com,' gain = ',p[0],' rss = ',rss
  return,rss
end
;
;  CalcRefSpotSize
;   Calculate the apparent size of the Hartmann spot for the
;   internal reference beam, given raw Hartmann data.
;   This produces a spot size estimate for each of the 28 fully
;   illuminated spots, and returns the results in a 28 element vector.
;
function CalcRefSpotSize,rawData,rawFlat
  common hartmannSpotCom, spot_com, fitSpot_n
  slx = [21,27,33,39, 15,21,27,33,39,45, 15,21,39,45, $
         15,21,39,45, 15,21,27,33,39,45, 21,27,33,39]
  sly = [7,7,7,7,  13,13,13,13,13,13,  19,19,19,19, $
         25,25,25,25,  31,31,31,31,31,31, 37,37,37,37]
  sl = [[slx],[sly]]
  if (type(rawData,/text) eq 'string') then rawData = rdview(rawData)
  if (type(rawFlat,/text) eq 'string') then rawFlat = rdview(rawFlat)
;  otherwise it is assumed to be a data cube
;  Make it a data cube if it isn't
  if ((size(rawData))[0] eq 2) then begin
    nx = (size(rawData))[1]
    ny = (size(rawData))[2]
    nz = ny/nx
    ny = nx
    data = reform(rawData,nx,ny,nz)
  endif
;  average the flats
  if ((size(rawFlat))[0] eq 2) then begin
    nx = (size(rawFlat))[1]
    ny = (size(rawFlat))[2]
    nz = ny/nx
    if (nz ne 1) then begin
      ny = nx
      flat = reform(rawFlat,nx,ny,nz)
      flat = total(flat,3)/float(nz)
    endif
  endif
;  average the data
  nz = (size(data))[3]
  for i=0,nz-1 do data[*,*,i] = data[*,*,i]-flat
  data = total(data,3)/float(nz)
;  now pick a spot and fit the x and y shift and std dev
;  thresh = 5.
  thresh = 0.
  nspots = 28
  stat = fltarr(nspots)
  for spotNo=0,nspots-1 do begin
    x = sl[spotNo,0]
    y = sl[spotNo,1]
    spot = (data[x:x+3,y:y+3]-thresh)>0.
;   spot = (data[21:24,31:34]-thresh)>0.
;   spot = (data[39:42,31:34]-thresh)>0.
    spot = spot/total(spot)
    spot_com = spot
    c = centroid(spot)
    sd = .8
    p = [c[0],c[1],sd]
    fitSpot_n = 0
    print,'<CalcRefSpotSize> fitting spot parameters ',format='($,a)'
    result = amoeba(.01,function_name = 'fitSpot',p0=p,scale=[.1,.1,.1],ncalls=30)
    print,'done'
    c = [result[0],result[1]]
    sd = result[2]
    ss = 32
    g = gauss2(4*ss,(c[0]+.5)*ss,(c[1]+.5)*ss,sd*ss)
    gr = rebin(g,4,4)
    gr = gr/total(gr)
;  calculate the spot size factor
    g = gauss2(4*ss,2*ss,2*ss,sd*ss)
    g = g/total(g)
    pixelScale = 1.0 ; arcseconds per Hartmann pixel
    dx = pixelScale/ss
    spotSizeFactor = dx/(sqrt(2)*total(g[*,2*ss])) ; 2-axis spot size factor
    print,'<CalcRefSpotSize> spotNo:', spotNo,' spot size factor = ',spotSizeFactor,' arcsec'
    stat[spotNo] = spotSizeFactor
  endfor
  return,stat
end
;
;  function to calculate the spot fitting error
;  for the the spot parameter search algorithm
function fitSpot,p
  common hartmannSpotCom, spot_com, fitSpot_n
;   spot_com must be normalized to total=1.0
  fitSpot_n = fitSpot_n + 1
  c = fltarr(2)
  c[0] = p[0]
  c[1] = p[1]
  sd = p[2]
  ss = 32
  g = gauss2(4*ss,(c[0]+.5)*ss,(c[1]+.5)*ss,sd*ss)
  gr = rebin(g,4,4)
  gr = gr/total(gr)
  resid = spot_com - gr
  rss = total(resid^2)
;  print,'<fitSpot> ',fitSpot_n,' p = ',p[0],p[1],p[2],' rss = ',rss, $
;     format='($,a,i,a,f,f,f,a,f,T1)'
  print,format='($,".")'
  return,rss
end

;-------------------------------------------------------------------
;     Pre-compile these files:
;
;.compile /u2/gavel/pro/psplot.pro
;.compile control.pro
;.compile pipeline.pro
;

phaseScaleFactor = 1.03639
;referenceSpotSize = .754 ; arcseconds, assuming 1 arcsec/pixel platescale and a threshold of 5.
referenceSpotSize = 0.8 ; arcsec with a threshold of 0.

;------------------------ 1st example dataset ------------------
; CMMay17_00 was created using center-of-mass centroider
cmFile = '/duck5/lickdata/jun00/lgs6data/Matrices/CMMay17_00'
datestr = ' Saturday 09/09/00 23:03:44 PDT '
ddir = '/duck5/lickdata/sep00/lgs6data/sep08/'
centroider = 'quad'
colFile = 'cent_08'
cclFile = 'cent_07'
intenFile = 'inten_07'
mirrorFile = 'mirror_07'
fs = 500
camGain = 1
loopGain = 0.8 ; ? logs report 0.8, but data line says 1.0, also says 0.8 in cent_07.spr file
intGain = 0.998
cwt1 = -0.5296; defaults to [-0.07125 ,-0.1176, ?,-0.2721 ,-0.5296] at [55,100,200,250,500] Hz
;
;    would presume phaseScaleFactor is -smaller- (2x?) for center-of-mass
;    since effective centroid gain is lower
;
;  spot size of the reference source
rawData = '/duck5/lickdata/oct00/lgs6data/oct04/rawImage_05'
rawFlat = '/duck5/lickdata/oct00/lgs6data/oct04/rawImage_01'

;  spot size on the sky, Sep 8, 2000, 23:03 pdt
rawData = ddir + 'rawImage_08'
rawFlat = ddir + 'rawImage_06'
starSpotSize = 0.79 ; arcsec, threshold=0.
irDir = '/duck5/lickdata/sep00/ircal/sep09/'
irData = 'ircal0127.fits' ; 0.2 second, open closed, J band
ird = readfits(irDir+irData)
irSky = 'ircal0128.fits' ; 0.2 seconds, J sky
irs = readfits(irDir+irSky)
;---------------------- 2nd example dataset -------------------------
; here's another dataset
;
ddir = '/duck5/lickdata/may00/lgs6/may21/'
datestr = '5/22/00, 5:09 UT'
colFile = 'cent_04'
cclFile = 'cent_03'
intenFile = 'inten_03'
mirrorFile = 'mirror_03'
rawData = ddir+'rawImage_04'
rawFlat = ddir+'rawImage_02'
cmFile = '/duck5/lickdata/sep00/lgs6data/sep20ut/CMMay17_00'
;  AO system configuration -
fs = 500
camGain = 1
fs = 500.
loopGain = .5
intGain = 0.998
cwt1 = -0.5296
centroider = 'quad'
; stat = calcRefSpotSize(rawData,rawFlat)
;  got an average of 0.8 arcsec -- rather low estimate
;
pipeline,datestr,ddir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs, $
   loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
   micronsPerVolt = phaseScaleFactor
;
 microns=1.e-6
 lambda=.55*microns
 nm=1.e-9
 fc= 30.5027
 fg= 40.2416
 print,((2*!pi/lambda)*174.68*nm)^2/(fg/fc)^(5./3.);  2.12953, 2.509
;-----------------------------------------------------------------------
;   some r0 "data" from May and June 2000, based on rms mirror voltages:
;
; ddir = '/duck5/lickdata/may00/lgs6/may21/'
; idir = '/duck5/lickdata/may00/ircal/May21/'
;
;     file      r0       time                                ircal
;  mirror_03: 13 cm      05:09:57 UT  Monday  05/22/00        0511, 0512
;  mirror_05:  8.5 cm    05:41:44 UT    10:41 pm pdt
;  mirror_07: 10 cm      06:25:49 UT
;  mirror_09: 11 cm      06:44:07 UT
;  mirror_11: 14 cm      07:10:10 UT    12:10 am pdt
;  mirror_13: 10 cm      09:12:54 UT
;  mirror_15:  2 cm      09:43:59 UT
;  mirror_16: 11.9 cm    10:55:52 UT     3:55 am pdt
;  mirror_18:  9.3 cm    11:21:56 UT
;  mirror_20: 11.3 cm    11:51:56 UT
; 
;  mirror_28: 12.9 cm    04:35:16 UT  Tuesday  05/23/00  "seeing .7 arcsec at K"
;  mirror_30:  9.6 cm    05:58:43 UT    10:41 pm pdt
;  mirror_32: 10.1 cm    06:37:37 UT
;  mirror_34:  9.6 cm    07:02:50 UT
;  mirror_37: 11.3 cm    08:11:20 UT
;  mirror_39:  9.7 cm    08:38:58 UT
;  mirror_41:  9.2 cm    09:05:13 UT     2:05 am pdt
;  mirror_43:  9.8 cm    09:36:07 UT
;  mirror_45:  8.9 cm    10:08:58 UT
;  mirror_47: 10.9 cm    10:28:35 UT
;  mirror_49:  8.5 cm    10:51:00 UT
;  mirror_52:  5.77 cm   11:36:33 UT     4:36 am pdt
;  mirror_53:  7.7 cm    11:57:30 UT
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun16/'
;  colFile='cent_02' & cclFile='cent_02' & intenFile = 'inten_02' ; (dummy)
;
;  mirror_05:  10.3 cm   04:57:18 UT  Saturday  06/17/00
;  mirror_08:   7.78 cm  07:10:53 UT
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun18/'
;  colFile='cent_16' & cclFile='cent_16' & intenFile = 'inten_16' ; (dummy)
;
;  mirror_17:   5.99 cm  04:25:27 UT  Monday  06/19/00
;  mirror_24:   7.0  cm  08:04:22 UT    1 am pdt
;  mirror_29:   89.7 cm  12:58:06 UT    5 am pdt
;                                     Tuesday  06/20/00
;  mirror_40:    7.13 cm 06:20:43 UT   11:20 pm pdt
;  mirror_41:    7.94 cm 06:22:42 UT
;  mirror_44:    9.3 cm  07:20:51 UT
;  mirror_46:    8.04 cm 07:39:33 UT
;  mirror_48:    9.59 cm 07:44:47 UT
;  mirror_51:   10.77 cm 08:10:54 UT
;  mirror_53:    9.7 cm  08:47:36 UT
;  mirror_54:    9.9 cm  09:01:49 UT
;  mirror_56:    9.9 cm  09:04:25 UT
;  mirror_58:    5.6 cm  10:34:10 UT
;  mirror_60:    7.0 cm  10:36:23 UT
;  mirror_62:    7.4 cm  11:11:03 UT
;  mirror_65:    8.3 cm  11:59:33 UT
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun20/'
;  colFile='cent_01' & cclFile='cent_01' & intenFile = 'inten_01' ; (dummy)
;
;  mirror_10:   10.7 cm  05:47:26 UT Wednesday  06/21/00   10 pm pdt
;  mirror_14:   11.2 cm  10:41:18 UT   3 am pdt
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun21/'
;  colFile='cent_15' & cclFile='cent_15' & intenFile = 'inten_15' ; (dummy)
;
;  mirror_17:   3.08 cm  06:51:29 UT  Friday  06/23/00  11:51 pm pdt
;  mirror_19:   3.85 cm  07:01:22 UT
;  mirror_20:   3.19 cm  07:03:15 UT unstable?
;  mirror_22:  12.8 cm   07:11:54 UT
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun23/'
;  colFile='cent_01' & cclFile='cent_01' & intenFile = 'inten_01' ; (dummy)
;
;  mirror_01:   9.06 cm  08:23:01 UT Friday  06/23/00  1 am pdt
;  mirror_03:  10.9 cm   08:49:42 UT
;  mirror_05:  10.2 cm   08:51:39 UT
;  mirror_07:  10.8 cm   08:52:32 UT
;  mirror_08:   8.9 cm   09:04:50 UT
;  mirror_09:   9.9 cm   09:23:12 UT
;  mirror_12:   9.21 cm  11:30:35 UT
;  mirror_13:  11.53 cm  11:54:26 UT
;
;  mirror_21:  12.23 cm  04:12:11 UT Saturday  06/24/00  9 pm pdt
;  mirror_22:   9.05 cm  04:28:35 UT
;  mirror_24:   3.14 cm  04:51:49 UT
;  mirror_28:   9.69 cm  05:18:52 UT
;  mirror_29:  11.2 cm   05:38:00 UT
;  mirror_30:  12.52 cm  05:49:47 UT
;  mirror_33:  20.37 cm  07:04:17 UT  12:04 am pdt
;  mirror_38:   8.17 cm  09:51:51 UT
;  mirror_42:  12.46 cm  10:49:05 UT    3:49 am pdt
;
;ddir = '/duck5/lickdata/jun00/lgs6data/jun24/'
;  colFile='cent_09' & cclFile='cent_09' & intenFile = 'inten_09' ; (dummy)
;
;  mirror_14:  11.95 cm  04:52:35 UT  Sunday  06/25/00  9:52 pm pdt
;  mirror_18:   9.22 cm  10:33:41 UT   3:33 am pdt
;  mirror_21:  incomplete data set
;  
;ddir = '/duck5/lickdata/jun00/lgs6data/jun25/'
;  colFile='cent_04' & cclFile='cent_04' & intenFile = 'inten_04' ; (dummy)
;
;                        03:58:11 UT  "seeing 2 arcsec at Ks"
;  mirror_11:  10.87 cm  04:32:22 UT  Monday  06/26/00  9:32 pm pdt
;                        05:04:02 UT  "seeing highly variable tonight"
;  mirror_14:   7.42 cm  05:05:50 UT
;  mirror_19:   9.34 cm  06:49:24 UT
;  mirror_24:   9.64 cm  07:41:54 UT
;  mirror_25:   8.74 cm  08:29:32 UT  1 am pdt
;  mirror_28:   4.98 cm  08:49:12 UT
;  mirror_30:   5.53 cm  09:04:56 UT
;  mirror_34:   7.44 cm  11:29:48 UT  4 am pdt
;  mirror_37:   6.7 cm   11:59:09 UT
;  
;---------------------------------------------------------------
pipeline,datestr,ddir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs, $
   loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
   micronsPerVolt = phaseScaleFactor,/r0only
;
end
