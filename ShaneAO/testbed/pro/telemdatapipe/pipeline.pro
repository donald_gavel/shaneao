;+
; pipeline.pro
;
;    pro pipeline - analyzes open and closed loop AO diagnostic data, assign
;           numbers to error budget contributors fitting error, servo error,
;           wavefront measurement error, etc.  Plot temporal spectra and
;           overlay with a fitted model.
;
;    function r0estimate - analyze closed loop mirror actuator data and
;           estimate r0.  Plot temporal spectra and overlay with a Kolmogorov
;           model.
;
;    See end of file for examples of code usage.
;
;-
;
;----------------------------------------------------------------------------------------
; loopGainFitFunc
;  This routine calculates the rms difference between the rejection
;  curve calculated from the data and the rejection curve derived
;  from the control loop model.  This is used to fit the "effective"
;  control loop gain, and from there ascertain the Hartmann spot size.
;
function loopGainFitFunc,p
  common loopGainFit, f_com, ltr_com, rejection_com, ncall_com
  ncall_com = ncall_com + 1
  rejectionModel = abs(1./(1.+p[0]*ltr_com))^2
  n = (size(rejectionModel))[1]
  residual = (rejection_com - rejectionModel)[1:n-1]
  weights = sqrt(1./f_com[1:n-1])
  rss = total((residual*weights)^2)
  print,'<loopGainFitFunc>',ncall_com,' gain = ',p[0],' rss = ',rss
  return,rss
end

;----------------------------------------------------------------------------------------
;
;  CalcRefSpotSize
;   Calculate the apparent size of the Hartmann spot for the
;   internal reference beam, given raw Hartmann data.
;   This produces a spot size estimate for each of the 28 fully
;   illuminated spots, and returns the results in a 28 element vector.
;
function CalcRefSpotSize,rawData,rawFlat
  common hartmannSpotCom, spot_com, fitSpot_n
  slx = [21,27,33,39, 15,21,27,33,39,45, 15,21,39,45, $
         15,21,39,45, 15,21,27,33,39,45, 21,27,33,39]
  sly = [7,7,7,7,  13,13,13,13,13,13,  19,19,19,19, $
         25,25,25,25,  31,31,31,31,31,31, 37,37,37,37]
  sl = [[slx],[sly]]
  if (type(rawData,/text) eq 'string') then rawData = rdview(rawData)
  if (type(rawFlat,/text) eq 'string') then rawFlat = rdview(rawFlat)
;  otherwise it is assumed to be a data cube
;  Make it a data cube if it isn't
  if ((size(rawData))[0] eq 2) then begin
    nx = (size(rawData))[1]
    ny = (size(rawData))[2]
    nz = ny/nx
    ny = nx
    data = reform(rawData,nx,ny,nz)
  endif
;  average the flats
  if ((size(rawFlat))[0] eq 2) then begin
    nx = (size(rawFlat))[1]
    ny = (size(rawFlat))[2]
    nz = ny/nx
    if (nz ne 1) then begin
      ny = nx
      flat = reform(rawFlat,nx,ny,nz)
      flat = total(flat,3)/float(nz)
    endif
  endif
;  average the data
  nz = (size(data))[3]
  for i=0,nz-1 do data[*,*,i] = data[*,*,i]-flat
  data = total(data,3)/float(nz)
;  now pick a spot and fit the x and y shift and std dev
;  thresh = 5.
  thresh = 0.
  nspots = 28
  stat = fltarr(nspots)
  for spotNo=0,nspots-1 do begin
    x = sl[spotNo,0]
    y = sl[spotNo,1]
    spot = (data[x:x+3,y:y+3]-thresh)>0.
;   spot = (data[21:24,31:34]-thresh)>0.
;   spot = (data[39:42,31:34]-thresh)>0.
    spot = spot/total(spot)
    spot_com = spot
    c = centroid(spot)
    sd = .8
    p = [c[0],c[1],sd]
    fitSpot_n = 0
    print,'<CalcRefSpotSize> fitting spot parameters ',format='($,a)'
    result = amoeba(.01,function_name = 'fitSpot',p0=p,scale=[.1,.1,.1],ncalls=30)
    print,'done'
    c = [result[0],result[1]]
    sd = result[2]
    ss = 32
    g = gauss2(4*ss,(c[0]+.5)*ss,(c[1]+.5)*ss,sd*ss)
    gr = rebin(g,4,4)
    gr = gr/total(gr)
;  calculate the spot size factor
    g = gauss2(4*ss,2*ss,2*ss,sd*ss)
    g = g/total(g)
    pixelScale = 1.0 ; arcseconds per Hartmann pixel
    dx = pixelScale/ss
    spotSizeFactor = dx/(sqrt(2)*total(g[*,2*ss])) ; 2-axis spot size factor
    print,'<CalcRefSpotSize> spotNo:', spotNo,' spot size factor = ',spotSizeFactor,' arcsec'
    stat[spotNo] = spotSizeFactor
  endfor
  return,stat
end

;----------------------------------------------------------------------------------------
;  function to calculate the spot fitting error
;  for the the spot parameter search algorithm

function fitSpot,p
  common hartmannSpotCom, spot_com, fitSpot_n
;   spot_com must be normalized to total=1.0
  fitSpot_n = fitSpot_n + 1
  c = fltarr(2)
  c[0] = p[0]
  c[1] = p[1]
  sd = p[2]
  ss = 32
  g = gauss2(4*ss,(c[0]+.5)*ss,(c[1]+.5)*ss,sd*ss)
  gr = rebin(g,4,4)
  gr = gr/total(gr)
  resid = spot_com - gr
  rss = total(resid^2)
;  print,'<fitSpot> ',fitSpot_n,' p = ',p[0],p[1],p[2],' rss = ',rss, $
;     format='($,a,i,a,f,f,f,a,f,T1)'
  print,format='($,".")'
  return,rss
end

;----------------------------------------------------------------------------------------
;  r0 estimator

function r0estimate,ddir,mirrorFile,fs,phaseScaleFactor=phaseScaleFactor, $
    window=wno,oplot=oplot,plotcum=plotcum
  forward_function dmgeom
  common r0est ,sf
  if (n_elements(ddir) eq 0) then ddir = '/net/lgs6/home/winkerbean/AO/Data/RunData/'
  if (n_elements(wno) eq 0) then wno=0 ; window number for plots
  if (n_elements(plotcum) eq 0) then plotcum = 0
  if (keyword_set(oplot)) then plotcum = 0
  cm = .01
  microns = 1.e-6
  d = 3. ; aperture diameter
  lambda = .55*microns ; wavelength at which we'll calculate r0
  if (n_elements(phaseScaleFactor) eq 0) then begin
    phaseScaleFactor = 0.5
  endif
  if (type(mirrorFile,/text) eq 'string') then act = rdview(ddir + mirrorFile) else act=mirrorFile
  dum = lickdm()
  dmgeo = dmgeom()
;
;  isetpupil is the set of actuators located inside the pupil
;
  isetpupil = [6,7,8,9,12,13,14,15,16,19,20,21,22,23,24,27, $
         28,29,31,32,33,36,37,38,39,40,41,44,45,46,47,48,51,52,53,54]
  n = 1024
  dt = 1./fs
  df = fs/float(n)
  npupil = (size(isetpupil))(1)
;  remove piston
  piston = total(act[isetpupil,*],1)/float(npupil)
  actpr = act[isetpupil,*] - (ones(npupil) # piston)
;  remove tip/tilt
  tip = (actpr ## dmgeo[isetpupil,0])/total(dmgeo[isetpupil,0]^2)
  tip = reform(tip,n)
  acttr = actpr - dmgeo[isetpupil,0] # tip
  tilt = (actpr ## dmgeo[isetpupil,1])/total(dmgeo[isetpupil,1]^2)
  tilt = reform(tilt,n)
  acttr = acttr - dmgeo[isetpupil,1] # tilt
;
;  actave = total(act[isetpupil,*],2)/float(n)
;  a = act[isetpupil,*] - (actave # ones(n))
  a = acttr
  ps = fltarr(n)
  f = findgen(n)*(fs/float(n))
  for i=0,npupil-1 do begin $
    u = transpose(a[i,*]) & $
    ps = ps + abs(fft(u)*n*dt)^2 & $
  endfor
  ps = ps/float(npupil)
  ps = ps[0:n/2]*2.*df ; makes it a one-sided power spectral density
  if (keyword_set(oplot)) then begin
    oplot,f[1:n/2],ps[1:n/2]
  endif else begin
    window,wno,xsize=500,ysize=400,xpos=0,ypos=0
    wno = wno + 1
    plot,f[1:n/2],ps[1:n/2],/xlog,/ylog, $
      title = 'Actuator Power Spectrum '+mirrorFile, charSize=1.5, $
      xtitle = 'Frequency, Hz',ytitle = 'D/A units^2/Hz'
;   tickgrid,minmax(f[1:n/2]),minmax(ps[1:n/2]),/xlog,/ylog
  endelse
;
;   fit a f^(-8/3) power law
;
  n1 = fix((fs/50.)/df)
  n2 = fix((fs/10.)/df)
  logc = average( alog10( ps[n1:n2]) + (8./3.)*alog10(f[n1:n2]))
  c = 10.^logc
  powerLawModel = c*f^(-8./3.)
  oplot,f[1:n/2],powerLawModel[1:n/2],color=150
;   fit a noise floor
  noisefloor = average(ps[n/2-100:n/2-1])
  oplot,f[1:n/2],ones(n/2+1)*noisefloor,color=150
;
;  estimate r0
;
  actsd = sqrt(total((ps[1:n/2]-noiseFloor)*df))
  print,'<r0estimate> actuator standard deviation (D/A units):',actsd
  opdsd = actsd*phaseScaleFactor*microns
  print,'<r0estimate> opd standard deviation (microns):',opdsd/microns
  phasesd = opdsd*2*!pi/lambda
  print,'<r0estimate> phase standard deviation (radians):',phasesd
  r0 = (phasesd^2 / 0.135)^(-3./5.)*d
  print,'<r0estimate> determined from DM voltages: r0 = ',r0/cm,' cm at ',lambda/microns,' microns wavelength'
  lambda2 = 2.2*microns
  arcsec = !pi/(180.*3600.)
  print,'<r0estimate> predicted FWHM at 2.2 microns: ',(lambda2/(r0*(lambda2/lambda)^(6./5.)))/arcsec,' arcsec'
;
;   estimate the break frequency -> wind velocity
;
;  ratio = ps/powerLawModel
  ratio = smooth(ps/powerLawModel,15)
;  ratio = smooth(ps,15)/powerLawModel
  k = 0
  while (ratio[k] lt .5) do k = k+1
  fb = f[k]
  v = fb*d
  print,'<r0estimate> wind velocity (m/sec) = ',v
;
;   build a model of the piston & tip/tilt removed spectrum
;

  dk = 0.01
  kx = findgen(n)*dk
  kx[0] = dk/100.
  if (n_elements(sf) ne n) then begin
    print,'generating generic Kolmogorov temporal power spectrum...'
    print,'(Only done on this first call)'
    k = sqrt(kx^2 # ones(n) + ones(n) # kx^2)
    s0 = 0.023*k^(-11./3.)
    print,'...piston filter...'
    hp = (2*beselj(!pi*k,1)/(!pi*k))^2
    print,'...tilt filter...'
    ht = 16.*(beselj(!pi*k,2)/(!pi*k))^2
    s = s0*(1.-hp-ht)
    s[0,0] = 0
    sf = (4*total(s,1) - s[*,0])*dk
    sig2 = total(sf)*dk
    print,'done.  Generic spectrum will be retained for subsequent calls'
    print,'<r0estimate> This number should be 0.135: ',sig2 ; 0.135
  endif
;  plot,kx[1:n-1],sf[1:n-1],/xlog,/ylog
;  convert model from phase^2/hz to DA units^2/hz
  model = ((lambda/(2*!pi))/(phaseScaleFactor*microns))^2*(d/r0)^(5./3.)*(d/v)*sf
  fmodel = kx*(v/d)
;  wset,0
;  plot,f[1:n/2],ps[1:n/2],/xlog,/ylog
  oplot,fmodel[1:n-1],model[1:n-1],color=120
;  oplot,f[1:n/2],ones(n/2)*noiseFloor,color=120
;;-->  print,sqrt(total(model)*dk*(v/d)) ; should = actsd
;
;   reverse-integrate the power spectrum to get cumulative rms phase
;
  cum_phase = sqrt(reverse(integral(reverse(ps[1:n/2]))*df))
  cum_phase_model = sqrt(reverse(integral(reverse((model+noiseFloor)[1:n-1]))*dk*(v/d)))
  if (plotcum eq 1) then begin
    window,wno,xsize=500,ysize=400,xpos=100,ypos=100
    wno = wno+1
    plot,f[1:n/2-1],cum_phase[1:n/2-1],/xlog,/ylog,charSize = 1.5, $
        title = 'Cumulative rms actuator spectrum',xtitle = 'Frequency, Hz', $
        ytitle = 'rms D/A units'
    oplot,fmodel[1:n-2],cum_phase_model[1:n-2],color=120
  endif
  int_noise = sqrt(noisefloor*fs*df)
  snr = cum_phase[0]/int_noise
  print,'<r0estimate> snr = ',snr
;
  return,r0
end
;
;----------------------------------------------------------------------------------------
;  pipline

pro pipeline,datestr,ddir,colFile,cclFile,intenFile,mirrorFile,cmFile, $
  fs,loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
  r0=r00,micronsPerVolt=phaseScaleFactor, r0only=r0only, redo=redo, $
  window=wno, colortab=colortab, pause=pause
;
common pipeline ,psol_com,pscl_com
if (n_params() eq 0) then begin
	return
endif
if (n_elements(wno) eq 0) then wno = 0 ; first window number, for plots
if (n_elements(colortab) eq 0) then colortab = 5
loadct,colortab
tvlct,r,g,b,/get
tvlct,reverse(r),reverse(g),reverse(b)
!p.background = 3
;---------------------
;  standard constants
;---------------------
nm = 1.e-9
microns = 1.e-6
cm = 1.e-2
arcsec = !pi/(180.*3600.)
;---------------------
; gather some data
;---------------------
if (type(colFile,/text) eq 'string') then col = rdview(ddir + colFile) else col=colFile
if (type(cclFile,/text) eq 'string') then ccl = rdview(ddir + cclFile) else ccl=cclFile
if (type(mirrorFile,/text) eq 'string') then act = rdview(ddir + mirrorFile) else act=mirrorFile
if (type(intenFile,/text) eq 'string') then inten = rdview(ddir + intenFile) else inten=intenFile
if (type(cmFile,/text) eq 'string') then cmat = rdview(cmFile,/swap) else cmat=cmFile
lambda = .7*microns ; average wavelength into wfs
lambda0 = .55*microns
;----------------------------------
; determine the Hartmann sensor SNR
;----------------------------------
; average intensity over illuminated subapertures
isetillum = $
         [ 3,          4,          5,          6,          8,          9,$
          10,         11,         12,         13,         15,         16,$
          17,         18,         21,         22,         23,         24,$
          26,         27,         28,         29,         30,         31,$
          33,         34,         35,         36]
nillum = (size(isetillum))(1)
; measure average illumination in photons/subap/sample
case camGain of
 1:  phdn = 2
 2:  phdn = 1
 else: print,'<pipeline> ERROR: camGain not valid'
endcase
nph = average(inten[isetillum])*phdn
readnoise = 6*phdn
SNR = nph / sqrt(nph + readnoise^2)
print,'<pipeline> SNR = ',SNR
print,'<pipeline> average intensity = ',nph,' photons'
;-----------------------------------------
; Compute the control compensator function
;-----------------------------------------
n = 2049
fn = fs/2.
df = fn / float(n)
T = 1./float(fs)
f = findgen(n)*df ;  include f="0" and f_nyquist
f[0] = df/10.
i = complex(0.,1.)
s = 2*!pi*f*i
z = exp(s*T)
cf = z^2 / ((z - intGain)*(z - cwt1)); compensator
sh = (1-z^(-1))/(s*T) ; sample and hold
stare = (1-z^(-1))/(s*T) ; CCD stare
tc = .0012
delay = exp(-s*tc); compute delay
ltr = cf*sh*stare*delay ; loop transfer function
;
;  Atmospheric phase rejection
;  ---------------------------
;  Atmospheric phase rejection, from the science image, is
;  through the filter 1./(1.+loopGain*ltr), as seen in the following diagram
;
;                               meas noise
;            to science image       |       to telemetry
;                        ^          |       ^
;                 +      |          v       |               H: push matrix
;  atmos phase ---->(+)---->[H]--->(+)--------->[K]----     K: reconstructor matrix
;                  - ^          wf-slope              |     KH=I
;                    |                                |
;                    -----------[ cf(z) ]<-------------
;             phase-correction                  delta-phase-correction
;
;
;  Noise amplification
;  -------------------
;     Noise seen by the telemetry, open loop, is basically measurement noise
;     at high frequencies since the Kolmogorov atmosphere spectrum drops off quickly.
;     For white measurement noise,
;     the high freq noise floor on the slope power spectrum is flat
;     with an amplitude equal to its power spectral density.  We have
;     not calibrated the slope data to angle on the sky, so this density is
;     in "wfs units".
;
;     Phase = K*slope, so there is a high freq noise floor on the phase
;     power spectrum from which we can read off the "measurement noise
;     induced phase noise".  This number is calibrated (via the phaseScaleFactor)
;     to nanometers.  The noise relationship is Phase noise = K*slope noise,
;     so the equivalent diagram has "phase noise" entering after K:
;
;                               meas noise induced
;            to science image        phase "noise"  to telemetry
;                        ^                     |    ^
;                 +      |                     V    |       H: push matrix
;  atmos phase ---->(+)---->[H]------->[K]----(+)------     K: reconstructor matrix
;                  - ^          wf-slope                    KH=I
;                    |
;        0 (no correction open loop)
;
;     Since KH=I, we can, equivalently, move the noise source to before H:
;
;                            meas noise induced
;         to science image   phase "noise"        to telemetry
;                        ^   |                    ^
;                 +      |   V                    |         H: push matrix
;  atmos phase ---->(+)-----(+)---->[H]--->[K]---------     K: reconstructor matrix
;                  - ^               wf-slope               KH=I
;                    |
;                   0
;
;  In closed loop, the measurement-induced-phase noise is amplified by the filter
;  loopGain*ltr/(1.+loopGain*ltr), as seen in the following diagram:
;
;                            meas noise induced
;         to science image   phase "noise"        to telemetry
;                        ^   |                    ^
;                 +      |   V                    |         H: push matrix
;  atmos phase ---->(+)-----(+)---->[H]--->[K]---------     K: reconstructor matrix
;                  - ^               wf-slope         |     KH=I
;                    |                                |
;                    -----------[ cf(z) ]<-------------
;             phase-correction                  delta-phase-correction
;
modelRejection = abs(1./(1.+loopGain*ltr))^2
; <<< Plot Bode Plot of controller loop tranfer function >>>
if (keyword_set(r0only) eq 0) then begin
;  window,wno,title='Bode Plot'+string(wno)
;  bodePlot,ltr,fs
;  wno = wno+1
; <<< Rejection plot >>>
;  window,wno,title='Rejection plot 0'+string(wno)
;  rejectionwin = wno
;  wno = wno+1
;  plot,f[1:n-1],modelRejection[1:n-1],/xlog,/ylog,ytitle='Ratio of closed to open transfer fcn',xtitle='Frequency, Hz',charsize=2
;  tickgrid,[f[1],max(f)],[min(modelRejection),max(modelRejection)],/xlog,/ylog
endif
;------------------------------
; measure the actuator variance
;------------------------------
;  isetpupil is the set of actuators located inside the pupil
isetpupil = [6,7,8,9,12,13,14,15,16,19,20,21,22,23,24,27, $
         28,29,31,32,33,36,37,38,39,40,41,44,45,46,47,48,51,52,53,54]
n = 1024
dt = 1./fs
df = fs/float(n)
npupil = (size(isetpupil))(1)
dum = lickdm()
dmgeo = dmgeom()
; remove piston
piston = total(act[isetpupil,*],1)/float(npupil)
actpr = act[isetpupil,*] - (ones(npupil) # piston)
; remove tip/tilt
  tip = (actpr ## dmgeo[isetpupil,0])/total(dmgeo[isetpupil,0]^2)
  tip = reform(tip,n)
  acttr = actpr - dmgeo[isetpupil,0] # tip
  tilt = (actpr ## dmgeo[isetpupil,1])/total(dmgeo[isetpupil,1]^2)
  tilt = reform(tilt,n)
  acttr = acttr - dmgeo[isetpupil,1] # tilt
;
;actsd = sqrt(total(acttr^2)/(float(n)*npupil))
  a = acttr
  ps = fltarr(n)
  f = findgen(n)*(fs/float(n))
  for i=0,npupil-1 do begin $
    u = transpose(a[i,*]) & $
    ps = ps + abs(fft(u)*n*dt)^2 & $
  endfor
  ps = ps/float(npupil)
  ps = ps[0:n/2]*2.*df ; makes it a one-sided power spectral density
;   fit a noise floor
  noisefloor = average(ps[n/2-100:n/2-1])
;--------------------------------------------------------------------------------
; now, we either calibrate to the given r0, or determine r0 given the calibration
;--------------------------------------------------------------------------------
dtele = 3.0
actsd = sqrt(total((ps[1:n/2]-noiseFloor)*df))
case 1 of
  (n_elements(phaseScaleFactor) eq 0): begin
    if (n_elements(r00) eq 0) then begin
      print,"<pipeline> ERROR: you need to specify either r0 or the phase scale factor"
      return
    endif
    r0 = r00*(lambda/lambda0)^(6./5.)
;   calculate the theoretical, tilt-removed phase variance, in microns
    opdsdTheory = sqrt(0.34*(dtele/r0)^(5./3.))*(lambda/(2*!pi))/microns
;   here's the scale factor for phase (microns/actuator-unit)
    phaseScaleFactor = opdsdTheory / actsd
    print,'<pipeline> DM actuator scale: ',phaseScaleFactor, ' microns/actuator-unit'
  end
  (n_elements(phaseScaleFactor) ne 0): begin
    print,'<pipeline> actuator standard deviation (D/A units):',actsd
    opdsd = actsd*phaseScaleFactor*microns
    print,'<pipeline> opd standard deviation (microns):',opdsd/microns
    phasesd = opdsd*2*!pi/lambda
    r0 = (phasesd^2 / 0.135)^(-3./5.)*dtele
    print,'<pipeline> determined from DM voltages: r0 = ',r0/cm,' cm at ',lambda/microns,' microns wavelength'
    r00 = r0*(lambda/lambda0)^(-6./5.)
    print,'<pipeline> determined from DM voltages: r0 = ',r00/cm,' cm at ',lambda0/microns,' microns wavelength'
  end
  else: print,'<pipeline> ERROR: phase not calibrated'
endcase
; calculate the fitting error
muDM = 0.27 ; magic number for Lick DM
d = dtele/7.; subaperture diameter at Lick
sigmaDM = sqrt(muDM*(d/r0)^(5./3.))*(lambda/(2*!pi))
print,'<pipeline> sigmaDM = ',sigmaDM/nm,' nm rms'
if keyword_set(r0only) then return
;-----------------------------------------
; compute the "measured" residual spectrum
;-----------------------------------------
if (keyword_set(redo)) then begin
  psol=psol_com
  pscl=pscl_com
endif else begin
  psol=aophaseps(col,cmat,fs)*phaseScaleFactor^2 ; (microns^2/hz)
  pscl=aophaseps(ccl,cmat,fs)*phaseScaleFactor^2
  psol_com = psol
  pscl_com = pscl
endelse
;  <<< Plot - temporal phase power spectra >>>
pswin = wno
window,wno,title='Power spectrum plot'+string(wno)
wno = wno+1
powerSpectrumPlot,psol,fs,ytitle='Power, microns^2/hz',title = 'WFS Residual Phase Data'
plots,[[.75,.75],[.8,.75]],/normal
xyouts,.8,.75,'open loop',/normal
powerSpectrumPlot,pscl,fs,/oplot,color=150
tickgrid,[df,max(fs)],[min(psol),max(psol)],/xlog,/ylog
plots,[[.75,.7],[.8,.7]],/normal,color=150
xyouts,.8,.7,'closed loop',/normal
;
; plot a fitting power law f^(-8/3)
n = 2048
fn = fs/2.
df = fn / float(n)
f = (findgen(n)+1)*df
n1 = fix((fs/50.)/df) & n2 = fix((fs/10.)/df)
loga = average( alog10(2*psol[n1:n2]) + (8./3.)*alog10(f[n1:n2]))
a = 10.^loga
print,'<pipeline> power law fit to Kolmogorov phase spectrum: ',a,' *f^(-8/3) microns^2/Hz'
oplot,f,a*f^(-8./3.)
;
; make some statements about the Greenwood model spectrum, given the fitted power law
; a = 0.077 * r0^(-5/3) * v^(5/3)
arad2 = a * ( 2*!pi / (lambda/microns) )^2
windVelocity = r0 * (arad2/0.077)^(3./5.)
print,'<pipeline> model wind velocity: ',windVelocity,' m/sec'
fg = 0.158 * (windVelocity/r00)
print,'<pipeline> model Greenwood frequency: ',fg, ' Hz'
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;----------------------------------------
;  compute the controller rejection curve
;----------------------------------------
rejection = pscl/psol
window,wno,title='Rejection plot'+string(wno)
wno = wno+1
;wset,rejectionwin
plot,f[1:n-1],rejection[1:n-1],/xlog,/ylog,ytitle='Ratio', $
	xtitle='Frequency, Hz',charsize=2,nsum=10, $
	title='Disturbance rejection',subtitle='smoothed data and model fit'
lx = .65
ly = .35
lw = .05
tickgrid,[f[1],max(f)],[min(rejection[1:n-1]),max(rejection[1:n-1])],/xlog,/ylog
plots,[[lx,ly],[lx+lw,ly]],/normal
xyouts,lx+lw,ly,'disturbance rejection',/normal
ly -= .05
oplot,f[1:n-1],modelrejection[1:n-1],color=150
plots,[[lx,ly],[lx+lw,ly]],/normal,color=150
xyouts,lx+lw,ly,'loop gain='+strtrim(string(loopGain),2),/normal
;----------------------------
; fit the effective loop gain
;----------------------------
common loopGainFit, f_com, ltr_com, rejection_com, ncall_com
n = 2049
fn = fs/2.
df = fn / float(n)
T = 1./float(fs)
f = findgen(n)*df ;  include f="0" and f_nyquist
f_com = f
ltr_com = ltr
rejection_com = rejection
ncall_com = 0
result = amoeba(.001,function_name = 'loopGainFitFunc',p0=[loopGain],scale=[.1],ncalls=30)
apparentLoopGain = result[0]
modelrejection = abs(1./(1.+apparentLoopGain*ltr))^2
; find the crossover frequency
i = 1
while (abs(modelrejection[i]) lt 1.0) do i = i+1
fc = f[i]
; revise the rejection plot
oplot,f[1:n-1],modelrejection[1:n-1],color=200
ly -= .05
plots,[[lx,ly],[lx+lw,ly]],/normal,color=200
xyouts,lx+lw,ly,'loop gain (fit)='+strtrim(string(apparentLoopGain),2),/normal
hartmannSpotSize = (loopGain/apparentLoopGain)*referenceSpotSize
print,'<pipeline> Loop Gain is apparently ',apparentLoopGain
print,'<pipeline> Spot Size is apparently ',hartmannSpotSize,' arcseconds'
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;
;---------------------------------------------------
; calculate the closed loop noise averaging factor
;---------------------------------------------------
noiseFilter = apparentLoopGain*ltr / (1. + apparentLoopGain*ltr)
;plot,f[1:n-1],abs(noiseFilter[1:n-1]),/xlog,/ylog
loopAverageFactor = sqrt( total( abs(noiseFilter)^2 * df ) / fs )
print,'<pipeline> closed loop control noise averaging factor = ',loopAverageFactor
;-------------------------------------------------------------------------
; determine the sensor noise component of the measured phase power spectra
;-------------------------------------------------------------------------
;    method 1: from the noise floor of the open loop tilt power spectrum
;
;\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
; NOTE: there is an assumption that the noise floor is the last nav (~200) data points!
;\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\
;
nav = 100
pss = aops(col,fs)
sigma2tilt = (average(pss[0,2047-nav:2047]) + average(pss[1,2047-nav:2047])) * fs ; wfs slope units^2
print,'<pipeline> rms wfs noise is ',sqrt(sigma2tilt),' wfs units'
print,'<pipeline> this should be close to 1/SNR = ',1/SNR
;
kk = diagonal(cmat ## transpose(cmat))
kk = average(kk[isetpupil])
Snph_1 = (sigma2tilt/fs)*kk*phaseScaleFactor^2 ;   phase noise spectral density, microns^2 / Hz
print,'<pipeline> measurement noise induced phase open loop, based on slope data, is ',sqrt(Snph_1*fs)*microns/nm,' nm rms'
;
;    method 2: from the noise floor of the open loop phase power spectrum.
;        note: method 2 results in a lower rms phase noise I think because the
;              reconstruction suppresses some modes in the noise (both unreconstructable
;              and specifically removed in the SVD).  These modes are
;              not propagated and hence are not seen in the science wavefront
;
sigma2phase = average(psol[2047-200:2047]) * fs
Snph_2 = (sigma2phase/fs) ; phase noise spectral density, microns^2 / Hz
print,'<pipeline> measurement noise induced phase open loop, based on reconstructed phase, is ',sqrt(sigma2phase)*microns/nm,' nm rms'
;
;  we get to "average" (that is, low-pass filter) the phase errors in closed loop,
;  use the loopAveragingFactor
;
print,'<pipeline> closed loop measurement induced phase noise is ',sqrt(sigma2phase)*loopAverageFactor*microns/nm,' nm rms'
;
;-------------------------------------------
;  account for sensor noise in phase spectra
;-------------------------------------------
; Computing the residual phase power spectrum, given the wfs telemetry and wfs noise
;  s = H p + n  ;  s=wfs slope,
;                  p=wavefront phase after closed loop correction, as seen by the science camera
;                  H=push matrix,
;                  n=wfs noise
;
;  Ss(f) = H Sp(f) H' + H Spn(f) + Snp(f) H' + Sn(f) ;  Sx(f)=power spectrum of "x"
;                                                       Sxy(f)=cross spectrum of "x" with "y"
;  Spn(f) = - C(f) K Sn(f) / (1. + C(f))             ;  C(f) = compensator function x loopGain
;
;  let
;  Cl(f) := C(f)/(1.+C(f))
;
;  K Ss(f) K' = Sp(f) - Cl(f) K Sn(f) K' - Cl'(f) K Sn(f) K' + K Sn(f) K'
;    where Cl'(f)=complex conjugate of Cl(f)
;
;   solving for Sp(f):
;     Sp(f) = K Ss(f) K' + Sn(f) K K' ( 2 Re(Cl(f)) - 1 )
;
n = (size(ltr))[1]
cl = apparentLoopGain*ltr/(1.+apparentLoopGain*ltr)
psnoisecl = Snph_2 * (1. - 2*real(cl)) ; noise correction (may be + or -)
psnoisecl = [psnoisecl , fltarr(n-2)]
psnoiseol = Snph_2 * ones(n)
psnoiseol = [psnoiseol , fltarr(n-2)]
;
psclScience = (pscl - psnoisecl); > (min(abs(psol))/100.)
psolScience = (psol - psnoiseol); > (min(abs(pscl))/100.)
;
;  overplot the power spectra with the noise correction factors to be applied
;wset,pswin
window,wno,title='Power Spectrum '+string(wno)
wno = wno+1
powerSpectrumPlot,psol,fs,title = 'WFS Residual Phase, microns^2/hz'
powerSpectrumPlot,pscl,fs,/oplot,color=150
tickgrid,[df,max(fs)],[min(psol),max(psol)],/xlog,/ylog
powerSpectrumPlot,abs(psnoisecl)>1.e-10,fs,/oplot
powerSpectrumPlot,psnoiseol>1.e-10,fs,/oplot
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;
;   plot the science phase spectra
window,wno,title='science phase spectra'+string(wno)
wno = wno + 1
powerSpectrumPlot,psolScience,fs,yrange=[min(abs(psol)),max(abs(psol))],title = 'Science Residual Phase, microns^2/hz'
powerSpectrumPlot,psclScience,fs,/oplot,color=150
tickgrid,[df,fs],[min(psol),max(psol)],/xlog,/ylog
oplot,f,a*f^(-8./3.)
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;
; replot the rejection curve
window,wno,title='Rejection re-Plot'+string(wno)
wno = wno+1
plot,f[1:n-1],rejection[1:n-1],/xlog,/ylog,ytitle='Ratio',xtitle='Frequency, Hz',charsize=2,nsum=10,title='Disturbance rejection',subtitle='smoothed data and model'
tickgrid,[f[1],max(f)],[min(rejection[1:n-1]),max(rejection[1:n-1])],/xlog,/ylog
oplot,f[1:n-1],modelrejection[1:n-1],color=150
newrejection = psclScience/psolScience
oplot,f[1:n-1],abs(newrejection[1:n-1]),color=200,nsum=10
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;
;---------------------------------------------------
;  calculate the integrated temporal power rejection
;---------------------------------------------------
; Backwards-integrate the open loop spectrum
n = (size(psol))[1]/2
df = float(fs)/4098.
cum_ol_mic = sqrt(reverse(integral(reverse(psolScience[1:n-1]>0)))*df)
cum_cl_mic = sqrt(reverse(integral(reverse(psclScience[1:n-1]>0)))*df)
cum_ol_model = sqrt(reverse(integral(reverse((a*f^(-8./3.))[1:n-1]>0)))*df)
;
;  <<< Plot - cumulative error in phase power spectrum >>>
window,wno,title='Cumulative bandwidth error plot'+string(wno)
wno = wno+1
plot,f[1:n-1],cum_ol_mic>.001,/xlog,/ylog,ticklen=1.,xtitle='Freqency, Hz',ytitle='Cum Error, microns rms',charsize=2.
tickgrid,[f[1],f[n-1]],[min(cum_ol_mic>.001),max(cum_ol_mic>0.01)],/xlog,/ylog
oplot,f[1:n-1],cum_cl_mic>.001,color=150
oplot,f[1:n-1],cum_ol_model>.001,color=180
;
sigma_BW = sqrt(total(psclScience[1:2047])*df)*microns/nm
print,'<pipeline> Integrated closed-loop temporal spectrum:' & $
print,'<pipeline> sigma_BW = ', sigma_BW,' nm rms'
if (keyword_set(pause)) then begin
	print,'<pipeline> press any key to continue'
	wait,.01
	r = get_kbrd(1)
	print,'--------------------------------------------------------------'
endif
;
;  do a "pretty print" summary of the error budget
;
print,'<pipeline> ============================================='
print,'Lick 3m error budget'
print,'  ',ddir+cclFile
print,'  ',datestr
print,'---------------------------------------------'
print,'Fitting Error (sigmaDM) ',sigmaDM/nm,' nm'
print,'   d = ',d/cm,' cm'
print,'   r0Hv = ',r00/cm,' cm'
print,'---------------------------------------------'
print,'Servo Error (sigma_BW) ',sigma_BW,' nm'
print,'   fc = ',fc,' Hz'
print,'   fgHv = ',fg,' Hz'
print,'   fs = ',fs,' Hz'
print,'---------------------------------------------'
print,'Measurement Error (sigma2phase) ',sqrt(sigma2phase)*loopAverageFactor*microns/nm, ' nm'
print,'   SNR = ',SNR
print,'   control loop averaging factor = ',loopAverageFactor
print,'   spotSizeFactor = ',hartmannSpotSize,' arcsec'
print,'---------------------------------------------'
print,'           TOTAL:    ',sqrt((sigmaDM/nm)^2+(sigma_BW)^2+sigma2phase*(loopAverageFactor*microns/nm)^2),' nm'
print,'<pipeline> ============================================='
end
;
;===================================================================
;   Examples of code usage
;-------------------------------------------------------------------
;     Pre-compile these files:
;
;.compile powerSpectrumPlot.pro
;.compile control.pro
;.compile pipeline.pro
;.compile lickdm.pro
;

;  set "known" factors
phaseScaleFactor = 0.49
referenceSpotSize = 0.8 ; arcsec with a threshold of 0.

;---------------------------------------------------------------
; which machine are you on?
machine = 'mac'
machine = 'pc'
if machine eq 'mac' then sep = "/"
if machine eq 'pc' then sep = "\"
;------------------------ 1st example dataset ------------------
;

datestr = ' June 27, 2005 '
;topdir = 'C:\pro\telemdatapipe\'
if (machine eq 'pc') then topdir = 'C:\Users\SONY USER\Documents\My Dropbox\pro\telemdatapipe\'
if (machine eq 'mac') then topdir = '/Users/DonGavel/Dropbox/pro/telemdatapipe/'
;datadir = topdir+'Data\2005_June27\'
datadir = topdir+strjoin(['Data','2005_june27'],sep)+sep
dataset = [11,10] ; [open loop, closed loop]
;cmFile = topdir+'Data\ControlMatrices\CMSep03_00'
cmfile = topdir+strjoin(['Data','ControlMatrices','CMSep03_00'],sep)

colFile = 'cent_'+string(dataset[0],format='(i03)')
cclFile = 'cent_'+string(dataset[1],format='(i03)')
intenFile = 'inten_'+string(dataset[1],format='(i03)')
mirrorFile = 'mirror_'+string(dataset[1],format='(i03)')

; AO system configuration -
fs = 500.
loopGain = 0.9
intGain = 0.998
cwt1 = -0.56; defaults to [-0.07125 ,-0.1176, ?,-0.2721 ,-0.5296] at [55,100,200,250,500] Hz
centroider = 'quad'
camGain = 1

;--------- example use of the r0 estimator ------------
print,r0estimate(datadir,mirrorFile,fs,phaseScaleFactor = phaseScaleFactor,window=15)

;-------------------------------------------------------------------
;pipeline,datestr,datadir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs, $
;   loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
;   micronsPerVolt = phaseScaleFactor,/r0only
;-------------------------------------------------------------------
;-------------------------------------------------------------------
pipeline,datestr,datadir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs, $
   loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
   micronsPerVolt = phaseScaleFactor
;-------------------------------------------------------------------
doThis = 0
if (doThis) then begin
;
;    The following little bit of code attempts to determine the
;    size of the Hartmann spot, in arcseconds, by fitting
;    a pixelized 2-d Gaussian to the pixel illuminations
;    in the "rawImage" datasets.  Fits are made to all 28
;    fully illuminated Hartmann subapertures.
;      The fitting takes a while.  You don't need to run on
;    these datasets since I've already done that.
;
;  spot size of the reference source
    rawData = '/duck5/lickdata/oct00/lgs6data/oct04/rawImage_05'
    rawFlat = '/duck5/lickdata/oct00/lgs6data/oct04/rawImage_01'
    stat = calcRefSpotSize(rawData,rawFlat)
    referenceSpotSize = average(stat) ;  got an average of 0.8 arcsec

;  spot size on the sky, Sep 8, 2000, 23:03 pdt
    rawData = datadir + 'rawImage_08'
    rawFlat = datadir + 'rawImage_06'
    stat = calcRefSpotSize(rawData,rawFlat)
    starSpotSize = average(stat) ;  0.79 arcsec, threshold=0.

;  J-band IR image for comparison (1 pixel = 0.076 arcsec)
    irDir = '/duck5/lickdata/sep00/ircal/sep09/'
    irData = 'ircal0127.fits'   ; 0.2 second, open closed, J band
    ird = readfits(irDir+irData)
    irSky = 'ircal0128.fits'    ; 0.2 seconds, J sky
    irs = readfits(irDir+irSky)
endif
;---------------------- 2nd example dataset -------------------------
;
doThis = 0
if (doThis) then begin
    datestr = '5/22/00, 5:09 UT'
    datadir = '/duck5/lickdata/may00/lgs6/may21/'
    colFile = 'cent_04'
    cclFile = 'cent_03'
    intenFile = 'inten_03'
    mirrorFile = 'mirror_03'
    rawData = datadir+'rawImage_04'
    rawFlat = datadir+'rawImage_02'
    cmFile = '/duck5/lickdata/sep00/lgs6data/sep20ut/CMMay17_00'

;  AO system configuration -
    fs = 500.
    loopGain = .5
    intGain = 0.998
    cwt1 = -0.5296
    centroider = 'quad'
    camGain = 1
;-------------------------------------------------------------------
    pipeline,datestr,datadir,colFile,cclFile,intenFile,mirrorFile,cmFile,fs, $
   loopGain,intGain,cwt1,camGain,referenceSpotSize,psol,pscl, $
   micronsPerVolt = phaseScaleFactor
;-------------------------------------------------------------------
endif
                          ;
;--------- example use of the r0 estimator ------------
;datadir = '/duck5/lickdata/oct00/lgs6data/oct07/'
;mirrorFile = 'mirror_02' & fs = 500.
;print,r0estimate(datadir,mirrorFile,fs,phaseScaleFactor = phaseScaleFactor)

end
