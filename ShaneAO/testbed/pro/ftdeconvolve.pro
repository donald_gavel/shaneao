;
;  ftdeconvolve.pro - deconvolve 2 images using the fast-Fourier-transform
;
;  usage:
;    res = ftdeconvolve(im,psf,[reg=reg],[domain=domain])
;
;  inputs:
;    im = image to deconvolve
;    psf = point spread function
;    reg = regularization parameter, a scalar fraction of the largest
;          frequency component of the psf at which deconvolution is
;          cut off in the frequency domain
;    domain = 3-vector of logicals delineating special cases for domains:
;      domain[0] = im1 is already in the transform domain
;      domain[1] = im2 is already in the transform domain
;      domain[2] = the result is to be returned in the transform domain
;
function ftdeconvolve,im,psf,reg=reg,domain=domain
    dim = (size(im))[0]
    n = (size(im))[1]
    m = (size(im))[2]
    if (dim eq 1) then m = 1
    if (n_elements(domain) eq 0) then domain = [0,0,0]
    if (n_elements(reg) eq 0) then reg = 1.e-6
    if ((domain[0] eq 0) and (domain[1] eq 0)) then begin
	    fim = ft(im)
	    fpsf = ft(psf)
	    r = max(abs(fpsf))*reg
	    fres = fim/(fpsf>r)
;	    fres *= fpsf gt r
	  endif
	  if ((domain[0] eq 1) and (domain[1] eq 0)) then begin
	    fpsf = ft(psf)
	    r = max(abs(fpsf))*reg
	    fres = im/(fpsf>r)
;	    fres *= fpsf gt r
	  endif
    if ((domain[0] eq 0) and (domain[1] eq 1)) then begin
	    fim = ft(im)
	    r = max(abs(psf))*reg
	    fres = fim/(psf>r)
;	    fres *= psf gt r
	  endif
    if ((domain[0] eq 1) and (domain[1] eq 1)) then begin
	    r = max(abs(psf))*reg
	    fres = im/(psf>r)
;	    fres *= psf gt r
	  endif
	  if (domain[2] eq 1) then return,fres
    res = ft(fres,/inverse)/(float(n)*float(m))
    if (dim eq 1) then res = shift(res,-n/2)
    return,res
end
; test it
n = 256
sig = 5 ; size of influence function
f = gauss2(n,n/2,n/2,sig)
f = f/total(f)
im0 = circle(n,n,n/2,n/2,n/4,1.)
im = ftconvolve(im0,f)
fim = ft(im)
ff = ft(f)
res = ftdeconvolve(im,f,reg=1.e-6) & disp,real(res),'res'
;  all these should be the same result
;res000 = ftdeconvolve(im,f) & disp,real(res000),'000'
;res010 = ftdeconvolve(im,ff,domain=[0,1,0]) & disp,real(res010),'010'
;res100 = ftdeconvolve(fim,f,domain=[1,0,0]) & disp,real(res100),'100'
;res110 = ftdeconvolve(fim,ff,domain=[1,1,0]) & disp,real(res110),'110'
;res001 = ft(ftdeconvolve(im,f,domain=[0,0,1]),/inverse)/float(n)^2 & disp,real(res001),'001'
end
