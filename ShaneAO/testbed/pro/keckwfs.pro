;+
;  keckwfs.pro - routines for creating and manipulating the
;       keck wavefront sensor
;
;  USAGE
;    sl = subaps_inap(ap,ds,du) ; find illuminated subapertures
;    im = keckwfs_display(n,d,du,sl) ; display subapertures
;
;  INPUTS:
;    ap - aperture
;    n - image dimensions
;    d - subaperture diameter, meters
;    du - pixel size, meters
;    sl - 2xNS array of x,y locations of subaps, meters
;
;  OUTPUT:
;    im - display image of Keck aperture with the subaperture array overlayed
;
;-

function keckwfs_display,n,d,du,sl
  ns = (size(sl))(2)
  subap = circle(n,n,n/2,n/2,d/(2.*du),1.)
  r = fltarr(n,n)
  for i=0,ns-1 do begin
    x = sl[0,i]/du
    y = sl[1,i]/du
    r = r + shift(subap,x,y)
  endfor
  return,r
end

;  if sl is not provided, use ds to generate a grid of possible
;    initial subap locations.  return sl, a list of x,y locations.
;  if sl is provided, return a list of indices into sl that
;    indicates >1/2 illuminated subaps
function subaps_inap,ap,ds,du,sl
  n = (size(ap))(1)
;   x and y are the positions of the lenslets
  if (n_elements(sl) eq 0) then begin
    ns = round((n/2)*du/ds)
    x = findgen(ns)*ds + ds/2.
    x = [-reverse(x),x]
    y = x
    x = x # (x*0+1)
    y = (y*0+1) # y
  endif else begin
    ns = (size(sl))(2)
    x = reform(sl(0,*),ns)
    y = reform(sl(1,*),ns)
  endelse
;   is ap(x,y) eq 1 ?
  ix = round(x/du + n/2);  ix, iy are double-indices into ap
  iy = round(y/du + n/2)
;  ix = ix # (ix*0+1)
;  iy = (iy*0+1) # iy
  ind = iy*n + ix;   ind is a single-index into ap
  ti = where(ap[ind] eq 1);  ti indexes into ind
  if (n_elements(sl) eq 0) then begin
    x = (ix[ti] - n/2)*du
    y = (iy[ti] - n/2)*du
    sl = transpose([[x],[y]])
    return,sl
  endif
  return,ti
end

pro keckwfs
;  dummy procedure to force loading
;  resolve_routine,'keckwfs'
  doc_library,'keckwfs'
end

; example code
d = .5 ; subaperture diameter
ds = .5 ; subap spacing
du = .05 ; pixel size
n = 256
ap = keckap(n,du=du)
sl = subaps_inap(ap,ds,du)
a = keckwfs_display(n,d,du,sl)
end
