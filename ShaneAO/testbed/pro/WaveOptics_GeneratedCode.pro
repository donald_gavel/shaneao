;=========== Wave Optics Simulator ================
; (c) 2010, Donald Gavel, UC Santa Cruz            
;==================================================
; Code Generated on Fri Sep 10 12:09:45 2010
;
; (line 0) ....Example Propagation (Definition)....
lambda = 5.00000e-07
du = 7.81250e-05
n = 256
roc = 0.
wf = {du: du, roc: roc, wf:fltarr(n,n)}
;
; (line 1) ....Fiber (PlaneSource)....
wf1 = complexarr(n,n)
wf1 += 1.00000
roc = 0.
;
; (line 2) ....Circle (Aperture)....
r = 10.0000*mm/(2*du)
ap = circle(n,n,n/2,n/2,r,1.)
wf3=ap*wf1
;
; (line 3) ....L (Distance)....
; INFO: L = 50.0000 meters; rayleighRange = 200.000 meters
L = 50000.0*mm
A = 1.
B = L 
C = 0.
D = 1.
dx = sqrt(L*lambda/float(n))
x = (findgen(n)-n/2)*(dx/du)+n/2
wfr = interpolate(wf3,x,x,/grid,cubic=-0.5)
wfr = fresnelft(wfr,dx,A,B,C,D,lambda)
x = (findgen(n)-n/2)*(du/dx)+n/2
wf4 = interpolate(wfr,x,x,/grid,cubic=-0.5)
;
; (line 4) ....L1 (Lens)....
roc = roc + f
f = 150.000*mm
L = 100.000*mm
Lprime = 1./(1./L - 1./f)
A = 1.
B = Lprime 
C = 0.
D = 1.
dx = sqrt(L*lambda/float(n))
x = (findgen(n)-n/2)*(dx/du)+n/2
wfr = interpolate(wf4,x,x,/grid,cubic=-0.5)
wfr = fresnelft(wfr,dx,A,B,C,D,lambda)
x = (findgen(n)-n/2)*(du/dx)+n/2
wf5 = interpolate(wfr,x,x,/grid,cubic=-0.5)
du = (f/(f-L))*du
end
