;
;  fileexists.pro - determine if a file exists
;
function fileexists,filename
  openr,1,filename,error = err
  rv = err eq 0
  close,1
  return,rv
end
