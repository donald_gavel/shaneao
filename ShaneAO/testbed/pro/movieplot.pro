;+
;
;  moviePlot.pro - produce a movie of plots
;
;-
pro moviePlot,x_arg,data
	if (n_params() eq 1) then begin
		data = x_arg
		n = (size(data))[1]
		x = findgen(n)
	endif
	if (n_params() eq 2) then begin
		n = (size(data))[1]
		x = x_arg
	endif
	mmx = minmax(x)
	mmy = minmax(data)
	window,/free
	plot,x,data[*,0],xrange=mmx,yrange=mmy,/nodata
	m = (size(data))[2]
	for k=0,m-1 do begin
		oplot,x,data[*,k]
		wait,.2
		oplot,x,data[*,k],color=0
	endfor
end
