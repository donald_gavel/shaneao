;+
;  fractalControl.pro
;     experiment with Wiener filters for
;     fractal processes
;
;   phi: phase
;   n : noise
;   z = phi + n : measurement
;
;  Rphi(t) = Integral[ Rz(t-tau) H(tau) dtau]
;
;  Rz(t) = Rphi(t) + delta(t) Sn
;  Rphi(t) = Rphi(0) - (1/2) Dphi(t)
;
;  Rphi(0) - (1/2)Dphi(t) =
;        Integral[ H(tau) dtau] Rz(0)
;        - (1/2) Integral[ Dphi(t-tau) H(tau) dtau]
;        + Sn H(t)
;
;-
dt = 0.01
t0 = 1.0
A = 1.0
n = 4096
Sn = 1.0
beta = 8./3. ; Kolmogorov process
f0 = (A/Sn)^(1./beta)
t = findgen(n)*dt
Dphi = A*(t/t0)^(beta-1)
;Rphi = (1./2.)*(max(Dphi)-Dphi)
Rphi = -(1./2.)*Dphi
Rz = Rphi*dt
Rz[0] = Rz[0]+Sn
a = [[Rz],[Rphi]]
infile = "c:\dad's\src\test.fits"
outfile = "c:\dad's\src\tout.fits"
writefits,infile,a
print,'now, run ./test.exe test.fits tout.fits and .cont'
stop
;print,'solving for h'
h = readfits(outfile)
; - optional: cut off last 10% -
h[fix(.9*n):n-1]=0.
;h = levinson(Rz,Rphi)
;Dz = -(1./2.)*Toeplitz(Dphi) + Sn*Diagonal(ones(n))
;A = Dz*dt
;b = -(1./2.)*Dphi
;ludc,a,index
;h0 = lusol(a,index,b)
;; h = invert(Dz) ## b
;plot,t,h
df = 1./(n*dt)
f = findgen(n)*df
hf = fft([h,fltarr(n)])*(2*n)*dt
i = complex(0,1.)
;  - spectral pass plot -
window,0
plot,f[1:n-1],abs(hf[1:n-1]),/xlog,/ylog
tickgrid,[.01,100.],[.001,10]
oplot,f[1:n-1],abs( (.3/(i*f[1:n-1]))/ (1.-(.3/(i*f[1:n-1])))),color=150
;  - spectral rejection plot -
window,1
plot,f[1:n-1],abs(1-hf[1:n-1]),/xlog,/ylog,yrange=[.01,10.]
tickgrid,[.01,100.],[.001,10]
oplot,f[1:n-1],abs( 1./ (1.-(.3/(i*f[1:n-1])))),color=150
;
gf = hf/(1.-hf) ; null-seeking system's loop transfer
;g = real(fft(gf,/inverse)/(2*n*dt))
; Solve g(t) - h(t)(*)g(t) = h(t) for g(t)
vp = -h*dt
vp[0] = vp[0]+1
vm = 0*vp
vm[0] = vp[0]
;print,'solving for g'
;deln = n/10
;for k=deln,n,deln do begin
;  print,'plot ',k
;  g = levinson2(vp[0:k],vm[0:k],h[0:k])
;  oplot,t[0:k],g[0:k]
;endfor
;
;g = levinson2(vp,vm,h)
;gf = fft([g,fltarr(n)])*(2*n)*dt
;
;plot,f[1:n-1],abs(hf[1:n-1]),/xlog,/ylog,ticklen=1
;oplot,f[1:n-1],abs(hf[1:n-1]),color=125
end
