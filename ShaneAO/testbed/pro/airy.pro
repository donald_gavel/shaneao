;+
;  airy.pro - perfect beam pattern
;   s is the distance in pixels to the first Airy null
;   example:
;      p = airy(n,n,1.22*(lambda/d)/pixelscale)
;-
function airy,n,m,s
  x = findgen(n) # (fltarr(m)+1) - n/2
  y = (fltarr(n) + 1) # findgen(m) - m/2
  r = sqrt(x^2 + y^2)>.000001
  ret = (beselj(1.22*r/s*!pi,1)/r)^2
  return,ret
end

