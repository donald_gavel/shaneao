;+
; torect.pro - convert from polar to rectangular coordinates
;  see also: topolar.pro
;
;  a = torect(pa)
;
;   pa = n/2 x 2*n array, organized as r,theta values of a
;   a = n x n array, organized as x,y values of a
;-
function torect,pa
  n = (size(pa))[1]*2
  a = fltarr(n,n)
  x = ones(n) # findgen(n) - n/2
  y = transpose(x)
  th = round(atan(y,x)*float(n)/!pi) + n
  r = round(sqrt(x^2+y^2))
  a = pa(r,th)
;  indpa = reform(r+th*n/2,n*n)
;  a = pa[indpa]
;  a = reform(a,n,n)
return,a
end
;
; test pattern
;
n = 128
pa = fltarr(n/2,2*n)
pa[0,*] = 1
pa[1,*] = .5
pa[n/4,*] = .25
a = torect(pa)
; here's a wild test:
; a2 = torect(topolar(torect(topolar(torect(pa)))))
end
