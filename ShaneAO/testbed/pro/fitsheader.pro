;+
;  fitsheader.pro - document the Jupiter data reductions
;       by writing a fits header
;-
function fitsheader,data,dataset
  nx = (size(data))(1)
  ny = (size(data))(2)
  hdr = strarr(28)
  hdr(0) = headerline('SIMPLE','T','')
  hdr(1) = headerline('BITPIX',-32,'')
  hdr(2) = headerline('NAXIS',2,'')
  hdr(3) = headerline('NAXIS1',nx,'')
  hdr(4) = headerline('NAXIS2',ny,'')
  hdr(5) = headerline('DATE',"'" + dataset.date + "'",'Original date of data collection')
  hdr(6) = headerline('UTC',"'" + dataset.time + "'",'Time of data collection, UT')
  hdr(7) = headerline('LAMBDA',dataset.wavelength,'central wavelength')
  hdr(8) = headerline('FILTER',"'" + dataset.filter + "'",'550, 700, 850, 880')
  hdr(9) = headerline('SELON',dataset.selon,'sub earth longitude')
  hdr(10) = headerline('SELAT',dataset.selat,'sub earth latitude')
  hdr(11) = headerline('SSLON',dataset.sslon,'sub sun longitude')
  hdr(12) = headerline('SSLAT',dataset.sslat,'sub sun latitude')
  hdr(13) = headerline('ROTANG',dataset.rotang,'original rotation angle')
  hdr(14) = headerline('DATASET',"'" + dataset.datafile + "'",'original data set name')
  hdr(15) = headerline('TINT',dataset.tint,'integration time, per frame, in seconds')
  hdr(16) = headerline('NFRAMES',dataset.nframes,'number of speckle frames used')
  hdr(17) = headerline('PIXSIZE',dataset.pixelsize,'pixel scale, in arcseconds')
  hdr(18) = headerline('MAJAXIS',dataset.majoraxis,'major axis of planet ellipse, in arcseconds')
  hdr(19) = headerline('MINAXIS',dataset.minoraxis,'minor axis of planet ellipse, in arcseconds')
  hdr(20) = headerline('PROCDATE',"'" + dataset.procdate + "'",'Date of speckle processing')
  hdr(21) = headerline('PROJLAT',dataset.spotlat,'central pixel latitude')
  hdr(22) = headerline('PROJLON',dataset.spotlon,'central pixel longitude')

  line = string(' ',format='(A80)')
  strput,line,'COMMENT'
  hdr(23) = line
  
  line = string(' ',format='(A80)')
  strput,line,'COMMENT   speckle processed image of'
  hdr(24) = line

  line = string(' ',format='(A80)')
  strput,line,'COMMENT   Jupiter/SL9 collision'
  hdr(25) = line

  line = string(' ',format='(A80)')
  strput,line,'COMMENT'
  hdr(26) = line

  line = string(' ',format='(A80)')
  strput,line,'END'
  hdr(27) = line

  return,hdr
end
