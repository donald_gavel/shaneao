;
;  sf.pro - calculate the sample structure function of data
;
;   D_phi = 1/N Integral( [phi(x) - phi(x+r)]^2 dx )
;
;  revision: 12/16/04 DG
;     allow for aperture (ap) and two types of normalization:
;      0: divide by the number of points where both x and x+r are in the aperture
;      1: divide by the number of points where x is in the aperture
;
function sf,phi,ap,normalization=normalization
  s = size(phi)
  n = s[1]
  m = s[2]
  type = s[3]
  if (type eq 5) then begin
    phi2 = dblarr(2*n,2*m)
    w2 = dblarr(2*n,2*m)
    fuzz = 1.d-16
  endif else begin
    phi2 = fltarr(2*n,2*m)
    w2 = fltarr(2*n,2*m)
    fuzz = 1.e-6
  endelse
  if (n_elements(ap) eq 0) then ap = ones(n,n)
  if (n_elements(normalization) eq 0) then normalization = 0
  phi2[n/2:n+n/2-1,m/2:m+m/2-1] = phi*ap
  w2[n/2:n+n/2-1,m/2:m+m/2-1] = ap
  phi2f = ft(phi2)
  w2f = ft(w2)
  s = real(ft(w2f*conj(w2f),/inverse))
  ind = where(s le fuzz,count)
  cphi = real(ft(phi2f*conj(phi2f),/inverse))
  if (normalization eq 0) then begin
    if (count gt 0) then s[ind] = 1.0
    cphi = cphi / s
  endif else begin
    cphi = cphi / max(s)
  endelse
  dphi = 2*(cphi[n,m] - cphi)
  if (count gt 0) then dphi[ind] = 0.0
  return,dphi
end