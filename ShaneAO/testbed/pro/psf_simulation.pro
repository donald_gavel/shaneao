;+
; psf_simulation.pro
;    code to generate a "realistic" PSF image
;    for a given
;     - science detector characteristics
;     - seeing conditions
;     - AO system and telescope
;
; Author:
;   Donald Gavel, UCSC
;   March 29, 2010
;-

; -- general constants --
arcsec = !pi/(180.*3600.)
microns = 1.e-6
cm = 1.e-2  ; centimeter
nm = 1.e-9  ; nanometer
mas = 0.001*arcsec  ; milli-arcsecond

; -- input your parameters here: --
Dtele = 10            ; telescope diameter
lambda = 1.2*microns  ; science wavelength
r00 = 14*cm           ; seeing (r0 at 0.5 microns)
pixelScale = 2*mas    ; size of a pixel on-sky
n = 1024              ; number of pixels across detector
sigma_n = 0           ; read noise in photons per pixel
sky = 0               ; sky background in photons per pixel
nPh = 10.e6            ; total number of detected photons in PSF in an exposure
wfe = 200*nm          ; residual rms high-order wavefront error after AO
ttBlur = 0.*mas       ; residual rms tip/tilt blur after AO

;  -- core-halo model of psf --
;   core is Airy, with Strehl % of light
;   halo is Lorentzian, with FWHM equal to seeing disk
lambda0 = 0.5*microns
r0 = r00*(lambda/lambda0)^(6./5.)
strehl = exp(-((2*!pi/lambda)*wfe)^2)
;core = airy(n,n,1.22*(lambda/Dtele)/pixelscale)
print,'<psf_simulation> computing core' & wait,.01
core = keckpsf(n,lambda=lambda,dx=pixelscale,ap=ap)
core /= total(core)
print,'<psf_simulation> computing halo' & wait,.01
halo = lorentzian(n,n/2,n/2,0.5*(lambda/r0)/pixelscale)
halo /= total(halo)
gamma = max(halo)/max(core)
alpha = (strehl-gamma)/(1-gamma)
psf = alpha*core + (1-alpha)*halo
image = psf*nph
print,'<psf_simulation> blurring by tip/tilt residual' & wait,.01
imageHO = image
if (ttblur ne 0) then begin
  ttkernel = gauss2(n,n/2,n/2,ttBlur/pixelscale)
  ttkernel /= total(ttkernel)
  image = convolve(imageHO,ttkernel)
endif
print,'<psf_simulation> adding noise' & wait,.01
photonNoiseSD = sqrt(image)
photonNoise = randomn(seed,n,n)*photonNoiseSD
image += photonNoise
readnoise = randomn(seed,n,n)*sigma_n
image += readNoise
skynoise = randomn(seed,n,n)*sqrt(sky)
image += skynoise
print,'<psf_simulation> displaying result' & wait,.01
disp,image,'noisy PSF image, nPh = '+strtrim(nPh,2)+' photons',x0 = -(n/2)*pixelScale/arcsec*[1,1],dx = pixelScale/arcsec*[1,1]
print,'<psf_simulation> done' & wait,.01
end
