; ----------------------------------------------
; pwfs_widget_v3.pro
;   Simulates the Pyramid wavefront sensor
;   Don Gavel, 2/21/10

;------------------------------------
; Widgets for GUI
;
PRO pwfs_widget_event, event 
  
  wTarget = event.id
  wWidget = event.top
  case wTarget of
  
  widget_info(wWidget,find_by_uname='Info'): begin
    w = widget_info(wWidget,find_by_uname='InfoLabel')
    widget_control,w,get_value=txt
    printInfo,txt
  end
  
  widget_info(wWidget,find_by_uname='FocusCheck'): begin
    print,'<pwfs_widget_event> FocusCheck'
  end

  widget_info(wWidget,find_by_uname='Calibrate'): begin
    print,'<pwfs_widget_event> Calibrate'
    w = widget_info(wWidget,find_by_uname='Active')
    widget_control,w,set_value='<<< STOP >>>',/sensitive
    
    calibrate
    
    widget_control,w,set_value='   Ready   ',sensitive=0
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,/sensitive,set_value='Disp*'
    w = widget_info(wWidget,find_by_uname='Run')
    widget_control,w,/sensitive
  end
  
  
  widget_info(wWidget,find_by_uname='Run'): begin
    print,'<pwfs_widget_event> Run'
    w = widget_info(wWidget,find_by_uname='Active')
    widget_control,w,set_value='<<< STOP >>>',/sensitive
    
    run
        
    widget_control,w,set_value='   Ready   ',sensitive=0
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,/sensitive,set_value='Disp*'
  end
  
  widget_info(wWidget,find_by_uname='Disp'): begin
    print,'<pwfs_widget_event> Disp'

    dispResults

    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,set_value='Disp'
  end
  
  widget_info(wWidget,find_by_uname='Reset'): begin
    print,'<pwfs_widget_event> Reset'
    w = widget_info(wWidget,find_by_uname='Disp')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='Run')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='FocusCheck')
    widget_control,w,sensitive=0
    w = widget_info(wWidget,find_by_uname='Calibrate')
    widget_control,w,sensitive=0
  end
  
  widget_info(wWidget,find_by_uname='Active'): begin
    print,'<pwfs_widget_event> STOP!'
  end
  
  widget_info(wWidget,find_by_uname='Clear'): begin
    print,'<pwfs_widget_clear> Clear'
    clearwins
  end
  
  widget_info(wWidget,find_by_uname='Quit'): begin
    print,'<pwfs_widget_event> Quit'
    widget_control, event.top, /destroy
  end
  
  else:
  
  endcase
END 
 
PRO pwfs_widget 
  common pwfsWidget,version,date,base

  if (n_elements(base) ne 0) then begin
    if Widget_info(base,/valid_id) then Widget_control,base,/destroy
  endif
  
  version = '3.1'
  date = '10/8/2010'

  infoText = 'PWFS_widget v'+version+' '+date

  device,decomposed=1
  base = WIDGET_BASE(COLUMN=1) 
  print,'<pwfs_widget> base = ',base
  infoLabel = widget_label(base, value=infoText,uname='InfoLabel')
  infoButton = widget_button(base, value='Info',uname='Info')
  dispButton = widget_button(base, value='Calibrate',uname='Calibrate')
  dispButton = widget_button(base, value='Focus Check',uname='FocusCheck',sensitive=0)
  dispButton = widget_button(base, value='Run',uname='Run',sensitive=0)
  dispButton = widget_button(base, value='Display',uname='Disp',sensitive=0)
  resetButton = widget_button(base, value='Reset',uname='Reset')
  clearButton = widget_button(base, value='Clear',uname='Clear')
  activeLabel = widget_button(base,value='   Ready   ',uname='Active',sensitive=0)
  quitButton = widget_button(base, value='Quit',uname='Quit')
  
  WIDGET_CONTROL, base, /REALIZE 
  XMANAGER, 'pwfs_widget', base, /no_block 
  
END

; -------------- Callbacks ---------------------

pro initPhysical
  common physical, i, microns, nm, mm, cm, arcsec
  common main,runId,theLightBeam,theLightBeamReference,theAtmosphere,testWavefront,theTelescope,theDM,theLensletArray,thePWFS

  i = complex(0,1.)
  microns = 1.e-6
  nm = 1.e-9
  mm = 1.e-3
  cm = 1.e-2
  arcsec = !pi/(180.*3600.)
end

pro printInfo,label
    common main
    bar = '========================================='
    print,bar
    print,label
    print,bar
    print,'------- theTelescope ---------'
    help,/structure,theTelescope
    print,'------- theAtmosphere --------'
    help,/structure,theAtmosphere
    print,'------------theDM ------------'
    help,/structure,theDM
    print,'------- theLightBeam ---------'
    help,/structure,theLightBeam
    print,'theLightBeam.planes = ',strjoin(theLightBeam.planes,', ')
    print,'theLightBeam.du = ',theLightBeam.du
    print,'------- theLensletArray ------'
    help,/structure,theLensletArray
    print,'---------- thePWFS -----------'
    help,/structure,thePWFS
    print,bar
end

pro show,it
  common physical
  common main
  
  isa = tag_names(it,/structure_name)

  case isa of

  'LIGHTBEAM': begin
    lightBeam_disp,it
  end
  'LENSLETARRAY': begin
    disp,arg(it.opd),'LensletArray'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
  end
  'TELESCOPE': begin
    n = it.n
    du = it.du
    disp,it.ap,'Telescope'+' '+runId,dx=[1,1]*du,x0=-[1,1]*float(n/2)*du
  end
  'WAVEFRONT': begin
    n = it.n
    du = it.du
    disp,it.phase,it.kind+' wavefront phase'+' '+runId,dx=[1,1]*du,x0=-[1,1]*float(n/2)*du
  end
  'DM': begin
    n = it.n
    du = it.du
    disp,it.phase,'DM'+' '+runId,dx=[1,1]*du,x0=-[1,1]*float(n/2)*du
  end
  'PWFS': begin
    n = it.n
    du = it.du
    du0 = theDM.du
    disp,it.pupilImageReference,'PWFS calibration flat image'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
    disp,it.pupilImage,'PWFS pupil image'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
    disp,it.pupilImageMask,'PWFS pupils mask'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
    u = fltarr(n,n,2)
    u[*,*,0] = it.sx0
    u[*,*,1] = it.sy0
    disp,u,'PWFS reference slopes'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
    u[*,*,0] = it.sx
    u[*,*,1] = it.sy
    disp,u,'PWFS measured slopes'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm
    u[*,*,0] = it.sx-it.sx0
    u[*,*,1] = it.sy-it.sy0
    disp,u,'PWFS measured-reference slopes'+' '+runId,dx=[1,1]*du/mm,x0=-[1,1]*float(n/2)*du/mm    
    disp,it.phase_est,'PWFS estimated phase'+' '+runId,dx=[1,1]*du0,x0=-[1,1]*float(n/2)*du0
    
  end
  else:

  endcase

  help,/structure,it
  
end

pro prop,theWavefront
  common physical
  common main
  
  print,'<prop> starting propagation of '+theWavefront.kind+' wavefront' & wait,.01
  E = theTelescope.ap * exp(i*(theWavefront.phase-theDM.phase))              & theLightBeam.E[*,*,0] = E
  E = conj(ft(E))
  E = fshift(E,-0.5,-0.5)                                                     & theLightBeam.E[*,*,1] = E
  E *= theLensletArray.opd                                                    & theLightBeam.E[*,*,2] = E
  E = fresnel(E,theLensletArray.du,theLensletArray.f,theLensletArray.lambda)  & theLightBeam.E[*,*,3] = E
  thePWFS.pupilImage = abs(E)^2
  print,'<prop> done' & wait,.01
end

pro calibrate
  common main
  theSource = theAtmosphere
  theSource.kind = 'Plane'
  wavefront_init,theSource
  prop,theSource
  theLightBeamReference = theLightBeam
  thePWFS.pupilImageReference = abs(theLightBeamReference.E[*,*,3])^2
  reconstruct,/reference
end

pro calScale
  common main
  calibrate
  ap = theTelescope.ap
  prop,testWavefront
  reconstruct
  f = depiston(testWavefront.phase,ap)
  g = depiston(thePWFS.phase_est,ap)
  alpha = total(ap*f*g)/total(ap*g*g)
  r = f - alpha*g
  print,'alpha =',alpha,' rms residual: ',rms(r,ap),' vs data: ',rms(f,ap)
  thePWFS.scaleFactor = alpha
end

pro run,test=test
  common main
  
  if (keyword_set(test)) then prop,testWavefront else prop,theAtmosphere
  reconstruct
end

pro runCL,niter,test=test,restart=restart ; run closed loop
  common physical
  common main
  common diagnostics, wfe_rms, dm_hist, psf_hist, wfe_hist
  
  if (keyword_set(restart)) then theDM.phase *= 0.0
  if (keyword_set(test)) then ph0 = testWavefront.phase else ph0 = theAtmosphere.phase
  
  n = theDM.n
  lambda = theLightBeam.lambda
  ap = theTelescope.ap
  
  dm_hist = fltarr(n,n,niter)
  psf_hist = fltarr(n,n,niter)
  wfe_hist = fltarr(n,n,niter)
  wfe_rms = fltarr(niter+1)
  wfe_rms[0] = rms(ph0-theDM.phase,ap)*lambda/(2*!pi)/nm
  print,'<runCL> starting rms wf error = ',strtrim(wfe_rms[0]),' nm'
  for iter = 0,niter-1 do begin
    ;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    run,test=test
    theDM.phase += thePWFS.feedbackGain * thePWFS.phase_est
    ;~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    dm_hist[*,*,iter] = theDM.phase
    psf_hist[*,*,iter] = abs(theLightBeam.E[*,*,1])^2
    wfe_hist[*,*,iter] = depiston(ph0 - theDM.phase,ap)*ap*lambda/(2*!pi)/nm
    wfe_rms[iter+1] = rms(wfe_hist[*,*,iter],ap)
    print,'iter ',strtrim(iter,2),' rms wf error = ',strtrim(wfe_rms[iter]),' nm'
    wait,.01 ; force print out at end of each iteration
  endfor
  
  limdm = minmax(dm_hist)
  limp = minmax(psf_hist)
  limw = minmax(wfe_hist)
  for iter=0,niter-1 do begin
    dm_hist[0,0:1,iter] = limdm   ; this fixes the scale range for display
    psf_hist[0,0:1,iter] = limp
    wfe_hist[0,0:1,iter] = limw
  endfor
end

pro dispResults
  common main
  disp,(abs(theLightBeam.E[*,*,3])-abs(theLightBeamReference.E[*,*,3]))*total(thePWFS.pupilImageMask,3)
  disp,theAtmosphere.phase*theTelescope.ap
end

pro reconstruct,reference=reference
  common main
 
  n = theLensletArray.n
  du = theLensletArray.du
  s = round(theLensletArray.d/du/2.)

  if (keyword_set(reference)) then begin
    u = thePWFS.pupilImageReference
  endif else begin
    u = thePWFS.pupilImage
  endelse
  
  a = shift(u*thePWFS.PupilImageMask[*,*,0],s,-s)
  b = shift(u*thePWFS.PupilImageMask[*,*,1],-s,-s)
  c = shift(u*thePWFS.PupilImageMask[*,*,2],s,s)
  d = shift(u*thePWFS.PupilImageMask[*,*,3],-s,s)
  pmask = shift(thePWFS.PupilImageMask[*,*,0],s,-s)
  denom = a+b+c+d
  eps = 0.001*max(denom)
  dmask = real(denom gt eps)
  denom = denom*dmask + eps*(1-dmask)
  sx = dmask*pmask*((b+d)-(a+c))/denom
  sy = dmask*pmask*((a+b)-(c+d))/denom
    
  if (keyword_set(reference)) then begin
    thePWFS.sx0 = sx
    thePWFS.sy0 = sy
  endif else begin
    thePWFS.sx = sx
    thePWFS.sy = sy
    thePWFS.phase_est = ftwfs(sx-thePWFS.sx0,sy-thePWFS.sy0,pmask,boundaryFix=thePWFS.fixMethod,/verbose)*pmask
  
  ; interpolate the estimate on to the DM grid
    x = -(findgen(n)-n/2)*(theDM.du/thePWFS.duPrime) + n/2
    thePWFS.phase_est = interpolate(-thePWFS.phase_est,x,x,/grid,cubic=-0.5)
    thePWFS.phase_est *= thePWFS.scaleFactor
  endelse
end

pro stretch,sf
  common main
  n = thePWFS.n
  u = thePWFS.phase_est
  dlenslet = theLensletArray.d
  du = theLensletArray.du
  
  s = round((dlenslet/2)/du)
  r = s*(theLensletArray.fnum/theTelescope.fnum)
  magicStretchFactor = sf
  r = r/magicStretchFactor
  duPrime = (theTelescope.d/theLensletArray.d)*(theTelescope.fnum/theLensletArray.fnum)*theLensletArray.du
  duPrime *= magicStretchFactor
  thePWFS.duPrime = duPrime
  mask = circle(n,n,n/2,n/2,r,1.)
  thePWFS.pupilImageMask[*,*,0] = shift(mask,-s,s)
  thePWFS.pupilImageMask[*,*,1] = shift(mask,s,s)
  thePWFS.pupilImageMask[*,*,2] = shift(mask,-s,-s)
  thePWFS.pupilImageMask[*,*,3] = shift(mask,s,-s)
  
end

pro stretchOpt
  common main
  
  savedPWFS = thePWFS
  n = thePWFS.n
  ap = theTelescope.ap
  calibrate
  prop,testWavefront
  
  nsteps = 30
  sf_min = 0.7
  sf_max = 1.3
  dsf = (sf_max-sf_min)/float(nsteps)
  sf_steps = findgen(nsteps+1)*dsf + sf_min
  ph = fltarr(n,n,nsteps+1)
  for k = 0,nsteps do begin
    stretch,sf_steps[k]
    reconstruct,/reference
    reconstruct
    f = depiston(testWavefront.phase,ap2)
    g = depiston(thePWFS.phase_est,ap2)
    alpha = total(ap2*f*g)/total(ap2*g*g)
    r = f - alpha*g
    ph[*,*,k] = r*ap
    print,strtrim(k,2),' sf =',strtrim(sf_steps[k],2),' alpha =',alpha,' rms residual: ',rms(r,ap),' vs data: ',rms(f,ap)   
  endfor
  disp,ph
  
  thePWFS = savedPWFS  ; bring it back to the original state
end


; ------------ initializers -----------
;

pro lightBeam_disp,aLightBeam
  common physical
  common main
  title = aLightBeam.description+' '+runId
  n = aLightBeam.n
  du = aLightBeam.du
  help,/structure,aLightBeam
  print,aLightBeam.planes
  disp,abs(aLightBeam.E),'Abs '+title,dx=[1,1]*du/microns,x0=-[1,1]*du*n/2/microns
  disp,arg(aLightBeam.E),'Arg '+title,dx=[1,1]*du/microns,x0=-[1,1]*du*n/2/microns
end

pro wavefront_init,theWavefront
  common physical
  
  n = theWavefront.n
  du = theWavefront.du
  r0 = theWavefront.r0
  lambda0 = theWavefront.lambda0
  
  if (theWavefront.kind eq 'Kolmogorov') then begin
    f=screengen(n,n,r0,du)
    theWavefront.phase = screengen(f,seed)
  endif
  
  if (theWavefront.kind eq 'Sine') then begin
    a = theWavefront.par[0]  ; amplitude in radians of phase; 0.1
    f = [theWavefront.par[1],theWavefront.par[2]]; [4.,4.] ; spatial frequency, cycles per meter
    c = theWavefront.par[3]  ; phase shift, radians of cycle
    x = (ones(n) # findgen(n) - n/2)*du
    y = transpose(x)
    theWavefront.phase = a*sin(2*!pi*(f[0]*x+f[1]*y)+c)
  endif
  
  if (theWavefront.kind eq 'Plane') then begin
    theWavefront.phase = fltarr(n,n)
  endif
  
  if (theWavefront.kind eq 'Zernike') then begin
    nx = theWavefront.n
    ny = nx
    a = theWavefront.par[0]; amplitude
    n = theWavefront.par[1]; radial index n; 2,0 = focus
    m = theWavefront.par[2]; azimuthal index
    l = theWavefront.par[3]; selector index when m>0
    r = 0.5/du ; radius of Nickel telescope ap
    z = zernike(nx,ny,r,n,m)
    if (l eq -1) then theWavefront.phase = imaginary(z) else theWavefront.phase = real(z)
  endif
  
  if (theWavefront.kind eq 'Bump') then begin
    a = theWavefront.par[0]  ; amplitude
    x = theWavefront.par[1]  ; position x; 0.25
    y = theWavefront.par[2]  ; position y; 0.25
    sd = theWavefront.par[3] ; standard deviation; 0.1
    theWavefront.phase = gauss2(n,n/2+x/du,n/2+y/du,sd/du)
  endif
  
  if (theWavefront.kind eq 'Tilt') then begin
    theWavefront.phase = ones(n) ## (findgen(n) - n/2)*2*!pi/(1./du)
  end
   
end

pro go
  pwfs_widget
end

; ---------------- Basic Parameters ------------------------------

common pwfsWidget ; defined in pwfs_widget
common physical; i, microns, nm, mm, cm, arcsec; defined in initPhysical
common main ; runId,theLightBeam,theLightBeamReference,theAtmosphere,testWavefront,theTelescope,theDM,theLensletArray,thePWFS ; defined in initPhysical
common diagnostics ; wfe_rms, dm_hist, psf_hist, wfe_hist ; defined in runCL

initPhysical

runID = ''             ; title for run
n = 256; 512           ; fine grid size
du = 4*microns         ; fine sampling on the lenslet array and pupil images
lambda = 0.658*microns ; wavelength
f_num_in = 20.         ; f/number of beam into lenslet
f_num_out = 18.        ; f/number of lenslet
dtele = 3.0            ; telescope diameter
dsec = 0.8             ; secondary diameter
telescope = 'Shane 3-meter' ;'Nickel 1-meter'
dlenslet = 500*microns ; diameter and pitch of lenslet
r0 = 10*cm             ; seeing parameter
lambda0 = 0.5*microns  ; wavelength corresponding to seeing parameter
feedbackGain = 0.05     ; control loop gain

; --------------- Definition of Data Structures -------------------
; and derived parameters

nPlanes = 4
planes = [ 'aperture', 'before lenslet', 'after lenslet', 'lenslet pupil image' ]
f = f_num_in * dtele       ; focal length of telescope
du0 = f * lambda / (n*du)  ; fine samping across telescope aperture
duPrime = (dtele/dlenslet)*(f_num_in/f_num_out)*du ; sampling of telescope aperture on pupil image fine sample

theLightBeam = {lightBeam, $
                description: 'complex phase at intermediate planes', $
                n: n, $                       ; number of fine pixels across
                du: [du0,du,du,du], $         ; fine pixel size at each plane
                lambda: lambda, $             ; wavelength of the light
                planes: planes,  $            ; names of the planes
                E: complexarr(n,n,nPlanes) $  ; the complex E field at various planes
                }

theLensletArray = {lensletArray, $
                  n: n, $            ; number of fine pixels across
                  du: du, $          ; fine pixel size
                  fnum: f_num_out, $ ; f-number of a lenslet
                  d: dlenslet, $     ; diameter of a lenslet, also known as the pitch
                  f: real(0.), $     ; focal length of a lenslet
                  lambda: lambda, $  ; wavelength at which opd is defined
                  opd: complexarr(n,n) $ ; the optical-path delta of the lenslet
                  }
                  theLensletArray.f = theLensletArray.fnum * theLensletArray.d
                  theLensletArray.opd = genlensletarray(theLensletArray.d, theLensletArray.f, theLensletArray.du, theLensletArray.n, wavelength = theLensletArray.lambda)

theTelescope = {telescope, $
                description: telescope, $
                n: n,  $           ; number of fine pixels across
                du: du0, $         ; fine pixel size on aperture
                dx: du, $          ; fine pixel size at focus
                fnum: f_num_in, $  ; f-number of the beam (into the wavefront sensor)
                d: dtele, $        ; diameter of the primary
                dsec: dsec, $      ; diameter of the secondary
                f: real(0.),  $    ; apparent distance from aperture to focus
                ap: fltarr(n,n) $  ; aperture function
                }
                theTelescope.f = theTelescope.fnum * theTelescope.d
                if (n*du0 le dtele) then print,'<telescope_init> *WARNING* aperture grid ', n*du0, 'is smaller than the telescope diameter ',dtele
                theTelescope.ap = circle(n,n,n/2,n/2,(dtele/2.)/du0,1.) - circle(n,n,n/2,n/2,(dsec/2.)/du0,1.)

theAtmosphere = {wavefront, $
                n:n, $                 ; fine sample grid size
                du: du0, $             ; fine sampling on atmosphere
                r0: r0, $              ; seeing
                lambda0: lambda0, $    ; specification wavelength
                kind: 'Kolmogorov', $  ; select: 'Kolmogorov', 'Sine','Zernike','Plane', 'Bump'
                par: fltarr(4),    $   ; parameters for selected kind: see wavefront_init
                phase: fltarr(n,n) $   ; phase aberration
                }
                wavefront_init,theAtmosphere
                
testWavefront = theAtmosphere
        testWavefront.kind = 'Sine'
        a = 200.*nm*2*!pi/lambda
        kx = 4.
        ky = 2.
        testWavefront.par = [a,kx,ky,0.]
        wavefront_init,testWavefront
  
                
theDM = {DM, $
        n:n, $
        du: du0, $
        phase: fltarr(n,n) $
        }

thePWFS = {PWFS, $
          n: n, $
          du: du, $                            ; fine pixel size on the PWFS detector
          duPrime: duPrime, $                  ; PWFS pixel size as mapped to telescope aperture
          pupilImageReference: fltarr(n,n), $  ; calibration image of flat wavefront
          pupilImage: fltarr(n,n), $           ; from the aberated wavefront
          pupilImageMask: fltarr(n,n,4), $     ; the locations of the pupils
          sx0: fltarr(n,n), $                  ; reference slopes
          sy0: fltarr(n,n), $
          sx: fltarr(n,n), $                   ; measured slopes
          sy: fltarr(n,n), $
          fixMethod: 'none', $                 ; boundary fix technique: 'none' 'noCurl' 'iterative'
          phase_est: fltarr(n,n), $            ; output of reconstructor
          scaleFactor: 1.0, $                  ; scale factor mapping DN from the reconstructor to phase on the DM (calculated in calScale)
          feedbackGain: feedbackGain $         ; closed loop iteration feedback gain
          }
          s = round((dlenslet/2)/du)
          r = s*(theLensletArray.fnum/theTelescope.fnum)
          magicStretchFactor = 1.0
          r = r/magicStretchFactor
          thePWFS.duPrime = duPrime*magicStretchFactor
          mask = circle(n,n,n/2,n/2,r,1.)
          thePWFS.pupilImageMask[*,*,0] = shift(mask,-s,s)
          thePWFS.pupilImageMask[*,*,1] = shift(mask,s,s)
          thePWFS.pupilImageMask[*,*,2] = shift(mask,-s,-s)
          thePWFS.pupilImageMask[*,*,3] = shift(mask,s,-s)

; --------------- go --------------------------

go
end
