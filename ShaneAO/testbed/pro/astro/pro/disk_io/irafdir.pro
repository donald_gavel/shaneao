pro irafdir,directory,TEXTOUT=textout
;+
; NAME:
;	IRAFDIR
; PURPOSE:
;	Provide a brief description of the IRAF images on a directory
; CALLING SEQUENCE:
;     IRAFDIR, [ directory, TEXTOUT = ]
;
; OPTIONAL INPUT PARAMETERS:
;	DIRECTORY - String giving file name, disk or directory to be searched 
;
; OPTIONAL INPUT KEYWORD:
;	TEXTOUT - specifies output device (see TEXTOPEN)
;		textout=1	TERMINAL using /more option
;		textout=2	TERMINAL without /more option
;		textout=3	<program>.prt
;		textout=4	laser.tmp
;		textout=5      user must open file
;		textout = filename (default extension of .prt)
;
; OUTPUT PARAMETERS:
;	None
;
; PROCEDURE:
;	FINDFILE is used to find all '.imh' files in the directory. 
;	The object name and image size (NAXIS1, NAXIS2) are extracted
;	from the header. Each header is also searched for the parameters
;	DATE-OF-OBS, TELESCOP, EXPTIME.
;  
; SYSTEM VARIABLES:
;	If 'textout' keyword is not specified to select an output device,
;	!TEXTOUT will be the default.    This non-standard system variable
;	can be added using the procedure ASTROLIB.
;
; PROCEDURE CALLS:
;	FDECOMP, REMCHAR, TEXTOPEN, TEXTCLOSE
; MODIFICATION HISTORY:
;	Written, K. Venkatakrishna, ST Systems Corp, August 1991
;-

 On_error,2                          ;Return to caller
 
 ext='*.imh'

 if keyword_set(directory) then dir = strlowcase(directory)

 if N_ELEMENTS(dir) eq 0 then cd,current = dir

 if !VERSION.OS NE "vms" then dir = dir + "/"

 fil = findfile( dir + ext, COUNT=nfiles)
 if nfiles EQ 0 then $
    message,'No IRAF (*.imh) files found '

; Set output device according to keyword TEXTOUT or system variable !TEXTOUT

 if not keyword_set(textout) then textout=!textout
 textopen,'irafdir',TEXTOUT=textout

;  Print the title header
 printf,!textunit,format='(a,/)','IRAF file directory  '+strmid(!stime,0,17)
 printf,!textunit,$
'       NAME         SIZE     OBJECT      DATE-OF-OBS     TELESCOP      EXP TIME'

 get_lun,lun1
 fmt = '(a15,2x,i4,2x,i4,2x,a10,4x,a8,7x,a8,5x,a8)'
 index = 2052 + indgen(80)*2
 dir2 = 'dummy'
 for i=0,nfiles-1 do begin                          ;Loop over each .imh file
   file1 = fil(i)                                       
   fdecomp,file1,disk,dir2,fname,qual             ;Decompose into disk+filename
   openr,lun1,file1,/stream                       ;open the file
   tmp = assoc(lun1,bytarr(32))
   hdr = tmp(0)
   hdr2 = hdr                                         ;Read the first 572 bytes
   if !VERSION.OS NE "vms" then begin
      byteorder,hdr,/sswap                               ; Perform byte swaps
      byteorder,hdr,/lswap 
   endif
   hdrlen = fix(hdr,12)                               ;Extract header length,
   ndim = fix(hdr,20)                                 ; number of dimensions,
   naxis1 = long(hdr2,24)                             ; dimension vector
   naxis2 = long(hdr2,28)
   tmp1 = assoc(lun1,bytarr(hdrlen*4,/NOZERO))               
   hdr = tmp1(0)                                     ;Read the entire header
   close,lun1
   if !VERSION.OS NE "vms" then byteorder,hdr,/sswap  ;
   nfits = (hdrlen*4-2054)/162                     ; find the number of records
   hd = ''                                         ; to break the header into

; Form the string 'hd', skipping alternate bytes from byte 2052 onwards.
; hd will be a FITS style header, that contains all the basic information 

 for j = 0,nfits-1 do hd = [hd,string ( hdr( (162*j + index)))]    
   keyword = strtrim( strmid(hd,0,8),2 )
   value = strtrim( strmid(hd,10,20),2 )

 l = where(keyword EQ 'OBSERVAT',nfound)           ;Search for OBSERVAT keyword
 if nfound GT 0 then begin
      telescop = value(l(0))
      remchar,telescop,"'"
 endif else telescop = '   ?      '

 l = where(keyword EQ 'EXPTIME',nfound)           ;Search for EXPTIME keyword
 if nfound GT 0 then begin
    exptim = float(value(l(0)))
    if exptim EQ 0. then exptim = '   ?      ' else $
                 exptim = string(exptim,format= '(f7.1)')     
 endif else exptim ='    ?   '

 l = where(keyword EQ 'DATE-OBS' ,nfound)       ;Search for DATE-OBS keyword 
 if nfound GT 0 then begin
   date=value(l(0))
   remchar,date,"'"
 endif else date = '   ?      '
 object = string(hdr(732+indgen(8)*2))               ;Extract object name from

 if dir2 NE dir then begin			;Has directory changed?   
       if ( dir2 EQ '' ) then cd,current=dir else dir = dir2
       printf,!textunit,format='(/a/)',disk+dir   ;Print new directory
       dir = dir2                                  ;Save new directory
 endif                   
;                                                  original header 
 printf,!textunit,FORMAT=fmt,fname,naxis1,naxis2,object,date,telescop,exptim
 if textout EQ 1 then if !ERR EQ 1 then return
 endfor

 textclose, TEXTOUT=textout
 free_lun, lun1

 return
 end

