;+
; Project     : SOHO - CDS     
;                   
; Name        : TAG_EXIST()
;               
; Purpose     : To test whether a tag name exists in a structure.
;               
; Explanation : Routine obtains a list of tagnames and tests whether the
;               requested one exists or not. The search is recursive so 
;               if any tag names in the structure are themselves structures
;               the search drops down to that level.
;               
; Use         : IDL>  status = tag_exist(str, tag)
;    
; Inputs      : str  -  structure variable to search
;               tag  -  tag name to search for
;               
; Opt. Inputs : None
;               
; Outputs     : Function returns 1 if tag name exists or 0 if it does not.
;               
; Opt. Outputs: None
;               
; Keywords    : INDEX = index of matching tag
;
; Calls       : None
;
; Common      : None
;               
; Restrictions: None
;               
; Side effects: None
;               
; Category    : Util, structure
;               
; Prev. Hist. : None
;
; Written     : C D Pike, RAL, 18-May-94
;               
; Modified    : Version 1.1, D Zarro, ARC/GSFC, 27-Jan-95
;               Passed out index of matching tag
;
; Version     : Version 1.1, 27-Jan-95
;-            

function tag_exist, str, tag,index=index

;
;  check quantity of input
;
if n_params() lt 2 then begin
   print,'Use:  status = tag_exist(structure, tag_name)
   return,0b
endif

;
;  check quality of input
;
if datatype(str,1) ne 'Structure' or datatype(tag,1) ne 'String' then begin
   print,'Use: status = tag_exist(str, tag)'
   print,'str = structure variable'
   print,'tag = string variable'
   return,0b
endif

i=-1
tn = tag_names(str)
nt = where(tn eq strupcase(tag)) & index=nt(0)
if nt(0) eq -1 then begin
   status = 0b
   for i=0,n_elements(tn)-1 do begin
     if datatype(str.(i),1) eq 'Structure' then status=tag_exist(str.(i),tag,index=index)
     if status eq 1b then return,status
   endfor
   return,0b
endif else begin
   return,1b
endelse
end
