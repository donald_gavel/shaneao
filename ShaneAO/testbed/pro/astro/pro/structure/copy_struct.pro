;+
; NAME:
;	COPY_STRUCT 
; PURPOSE:
; 	Copy all Fields with matching Tags names (except for "except_Tags")
;	from one structure array to another structure array (may be different).
;	Can also recursively copy from/to structures nested within structures.
; CATEGORY:
;			Structures
; CALLING EXAMPLES:
;		copy_struct, struct_From, struct_To
;		copy_struct, struct_From, struct_To, EXCEPT=["image","misc"]
;		copy_struct, struct_From, struct_To, /RECUR_FROM
; INPUTS:
;		struct_From = structure array to copy from.
;		struct_To = structure array to copy values to.
; KEYWORDS:
;		EXCEPT_TAGS = string array of Tag names to ignore (NOT to copy).
;
;		SELECT_TAGS = Tag names to copy (takes priority over EXCEPT).
;
;		/RECUR_FROM = option to search for Tag names
;				in sub-structures of struct_From,
;				and then call copy_struct recursively
;				for those nested structures.
;		/RECUR_TO = search for sub-structures of struct_To,
;				and then call copy_struct recursively
;				for those nested structures.
;		/RECUR_TANDEM = call copy_struct recursively
;				for the sub-structures with matching
;				Tag names in struct_From and struct_To
;		(for use when Tag names match but structure types differ).
; OUTPUTS:
;		NF_copied = incremented by total # of Fields copied (optional)
; INTERNAL:
;	Recur_Level = # of times copy_struct calls itself.
;		This argument is for internal recursive execution only.
;		The user call is 1, subsequent recursive calls increment it,
;		and the counter is decremented before returning.
;		The counter is used just to find out if argument checking
;		should be performed, and to set NF_copied = 0 first call.
; SIDE EFFECTS:
;		The arrays of values for tags in struct_From
;		are copied to tags with matching names in struct_To.
; EXTERNAL CALLS:
;		For option SELECT_TAGS calls pro match.
; PROCEDURE:
;		Match Tag names and then use corresponding Tag numbers.
; MODIFICATION HISTORY:
;	written 1989 Frank Varosi STX @ NASA/GSFC
; 	modif Jul.90 by F.V. added option to copy sub-structures RECURSIVELY.
;	modif Aug.90 by F.V. adjust # elements in TO (output) to equal
;			# elements in FROM (input) & count # of fields copied.
;	modif Jan.91 by F.V. added Recur_Level as internal argument so that
;			argument checking done just once, to avoid confusion.
;			Checked against Except_Tags in RECUR_FROM option.
;	modif Oct.91 by F.V. added option SELECT_TAGS= selected field names.
;	modif Aug.95 by W. Landsman to fix match of a single selected tag.
;-

pro copy_struct, struct_From, struct_To, NF_copied, Recur_Level,            $
						EXCEPT_TAGS  = except_Tags, $
						SELECT_TAGS  = select_Tags, $
						RECUR_From   = recur_From,  $
						RECUR_TO     = recur_To,    $
						RECUR_TANDEM = recur_tandem

	if N_elements( Recur_Level ) NE 1 then Recur_Level = 0

	Ntag_from = N_tags( struct_From )
	Ntag_to = N_tags( struct_To )

	if (Recur_Level EQ 0) then begin	;check only at first user call.

		NF_copied = 0

		if (Ntag_from LE 0) then begin
			message," 1st argument must be a structure",/INFO
			return
		   endif

		if (Ntag_to LE 0) then begin
			message," 2nd argument must be a structure",/INFO
			return
		   endif

		N_from = N_elements( struct_From )
		N_to = N_elements( struct_To )

		if (N_from GT N_to) then begin

			message," # elements (" + strtrim( N_to, 2 ) + $
					") in output TO structure",/INFO
			message," increased to (" + strtrim( N_from, 2 ) + $
					") as in FROM structure",/INFO
			struct_To = [ struct_To, $
					replicate( struct_To(0), N_from-N_to ) ]

		  endif	else if (N_from LT N_to) then begin

			message," # elements (" + strtrim( N_to, 2 ) + $
					") in output TO structure",/INFO
			message," decreased to (" + strtrim( N_from, 2 ) + $
					") as in FROM structure",/INFO
			struct_To = struct_To(0:N_from-1)
		   endif
	   endif

	Recur_Level = Recur_Level + 1		;go for it...

	Tags_from = Tag_names( struct_From )
	Tags_to = Tag_names( struct_To )
	wto = indgen( Ntag_to )

;Determine which Tags are selected or excluded from copying:

	Nseltag = N_elements( select_Tags )
	Nextag = N_elements( except_Tags )

	if (Nseltag GT 0) then begin

		match, Tags_to, [strupcase( select_Tags )], mt, ms,COUNT=Ntag_to

		if (Ntag_to LE 0) then begin
			message," selected tags not found",/INFO
			return
		   endif

		Tags_to = Tags_to(mt)
		wto = wto(mt)

	  endif else if (Nextag GT 0) then begin

		except_Tags = [strupcase( except_Tags )]

		for t=0,Nextag-1 do begin
			w = where( Tags_to NE except_Tags(t), Ntag_to )
			Tags_to = Tags_to(w)
			wto = wto(w)
		  endfor
	   endif

;Now find the matching Tags and copy them...

	for t = 0, Ntag_to-1 do begin

		wf = where( Tags_from EQ Tags_to(t) , nf )

		if (nf GT 0) then begin

			from = wf(0)
			to = wto(t)

			if keyword_set( recur_tandem ) AND		$
			   ( N_tags( struct_To.(to) ) GT 0 ) AND	$
			   ( N_tags( struct_From.(from) ) GT 0 ) then begin

				struct_tmp = struct_To.(to)

				copy_struct, struct_From.(from), struct_tmp,  $
						NF_copied, Recur_Level,       $
						EXCEPT=except_Tags,           $
						SELECT=select_Tags,           $
						/RECUR_TANDEM,                $
						RECUR_FROM = recur_From,      $
						RECUR_TO   = recur_To

				struct_To.(to) = struct_tmp

			  endif else begin

				struct_To.(to) = struct_From.(from)
				NF_copied = NF_copied + 1
			   endelse
		  endif
	  endfor

;Handle request for recursion on FROM structure:

	if keyword_set( recur_From ) then begin

		wfrom = indgen( Ntag_from )

		if (Nextag GT 0) then begin

			for t=0,Nextag-1 do begin

			    w = where( Tags_from NE except_Tags(t), Ntag_from )
			    Tags_from = Tags_from(w)
			    wfrom = wfrom(w)
			  endfor
		   endif

		for t = 0, Ntag_from-1 do begin

		     from = wfrom(t)

		     if N_tags( struct_From.(from) ) GT 0 then begin

			copy_struct, struct_From.(from), struct_To,        $
						NF_copied, Recur_Level,    $
						EXCEPT=except_Tags,        $
						SELECT=select_Tags,        $
						/RECUR_FROM,               $
						RECUR_TO     = recur_To,   $
						RECUR_TANDEM = recur_tandem
			endif
		  endfor
	  endif

;Handle request for recursion on TO structure:

	if keyword_set( recur_To ) then begin

		for t = 0, Ntag_to-1 do begin

		   to = wto(t)

		   if N_tags( struct_To.(to) ) GT 0 then begin

			struct_tmp = struct_To.(to)

			copy_struct, struct_From, struct_tmp,              $
						NF_copied, Recur_Level,    $
						EXCEPT=except_Tags,        $
						SELECT=select_Tags,        $
						/RECUR_TO,                 $
						RECUR_FROM = recur_From,   $
						RECUR_TANDEM = recur_tandem
			struct_To.(to) = struct_tmp
		     endif
		  endfor
	  endif

	Recur_Level = Recur_Level - 1
return
end
