	PRO FXTPIO_READ,UNIT,NAME,KEYWORD
;+
; Project     : SOHO - CDS
;
; Name        : 
;	FXTPIO_READ
; Purpose     : 
;	Copies FITS files from tape to disk -- internal routine.
; Explanation : 
;	Procedure to copy a FITS file from a tape on the specified tape unit to
;	the disk file <name>.FITS.  Data is left in FITS format, and not
;	converted to SDAS.  *For Unix or VMS only*
;
;	The procedure FXTAPEREAD is normally used to read a FITS tape.
;	FXTPIO_READ is a procedure call internal to FXTAPEREAD.
;
; Use         : 
;	FXTPIO_READ,UNIT,NAME
;	FXTPIO_READ,UNIT,NAME,KEYWORD
; Inputs      : 
;	UNIT	= Tape unit number.
;	NAME	= File name (without an extension).
; Opt. Inputs : 
;	KEYWORD	= If supplied and not equal to the null string then the file
;		  name will be taken from the value of the header keyword
;		  specified.
; Outputs     : 
;	NAME	= Name of file if input keyword parameter is supplied.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	None.
; Calls       : 
;	REMCHAR, FITSTAPE, FXPAR
; Common      : 
;	None.
; Restrictions: 
;	This routine is currently only supported under VMS or Unix IDL
;	Unix users must have the procedures TAPRD and SKIPF (called by 
;	FITSTAPE) which emulate the VMS-only intrinsic commands
; Side effects: 
;	The FITS file is copied to a disk file called <name>.FITS.
;
;	The FITS file is copied over record by record with no conversion,
;	until the end-of-file marker is reached.  No testing is done of the
;	validity of the FITS file.
;
;	Images are NOT converted using BSCALE and BZERO factors in the header.
;
; Category    : 
;	Data Handling, I/O, FITS, Generic.
; Prev. Hist. : 
;	William Thompson, March 1992, from FITSREAD by D. Lindler, M. Greason,
;				      and W. Landsman.
;
;	W. Thompson, May 1992, changed open statement to force 2880 byte fixed
;			       length records (VMS).  The software here does not
;			       depend on this file configuration, but other
;			       FITS readers might.
;
;	William Thompson, Jan. 1993, renamed to be compatible with DOS file
;		naming limitations.
; Written     : 
;	William Thompson, GSFC, March 1992.
; Modified    : 
;	Version 1, William Thompson, GSFC, 12 April 1993.
;		Incorporated into CDS library.
; Version     : 
;	Version 1, 12 April 1993.
;-
;
	ON_ERROR,2
;
	IF N_PARAMS() LT 3 THEN KEYWORD=''
;
;  Read FITS header.
;
	HEADER=STRARR(100)			;FITS header array
	NHEAD=0
	NH = 100				;Number of lines in header
	REC=ASSOC(7,BYTARR(2880))		;Define record type
	X=BYTARR(2880)				;FITS records are 2880 bytes
	FOR II=1,100 DO BEGIN
		STATUS=FITSTAPE('read',UNIT,8,X)
		IF STATUS EQ -4 THEN BEGIN
			PRINT,'EOF while reading fits header'
			PRINT,'processing terminated'
			RETALL
		ENDIF
		IF STATUS LT 0 THEN RETURN
		FOR I=0,35 DO BEGIN		;Process 36 header lines
			H=X((I*80):(I*80+79))	;Extract next line
			HEADER(NHEAD)=STRING(H)	;Add to header
			NHEAD=NHEAD+1
			IF NHEAD EQ NH THEN BEGIN
				HEADER = [HEADER,STRARR(100)]
				NH = NH + 100
			ENDIF
;
;  Check for end of header.
;
			IF STRING(H(0:7)) EQ 'END     ' THEN GOTO,L1
		ENDFOR ; I LOOP
	ENDFOR ; II LOOP
;
L1:
	HEADER = HEADER(0:NHEAD-1)
	NREC = FIX((NHEAD+35)/36)	;Number of 2880 byte records
;
;  Determine file name.
;
	IF KEYWORD NE '' THEN BEGIN
		NAME=FXPAR(HEADER,KEYWORD)
		IF !ERR LT 0 THEN BEGIN
			PRINT,'Keyword ',KEYWORD,' not in header'
			PRINT,'No file created'
			RETURN
		ENDIF
	ENDIF
	REMCHAR,NAME,' '		;Remove all blanks
;
;  Get the UNIT number, and open the file.
;
       	GET_LUN, LUN      
       	OPENW, LUN, NAME+'.fits', 2880, /BLOCK
;
;  Convert the header to byte and force into 80 character lines.
;
	BHDR = REPLICATE(32B, 80, 36*NREC)
	FOR N = 0,NHEAD-1 DO BHDR(0,N) = BYTE( STRMID(HEADER(N),0,80) )
	WRITEU, LUN, BHDR
;
;  Read and write the rest of the FITS file, until the EOF is reached.
;
NEXT_REC:
	STATUS = FITSTAPE('read',UNIT,8,X)
	IF STATUS LT 0 THEN BEGIN
		IF STATUS NE -4 THEN MESSAGE,'Unexpected error',/CONTINUE
		GOTO, DONE
	ENDIF
	WRITEU, LUN, X
	GOTO, NEXT_REC
;
;  Close the output file, and return.
;
DONE:
	FREE_LUN,LUN
	RETURN
	END
