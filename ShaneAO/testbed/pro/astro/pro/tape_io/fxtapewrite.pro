	PRO FXTAPEWRITE		;FITS tape writer
;+
; Project     : SOHO - CDS
;
; Name        : 
;	FXTAPEWRITE
; Purpose     : 
;	Interactive procedure to copy disk FITS files to tape.
; Explanation : 
;	Asks the user a series of questions to walk him or her through copying
;	a number of FITS files from disk to tape.
; Use         : 
;	FXTAPEWRITE
; Inputs      : 
;	User will be prompted for the following:
;		(1)  Tape unit number.
;		(2)  Blocking factor (1-10) = # of 2880 byte records per block.
;		(3)  Name of a FITS keyword to put file names into.  This will
;		     simplify subsequent reading of the FITS tape, since
;		     individual filenames will not have to be specified.  If
;		     you don't want to put the file names into the FITS header,
;		     then just hit [RETURN].
;		(4)  File names - these may either be specified individually,
;		     or a tapename may be specified, and all files in the form
;		     "tapename<number>.FITS" will be written to tape.
; Opt. Inputs : 
;	None.
; Outputs     : 
;	None.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	None.
; Calls       : 
;	FITSTAPE, GETFILES, FXTPIO_WRITE
; Common      : 
;	None.
; Restrictions: 
;	Currently only supported under Unix or VMS IDL
;	Unix users must have access to the procedures TAPRD.PRO and SKIPF.PRO
;	which emulate the VMS-only intrinisc IDL commands
; Side effects: 
;	Tape is not rewound before files are written.  Tape should be
;	positioned with REWIND or SKIPF before calling FXTAPEWRITE.  If you
;	want to append new FITS files to a tape, then call TINIT (tape init) to
;	position tape between final double EOF.
; Category    : 
;	Data Handling, I/O, FITS, Generic.
; Prev. Hist. : 
;	William Thompson, March 1992, from FITSWRT by D. Lindler.
;	William Thompson, May 1992, removed call to TINIT.
;	William Thompson, Jan. 1993, changed for renamed FXTPIO_WRITE.
; Written     : 
;	William Thompson, GSFC, March 1992.
; Modified    : 
;	Version 1, William Thompson, GSFC, 12 April 1993.
;		Incorporated into CDS library.
; Version     : 
;	Version 1, 12 April 1993.
;-
;
	ON_ERROR, 2
;
;  Get the tape unit number.
;
	UNIT = 0
	READ,'Enter output tape unit number: ',UNIT
;
;  Get tape block size.
;
	NB = 0
	READ,'Enter blocking factor for tape records (<10): ',NB
	IF (NB LT 1) OR (NB GT 10) THEN MESSAGE,'Blocking factor must be 1-10'
;
;  Initialize tape buffers.
;
	STATUS = FITSTAPE('init',UNIT,NB)
;
;  Get keyword for file names.
;
	SELECT = ''
	KEYWORD = ''
	PRINT,'Do you want to put filename into FITS header? (Y/N)'
	READ,SELECT
	IF STRUPCASE(STRMID(SELECT,0,1)) EQ 'Y' THEN $
		READ,'Enter header keyword in which to put file name: ',KEYWORD
;
;  Get file names -- menu.
;
MENU:
	SELECT = 0
	PRINT,'How do you want to select files to be written to tape?'
	PRINT,'Choose one of the following: '
	PRINT,'(1)  Specify tape name- Files are in form tapename<number>.FITS'
	PRINT,'(2)  Specify file names individually'
	READ,SELECT
	TAPENAME=''
;
	CASE SELECT OF
		1:  BEGIN
			PRINT,'Files are in form tapename<number>.FITS'
			READ,'Enter tape name. ',TAPENAME
			IF TAPENAME NE '' THEN BEGIN
				GETFILES,LIST
				NFILES=N_ELEMENTS(LIST)
			ENDIF
			END
		2:  BEGIN
			PRINT,'Enter file names (with extension), one per line.'
			PRINT,'Enter blank line to quit'
			ST=''
			FIRST = 1
			REPEAT BEGIN
				READ,ST
				IF ST NE '' THEN IF FIRST THEN	$
					FILEBUF = ST	  ELSE	$
					FILEBUF = [FILEBUF,ST]
				FIRST = 0
			ENDREP UNTIL ST EQ ''
			NFILES=N_ELEMENTS(FILEBUF)
			END
		ELSE:  BEGIN
			PRINT,'ERROR- Invalid choice.'
			GOTO,MENU
			END
	ENDCASE
;
;  Loop on files.
;
	FOR I=0,NFILES-1 DO BEGIN
;
;  Get filename.
;
		IF TAPENAME EQ '' THEN BEGIN
			FNAME=STRTRIM(FILEBUF(I),2)
		END ELSE BEGIN
			FNAME=TAPENAME+STRTRIM(LIST(I),2)+'.fits'
		ENDELSE
;
;  Write file to tape.
;
		FXTPIO_WRITE,UNIT,FNAME,KEYWORD
	ENDFOR
;
	RETURN
	END
