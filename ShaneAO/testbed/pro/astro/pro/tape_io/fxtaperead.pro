	PRO FXTAPEREAD,UNIT,LIST,KEYWORD,TAPENAME,FNAMES    ;FITS_file_reader
;+
; Project     : SOHO - CDS
;
; Name        : 
;	FXTAPEREAD
; Purpose     : 
;	Interactive procedure copies FITS files from tape to disk.
; Explanation : 
;	Interactive procedure to copy FITS files from tape onto disk.  Data is
;	left in FITS format, and not converted to SDAS.   Works under VMS or
;	Unix
; Use         : 
;	FXTAPEREAD                      ;Prompt for all parameters
;
;	FXTAPEREAD,UNIT,LIST,KEYWORD,TAPENAME,FNAMES
;
;	FXTAPEREAD,1,INDGEN(5)+1,'IMAGE'
;			;Read the first 5 files on unit 1.  The filenames are
;			;taken from the IMAGE keyword.
;
;	FXTAPEREAD,1,[2,4],'','',['GALAXY','STAR']
;			;Read files 2 and 4 on unit 1.  Create files named
;			;GALAXY and STAR.
;
;	FXTAPEREAD,1,[2,4]
;			;Read files 2 and 4, and prompt for filenames.
;
; Inputs      : 
;	None required.
; Opt. Inputs : 
;	Interactive users will normally just type FXTAPEREAD and be prompted
;	for all parameters.  However, the following parameters can be passed
;	directly to FXTAPEREAD
;
;	UNIT	 = Tape unit number (scalar).
;	LIST	 = Vector containing list of file numbers to read.
;	KEYWORD	 = Scalar string giving a FITS keyword which will be extracted
;		   from the headers on tape and used for file names.  Set
;		   KEYWORD to the null string '', if such a keyword is not to
;		   be used.
;	TAPENAME = Scalar string giving a name for the tape.  Filenames will be
;		   constructed by concatenating TAPENAME with the file number.
;		   TAPENAME is used only if KEYWORD is passed as the null
;		   string ''.
;	FNAME	 = Vector string giving a file name for each file number given
;		   in LIST.  FNAME is used only if both KEYWORD = '' and
;		   TAPENAME = ''.  Spaces are trimmed from names in FNAME.
; Outputs     : 
;	None.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	None.
; Calls       : 
;	FITSTAPE, GETFILES, FXTPIO_READ
; Common      : 
;	None.
; Restrictions: 
;	None.
; Side effects: 
;	FXTAPEREAD will always rewind the tape before processing.
;
;	The FITS file is copied over record by record with no conversion,
;	until the end-of-file marker is reached.  No testing is done of the
;	validity of the FITS file.
;
;	Images are NOT converted using BSCALE and BZERO factors in the header.
;
;	For each tape file a FITS disk file will be created with the name
;	"<name>.FITS".
;
; Category    : 
;	Data Handling, I/O, FITS, Generic.
; Prev. Hist. : 
;	William Thompson, March 1992, from FITSRD by D. Lindler.
;	William Thompson, May 1992, fixed TPOS bug when reading multiple files.
;	William Thompson, Jan. 1993, changed for renamed FXTPIO_READ.
; Written     : 
;	William Thompson, GSFC, March 1992.
; Modified    : 
;	Version 1, William Thompson, GSFC, 12 April 1993.
;		Incorporated into CDS library.
; Version     : 
;	Version 1, 12 April 1993.
;-
;
	IF N_ELEMENTS(UNIT) NE 1 THEN BEGIN
		UNIT = 0
		READ,'Enter tape unit number: ',UNIT
	ENDIF
;
;  Get files to process.
;
	NFILES = N_ELEMENTS(LIST)
	IF NFILES EQ 0 THEN BEGIN
		GETFILES,LIST
		NFILES = N_ELEMENTS(LIST)
	ENDIF ELSE IF NFILES EQ 1 THEN LIST = INTARR(1) + LIST
;
;  Check parameters.
;
	IF N_ELEMENTS(KEYWORD) NE 1 THEN KEYWORD = '' ELSE GOTO,READER  
	IF N_ELEMENTS(TAPENAME) NE 1 THEN TAPENAME = '' ELSE GOTO,READER
	IF N_ELEMENTS(FNAME) NE 0 THEN GOTO,READER   
;
;  MENU: used only if no parameters for filename given.
;
MENU:
	PRINT,'How do you want to name files to be written to disk?'
	PRINT,'Choose one of the following: '
	PRINT,'(1)  Use specified FITS keyword.'
	PRINT,'(2)  Use tape name concatenated with file number.'
	PRINT,'(3)  Specify file names individually.'
	READ,SELECT
;
	CASE SELECT OF
		1:  BEGIN
			PRINT,'Specify FITS keyword to use for file names.'
			READ,KEYWORD
			END
		2:  BEGIN
			PRINT,'Specify tape name (no extension).  File will be created'
			PRINT,'from tape name and file number.'
			READ,TAPENAME
			END
		3:  BEGIN
			FNAMES = STRARR(NFILES)		;Read file names.
			PRINT,'Specify file names, one per line (no extension).'
			FOR I=0,NFILES-1 DO BEGIN
				ST = ''
				READ,'file '+STRING(LIST(I))+': ',ST
				FNAMES(I) = ST
			ENDFOR ; I
			END
		ELSE: BEGIN
			PRINT,'ERROR- Not a valid choice!'
			GOTO,MENU
			END
	ENDCASE
;
;  Process tape file by file.
;
READER:
;
	REWIND,UNIT		;Rewind tape.
	TPOS=1			;Present file position.
;
	FOR I=0,NFILES-1 DO BEGIN
		STATUS = FITSTAPE('init',UNIT,10)
		NSKIP = LIST(I)-TPOS	;NUMBER OF FILES TO SKIP
		IF NSKIP GT 0 THEN SKIPF,UNIT,NSKIP
		TPOS = TPOS+NSKIP
;
;  Determine file name if not from keyword.
;
		IF KEYWORD EQ '' THEN BEGIN
			IF TAPENAME EQ '' THEN NAME=STRTRIM(FNAMES(I),2) $
					  ELSE NAME=TAPENAME+STRTRIM(LIST(I),2)
		ENDIF
;
;  Read file.
;
		PRINT,'FILE ',LIST(I),'                      '
		FXTPIO_READ,UNIT,NAME,KEYWORD
		PRINT,'   ',NAME
		TPOS = TPOS + 1
	ENDFOR
;
	RETURN
	END
