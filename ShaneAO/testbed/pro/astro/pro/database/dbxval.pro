function dbxval,entry,idltype,nvalues,sbyte,nbytes
;+
; NAME:
;	DBXVAL
; PURPOSE:
;	procedure to quickly return a value of the specified item number
;	from the entry
;
; CALLING SEQUENCE:
;	result = dbxval( entry, idltype, nvalues, sbyte, nbytes )
;
; INPUTS:
;	entry - entry from data base (bytarr)
;	idltype - idl data type (obtained with db_item_info)
;	nvalues - number of values to return (obtained with db_item)
;	sbyte - starting byte in the entry (obtained with db_item)
;	nbytes - number of bytes (needed only for string type)
;			(obtained with db_item)
;
; OUTPUT:
;	function value is value of the specified item in entry
;
; OPERATIONAL NOTES:
;	To increase speed the routine assumes that entry and item are
;	valid and that the data base is already opened using dbopen.
;
; HISTORY:
;	version 2  D. Lindler Nov. 1987  (for new db format)
;-
;
;----------------------------------------------------------------
;
case idltype of 		;case of data type
  1: val = byte(entry,sbyte,nvalues)
  2: val = fix(entry,sbyte,nvalues)
  3: val = long(entry,sbyte,nvalues)
  4: val = float(entry,sbyte,nvalues)
  5: val = double(entry,sbyte,nvalues)
  7: val = string( entry(sbyte:sbyte+nbytes-1))
endcase
;
if ( nvalues EQ 1 ) then return,val(0) else return,val
end
