pro tab_to_db,table,database,new
;+
;  NAME:
;	TAB_TO_DB 
;  PURPOSE:
;	Add STSDAS table contents to an existing or new database file.
;
; CALLING SEQUENCE:
; 	tab_to_db, table, database, new
;
; INPUTS:
;	table - table name
;	database - database name
;
; OPTIONAL INPUTS:
;	new - integer flag, 0 - add to existing data base
;			    1 - create new data base
;		(default = 0)
;
; OPERATIONAL NOTES:
;	!PRIV must be 2 or greater to execute tab_to_db.
;	If a new data base is to be created, It will be created in
;	your current default directory and your current default directory
;	must be included in the logical definition of ZDBASE:
;	WARNING: If a table column ENTRY is present.  It will cause
;	the present data base entries (as specified by ENTRY values) to
;	be updated.
;
; HISTORY:
;	version 1  D. Lindler  Nov 1989
;	converted to IDL v 2, D. Neill  Mar 1991
;-
;-----------------------------------------------------------------------
;
; set default.
;
	if n_params(0) lt 3 then new = 0
;
; check !PRIV
;
	if !priv lt 2 then begin
		print,'TAB_TO_DB-- !priv must be 2 or greater'
		retall
	endif
;
; read table and get column information
;
	tab_read,table,tcb,tab
	tab_size,tcb,nrows,ncols
	dtype = intarr(ncols)			;column data types
	colname = strarr(ncols)			;column names
	twidth = intarr(ncols)			;column width (bytes)
	for i=0,ncols-1 do begin
		tab_col,tcb,i+1,offset,width,datatype,name
		colname(i) = name
		twidth(i) = width
		if datatype lt 0 then begin	;minus string length?
			twidth(i) = -datatype
			datatype = 2		;string code
		endif
		dtype(i) = datatype
	endfor
;
; if new database, create it -------------------------------------------------
;
;
; create .dbd file
;
	if new then begin
		get_lun,unit
		openw,unit,strtrim(database)+'.dbd'	;definition file
		printf,unit,'#title'
		printf,unit,'Created from table '+table
		printf,unit,'#maxentries'
		printf,unit,(nrows*10)>1000
		printf,unit,'#items'
	
		for i=0,ncols-1 do begin
		    case dtype(i) of
			1: st = 'L*1'
			2: st = 'CH*'+strtrim(twidth(i),2)
			4: st = 'I*4'
			6: st = 'R*4'
			7: st = 'R*8'
		    endcase
		    printf,unit,colname(i)+' '+st
		endfor
		printf,unit,'#formats'
		printf,unit,'#index'
		printf,unit,'#pointers'
		close,unit
		free_lun,unit
;
; create database from .dbd file
;
		dbcreate,database,1,1,ncols+1
	endif
;
; open data base file -------------------------------------------------------
;
	dbopen,database,1
	dbrd,0,entry				  ;sample entry
	entries = bytarr(n_elements(entry),nrows) ;space for all new entries
;
; find data base fields for given column names
;
	db_item,colname,itnums,ivalnum,idltype,sbyte,numvals,nbytes
;
; loop on columns
;
	for i=0,ncols-1 do begin
		val = tab_val(tcb,tab,strtrim(colname(i)))
;
; convert to correct type for the data base
;
		case idltype(i) of
			1: begin			;string
				values = strarr(nrows)
				for j=0,nrows-1 do values(j)=val(j)
				val = byte(values)
			   end
			2: val = byte(val)
			4: val = fix(val)
			8: val = float(val)
			16: val = long(val)
			32: val = double(val)
		endcase
;
; convert to bytes and insert into entries array
;
		if idltype(i) ne 1 then val=byte(val,0,nbytes(i),nrows)
		entries(sbyte(i),0) = val
	endfor
;
; write the new entries to the database
;
	for i=0,nrows-1 do dbwrt,entries(*,i)
	dbclose
return
end
