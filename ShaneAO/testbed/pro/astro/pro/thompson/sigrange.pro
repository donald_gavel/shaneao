	FUNCTION SIGRANGE,ARRAY,FRACTION=FRACTION,MISSING=MISSING
;+
; Project     : SOHO - CDS
;
; Name        : 
;	SIGRANGE()
; Purpose     : 
;	Selects the most significant data range in an image.
; Explanation : 
;	Selects out the most significant range in the data to be used in 
;	displaying images.  The histogram of ARRAY is used to select the most
;	significant range.
; Use         : 
;	OUTPUT = SIGRANGE( ARRAY )
; Inputs      : 
;	ARRAY	 = Array to take most significant range of.
; Opt. Inputs : 
;	None.
; Outputs     : 
;	The function returns an array where values above and below the
;	selected range are set equal to the maximum and minimum of the
;	range respectively.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	FRACTION = Fraction of data to consider most significant.
;		   Defaults to 0.9
;	MISSING	 = Value used to flag missing points.  Data points with this
;		   value are not considered or changed.
; Calls       : 
;	GET_IM_KEYWORD
; Common      : 
;	None.
; Restrictions: 
;	ARRAY must have more than two points.  Fraction must be greater than 0 
;	and less than 1.
;
;	In general, the SERTS image display routines use several non-standard
;	system variables.  These system variables are defined in the procedure
;	IMAGELIB.  It is suggested that the command IMAGELIB be placed in the
;	user's IDL_STARTUP file.
;
;	Some routines also require the SERTS graphics devices software,
;	generally found in a parallel directory at the site where this software
;	was obtained.  Those routines have their own special system variables.
;
; Side effects: 
;	None.
; Category    : 
;	None.
; Prev. Hist. : 
;	W.T.T., Oct. 1987.
;	W.T.T., Jan. 1991.  Changed FRACTION to keyword, and added keyword
;			    BADPIXEL.
;	W.T.T., Mar. 1992.  Rewrote to apply histograms to only a fraction of
;			    the array to speed up the process.
;	William Thompson, August 1992, renamed BADPIXEL to MISSING.
; Written     : 
;	William Thompson, GSFC, October 1987.
; Modified    : 
;	Version 1, William Thompson, GSFC, 12 May 1993.
;		Incorporated into CDS library.
;	Version 2, William Thompson, GSFC, 25 May 1993.
;		Changed call to HISTOGRAM to be compatible with OpenVMS/ALPHA
; Version     : 
;	Version 2, 25 May 1993.
;-
;
	GET_IM_KEYWORD, MISSING, !IMAGE.MISSING
;
	IF N_ELEMENTS(FRACTION) NE 1 THEN FRACTION = 0.9
	IF N_ELEMENTS(ARRAY) LE 2 THEN BEGIN
		PRINT,'*** Not enough points to form histogram, routine SIGRANGE.'
		RETURN,ARRAY
	END ELSE IF (FRACTION LE 0) OR (FRACTION GE 1) THEN BEGIN
		PRINT,'*** Fraction must be GT 0 and LT 1, routine SIGRANGE.'
		RETURN,ARRAY
	ENDIF
;
;  To speed up the process, work on a reduced version of ARRAY.
;
	ATEMP0 = ARRAY(25*LINDGEN(N_ELEMENTS(ARRAY)/25))
;
;  Get the total range of the data, excluding any missing points.
;
	BANG_C = !C
	IF N_ELEMENTS(MISSING) EQ 1 THEN BEGIN
		WW = WHERE(ATEMP0 EQ MISSING,N_MISSING)
		N_TOTAL = N_ELEMENTS(ATEMP0) - N_MISSING
		AMAX = 1.*MAX(ATEMP0(WHERE(ATEMP0 NE MISSING)))
		AMIN = 1.*MIN(ATEMP0(WHERE(ATEMP0 NE MISSING)))
	END ELSE BEGIN
		N_MISSING = 0
		N_TOTAL = N_ELEMENTS(ATEMP0)
		AMAX = 1.*MAX(ATEMP0)
		AMIN = 1.*MIN(ATEMP0)
	ENDELSE
	ATEMP = ATEMP0
	IF AMIN EQ AMAX THEN GOTO,EXIT_POINT
	DELTA = 0
;
;  Form the histogram, and calculate an array expressing the fraction of points
;  that fall within or below the given bin.
;
FIND_RANGE:
	LAST_DELTA = DELTA
	X = AMIN  +  FINDGEN(1001) * (AMAX - AMIN) / 1000.
	H = HISTOGRAM(LONG((ATEMP-AMIN)*1000./(AMAX - AMIN)))
	FOR I = 1,N_ELEMENTS(H)-1 DO H(I) = H(I) + H(I-1)
	H = H / FLOAT(N_TOTAL)
;
;  Estimate the endpoints corresponding to the specified range, and calculate
;  the values at these endpoints.  Limit the array to be within these values.
;
	IMIN = (MIN( WHERE( H GT ((1. - FRACTION) / 2.) )) - 1) > 0
	IMAX =  MIN( WHERE( H GT ((1. + FRACTION) / 2.) ))
	AMIN = X(IMIN)
	AMAX = X(IMAX)
	ATEMP = AMIN > ATEMP0 < AMAX
;
;  If the range calculated has changed by more than 5% from the last iteration,
;  the reiterate.
;
	DELTA = AMAX - AMIN
	RATIO = (DELTA - LAST_DELTA) / (DELTA + LAST_DELTA)
	IF ABS(RATIO) GT 0.05 THEN GOTO,FIND_RANGE
;
;  If a missing pixel flag value was passed, then reset those points to the
;  flag value.  Return the adjusted array.
;
EXIT_POINT:
	!C = BANG_C
	ATEMP = AMIN > ARRAY < AMAX
	IF N_ELEMENTS(MISSING) EQ 1 THEN BEGIN
		WW = WHERE(ARRAY EQ MISSING,N_MISSING)
		IF N_MISSING GT 0 THEN ATEMP(WW) = MISSING
	ENDIF
	RETURN,ATEMP
	END
