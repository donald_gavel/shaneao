	PRO PSPLOT,FILENAME,DELETE=DELETE,QUEUE=QUEUE,COLOR=COLOR,	$
		COMMAND=COMMAND
;+
; Project     : SOHO - CDS
;
; Name        : 
;	PSPLOT
; Purpose     : 
;	Prints PostScript plots and resets to the previous device.
; Explanation : 
;	Send a PostScript plot file generated by IDL to the PostScript laser
;	printer.  The default queue is defined by the logical name/environment
;	variable PSLASER.
;
;	If PSPLOT is being used to close the PostScript file as well as print
;	it, and the queue name is not passed explicitly, then it will
;	automatically select a color printer (PSCOLOR) if the file was opened by
;	the PS procedure with /COLOR set, and a regular PostScript printer
;	(PSLASER) otherwise.  However, if the file was previously closed with
;	PSCLOSE or PSPLOT, then either the /COLOR or QUEUE keyword will need to
;	be used to direct a color PostScript file to the appropriate printer.
;
; Use         : 
;	PSPLOT  [, FILE ]  [, /DELETE ]
;
;	PS				;Open PostScript plot file
;	   ... plotting commands ...	;Create plot
;	PSPLOT				;Close & plot file, reset to prev. dev.
;	   or
;	PSCLOSE				;Close w/o printing,  "    "   "    "
;
; Inputs      : 
;	None required.
; Opt. Inputs : 
;	The default filename is either taken from the last call to the PS
;	routine, or is "idl.ps".
;
;	A filename other than the default can be passed in one of three ways:
;
;		Explicitly:		e.g. PSPLOT,'graph.ps'
;		By number (VMS)		e.g. PSPLOT,3   for "idl.ps;3"
;		All versions (VMS)	e.g. PSPLOT,'*' for "idl.ps;*"
;		All ".ps" files (UNIX)	e.g. PSPLOT,'*' for "*.ps"
; Outputs     : 
;	A message is printed to the screen.
; Opt. Outputs: 
;	None.
; Keywords    : 
;	DELETE	= If set, then file is deleted after printing.
;	QUEUE	= Name of printer queue to be used in printing the file.  If
;		  not passed, then the environment variable PSLASER (or
;		  PSCOLOR) is checked for the name of the print queue.
;	COLOR	= If set, then the environment variable PSCOLOR is checked for
;		  the name of the print queue rather then PSLASER.  Ignored if
;		  QUEUE is passed.
;	COMMAND	= (Unix only.)  Command to be used to send the plot file to the
;		  printer.  If not passed, then the environment variable
;		  PRINTCOM is checked.  If neither of these is set, then the
;		  standard command "lpr" is used.
; Calls       : 
;	SETPLOT
; Common      : 
;	PS_FILE which contains PS_FILENAME, the name of the plotting file,
;	LAST_DEVICE, which is the name of the previous graphics device, and
;	various parameters used to keep track of which configuration is being
;	used.
; Restrictions: 
;	The requested plot file must exist.
;
;	In general, the SERTS graphics devices routines use the special system
;	variables !BCOLOR and !ASPECT.  These system variables are defined in
;	the procedure DEVICELIB.  It is suggested that the command DEVICELIB be
;	placed in the user's IDL_STARTUP file.
;
; Side effects: 
;	The plot file is queued on the printer.  Also, any files "idl.ps" that
;	may be open will be closed.  The previous plotting device is reset.
; Category    : 
;	Utilities, Devices.
; Prev. Hist. : 
;	William Thompson, November 1989.
;	W.T.T., added check for LAST_DEVICE, August 1990.
;	W.T.T., changed DELETE to keyword, February, 1991.
;	W.T.T., Feb. 1991, modified to reflect change in common block PS_FILE.
;	W.T.T., May 1991, extended environment variable PSLASER to UNIX.
;	W.T.T., Jun 1992, added check for PSCOLOR.
;	W.T.T., Jul 1992, added check for PS_FILENAME in common block.
;	W.T.T., Sep 1992, added COLOR keyword.
; Written     : 
;	William Thompson, GSFC, November 1989.
; Modified    : 
;	Version 1, William Thompson, GSFC, 27 April 1993.
;		Incorporated into CDS library.
;	Version 2, William Thompson, GSFC, 2 January 1994.
;		Added save into common plot of current configuration.
;       Version 2.1, S.V. Haugan, ItA/UiO
;		Added support for alternative UNIX print commands via $PRINTCOM
;	Version 3, William Thompson, GSFC, 8 June 1994
;		Added keyword COMMAND
; Version     : 
;	Version 3, 8 June 1994.
;-
;
	ON_ERROR,2
	COMMON PS_FILE, PS_FILENAME, LAST_DEVICE, CONFIGS, CUR_CONFIG,  $
		SAVE_CONFIG
;
	SWITCH = ""
	STRING_TYPE = 7
;
;  No parameters passed:  assume either the name from the last time PS was
;  called, or "idl.ps".
;
	IF N_PARAMS(0) EQ 0 THEN BEGIN
		IF N_ELEMENTS(PS_FILENAME) EQ 1 THEN BEGIN
			IF PS_FILENAME EQ "" THEN FILENAME = "idl.ps"	$
				ELSE FILENAME = PS_FILENAME
		END ELSE FILENAME = "idl.ps"
	ENDIF
;
;  Test and interpret FILENAME.
;
	S = SIZE(FILENAME)
	IF S(0) NE 0 THEN BEGIN
		PRINT,'*** Variable must not be an array, ' +	$
			'name= FILE, routine PSPLOT.'
		RETURN
;
;  If of type string, then must be the filename.  If the string "*", then the
;  meaning depends on which operating system is being used.
;
	END ELSE IF S(1) EQ STRING_TYPE THEN BEGIN
		IF FILENAME EQ "*" THEN BEGIN
			IF !VERSION.OS EQ "vms" THEN BEGIN
				FILENAME = "idl.ps;*"
			END ELSE BEGIN
				FILENAME = "*.ps"
			ENDELSE
		ENDIF
;
;  If numerical, then either it is the file version number (VMS) or it is
;  simply incorrect (UNIX).
;
	END ELSE IF !VERSION.OS EQ "vms" THEN BEGIN
		FILENAME = "idl.ps;" + TRIM(FILENAME)
	END ELSE BEGIN
		PRINT,'*** Variable must be of type string, ' +		$
			'name= FILENAME, routine PSPLOT.'
		RETURN
	ENDELSE
;
;  If passed, then check the value of QUEUE.
;
	IF N_ELEMENTS(QUEUE) NE 0 THEN BEGIN
		SQ = SIZE(QUEUE)
		IF S(0) NE 0 THEN BEGIN
			PRINT,'*** Variable must not be an array, ' +	$
				'name= QUEUE, routine PSPLOT.'
			RETURN
		END ELSE IF S(1) NE STRING_TYPE THEN BEGIN
			PRINT,'*** Variable must be of type string, ' +	$
				'name= QUEUE, routine PSPLOT.'
			RETURN
		ENDIF
;
;  Otherwise, check the logical name/environment variable PSLASER to get the
;  name of the queue.  A queue name is required in VMS.
;
	END ELSE BEGIN
		IF N_ELEMENTS(CUR_CONFIG) EQ 0 THEN CUR_CONFIG = ""
		IF (CUR_CONFIG EQ "LCOLOR") OR (CUR_CONFIG EQ "PCOLOR") OR $
			KEYWORD_SET(COLOR) THEN PRINTER = "PSCOLOR" ELSE   $
			PRINTER = "PSLASER"
		PSLASER = GETENV(PRINTER)
		IF PSLASER NE "" THEN BEGIN
			QUEUE = PSLASER
		END ELSE IF !VERSION.OS EQ "vms" THEN BEGIN
			MESSAGE,'Logical name ' + PRINTER +		$
				' or keyword QUEUE must be defined'
		ENDIF
	ENDELSE
;
;  Close any PostScript files.
;
	DEVICE = !D.NAME
	IF N_ELEMENTS(LAST_DEVICE) EQ 0 THEN LAST_DEVICE = !D.NAME
	IF !D.NAME NE 'PS' THEN SETPLOT,'PS'
	DEVICE,/CLOSE_FILE
	PS_FILENAME = ""
;
;  Form the print command according to the operating system.
;
	IF !VERSION.OS EQ "vms" THEN BEGIN
		PSLASER = TRIM(STRUPCASE(QUEUE))
		COM_LINE = "PRINT /NOTIFY /QUEUE=" + PSLASER + " "
		IF PSLASER EQ "SOLAR$TALARIS" THEN	$
			COM_LINE = COM_LINE + "/SETUP=PSI "
	END ELSE BEGIN
;
;  In UNIX, use the switch "-P" to control which queue is used.  Otherwise, the
;  default print queue is used.  The printing command can be set by the keyword
;  COMMAND, by the environment variable PRINTCOM, or the standard command "lpr"
;  can be used.
;
		IF N_ELEMENTS(COMMAND) EQ 1 THEN PRINTCOM = COMMAND ELSE $
			PRINTCOM = GETENV("PRINTCOM")
		IF PRINTCOM EQ "" THEN PRINTCOM = "lpr"
		COM_LINE = PRINTCOM + " "
		IF N_ELEMENTS(QUEUE) NE 0 THEN	$
			COM_LINE = COM_LINE + "-P" + QUEUE + " "
	ENDELSE
;
;  Test to see if the DELETE keyword was set.
;
	IF KEYWORD_SET(DELETE) THEN IF !VERSION.OS EQ "vms" THEN	$
		COM_LINE = COM_LINE + "/DELETE "	    ELSE	$
		COM_LINE = COM_LINE + "-r "
	COM_LINE = COM_LINE + FILENAME
	PRINT,"$ " + COM_LINE
	SPAWN,COM_LINE
;
;  Save the parameters for the current configuration.
;
	IF CUR_CONFIG NE '' THEN BEGIN
		NCONFIG = WHERE(CONFIGS EQ CUR_CONFIG)
		SAVE_CONFIG(NCONFIG).POSITION = !P.POSITION
		SAVE_CONFIG(NCONFIG).XMARGIN = !X.MARGIN
		SAVE_CONFIG(NCONFIG).XWINDOW = !X.WINDOW
		SAVE_CONFIG(NCONFIG).YMARGIN = !Y.MARGIN
		SAVE_CONFIG(NCONFIG).YWINDOW = !Y.WINDOW
		SAVE_CONFIG(NCONFIG).ZMARGIN = !Z.MARGIN
		SAVE_CONFIG(NCONFIG).ZWINDOW = !Z.WINDOW
		SAVE_CONFIG(NCONFIG).THICK   = !P.THICK
		SAVE_CONFIG(NCONFIG).CHARTHICK = !P.CHARTHICK
		SAVE_CONFIG(NCONFIG).XTHICK  = !X.THICK
		SAVE_CONFIG(NCONFIG).YTHICK  = !Y.THICK
	ENDIF
;
;  Reset the plotting device.
;
	IF DEVICE NE 'PS' THEN SETPLOT,DEVICE ELSE SETPLOT,LAST_DEVICE
	PRINT,'The plotting device is now set to ' + TRIM(LAST_DEVICE) + '.'
;
	RETURN
	END
