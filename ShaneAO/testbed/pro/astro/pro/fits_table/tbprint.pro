pro tbprint,h,tab,columns,rows,textout=textout
;+
; NAME:
;	TBPRINT
;  PURPOSE:
;	Procedure to print specified columns and rows of a FITS table
;
; CALLING SEQUENCE:
;	tbprint, h, tab, columns, [ rows, TEXTOUT = ]
;
; INPUTS:
;	h - Fits header for table, string array
;	tab - table array 
;	columns - string giving column names, or vector giving
;		column numbers (beginning with 1).  If string 
;		supplied then column names should be separated by comma's.
;	rows - (optional) vector of row numbers to print.  If
;		not supplied or set to scalar, -1, then all rows
;		are printed.
;
; OUTPUTS:
;	None
; KEYWORDS:
;       TEXTOUT controls the output device; see the procedure TEXTOPEN
;
; SYSTEM VARIABLES:
;	Uses nonstandard system variables !TEXTOUT and !TEXTOPEN
;	Set !TEXTOUT = 3 to direct output to a disk file.   The system
;	variable is overriden by the value of the keyword TEXTOUT
;
; EXAMPLES:
;       tbprint,h,tab,'STAR ID,RA,DEC'    ;print id,ra,dec for all stars
;       tbprint,h,tab,[2,3,4],indgen(100) ;print columns 2-4 for 
;                                          first 100 stars
;       tbprint,h,tab,text="STARS.DAT"    ;Convert entire FITS table to
;                                         ;an ASCII file named STARS.DAT
;
; PROCEDURES USED:
;	TEXTOPEN,TEXTCLOSE,TBSIZE,TBINFO
;
; RESTRICTIONS: 
;	(1) Program does not check whether output length exceeds output
;		device capacity (e.g. 80 or 132).
;	(2) Column heading may be truncated to fit in space defined by
;		the FORMAT specified for the column
;	(3) Program does not check for null values
;
; HISTORY:
;	version 1  D. Lindler Feb. 1987
;-
 On_error,2

; set defaulted parameters

 common tb_common,tbcol,width,idltype,numval,tunit,tnull,tform,ttype,maxval
 npar = N_params()
 if npar lt 2 then begin
   print,'Syntax -  tbprint, h, tab, [ columns, rows, device ]
   return
 endif

 if npar lt 3 then columns = -1
 if npar lt 4 then rows= -1
 if (not keyword_set(textout)) then textout = 1

; make sure rows is a vector

 tbsize, h, tab, ncols, nrows, tfields, allcols, allrows   ;table size
 r = long(rows)
 if r(0) eq -1 then r = lindgen(nrows)		;default
 n = N_elements(r)
;
if !err lt 0 then $
	message,'Invalid FITS header (first parameter) for table file'
tbinfo,h,tbcol,width,idltype,numval,tunit,tnull,tform,ttype
tfields = N_elements(ttype)

; if columns is a string, change it to string array

 s = size(columns) & ndim = s(0) & dtype = s(ndim+1)

 if dtype eq 7 then begin
	colnames = strarr(tfields)		;string array to hold names
	numcol = 0			;number of columns
	st = columns			;don't want to change columns var.
	while st ne '' do begin
		colnames(numcol) = gettok(st,',')
		numcol = numcol+1
	endwhile
        colnum = intarr(numcol)
        for i = 0,numcol-1 do begin 
        colnum(i) = where(ttype eq strupcase(field),nfound)
        if nfound eq 0 then $ 
           message,'Field '+strupcase(field) + ' not found in header'
       end
   end else begin			;user supplied vector
	colnum = fix(columns)		;make sure it is integer
	numcol = N_elements(colnum)	;number of elements
        if colnum(0) eq -1 then begin 
              colnum = indgen(tfields) + 1 & numcol = tfields
        endif 
 end

 output = strarr(n,numcol)
 for i = 0,numcol-1 do begin
        output(0,i) = string(tbget(h,tab,colnum(i),r,/CON))
 endfor

; done

 textopen,'TBPRINT', TEXTOUT = textout
 printf,!TEXTUNIT,transpose(output)
 textclose, TEXTOUT = textout

 return
 end
