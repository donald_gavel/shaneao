pro tbhelp,h
;+
; NAME:
;	TBHELP
; PURPOSE:
;	Routine to print a description of a disk 3D binary FITS table
;
; CALLING SEQUENCE:
;	tbhelp, h
;
; INPUTS:
;	h - FITS header for a binary table, string array
;
; METHOD:
;	FITS 3D Table keywords NAXIS*,EXTNAME,TFIELDS,TTYPE*,TFORM*,TUNIT*,
;	are read from the header and displayed at the terminal
;
;	A FITS header is recognized as bein for a binary table if the keyword 
;	XTENSION has the value 'BINTABLE' or 'A3DTABLE'
;
; NOTES:
;	Certain fields may be truncated in the display
;
; HISTORY:
;	W. Landsman       February, 1991
;	Parsing of a FITS binary header made more robust    May, 1992
;-
 On_error,2

 if N_params() LT 1 then begin
     print,'Syntax - tbhelp, hdr     
     return
 endif

 zparcheck, 'TBHELP', h, 1, 7, 1, 'Table Header'

 naxis = sxpar( h, 'NAXIS*')
 if N_elements(naxis) LT 2 then $
         message,'ERROR - FITS Binary table must have NAXIS = 2'

 ext_type = strmid( strtrim( sxpar( h, 'XTENSION'), 2 ), 0, 8)
 if (ext_type NE 'A3DTABLE') and (ext_type NE 'BINTABLE') then message, $
 'WARNING - Header type of ' + ext_type + ' is not for a FITS Binary Table',/CON

 n = sxpar( h, 'TFIELDS', Count = N_tfields)  
 if N_tfields EQ 0 then message, $
	'ERROR - Required TFIELDS keyword is missing from binary table header'

 print,'FITS Table Header'
 print,'Size of Table Array: ',strtrim(naxis(0),2),' by ',strtrim(naxis(1),2)
 print,'Extension Name:   ',sxpar(h,'EXTNAME')
 print,' '
 print,'Field      Name       Unit      Frmt   Null    Comment'
 print,' '

 tnull =  strarr(n)
 tform = strarr(n) & tunit = tform & ttype =tform & tcomm = tform
 key = strmid( h, 0, 5)

 for i = 1, N_elements(h)-1 do begin

 case key(i) of
 'TTYPE':   begin
           j = fix(strtrim(strmid(h(i),5,3),2))
          apos = strpos( h(i), "'") 
          ttype(j-1) = strmid( h(i), apos+1, 20)
          slash = strpos(h(i),'/')
          if slash GT 0 then $
              tcomm(j-1) = strcompress( strmid(h(i), slash+1, 35))
          end

 'TFORM':  begin
          apos = strpos( h(i), "'") 
          tform(fix(strtrim(strmid(h(i),5,3),2))-1) = $
                                  strtrim( strmid( h(i), apos+1, 20), 2)
          end

 'TUNIT':  begin 
          apos = strpos( h(i), "'") 
          tunit(fix(strtrim(strmid(h(i),5,3),2))-1) = strmid(h(i),apos+1,20)
          end
 'TNULL':  begin
          tnull(fix(strtrim(strmid(h(i),5,3),2))-1) = $
                                 strtrim( strmid( h(i), 10, 20 ),2)
          end
 'END  ':  goto, DONE 
 ELSE :
 end
 endfor

DONE:

 for i=0,n-1 do begin 
        xtype = gettok(ttype(i),"'") & xtype = gettok(ttype(i),"'")
        xunit = gettok(tunit(i),"'") & xunit = gettok(tunit(i),"'")
        xform = gettok(tform(i),"'") & xform = gettok(tform(i),"'")
	print,i+1,xtype,xunit,xform,strtrim(tnull(i),2),tcomm(i), $
                format='(I3,1x,A12,1X,A12,3x,A3,A10,A)'
 endfor

 return
 end
