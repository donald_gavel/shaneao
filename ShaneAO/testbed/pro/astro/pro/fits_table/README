/FITS_TABLE                                               December 1994

This directory contains IDL procedures for reading and writing FITS ASCII 
tables, and for reading FITS binary tables.   These procedures assume that a 
FITS table array and header have already been read using the procedure 
READFITS.     For example, to read the first table extension of the FITS file 
test.fits into a table array, tab, and header, htab:

     IDL> tab = readfits( 'test.fits', h, /EXTEN)


The FT* procedures in this directory are used to manipulate an ASCII table
and the TB* procedures are used to manipulate a binary table.    Currently,
procedures can be used to read or write an ASCII table, but only to read
a binary table.    In addition, the TB* procedures cannot be used to read
variable length binary tables.   (Additional notes on the TB* procedures are
given in Appendix 1 below.)   In April 1992, Bill Thompson wrote a more 
complete set of IDL procedures to both read and write FITS binary tables.    
These procedures are located in the pro/fits_bintable and should probably be 
used in preference to the ones in this directory.     

The LaTeX file FT.TEX gives some documentation on the use of these procedures.

The procedures in this directory make use of the STSDAS I/O procedures 
in the /SDAS subdirectory and some additional procedures in the /MISC
directory.  

The procedures FTDIR, FTHELP, FTPRINT, TBPRINT, TBHELP use the non-standard 
system variables !TEXTOUT and !TEXTUNIT.  These can be added to one's session 
using the procedure ASTROLIB.

FTADDCOL - Add a new column to a FITS ASCII table
FTCREATE - Create an empty FITS ASCII table (H and TAB)
FTDELCOL - Delete specified column from a FITS ASCII table
FTDELROW - Delete specified row(s) from a FITS ASCII table
FTGET() - Extract a specified field from a column in a FITS ASCII table
FTHELP - Display the fields in a FITS ASCII table header
FTHMOD - Modify the attributes of a field in a FITS ASCII table
FTINFO - Give information about a specified field in a FITS ASCII table
FTPRINT - Print specified columns and rows in a FITS ASCII table
FTPUT - Update or add data to a field in a FITS ASCII table
FTSIZE - Return the size and number of fields in a FITS ASCII table
FTSORT - Sort a FITS ASCII table according to the values in a specified field.
TBDELCOL - Delete specified column from a FITS Binary table
TBDELROW - Delete specified row(s) from a FITS Binary table
TBGET() - Extract a specified field from a column in a FITS Binary table
TBHELP - Display the fields in a FITS Binary table header
TBINFO - Give information about a specified field in a FITS  Binary table
TBPRINT - Print specified columns and rows in a FITS Binary table
TBSIZE - Return the size and number of fields in a FITS Binary table

Appendix 1    Notes on FITS Binary Table (TB*) procedures

                                      
1.   The procedure TBPRINT only gives a very crude conversion of the 3D table
     to ASCII format, and does not work for vector fields.   The procedure
     TBGET should be used to obtain all values of a specified column.  

2.   Below is a sample session with a ROSAT binary table file.    The ROSAT
     IDL RECIPES LaTeX file (available via anonymous FTP on 
     legacy.gsfc.nasa.gov in the software/IDL directory) gives many more 
     example of the use of the tb*.pro procedures to analyze FITS binary
     tables

 IDL> tab= readfits('rh150013.abk', h,/ext)  ;Read FITS binary table & header
 IDL> tbhelp,h                              ;Describe FITS binary table header

;FITS Table Header
;Size of Table Array: 20 by 89
;Extension Name:   AVGBKS  
; 
;Field      Name       Unit      Frmt   Null    Comment
; 
;  1 SECT_START_T      SECS      1D            
;  2 SECT_STOP_TI      SECS      1D            
;  3 AVGBK_IN_SEC CNTS/PIX2/SE   1E            

IDL> a = tbget(h,tab,'AVGBK_IN_SEC')    ;Read contents of AVGBK_IN_SEC column
                                        ;into IDL variable, a 
IDL> print,tbget(h,tab,2,indgen(16) )   ;Print second column, first 16 values
;       4590203.0       4590219.0       4590229.0       4590239.0
;       4590249.0       4590259.0       4590269.0       4590279.0

IDL> tbprint,h,tab,-1,indgen(5)+1       ;Print all columns, first 5 rows
;      4590210.0        4590219.0  3.944123e-07
;      4590219.0        4590229.0  4.930154e-07
;      4590229.0        4590239.0  4.683646e-07

