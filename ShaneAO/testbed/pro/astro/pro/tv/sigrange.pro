	FUNCTION SIGRANGE,ARRAY,FRACTION=FRACTION,BADPIXEL=BADPIXEL
;+
; NAME:
;	SIGRANGE
; PURPOSE:
;	Selects out the most significant range in the data to be used in 
;	displaying images.
;
; CALLING SEQUENCE:
;	OUTPUT = SIGRANGE( ARRAY  [, FRACTION ] )
;
; INPUTS:
;	ARRAY	 = Array to take most significant range of.
;
; OPTIONAL KEYWORD PARAMETERS:
;	FRACTION = Fraction of data to consider most significant.
;		   Defaults to 0.9
;	BADPIXEL = Value used to flag bad points.  Data points with this value
;		   are not considered or changed.
;
; OUTPUTS:
;	The function returns an array where values above and below the
;	selected range are set equal to the maximum and minimum of the
;	range respectively.
;
; RESTRICTIONS:
;	ARRAY must have more than two points.  Fraction must be greater than 0 
;	and less than 1.
;
; PROCEDURE:
;	The histogram of ARRAY is used to select the most significant range.
;
; MODIFICATION HISTORY:
;	W.T.T., Oct. 1987.
;	W.T.T., Jan. 1991.  Changed FRACTION to keyword, and added keyword
;			    BADPIXEL.
;-
;
	IF N_ELEMENTS(FRACTION) NE 1 THEN FRACTION = 0.9
	IF N_ELEMENTS(ARRAY) LE 2 THEN BEGIN
		PRINT,'*** Not enough points to form histogram, routine SIGRANGE.'
		RETURN,ARRAY
	END ELSE IF (FRACTION LE 0) OR (FRACTION GE 1) THEN BEGIN
		PRINT,'*** Fraction must be GT 0 and LT 1, routine SIGRANGE.'
		RETURN,ARRAY
	ENDIF
;
;  Get the total range of the data, excluding any bad points.
;
	BANG_C = !C
	IF N_ELEMENTS(BADPIXEL) EQ 1 THEN BEGIN
		WW = WHERE(ARRAY EQ BADPIXEL)
		AMAX = MAX(ARRAY(WHERE(ARRAY NE BADPIXEL)))
		AMIN = MIN(ARRAY(WHERE(ARRAY NE BADPIXEL)))
	END ELSE BEGIN
		AMAX = MAX(ARRAY)
		AMIN = MIN(ARRAY)
	ENDELSE
	ATEMP = ARRAY
	IF AMIN EQ AMAX THEN GOTO,EXIT_POINT
	DELTA = 0
;
;  Form the histogram, and calculate an array expressing the fraction of points
;  that fall within or below the given bin.
;
FIND_RANGE:
	LAST_DELTA = DELTA
	X = AMIN  +  FINDGEN(101) * (AMAX - AMIN) / 100.
	H = HISTOGRAM(ATEMP, MIN=AMIN, BINSIZE=(AMAX - AMIN)/100.)
	FOR I = 1,N_ELEMENTS(H)-1 DO H(I) = H(I) + H(I-1)
	H = H / FLOAT(N_ELEMENTS(ATEMP))
;
;  Estimate the endpoints corresponding to the specified range, and calculate
;  the values at these endpoints.  Limit the array to be within these values.
;
	IMIN = (MIN( WHERE( H GT ((1. - FRACTION) / 2.) )) - 1) > 0
	IMAX =  MIN( WHERE( H GT ((1. + FRACTION) / 2.) ))
	AMIN = X(IMIN)
	AMAX = X(IMAX)
	ATEMP = AMIN > ARRAY < AMAX
;
;  If the range calculated has changed by more than 5% from the last iteration,
;  the reiterate.
;
	DELTA = AMAX - AMIN
	RATIO = (DELTA - LAST_DELTA) / (DELTA + LAST_DELTA)
	IF ABS(RATIO) GT 0.05 THEN GOTO,FIND_RANGE
;
;  If a bad pixel flag value was passed, then reset those points to the flag
;  value.  Return the adjusted array.
;
EXIT_POINT:
	!C = BANG_C
	IF N_ELEMENTS(BADPIXEL) EQ 1 THEN ATEMP(WW) = BADPIXEL
	RETURN,ATEMP
	END
