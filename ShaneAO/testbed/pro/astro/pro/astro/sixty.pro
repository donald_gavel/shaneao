      FUNCTION sixty,scalar
;+
; NAME:
;	SIXTY()
; PURPOSE:
;	Converts decimal number to sexigesimal.
;	Reverse of TEN function.
;
; CALLING SEQUENCE:
;	X = SIXTY( SCALAR ) 
;
; INPUTS:
;	SCALAR -- Decimal quantity.  
; OUTPUTS:
;	Function value returned = double real vector of three elements, 
;	sexigesimal equivalent of input decimal quantity.
;	A negative number is signified by making the first non-zero
;	element of the output vection negative.
;
; PROCEDURE:
;	Mostly involves checking arguments and setting the sign.
;
; MODIFICATION HISTORY:
;	Written by R. S. Hill, STX, 19-OCT-87         
;	Output changed to single precision.  RSH, STX, 1/26/88
;-
      np = n_params(0)
      if (np ne 1) then goto,arg_error
      sz = size(scalar)
      ndim = sz(0) 
      if (ndim ne 0) then goto,arg_error
      ss=abs(3600.0d0*scalar)
      mm=abs(60.0d0*scalar) 
      dd=abs(scalar) 
      result=fltarr(3)
      result(0)=float(fix(dd))
      result(1)=float(fix(mm-60.0d0*result(0)))
      result(2)=float(ss-3600.d0*result(0)-60.0d0*result(1))
      if scalar lt 0.0d0 then begin 
         if result(0) ne 0 then result(0) = -result(0) else $
         if result(1) ne 0 then result(1) = -result(1) else $
         result(2) = -result(2)
      endif
      return,result
arg_error:  
      print,'One scalar argument needed'                           
      return,replicate(100.0e0,3)
      end
