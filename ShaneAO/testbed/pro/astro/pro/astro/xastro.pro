; XASTRO.pro,1995/05/9 
; =============================================================================


pro widastro, se1, se2, EQUINOX1, equinox2, RA_DEC, RA, DEC, newRA,$
             newDEC,Bchk1,Bchk2
;
; NAME:
;	WIDASTRO
; PURPOSE:
;       astronomical utitlity for precession and coordinate conversion.
;       subroutine used by XASTRO
;
; CALLING SEQUENCE:
;	WIDASTRO, SE1, SE2, EQUINOX1, ......
;
; INPUTS:
; se1  se2  selection = 
;  0    0     (0) 'Precession: (RA, Dec)',                  
;  0    1     (1) 'Conversion: (RA, Dec) --> Galactic', 
;  1    0     (2) 'Conversion: Galactic --> (RA, Dec 1950)', 
;  0    2     (3) 'Conversion: (RA, Dec 1950) --> Ecliptic', 
;  2    0     (4) 'Conversion: Ecliptic --> (RA, Dec 1950)', 
;  2    1     (5) 'Conversion: Ecliptic --> Galactic',       
;  1    2     (6) 'Conversion: Galactic --> Ecliptic'
;
; EQUINOX1, equinox2 - 
;    the input equinox to be used when converting 
;  	      between celestial and other coordinates. 
;
; RA_DEC - the concatenated input string of RA and DEC
;
; RA, DEC - the decomposed input strings of RA and DEC
;
; newRA, newDEC - output strings of RA and DEC
;
; Bchk1, Bchk2 - first character of the input strings of RA and DEC
;
; METHOD:
;	WIDASTRO uses PRECESS, BPRECESS, JPRECESS to compute precession, 
;   and EULER to compute coordinate conversions.   
;   The procedure GET_COORDS is used to
;	read the coordinates, and ADSTRING to format the RA,Dec output.
;
; NOTES:
;	WIDASTRO temporarily sets !QUIET to suppress compilation messages and
;	keep a pretty screen display.   
;         
; PROCEDURES USED:
;	Procedures: GET_COORDS, EULER, PRECESS, BPRECESS,JPRECESS
;   Function: ADSTRING
; REVISION HISTORY
;   Written by C.-H. Joseph Lyu    May 1995
;
 On_error,2                    ;Return to caller
 input_type =   [0,0,1,0,2,2,1]     ;0= RA,Dec  1= Galactic   2 = Ecliptic
 output_type =  [0,1,0,2,0,1,2]        

 sv_quiet = !quiet & !quiet = 1	;Don't display compiled procedures
 if se1 eq 1 then equinox1=1950.0
 if se2 eq 1 then equinox2=1950.0
 yeari = equinox1  & yearf = equinox2   ;equinox values except for Precession

 for i=0, 6 do begin
   if (strtrim(string(se1),2) eq input_type(i)) and $
   (strtrim(string(se2),2) eq output_type(i)) then selection=i
 endfor

 case output_type( selection) of
   0:  OutName = " RA Dec (" + string( yearf, f= "(F6.1)" ) + "):  "
   1:  OutName = " Galactic longitude and latitude: "
   2:  OutName = " Ecliptic longitude and latitude: "
 endcase 

 case input_type( selection ) of 
  0:  InName = "RA Dec (" + string(yeari ,f ='(F6.1)' ) + ')'
  1:  InName = "galactic longitude and latitude: "
  2:  InName = "ecliptic longitude and latitude: "
 endcase
 
     get_coords,coords,'Enter '+ InName, Numcoords, Instring=RA_DEC 

 if ( coords(0) EQ -999 ) then begin        ;Normal Return
   print,'Invalid RA & DEC input !!!' 
   newRA='ErrorEntry' & newDEC='ErrorEntry'
	!quiet = sv_quiet
	return
 endif

 ra = coords(0) & dec = coords(1)
 if Numcoords EQ 6 then ra = ra*15.

; -------------------------------------------------------------------
; For Jyeari --> Jyearf    Do Precess only
 If (Bchk1 eq 74) and (Bchk2 eq 74) then begin
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      precess, ra , dec , yeari, yearf    ;Actual Calculations
      newra = ra & newdec = dec

   endif else begin 
      if yeari NE 1950 then precess, ra, dec, yeari, 1950
      euler, ra, dec, newra, newdec, selection
      if yearf NE 1950 then precess, newra,newdec, 1950, yearf
		 
   endelse
 endif
 
; -------------------------------------------------------------------
; For Byeari --> Byearf,    do Precess,/fk4
 If (Bchk1 eq 66) and (Bchk2 eq 66) then begin
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      precess, ra , dec , yeari, yearf,/FK4    ;Actual Calculations
      newra = ra & newdec = dec

   endif else begin 
      if yeari NE 1950 then precess, ra, dec, yeari, 1950,/FK4
      euler, ra, dec, newra, newdec, selection
      if yearf NE 1950 then precess, newra,newdec, 1950, yearf,/FK4
		 
   endelse
 endif

; --------------------------------------------------------------------
; For Jyeari --> Byearf
; Do Precess(,/FK5 is default) for Jyeari --> J2000
;    Bprecess for J2000 --> B1950
;    Precess,/fk4 for B1950 --> Byearf
 If (Bchk1 eq 74) and (Bchk2 eq 66) then begin

  if fix(equinox1) eq 2000 then goto,NXT4  
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      precess, ra , dec , yeari, 2000   ;Actual Calculations
   endif else begin 
      euler, ra, dec, newra, newdec, selection
      precess, newra, newdec, yeari, 2000
	  ra=newra & dec=newdec		 
   endelse
   
 NXT4:  
    if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      Bprecess, ra , dec , ra1950, dec1950   
      newra = ra1950 & newdec = dec1950
   endif else begin 
      Bprecess, ra, dec, ra1950, dec1950
      newra = ra1950 & newdec = dec1950	  
      if fix(equinox1) eq 2000 then $
	       euler, ra1950, dec1950, newra, newdec, selection		 
   endelse

  if fix(equinox2) eq 1950 then goto,NXT9  
   ra=newra & dec=newdec
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      precess, ra , dec , 1950, yearf,/FK4    ;Actual Calculations
         newra = ra & newdec = dec
   endif else begin 
    precess, newra,newdec, 1950, yearf,/FK4		 
   endelse
   
 endif

; ----------------------------------------------------------------------
; For Byeari --> Jyearf
; Do  Precess,/fk4  for    Byeari --> B1950
;    JPrecess   for        B1950 --> J2000
;     Precess(,/FK5 is default) for     J2000 --> Jyearf
 If (Bchk1 eq 66) and (Bchk2 eq 74) then begin

   if fix(equinox1) eq 1950 then goto,NXT7
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
      precess, ra , dec , yeari, 1950,/FK4    ;Actual Calculations
      newra = ra & newdec = dec
   endif else begin 
      precess, ra, dec, yeari, 1950,/FK4
      euler, ra, dec, newra, newdec, selection
    endelse   
   ra=newra & dec=newdec
   
   NXT7:
   if ( se1 EQ 0 ) and (se2 eq 0) then begin 
	 Jprecess, ra , dec , ra2000, dec2000    ;Actual Calculations
  endif else begin 
     if fix(equinox1) eq 1950 then $
	    euler, ra, dec, newra, newdec, selection
     Jprecess, newra,newdec, ra2000, dec2000
   endelse
     newra = ra2000 & newdec = dec2000
 
  if fix(equinox2) eq 2000 then goto,NXT9
   ra=newra & dec=newdec
   precess, ra , dec, 2000, yearf  ;Actual Calculations
   newra=ra & newdec=dec		 
 
  endif
 NXT9:
 if newra LT 0 then newra = newra + 360.
 end    


; =============================================================================


; XHELP.PRO  -- 6/20/95
;
; NAME:
;	XHELP
; PURPOSE:
;   Display useful information for performing coordinate conversion/precession
;   used by the procedures XASTRO to precess astronomical coordinates
;
; CATEGORY:
;       Widgets
;
; REVISION HISTORY
;	Written, C.-H. Joseph Lyu, HSTX Corporation, June 1995
;    
Pro xhelp_event, event 
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control,event.id, get_value=buttonvalue 
  CASE buttonValue of 
  'OK' : BEGIN 
         widget_control, event.top,/destroy 
         END 
  ENDCASE 
ENDIF 
END 
 
 
Pro xhelp
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='XASTRO (Coordinate Calculation) Help') 

 dir=getenv('ASTRO_DATA') 
; IF !VERSION.OS NE 'vms' then $ dir= dir +'/'    ; Unix
 openr,inunit,dir+'xastrohelp.dat',/get_lun

lenmax0=0
txt0=strarr(1) & txt1=strarr(650) & i=-1
txt2=' '
while not EOF(INUNIT) do begin
 readf,inunit,txt0
 txt2=txt0(0)
 i=i+1 & lenmax1=strlen(txt2)
 if lenmax0 lt lenmax1 then lenmax0=lenmax1
 txt1(i)=txt0
endwhile
 free_lun,inunit
 txt=txt1(0:I) 
 if lenmax0 gt 80 then lenmax0=80
 xh1=widget_text(tlb,Value=txt,Uvalue='txt',xsize=lenmax0,ysize=25,/Scroll)
butbase=widget_base(tlb,column=1) 
accept=widget_button(butbase, Value='OK') 
widget_control, tlb, /Realize 
Xmanager, 'xhelp', tlb,/Modal 
return
end


; =============================================================================


pro GET_string,Instring,outputs

;
;   Define some parameters and variables.
;
Bell     = string(7B)
Minus    = 45      ; ascii of "-"
Decimal  = 46      ; ascii of "."
Zero     = 48      ; ascii of "0"
Nine     = 57      ; ascii of "9"
outputs=strarr(80)
NumVals  = 0

 ; The final space is needed for parsing purposes
 Instring = strtrim(Instring,2) + " " 

;
;   All's well.  Get the byte values for the characters in the input string.
;
BInstring = byte(Instring)

;
;   Begin the loop that parses the input string.
;   Start by loading the byte value of the input string into the BC variable.
; Check to see if the character is a numeral between 0-9 
; or a blank space
nmax=(strlen(Instring)-1)
I=0 & J=0 & K=0
for N = 0,nmax do begin
   BC = BInstring(N)
   if BC ne 32 then K=K+1
   if (N ge 1) and (BC eq 32) then begin
     outputs(I)=strmid(Instring,J,K) 
	 J=N+1 & i=i+1 & K=0
   endif
  endfor
if i lt 1 then print,' I is too small, I= ',I
outputs=outputs(0:I-1)
return
end   ;   procedure GET_String   


; =============================================================================


; Open_file.pro  -- 6/20/95
;
; NAME:
;	OPEN_FILE
; PURPOSE:
;	Open a data file to save all the computed transformed coordinates
;	used by the procedures XASTRO to precess astronomical coordinates
;
; CATEGORY:
;       Widgets
;
; REVISION HISTORY
;	Written, C.-H. Joseph Lyu, HSTX Corporation, June 1995
;    
Pro Open_file_event, event 
common DATAOUT,outunit
common Data2,printset
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control, event.top, get_Uvalue=info,/no_copy 
  widget_control,event.id, get_value=buttonvalue 
CASE buttonValue of 
  'CANCEL' : BEGIN 
  printset=0
  widget_control, event.top,/destroy 
  END 
  'ACCEPT' : BEGIN
  printset=1 
  widget_control, info.path_id,get_value=npath
  widget_control, info.file_id,get_value=nfile
  newfile=nfile(0) & newpath=npath(0)
  if (!version.os eq 'MacOS') or (!version.os eq 'vms') then begin
    openw,outunit,newpath+newfile,/get_lun
  endif else begin
; for other os such as OSF, sunos, etc.
    openw,outunit,newpath+'/'+newfile,/get_lun
  endelse
  widget_control, event.top,/destroy 
  END 
  ENDCASE 
ENDIF 
END 
 
 
Pro Open_file
common DATAOUT,outunit
common Data2,printset
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='Confirm/Cancel Information') 
cd,current=cdir
path_id=cw_field(tlb, title='path= ',$ 
        Value=cdir,xsize=60) 
file_id=cw_field(tlb, title='filename= ', Value='xastro.dat') 
butbase=widget_base(tlb,row=1, /frame) 
cancel=widget_button(butbase, Value='CANCEL') 
accept=widget_button(butbase, Value='ACCEPT') 
widget_control, tlb, /Realize 
info={file_id:file_id,path_id:path_id} 
widget_control, tlb, set_Uvalue=info, /no_copy 
Xmanager, 'Open_file', tlb,/Modal 
return
end


; =============================================================================


PRO PURENUMBER,STRING1,STRING2
; TAKE OUT ANYTHING THAT DOES NOT BELONG TO A REAL NUMBER
;
; NAME:
;	PURENUMBER
; PURPOSE:
;	Take out anything that does not belong to a real number and replace 
;       it by blank space
;
; CALLING SEQUENCE:
;	Purenumber, string1, string2
;
; INPUTS:
;	STRING1 - any character string, e.g., '-12d23m25.5s'
;
; OUTPUT:
;	STRING2 - a real number character string, e.g., '-12 23 25.5'
;
; REVISION HISTORY
;	Written, C.-H. Joseph Lyu, HSTX Corporation, June 1995
;    
On_error,2
STR1=BYTE(STRING1) & STR2=STR1
LEN1=STRLEN(STRING1)-1

FOR I=0, LEN1 DO BEGIN
  BC=STR1(I)
  IF (BC LT 43) OR (BC GT 57) OR (BC EQ 44) OR (BC EQ 47) THEN $
     STR2(I)=BYTE(32)
ENDFOR

STRING2=STRING(STR2)

END


; =============================================================================


Pro xmsg1_event, event 
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control,event.id, get_value=buttonvalue 
  CASE buttonValue of 
  'OK' : BEGIN 
         widget_control, event.top,/destroy 
         END 
  ENDCASE 
ENDIF 
END 
 
 
Pro xmsg1
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='Confirm Information') 
path_id=cw_field(tlb, title='Error Message ',$ 
        Value='No/invalid RA or DEC coordinate(s) !!',xsize=38) 
file_id=cw_field(tlb, title='  Solution:   ',$
        Value='Re-enter RA/DEC (note -90 < DEC < 90)',xsize=38) 
butbase=widget_base(tlb,column=1) 
accept=widget_button(butbase, Value='OK') 
widget_control, tlb, /Realize 
Xmanager, 'xmsg1', tlb,/Modal 
return
end


; =============================================================================


Pro xmsg2_event, event 
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control,event.id, get_value=buttonvalue 
  CASE buttonValue of 
  'OK' : BEGIN 
         widget_control, event.top,/destroy 
         END 
  ENDCASE 
ENDIF 
END 
 
 
Pro xmsg2
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='Confirm Information') 
path_id=cw_field(tlb, title='Error Message ',$ 
        Value='No coordinate(s) conversion !!',xsize=38) 
file_id=cw_field(tlb, title='  Solution:   ',$
        Value='Click on RUN button',xsize=38) 
butbase=widget_base(tlb,column=1) 
accept=widget_button(butbase, Value='OK') 
widget_control, tlb, /Realize 
Xmanager, 'xmsg2', tlb,/Modal 
return
end


; =============================================================================


Pro xmsga_event, event 
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control,event.id, get_value=buttonvalue 
  CASE buttonValue of 
  'OK' : BEGIN 
         widget_control, event.top,/destroy 
         END 
  ENDCASE 
ENDIF 
END 
 
 
Pro xmsga
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='Confirm Information') 
path_id=cw_field(tlb, title='Error Message ',$ 
        Value='Invalid RA coordinate !!',xsize=30) 
file_id=cw_field(tlb, title='  Solution:   ',$
        Value='Re-enter RA coordinate',xsize=30) 
butbase=widget_base(tlb,column=1) 
accept=widget_button(butbase, Value='OK') 
widget_control, tlb, /Realize 
Xmanager, 'xmsga', tlb,/Modal 
return
end


; =============================================================================


Pro xmsgb_event, event 
eventName=Tag_Names(event,/structure_name) 
If eventName eq 'WIDGET_BUTTON' then begin 
  widget_control,event.id, get_value=buttonvalue 
  CASE buttonValue of 
  'OK' : BEGIN 
         widget_control, event.top,/destroy 
         END 
  ENDCASE 
ENDIF 
END 
 
 
Pro xmsgb
tlb = widget_base(column=1, xoffset=70, yoffset=200, $ 
   title='Confirm Information') 
path_id=cw_field(tlb, title='Error Message ',$ 
        Value='Invalid DEC coordinate !!',xsize=30) 
file_id=cw_field(tlb, title='  Solution:   ',$
        Value='Re-enter DEC coordinate',xsize=30) 
butbase=widget_base(tlb,column=1) 
accept=widget_button(butbase, Value='OK') 
widget_control, tlb, /Realize 
Xmanager, 'xmsgb', tlb,/Modal 
return
end


; XASTRO.pro,1995/05/9 
; Copyright (c) 1995, Hughes STX
;	Unauthorized reproduction prohibited.
;+
; NAME:
;       XASTRO	
;
; PURPOSE:
;   Astronomical utility for precession and coordinate conversion
;
; CATEGORY:
;	Widgets.
;
; CALLING SEQUENCE:
;	NO KEYWORDS
;
; INPUTS:
;   RA,DEC - Input right ascension and declination for the intended equinox
;            either Jyear or Byear.
;
; OUTPUTS:
;   RA,DEC - The corresponding right ascension and declination in the given
;            equinox, either Jyear or Byear.
;
; OPTIONAL OUTPUTS:
;   To click on SAVE-DATA, one can save the results of the computed precession 
;   and coordinate conversion to the designated file (test.dat) under the
;   current working directory
;
; EXAMPLE:
;   Inputs: Galactic, Lon Lat (B1950):  0   0
;   Outputs: Equatorial, RA Dec (J2000): 17h45m37.224183s -28d56m10.236585s
;
; MODIFICATION HISTORY:
; 	Written by:	C.-H. Joseph Lyu ;	May, 1995
;-

PRO XASTRO_EVENT, EVENT
COMMON DATAI, SE1, SE2,NEWRA,NEWDEC,EQUINOX1,EQUINOX2
COMMON DATAJ, RA0,DEC0,EQUIN1,EQUIN2,BGROUP82,BGROUP68,PRINTER
COMMON DATAS0, RA1, DEC1
COMMON DATAS, RA2, DEC2
COMMON DATAS2, INPUT1
COMMON DATAS3, INPUT2
COMMON DATAOUT, OUTUNIT
COMMON DATA2, PRINTSET
COMMON DATAM, BASE3A,BASE3B

; GET THE USER VALUE OF THE WIDGET THAT CAUSED THE EVENT
WIDGET_CONTROL, EVENT.ID, GET_UVALUE=EVENTUVALUE
WIDGET_CONTROL, EVENT.TOP, GET_UVALUE=INFO

WIDGET_CONTROL,INFO.INPUT1,GET_VALUE=OUT1
WIDGET_CONTROL,INFO.INPUT2,GET_VALUE=OUT2
WIDGET_CONTROL,INFO.RA1,GET_VALUE=RA
WIDGET_CONTROL,INFO.DEC1,GET_VALUE=DEC
WIDGET_CONTROL,INFO.RA2,GET_VALUE=RATS
WIDGET_CONTROL,INFO.DEC2,GET_VALUE=DECTS

RAX=RA(0) & DECX=DEC(0)
PURENUMBER,RAX,OUTRA & PURENUMBER,DECX,OUTDEC
RAX=STRTRIM(OUTRA,2) & DECX=STRTRIM(OUTDEC,2)
RSTR=BYTE(RAX) & LEN1=STRLEN(RAX)
DSTR=BYTE(DECX) & LEN2=STRLEN(DECX) 
; CHECK TO SEE IF DATA FORMAT IS O.K. E.G. 0.0. OR -1-1 IS NOT VALID
FOR I=1, LEN1-1 DO BEGIN
  XTST=RSTR(I) & XTSTO=RSTR(1) & XTST0=RSTR(0)
  IF (XTSTO EQ 46) AND (XTST0 EQ 46) THEN BEGIN
    XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
  ENDIF
  IF (XTST EQ 45) OR (XTST EQ 43) THEN BEGIN
    XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
  ENDIF
  IP2=I+2 & IP1=I+1
  IF (IP1 LE LEN1-1) THEN BEGIN
    XTST1=RSTR(IP1) 
    IF (XTST EQ 46) AND (XTST1 EQ 46) THEN BEGIN
      XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
  ENDIF
  IF (IP2 LE LEN1-1) THEN BEGIN
    XTST1=RSTR(IP1) & XTST2=RSTR(IP2)
    IF (XTST EQ 46) AND (XTST1 EQ 46) THEN BEGIN
      XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
    IF (XTST1 EQ 46) AND (XTST2 EQ 46) THEN BEGIN
      XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
    IF (XTST EQ 46) AND (XTST2 EQ 46) THEN BEGIN
      IF (XTST1 GE 48) AND (XTST1 LE 57) THEN BEGIN
        XMSGA & RAX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
      ENDIF
    ENDIF
  ENDIF
ENDFOR
FOR I=1, LEN2-1 DO BEGIN
  XTST=DSTR(I) & XTSTO=DSTR(1) & XTST0=DSTR(0)
  IF (XTSTO EQ 46) AND (XTST0 EQ 46) THEN BEGIN
    XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
  ENDIF
  IF (XTST EQ 45) OR (XTST EQ 43) THEN BEGIN
    XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
  ENDIF
  IP2=I+2 & IP1=I+1
  IF (IP1 LE LEN2-1) THEN BEGIN
    XTST1=DSTR(IP1) 
    IF (XTST EQ 46) AND (XTST1 EQ 46) THEN BEGIN
      XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
  ENDIF
  IF (IP2 LE LEN2-1) THEN BEGIN
    XTST1=DSTR(IP1) & XTST2=DSTR(IP2)
    IF (XTST EQ 46) AND (XTST1 EQ 46) THEN BEGIN
      XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
    IF (XTST1 EQ 46) AND (XTST2 EQ 46) THEN BEGIN
      XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
    ENDIF
    IF (XTST EQ 46) AND (XTST2 EQ 46) THEN BEGIN
      IF (XTST1 GE 48) AND (XTST1 LE 57) THEN BEGIN
        XMSGB & DECX='0' & SHOWSTRINGIN,RAX,DECX & GOTO,NXT2
      ENDIF
    ENDIF
  ENDIF
ENDFOR
TXT1=STRTRIM(OUT1(0),2) & TXT2=STRTRIM(OUT2(0),2)
IF SE1 EQ 1 THEN TXT1='B1950.0'
IF SE2 EQ 1 THEN TXT2='B1950.0'
BSTR1=BYTE(TXT1) & BSTR2=BYTE(TXT2)
LEN1=STRLEN(TXT1)-1 & LEN2=STRLEN(TXT2)-1
BC1=BSTR1(0) & BC2=BSTR2(0) 
IF BC1 EQ 98 THEN BEGIN
  BC1=BYTE(66) & BSTR1(0)=BC1
  SHOWSTRINGA,STRING(BSTR1)
ENDIF
IF BC2 EQ 98 THEN BEGIN
  BC2=BYTE(66) & BSTR2(0)=BC2
  SHOWSTRINGB,STRING(BSTR2)
ENDIF
STR1=STRING(BC1) & STR2=STRING(BC2)
BC10=BC1 & BC20=BC2
IF (BC1 NE 66) THEN BEGIN
   IF (BC1 GE 48) AND (BC1 LE 57) THEN BEGIN
      EQUIN1=STRING(BYTE(74))+STRING(BSTR1)
   ENDIF ELSE BEGIN
      EQUIN1=STRING(BYTE(74))+STRING(BSTR1(1:LEN1))
   ENDELSE
   BC1=BYTE(74)  
   IF TXT1 NE EQUIN1 THEN SHOWSTRINGA,EQUIN1
END
IF (BC2 NE 66) THEN BEGIN
    IF (BC2 GE 48) AND (BC2 LE 57) THEN BEGIN
      EQUIN2=STRING(BYTE(74))+STRING(BSTR2)
    ENDIF ELSE BEGIN
      EQUIN2=STRING(BYTE(74))+STRING(BSTR2(1:LEN2))
    ENDELSE
    BC2=BYTE(74)    
    IF TXT2 NE EQUIN2 THEN SHOWSTRINGB,EQUIN2
END
 FLAG1=0 & FLAG2=0
FOR I0=1, LEN1 DO BEGIN
  BC=BSTR1(I0)
  IF (BC LT 46) OR (BC GT 57) OR (BC EQ 47) THEN FLAG1=1
ENDFOR
FOR I0=1, LEN2 DO BEGIN
  BC=BSTR2(I0)
  IF (BC LT 46) OR (BC GT 57) OR (BC EQ 47) THEN FLAG2=1
ENDFOR
IF (FLAG1 NE 0) OR (FLAG2 NE 0) THEN GOTO,NXT
IF (BC10 GE 48) AND (BC10 LE 57) THEN BEGIN
   EQUINOX1=FLOAT(TXT1)   
ENDIF ELSE BEGIN
   EQUINOX1=FLOAT(STRING(BSTR1(1:LEN1)))
ENDELSE
IF (BC20 GE 48) AND (BC20 LE 57) THEN BEGIN
   EQUINOX2=FLOAT(TXT2)   
ENDIF ELSE BEGIN
   EQUINOX2=FLOAT(STRING(BSTR2(1:LEN2)))
ENDELSE
IF (EQUINOX1 LT 1500) OR (EQUINOX1 GT 2500) THEN FLAG1=1
IF (EQUINOX2 LT 1500) OR (EQUINOX2 GT 2500) THEN FLAG2=1
NXT:

CASE EVENTUVALUE OF

 'BTN2': BEGIN
         CASE Event.Value OF
         ; Button Run pressed ...........
         0: BEGIN
             IF (FLAG1 NE 0) THEN SHOWSTRINGA,'REENTERYRS'
             IF (FLAG2 NE 0) THEN SHOWSTRINGB,'>1500<2500' 
             IF (FLAG1 NE 0) OR (FLAG2 NE 0) THEN GOTO,NXT2
             IF (STRTRIM(RAX,2) EQ '') OR (STRTRIM(DECX,2) EQ '') THEN BEGIN
                XMSG1 & GOTO,NXT2
             ENDIF
             GET_STRING,RAX,OUT1
             GET_STRING,DECX,OUT2
             TMDEC=FIX(OUT2(0))
             IF (TMDEC LT -90) OR (TMDEC GT 90) THEN BEGIN
               XMSG1 & GOTO,NXT2
             ENDIF
	     NUMB=N_ELEMENTS(OUT1) & NUM2=N_ELEMENTS(OUT2)           
             FOUTB=BYTE(OUT2(0)) & FOUTB2=BYTE(DECX)
             IF NUMB GE 3 THEN RA0=OUT1(0)+'h'+OUT1(1)+'m'+OUT1(2)+'s'
             IF NUM2 GE 3 THEN BEGIN
                  IF (FLOAT(OUT2(0)) GE 0.0) AND (FOUTB(0) NE 43) THEN $
                     DEC0='+'+OUT2(0)+'d'+OUT2(1)+'m'+OUT2(2)+'s' ELSE $
                     DEC0=OUT2(0)+'d'+OUT2(1)+'m'+OUT2(2)+'s' 
               ENDIF
             IF NUMB EQ 2 THEN RA0=OUT1(0)+'h'+OUT1(1)+'m'+'0.00s'
             IF NUM2 EQ 2 THEN BEGIN
                  IF (FLOAT(OUT2(0)) GE 0.0) AND (FOUTB(0) NE 43) THEN $
                     DEC0='+'+OUT2(0)+'d'+OUT2(1)+'m'+'0.00s' ELSE $
                     DEC0=OUT2(0)+'d'+OUT2(1)+'m'+'0.00s' 
               ENDIF
             IF NUMB EQ 1 THEN RA0=RAX
             IF NUM2 EQ 1 THEN BEGIN
                  IF (FLOAT(DECX) GE 0.0) AND (FOUTB2(0) NE 43) THEN $
                     DEC0='+'+DECX ELSE DEC0=DECX
               ENDIF

             IF (SE1 EQ 0) AND (NUMB EQ 1) THEN BEGIN
               TMP=(FLOAT(RAX) MOD 24) 
               RA7=FIX(TMP) & TMP2=(TMP-RA7)*60 & RA8=FIX(TMP2)
               RA9=(TMP2-RA8)*60. 
               RA0=STRTRIM(STRING(RA7),2)+'h'+$
                      STRTRIM(STRING(RA8),2)+'m'+$
	              STRTRIM(STRING(RA9),2)+'s'
               RAX=STRTRIM(STRING(RA7),2)+' '+$
                      STRTRIM(STRING(RA8),2)+' '+$
	              STRTRIM(STRING(RA9),2)+' '
              ENDIF
	      IF (NUM2 EQ 1) AND (SE1 EQ 0) THEN BEGIN
	          IF (FLOAT(DECX) GE 0) AND (FOUTB2(0) NE 43) THEN $
                    DEC0='+'+STRTRIM(DECX,2)+'d'+'0m0.00s' ELSE $
                    DEC0=STRTRIM(DECX,2)+'d'+'0m0.00s'
                  DECX=DECX+' 0'+' 0'
              ENDIF

	      RA_DEC=RAX+DECX
              IF (NUMB EQ 2) THEN RA_DEC=RAX+' 0'+' '+DECX
              IF (NUM2 EQ 2) THEN RA_DEC=RAX+' '+DECX+' 0'
              IF (NUMB EQ 2) AND (NUM2 EQ 2) THEN $
                         RA_DEC=RAX+' 0'+' '+DECX+' 0'
              IF (SE1 EQ 0) AND (SE2 EQ 0) THEN BEGIN
	        WIDGET_CONTROL,BGROUP82,SET_VALUE=0
	        WIDGET_CONTROL,BGROUP68,SET_VALUE=0
	      ENDIF
	      IF BC1 EQ 66 THEN EQUIN1=TXT1
	      IF BC2 EQ 66 THEN EQUIN2=TXT2

	      IF (SE1 EQ SE2) AND (EQUIN1 EQ EQUIN2) THEN BEGIN
		  NEWRA=RA0 & NEWDEC=DEC0
 	      ENDIF ELSE BEGIN
               IF (FLAG1 EQ 0) AND (FLAG2 EQ 0) THEN $
		  WIDASTRO,SE1,SE2,EQUINOX1,EQUINOX2,RA_DEC,$
                       RA,DEC,NEWRA,NEWDEC,BC1,BC2
;	       PRINT,SE1,SE2,NEWRA,NEWDEC
	       IF (SE2 EQ 0) THEN BEGIN
                RADEC,NEWRA,NEWDEC,IH,IM,XS,IDEG,IM2,XS2
                NEWRA=STRTRIM(IH,2)+'h'+STRTRIM(IM,2)+'m'+$
		          STRTRIM(XS,2)+'s'
                IDEG3=STRTRIM(IDEG,2) & IM3=STRTRIM(IM2,2)
                XS3=STRTRIM(XS2,2)
                IF FLOAT(IDEG3) GE 0 THEN $
                  NEWDEC='+'+IDEG3+'d'+IM3+'m'+XS3+'s' ELSE $
                  NEWDEC=IDEG3+'d'+IM3+'m'+XS3+'s'
                IF FIX(IM3) LT 0 THEN BEGIN
                 DSTR=BYTE(IM3) & LEN2=STRLEN(IM3)
                 IM3=STRING(DSTR(1:LEN2-1))
                 NEWDEC='-'+IDEG3+'d'+IM3+'m'+XS3+'s'
                ENDIF
                IF FLOAT(XS3) LT 0 THEN BEGIN
                 DSTR=BYTE(XS3) & LEN2=STRLEN(XS3)
                 XS3=STRING(DSTR(1:LEN2-1))
                 NEWDEC='-'+IDEG3+'d'+IM3+'m'+XS3+'s'
                ENDIF
	       ENDIF
               IF (SE2 EQ 1) OR (SE2 EQ 2) THEN BEGIN
                 NEWRA=STRING(NEWRA,'$(F14.7)')
                 IF FLOAT(NEWDEC) GE 0 THEN NEWDEC='+'+NEWDEC
                 NEWDEC=STRING(NEWDEC,'$(F14.7)')
               ENDIF
	      ENDELSE

               SHOWSTRINGIN,RA0,DEC0
               SHOWSTRINGOUT,NEWRA,NEWDEC

	       IF PRINTER NE 0 THEN BEGIN
		 IF SE1 EQ 0 THEN $
		    PRINTF,OUTUNIT,'INPUT RA,DEC=  ',RA0,', ',DEC0
		 IF SE1 EQ 1 THEN PRINTF,OUTUNIT,$
		     'Input Galactic Long & Lat=  ',RA0,', ',DEC0
		 IF SE1 EQ 2 THEN PRINTF,OUTUNIT,$
		      'Input Ecliptic Long & Lat=  ',RA0,', ',DEC0
		 PRINTF,OUTUNIT,'........Initial Equinox= ',EQUIN1
	         IF SE2 EQ 0 THEN BEGIN
                   PRINTF,OUTUNIT,'Output RA,DEC=  ',NEWRA,', ',NEWDEC
		   PRINTF,OUTUNIT,'........Final equinox= ',EQUIN2
                 ENDIF
		 IF SE2 EQ 1 THEN BEGIN
                   PRINTF,OUTUNIT,'Output Galactic Long & Lat=  ',$
                       NEWRA,', ',NEWDEC
                 ENDIF
		 IF SE2 EQ 2 THEN BEGIN
                    PRINTF,OUTUNIT,$
		     'Output Ecliptic Long & Lat=  ',NEWRA,', ',NEWDEC
		    PRINTF,OUTUNIT,'........Final Equinox= ',EQUIN2
                 ENDIF
		 PRINTF,OUTUNIT,' '
		ENDIF			  		
         END		
   
         1: XHELP
   
         ; Button Save-Data pressed ..........................
         2: IF PRINTER EQ 0 THEN BEGIN
		OPEN_FILE & IF PRINTSET EQ 0 THEN PRINTER=0
		IF PRINTSET EQ 1 THEN PRINTER=1
		IF PRINTER EQ 1 THEN BEGIN
		  PRINTF,OUTUNIT,STRMID(SYSTIME(),4,24)
                  PRINTF,OUTUNIT,' '
		  PRINTER=2
                 IF (STRTRIM(RAX,2) EQ '') OR (STRTRIM(DECX,2) EQ '') THEN BEGIN
                   XMSG1 & GOTO,NXT2
                 ENDIF
                 IF (STRTRIM(RATS(0),2) EQ '') OR $
                    (STRTRIM(DECTS(0),2) EQ '') THEN BEGIN
                   XMSG2 & GOTO,NXT2
                 ENDIF
		 IF SE1 EQ 0 THEN $
		    PRINTF,OUTUNIT,'INPUT RA,DEC=  ',RA0,', ',DEC0
		 IF SE1 EQ 1 THEN PRINTF,OUTUNIT,$
		     'INPUT GALACTIC LONG & LAT=  ',RA0,', ',DEC0
		 IF SE1 EQ 2 THEN PRINTF,OUTUNIT,$
		      'INPUT ECLIPTIC LONG & LAT=  ',RA0,', ',DEC0
		 PRINTF,OUTUNIT,'........INITIAL EQUINOX= ',EQUIN1
	         IF SE2 EQ 0 THEN BEGIN
                   PRINTF,OUTUNIT,'OUTPUT RA,DEC=  ',NEWRA,', ',NEWDEC
		   PRINTF,OUTUNIT,'........FINAL EQUINOX= ',EQUIN2
                 ENDIF
		 IF SE2 EQ 1 THEN BEGIN
                   PRINTF,OUTUNIT,'OUTPUT GALACTIC LONG & LAT=  ',$
                          NEWRA,', ',NEWDEC
                 ENDIF
		 IF SE2 EQ 2 THEN BEGIN
                    PRINTF,OUTUNIT,$
		     'OUTPUT ECLIPTIC LONG & LAT=  ',NEWRA,', ',NEWDEC
		    PRINTF,OUTUNIT,'........FINAL EQUINOX= ',EQUIN2
                 ENDIF
		 PRINTF,OUTUNIT,' '
		ENDIF
            ENDIF

         ; Button Quit pressed ..................
         3: BEGIN
            IF PRINTER NE 0 THEN FREE_LUN,OUTUNIT 
              WIDGET_CONTROL, EVENT.TOP, /DESTROY   
            END
         ENDCASE
         END

  'Bg1': BEGIN
         CASE Event.Value OF
         0: BEGIN
             SE1=0  & WIDGET_CONTROL,BASE3A,MAP=1
            END
         1: BEGIN
             SE1=1  & WIDGET_CONTROL,BASE3A,MAP=0
            END
         2: BEGIN
             SE1=2  & WIDGET_CONTROL,BASE3A,MAP=1
            END
         ENDCASE
         END
   

  'Bg2': BEGIN
         CASE Event.Value OF
         0: BEGIN
             SE2=0  & WIDGET_CONTROL,BASE3B,MAP=1
            END
         1: BEGIN
             SE2=1  & WIDGET_CONTROL,BASE3B,MAP=0
            END
         2: BEGIN
             SE2=2  & WIDGET_CONTROL,BASE3B,MAP=1
            END
         ENDCASE
         END

  'Equ1': WIDGET_CONTROL, RA1, /INPUT_FOCUS
 
  'Equ2': WIDGET_CONTROL, RA1, /INPUT_FOCUS
 
  'Fd3': WIDGET_CONTROL, DEC1, /INPUT_FOCUS

  'Fd5': WIDGET_CONTROL, RA1, /INPUT_FOCUS

ENDCASE
NXT2: IF WIDGET_INFO(EVENT.TOP,/VALID) THEN $
   WIDGET_CONTROL,EVENT.TOP,SET_UVALUE=INFO
END ; ******************************************************************

PRO SHOWSTRINGIN, STRING1, STRING2
COMMON DATAS0, RA1, DEC1
WIDGET_CONTROL, RA1, SET_VALUE=STRING1
WIDGET_CONTROL, DEC1, SET_VALUE=STRING2
END

PRO SHOWSTRINGOUT, STRING1, STRING2
COMMON DATAS, RA2, DEC2
WIDGET_CONTROL, RA2, SET_VALUE=STRING1
WIDGET_CONTROL, DEC2, SET_VALUE=STRING2
END

PRO SHOWSTRINGA,STRING1
COMMON DATAS2,INPUT1
WIDGET_CONTROL,INPUT1,SET_VALUE=STRING1
END

PRO SHOWSTRINGB,STRING2
COMMON DATAS3,INPUT2
WIDGET_CONTROL,INPUT2,SET_VALUE=STRING2
END

PRO XASTRO, GROUP=Group

COMMON DATAI, SE1, SE2,NEWRA,NEWDEC,EQUINOX1,EQUINOX2
COMMON DATAJ, RA0,DEC0,EQUIN1,EQUIN2,BGROUP82,BGROUP68,PRINTER
COMMON DATAS0, RA1, DEC1
COMMON DATAS, RA2, DEC2
COMMON DATAS2, INPUT1
COMMON DATAS3, INPUT2
COMMON DATAOUT, OUTUNIT
COMMON DATAM, BASE3A,BASE3B

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }

  PRINTER=0 & SE1=0 & SE2=0

  widget_control,default_font='6x13'

  TLB = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      TITLE='XASTRO -- Coord. Calc', $
      UVALUE='TLB', $
      XSIZE=500)

  BASE2 = WIDGET_BASE(TLB, $
      COLUMN=4, $
      MAP=1, $
      TITLE='Base1', $
      UVALUE='BASE2')

  BASE42 = WIDGET_BASE(BASE2, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='BASE42',XSIZE=110, YSIZE=100)

  LABEL53 = WIDGET_LABEL( BASE42, $
      UVALUE='LABEL53', $
      VALUE='   ')

  LABEL48 = WIDGET_LABEL( BASE42, $
      UVALUE='LABEL48', $
      VALUE='   ')

  LABEL44 = WIDGET_LABEL( BASE42, $
      UVALUE='LABEL44', $
      VALUE='Coordinate', $
      XSIZE=80, $
      YSIZE=20)

  LABEL41 = WIDGET_LABEL( BASE42, $
      UVALUE='LABEL41', $
      VALUE='System', $
      XSIZE=80, $
      YSIZE=20)


  Btns331 = [ $
    'Equatorial', $
    'Galactic', $
    'Ecliptic' ]
  BGROUP82 = CW_BGROUP( BASE2, Btns331, $
      COLUMN=1, $
      EXCLUSIVE=1, $
      FRAME=1, $
      LABEL_TOP='Input Parameters', $
      UVALUE='Bg1', $
      YSIZE=100)
  WIDGET_CONTROL,BGROUP82,SET_VALUE=0

  LABEL83 = WIDGET_LABEL( BASE2, $
      UVALUE='LABEL83', $
      VALUE='   ', $
      YSIZE=100)

  Btns334 = [ $
    'Equatorial', $
    'Galactic', $
    'Ecliptic' ]
  BGROUP68 = CW_BGROUP( BASE2, Btns334, $
      COLUMN=1, $
      EXCLUSIVE=1, $
      FRAME=1, $
      LABEL_TOP='Output Parameters', $
      UVALUE='Bg2', $
      YSIZE=100)
  WIDGET_CONTROL,BGROUP68,SET_VALUE=0

  BASE3 = WIDGET_BASE(TLB, $
      COLUMN=2, $
      SPACE=110, $
      XPAD=65, $
      MAP=1, $
      TITLE='Base2', $
      UVALUE='BASE3')

  BASE3A = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='BASE3A')

  BASE3B = WIDGET_BASE(BASE3, $
      COLUMN=1, $
      MAP=1, $
      UVALUE='BASE3B')

  FieldVal337 = [ $
    'B1950.0' ]

  INPUT1 = CW_FIELD( BASE3A,VALUE=FieldVal337, $
      ROW=1, $
      STRING=1, $
      TITLE='Equinox', $
      UVALUE='Equ1', $
      XSIZE=7,/RETURN_EVENTS)

  FieldVal339 = [ $
    'B1950.0' ]
  INPUT2 = CW_FIELD( BASE3B,VALUE=FieldVal339, $
      ROW=1, $
      STRING=1, $
      TITLE='Equinox', $
      UVALUE='Equ2', $
      XSIZE=7,/RETURN_EVENTS)


  BASE69 = WIDGET_BASE(TLB, $
      COLUMN=4, $
      SPACE=10, $
      XPAD=10, $
      YPAD=10, $
      MAP=1, $
      TITLE='Base3', $
      UVALUE='BASE69')

  LABEL103 = WIDGET_LABEL( BASE69, $
      UVALUE='Lab2', $
      VALUE='   ',XSIZE=100)

  LABEL104 = WIDGET_LABEL( BASE69, $
      UVALUE='Lab3', $
      VALUE='Input Position',XSIZE=100)

  LABEL105 = WIDGET_LABEL( BASE69, $
      UVALUE='Lab4', $
      VALUE='   ',XSIZE=100)

  LABEL96 = WIDGET_LABEL( BASE69, $
      UVALUE='Lab5', $
      VALUE='Output Position',XSIZE=100)


  BASE76 = WIDGET_BASE(TLB, $
      COLUMN=4, $
      XPAD=10, $
      MAP=1, $
      TITLE='Base4', $
      UVALUE='BASE76')

  FieldVal345 = [ $
    '     ' ]
  RATXT=WIDGET_LABEL(BASE76,VALUE='RA or Longitude')

  RA1 = WIDGET_TEXT(BASE76,VALUE=FieldVal345, $
      UVALUE='Fd3', $
      XSIZE=15, /EDITABLE)

  FieldVal347 = [ $
    '   ' ]
  RATXT=WIDGET_LABEL(BASE76,VALUE='   ')

  RA2 = WIDGET_TEXT(BASE76,VALUE=FieldVal345, $
      UVALUE='Fd4', $
      XSIZE=15)

  BASE113 = WIDGET_BASE(TLB, $
      COLUMN=4, $
      XPAD=10, $
      MAP=1, $
      TITLE='Base5', $
      UVALUE='BASE113')

  FieldVal2037 = [ $
    '     ' ]
  DETXT=WIDGET_LABEL(BASE113,VALUE='DEC or Latitude')

  DEC1 = WIDGET_TEXT(BASE113,VALUE=FieldVal2037, $
      UVALUE='Fd5', $
      XSIZE=15, /EDITABLE)

  FieldVal2039 = [ $
    '   ' ]

  DETXT=WIDGET_LABEL(BASE113,VALUE='   ')

  DEC2 = WIDGET_TEXT(BASE113,VALUE=FieldVal2039, $
      UVALUE='Fd6', $
      XSIZE=15)


  BASE106 = WIDGET_BASE(TLB, $
      COLUMN=1, $
      SPACE=10, $
      XPAD=20, $
      MAP=1, $
      TITLE='Base6', $
      UVALUE='BASE106', $
      XSIZE=500)

  Btns1872 = [ $
    '   RUN   ', $
    '   HELP   ', $
    ' SAVE-DATA ', $
    '   QUIT   ' ]
  BGROUP107 = CW_BGROUP( BASE106, Btns1872, $
      ROW=1, $
      SPACE=40, $
      UVALUE='BTN2', $
      XSIZE=500)


WIDGET_CONTROL, TLB, /REALIZE
INFO={INPUT1:INPUT1,INPUT2:INPUT2,RA1:RA1,DEC1:DEC1,RA2:RA2,DEC2:DEC2}
WIDGET_CONTROL, TLB, SET_UVALUE=INFO

XMANAGER, 'XASTRO', TLB
END ; ******************************************************************
