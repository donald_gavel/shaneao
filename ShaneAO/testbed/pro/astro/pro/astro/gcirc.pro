      PRO gcirc,u,ra1,dc1,ra2,dc2,dis                         
;+
; NAME:
;	GCIRC
; PURPOSE:
;	Computes rigorous great circle arc distances.
;
; CALLING SEQUENCE:
;	GCIRC, U, RA1, DC1, RA2, DC2, DIS
;
; INPUTS:
;	U    -- Describes units of inputs and output:
;		0:  everything radians
;		1:  RAx in decimal hours, DCx in decimal
;			degrees, DIS in arc seconds 
;	RA1  -- Right ascension of point 1
;	DC1  -- Declination of point 1
;	RA2  -- Right ascension of point 2
;	DC2  -- Declination of point 2
;
;   OUTPUTS:
;	DIS  -- Angular distance on the sky between points 1 and 2
;		See U above for units;  double scalar  
;
;   PROCEDURE:
;	Ordinary spherical trig formula.
;
;   NOTES:
;	Either RA1,DC1 or RA2,DC2 (but not both coordinates) can be vectors.
;	In this case DIS is a vector giving the angular distance between
;	each of the vector coordinates to the scalar coordinate
;
;   MODIFICATION HISTORY:
;	Written in Fortran by R. Hill -- SASC Technologies -- January 3, 1986
;	Translated from FORTRAN to IDL, RSH, STX, 2/6/87
;	Vector arguments allowed    W. Landsman    April 1989
;-
      On_error,2                            ;Return to caller
      IF n_params() ne 6 THEN BEGIN
         print,'Calling sequence:  GCIRC,U,RA1,DC1,RA2,DC2,DIS'
         print,'   U = 0  ==> Everything in radians'
         print, $
         '   U = 1  ==> RAx decimal hours, DCx decimal degrees, DIS arc sec'
         RETURN
      ENDIF
      scalar = (not isarray(ra1) ) and (not isarray(ra2) )
      IF scalar THEN BEGIN
      IF (ra1 eq ra2) and (dc1 eq dc2) THEN BEGIN
         dis = 0.0d0
         RETURN
      ENDIF
      ENDIF
      d2r    = !DPI/180.0d0
      as2r   = !DPI/648000.0d0                                        
      h2r    = !DPI/12.0d0
      CASE u OF
         0:  BEGIN                    
                rarad1 = ra1
                rarad2 = ra2
                dcrad1 = dc1
                dcrad2 = dc2
             END
         1:  BEGIN                    
                rarad1 = ra1*h2r
                rarad2 = ra2*h2r
                dcrad1 = dc1*d2r
                dcrad2 = dc2*d2r
             END
         ELSE:  MESSAGE, $
                      'U must be 0 for radians or 1 for hours, degrees, arcsec'
      ENDCASE
;
      radif  = abs(rarad2-rarad1)
      pi_mod = where(radif gt !DPI, npi)         ;Make sure between 0 and 2*!PI
      if npi gt 0 then radif(pi_mod) = 2.0*!DPI - radif(pi_mod)
;
      cosdis = sin(dcrad1)*sin(dcrad2) + cos(dcrad1)*cos(dcrad2)*cos(radif)
      dis    = acos(cosdis<1.0d0>(-1.0d0))
      IF (u ne 0) THEN dis = dis/as2r  
      RETURN
      END                   
