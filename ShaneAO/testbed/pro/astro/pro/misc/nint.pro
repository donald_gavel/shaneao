function nint, x, LONG = long             ;Nearest Integer Function
;+
; NAME:
;	NINT
; PURPOSE:
;	Nearest integer function.   Similar to the intrinsic ROUND function
;	introduced in IDL V3.1, but the default type returned by NINT is
;	INTEGER rather than LONG
;
; CALLING SEQUENCE:
;	result = nint( x, [ /LONG] )
;
; INPUT:
;	X - An IDL variable, scalar or vector, usually floating or double
;		Unless the LONG keyword is set, X must be between -32767.5 and 
;		32767.5 to avoid integer overflow
;
; OUTPUT
;	RESULT - Nearest integer to X
;
; OPTIONAL KEYWORD INPUT:
;	LONG - If this keyword is set and non-zero, then the result of NINT
;		is of type LONG.   Otherwise, the result is of type INTEGER
;               If the /LONG keyword is set, then NINT is identical to the 
;		intrinsic ROUND function introduced in IDL V3.1
; EXAMPLE:
;	If X = [-0.9,-0.1,0.1,0.9] then NINT(X) = [-1,0,0,1]
;
; REVISION HISTORY:
;	Written W. Landsman        January 1989
;	Added LONG keyword         November 1991
;	Use ROUND if since V3.1.0  June 1993
;	Always start with ROUND function    April 1995
;-
  if keyword_set( LONG ) then return, round(x) 

  return,  fix( round(x) )

  end
