	FUNCTION FIND_WITH_DEF, FILENAME, PATHS, EXTENSIONS,	$
		NOCURRENT=NOCURRENT
;+
; NAME:
;	FIND_WITH_DEF()
; PURPOSE: 
;	Searches for files with a default path and extension.
;	Finds files using default paths and extensions, similar to using the
;	DEFAULT keyword with the OPEN statement in VMS.  Using this routine
;	together with environment variables allows an OS-independent approach
;	to finding files.
;
; CALLING SEQUNCE: 
;	Result = FIND_WITH_DEF( FILENAME, PATHS  [, EXTENSIONS, /NOCURRENT ] )
;
; INPUTS: 
;	FILENAME   = Name of file to be searched for.  It may either be a
;		     complete filename, or the path or extension could be left
;		     off, in which case the routine will attempt to find the
;		     file using the default paths and extensions.
;
;	PATHS	   = One or more default paths to use in the search in case
;		     FILENAME does not contain a path itself.  The individual
;		     paths are separated by commas, although in UNIX, colons
;		     can also be used.  In other words, PATHS has the same
;		     format as !PATH, except that commas can be used as a
;		     separator regardless of operating system.  The current
;		     directory is always searched first, unless the keyword
;		     NOCURRENT is set.
;
;		     A leading $ can be used in any path to signal that what
;		     follows is an environmental variable, but the $ is not
;		     necessary.  (In VMS the $ can either be part of the path,
;		     or can signal logical names for compatibility with Unix.)
;		     Environmental variables can themselves contain multiple
;		     paths.
;
; OPTIONAL INPUT: 
;	EXTENSIONS = One or more extensions to append to end of filename if the
;		     filename does not contain one (e.g. ".dat").  The period
;		     is optional.  Multiple extensions can be separated by
;		     commas or colons.
; OUTPUT:
;	The result of the function is the name of the file if successful, or
;	the null string if unsuccessful.
;
; OPTIONAL INPUT KEYWORDS : 
;	NOCURRENT = If set, then the current directory is not searched.
;
; EXAMPLE:
;	FILENAME = ''
;	READ, 'File to open: ', FILENAME
;	FILE = FIND_WITH_DEF( FILENAME, 'SERTS_DATA', '.fix' )
;	IF FILE NE '' THEN ...
;
; CALLS: 
;	STR_SEP, BREAK_PATH
; CATEGORY: 
;	Utilities, Operating_system
; REVISION HISTORY:
;	Version 2, William Thompson, GSFC, 3 November 1994
;	Modified documentation  W. Landsman  November 1994
;-
;
	ON_ERROR, 2
;
;  Check the number of parameters:
;
	IF N_PARAMS() LT 2 THEN MESSAGE, /NONAME, 'Syntax - Result = ' +  $
		'FIND_WITH_DEF(FILENAME, PATHS [, EXTENSIONS])'
;
;  Reformat PATHS into an array.  The first element is the null string.
;
	PATH = BREAK_PATH(PATHS)
;
;  If NOCURRENT was set, then remove the first (blank) entry from the PATH
;  array.
;
	IF KEYWORD_SET(NOCURRENT) THEN PATH = PATH(1:*)
;
;  Reformat EXTENSIONS into an array.  The first element is the null string.
;
	EXT = ''
	IF N_PARAMS() EQ 3 THEN BEGIN
		TEMP = STR_SEP(EXTENSIONS,',')
		FOR I = 0,N_ELEMENTS(TEMP)-1 DO		$
			EXT = [EXT, STR_SEP(TEMP(I),':')]
	ENDIF
;
;  Make sure that the extensions begin with a period.
;
	FOR I = 0,N_ELEMENTS(EXT)-1 DO IF EXT(I) NE '' THEN	$
		IF STRMID(EXT(I),0,1) NE '.' THEN EXT(I) = '.' + EXT(I)
;
;  Set up variables used by the loops below.
;
	I_PATH = -1
	I_EXT = -1
	GET_LUN, UNIT
;
;  Step through each of the paths.
;
NEXT_PATH:
	I_PATH = I_PATH + 1
	IF I_PATH GE N_ELEMENTS(PATH) THEN GOTO, NO_FILE
	PTH = PATH(I_PATH)
;
;  Step through each of the extensions.
;
NEXT_EXT:
	I_EXT = I_EXT + 1
	IF I_EXT GE N_ELEMENTS(EXT) THEN BEGIN
		I_EXT = -1
		GOTO, NEXT_PATH
	ENDIF
	EX = EXT(I_EXT)
;
;  Try to open the file.  Note:  ON_IOERROR and OPENR are used instead of
;  FINDFILE because it is much faster.
;
	ON_IOERROR, NEXT_EXT
	FILE = PTH + FILENAME + EX
	OPENR, UNIT, FILE
	!ERR = 0
	GOTO, DONE
;
;  If we reach this point, then no file has been found.
;
NO_FILE:
	FILE = ''
;
;  Otherwise, we jump directly to here when we find a file.
;
DONE:
	FREE_LUN, UNIT
	RETURN, FILE
	END
