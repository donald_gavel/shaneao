function nulltrim,st
;+
; NAME:
;	NULLTRIM
; PURPOSE:
;	Function to trim a string of all characters after and including the
;	first null character (ascii 0)
;
; CALLING SEQUENCE:
;	result = nulltrim( st )
;
; INPUTS:
;	st = input string
; OUTPUTS:
;	trimmed string returned as the function value.
; HISTORY:
;	D. Lindler  July, 1987
;-
;--------------------------------------------------------------------
;
 b = byte(st)
 null = where( b eq 0, nfound )
 if nfound lt 1 then return, st else return, strmid( st,0,null(0) )
 end
