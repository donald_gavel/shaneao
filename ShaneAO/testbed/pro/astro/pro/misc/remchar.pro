pro remchar,st,char	;Remove character
;+
; NAME:
;	REMCHAR
; PURPOSE:
;	Remove all appearances of character (char) from string (st)
;
; CALLING SEQUENCE:
;	REMCHAR, ST, CHAR
;
; INPUTS:
;	ST  - String from which character will be removed.  
;	CHAR- Character to be removed from string. 
;
; EXAMPLE:
;	If a = 'a,b,c,d,e,f,g' then 
;
;	IDL> remchar,a, ','
;
;      will give a = 'abcdefg'
;
; REVISIONS HISTORY
;	Written D. Lindler October 1986
;	Test if empty string needs to be returned   W. Landsman  Feb 1991
;-
 bst = byte(st)                                 ;Convert string to byte

 bchar = byte(char) & bchar = bchar(0)          ;Convert character to byte

 good = where( bst NE bchar, Ngood)
 if Ngood GT 0 then st = string(bst(good)) else st = ''

 return
 end
