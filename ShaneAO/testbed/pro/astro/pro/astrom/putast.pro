 pro putast, hdr, astr, crpix, crval, crtype, EQUINOX=equinox, $
				CD_TYPE = cd_type
;+
; NAME:
;	PUTAST
; PURPOSE:
;	Put astrometry parameters into a given FITS header.
;
; CALLING SEQUENCE:
;	putast, hdr, astr, [EQUINOX =, CD_TYPE = ]
;	     or
;	putast, hdr, cd,[ crpix, crval], [ EQUINOX =, CD_TYPE = ]
;
; INPUTS:
;	HDR -  FITS header, string array.   HDR will be updated to contain
;		the supplied astrometry.
;	ASTR - IDL structure containing values of the astrometry parameters
;	       CDELT, CRPIX, CRVAL, CTYPE, LONGPOLE, PROJP1, and PROJP2
;		See EXTAST.PRO for more info about the structure definition
;			or
;	CD   - 2 x 2 array containing the astrometry parameters CD1_1 CD1_2
;								CD2_1 CD2_2
;		in units of DEGREES/PIXEL
;	CRPIX - 2 element vector giving X and Y coord of reference pixel
;		BE SURE THE COORDINATES IN CRPIX ARE GIVEN IN FORTRAN STANDARD
;		(E.G. FIRST PIXEL IN IMAGE IS (1,1) )
;	CRVAL - 2 element vector giving R.A. and DEC of reference pixel 
;		in degrees
;
; OUTPUTS:
;	HDR - FITS header now contains the updated astrometry parameters
;		A brief HISTORY record is also added.
;
; OPTIONAL KEYWORD INPUTS:
;	EQUINOX - scalar giving equinox (e.g. 1950) of the reference 
;		coordinates 
;
;	CD_TYPE - Scalar, either 0, 1 , or 2 specifying how the CD matrix
;		is to be written into the header
;		(0) as CD00n00m values 
;		(1) convert to rotation and write as a CROTA2 value
;		(2) as CDn_m value
;		Method 0 is the preferred use suggested
;		in the document "Representations of Celestial Coordinates in
;		FITS" by Griesen and Calabretta.   If CD_TYPE is not supplied
;		PUTAST will try to determined the type of astrometry already
;		in the header.   If there is no astrometry in the header then
;		the default is CD_TYPE = 0
; NOTES:
;	The recommended use of this procedure is to supply an astrometry
;	structure.    
;
; PROMPTS:
;	If only a header is supplied, the user will be prompted for a plate 
;	scale, the X and Y coordinates of a reference pixel, the RA and
;	DEC of the reference pixel, the equinox of the RA and Dec and a 
;	rotation angle.
;
; PROCEDURES USED:
;	ZPARCHECK, DATATYPE(), SXADDPAR, GETOPT()
; REVISION HISTORY
;	Written by W. Landsman 9-3-87
;	Major rewrite, use new astrometry structure   March, 1994
;	Use both CD and CDELT to get plate scale for CD_TYPE=1   September 1995
;-
 npar = N_params()

 if ( npar EQ 0 ) then begin	;Was header supplied?
	print,'Syntax: PUTAST, astr, [ EQUINOX = , CD_TYPE = ]
	print,'       or
	print,'Syntax: PUTAST, Hdr, [ cd, crpix, crval, EQUINOX = , CD_TYPE =]   
	return
 end

 zparcheck, 'PUTAST', hdr, 1, 7, 1, 'FITS image header'

 if ( npar EQ 1 ) then begin            ;Prompt for astrometry parameters?
   read,'Enter plate scale in arc seconds/pixel: ',cdelt
   inp =''
   print,'Reference pixel position should be in FORTRAN convention'
   print,'(First pixel has coordinate (1,1) )'
   GETCRPIX: print, $
  'Enter X and Y position of a reference pixel ([RETURN] for plate center)'
   read, inp
   if ( inp EQ '' ) then $ 
          crpix = [ sxpar(hdr,'NAXIS1'), sxpar(hdr,'NAXIS2')] / 2. $
     else crpix = getopt( inp, 'F')

   if N_elements( crpix ) NE 2 then begin
      print,string(7B),'PUTAST: INVALID INPUT - Enter 2 scalar values'
      goto, GETCRPIX     
   endif

   inp = ''
   GETRA:  read, 'Enter RA of plate center in HR, MIN, SEC: ',inp
   ra = getopt( inp, 'F' )

   if ( N_elements(ra) NE 3 ) then begin
      print,string(7B),'PUTAST: INVALID INPUT - Enter 3 scalar values'
      goto, GETRA 
   endif

   GETDEC:  read,'Enter DEC of reference pixel in DEG, MIN, SEC: ',inp
   dec = getopt( inp, 'F')
   if ( N_elements(dec) NE 3 ) then begin
      print,string(7B),'PUTAST: INVALID INPUT - Enter 3 scalar values'
      goto, GETDEC
   endif
   crval = [ ten(ra*15.), ten(dec)]
   inp = ''
   read,'Enter rotation angle in degrees, clockwise, from north [0.]: ',inp
   rotat = getopt(inp,'F')/!RADEG
   cd = (cdelt / 3600.)*[ [-cos(rotat),-sin(rotat)], [-sin(rotat), cos(rotat)] ]
   npar = 4
 endif else begin

   if datatype(astr) EQ 'STC' then begin       ;User supplied astrometry structure
	cd = astr.cd
	cdelt = astr.cdelt
	crval = astr.crval
	crpix = astr.crpix
	ctype = astr.ctype
   endif else  begin
	cd = astr
	zparcheck,'PUTAST', cd, 2, [4,5], 2, 'CD matrix'
   endelse
 endelse

;   Add CTYPE to FITS header

 if N_elements( ctype ) GE 2 then begin

 sxaddpar,hdr,'CTYPE1',ctype(0),'COORDINATE TYPE','HISTORY'
 sxaddpar,hdr,'CTYPE2',ctype(1),'COORDINATE TYPE','HISTORY'

 endif

;   Add EQUINOX keyword and value to FITS header

 if N_elements( equinox ) EQ 0 then begin
    equinox = sxpar( hdr, 'EQUINOX')            ;Is EQUINOX already in header?
    if !ERR eq -1 then begin
      read,'Enter equinox for reference pixel coordinates (e.g. 1950): ',equinox
    if (equinox LT 1850.) or (equinox GT 2100.) then message, $
     'Equinox value of '+ strtrim(equinox,2) + ' not added to header', /INFORM $
   else sxaddpar, hdr, 'EQUINOX', equinox, 'EQUINOX OF REF. COORD.', 'HISTORY'
   endif 

 endif else $
     sxaddpar,hdr, 'EQUINOX', equinox, 'EQUINOX OF REF. COORD.', 'HISTORY'

; Add coordinate description (CD) matrix to FITS header
; CD_TYPE = 0: CD001001 + CDELT   1. CROTA + CDELT     2: CD1_1 
  
 
 if not keyword_set(cd_type) then begin
 cd_type = 0
 cd1_1 = sxpar( hdr, 'CD001001', COUNT = N_CD)
 if N_CD EQ 0 then begin
	cd1_1 = sxpar( hdr, 'CD1_1', COUNT = N_CD)
        if N_CD GE 1 then CD_TYPE = 2 else begin 
		CDELT1 = sxpar( hdr,'CDELT1', COUNT = N_CDELT1)
		if N_CDELT1 GE 1 then cd_type = 1
	endelse
 endif
 endif
 if cd_type EQ 0 then begin
    sxaddpar, hdr, 'CD001001', cd(0,0), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD002001', cd(1,0), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD001002', cd(0,1), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD002002', cd(1,1), 'DEGREES/PIXEL', 'HISTORY'

 	if N_elements(CDELT) GE 2 then begin
		sxaddpar, hdr, 'CDELT1', cdelt(0)
		sxaddpar, hdr, 'CDELT2', cdelt(1)
	endif
 endif else if cd_type EQ 2 then begin

    sxaddpar, hdr, 'CD1_1', cd(0,0), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD2_1', cd(1,0), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD1_2', cd(0,1), 'DEGREES/PIXEL', 'HISTORY'
    sxaddpar, hdr, 'CD2_2', cd(1,1), 'DEGREES/PIXEL', 'HISTORY'

 endif else begin

	rot = atan( cd(0,1), cd(1,1) )	
	sxaddpar, hdr, 'CROTA2', rot, 'ROTATION ANGLE (DEGREES)'
 	if N_elements(CDELT) GE 2 then begin
		sxaddpar, hdr, 'CDELT1', cdelt(0)*sqrt(cd(0,0)^2 + cd(0,1)^2)
		sxaddpar, hdr, 'CDELT2', cdelt(1)*sqrt(cd(1,0)^2 + cd(1,1)^2)
	endif

 endelse

 hist = ' CD MATRIX WRITTEN'

; Add CRPIX keyword to FITS header

 if N_elements( crpix ) GE 2  then begin		;Add CRPIX vector?

	zparcheck, 'PUTAST', crpix, 3, [1,2,4,3,5], 1, 'CRPIX vector'

        sxaddpar, hdr, 'CRPIX1',crpix(0), 'REFERENCE PIXEL IN X','HISTORY'
        sxaddpar, hdr, 'CRPIX2',crpix(1), 'REFERENCE PIXEL IN Y','HISTORY'

	hist = ' CD AND CRPIX PARAMETERS WRITTEN'
 endif

;  Add CRVAL keyword and values to FITS header.   Convert CRVAL to double
;  precision to ensure enough significant figures

 if N_elements( crval ) GE 2 then begin		

	zparcheck, 'PUTAST', crval, 3, [2,4,3,5], 1, 'CRVAL vector'
	sxaddpar, hdr, 'CRVAL1', double(crval(0)), 'R.A. (DEGREES)', 'HISTORY'
	sxaddpar, hdr, 'CRVAL2', double(crval(1)), 'DEC  (DEGREES)', 'HISTORY'
	hist = ' CD, CRPIX AND CRVAL PARAMETERS WRITTEN'

  endif
 
 sxaddhist,'PUTAST:' + strmid(!STIME,0,17) + hist,hdr

 return
 end
