pro fixdaotab,hdr
;+
; NAME:
;	FIXDAOTAB
; PURPOSE:
;	Prior to March 1995, the IDL photometry procedures that created 
;	ASCII FITS tables would include blanks in the some of the column
;	names (e.g. "STAR ID","AP1 MAG").   This practice, although allowed
;	by the rules of FITS, is now strongly deprecated.    FIXDAOTAB
;	will replace the blanks in TTYPE* values with underscores.
;
; CALLING SEQUENCE:
;	FIXDAOTAB, hdr        
;
; INPUT-OUTPUT
;	hdr - FITS ASCII or Binary table table header.   Upon output any
;		blanks will be replaced by underscores in the TTYPE* names.
;
; PROCEDURES CALLED:
;	zparcheck, repchr, sxpar, sxaddpar
; REVISION HISTORY
;    Written    W. Landsman              Mar 1995
;-
 On_error,2

 if N_params() EQ 0 then begin
	print,'Syntax - FIXDAOTAB, hdr
	return
 endif

 zparcheck, 'FIXDAOTAB', hdr, 1, 7, 1, 'FITS table header' 

  ttype = sxpar(hdr,'TTYPE*',Count = n)
  if N EQ 0 then begin
	message,'FITS header not modified',/INF
	return
  endif		

  ttype = strtrim(ttype,2)
  ttype1 = repchr(ttype,' ','_')

  for i=0,n-1 do begin
	if ttype(i) NE ttype1(i) then $
		sxaddpar,hdr,'TTYPE' + strtrim(i+1,2),ttype1(i)
  endfor
 return
 end

