pro rdpsf,psf,hpsf,psfname
;+
; NAME:
;	RDPSF
; PURPOSE:
;	Read the STSDAS file created by GETPSF in the DAOPHOT sequence; combine 
;	the gaussian with the residuals to create an output PSF array.
;
; CALLING SEQUENCE:
;	RDPSF, PSF, HPSF, [ PSFNAME]
;
; OPTIONAL INPUTS
;	psfname - string giving the name of the SDAS file containing the PSF
;		residuals
;
; OUTPUTS
;	psf - array containing the actual PSF
;	hpsf - header associated with psf
;
; PROCEDURES CALLED:
;	DAO_VALUE, MAKE_2D, SXADDPAR, SXOPEN, SXPAR(), SXREAD()
; REVISION HISTORY:
;	Written W. Landsman              December, 1988
;	Checked for IDL Version 2, J. Isensee & J. Hill, December, 1990
;-
 On_error,2

 if N_params() LT 2 then begin
    print,'Syntax - PSFRD, PSF, HPSF, [ PSFNAME ]
    print,'    PSF,HPSF - are the output PSF array and header
    print,'    PSFNAME - the name of the file containing the PSF, input'
   return
 endif

 if N_params() EQ 2 then begin
    psfname = ''
    read,'Enter name of the SDAS file containing the PSF: ',psfname
 endif

 sxopen,1,psfname,hpsf         ;Open the PSF file and read header
 resid = sxread(1)             ;Read in array of residuals 
 close,1                       ;Close file
 gauss = sxpar(hpsf,'GAUSS*')  ;Get gaussian parameters (5)
 psfrad = sxpar(hpsf,'PSFRAD') ;Get PSF radius
 npsf = 2*psfrad+1             ;Width of output array containing PSF
 psf = fltarr(npsf,npsf)       ;Create output array
 dx = indgen(npsf) - psfrad    ;Vector gives X distance from center of array
 dy = dx                       ;Ditto for dy
 make_2d,dx,dy                 ;Now have X and Y values for each pixel in
;                              the output array   

 psf = psf + dao_value(dx,dy,gauss,resid) ;Compute DAOPHOT value at each point

 sxaddpar,hpsf,'NAXIS1',npsf   ;Update header to contain PSF size
 sxaddpar,hpsf,'NAXIS2',npsf   ;rather than residual array size

 return
 end
