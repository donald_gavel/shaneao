PRO medarr, inarr, outarr
;+
; NAME:
;	MEDARR
; PURPOSE:
;	To combine a series of arrays into a single array by filling each
;	pixel in the output array with the median of the corresponding pixels
;	in the input arrays.   Useful, for example to combine a stack of CCD
;       images, while removing cosmic ray hits.
 ; CALLING SEQUENCE:
;	MEDARR, inarr, outarr
; INPUTS:
;	inarr  -- A three dimensional array containing the input arrays to 
;	          combine together.  Each of the input arrays must be two 
;	          dimensional and must have the same dimensions.  These arrays
;	          should then be stacked together into a single 3-D array,
;	          creating INARR.
; OUTPUTS:
;	outarr -- The output array.  It will have dimensions equal to the
;	          first two dimensions of the input array.
; SIDE EFFECTS:
;	OUTARR is created.
; RESTRICTIONS:
;	INARR MUST be three dimensional.  Further, it may be of any type except
;	COMPLEX, STRING, or STRUCTURE.
;
; PROCEDURE:
;	A scalar median function over the third dimension is looped over 
;       each pixel of the first two dimensions.  
;
;       MEDARR is also available as a C procedure linked to IDL via
;       CALL_EXTERNAL.   The callable C version is 2-3 times faster for large 
;	(~ 500 x 500 x 7) images.   Contact W. Landsman 
;	(landsman@stars.gsfc.nasa.gov) for the C program
; MODIFICATION HISTORY:
;	Written by Michael R. Greason, STX, 12 June 1990.
;-
 On_error,2
;			Check parameters.

 if N_params() LT 2 then begin			; # parameters.
	print, "Syntax -  MEDARR, inputarr, outputarr"
	return
 endif
 
 s = size(inarr)
 if s(0) NE 3 then $			; Input array size.
	message, "Input array must have 3 dimensions"

;			Create the output array.
 ncol = s(1)
 nrow = s(2)
 narr = s(3)
 type = s(s(0) + 1)
 outarr = make_array( dimen = [ncol,nrow], /NOZERO, TYPE = type )

;			Combine the input arrays into the output array.

 for j = 0, (nrow-1) do begin    
       	for i = 0, (ncol-1) do begin   
		marr = inarr(i,j,*)
		outarr(i,j) = median(marr)       ;Scalar median over 3rd dim.
	endfor
 endfor

 return
 end
