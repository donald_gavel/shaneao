;
; Auto Save File For ./hartmannwfgui.pro
;
;  Fri Sep 26 14:16:08 PDT 1997
;



; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN HEADER




; DO NOT REMOVE THIS COMMENT: END HEADER
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.


; CODE MODIFICATIONS MADE ABOVE THIS COMMENT WILL BE LOST.
; DO NOT REMOVE THIS COMMENT: BEGIN MAIN13




;PRO MAIN13_Event, Event
PRO hartmannwfgui_Event, Event

  WIDGET_CONTROL,Event.Id,GET_UVALUE=Ev
  zoomval = 0.25

  t = size(Ev)
  if (t(0) eq 0) then evname = Ev else evname = Ev.name
;  CASE Ev OF 
  CASE evname OF 

  'DRAW3': BEGIN
      Print, 'Event for Combined Wavefront'
      widget_control, event.id, get_value = index
      wset, index
      tvdevice, index
      widget_control, event.id, get_uvalue = uv
      if (event.press and 1b) then begin
        print, 'Units are in WAVES'
        tvvalue, uv.data
      endif
      END
  'BUTTON6': BEGIN
      print,'-----------------------------------------------------------------'
      Print, '             Zernike Analysis'
      win = $
        widget_info(/sibling, $ ; 
        widget_info(/child, $ ; 
          event.top))
      widget_control, win, get_uvalue = uv
      wf = uv.data
      zset = [[2,0,0],[2,2,0],[2,2,1], $
              [3,1,0],[3,1,1],[3,3,0],[3,3,1], $
              [4,0,0]]
      znameset = ['focus(2,0)','astig(2,2)','astig45(2,2)', $
                  'coma(3,1)','coma45(3,1)','coma(3,3)', 'coma45(3,3)', $
                  'sphere(4,0)']
      zcount = 8
      mask = wf ne 0.0
      sv =  centroid(mask) - [25,25]
      rms_ = fltarr(zcount)
      cset = fltarr(zcount)
      print,'component        waves rms'
      wf1 = wf
      wf2 = wf
      for i=0,zcount-1 do begin
        n = zset(0,i)
        m = zset(1,i)
        z = zernike(51,51,30,n,m) ; 30 is the circle of interest radius
        if (zset(2,i) eq 0) then z = real(z) else z = imaginary(z)
;        nf = sqrt(total(z*z*circle(51,51,25,25,30,1.)))
        z = shift(z,sv(0),sv(1))
        nf = sqrt(total(z*z*mask))
        z = z/nf
        c = total(z*wf)
        wf0 = c*z*mask
        if (i lt 1) then wf1 = wf1 - wf0
        if (i lt 2) then wf2 = wf2 - wf0
        rms_(i) = sqrt( total(wf0*wf0)/total(mask) )
        cset(i) = c
        print, znameset(i), ': ', rms_(i)
      endfor
      rms0 = sqrt( total(wf*wf)/total(mask) )
      rmsz = sqrt( total(rms_*rms_) )
      rms1 = sqrt( total(mask*wf1*wf1)/total(mask) )
      rms2 = sqrt( total(mask*wf2*wf2)/total(mask) )
      lbl = widget_info(/child,event.top)
      widget_control, lbl, get_value = name
      print,'-----------------------------------------------------------------'
      print,name + ' total: ',rms0
      print,name + ' total in first 8 zernikes: ', rmsz
      print,name + ' total after focus removed: ',rms1
      print,name + ' total after focus & astigmatism removed: ',rms2
      print,'-----------------------------------------------------------------'
      END
  'BUTTON4': BEGIN
      Print, 'Event for Save Wavefront'
      win = $
        widget_info(/sibling, $
        widget_info(/child, $ ; 
          Event.Top))
      widget_control, win, get_uvalue = uv
      path = '~/guidestar/lickLaser/data'
      filename = pickfile(path=!filepath,file=!filepath,/write)
      if (filename eq '') then res = 'No' else res = 'Yes'
      if ((res eq 'Yes') and fileexists(filename)) then $
        res = widget_message(['overwrite existing file',filename + '?'], $
                             /question)
      if (res eq 'Yes') then begin
        print,'writing combined phasedata'
        writefits,filename,uv.data
      endif else begin
        print,'canceling write'
      endelse
      END
  'BUTTON5': BEGIN
      Print, 'Event for Done'
      widget_control, event.top, /destroy
      END
  ENDCASE
END


; DO NOT REMOVE THIS COMMENT: END MAIN13
; CODE MODIFICATIONS MADE BELOW THIS COMMENT WILL BE LOST.



PRO hartmannwfgui, wf, GROUP=Group, name=name, help=helpq

  if keyword_set(helpq) then begin
    print,'usage: hartmannwfgui,wf,name=name,/help
    return
  endif  

  IF N_ELEMENTS(Group) EQ 0 THEN GROUP=0

  junk   = { CW_PDMENU_S, flags:0, name:'' }
  zoomval = 0.25
  if (n_elements(name) eq 0) then name = 'wavefront analysis'

  MAIN13 = WIDGET_BASE(GROUP_LEADER=Group, $
      COLUMN=1, $
      MAP=1, $
      TITLE='wavefront analysis', $
      UVALUE='MAIN13')

  LABEL2 = WIDGET_LABEL( MAIN13, $
      UVALUE='LABEL2', $
      FONT='-adobe-times-medium-i-normal--18-180-75-75-p-94-iso8859-1', $
      VALUE=name)

  DRAW3 = WIDGET_DRAW( MAIN13, $
      BUTTON_EVENTS=1, $
      RETAIN=1, $
      UVALUE={name:'DRAW3',data:wf}, $
      XSIZE = 51/zoomval, $
      YSIZE = 51/zoomval)

  BUTTON6 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON6', $
      VALUE='Zernike Analysis')

  BUTTON4 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON4', $
      VALUE='Save Wavefront')

  BUTTON5 = WIDGET_BUTTON( MAIN13, $
      UVALUE='BUTTON5', $
      VALUE='Done')

  WIDGET_CONTROL, MAIN13, /REALIZE

  ; Get drawable window index

  COMMON DRAW3_Comm, DRAW3_Id
  WIDGET_CONTROL, DRAW3, GET_VALUE=DRAW3_Id

  widget_control, draw3, get_uvalue=uv
  wset,DRAW3_Id
  tvdevice,DRAW3_Id
  exptv, uv.data, /data, /nobox, /noexact

;  XMANAGER, 'MAIN13', MAIN13, /modal
;  XMANAGER, 'hartmannwfgui', MAIN13, /modal
;  XMANAGER, 'hartmannwfgui', MAIN13, /no_block
  XMANAGER, 'hartmannwfgui', MAIN13
END
