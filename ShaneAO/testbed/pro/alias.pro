;+ 
;  Alias.pro
;    Alias a power spectrum
;-
function alias,S0,ns
  n = (size(S0))[1]
  r = rectangle(n,n,n/2,n/2,ns,ns,1.)
  
  rs = shift(r,[-ns,-ns])
  S = shift(reverse(reverse(S0*rs,1),2),[-ns,-ns])
  rs = shift(r,[-ns,0])
  S += shift(reverse(S0*rs,1),[-ns,0])
  rs = shift(r,[-ns,ns])
  S += shift(reverse(reverse(S0*rs,1),2),[-ns,ns])
  
  rs = shift(r,[0,-ns])
  S += shift(reverse(S0*rs,2),[0,-ns])
  rs = shift(r,[0,ns])
  S += shift(reverse(S0*rs,2),[0,ns])
  
  rs = shift(r,[ns,-ns])
  S += shift(reverse(reverse(S0*rs,1),2),[ns,-ns])
  rs = shift(r,[ns,0])
  S += shift(reverse(S0*rs,1),[ns,0])
  rs = shift(r,[ns,ns])
  S += shift(reverse(reverse(S0*rs,1),2),[ns,ns])
  
  return,S+S0
end