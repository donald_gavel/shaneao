function window_valid,w,list=list
  device, window_state=winlist
  if (keyword_set(list)) then return,where(winlist eq 1)
  if (w ge n_elements(winlist)) then return,0
  return,winlist[w]
end
