; shiftnadd.pro - shift and add speckle frames based on
;               brightest speckle
;
pro shiftnadd,incube,outcube,specklesize
  if n_elements(specklesize) eq 0 then specklesize = 1
  na = size(incube)
  n = na(1)
  m = na(2)
  nframes = na(3)
  mid = [ n/2, m/2 ]
  outcube = 0*incube
  for i = 0, nframes-1 do begin
    if specklesize gt 1 then tmps = smooth(incube(*,*,i),specklesize) $
    else tmps = incube(*,*,i)
    center = brightest(tmps)
    outcube(*,*,i) = shift(incube(*,*,i),mid(0)-center(0),mid(1)-center(1))
    print,format='($,I0," ")',i
  endfor
  print,'done'
end
