;+
;  enclose_ellipse.pro - determine the pixel position of an ellipse
;           that encloses a planet image
;
;  USAGE:
;     enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step,/first
;     enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step    ; iteration
;     e = check_ellipse(image,a,b,x0,y0,th)
;     tvscl, image + real(e) ; shows the planet and the ellipse
;  INPUTS:
;      image = 2-D image of a planet
;      t1, t2 = data range which defines the edge of the planet
;              (example: t1 = .095 and t2 = .105 times max value in image)
;      a, b = major and minor radii of planet ellipsoid, in pixels
;      x0, y0 = center of ellipse, first guess, in pixels
;      th = rotation angle of planet (minor axis CCW with respect to vertical)
;              in radians
;      step = step size for iteration (recommend 20 to start)
;  OUTPUTS:
;      x0, y0 = center of ellipse, in pixels
;  OPTIONAL KEYWORD INPUT:
;      first = this is the first iteration, so set up initial values
;-
pro enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step,first=fi
  common ellipseCom, p,x,y,sth,cth,xprime,yprime,eps,gradx0,grady0
  if (n_elements(fi) eq 0) then fi = 0
  n = (size(image))(1)
  if (fi eq 1) then begin
    p = where((image gt t1) and (image lt t2),count)
    if (count eq 0) then begin
      print,'<enclose_ellipse> error: cannot find planet bounds'
      return
    endif
    y = p/n
    x = p - (p/n)*n
    sth = sin(th)
    cth = cos(th)
  endif
  xprime = (x - x0)*cth + (y - y0)*sth
  yprime = -(x - x0)*sth + (y - y0)*cth
  eps = ( 1 - (xprime/a)^2 - (yprime/b)^2 )
  gradx0 = total( eps*( (xprime/a^2)*cth - (yprime/b^2)*sth ) )
  grady0 = total( eps*( (xprime/a^2)*sth + (yprime/b^2)*cth ) )
  x0 = x0 - step*gradx0
  y0 = y0 - step*grady0
  return
end

function draw_ellipse,n,a,b,x0,y0,th
  xx = extend(indgen(n),2,n)
  yy = transpose(xx)
  cth = cos(th)
  sth = sin(th)
  xxprime = (xx-x0)*cth + (yy-y0)*sth
  yyprime = -(xx-x0)*sth + (yy-y0)*cth
  e = (xxprime/a)^2 + (yyprime/b)^2
  return,e
end

function check_ellipse,image,a,b,x0,y0,th
  n = (size(image))(1)
  e = draw_ellipse(n,a,b,x0,y0,th)
  e = (e gt .99) and (e lt 1.01)
  return, e
end

pro center_ellipse_set,data,t1,t2,a,b,x0,y0,th,step,stat
  common ellipseCom
  n = (size(data))(1)
  nframes = (size(data))(3)
  print,'allocating space for result...'
  print,'done'
  stat = fltarr(11,nframes)
  print,'starting frame set...'
  for i = 0,nframes-1 do begin
    image = data(*,*,i)
    image = real(image)/max(image)
    enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step,/first
    for j = 0,4 do begin
      enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step
    endfor
    if (norm([gradx0,grady0]) gt 0.001) then begin
      for j = 0,4 do begin
        enclose_ellipse,image,t1,t2,a,b,x0,y0,th,step
      endfor
    endif
    er = (1 - sqrt((xprime/a)^2 + (yprime/b)^2))*a
    mer = moment(er,mdev=mdev,sdev=sdev)
    print,format='($,I0," ")',i
;    el = draw_ellipse(n,a,b,x0,y0,th)
;    el = (el gt .99) and (el lt 1.01)
;    e = (image gt t1) and (image lt t2)
    stat(*,i) = [x0,y0,mer(0),sdev,mdev,mer(1),mer(2),mer(3),n_elements(er),total(abs(er) gt 1),norm([gradx0,grady0])]
  endfor
  print,"done"
  return
end

common ellipseCom
t1 = 0.095
t2 = 0.105
a = (38.2/.24)/2.
b = a/1.069
x0 = 128
y0 = 128
step = 20
pi = 4.*atan(1.)
degrees = pi/180.
th = 16*degrees
end
; for phantom:
;   t1 = .5 & t2 = .51
;   a = .4*n & b = a
;   th = 0
;   x0 = n/2 & y0 = n/2
