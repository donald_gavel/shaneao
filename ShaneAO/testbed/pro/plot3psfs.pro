;
;   image showing 3 psfs
;
home = '/duck1/gavel/keck/sep96/titan/star342/'
if (n_elements(imspec) eq 0) then begin & $
  imspec = readfits(home+'speckleFrame348.fits') & $
  imave = readfits(home+'average348.fits') & $
  improc = readfits(home+'doubleStar348_image.fits') & $
  imspecn = imspec/total(imspec>0) & $
  imaven = imave/total(imave>0) & $
  improcn = improc/total(improc>0) & $
endif
if (!d.name eq 'X') then begin & $
  window,1,xsize=256*3,ysize=300 & $
  textcolor = 255 & $
endif
if (!d.name eq 'PS') then begin & $
  device,filename = 'psf3images.ps',/color,xsize=25.6,ysize=10,/encapsulated,bits_per_pixel=8 & $
  textcolor = 255 & $
endif
loadct,5
gamma_ct,.7
pixelscale = 0.02
zoomfac = 4
t = fltarr(256*3,300)
t(*,0:255) = [zoomi(shift(imspecn,0,-22),zoomfac),zoomi(shift(imaven,0,-19),zoomfac),zoomi(improcn,zoomfac)/10.]
m = max(t)
has = zoomfac*0.5/pixelscale
t(50,128-has/2:128+has/2) = m
t(50-4:50+4,128-has/2) = m
t(50-4:50+4,128+has/2) = m
t = shift(t,0,20)
tvscl,t
xyouts,.025,.45,'!181/2 arcsec',/normal,color=textcolor
xyouts,.47,.1,'!18long exposure',/normal,color=textcolor
xyouts,.47,.05,  '   (20 s)',/normal,color=textcolor
xyouts,.15,.1, 'short exposure',/normal,color=textcolor
xyouts,.15,.05,'   (200 ms)',/normal,color=textcolor
xyouts,.75,.1,'reconstructed image',/normal,color=textcolor
xyouts,.245,.90,'Speckle reconstruction of SAO 188588 at !4k!18 = 1.6!4l!18, Keck 10m telescope, 9/6/96',/normal,color=textcolor
if (!d.name eq 'PS') then device,/close
