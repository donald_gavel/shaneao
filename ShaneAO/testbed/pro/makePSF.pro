home = '/duck1/gavel/keck/sep96/titan/'
star517 = readfits(home+'star517/star517_image.fits')
npang = 3.3
starave = (rot(reverse(star517), 7.921035468645 + npang) + $
          rot(reverse(star517), -2.573066550406 + npang) + $
          rot(reverse(star517), -6.188985854542 + npang) + $
          rot(reverse(star517), -10.2883 + npang) + $
          rot(reverse(star517), -38.8096 + npang))/5.
