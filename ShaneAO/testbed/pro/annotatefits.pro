;+
;  annotatefits.pro - annotate Jupiter images
;    annotatefits,header
;-
pro annotatefits, hdr
  file = getkeyval(hdr,'DATASET',/string)
  date = getkeyval(hdr,'DATE',/string)
  time = getkeyval(hdr,'UTC',/string)
  filter = getkeyval(hdr,'FILTER',/string)
  cml = fix(getkeyval(hdr,'SELON'))
  y = 10
  disp_txt, strmid(file,2,strpos(file,"'",2)-2),"",y
  y = y + 15
  disp_txt, strmid(date,2,strpos(date,"'",2)-2) + ' ' + strmid(time,2,strpos(time,"'",2)-2),"",y
  y = y + 15
  disp_txt, strmid(filter,2,strpos(filter,"'",2)-2) + ' nm',"",y
  y = y + 15
  disp_txt, 'cml ' + string(cml),"",y
  return
end
