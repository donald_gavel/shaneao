;
;  Handy charts of:
;    Strehl vs rms wf error for various wavelengths
;    Strehl vs rms tip tilt for various wavelengths
;    
;    Written by: Don Gavel
;    Date: 10/1/2009
;
function cstrehl,wavelength,wfe
  return,exp(-((2*!pi/wavelength)*wfe)^2)
end

function cwfe,wavelength,strehl
  return,(wavelength/(2*!pi))*sqrt(-alog(strehl))
end

microns = 1.e-6
nm = 1.e-9
dtele = 10 ; Keck telescope
lambda_set = [0.5, 0.7, 0.9, 1.0, 1.2, 1.6, 2.0]*microns
nl = (size(lambda_set))[1]
n = 100
sigma_range = [10.,500.]*nm
;
delta = (max(sigma_range)-min(sigma_range))/(float(n-1))
sigma = findgen(n)*delta + sigma_range[0]
strehl = fltarr(n,nl)
for k = 0,nl-1 do begin
  lambda = lambda_set[k]
  strehl[*,k] = exp(-((2*!pi/lambda)*sigma)^2)
endfor

clearwins
loadct,5

!p.ticklen = 0.5
!p.background = 255
!p.color = 0
!p.charsize = 2
!p.charthick = 2
!p.thick = 1

window,/free,title = 'Strehl vs rms wfe'
for k = 0,nl-1 do begin
  if (k eq 0) then plot,sigma/nm,strehl[*,k],xtitle='wf error, nm',ytitle='Strehl',title='Strehl vs rms error, Marechal approximation'
  if (k ne 0) then oplot,sigma/nm,strehl[*,k]
  xyouts,sigma[20]/nm,strehl[20,k],string(lambda_set[k]/microns,format="(f4.2)"),charsize=1.8
endfor
tickgrid,[0,500,100,10],[0,1.0,.2,.05],color=[100,150]
xyouts,sigma[20]/nm,.95 ,'wavelength, microns'

arcsec = !pi/(180.*3600.)
mas = arcsec/1000.
sigma_tt_range = [0.1,100.]*mas
sigma_tt = findgen(n)*delta + sigma_tt_range[0]
rms_wfe_vs_tt = sqrt(!pi)*(dtele^2/2)*sigma_tt
strehl_tt = fltarr(n,nl)
for k = 0,nl-1 do begin
  lambda = lambda_set[k]
  strehl_tt[*,k] = (lambda/dtele)^2 / ( (lambda/dtele)^2 + sigma_tt^2 )
endfor

window,/free,title = 'Strehl vs tip/tilt error'
for k = 0,nl-1 do begin
  if (k eq 0) then plot,sigma_tt/mas,strehl_tt[*,k],xtitle='tip/tilt, mas',ytitle='Strehl',title='Strehl vs tip/tilt error'
  if (k ne 0) then oplot,sigma_tt/mas,strehl_tt[*,k]
  xyouts,sigma_tt[20]/mas,strehl_tt[20,k],string(lambda_set[k]/microns,format="(f4.2)"),charsize=1.8
endfor
xyouts,sigma_tt[20]/mas,.9,'wavelength, microns'
xyouts,62,.61,'D = '+string(dtele,format="(f6.2)")+' m',charsize=2.5
tickgrid,[0,120,20,5],[0,1.0,.2,.05],color=[100,150]
end
