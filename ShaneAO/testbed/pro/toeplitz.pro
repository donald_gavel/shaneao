;+
;   Toeplitz.pro - create a Toeplitz matrix given a vector
;                  multiply a Toeplitz matrix times a vector
;
;  usage:
;    t = toeplitz(v)
;    t = toeplitz2(vp,vm)
;    y = toeplitzmultiply(v,x)
;    y = toeplitzmultiply2(vp,vm,x)
;
;  input:
;    v - vector of length n, representing the matrix band entries
;    vp, vm - vectors of length n, representing the lower and upper
;             triangle band entries, respectively
;    x - vector of length n
;
;  output:
;    t - Toeplitz matrix of size n x n, T[i,j] = v[|i-j|]
;        or, T[i,j] = vp[i-j], i>j
;                   = vm[j-i], i<j
;    y - output vector of length n,  y = T x
;-
function Toeplitz,v
  n = (size(v))(1)
  t = fltarr(n,n)
  for i=0,n-1 do begin
    t[i:n-1,i] = vm[0:n-1-i]
    t[i,i:n-1] = vp[0:n-1-i]
  endfor
  return, t
end
;
function Toeplitz2,vp,vm
  n = (size(vp))(1)
  t = fltarr(n,n)
  for i=0,n-1 do begin
    t[i:n-1,i] = vm[0:n-1-i]
    t[i,i:n-1] = vp[0:n-1-i]
  endfor
  return, t
end
;
function ToeplitzMultiply,v,x
  n = (size(v))(1)-1
  vv = [reverse(v[1:n]),v]
  y = fltarr(n+1)
  for k=0,n do begin
    y[k] = total(vv[n-k:2*n-k]*x)
  endfor
  return,y
end
;
function ToeplitzMultiply2,vp,vm,x
  n = (size(vp))(1)-1
  vv = [reverse(vp[1:n]),vm]
  y = fltarr(n+1)
  for k=0,n do begin
    y[k] = total(vv[n-k:2*n-k]*x)
  endfor
  return,y
end
