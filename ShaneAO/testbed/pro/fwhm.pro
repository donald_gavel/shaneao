;
; fwhm - find the full-width half-max given a 1d slice through the peak
;  input
;   y: a list of sample data points
;   x: an optional list of sample positions. If not present the data are assumed equal sampled.
;  output
;   the full-width half max, in the same units as x, or index position if x is not present
;
function fwhm, y,x,verbose=verbose
  n = (size(y))[1]
  if (n_elements(x) eq 0) then x = findgen(n)
  ymax = max(y,ind)
  if (keyword_set(verbose)) then print,'<fwhm> found maximum at x = '+strtrim(x[ind],2)+'; index = '+strtrim(ind,2)
  for k = ind+1,n-1 do begin
    if (y[k] lt ymax/2) then break
  endfor
  xu = x[k] - (x[k]-x[k-1])*((ymax/2)-y[k])/(y[k-1]-y[k])
  if (keyword_set(verbose)) then print,'<fwhm> upper 1/2 max point at x = '+strtrim(xu,2)+'; index = '+strtrim(k,2)
  for k = ind-1,0,-1 do begin
    if (y[k] lt ymax/2) then break
  endfor
  xl = x[k] + (x[k+1]-x[k])*((ymax/2)-y[k])/(y[k+1]-y[k])
  if (keyword_set(verbose)) then print,'<fwhm> lower 1/2 max point at x = '+strtrim(xl,2)+'; index = '+strtrim(k,2)
  return,xu-xl
end
