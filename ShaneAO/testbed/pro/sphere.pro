;  illuminated 3-d sphere
lat = ones(360) # (findgen(180)-90.)
lon = findgen(360) # ones(180) - 180
degrees=!pi/180.
il_lat = 30*degrees ; sub sun latitude
il = cos(lat*degrees)*cos(lon*degrees)*cos(il_lat) + sin(lat*degrees)*sin(il_lat)
alb = ones(360,180)
alb[*,90+40:90+50]=1.2 ; brighter latitude "band"
map = il*alb > 0
im = planetproject(map,128,64,64,40,40,50.,30.)
rim = rot(im,-83.) ; image orientation
g = gauss2(128,64,64,7.)
rimg = real(ftconvolve(rim,g))
end
