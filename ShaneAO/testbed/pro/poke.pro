;+
;  poke - poke an influence function at actuator locations on a
;         deformable mirror
;
;    poke,a,r,
;      a - na x ma array containing the DM.
;            The "middle" pixel (na/2,ma/2) is location (0,0).
;            The array must be large enough to include the whole
;            influence funtion at all locations
;      r - influence function (for best results, the dimensions should be odd;
;            the middle pixel is location 0,0)
;      al - n x 2 array of actuator locations, in units of pixels on a
;      v - n vector of actuator values
;
;-
pro poke,a,r,al,v
  na = (size(a))(1)
  ma = (size(a))(2)
  n = (size(v))(1)
  nr = (size(r))(1)
  if ( (nr/2)*2 eq nr ) then nev = 1 else nev = 0
  mr = (size(r))(2)
  if ( (mr/2)*2 eq mr ) then mev = 1 else mev = 0
  for i=0,n-1 do begin
    x = round(al(i,0)) + na/2  ; location relative to lower left
    y = round(al(i,1)) + ma/2
    px1 = ((x - nr/2) > 0) < (na-1)
    px2 = ((x + nr/2 - nev) > 0) < (na-1)
    py1 = ((y - mr/2) > 0) < (ma-1)
    py2 = ((y + mr/2 - mev) > 0) < (ma-1)
    rx1 = px1 - x + nr/2
    rx2 = px2 - x + nr/2
    ry1 = py1 - y + mr/2
    ry2 = py2 - y + mr/2
    if ((rx2 ge rx1) and (ry2 ge ry1)) then $
      a(px1:px2,py1:py2) = a(px1:px2,py1:py2) + v(i)*r(rx1:rx2,ry1:ry2)
  endfor
end
