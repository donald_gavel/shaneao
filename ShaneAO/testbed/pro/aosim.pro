;+
;     aosim - general purpose AO simulator
;     
;     Author: Donald Gavel, UCSC
;     Version: 0.1, 10/3/2011
;-
; ============= Basic Constants =====================
pro setConstants
  common constants, widget_id,run_no,versionText,ms,cm,microns,nm,km,degrees,arcsec,arcmin,i,percent
  if (n_elements(run_no) eq 0) then run_no = 0
  widget_id = 0L
  versionText = ['AOSim','version 0.1','Don Gavel, UCSC','Oct 4, 2011']

  ms = 1.e-3
  cm = 1.d-2
  microns = 1.d-6
  nm = 1.d-9
  km = 1.d3
  degrees = !pi/double(180)
  arcsec = !pi/double(180*3600.)
  arcmin = 60*arcsec
  i = dcomplex(0.d0,1.d0)
  percent = 0.01
end
function int,a
  return,fix(a)
end
; ============ Definitions of Data Structures =============
; ============= simulation parameters ==============
function simulation_init,model=model
  common constants
  
  telescope = 'Gemmini'
  dtele = 8. ; meters
  dsec = 1.3 ; meters (guess - check on this)
  d = dtele/43. ; scale of a subaperture
  subsample = 2
  du = d/subsample
  n = nextpow2(2*dtele/du)
  t0 = 0.
  dt = 1.*ms
  tf = 1.
  ap = circle(n,n,n/2,n/2,dtele/du/2.,1.) - circle(n,n,n/2,n/2,dsec/du/2.,1.)
  sim = {n:n, telescope:telescope, dtele:dtele, dsec:dsec, subsample:subsample, $
         du:du, t0:t0, dt:dt, tf:tf, ap:ptr_new(ap)}
  return,sim
end
; ============ Atmosphere ================
function atmosphere_init,sim,model=model
  common constants
  forward_function blowingScreen_init
  
  if (n_elements(model) eq 0) then model = 'GPI'
  ; -----------------------------
  if (model eq 'Cerro Pachon') then begin
  endif
  
  if (model eq 'Mauna Kea') then begin
  endif
  
  if (model eq 'GPI') then begin
    n = sim.n
    m = 2*n
    lx = n*sim.du ; meters wide
    ly = 2*lx ; meters long
    du = sim.du
    r0 = 1.; 15.*cm
    theta0 = 5*arcsec
    lambda0 = 0.55*microns
    zenith = 25*degrees
    percent_atmos = [100.]*percent  ; percentage atmosphere at each layer
    h = [0.]*km  ; layer altitudes
    nlayers = (size(h))[1]
    windModel = 'custom'
    wind = [5] ; layer speeds, m/sec
    wind_dir = randomu(seed,nlayers)*2*!pi ; wind direction angles
  endif
  ; --------------------------------
  
  seed = 5 ; fix a starting point
  t = 0 ; time
  dum = randomn(seed,1) ; this sets the longword vector
  k = 2*!pi/lambda0
  c0 = (1./cos(zenith))*r0^(-5./3.)/(0.423*k^2)
  Cn2 = c0*percent_atmos
  r0_h = r0*(percent_atmos)^(-3./5.)
  
  atm = {type:'atmosphere', model:model, n:n, m:m, nlayers:nlayers, t:t, du:du, lx:lx, ly:ly, seed:seed, $
    r0:r0, theta0:theta0, lambda0:lambda0, zenith:zenith, Cn2:Cn2, h:h, s:ptr_new(), $
    windModel:windModel, wind:wind, wind_dir:wind_dir}

  blowingScreen
  s = fltarr(m+n,n,nlayers)
  for layer = 0,nlayers-1 do begin
    s[*,*,layer] = blowingScreen_init(n,m,r0_h[layer],du,seed=seed)
    print,layer & wait,.1
  endfor
  atm.s = ptr_new(s,/no_copy)
  return,atm
end

function atmosphere_get,atm,t
  forward_function blowingScreen_get
  
  phase = blowingScreen_get((*atm.s),t,atm.wind[0]) ; ***assumes a single layer***
  return,phase
end

; =============== AO System =============
function AO_init,sim,model=model
  forward_function dm_init, wfs_init, rtc_init, simulator_init
  
  if (n_elements(model) eq 0) then model = 'GPI'
  if (model eq 'GPI') then begin
    if (n_elements(sim) eq 0) then begin
      sim = simulator_init(model=model)
      atm = atmosphere_init(sim,model=model)
    endif
    tweeter = dm_init(sim,model=model+'_tweeter')
    woofer = dm_init(sim,model=model+'_woofer')
    wfs = wfs_init(sim,model=model)
    rtc = rtc_init(sim,model=model)
  endif
  
  aos = {type:'AO system',tweeter:tweeter,woofer:woofer,wfs:wfs,rtc:rtc}
  
  return,aos
end
; =============== Deformable Mirror =============
function dm_init,sim,model=model
  common constants
  
  if(n_elements(model) eq 0) then model = 'GPI_tweeter'
  if (model eq 'GPI_tweeter') then begin
    n = 64 ; actuators across tweeter
    dtele = sim.dtele
    na_across = 44 ; actuators across diameter, pupil edge to edge
    pitch = dtele/float(na_across-1) ; actuator spacing
    a = fltarr(n,n)
    x = ((ones(n) ## findgen(n))-n/2)*pitch + pitch/2. ; actuator locations
    y = transpose(x)
    aloc = [[x],[y]]
    dm = {type:'dm',model:model,pitch:pitch,a:a,aloc:aloc,phase:ptr_new()}
  endif
  if (model eq 'GPI_woofer') then begin
    n = 16
    dtele = 8.0
    na_across = 10
    pitch = dtele/float(na_across-1)
    a = fltarr(n,n)
    x = ((ones(n) ## findgen(n))-n/2)*pitch + pitch/2. 
    y = transpose(x)
    aloc = [[x],[y]]
    dm = {type:'dm',model:model,pitch:pitch,a:a,aloc:aloc,phase:ptr_new()}
  endif
  return,dm
end
; =============== Wavefront Sensor ==============
function wfs_init,sim,model=model
  common constants
  
  if(n_elements(model) eq 0) then model = 'GPI'
  if (model eq 'GPI') then begin
  
    kind = 'Hartmann'
    geometry = 'Fried'
    dtele = sim.dtele ; diameter of telescope aperture, meters
    n = 64 ; size of the enclosing screen (e.g. for Fourier transform), subap spacings
    ns_across = 43 ; subapertures across
    if odd(ns_across) then r = (ns_across + 1)/2 else r = ns_across/2
    d = dtele / float(ns_across) ; size of subaperture, meters
    na_across = 44 ; actuators across
    ns_sec = 7 ; subapertures across secondary obscuration
    if odd(ns_sec) then rs = (ns_sec + 1)/2 else rs = ns_sec/2
    aps = circle(n,n,n/2,n/2,r,1.)-circle(n,n,n/2,n/2,rs,1.) ; illuminated subapertures
    badact = ptr_new() ; list of bad actuator locations: #badacts x 2 array (x and y positions on nxn grid)
    ; numbering: actuator i,j is at lower left of subaperture i,j (Fried geometry)
    ; this algorithm assumes all 4 actuators surrounding an illuminated subaperture are active:
    apa = aps or shift(aps,1,0) or shift(aps,0,1) or shift(aps,1,1)
    xloc = int(ones(n)) ## indgen(n)
    yloc = indgen(n) ## int(ones(n))
    index = where(apa,na)
    aloc = [[xloc[index]],[yloc[index]]] ; list of actuator locations
    anum = intarr(n,n)
    anum[index] = indgen(na) ; actuator numbers given x and y position
    index = where(aps,ns)
    sloc = [[xloc[index]],[yloc[index]]] ; list of subaperture locations
    snum = intarr(n,n)
    snum[index] = indgen(ns) ; subaperture numbers given x and y position
    ; form an influence matrix
    smat = fltarr(2*ns,na)
    for subap = 0,ns-1 do begin
      x = sloc[subap,0]
      y = sloc[subap,1]
      ; simple tent function influence
      smat[subap,anum[x,y]] = -1.  ; x slopes
      smat[subap,anum[x+1,y]] = 1.
      smat[subap,anum[x,y+1]] = -1.
      smat[subap,anum[x+1,y+1]] = 1.
      smat[ns+subap,anum[x,y]] = -1. ; y slopes
      smat[ns+subap,anum[x+1,y]] = -1.
      smat[ns+subap,anum[x,y+1]] = 1.
      smat[ns+subap,anum[x+1,y+1]] = 1.
    endfor
    lambda = 0.6*microns
    spatialFilterSize = 176; number of lambda/D diameter
    useSpatialFilter = 0; yes or no
    s = fltarr(ns,2) ; x and y slopes
    a = fltarr(na) ; phase points
    ap = sim.ap
    wfs = {type:'Wavefront Sensor',kind:kind,geometry:geometry,n:n,badact:badact, $
      lambda:lambda,spatialFilterSize:spatialFilterSize, useSpatialFilter: useSpatialFilter, $
      aloc:aloc,sloc:sloc,anum:anum,snum:snum,smat:smat,s:s,a:a,subsample:sim.subsample, $
      ap:ap,aps:aps,apa:apa,focalPlane:ptr_new(),detectorPlane:ptr_new(),amplitude:ptr_new(),phase:ptr_new(), $
      phase_est:ptr_new()}
    
  endif
  return,wfs
end

pro wfs_measure,wfs,sim
  common constants
  
  sloc = wfs.sloc
  n = wfs.n
  nn = sim.n
  sub = max([wfs.subsample,2])
  
  if (wfs.useSpatialFilter) then begin
    wf = (*wfs.ap)*exp(i*(*wfs.phase))
    dtheta = wfs.lambda/(nn*sim.du)
    sx = wfs.spatialFilterSize*wfs.lambda/sim.dtele/dtheta
    sy = sx
    sf = rectangle(nn,nn,nn/2,nn/2,sx,sy,1.)
    ff = ft(wf)*sf
    wf2 = ft(ff,/inverse)
    lpf_phase = unwrap(arg(wf2),(*wfs.ap))*(*wfs.ap)
    if (ptr_valid(wfs.phase)) then (*wfs.phase)=lpf_phase else wfs.phase = ptr_new(lpf_phase)
    if (ptr_valid(wfs.amplitude)) then (*wfs.amplitude)=abs(wf2) else wfs.amplitude = ptr_new(abs(wf2))
    if (ptr_valid(wfs.focalPlane)) then (*wfs.focalPlane)=ff else wfs.focalPlane = ptr_new(ff)
  endif
  nsubaps = (size(sloc))[1]
  tip = (ones(sub) ## findgen(sub) - sub/2)+0.5
  nf = total(tip*tip)
  tilt = transpose(tip)
  x0 = sim.n/2 - (wfs.n/2)*sub
  y0 = x0
  for subap = 0,nsubaps-1 do begin
    x = x0+sloc[subap,0]*sub ; scaled to set location on fine grid
    y = y0+sloc[subap,1]*sub
    s = (*wfs.phase)[x:x+sub-1,y:y+sub-1]
    wfs.s[subap,0] = total(tip*s)/nf
    wfs.s[subap,1] = total(tilt*s)/nf
  endfor
end

pro wfs_reconstruct,wfs,sim
  n = wfs.n
  sx = fltarr(n,n)
  sy = sx
  sx[wfs.sloc[*,0],wfs.sloc[*,1]]=wfs.s[*,0]
  sy[wfs.sloc[*,0],wfs.sloc[*,1]]=wfs.s[*,1]
  ph = ftwfs(sx,sy,wfs.aps)
  if (ptr_valid(wfs.phase_est)) then begin
    (*wfs.phase_est)=ph
  endif else begin
    wfs.phase_est = ptr_new(ph)
  endelse
end

; =============== Real-Time Controller (including algorithm) ==============
function rtc_init,model=model
end

; ================ main program (run at IDL top level) ==================
common constants

setConstants
sim = simulation_init()
atm = atmosphere_init(sim)
wfs = wfs_init(sim)
wfs.useSpatialFilter = 1
dmt = dm_init(sim,model='GPI_tweeter')
dmw = dm_init(sim,model='GPI_woofer')
sim = create_struct(sim,'atm',atm,'wfs',wfs,'dmt',dmt,'dmw',dmw)

t = 0.
if (n_elements(ids) eq 0) then ids = 0
if (n_elements(idp) eq 0) then idp = 0
if (n_elements(idf) eq 0) then idf = 0
if (widget_info(ids,/valid_id) eq 0) then ids = 0
if (widget_info(idp,/valid_id) eq 0) then idp = 0
if (widget_info(idf,/valid_id) eq 0) then idf = 0

for k = 0,5 do begin
  t = k*500*ms
  if (ptr_valid(wfs.phase)) then ptr_free,wfs.phase
  wfs.phase = ptr_new((atm.lambda0/wfs.lambda)*atmosphere_get(atm,t),/no_copy)
  wfs_measure,wfs,sim
  wfs_reconstruct,wfs,sim
  ux = fltarr(wfs.n,wfs.n)
  uy = ux
  ux[wfs.sloc[*,0],wfs.sloc[*,1]]=wfs.s[*,0]
  uy[wfs.sloc[*,0],wfs.sloc[*,1]]=wfs.s[*,1]
  if (not widget_info(ids,/valid_id)) then disp,[ux,uy],id=ids else dispWidget_newData,ids,[ux,uy]
  if (not widget_info(idp,/valid_id)) then disp,(*wfs.phase),id=idp else dispWidget_newData,idp,(*wfs.phase)
  if (wfs.useSpatialFilter) then begin
    if (not widget_info(idf,/valid_id)) then begin
      disp,abs(*wfs.focalPlane),id=idf
      dispWidget_scaleLog,{top:idf}
    endif else begin
      dispWidget_newData,idf,abs(*wfs.focalPlane)
    endelse
  endif
  dum = widget_event(/nowait)
  wait,.001
endfor

print,'<aosim> done'
end

