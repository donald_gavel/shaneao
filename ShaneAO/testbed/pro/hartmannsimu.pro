;+
;  hartmannsimu.pro - simulate a hartmann sensor
;;
;  USAGE:
;    s = hartmannAveGrad(in,ph,sl,d)
;    s = hartmannTilt(ap,ph,sl,d)
;    s = hartmannFFT(ap,wf,sl,d)
;
;  INPUTS:
;    in - intensity (n X n screen, centered on n/2,n/2)
;         must be zero outside aperture
;    ap - aperture mask (n X n screen, centered on n/2,n/2)
;         one inside aperture, zero outside aperture
;    wf - complex wavefront (n X n screen, centered on n/2,n/2)
;    ph - wavefront phase over the aperture - must be unwrapped
;         extend at least 1 pixel beyond the aperture so that
;         valid gradients can be taken at the aperture boundary
;    sl - subap locations (in pixels from "center" of aperture screen), 2 X ns array
;    d - subap size (in pixels)
;    subap - (optional instead of d), must be square, even-dimensioned
;    shape - 1=circle, 0=square, shape of subap
;
;  OUTPUTS:
;    s - wavefront slopes, ns X 2 array, units are phase(wf)/pixel
;
;-
function hartmannAveGrad,in,ph,sl,d,subap,shape=shape
;    intensity-weighted average-gradient-phase method

  ns = (size(sl))(2)
  nn = (size(in))(1)
  if (not keyword_set(shape)) then shape = 0 ; square is default
  if (n_elements(subap) eq 0) then begin
    n = round(d+1)
    if (n/2)*2 ne n then n = n+1; i.e. if not odd, bump up by one
    if (shape eq 1) then subap = circle(n,n,n/2,n/2,d/2,1)
    if (shape eq 0) then subap = fltarr(n,n)+1
  endif else begin
    n = (size(subap))(1)
    if (n/2)*2 ne n then begin
      print,'<hartmannAveGrad> subap size must be even'
      return,0
    endif
  endelse
  gradPhaseX = ph-shift(ph,1,0)
  gradPhaseY = ph-shift(ph,0,1)
  s = fltarr(2,ns)
  for i=0,ns-1 do begin
    ix = sl(0,i) + nn/2
    iy = sl(1,i) + nn/2
    gx = gradPhaseX(ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1)
    gy = gradPhaseY(ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1)
    intensity = in(ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1)*subap
    intensity = intensity/total(intensity)
    s(0,i) = total(gx*intensity)
    s(1,i) = total(gy*intensity)
  endfor
  return,s
end
;
function hartmannTilt,ap,ph,sl,d,subap
;    tilt-phase method

  ns = (size(sl))(2)
  nn = (size(ap))(1)
  if (n_elements(subap) eq 0) then begin
    n = round(d+1)
    if (n/2)*2 ne n then n = n+1; i.e. if not odd, bump up by one
;    subap = circle(n,n,n/2,n/2,d/2,1)
    subap = fltarr(n,n)+1
  endif else begin
    n = (size(subap))(1)
    if (n/2)*2 ne n then begin
      print,'<hartmannTilt> subap size must be even'
      return,0
    endif
  endelse
  x = findgen(n) # (fltarr(n)+1) - n/2
  y = transpose(x)
  s = fltarr(2,ns)
  for i=0,ns-1 do begin
    ix = sl(0,i) + nn/2
    iy = sl(1,i) + nn/2
    phase = ph(ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1)
    mask = ap(ix-n/2:ix+n/2-1,iy-n/2:iy+n/2-1)*subap
    piston = total(phase*mask)/total(mask)
    s(0,i) = total(x*(phase-piston)*mask)/total(mask)
    s(1,i) = total(y*(phase-piston)*mask)/total(mask)
  endfor
  return,s
end
;
function hartmannFFT,ap,wf,sl,d,wfs,blursize=blursize
;    subaperture-FFT method
  bin = 4
  ns = (size(sl))(2)
  n = 2*nextpow2(d+1)
  nn = (size(ap))(1)
  wfs = fltarr(nn,nn)
;  subap = circle(n,n,n/2,n/2,d/2,1)
  subap = fltarr(n,n)
  if (n_elements(blursize) ne 0) then blur = gauss2(n,n/2,n/2,blursize/2)
  subap[n/2-d/2:n/2+d/2,n/2-d/2:n/2+d/2] = 1
  x = findgen(n) # (fltarr(n)+1) - n/2
  ;x = rebin(x,n/bin,n/bin)
  y = transpose(x)
  s = fltarr(2,ns)
  for i=0,ns-1 do begin
    ix = sl(0,i) + nn/2
    iy = sl(1,i) + nn/2
    wavefront = wf[ix-n/2+1:ix+n/2,iy-n/2+1:iy+n/2]
    mask = ap[ix-n/2+1:ix+n/2,iy-n/2+1:iy+n/2]*subap
    psf = abs(ft(wavefront*mask))^2
    if (n_elements(blur) ne 0) then psf = real(ftconvolve(psf,blur))
    psfd = rebin(psf,n/bin,n/bin)
    m = n/(2*bin)
    wfs[ix-m+1:ix+m,iy-m+1:iy+m] = psfd+wfs[ix-m+1:ix+m,iy-m+1:iy+m]
    psf = psf/total(psf)
    s[0,i] = total(x*psf)
    s[1,i] = total(y*psf)
  endfor
  return,s
end
;
pro hartmannDisplay,c,sl,scale=scale
  if (n_elements(scale) eq 0) then scale=1.0
  ns = (size(sl))[2]
  x = transpose(sl[0,*])
  y = transpose(sl[1,*])
  cx = transpose(c[0,*])
  cy = transpose(c[1,*])
  cxn = cx*scale
  cyn = cy*scale
  plot,x,y,psym=6,/isotropic
  oplot,x,y,psym=6,color=100
  for k=0,ns-1 do begin
    plots,x[k],y[k]
    plots,x[k]+cxn[k],y[k]+cyn[k],/continue
  endfor
end
;
pro hartmannsimu
  doc_library,'hartmannsimu'
end
;

; --------- sample code -------------
i = complex(0,1.)
cm = 1.0e-2
microns = 1.0e-6
n = 256
d_tele = 3.
ns1 = 64. ; 8 ; number of subaps across
d_subap = d_tele/float(ns1)
r0 = 10.*cm
du = 4.0/float(n)

subap = rectangle(n,n,n/2,n/2,d_subap/du,d_subap/du,1.)
ds = round(d_subap/du) ; pixels across a subap
du = d_subap/ds ; recompute pixel size to make an interger number of pixels/subap
ap = circle(n,n,n/2,n/2,d_tele/(du*2),1.)

ns = ns1^2
slx = ( indgen(ns1) - ns1/2 + 0.5*even(ns1) ) ## ones(ns1)*ds
sly = transpose(slx)
slx = reform(slx,ns)
sly = reform(sly,ns)
mark = intarr(ns)
for k = 0,ns-1 do if (total(shift(subap,slx[k],sly[k])*ap) ge 0.3*total(subap)) then mark[k] = 1
slx = slx[where(mark)]
sly = sly[where(mark)]
ns = (size(slx))[1]
sl = [transpose(slx),transpose(sly)]
show = fltarr(n,n)
for k = 0,ns-1 do show[slx[k]+n/2,sly[k]+n/2] = 1
show2 = ftconvolve(show,subap)

f = screengen(n,n,r0,du)
ph = screengen(f,seed)
wf = ap*exp(i*ph)

c = hartmannFFT(ap,wf,sl,d_subap/du)
hartmannDisplay,c,sl
end
