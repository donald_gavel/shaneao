function Printcoo, x, y

;+
; routine for printing coordinates in the form "(X,Y)"
; 04/20/96 M. C. Liu (UCB)
;
;-


return, '('+strc(x)+','+strc(y)+')'

end

