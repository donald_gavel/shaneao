function radavk,powspec,x0,y0,maskang=maskang,nokcam=nokcam,sigmap=sigmap,showmask=showmask,thresh=thresh
;function for radially averaging arbitrary images (adapted from radav)
;bmac jan 2000
;
;useage: radial=radavk,powspec,x0,y0,maskang=maskang,/nokcam,sigmap=sigmap,
;/showmask
;inputs: 
;	powspec: the image to be radially averaged (called powspec for 
;		historical regions
;	x0,y0  : coordinates of center of radial average (default is
;		 center of image
;	maskang: two-element vector giving the range of angles (measured
;		 in degrees clockwise from upward) to mask
;		 eg maskang=[10,50] exlcudes all pixels at angles between
;		  10 and 50 from the averaging. 
;	nokcam: by default, explicitly masks the KCam bad (bottom left)
;	        quadrant. Set /nokcam to disable this behaviour
;	showmask: displays the mask image (requires exptv)
;	thresh: threshold below which values will be rejected as bad
;outputs:
;	sigmap: outputs the variance at each radius (optional)
;except this version explicitly masks out the bad kcam quadrant

sizep=size(powspec)
if n_elements(x0) eq 0 then x0=sizep(1)/2.0 - 0.5
if n_elements(y0) eq 0 then y0=sizep(2)/2.0 - 0.5
if n_elements(thresh) eq 0 then thresh = -1e9

print,'radial average from center ',x0,y0
xs=sizep(1)
ys=sizep(2)
mask=intarr(xs,ys)+1

if (keyword_set(maskang)) then begin
print,'Warning: Angle masking not fully tested'
ndeg = maskang
pi = !pi
degrees = pi/180
y = extend(findgen(ys)-y0,1,xs)
x = extend(findgen(xs)-x0,1,ys)
x = rotate(x,3)
ang = atan(x,y)
u=(ang gt (ndeg(0)*degrees)) and (ang lt (ndeg(1)*degrees))
;u = u or shift(rotate(u,1),1,0) or shift(rotate(u,2),0,1) or rotate(u,3)
mask = 1 - u
endif
;help,mask
if (not (keyword_set(maskkcam))) then begin
if (xs eq 256 and ys eq 256) then mask(128:255,0:127)=0
if (xs ne 256 or ys ne 256) then print,'Quadrant masking only valid for256x256'
endif
bthesh=-1
if n_elements(thresh) ne 0 then bthresh=where(powspec lt thresh)
if n_elements(bthresh) ne 1 then mask(bthresh)=0
if keyword_set(showmask) then exptv,mask
xs=sizep(1)
ys=sizep(2)

dists=fltarr(xs,ys)

for i=0,xs-1 do begin
  for j=0,ys-1 do begin
    dists(i,j)=norm([float(i)-x0,float(j)-y0])
  endfor
endfor

dists=floor(dists+0.5)
print,min(dists)
size=sqrt(max([x0-2,xs-x0-2])^2+max([y0-2,ys-y0-2])^2)
sigmap=fltarr(size)

vals=fltarr(size)
for i=0,size-1 do begin
   valid=where(dists eq i and mask eq 1)
;   vals(i)=avg(powspec(where(dists eq i and mask eq 1)))
   if valid(0) gt 0 then vals(i) = avg(powspec(valid))
;   if keyword_set(sigmap) then begin
       if valid(0) gt 0 then sigmap(i)=sigma(powspec(valid))
;   endif
endfor
;print,sigmap(0:4)
vals(0)=powspec(x0,y0)
return,vals
end


