pro imex,image, $
         box=box,cbox = cbox, $
         aprad=aprad,insky=insky,outsky=outsky, $
         plotrad=plotrad,  $
         zpt = zpt, $
         keep = keep,  $
         nodisp = nodisp, tvscl = tvscl, $
         lscl = lscl, uscl = uscl, $
         dev=dev, data=dat

;+
; procedure to interact with an image, modelled after
; the IRAF utility of the same name - nned to display the image to
; the desired stretch before using this
;
; INPUTS:
;	image	the array that represents the image being displayed.  This 
;		array may be of any type.  Rather reading pixel values from 
;		the display, they are taken from this parameter, avoiding 
;		scaling difficulties.
;
; OUTPUTS:
;       none
;
; KEYWORD INPUTS:
;	box	length of side of square box centered on the cursor
;		for doing statistics (if not odd, will be made odd)
;	aprad	radius for aperture photometry
;	insky	inner radius of sky annulus for aperture photometry
;	outsky 	outer radius of sky annulus for aperture photometry
;	plotrad	radius for radial profile plots
;       dev     use device coordinates (default)
;       data    use data coordinates 
;
; SIDE EFFECTS:
;	if program crashes, you may be left with the IMEX window (12)
;	active and will have to use 'wset' before running imex again
;
; PROCEDURE:
;	Instructions are printed and the pixel values are printed as the 
;	cursor is moved over the image.
;
;	Press the left or center mouse button to create a new line of output,
;	saving the previous line.
;
;	Press the right mouse button to exit the procedure.
;
; USES:
;   	imcenterf, radplotf, fwhm, getnum
;
; HISTORY
; Written by M.C. Liu (UCB): 08/01/95 
; 11/08/96(MCL): changed to data coords
; 08/31/98(MCL): added printing of offsets & PAs from ref position
; 09/05/98(MCL): added cbox parm
; 10/06/98(MCL): changed back to device coords as default
;                  b/c data coords don't work right
;                added zoom window (only works in data coords) - cool!
;                added 'nodisp' and 'keep' keywords
; 10/26/98(MCL): changed default phot apertures to {5,10,20}
; 05/11/99(MCL): prints S/N estimate for aperture phot
;                gets new parms with the GETNUM function
;                changed default width of sky annulus from 15 to 7
;                brings active window to the front 
;                bug fix - 'plotrad' parm actually works now
; 05/27/99(MCL): after centering, display object in zoom box
;                shows aperture phot radii on radial profile plot
;                added brief wait after print stats 
;                cleaned up comments
;                added 'zpt'
; 06/28/99(MCL): no longer prints FWHM results since prob. wrong
;                use fwhm printed on the RADPLOTF profile plot
;                  since this is correct
;
; -> should RADPLOTF.PRO be using same apertures as APPHOT?
;    now RADPLOTF uses a large sky aperture (i.e. just for plotting)
;    maybe it should use same as APPHOT, or none at all...
;-

;on_error, 2                     ;Return to caller if an error occurs

BADVAL = -1e6

; set defaults
if not(keyword_set(box)) then box=11
if not(keyword_set(aprad)) then aprad = 5.
if not(keyword_set(insky)) then insky = aprad+5.0
if not(keyword_set(outsky)) then outsky = insky+7.
if not(keyword_set(plotrad)) then plotrad = 20.0
if n_params() lt 1 then begin
	print, 'pro imex,image'
	print, '         [box=',strc(box),'],[cbox=],'+$
          '[aprad=',strc(aprad),'],[insky=',strc(insky), '],'+ $
          '[outsky=',strc(outsky),']'
	print, '         [plotrad=',strc(plotrad),'],[dev],[dat],'
        print, '         [keep],[nodisp],[lscl=],[uscl=],[zpt=]'
        return
end


; initialize, make sure box is odd
wshow
sz = size(image)
IF sz(0) NE 2 THEN begin
    message, 'Image is not 2d.', /info
    retall
endif
sz(1) = sz(1)-1                   
sz(2) = sz(2)-1
box = box > 2
bb = (box-1)/2 
if not(keyword_set(lscl)) then lscl = min(image)
if not(keyword_set(uscl)) then uscl = max(image)


; imex radial profile window
IMEXWIN = 12
if (!d.window eq IMEXWIN) then win, 0


;set up for device or data coords
if keyword_set(data) then begin
    dat = 1
    dev = 0
    message, 'using data coords', /info
endif else begin
    dat = 0
    dev = 1
    message, 'using device coords', /info
    if keyword_set(tvscl) then tvscl, image $
      else if not(keyword_set(nodisp)) then tvh, image > lscl < uscl
endelse


; set up format statement for cursor output,
;   checking if image is floating point
IF sz(sz(0)+1) GE 4 THEN form = 'F' ELSE form = 'I'
form = "($,'x=',i4,', y=',i4,', value=',"+form+",a)"
nl = string("15b)               ; go to beginning of line
cr = string("12b)		; carriage return


; print out info
print
print,' -- left mouse button to print values --
print,'    middle button to change parameters
print,'    right button to exit;
print,'  * stat box size    = ',strc(box),' *'
print,'  * apphot radius    = ',strc(aprad),' *'
print,'  * inner sky radius = ',strc(insky),' *'
print,'  * outer sky radius = ',strc(outsky),' *'
print,'  * plotting radius  = ',strc(plotrad),' *'


; zoom window info
zwin = 20     ; window #
zwsize = 200  ; window size
zsize = 20    ; size of image to zoom on
zscl = zwsize/zsize   ; image magnifying factor
zflag = 0     ; flag to see if zoom window is open


; phot window info
pwin = 21
pflag = 0  ; flag to see if phot window is open


; various flags
!err = 0
wflag = 0  ; if radial profile plot window is open
hflag = 0  ; if column headings for stats has been printed once
rflag = 0  ; if reference position is set
quit = 0


; plotrad must be largest radii
plotrad = plotrad > outsky


; start loop
repeat begin
    
    ; get cursor location & image value continuously
    cursor,x,y,2,dev=dev,dat=dat  ; is this an IDL 5 command?
    ;tvrdc,x,y,2,dev=dev,dat=dat 
    ;tvrdc,x,y,2,/dev

    ; make sure cursor is in image before doing anything
    if ((x LE sz(1)) AND (y LE sz(2)) $
      AND (x GE 0) AND (y GE 0)) then begin

        ;--------------------
        ; Left Button = radial profile + aperture photometry
        ; open imex window if not already open
        ; --------------------
        if (!err eq 1) then begin
            print,form="($,a)",string("12b)
            
	    ; open window if necessary
            ww = !d.window
            if (wflag eq 0) then begin
                window,IMEXWIN,title='IMEX: radial profile',xs=400,ys=400
                wflag = 1
            endif else wset,12
            
            ; find center 
            imcenterf,image,x,y,xcen,ycen, badpix = (image ne BADVAL), $
              cbox = cbox
            print,' imcenter: (',strc(xcen),',',strc(ycen),'), pix=', $
              strc(image(round(xcen),round(ycen)))
            
	    ; aperture photometry + radial profile
            apphot,image,xcen,ycen,aprad,insky,outsky,out,$
              badpix = (image ne BADVAL), zpt = zpt, /silent
            print,' net flux=',strc(out(4)),', sky=',strc(out(2)), $
              ', mag=', strc(zpt-2.5*alog10(out(4))), $
              ', sigma(sky)=', strc(out(6)),  $
              ', S/N=', strc(out(4)/(sqrt(out(1)*out(6))))
            ;print, ' fwhm=',strc(out(7)),', ellip=',strc(out(9)), $
            ;  ', pa=',strc(out(8))  
            radplotf, image, xcen, ycen,  $
              outrad = plotrad,  $
              insky = plotrad+1.0, outsky= plotrad+1.0+(outsky-insky), $
              zpt = zpt, $
              badpix = (image ne BADVAL), /silent, /plot

            ; overplot the phot apertures on radial plot
            plots, [aprad, aprad], !y.crange, line = 1
            xyouts, /data, aprad, !y.crange(1)*0.96, ' aprad'
            plots, [insky, insky], !y.crange, line = 1
            xyouts, /data, insky, !y.crange(1)*0.93, ' insky'
            plots, [outsky, outsky], !y.crange, line = 1
            xyouts, /data, outsky, !y.crange(1)*0.90, ' outsky'            

            ; photometry zoom window
            ; this will mess up at the edges of the image
            ; and does not handle fractional pixel values quite right
            ;   (so apertures may appear misaligned for small sizes)
            if (pflag eq 0) and (dev eq 1) then begin
                window, pwin, xs = zwsize, ys = zwsize,  $
                  tit = 'IMEX: phot apertures', $
                  xpos = 500+zwsize, ypos = 500
                pflag = 1
            endif else  $
              wset, pwin
            display2, imcut(image, 2*plotrad, xcen, ycen, /fix, /silent),  $
              indgen(2*plotrad)-plotrad+xcen, indgen(2*plotrad)-plotrad+ycen,  $
              /silent
            tvcircle, /data, aprad, xcen, ycen
            tvcircle, /data, insky, xcen, ycen
            tvcircle, /data, outsky, xcen, ycen

            ; if this is the first object, or first one after 
            ;   changing the parms, then mark as reference position
            ; PA is measured counterclockwise, from top 
            ;  (i.e. as if N is up, and E is left)
            if (rflag eq 0) then begin
                print, '  setting as reference point'
                rx = xcen
                ry = ycen
                rflag = 1
            endif else begin
                dx1 = float(xcen-rx)
                dy1 = float(ycen-ry)
                dx2 = 0
                dy2 = 1.
                dp = dx1*dx2+dy1*dy2
                cp = dy1*dx2-dx1*dy2
                ang = atan(cp,dp)*!radeg
                print, ' from ref position:  (dx,dy)=', $
                  printcoo(dx1, dy1), ', dist=', strc(sqrt(dx1^2.+dy1^2.)), $
                  ', pa = ', strc(ang)
            endelse
            print

            ; show centered object in zoom box
            wset, zwin
            zimg = rebin(imcut(image, zsize, xcen, ycen, /fix, /silent), $
                         zwsize, zwsize, /sample)
            w = where(zimg eq BADVAL, nw)
            if (nw gt 0) then  $
              zimg = zimg * (zimg ne BADVAL)        
            tvscl, zimg
            wset,ww
            
            
        ;---------------------------------------------
        ; Middle Button = stats for box around cursor
        ;---------------------------------------------
        endif else if (!err eq 2) then begin
            print,form="($,a)",string("12b)
            x0 = x-bb > 0 < (sz(1)-1)
            x1 = x+bb < (sz(1)-1) > 0
            y0 = y-bb > 0 < (sz(2)-1)
            y1 = y+bb < (sz(2)-1) > 0
            if (box ge 3) then begin
                print,' region: (',strc(x0),':',strc(x1),',', $
                  strc(y0),':',strc(y1),')'
                if not(hflag) then begin
                    hflag = 1
                    stat,image(x0:x1,y0:y1) 
                endif else $
                  stat,image(x0:x1,y0:y1),/nolabel 
            endif
            wait, 0.2
            print
            

        ;---------------------------------------------
        ; Right Button = change parameters or quit
        ;---------------------------------------------
        endif else if (!err eq 4) then begin
            print,form='($,a)',string("12b)
            print,' q to quit, any other key to change parameters'
            case get_kbrd(1) of
                
                'q': begin
                    quit = 1
                end
                
                else: begin
                    box = getnum('enter stat box size', box)
                    if (box/2 eq box/2.0) then box = box+1
                    bb = (box-1)/2
                    aprad = getnum('enter radius for apphot', aprad) > 1
                    insky = getnum('enter inner sky radius', insky) > aprad
                    outsky = getnum('enter outer sky radius', outsky) > (insky+1.0)
                    plotrad = getnum('enter plotting radius', plotrad) > outsky

                    print, '  * stat box size    = ', strc(box), ' *'
                    print, '  * apphot radius    = ', strc(aprad), ' *'
                    print, '  * inner sky radius = ', strc(insky), ' *'
                    print, '  * outer sky radius = ', strc(outsky), ' *'
                    print, '  * plotting radius  = ', strc(plotrad), ' *'

                    print, 'resetting reference point'
                    rflag = 0
                end
                
            endcase


        ;------------------------------------------------------------
        ; if no button pressed, 
        ; continuously print cursor location and pixel value 
        ;   and show zoom window
        ;------------------------------------------------------------
        endif else begin
            
            if (!Order EQ 1) THEN yy = sz(2) - y ELSE yy = y
            print,form = form, x,y,Image(x,yy),nl
            
            ; zoom window
            ww = !d.window
            if (zflag eq 0) and (dev eq 1) then begin
                window, zwin, xs = zwsize, ys = zwsize,  $
                  tit = 'IMEX: zoom window', $
                  xpos = 500, ypos = 500
                zflag = 1
            endif else  $
              wset, zwin
            zimg = rebin(imcut(image, zsize, x, y, /fix, /silent), $
                         zwsize, zwsize, /sample)
            w = where(zimg eq BADVAL, nw)
            if (nw gt 0) then  $
              zimg = zimg * (zimg ne BADVAL)        
            tvscl, zimg
            wset, ww

        endelse
        
    endif

endrep until (quit)


if not(keyword_set(keep)) then begin
    wdelete, zwin
    if (pflag) then wdelete, pwin
endif


END

