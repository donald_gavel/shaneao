function rkcamloop,firstim,lastim,firsthead,path=path,date=date
; function to read multiple kcam images into a 256x256xn array
; quick version		Bruce Macintosh		Jan 2000

	if n_elements(path) eq 0 then path='.'
        if n_elements(date) eq 0 then date='25NOV'
	nims=lastim-firstim+1
	outarray=fltarr(256,256,nims)
	outarray(*,*,0)=readkcam(firstim,firsthead,path=path,date=date)
;	print,'AVG=',avg(outarray(*,*,0))
	for i=1,nims-1 do begin 
		outarray(*,*,i)=readkcam(firstim+i,temphead,path=path,date=date)
;		print,'AVG=',avg(outarray(*,*,i))
	endfor
	return,outarray
end