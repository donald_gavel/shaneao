pro whereis,image,w,x,y

; given the index of a pixel in an array retrun the
; x and y value
;
; jrg/ucb 94/8/16
; 
; if only one pixel, then return scalars rather than vectors
; 4/16/96 MCL
;


if n_params() lt 3 then begin
	print,'pro whereis, image, w, x, y'
	retall
endif

sz = size(image)


y = floor(w/sz(1))
x =  w - y*sz(1)

if n_elements(w) eq 1 then begin
    x =  x(0)
    y =  y(0)
endif

end
