function scalesub,image,psf,xcen,ycen,stepsize=stepsize,rscales=rscales,scthresh=thresh,pixthresh=pthresh
;function to do PSF subtraction with annulus-by-annulus scaling
;assumes to pre-registered PSFs
;stepsize not supported yet; not fully documented
;needs lots of work in sensible thresholding and bad quadrant behaviour
;by bmac

rmap=round(radmap(image,xcen,ycen))
rsize=n_elements(rmap)
if rsize(0) eq 65536 then rmap(128:255,0:127)=-1 ; bad kcam quadrant
if n_elements(thresh) eq 0 then thresh = 1.0
if n_elements(pthresh eq 0) then pthresh = -1e5

outim=image
rmin=min(rmap(where (rmap ne -1)))
rmax=max(rmap)
rscales=fltarr(rmax-rmin+1)
for i=rmin,rmax do begin

	valid = where (rmap eq i and image gt pthresh and psf gt pthresh)
	if n_elements(valid) ne 1 then begin
	imav=median(image(valid))
	psfav=median(psf(valid))
	imsig=sigma(image(valid))
;	valid = where (rmap eq i and image gt pthresh and psf gt pthresh and abs(image-imav) lt 5*imsig)
	scale=imav/psfav
	if (imav lt thresh)then scale=1.0
	rscales(i-rmin)=scale
	outim(valid)=image(valid)-scale*psf(valid)
	endif 
endfor
return,outim
end

