function Imcut, image, imsize, xc, yc, xc1, yc1, $
                help=help, fixsize=fixsize, $
                silent=silent 

;+
; Function to cut out a square subsection out of an image,
; with the user specifying the center of the subsection.
; Will return a non-square image if the specified center is
; too close to the edges.
;
; INPUTS
;   image	original image
;
; OPTIONAL INPUTS
;   imsize	size of the subsection (256 default)
;               can be a scalar or a 2-element vector (x,y)
;   xc,yc	center location of the subsection 
;		(default is center)
; 
; OPTIONAL OUTPUTS
;  xc1,yc1      if too close to edge, returns the coords
;               of the original point (xc,yc) in the new subarray
;
; KEYWORD PARAMETERS
;   /fixsize    will adjust center so return an image of desired size
;               (by default it trims the output image size keeping
;                the desired center fixed) 
;
; RETURNS
;	the image subsection
;
; HISTORY
; Written by MCL(UCB): 10/01/95
; 02/10/96 (MCL): changed order of parameters (imsize,xc,yc)
; 04/23/96 (MCL): allowed imcut to be 2d 
; 05/04/96 (MCL): added /fixsize
; 03/04/98 (MCL): returns (xc1,yc1), useful for image labelling
;
; -> need to check if handling odd numbered sizes properly
;-

if keyword_set(help) or n_params() eq 0 or n_elements(image) eq 0 then begin
	print,'function imcut(image,{imsize},{xc},{yc},[help],[silent])'
	retall
endif

sz = size(image)
if n_elements(imsize) eq 0 then begin
    nx =  256
    ny =  256
endif else if n_elements(imsize) eq 1 then begin
    nx =  imsize
    ny =  imsize
endif else if n_elements(imsize) eq 2 then begin
    nx = imsize(0)
    ny = imsize(1)
endif else begin
    message, 'imsize must contain 1 or 2 elements!', /info
    stop
endelse

if n_elements(xc) eq 0 then xc = sz(1)/2
if n_elements(yc) eq 0 then yc = sz(2)/2



dx = fix(nx/2)
dy = fix(ny/2)
x0 = round(xc) - dx > 0
x1 = round(xc) + dx < (sz(1)-1)
y0 = round(yc) - dy > 0
y1 = round(yc) + dy < (sz(2)-1)
if not(odd(nx)) then x1 = x1-1
if not(odd(ny)) then y1 =  y1 -1


if (x1-x0+1) lt nx or (y1-y0+1) lt ny then begin
;	print,imsize,dd
;	print,x1,x0,x1-x0+1
;	print,y1,y0,y1-y0+1
    if not(keyword_set(silent)) then begin
        message, 'x-coords = '+printcoo(x0, x1)+$
          'y-coords = '+printcoo(y0, y1), /info
        message, '* too close to edge: '+strc(x1-x0+1)+$

          ','+strc(y1-y0+1)+' *', /info
    endif
    if keyword_set(fixsize) then begin
        if (round(xc)-dx) lt 0 then begin
            x0 = 0
            x1 =  2*dx-1
        endif
        if (round(xc)+dx) gt (sz(1)-1) then begin
            x0 =  sz(1)-2*dx
            x1 = sz(1)-1
        endif
        if (round(yc)-dy) lt 0 then begin
            y0 = 0
            y1 =  2*dx-1
        endif
        if (round(yc)+dy) gt (sz(2)-1) then begin
            y0 =  sz(2)-2*dy
            y1 = sz(2)-1
        endif
        if not(keyword_set(silent)) then $
          print, '* fixing size: x=', printcoo(x0, x1), ', y=', $
          printcoo(y0, y1) 
    endif 
endif

if not(keyword_set(silent)) then $
    print, 'cutting = ', printcoo(x0, y0), ' to ', printcoo(x1, y1)

return,image(x0:x1,y0:y1)

end
