Pro Display2, image, $
              xs, ys, $
              Title=t, XTitle=xt, YTitle=yt, $
              MIN=minval, MAX=maxval, $
              LOG=log_scaling, $
              NOTVH=notvh, $
              TVSCL=tvscl, $
              LEVELS=l, $
              ASPECT=aspect, $
              NOASPECT=noaspect, $
              INTERPOLATE=interp, $
              MASKVALUE=maskvalue, $
              PSFINE=psfine, $
              NO_EXPAND=no_expand, $
              NOERASE=noerase, $
              NOLABELS=nolabels, $
              NOTICKS=noticks, $
              CHARSIZE=cs, $
              SILENT=silent,  $
              HELP=help

SccsId = '@(#)display.pro 3.3 7/16/93 Fen Tamanaha'
;+
; NAME:
;	DISPLAY
;
; PURPOSE:
;	This procedure will display an image with the TV command that fills
;	the plotting window.  It handles scale, annotations, X and PostScript
;	devices, aspect ratios, logarithmic scaling, and interpolation.  The
;	first colormap entry is reserved for the background (pixels flagged
;	with the MASKVALUE value are mapped to this color) and the last entry
;	is reserved for user defined colored annotations.  The annotation
;	plotted by this procedure are in the color !P.Color.
;
; CATEGORY:
;	Image display.
;
; CALLING SEQUENCE:
;	DISPLAY, Image, XS, YS
;
; INPUTS:
;	Image:	Two-dimensional array to be displayed.
;
; OPTIONAL INPUTS:
;	XS:	Vector of x-axis values.  The length must equal the number of
;		rows in <Image>
;
;	YS:	Vector of y-axis values.  The length must equal the number of
;		columns in <Image>
;
; KEYWORD PARAMETERS:
;	TITLE=	Set this keyword to a string containing the title annotation
;		to be used by PLOT.
;
;	XTITLE=	Set this keyword to a string containing the x-axis annotation
;		to be used by PLOT.
;
;	YTITLE=	Set this keyword to a string containing the y-axis annotation
;		to be used by PLOT.
;
;	ASPECT=	Set this keyword to the aspect ratio (width/height) of the
;		pixels.  /ASPECT is the same as ASPECT=1 and produces square
;		pixels.
;
;	/INTERPOLATE:
;		Set this switch to enable bilinear interpolation for pixels
;		in the expanded image.  See /PS_FINE for information
;		on using this switch on a PostScript device.
;
;	MASKVALUE=
;		Set this keyword to the value that pixels with bad data or
;		no data have been flagged with.  These will be mapped to 0B.
;
;	MIN=	The minimum value of <Image> to be considered.  If MIN is not
;		provided, <Image> is searched for its minimum value.  All
;		values less than or equal to MIN are set to 1 in the Result.
;
;	MAX=	The maximum value of <Image> to be considered.  If MAX is not
;		provided, <Image> is searched for its maximum value.  All
;		values greater than or equal to MAX are set to TOP in the
;		Result.
;
;	TOP=	The maximum value of the scaled result.  If TOP is not
;		specified, 255 is used. Note that the minimum value of the
;		scaled result is always 1 (NOT 0 as in BYTSCL).
;
;	LEVELS=	Set this keyword to a vector of data value boundaries between
;		which all elements of <Image> have the same scaled byte
;		value.  e.g. LEVELS=[0,1,2,5] maps all values below 0 and
;		above 5 to 0B, map values between 0 and 1 to 1B, map values
;		between 1 and 2 to 128B, and map values between 2 and 5 to
;		255B.  This does not plot contours.
;
;	/LOG:	Set this switch to cause a logarithmic mapping.  This is
;		overridden by the LEVELS keyword.
;
;	/PS_FINE:
;		Set the switch to enable higher resolution images on a
;		PostScript device.  This is only useful with /INTERPOLATE and
;		will increase the size of the PostScript file.
;
;	/NOERASE:
;		Set the switch to prevent output device from being erased
;		before the image, scales, and annotations are displayed.
;
;	/NO_EXPAND:
;		Set this switch to prevent the image from being expanded
;		to fill the plotting window.  Scaling to byte type is still
;		performed.
;
; SIDE EFFECTS:
;	TV display is altered.
;
; RESTRICTIONS:
;	This routine may work for other devices, but it has only been tested
;	on 'X' and 'PS'.
;
; PROCEDURE:
;	Straight forward.  :-)
;
; EXAMPLE:
;	LoadCT, 3
;	image = SHIFT(DIST(20, 20), 10, 10)
;	scale = FINDGEN(20) - 10.0
;	DISPLAY, image, scale, scale, /INTERPOLATE, TITLE='!6Smooth Slope', $
;		/ASPECT
;	;Use CONTOUR with /OVERPLOT to overlay contours.
;	CONTOUR, image, scale, scale, LEVELS=1.0+FINDGEN(4)*2.0, /OVERPLOT
;
;	DISPLAY		;prints out a "Usage:" line
;
; MODIFICATION HISTORY:
; 	Written by:	Fen Tamanaha, July 10, 1993.  Release 3.1
;	July 13, 1993	Fen: (3.2) Fixed /No_Expand
;	July 16, 1993	Fen: (3.3) Really fixed /No_Expand
;
; changed to "display2.pro" 
;    	added NOAXIS flag; if set, no tick marks, axes, or axes labels drawn
;	does this by drawing the axis stuff in black (a quick scam)
; this is useful for doing labelling on images in the data coordinates
; 8/29/94 MCL
;
; default scales using TVH, set /notvh to disable this
; and by default displays with the proper aspect ratio (/noaspect
; to disable)
; 8/1/95 MCL
;
; added CHARSIZE
; 11/30/95 MCL
;
; adjusted /NOTICKS and /NOLABELS so can use all 4 possible combos
; 09/20/96 MCL
;
; added /tvscl - same as /notv
; 10/14/96 MCL
;
; adjusted call to TVH to determine color scaling: now uses the
; original image instead of the expanded one (this is faster)
; 02/21/97 MCL
;
; ignores BADVAL pixles even if /tvscl is set
; 04/28/97 MCL
;
; Please send comments/questions to <mliu@astro.berkeley.edu>
;-

    On_Error, 2

;
; Validate arguments.
;
    nparms = N_Params()
    If ( Keyword_Set(help) ) Then nparms = 0	;force a "Usage:" line
    Case ( nparms ) Of
        1: Begin
            sz = Size(image)
            If ( sz(0) NE 2 ) Then Begin
                Message, '<image> must be an array.'
            EndIf
            xs = FIndGen(sz(1))
            ys = FIndGen(sz(2))
        End
        2: Begin
            sz = Size(image)
            If ( sz(0) NE 2 ) Then Begin
                Message, '<image> must be an array.'
            EndIf
            If ( N_Elements(xs) NE sz(1) ) Then Begin
                Message, '<xs> does not match <image> dimensions.'
            EndIf
            ys = FIndGen(sz(2))
        End
        3: Begin
            sz = Size(image)
            If ( sz(0) NE 2 ) Then Begin
                Message, '<image> must be an array.'
            EndIf
            If ( N_Elements(xs) NE sz(1) ) Then Begin
                Message, '<xs> does not match <image> dimensions.'
            EndIf
            If ( N_Elements(ys) NE sz(2) ) Then Begin
                Message, '<ys> does not match <image> dimensions.'
            EndIf
        End
        Else: Begin
            Message, 'Usage: DISPLAY, image [,xs [,ys]] [,TITLE=] [,XTITLE=] [,YTITLE=]', /Info
	    Message, '           [,MIN=] [,MAX=] [,/LOG] [,LEVELS=]', /Info
            Message, '           [,ASPECT=] [,/INTERPOLATE] [MASKVALUE=]', /Info
	    Message, '           [,/NO_EXPAND] [,/NOERASE] [,/PSFINE]', /Info
	    Message, '           [,/NOLABELS] [,/NOTICKS] [CHARSIZE=]', /Info
            Return
        End
    EndCase


; set ASPECT to default
if keyword_set(noaspect) then aspect=0 else aspect=1

 
;
; The plotting device must be erased to reset the system variables so that
;	IMGEXP will get the default values.  The /NOERASE keyword should
;	be used to prevent this.  One typical situation is when DISPLAY
;	is called after a !P.MULTI change.  An ERASE at this point would
;	destroy the previous plots.
;
    If ( Not Keyword_Set(noerase) ) Then Begin
	Erase
    EndIf

;
; If /PSFINE is set then up the intermediate interpolated image width.
;	This only has an effect on PostScript output.
;
    If (Keyword_Set(psfine) ) Then Begin
	psis = 512.0
    Endif
    im = ImgExp(image, xs, ys, xscale, yscale, xrange, yrange, $
		Aspect=aspect, Interpolate=Keyword_Set(interp), $
		MaskValue=maskvalue, Position=dev_pos, PS_Interp_Size=psis, $
		No_Expand=Keyword_Set(no_expand))
    sz = Size(im)
    im_x_width = Float(sz(1))                   ;image width
    im_y_width = Float(sz(2))                   ;image height
 
;
; Determine the device coordinates of the plotting regions.
;
    dev_x_width = dev_pos(2) - dev_pos(0) + 1
    dev_y_width = dev_pos(3) - dev_pos(1) + 1
    If ( (im_x_width GT dev_x_width) Or (im_y_width GT dev_y_width) ) Then Begin
	Message, 'Error: Scaled image is larger than plotting window.'
    EndIf



;
; set up tvh scaling by default
common tvh, cb_minl,cb_maxl
if not(keyword_set(notvh)) and not(keyword_set(tvscl)) then begin
    szi = size(image)
    if keyword_set(silent) then begin
        if szi(szi(0)+2) gt sz(sz(0)+2) then $
          tvh, im, /nodisplay, /silent $
        else $
          tvh, image, /nodisplay, /silent 
    endif else begin
        if szi(szi(0)+2) gt sz(sz(0)+2) then $
          tvh, im, /nodisplay $
        else $
          tvh, image, /nodisplay
    endelse
    minval = cb_minl
    maxval = cb_maxl

; regardless of the image, do not display BADVAL pixels
endif else begin

    BADVAL = -1e6
    minval = min(image(where(image ne BADVAL)))
    maxval = max(image)

endelse


;
; Convert a non-byte type image to byte with IMGSCL.  The bottom entry
;	of the color table is reserved for the background/NODATA color
;	by IMGSCL.  The top color table entry will also be reserved
;	here for annotation color.
;
    byte_im = ImgScl(im, Min = minval, Max = maxval, Top = !D.table_size-2, $
                 Log = log_scaling, Levels = l, MaskValue = maskvalue)
;    If ( sz(sz(0)+1) GT 1 ) Then Begin
;        byte_im = ImgScl(im, Min = minval, Max = maxval, Top = !D.table_size-2, $
;        Log = log_scaling, Levels = l, MaskValue = maskvalue)
;    EndIf Else Begin
;        Message, '<Image> is already byte type. No scaling done.', /Info
;        byte_im = im
;    EndElse

;
; Put the image on the TV.
;
    TV, byte_im, /Device, dev_pos(0), dev_pos(1), $
		XSize=dev_pos(2)-dev_pos(0), YSize=dev_pos(3)-dev_pos(1)

;
; Manage the title and axis labels.
;
    If ( Keyword_Set(t) ) Then Begin
        title = String(t)
    EndIf Else Begin
        title = ' '
    EndElse
 
    If ( Keyword_Set(xt) ) Then Begin
        xtitle = String(xt)
    EndIf Else Begin
        xtitle = ' '
    EndElse
 
    If ( Keyword_Set(yt) ) Then Begin
        ytitle = String(yt)
    EndIf Else Begin
        ytitle = ' '
    EndElse

   if not(keyword_set(cs)) then cs = 1.0 $
	else if (cs le 0.0) then cs = 1.0
 
;
; Overplot annotations.
;


   if keyword_set(nolabels) eq 0 and keyword_set(noticks) eq 0 then $
     begin 
       Plot, [0, 1], /NoErase, /NoData, XStyle = 1, YStyle = 1, $
         /Device, Position = dev_pos, $
         XRange = xrange, YRange = yrange, $
         Title = title, XTitle = xtitle, YTitle = ytitle, $
         Charsize = cs
   endif else if keyword_set(nolabels) eq 0 and keyword_set(noticks) $
     then begin  
       Plot, [0, 1], /NoErase, /NoData, XStyle = 1, YStyle = 1, $
         /Device, Position = dev_pos, $
         XRange = xrange, YRange = yrange, $
         Title = title, XTitle = xtitle, YTitle = ytitle, $
         Ticklen = 0, $
         Charsize = cs
    endif else if keyword_set(nolabels) and not(keyword_set(noticks)) $
      then begin
        Plot, [0, 1], /NoErase, /NoData,  $
          /Device, Position = dev_pos, $
          XRange = xrange, YRange = yrange, $
          Title = title, XTitle = xtitle, YTitle = ytitle, $
          xtickname = replicate(' ', 10), $
          ytickname = replicate(' ', 10), $
          Charsize = cs
    endif else begin
        Plot, [0, 1], /NoErase, /NoData, XStyle = 1, YStyle = 1, $
          /Device, Position = dev_pos, $
          XRange = xrange, YRange = yrange, $
          Title = title, XTitle = xtitle, YTitle = ytitle, $
          Ticklen = 0, xtickname = replicate(' ', 10), $
          ytickname = replicate(' ', 10), $
          Charsize = cs
    endelse


    Return

End

