function radmap,inarray,x0,y0
;function to generate a radius map to match array inarray from center x0,y0

sizein=size(inarray)
if n_elements(x0) eq 0 then x0=sizein(1)/2.0 - 0.5
if n_elements(y0) eq 0 then y0=sizein(2)/2.0 - 0.5
xs=sizein(1)
ys=sizein(2)

dists=fltarr(xs,ys)

for i=0,xs-1 do begin
  for j=0,ys-1 do begin
    dists(i,j)=norm([float(i)-x0,float(j)-y0])
  endfor
endfor

return,dists
end
