function getnum, prompt, defval

;+
; print a string and then let user enter a number
; just like READ command, except can enter <Return> to choose default
; and will re-prompt if user doesn't enter a number
;
; INPUTS
;   prompt  string to print (no colon)
;   defval  default value 
;
; USES
;   strc, isnumber (JHU/APL)
;
; 05/12/99 M. Liu (UCB)
;-

if n_elements(prompt) eq 0 then prompt = 'enter a value'
if n_elements(defval) eq 0 then defstr = ''  $
  else defstr = strc(defval)

ans = ''
done = 0
repeat begin
    read, prompt+' ['+defstr+']: ', ans
    if (ans eq '') and (n_elements(defval) ne 0) then begin
        done = 1
        retval = defval
    endif else if (isnumber(ans) gt 0) then begin
        done = 1
        retval = ans
    endif
endrep until (done)

return, retval
end

