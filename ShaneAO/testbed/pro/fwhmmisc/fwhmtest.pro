pro fwhmtest,image,outparams,radius=radius,single=single

if n_elements(radius) eq 0 then radius=4
continue=1
print,'left button for fwhm, right button to exit'
print,'  xcen   ycen     peak       xfwhm yfwhm     rot     flux     background strehl'
windex=!D.WINDOW
window,30,xs=400,ys=300,title='Radial profile'
wset,windex
while continue do begin
	if (keyword_set(single)) then continue=0
	tvpos,xcur,ycur
	if !err eq 4 then continue = 0
	if !err eq 1 then begin
;	print,!ERR
	box=radius*2+1
        cntrd,image,xcur,ycur,x,y,radius,/silent
;	print,x,y
        if (x ne -1 and y ne -1) then begin
	fwhmastro,image,x,y,box,params
	sky=params(0)
	peak=params(1)
	xsig=params(2)
	ysig=params(3)
	xc=params(4)
	yc=params(5)
	rot=params(6)*180./!pi
	outparams=params
	aper,image,x,y,mag,magerr,sky,skyerr,8.6,[radius*2],[radius*3,radius*3+5],[-32765,32765],/flux,/silent
	starflux=mag
	strehl=(peak/starflux) / (0.14)
	print,xc,yc,peak,xsig*2.35,ysig*2.35,rot,starflux,sky,strehl,$
	format='(2x,f5.1,2x,f5.1,4x,f7.1,4x,f5.2,1x,f5.2,4x,f4.0,4x,F9.0,2x,f6.1,2x,F4.2)'
	wset,30
	plotgauss,image,params,radius*2
	wset,windex
        endif
        if (x eq -1 or y eq -1) then begin
         continue=0
         print,'Centroider error; rerun with bigger aperture.'
        endif
	endif
;	print,continue
	wait,0.25
;	result=get_kbrd(1)
;	if (result eq 'q') then continue=0
endwhile
;print,'end of while'
end
