pro Whereismin, image, x, y, minv, $
                mark = mark, nobadval = nobadval, silent=silent

; given an array, returns location and value of min pixel
; 10/31/94 MCL
;
; added /mark = draw a cross at min pixel on the current window (dev coords)
;       /nobadval = exclude BADVAL pixels when finding the min pixel
; 01/15/97 MCL


BADVAL = -1e6
if n_params() lt 1 then begin
	print,'pro whereismin,image,(x),(y),(minv),[mark],[nobadval],[silent]'
	retall
endif


; find min pixel
if keyword_set(nobadval) then $
  minv = min(image(where(image ne BADVAL))) $
else $
  minv = min(image)
whereis,image,where(image eq minv),x,y


; print info
if keyword_set(silent) eq 0 then $
  print, 'min = ', strc(minv), ' at (', strc(fix(x)), ',', strc(fix(y)), ')'


; mark on screen
if keyword_set(mark) then begin
    if (!d.window ne -1) then begin
      oplot, [x], [y], sym = 10, ps = 1, color = 1
      tvcircle, 20, x, y, /data, color = 1
    endif else $
      message, 'unable to mark b/c no windows available'
endif


end
