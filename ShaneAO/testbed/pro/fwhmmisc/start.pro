;+
; start.pro - start up file for IDL
;  sets up the path to the astronomy routines
;  usage: @start.pro
;-
!path = EXPAND_PATH('+/u/user/observers/llnl/pro') +':'+!path
!path = !path + ':'+ EXPAND_PATH('+/u/user/observers/bmac/idlprocs')
;print,'this is a test'
