function fixkcam,inimage
;function to fix the kcam pixels-off-by-one bug
	image=inimage
	for ix=0,1 do begin
		for iy=0,1 do begin
			quad=image(128*ix:128*ix+127,128*iy:128*iy+127)
			quad=shift(quad,1)
			image(128*ix:128*ix+127,128*iy:128*iy+127)=quad
		endfor
	endfor
	return,image
end