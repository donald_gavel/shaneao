pro Whereismax, image, x, y, maxv, mark=mark, silent=silent

; given an array, returns location and value of max pixel
;
; note that this will return a long integer if there is
; a unique max pixel, otherwise it will return an long array
; which may be a problem for some routines
;
; 10/31/94 MCL
;
; added mark feature, which will draw a cross at the max
; pixel on the current window
; 8/19/96 MCL


if n_params() lt 1 then begin
	print,'pro whereismax,image,(x),(y),(maxv),[mark],[silent]'
	retall
endif


maxv = max(image)

whereis,image,where(image eq maxv),x,y

if n_elements(x) eq 1 then begin
	x = x(0)
	y = y(0)
endif

if keyword_set(silent) eq 0 then $n
  print,'max = ',strc(maxv),' at (',strc(fix(x)),',',strc(fix(y)),')'

if keyword_set(mark) then begin
    if (!d.window ne -1) then begin
      oplot, [x], [y], sym = 10, ps = 1, color = 1
      tvcircle, 20, x, y, /data, color = 1
    endif else $
      message, 'unable to mark b/c no windows available'
endif

end
