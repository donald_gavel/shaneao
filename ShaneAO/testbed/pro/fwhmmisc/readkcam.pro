function readkcam,imnum,imheader,path=path,date=date,fix=fix
; function to read KCam images
; and divide them by the number of coadds
; and automatically reorder them if 'fix' is set
; quick version		Bruce Macintosh		Nov 1999

	if n_elements(path) eq 0 then path='.'
        if n_elements(date) eq 0 then date='20FEB'
	if n_elements(fix) eq 0 then fix=0
	fname=date+string(imnum,format="(I3.3)")+'.fits.Z'
;	print,fname
;	print,path+'/'+fname
	image=readfits(path+'/'+fname,imheadertemp,/silent)
        coadds=sxpar(imheadertemp,'COADDS')
;recover number of coadds
        image=image/coadds
;decoadd
        imheader=imheadertemp
	exptime=sxpar(imheadertemp,'ITIME')
	filter1=sxpar(imheadertemp,'FILTER')
	filter2=sxpar(imheadertemp,'OBKCNAME')
	aostat=sxpar(imheadertemp,'AODMSTAT')
	obj=sxpar(imheadertemp,'OBJECT')
	avim=avg(image)
	print,fname,obj,coadds,exptime,filter1,filter2,strmid(aostat,0,2),avim,$
	format="(A15,1X,A20,1X,I3,'x',F6.2,' s',2x,A2,' ',A6,'AO=',A2,F7.1)"
	if (fix eq 1) then begin
;fix/shuffle kcam quadrants
	for ix=0,1 do begin
		for iy=0,1 do begin
			quad=image(128*ix:128*ix+127,128*iy:128*iy+127)
			quad=shift(quad,1)
			image(128*ix:128*ix+127,128*iy:128*iy+127)=quad
		endfor
	endfor
	endif
return,image
end
