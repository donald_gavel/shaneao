pro fwhm,image,x,y,aprad,fwhm,pa,ellip,silent=silent
		
; estimates fwhm, ellipticity and pa for an image,
; assuming the image is sky subtraced and centroid is accurate,
; using the same algorithm as IRAF's 'imexamine'
;
; seems to give unusually large FWHMs, larger for brighter objects
;
; Written by M. Liu (UCB): 08/01/95


if n_params() le 0 then begin
	print,'pro fwhm,image,x,y,aprad,(fwhm),(ellip),(pa),[silent]'
	return
endif


; trim image to smallest size (may be rectangular), translate coord origin
;   (there must be a cute IDL way to do this neater)
sz = size(image)
x0 = floor(x-aprad) 
x1 = ceil(x+aprad)
y0 = floor(y-aprad) 
y1 = ceil(y+aprad)
if (x0 lt 0.0) or (x1 gt (sz(1)-1)) or (y0 lt 0.0) or (y0 gt (sz(2)-1)) then $
	print,'** WARNING: apertures are off the image! **'
x0 = x0 > 0.0
x1 = x1 < (sz(1)-1)
y0 = y0 > 0.0
y1 = y1 < (sz(2)-1)
if keyword_set(silent) eq 0 then begin
	print,'image trim: (',strc(fix(x0)),':',strc(fix(x1)),',',$
		      strc(fix(y0)),':',strc(fix(y1)),')'
endif
img = image(x0:x1,y0:y1)
xcen = x - x0
ycen = y - y0


xs = x1 - x0 + 1
ys = y1 - y0 + 1
xx = findgen(xs)
yy = findgen(ys)
xof = (xx - xcen)	; offsets
yof = (yy - ycen)


; calculate square of the 2nd intensity moments
mxx = 0.
mxy = 0.
myy = 0.
;for i = 0L,(ys-1)  do begin
;   for j = 0L,(xs-1)  do begin 
for i = 0L,(xs-1)  do begin
   for j = 0L,(ys-1)  do begin 
      mxx = mxx + xof(i)*xof(i)*img(i,j)
      myy = myy + yof(j)*yof(j)*img(i,j)
    mxy = mxy + xof(i)*yof(j)*img(i,j)
   endfor
endfor


; determine image profile
; if no actual object, return junk
flux = total(img)
if (flux le 0.0) then begin

	fwhm = 999.
	ellip = 999.
	pa = 999.

endif else begin

	mxx = mxx/flux
	mxy = mxy/flux
	myy = myy/flux
	fwhm = 2. * sqrt (alog(2) * (Mxx + Myy))
	ellip = sqrt ((Mxx - Myy)^ 2. + (2. * Myy)^ 2.)
	pa = 0.5 * atan (2. * Mxy / (Mxx - Myy)) * 180. / !pi

endelse


;   Mxx = sum (x * x * I) / sum (I)
;   Myy = sum (y * y * I) / sum (I)
;   Mxy = sum (x * y * I) / sum (I)
;   r = 2 * sqrt (ln (2) * (Mxx + Myy))
;   e = sqrt ((Mxx - Myy) ** 2 + (2 * Myy) ** 2)
;   pa = 0.5 * atan (2 * Mxy / (Mxx - Myy))
        


if keyword_set(silent) eq 0 then  print,'simga=',fwhm,' pa=',pa,' e=',e


end
