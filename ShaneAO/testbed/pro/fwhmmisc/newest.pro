function newest,imheader
             spawn,'ls -lt /home/callisto/ccdev/data/*fits | grep 267840 | head -1 | cut -c47-', x
	     image=readfits(x[0],imheadertemp)
		coadds=sxpar(imheadertemp,'ncoadds')
	     image=image/coadds

	     imheader=imheadertemp
        exptime=sxpar(imheadertemp,'coaddtim')
        filter1=sxpar(imheadertemp,'filter1')
        filter2=sxpar(imheadertemp,'filter2')
        print,x[0],coadds,exptime/1000.,filter1,filter2,$
        format="(A40,2X,I3,' x ',F7.3,' s',4x,A8,4x,A8)"


return,image
end

