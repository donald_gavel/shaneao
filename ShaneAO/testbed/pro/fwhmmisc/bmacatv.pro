;------------------------------------------------------------------------
; ATV.PRO
;
; by Aaron J. Barth
;
; version 1.0b1
;
; The current version, and instructions, can be found at:
;   http://cfa-www.harvard.edu/~abarth/atv/atv.html
;
;
; changes to make in future versions:
; set up 3 blink buffers
; create more custom color maps
; Force line plot window to always be white on black
; line plots along an arbitrary diagonal
; gaussian fits and fwhm
; try it out under different window managers
; allow user to choose size of contour or surface plot
; deal with fractional pixels better in zoomed image
; add plot annotation for photometry events
;
; last modified 12/17/98
;------------------------------------------------------------------------


pro atv_startup

; This routine creates and realizes the window widgets for atv.

common atv_state, state
common atv_images, $
  main_image, $
  display_image, $
  scaled_image, $
  blink_image1, $
  pan_image
common atv_color, r_vector, g_vector, b_vector

; Read in a color table to initialize !d.table_size
loadct, 0, /silent

; Set starting window sizes    
state.draw_window_size = [512L, 512L]

; pan and track window sizes should be an integer multiple of 11
state.track_window_size = 121L   ; this window is always square
state.pan_window_size = 121L     ; this window is always square


; Define widgets

base = widget_base(title = 'atv', $
                   /column, /base_align_right, $
                   app_mbar = top_menu, $
                   uvalue = 'atv_base', $
                   /tlb_size_events)
state.base_id = base

junk = {cw_pdmenu_s, flags:0, name:''}

top_menu_desc = [ $
                  {cw_pdmenu_s, 1, 'File'}, $         ; file menu
                  {cw_pdmenu_s, 0, 'ReadFits'}, $
                  {cw_pdmenu_s, 2, 'Quit'}, $
                  {cw_pdmenu_s, 1, 'ColorMap'}, $     ; color menu
                  {cw_pdmenu_s, 0, 'Grayscale'}, $
                  {cw_pdmenu_s, 0, 'Blue-White'}, $
                  {cw_pdmenu_s, 0, 'Red-Orange'}, $
                  {cw_pdmenu_s, 0, 'Rainbow'}, $
                  {cw_pdmenu_s, 0, 'BGRY'}, $
                  {cw_pdmenu_s, 2, 'ATV Special'}, $
                  {cw_pdmenu_s, 1, 'Scaling'}, $      ; scaling menu
                  {cw_pdmenu_s, 0, 'Linear'}, $
                  {cw_pdmenu_s, 0, 'Log'}, $
                  {cw_pdmenu_s, 2, 'HistEq'}, $
                  {cw_pdmenu_s, 1, 'Help'}, $
                  {cw_pdmenu_s, 2, 'ATV Help'} $
                ]

top_menu = cw_pdmenu(top_menu, top_menu_desc, $
                      ids = menu_ids, $
                      /mbar, $
                      /return_id, $
                      uvalue = 'top_menu')
state.menu_ids = menu_ids


track_base =    widget_base(base, /row)
track_base_1 =  widget_base(track_base, /column, /align_right)
track_base_1a = widget_base(track_base_1, /row, /align_bottom)
slider_base =   widget_base(track_base_1a, /column, /align_right)
minmax_base =   widget_base(track_base_1a, /column, /align_right)
track_base_2 =  widget_base(track_base, /row, /base_align_bottom)
button_base1 =  widget_base(base, /row, /base_align_bottom)
button_base2 =  widget_base(base, /row, /base_align_bottom)
mode_base =     widget_base(button_base1, /row, /base_align_bottom, /exclusive)
draw_base =     widget_base(base, /column, /base_align_left, $
                            /tracking_events, $
                            uvalue = 'draw_base', $
                            frame = 2)
state.draw_base_id = draw_base

brightness_slider = widget_slider(slider_base, $
                                /drag, $
                                minimum = 0, $
                                maximum = 2 * !d.table_size - 1, $
                                title = 'Brightness', $
                                uvalue = 'brightness', $
                                value = !d.table_size, $
                                /suppress_value)
state.brightness_slider_id = brightness_slider

contrast_slider = widget_slider(slider_base, $
                                /drag, $
                                minimum = 0, $
                                maximum = 100, $
                                title = 'Contrast', $
                                uvalue = 'contrast', $
                                value = 50, $
                                /suppress_value)
state.contrast_slider_id = contrast_slider

zoommode_button = widget_button(mode_base, $
                                value = 'ZoomMode', $
                                uvalue = 'zoom_mode')

blinkmode_button = widget_button(mode_base, $
                                 value = 'BlinkMode', $
                                 uvalue = 'blink_mode')

invert_button = widget_button(button_base1, $
                              value = 'Invert', $
                              uvalue = 'invert')

reset_button = widget_button(button_base1, $
                              value = 'ResetColor', $
                              uvalue = 'reset_color')

autoscale_button = widget_button(button_base1, $
                                 uvalue = 'autoscale_button', $
                                 value = 'AutoScale')

fullrange_button = widget_button(button_base1, $
                                 uvalue = 'full_range', $
                                 value = 'FullRange')

keyboard_text = widget_text(button_base2, $
                          /all_events, $
                          scr_xsize = 1, $
                          scr_ysize = 1, $
                          units = 0, $
                          uvalue = 'keyboard_text', $
                          value = '')
state.keyboard_text_id = keyboard_text

zoomin_button = widget_button(button_base2, $
                              value = 'ZoomIn', $
                              uvalue = 'zoom_in')

zoomout_button = widget_button(button_base2, $
                              value = 'ZoomOut', $
                              uvalue = 'zoom_out')

zoomone_button = widget_button(button_base2, $
                               value = 'Zoom1', $
                               uvalue = 'zoom_one')

center_button = widget_button(button_base2, $
                              value = 'Center', $
                              uvalue = 'center')

setblink1_button = widget_button(button_base2, $
                                 uvalue = 'set_blink1', $
                                 value = 'SetBlink')
;setblink2_button = widget_button(button_base2, $
;                                 uvalue = 'set_blink2', $
;                                 value = 'SetBlink2')
;setblink3_button = widget_button(button_base2, $
;                                 uvalue = 'set_blink3', $
;                                 value = 'SetBlink3')

done_button = widget_button(button_base2, $
                            value = 'Done', uvalue = 'done')

min_text = cw_field(minmax_base, $
                    uvalue = 'min_text', $
                    /floating,  $
                    title = 'Min=', $
                    value = state.min_value,  $
                    /return_events, $
                    xsize = 8)
state.min_text_id = min_text

max_text = cw_field(minmax_base, $
                    uvalue = 'max_text', $
                    /floating,  $
                    title = 'Max=', $
                    value = state.max_value, $
                    /return_events, $
                    xsize = 8)
state.max_text_id = max_text

tmp_string = string(1000, 1000, 1.0e-10, $
                    format = '("(",i4,",",i4,") ",g12.5)' )

location_bar = widget_label (track_base_1, $
                             value = tmp_string,  $
                             uvalue = 'location_bar',  frame = 1)
state.location_bar_id = location_bar


pan_window = widget_draw(track_base_2, $
                         xsize = state.pan_window_size, $
                         ysize = state.pan_window_size, $
                         frame = 2, uvalue = 'pan_window', $
                         /button_events, /motion_events)

track_window = widget_draw(track_base_2, $
                          xsize=state.track_window_size, $
                          ysize=state.track_window_size, $
                          frame=2, uvalue='track_window')

draw_window = widget_draw(draw_base, $
                          uvalue = 'draw_window', $
                          /motion_events,  /button_events, $
                          scr_xsize = state.draw_window_size[0], $
                          scr_ysize = state.draw_window_size[1]) 
state.draw_widget_id = draw_window

widget_control, base, /realize

widget_control, zoommode_button, /set_button

widget_control, track_window, get_value = tmp_value
state.track_window_id = tmp_value
widget_control, draw_window, get_value = tmp_value
state.draw_window_id = tmp_value
widget_control, pan_window, get_value = tmp_value
state.pan_window_id = tmp_value

; Find window padding sizes needed for resizing routines.
; Add extra padding for menu bar, since this isn't included in 
; the geometry returned by widget_info.
; Also add extra padding for margin (frame) in draw base.

basegeom = widget_info(state.base_id, /geometry)
drawbasegeom = widget_info(state.draw_base_id, /geometry)

state.pad[0] = basegeom.xsize - state.draw_window_size[0] 
state.pad[1] = basegeom.ysize - state.draw_window_size[1] + 30 
state.base_pad[0] = basegeom.xsize - drawbasegeom.xsize $
  + (2 * basegeom.margin)
state.base_pad[1] = basegeom.ysize - drawbasegeom.ysize + 30 $
  + (2 * basegeom.margin)

state.base_min_size = [512, state.base_pad[1] + 100]

; Initialize the vectors that hold the current color table.
; See the routine atv_stretchct to see why we do it this way.

r_vector = bytarr(!d.table_size * 3)
g_vector = bytarr(!d.table_size * 3)
b_vector = bytarr(!d.table_size * 3)

tmp_array = replicate(255, !d.table_size)
r_vector[!d.table_size * 2] = tmp_array
g_vector[!d.table_size * 2] = tmp_array
b_vector[!d.table_size * 2] = tmp_array

atv_getct, 0
state.invert_colormap = 0

end

;------------------------------------------------------------------------

function atv_polycolor, p

; Routine to return an vector of length !d.table_size,
; defined by a 5th order polynomial.   Called by atv_makect
; to define new color tables in terms of polynomial coefficients.

x = findgen(256)

y = p[0] + x * p[1] + x^2 * p[2] + x^3 * p[3] + x^4 * p[4] + x^5 * p[5]

w = where(y GT 255, nw)
if (nw GT 0) then y(w) = 255

w =  where(y LT 0, nw)
if (nw GT 0) then y(w) = 0

z = congrid(y, !d.table_size)

return, z
end

;-----------------------------------------------------------------------

pro atv_makect, tablename

; Define new color tables here, in terms of 5th order polynomials.
; To define a new color table, first set it up using xpalette,
; then load current color table into 3 256-element vectors, and
; do a 5th order poly_fit.  Store the coefficients and name
; the color table here.  Invert if necessary.

common atv_state
common atv_color

case tablename of
    'ATV Special': begin
 
        r = atv_polycolor([39.4609, $
                           -5.19434, $
                           0.128174, $
                           -0.000857115, $
                           2.23517e-06, $
                           -1.87902e-09])
        
        g = atv_polycolor([-15.3496, $
                           1.76843, $
                           -0.0418186, $
                           0.000308216, $
                           -6.07106e-07, $
                           0.0000])
        
        b = atv_polycolor([0.000, $ 
                           12.2449, $
                           -0.202679, $
                           0.00108027, $
                           -2.47709e-06, $
                           2.66846e-09])
   end

; add more color table definitions here as needed...
    else:

endcase

tvlct, r, g, b

if (state.invert_colormap EQ 1) then begin
    r = abs (r - 255)
    g = abs (g - 255)
    b = abs (b - 255)
endif

r_vector(!d.table_size) = r
g_vector(!d.table_size) = g
b_vector(!d.table_size) = b
    
end

;-----------------------------------------------------------------------

pro atv_getstats

; Get basic image stats: min and max, and size.

common atv_state
common atv_images

; this routine operates on main_image, which is in the
; atv_images common block

state.image_size = [ (size(main_image))[1], (size(main_image))[2] ]

state.image_min = min(main_image)
state.image_max = max(main_image)

state.min_value = state.image_min
state.max_value = state.image_max

if (state.min_value GE state.max_value) then begin
    state.min_value = state.min_value - 1
    state.max_value = state.max_value + 1
endif

; zero the current position on the center of the image

state.mouse = round(state.image_size / 2.)
state.centerpix = round(state.image_size / 2.)
state.offset = round( state.centerpix - $
                      (0.5 * state.draw_window_size / state.zoom_level) )

end

;-----------------------------------------------------------------------

pro atv_gettrack

; Create the image to display in the track window that tracks
; cursor movements.

common atv_state
common atv_images

; Get x and y for center of track window

zcenter = (0 > state.mouse < state.image_size)

track_image = rebin(scaled_image[zcenter[0]:zcenter[0]+10,  $
                                       zcenter[1]:zcenter[1]+10], $
                   state.track_window_size, state.track_window_size, $
                   /sample)

wset, state.track_window_id
tv, track_image

device, set_graphics = 10
plots, [0.46, 0.54], [0.46, 0.54], /normal
plots, [0.46, 0.54], [0.54, 0.46], /normal
device, set_graphics = 3

; update location bar
loc_string = string(state.mouse[0], $
                    state.mouse[1], $
                    main_image[state.mouse[0], $
                               state.mouse[1]], $
                    format = '("(",i4,",",i4,") ",g12.5)') 
widget_control, state.location_bar_id, $
  set_value = loc_string

end

;--------------------------------------------------------------------------

pro atv_event, event

; Main event loop for ATV widgets.

common atv_state
common atv_images
common atv_color, r_vector, g_vector, b_vector

widget_control, event.id, get_uvalue = uvalue

case uvalue of

    'zoom_mode': state.mode = 'zoom'
    'blink_mode': state.mode = 'blink'

    'atv_base': begin
        atv_resize, event
        atv_refresh
        widget_control, state.keyboard_text_id, set_value = ''
        widget_control, state.draw_base_id, /clear_events
    end

    'top_menu': begin
        widget_control, event.value, get_value = event_name
        parent = widget_info(event.value, /parent)
        widget_control, parent, get_value = parent_name
        
        case parent_name of
            
            'File': begin
                case event_name of
                    'ReadFits': begin
                        fitsfile = $
                          dialog_pickfile(filter = '*.fits', $
                                          /fix_filter, $
                                          group = state.base_id, $
                                          /must_exist, $
                                          /read, $
                                          title = 'Select Fits Image')
                        
                        if (fitsfile NE '') then begin
; note:  found that "readfits" chokes on some non-standard 
; fits files, but "fits_read" handles them ok.
                            fits_read, fitsfile, tmp_image
                            if ((size(tmp_image))[0] NE 2) then begin
                                print, 'Image must be a 2-d array!'
                            endif else begin
                                main_image = temporary(tmp_image)
                            endelse
                            atv_getstats
                            state.zoom_level = 1.0
                            atv_scaleimage
                            widget_control, $
                              state.min_text_id, $
                              set_value = string(state.min_value)
                            widget_control, $
                              state.max_text_id, $
                              set_value = string(state.max_value)
                            atv_makepan
                            atv_refresh
                        endif
                                                                    
                        widget_control, state.draw_base_id, /clear_events
                        widget_control, state.keyboard_text_id, set_value = ''
                    end
                    
                    'Quit': begin
                        atv_shutdown
                        widget_control, event.top, /destroy
                    end
                    else:
                endcase
            end

            'ColorMap': begin
                case event_name of
                    'Grayscale': atv_getct, 0
                    'Blue-White': atv_getct, 1
                    'Red-Orange': atv_getct, 3
                    'BGRY': atv_getct, 4
                    'Rainbow': atv_getct, 13
                    'ATV Special': atv_makect, event_name
                    else:
                endcase
                atv_stretchct
            end

            'Scaling':  begin
                case event_name of
                    'Linear': state.scaling = 0
                    'Log': state.scaling = 1
                    'HistEq': state.scaling = 2
                    else:
                endcase
                atv_scaleimage
                atv_makepan
                atv_refresh
                widget_control, state.draw_base_id, /clear_events
                widget_control, state.keyboard_text_id, set_value = ''
            end
            
            'Help': begin
                atv_help
            end

            else: print, 'Unknown event in file menu!'
        endcase
        
    end


; If the mouse enters the main draw base, set the input focus to
; the invisible text widget, for keyboard input.
; When the mouse leaves the main draw base, de-allocate the input
; focus by setting the text widget value.

    'draw_base': begin
        case event.enter of
            0: begin
                widget_control, state.keyboard_text_id, set_value = ''
            end
            
            1: begin
                widget_control, state.keyboard_text_id, /input_focus
            end
        endcase      
    end





    'draw_window': begin

        if (event.type EQ 2) then begin      ; motion event
            tmp_event = [event.x, event.y]
            state.mouse = $
              round( (0.5 > $
                      ((tmp_event / state.zoom_level) + state.offset) $
                      < (state.image_size - 0.5) ) - 0.5)
            atv_gettrack
        endif
               

        case state.mode of                   ; button events

            'blink': begin
                case event.type of                    
                    0: begin                     ; button press
                        wset, state.draw_window_id
                        if n_elements(blink_image1) GT 0 then $
                          tv, blink_image1
                    end

                    1: begin                     ; button release
                        wset, state.draw_window_id
                        tv, display_image
                        
                    end
                    else:
                endcase
            end

            
            'zoom': begin
                if (event.type EQ 0) then begin  ; button press

                    case event.press of
                        1: begin             ; zoom in and center
                            atv_drawbox
                            state.zoom_level = (state.zoom_level * 2) < 64
                            state.centerpix = state.mouse
                            state.offset = $
                              round( state.centerpix - $
                                     (0.5 * state.draw_window_size / $
                                      state.zoom_level) )
                            atv_drawbox
                            atv_getdisplay
                        end
                        
                        2: begin             ; center on current posn
                            atv_drawbox
                            state.centerpix = state.mouse
                            state.offset = $
                              round( state.centerpix - $
                                     (0.5 * state.draw_window_size / $
                                      state.zoom_level) )
                            atv_drawbox
                            atv_getdisplay
                        end
                        
                        4: begin             ; zoom out and center
                            atv_drawbox
                            state.zoom_level = $
                              (state.zoom_level / 2) > (1/16.)
                            state.centerpix = state.mouse
                            state.offset = $
                              round( state.centerpix - $
                                     (0.5 * state.draw_window_size / $
                                      state.zoom_level) )
                            atv_drawbox
                            atv_getdisplay
                        end
                        
                    endcase
                    
                    newpos = $
                      (state.mouse - state.offset + 0.5) * $
                      state.zoom_level                        
                    wset,  state.draw_window_id
                    tvcrs, newpos[0], newpos[1], /device 
                    atv_gettrack
                    
                endif
                
            end

        endcase
        widget_control, state.keyboard_text_id, /input_focus
                    
    end




    'brightness': atv_stretchct
    'contrast'  : atv_stretchct

    'invert': begin
        state.invert_colormap = abs(state.invert_colormap - 1)
        tvlct, r, g, b, /get

        r = abs( r - 255 )
        g = abs( g - 255 )
        b = abs( b - 255 )
        r_vector = abs( r_vector - 255 )
        g_vector = abs( g_vector - 255 )
        b_vector = abs( b_vector - 255 )

        tvlct, r, g, b

        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end

    'reset_color': begin
        widget_control, $
          state.brightness_slider_id, set_value = !d.table_size 
        widget_control, $
          state.contrast_slider_id, set_value = 50
        atv_stretchct

        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end


    'min_text': begin
        atv_set_minmax, uvalue, event.value
        atv_scaleimage
        atv_makepan
        atv_refresh
    end

    'max_text': begin
        atv_set_minmax, uvalue, event.value
        atv_scaleimage
        atv_makepan
        atv_refresh
    end

    'autoscale_button': begin
        atv_autoscale
        atv_scaleimage
        atv_makepan
        atv_refresh

        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end

    'full_range': begin
        state.min_value = state.image_min
        state.max_value = state.image_max
        if state.min_value GE state.max_value then begin
            state.min_value = state.max_value - 1
            state.max_value = state.max_value + 1
        endif
        widget_control, state.min_text_id, set_value = state.min_value
        widget_control, state.max_text_id, set_value = state.max_value
        atv_scaleimage
        atv_makepan
        atv_refresh

        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end

    'set_blink1': begin
        blink_image1 = display_image
    end

;    'set_blink2': begin
;        blink_image2 = display_image
;    end
;
;    'set_blink3': begin
;        blink_image3 = display_image
;    end
    
    'keyboard_text': begin
        eventchar = string(event.ch)
        case eventchar of
            '1': atv_move_cursor, eventchar
            '2': atv_move_cursor, eventchar
            '3': atv_move_cursor, eventchar
            '4': atv_move_cursor, eventchar
            '6': atv_move_cursor, eventchar
            '7': atv_move_cursor, eventchar
            '8': atv_move_cursor, eventchar
            '9': atv_move_cursor, eventchar
            'r': atv_lineplot, eventchar
            'c': atv_lineplot, eventchar
            's': atv_lineplot, eventchar
            't': atv_lineplot, eventchar
            'p': atv_mapphot
            else:
        endcase
    end

    'zoom_in': begin
            state.zoom_level = (state.zoom_level * 2) < 64
            atv_refresh
            widget_control, state.draw_base_id, /clear_events
            widget_control, state.keyboard_text_id, set_value = ''
    end
        
    'zoom_out': begin
        state.zoom_level = (state.zoom_level / 2) > (1./16.)
        atv_refresh
        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end

    'zoom_one': begin
        state.zoom_level = 1.
        atv_refresh
        widget_control, state.draw_base_id, /clear_events
        widget_control, state.keyboard_text_id, set_value = ''
    end

    'center': begin
        atv_drawbox
        state.centerpix = round(state.image_size / 2.)
        state.offset = $
          round( state.centerpix - $
                 (0.5 * state.draw_window_size / state.zoom_level) )
        atv_drawbox
        atv_getdisplay
    end

    'pan_window': begin
        case event.type of
            2: begin                     ; motion event
                if (state.pan_track EQ 1) then begin
                    atv_pantrack, event
                endif
            end
            
            0: begin                     ; button press
                state.pan_track = 1
                atv_pantrack, event
            end
            1: begin                     ; button release
                state.pan_track = 0
                atv_getdisplay
            end
            else:
        endcase
    end

    'done': begin
        atv_shutdown
        widget_control, event.top, /destroy
    end

    else:  print, 'No match for uvalue....'

endcase

end
 
 
;------------------------------------------------------------------------

pro atv_drawbox

; routine to draw the box on the pan window, given the current center
; of the display image

common atv_state

wset, state.pan_window_id

view_min = round(state.centerpix - $
                  (0.5 * state.draw_window_size / state.zoom_level))
view_max = round(view_min + state.draw_window_size / state.zoom_level)

box_x = float((([view_min[0], $
                 view_max[0], $
                 view_max[0], $
                 view_min[0], $
                 view_min[0]]) * state.pan_scale) + state.pan_offset[0]) 

box_y = float((([view_min[1], $
                 view_min[1], $
                 view_max[1], $
                 view_max[1], $
                 view_min[1]]) * state.pan_scale) + state.pan_offset[1]) 

device, set_graphics = 6
plots, box_x, box_y, /device, thick = 2
device, set_graphics = 3

end

;-----------------------------------------------------------------------

pro atv_shutdown

; routine to clear the variables in the atv_images common block to 
; conserve memory when quitting atv.  Since we can't delvar these
; variables, just set them equal to zero so they don't
; take up a lot of space.  Also clear the state and the color map vectors.

common atv_images
common atv_state
common atv_color

main_image = 0
display_image = 0
scaled_image = 0
blink_image1 = 0
pan_image = 0
r_vector = 0
g_vector = 0
b_vector = 0
state = 0

end

;-----------------------------------------------------------------------
pro atv_pantrack, event

; routine to track the view box in the pan window during cursor motion

common atv_state

; erase the old box
atv_drawbox

; get the new box coords and draw the new box

tmp_event = [event.x, event.y]

newpos = state.pan_offset > tmp_event < $
  (state.pan_offset + (state.image_size * state.pan_scale))

state.centerpix = round( (newpos - state.pan_offset ) / state.pan_scale)

atv_drawbox

state.offset = round( state.centerpix - $
 (0.5 * state.draw_window_size / state.zoom_level) )

end

;-----------------------------------------------------------------------

pro atv_resize, event

; Routine to resize the draw window when a top-level resize event
; occurs.

common atv_state

tmp_event = [event.x, event.y]

window = (state.base_min_size > tmp_event)

newbase = window - state.base_pad

newsize = window - state.pad

widget_control, state.draw_base_id, $
  xsize = newbase[0], ysize = newbase[1]
widget_control, state.draw_widget_id, $
  xsize = newsize[0], ysize = newsize[1]

state.draw_window_size = newsize

end

;------------------------------------------------------------------------

pro atv_scaleimage

; Create a byte-scaled copy of the image, scaled according to
; the state.scaling parameter.

common atv_state
common atv_images

case state.scaling of
    0: tmp_image = bytscl(main_image, $                           ; linear
                          min=state.min_value, $
                          max=state.max_value, $
                          top = !d.table_size)
    
    1: tmp_image = $
      bytscl( alog10 (bytscl(main_image, $                        ; log
                             min=state.min_value, $
                             max=state.max_value, $
                             top = !d.table_size) + 1))
    
    2: tmp_image = bytscl(hist_equal(main_image, $
                                     minv = state.min_value, $    ; histeq
                                     maxv = state.max_value), $
                          top = !d.table_size)
    
endcase

scaled_image = bytarr(state.image_size[0] + 10, $
                             state.image_size[1] + 10)

scaled_image[5, 5] = temporary(tmp_image)

end

;------------------------------------------------------------------------

pro atv_getdisplay

; make the display image

common atv_state
common atv_images

display_image = $
  bytarr(state.draw_window_size[0] + 2 * (round(state.zoom_level) > 1), $
         state.draw_window_size[1] + 2 * (round(state.zoom_level) > 1))

view_min = round(state.centerpix - $
                  (0.5 * state.draw_window_size / state.zoom_level))
view_max = round(view_min + state.draw_window_size / state.zoom_level)



view_min = (0 > view_min < (state.image_size - 1)) + 5
view_max = (0 > view_max < (state.image_size - 1)) + 5

newsize = round( (view_max - view_min + 1) * state.zoom_level) > 1
startpos = abs( round(state.offset * state.zoom_level) < 0)

tmp_image = congrid(scaled_image[view_min[0]:view_max[0], $
                                            view_min[1]:view_max[1]], $
                                            newsize[0], newsize[1])
display_image[startpos[0], startpos[1]] = tmp_image

wset, state.draw_window_id
erase
tv, display_image

end

;----------------------------------------------------------------------


pro atv_makepan

; Make the 'pan' image that shows a miniature version of the full image.

common atv_state
common atv_images

sizeratio = state.image_size[1] / state.image_size[0]

if (sizeratio GE 1) then begin
    state.pan_scale = float(state.pan_window_size) / float(state.image_size[1])
endif else begin
    state.pan_scale = float(state.pan_window_size) / float(state.image_size[0])
endelse

tmp_image = $
  scaled_image[5:state.image_size[0]+4, 5:state.image_size[1]+4]

pan_image = congrid(tmp_image, round(state.pan_scale * state.image_size[0]), $
                    round(state.pan_scale * state.image_size[1]) )

state.pan_offset[0] = round((state.pan_window_size - (size(pan_image))[1]) / 2)
state.pan_offset[1] = round((state.pan_window_size - (size(pan_image))[2]) / 2)

end


;------------------------------------------------------------------------

pro atv_move_cursor, direction

; Use keypad arrow keys to step cursor one pixel at a time.
; Get the new track image, and update the cursor position.

common atv_state

i = 1L

case direction of
    '2': state.mouse[1] = max([state.mouse[1] - i, 0])
    '4': state.mouse[0] = max([state.mouse[0] - i, 0])
    '8': state.mouse[1] = min([state.mouse[1] + i, state.image_size[1] - i])
    '6': state.mouse[0] = min([state.mouse[0] + i, state.image_size[0] - i])
    '7': begin
        state.mouse[1] = min([state.mouse[1] + i, state.image_size[1] - i])
        state.mouse[0] = max([state.mouse[0] - i, 0])
    end
    '9': begin
        state.mouse[1] = min([state.mouse[1] + i, state.image_size[1] - i])
        state.mouse[0] = min([state.mouse[0] + i, state.image_size[0] - i])
    end
    '3': begin
        state.mouse[1] = max([state.mouse[1] - i, 0])
        state.mouse[0] = min([state.mouse[0] + i, state.image_size[0] - i])
    end
    '1': begin
        state.mouse[1] = max([state.mouse[1] - i, 0])
        state.mouse[0] = max([state.mouse[0] - i, 0])
    end

endcase

newpos = (state.mouse - state.offset + 0.5) * state.zoom_level

wset,  state.draw_window_id
tvcrs, newpos[0], newpos[1], /device

atv_gettrack

; Prevent keyboard_text buffer from storing large numbers of
; events if keyboard button is held down:

widget_control, state.keyboard_text_id, /clear_events

; Prevent the cursor move from causing a mouse event in the draw window

widget_control, state.draw_widget_id, /clear_events

end


;--------------------------------------------------------------------------

pro atv_set_minmax, uvalue, newvalue

; Change the min and max when user inputs new numbers in the text boxes.

common atv_state

case uvalue of
    
    'min_text': begin
        if (newvalue LT state.max_value) then begin
            state.min_value = newvalue
        endif
    end

    'max_text': begin
        if (newvalue GT state.min_value) then begin
            state.max_value = newvalue
        endif
    end
        
endcase

widget_control, state.min_text_id, set_value = string(state.min_value)
widget_control, state.max_text_id, set_value = string(state.max_value)

end


;----------------------------------------------------------------------

pro atv_refresh

; Re-create the display, track, and pan images, and re-display 
; everything.

common atv_state
common atv_images

state.offset = round(state.centerpix - $
                     (0.5 * state.draw_window_size / state.zoom_level))

atv_getdisplay

; redisplay the pan image and plot the boundary box

wset, state.pan_window_id
erase
tv, pan_image, state.pan_offset[0], state.pan_offset[1]
atv_drawbox

; redisplay the tracking image

wset, state.track_window_id
atv_gettrack

wset, state.draw_window_id

end

;----------------------------------------------------------------------

pro atv_stretchct

; Change brightness and contrast according to slider values.
; For contrast, use same algorithm as IDL 'stretch' routine.
; For brightness, want a linear 'slide' of color table.
; Store the current color table in 3 vectors of length 
; (3 * !d.table_size), and when brightness slider moves,
; just 'slide' the color table along these larger vectors.

common atv_state
common atv_color, r_vector, g_vector, b_vector

widget_control, state.brightness_slider_id, $
  get_value = brightness

widget_control, state.contrast_slider_id, $
  get_value = contrast

gamma = 10^( (contrast/50.) - 1 )

case gamma of
    1.0: p = lindgen(!d.table_size)
    else: $
      p = long( ((findgen(!d.table_size) / !d.table_size ) ^ gamma) $
                * !d.table_size)
endcase

; use brightness slider value as zero-point of color table mapping.

r = r_vector[p + brightness]
g = g_vector[p + brightness]
b = b_vector[p + brightness]
tvlct, r, g, b

end


;----------------------------------------------------------------------

pro atv_getct, tablenum

; Read in a pre-defined color table, and invert if necessary.

common atv_color, r_vector, g_vector, b_vector
common atv_state, state

loadct, tablenum, /silent
tvlct, r, g, b, /get

if (state.invert_colormap EQ 1) then begin
    r = abs (r - 255)
    g = abs (g - 255)
    b = abs (b - 255)
endif

r_vector(!d.table_size) = r
g_vector(!d.table_size) = g
b_vector(!d.table_size) = b

end

;----------------------------------------------------------------------

pro atv_autoscale

; Routine to auto-scale the image.

common atv_state 
common atv_images

med = median(main_image)
sig = stdev(main_image)

state.min_value = (med - (2 * sig))  > min(main_image)
state.max_value = (med + (10 * sig)) < max(main_image)

if (state.min_value GE state.max_value) then begin
    state.min_value = state.min_value - 1
    state.max_value = state.max_value + 1
endif

widget_control, state.max_text_id, set_value = string(state.max_value)
widget_control, state.min_text_id, set_value = string(state.min_value)

end  


;----------------------------------------------------------------------

pro atv_lineplot, eventchar

; Create column, row, contour, and surface plots.

common atv_state
common atv_images

if (not (xregistered('atv_lineplot'))) then begin
    lineplot_base =  widget_base(/floating, $
                                 group_leader = state.base_id, $
                                 /column, $
                                 /base_align_right, $
                                 title = 'atv plot', $
                                 /tlb_size_events, $
                                 uvalue = 'lineplot_base')
    state.lineplot_base_id = lineplot_base

    lineplot_window = widget_draw(lineplot_base, $
                                  frame = 0, $
                                  scr_xsize = state.lineplot_size[0], $
                                  scr_ysize = state.lineplot_size[1], $
                                  uvalue = 'lineplot_window')
    state.lineplot_widget_id = lineplot_window

    lineplot_done = widget_button(lineplot_base, $
                                  value = 'Done', $
                                  uvalue = 'lineplot_done')

    widget_control, lineplot_base, /realize
    widget_control, lineplot_window, get_value = tmp_value
    state.lineplot_window_id = tmp_value

    basegeom = widget_info(lineplot_base, /geometry)
    drawgeom = widget_info(lineplot_window, /geometry)

    state.lineplot_pad[0] = basegeom.xsize - drawgeom.xsize
    state.lineplot_pad[1] = basegeom.ysize - drawgeom.ysize

endif

wset, state.lineplot_window_id
erase

case eventchar of
    'r': begin
        plot, main_image[*, state.mouse[1]], $
          xst = 3, yst = 3, psym = 10, $
          title = strcompress('Plot of row ' + $
                              string(state.mouse[1])), $
          xtitle = 'Column', $
          ytitle = 'Pixel Value'
    end

    'c': begin
        plot, main_image[state.mouse[0], *], $
          xst = 3, yst = 3, psym = 10, $
          title = strcompress('Plot of column ' + $
                              string(state.mouse[0])), $
          xtitle = 'Row', $
          ytitle = 'Pixel Value'
    end

    's': begin
        
        plotsize = $
          fix(min([50, state.image_size[0]/2., state.image_size[1]/2.]))
        center = plotsize > state.mouse < (state.image_size - plotsize) 
        
        surface, main_image[center[0]-plotsize:center[0]+plotsize-1, $
                            center[1]-plotsize:center[1]+plotsize-1], $
          title = strcompress('Surface plot of ' + $
                              strcompress('['+string(center[0]-plotsize)+ $
                                          ':'+string(center[0]+plotsize-1)+ $
                                          ','+string(center[1]-plotsize)+ $
                                          ':'+string(center[1]+plotsize-1)+ $
                                          ']', /remove_all)), $
          xtitle = 'X', ytitle = 'Y', ztitle = 'Pixel Value'
    end

    't': begin
        plotsize = $
          fix(min([50, state.image_size[0]/2., state.image_size[1]/2.]))
        center = plotsize > state.mouse < (state.image_size - plotsize) 
        
        contour_image =  main_image[center[0]-plotsize:center[0]+plotsize-1, $
                            center[1]-plotsize:center[1]+plotsize-1]
        if (state.scaling EQ 1) then begin
            contour_image = alog10(contour_image)
            logflag = 'Log'
        endif else begin
            logflag = ''
        endelse
            
        contour, contour_image, $
          nlevels = 10, $
          /follow, $
          title = $
          strcompress(logflag + ' Contour plot of ' + $
                      strcompress('['+string(round(center[0]-plotsize))+ $
                                  ':'+string(round(center[0]+plotsize-1))+ $
                                  ','+string(round(center[1]-plotsize))+ $
                                  ':'+string(round(center[1]+plotsize-1))+ $
                                  ']', /remove_all)), $
          xtitle = 'X', ytitle = 'Y'
    end

endcase

widget_control, state.lineplot_base_id, /clear_events
        
if (not(xregistered('atv_lineplot'))) then begin
    xmanager, 'atv_lineplot', lineplot_base, /no_block
endif

end

;------------------------------------------------------------------------

pro atv_lineplot_event, event

common atv_state

widget_control, event.id, get_uvalue = uvalue

case uvalue of

    'lineplot_done': widget_control, event.top, /destroy

    'lineplot_base': begin                       ; Resize event

        state.lineplot_size = [event.x, event.y]- state.lineplot_pad

        widget_control, state.lineplot_widget_id, $
          xsize = (state.lineplot_size[0] > 100), $
          ysize = (state.lineplot_size[1] > 100)
        
        wset, state.lineplot_window_id
;        erase

;        widget_control, state.lineplot_base_id, /clear_events

    end
    
else:

endcase

end

;--------------------------------------------------------------------------

pro atv_help
common atv_state, state

h = strarr(42)
h[0] =  'ATV HELP'
h[1] =  ''
h[2] =  'MENU BAR:'
h[3] =  'File.ReadFits:  read in a new fits image from disk'
h[4] =  'File.Quit:      quits atv'
h[5] =  'ColorMap Menu:  selects color table'
h[6] =  'Scaling Menu:   selects linear, log, or histogram-equalized scaling'
h[7] =  ''
h[8] =  'CONTROL PANEL ITEMS:'
h[9] =  'Brightness:     fairly self-explanatory'
h[10] = 'Contrast:       also fairly self-explanatory'
h[11] = 'Min:            shows min data value displayed; click to modify'
h[12] = 'Max:            shows max data value displayed; click to modify'
h[13] = 'Pan Window:     use mouse to drag the image-view box around'
h[14] = ''
h[15] = 'BUTTONS:'
h[16] = 'ZoomMode:       toggles zoom mode:'
h[17] = '                   button1 = zoom in & center'
h[18] = '                   button2 = center on current position'
h[19] = '                   button3 = zoom out & center'
h[20] = 'BlinkMode:      toggles blink mode:'
h[21] = '                   press mouse button in main window to show blink image'
h[22] = 'Invert:         inverts the current color table'
h[23] = 'ResetColor:     sets the sliders back to defaults'
h[24] = 'AutoScale:      sets min and max to show data values around histogram peak'
h[25] = 'FullRange:      sets min and max to show the full data range of the image'
h[26] = 'ZoomIn:         zooms in by x2'
h[27] = 'ZoomOut:        zooms out by x2'
h[28] = 'Zoom1:          sets zoom level to original scale'
h[29] = 'Center:         centers image on display window'
h[30] = 'SetBlink:       puts current image in blink buffer'
h[31] = 'Done:           quits atv'
h[32] = ''
h[33] = 'Keyboard commands in display window:'
h[34] = '    Numeric keypad (with NUM LOCK on) moves cursor'
h[35] = '    r: row plot'
h[36] = '    c: column plot'
h[37] = '    s: surface plot'
h[38] = '    t: contour plot'
h[39] = '    p: aperture photometry at current position'

if (not (xregistered('atv_help'))) then begin
    help_base =  widget_base(/floating, $
                             group_leader = state.base_id, $
                             /column, $
                             /base_align_right, $
                             title = 'atv help', $
                             uvalue = 'help_base')

    help_text = widget_text(help_base, $
                            /scroll, $
                            value = h, $
                            xsize = 75, $
                            ysize = 24)
    
    help_done = widget_button(help_base, $
                              value = 'Done', $
                              uvalue = 'help_done')

    widget_control, help_base, /realize
    xmanager, 'atv_help', help_base, /no_block
    
endif

end

;--------------------------------------------------------------------------

pro atv_help_event, event

widget_control, event.id, get_uvalue = uvalue

case uvalue of
    'help_done': widget_control, event.top, /destroy
    else:
endcase

end

;-------------------------------------------------------------------------

pro atv_mapphot_refresh

; Aperture photometry routine by W. Colley, adapted for 
; inclusion in ATV by AJB

common atv_state
common atv_images

; coarse center on the star

xmin = (state.cursorpos[0] - ((state.centerboxsize - 1) / 2)) > 0
xmax = (xmin + state.centerboxsize) < (state.image_size[0] - 1)
ymin = (state.cursorpos[1] - ((state.centerboxsize - 1) / 2)) > 0
ymax = (ymin + state.centerboxsize) < (state.image_size[1] - 1)

small_image = main_image[xmin:xmax, ymin:ymax]

nx = (size(small_image))[1]
ny = (size(small_image))[2]

if (total(small_image) EQ 0.) then small_image = small_image + 1.

tt = findgen(nx)#(fltarr(ny)+1)
xcenter = round(total(tt*small_image)/float(total(small_image)))
tt = (fltarr(nx)+1)#findgen(ny)
ycenter = round(total(tt*small_image)/float(total(small_image)))

x = 0 > (xcenter + xmin) < (state.image_size[0] - 1)
y = 0 > (ycenter + ymin) < (state.image_size[1] - 1)

; calculate the sky

xmin = (x - state.outersky) > 0
xmax = (xmin + (2 * state.outersky + 1)) < (state.image_size[0] - 1)
ymin = (y - state.outersky) > 0
ymax = (ymin + (2 * state.outersky + 1)) < (state.image_size[1] - 1)

small_image = main_image[xmin:xmax, ymin:ymax]
nx = (size(small_image))[1]
ny = (size(small_image))[2]
i = lindgen(nx)#(lonarr(ny)+1)
j = (lonarr(nx)+1)#lindgen(ny)
xc = x - xmin
yc = y - ymin

w = where( (((i - xc)^2 + (j - yc)^2) GE state.innersky^2) AND $
           (((i - xc)^2 + (j - yc)^2) LE state.outersky^2),  nw)
if nw EQ 0 then begin
    print, 'No pixels in sky!!!!'
    xcent = -1.
    ycent = -1.
    sky = -1.
    flux = -1.
    goto, BADSKIP
endif

if nw GT 0 then sky = median(small_image(w))

; do the photometry

emask = (main_image lt -200.) * $
  ((main_image eq -999.) + 2*(main_image eq -1000.) + 4*(main_image eq -1001.))

mag = 1.

s = size(main_image)
nxi = s[1]
nyi = s[2]

instr = string(0)

nx = ceil(state.r*2.)+4
pi = !pi
twopi = pi*2.

flux = float(x-x)
xcent = flux
ycent = flux
s = size(x)


dist = 0

s = size(badsources)
if ((s[1] eq 0) and (s[0] eq 0)) then badsources = 0B

i = findgen(nx)-float(nx)*0.5
ii0 = i # (i-i+1)
jj0 = (i-i+1) # i

i = (findgen(nx*mag)-(float(nx)*0.5)*mag - mag*0.5 + 0.5) / mag
ii1 = i # (i-i+1)
jj1 = (i-i+1) # i

str = string(0)
    
xcent = 0.
ycent = 0.

if (badsources gt 0) then begin
    
    xcent = -1.
    ycent = -1.
    flux = -1.
    print, 'Bad data values: photometry no good at that position!'
    goto, BADSKIP
endif

ix = floor(x)
iy = floor(y)

xi = (x-float(ix))
yi = (y-float(iy))

rx = ii0-xi
ry = jj0-yi

mask0 = (rx*rx + ry*ry le (state.r-0.5)^2)
clipmask = (rx*rx + ry*ry le (state.r+0.5)^2)

rx = ii1-xi
ry = jj1-yi

mask1 = (rx*rx + ry*ry le state.r^2)

bm0 = rebin(mask0,nx*mag,nx*mag,/sample)
mask2 = (mask1 eq 1) * (bm0 eq 0)

norm = total(mask1)

ix = floor(x)
iy = floor(y)

i1 = ix-nx/2
i2 = ix+nx/2-1
j1 = iy-nx/2
j2 = iy+nx/2-1

if ((i1 lt state.r) or (i2 gt nxi-state.r-1) or $
    (j1 lt state.r) or (j2 gt nyi-state.r-1)) then begin
        
    xcent = -1.
    ycent = -1.
    flux = -1.
    
endif else begin
    
    marr_sml = main_image[i1:i2,j1:j2] - sky
    marr = marr_sml
    earr = emask[i1:i2,j1:j2]
    
    if (total(clipmask*earr) gt 0) then begin
        badsources = max(clipmask*earr)
        xcent = -1.
        ycent = -1.
        flux = -1.
        print, 'Bad data values: photometry no good at that position!'
        goto,BADSKIP
        
    endif else begin
        t = marr*mask2+rebin(mask0*marr_sml,nx*mag,nx*mag,/sample)
        
        xcent = total(ii1*t)/total(t) + float(i1+nx/2)
        ycent = total(jj1*t)/total(t) + float(j1+nx/2)
        
        flux = total(marr_sml*mask0) + total(marr*mask2)
        
    endelse
endelse

BADSKIP: begin
end

; output the results
  
state.centerpos = [xcent, ycent]

tmp_string = string(state.cursorpos[0], state.cursorpos[1], $
                    format = '("Cursor position:  x=",i4,"  y=",i4)' )
tmp_string1 = string(state.centerpos[0], state.centerpos[1], $
                    format = '("Object centroid:  x=",f6.1,"  y=",f6.1)' )
tmp_string2 = string(flux, $
                    format = '("Object counts: ",g12.6)' )
tmp_string3 = string(sky, $
                    format = '("Sky level: ",g12.6)' )

widget_control, state.centerbox_id, set_value = state.centerboxsize
widget_control, state.cursorpos_id, set_value = tmp_string
widget_control, state.centerpos_id, set_value = tmp_string1
widget_control, state.radius_id, set_value = state.r 
widget_control, state.outersky_id, set_value = state.outersky
widget_control, state.innersky_id, set_value = state.innersky
widget_control, state.skyresult_id, set_value = tmp_string3
widget_control, state.photresult_id, set_value = tmp_string2

end

;-------------------------------------------------------------------------

pro atv_mapphot_event, event

common atv_state
common atv_images

widget_control, event.id, get_uvalue = uvalue

case uvalue of

    'centerbox': begin
        state.centerboxsize = long(event.value) > 0
        if ( (state.centerboxsize / 2 ) EQ $
             round(state.centerboxsize / 2.)) then $
          state.centerboxsize = state.centerboxsize + 1
        atv_mapphot_refresh
    end
        
    'radius': begin
        state.r = 1 > long(event.value) < state.innersky
        atv_mapphot_refresh
    end

    'innersky': begin
        state.innersky = state.r > long(event.value) < (state.outersky - 1)
        atv_mapphot_refresh
    end

    'outersky': begin
        state.outersky = long(event.value) > (state.innersky + 1)
        atv_mapphot_refresh
    end

    'mapphot_done': widget_control, event.top, /destroy
    else:
endcase

end

;--------------------------------------------------------------------------

pro atv_mapphot

; aperture photometry front end

common atv_state, state

state.cursorpos = state.mouse

if (not (xregistered('atv_mapphot'))) then begin

mapphot_base = widget_base(/floating, $
                           /base_align_left, $
                           group_leader = state.base_id, $
                           /column, $
                           title = 'atv aperture photometry', $
                           uvalue = 'mapphot_base')

tmp_string = string(1000, 1000, $
                    format = '("Cursor position:  x=",i4,"  y=",i4)' )

cursorpos = widget_label(mapphot_base, $
                         value = tmp_string, $
                         uvalue = 'cursorpos')
state.cursorpos_id = cursorpos

centerbox = cw_field(mapphot_base, $
                     /long, $
                     /return_events, $
                     title = 'Centering box size (pix):', $
                     uvalue = 'centerbox', $
                     value = state.centerboxsize, $
                     xsize = 5)
state.centerbox_id = centerbox

tmp_string1 = string(99999.0, 99999.0, $
                    format = '("Object centroid:  x=",f7.1,"  y=",f7.1)' )

centerpos = widget_label(mapphot_base, $
                         value = tmp_string1, $
                         uvalue = 'centerpos')
state.centerpos_id = centerpos

radius = cw_field(mapphot_base, $
                  /long, $
                  /return_events, $
                  title = 'Aperture radius:', $
                  uvalue = 'radius', $
                  value = state.r, $
                  xsize = 5)
state.radius_id = radius


innersky = cw_field(mapphot_base, $
                    /long, $
                    /return_events, $
                    title = 'Inner sky radius:', $
                    uvalue = 'innersky', $
                    value = state.innersky, $
                    xsize = 5)
state.innersky_id = innersky

outersky = cw_field(mapphot_base, $
                    /long, $
                    /return_events, $
                    title = 'Outer sky radius:', $
                    uvalue = 'outersky', $
                    value = state.outersky, $
                    xsize = 5)
state.outersky_id = outersky

tmp_string3 = string(10000000.00, $
                    format = '("Sky level: ",g12.6)' )

skyresult = widget_label(mapphot_base, $
                         value = tmp_string3, $
                         uvalue = 'skyresult')
state.skyresult_id = skyresult

tmp_string2 = string(1000000000.00, $
                    format = '("Object counts: ",g12.6)' )

photresult = widget_label(mapphot_base, $
                          value = tmp_string2, $
                          uvalue = 'photresult', $
                          /frame)
state.photresult_id = photresult

mapphot_done = widget_button(mapphot_base, $
                             value = 'Done', $
                             uvalue = 'mapphot_done')

widget_control, mapphot_base, /realize

endif



atv_mapphot_refresh

if (not(xregistered('atv_mapphot'))) then begin
    xmanager, 'atv_mapphot', mapphot_base, /no_block
endif

end

;-----------------------------------------------------------------------

; Main program routine for ATV.  If there is no current ATV session,
; then run atv_startup and xmanage the widgets.  If ATV already 
; exists, then display the new image to the current ATV window.

pro atv, image, $
             min = minimum, $
             max = maximum, $
             autoscale = autoscale,  $
             linear = linear, $
             log = log, $
             histeq = histeq

common atv_state
common atv_images

if ( (n_params() EQ 0) AND (xregistered('atv'))) then begin
    print, 'USAGE: atv, array_name'
    print, '            [,min = min_value] [,max=max_value]'
    print, '            [,/autoscale] [,/linear] [,/log] [,/histeq]'
    retall
endif

if ( (n_params() NE 0) AND (size(image))[0] NE 2) then begin
    print, 'Input data must be a 2-d array!'
    retall
endif


if ( (n_params() EQ 0) AND (not (xregistered('atv')))) then begin
    
    main_image = bytscl(dist(500,500)^2 * sin(dist(500,500)/2.)^2)

endif else begin

    main_image = 0
    scaled_image = 0
    display_image = 0
    main_image =  image

endelse


if (not (xregistered('atv'))) then begin

    state = {                   $
              base_id: 0L, $                 ; id of top-level base
              base_min_size: [512L, 512L], $ ; min size for top-level base
              draw_base_id: 0L, $            ; id of base holding draw window
              draw_window_id: 0L, $          ; window id of draw window
              draw_widget_id: 0L, $          ; widget id of draw widget
              track_window_id: 0L, $         ; widget id of tracking window
              pan_window_id: 0L, $           ; widget id of pan window
              location_bar_id: 0L, $         ; id of (x,y,value) label
              min_text_id: 0L,  $            ; id of min= widget
              max_text_id: 0L, $             ; id of max= widget
              menu_ids: lonarr(17), $        ; list of top menu items
              brightness_slider_id: 0L, $    ; id of brightness widget
              contrast_slider_id: 0L, $      ; id of contrast widget
              keyboard_text_id: 0L, $        ; id of keyboard input widget
              image_min: 0.0, $              ; min(main_image)
              image_max: 0.0, $              ; max(main_image)
              min_value: 0.0, $              ; min value mapped to colors
              max_value: 0.0, $              ; max value mapped to colors
              mode: 'zoom', $                ; zoom or blink
              draw_window_size: [512L, 512L], $    ; size of main draw window
              track_window_size: 0L, $       ; size of tracking window
              pan_window_size: 0L, $         ; size of pan window
              pan_scale: 0.0, $              ; magnification of pan image
              image_size: [0L,0L], $         ; size of main_image
              invert_colormap: 0L, $         ; invert colormap flag
              mouse: [0L, 0L],  $            ; cursor position in image coords
              scaling: 0L, $                 ; 0=lin, 1=log, 2=histeq
              offset: [0L, 0L], $            ; offset to viewport coords
              base_pad: [0L, 0L], $          ; padding around draw base
              pad: [0L, 0L], $               ; padding around draw widget
              zoom_level: 1.00, $            ; magnification of display
              centerpix: [0L, 0L], $         ; pixel at center of viewport
              pan_track: 0L, $               ; flag=1 while mouse dragging
              pan_offset: [0L, 0L], $        ; image offset in pan window
              lineplot_widget_id: 0L, $      ; id of lineplot widget
              lineplot_window_id: 0L, $      ; id of lineplot window
              lineplot_base_id: 0L, $        ; id of lineplot top-level base
              lineplot_size: [600L, 450L], $ ; size of lineplot window
              lineplot_pad: [0L, 0L], $      ; padding around lineplot window
              cursorpos: lonarr(2), $        ; cursor x,y for photometry
              centerpos: fltarr(2), $        ; centered x,y for photometry
              cursorpos_id: 0L, $            ; id of cursorpos widget
              centerpos_id: 0L, $            ; id of centerpos widget
              centerbox_id: 0L, $            ; id of centeringboxsize widget
              radius_id: 0L, $               ; id of radius widget
              innersky_id: 0L, $             ; id of inner sky widget
              outersky_id: 0L, $             ; id of outer sky widget
              skyresult_id: 0L, $            ; id of sky widget
              photresult_id: 0L, $           ; id of photometry result widget
              centerboxsize: 11L, $          ; centering box size
              r: 5L, $                       ; aperture photometry radius
              innersky: 10L, $               ; inner sky radius
              outersky: 20L $                ; outer sky radius
    }

    atv_startup

endif

atv_getstats

; check for command line keywords

if n_elements(minimum) GT 0 then begin
    state.min_value = minimum
endif

if n_elements(maximum) GT 0 then begin 
    state.max_value = maximum
endif

if state.min_value GE state.max_value then begin
    state.min_value = state.max_value - 1.
endif


if (keyword_set(autoscale)) then atv_autoscale

if (keyword_set(linear)) then state.scaling = 0
if (keyword_set(log))    then state.scaling = 1
if (keyword_set(histeq)) then state.scaling = 2

widget_control, state.min_text_id, set_value = state.min_value
widget_control, state.max_text_id, set_value = state.max_value

state.zoom_level = 1.0

; Set up the display image and display it.

atv_scaleimage
atv_makepan
atv_refresh


if (not(xregistered('atv'))) then begin
    xmanager, 'atv', state.base_id, /no_block
endif


end

