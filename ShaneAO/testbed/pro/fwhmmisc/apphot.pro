pro Apphot, image, xphot, yphot, aprad, inskyrad, outskyrad, out, $
            skyann, badpix=badpix, zpt=zpt, sky=sky, fixpix=fixpix, $
            overplot = overplot, $
            silent=silent 

;+
; program to do circular aperture photometry on an image
;  given aperture location, size, and inner and outer radius for
;  sky subtraction annulus.  Calculates sky by median.
;  Returns the net flux.
;
; to disable sky subtraction, just set inskyrad = outskyrad
; can input a constant sky value, if set this disables the sky annulus also
;
; writing to img, photimg, and skyimg as debugging checks
;
; out(0) = total flux
; out(1) = number of pixel in flux aperture
; out(2) = median sky level
; out(3) = number of pixel in sky annulus
; out(4) = net flux
; out(5) = zpt - 2.5*log10(netflux))  (magnitude)
; out(6) = sigma in sky annulus
; out(7) = fwhm calculated by finding the std dev of pixels (w/o sky)
; out(8) = pa
; out(9) = ellipticity
; out(10) = error in net flux (from sky error alone)
; out(11) = error in mags
; out(12) = # of bad pixels in phot aperture
;
; 7/01/94 M.C. Liu (UCB) 
;
; added 'zpt' = magnitude of a 1 count source (24.0 default)
; 2/18/95 MCL
;
; if pass it a bad pixel mask, will use 'fixpix'
; routine to interpolate for bad pixels before doing photometry,
;
; photometry is reproducible to 0.2% to that produced by IRAF's
; 'imexam' task; eliminated coordinate rounding to reduce the scatter
; 7/26/95 MCL
;
; added fwhm output -> seems to give overestimates 
; 8/1/95 MCL
;
; modified bad pixel handling: bad pixels are masked out, not
; interpolated (using FIXPIX) as was the case before
; 2/1/96 MCL
;
; by default bad pixels are excluded from the photometry
; (corrupting the total flux, but not the measured radial profile)
; but if set /fixpix, use 'FIXPIX' to interpolate bad pixels
; 04/22/96 MCL
;
; now does separate checks to see if phot & sky apertures are off image
; 09/16/98 MCL
;
; added /overplot: will show location of phot radii
;   if the image has already been shown with DISPLAY.PRO
; 02/14/99 MCL
;
; there is this issue of label pixels at their half pixel center,
;   which I haven't bothered with yet - means photometry may be off be
;   +/- 0.5 pixel
;
; does not handle pixels which are partially inside the flux aperture,
;   either pixels are in or not. Preferably, we'd like to use a simple
;   weighting scheme for these pixels (like in IRAF) by taking a fraction
;   of the pixel value equal to its fraction inside the aperture.
; would be slightly nicer if output was in form out a structure, instead
;   of having to know which numbers correspond to what
;
;-


; number of fields in output
NOUT =  13

if keyword_set(zpt) eq 0 then zpt = 26.0
if n_params() lt 4 then begin
	print,'apphot,image,x,y,aprad,(inskyrad),(outskyrad),(output),'
	print,'       [badpix=],[zpt='+strc(zpt)+'],[sky=],[fixpix],[overplot],[silent]'
	print,'   out(0)  = total flux'
	print,'   out(1)  = number of pixel in flux aperture'
	print,'   out(2)  = median sky level'
	print,'   out(3)  = number of pixel in sky annulus'
	print,'   out(4)  = net flux'
	print,'   out(5)  = zpt - 2.5*log10(netflux))  (magnitude)'
        print,'   out(6)  = sigma in sky annulus'
        print,'   out(7)  = fwhm of points in flux aperture' 
	print,'   out(8)  = pa'
	print,'   out(9)  = ellipticity'
	print,'   out(10) = error in net flux in counts'
	print,'   out(11) = error in net flux in mags'
        print,'   out(12) = # of bad pixels in phot aperture'
	return
endif


; check ski radii and set to default values if not given 
if n_params() lt 6 and n_elements(sky) eq 1 then begin
	inskyrad = aprad + 1.0
	outskyrad = inskyrad + 1.0
endif
if (aprad gt inskyrad) or (aprad gt outskyrad) or (inskyrad gt outskyrad) $
  or (aprad le 0.0) or (outskyrad le 0.0) or (inskyrad le 0.0) then begin 
	print,'** your radii are screwed up! **'
	return
endif


; DON'T round the x and y to nearest integer to reduce scatter 
x=xphot
y=yphot
;x = round(xphot)
;y = round(yphot)

; set up bad pixel mask if necessary
sz = size(image)
if keyword_set(badpix) eq 0 then badpix = fltarr(sz(1),sz(2)) + 1.0

; trim image to smallest size (may be rectangular), translate coord origin
;   (there must be a cute IDL way to do this neater)
x0 = floor(x-outskyrad) 
x1 = ceil(x+outskyrad)
y0 = floor(y-outskyrad) 
y1 = ceil(y+outskyrad)
if (x0 lt 0.0) or (x1 gt (sz(1)-1)) or (y0 lt 0.0) or (y0 gt (sz(2)-1)) then $
  message,' sky apertures are off the image!', /info
x0 = x0 > 0.0
x1 = x1 < (sz(1)-1)
y0 = y0 > 0.0
y1 = y1 < (sz(2)-1)
;print,'image trim: (',strc(fix(x0)),':',strc(fix(x1)),',',$
;		      strc(fix(y0)),':',strc(fix(y1)),')'
img = image(x0:x1,y0:y1)
bp = badpix(x0:x1,y0:y1)
xcen = x - x0
ycen = y - y0


; double check if phot apertures are off image (very bad!!)
x0p = floor(x-aprad) 
x1p = ceil(x+aprad)
y0p = floor(y-aprad) 
y1p = ceil(y+aprad)
if (x0p lt 0.0) or (x1p gt (sz(1)-1)) or  $
  (y0p lt 0.0) or (y0p gt (sz(2)-1)) then $
  message,' phot apertures are off the image!!!', /info


; makes an (aprad)*(aprad) array of (distance)^2 from center of aperture
;  routine from DIST_CIRCLE by W.Landman in Goddard IDL package
xs = x1 - x0 + 1
ys = y1 - y0 + 1
distsq = fltarr(xs,ys,/nozero)
xx = findgen(xs)
yy = findgen(ys)
x2 = (xx - xcen)^(2.0)
y2 = (yy - ycen)^(2.0)
for i = 0L,(ys-1) do $			; row loop
	distsq(*,i) = x2 + y2(i)

; mask out bad pixels
ww = where(bp eq 0, nbad)
if keyword_set(fixpix) and (nbad gt 0) then begin
    fixpix, img, bp, img, /silent
    w = where(bp eq 0, nbad)
endif else $
  if (nbad gt 0) then distsq(ww) = outskyrad^2.0 + 1.0


; get flux by masking out all pixels too far away
ap2 = aprad^(2.0)
wflux = where(distsq le ap2,nf)
flux = total(img(wflux))
peak =  max(img(wflux))
nadir =  min(img(wflux))


; error check: map of photometry region
photimg = fltarr(xs,ys)			; don't use /nozero!!
photimg(wflux) = 10.0


; get sky level by masking and then medianing remaining pixels
; note use of "gt" to avoid picking same pixels as flux aperture
;  (checked graphically and there is no overlap between the 2 regions)
;  can disable sky subtraction then by setting inskyrad=outskyrad
ns = 0
msky = 0.0
sigsky = 0.0
if n_elements(sky) ne 0 then begin
;  message,'using constant sky value', /info
  ns = 0
  msky = sky
  sigsky = 0.0
endif else begin
;  print,'finding sky level'
  in2 = inskyrad^(2.0)
  out2 = outskyrad^(2.0)
  wsky = where((distsq gt in2) and (distsq le out2),ns)
  if ns gt 0 then begin
	skyann = img(wsky)
	msky = median(skyann) 
	iterstat,skyann,imgst,/silent
;	print,msky - imgst(2)
	sigsky = imgst(3)
;	print,sigsky - stdev(skyann)

;	use Matt Richter's SKYMODE routine to find
;	sky level by fitting a histogram to the sky annulus points
;	msky = skymode(skyann,skysig)

; 	debugging checks
	skyimg = fltarr(xs,ys)			
	skyimg(wsky) = 5.0
	photimg = temporary(photimg) + skyimg
  endif
endelse


; find net flux and its error
netflux = flux - nf * msky
enetflux = sqrt(nf) * sigsky
if (enetflux gt 0) then sn = netflux/enetflux else sn=-999

; find fwhm
fwhm,img-msky,xcen,ycen,aprad,fwhm,pa,ellip,/silent


; find magnitude
if (netflux le 0) then begin
  mag  = -999. 
  emag = -999.
endif else begin
  mag = zpt - 2.5*alog10(netflux)
  emag = 2.5 / alog(10.) * enetflux/netflux
endelse


; print it out
if keyword_set(silent) eq 0 then begin
  print,'OBJ: (x,y) = ', printcoo(xphot, yphot), '    # badpix = ', strc(nbad)
  print,'     flux = ',strc(flux),', nflux = ',strc(nf),$
	', peak = ', strc(peak), ', nadir = ', strc(nadir)
  print,'     fwhm = ',strc(fwhm),', ellip = ',strc(ellip),', pa = ',strc(pa)
  print,'SKY:  skymed = ',strc(msky),', nsky = ',strc(ns), $
	', sigsky = ',strc(sigsky)
  print,'PHOT: net flux = ',strc(netflux),' +/- ',strc(enetflux)
  print,'      magnitude = ', strc(mag),' +/- ',strc(emag)
  print,'      S/N = ',strc(sn)
endif


; for some strange reason, if I do this test with "n_elements(out) eq 0"
;  it bombs, complaining about 'out' being undefined
if n_params() ge 7 then begin
	out = fltarr(NOUT)
	out(0) = flux
	out(1) = nf
	out(2) = msky
	out(3) = ns
	out(4) = netflux
	if (netflux le 0) then $
          out(5) = -999. $
        else $
          out(5) = zpt - 2.5*alog10(netflux)
        out(6) = sigsky
        out(7) = fwhm
        out(8) = pa
        out(9) = ellip
        out(10) = enetflux
        out(11) = emag
        out(12) = nbad
endif

if keyword_set(overplot) then $
  if (!d.window ge 0) then begin
    tvcircle, aprad, xphot, yphot, /data, col = 0
    if not(keyword_set(sky)) then begin
        tvcircle, inskyrad, xphot, yphot, /data
        tvcircle, outskyrad, xphot, yphot, /data
    endif
endif

end
