function readircal,imnum,imheader,path=path
; function to read IRCAL images from an arbitrary Lick machine
; and divide them by the number of coadds
; quick version		Bruce Macintosh		June 1999

	if n_elements(path) eq 0 then path='/home/callisto/ccdev/data'
; change as IRCAL data moves
	fname='ircal'+string(imnum,format="(I4.4)")+'.fits.Z'
;	print,fname
	image=readfits(path+'/'+fname,imheadertemp)
        coadds=sxpar(imheadertemp,'ncoadds')
;recover number of coadds
        image=image/coadds
;decoadd
        imheader=imheadertemp
	exptime=sxpar(imheadertemp,'coaddtim')
	filter1=sxpar(imheadertemp,'filter1')
	filter2=sxpar(imheadertemp,'filter2')
	print,fname,coadds,exptime/1000.,filter1,filter2,$
	format="(A14,2X,I3,' x ',F7.3,' s',4x,A8,4x,A8)"

	
return,image
end
