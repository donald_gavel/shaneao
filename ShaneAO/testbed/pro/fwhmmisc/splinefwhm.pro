function Splinefwhm, rad, prof, splrad, splprof

;+
; given a radial profile (counts vs radius) will use
; a spline to extract the FWHM
;
; INPUTS
; 	rad	list of radii
;	prof	corresponding avg counts at each radius
;		(assumes no sky level)
;
; RETURNS
;	fwhm	the derived fwhm
; 
; NOTES
;   I tried using the spline of inverse profile (x vs y) 
;   but this bad for low S/N since profile is then multivalued
;
; ALGORITHM
;   finds peak in radial profile, then marches along until finds
;   where radial profile has dropped to half of that,
;   assumes peak value of radial profile is at minimum radius
;
; Written by MCL(UCB): 08/01/95
; 02/22/99 (MCL): uses 50x sampling of profile, instead of 10x
;-




if n_params() lt 2 then begin
	print,'function splinefwhm,radius,profile,(splprof)'
	retall
endif


nrad = n_elements(rad)
if nrad ne n_elements(prof) then $
  message,'radius and profile have unequal # of elements'


; check the peak
w = where(prof eq max(prof))
if float(rad(w(0))) ne min(rad) then begin
;  message,'peak of profile not at minimum radius',/info
  return,999.
endif


;; now spline the inverse profile to find half-max
;return, 2. * spline(reverse(prof), reverse(rad), 0.5*max(prof))


; interpolate radial profile at 50 times as many points
splrad = min(rad) + findgen(nrad*50+1) * (max(rad)-min(rad)) / (nrad*50)


; spline the profile
splprof = spline(rad,prof,splrad)


; march along splined profile until cross 0.5*peak value
found = 0
i = 0
repeat begin
  if splprof(i) lt 0.5*max(splprof) then $
	found = 1 $
  else $
	i = i+1
endrep until (found)


if (i lt 2) then begin
;  message,'radial profile too sharply peaked for spline',/info
  return,999.
endif


; now interpolate across the 2 points straddling the 0.5*peak
fwhm = splrad(i)+splrad(i-1)


return,fwhm

end

