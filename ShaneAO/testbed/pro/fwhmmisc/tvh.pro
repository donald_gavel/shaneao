pro Tvh, img, lscl, uscl, min=min, max=max, cursor=cursor, mag=mag, $
         nodisplay=nodisplay, silent=silent

;+
; Display an image, using the image histogram to estimate
; a +/- 3 sigma stretch.  The range is sent to the common
; block variables 'cb_minl' and 'cb_maxl' (for use by other
; programs, e.g., DISPLAY2.PRO)
;
; Program iteratively examines the image histogram to 
; eliminate any outlier pixels before attempting to estimate
; the +/- 3 sigma range. This works quite well for images
; which have one set of very deviant high and/or low pixels,
; but this will fail if there are two sets of high or low pixels
; which are widely spaced (since the iterative scheme chops a 
; max of one bin from the top & one from the bottom of the histogram)
;
; INPUTS
;   img     the image 
;
; OUTPUTS
;   lscl    final display limits
;   uscl
;
; KEYWORD PARAMETERS
;   min     min value to display
;   max     max value to display
;   cursor  after displaying image, run RDPIX to exam the image
;   mag     magnification for image display
;   nodisp  don't display the image, only calculate the display limits
;   silent
; 
; NOTES
;   -automatically ignores pixels of value BADVAL (-1.0e6)
;     (convention for image processing routines): somewhat
;     unnecessary since the clipping routine will chop them out
;   -if both 'min' and 'max' are set, then skips iterative scheme
; 
; HISTORY: Written by JRG (UCB) 08/10/94
; 08/12/94 (MCL): display limits written to common block
; 04/18/96 (MCL): modifying binning routine to iterative procedure
;                 and reduced sensitivity to bad pixels -> better!
; 05/28/96 (MCL): bug fix in binning routine,
;                 will resort to using TVSCL if image has only 2 values
; 09/22/96 (MCL): changed binning algorithm again, 
;                   seems very robust (uses HISTOGRAM's reverse indices)
; 03/02/97 (MCL): uses only a subset of image pixels (MUCH faster)
;                 more sensible checking of when to use TVSCL 
;                 (now won't bomb for images w/small range of values)
;                 previous version saved as TVH_OLD.PRO
; 05/05/97 (MCL): added lscl & uscl outputs
; 10/27/97 (MCL): removed check if range of images < # of colors
; 09/04/98 (MCL): fixed bug - HISTOGRAM could crash if sampled pixels 
;                   were not floating point & bin size was non-integer
; 10/06/98 (MCL): improved function for logical masks (all 0's and 1's)
;
; Please send comments/questions to <mliu@astro.berkeley.edu>
;-


;on_error, 2   ; return to program calling this one if it bombs

if n_params() lt 1  then begin
    print, 'pro Tvh,image,lscl,uscl,[min=],[max=],[cursor],[mag=],[nodisplay],[silent]'
    return
endif


; common block stores calculated display limits
common Tvh, cb_minl, cb_maxl 


; bad pixel flag value (pixels <= BADVAL are ignored)
BADVAL =  -1e6


; check that we have a 2D array
sz = size(img)
if sz(0) ne 2 then begin
    message, 'image is not 2D!', /info
    retall
endif


; initialize
i = 0L
sum = 0.0D
MN =  0.025                   ; lower percentile for pixel values
MX =  0.975                   ; upper percentile for pixel values
nbin =  512 < sz(sz(0)+2)/2   ; # of bins for creating pixel histogram
if keyword_set(min) then minl = min
if keyword_set(max) then maxl = max


;; check if range of values <= number of available colors
;if (max(img)-min(img)) le (!d.n_colors) then begin
;    if not(keyword_set(nodisplay)) then tvscl, img
;    cb_maxl = max(img)
;    cb_minl = min(img)
;    if not(keyword_set(silent)) then $
;      message, 'final disp limits = '+strc(cb_minl)+', '+strc(cb_maxl), /info
;    return
;endif 


; extract a subset of pixels
MINPIX =  1024 <  sz(sz(0)+2)
MAXPIX =  16384L
npix = sz(sz(0)+2)/4 <  MAXPIX >  MINPIX
ii = indgen(NPIX) * round((sz(1)*sz(2))/NPIX)
cut = float(img(ii))
cut = cut(where(cut ne BADVAL))    
imn = min(cut)
imx = max(cut)
cb_minl = imn
cb_maxl = imx


; check if image is only one or two good values
if (imn eq imx) then begin
    message, 'image is entirely one good value!', /info
    if not(keyword_set(nodisplay)) then tvscl, img
    return
endif else begin
    w = where(cut ne imx and cut ne imn, nn)
    if (nn eq 0) then begin
        if not(keyword_set(nodisplay)) then tvscl, img
        message, 'image is has only two good values!', /info
        return
    endif
endelse


; calculate histogram of all good pixels and corresponding pixel values
; make sure there is >1 bin
bin = (imx - imn) / double(nbin-1.0)
h = histogram(cut, bin = bin, omax = maxh, omin = minh, rev = rr)
wh = where(h ne 0, ngood)
;print, 'nbins = ', n_elements(h), min(cut), max(cut)
;xh = lindgen(n_elements(h)) * bin + min(cut)         


;------------------------------------------------------------
; if user hasn't set both MIN and MAX, 
;   iteratively go through histogram of pixel values to determine
;   sensible display limits, by chopping up to one bin  
;   from the top & one from the bottom of the pixel histogram
;   (bins are only chopped if they are outliers)
; if both bins are outliers, toss first the bin with fewer pixels
;------------------------------------------------------------
if not(keyword_set(min)) or not(keyword_set(max)) then begin
    found = 0
    repeat begin
        
;       see if top & bottom bin are outliers, i.e the adjacent
;         bin in the histogram is empty. 
;       these variables = 0 if outliers, = 1 if not
        ntop = wh(ngood-2) eq (max(wh)-1)
        nbot = wh(1) eq (min(wh)+1)

;       if bin is an outlier, than exclude all pixels in that bin.
;       then use the reverse indices from HISTOGRAM to determine
;         the new display limit (min/max value of remaining pixels) -
;         confusing but it works!  The arrays 'wtop' and 'wbot' have the
;         array indices of the elements in the top & bottom bins.
        if (ntop lt nbot) and (found ne 1) then begin
            wtop =  rr(rr(wh(ngood-2)):rr(wh(ngood-2)+1)-1)
            imx =  max((cut)(wtop))
            found = found+1
;            message, 'chopping top bin', /info

        endif else if (nbot lt ntop) and (found ne 2) then begin
            wbot = rr(rr(wh(1)):rr(wh(1)+1)-1)
            imn =  min((cut)(wbot))
            found = found+2
;            message, 'chopping bottom bin', /info

        endif else if (nbot eq ntop) and (nbot eq 0) then begin
;            if (h(wh(0)) gt h(wh(ngood-1))) then begin
            if (h(wh(0)) gt h(wh(ngood-1))) and (found ne 1) then begin
                wtop = rr(rr(wh(ngood-2)):rr(wh(ngood-2)+1)-1)
                imx = max((cut)(wtop))
                found = found+1
;                message, 'both are outliers - chopping top bin (more ' + $
;                  'pixels)', /info 
            endif else begin
                if (found ne 2) then begin
                    wbot = rr(rr(wh(1)):rr(wh(1)+1)-1)
                    imn = min((cut)(wbot))
                    found = found+2
;                    message, 'both are outliers - chopping bottom bin ' + $
;                      '(more pixels)', /info
                endif else if (found eq 2) then begin
                    wtop = rr(rr(wh(ngood-2)):rr(wh(ngood-2)+1)-1)
                    imx = max((cut)(wtop))
                    found = found+1
;                    message, 'both are outliers - chopping top bin ' + $
;                      '(already chopped bottom)', /info
                endif else begin
                    found = 3
;                    message, 'both at outliers but have reached ' + $
;                      'chopping limit', /info 
                endelse
            endelse
            
        endif else begin
            found = 3
;            message, 'no outlying bins', /info
        endelse
        
        bin = (imx - imn) / double(nbin-1.0)
;        message, 'disp limits = '+strc(imn)+', '+strc(imx), /info
;        print, found, ntop, nbot, imx, imn, ngood, nbin, bin

        w =  where(cut gt imn and cut le imx, nw)
        if (nw gt 0) then begin
            cut = cut(w) 
            h = histogram(cut, bin = bin, omax = maxh, omin = minh, rev = rr)
            wh = where(h ne 0, ngood)
;            xh = lindgen(n_elements(h)) * bin + min(cut) 
        endif else $
            found = 3

    endrep until (found ge 3) 
endif



; total number of pixels within display limits
tot = total(h)     


; sanity check (effective for logical masks, with just 0's and 1's)
; added 10/06/98 
if (imn eq imx) then begin
    message, 'overriding and reverting to original stretch', /info
    minl = cb_minl
    maxl = cb_maxl

endif else begin

    if not(keyword_set(min)) then begin
        while sum le MN*tot do begin 
            sum = sum + h(i)
            i = i + 1
        endwhile
        
        if rr(i) lt rr(i+1) then begin
;        if rr(i) ne rr(i+1) then begin
            wmn =  rr(rr(i):rr(i+1)-1)
            minl = min(cut(wmn))
        endif else begin
            if (rr(i) lt n_elements(rr)) then $
              minl = cut(rr(rr(i))) $
            else $
              minl = min(cut)
        endelse
        ;    print, 'min diff = ', minl, i*bin+minh
        ;    minl = i*bin + minh

    endif    
    ;if not(keyword_set(silent)) then print, 'minimum = ', minl, mn 

    if not(keyword_set(max)) then begin
        while sum lt MX*tot do begin 
            sum = sum + h(i)
            i = i + 1
        endwhile
        
        if (rr(i) ne rr(i+1)) and (rr(i) lt n_elements(rr)) then begin
        ;    if (rr(i) ne rr(i+1)) then begin
            wmx =  rr(rr(i):rr(i+1)-1)
            maxl = max(cut(wmx))
        endif else begin
            maxl = cut(rr(rr(i) < n_elements(rr)-1))
        endelse
        
        ;    print, 'max diff = ', maxl, i*bin+minh
        ;    maxl = i*bin + minh 

    endif    
    ;if keyword_set(silent) eq 0 then print, 'maximum = ', maxl, mx
endelse


; display the image if desired
if keyword_set(nodisplay) eq 0 then begin
    if not(keyword_set(mag)) then tvscl, img > minl < maxl $
    else begin
        sz = size(img)
        tvscl, rebin(img, sz(1)*mag, sz(2)*mag, /sample) > minl < maxl
    endelse
endif


; write the display limits to the common block & output variables
cb_minl = minl
cb_maxl = maxl
lscl = minl
uscl = maxl
if not(keyword_set(silent)) then $
  message, 'final disp limits = '+strc(minl)+', '+strc(maxl), /info

if keyword_set(cursor) then rdpix, img


end

