function radav2,powspec,x0,y0,maskang=maskang,maskright=maskright,sigmap=sigmap,showmask=showmask
;function for radially averaging arbitrary images (adapted from radav)
;bmac dec 98
;note that all the mask stuff doesn't work yet because it was hardwired for
;128x256
sizep=size(powspec)
if n_elements(x0) eq 0 then x0=sizep(1)/2.0 - 0.5
if n_elements(y0) eq 0 then y0=sizep(2)/2.0 - 0.5

print,'radial average from center ',x0,y0
xs=sizep(1)
ys=sizep(2)
mask=intarr(xs,ys)+1

if (keyword_set(maskang)) then begin
print,'Warning: Masking not yet operational - results unpredictable'
ndeg = maskang
pi = !pi
degrees = pi/180
y = extend(findgen(32)/16 - 1,1,32)
x = rotate(y,3)
ang = atan(y,x)
u=(ang gt (-ndeg*degrees)) and (ang lt (ndeg*degrees))
u = u or shift(rotate(u,1),1,0) or shift(rotate(u,2),0,1) or rotate(u,3)
mask = 1 - u
;mask(117:128,128) = 1
;mask(128,117:139) = 1
;implicitly assumes that array is 129x256 anyway
endif
help,mask
if (keyword_set(maskright)) then begin
print,'Warning: Masking not yet operational - results unpredictable'
mask(xs-maskright:xs-1,*)=0
mask(xs-maskright:xs-1,(ys/2.)-30:(ys/2.)+30)=1
endif
if keyword_set(showmask) then exptv,mask,/nobox
xs=sizep(1)
ys=sizep(2)

dists=fltarr(xs,ys)

for i=0,xs-1 do begin
  for j=0,ys-1 do begin
    dists(i,j)=norm([float(i)-x0,float(j)-y0])
  endfor
endfor

dists=floor(dists+0.5)
print,min(dists)
size=sqrt(max([x0-2,xs-x0-2])^2+max([y0-2,ys-y0-2])^2)
sigmap=fltarr(size)

vals=fltarr(size)
for i=0,size-1 do begin
   valid=where(dists eq i and mask eq 1)
;   vals(i)=avg(powspec(where(dists eq i and mask eq 1)))
   if valid(0) gt 0 then vals(i) = avg(powspec(valid))
;   if keyword_set(sigmap) then begin
       if valid(0) gt 0 then sigmap(i)=sigma(powspec(valid))
;   endif
endfor
;print,sigmap(0:4)
return,vals
end


