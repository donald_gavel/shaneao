function makecirc,inarray,xc,yc,radius,value=value
;function to make a cicular mask of a given radius and add it into the
;current image
;currently works in integer pixels

if n_elements(value) eq 0 then value=1.0

rmap=radmap(inarray,xc,yc)
outim=inarray
outim(where(rmap le radius))=inarray( where(rmap le radius)) + value

return, outim
end
