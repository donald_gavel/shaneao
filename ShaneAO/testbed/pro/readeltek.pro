;+
;  readeltek.pro - read the image files
;    produced by the Eltek cameras by Holger Jones
;-

function readeltek,filename
  header = bytarr(172)
  data = bytarr(768,480)
  openr, unit, filename, error = err, /get_lun
  if (err ne 0) then begin
    print,'cannot open file ' + filename
    return,0
  endif
  readu, unit, header
  readu, unit, data
  close, unit
  return, data
end
