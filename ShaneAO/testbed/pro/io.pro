;+
; io.pro
;  IO - get Io data from the raw data disk and process
;   usage: .run, io
;
;-

pro getiodata,dataset,io,ioDark,iomdark,aveDark
  if n_params() eq 0 then begin
    print,"usage: getiodata, dataset, io,ioDark, iomdark, iomdarkb"
    return
  end
  io = mrdfits(dataset.datadir + dataset.iofile,0)
;  io = mrdfits("/duck1/gavel/keck/sep96/rawData/s00248.fits",0)
  ioDark = mrdfits(dataset.datadir + dataset.darkfile,0)
;  ioDark = mrdfits("/duck1/gavel/keck/sep96/rawData/s00249.fits",0)
  aveDark = total(ioDark,3)/100
  iomdark = make_array(256,256,100)
  for i=0,99 do iomdark(*,*,i) = io(*,*,i) - aveDark
end

;
;   get the sky data
;
function getiosky,dataset,badpix,sky,skyDark
  if n_params() eq 0 then begin
    print,"usage: skyframe = getiosky(dataset,badpix,[sky,skyDark])"
    return,0
  end
  sky = mrdfits(dataset.datadir + dataset.skyfile,0)
;  sky = mrdfits("/duck1/gavel/keck/sep96/rawData/s00305.fits",0)
  skyDark = mrdfits(dataset.datadir + dataset.skydarkfile,0)
;  skyDark = mrdfits("/duck1/gavel/keck/sep96/rawData/s00304.fits",0)
  skymdark = sky - skyDark
  skyframe = total(skymdark,3)/100;  average over the frames
  skyframe = skyframe*(1-badpix) + median(skyframe,5)*badpix
  br = total(skyframe,2)/256;   remove the vertical stripes
  brv = total(br)/256;
  bra = make_array(256,256)
  for i=0,255,1 do bra(*,i) = br
  skyframe = skyframe - bra + brv
  return, skyframe
end

;
;   make a flat frame
;
function makeflat,badpix
  flat690 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00690.fits",0)
  flat691 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00691.fits",0)
  flat692 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00692.fits",0)
  flat700 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00700.fits",0)
  flat701 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00701.fits",0)
  flat702 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00702.fits",0)
;          subtract darks, find incremental gains
  flat1mdark = flat691 - (flat690+flat692)/2
  flat2mdark = flat701 - (flat700+flat702)/2
  flatdif = flat2mdark - flat1mdark
;             remove bad pixels
  if n_elements(badpix) eq 0 then badpix = findbad(flatdif)
  fd = flatdif*(1-badpix) + median(flatdif,5)*badpix
;          normalize to one over non-divot area
  tmp = fd(*,100:255)
  nf = total(tmp)/n_elements(tmp)
  flat = fd/nf
;            set "start-up" pixels to one
  flat(0:2,0:5) = 1
  return,flat
end

;
;   make a flat frame - K-cont
;
function makeflatKcont,badpix
  flat690 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00712.fits",0)
  flat691 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00713.fits",0)
  flat692 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00714.fits",0)
  flat700 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00717.fits",0)
  flat701 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00718.fits",0)
  flat702 = mrdfits("/duck1/gavel/keck/sep96/rawData/s00719.fits",0)
;          subtract darks, find incremental gains
  flat1mdark = flat691 - (flat690+flat692)/2
  flat2mdark = flat701 - (flat700+flat702)/2
  flatdif = flat2mdark - flat1mdark
;             remove bad pixels
  if n_elements(badpix) eq 0 then badpix = findbad(flatdif)
  fd = flatdif*(1-badpix) + median(flatdif,5)*badpix
;          normalize to one over non-divot area
  tmp = fd(*,100:255)
  nf = total(tmp)/n_elements(tmp)
  flat = fd/nf
;            set "start-up" pixels to one
  flat(0:2,0:5) = 1
  return,flat
end

;
;   subtract sky frame from data
;
pro remsky,datain,skyframe,dataout
  if n_params() lt 2 then begin
    print,"usage: remsky,datain,skyframe,dataout"
    return
  end
  print,"remsky: subtracting sky background"
  if n_elements(dataout) eq 0 then dataout = make_array(256,256,100)
  for i=0,99 do begin
    dataout(*,*,i) = datain(*,*,i) - skyframe
  endfor
end

;
;   remove vertical and horizontal stripe patterns
;
pro devstripe,datain,dataout
  if n_params() eq 0 then begin
    print,"usage: devstripe,datain,dataout"
    return
  end
  print,"devstripe..."
  if n_elements(dataout) eq 0 then dataout = make_array(256,256,100)
  bra = make_array(256,256)
  for i=0,99 do begin
    print,format='($,I0," ")', i
    br = total(datain(0:255,222:255,i),2)/34  ;  vertical stripes
    for j=0,255,1 do bra(*,j) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end

pro dehstripe,datain,dataout
  if n_params() eq 0 then begin
    print,"usage: dehstripe,datain,dataout"
    return
  end
  print,"dehstripe..."
  if n_elements(dataout) eq 0 then dataout = make_array(256,256,100);
  bra = make_array(256,256)
  for i=0,99 do begin
    print,format='($,I0," ")', i
    br = total(datain(0:99,0:255,i),1)/100  ; horizontal stripes
    for j=0,255,1 do bra(j,*) = br
    dataout(*,*,i) = datain(*,*,i) - bra
  endfor
  print,"done"
end

;
;  find the bad pixels
;
function findbad,frame,thresh
  if n_params() eq 0 then begin
    print,"usage: badpix = findbad(frame[,thresh])"
    return,0
  end
  if n_elements(thresh) eq 0 then thresh = 23500
  badpix = frame gt thresh
  return, badpix
end 

;
;  remove the bad pixels from a datacube
;
pro debad,incube,outcube,badpix
  if n_params() eq 0 then begin
    print,"usage: debad,incube,outcube,badpix"
    return
  end
  if n_elements(outcube) eq 0 then outcube = make_array(256,256,100);
  goodpix = 1-badpix
  print,"removing bad pixels..."
  for i=0,99 do begin
    outcube(*,*,i) = incube(*,*,i)*goodpix + median(incube(*,*,i),5)*badpix
    print,format='($,I0," ")',i
  endfor
  print,"done"
  return
end

function supergauss,n,cx,cy,width,power
  result = make_array(n,n)
  x = double(extend(indgen(n),2,n))
  y = double(transpose(x))
  x = x - cx
  y = y - cy
  r = sqrt(x^2+y^2)
  fuzz = 10.^(-8)
  result = exp( - (r/width)^power )
  result = result>fuzz
  return,result
end

;
;  cshiftg - centroid shift and gaussian window
;
pro cshiftg,incube,outcube,threshold = th,gwsize = gws
  if n_elements(outcube) eq 0 then outcube = make_array(256,256,100)
  if n_elements(th) eq 0 then th = 40
  if n_elements(gws) eq 0 then gws = 55
  mask = make_array(256,256)
  sg = supergauss(256,128,128,gws,10)
  mask(5:250,5:250)=1
  print,"centeroid shift and window..."
  for i=0,99 do begin
    tmps = smooth(incube(*,*,i),5)
    center = centroid(((tmps>th)-th)*mask)
    outcube(*,*,i) = shift(incube(*,*,i),128-center(0),128-center(1))*sg
    print,format='($,I0," ")',i
  endfor
  print,"done"
end

;
;  extend - general purpose extend dimensions routine
;
function extend,vector,dimension,length
  n = n_elements(vector)
  if n_elements(length) eq 0 then m = n else m = length
  if dimension eq 1 then begin
    result = make_array(m,n)
    for i=0,m-1 do result(i,*) = vector
  endif else begin
    result = make_array(n,m)
    for i=0,m-1 do result(*,i) = vector
  endelse
  return,result
end

;
;  centroid
;
function centroid,array
  sv = size(array)
  n = sv(1)
  m = sv(2)
  x = extend(indgen(n),2,m)
  y = extend(indgen(m),1,n)
  nf = total(array)
  result = [total(x*array), total(y*array)]/nf
  return, result
end

function gauss,n,cx,cy,stddev
  result = make_array(n,n)
  x = extend(indgen(n),2,n)
  y = transpose(x)
  result = exp( - ((x-cx)^2 + (y-cy)^2)/stddev^2 )
  return,result
end

function circle,n,cx,cy,radius
  x = extend(indgen(n),2,n)
  y = transpose(x)
  x = x - cx
  y = y - cy
  r = sqrt(x^2 + y^2)
  result = r lt radius
  return, result
end

;
;  analyze the average power spectrum
;
pro analps, iomsf, aveps
  print,"computing average power spectrum"
  t = make_array(256,256,100)
  for i=0,99 do t(*,*,i) = abs(fft(iomsf(*,*,i)))^2
  aveps = total(t,3)/100
end

;
; do-it-all routine
;
pro doio, dataset, io, iomsky, iomsf, skyframe, flat, avedark
  getiodata, dataset, io, ioDark, iomdark, avedark
  badpix = findbad(io(*,*,0))
  debad,iomdark,iomdark,badpix
  if n_elements(skyframe) eq 0 then skyframe = getiosky(dataset,badpix)
  remsky, iomdark, skyframe, iomsky
  devstripe, iomsky, iomsky
  dehstripe, iomsky, iomsky
;  cshiftg, iomsky, iomskys  - no no, don't shift before flat-fielding
  print,"flat-fielding"
  if n_elements(flat) eq 0 then flat = makeflat(badpix)
  iomsf = make_array(256,256,100)
  for i=0,99 do iomsf(*,*,i) = iomsky(*,*,i)/flat
  cshiftg, iomsf, iomsfsw  ; shifted and windowed
;  analps, iomsfsw, aveps
  prepfile = dataset.basename + "_prep.fits"
  specklefile = dataset.basename + ".speckle"
; writefits,prepfile,long(iomsfsw)
;  --- at this point, process through the bispectrum code ---
;           fitstospeckle prepfile specklefile
;           frame io.frameparams basename
;  ----------------------------------------------------------
;  prepspd,dataset,ps,phase,ref_ps,mtf
;  ioimage = spdeconvolve(ps>1,phase,ref_ps,mtf(0:129,*),0,90,.99)
;  imagefile = basename + "_image.fits"
; writefits,dataset.resultsdir + imagefile,ioimage
end

pro prepspd,dataset,ps,phase,ref_ps,mtf
  if n_params() eq 0 then begin
    print,"usage: prepspd,ps,phase,ref_ps,mtf"
    return
  end
  ref_ps = rdview(dataset.resultsdir + dataset.starname + "_ps.sdt",129,256)
;  ref_ps = rdview("/duck1/gavel/keck/sep96/io/io242/star265_ps.sdt",129,256)
  ref_ps = ref_ps/max(ref_ps)
  mtf = rdview("/duck1/gavel/keck/sep96/io/io242/keckmtfKp.sdt",256,256)
  mtf = mtf/max(mtf)
  ps = rdview(dataset.resultsdir + dataset.basename + "_ps.sdt",129,256)
;  ps = rdview("/duck1/gavel/keck/sep96/io/io248/io248_ps.sdt",129,256)
  phase = rdview(dataset.resultsdir + dataset.basename + "_phase.sdt",129,256)
;  phase = rdview("/duck1/gavel/keck/sep96/io/io248/io248_phase.sdt",129,256)
  return
end

function spdeconvolve,ps,phase,ref_ps,tele_mtf,div_thresh,div_radius,alpha
  n = (size(ps))(2)
  mask = circle(n,n/2,n/2,div_radius)
  mag = sqrt( ps / ((1-alpha) + alpha*ref_ps )) * tele_mtf
  i = sqrt(complex(-1))
  mag = [mag,(reverse(reverse(mag,2)))(1:(n/2-1),*)]
  ph = [phase,(reverse(reverse(-phase,2)))(1:(n/2-1),*)]
  tmp = mag*exp(i*ph)*mask
  result = shift(abs(fft(tmp,/inverse)),n/2,n/2)
  return, result
end

pro pwd
  spawn,"pwd"
end

pro ls,dir
  if n_params() eq 0 then dir = "."
  spawn,"ls " + dir
end

dataset = {dataset, $
           basename:'', iofile:'', darkfile:'', $
           starname:'', starfile:'', stardarkfile:'', $
           skyfile:'',skydarkfile:'', $
           datadir:'', resultsdir:''}

badpixfile = "/duck1/gavel/keck/sep96/io/badpix.fits"
; badpix = mrdfits(badpixfile,0)

;  --- this structure determines the dataset to process ---
@dataset.pro
; ---------------------------------------------------------

  iofiles = ["s00231.fits", $ ; in eclipse
             "s00242.fits", $
             "s00245.fits", $
             "s00248.fits", $
             "s00250.fits", $ ; just out of eclipse
             "s00251.fits", $
             "s00315.fits"]   ; well out of eclipse

  darkfiles = ["s00230.fits", $
               "s00241.fits", $
               "s00244.fits", $
               "s00247.fits", $
               "s00249.fits", $ ; (nearest 250 and 251, just out of eclipse)
               "s00249.fits", $
               "s00314.fits"]

  starfiles = ["s00265.fits", $ ; sao 186886
               "s00268.fits", $
               "s00271.fits", $
               "s00281.fits", $ ; sao 187017
               "s00284.fits", $
               "s00294.fits"]

  stardarkfiles = ["s00264.fits", $
                   "s00267.fits", $
                   "s00270.fits", $
                   "s00280.fits", $
                   "s00283.fits", $
                   "s00293.fits"]
end
