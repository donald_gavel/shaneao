; scintillation simulation
;

i = complex(0.,1.)
microns = 1.0e-6
cm = 0.01
km = 1.0e3

n = 512
m = 512
nlayers = 7
h = [0,1.8,3.3,5.8,7.4,13.1,15.8]*km
cn2 = [0.646,.078,.119,.035,.025,.08,0.017]
r0 = 0.18
lambda = 0.5*microns
r0h = (r0^(-5./3)*cn2)^(-3./5.)
du = 0.005
Dtele = 1.0
ap = circle(n,n,n/2,n/2,Dtele/2/du,1.)
s = fltarr(n,m,nlayers)
print,'generating screens' & wait,.01
for layer = 0,nlayers-1 do begin
  print,'layer = ',layer & wait,.01
  f = screengen(n,m,r0h[layer],du)
  s[*,*,layer] = screengen(f,seed)
endfor
wf = complexarr(n,m,nlayers)
wf[*,*,layer-1] = ones(n,n)
print,'propagating' & wait,.01
for layer = nlayers-1,1,-1 do begin
  print,'layer = ',layer & wait,.01
  L = h[layer]-h[layer-1]
  wf[*,*,layer] *= exp(i*s[*,*,layer])
  wf[*,*,layer-1] = fresnel(wf[*,*,layer],du,L,lambda)
endfor
print,rms(abs(wf[*,*,0])^2-ap,ap)
end

  