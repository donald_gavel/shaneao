;
;  plot3slices.pro - slices of a psf from speckle
;
home = '/duck1/gavel/keck/sep96/titan/doubleStar342/'
if (n_elements(imspec) eq 0) then begin & $
  imspec = readfits(home+'speckleFrame348.fits') & $
  imave = readfits(home+'average348.fits') & $
  improc = readfits(home+'doubleStar348_image.fits') & $
  imspecn = imspec/total(imspec>0) & $
  imaven = imave/total(imave>0) & $
  improcn = improc/total(improc>0) & $
endif

if (!d.name eq 'X') then begin & $
  window,1,xsize=370,ysize=350 & $
endif
if (!d.name eq 'PS') then begin & $
  device,filename = 'psf3slices.ps',xsize=1.2*10*370/300.,ysize=1.2*10*350/300.,/encapsulated & $
endif

pixelscale = 0.02
x = (indgen(71)-35)*pixelscale
;
;   plot showing 3 slices
;
;window,0,xsize=370,ysize=350
xyouts,0,0,'!18',/normal
nf = max(improcn)
plot,x,(improcn(95:165,129)/nf)/10.>0,xtitle='arcseconds',ytitle='relative intensity',charsize=1.2,background=255,color=0
oplot,x,imaven(97:167,139)/nf,color=0
oplot,x,imspecn(108:178,151)/nf,color=0
xyouts,.64,.7,'!6reconstructed/10',/normal,/noclip,color=0
;xyouts,.64,.7,'reconstructed!3'+string("367b)+'!1810',/normal,/noclip
xyouts,.31,.37,'specklegram',/normal,/noclip,color=0
xyouts,.22,.2,'long exposure',/normal,/noclip,color=0
xyouts,.23,.86,'!18PSF',/normal,/noclip,size=2.3,color=0
xyouts,.25,.82,'!18slices',/normal,/noclip,color=0
if (!d.name eq 'PS') then device,/close
