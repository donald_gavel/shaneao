;
; fftw.pro
;  usage:
;    plan = fftw(a,/plan[,/inverse,/hinverse])
;    b = fftw(a,[plan],[/inverse,/hinverse])
;
function fftw,a,thePlan,plan=plan,inverse=inverse,hinverse=hinverse
    library = 'C:\cygwin\home\DonGavel\FFTW\IDLwrapper\fftw_wrapper.dll'

    n = (size(a))[1]
    m = (size(a))[2]

    if (keyword_set(plan)) then begin
        if (size(a,/tname) eq 'DCOMPLEX') then begin
            u = dcomplexarr(n,m)
            v = u
            if (keyword_set(inverse)) then sign = long(1) else sign = long(-1)
            flags = long(0)
            retval = call_external(library,'fftw_plan_dft_2d_wrapper',n,m,u,v,sign,flags)
        endif
        if (size(a,/tname) eq 'COMPLEX') then begin
            if (keyword_set(hinverse)) then begin
                u = complexarr(n/2+1,m)
                v = fltarr(n,m)
                flags = long(0)
                retval = call_external(library,'fftwf_plan_dft_c2r_2d_wrapper',n,m,u,v,flags)
            endif else begin
                u = complexarr(n,m)
                v = u
                if (keyword_set(inverse)) then sign = long(1) else sign = long(-1)
                flags = long(0)
                retval = call_external(library,'fftwf_plan_dft_2d_wrapper',n,m,u,v,sign,flags)
            endelse
        endif
        if (size(a,/tname) eq 'FLOAT') then begin
            u = fltarr(n,m)
            v = complexarr(n,m)
            flags = long(0)
            retval = call_external(library,'fftwf_plan_dft_r2c_2d_wrapper',n,m,u,v,flags)
        endif
        return,retval
    endif

    if (size(a,/tname) eq 'DCOMPLEX') then begin
        b = dcomplexarr(n,m)
        if (n_params() eq 2) then begin
            val = byte([0,0,1])
            retval = call_external(library,'fftw_execute_wrapper',a,b,thePlan,value=val)
        endif else begin
            retval = call_external(library,'fftw_execute_wrapper',a,b)
        endelse
    endif
    if (size(a,/tname) eq 'COMPLEX') then begin
        if (keyword_set(hinverse)) then begin
;            a_tmp = a[0:n/2,*]
            b = fltarr(n,m)
            if (n_params() eq 2) then begin
                val = byte([0,0,1])
                retval = call_external(library,'fftwf_execute_c2r_wrapper',a,b,thePlan,value=val)
            endif else begin
                retval = call_external(library,'fftwf_execute_c2r_wrapper',a,b)
            endelse
        endif else begin
            b = complexarr(n,m)
            if (n_params() eq 2) then begin
                val = byte([0,0,1])
                retval = call_external(library,'fftwf_execute_wrapper',a,b,thePlan,value=val)
            endif else begin
                retval = call_external(library,'fftwf_execute_wrapper',a,b)
            endelse
        endelse
    endif
    if (size(a,/tname) eq 'FLOAT') then begin
        b = complexarr(n/2+1,m)
        if (n_params() eq 2) then begin
            val = byte([0,0,1])
            retval = call_external(library,'fftwf_execute_r2c_wrapper',a,b,thePlan,value=val)
        endif else begin
            retval = call_external(library,'fftwf_execute_r2c_wrapper',a,b)
        endelse
;        b = [conj(reverse(b[1:n/2-1,*])),b]
;        b = shift(b,-n/2+1,0)
    endif

    if (keyword_set(inverse) or keyword_set(hinverse)) then b /= n*m
    return,b
end

n = long(64)
a = dcomplexarr(n,n)
b = dcomplexarr(n,n)
a += complex(1.)
for k=0,3 do a[k,0]=dcomplex(k+1,-k-1)
a -= average(a)

plan = fftw(a,/plan)
b = fftw(a,plan)

end

