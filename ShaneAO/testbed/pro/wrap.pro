; wrap.pro
;
;  wrap the input into a given peak to valley range
;
;                      ^Output
;                      |
;                      |
;        ^             |
;        | Range    /  | /    /
;        |         /   |/    /
;   -----|--------/----/----/-------------->Input
;        |       /    /|   /
;        |      /    / |  /
;        v             |
;                      |
;                      |
;                      |
;
;  This is different from the mod function, which
;  inverts its slope for negative inputs
;
;  usage: output = wrap(input,range)
;
function wrap,x,range
  mag = (x gt 0)*(x mod range)+(x lt 0)*(range-((-x) mod range))
  return,mag - range/2.
end
;
x = (findgen(200)-100)*.1
y = wrap(x,5.0)
plot,x,y
end
