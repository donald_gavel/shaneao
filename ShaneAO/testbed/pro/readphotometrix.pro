;+
;  readphotometrix.pro - read the photometrix format image files
;        produced by Mark Feldman's PIB camera
;-
.compile spmisheader__define
.compile readpmi
sPmisHeader__define
print,"example: ret=ReadPmi(<filename>,pHdr,a,/pmi)"
