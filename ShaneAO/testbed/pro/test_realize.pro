
PRO doc_widget2_event, ev
;  experimentally, the character width is 318/50 = 6.36 pixels per character width
;                  the character height is 25 pixels per line
;  help,/structure,ev
  if (widget_info(ev.id,/valid_id) eq 0) then return
  wig_name = widget_info(ev.id,/uname)
  geo = widget_info(ev.id,/geometry)
;  help,/structure,geo
  n = n_tags(geo)
  report = strarr(n+1,2)
  report[0] = '------ '+wig_name + ' ------ id:'+strtrim(ev.id,2)+' ------'
  names = tag_names(geo)
  for i = 0,n-1 do report[i+1,0] = names[i]+' '+strtrim(geo.(i),2)

  text = widget_info(ev.top,find_by_uname='text')
  message = wig_name+' id:'+strtrim(ev.id,2)+' '+strtrim(geo.xsize,2)+' x '+strtrim(geo.ysize,2)
  if widget_info(text,/valid_id) then widget_control,text,set_value=report

  silliness = 1
  if (silliness and (wig_name eq 'doneButton')) then begin
    if (tag_names(ev,/structure_name) eq 'WIDGET_TRACKING') then begin
      efac = 5
      if (ev.enter eq 1) then begin
        if (widget_info(ev.id,/type) ne -1) then widget_control,ev.id,set_value='DONE'
      endif else begin
        if (widget_info(ev.id,/type) ne -1) then widget_control,ev.id,set_value='Done'
      endelse
    endif
  endif
  
  if (tag_names(ev,/structure_name) eq 'WIDGET_BUTTON') then begin
    CASE wig_name OF 
      'doneButton': begin
        widget_control, ev.TOP, /DESTROY 
        end
      else: begin
        print,'<doc_widget2_event> no case match'
      end
    ENDCASE 
  endif
END 
 
PRO doc_widget2 
  frame = 1
  xpad = 1
  width = 350
  base = widget_base(column=1,uname='base',frame=frame,/tracking_events) 
    row1 = widget_base(base,column=1,xsize=100,xpad=xpad,ysize=22,uname='row1',frame=frame,/tracking_events)
    row2 = widget_base(base,column=3,uname='row2',xpad=xpad,frame=frame,/tracking_events)
      frameScroll = widget_slider(row2,/vertical,ysize=200,uname='frameScroll',/tracking_events,frame=frame)
      display = widget_draw(row2,xsize=200,ysize=200,uname='display',/tracking_events,frame=1)
      controlPanel = widget_base(row2,xsize=100,ysize=200,uname='controlPanel',/tracking_events,frame=frame)
    row3 = widget_base(base,column=3,uname='row3',xpad=xpad,frame=frame,/tracking_events)
      minScroll = widget_slider(row3,xsize=width/2,uname='minScroll',/tracking_events,frame=frame)
      maxScroll = widget_slider(row3,xsize=width/2,uname='maxScroll',/tracking_events,frame=frame)
    row4 = widget_base(base,column=3,uname='row4',xpad=xpad,frame=frame,/tracking_events)
      tbase = widget_base(row4,column=1,xsize=240,uname='tbase',frame=frame,/tracking_events)
        text = widget_text(tbase,ysize=13,uname='text',/tracking_events)
      sbase = widget_base(row4,column=1,xsize=width-240-50,uname='sbase',frame=frame,/tracking_events)
      bbase = widget_base(row4,row=1,xsize=50,ysize=186,/base_align_bottom,uname='bbase',frame=frame,/tracking_events)
        doneButton = widget_button(bbase,value='Done',uname='doneButton',/tracking_events)
  WIDGET_CONTROL, base, /REALIZE 
  XMANAGER, 'doc_widget2', base, /no_block
END 

;----------------------
percent = .01
inch = 140 ; pixels per inch
mm = inch/25.4
wsize = [5,5]*inch
space = 0
pad = 0
padplus = 0
if (!version.os_family eq 'Windows') then padplus = 5

doc_widget2
end
