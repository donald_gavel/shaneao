;+
;  Kolmogorov structure function example
;   for Claire's class
;  Oct 5, 2011
;-
; program switches
random = 0
; -----constants------
cm = 0.01
micron = 1.e-6
nm = 1.e-9
; -----parameters-------
n = 512
m = n
L = 20.
du = L/float(n)
r0 = 40.*cm
startSeed = 59
; ---------------------
if (n_elements(seed) eq 0) then begin
  seed = startSeed
  window,0,title='phase data'
  window,1,title='structure function'
  window,2,title='autocorrelation'
endif
if ((n_elements(seed_was) ne 0) and (random eq 0)) then seed = seed_was
lambda = 1.0*micron
nsh = 5

seed_was = seed
f = screengen(n,m,r0,du)
s = (lambda/(2*!pi))*screengen(f,seed)/micron

if (n_elements(id) eq 0) then begin
  disp,s,dx=du*[1,1],x0=-n/2*[1,1]*du,'Phase, microns'
  id = !disp.windows[!disp.wincount-1]
endif else begin
  dispWidget_newData,id,s
endelse

ss = s[n/4:3*n/4,n/2]
x = findgen(n/2)*du

; half wave lines (to be shown in light grey)
hw = lambda/micron/2.
ymin = min(ss)
ymax = max(ss)
yset = [0]
y = 0
while y lt ymax do begin & yset = [yset,y] & y += hw & endwhile
y = -hw
while y gt ymin do begin & yset = [y,yset] & y -= hw & endwhile
nylines = (size(yset))[1]

; r0 patches
xmin = min(x)
xmax = max(x)
xset = [0]
xx = 0
while xx lt xmax do begin & xset = [xset,xx] & xx += r0 & endwhile
nxlines = (size(xset))[1]

; autocorrelation
acf = fltarr(n/4)
r = fltarr(n/4)
for k = 0,n/4-1 do begin
  acf[k] = average(ss*shift(ss,k-n/8))
  r[k] = (k-n/8)*du
endfor
var = acf[n/8]
sf = 2*var - 2*acf
r53 = (lambda/(2*!pi))^2*6.88*(abs(r)/r0)^(5./3.)/micron^2

psave = !p
!p.background = 255
lightgrey = 200
!p.color = 0
!p.charsize = 4
!p.charthick = 2
!p.thick = 2
loadct,0

wset,0
xtitle = 'position, meters'
ytitle = 'phase, microns'
plot,x,ss,/nodata,xtitle=xtitle,ytitle=ytitle
oplot,x,ss
oplot,x,s[n/4-25:3*n/4-25,n/2],color=lightgrey
;oplot,x,s[n/4-50:3*n/4-50,n/2],color=lightgrey
for line = 0,nylines-1 do oplot,x,yset[line]*ones(n),color=lightgrey,linestyle=2
for line = 0,nxlines-1 do oplot,[xset[line],xset[line]],[ymin,ymax],color = lightgrey,linestyle = 2

wset,1
xtitle = 'separation, meters'
ytitle = 'struc fcn, microns^2'
plot,r,sf,xtitle=xtitle,ytitle=ytitle
oplot,r,r53,color=lightgrey

wset,2
xtitle = 'separation, meters'
ytitle = 'autocorrelation, microns^2'
plot,r,acf,xtitle=xtitle,ytitle=ytitle

!p = psave

end
