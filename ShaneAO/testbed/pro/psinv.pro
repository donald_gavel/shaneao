;+
;  psinv.pro - pseudo-inverse
;
; usage:
;   minv = psinv(mat,[w,u,v,thresh=thresh])
;-
function psinv, mat, w, u, v, thresh=thresh, verbose=verbose
  if (n_elements(verbose) eq 0) then verbose = 0
  if (n_elements(thresh) eq 0) then thresh = 0.01
  if (verbose) then print,'calling svdc...' & wait,.01
  svdc, mat, w, u, v
  if (verbose) then print,'done' & wait,.01
  n = (size(w))(1)
  maxsv = max(w)
  svi = 1./(w > thresh*maxsv)
  index = where(w lt thresh*maxsv,count)
  if (count gt 0) then svi[index] = 0
  svi = diagonal(svi)
  if (verbose) then print,'calculating inverse...' & wait,.01
  minv = v ## svi ## transpose(u)
  if (verbose) then print,'done' & wait,.01
  return, minv
end
