;+
;  ExtendPhase2.pro - extend the phase measurement to outside the
;      aperture according to a Wiener-like filtering methodology
;
;-

function extendPhase2,ph,sphi,pupil
  n = (size(ph))[1]
  pupil_f = ft(pupil)
  pupil_f2 = abs(pupil_f)^2
  Q = ftconvolve(pupil_f2,sphi)
  A = real(pupil_f[n/2,n/2])
  L = sphi/(Q/A); - 1.
  ph_f = ft(ph)
  phe_f = L*ph_f
  phe = real(ft(phe_f,/inverse))
  return,phe
end

function extendPhase3,ph,sphi,pupil
  n = (size(ph))[1]
  pupil2 = zeropad(pupil,2*n,2*n)
  ph2 = zeropad(ph,2*n,2*n)
  sphi2 = rebin(sphi,2*n,2*n)

  pupil_f = ft(pupil2)
  pupil_f2 = abs(pupil_f)^2
  Q = ftconvolve(pupil_f2,sphi2)
  A = real(pupil_f[n,n])
  L = sphi2/(Q/A); - 1.
  ph_f = ft(ph2)
  phe_f = L*ph_f
  phe = real(ft(phe_f,/inverse))
  phe = phe[n-n/2:n+n/2-1,n-n/2:n+n/2-1]
  return,phe
end

f = screengen(n,m,r0,du)
sphi = f^2
; make sure to depiston the phase
ph = depiston(ph,pupil)
phe = extendPhase2(ph,sphi,pupil)
window,0
plot,ph[*,128]
oplot,phe[*,128],color=128
end