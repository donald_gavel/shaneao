pro myScreen
 golden_ratio = (1+sqrt(5.))/2.
 height = 500
 width = fix(height*golden_ratio)
 if (!d.name eq 'X') then begin
   pref_set,'IDL_GR_X_QSCREEN',0
   pref_set,'IDL_GR_X_WIDTH',width
   pref_set,'IDL_GR_X_HEIGHT',height
   pref_commit
   monitor = 2 ; for my 2-headed dislay only
 endif else begin
   monitor = 1
 endelse
 device,get_screen_size = ss
 defsysv,'!w',exists=exists
 if (not exists) then defsysv,'!w',{screenwidth:ss[0],screenheight:ss[1], $
                    xoffset:ss[0]*monitor-width,yoffset:0,width:width,height:height}
end
