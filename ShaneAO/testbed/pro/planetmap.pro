;+
;   planetmap.pro - map a planet image onto latitude-longitude grid
;           (planetocentric mapping)
;
;   USAGE:
;      map = planetMap(image,cx,cy,rx,ry,subEarthLat,th,d,k,
;                      subSunLat,subSunLong,ifmax)
;
;   INPUTS:
;     rx,ry = major and minor ellipse radii, in pixels
;     subEarthLat = in degrees
;     th = image rotation angle, in degrees
;     d = desired pixel size in the resulting map image, in degrees
;     k = Minnaert parameter.  If given, the Minnaert illumination
;            model will be applied to compensate limb darkening
;           (k = 0.65 works with jupiter data)
;     subSunLat = in degrees (used only with Minnaert illumination model)
;     subSunLong = in degrees (used only with Minnaert illumination model)
;     ifmax = maximum illumination compensation factor (6 recommended)
;                (used only with Minnaert illumination model)
;
;   OUTPUT:
;     map = a latitude/longitude map of the planet
;      The central longitude on the resulting map is the sub-observer longitude
;      Use the IDL shift function to move the image in longitude, for
;      example:   map2 = shift(map,-subEarthLong/d,0)
;      (the convention in planetary astronomy is longitude increases westward
;      from the central meridian. For moons in synchronous orbit, zero degrees
;      longitude is defined as the planet-facing meridian)
;
;   See the web page http://www-igpp.llnl.gov/people/gavel/titan.html
;    for an example of a latitude/longitude map created using this algorithm
;
;   AUTHOR:
;     Don Gavel, LLNL
;-
function planetmap,image,cx,cy,rx,ry,subEarthLat,th,d,k,subSunLat,subSunLong,ifmax,index = i,xobs = xobs, yobs = yobs, zobs = zobs
  n = (size(image))(1)
  m = 180./d
  map = fltarr(m,m)
  degrees = !pi/180.
  dlat = d*degrees
  dlong = d*degrees
  alpha = subEarthLat*degrees
  theta = th*degrees
  long = indgen(m)*dlong - !pi/2
  lat = indgen(m)*dlat - !pi/2
;  long = extend(indgen(m),2,m)*dlat - !pi/2
;  lat = transpose(lat)
  sinlat = sin(lat)
  coslat = cos(lat)
  sinlong = sin(long)
  coslong = cos(long)
  r = sqrt( (coslat/rx)^2 + (sinlat/ry)^2 )^(-1)
  x = transpose(r*coslat)##sinlong
  y = extend(r*sinlat,1,m)
  z = transpose(r*coslat)##coslong
  xprime = x
  yprime = y*cos(alpha) - z*sin(alpha)
  zprime = y*sin(alpha) + z*cos(alpha)
  xobs = xprime*cos(theta) - yprime*sin(theta)
  yobs = xprime*sin(theta) + yprime*cos(theta)
  zobs = zprime
  xobs = xobs + cx
  yobs = yobs + cy
;  ix = where((xobs ge 0) and (xobs lt n))
;  iy = where((yobs ge 0) and (yobs lt n))
;  i = ix and iy
  i = where((xobs ge 0) and (xobs lt n) and $
            (yobs ge 0) and (yobs lt n) and $
            (zobs ge 0))
  map(i) = image(xobs(i),yobs(i))
  if (n_elements(k) eq 0) then return,map
;
;  Minnaert law
;
  beta = subSunLat*degrees
  lambda = -subSunLong*degrees
  xnormal = x/rx^2
  ynormal = y/ry^2
  znormal = z/rx^2
  nf = sqrt(xnormal^2 + ynormal^2 + znormal^2)
  xnormal = xnormal/nf
  ynormal = ynormal/nf
  znormal = znormal/nf
  obslos = [0,sin(alpha),cos(alpha)]
  sunlos = [sin(lambda)*cos(beta) , sin(beta), cos(lambda)*cos(beta) ]
  cose = obslos(0)*xnormal + obslos(1)*ynormal + obslos(2)*znormal
  cose = cose*(cose gt 0) + (cose le 0)
  cosi = sunlos(0)*xnormal + sunlos(1)*ynormal + sunlos(2)*znormal
  cosi = cosi*(cosi gt 0)
  illumFunc = cosi^k * cose^(k-1)
  illumFunc = illumFunc + (illumFunc eq 0)
  comp = map*((1/illumFunc)<ifmax)
  return, comp  
end
