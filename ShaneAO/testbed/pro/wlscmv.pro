;+
;  wlscmv.pro - compute the weighted least squares control matrix for
;              the Villages (Lick 40 inch) AO system
;
;    This is a rewrite of  wlscm.pro, originally written for the 3
;    meter AO system
;
;  usage example:
;    .compile wls.pro
;    frac = 0.3
;    sig = 100
;    weights = weightingv('snr',frac,sig)
;    thresh = 0.15
;    cm = wlscmv(sm,weights,thresh=thresh)
;
;  inputs:
;    sm - system matrix
;    weights - vector of weights (40 elements) one per subaperture
;    thresh - threshold for pseudoinverse (.15 typical)
;
;  requires:
;    lickdm.pro
;-
function weightingv,type,frac,signal
;
;  create a subaperture weighting vector according to various theorys
;  of best use of the light
;
;  usage:
;    w = weighting(type,frac,signal)
;
;  inputs:
;    type - string:
;           'uniform' - equal weighting on all subaps
;           'illum' - weight according to subap illumination fraction
;           'snr' - weight according to subap photon signal to noise ratio
;           'laser' - weight=frac on subaps next to the laser
;                     ***disabled for Villages until we learn mor
;                     about the laser***
;    frac - the ratio of photons in a partially illuminated subap relative
;           to a fully illuminated one
;           (for Lick: thoretical: 0.5, measured: ~0.3)
;    signal - the number of photons in a fully illuminated subap
;
;  output:
;    w - vector of weights that can be sent to wlscm
;
  if (n_elements(type) eq 0) then return,0
  weights = fltarr(60)+1
  halflum = [0,4,5,11,12,20,39,47,48,54,55,59]
;  nolum = [39,40,41,48,49,55]
;  laserlum = []
;  dimlaser = [18,19,24,25,31,37]
  readnoise = 6.
  if ( type eq 'uniform' ) then return,weights
  if ( type eq 'illum' ) then begin
    weights[halflum] = frac
    ;weights[nolum] = 0
    return,weights
  endif
  if ( type eq 'snr' ) then begin
    snr = signal/sqrt(signal+readnoise^2)
    weights[halflum] = ((frac*signal)/sqrt(frac*signal+readnoise^2))/snr
    return,weights
  endif
;  if ( type eq 'laser' ) then begin
;    weights[laserlum] = frac
;    return,weights
;  endif
;  if ( type eq 'laserwls' ) then begin
;    weights[halflum] = frac
;    weights[laserlum] = 0
;    return,weights
;  endif
;  if ( type eq 'dimlaser' ) then begin
;    weights[dimlaser] = 0
;    return,weights
;  endif
end

function modmat,r
;  remove sensor space tip and tilt
  ns = (size(r))(1)/2
  sttx = fltarr(1,2*ns)
  sttx[*,0:ns-1] = 1/sqrt(float(ns))
  sxt = sttx ## transpose(sttx)
  stty = fltarr(1,2*ns)
  stty[ns:2*ns-1] = 1/sqrt(float(ns))
  syt = stty ## transpose(stty)
  I = identity(2*ns)
  rMod = r ## (I - sxt) ## (I - syt)
  return,rMod
end

function modmata,r ; not used in Villages
;  remove actuator space tip and tilt
  forward_function dmgeom
  dum = lickdm() ;  forces compilation of dmgeom
  actLoc = transpose(dmgeom())
  na = (size(r))(2)
  ax = actLoc[0,*]
  ax = ax / sqrt(total(ax*ax))
  axm = transpose(ax) # ax
  ay = actLoc[1,*]
  ay = ay / sqrt(total(ay*ay))
  aym = transpose(ay) # ay
  I = identity(na)
  rMod = (I - axm) ## (I - aym) ## r
  return,rMod
end

function waffleSet,n=n
  if (n_elements(n) eq 0) then n=140
  waf = fltarr(12,12,121)
  wafker = [[1,-1],[-1,1]]
  k = 0
  for x = 0,10 do begin
    for y = 0,10 do begin
      waf[x:x+1,y:y+1,k] = wafker
      k += 1
    endfor
  endfor
  waf = reform(waf,144,121)
  if (n eq 144) then return,waf
  waf2 = fltarr(140,121)
  waf2[0:9,*] = waf[1:10,*]
  waf2[10:129,*] = waf[12:131,*]
  waf2[130:139,*] = waf[133:142,*]
  return,waf2
end

function wlscmvr,s,weights,badModes,reg,thresh=thresh,verbose=verbose
  w = diagonal([weights,weights])
  p = transpose(s) ## w ## s
  if (n_elements(badModes) ne 0) then begin
    p = p + reg*(transpose(badModes) ## badModes)
  endif
  pinv = pseudoinverse(p,thresh,modeCount)
  if (keyword_set(verbose)) then print,'<wlscmvr> ',strtrim(modeCount),' singular modes retained'
  c = - pinv ## transpose(s) ## w
  return,c
end

function wlscmv,s,weights,thresh=thresh,keeptt=keeptt,verbose=verbose
  if (n_elements(s) eq 0) then begin
    print,'<wlscmv> usage: cm = wlscmv(sm,[weights],[thresh=thresh])'
    dummy = weightingv()
    return,0
  endif
  if (n_elements(weights) eq 0) then weights = weightingv('uniform')
  if (n_elements(thresh) eq 0) then thresh = 0.15
  w = diagonal([weights,weights])
  ws = w ## s
  wsi = pseudoinverse(ws,thresh,modeCount)
  if (keyword_set(verbose)) then print,'<wlscmv> ',strtrim(modeCount),' singular modes retained'
  r = wsi ## w
;  remove tip-tilt
  if (not keyword_set(keeptt)) then r = modmat(r)
  c = r;  c = modmata(r1) - forget about actuator space for Villages
  c = -c
  return,c
end


frac = 0.3
sig = 100
weights = weightingv('snr',frac,sig)
thresh = 0.15
cm = wlscmv(sm,weights,thresh=thresh)
end