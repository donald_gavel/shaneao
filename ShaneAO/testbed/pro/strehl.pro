;+
;   strehl.pro - do a "best-fit" calculation of Strehl ratio
;
;    S = strehl(image,wavelength,center,annulus,camera=camera,pixel=pixelscale,d=d,ds=ds,secondary=secondary)
;
;   inputs:
;     image: image data array
;     wavelength, in microns
;     center: 2-vector approx. center of PSF
;     anunlus: 3-vector [r1,r2,r3] where
;              r1 = photometry radius
;              r2,r3 = inner and outer radius of sky background annular regiobn
;
;   optional inputs:
;     camera: 'ircal' - ircal IR camera
;             'villages' - Villages CCD (dewar 2 at 0.027 arcsec/mm platescale)
;             'pm' - photometrics camera in scoring mode (Bravais lens out)
;             'pmb' - photometrics camera in wide field mode (Bravais lens in)
;     pixelscale: pixel scale in arcseconds.  Required if camera not specified.
;     d: primary mirror diameter in meters. Default 3 meters
;     ds: secondary obscuration in meters. Default 0.8 meters
;     secondary: set this keyword if there is a secondary obscuration
;             (not necessary for 'ircal', which always has a secondary obs)
;     model: return the PSF model
;     psf: return the PSF, apertured and normalized
;     mask: return the annular background mask
;-
function strehl,data,lam,center,anulus,camera=cam,pixel=pixelscalearcsec,dp=dp,ds=ds,secondary=secondary,model=psf0,psf=psf,mask=mask
  n0 = (size(data))[1]
  m0 = (size(data))[2]
  arcsec = !pi/(180.*3600.)
  microns = 1.e-6
  i = complex(0,1.)
  lambda = lam*microns
  r1 = 100 ; default photometric aperture
  r2 = 100 & r3 = 150 ; default background aperture

  if keyword_set(secondary) then secondary = 1 else secondary = 0
  if n_elements(ds) ne 0 then secondary = 1
  if n_elements(center) eq 0 then center = [n/2,m/2]
  if n_elements(anulus) eq 0 then anulus = [r1,r2,r3]
  if (n_elements(pixelscalearcsec) eq 0) then begin
    if cam eq 'ircal' then begin
      pixelscale = 0.076*arcsec
      secondary = 1
    endif
    if cam eq 'pm' then pixelscale = 0.016*arcsec
    if cam eq 'pmb' then pixelscale = 0.28*arcsec
    if cam eq 'villages' then begin
      pixelscale = 0.029*arcsec; 0.027*arcsec
      secondary = 1
    endif
  endif else begin
    pixelscale = pixelscalearcsec*arcsec
  endelse
  if (n_elements(cam) ne 0) then begin
	  if (cam eq 'villages') then begin
		  if (n_elements(dp) eq 0) then dp = 1.    ;  Nickel telescope
		  if (n_elements(ds) eq 0) then ds = 0.39
	  endif
  endif
  if (n_elements(dp) eq 0) then dp = 3.    ;  Shane telescope
  if (n_elements(ds) eq 0) then ds = 0.8
  r1 = anulus[0]
  r2 = anulus[1]
  r3 = anulus[2]
;
;  set up photometric aperture and background masks
  dmask = circle(n0,m0,center[0],center[1],r1,1)
  bmask = circle(n0,m0,center[0],center[1],r3,1)-circle(n0,m0,center[0],center[1],r2,1)
;
;  shift to the peak of the data within the inital aperture
  ma = max(data*dmask)
  c = coords(data,!c)
  datas = shift(data,center[0]-c[0],center[1]-c[1])
;  remove the background
  dataB = depiston(datas,bmask)  
;
;  excise the data
  n = max([2*r3,256])
  dataA = fltarr(n,n)
  dataA = screenadd(dataA,dataB,center-n/2,1,[0,0],1,center-n/2,1,n,n,/nointerp)
  mask = fltarr(n,n)
  mask = screenadd(mask,dmask,center-n/2,1,[0,0],1,center-n/2,1,n,n,/nointerp)
;
;  set up pupil mask
  dx = lambda/(n*pixelscale)
  pupil = circle(n,n,n/2,n/2,(dp/2.)/dx,1.)
  if secondary eq 1 then begin
    pupil = pupil - circle(n,n,n/2,n/2,(ds/2.)/dx,1.)
;    pupil[n/2,*] = 0
;    pupil[*,n/2] = 0
  endif
;      shift the peak of the data to the center pixel
  ma = max(dataA)
  c = coords(dataA,!c)
  datas = shift(dataA,n/2-c[0],n/2-c[1])
;
;      now, find the sub-pixel centroid of the central core
  x = (findgen(n)-n/2)*dx # (fltarr(n)+1)
  y = (fltarr(n)+1) # (findgen(n)-n/2)*dx
  aperture = circle(n,n,n/2,n/2,1.22*(lambda/dp)/pixelscale,1.) ; the central core
  c = (centroid(datas*aperture) - [n/2,n/2])*pixelscale
;
;      and center the model psf there
  ph = (c[0]*x + c[1]*y)*(2*!pi/lambda)*pupil
  wf = pupil*exp(i*ph)
  psf0 = abs(ft(wf))^2
  psf0 = psf0/total(psf0*mask)
  psf = datas/total(datas*mask)
;
;      strehl is the ratio of energy in brightest pixel to that of model
  strehl = max(psf)/max(psf0)
  return,strehl
end
;
dir = 'C:\Documents and Settings\Don Gavel\Desktop\Data\080416\'
!p.charsize = 2

;  AO corrected
temp = {file:'',lam:0.0,center:[0,0],desc:''}
dset = replicate(temp,19)
dset[0] = {file:'d1502.fits',lam:0.63,center:[460,330],desc:'internal fiber'}
dset[1] = {file:'d1581.fits',lam:0.9 ,center:[491,355],desc:'900 nm'}
dset[2] = {file:'d1589.fits',lam:0.8 ,center:[491,355],desc:'I band'}
dset[3] = {file:'d1598.fits',lam:0.65,center:[491,355],desc:'R band'}
dset[4] = {file:'d1605.fits',lam:0.55,center:[491,355],desc:'V band'}
dset[5] = {file:'d1346.fits',lam:0.9,center:[170,150],desc:'900 nm'}
dset[6] = {file:'d1389.fits',lam:0.9,center:[170,150],desc:'900 nm'}
dset[7] = {file:'d1414.fits',lam:0.8,center:[470,340],desc:'I band'}
dset[8] = {file:'d1422.fits',lam:0.65,center:[468,333],desc:'R band'}
dset[9] = {file:'d1458.fits',lam:0.8,center:[460,340],desc:'I band'}
dset[10] = {file:'d1505.fits',lam:0.63,center:[461,331],desc:'fiber with turbulator'}

;  Seeing (Tip/Tilt controlled)
;temp = {file:'',lam:0.0,center:[0,0],desc:''}
;dset = replicate(temp,8)
dset[11] = {file:'d1581.fits',lam:0.9 ,center:[755,604],desc:'seeing 900 nm'}
dset[12] = {file:'d1589.fits',lam:0.8 ,center:[755,604],desc:'seeing I band'}
dset[13] = {file:'d1598.fits',lam:0.65,center:[755,604],desc:'seeing R band'}
dset[14] = {file:'d1605.fits',lam:0.55,center:[755,604],desc:'seeing V band'}
dset[15] = {file:'d1414.fits',lam:0.8,center:[755,604],desc:'seeing I band'}
dset[16] = {file:'d1422.fits',lam:0.65,center:[755,604],desc:'seeing R band'}
dset[17] = {file:'d1458.fits',lam:0.8,center:[755,604],desc:'seeing I band'}
dset[18] = {file:'d1505.fits',lam:0.63,center:[755,604],desc:'seeing fiber with turbulator'}

n_cases = n_elements(dset)
firstTime = 1
res = fltarr(3,n_cases)
for theCase = 0, n_cases -1 do begin
;for theCase = 1,1 do begin

file = dset[theCase].file
lam = dset[theCase].lam
center = dset[theCase].center

print,dset[theCase].file,' ',dset[theCase].desc
data = readfits(dir+file)
pixel = 0.027
anulus = [2.5/pixel,2.5/pixel,2.6/pixel]
;anulus = [15,15,30]
camera = 'villages'
strehl = strehl(data,lam,center,anulus,camera=camera,model=psf0,psf=psf,mask=mask)
print,'Strehl = ',strtrim(strehl,2)
n = (size(psf0))[1]
dx = pixel*[1,1]
x0 = -(n/2)*dx
;disp,psf,dx=dx,x0=x0,'psf '+file
;disp,psf0,dx=dx,x0=x0,'psf0'
;
fwhm_mask = ((psf/max(psf))-0.5) gt 0
fwhm = 2*sqrt(total(fwhm_mask)/!pi)*pixel
print,'FWHM = ',strtrim(fwhm,2),' arcsec'
u = ravg(psf/max(psf))
n = n_elements(u)
r = findgen(n)*pixel
if ((theCase eq 10) or (theCase eq 18)) then continue ; skip the internal fiber
if (theCase eq 0) then begin
  window,/free
  plot,r,u,xtitle='arcsec',xrange=[0,1],yrange=[0,1]
endif else begin
  if (theCase ge 11) then begin
;    firstTime = 0
    oplot,r,u,linestyle=1
  endif else begin
    oplot,r,u
  endelse
endelse
res[0,theCase] = lam
res[1,theCase] = strehl
res[2,theCase] = fwhm
endfor

window,/free
plot,res[0,0:11],res[1,0:10],psym=1,xrange=[.5,1.],yrange=[0,.25],xtitle='wavelength, microns',ytitle='Strehl'
oplot,res[0,11:18],res[1,11:18],psym=2

end

