;+
; dispgui.pro
;
;  Display images in a GUI
;
;  Usage:
;    Dispgui, DataArray2or3d, [Name], [size=[x,y]]
;
;  Author: Don Gavel, LLNL
;
;  Version: 3
;    Allows pointer argument.  If data is passed in directly,
;    a local copy is made.  3d data (datacube) is displayed one x-y plane
;    at a time, with a scroll bar on the left for picking the z
;    coordinate.
;
;  To Do:
;    Window resize
;    Auto-shape to original data aspect ratio
;    Slice plots
;    Automatic display of pixel values - Done
;    Menu to allow chosing a file
;    Log, Lin, HistEq scaling
;    Help
;
;-

pro dispgui_Event, Event

  widget_control, Event.Id, get_uvalue=Ev
  t = size(ev)
  if (t(0) eq 0) then evname = ev else evname = ev.name
  widget_control, event.top, get_uvalue = uv
  field = uv.field
  widget_control, field.draw, get_value = win
  tvdevice, win
  widget_control, field.draw, get_uvalue = uval

  data = uval.data
;  help,data

  case evname of

  'dispgui_base': begin   ; resize event
;     print,'requested window size: ',event.x,event.y
     s = size(*data)
     n = s(1)
     m = s(2)
     aspect = float(n)/float(m)
     minsize = [310, 356]
     newsize = (minsize > [event.x, event.y]) - field.pad
     thesize =  newsize[1]>newsize[0]
     newsize = [thesize,thesize]
;      print,'newsize (new draw window size)',newsize
     widget_control, field.draw, $
       xsize=newsize[0], ysize=newsize[1]

;       redraw window
     widget_control, field.draw, get_value = win
     tvdevice, win
      widget_control,field.minslide, get_value = mind
      widget_control,field.maxslide, get_value = maxd
      frame = 0
      if field.frameslide ne 0 then $
        widget_control,field.frameslide, get_value = frame
      x0 = uval.box(0)
      x1 = uval.box(1)
      y0 = uval.box(2)
      y1 = uval.box(3)
      dispdata = (*data)[x0:x1,y0:y1,frame]
;      print,'min = ',mind,' max = ',maxd
      exptv, (dispdata > mind) < maxd, /noexact, /data, relative = .8
      tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
      tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]

     end

  'top_menu': begin
     widget_control, event.value, get_value = event_name
     parent = widget_info(event.value, /parent)
     widget_control, parent, get_value = parent_name
        
     case parent_name of
      'File': begin
        case event_name of
         'Quit': begin
           widget_control,event.top,/destroy
           end
          else: begin
           Print, event_name 
           end
        endcase
        end
      'ColorMap': begin
         xloadct
        end
      else: begin
        Print, parent_name
        end
     endcase
     end

  'frame_slider': begin
;      Print, 'Event for frame_slider'
      frame = event.value
;      Print, 'frame = ', frame
;      widget_control, field.draw, get_uvalue = uval
      uval.frame = frame
      x0 = uval.box(0)
      x1 = uval.box(1)
      y0 = uval.box(2)
      y1 = uval.box(3)
      widget_control, field.draw, set_uval = uval
      dispdata = (*data)[x0:x1,y0:y1,uval.frame]
      exptv, dispdata,/noexact, /data, relative = .8
      tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
      tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
      mind = min(dispdata)
      maxd = max(dispdata)
      widget_control,field.minslide, set_value = mind
      widget_control,field.maxslide, set_value = maxd
      end

  'min_slider': begin  ; min
      widget_control,field.minslide, get_value = mind
      widget_control,field.maxslide, get_value = maxd
      mind = mind < maxd
      widget_control,field.minslide, set_value = mind
      frame = 0
      if field.frameslide ne 0 then $
        widget_control,field.frameslide, get_value = frame
      x0 = uval.box(0)
      x1 = uval.box(1)
      y0 = uval.box(2)
      y1 = uval.box(3)
      dispdata = (*data)[x0:x1,y0:y1,frame]
;      print,'min = ',mind,' max = ',maxd
      exptv, (dispdata > mind) < maxd, /noexact, /data, relative = .8
      tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
      tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
      end

  'max_slider': begin  ; max
      widget_control,field.minslide, get_value = mind
      widget_control,field.maxslide, get_value = maxd
      maxd = maxd > mind
      widget_control,field.maxslide, set_value = maxd
      frame = 0
      if field.frameslide ne 0 then $
        widget_control,field.frameslide, get_value = frame
      x0 = uval.box(0)
      x1 = uval.box(1)
      y0 = uval.box(2)
      y1 = uval.box(3)
      dispdata = (*data)[x0:x1,y0:y1,frame]
;      print,'min = ',mind,' max = ',maxd
      exptv, (dispdata > mind) < maxd, /noexact, /data, relative = .8
      tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
      tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
      end

  'draw_window': begin

      if (event.type eq 2) then begin  ; mouse motion, print values
        drawgeom = widget_info(field.draw, /geometry)
        nw = drawgeom.xsize  ; uval.wsize(0)
        mw = drawgeom.ysize  ; uval.wsize(0)
        x = event.x
        y = event.y
        s = size(*data)
        n = s(1)
        m = s(2)
        aspect = float(n)/float(m)
        offset = round([ (((1.-(aspect<1))/2.)*.8+.1)*nw, $
                         (((1.-(1./(aspect>1)))/2.)*.8+.1)*mw ])
        scale = [ (aspect<1)*nw*.8, (1/(aspect>1))*mw*.8 ]
        x = x - offset[0]
        y = y - offset[1]
        xo = float(x) / scale[0]
        yo = float(y) / scale[1]
        rangex = uval.box(1) - uval.box(0) + 1
        rangey = uval.box(3) - uval.box(2) + 1
        x = floor(uval.box(0) + xo*rangex)
        y = floor(uval.box(2) + yo*rangey)
        if ((x ge 0) and (x lt n) and (y ge 0) and (y lt m)) then begin
          z = (*data)[x,y]
          txtstr = '(' + string(x) + ',' + string(y) + ',' + string(z) + ')'
          widget_control, field.valuedisplay, set_value = txtstr
        endif
      endif
      if (event.press and 1b) then begin
;        widget_control, event.id, get_uvalue = uval
        drawgeom = widget_info(field.draw, /geometry)
        nw = drawgeom.xsize ; uval.wsize(0)
        mw = drawgeom.ysize ; uval.wsize(1)
        widget_control, event.id, get_value = windex
        tvdevice, windex
;   determine what to do!
        buttongroup = field.buttongroup
        widget_control, buttongroup, get_value = bindex
        case bindex of
       0: begin    ; Pan
            x = event.x
            y = event.y
            s = size(*data)
            n = s(1)
            m = s(2)
            aspect = float(n)/float(m)
            offset = round([ (((1.-(aspect<1))/2.)*.8+.1)*nw, $
                             (((1.-(1./(aspect>1)))/2.)*.8+.1)*mw ])
            scale = [ (aspect<1)*nw*.8, (1/(aspect>1))*mw*.8 ]
;  convert from device to pixel coordinates
            x = x - offset[0]
            y = y - offset[1]
;  convert from pixel to original image pixel
            xo = float(x) / scale[0]
            yo = float(y) / scale[1]
;            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
;            print, 'original data index: [',x,', ',y,']'
;  pan
            aspect = float(rangey) / float(rangex)

            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1)>0
            endif

            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1)>0
            endif
            uval.box = [x0,x1,y0,y1]
;            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            widget_control,field.minslide, get_value = mind
            widget_control,field.maxslide, get_value = maxd
            if uval.nframes eq 0 then $
              exptv, ((*data)[x0:x1,y0:y1]<maxd)>mind, /noexact, /data, relative = .8
            if uval.nframes gt 0 then $
              exptv, ((*data)[x0:x1,y0:y1,uval.frame]<maxd)>mind,/noexact, /data, relative = .8
            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
       1: begin    ; Zoom
            x = event.x
            y = event.y
            s = size(*data)
            n = s(1)
            m = s(2)
            aspect = float(n)/float(m)
            offset = round([ (((1.-(aspect<1))/2.)*.8+.1)*nw, $
                             (((1.-(1./(aspect>1)))/2.)*.8+.1)*mw ])
            scale = [ (aspect<1)*nw*.8, (1/(aspect>1))*mw*.8 ]
;  convert from device to pixel coordinates
            x = x - offset[0]
            y = y - offset[1]
;  convert from pixel to original image pixel
            xo = float(x) / scale[0]
            yo = float(y) / scale[1]
;            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
;            print, 'original data index: [',x,', ',y,']'
;  zoom!
            aspect = float(rangey) / float(rangex)
            rangex = round(rangex/2) > 2
            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1) > 0
            endif
            rangey = round(aspect*rangex)
            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1) > 0
            endif
            uval.box = [x0,x1,y0,y1]
;            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            widget_control,field.minslide, get_value = mind
            widget_control,field.maxslide, get_value = maxd
            exptv, ((*data)[x0:x1,y0:y1,uval.frame]>mind)<maxd,/noexact, /data, relative = .8
            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
       2: begin    ; Unzoom
            x = event.x
            y = event.y
            s = size(*data)
            n = s(1)
            m = s(2)
            aspect = float(n)/float(m)
            offset = round([ (((1.-(aspect<1))/2.)*.8+.1)*nw, $
                             (((1.-(1./(aspect>1)))/2.)*.8+.1)*mw ])
            scale = [ (aspect<1)*nw*.8, (1/(aspect>1))*mw*.8 ]
;  convert from device to pixel coordinates
            x = x - offset[0]
            y = y - offset[1]
;  convert from pixel to original image pixel
            xo = float(x) / scale[0]
            yo = float(y) / scale[1]
;            print, 'display-normalized position: [',xo,', ',yo,']'
            rangex = uval.box(1) - uval.box(0) + 1
            rangey = uval.box(3) - uval.box(2) + 1
            x = round(uval.box(0) + xo*rangex)
            y = round(uval.box(2) + yo*rangey)
;            print, 'original data index: [',x,', ',y,']'
;  unzoom!
            aspect = float(rangey) / float(rangex)
            rangex = round(rangex*2) < n
            x0 = (x - round(rangex/2)) > 0
            x1 = x0 + rangex - 1
            if x1 gt n-1 then begin
              x1 = n-1
              x0 = (x1 - rangex + 1) > 0
            endif
            rangey = round(aspect*rangex) < m
            y0 = (y - round(rangey/2)) > 0
            y1 = y0 + rangey - 1
            if y1 gt m-1 then begin
              y1 = m-1
              y0 = (y1 - rangey + 1) > 0
            endif
            uval.box = [x0,x1,y0,y1]
;            print, 'new box: ',uval.box
            widget_control,event.id,set_uval = uval
            widget_control,field.minslide, get_value = mind
            widget_control,field.maxslide, get_value = maxd
            if uval.nframes eq 0 then $
              exptv, ((*data)[x0:x1,y0:y1]>mind)<maxd, /noexact, /data, relative = .8
            if uval.nframes gt 0 then $
              exptv, ((*data)[x0:x1,y0:y1,uval.frame]>mind)<maxd,/noexact, /data, relative = .8

            tvaxis, xaxis = 0, yaxis = 0, xrange = [x0,x1+1], yrange = [y0, y1+1]
            tvaxis, xaxis = 1, yaxis = 1, xrange = [x0,x1+1], yrange = [y0, y1+1]
          end
        endcase
      endif
      end
  'radio_button_group': begin
      case Event.Value of
      0: ;Print,'Button Pan       Pressed'
      1: ;Print,'Button Zoom      Pressed'
      2: ;Print,'Button Unzoom  Pressed'
      3: ;Print,'Button Values Pressed'
      else: Message,'Unknown button pressed'
      endcase
      end
  'revert_button': Begin
;      Print, 'Event for Revert'
        widget_control, field.draw, get_value = windex
        s = size(*data)
        n = s(1)
        m = s(2)
        uval.box = [0,n-1,0,m-1]
        widget_control, field.draw, set_uval = uval
        mind = min((*data)[*,*,uval.frame])
        maxd = max((*data)[*,*,uval.frame])
        widget_control,field.minslide, set_value = mind
        widget_control,field.maxslide, set_value = maxd
        exptv, (*data)[*,*,uval.frame], /noexact, /data, relative = .8
        tvaxis, xaxis = 0, yaxis = 0
        tvaxis, xaxis = 1, yaxis = 1
      end
  endcase

  return
end


pro dispgui, dataarg, name, group=Group, size = wsize

  if n_params() eq 0 then begin
    print,'usage: dispgui,data,name,size=size'
    return
  end

  Print, 'DISPGUI -- v.4 experimental'

  defsysv, "!image", exists = exists
  if (exists eq 0) then imagelib
  defsysv, "!aspect", exists=exists
  if (exists eq 0) then devicelib

  !quiet = 1

  if n_elements(Group) eq 0 then group=0
  if n_elements(wsize) eq 0 then wsize = 285

  junk   = { cw_pdmenu_s, flags:0, name:'' }

  dispgui_base = widget_base(group_leader=Group, /base_align_left, $
      /column, /map, app_mbar = top_menu, /tlb_size_events, $
      uvalue={name:'dispgui_base'})

  top_menu_desc = [ $
                  {cw_pdmenu_s, 1, 'File'}, $         ; file menu
                  {cw_pdmenu_s, 0, 'ReadFits'}, $
                  {cw_pdmenu_s, 2, 'Quit'}, $
                  {cw_pdmenu_s, 1, 'ColorMap'}, $     ; color menu
                  {cw_pdmenu_s, 2, 'ColorMap'}, $
                  {cw_pdmenu_s, 1, 'Scaling'}, $      ; scaling menu
                  {cw_pdmenu_s, 0, 'Linear'}, $
                  {cw_pdmenu_s, 0, 'Log'}, $
                  {cw_pdmenu_s, 2, 'HistEq'}, $
                  {cw_pdmenu_s, 1, 'Help'}, $
                  {cw_pdmenu_s, 2, 'Help'} $
                ]

  top_menu = cw_pdmenu(top_menu, top_menu_desc, $
                      ids = menu_ids, $
                      /mbar, $
                      /return_id, $
                      uvalue = 'top_menu')

  name_label = widget_label( dispgui_base, $
      uvalue='name_label',/dynamic_resize, $
      font='-adobe-times-medium-r-normal--24-240-75-75-p-124-iso8859-1', $
      value=' ')

  data = 0
  if (size(dataarg))(0) eq 0 then begin
    if (ptr_valid(dataarg)) then begin
;      print,'argument is a pointer'
      data = dataarg
    endif
  endif else begin
;    print,'argument is data, creating local copy'
    data = ptr_new(dataarg)
  endelse

  s = size(*data)

  if s(0) eq 3 then nframes = s(3) else nframes = 0
  frame = 0

  base1 = widget_base( dispgui_base, row=1, map=1, uvalue='base1' )

  if nframes gt 1 then begin
    frame_slider = widget_slider( base1,maximum = nframes-1, $
           value = 0, title='frame', /vertical, uvalue = 'frame_slider')
  endif else begin
    frame_slider = 0
  endelse

  draw_window = widget_draw( base1, $
        /motion_events, /button_events, /retain, $
        uvalue={name:'draw_window', data:data, box:[0,s(1)-1,0,s(2)-1], $
                nframes: nframes, frame: frame, wsize:wsize}, $
        /no_copy, xsize=wsize, ysize=wsize)

  slide_base = widget_base(dispgui_base, /row, /map, uvalue='slide_base')
  mind = min(*data)
  maxd = max(*data)
  min_slider = cw_fslider( slide_base,maximum = maxd, minimum = mind, $
           edit = 1, $
           value = mind, title='min', uvalue = 'min_slider')

  max_slider = cw_fslider( slide_base,maximum = maxd, minimum = mind, $
           edit = 1, $
           value = maxd, title='max', uvalue = 'max_slider')

  value_display = widget_label(dispgui_base,frame=1,/dynamic_resize, $
      value='('+string(0)+','+string(0)+','+string(0)+')')

  baseButtons = widget_base(dispgui_base,/row,/map,uvalue='baseButtons')

  radio_buttons = [ 'Pan    ', 'Zoom   ', 'Unzoom ']

  radio_button_group = cw_bgroup( baseButtons, radio_buttons, $
      /row, /exclusive, set_value = 1, $
      uvalue='radio_button_group')

  revert_button = widget_button( baseButtons, $
      uvalue = 'revert_button', value = 'Revert')

  widget_control, dispgui_base, /realize

  field = {base: dispgui_base, $
           frameslide: frame_slider, $
           draw: draw_window, $
           pad: [0,0], $
           valuedisplay: value_display, $
           minslide: min_slider, $
           maxslide: max_slider, $
           buttongroup: radio_button_group}

  basegeom = widget_info(field.base, /geometry)
  drawgeom = widget_info(field.draw, /geometry)
;  print,'basegeom.size ',basegeom.xsize,basegeom.ysize
  field.pad = [(basegeom.xsize-drawgeom.xsize),(basegeom.ysize-drawgeom.ysize)+33]

  widget_control, dispgui_base, get_uvalue = uv
  uv = {name:uv.name, field:field}
  widget_control, dispgui_base, set_uvalue = uv

  ; Get drawable window index
  widget_control, draw_window, get_value=draw_window_id

  if n_elements(name) ne 0 then $
     widget_control, name_label, set_value = name
  widget_control, field.draw, get_value = win
  tvdevice, win

  mind = min((*data)[*,*,0])
  maxd = max((*data)[*,*,0])
  widget_control,field.minslide, set_value = mind
  widget_control,field.maxslide, set_value = maxd
  exptv, (*data)[*,*,0], /noexact, /data, relative = .8

  tvaxis, xaxis=0, yaxis=0
  tvaxis, xaxis=1, yaxis=1

  xmanager, 'dispgui', dispgui_base, /no_block

end
