;
;  specfilesize.pro - determine the size of the data in a speckle file
;
function specfilesize,file
  openr,1,file
  s = fstat(1)
  n = 0
  m = 0
  dum = long(0)
  readu,1,dum
  readu,1,n
  readu,1,m
  close,1
  nf = s.size/(2*long(n)*long(m))
  return,[n,m,nf]
end
