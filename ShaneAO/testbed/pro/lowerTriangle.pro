;
; lowerTriangle,a
;   return the lower triangle of a matrix
;   (where row index is less than column index)
function lowerTriangle,a
  n = (size(a))[1]
  m = (size(a))[2]
  i = ones(n) ## indgen(n)
  j = transpose(i)
  lowtri = i lt j
  return,a*lowtri
end
