;+
; StarDensityModels.pro
;
;    Statistically generate star clusters. Plot the stars as points in a simulated
;    star chart and return a list of the stars.  The actual star count
;    is assumed Poisson distributed around this mean.
;    The stars' positions are assumed to be randomly
;    distributed over the field of view with a uniform
;    probability distribution function and no spatial correlation.
;
;    Bahcall-Soneira Model -
;
;    Based on the paper by Bahcall & Soneira, "The Universe at Faint
;    Magnitudes. I. Models for the Galaxy and the Predicted Star Counts,
;    Astrophysical Journal Supplement, 44:73-110, Sept., 1981, which
;    covers the star densities in B and V bands.
;    This code calculates only the V band densities.  One can choose to use either
;    the published star density tables or the simplified formula (equation B1 in
;    Bahcall and Soneira's paper) to provide the mean star count at each
;    magnitude.
;
;    Spagna Model -
;
;    Based on the paper by Spagna, "Guide Star Requirements for NGST:
;    Deep NIR Starcounts and Guide Star Catalogs STScI-NGST-R-0013B, May, 2001,
;    which covers the star densities in J and K bands.  One can choose
;    either J band or K band calculation via a keyword; J band is default
;
;  syntax:
;    bahcallSoneiraModel,fov,glat,glong,starList [,mv_max=mv_max] [,seed=seed] [,/formula]
;    spagnaModel,fov,glat,glong,starList [,mv_max=mv_max] [,seed=seed] [,/Kband] [,/Jband]
;
;  arguments:
;    input:
;      fov = field of view in arcseconds
;      glat = Galactic latitude in degrees (if using tables, must be one of 20,30,50,90)
;      glong = Galactic longitude in degrees (if using tables, must be one of 0, 90, 180)
;    output:
;      starList - this is a table of the randomly generated "stars", each row
;                 containing the values of star magnitude, x position, and y position.
;                 If there are no stars in the field, starList has the scalar value 0.
;    keywords:
;      mvmax - upper limit of star magnitude in the simulation
;      seed - random number seed.  Set seed to a constant to reset the random number
;             generator.  Pass in the name of an undefined variable initially and
;             keep passing it in on subsequent calls to continue to generate new random
;             numbers.
;      formula - (Bahcall-Soneira model only)
;                set (=1) this keyword to use the simplified formula for star density.
;                unset (=0) it to take star density numbers from the
;                tables
;      Jband
;      Kband - keywords that indicate which band to calculate star
;              densities in the Spagna model.  Without a keyword, J
;              band is the default
;
;   Assumptions:
;           Star density look-up works only at the discrete latitudes
;           and longitudes stored in the tables.
;           For Bahcall-Soneira, glat={20,30,50,90}, glong={0,90,180} degrees
;           For Spagna, glat={0,30,40,50,60,70,90}, glong={0,90,180} degrees
;-
pro createSpagnaTables
  common spagnaModelTables, NGP, Jcounts, Kcounts
  NGP = [ $
    [10,    7.6,   10.4], $
    [10.5,  11.7,    16.7], $
    [11,    17.7,  26.3], $
    [11.5,  26.6,    41.2], $
    [12,    39.9,  63.8], $
    [12.5,  59.5,    97.4], $
    [13,    88,    145.4], $
    [13.5,  128, 211.6], $
    [14,    182.3, 298.6], $
    [14.5,  253.7,   408.1], $
    [15,    344.5, 540.1], $
    [15.5,  456.3,   693.6], $
    [16,    590.4, 867.7], $
    [16.5,  747.6,   1063.6], $
    [17,    929.1, 1286.7], $
    [17.5,  1137.1,  1547.9], $
    [18,    1377.4,    1863.8], $
    [18.5,  1660.4,  2254.6], $
    [19,    2001.4,    2740.6], $
    [19.5,  2418.7,  3336.7], $
    [20,    2928.7,    4050.4], $
    [20.5,  3541.8,  4881.5], $
    [21,    4261,  5822.2], $
    [21.5,  5083.1,  6855.2], $
    [22,    5999.9,    7953.2]]

  Jcounts = [$
    [0, 0,  11,  12,  13,  14,  15,  16,  17,  18,  19], $
    [30,    0, 83, 162,    324,   681,  1440,    2931,  5620,    10148, 17356], $
    [30,    90,    52,    105,   220,  474, 1001,   1961, 3477,   5595, 8299], $
    [30,    180,   42,   87,   184,  394, 812,    1530,  2605,    4024,  5709], $
    [40,    0, 47, 96, 202,    435,   904,  1755,    3155,  5327,    8650], $
    [40,    90,    35,    73,    159,   343,  702, 1310,   2216, 3433,   4985], $
    [40,    180,   30,   64,   138,  296, 592,    1073,  1757,    2628,  3661], $
    [50,    0, 32, 68, 147,    316,   641,  1193,    2047,  3304,    5186], $
    [50,    90,    26,    57,    126,   269,  535, 965,    1583,  2400,    3465], $
    [50,    180,   24,   52,   113,  240, 468,    825,   1318, 1937,   2690], $
    [60,    0, 25, 54, 118,    251,   496,  895, 1484,   2321, 3556], $
    [60,    90,    22,    48,    107,   226,  438, 772,    1241,  1858,    2684], $
    [60,    180,   20,   45,   99,   207,  396, 686,    1080,  1576,    2202], $
    [70,    0, 21, 46, 102,    214,   413,  728, 1178,   1801, 2706], $
    [70,    90,    19,    43,    96,    200,   382,  663, 1053,   1567, 2270], $
    [70,    180,   18,   41,   91,   189,  357, 612,    958,   1399, 1975], $
    [80,    0, 29, 42, 93, 193,    367,   636,  1012,    1521,  2246], $
    [80,    90,    18,    41,    90,    187,   354,  609, 960,    1424,  2067], $
    [80,    180,   18,   40,   88,   181,  342, 584,    914,   1342, 1920], $
    [90,    0, 18, 40, 88, 182,    344,   590,  929, 1377,   2002]]

; K band counts
  Kcounts = [$
    [0, 0,  11,  12,  13,  14,  15,  16,  17,  18,  19], $
    [30,    0, 111,    223,   479,  1071,    2333,  4718,    8676,  14857,   25007], $
    [30,    90,    69,    149,   338,  774, 1656,   3154, 5279,   7930, 11179], $
    [30,    180,   56,   125,  287, 645,    1332,  2423,    3867,  5537,    7393], $
    [40,    0, 63, 138,    315,   705,  1470,    2761,  4694,    7568,  12376], $
    [40,    90,    47,    109,   253,  566, 1147,   2048, 3243,   4730, 6685], $
    [40,    180,   41,   96,   223,  490, 962,    1656,  2526,    3532,  4725], $
    [50,    0, 44, 102,    236,   519,  1034,    1836,  2967,    4619,  7395], $
    [50,    90,    37,    88,    205,   445,  864, 1475,   2262, 3266,   4669], $
    [50,    180,   34,   80,   186,  397, 751,    1247,  1855,    2579,  3496], $
    [60,    0, 35, 83, 193,    413,   793,  1351,    2111,  3212,    5040], $
    [60,    90,    32,    76,    175,   372,  699, 1159,   1745, 2517,   3640], $
    [60,    180,   29,   71,   163,  341, 629,    1021,  1502,    2097,  2894], $
    [70,    0, 30, 73, 167,    351,   655,  1084,    1655,  2473,    3800], $
    [70,    90,    28,    69,    158,   329,  604, 984,    1468,  2120,    3094], $
    [70,    180,   27,   66,   150,  310, 563,    903,   1324, 1868,   2630], $
    [80,    0, 28, 67, 153,    316,   578,  939, 1409,   2072, 3115], $
    [80,    90,    27,    65,    149,   306,  556, 896,    1331,  1926,    2827], $
    [80,    180,   26,   64,   145,  297, 536,    858,   1262, 1803,   2593], $
    [90,    0, 26, 64, 145,    299,   540,  868, 1287,   1864, 2741]]
end

pro createBahcallSoneiraTables
  common bahcallSoneiraModelTables,mags,glats,glongs,starDensities
  mags = [10,11,12,13,14,15,16,17,18,19,20,22,24,26,28,30]
  glats = [90,50,30,20]
  glongs = [0,90,180]
  starDensities = fltarr(4,16,3)
  starDensities[*,*,0] = [ $
[2.30E+00,    3.19E+00,   4.21E+00,  6.23E+00],$
[5.46E+00,    7.77E+00,   1.04E+01,  1.55E+01],$
[1.31E+01,    1.96E+01,   2.65E+01,  3.94E+01],$
[3.03E+01,    4.91E+01,   6.97E+01,  1.05E+02],$
[6.67E+01,    1.19E+02,   1.85E+02,  2.92E+02],$
[1.35E+02,    2.74E+02,   4.83E+02,  8.12E+02],$
[2.53E+02,    5.86E+02,   1.19E+03,  2.15E+03],$
[4.37E+02,    1.15E+03,   2.68E+03,  5.25E+03],$
[7.09E+02,    2.09E+03,   5.43E+03,  1.15E+04],$
[1.10E+03,    3.52E+03,   9.89E+03,  2.24E+04],$
[1.64E+03,    5.56E+03,   1.64E+04,  3.89E+04],$
[3.26E+03,    1.18E+04,   3.62E+04,  8.97E+04],$
[5.66E+03,    2.10E+04,   6.53E+04,  1.64E+05],$
[8.85E+03,    3.31E+04,   1.04E+05,  2.60E+05],$
[1.27E+04,    4.79E+04,   1.51E+05,  3.79E+05],$
[1.70E+04,    6.51E+04,   2.06E+05,  5.19E+05 ]]
  starDensities[*,*,1] = [ $
[2.30E+00,    3.01E+00,   3.84E+00,    5.54E+00],$
[5.46E+00,    7.13E+00,   9.09E+00,    1.31E+01],$
[1.31E+01,    1.72E+01,   2.16E+01,    3.04E+01],$
[3.03E+01,    4.08E+01,   5.13E+01,    7.11E+01],$
[6.67E+01,    9.20E+01,   1.20E+02,    1.67E+02],$
[1.35E+02,    1.93E+02,   2.69E+02,    3.85E+02],$
[2.53E+02,    3.72E+02,   5.64E+02,    8.48E+02],$
[4.37E+02,    6.54E+02,   1.08E+03,    1.74E+03],$
[7.09E+02,    1.06E+03,   1.88E+03,    3.26E+03],$
[1.10E+03,    1.61E+03,   2.97E+03,    5.50E+03],$
[1.64E+03,    2.33E+03,   4.34E+03,    8.42E+03],$
[3.26E+03,    4.39E+03,   8.03E+03,    1.61E+04],$
[5.66E+03,    7.35E+03,   1.31E+04,    2.64E+04],$
[8.85E+03,    1.12E+04,   1.95E+04,    3.92E+04],$
[1.27E+04,    1.57E+04,   2.70E+04,    5.44E+04],$
[1.70E+04,    2.06E+04,   3.47E+04,    7.02E+04]]
  starDensities[*,*,2] = [ $
[2.30E+00,   2.90E+00,   3.62E+00,   5.13E+00],$
[5.46E+00,   6.78E+00,   8.48E+00,   1.20E+01],$
[1.31E+01,   1.16E+01,   1.98E+01,   2.74E+01],$
[3.03E+01,   3.73E+01,   4.60E+01,   6.27E+01],$
[6.67E+01,   8.17E+01,   1.04E+02,   1.43E+02],$
[1.35E+02,   1.65E+02,   2.25E+02,   3.16E+02],$
[2.53E+02,   3.07E+02,   4.51E+02,   6.62E+02],$
[4.37E+02,   5.18E+02,   8.26E+02,   1.29E+03],$
[7.09E+02,   8.02E+02,   1.37E+03,   2.28E+03],$
[1.10E+03,   1.17E+03,   2.07E+03,   3.65E+03],$
[1.64E+03,   1.63E+03,   2.93E+03,   5.38E+03],$
[3.26E+03,   2.88E+03,   5.15E+03,   9.81E+03],$
[5.66E+03,   4.62E+03,   8.08E+03,   1.56E+04],$
[8.85E+03,   6.83E+03,   1.17E+04,   2.27E+04],$
[1.27E+04,   9.35E+03,   1.58E+04,   3.09E+04],$
[1.70E+04,   1.19E+04,   1.98E+04,   3.91E+04]]

end

pro bahcallSoneiraModel,fov,glat,glong,starList,mv_max=mv_max,seed=seed,formula=formula
  common bahcallSoneiraModelTables
  degrees = !pi/180.
  if (n_elements(mv_max) eq 0) then mv_max = 22

  mv_max_index = where(mags eq mv_max)
  if (mv_max_index eq -1) then begin
    print,'<bahcallSoneiraModel> ERROR mv_max must be one of', mags
    return
  endif
  mv_max_index = mv_max_index[0]

  if (not keyword_set(formula)) then begin
    case glat of
      20: glatindex = 3
      30: glatindex = 2
      50: glatindex = 1
      90: glatindex = 0
      else: begin
        print,'<bahcallSoneiraModel> ERROR glat must be one of {20,30,50,90}'
        return
      end
    endcase
    case glong of
      0: glongindex = 0
      90: glongindex = 1
      180: glongindex = 2
      else: begin
        print,'<bahcallSoneiraModel> ERROR glong must be one of {0,90,180}'
        return
      end
    endcase
  endif

  if (keyword_set(formula)) then begin
    sigma = 1.45-0.20*cos(glat*degrees)*cos(glong*degrees)
    c1 = 925
    c2 = 1050
    alpha = -0.132
    beta = 0.035
    kappa = -0.180
    eta = 0.087
    delta = 3.0
    lambda = 2.50
    m_star = 15.75
    m_dag = 17.5
  endif

  lastCount = 0
  for mvindex = 0,mv_max_index do begin
    if (keyword_set(formula)) then begin
      mv = mags[mvindex]
      if (mv le 12) then begin
        mu = 0.03
        gamma = 0.36
      endif
      if ((mv gt 12) and (mv lt 20)) then begin
        mu = 0.0075*(mv-12)+0.03
        gamma = 0.04*(12-mv)+0.36
      endif
      if (mv ge 20) then begin
        mu = 0.09
        gamma = 0.04
      endif

      average_nr_stars = C1*10^(beta*(mv-m_star))/(1+10^(alpha*(mv-m_star))) * $
                         1.0/(sin(glat*degrees)*(1-mu*(1/tan(glat))*cos(glong)))^(3-5*gamma) + $
                         C2*10^(eta*(mv-m_dag))/(1+10^(kappa*(mv-m_dag)))^lambda * $
                         1.0/(1-cos(glat*degrees)*cos(glong*degrees))^sigma
      average_nr_stars = average_nr_stars*(fov/3600.)^2
    endif else begin
      average_nr_stars = (starDensities[glatindex,mvindex,glongindex]-lastCount)*(fov/3600.)^2
    endelse
    lastCount = average_nr_stars
    nr_stars = randomn(seed,poisson=average_nr_stars)
    if (nr_stars ge 1) then begin
      x = randomu(seed,nr_stars)*fov - fov/2
      y = randomu(seed,nr_stars)*fov - fov/2
      starList_mv = fltarr(3,nr_stars)
      for k=0,nr_stars-1 do starList_mv[*,k] = [mags[mvindex],x[k],y[k]]
      if (nr_stars eq 1) then starList_mv = reform(starList_mv,3,1)

      ; append to a cumulative star list
      if (n_elements(allStarList) ne 0) then begin
        nAllStars = (size(allStarList))[2]
        savedList = allStarList
        allStarList = fltarr(3,nAllStars+nr_stars)
        allStarList[*,0:nAllStars-1] = savedList
        allStarList[*,nAllStars:nAllStars+nr_stars-1] = starList_mv
      endif else begin
        allStarList = starList_mv
        if ((size(allStarList))[0] eq 1) then allStarList = reform(allStarList,3,1)
      endelse

    endif
  endfor
  if (n_elements(allStarList) ne 0) then begin
    starList = allStarList
    if ((size(StarList))[0] eq 1) then starList = reform(starList,3,1)
  endif else begin
    starList = 0
  endelse
end

pro spagnaModel,fov,glat,glong,starList,mv_max=mv_max,seed=seed,Jband=Jband,Kband=Kband
  common spagnaModelTables

  degrees = !pi/180.
  if (keyword_set(Kband)) then counts = Kcounts else counts = Jcounts
  nm = (size(counts))[1]
  nl = (size(counts))[2]
  mags = counts[0:nm-1,0]
  if (n_elements(mv_max) eq 0) then mv_max = mags[nm-1]
  mv_max_index = where(mags[2:nm-1] eq mv_max)
  if (mv_max_index eq -1) then begin
    print,'<spagnaModel> ERROR mv_max must be one of', mags[2:nm-1]
    return
  endif
  mv_max_index = mv_max_index[0]+2
  glatList = counts[0,*]
  glatIndex = where(glatList eq glat)
  if (glatIndex[0] eq -1) then begin
     print,'<spagnaModel> ERROR glat must be one of ',glatList
     return
  end
  latCounts = counts[*,glatIndex]
  glongList = latCounts[1,*]
  glongIndex = where(glongList eq glong)
  if (glongIndex[0] eq -1) then begin
     print,'<spagnaModel> ERROR glong must be one of ',glongList,' for lat = ',glat
     return
  end
  latLongCounts = latCounts[*,glongIndex]

  lastCount = 0
  for mvindex = 2,mv_max_index do begin

    average_nr_stars = (latCounts[mvindex]-lastCount)*(fov/3600.)^2
    lastCount = average_nr_stars
    nr_stars = randomn(seed,poisson=average_nr_stars)
    if (nr_stars ge 1) then begin
      x = randomu(seed,nr_stars)*fov - fov/2
      y = randomu(seed,nr_stars)*fov - fov/2
      starList_mv = fltarr(3,nr_stars)
      for k=0,nr_stars-1 do starList_mv[*,k] = [mags[mvindex],x[k],y[k]]
      if (nr_stars eq 1) then starList_mv = reform(starList_mv,3,1)

      ; append to a cumulative star list
      if (n_elements(allStarList) ne 0) then begin
        nAllStars = (size(allStarList))[2]
        savedList = allStarList
        allStarList = fltarr(3,nAllStars+nr_stars)
        allStarList[*,0:nAllStars-1] = savedList
        allStarList[*,nAllStars:nAllStars+nr_stars-1] = starList_mv
      endif else begin
        allStarList = starList_mv
        if ((size(allStarList))[0] eq 1) then allStarList = reform(allStarList,3,1)
      endelse

    endif
  endfor
  if (n_elements(allStarList) ne 0) then begin
    starList = allStarList
    if ((size(starList))[0] eq 1) then starList = reform(starList,3,1)
  endif else begin
    starList = 0
  endelse
end

pro plotStarList,fov,starList,title=title
  common bahcallSoneiraModelTables

  if (n_elements(title) eq 0) then title = ' '
  nvert = 8
  px = fltarr(nvert+1)
  py = fltarr(nvert+1)
  for vert = 0,nvert do begin
    px[vert] = cos(vert*2*!pi/float(nvert))
    py[vert] = sin(vert*2*!pi/float(nvert))
  endfor

  nm = (size(mags))[1]
  psize = 2 - 2*findgen(nm)/float(nm)
  plot,[-fov/2,fov/2],[-fov/2,fov/2],xtitle='arcseconds',ytitle='arcseconds', $
     /nodata,/isotropic,xrange=[-fov/2,fov/2],yrange=[-fov/2,fov/2], $
     xstyle = 1, ystyle = 1, title = title
  if ( (size(starList))[0] eq 0 ) then begin
    xyouts,0,0,'NO STARS',alignment=0.5
    return
  endif

    if ( (size(starList))[0] eq 1) then starList = reform(starList,3,1)
    nr_stars = (size(starList))[2]
    for k =0,nr_stars-1 do begin
      mv = starList[0,k]
      mvindex = where(mags eq mv)
      if (mvindex eq -1) then begin
        print,'<plotStarList> Warning: star magnitude, ',mv,' not supported'
      endif else begin
        mvindex = mvindex[0]
        x = starList[1,k]
        y = starList[2,k]
        usersym,px*psize[mvindex],py*psize[mvindex],/fill
        oplot,[x],[y],psym=8
      endelse
    endfor

end

pro starDensityModels
  createSpagnaTables
  createBahcallSoneiraTables
end

; example code
starDensityModels ; initialization

;seed = 3
glat = 50
glong = 0
fov = 60
mv_max = 19

bahcallSoneiraModel,fov,glat,glong,starList,mv_max=mv_max,seed=seed
window,0,xsize=500,ysize=500
plotStarList,fov,starList,title='Bahcall-Soneira'

spagnaModel,fov,glat,glong,starList,mv_max=mv_max,seed=seed,/Jband
window,1,xsize=500,ysize=500
plotStarList,fov,starList,title='Spagna J'

spagnaModel,fov,glat,glong,starList,mv_max=mv_max,seed=seed,/Kband
window,2,xsize=500,ysize=500
plotStarList,fov,starList,title='Spagna K'
end
