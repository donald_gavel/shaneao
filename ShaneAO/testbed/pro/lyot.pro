;+
;  lyot.pro
;    simulate a lyot cornonagraph
;-
microns = 1.e-6
mm = 1.e-3
n=256
dx = .05*mm
lambda = 0.532*microns
d = 10*mm
du = 40*mm/float(n)
dtheta = lambda/(n*du)
f = dx/dtheta
; aperture
ap0 = circle(n,n,n/2,n/2,(d/2.)/du,1.)
; focal plane stop
stopType = 'gaussian'
if (stopType eq 'hard') then begin
  dstop = 12.5*f*lambda/d
  fpmask = 1.-circle(n,n,n/2,n/2,(dstop/2.)/dx,1.)
endif
if (stopType eq 'gaussian') then begin
  dstop = 10*f*lambda/d
  fpmask = 1.-gauss2(n,n/2,n/2,dstop/dx)
endif
; lyot stop
lyotType = 'hard'
if (lyotType eq 'hard') then begin
  dlyot = 0.9*d
  lyotstop = circle(n,n,n/2,n/2,(dlyot/2.)/du,1.)
endif
if (lyotType eq 'gaussian') then begin
  dlyot = 0.25*d
  lyotstop = gauss2(n,n/2,n/2,dlyot/du)
endif
; light propagation
i = complex(0,1)
wf = complex(ap0,0*ap0)
fp1 = ft(wf)  ; first focal plane
fp1 /= max(abs(fp1))
fp1m = fp1*fpmask
if (stopType eq 'hard') then begin
  disp,fpmask*abs(fp1) + .1*abs(fp1),'magnitude at coronagraph stop (+1% leakage)'
endif else begin
  disp,fpmask*abs(fp1),'magnitude at coronagraph stop'
endelse
pp2 = ft(fp1m,/inverse) ; second pupil plane
disp,abs(pp2),'magnitude at lyot stop'
pp2m = pp2*lyotstop
throughput = total(lyotstop*ap0)/total(ap0)
disp,abs(pp2m),'magnitude just after lyot stop'
fp2 = ft(pp2m)
im2 = abs(fp2)^2
disp,alog10(im2),'intensity at second focal plane'
window,/free
ld = findgen(n/2)*dtheta/(lambda/d)
sl = im2[n/2:n-1,n/2]
title = 'stop: '+stopType+' '+strtrim(dstop/(f*lambda/d),2)
title += ' lyot: '+lyotType+' '+strtrim(dlyot/d,2)
title += ' throughput='+strtrim(throughput,2)
plot,ld,sl,/ylog,ticklen=0.5,xtitle='lambda/d',ytitle='relative intensity', $
  title=title,yrange=[1.e-10,1.e-4]
end
