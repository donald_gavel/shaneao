; ftwls.pro
;  Fourier-transform weighted least square reconstructor
;  phase from slopes
;
;  Inputs:
;    sx - array of x slopes
;    sy - array of y slopes
;    ap - mask of illumination
;    W - subaperture weights
;  Keyword input:
;    r0 - builds a kolmogorov power spectrum as a penalty
;         P~ = 0.27 k^(-11/3) r0^(-5/3)
;         r0 is in units of subap spacing
;    dewaffle - buids a penalty based on 2x2 waffle pattern
;         the value is a scalar weight of this penalty
;  Depends on:
;   ftwfs.pro (deNyquist)
;
;  Output:
;   y - array of phases
;---------------------------------
function grad,n,subapFilter=subapFilter,differenceMethod=differenceMethod,geometry=geometry,verbose=verbose
  if (keyword_set(differenceMethod) eq 0) then differenceMethod = 'finiteDifference'
  if (keyword_set(geometry) eq 0) then geometry = 'Fried'
  if (differenceMethod eq 'finiteDifference') then begin
    G = fltarr(n,n, n,n, 2)
    a = fltarr(n+1,n+1)
    if (geometry eq 'Shearing') then begin ; straight first difference
      for x = 0,n-1 do begin
        for y = 0,n-1 do begin
          a *= 0.
          a[x,y] = 1.
          G[x,y,*,*,0] = (a-shift(a,1,0))[0:n-1,0:n-1]
          G[x,y,*,*,1] = (a-shift(a,0,1))[0:n-1,0:n-1]
        endfor
      endfor
    endif
    if (geometry eq 'Fried') then begin ; average first difference with neighbor first difference one delta away in orthogonal direction
      for x = 0,n-1 do begin
        for y = 0,n-1 do begin
          a *= 0.
          a[x,y] = 1.
          sx = a-shift(a,1,0)
          sx = (sx + shift(sx,0,1))/2.
          sy = a-shift(a,0,1)
          sy = (sy + shift(sy,1,0))/2.
          G[x,y,*,*,0] = sx[0:n-1,0:n-1]
          G[x,y,*,*,1] = sy[0:n-1,0:n-1]
        endfor
      endfor
    endif
    if (geometry eq 'Hudgin') then begin ; compute as a central difference
      for x = 0,n-1 do begin
        for y = 0,n-1 do begin
          a *= 0.
          a[x,y] = 1.
          sx = a-shift(a,1,0)
          sx = (sx + shift(sx,-1,0))/2.
          sy = a-shift(a,0,1)
          sy = (sy + shift(sy,0,-1))/2.
          G[x,y,*,*,0] = sx[0:n-1,0:n-1]
          G[x,y,*,*,1] = sy[0:n-1,0:n-1]
        endfor
      endfor
    endif
    nn = long(n)*long(n)
    return,reform(G,nn,nn*2)
  endif
  if (differenceMethod eq 'Fourier') then begin
      i = dcomplex(0,1.)
      m = n
      dkx = 2*!dpi/double(n)
      kx = (dindgen(n)-n/2) # ones(m)*dkx
      dky = 2*!dpi/double(m)
      ky = ones(n) # (dindgen(m)-m/2)*dky
      mask = 1-((kx eq 0) and (ky eq 0)) ; removes piston and Nyquist components
      mask[0,*] = 0.
      mask[*,0] = 0.
      if (keyword_set(subapFilter)) then begin
          fsax = 2*sin(kx/2)/(kx+(kx eq 0))
          fsax += kx eq 0
          fsay = 2*sin(ky/2)/(ky+(ky eq 0))
          fsay += ky eq 0
          fsa = fsax*fsay
      endif else begin
          fsa = 1
      endelse
      
;          Gradient
      cx = i*kx*fsa*mask
      cy = i*ky*fsa*mask
;
      if (geometry eq 'Shearing') then begin
          cx *= exp(-i*kx*0.5)
          cy *= exp(-i*ky*0.5)
      endif
      if (geometry eq 'Fried') then begin
          cx *= exp(-i*(kx+ky)*0.5)
          cy *= exp(-i*(kx+ky)*0.5)
      endif
;     if (geometry eq 'Hudgin') then begin  ; no shift
;     endif

  endif
  g = complexarr(n,n,2)
  g[*,*,0] = cx
  g[*,*,1] = cy
  return,g
end

function div,n,subapFilter=subapFilter,differenceMethod=differenceMethod,geometry=geometry,verbose=verbose
  if (keyword_set(differenceMethod) eq 0) then differenceMethod = 'finiteDifference'
  if (keyword_set(geometry) eq 0) then geometry = 'Fried'
  G = grad(n,subapFilter=subapFilter,differenceMethod=differenceMethod,geometry=geometry,verbose=verbose)
  if (differenceMethod eq 'finiteDifference') then begin
    D = transpose(G)
  endif
  if (differenceMethod eq 'Fourier') then begin
    D = conj(G)
  endif
  return,D
end

n = 128
r = 0.25*n ; window radius of aperture
rw = 0.4*n ; window radius for generating data
i = dcomplex(0,1.)
dkx = 2*!dpi/double(n)
kx = (dindgen(n)-n/2) # ones(n)*dkx
ky = transpose(kx)

Gx_f = i*kx
Gy_f = i*ky ; Gradient, Divergence operators

;y = -(ones(n) ## findgen(n) - n/2)
y = real(zernike(n,n,r,3,1))
;y = deNyquist(y)
y *= supergauss(n,n/2,n/2,rw,10.)
ap = circle(n,n,n/2,n/2,r,1.)
G = grad(n,differenceMethod='Fourier',geometry='Hudgin')
sx = ft(G[*,*,0]*ft(y),/inverse)*ap
sy = ft(G[*,*,1]*ft(y),/inverse)*ap

seed = 5
sigma_n = 0.00 ; noise standard deviation in slope units
noise = randomn(seed,n,n,2)*sigma_n
sx += noise[*,*,0]
sy += noise[*,*,1]
W = circle(n,n,n/2,n/2,r,1.)
dewaffle = .5
debasket = 0.
;------------------------------------
;
  verbose = 1
  diagnostics = 1
  
  nx = (size(sx))[1]
  ny = (size(sy))[1]
  if (nx ne ny) then begin
    print,'<ftwls> ERROR, size of sensor arrays not equal'
;    return,0
  endif
  n = nx
  gain = .7 ; iterator gain
  max_iter = 100 ; maximum number of iterations
;
  i = dcomplex(0,1.)
  dkx = 2*!dpi/double(n)
  kx = (dindgen(n)-n/2) # ones(n)*dkx
  ky = transpose(kx)

;  Gx_f = i*kx
;  Gy_f = i*ky ; Gradient, Divergence operators

;  set up penalty, P in Fourier domain
  P_f = 0 ; ***temporary - calculate penalty here ***
;  waffle penalty
  waffle = [[1,-1],[-1,1]]
  wn = zeropad(waffle,n,n)
  P_f = abs(ft(wn))^2
  P_f /= max(P_f) ; normalize
  P_f *= 2*!pi^2 ; make it comparable in magnitude to the divergence-gradient operator
  P_f *= dewaffle ; apply the dewaffling weighting factor (~1)
;  basket penalty
  basket = [[0,1],[-1,0]]
  bn = zeropad(basket,n,n)
  P_f2 = abs(ft(bn))^2
  P_f2 /= max(P_f2)
  P_f2 *= 2*!pi^2
  P_f2 *= debasket
  cbasket = [[1,0],[0,-1]]
  cbn = zeropad(cbasket,n,n)
  P_f3 = abs(ft(cbn))^2
  P_f3 /= max(P_f3)
  P_f3 *= 2*!pi^2
  P_f3 *= debasket
  
;  iterator matrix
  zz = fltarr(n,n)
  zz[n/2,n/2] = 1
  C_f = 1/(abs(Gx_f)^2+abs(Gy_f)^2 + P_f +P_f2 + P_f3 + zz)
  C_f[n/2,n/2] = 0.
;
;  begin solver
  sx_f = ft(W*sx)
  sy_f = ft(W*sy)
;
  d_f = conj(Gx_f)*sx_f + conj(Gy_f)*sy_f ; divergence
  y_f = 0 ; a-priori phase estimate
;
  if (diagnostics) then begin
    y_save = fltarr(n,n,max_iter)
    e_save = fltarr(n,n,max_iter)
    rms_save = fltarr(max_iter+1)
  endif
  if (verbose) then print,'<ftwls> starting iteration' & wait,.01
  for k = 0,max_iter do begin
;  forward operation  M y = (G'WG + P) y
      My_f = conj(Gx_f)*ft(W*ft(Gx_f*y_f,/inverse)) + $
             conj(Gy_f)*ft(W*ft(Gy_f*y_f,/inverse)) + P_f*y_f
;  iterative update
      e_f = (d_f - My_f)
      if (diagnostics or verbose) then rmserr = total(abs(e_f)^2)/float(n^2)
      if (diagnostics) then rms_save[k] = rmserr
      if (verbose) then begin
        print,'rms error[',strtrim(k,2),'] = ',strtrim(rmserr,2)
        wait,.01
      endif
      if (k eq max_iter) then break
      y_f = y_f + gain*C_f*e_f
      if (diagnostics) then begin
        y_save[*,*,k] = real(ft(y_f,/inverse))
        e_save[*,*,k] = real(ft(e_f,/inverse))
      endif
  endfor
  if (verbose) then begin
    disp,y_save,'y history'
    disp,e_save,'e history'
    window,/free
    plot,rms_save,title='rms error vs iteration',charsize=2,/ylog,ticklen=.5
  endif
  if (diagnostics) then begin
    yhat = real(ft(y_f,/inverse))
    d = real(ft(d_f,/inverse))
    e = real(ft(e_f,/inverse))
    My = real(ft(My_f,/inverse))
  endif
disp,depiston(y-yhat,w),'y - yhat'
end

  
  