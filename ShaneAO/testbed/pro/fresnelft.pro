;+
;  fresnelft.pro - fresnel propagation, Fourier transform method
;
;  USAGE:
;    wf1 = fresnelft(wf0,du,A,B,C,D,lambda,dx=dx,check=check)
;
;  Inputs:
;    wf0  - complex initial wavefront, at "input" plane
;    du - sample spacing on wf0
;    A,B,C,D - matrix parameters of the interving optical system
;             (see Siegman, Lasers, Ch. 15)
;           note: free space: A=1, B=L, C=0, D=1
;                 lens: A=1-L/f, B=L, C=-1/f, D=1
;    lambda - wavelength
;    check - check if the phase fronts are too steep for the pixelation and print out a warning message
;
;  Output:
;    wf1 - wavefront at "output" plane
;    dx - sample spacing on wf1
;
;  Notes:
;    Flux. It is assumed that the value in each pixel represents
;       the integrated flux in a square the size of the pixel.
;       Through the propagation, flux is preserved in the sense that the
;       the sum of the intensities (squared modulus of the wavefront)
;       over all pixels is preserved from input, wf0, to output, wf1.
;       To get flux density, one must divide by du^2 or dx^2 as appropriate.
;-
function fresnelft,wf0,du,A,B,C,D,lambda,ap,dx=dx,check=check
  n = (size(wf0))(1)
  i = complex(0,1)
  dx = B*lambda/(n*du)
  x = (findgen(n)-n/2)*du # (fltarr(n)+1); + 0.5*du
  y = transpose(x)
  r2 = x^2 + y^2
  ph = 2*!pi*(A/(2*B*lambda))*r2
  if (keyword_set(check)) then begin
    if (n_elements(ap) eq 0) then ap = circle(n,n,n/2,n/2,n/4-1,1)
    dph = (ph-shift(ph,1))*ap
    print,'<fresnelft> check for phase wrapping (must be < pi)',max(abs(dph))
  endif
  phaseFactor = exp(-i*ph)
  wfl = ft(wf0*phaseFactor,/inverse)/float(n)
  xl = (findgen(n)-n/2)*dx # (fltarr(n)+1)
  yl = transpose(xl)
  rl2 = xl^2 + yl^2
  phl = 2*!pi*(D/(2*B*lambda))*rl2
  phaseFactorL = exp(-i*phl)
  wfl = wfl*phaseFactorL*i
  return,wfl
end

function scongrid,im,n2,m2,cubic=cubic
  s = size(im)
  n1 = s[1]
  m1 = s[2]
  ims = shift(im,-n1/2,-m1/2)
  imos = congrid(ims,n2,m2,cubic=cubic)
  imo = shift(imos,-n2/2,-m2/2)
  return,imo
end

; ------------- demo code ---------------
n = 256
mm = 1.0e-3
microns = 1.0e-6
i = complex(0.0,1.0)
lambda = 0.55*microns
Dtele = 10.*mm
RayleighRange = Dtele^2/lambda
L = 0.02*RayleighRange 
fresnelNumber = Dtele^2/(lambda*L)
du = 2*Dtele/float(n); sqrt(L*lambda/float(n))
ap = circle(n,n,n/2,n/2,(Dtele/(2*du)),1.);,/grey)
wf0 = ap
A = 1.0
B = L
C = 0.0
D = 1.0

wf1 = fresnel(wf0,du,L,lambda) ; best for <~ 10% of RayleighRange
wf1a = fresnelft(wf0,du,A,L,C,D,lambda,ap,/check,dx=dx) ; best for >~ 10% of Rayleigh range
wf2 = ft(wf0)*float(n) ; this is Fraunhofer propagation to the far field

;disp,abs(wf0)^2,'wf0',dx=[du,du]/mm,x0=-[1,1]*(n/2)*du/mm
startPhot = total(abs(wf0)^2)
;disp,abs(wf1)^2,'fresnel',dx=[du,du]/mm,x0=-[1,1]*(n/2)*du/mm
print,'fresnel ',total(abs(wf1)^2)/startPhot
;disp,abs(wf1a)^2,'fresnel ft',dx=[dx,dx]/mm,x0=-[1,1]*(n/2)*dx/mm
print,'fresnel ft',total(abs(wf1a)^2)/startPhot
dx2 = L*lambda/(float(n)*du)
;disp,abs(wf2)^2,'far field',dx=[dx2,dx2]/mm,x0=-[1,1]*(n/2)*dx2/mm
print,'far field',total(abs(wf2)^2)/startPhot

npl = 40  ;      ///// number of intermediate planes at which to compute the wavefront /////
wf_f = complexarr(n,n,npl)
wf_ft = wf_f

Lmax = 0.2*rayleighRange ; ; ////////// total propagation distance, meters //////////
dL = Lmax/float(npl)
Lset = findgen(npl)*dL

logSpacing = 0
if (logSpacing) then begin
	Lmin = 0.0001*rayleighRange
	logLmax = alog10(Lmax)
	logLmin = alog10(Lmin)
	dlogL = (logLmax-logLmin)/float(npl)
	logLvec = findgen(npl)*dlogL + logLmin
	Lvec = 10.^logLvec
	Lvec = shift(Lvec,1)
	Lvec[0] = 0
endif

ap = circle(n,n,n/2,n/2,(Dtele/(2*du)),1.d0,/grey)
;ap[n/2,*] = 0
;ap[*,n/2] = 0
nphot = total(ap^2)
ap /= sqrt(nphot)

for k=0,npl-1 do begin
	print,strtrim(k,2),' of ',strtrim(npl,2) & wait,.1
	L = Lset[k]

; fresnel - works ok for L < 10% of Rayleigh range
	wf0 = ap
	wf1 = fresnel(wf0,du,L,lambda)
	wf_f[*,*,k] = wf1

;	fresnelFT - works ok for L > 10% of Rayleigh range
	if (L gt 0.) then begin
		dx = sqrt(L*lambda/float(n))
		; resample on dx
    x = (findgen(n)-n/2)*(dx/du)+n/2
    wf0 = interpolate(ap,x,x,/grid,cubic=-0.5)
    wf1 = fresnelft(wf0,dx,A,L,C,D,lambda)
    x = (findgen(n)-n/2)*(du/dx)+n/2
    wf1 = interpolate(wf1,x,x,/grid,cubic=-0.5)
		wf_ft[*,*,k] = wf1
	endif

endfor

dz = (Lmax/float(n))/rayleighRange
disp,abs(wf_f)^2,'fresnel method'
u = fltarr(npl,n)
for k = 0,npl-1 do begin
  u[k,*] = abs(wf_f[n/2,*,k])
endfor
vf = congrid(u,n,n)
disp,vf,dx=[dz,du],x0=[0,-(n/2)*du],'fresnel method'

disp,abs(wf_ft)^2,'fresnelFT method'
u = fltarr(npl,n)
for k = 0,npl-1 do begin
  u[k,*] = abs(wf_ft[n/2,*,k])
endfor
vf = congrid(u,n,n)
disp,vf,dx=[dz,du],x0=[0,-(n/2)*du],'fresnelFT method'

u = fltarr(npl,n)
for k = 0,npl-1 do begin
  if (Lset[k] lt 0.05*rayleighRange) then begin
    u[k,*] = abs(wf_f[n/2,*,k])
  endif else begin
    u[k,*] = abs(wf_ft[n/2,*,k])
  endelse
endfor
v = congrid(u,n,n)
disp,v,dx=[dz,du],x0=[0,-(n/2)*du]

end
