; dopost.pro - post process speckle data after bispectrum calculations
;               produces image given reference psf, object phase, and ps
;
prepspd,dataset,ps,phase,ref_ps,mtf
ioimage = spdeconvolve(ps>1,phase,ref_ps>0,mtf(0:129,*),0,110,.99)
imagefile = dataset.basename + "_image.fits"
; writefits,dataset.resultsdir + imagefile,ioimage
end
