pro sematest

centsReadFile = './centroids_ready'
readCentsFile = './read_centroids'

yesno=''
read, "Start loop? [y/n] ", yesno

if yesno eq 'y' or yesno eq 'Y' then begin
   if file_test(readCentsFile) then file_delete, readCentsFile
   print, "Loop started."
   repeat begin
      ; wait for keyboard input
      ; get_kbrd(0) will not wait, returns null string if no kbd input
      ; get_kbrd(1) waits indefinitely

      ; create command to read centroids
      openw, lun, readCentsFile, /get_lun
      free_lun, lun
      print, 'sent command to read centroids'
      ; wait for centroids_read file to be created
      while file_test(centsReadFile) eq 0 do begin
         print, 'waiting for response...'
         wait, 1
      endwhile

      ; read in centroids once indication received from python program
      readcol, 'centX.out', xcent
      readcol, 'centY.out', ycent
      file_delete, centsReadFile
      print, 'Centroids read successfully'

      endprog = get_kbrd(0)
   endrep until endprog eq 'q'
endif

end
