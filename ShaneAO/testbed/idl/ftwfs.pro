; ftwfs.pro
;  implements the Fourier-transform wavefront sensor
;
;  Reference: Poyneer, Gavel, Brase, "Fast wave-front reconstruction
;     in large adaptive optics systems with use of the Fourier transform,"
;     JOSA-A, 19, 10, Oct 2002, pp 2100-2111.
;
;  Inputs:
;     sx - array of x slopes
;     sy - array of y slopes
;     ap - mask of illuminated subapertures (1=illuminated)
;     subapFilter - option switch to divide the answer by the
;              subaperture transfer function assuming that the wavefront
;              sensor averaged the gradient over a square subaperture (bisinc)
;     geometry - Fried (0), Shering (1), or Hudgin (2) geometry.  This tells the algorithm
;            how to half-pixel shift the slope data so that it aligns properly with
;            the phase sample points.  Fried is the default.
;     boundaryFix - method of correcting the boundary conditions when
;                   slopes are not available outside an irregular aperture
;            'noCurl' means use the Poyneer/Gavel/Brase extend-grid algorithm
;            'none' means do nothing, i.e. take the full hit of
;                   boundary condition errors (you would use this if the full
;                   square aperture of slopes were available)
;            'iterative' means use an iterative method so that
;                   grad(phase) = slopes is solved as accurately as
;                   possible. Presently this applies a fixed (or
;                   Richardson) iteration; in the future a
;                   Fourier-pre-conditioned conjugate-gradient (FDPCG)
;                   algorithm will be implemented. Credit for FDPCG
;                   studies for the wavefront reconstruction problem
;                   goes to Curt Vogel, Mathematics Department,
;                   Montana State University at Bozeman.
;            Note: the iterative method, now set at 10 internal iterations, is
;                   almost 10 times more accurate than the noCurl
;                   method (although it will be slower to compute).
;  Output:
;     a - array of phases
;     rx,ry - (optional) the Fourier reconstructor coefficients
;
function ftwfs,sx,sy,ap,subapFilter=subapFilter,geometry=geometry,verbose=verbose,boundaryFix=boundaryFix,niter=niter,rx=rx,ry=ry,r=r,noiseVar=noiseVar, _extra=ex
;
;  this function solves the inverse problem:
;  s = grad(a) for a given s
;
  forward_function ftwfs_iterative
  if (n_elements(verbose) eq 0) then verbose = 0
  if (n_elements(geometry) eq 0) then geometry = 0
  if (n_elements(subapFilter) eq 0) then subapFilter = 0
;
  nx = (size(sx))[1]
  ny = (size(sy))[1]
  if (nx ne ny) then begin
    print,'<ftwfs> ERROR, size of sensor arrays not equal'
    return,0
  endif
  n = nx
;--------------------------------------------------------------------
; Either invoke the iterative method or the slope extension method
;
  if (n_elements(noiseVar) eq 0) then noiseVar = 0.
  if (n_elements(boundaryFix) eq 0) then boundaryFix = 'noCurl'
  if ((boundaryFix eq 'iterative') or (n_elements(niter) ne 0)) then begin
    bf = boundaryFix
    if (boundaryFix eq 'iterative') then bf = 'none'
    if (n_elements(niter) eq 0) then niter = 10
    a = ftwfs_iterative(sx,sy,ap,subapFilter=subapFilter,geometry=geometry,verbose=verbose,boundaryFix=bf,niter=niter,noiseVar=noiseVar,_extra=ex)
    return,a
  endif
  if (boundaryFix eq 'noCurl') then begin
    if (min(ap) eq 0) then begin
      if (keyword_set(verbose)) then print,'<ftwfs> correcting boundary'
      extend_grid,ap,sx,sy
    endif
  endif
;
  FriedGeometry = 0
  ShearingGeometry = 1
  HudginGeometry = 2
  if (n_elements(geometry) eq 0) then geometry = FriedGeometry ; default
  if (type(geometry) eq 7) then begin
    case geometry of
     'Fried': geometry = 0
     'Shearing': geometry = 1
     'Hudgin': geometry = 2
    endcase
  endif
;----------------------------------------------------------------------
;  transform the slope data
;
  fsx = ft(sx)
  fsy = ft(sy)
;----------------------------------------------------------------------
; set up the reconstruction filters
;
  i = dcomplex(0,1.)
  dkx = 2*!dpi/double(n)
  kx = (dindgen(n)-n/2) # ones(n)*dkx
  ky = transpose(kx)

  if (keyword_set(subapFilter)) then begin
    fsax = 2*sin(kx/2)/(kx+(kx eq 0))
    fsax += kx eq 0
    fsay = 2*sin(ky/2)/(ky+(ky eq 0))
    fsay += ky eq 0
    fsa = fsax*fsay
  endif else begin
    fsa = 1
  endelse
;          Invsere Laplacian
  k2 = kx^2 + ky^2    ;  the Laplacian  
  lap = k2 + noiseVar*k2^(11./6.) ;  Kolmogorov spectrum for Weiner filtering
  lap *= fsa^2
  mask = lap ne 0        ;  k0 masks out places in the Fourier domain where denominator = 0
  r = 1./(lap + (1-mask))
  r *= mask
  
;            Divergence
  cx = i*kx*fsa*mask  ;  raw reconstructors: inverse Laplacian x Divergence
  cy = i*ky*fsa*mask
;
;  account for half-pixel shifts...
;
;   Shearing sensor geometry - assumes that the slope data sample points are
;         located 1/2 pixel in the negative slope-vector direction from the phase sample points
;
  if (geometry eq ShearingGeometry) then begin
    cx *= exp(-i*kx*.5)
    cy *= exp(-i*ky*.5)
  endif
;
;   Fried geometry - assumes that the slope data sample points are located
;         1/2 pixel diagonal (in the negative, negative sense) from the phase sample points
;
;
  if (geometry eq FriedGeometry) then begin
    cx *= exp(-i*kx*.5)*exp(-i*ky*.5)
    cy *= exp(-i*kx*.5)*exp(-i*ky*.5)
  endif
;
;   Hudgin geometry - slope sample points overlap phase sample points - no pixel shift

;  -------------------------------------------------------
;
; Invoke the reconstruction filter and inverse transform the result
;
  rx = conj(cx)*r
  ry = conj(cy)*r
  fa = rx*fsx + ry*fsy
  a = real(ft(fa,/inverse))
  return,a
end

function computeSlopes,a,subapFilter=subapFilter,differenceMethod=differenceMethod,geometry=geometry,verbose=verbose,cx=cx,cy=cy
;
;  this function performs the forward computation, i.e. s = grad(a)
;  which is used in iterative algorithms to solve the inverse problem
;
;  optional outputs are cx and cy, the Fourier coefficients of the slope computer
;   (provided only when differenceMethod = 'Fourier')
;
  n = (size(a))[1]
  m = (size(a))[2]
  if (keyword_set(differenceMethod) eq 0) then differenceMethod = 'finiteDifference'
  if (keyword_set(geometry) eq 0) then geometry = 'Fried'
  if (differenceMethod eq 'finiteDifference') then begin
    if (geometry eq 'Shearing') then begin ; straight first difference
      sx = a-shift(a,1,0)
      sy = a-shift(a,0,1)
    endif
    if (geometry eq 'Fried') then begin ; average first difference with neighbor first difference one delta away in orthogonal direction
      sx = a-shift(a,1,0)
      sx = (sx + shift(sx,0,1))/2.
      sy = a-shift(a,0,1)
      sy = (sy + shift(sy,1,0))/2.
    endif
    if (geometry eq 'Hudgin') then begin ; compute as a central difference
      sx = a-shift(a,1,0)
      sx = (sx + shift(sx,-1,0))/2.
      sy = a-shift(a,0,1)
      sy = (sy + shift(sy,0,-1))/2.
    endif
  endif
  if (differenceMethod eq 'Fourier') then begin
      i = dcomplex(0,1.)
      dkx = 2*!dpi/double(n)
      kx = (dindgen(n)-n/2) # ones(m)*dkx
      dky = 2*!dpi/double(m)
      ky = ones(n) # (dindgen(m)-m/2)*dky
      mask = 1-((kx eq 0) and (ky eq 0)) ; removes piston and Nyquist components
      mask[0,*] = 0.
      mask[*,0] = 0.
      if (keyword_set(subapFilter)) then begin
          fsax = 2*sin(kx/2)/(kx+(kx eq 0))
          fsax += kx eq 0
          fsay = 2*sin(ky/2)/(ky+(ky eq 0))
          fsay += ky eq 0
          fsa = fsax*fsay
      endif else begin
          fsa = 1
      endelse
      
;          Gradient
      cx = i*kx*fsa*mask
      cy = i*ky*fsa*mask
;
      if (geometry eq 'Shearing') then begin
          cx *= exp(-i*kx*0.5)
          cy *= exp(-i*ky*0.5)
      endif
      if (geometry eq 'Fried') then begin
          cx *= exp(-i*(kx+ky)*0.5)
          cy *= exp(-i*(kx+ky)*0.5)
      endif
;     if (geometry eq 'Hudgin') then begin  ; no shift
;     endif

;
;   transform the phase data, multiply by Fourier slope coefficients and inverse transform the results
;
      fa = ft(a)
      fsx = cx*fa
      fsy = cy*fa
      sx = real(ft(fsx,/inverse))
      sy = real(ft(fsy,/inverse))
  endif
  r = dblarr(n,m,2)
  r[*,*,0] = sx
  r[*,*,1] = sy
  return,r
end

function ftwfs_iterative,sx,sy,ap,warmRestart=a0,subapFilter=subapFilter,geometry=geometry,verbose=verbose,boundaryFix=boundaryFix,niter=niter,differenceMethod=differenceMethod,noiseVar=noiseVar,_extra=ex
  n = (size(sx))[1]
  if (keyword_set(subapFilter) eq 0) then subapFilter = 0
  if (keyword_set(differenceMethod) eq 0) then differenceMethod = 'Fourier'
  s0 = reform(transpose([transpose(sx),transpose(sy)]),n,n,2)
  shat = 0*s0
  ahat = fltarr(n,n)
  ap2 = reform(transpose([transpose(ap),transpose(ap)]),n,n,2)
  if (n_elements(a0) ne 0) then begin
    ahat = a0
    shat = computeSlopes(ahat,differenceMethod=differenceMethod,subapFilter=subapFilter)   
  endif
  itergain = 1.00
  if (n_elements(niter) eq 0) then n_iterations = 10 else n_iterations = niter
  for iteration = 0,n_iterations do begin
    es = (s0-shat)*ap2
    ahat = ahat + itergain*ftwfs(es[*,*,0]*ap,es[*,*,1]*ap,ap,geometry=geometry,subapFilter=subapFilter,verbose=verbose,boundaryFix=boundaryFix,noiseVar=noiseVar,_extra=ex)
    shat = computeSlopes(ahat,differenceMethod=differenceMethod,subapFilter=subapFilter)
  endfor
  return,ahat
end

function deNyquist,a
  fa = ft(a)
  n = (size(a))[1]
  m = (size(a))[2]
  kmask = ones(n,m)
  kmask[n/2,m/2] = 0.
  kmask[*,0] = 0.
  kmask[0,*] = 0.
  fa *= kmask  ; remove piston and Nyquist components
  an = double(ft(fa,/inverse))
  return,an
end


;-----------------------------------------------------
;  test software
;-----------------------------------------------------
do_test1 = 1
do_test2 = 0
do_test3 = 0
do_test4 = 0
do_test5 = 0

if (do_test3) then do_test2 = 1
if (do_test5) then do_test4 = 1

seed = 5
n = 128;48
apDiam = 0.5*n
dtele = 8.
du = dtele/double(apDiam)
r00 = 0.2
lambda = 1.6
r0 = r00*(lambda/0.5)^(6./5.)
;
FriedGeometry = 0
ShearingGeometry = 1
HudginGeometry = 2
i = dcomplex(0,1.)
dkx = 2*!dpi/double(n)
kx = (dindgen(n)-n/2) # ones(n)*dkx
kx[0,*]=0.
kx[*,0]=0.
ky = transpose(kx)
;
ap = circle(n,n,n/2,n/2,apdiam/2,1.)
f = screengen(n,n,r0,du)
a = screengen(f,seed)*(lambda/(2*!dpi))
;a = randomn(seed,n,n)
;g = gauss2(n,n/2,n/2,n/8.)
;a = real(ftconvolve(a,g))
fa = ft(a)
kmask = ones(n,n)
kmask[n/2,n/2] = 0.
kmask[*,0] = 0.
kmask[0,*] = 0.
fa *= kmask  ; remove piston and Nyquist components
a = double(ft(fa,/inverse))

;==================================================================================
if (do_test1) then begin
;
;  Test 1: run through all the modes of ftwfs
;

;
;  ----- finite backward difference ----------
;
test = '<Test 1> '
title = 'finite backward difference'
ap = ones(n,n)
sx = a-shift(a,1,0)
sy = a-shift(a,0,1)
ahat = ftwfs(sx,sy,ap,geometry='Shearing'); ,/subapFilter)
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  finite backward difference with subap filter turned on
;
title = 'finite backward difference, with subap filter'
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Shearing')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fried Geometry averaging ------
;
title = 'finite difference Fried Geometry'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
ahat = ftwfs(sx,sy,ap,geometry='Fried')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fried Geometry averaging with subap filter turned on ------
;
title = 'finite difference Fried Geometry, with subap filter'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Fried')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fourier-derivative with 1/2 pixel shift ------
;    (this one should give zero error, since it is the inverse of ftwfs with subapFilter turned off)
;
title = 'Fourier derivative, Shearing'
fsx = fa*i*kx*exp(-i*kx*0.5)
fsy = fa*i*ky*exp(-i*ky*0.5)
sx = real(ft(fsx,/inverse))
sy = real(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Shearing')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Subap filtered derivative ------
;    Average gradient over a subaperture, computed in Fourier space, shifted 1/2 pixel
;    (this one should give zero error, since it is the inverse of ftwfs with subapFilter turned on)
;
title = 'Fourier deriv, Shearing, with subap filter'
fsax = 2*sin(kx/2)/(kx+(kx eq 0))
fsax += kx eq 0
fsay = 2*sin(ky/2)/(ky+(ky eq 0))
fsay += ky eq 0
fsa = fsax*fsay
fsx = fa*i*kx*fsa*exp(-i*kx*0.5)
fsy = fa*i*ky*fsa*exp(-i*ky*0.5)
sx = real(ft(fsx,/inverse))
sy = real(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Shearing')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fourier derivative, Fried geometry ----
;    (should give zero error)
;
title = 'Fourier derivative, Fried'
fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Fried')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fourier derivative, Fried geometry, with subap filter -----
;
title = 'Fourier deriv, Fried, with subap filter'
fsx = fa*i*kx*fsa*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*fsa*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Fried')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fourier derivative, Hudgin geometry ----
;    (should give zero error)
;
title = 'Fourier derivative, Hudgin'
fsx = fa*i*kx
fsy = fa*i*ky
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,geometry='Hudgin')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ Fourier derivative, Hudgin geometry, with subap filter -----
;
title = 'Fourier deriv, Hudgin, with subap filter'
fsx = fa*i*kx*fsa
fsy = fa*i*ky*fsa
sx = double(ft(fsx,/inverse))
sy = double(ft(fsy,/inverse))
ahat = ftwfs(sx,sy,ap,/subapFilter,geometry='Hudgin')
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ use an aperture ------
;
title = 'with aperture, finite difference Fried, no boundary correction'
sx = a-shift(a,1,0)
sx = (sx + shift(sx,0,1))/2.
sy = a-shift(a,0,1)
sy = (sy + shift(sy,1,0))/2.
;
;fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
;fxy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
;sx = real(ft(fsx,/inverse))
;sy = real(ft(fsy,/inverse))
;
ap = circle(n,n,n/2,n/2,apDiam/2.,1.)
sxa = sx*ap
sya = sy*ap
ap0 = ones(n,n)
ahat = ftwfs(sxa,sya,ap0,geometry='Fried',/subapFilter)
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;
;  ------ aperture with boundary correction ------
;
title = 'with aperture, finite difference Fried, and boundary correction'
ap = circle(n,n,n/2,n/2,apDiam/2.,1.)
sxa = sx*ap
sya = sy*ap
ahat = ftwfs(sxa,sya,ap,geometry='Fried',/subapFilter)
e = depiston(a-ahat,ap)
print,test+title+' ',rms(e,ap),' rms'
disp,[[a,ahat,e],[sx,sy,sx^2+sy^2]],name=test+title
;

endif ; do_test1

;==================================================================================
if (do_test2) then begin
;
;  Test 2:   test the slope computer, and run tests iterating around ftwfs and computeSlopes
;
test = '<Test 2> '
title = 'Fourier derivative, Fried'
ap = circle(n,n,n/2,n/2,apDiam/2.,1.); ones(n,n)
fsx = fa*i*kx*exp(-i*kx*0.5)*exp(-i*ky*0.5)
fsy = fa*i*ky*exp(-i*kx*0.5)*exp(-i*ky*0.5)
sx = double(ft(fsx,/inverse))*ap
sy = double(ft(fsy,/inverse))*ap
s0 = reform(transpose([transpose(sx),transpose(sy)]),n,n,2)
ap2 = reform(transpose([transpose(ap),transpose(ap)]),n,n,2)
ap0 = ones(n,n) ; ap ; set to all ones to force no boundary compensation

ahat = ftwfs(sx,sy,ap0,geometry='Fried')
shat = computeSlopes(ahat,differenceMethod='Fourier')
print,test+title+' ',rms(depiston(a-ahat,ap),ap),rms(sx-shat[*,*,0],ap),rms(sy-shat[*,*,1],ap),' rms'
;disp,s0-shat,'s0 - s_hat',ap=ap
iteration = 0
itergain = 1.0
n_iterations = 30

; iterate
esave = dblarr(n,n,4,n_iterations+1)
esave[*,*,0,0] = a-ahat
esave[*,*,1:2,0] = s0-shat
esave[*,*,3,0] = ahat
stat = fltarr(n_iterations+1)
stat[0] = rms((s0-shat)[*,*,0],ap)
for iteration = 1,n_iterations do begin
    es = (s0-shat)*ap2
    ahat2 = ahat + itergain*ftwfs(es[*,*,0]*ap,es[*,*,1]*ap,ap0,geometry='Fried')
    shat2 = computeSlopes(ahat2,differenceMethod='Fourier')
;    disp,s0-shat2,'s0 - s_hat iter '+strtrim(iteration,2),ap=ap
	  esave[*,*,0,iteration] = depiston(a-ahat2,ap)
    esave[*,*,1:2,iteration] = s0-shat2
    esave[*,*,3,iteration] = ahat2
    stat[iteration] = rms((s0-shat2)[*,*,0],ap)
    ahat = ahat2
    shat = shat2
    print,test+title+' iter '+strtrim(iteration,2)+' ',rms(depiston(a-ahat,ap),ap),rms(s0[*,*,0]-shat[*,*,0],ap),rms(s0[*,*,1]-shat[*,*,1],ap),' rms'
endfor

;if (n_elements(plot_exists) eq 0) then begin
;  plot,stat,/ylog,/xlog,xrange=[1,n_iterations],xtitle='iteration',ytitle='rms error in x-slope',title=test+title
;  plot_exists = 1
;endif else begin
;  oplot,stat,linestyle=2
;endelse

disp,reform(esave[*,*,0,*],n,n,n_iterations+1),test+title

endif ; do_test2

;==================================================================================
if (do_test3) then begin  ; note: need to run test 2 first to get a, ap, sx, sy, and s0
;
;   Test 3: checks funcionality of ftwfs_iterative and compares results to the noCurl method
;
  test = '<Test 3> '
  title = 'Test of iterative procedure ftwfs_iterative'
;  ahat = ftwfs_iterative(sx,sy,ap)
  ahat = ftwfs(sx,sy,ap,boundaryFix='iterative',niter=30)
  shat = computeSlopes(ahat,differenceMethod='Fourier')
  print,test+title+';  phaseReconstructionError   ;  slopeReconstructionError(x,y)'
  print,test+title+' ',rms(depiston(a-ahat,ap),ap),rms(s0[*,*,0]-shat[*,*,0],ap),rms(s0[*,*,1]-shat[*,*,1],ap),' rms'
;
  title = 'Comparison to noCurl (extend grid)'
  ahat2 = ftwfs(sx,sy,ap,boundaryFix='noCurl')
  shat2 = computeSlopes(ahat2,differenceMethod='Fourier')
  print,test+title+' ',rms(depiston(a-ahat2,ap),ap),rms(s0[*,*,0]-shat2[*,*,0],ap),rms(s0[*,*,1]-shat2[*,*,1],ap),' rms'
endif ; do_test3

;==================================================================================
if (do_test4) then begin
;
;  Test 4: runs the iterative ftwfs on wind-blown turbulence
;          experiment with iterations per measurement needed vs wind speed
;
;          Plots a comparison of closed loop wavefront error variance vs time
;              with and without the extend_grid boundary fix
;
test = '<Test 4> '
title = '/ftwfs on blowing screens/ '

blowingScreen
m = n*8
t = 0
dt = 0.001
windSpeed = 0 ; m/s
windAngle = 0 ; degrees
phi = windAngle*(!dpi/180.)
v_wind = windSpeed*[cos(phi),sin(phi)]
forward_function blowingScreen_init,blowingscreen_get

s = blowingScreen_init(n,m,r0,du,seed=seed)
sl = computeSlopes(s[0:m-1,*],/subapFilter,differenceMethod='Fourier',geometry='Hudgin')

boundaryFix_set = ['none','noCurl']
ncases = 2
for theCase=0,ncases-1 do begin
  cutoff = 0.45
  w = supergauss(n,n/2,n/2,n*cutoff,10) ; envelope the spatial extent of the Fourier-based derivative
;  w = window(n,rin=n/4)
;  w = window(4*n,rin=n) ; 2x change
  nsteps = 20
  ahat = dblarr(n,n,nsteps)
  e = dblarr(n,n,nsteps)
  a0 = dblarr(n,n)
  e_rms = dblarr(nsteps)
  for k = 0, nsteps-1 do begin
    t = k*dt
    a = blowingScreen_get(s,t,v_wind/du)*(lambda/(2*!dpi))
    a = depiston(a,ap)
    a = detilt(a,ap)
    sl = computeSlopes(a*w,differenceMethod='Fourier',/subapFilter,geometry='Fried')
    slx = sl[*,*,0]
    sly = sl[*,*,1]
;    slx = blowingScreen_get(sl[*,*,0],t,v/du)*(lambda/(2*!dpi))
;    sly = blowingScreen_get(sl[*,*,1],t,v/du)*(lambda/(2*!dpi))
    sx = slx*ap
    sy = sly*ap
    ahat[*,*,k] = ftwfs(sx,sy,ap,boundaryFix=boundaryFix_set[theCase],/subapFilter,warmRestart=a0,niter = 0,differenceMethod='Fourier',geometry='Fried')
    e[*,*,k] = depiston(a-ahat[*,*,k],ap)
    e_rms[k] = rms(e[*,*,k],ap)
    print,test+title+'step ',strtrim(k,2),' rms error = ',strtrim(e_rms[k]*1000,2),' nm' & wait,.01   
    a0 = ahat[*,*,k]
  endfor
  if (theCase eq 0) then begin
    window,/free,title=test+title,' rms wf err'
    plotwin = !d.window
    plot,e_rms,/ylog,xtitle='time',ytitle='microns, rms'
    plots,[.7,.77],[.8,.8],/normal
    xyouts,.8,.8,boundaryFix_set[theCase],/normal
    e0 = e_rms
  endif else begin
    wset,plotwin
    plot,e0,/ylog,/noerase,/nodata
    oplot,e_rms,linestyle=2
    plots,[.7,.77],[.75,.75],/normal,linestyle=2
    xyouts,.8,.75,boundaryFix_set[theCase],/normal
  endelse
  u=e
  for k=0,nsteps-1 do u[*,*,k] = ap*u[*,*,k]+(1-ap)*u[*,*,k]/100.
  disp,u,test+title+boundaryFix_set[theCase]
  filter = zeropad(ones(fix(0.8*n),fix(0.8*n)),n,n) ; low pass to xxx% of Nyquist
  u = real(ft(ft(e[*,*,nsteps-1]*ap)*filter,/inverse))
  print,test+title+boundaryFix_set[theCase]+'low-pass filtered rms = ',strtrim(rms(u,ap)*1000,2),' nm'
  disp,u,test+title+boundaryFix_set[theCase]+'low-pass filtered wf err (microns)'
endfor
;

endif ; do_test4

;==================================================================================
if (do_test5) then begin
;
;  Test 5: check if the reconstructor coefficients are correct
;
  test = '<Test 5> '
  title = 'reconstructor coefficients check'
  ahat = ftwfs(sx,sy,ap,boundaryFix='none',differenceMethod='Fourier',geometry='Fried',rx=rx,ry=ry,/subapFilter)
  fa = rx*ft(sx) + ry*ft(sy)
  ahat2 = real(ft(fa,/inverse))
  e = ahat-ahat2
  print,test+title+' ',max(abs(e)),' should be 0.0'
  
  title = 'slope coefficients check'
  cutoff = 0.4
  w = supergauss(n,n/2,n/2,n*cutoff,10) ; envelope the spatial extent of the Fourier-based derivative
;  w = window(n,rin=n/4)
;  w = ones(n,n)
  sla = computeSlopes(a*w,cx=cx,cy=cy,differenceMethod='Fourier',/subapFilter,geometry='Fried')
  fa = ft(a*w)
  sx = real(ft(cx*fa,/inverse))
  sy = real(ft(cy*fa,/inverse))
  e = [sx,sy]-[sla[*,*,0],sla[*,*,1]]
  print,test+title+' ',max(abs(e)),' should be 0.0'
  
  title = 'check coefficiets are self-inverse'
  u = rx*cx + ry*cy
  v = ry*cx - rx*cy
  umask = fltarr(n,n)
  umask[0,*] = 1 & umask[*,0] = 1 & umask[n/2,n/2] = 1
  print,test+title+' div ',min(u+umask),max(u+umask),' should be 1.0, 1.0'
  print,test+title+' curl ',min(v),max(v),' should be 0.0, 0.0'
  
  ; now, start from sx,sy and do a complete iteration using the coefficients to find ahat
  title = 'check coefficient-based iteration'
  print,test+title+' ...'
  algo = 1 ; 0) no curl removal, 1) Fourier curl removal, 2) extend grid curl removal
  
  sx *= ap
  sy *= ap   ; aperture the slope measurements
  if (algo eq 2) then extend_grid,ap,sx,sy
  fsx = ft(sx)
  fsy = ft(sy) ; Fourier transform the slope measurements
;    remove the curl part
;  fLinv = (1./(cx*conj(cx)+cy*conj(cy)+(1-kmask)))*kmask
;  fcr_xx = fLinv*(-cy*conj(cy)) + ones(n,n)
;  fcr_xy = fLinv*( cy*conj(cx))
;  fcr_yx = fLinv*( cx*conj(cy))
;  fcr_yy = fLinv*(-cx*conj(cx)) + ones(n,n)
  fLinv = (1./(kx*conj(kx)+ky*conj(ky)+(1-kmask)))*kmask
  fcr_xx = fLinv*(-ky*conj(ky)) + ones(n,n)
  fcr_xy = fLinv*( ky*conj(kx))
  fcr_yx = fLinv*( kx*conj(ky))
  fcr_yy = fLinv*(-kx*conj(kx)) + ones(n,n)
;  fsex = fLinv*(-cy*conj(cy)*fsx + cy*conj(cx)*fsy)
;  fsey = fLinv*( cx*conj(cy)*fsx - cx*conj(cx)*fsy)
;  fsx += fsex
;  fsy += fsey
;  fsxp = fcr_xx*fsx + fcr_xy*fsy
;  fsyp = fcr_yx*fsx + fcr_yy*fsy
;  fsx = fsxp
;  fsy = fsyp
;
  fsx_hat = complexarr(n,n)
  fsy_hat = complexarr(n,n)
  fa_hat = complexarr(n,n)
  divs = real(ft(conj(cx)*fsx+conj(cy)*fsy,/inverse))
  curls = real(ft(conj(cy)*fsx-conj(cx)*fsy,/inverse))
  niter = 1
  e_set = fltarr(n,n,niter)
  da_set = fltarr(n,n,niter)
  dive_set = fltarr(n,n,niter)
  curle_set = fltarr(n,n,niter)
  divh_set = fltarr(n,n,niter)
  curlh_set = fltarr(n,n,niter)
  iterGain = 1.

  cc = cy*ft(ap*ft(cx,/inverse))-cx*ft(ap*ft(cy,/inverse))
  ccmask = abs(cc) le 1e-8
  cci = (1./(cc + ccmask))*(1-ccmask)
  for k=0, niter-1 do begin
    ; ------- real operations --------
    fex = (fsx - fsx_hat)
    fey = (fsy - fsy_hat) ; calculate error

    if (algo eq 1) then begin
      fexp = fcr_xx*fex + fcr_xy*fey ; remove curl part
      feyp = fcr_yx*fex + fcr_yy*fey
      fex = fexp
      fey = feyp
    endif
    
    dfa_hat = rx*fex + ry*fey
    fa_hat += iterGain*dfa_hat
    fsx_hat = cx*fa_hat
    fsy_hat = cy*fa_hat
    sx_hat = real(ft(fsx_hat,/inverse))
    sy_hat = real(ft(fsy_hat,/inverse))
    sx_hat *= ap
    sy_hat *= ap
    if (algo eq 2) then extend_grid,ap,sx_hat,sy_hat
    fsx_hat = ft(sx_hat)
    fsy_hat = ft(sy_hat)
    ; ------- diagnostics stream --------
    e_set[*,*,k] = depiston(a - real(ft(fa_hat,/inverse)),ap)
    da_set[*,*,k] = real(ft(dfa_hat,/inverse))
    dive_set[*,*,k] = real(ft(cx*fex+cy*fey,/inverse))
    curle_set[*,*,k] = real(ft(cy*fex-cx*fey,/inverse))
    divh_set[*,*,k] = real(ft(cx*fsx_hat+cy*fsy_hat,/inverse))
    curlh_set[*,*,k] = real(ft(cy*fsx_hat-cx*fsy_hat,/inverse))
    ; ------- telemetry -------
    a_hat = real(ft(fa_hat,/inverse))
    rms_a = rms(depiston(a-a_hat,ap),ap)
    print,test+title+' ',k,' rms phase error ',rms_a*1000.,' nm'
  endfor
  a_hat = real(ft(fa_hat,/inverse))
  print,test+title+' done'
  rms_a = rms(depiston(a-a_hat,ap),ap)
  rms_sx = rms(sx - sx_hat,ap)
  rms_sy = rms(sy - sy_hat,ap)
  print,test+title+' fit:',rms_a,rms_sx,rms_sy,' (a, sx, sy), microns'
  rms_e = fltarr(niter)
  rms_da = fltarr(niter)
  rms_div = fltarr(niter)
  rms_curl = fltarr(niter)
  for k = 0,niter-1 do rms_e[k] = rms(e_set[*,*,k],ap)
  for k = 0,niter-1 do rms_da[k] = rms(da_set[*,*,k],ap)
  for k = 0,niter-1 do rms_div[k] = rms(dive_set[*,*,k],ap)
  for k = 0,niter-1 do rms_curl[k] = rms(curle_set[*,*,k],ap)
  window,/free,title=test+title+' da div curl'
  plot,rms_da/rms_da[0],/ylog,ytitle='rms da, div, curl, microns',xtitle='iteration'
  oplot,rms_div/rms_da[0],linestyle=2 ; dashed
  oplot,rms_curl/rms_da[0],linestyle=3 ; dash-dot
  window,/free,title=test+title+' a-ahat'
  plot,rms_e,/ylog,ytitle='rms a-ahat, microns',xtitle='iteration'
  disp,e_set,test+title+' a - ahat',ap=ap
endif ; do_test5
end
