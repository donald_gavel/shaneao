pro closeloop

;help, xcent
;apmask = where(xcent ne 0)
;xmask = xcent
;xmask[apmask] = 1
;xm = reform(xmask,10,10)

key = ''
while key ne 'q' do begin
   read, 'New centroids acquired from RTC? [Y/N/q] ', key
   if (key eq 'Y' or key eq 'y') then begin

      ; Read centroids from files
      readcol, 'centX.out', xcent
      readcol, 'centY.out', ycent
      readcol, 'centInt.out', centInt
      readcol, 'centMask.out', centMask
   
      ; Reconstruct phase
      phase=ftwfs(xcent,ycent,centMask,/verbose,niter=1,/rx,/ry)

      ; Call Andy's routine with phase
   endif
endwhile

print, 'Quitting...'
end

@ftwfs
