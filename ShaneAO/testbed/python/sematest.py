# To run at prompt in python 2.7 - >>>execfile('rtcsock-nh.py')
""" 
IDL to Python semaphore test
"""
import socket          # to connect to RTC socket and extract data
import os              # for scp command and file operations
import sys             # to determine platform type
import datetime        # to create date directory string for dumpData output
import subprocess      # supposedly a cleaner way to run an os process under
                       # unix
import time

# Cross-platform test to see if keyboard is hit
try:
    from msvcrt import kbhit
except ImportError:
    import termios, fcntl, sys, os
    def kbhit():
        fd = sys.stdin.fileno()
        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
        try:
            while True:
                try:
                    c = sys.stdin.read(1)
                    return True
                except IOError:
                    return False
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
            fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)


def touch(fname, times=None):
    with file(fname, 'a'):
        os.utime(fname, times)

centsReadFile = './centroids_ready'
readCentsFile = './read_centroids'

prompt = raw_input("Start loop?[y/n] ")
if prompt == 'Y' or prompt == 'y':
    print "Loop started."
    if os.path.exists(centsReadFile):
        os.remove(centsReadFile)    
    if os.path.exists(readCentsFile):
        os.remove(readCentsFile)
    print "Semaphore files deleted. Waiting for indication from IDL"
    print "Press any key to exit"
    while True:
        # See if the command to read a centroid has been given
        if os.path.exists(readCentsFile):
            print "Reading centroids..."
            time.sleep(3)
            # remove the command file
            os.remove(readCentsFile) 
            # create file saying centroids have been read
            touch(centsReadFile, None)
            print "Centroids read..."
        checkKey = kbhit()
        if checkKey:
            break
            
