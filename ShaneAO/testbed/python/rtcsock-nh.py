# To run at prompt in python 2.7 - >>>execfile('rtcsock-nh.py')
""" 
IDL to Python semaphore test
"""
import socket          # to connect to RTC socket and extract data
import os              # for scp command and file operations
import sys             # to determine platform type
import datetime        # to create date directory string for dumpData output
import subprocess      # supposedly a cleaner way to run an os process under
                       # unix
import time

# Cross-platform test to see if keyboard is hit
try:
    from msvcrt import kbhit
except ImportError:
    import termios, fcntl, sys, os
    def kbhit():
        fd = sys.stdin.fileno()
        oldterm = termios.tcgetattr(fd)
        newattr = termios.tcgetattr(fd)
        newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
        termios.tcsetattr(fd, termios.TCSANOW, newattr)
        oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
        fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)
        try:
            while True:
                try:
                    c = sys.stdin.read(1)
                    return True
                except IOError:
                    return False
        finally:
            termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
            fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)


def touch(fname, times=None):
    with file(fname, 'a'):
        os.utime(fname, times)

### Define constants here
## Current host and port values for RTC and socket
user = 'palmer25'
host = 'rtc.ucolick.org'     # or '128.114.22.53'
port = 15000

telemStop = "telem 0\x00"
telemCent = "telem 2\x00"
setCam15  = "setCamRate 1500\x00"

## Centroid string specific values
centStart  = '~S~2'                # centroid location array start
centLength = 200                   # centroid array has 200 entries - 100 x
                                   # then 100 y coords
centXLength = 100
step = 10

intStart  = '~S~3'                 # centroid intensity array start
intLength = 100                    # intensity length is 100 values

arrEnd    = '~E~'                  # array end string
    # pick a string size that guarantees you catch a message boundary
    # for centroids and intensity
buffSize  = 8192

centsReadFile = './centroids_read'
readCentsFile = './read_centroids'

### Start of program
# Set the proper scp command to transfer files back and forth
# Assumes the host this is running on has a private key whose public
# key stub is in the ~/.ssh/authorized_keys file on RTC
if sys.platform == 'win32':  # or os.name == 'nt'
    scpcmd = '"C:\Program Files\PuTTY\pscp.exe"'
    print "Make sure Pageant is up and running with Morley's key unlocked"
else:
    scpcmd = 'scp'

# Set up directory name for today's data dump
# However, RTC clock seems to change part way through the day. 
# Probably on UTC?
#now = datetime.datetime.now()
now = datetime.datetime.utcnow()
today = now.strftime("%y%m%d")

rtcdatadir = '/home/palmer25/gpi_root/aoc/data/' + today + "/"
print "RTC data dump directory set to " + rtcdatadir

# Socket type is unknown as per Marco 
soc = socket.socket()
soc.settimeout(20) # seconds
socketConnectFlag = 0

try:
    soc.connect((host, port))    
    socketConnectFlag = 1
except:
    print "Cannot connect to " + host + " on port: " + str(port)
    print "Check if AOC is running on RTC by typing 'lsmod' at prompt"
    print "and seeing if dpio2 and SysCon are listed towards the bottom."
    print "Or socket could be in use by other client."

# Check if server is responding to commands
telemFlag = 0
soc.send(telemStop)
response = ' '

if socketConnectFlag:
    print "Success connecting to " + host + " on port: " + str(port)
    try:
        while response.find('ACK') == -1 and response.find('telem 0') == -1:
            response = soc.recv(buffSize)
        print "Socket flushed"
        telemFlag = 1
    except:
        print "Socket hanging. Restart AOC on RTC"


telemStreamFlag = 0
# Set telem flag means server is accepting and responding to commands
if telemFlag:
    # set camera capture rate
    print "Set camRate to highest possible rate that works"
    camPrompt = raw_input("Set camera frame rate: [50/100/500/1500] ")
    allowedRates = ['50', '100', '250', '500', '1000', '1500']
    if camPrompt in allowedRates:
        camRate = camPrompt
    else:
        camRate = '100'
    camRateCmd = "setCamRate " + camRate + "\x00"
    soc.send(camRateCmd)
    response = soc.recv(1024)
    if  response.find('ACK') != -1 and response.find('setCamRate') != -1:
        print "Cam rate set to " + camRate +" frames per second"

    # Send telem command to get centroid stream
    soc.send(telemCent)
    response = soc.recv(1024)
    # First response if ACK for telem command. Following one should have
    # centroids
    response = soc.recv(buffSize)
#    print response
    if  response.find(centStart) != -1 and response.find(arrEnd) != -1:
        print "Telemetry centroid stream appears to be working."
        telemStreamFlag = 1
    else:
        print "No telemetry. Setting camRate to 100"
        soc.send("setCamRate 100\x00")
        print "Restart AOC on RTC for new rate to take effect"

if telemStreamFlag:
    prompt = raw_input("Start centroid acquisition loop?[y/n] ")
    if prompt == 'Y' or prompt == 'y':
        print "Loop started."
        if os.path.exists(centsReadFile):
            os.remove(centsReadFile)    
        if os.path.exists(readCentsFile):
            os.remove(readCentsFile)
            print "Semaphore files deleted. Waiting for indication from IDL"
            print "Press any key to exit"
            while True:
                # See if the command to read a centroid has been given
                if os.path.exists(readCentsFile):
                    print "Reading centroids..."
                    centFlag = False  # check if centroid array was populated properly
                    intFlag  = False  # check if intensity array populated properly
                    # Drain the recv buffer to get the most recent telemetry
                    response = soc.recv(buffSize)
                    while len(response) == buffSize:
                        response = soc.recv(buffSize)
                    # Read fresh data from the buffer
                    while centFlag == False:
                        socBuff = soc.recv(buffSize)         # socket buffer
                        socBufflines = socBuff.splitlines()  # splits buffer at '\n'
                        print socBufflines

                        for s in socBufflines:
                            # Search for centroid x,y arrays
                            if s.startswith(centStart) and s.endswith(arrEnd):
                                # Drop the start and end character in the string
                                clist = s.split()[1:-1]
                                # xlist = clist[:centXLength]
                                # print len(xlist)
                                # Get the x coordinates and write to a file
                                cx = open("centX.out", "w")
                                for item in clist[:centXLength]:
                                    cx.write("%s\n" % item)
                                cx.close()

                                # ylist = clist[centXLength:]
                                # print len(ylist)
                                # Get the y coordinates and write to a file
                                cy = open("centY.out", "w")
                                for item in clist[centXLength:]:
                                    cy.write("%s\n" % item)
                                cy.close()

                                centFlag = True              # centroids acquired 
                                print "Centroid X, Y files written"

                    touch(centsReadFile, None)
                    print "Centroids read..."
                    checkKey = kbhit()
                    if checkKey:
                        break

# If telemetry working, stop telemetry before exit
# Ask to send quit command to server if done for the day or aoc restart
# needed
if telemFlag == 1:
    print "Stopping telemetry..."
    soc.send(telemStop)
    if prompt == 'N':
        prompt = raw_input("Send quit command to server? [Y/N] ")    
        if prompt == 'Y':
            soc.send("quit\x00")
            quitFlag = 1
            print "Server quit command sent. Stop AOC on RTC."

# Close socket
# Come directly here is socket connect fails or connect passes and telemetry
# fails
print "Closing socket..."
soc.shutdown(2)
soc.close()
print "Socket closed."
