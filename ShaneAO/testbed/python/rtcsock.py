# To run at prompt in python 2.7 - >>>execfile('rtcsock.py')
""" Opens a connection to the RTC AOC socket and attempts to either
read centroid telemetry data (with the telem command) or fits images
with dumpData and scp of fits files
 """
import socket          # to connect to RTC socket and extract data
import select          # waiting for i/o completion on sockets
import os              # for scp command
import sys             # to determine platform type
import datetime        # to create date directory string for dumpData output
import time            # to make routine wait between telemetry dumps
import subprocess      # supposedly a cleaner way to run an os process under
                       # unix

### Define constants here
## Current host and port values for RTC and socket
user = 'palmer25'
host = 'rtc.ucolick.org'     # or '128.114.22.53'
port = 15000

## Server specific values
connectMsg = "~S~0NAK:         'ver'~E~" # server reply to bogus 'ver' command
connectMsgSize = len(connectMsg)   
telem0Msg =   "~S~0ACK:         'telem 0'~E~"    # acknowledge telem command
telem0MsgSize = len(telem0Msg)
telem2Msg =   "~S~0ACK:         'telem 2'~E~"    # acknowledge telem command
telem2MsgSize = len(telem2Msg)

telemStop = "telem 0\x00"
telemCent = "telem 2\x00"
setCam15  = "setCamRate 1500\x00"

## Centroid string specific values
centStart  = '~S~2'                # centroid location array start
centLength = 200                   # centroid array has 200 entries - 100 x
                                   # then 100 y coords
centXLength = 100
step = 10

intStart  = '~S~3'                 # centroid intensity array start
intLength = 100                    # intensity length is 100 values

arrEnd    = '~E~'                  # array end string
    # pick a string size that guarantees you catch a message boundary
    # for centroids and intensity
buffSize  = 8192

### Define functions here
def empty_socket(sock):
    """remove the data present on the socket"""
    input = [sock]
    while 1:
        inputready, o, e = select.select(input,[],[], 0.0)
        if len(inputready)==0: break
        for s in inputready: s.recv(1)

### Start of program
# Set the proper scp command to transfer files back and forth
# Assumes the host this is running on has a private key whose public
# key stub is in the ~/.ssh/authorized_keys file on RTC
if sys.platform == 'win32':  # or os.name == 'nt'
    scpcmd = '"C:\Program Files\PuTTY\pscp.exe"'
    print "Make sure Pageant is up and running with Morley's key unlocked"
else:
    scpcmd = 'scp'

# Set up directory name for today's data dump
# However, RTC clock seems to change part way through the day. 
# Probably on UTC?
#now = datetime.datetime.now()
now = datetime.datetime.utcnow()
today = now.strftime("%y%m%d")

rtcdatadir = '/home/palmer25/gpi_root/aoc/data/' + today + "/"
print "RTC data dump directory set to " + rtcdatadir

# Socket type is unknown as per Marco 
# if TCP socket uncomment next line
#s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# if UDP socket uncomment next line
#s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

soc = socket.socket()
soc.settimeout(20) # seconds
socketConnectFlag = 0

try:
    soc.connect((host, port))    
    socketConnectFlag = 1
except:
    print "Cannot connect to " + host + " on port: " + str(port)
    print "Check if AOC is running on RTC by typing 'lsmod' at prompt"
    print "and seeing if dpio2 and SysCon are listed towards the bottom."

# Check if server is responding to commands
telemFlag = 0
soc.send(telemStop)
response = ' '

if socketConnectFlag:
    print "Success connecting to " + host + " on port: " + str(port)
    try:
        while response.find('ACK') == -1 and response.find('telem 0') == -1:
            response = soc.recv(buffSize)
        telemFlag = 1
    except:
        print "Socket hanging. Restart AOC on RTC"

print "Socket flushed"

telemStreamFlag = 0
if telemFlag:
    # set camera capture rate
    print "Set camRate to highest possible rate that works"
    camPrompt = raw_input("Set camera frame rate: [50/100/500/1500] ")
    allowedRates = ['50', '100', '250', '500', '1000', '1500']
    if camPrompt in allowedRates:
        camRate = camPrompt
    else:
        camRate = '100'
    camRateCmd = "setCamRate " + camRate + "\x00"
    soc.send(camRateCmd)
    response = soc.recv(1024)
    if  response.find('ACK') != -1 and response.find('setCamRate') != -1:
        print "Cam rate set to " + camRate +" frames per second"

    # Send telem command to get centroid stream
    soc.send(telemCent)
    response = soc.recv(1024)
    # First response if ACK for telem command. Following one should have
    # centroids
    response = soc.recv(buffSize)
#    print response
    if  response.find(centStart) != -1 and response.find(arrEnd) != -1:
        print "Telemetry centroid stream appears to be working."
        telemStreamFlag = 1
    else:
        print "No telemetry. Setting camRate to 100"
        soc.send("setCamRate 100\x00")
        print "Restart AOC on RTC for new rate to take effect"

if telemStreamFlag:
    prompt = 'Y'
    while prompt != 'N':
        prompt = raw_input("Get centroids?[Y/N] ")
        if prompt == 'Y' or prompt == 'y':
            centFlag = False  # check if centroid array was populated properly
            intFlag  = False  # check if intensity array populated properly
            # Drain the recv buffer to get the most recent telemetry
#            empty_socket(soc)

            response = soc.recv(buffSize)
            while len(response) == buffSize:
                response = soc.recv(buffSize)
            # Read fresh data from the buffer
            while centFlag == False:
                socBuff = soc.recv(buffSize)         # socket buffer
                socBufflines = socBuff.splitlines()  # splits buffer at '\n'
#               print socBufflines

                for s in socBufflines:
                # Search for centroid x,y arrays
                    if s.startswith(centStart) and s.endswith(arrEnd):
                        # Drop the start and end character in the string
                        clist = s.split()[1:-1]
                        # xlist = clist[:centXLength]
                        # print len(xlist)
                        # Get the x coordinates and write to a file
                        cx = open("centX.out", "w")
                        for item in clist[:centXLength]:
                            cx.write("%s\n" % item)
                        cx.close()

                        # ylist = clist[centXLength:]
                        # print len(ylist)
                        # Get the y coordinates and write to a file
                        cy = open("centY.out", "w")
                        for item in clist[centXLength:]:
                            cy.write("%s\n" % item)
                        cy.close()

                        centFlag = True              # centroids acquired 
                        print "Centroid X, Y files written"

            # No need to bother with conversion - just write out as a string 
                # centArray = [float(st) for st in s.split()[1:-1]]
                # Split centArray into 2 10x10 lists for X & Y coords
                # centXArray = [centArray[i:i+step] for i in range(0, centXLength-1, step)]
                # centYArray = [centArray[i:i+step] for i in range(centXLength, centLength-1, step)]
                # Search for the intensity array
#                if centFlag and s.startswith(intStart) and s.endswith(arrEnd):
                    # intList = s.split()[1:-1]
#                    intCen = open("centInt.out", "w")
#                    for item in s.split()[1:-1]:
#                       intCen.write("%s\n" % item)
#                    intCen.close()
#                    intFlag = True               # centroid intensities acquired

#    s.send("dumpData 0x1 1 2\x00")
#    fitsfile = 'rawImage_0001.fits'
#    source = user + "@" + host + ":" + rtcdatadir + fitsfile
#    dest = ' .'
#    if sys.platform == 'win32':
#        getFits = os.system(scpcmd + " " + source + dest)
#    else:
#        getFits = subprocess.Popen([scpcmd, source, dest]).wait()


# If telemetry is fubared then quit the server
quitFlag = 0
if telemFlag == 0 or telemStreamFlag == 0 or prompt == 'N':
    prompt = raw_input("Send quit command to server? [Y/N] ")    
    if prompt == 'Y':
        soc.send("quit\x00")
        soc.close()
        quitFlag = 1
        print "Server quit command sent. Socket closed. Stop AOC on RTC."

# Stop telemetry and close server connection cleanly
if quitFlag == 0:
    if socketConnectFlag == 1:
        print "Stopping telemetry..."
        soc.send(telemStop)
    print "Closing socket..."
    soc.shutdown(2)
    soc.close()
    print "Socket closed."
