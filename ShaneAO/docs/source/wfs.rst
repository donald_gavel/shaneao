
WFS
---
    
.. automodule:: wfs

.. autoclass:: wfs
   :members: gmodes, zmodes, m2a, Hmatrix, Rmatrix, subaps, sense, show, type
