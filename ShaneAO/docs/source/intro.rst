Introduction
------------

This explains how to start up and use the ShaneAO real-time controller,
and how to set up parameters to run it.

Basic introduction to the rtc.py and rtc2.c codes
-------------------------------------------------

These files are a baseline for the real-time reconstructor, written in c, the supervisor, written in python, and the rtc parameter file set.

Files:

    * :ref:`rtc.py <rtc>`     -   The rtc supervisor
    * :ref:`rtc2.c <rtc2>`    -   The rtc c code (c-extension)
    * setup.py                -   The instructions to python to create the c-extension. Invoke with bash-prompt$ python setup.py build. This builds the rtc2.so dynamic library.
    * :ref:`parameterFiles <file-system>`      -   Directory containing the .fits parameter files
    * :ref:`parameterFiles/scripts <scripts>`  -   Directory contining scripts that generate the .fits parameter files

Right now, the code is only a baseline, not yet connected to the camera (EDT) or deformable mirror (dpio) modules.
What it does is

    * Connect python to the c-extension
    * Loads parameter files into the c-extension memory
    * Defines a basic set of "supervisory" commands

The rtc.so needs to be built and stored in a library directory visible to python.
python setup.py install --home==(path to library dirctory)

To run::

    python
    from rtc import *
    u = rtc('16x')

That should do these initialization steps:

    * import all the needed python packages, notable numpy and pyfits
    * actally load a set of parameter files into rtc2
    * creates an object that has a number of methods that implement supervisory functions, and instance variables one-one corresponding to parametric and real-time data.

Supervisory functions are described here: :ref:`rtc <rtc>`.

Low-level operations within the c-extension module are described in :ref:`rtc2 <rtc2>`. 
Note that the rtc2 object methods are lower level than the rtc object methods. A "user" at the supervisory level does not need to call them directly, as they are orchestrated by the supervisor.

