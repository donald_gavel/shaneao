README.txt

Basic introduction to the rtc.py and rtc2.c codes

These files are a baseline for the, rtc reconstructor written in c, the supervisor written in python, the python-c extension connecting the two, and the rtc parameter file set.

Files:
rtc.py                      The rtc supervisor
rtc2.c                      The rtc c code
setup.py                    The instructions to python to create the c-extension. Invoke with bash-prompt$ python setup.py build. This builds the rtc2.so dynamic library.
parameterFiles              Directory containing the .fits parameter files
parameterFiles/scripts      Directory contining scripts that generate the .fits parameter files

Right now, the code is only a baseline, not yet connected to the camera (EDT) or deformable mirror (dpio) modules.
What it does is
- Connect python to the c-extension
- Loads parameter files into the c-extension memory
- Defines a basic set of "supervisory" commands

The rtc.so needs to be built and stored in a library directory visible to python.
python setup.py install --home==(path to library dirctory)

To run:
    python
    from rtc2 import *

That should do these initialization steps:
- import all the needed python packages, notable numpy and pyfits
- actally load a set of parameter files into rtc2
- creates and object that has a number of methods that implement supervisory functions, and instance variables one-one corresponding to parametric and real-time data.

Supervisory functions defined in rtc.py are  (u is the object name):

u.load()            Reloads the parameter files
u.close_loop()      Close the AO control loop
u.open_loop()       Open the AO control loop
u.go()              Start the rtc thread (presently this only sets a flag). Note: from the supervisor's perspective this is a low-level call. load, open_loop, and close_loop orchestrate the go and stop states of the rtc thread.
u.stop()            Stop the rtc thread (presently this only sets a flag). Note: from the supervisor's perspective this is a low-level call. load, open_loop, and close_loop orchestrate the go and stop states of the rtc thread.
u.set_gain(gain)    Set the feedback control gain
u.status()          Returns the current AO status (loops open or closed, rtc thread running)
u.oneStep()         A python/numpy version of the operations performed in rtc.c. For checking the rtc thread is producing correct results.
u.manyStep(n)       Run oneStep many times.

Functions defined in rtc2.so:
rtc2.hello_world                    The usual "am I alive" routine
rtc2.param_list                     List the loaded parameter sets and their pointers in memory
rtc2.get_state                      Return the rtc thread state: basically 1: running, 2: stopped. 0 is reserved for "shutting down"
rtc2.set_state(state)               Set the rtc thread state
rtc2.set_sizes(xxx)                 Private - support routine for loading size parameters
rtc2.set_verbosity(verbosity)       Set the amount of chatter back from rtc2 telling what is loaded, what is happening, etc. verbosity=0 (the default) is quiet
rtc2.load(parameterName,data)       Load a particular parameter, which must be a numpy array.
rtc2.iload(mapName,mapData)         Loads integer map parameter arrays (deinterlace map, dm driver descramble map)
rtc2.peek(parameterName)            Returns a numpy array with the named parameter or data. The peeked data is literally a peek into rtc2's memory space (the returned numpy array "points" to the data).

Note these rtc2 routines are lower level than the rtc.py object methods. A "user" at the supervisory level does not need to call them directly, as they are orchestrated by the supervisor.

There will be more capabilities added as we progress, including:
supervisor command to change camera frame rate(s)
supervisor commands to load static deformable mirror shapes
supervisor command to switch wavefront sensing modes (8x, 16x, 30x)
supervisor command to change centroider algorithms
some peek data will be the basis of deriving offloading commands (via KTL) (telescope track...)
some peek data will be pipelined into user displays (camera data, mirror commands)
we need to add specific support for LGS mode: tip/tilt camera processing, laser uplink tip/tilt control.
supervisor interface to a user-level GUI, running on another host machine

