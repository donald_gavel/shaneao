.. ShaneAO documentation master file, created by
   sphinx-quickstart on Thu May  9 15:14:50 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ShaneAO's documentation
==================================

The Real-Time Controller
------------------------

.. toctree::
   :maxdepth: 2

   intro
   rtc
   rtc2
   file-system
   Preparation Scripts <scripts>
   AO DMs and Sensors <drivers>

Operations
----------

.. toctree::
   :maxdepth: 1

   GUI
   Motion Stages <motors>
   
Science Camera
--------------

.. toctree::
   :maxdepth: 1
   
   Science Camera <sharcs>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

