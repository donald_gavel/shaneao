README

Author: Don Gavel
Date: 3/26/2013
Update: 4/4/2013

This directory contains files which define the ShaneAO rtc parameter set

Please refer to the Knowledge Tree directories for descriptions of the algorithms and the data sets used by rtc
     https://shaneao.knowledgetree.com/007j#
        RTC Software Definitions.pdf
        ShaneAO_RTC_DataRequirements.xlsx

In this directory:

aa_filt_alpha.fits      anti-alias filter coefficients
actuator_defaults.fits  default settings of actuators - nominally the "flat" surface settings
actuator_limits.fits    limits of the actuators
cent_weights_cog.fits   centroider weight for center of gravity algorithm
cent_weights_q2.fits    centroider weight for 2x2 quadcell algorithm
cent_weights_q4.fits    centroider weight for 4x4 quadcell algorithm
i_map.fits              de-interlace map for the wavefront sensor camera
i_map_subaps_16x.fits   de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
i_map_subaps_30x.fits   de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
i_map_subaps_8x.fits    de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
integrator_bleeds.fits  decay weights for each integrator
ref_cent_16x.fits       reference centroids
ref_cent_30x.fits       reference centroids
ref_cent_8x.fits        reference centroids
tweeter_map.fits        descramble map for the tweeter driver - maps 32x32 array of actuators into dpio2 1024 element buffer
tt_dark.fits            tip/tilt camera dark
tt_flat.fits            tip/tilt camera flat field compensation
tt_centWts.fits         tip/tilt camera centroider weights
wfs_dark.fits           wfs camera average dark frame
wfs_flat.fits           wfs camera flat field compensation

The scripts directory has IDL scripts that can generate a basic set of rtc parameter frames.

deint_map.pro           writes the camera de-interlace maps
illums.pro              determines the set of illuminated subapertures depending on wfs mode and telescope secondary
param_gen.pro           generates parameter sets with the exception of de-interlace and reconstructor matrix
pMatrix.pro             generates the "pM" matrix needed by the matrix reconstructor


