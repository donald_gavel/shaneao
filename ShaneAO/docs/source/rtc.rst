.. _rtc:

RTC
===

.. module:: rtc

.. autoclass:: rtc
   :members: load, set_gain, close_loop, open_loop, stop, go, status, oneStep, manyStep

RTC class data
--------------

.. data:: pdict8x

.. data:: pdict16x

.. data:: pdict30x

    These are the dictionaries that map rtc variable names to their FITS files. 

RTC instance data
-----------------

An rtc contains instance variables for every parameter that is loaded from FITS files, plus a few
internal ones of its own. Here
are some of the important ones. You can list these
data names with the command::

    self.__dict__.keys()

.. module:: self

**Control**

.. data:: gain

    The gain of the control loop. It multiplies controlMatrix.

.. data:: controlMatrix

    The control matrix, as loaded from the FITS file.

.. data:: cm

    The control matrix after it is multiplied by the gain. This is loaded into rtc2.

.. data:: mode

    The string '8x', '16x', or '30x' depending on the wavefront sensing mode.

.. data:: loop

    Loop state - either 'open' or 'closed'
    
.. data:: savedGain

    The when the loop is openned, the closed loop :py:data:`gain` is stored here before it is
    is set to zero. :py:data:`gain` gets restored to this value when the loop is closed again.

.. data:: defaultGain

    The system default gain. It can be restored with :py:func:`set_gain`('default')

**Wavefront Sensor**

.. data:: wfs

    Most recent camera image (de-interlaced).
    
.. data:: wfs_background

    The wavefront sensor background image
    
.. data:: wfs_flat

    The wavefront sensor camera flat-field
    
.. data:: npix

    The size of the wavefront sensor array, in pixels across.
    
.. data:: i_map

    The de-interlace map for the wavefront sensor camera. The camera data
    come in scrambled. Depending on application, this
    map can convert to a 160x160 delinterlaced image or map to a python-list of 25x25 images
    one for each subaperture. The later is more commonly used as it is the most
    efficient way to progress forward to the centroider.

**Tip/Tilt Camera**

.. data:: ntt

    The size of the tip/tilt sensor array, in pixels across.
    
.. data::tt_background

    Background image for the tip/tilt camera
    
.. data:: tt_flat

    Tip/tilt camera flat-field
    
**Centroider**
    
.. data:: s

    The most recently measured centroids
    
.. data:: inten

    List of most recently measured Hartmann subaperture intensities
    
.. data:: s_ref

    The reference centroids, stacked as all x centroids followed by
    all y centroids
    
.. data:: ns

    Number of Hartmann subapertures
    
.. data:: nsub

    The size of the Hartmann subaperture, in pixels across.
    
.. data:: centWts

    The centroider weights. This is a 3 element python list of
    5x5 coefficients: x, y, intensity
    
.. data:: tt_centWts

    Array of tip/tilt centroider weights
    
**State Vector**

.. data:: na

    The length of the controller state vector = #tweeter acts + #woofer acts +
    #auxilliary variables for filters

.. data:: a

    The controller state vector.
    
.. data:: da

    The most recent delta-actuator commands
    
.. data:: integrator_bleeds

    Integrator bleed coefficients. 1.0 = no bleed
    
.. data:: a0

    The reference actuator values. Integrator leaks leak to this
    value. Good practice is to set this to the mirror-flattening
    values.

.. data:: a_limit

    Upper and lower limits for the actuator commands.
    
.. data:: na_tw

    Number of tweeter actuators

.. data:: tweeter_map

    The mapping from tweeter actuator
    index (raster scan on 32x32 grid) to the memory
    location in the buffer loaded to the dpio2 driver
    
.. data:: buf

    The command buffer to the tweeter. This is the buffer that is
    sent to the dpio2 driver, so it is mapped accordingly using
    :py:data:`tweeter_map`. 
    
.. data:: na_woof

    Number of woofer actuators

.. data:: woof

    The woofer actuator command list. This list is sent to the
    woofer driver via a socket interface.
    
.. data:: nfilt

    Number of state variables associated with the filters
    
.. data:: aa_filt

    Anti-alias filter coefficient.
    
RTC test code
-------------

The code testRTC.py runs a test script for the RTC that does not require
the hardware drivers. Invoke as::

    execfile('testRTC.py')
    dimg(aset)
    

To Do
-----

There will be more capabilities added as we progress, including:

    * supervisor command to change camera frame rate(s)
    * supervisor commands to load static deformable mirror shapes
    * supervisor command to switch wavefront sensing modes (8x, 16x, 30x)
    * supervisor command to change centroider algorithms
    * some peek data will be the basis of deriving offloading commands (via KTL) (telescope track...)
    * some peek data will be pipelined into user displays (camera data, mirror commands)
    * we need to add specific support for LGS mode: tip/tilt camera processing, laser uplink tip/tilt control.
    * supervisor interface to a user-level GUI, running on another host machine
