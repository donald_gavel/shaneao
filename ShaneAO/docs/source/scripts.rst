.. _scripts:

Scripts
=======

Scripts are pieces of "offline" code that can be used
to create parameter file sets for the
real-time controller. Presently, there are a number
of scripts written in IDL language, hopefully soon to
be ported to python. There is one key script, wfs,
written in python, that defines the reconstruction
matrix in a clean and general mathematical fashion.

.. toctree::

    wfs
    tweeter

IDL scripts
-----------

=============           ============================================================================================
IDL script              Function
=============           ============================================================================================
deint_map.pro           writes the camera de-interlace maps
illums.pro              determines the set of illuminated subapertures depending on wfs mode and telescope secondary
param_gen.pro           generates parameter sets with the exception of de-interlace and reconstructor matrix
pMatrix.pro             generates the "pM" matrix needed by the matrix reconstructor
=============           ============================================================================================
