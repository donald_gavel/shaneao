.. _rtc2:

RTC2
====

rtc2 is the c-extension module that implements the
real-time control engine. This engine does
the real-time reconstructor calculations and communicates
to the AO wavefront sensor, tip/tilt sensor, woofer DM, and
tweeter DM.

The operating modes of the control engine are
purposely made simple, as are the data structures
it deals with, so that it can be concentrated
on performing only the time-critical AO functions.
This puts the burden of controller
definition, consistency checking, calibration operations,
user interface, etc.
on the supervisory python code module :ref:`rtc <rtc>` and
the :ref:`parameter file set <file-system>`.

For the most part, the "user" does not need
to know much about the rtc2 extension module methods,
as all interfaces
to it are through the :py:class:`rtc` python module.

For further details about the DM and camera drivers,
see :ref:`low-level drivers <drivers>`.

.. py:function:: init()

   initialize the names, pointers, and  (maximum) sizes in the data and parameter dictionaries
   then allocate the memory for each

   all data space is preallocated at this init step
   so provide room for the largest case

.. py:function:: param_list()

   list all the data and parameter names, pointers, and sizes

.. py:function:: hello_world()

   make sure the rtc2 module is imported and responding

.. py:function:: get_state()

   return the state of the controller

.. py:function:: set_state(state)

   set the state of the controller (0: exit, 1: go, 2: pause)

.. py:function:: set_sizes(tuple)

   set various scalar parameters that are associated with sizes of objects

.. py:function:: set_verbosity(vebosity_level)

   sets how much verbiage feedback should spew out to the terminal
   when rtc2 methods are executed. Default of 0 means quiet.

.. py:function:: load(parameterName,parameterArray)

   load the given parameter array into the memory slot allocated
   for it in the c-extension module.
   
   
.. py:function:: iload(parameterName,parameterArray)

   same as load, but for integer arrays like the de-interlace
   maps
   
.. py:function:: peek(parameterName)

   returns a python array whose data entry points into the c-extension
   memory at the allocated location. This gives the python
   interface module :py:class:`rtc` direct access to the
   real-time memory region

.. py:function:: recon(camData)

    This is a python-callable wrapper for the internal c code that
    does the real-time recontructor calculations. The camera data
    is assumed to be scrambled, i.e. the recontstructor de-interlaces it
    as a first step. The data must be cast as type uint16, as in::
    
        d.astype(uint16)


   
