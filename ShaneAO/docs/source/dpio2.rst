.. _dpio2:

:mod:`dpio2` - Initialize and Drive the Tweeter DM
==================================================

dpio2 refers to the output card (a "digital program input/output" card)
that ultimately drives the tweeter deformable mirror.

*class* Output
~~~~~~~~~~~~~~

.. py:class:: dpio2.Output(deviceNumber, outputSize)

   Creates a new object of type dpio2.Output. In ShaneAO, the deviceNumber
   is 1 and the outputSize is 1024.::
   
        import dpio2
        foo = dpio2.Output(1,1024)
        buf = [0,] * 1024
        buf[0] = 43000
        foo.write(buf)
   
Methods
-------

.. py:method:: dpio2.Output.write(buffer)

    Writes the data in buffer, a tuple which must have the length
    outputSize, to the dpio2 driver. These int values are sent to
    a D/A converter. The valid range of these values is 0..65535,
    which will be converted to an analog output voltage from 0 to ~200V.

.. py:method:: dpio2.Output.current

    Returns a tuple of the current buffer contents.

.. py:method:: dpio2.Output.logging

    Configure logging for this device.

.. py:method:: dpio2.Output.stdout

    Print the current value of the buffer to stdout. Only of any use for debugging.

.. py:method:: dpio2.Output.timing

    Return a dictionary containing the current timing information recorded
    for this :class:`DPIO` instance.

Data
----

.. py:attribute:: dpio2.Output.buffer_size

    DMA buffer size

.. py:attribute:: dpio2.Output.device

    DPIO2 device number

.. py:attribute:: dpio2.Output.frequency

    Strobe frequency
   