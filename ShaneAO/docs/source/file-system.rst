.. _file-system:

RTC File System
===============

Parameter files define the quasi-constant numerical values used in the real-time computational engine,
basically defining the AO system's behavior. Some files define what would
be considered the system-defining components,
such as the reconstruction matrix, and some files provide operational
information, such as the most recents set of camera darks and reference centroids.

All the files are in FITS format.

Please refer to the Knowledge Tree directories for descriptions of the algorithms and the data sets used by rtc

`Knowledge Tree Link <https://shaneao.knowledgetree.com/007j>`_

    * RTC Software Definitions.pdf
    * ShaneAO_RTC_DataRequirements.xlsx

Files in the current system
---------------------------

======================  =====================================================================================================================================
Filename                Purpose
======================  =====================================================================================================================================
aa_filt_alpha.fits      anti-alias filter coefficients
actuator_defaults.fits  default settings of actuators - nominally the "flat" surface settings
actuator_limits.fits    limits of the actuators
cent_weights_cog.fits   centroider weight for center of gravity algorithm
cent_weights_q2.fits    centroider weight for 2x2 quadcell algorithm
cent_weights_q4.fits    centroider weight for 4x4 quadcell algorithm
i_map.fits              de-interlace map for the wavefront sensor camera
i_map_subaps_16x.fits   de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
i_map_subaps_30x.fits   de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
i_map_subaps_8x.fits    de-interlace map for the wavefront sensor camera - sends pixels associated with illuminated subaps into contiguous groups of elements
integrator_bleeds.fits  decay weights for each integrator
ref_cent_16x.fits       reference centroids
ref_cent_30x.fits       reference centroids
ref_cent_8x.fits        reference centroids
tweeter_map.fits        descramble map for the tweeter driver - maps 32x32 array of actuators into dpio2 1024 element buffer
tt_dark.fits            tip/tilt camera dark
tt_flat.fits            tip/tilt camera flat field compensation
tt_centWts.fits         tip/tilt camera centroider weights
wfs_dark.fits           wfs camera average dark frame
wfs_flat.fits           wfs camera flat field compensation
======================  =====================================================================================================================================
