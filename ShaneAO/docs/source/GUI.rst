.. _GUI:

Graphical User Interface
========================

The purpose of the Graphical User Interface (GUI) will
be to provide a quick and easy to use display and
control panel for on-sky operations of ShaneAO.

The user/operator GUI will
likely be written in pyton or tcl and will perhaps
resemble the GUIs of the present AO system or ViLLaGES.
This will have a number of elements:

    * Display of the raw wavefront sensor and tip/tilt camera
      images
    * Buttons for openning and closing the AO loops
    * An interactive logging sheet
    * Controls for the optical bench configuration (motor GUIs)
