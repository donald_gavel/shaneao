\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Basic introduction to the rtc.py and rtc2.c codes}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}RTC}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}RTC class data}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}RTC instance data}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}To Do}{9}{section.3.3}
\contentsline {chapter}{\numberline {4}RTC2}{11}{chapter.4}
\contentsline {chapter}{\numberline {5}RTC File System}{13}{chapter.5}
\contentsline {section}{\numberline {5.1}Files in the current system}{14}{section.5.1}
\contentsline {chapter}{\numberline {6}Scripts}{15}{chapter.6}
\contentsline {section}{\numberline {6.1}WFS}{15}{section.6.1}
\contentsline {section}{\numberline {6.2}Tweeter}{17}{section.6.2}
\contentsline {section}{\numberline {6.3}IDL scripts}{18}{section.6.3}
\contentsline {chapter}{\numberline {7}Graphical User Interface}{19}{chapter.7}
\contentsline {chapter}{\numberline {8}Advanced Topics}{21}{chapter.8}
\contentsline {section}{\numberline {8.1}\texttt {dpio2} - Initialize and Drive the Tweeter DM}{21}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}\emph {class} Output}{21}{subsection.8.1.1}
\contentsline {subsubsection}{Methods}{21}{subsubsection*.45}
\contentsline {subsubsection}{Data}{22}{subsubsection*.51}
\contentsline {section}{\numberline {8.2}EDT - \texttt {edt} Camera driver module}{22}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}\emph {class} Camera}{22}{subsection.8.2.1}
\contentsline {subsubsection}{Methods}{22}{subsubsection*.56}
\contentsline {paragraph}{Exposures \& Handler}{22}{paragraph*.57}
\contentsline {paragraph}{Camera Commands \& Messages}{23}{paragraph*.61}
\contentsline {subsubsection}{Members}{23}{subsubsection*.66}
\contentsline {subsection}{\numberline {8.2.2}\emph {class} Handler}{24}{subsection.8.2.2}
\contentsline {subsubsection}{Methods}{24}{subsubsection*.76}
\contentsline {subsubsection}{Members}{24}{subsubsection*.78}
\contentsline {subsection}{\numberline {8.2.3}Processing Thread}{24}{subsection.8.2.3}
\contentsline {chapter}{\numberline {9}Indices and tables}{25}{chapter.9}
\contentsline {chapter}{Python Module Index}{27}{section*.82}
