using System;
using System.Collections.Generic;
using System.Text;

namespace Sidecar {
  class SidecarRamp {
    private int resets;

    public int Resets {
      get { return resets; }
      set { resets = value; }
    }

    private int reads;

    public int Reads {
      get { return reads; }
      set { reads = value; }
    }

    private int drops;

    public int Drops {
      get { return drops; }
      set { drops = value; }
    }
    private int groups;

    public int Groups {
      get { return groups; }
      set { groups = value; }
    }
    private int coadds;

    public int Coadds
    {
        get { return coadds; }
        set { coadds = value; }
    }
    public SidecarRamp Clone()
    {
      //. could use this.MemberwiseClone(), but this is already coded, so... 
      return new SidecarRamp(this);
    }
    public SidecarRamp(SidecarRamp ramp) :
      this(ramp.Resets, ramp.Reads, ramp.Drops, ramp.Groups, ramp.Coadds) {
    }
    public SidecarRamp()
      : this(1, 1, 0, 1, 1) { }
    public SidecarRamp(int resets, int reads, int drops, int groups, int coadds) {
      Resets = resets;
      Reads = reads;
      Drops = drops;
      Groups = groups;
      Coadds = coadds;
    }

  }
}
