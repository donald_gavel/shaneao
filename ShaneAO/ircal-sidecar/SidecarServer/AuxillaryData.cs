using System;
using System.Collections.Generic;
using System.Text;
using nom.tam.fits;
using nom.tam.util;
using nom.tam.image;
using log4net;

namespace Sidecar {
  class AuxillaryData {
    private static readonly ILog logger = LogManager.GetLogger(typeof(AuxillaryData));

    public void createDefaultBadPixelMap(int xs, int ys, byte data) {
      int ii,jj;
      badPixelMap = new byte[xs, ys];
      for (ii = 0; ii < xs; ii++) {
        for (jj = 0; jj < ys; jj++) {
          badPixelMap[ii, jj] = data;
        }
      }
    }

    public void createDefaultReadVariance(int xs, int ys, float data) {
      int ii, jj;
      readVariance = new float[xs, ys];
      for (ii = 0; ii < xs; ii++) {
        for (jj = 0; jj < ys; jj++) {
          readVariance[ii, jj] = data;
        }
      }
    }

    public void createDefaultLinearization(int xs, int ys, int degree) {
      int ii, jj;
      linearization = new float[xs, ys, (degree + 1)];
      for (ii = 0; ii < xs; ii++) {
        for (jj = 0; jj < ys; jj++) {
          //. note: default value of arrays is 0.0f
          linearization[ii, jj, 1] = 1.0F;
        }
      }
    }

    private byte[,] badPixelMap;

    public byte[,] BadPixelMap {
      get { return badPixelMap; }
      set { badPixelMap = value; }
    }

    private float[,] readVariance;

    public float[,] ReadVariance {
      get { return readVariance; }
      set { readVariance = value; }
    }

    private float[,,] linearization;

    public float[,,] Linearization {
      get { return linearization; }
      set { linearization = value; }
    }


    public void initBadPixelMap(string badPixelMapFitsFile) {
      Fits f = new Fits(badPixelMapFitsFile);
      ImageHDU h = (ImageHDU)f.ReadHDU();
      if (h == null) {
        f.Close();
        throw new FitsException("Null badpix fits file.");
      }
      Array[] rowArray = (Array[])(h.Kernel);
      int[] axes = h.Axes;
      int bitpix = h.BitPix;

      //. don't close until we everything we need from the HDU.
      //. for some reason, closing the fits file before causes
      //. the Kernel to be null.
      f.Close();

      if (axes.Length != 2) {
        throw new FitsException("Invalid bad pixel map.  Not two dimensional.");
      }

      //. note: CSharpFits inverses dimensions, such that axes[0] is actually FITS NAXIS2, etc.  
      //. this somewhat follows the C++ convention, but to me is confusing and unnecessary
      logger.Debug("badpix: bitpix = " + bitpix + ", xsize = " + axes[1] + ", ysize = " + axes[0] + ", h.kernel type = " + rowArray[0].GetType() + ".");
      badPixelMap = new byte[axes[1],axes[0]];

      if (bitpix == 16) {
        //. h.Kernel is an Array of 2048 short arrays (rows) of 2048 elements (columns)
        short[] row;
        for (int jj = 0; jj < axes[0]; jj++) {
          row = (short[])rowArray[jj];
          for (int ii = 0; ii < axes[1]; ii++) {
            badPixelMap[ii, jj] = (byte)row[ii];
          }
        }
      } else if (bitpix == 8) {
        //. h.Kernel is an Array of 2048 byte arrays (rows) of 2048 elements (columns)
        byte[] row;
        for (int jj = 0; jj < axes[0]; jj++) {
          row = (byte[])rowArray[jj];
          for (int ii = 0; ii < axes[1]; ii++) {
            badPixelMap[ii, jj] = row[ii];
          }
        }
      } else {
        throw new FitsException("Invalid Bad Pixel Map.  Must be 8 or 16 bit.");
      }
    }

    public void initReadVariance(string readVarianceFitsFile) {
      Fits f = new Fits(readVarianceFitsFile);
      ImageHDU h = (ImageHDU)f.ReadHDU();
      if (h == null) {
        f.Close();
        throw new FitsException("Null noise fits file.");
      }
      Array[] rowArray = (Array[])h.Kernel;
      int[] axes = h.Axes;
      int bitpix = h.BitPix;

      //. don't close until we everything we need from the HDU.
      //. for some reason, closing the fits file before causes
      //. the Kernel to be null.
      f.Close();

      if (axes.Length != 2) {
        throw new FitsException("Invalid noise file.  Not two dimensional.");
      }

      //. note: CSharpFits inverses dimensions, such that axes[0] is actually FITS NAXIS2, etc.  
      //. this somewhat follows the C++ convention, but to me is confusing and unnecessary
      logger.Debug("variance: bitpix = " + bitpix + ", xsize = " + axes[1] + ", ysize = " + axes[0] + ", h.kernel type = " + rowArray[0].GetType() + ".");
      readVariance = new float[axes[1], axes[0]];

      //. h.Kernel is an Array of 2048 float arrays (rows) of 2048 elements (columns)
      float[] row;
      for (int jj = 0; jj < axes[0]; jj++) {
        row = (float[])rowArray[jj];
        for (int ii = 0; ii < axes[1]; ii++) {
          readVariance[ii, jj] = row[ii];
        }
      }
    }

    public void initLinearization(string linearizationFitsFile) {
      //. fits constructor may throw other exceptions
      Fits f = new Fits(linearizationFitsFile);
      ImageHDU h = (ImageHDU)f.ReadHDU();
      if (h == null) {
        f.Close();
        throw new FitsException("Null linearization fits file.");
      }
      Array[] arr = ((Array[])h.Kernel);
      int[] axes = h.Axes;
      int bitpix = h.BitPix;

      //. don't close until we everything we need from the HDU.
      //. for some reason, closing the fits file before causes
      //. the Kernel to be null.
      f.Close();

      if (axes.Length != 3) {
        throw new FitsException("Invalid linearization file.  Not three dimensional.");
      }
      //. note: CSharpFits inverses dimensions, such that axes[0] is actually FITS NAXIS3, etc.  
      //. this somewhat follows the C++ convention, but to me is confusing and unnecessary
      //. we use index1 for x, index2 for y and index3 for the coefficients.
      //. in SidecarControl.reformAuxillaryData, the data is transformed to a 2D array, 
      //. with the first index having all the pixels, and the second
      //. index being the coefficients for each pixel.
      logger.Debug("linearization: bitpix = " + bitpix + ", xsize = " + axes[2] + ", ysize = " + axes[1] + ", ncoeffs = " + axes[0] + ", h.kernel type = " + arr[0].GetType() + ".");
      linearization = new float[axes[2], axes[1], axes[0]];

      //. h.Kernel is an Array of 3 float arrays (coeffs) of 2048 float arrays (rows) of 2048 elements (columns)
      Array[] coeffArr;
      float[] row;
      for (int kk = 0; kk < axes[0]; kk++) {
        coeffArr = (Array[])(arr[kk]);
        for (int jj = 0; jj < axes[1]; jj++) {
          row = (float[])(coeffArr[jj]);
          for (int ii = 0; ii < axes[2]; ii++) {
            linearization[ii, jj, kk] = row[ii];
          }
        }
      }
    }
  }
}
