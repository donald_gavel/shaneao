using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using nom.tam.fits;
using nom.tam.util;
using nom.tam.image;
using Sidecar.SidecarIce;
using log4net;

namespace Sidecar {
  class FitsWriter {
    private static readonly ILog logger = LogManager.GetLogger(typeof(FitsWriter));
    Fits myFits;
    BasicHDU scienceHDU;
    BasicHDU noiseHDU;
    BasicHDU qualityHDU;
    BufferedDataStream bds;
    SidecarControlConfiguration controlConfig;
    List<List<SidecarIceFitsHeaderKey>> additionalHeaders;
    public bool fitsHeaderAdded { get; set;}
    public string currentFitsFilename { get; set;}

    public FitsWriter() {
      FitsFactory.UseContinue = true;
      fitsHeaderAdded = false;
    }

    public SidecarControlConfiguration ControlConfiguration {
      set { controlConfig = value; }
      get { return controlConfig; }
    }
    SidecarConfiguration sidecarConfig;
    public SidecarConfiguration SidecarConfiguration {
      set { sidecarConfig = value; }
      get { return sidecarConfig; }
    }
    public void initFits(FitsInfo fInfo) {
      bds = new BufferedDataStream(new FileStream(fInfo.Filename+controlConfig.fitsWritingInProgressExtension, FileMode.Create));
      currentFitsFilename = fInfo.Filename;

      //. memory leak?  clear internal lists? no, shouldn't be in C#
      //. old additionalHeaders reference and it's contents will be GC'd
      additionalHeaders = new List<List<SidecarIceFitsHeaderKey>>();
    }
    public void addFitsHeader(List<SidecarIceFitsHeaderKey> header) {
      additionalHeaders.Add(header);
    }
    public void setScienceData(float[] data, int rows, int cols) {
      setScienceData(data, rows, cols, 0);
    }
    public void setScienceData(float[] data, int rows, int cols, int rotations) {
      //. rotations = number of 90-degree clockwise rotations
      //. 
      //. normally, we need an array of rows
      //. dataCol[ii] = data[ii + rows * currentRow]
      //. rows[ii] = dataCol

      //. todo: this way assembles rows of output one at at time.
      //. this could be slower than pushing through the input
      //. data in the fastest way and assembling all row arrays \
      //. at one time.
      //. 

      Array[] dataRows = new Array[rows];
      if (((rotations % 4) == 1) || ((rotations % 4) == -3)) {
        //. 0,0 -> 0,2047
        //. 0,2047 -> 2047,2047
        //. 2047,2047 -> 2047, 0
        //. 2047,0 -> 0,0
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[jj] = data[ii + cols * jj];
          }
          dataRows[cols - ii - 1] = dataCol;
        }
      } else if (((rotations % 4) == 3) || ((rotations % 4) == -1)) {
        //. 0,0 -> 2047,0
        //. 0,2047 -> 0,0
        //. 2047,2047 -> 0,2047
        //. 2047,0 -> 2047,2047
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[rows - jj - 1] = data[ii + cols * jj];
          }
          dataRows[ii] = dataCol;
        }
      } else if (Math.Abs(rotations % 4) == 2) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[cols - jj - 1] = data[jj + cols * ii];
          }
          dataRows[rows - ii - 1] = dataCol;
        }
      } else if ((rotations % 4) == 0) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[jj] = data[jj + cols * ii];
          }
          dataRows[ii] = dataCol;
        }
      }
      scienceHDU = FitsFactory.HDUFactory(dataRows);
    }
    public void setNoiseData(float[] data, int rows, int cols) {
      setNoiseData(data, rows, cols, 0);
    }
    public void setNoiseData(float[] data, int rows, int cols, int rotations) {
      Array[] dataRows = new Array[rows];
      if (((rotations % 4) == 1) || ((rotations % 4) == -3)) {
        //. 0,0 -> 0,2047
        //. 0,2047 -> 2047,2047
        //. 2047,2047 -> 2047, 0
        //. 2047,0 -> 0,0
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[jj] = data[ii + cols * jj];
          }
          dataRows[cols - ii - 1] = dataCol;
        }
      } else if (((rotations % 4) == 3) || ((rotations % 4) == -1)) {
        //. 0,0 -> 2047,0
        //. 0,2047 -> 0,0
        //. 2047,2047 -> 0,2047
        //. 2047,0 -> 2047,2047
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[rows - jj - 1] = data[ii + cols * jj];
          }
          dataRows[ii] = dataCol;
        }
      } else if (Math.Abs(rotations % 4) == 2) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[cols - jj - 1] = data[jj + cols * ii];
          }
          dataRows[rows - ii - 1] = dataCol;
        }
      } else if ((rotations % 4) == 0) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[jj] = data[jj + cols * ii];
          }
          dataRows[ii] = dataCol;
        }
      }
      noiseHDU = FitsFactory.HDUFactory(dataRows);
    }
    public void setQualityData(byte[] data, int rows, int cols) {
      setQualityData(data, rows, cols, 0);
    }
    public void setQualityData(byte[] data, int rows, int cols, int rotations) {
      Array[] dataRows = new Array[rows];
      if (((rotations % 4) == 1) || ((rotations % 4) == -3)) {
        //. 0,0 -> 0,2047
        //. 0,2047 -> 2047,2047
        //. 2047,2047 -> 2047, 0
        //. 2047,0 -> 0,0
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          byte[] dataCol = new byte[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[jj] = data[ii + cols * jj];
          }
          dataRows[cols - ii - 1] = dataCol;
        }
      } else if (((rotations % 4) == 3) || ((rotations % 4) == -1)) {
        //. 0,0 -> 2047,0
        //. 0,2047 -> 0,0
        //. 2047,2047 -> 0,2047
        //. 2047,0 -> 2047,2047
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          byte[] dataCol = new byte[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[rows - jj - 1] = data[ii + cols * jj];
          }
          dataRows[ii] = dataCol;
        }
      } else if (Math.Abs(rotations % 4) == 2) {
        for (int ii = 0; ii < rows; ii++) {
          byte[] dataCol = new byte[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[cols - jj - 1] = data[jj + cols * ii];
          }
          dataRows[rows - ii - 1] = dataCol;
        }
      } else if ((rotations % 4) == 0) {
        for (int ii = 0; ii < rows; ii++) {
          byte[] dataCol = new byte[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[jj] = data[jj + cols * ii];
          }
          dataRows[ii] = dataCol;
        }
      }
      qualityHDU = FitsFactory.HDUFactory(dataRows);
    }

    public void writeFits() {
      writeFits(null, null);
    }
    public void writeFits(ExposureConfiguration expConfig, SidecarRamp ramp) {
      bool doNoise = true;
      bool doQuality = true;

      if (noiseHDU == null) {
        doNoise = false;
      }
      if (qualityHDU == null) {
        doQuality = false;
      }
      writeFits(expConfig, ramp, doNoise, doQuality);
    }
    public void writeFits(ExposureConfiguration expConfig, SidecarRamp ramp, bool writeNoise, bool writeQuality) {
      myFits = new Fits();
      assembleHeader(scienceHDU, expConfig, ramp);
      myFits.AddHDU(scienceHDU);
      if (writeNoise) {
        myFits.AddHDU(noiseHDU);
      }
      if (writeQuality) {
        myFits.AddHDU(qualityHDU);
      }

      //. dump header if enabled
      if (logger.IsInfoEnabled) {
        scienceHDU.Header.DumpHeader(Console.Out);
      }
      myFits.Write(bds);
    }
    public void assembleHeader(BasicHDU hdu, ExposureConfiguration expConfig, SidecarRamp ramp) {
      int voltagePrecision = 4;
      bool isSubarray = false;
      hdu.Header.AddCard(new HeaderCard("BUNIT", "ADU per coadd", "Units of the pixel value"));
      if (expConfig != null) {
        isSubarray = expConfig.Extents.IsSubarray;
        if (expConfig.ExposureStartTime.Length > 0) {
          hdu.Header.AddCard(new HeaderCard("DATE-OBS", expConfig.ExposureStartDate, "UT start date of exposure"));
          hdu.Header.AddCard(new HeaderCard("TIME-OBS", expConfig.ExposureStartTime, "UT start time of exposure"));
        }
        if (expConfig.ExposureEndTime.Length > 0) {
          hdu.Header.AddCard(new HeaderCard("TIME-END", expConfig.ExposureEndTime, "UT end time of exposure (after last read)"));
        }
        hdu.Header.AddCard(new HeaderCard("ITIME0", expConfig.ItimeInMicros, "Requested itime per coadd in microseconds"));
        hdu.Header.AddCard(new HeaderCard("COADDS0", expConfig.Coadds, "Requested number of coadditions"));
        hdu.Header.AddCard(new HeaderCard("SMPMODE0", expConfig.SampMode, "Requested samp mode (1:Sing,2:CDS,3:MCDS,4:UTR)"));
        hdu.Header.AddCard(new HeaderCard("READS0", expConfig.Reads, "Requested sampling mode reads"));
        hdu.Header.AddCard(new HeaderCard("STARTX", expConfig.Extents.StartX, "Start Column for subarray (0-based)"));
        hdu.Header.AddCard(new HeaderCard("ENDX", expConfig.Extents.EndX, "End Column for subarray (0-based)"));
        hdu.Header.AddCard(new HeaderCard("STARTY", expConfig.Extents.StartY, "Start Row for subarray (0-based)"));
        hdu.Header.AddCard(new HeaderCard("ENDY", expConfig.Extents.EndY, "End Row for subarray (0-based)"));
      }
      if (ramp != null) {
        if (sidecarConfig != null) {
          double trueItime = (ramp.Drops + ramp.Reads) * (ramp.Groups - 1) * sidecarConfig.ReadTimeInMicros / 1000000.0;
          hdu.Header.AddCard(new HeaderCard("TRUITIME", setPrecision(trueItime, 6, false), "True integration time in seconds (per coadd)"));
          hdu.Header.AddCard(new HeaderCard("READTIME", sidecarConfig.ReadTimeInMicros, "Read time for a frame in microseconds"));
        }
        hdu.Header.AddCard(new HeaderCard("RMPRESET", ramp.Resets, "Number of resets in ramp"));
        hdu.Header.AddCard(new HeaderCard("RMPREADS", ramp.Reads, "Number of reads in ramp"));
        hdu.Header.AddCard(new HeaderCard("RMPDROP", ramp.Drops, "Number of drop frames in ramp"));
        hdu.Header.AddCard(new HeaderCard("RMPGROUP", ramp.Groups, "Number of groups in ramp"));
      }
      if (expConfig != null) {
        hdu.Header.AddCard(new HeaderCard("RDITIME", expConfig.GroupItime, "Time between sampling groups in seconds"));
        hdu.Header.AddCard(new HeaderCard("ABORTED", expConfig.Aborted, "Exposure was stopped before completion"));
        hdu.Header.AddCard(new HeaderCard("COADDONE", expConfig.CoaddsDone, "Number of coadds actually completed"));
        hdu.Header.AddCard(new HeaderCard("READDONE", expConfig.ReadsDone, "Reads actually completed in this coadd"));
        hdu.Header.AddCard(new HeaderCard("GRPSDONE", expConfig.GroupsDone, "Groups actually completed in this coadd"));
      }

      if (controlConfig != null) {
        hdu.Header.AddCard(new HeaderCard("IGNORDRD", controlConfig.IgnoredInitialReads, "Number of discarded initial reads"));
        hdu.Header.AddCard(new HeaderCard("VRPSTYPE", (isSubarray ? "none (subarray)" : controlConfig.getVerticalReferencePixelSubtractionType()), "Vertical reference pixel subtraction type"));
        hdu.Header.AddCard(new HeaderCard("HRPSTYPE", (isSubarray ? "none (subarray)" : controlConfig.getHorizontalReferencePixelSubtractionType()), "Horizontal reference pixel subtraction type"));
      }

      if (sidecarConfig != null) {
        hdu.Header.AddCard(new HeaderCard("SATURATE", sidecarConfig.SaturationLevel, "Detector saturation level (DN) from config file"));
        hdu.Header.AddCard(new HeaderCard("DTGAINNM", sidecarConfig.GainSetting, "Detector gain setting [0-15]"));
        hdu.Header.AddCard(new HeaderCard("DTGAINDB", SidecarConfiguration.detectorGainTypes[sidecarConfig.GainSetting], "Detector gain in dB"));
        hdu.Header.AddCard(new HeaderCard("DETGAIN", SidecarConfiguration.detectorGainFactors[sidecarConfig.GainSetting], "Detector gain factor"));
        hdu.Header.AddCard(new HeaderCard("SYSGAIN", SidecarConfiguration.systemGainFactors[sidecarConfig.GainSetting], "System gain in e-/ADU"));
        hdu.Header.AddCard(new HeaderCard("VRESET",   setPrecision(sidecarConfig.VReset, voltagePrecision, true),     "Reset voltage in V, obtained from ASIC"));
        hdu.Header.AddCard(new HeaderCard("DSUB",     setPrecision(sidecarConfig.DSub, voltagePrecision, true),       "Substrate voltage in V, obtained from ASIC"));
        hdu.Header.AddCard(new HeaderCard("VBIASGAT", setPrecision(sidecarConfig.VBiasGate, voltagePrecision, true),  "Gate voltage of SCA in V, obtained from ASIC"));
        hdu.Header.AddCard(new HeaderCard("VBIASPWR", setPrecision(sidecarConfig.VBiasPower, voltagePrecision, true), "Source voltage of SCA in V, obtained from ASIC"));
        hdu.Header.AddCard(new HeaderCard("CELLDRAN", setPrecision(sidecarConfig.CellDrain, voltagePrecision, true),  "Celldrain (pix src follower) in V (from ASIC)"));
        hdu.Header.AddCard(new HeaderCard("DRAIN",    setPrecision(sidecarConfig.Drain, voltagePrecision, true),      "Drain (output buffer) in V, obtained from ASIC"));
        hdu.Header.AddCard(new HeaderCard("VDDA",     setPrecision(sidecarConfig.VDDA, voltagePrecision, true),       "Analog SCA supply voltage in V (from ASIC)"));
        hdu.Header.AddCard(new HeaderCard("VDD",      setPrecision(sidecarConfig.VDD, voltagePrecision, true),        "Digital SCA power supply in V (from ASIC)"));
        hdu.Header.AddCard(new HeaderCard("VREFMAIN", setPrecision(sidecarConfig.VRefMain, voltagePrecision, true),   "Ref voltage from bias block in V (from ASIC)"));
        hdu.Header.AddCard(new HeaderCard("VPREMDRF", setPrecision(sidecarConfig.VPreMidRef, voltagePrecision, true), "Mid ADC ref volt from bias blk in V (from ASIC)"));
        hdu.Header.AddCard(new HeaderCard("VPRAMRF1", setPrecision(sidecarConfig.VPreAmpRef1, voltagePrecision, true), "Ref volt 1 for internal preamp DAC in V (ASIC)"));
        hdu.Header.AddCard(new HeaderCard("HXRGCFG1", sidecarConfig.SidecarConfigurationDumpLine1, "ASIC Regs 0x4010:0x4017"));
        hdu.Header.AddCard(new HeaderCard("HXRGCFG2", sidecarConfig.SidecarConfigurationDumpLine2, "ASIC Regs 0x4018:0x401F"));
        hdu.Header.AddCard(new HeaderCard("HXRGVERS", sidecarConfig.HXRG_code_version, "HxRG timing code version"));

      }

      foreach (List<SidecarIceFitsHeaderKey> header in additionalHeaders) {
        foreach (SidecarIceFitsHeaderKey key in header) {
          HeaderCard newCard;
          try {
            if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeBoolean) {
              Boolean val = (key.value.CompareTo("0") != 0);
              newCard = new HeaderCard(key.name, val, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeInteger) {
              Int32 val = Int32.Parse(key.value);
              newCard = new HeaderCard(key.name, val, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeLong) {
              Int64 val = Int64.Parse(key.value);
              newCard = new HeaderCard(key.name, val, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeFloat) {
              Single val = Single.Parse(key.value);
              newCard = new HeaderCard(key.name, val, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeDouble) {
              Double val = Double.Parse(key.value);
              newCard = new HeaderCard(key.name, val, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeString) {
              newCard = new HeaderCard(key.name, key.value, key.comment);
            } else if (key.type == SidecarIceFitsHeaderKeyType.SidecarIceKeyTypeComment) {
              hdu.Header.AddComment(key.comment);
              continue;
            } else {
              continue;
            }
            hdu.Header.AddCard(newCard);
          } catch (Exception ex) {
            Console.Write(ex.Message);
          }
        }
      }
    }
    public void closeFits() {
      closeFits(false);
    }

    public void closeFits(bool delete) {
      bds.Close();
      if (delete) {
        File.Delete(currentFitsFilename + controlConfig.FitsWritingInProgressExtension);
      } else {
        if (controlConfig.FitsWritingInProgressExtension.Length > 0) {
          if (File.Exists(currentFitsFilename)) {
            File.Delete(currentFitsFilename);
          }
          File.Move(currentFitsFilename + controlConfig.FitsWritingInProgressExtension, currentFitsFilename);
        }
      }
      currentFitsFilename = "";

    }
    private double setPrecision(double num, int digits, bool showZeros) {
      StringBuilder b = new StringBuilder("0.");
      String tag = (showZeros ? "0" : "#");
      for (int ii = 0; ii < digits; ii++) {
        b.Append(tag);
      }
      return double.Parse(num.ToString(b.ToString()));
    }
  }
}
