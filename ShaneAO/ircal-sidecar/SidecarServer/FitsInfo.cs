using System;
using System.Collections.Generic;
using System.Text;

namespace Sidecar {
  class FitsInfo {
    private int rows;

    public int Rows {
      get { return rows; }
      set { rows = value; }
    }

    private int columns;

    public int Columns {
      get { return columns; }
      set { columns = value; }
    }

    private string filename;

    public string Filename {
      get { return filename; }
      set { filename = value; }
    }

    public FitsInfo(string filename, int rows, int columns) {
      Filename = filename;
      Rows = rows;
      Columns = columns;
    }
  }
}
