﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using nom.tam.fits;
using nom.tam.util;
using nom.tam.image;
using Sidecar.SidecarIce;
using log4net;
using System.Runtime;

namespace Sidecar {
  class RawFrameFitsWriter {
    private static readonly ILog logger = LogManager.GetLogger(typeof(RawFrameFitsWriter));
    FitsInfo fInfo;
    ushort[] data;
    float[] fdata;
    bool isFloat;
    Fits rawFits;
    RawFrameFitsWriterQueue q;
    FitsWriter fitsWriter;
    ExposureConfiguration expConfig;
    SidecarRamp sidecarRamp;
    int imageRotation = 0;
    string writingExt = "";
    public RawFrameFitsWriter(FitsInfo rawFitsInfo, ushort[] rawData, string writingExtension) {
      fInfo = rawFitsInfo;
      writingExt = writingExtension;
      try {
        data = new ushort[rawData.Length];
        isFloat = false;
        rawFits = new Fits();
        //. make a copy of the data so the member variable can be reused
        Array.Copy(rawData, data, rawData.Length);
      } catch (OutOfMemoryException) {
        throw new InsufficientMemoryException("Out of memory.");
      }
    }
    public RawFrameFitsWriter(FitsInfo rawFitsInfo, float[] rawData, FitsWriter writer, ExposureConfiguration config, SidecarRamp ramp, int rotation) {
      fInfo = rawFitsInfo;
      writingExt = writer.ControlConfiguration.FitsWritingInProgressExtension;
      try {
        fdata = new float[rawData.Length];
        isFloat = true;
        rawFits = new Fits();
        fitsWriter = writer;
        expConfig = config;
        sidecarRamp = ramp;
        imageRotation = rotation;
        //. make a copy of the data so the member variable can be reused
        Array.Copy(rawData, fdata, rawData.Length);
      } catch (OutOfMemoryException) {
        throw new InsufficientMemoryException("Out of memory.");
      }
    }
    public string getFilename() {
      return fInfo.Filename;
    }
    public void prepareData() {
      logger.Debug("Preparing data for " + fInfo.Filename);
      int rows = fInfo.Rows;
      int cols = fInfo.Columns;
      Array[] dataRows = new Array[rows];
      for (int ii = 0; ii < rows; ii++) {
        ushort[] dataCol = new ushort[cols];
        for (int jj = 0; jj < cols; jj++) {
          dataCol[jj] = (data[jj + cols * ii]);
        }
        dataRows[ii] = dataCol;
      }
      data = null;
      BasicHDU rawHDU = FitsFactory.HDUFactory(dataRows);
      logger.Debug("Adding HDU to " + fInfo.Filename);
      rawFits.AddHDU(rawHDU);
      logger.Debug("Data prepared for " + fInfo.Filename);

    }
    public void prepareFloatData() {
      logger.Debug("Preparing data for " + fInfo.Filename);
      int rows = fInfo.Rows;
      int cols = fInfo.Columns;
      Array[] dataRows = new Array[rows];
      if (((imageRotation % 4) == 1) || ((imageRotation % 4) == -3)) {
        //. 0,0 -> 0,2047
        //. 0,2047 -> 2047,2047
        //. 2047,2047 -> 2047, 0
        //. 2047,0 -> 0,0
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[jj] = fdata[ii + cols * jj];
          }
          dataRows[cols - ii - 1] = dataCol;
        }
      } else if (((imageRotation % 4) == 3) || ((imageRotation % 4) == -1)) {
        //. 0,0 -> 2047,0
        //. 0,2047 -> 0,0
        //. 2047,2047 -> 0,2047
        //. 2047,0 -> 2047,2047
        dataRows = new Array[cols];
        for (int ii = 0; ii < cols; ii++) {
          float[] dataCol = new float[rows];
          for (int jj = 0; jj < rows; jj++) {
            dataCol[rows - jj - 1] = fdata[ii + cols * jj];
          }
          dataRows[ii] = dataCol;
        }
      } else if (Math.Abs(imageRotation % 4) == 2) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[cols - jj - 1] = fdata[jj + cols * ii];
          }
          dataRows[rows - ii - 1] = dataCol;
        }
      } else if ((imageRotation % 4) == 0) {
        for (int ii = 0; ii < rows; ii++) {
          float[] dataCol = new float[cols];
          for (int jj = 0; jj < cols; jj++) {
            dataCol[jj] = fdata[jj + cols * ii];
          }
          dataRows[ii] = dataCol;
        }
      }
      fdata = null;
      BasicHDU rawHDU = FitsFactory.HDUFactory(dataRows);
      logger.Debug("Getting fits header");
      fitsWriter.assembleHeader(rawHDU, expConfig, sidecarRamp);
      logger.Debug("Adding HDU to " + fInfo.Filename);
      rawFits.AddHDU(rawHDU);
      logger.Debug("Data prepared for " + fInfo.Filename);

    }
    public void writeRaw() {
      lock (this) {
        logger.Debug("Writing raw frame to " + fInfo.Filename);
        BufferedDataStream rawBds = new BufferedDataStream(new FileStream(fInfo.Filename+writingExt, FileMode.Create));

        rawFits.Write(rawBds);
        rawBds.Close();
        if (writingExt.Length > 0) {
          if (File.Exists(fInfo.Filename)) {
            File.Delete(fInfo.Filename);
          }
          File.Move(fInfo.Filename + writingExt, fInfo.Filename);
        }
        data = null;
        rawBds = null;
        rawFits = null;
        System.Diagnostics.PerformanceCounter ramCounter = new System.Diagnostics.PerformanceCounter("Memory", "Available MBytes");
        logger.Debug("Ram available for " + fInfo.Filename + " before GC: " + ramCounter.NextValue() + "MB");
        System.GC.Collect();
        logger.Debug("Ram available for " + fInfo.Filename + " after GC: " + ramCounter.NextValue() + "MB");
        logger.Info("############# RAW DATA FITS WRITING COMPLETE : " + fInfo.Filename + " ############");
      }
    }
    public void ThreadPoolCallback(Object threadContext) {
      if (isFloat) {
        prepareFloatData();
      } else {
        prepareData();
      }
      if (q != null) {
        q.notifyDone(this);
      }
    }
    public void setQueue(RawFrameFitsWriterQueue queue) {
      q = queue;
    }
  }
}
