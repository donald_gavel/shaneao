using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Sidecar {
  class InvalidExposureConfigurationException : System.Exception, ISerializable {
    public InvalidExposureConfigurationException() : base() { }
    public InvalidExposureConfigurationException(string message) : base(message) { }
    public InvalidExposureConfigurationException(string message, System.Exception inner) : base(message, inner) { }

    // Constructor needed for serialization 
    // when exception propagates from a remoting server to the client.
    protected InvalidExposureConfigurationException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
      : base(info, context) { }

  }
}
