﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Threading;
using log4net;

namespace Sidecar {
  class RawFrameFitsWriterQueue {
    private static readonly ILog logger = LogManager.GetLogger(typeof(RawFrameFitsWriterQueue));

    class RawFrameFitsWriterWorker {
      private volatile bool runWriteFitsThread = false;
      private volatile Queue<RawFrameFitsWriter> q = new Queue<RawFrameFitsWriter>();
      int writeFitsThreadQueuePollPeriod = 10;  //. millis

      public void enqueue(RawFrameFitsWriter w) {
        q.Enqueue(w);
      }

      public void stop() {
        runWriteFitsThread = false;
      }

      public void writeFits() {
        runWriteFitsThread = true;
        logger.Debug("In start of raw fits queue thread");
        while (runWriteFitsThread) {
          while (q.Count != 0) {
            RawFrameFitsWriter w = q.Dequeue();
            try {
              w.writeRaw();
            } catch (Exception e) {
              logger.Error("Error writing raw frame: " + w.getFilename() + ": " + e.Message);
            }
          }
          while (q.Count == 0) {
            Thread.Sleep(writeFitsThreadQueuePollPeriod);
            if (!runWriteFitsThread) {
              break;
            }
          }
        }
        logger.Info("Exiting raw fits queue thread");
      }
    }
    RawFrameFitsWriterWorker worker;
    Thread writeFitsThread;
    bool threadRunning;
    public RawFrameFitsWriterQueue() {
      worker = new RawFrameFitsWriterWorker();
      threadRunning = false;
    }
    public void enqueueFitsWriter(RawFrameFitsWriter w) {
      w.setQueue(this);
      ThreadPool.QueueUserWorkItem(w.ThreadPoolCallback);
    }
    public void notifyDone(RawFrameFitsWriter w) {
      worker.enqueue(w);
    }
    public void start() {
      if (!threadRunning) {
        logger.Info("Starting raw frame fits writer queue.");
        writeFitsThread = new Thread(worker.writeFits);
        writeFitsThread.Start();
        threadRunning = true;
      } else {
        logger.Info("Raw frame fits writer queue already running.");
      }
    }
    public void stop() {
      if (threadRunning) {
        logger.Debug("Requesting stop of raw frame fits writer queue."); ;
        worker.stop();
        threadRunning = false;
      } else {
        logger.Info("Raw frame fits writer queue already not running.");
      } 
      
    }

  }
}
