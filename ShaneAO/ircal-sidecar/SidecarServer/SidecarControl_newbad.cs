using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using log4net;
using Sidecar.SidecarIce;
using System.Runtime;



//. TODO: [X] needs work, if UTR has multiple reads per sample
//. TODO: [X] initMemory should be recalled when detector type changes
//. TODO: [X] frameTime in FITS header
//. TODO: [X] get voltages and configuration when starting an exposure or after dl timing file
//. TODO: [ ] read time a function of number of outputs
//. TODO: [ ] no ignored reads for a single exposure.
//. TODO: [ ] make sure raw and reduced frames are not written at same time (mutex?)


namespace Sidecar {
  class SidecarControl {
    SidecarASICComComponents.SidecarASIC sidecar;

    private const string ASIC_REGISTER_RAMP = "0x6900";
    private const string ASIC_REGISTER_RAMP_RESETS = "0x4000";
    private const string ASIC_REGISTER_RAMP_READS = "0x4001";
    private const string ASIC_REGISTER_RAMP_PULSE_LED = "0x4002";
    private const string ASIC_REGISTER_RAMP_4003 = "0x4003";
    private const string ASIC_REGISTER_ASIC_FREQUENCY = "0x4003";
    private const string ASIC_REGISTER_RAMP_COADDS = "0x4003";
    private const string ASIC_REGISTER_RAMP_GROUPS = "0x4004";
    private const string ASIC_REGISTER_RAMP_DROPS = "0x4005";
    private const string ASIC_REGISTER_LINE_CLOCK_TIME = "0x4008";
    private const string ASIC_REGISTER_LINES_PER_FRAME = "0x4009";
    private const string ASIC_REGISTER_DETECTOR_TYPE = "0x4010";
    private const string ASIC_REGISTER_DETECTOR_OUTPUTS = "0x4011";
    private const string ASIC_REGISTER_ADCS_AVERAGED = "0x4012";
    private const string ASIC_REGISTER_DISABLE_HXRG = "0x4013";
    private const string ASIC_REGISTER_DISABLE_CLOCKS = "0x4014";
    private const string ASIC_REGISTER_OPERATING_TEMPERATURE = "0x4015";
    private const string ASIC_REGISTER_PREAMP_MODE = "0x4016";
    private const string ASIC_REGISTER_HXRG_CLOCKING = "0x4017";
    private const string ASIC_REGISTER_EXPOSURE_MODE = "0x4018";
    private const string ASIC_REGISTER_PREAMP_INPUT_MODE = "0x4019";
    private const string ASIC_REGISTER_PREAMP_GAIN = "0x401A";
    private const string ASIC_REGISTER_PREAMP_BUFFERED = "0x401B";
    private const string ASIC_REGISTER_DATA_TRANSMISSION_MODE = "0x401C";
    private const string ASIC_REGISTER_SERIAL_CHANNELS = "0x401D";
    private const string ASIC_REGISTER_HEADER_LENGTH = "0x401E";
    private const string ASIC_REGISTER_DATA_OUTPUT_LEVEL = "0x401F";
    private const string ASIC_REGISTER_WINDOW_STARTX = "0x4020";
    private const string ASIC_REGISTER_WINDOW_ENDX = "0x4021";
    private const string ASIC_REGISTER_WINDOW_STARTY = "0x4022";
    private const string ASIC_REGISTER_WINDOW_ENDY = "0x4023";
    private const string ASIC_REGISTER_INPUT_REF_OVERRIDE = "0x5100";
    private const string ASIC_REGISTER_VRESET = "0x6000";
    private const string ASIC_REGISTER_DSUB = "0x6002";
    private const string ASIC_REGISTER_VBIASGATE = "0x6004";
    private const string ASIC_REGISTER_VBIASPOWER = "0x6006";
    private const string ASIC_REGISTER_CELLDRAIN = "0x6008";
    private const string ASIC_REGISTER_DRAIN = "0x600A";
    private const string ASIC_REGISTER_VDDA = "0x600C";
    private const string ASIC_REGISTER_VDD = "0x600E";
    private const string ASIC_REGISTER_VREFMAIN = "0x602C";
    private const string ASIC_REGISTER_VPREMIDREF = "0x6034";
    private const string ASIC_REGISTER_VPREAMPREF1 = "0x6028";
    private const string ASIC_REGISTER_ADC_CLOCK = "0x6905";
    private const string ASIC_REGISTER_HXRG_CODE_VERSION = "0x5FFC";

    private const string ASIC_REGISTER_VALUE_STOP_RAMP = "0x8000";
    private const string ASIC_REGISTER_VALUE_START_RAMP = "0x8001";
    private const string ASIC_REGISTER_VALUE_RECONFIGURE = "0x8002";
    private const string ASIC_REGISTER_VALUE_PULSE_LED_TRUE = "0x0002";
    private const string ASIC_REGISTER_VALUE_PULSE_LED_FALSE = "0x0000";
    private const string ASIC_REGISTER_VALUE_SHORTED_PREAMPS = "0xAAAA";


    private const string JADE_REGISTER_DATA_ON_ACK = "0x0008";
    private const string JADE_REGISTER_JADE_FREQUENCY = "0x000C";
    private const string JADE_REGISTER_FIFO_PREP = "0x000D";
    private const string JADE_REGISTER_DATA_AVAILABLE = "0x000E";
    private const string JADE_REGISTER_GET_AVAILABLE_PIXEL_COUNT = "0x000F";
    private const string JADE_REGISTER_RESET_ASIC = "0x0015";
    private const string JADE_REGISTER_ASIC_POWER_BIAS = "0x0020";
    private const string JADE_REGISTER_ASIC_POWER_5V = "0x0021";

    /*  writing to Jade registers returns  "Success!" when successful. 
     *  writing to Asic registers returns 
     *  "Write to asic register @<XXXX> succuessful." when successful
     *  where <XXXX> is the address in hex (w/o the preceding 0x) 
     */
    private const string SIDECAR_SUCCESS_OPEN_APPLICATION = "Application opened.";
    private const string SIDECAR_SUCCESS_DOWNLOAD_FIRMWARE = "Download of jade firmware succeeded";
    private const string SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS = "Download of jade registers complete";
    private const string SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE = "Download MCD Succeeded";
    private const string SIDECAR_SUCCESS_WRITE_ASIC_REGISTER = "successful";
    private const string SIDECAR_SUCCESS_WRITE_JADE_REGISTER = "Success!";
    private const string SIDECAR_SUCCESS_READ_SCIENCE_DATA = "Success!";

    private const int WAITTIME_MS_AFTER_DATA_ON_ACK = 1000;
    private const int WAITTIME_MS_READ_IS_DATA_AVAILABLE = 10;
    private const int WAITTIME_MS_RECONFIGURE = 100;
    private const int WAITTIME_MS_ABORT_RAMP = 100;
     
    private const int RECONFIGURE_TIMEOUT_IN_SECONDS = 2000;  /* gets divided by number of inputs - 32-channel -> 62.5 s */
    private const int MAX_REPEATED_PIXEL_COUNT_READS = 20;
    private const int MAX_REPEATED_ABORT_RAMP_ATTEMPTS = 50;
    private const int COADD_OVERHEAD_MICROS = 0;
    private const int PROCESSING_OVERHEAD_MICROS = 500000;

    //. TODO: improve, with masks for specific types of bad pixels
    private const byte QUAL_BADPIX_MASK =               0x01;    /* 00000001 */
    private const byte QUAL_RAW_SATURATED_MASK =        0x02;    /* 00000010 */
    private const byte QUAL_DIFFERENCE_SATURATED_MASK = 0x04;    /* 00000100 */
    private const byte QUAL_MAX_DELTA_REMOVED_MASK =    0x08;    /* 00001000 */
    private const byte QUAL_MIN_DELTA_REMOVED_MASK =    0x10;    /* 00010000 */
    private const byte NOT_QUAL_BADPIX_MASK =               0xFE;    /* 11111110 */
    private const byte NOT_QUAL_RAW_SATURATED_MASK =            0xFD;    /* 11111101 */
    private const byte NOT_QUAL_DIFFERENCE_SATURATED_MASK = 0xFB;    /* 11111011 */
    private const byte NOT_QUAL_MAX_DELTA_REMOVED_MASK =    0xF7;    /* 11110111 */
    private const byte NOT_QUAL_MIN_DELTA_REMOVED_MASK =    0xEF;    /* 11101111 */
    private const byte DEFAULT_BADPIX_VALUE = 1;

    private const float DEFAULT_READVARIANCE_VALUE = 0.0f;
    private const int DEFAULT_LINEARIZATION_DEGREE = 1;   /* only used for generating default linearization array which does no correction */

    private const int UTR_MINIMUM_VALID_DELTA = -1000;
    private const int UTR_MAXIMUM_VALID_DELTA = 1000;

    private string oldBadPixelMapFile = "";
    private string oldReadVarianceFile = "";
    private string oldLinearizationFile = "";
    private DateTime oldBadPixelMapModDate;
    private DateTime oldReadVarianceModDate;
    private DateTime oldLinearizationModDate;
    private DateTime INVALID_FILE_DATE_TIME = new DateTime(0);
    private bool linearize = true;

    private const string VALUE_NAME_PROGRESS = "progress";
    private const string VALUE_NAME_READS_DONE = "readsDone";
    private const string VALUE_NAME_GROUPS_DONE = "groupsDone";
    private const string VALUE_NAME_COADDS_DONE = "coaddsDone";
    private const string VALUE_NAME_IGNORED_READS = "ignoredReads";
    private const string VALUE_NAME_CURRENT_READ = "currentRead";
    private const string VALUE_NAME_EXPOSURE_IN_PROGRESS = "exposureInProgress";
    private const string VALUE_NAME_OBSERVATION_IN_PROGRESS = "observationInProgress";
    private const string VALUE_NAME_ABORT_IN_PROGRESS = "abortInProgress";
    private const string VALUE_NAME_READOUT_IN_PROGRESS = "readoutInProgress";
    private const string VALUE_NAME_EXPOSING = "exposing";
    private const string VALUE_NAME_IMAGE_DONE = "imageDone";
    private const string VALUE_NAME_EXPOSURE_STATUS = "exposureStatus";
    private const string VALUE_NAME_EXPOSURE_STATUS_DETAIL = "exposureStatusDetail";
    private const string VALUE_NAME_INITIALIZE_SIDECAR = "initializeSidecar";
    private const string VALUE_NAME_LAST_FILENAME = "lastFilename";
    private const string VALUE_NAME_WRITING_DATA = "writingData";
    private const string VALUE_NAME_HEARTBEAT = "heartbeat";
    private const string VALUE_NAME_HEARTBEAT_PERIOD = "heartbeatPeriod";
    private const string VALUE_NAME_ESTIMATED_OBSERVATION_TIME = "observationTime";
    private const string VALUE_NAME_FITS_WRITING_EXTENSION = "fitsWritingExtension";

    //. todo get rid of the following two?
    private const string VALUE_NAME_VRESET = "vreset";
    private const string VALUE_NAME_DSUB = "dsub";

    private int arrayRows;
    private int arrayCols;
    private int exposureRows;
    private int exposureCols;
    private double pixelClockTime;
    bool applicationLoaded = false;

    private SidecarControlConfiguration config;
    private SidecarConfiguration sidecarConfig;
    private RawFrameFitsWriterQueue rawFrameWriterQ;
    private SidecarConfiguration sidecarConfigAtLastHardwareInit = null;
    private String sidecarApplicationAtLastHardwareInit = "";

    //. note: weight arrays are ushort, which limits reads to 256.
    //.   if we increase this limit, we must increase the storage size 
    //.   (also for weight local variable in coaddition)
    private ushort[] blockArray;    /* memory buffer for image block    */
    private ushort[] readImage;     /* full raw frame                   */
    private float[] referencePixelSubtractedImage; /* stores reference pixel subtracted raw image */
    private float[] firstread;     /* stores first read frame          */
    private float[] secondread;    /* stores second read frame         */
    private float[] utr_wdeltas;   /* stores succ read weighted deltas */
    private float[] utr_lastread;  /* stores last read                 */
    private float[] utr_maxdel;    /* stores maximum delta             */
    private float[] utr_mindel;    /* stores minimum delta             */
    private float[] utr_a;    /* stores y-intercept of fit             */
    private float[] utr_b;    /* stores slope of fit                */
    private ushort[] utr_weights;   /* stores weights                   */
    private ushort[] utr_maxw;      /* stores weight at maximum delta   */
    private ushort[] utr_minw;      /* stores weight at minimum delta   */
    private byte[] saturated;     /* array of saturated pixels        */
    private byte[] badpix;        /* bad pixel map                    */
    private float[] readvar;     /* read variance array               */
    private float[][] linearization;     /* linearization array               */
    private float[] dataimage;     /* final data array                 */
    private float[] errimage;      /* final noise array                */
    private byte[] qualimage;     /* final quality array              */

    private string jadeName = "";
    public string JadeName {
      get { return jadeName; }
      set { jadeName = value; }
    }

    private string asicName = "";
    public string AsicName {
      get { return asicName; }
      set { asicName = value; }
    }

    private int detectorSetupFlag;
    bool p1Exist = true;
    bool p2Exist = false;
    bool p3Exist = false;
    bool p4Exist = false;

    private string exposureStatus;
    public string ExposureStatus {
      get { return exposureStatus; }
      set { exposureStatus = value; }
    }

    private double exposurePercentComplete;
    public double ExposurePercentComplete {
      get { return exposurePercentComplete; }
      set { exposurePercentComplete = value; }
    }

    private int exposureStatusID;
    public int ExposureStatusID {
      get { return exposureStatusID; }
      set { exposureStatusID = value; }
    }

    private string exposureStatusDetail;
    public string ExposureStatusDetail {
      get { return exposureStatusDetail; }
      set { exposureStatusDetail = value; }
    }

    private ExposureExtents expExtents;
    private AuxillaryData auxData;

    private ValueListener valueListener;

    private volatile bool isExposing = false;
    private volatile int doAbort;
    private bool saveImageOnAbort = true;
    private volatile bool runHeartbeatThread = false;
    private uint heartbeatCounter = 0;
    private volatile ListDictionary openFitsWriters = new ListDictionary();

    private static readonly ILog logger = LogManager.GetLogger(typeof(SidecarControl));

    public SidecarControl(SidecarASICComComponents.SidecarASIC sidecar, ValueListener listener) {
      this.sidecar = sidecar;
      this.valueListener = listener;
      config = new SidecarControlConfiguration();
      sidecarConfig = new SidecarConfiguration();
      formatDetectorSetupFlag();
      initExposureStatus();
      auxData = new AuxillaryData();
      arrayRows = 2048;
      arrayCols = 2048;
      exposureRows = arrayRows;
      exposureCols = arrayCols;
      expExtents = new ExposureExtents(arrayRows, arrayCols);
      initMemory();
      auxData.createDefaultBadPixelMap(arrayCols, arrayRows, DEFAULT_BADPIX_VALUE);
      auxData.createDefaultReadVariance(arrayCols, arrayRows, DEFAULT_READVARIANCE_VALUE);
      rawFrameWriterQ = new RawFrameFitsWriterQueue();
      rawFrameWriterQ.start();
    }
    public int broadcastValue(string valueName) {
      if (valueName.CompareTo(VALUE_NAME_IGNORED_READS) == 0) {
        valueListener.setValue(VALUE_NAME_IGNORED_READS, ""+config.IgnoredInitialReads);
      } else if (valueName.CompareTo(VALUE_NAME_VRESET) == 0) {
        valueListener.setValue(VALUE_NAME_VRESET, ""+sidecarConfig.VReset);
      } else if (valueName.CompareTo(VALUE_NAME_DSUB) == 0) {
        valueListener.setValue(VALUE_NAME_DSUB, ""+sidecarConfig.DSub);
      } else if (valueName.CompareTo(VALUE_NAME_EXPOSURE_STATUS) == 0) {
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS, "" + ExposureStatus);
      } else if (valueName.CompareTo(VALUE_NAME_EXPOSURE_STATUS_DETAIL) == 0) {
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS_DETAIL, "" + ExposureStatusDetail);
      } else if (valueName.CompareTo(VALUE_NAME_HEARTBEAT_PERIOD) == 0) {
        valueListener.setValue(VALUE_NAME_HEARTBEAT_PERIOD, "" + config.HeartbeatPeriod);
      } else if (valueName.CompareTo(VALUE_NAME_EXPOSING) == 0) {
        valueListener.setValue(VALUE_NAME_EXPOSING, isExposing ? "1" : "0");
      } else if (valueName.CompareTo(VALUE_NAME_FITS_WRITING_EXTENSION) == 0) {
        valueListener.setValue(VALUE_NAME_FITS_WRITING_EXTENSION, config.FitsWritingInProgressExtension);
      } else if (valueName.ToUpperInvariant().CompareTo("ALL") == 0) {
        valueListener.setValue(VALUE_NAME_EXPOSING, isExposing ? "1" : "0");
        valueListener.setValue(VALUE_NAME_IGNORED_READS, "" + config.IgnoredInitialReads);
        valueListener.setValue(VALUE_NAME_VRESET, "" + sidecarConfig.VReset);
        valueListener.setValue(VALUE_NAME_DSUB, "" + sidecarConfig.DSub);
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS, "" + ExposureStatus);
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS_DETAIL, "" + ExposureStatusDetail);
        valueListener.setValue(VALUE_NAME_FITS_WRITING_EXTENSION, config.FitsWritingInProgressExtension);
      } else {
        throw new SidecarControlException("Value with name <" + valueName + "> cannot be broadcasted.");
      }
      return 0;
    }


    public int readSidecarControlConfigurationFile(string configFilename) {
      //. todo: check to make sure state ok if fails midway
      int oldArrayCols = arrayCols;
      int oldArrayRows = arrayRows;
      int returnStatus = 0;

      //. don't allow if currently exposing
      //. TODO check for other instances where we should check
      if (isExposing) {
        throw new SidecarControlException("Cannot load configuration file while exposure is in progress.");
      }
      //. read configFilename XML file
      XmlSerializer xmlSerializer;
      TextReader reader;
      SidecarControlConfiguration tempConfig;
      try {
        xmlSerializer = new XmlSerializer(typeof(SidecarControlConfiguration));
        reader = new StreamReader(configFilename);

        try {
          //. copy contents of configuration into a temporary SidecarControlConfiguration object until validated
          tempConfig = (SidecarControlConfiguration)xmlSerializer.Deserialize(reader);

          //. print info to screen for debugging purposes
          logger.Info("Sidecar Control Configuration File " + configFilename + " loaded.");
          logger.Info("Sidecar Application = " + tempConfig.SidecarApplication);
          logger.Info("Is Code Version HxRG = " + tempConfig.CodeVersionHxRG);
          logger.Info("Bad Pixel Map = " + tempConfig.BadPixelMapFilename);
          logger.Info("Read Variance File = " + tempConfig.ReadVarianceFilename);
          logger.Info("Linearization File = " + tempConfig.LinearizationFilename);
          logger.Info("Default RPS type = " + tempConfig.DefaultReferencePixelSubtractionType);
          logger.Info("RPS using median = " + tempConfig.ReferencePixelSubtractMedian);
          logger.Info("Write Raw Frames = " + tempConfig.WriteRawFrames);
          logger.Info("Write Raw Group Frames = " + tempConfig.WriteRawGroupFrames);
          logger.Info("Write Noise Extension = " + tempConfig.WriteNoiseExtension);
          logger.Info("Write Quality Extension = " + tempConfig.WriteQualityExtension);
          logger.Info("Ignore Initial Reads = " + tempConfig.IgnoredInitialReads);
          logger.Info("Detector rotation = " + tempConfig.DetectorRotation);
          logger.Info("Sidecar Configuration File = " + tempConfig.SidecarConfigurationFile);
          logger.Info("Heartbeat type = " + tempConfig.HeartbeatType);
          logger.Info("Heartbeat period = " + tempConfig.HeartbeatPeriod + " ms.");
          logger.Info("FITS writing extension = " + tempConfig.fitsWritingInProgressExtension);

          //. validate settings
          tempConfig.validate();

          //. broadcast number of ignored reads
          valueListener.setValue(VALUE_NAME_IGNORED_READS, "" + tempConfig.IgnoredInitialReads);
          valueListener.setValue(VALUE_NAME_FITS_WRITING_EXTENSION, tempConfig.fitsWritingInProgressExtension);

          //. if sidecar configuration specified, open and validate it.
          //. throws an exception if there is a problem.  exception will
          //. simply propagate up.
          if (tempConfig.SidecarConfigurationFile.Length > 0) {
            readSidecarConfigurationFile(tempConfig.sidecarConfigurationFile);
          }



          //. init memory (if sizes have changed)
          if ((arrayCols != oldArrayCols) || (arrayRows != oldArrayRows)) {
            initMemory();
          }

          //. try to open application
          if (tempConfig.SidecarApplication.Length > 0) {
            if ((sidecar.OpenApp(tempConfig.SidecarApplication)).CompareTo(SIDECAR_SUCCESS_OPEN_APPLICATION) != 0) {
              throw new SidecarControlException("Error opening application " + tempConfig.SidecarApplication);
            }
          }
          applicationLoaded = true;

          tempConfig.setReferencePixelSubtractionType(tempConfig.DefaultReferencePixelSubtractionType);

          this.config = tempConfig;

          if (!config.SidecarApplication.Equals(sidecarApplicationAtLastHardwareInit)) {
            returnStatus += 1;
          }

          if (!sidecarConfig.Equals(sidecarConfigAtLastHardwareInit)) {
            returnStatus += 2;
          }

          if (hasAuxillaryDataFilesChanged()) {
            //. read auxillary data
            try {
              initializeBadPixelMap();
            } catch (SidecarControlException scEx) {
              logger.Error(scEx.Message);
              auxData.createDefaultBadPixelMap(arrayCols, arrayRows, DEFAULT_BADPIX_VALUE);
              logger.Info("Creating a default map with all pixels having a value of " + DEFAULT_BADPIX_VALUE);
            }

            try {
              initializeReadVariance();
            } catch (SidecarControlException scEx) {
              logger.Error(scEx.Message);
              auxData.createDefaultReadVariance(arrayCols, arrayRows, DEFAULT_READVARIANCE_VALUE);
              logger.Info("Creating a default read variance map with all pixels having a value of " + DEFAULT_READVARIANCE_VALUE);
            }

            try {
              initializeLinearization();
              linearize = true;
            } catch (SidecarControlException scEx) {
              logger.Error(scEx.Message);
              auxData.createDefaultLinearization(arrayCols, arrayRows, DEFAULT_LINEARIZATION_DEGREE);
              logger.Info("Creating a default linearization array with degree of  " + DEFAULT_LINEARIZATION_DEGREE);
              //. note: added flag here that completely disables linearization if no valid file found.
              //. so default linearization is not used and could be removed.
              logger.Info("Disabling linerazation");
              linearize = false;
            }

            reformAuxillaryData(expExtents);
          }

          oldBadPixelMapFile = config.BadPixelMapFilename;
          oldReadVarianceFile = config.ReadVarianceFilename;
          oldLinearizationFile = config.LinearizationFilename;

          try {
            oldBadPixelMapModDate = (new FileInfo(oldBadPixelMapFile)).LastWriteTime;
          } catch {
            oldBadPixelMapModDate = INVALID_FILE_DATE_TIME;
          }

          try {
            oldReadVarianceModDate = (new FileInfo(oldReadVarianceFile)).LastWriteTime;
          } catch {
            oldReadVarianceModDate = INVALID_FILE_DATE_TIME;
          }

          try {
            oldLinearizationModDate = (new FileInfo(oldLinearizationFile)).LastWriteTime;
          } catch {
            oldLinearizationModDate = INVALID_FILE_DATE_TIME;
          }


          if ((tempConfig.HeartbeatType.CompareTo("counter") == 0) ||
            (tempConfig.HeartbeatType.CompareTo("timestamp") == 0)) {
            //. if thread is already running, stop it
            if (runHeartbeatThread) {
              runHeartbeatThread = false;
              Thread.Sleep(1000);
            }

            Thread heartbeatThread = new Thread(new ThreadStart(this.startHeartbeat));

            heartbeatThread.Start();
          } else {
            runHeartbeatThread = false;
          }

        } catch (InvalidOperationException ioEx) {
          throw new SidecarControlException("Error parsing Sidecar Control Configuration file. " + ioEx.Message);
        } finally {
          //. done with reader
          reader.Close();
        }
      } catch (Exception e) {
        throw new SidecarControlException("Error opening Sidecar Control Configuration file. " + e.Message);
      }

      return returnStatus;
    }

    private void readSidecarConfigurationFile(string sidecarConfigFile) {
      try {
        StreamReader reader = new StreamReader(sidecarConfigFile);
        XmlSerializer xmlSerializer = new XmlSerializer(typeof(SidecarConfiguration));
        SidecarConfiguration tempSidecarConfig;

        try {
          tempSidecarConfig = (SidecarConfiguration)xmlSerializer.Deserialize(reader);

          logger.Info("Sidecar Configuration File " + sidecarConfigFile + " loaded.");

          try {

            tempSidecarConfig.validateConfiguration();

            if (tempSidecarConfig.DetectorTypeInt == 1) {
              arrayCols = 1024;
              arrayRows = 1024;
            } else if (tempSidecarConfig.DetectorTypeInt == 4) {
              arrayCols = 4096;
              arrayRows = 4096;
            } else {
              arrayCols = 2048;
              arrayRows = 2048;
            }
            exposureCols = arrayCols;
            exposureRows = arrayRows;

            sidecarConfig = tempSidecarConfig;

            logger.Info("Detector Type = " + tempSidecarConfig.detectorType);
            logger.Info("Number of Detector Outputs = " + tempSidecarConfig.detectorOutputs);
            logger.Info("Number of Channels Averaged = " + tempSidecarConfig.numberChannelsAveraged);
            logger.Info("Operating Temperature is Warm? = " + tempSidecarConfig.operatingTemperatureWarm);
            logger.Info("Do kTC Noise Removal? = " + tempSidecarConfig.dokTCNoiseRemoval);
            logger.Info("Reset Once Per Row? = " + tempSidecarConfig.resetOncePerRow);
            logger.Info("Enhanced Horizontal Clocking? = " + tempSidecarConfig.enhancedHorizontalClocking);
            logger.Info("Do Global Reset? = " + tempSidecarConfig.doGlobalReset);
            logger.Info("Gain Setting = " + tempSidecarConfig.gainSetting + " (" + SidecarConfiguration.detectorGainTypes[tempSidecarConfig.gainSetting] + ")");
            logger.Info("Preamp Input Mode = " + tempSidecarConfig.preampInputMode + " (" + SidecarConfiguration.preampInputModeTypes[tempSidecarConfig.preampInputMode] + ")");
            logger.Info("Use Buffered Mode? = " + tempSidecarConfig.useBufferedMode);
            logger.Info("Data Transmission Mode = " + tempSidecarConfig.dataTransmissionMode + " (" + SidecarConfiguration.dataTransmissionModeTypes[tempSidecarConfig.dataTransmissionMode] + ")");
            logger.Info("Number of Serial Channels = " + tempSidecarConfig.numberSerialChannels);
            logger.Info("Science Block Header Length = " + tempSidecarConfig.scienceBlockHeaderLength);
            logger.Info("Data Output Level is LVDS (CMOS if false)? = " + tempSidecarConfig.dataOutputLevelLVDS);
            logger.Info("Saturation Level = " + tempSidecarConfig.saturationLevel);

          } catch (SidecarConfigurationException scEx) {
            throw new SidecarControlException("Error opening sidecar configuration file <" + sidecarConfigFile + ">. " + scEx.Message);

          }
        } catch (InvalidOperationException ioEx) {
          throw new SidecarControlException("Error parsing Sidecar Configuration file. " + ioEx.Message);
        } finally {
          //. done with reader
          reader.Close();
        }
      } catch (Exception e) {
        throw new SidecarControlException("Error opening Sidecar Configuration file. " + e.Message);
      }
    }

    public bool hasAuxillaryDataFilesChanged() {
      //. all the filenames must be the same, and 
      //. the file can't have changed.
      FileInfo fInfo;
      if (config.BadPixelMapFilename.CompareTo(oldBadPixelMapFile) != 0) {
        return true;
      }
      //. if they have the same filename, and the old one is null, than this one 
      //. will also be null, and is ok.
      if (oldBadPixelMapModDate != INVALID_FILE_DATE_TIME) {
        fInfo = new FileInfo(config.BadPixelMapFilename);
        if (fInfo.LastWriteTime.CompareTo(oldBadPixelMapModDate) != 0) {
          return true;
        }
      }
      if (config.ReadVarianceFilename.CompareTo(oldReadVarianceFile) != 0) {
        return true;
      }

      if (oldReadVarianceModDate != INVALID_FILE_DATE_TIME) {
        fInfo = new FileInfo(config.ReadVarianceFilename);
        if (fInfo.LastWriteTime.CompareTo(oldReadVarianceModDate) != 0) {
          return true;
        }
      }
      if (config.LinearizationFilename.CompareTo(oldLinearizationFile) != 0) {
        return true;
      }

      if (oldLinearizationModDate != INVALID_FILE_DATE_TIME) {
        fInfo = new FileInfo(config.LinearizationFilename);
        if (fInfo.LastWriteTime.CompareTo(oldLinearizationModDate) != 0) {
          return true;
        }
      }
      return false;
    }
    public void setReferencePixelSubtractionType(int type) {
      logger.Debug("Setting reference pixel type to " + type);
      config.setReferencePixelSubtractionType(type);
    }

    public void writeRawFrames(bool enable) {
      config.WriteRawFrames = enable;
    }



    public void stopHeartbeat() {
      runHeartbeatThread = false;
    }
    private void startHeartbeat() {
      string heartbeatString;
      runHeartbeatThread = true;
      heartbeatCounter = 0;
      logger.Info("Starting heartbeat with type <" + config.HeartbeatType + "> with period of " + config.heartbeatPeriod + " ms.");
      while (runHeartbeatThread) {
        if (config.HeartbeatType.CompareTo("counter") == 0) {
          heartbeatString = heartbeatCounter.ToString();
        } else {
          //. timestamp
          DateTime date = DateTime.Now;
          heartbeatString = date.ToString("yyyy/MM/dd HH:mm:ss");
        }
        //logger.Debug("Heartbeat: " + heartbeatString);
        valueListener.setValue(VALUE_NAME_HEARTBEAT, heartbeatString);
        heartbeatCounter++;
        Thread.Sleep(config.HeartbeatPeriod);
      }

    }
    private void formatDetectorSetupFlag() {
      detectorSetupFlag = 0;
      if (p1Exist) detectorSetupFlag |= 1;
      if (p2Exist) detectorSetupFlag |= 2;
      if (p3Exist) detectorSetupFlag |= 4;
      if (p4Exist) detectorSetupFlag |= 8;

    }
    private void initExposureStatus() {
      ExposurePercentComplete = 0.0;
      ExposureStatus = "No exposure in progress.";
      ExposureStatusDetail = "";
      ExposureStatusID = 0;
    }

    public void initSidecar(string jade, string asic, bool downloadFirmware) {
      string response;
      if (!applicationLoaded) {
        logger.Debug("Opening application <" + config.SidecarApplication + ">.");
        if (!(response = sidecar.OpenApp(config.SidecarApplication)).Contains(SIDECAR_SUCCESS_OPEN_APPLICATION)) {
          throw new SidecarControlException("Error opening application <" + config.SidecarApplication + ">: " + response);
        }
      }
      sidecarApplicationAtLastHardwareInit = config.SidecarApplication;

      logger.Debug("Checking to see if jade <" + jade + "> is available.");
      if (!isJadeAvailable(jade)) {
        throw new SidecarControlException("Jade <" + jade + "> is not available.");
      }

      if (downloadFirmware) {
        logger.Debug("Downloading Jade firmware.");
        if (!(response = sidecar.DownloadJADEFirmware(jade)).Contains(SIDECAR_SUCCESS_DOWNLOAD_FIRMWARE)) {
          throw new SidecarControlException("Error downloading JADE firmware: " + response);
        }
      }

      logger.Debug("Downloading Jade registers.");
      if (!(response = sidecar.DownloadJADERegisters(jade)).Contains(SIDECAR_SUCCESS_DOWNLOAD_JADE_REGISTERS)) {
        throw new SidecarControlException("Error downloading JADE registers: " + response);
      }

      //. turn dataOnAck off.
      logger.Debug("Turning DataOnAck off.");
      setDataOnAck(jade, false);

      //. reset ASIC
      logger.Debug("Resetting ASIC.");
      resetASIC(jade);

      Thread.Sleep(WAITTIME_MS_AFTER_DATA_ON_ACK);

      //. check to see if ASIC is available.
      logger.Debug("Checking to see if ASIC <" + asic + "> is available.");
      if (!isAsicAvailable(jade, asic)) {
        throw new SidecarControlException("Error initializing SIDECAR. ASIC <" + asic + "> is not available.");
      }

      //. download timing file
      logger.Debug("Downloading MCD file.");
      if (!(response = sidecar.DownloadMCD(jade, asic)).Contains(SIDECAR_SUCCESS_DOWNLOAD_MCD_FILE)) {
        throw new SidecarControlException("Error downloading timing file: " + response);
      }

      //. turn dataOnAck on.
      logger.Debug("Turning DataOnAck on.");
      setDataOnAck(jade, true);

      //. check to see if ASIC is available.
      logger.Debug("Checking to see if ASIC <" + asic + "> is available.");
      if (!isAsicAvailable(jade, asic)) {
        throw new SidecarControlException("Error initializing SIDECAR. ASIC <" + asic + "> is not available (MCD downloaded).");
      }

      if (config.CodeVersionHxRG) {
        configureSidecar(jade, asic);
        reconfigureSidecar(jade, asic);

        sidecarConfigAtLastHardwareInit = sidecarConfig.Clone();

        updateExposureExtents(jade, asic);
      }

      getVoltages(jade, asic);
    }
    private void getVoltages(string jade, string asic) {
      //. get Voltages
      //. note: getAsicRegister appears to succeed even if ASIC is not available,
      //. and returns 0x0000 in that case.  Since it is possible (though unlikely)
      //. these voltages could be 0, we can't check for errors.
      sidecarConfig.VReset = getVoltage(jade, asic, ASIC_REGISTER_VRESET);
      logger.Debug("VReset = " + sidecarConfig.VReset);
      valueListener.setValue(VALUE_NAME_VRESET, sidecarConfig.VReset.ToString());

      sidecarConfig.DSub = getVoltage(jade, asic, ASIC_REGISTER_DSUB);
      logger.Debug("DSub = " + sidecarConfig.DSub);
      valueListener.setValue(VALUE_NAME_DSUB, sidecarConfig.DSub.ToString());

      sidecarConfig.VBiasGate = getVoltage(jade, asic, ASIC_REGISTER_VBIASGATE);
      logger.Debug("VBiasGate = " + sidecarConfig.VBiasGate);

      sidecarConfig.VBiasPower = getVoltage(jade, asic, ASIC_REGISTER_VBIASPOWER);
      logger.Debug("VBiasPower = " + sidecarConfig.VBiasPower);

      sidecarConfig.CellDrain = getVoltage(jade, asic, ASIC_REGISTER_CELLDRAIN);
      logger.Debug("VBiasGate = " + sidecarConfig.VBiasGate);

      sidecarConfig.Drain = getVoltage(jade, asic, ASIC_REGISTER_DRAIN);
      logger.Debug("Drain = " + sidecarConfig.Drain);

      sidecarConfig.VDDA = getVoltage(jade, asic, ASIC_REGISTER_VDDA);
      logger.Debug("VDDA = " + sidecarConfig.VDDA);

      sidecarConfig.VDD = getVoltage(jade, asic, ASIC_REGISTER_VDD);
      logger.Debug("VDD = " + sidecarConfig.VDD);

      sidecarConfig.VRefMain = getVoltage(jade, asic, ASIC_REGISTER_VREFMAIN);
      logger.Debug("VRefMain = " + sidecarConfig.VRefMain);

      sidecarConfig.VPreMidRef = getVoltage(jade, asic, ASIC_REGISTER_VPREMIDREF);
      logger.Debug("VPreMidRef = " + sidecarConfig.VPreMidRef);

      sidecarConfig.VPreAmpRef1 = getVoltage(jade, asic, ASIC_REGISTER_VPREAMPREF1);
      logger.Debug("VPreAmpRef1 = " + sidecarConfig.VPreAmpRef1);
    }

    private void getGainSetting(string jade, string asic) {
      string response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_GAIN, false);
      int intVal = hexStringToInt(response);
      //. gain setting is last 4 bits
      sidecarConfig.GainSetting = intVal & 0xF;
    }
    private void getSidecarConfigurationRegisters(string jade, string asic) {
      StringBuilder buffer = new StringBuilder();
      string response;
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_TYPE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_OUTPUTS, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_ADCS_AVERAGED, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DISABLE_HXRG, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DISABLE_CLOCKS, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_OPERATING_TEMPERATURE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_MODE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_HXRG_CLOCKING, false);
      buffer.Append(response.Substring(2));

      sidecarConfig.SidecarConfigurationDumpLine1 = buffer.ToString();

      buffer = new StringBuilder();
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_INPUT_MODE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_GAIN, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_BUFFERED, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DATA_TRANSMISSION_MODE, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_SERIAL_CHANNELS, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_HEADER_LENGTH, false);
      buffer.Append(response.Substring(2) + " ");
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DATA_OUTPUT_LEVEL, false);
      buffer.Append(response.Substring(2));

      sidecarConfig.SidecarConfigurationDumpLine2 = buffer.ToString();

    }

    private void setDataOnAck(string jade, bool on) {
      //. get current dataOnAck
      string response = sidecar.ReadJadeRegister(jade, JADE_REGISTER_DATA_ON_ACK);

      //. set/unset bit 0
      int newVal;

      if (on) {
        newVal = setBit(hexStringToInt(response), 0);
      } else {
        newVal = unsetBit(hexStringToInt(response), 0);
      }
      //. write new value back to register
      sidecar.WriteJadeRegister(jade, JADE_REGISTER_DATA_ON_ACK, intToHexString(newVal));

    }

    private void initMemory() {
      long totalNumberOfPixels = arrayRows * arrayCols;

      readImage = new ushort[totalNumberOfPixels];
      referencePixelSubtractedImage = new float[totalNumberOfPixels];
      firstread = new float[totalNumberOfPixels];
      secondread = new float[totalNumberOfPixels];
      utr_wdeltas = new float[totalNumberOfPixels];
      utr_lastread = new float[totalNumberOfPixels];
      utr_maxdel = new float[totalNumberOfPixels];
      utr_mindel = new float[totalNumberOfPixels];
      utr_a = new float[totalNumberOfPixels];
      utr_b = new float[totalNumberOfPixels];
      utr_weights = new ushort[totalNumberOfPixels];
      utr_maxw = new ushort[totalNumberOfPixels];
      utr_minw = new ushort[totalNumberOfPixels];
      saturated = new byte[totalNumberOfPixels];
      badpix = new byte[totalNumberOfPixels];
      readvar = new float[totalNumberOfPixels];
      linearization = new float[totalNumberOfPixels][];
      dataimage = new float[totalNumberOfPixels];
      errimage = new float[totalNumberOfPixels];
      qualimage = new byte[totalNumberOfPixels];
    }


    private void setExposureStatus(string status, string detail) {
      if (status != null) {
        logger.Debug("exposureStatus: " + status);
        ExposureStatus = status;
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS, status);
      }
      if (detail != null) {
        ExposureStatusDetail = detail;
        logger.Debug("exposureStatusDetail: " + detail);
        valueListener.setValue(VALUE_NAME_EXPOSURE_STATUS_DETAIL, detail);
      }
      ExposureStatusID++;
    }
    private string intToHexString(int num) {
      return String.Format("0x{0:x4}", num);
    }
    private int hexStringToInt(string hex) {
      return Convert.ToInt32(hex, 16);
    }
    private int setBit(int num, int bit) {
      return num | (1 << bit);
    }
    private int unsetBit(int num, int bit) {
      return num & ~(1 << bit);
    }

    public int validateMCDFile(string filename) {
      StreamReader file = null;
      string line;
      int linenum = 0;
      char[] lineArray;
      try {
        file = new StreamReader(filename);
        int blockLine = 0;
        int blockLength = 0;
        while ((line = file.ReadLine()) != null) {
          linenum++;
          //. line can be either
          //.   - 1 or 2 columns of 4 digit hex numbers
          //.   - blockend or blockstart words alone
          //.   - address followed by 4 digit hex number
          //.   - length followed by number

          //. if blockstart, must be followed by address, then length, then n lines of 4 digit numbers
          //. where n if number after length, then blockend

          //. example: 
          //. blockstart
          //. address 0000
          //. length 3
          //. 0000
          //. 0b01
          //. af00
          //. blockend
          if (line.Equals("blockstart")) {
            blockLine = 1;
          } else {
            if (blockLine == 0) {
             //. must be two column 4 digit hex data
              lineArray = line.ToCharArray();
              if (lineArray.Length != 9) {
                logger.Debug("Badly Formed MCD - line " + linenum + ": expecting two column 4 digit hex data");
                return -1;
              }
              if (!isValidHexDigit(lineArray[0]) ||
                !isValidHexDigit(lineArray[1]) ||
                !isValidHexDigit(lineArray[2]) ||
                !isValidHexDigit(lineArray[3]) ||
                (lineArray[4] != ' ') ||
                !isValidHexDigit(lineArray[5]) ||
                !isValidHexDigit(lineArray[6]) ||
                !isValidHexDigit(lineArray[7]) ||
                !isValidHexDigit(lineArray[8])) {
                logger.Debug("Badly Formed MCD - line " + linenum + ": expecting two column 4 digit hex data");
                return -1;
              }
            } else if (blockLine == 1) {
              lineArray = line.ToCharArray();
              if (lineArray.Length != 12) {
                logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"address \" followed by 4 digit hex data");
                return -1;
              }
              if ((lineArray[0] != 'a') ||
                (lineArray[1] != 'd') ||
                (lineArray[2] != 'd') ||
                (lineArray[3] != 'r') ||
                (lineArray[4] != 'e') ||
                (lineArray[5] != 's') ||
                (lineArray[6] != 's') ||
                (lineArray[7] != ' ') ||
                !isValidHexDigit(lineArray[8]) ||
                !isValidHexDigit(lineArray[9]) ||
                !isValidHexDigit(lineArray[10]) ||
                !isValidHexDigit(lineArray[11])) {
                logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"address \" followed by 4 digit hex data");
                return -1;
              }
              blockLine = 2;
            } else if (blockLine == 2) {
              if (line.StartsWith("length ")) {
                string numString = line.Substring(7);
                try {
                  blockLength = int.Parse(numString);
                } catch (Exception) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"length \" followed by a positive integer");
                  return -1;
                }
                //. confirm no extra space
                if (!numString.Equals(blockLength.ToString())) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"length \" followed by a positive integer");
                  return -1;
                }
                if (blockLength <= 0) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"length \" followed by a positive integer");
                  return -1;
                }
                blockLine = 3;
              } else {
                logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"length \" followed by a positive integer");
                return -1;
              }
            } else {
              if (blockLine == (blockLength+3)) {
                if (!line.Equals("blockend")) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting the word \"blockend\"");
                  return -1;
                }
                blockLine=0;
              } else {
                lineArray = line.ToCharArray();
                if (lineArray.Length != 4) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting 4 digit hex data");
                  return -1;
                }
                if (!isValidHexDigit(lineArray[0]) ||
                !isValidHexDigit(lineArray[1]) ||
                !isValidHexDigit(lineArray[2]) ||
                !isValidHexDigit(lineArray[3])) {
                  logger.Debug("Badly Formed MCD - line " + linenum + ": expecting 4 digit hex data");
                  return -1;
                }
                blockLine++;
              }
            }
          }
        }
      } finally {
        if (file != null) {
          file.Close();
        }
      }
      
      return 0;
    }
    private bool isValidHexDigit(char digit) {
      return (Char.IsDigit(digit) || 
        (digit == 'a') ||
        (digit == 'b') ||
        (digit == 'c') ||
        (digit == 'd') ||
        (digit == 'e') ||
        (digit == 'f')
        );
    }
    private int getRemainingDiskSpace(DirectoryInfo root) {
      DriveInfo[] allDrives = DriveInfo.GetDrives();
      foreach (DriveInfo d in allDrives) {
        logger.Debug("Current drive: "+d.Name);
        if  (d.RootDirectory.Name.Equals(root.Name.ToUpper())) {
          int freeSpaceMB = (int)Math.Floor(d.AvailableFreeSpace / (double)(1024 * 1024));
          logger.Debug("Current drive: " + d.Name + ", FreeSpace=" + freeSpaceMB + " MB.");
          return freeSpaceMB;
        }
      }
      return -1;
    }
    public int validateFilename(string filename) {
      //. replace \\ with \
      while (filename.Contains(@"\\")) {
        filename = filename.Replace(@"\\", @"\");
      }

      FileInfo fileInfo = new FileInfo(filename);
      DirectoryInfo directoryInfo = new DirectoryInfo(filename);

      //. if filename does not end with \, check if it is a filename
      if (!filename.EndsWith("\\")) {
        if (fileInfo.Exists) {
          logger.Debug("File exists.");
          //. it is a filename
          if (fileInfo.IsReadOnly) {
            logger.Debug("File exists and is read-only");
            return -1;
          } else {
            logger.Debug("File exists and is writeable");
            return 1;
          }
        }
        //. do a check to see if it is a valid directory name
        if (!directoryInfo.Exists) {
          //. if not, assume is it a new file, and get the parent directory
          directoryInfo = fileInfo.Directory;
        }
      }

      logger.Debug("validateFilename: directory = " + directoryInfo.FullName);

      //. if directory exists
      if (!(directoryInfo.Exists)) {
        logger.Debug("Directory doesn't exist");
        return -2;
      }

      //. check to see if directory is writeable
      if ((directoryInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
        logger.Debug("Directory is read only");
        return -3;
      }

      logger.Debug("Directory is writeable.");
      return 0;

    }

    public void initializeBadPixelMap() {
      int badpixXS, badpixYS;

      try {
        //. get bad pixel map
        logger.Debug("initializing bad pixel map: " + config.BadPixelMapFilename);
        auxData.initBadPixelMap(config.BadPixelMapFilename);

        logger.Debug("creating badpix array");
        badpixXS = auxData.BadPixelMap.GetLength(0);
        if (badpixXS != arrayCols) {
          throw new SidecarControlException("Invalid bad pixel map.  X size <" + badpixXS + "> does not match array columns <" + arrayCols + ">.");
        }
        badpixYS = auxData.BadPixelMap.GetLength(1);
        if (badpixYS != arrayRows) {
          throw new SidecarControlException("Invalid bad pixel map.  Y size <" + badpixYS + "> does not match array rows <" + arrayRows + ">.");
        }
        logger.Debug("badpix array has length = " + badpix.Length);

      } catch (nom.tam.fits.FitsException fEx) {
        throw new SidecarControlException("FITS Error reading bad pixel map: " + fEx.Message);
      } catch (Exception ex) {
        throw new SidecarControlException("Error reading linearization file: " + ex.Message);
      }
    }

    public void initializeReadVariance() {
      int readvarXS, readvarYS;

      try {
        logger.Debug("initializing noise file: " + config.ReadVarianceFilename);
        auxData.initReadVariance(config.ReadVarianceFilename);

        logger.Debug("creating noise array");
        readvarXS = auxData.ReadVariance.GetLength(0);
        if (readvarXS != arrayCols) {
          throw new SidecarControlException("Invalid read variance array.  X size <" + readvarXS + "> does not match array columns <" + arrayCols + ">.");
        }
        readvarYS = auxData.ReadVariance.GetLength(1);
        if (readvarYS != arrayRows) {
          throw new SidecarControlException("Invalid read variance array.  Y size <" + readvarYS + "> does not match array rows <" + arrayRows + ">.");
        }
        logger.Debug("noise array has length = " + readvar.Length);


      } catch (nom.tam.fits.FitsException fEx) {
        throw new SidecarControlException("FITS Error reading read variance file: " + fEx.Message);
      } catch (Exception ex) {
        throw new SidecarControlException("Error reading linearization file: " + ex.Message);
      }
    }

    public void initializeLinearization() {
      int linearizationXS, linearizationYS;

      try {
        logger.Debug("initializing Linearization file: " + config.LinearizationFilename);
        auxData.initLinearization(config.LinearizationFilename);

        logger.Debug("creating linearization array");
        linearizationXS = auxData.Linearization.GetLength(0);
        if (linearizationXS != arrayCols) {
          throw new SidecarControlException("Invalid linearization array.  X size <" + linearizationXS + "> does not match array columns <" + arrayCols + ">.");
        }
        linearizationYS = auxData.Linearization.GetLength(1);
        if (linearizationYS != arrayRows) {
          throw new SidecarControlException("Invalid linearization array.  Y size <" + linearizationYS + "> does not match array rows <" + arrayRows + ">.");
        }
        logger.Debug("linearization has length = " + linearization.Length);

      } catch (nom.tam.fits.FitsException fEx) {
        throw new SidecarControlException("FITS Error reading linearization file: " + fEx.Message);
      } catch (Exception ex) {
        throw new SidecarControlException("Error reading linearization file: " + ex.Message);
      }
    }
    public void reformAuxillaryData(ExposureExtents extents) {
      int numLinearizationCoeffs;
      int ii, jj, kk, pix;

      logger.Debug("Reforming auxillary data");
      numLinearizationCoeffs = auxData.Linearization.GetLength(2);

      pix = 0;
      for (jj = extents.StartY; jj < (extents.EndY + 1); jj++) {
        for (ii = extents.StartX; ii < (extents.EndX + 1); ii++) {
          badpix[pix] = auxData.BadPixelMap[ii, jj];
          readvar[pix] = auxData.ReadVariance[ii, jj];
          linearization[pix] = new float[numLinearizationCoeffs];
          for (kk = 0; kk < numLinearizationCoeffs; kk++) {
            linearization[pix][kk] = auxData.Linearization[ii, jj, kk];
          }
          pix++;
        }
      }

    }

    public int addFitsHeader(string fitsFilename, List<SidecarIceFitsHeaderKey> header) {
      //. for now, print keys
      FitsWriter fits = (FitsWriter)openFitsWriters[fitsFilename];
      if (fits != null) {
        fits.addFitsHeader(header);
        fits.fitsHeaderAdded = true;
        foreach (SidecarIceFitsHeaderKey key in header) {
          logger.Debug("^^^^^^^^^^^^^ Fits keyword: " + key.name + " = " + key.value + " (" + key.comment + ")");
        }
      }
      return 0;
    }

    private void prepFifoForExposure(string jade) {
      string sValue;
      string response;
      int intVal;

      //. get current value
      sValue = sidecar.ReadJadeRegister(jade, JADE_REGISTER_FIFO_PREP);
      //. TODO error check (what is returned in erroneous call)?

      //. convert to int 
      intVal = hexStringToInt(sValue);

      //. clear bits 3-2, 7-4, 11-8
      intVal &= 0xF003;

      //. bit2: Enable/disable quad_FIFO or single_FIFO.  0 for single detector
      //. bit3: Rotate to next FIFO after RotateLength words. 0 for single detector
      //. bits 9-11: block length for autoRotation. 0 for single detector
      //. bits 4-7: same as detectorSetupFlag

      intVal = intVal | (detectorSetupFlag << 4) | 0x100;

      //. convert back to string
      sValue = intToHexString(intVal);

      //. write to jade register
      response = sidecar.WriteJadeRegister(jade, JADE_REGISTER_FIFO_PREP, sValue);

      if (response.CompareTo(SIDECAR_SUCCESS_WRITE_JADE_REGISTER) != 0) {
        throw new SidecarControlException("Error prepping FIFO.  Error writing to Jade register");
      }
    }
    public void resetASIC(string jade) {
      string sValue;
      string response;
      int intVal;

      //. resetting the ASIC is done by turning bit 12 of jade register 0x0015 off and then on 

      //. get current value
      sValue = sidecar.ReadJadeRegister(jade, JADE_REGISTER_RESET_ASIC);
      //. TODO error check (what is returned in erroneous call)?

      //. convert to int 
      intVal = hexStringToInt(sValue);

      //. clear bit 12
      intVal = unsetBit(intVal, 12);

      //. convert back to string
      sValue = intToHexString(intVal);

      //. write to jade register
      response = sidecar.WriteJadeRegister(jade, JADE_REGISTER_RESET_ASIC, sValue);

      if (response.CompareTo(SIDECAR_SUCCESS_WRITE_JADE_REGISTER) != 0) {
        throw new SidecarControlException("Error resetting ASIC.  Error writing to Jade register (unset bit)");
      }

      //. set bit 12
      intVal = setBit(intVal, 12);

      //. convert back to string
      sValue = intToHexString(intVal);

      //. write to jade register
      response = sidecar.WriteJadeRegister(jade, JADE_REGISTER_RESET_ASIC, sValue);

      if (response.CompareTo(SIDECAR_SUCCESS_WRITE_JADE_REGISTER) != 0) {
        throw new SidecarControlException("Error resetting ASIC.  Error writing to Jade register (set bit)");
      }
    }
    public void powerDownASIC(string jade) {
      string sValue;
      string response;
      int intVal;

      //. powering down the ASIC is done by:
      //.    SlowMode: set bits 11-8 to 0 in Jade register 0x0020
      //.    HxRG: set Jade registers 0x0021 and 0x0020 to 0x0000 in that order

      if (config.CodeVersionHxRG) {
        sValue = "0x0000";

        //. write to jade register
        response = sidecar.WriteJadeRegister(jade, JADE_REGISTER_ASIC_POWER_BIAS, sValue);

        if (response.CompareTo(SIDECAR_SUCCESS_WRITE_JADE_REGISTER) != 0) {
          throw new SidecarControlException("Error powering down ASIC Bias voltages.  Error writing <" + sValue + "> to Jade register <" + JADE_REGISTER_ASIC_POWER_BIAS + ">.");
        }


      } else {
        //. get current value
        sValue = sidecar.ReadJadeRegister(jade, JADE_REGISTER_RESET_ASIC);
        //. TODO error check (what is returned in erroneous call)?

        //. convert to int 
        intVal = hexStringToInt(sValue);

        //. clear bits 11-8
        intVal = (intVal & 0xF0FF);

        //. convert back to string
        sValue = intToHexString(intVal);
      }

      //. write to jade register
      response = sidecar.WriteJadeRegister(jade, JADE_REGISTER_ASIC_POWER_5V, sValue);

      if (response.CompareTo(SIDECAR_SUCCESS_WRITE_JADE_REGISTER) != 0) {
        throw new SidecarControlException("Error powering down ASIC 5V and ADC.  Error writing <" + sValue + "> to Jade register <" + JADE_REGISTER_ASIC_POWER_5V + ">.");
      }
    }
    private SidecarRamp createRamp(ExposureConfiguration expConfig, int readTimeInMicros) {
      /*
       * Ramp definition with discarded reads
       * note: single sampling mode does not do discarded reads
       * 
       * i = itime ms
       * r = reads (pairs for mcds)
       * g = groups (for utr.  = 2 for others)
       * c = coadds
       * z = resets
       * 
       * Z = ramp resets
       * R = ramp reads
       * D = ramp drops
       * G = ramp groups
       * 
       * x = # of discarded reads
       * t = read time for frame in ms
       *
       * For ALL: Z = z
       * 
       * For single:
       *    minItime = 0
       *    if (i < t)
       *      G = 1
       *      R = 1
       *      D = 0
       *   else
       *    G = 2
       *    R = x
       *    D = i/t - R  
       *    
       * For CDS:
       *    minItime = t
       *    G = 2
       *    R = x+1
       *    D = i/t - R
       * 
       * For MCDS:
       *    minItime = r*t
       *    G = 2
       *    R = x+r
       *    D = i/t - R
       * 
       * For UTR:
       *    minItime = ((x+r)*(g-1)) * t
       *    G = g
       *    R = x + r
       *    D = i/(G-1)t - R
       * 
       * 
       * For single, CDS, and MCDS:
       * 
       * If D >= 0, we take the last reads of each group, after the ignored reads
       * 
       * For example, for a config where
       * 
       *  s = MCDS     \    G = 2
       *  r = 4         \   R = 6
       *  i = 8t        /   D = 8 - 6 = 2
       *  x = 2        /
       * 
       * in this exposure, we take the last 4 reads of the first group for
       * the first set of reads, and the last 4 reads of the second group for
       * the second set of reads.
       * 
       * if D goes negative, specify the number of drops to 0 in hardware
       * but start taking reads of the second set that would have been 
       * discarded instead, starting with the absolute value of the drops value.
       * 
       * For example, for a config where
       * 
       *  s = MCDS     \    G = 2
       *  r = 4         \   R = 6
       *  i = 5t        /   D = 5 - 6 = -1
       *  x = 2        /
       * 
       * This means the first set of valid samples is the last four reads 
       * of the first group, and the second set of reads is the 2nd
       * through 5th read of the second group.  The first read of the second
       * group is ignored, and serves as the drop frame to match the 5t itime.
       * 
       * Note that D cannot be less than -x (ignored reads), because of minimum
       * allowed integration time.
       * 
       * For UTR: 
       * 
       * We will not try to handle the case of shorter integration times.  Each 
       * sample will be a set of reads of x+1, with the first x frames of each 
       * sample ignored.
       * 
       * read time per frame is readTimeInMicros (read time for full frame) 
       */
      SidecarRamp ramp = new SidecarRamp();
      ramp.Coadds = expConfig.Coadds;

      int minItime;

      if (expConfig.Resets < 0) {
        throw new InvalidExposureConfigurationException("Invalid number of resets: " + expConfig.Resets + ". Must be non-negative.");
      }
      ramp.Resets = expConfig.Resets;

      if (expConfig.Coadds < 1) {
        throw new InvalidExposureConfigurationException("Invalid number of coadds: " + expConfig.Coadds + ". Must be at least 1.");
      }

      if (expConfig.Reads < 1) {
        throw new InvalidExposureConfigurationException("Invalid number of reads: " + expConfig.Reads + ". Must be at least 1.");
      }
      if (expConfig.Reads > 255) {
        throw new InvalidExposureConfigurationException("Invalid number of reads: " + expConfig.Reads + ". Must be less than 256.");
      }

      if (expConfig.Groups < 2) {
        throw new InvalidExposureConfigurationException("Invalid number of groups: " + expConfig.Groups + ". Must be at least 2.");
      }

      if (readTimeInMicros <= 0) {
        throw new InvalidExposureConfigurationException("Invalid read time: " + readTimeInMicros + ". Must be greater than zero");
      }

      //. determine minimum integration time based on sampling mode
      if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_SINGLE) {
        //. itime must be positive
        minItime = 0;
      } else if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_CDS) {
        //. itime must be > readTime
        minItime = readTimeInMicros;
      } else if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_MCDS) {
        //. itime must be > readTime * number of reads
        minItime = readTimeInMicros * expConfig.Reads;
      } else if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_UTR) {
        //. itime must be > readTime * (number of groups - 1) * (ignored reads + number of reads)
        minItime = readTimeInMicros * (expConfig.Groups - 1) * (config.IgnoredInitialReads + expConfig.Reads);
      } else {
        throw new InvalidExposureConfigurationException("Invalid sampling mode: " + expConfig.SampMode + ".");
      }

      if (expConfig.ItimeInMicros < minItime) {
        throw new InvalidExposureConfigurationException("Invalid integration time: " + expConfig.ItimeInMicros + " microseconds.  Must be larger than " + minItime + " microseconds.");
      }

      ramp.Reads = expConfig.Reads + config.IgnoredInitialReads;
      switch (expConfig.SampMode) {
        case ExposureConfiguration.SAMPLING_MODE_SINGLE:
          ramp.Reads = 1;
          //. reset expConfig.Reads with actual used value
          expConfig.Reads = 1;
          if (expConfig.ItimeInMicros < readTimeInMicros) {
            ramp.Drops = 0;
            ramp.Groups = 1;
            break;
          } else {
            goto case ExposureConfiguration.SAMPLING_MODE_MCDS;
          }
        case ExposureConfiguration.SAMPLING_MODE_CDS:
          ramp.Reads = config.IgnoredInitialReads + 1;
          //. reset expConfig.Reads with actual used value
          expConfig.Reads = 1;
          goto case ExposureConfiguration.SAMPLING_MODE_MCDS;
        case ExposureConfiguration.SAMPLING_MODE_MCDS:
          ramp.Drops = (int)Math.Floor(expConfig.ItimeInMicros / (double)(readTimeInMicros)) - ramp.Reads;
          ramp.Groups = 2;
          break;
        case ExposureConfiguration.SAMPLING_MODE_UTR:
          ramp.Reads = config.IgnoredInitialReads + expConfig.Reads;
          ramp.Drops = (int)(Math.Floor(expConfig.ItimeInMicros / (double)(expConfig.Groups - 1) / (double)(readTimeInMicros))) - ramp.Reads;
          ramp.Groups = expConfig.Groups;
          break;
        default:
          throw new InvalidExposureConfigurationException("Invalid sampling mode: " + expConfig.SampMode + ".");
      }
      //. this isn't exactly true for SINGLE with itime < readTime, 
      //. but it isn't used for single sampling arithmetic anyway
      expConfig.GroupItime = (ramp.Drops + ramp.Reads) * readTimeInMicros / 1000000.0f;


      logger.Debug("ramp created: reads = " + ramp.Reads + ", drops = " + ramp.Drops + ", groups = " + ramp.Groups + ", resets = " + ramp.Resets + ".");

      return ramp;

    }

    private void executeRamp(string jade, string asic, SidecarRamp ramp) {
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_RESETS, intToHexString(ramp.Resets));
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_READS, intToHexString(ramp.Reads));
      //. drops can be negative if we are discarding reads.  see createRamp.
      if (ramp.Drops < 0) {
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_DROPS, intToHexString(0));
      } else {
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_DROPS, intToHexString(ramp.Drops));
      }
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_GROUPS, intToHexString(ramp.Groups));
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP_COADDS, intToHexString(ramp.Coadds));
    }
    private void setExposureSubarrayLimits(string jade, string asic, ExposureExtents config) {
      string mode;
      int newModeVal;

      //. check limits
      if ((config.StartX < 0) || (config.StartX > (arrayCols - 1))) {
        throw new InvalidExposureConfigurationException("Invalid subarray StartX: " + config.StartX + ".  Must be between 0 and " + (arrayCols - 1) + ".");
      }
      if ((config.EndX < 0) || (config.EndX > (arrayCols - 1))) {
        throw new InvalidExposureConfigurationException("Invalid subarray EndX: " + config.EndX + ".  Must be between 0 and " + (arrayCols - 1) + ".");
      }
      if ((config.StartY < 0) || (config.StartY > (arrayRows - 1))) {
        throw new InvalidExposureConfigurationException("Invalid subarray StartY: " + config.StartY + ".  Must be between 0 and " + (arrayRows - 1) + ".");
      }
      if ((config.EndY < 0) || (config.EndY > (arrayRows - 1))) {
        throw new InvalidExposureConfigurationException("Invalid subarray EndY: " + config.EndY + ".  Must be between 0 and " + (arrayRows - 1) + ".");
      }

      if (config.StartX > config.EndX) {
        throw new InvalidExposureConfigurationException("Invalid subarray limits.  StartX <" + config.StartX + "> must be less than or equal to EndX <" + config.EndX + ">.");
      }
      if (config.StartY > config.EndY) {
        throw new InvalidExposureConfigurationException("Invalid subarray limits.  StartY <" + config.StartY + "> must be less than or equal to EndX <" + config.EndX + ">.");
      }

      exposureRows = config.EndY - config.StartY + 1;
      exposureCols = config.EndX - config.StartX + 1;

      //. get current exposure mode
      mode = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, false);

      if ((exposureRows == arrayRows) && (exposureCols == arrayCols)) {
        //. if using full limits, set full mode in reg.  unset bit 1
        newModeVal = unsetBit(hexStringToInt(mode), 1);
        config.IsSubarray = false;

      } else {
        //. if not using full limits, set window mode in reg.  set bit 1
        newModeVal = setBit(hexStringToInt(mode), 1);

        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_STARTX, intToHexString(config.StartX));
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_ENDX, intToHexString(config.EndX));
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_STARTY, intToHexString(config.StartY));
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_ENDY, intToHexString(config.EndY));

        config.IsSubarray = true;
      }

      //. set exposure window mode
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, intToHexString(newModeVal));

    }

    private void configureSidecar(string jade, string asic) {
      logger.Debug("Configuring SIDECAR.");

      //      logger.Debug("Detector type: " + sidecarConfig.DetectorType + ". Number of outputs = " + sidecarConfig.DetectorOutputs);

      //. set detector type
      //. get current value of detector type register
      string currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_TYPE, false);
      int curIntVal = hexStringToInt(currentVal);
      //. set bits 2-0 to intType
      int newVal = (curIntVal & 0xFFF8) | (sidecarConfig.DetectorTypeInt & 0x0007);
      //. set new value of detector type register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_TYPE, intToHexString(newVal));

      //. set number of detector outputs
      //. get current value of detector outputs register
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_OUTPUTS, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 5-0 to sidecarConfig.DetectorOutputs
      newVal = (curIntVal & 0xFFC0) | (sidecarConfig.DetectorOutputs & 0x003F);
      //. set new value of detector outputs register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_DETECTOR_OUTPUTS, intToHexString(newVal));


      //. set number of SIDECAR ADC's averages per detector output (should be 1 for 32 channels)
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_ADCS_AVERAGED, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 3-0 to sidecarConfig.numberOfChannelsAveraged
      newVal = (curIntVal & 0xFFF0) | (sidecarConfig.numberChannelsAveraged & 0x000F);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_ADCS_AVERAGED, intToHexString(newVal));

      //. set operating temperature
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_OPERATING_TEMPERATURE, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 based on  sidecarConfig
      if (sidecarConfig.OperatingTemperatureWarm) {
        newVal = setBit(curIntVal, 0);
      } else {
        newVal = unsetBit(curIntVal, 0);
      }
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_OPERATING_TEMPERATURE, intToHexString(newVal));

      //. set preamp mode
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_MODE, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 for kTC noise removal based on  sidecarConfig
      if (sidecarConfig.DokTCNoiseRemoval) {
        newVal = setBit(curIntVal, 0);
      } else {
        newVal = unsetBit(curIntVal, 0);
      }
      //. set bit 1 for resetting based on sidecarConfig
      if (sidecarConfig.ResetOncePerRow) {
        newVal = setBit(newVal, 1);
      } else {
        newVal = unsetBit(newVal, 1);
      }
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_MODE, intToHexString(newVal));

      //. set clocking
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_HXRG_CLOCKING, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 for enhanced horizontal clocking
      if (sidecarConfig.EnhancedHorizontalClocking) {
        newVal = setBit(curIntVal, 0);
      } else {
        newVal = unsetBit(curIntVal, 0);
      }
      //. set bit 1 for global reset
      if (sidecarConfig.DoGlobalReset) {
        newVal = setBit(newVal, 1);
      } else {
        newVal = unsetBit(newVal, 1);
      }
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_HXRG_CLOCKING, intToHexString(newVal));

      //. set UTR sampling
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 to 0 for UTR
      newVal = unsetBit(curIntVal, 0);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, intToHexString(newVal));

      //. set preamp input mode
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_INPUT_MODE, false);
      curIntVal = hexStringToInt(currentVal);
      //. set up based on mode:
      switch (sidecarConfig.PreampInputMode) {
        case 0: //. shorted to ground
          newVal = setBit(newVal, 1);
          break;
        case 1: //. single ended
          newVal = unsetBit(curIntVal, 0);
          newVal = unsetBit(newVal, 1);
          break;
        case 2: //. differential
          newVal = setBit(curIntVal, 0);
          newVal = unsetBit(newVal, 1);
          break;
      };
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_INPUT_MODE, intToHexString(newVal));
      //. if shorted to preamps, you must write 0xAAAA to 0x5100 (Reference override register)
      if (sidecarConfig.PreampInputMode == 0) {
        sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_INPUT_REF_OVERRIDE, ASIC_REGISTER_VALUE_SHORTED_PREAMPS);
      }

      //. set gain
      //. get current value 
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_GAIN, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 3-0 to gain setting
      newVal = (curIntVal & 0xFFF0) | (sidecarConfig.GainSetting & 0x000F);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_GAIN, intToHexString(newVal));

      //. set buffered current source
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_BUFFERED, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 based on  sidecarConfig
      if (sidecarConfig.UseBufferedMode) {
        newVal = setBit(curIntVal, 0);
      } else {
        newVal = unsetBit(curIntVal, 0);
      }
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_PREAMP_BUFFERED, intToHexString(newVal));


      //. set data transmission mode
      //. get current value 
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DATA_TRANSMISSION_MODE, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 3-0 to based on sidecarConfig
      newVal = (curIntVal & 0xFFF0) | (sidecarConfig.DataTransmissionMode & 0x000F);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_DATA_TRANSMISSION_MODE, intToHexString(newVal));

      //. set number of serial channels
      //. get current value 
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_SERIAL_CHANNELS, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 1-0 to based on sidecarConfig
      newVal = (curIntVal & 0xFFFC) | (sidecarConfig.NumberSerialChannels & 0x0003);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_SERIAL_CHANNELS, intToHexString(newVal));

      //. set data header length
      //. get current value 
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_HEADER_LENGTH, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bits 1-0 to based on sidecarConfig
      newVal = (curIntVal & 0xFFFC) | (sidecarConfig.ScienceBlockHeaderLength & 0x0003);
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_HEADER_LENGTH, intToHexString(newVal));

      //. set data output level
      //. get current val
      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_DATA_OUTPUT_LEVEL, false);
      curIntVal = hexStringToInt(currentVal);
      //. set bit 0 based on  sidecarConfig
      if (sidecarConfig.DataOutputLevelLVDS) {
        newVal = setBit(curIntVal, 0);
      } else {
        newVal = unsetBit(curIntVal, 0);
      }
      //. set new value of register
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_DATA_OUTPUT_LEVEL, intToHexString(newVal));


    }

    private void updateExposureExtents(string jade, string asic) {
      string currentVal;
      int curIntVal;
      ExposureExtents oldExtents = expExtents.Clone();

      currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_EXPOSURE_MODE, false);
      curIntVal = hexStringToInt(currentVal);

      //. get window mode
      if ((curIntVal & 0x2) == 0x2) {
        //. window mode
        currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_STARTX, false);
        expExtents.StartX = hexStringToInt(currentVal);
        currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_STARTY, false);
        expExtents.StartY = hexStringToInt(currentVal);
        currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_ENDX, false);
        expExtents.EndX = hexStringToInt(currentVal);
        currentVal = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_WINDOW_ENDY, false);
        expExtents.EndY = hexStringToInt(currentVal);

        //. todo: should make sure extents are not full frame?
        //. probably not, because if it is in this mode, it shouldn't be full frame.
        //. also, in this mode, the detector will be clocked in subarray
        //. mode (e.g. through one channel), even if the extents are the full frame.
        expExtents.IsSubarray = true;
      } else {
        //. full frame
        expExtents = new ExposureExtents(arrayCols, arrayRows);
      }

      logger.Debug("Exposure Extents: [" + expExtents.StartX + "," + expExtents.StartY + "] to [" + expExtents.EndX + "," + expExtents.EndY + "].");

      //. update auxillary data only if extents have changed
      if (!expExtents.Equals(oldExtents)) {
        reformAuxillaryData(expExtents);
      }
    }

    private void reconfigureSidecar(string jade, string asic) {
      int timer = 0;
      sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP, ASIC_REGISTER_VALUE_RECONFIGURE);

      while (hexStringToInt(sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_RAMP, false)) != hexStringToInt(ASIC_REGISTER_VALUE_STOP_RAMP)) {
        Thread.Sleep(WAITTIME_MS_RECONFIGURE);
        timer++;
        if ((timer * WAITTIME_MS_RECONFIGURE) > (RECONFIGURE_TIMEOUT_IN_SECONDS * 1000 / sidecarConfig.DetectorOutputs)) {
          logger.Debug("reconfigure timer=" + timer + ", time elapsed = " + (timer * WAITTIME_MS_RECONFIGURE) + " ms, timeout = " + (RECONFIGURE_TIMEOUT_IN_SECONDS * 1000 / sidecarConfig.DetectorOutputs) + "ms.");
          throw new SidecarControlException("Sidecar Reconfigure Timeout.  Exceeded " + (RECONFIGURE_TIMEOUT_IN_SECONDS / (float)sidecarConfig.DetectorOutputs) + " seconds.");
        }
      }

    }

    //. returns read time for frame in microsecs.
    public int getReadTime(string jade, string asic) {
      string response;
      int lineClockTime;
      int numLinesPerFrame;
      int h000CVal;
      int h4003Val;
      int h6905Val;
      double jadeFeqFrequency;
      double asicFrequency;
      double clkTime;
      int readTime;

      //. get line clock time
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_LINE_CLOCK_TIME, false);
      lineClockTime = hexStringToInt(response);

      //. get number of lines per frame
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_LINES_PER_FRAME, false);
      numLinesPerFrame = hexStringToInt(response);

      //. get jade frequency
      response = sidecar.ReadJadeRegister(jade, JADE_REGISTER_JADE_FREQUENCY);
      h000CVal = hexStringToInt(response);
      //. freq = 480 Mhz / (count + 4)
      //. count is last 12 bits of h000C value
      //. h00CVal = 0x102c (for 10 usec pixel clocking)
      //.   jadeFeqFrequency = (480 / 48) = 10
      jadeFeqFrequency = (480.0) / (double)((h000CVal & 0x0FFF) + 4);

      if (config.codeVersionHxRG) {

        //. get asic frequency
        response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_ADC_CLOCK, false);
        h6905Val = hexStringToInt(response);
        //. asic freq = jade freq / h6905 value (LSB + MSB shifted right)
        //. h6905 = 0x0505 for mosfire --> 10 (for 10 usec pixel clocking)
        if (h6905Val == 0) {
          asicFrequency = jadeFeqFrequency;
        } else {
          asicFrequency = jadeFeqFrequency / (double)((h6905Val & 0xFF) + (h6905Val >> 8));
        }

        if (asicFrequency == 0) {
          throw new SidecarControlException("Error getting frame read time.  ASIC frequency is 0.");
        }
        //. convert to microsec 
        clkTime = 10.0 / asicFrequency;

      } else {
        //. get asic frequency
        response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_ASIC_FREQUENCY, false);
        h4003Val = hexStringToInt(response);
        //. asic freq = jade freq / h4003 value
        if (h4003Val == 0) {
          asicFrequency = jadeFeqFrequency / 100.0;
        } else {
          asicFrequency = jadeFeqFrequency / (double)h4003Val;
        }

        if (asicFrequency == 0) {
          throw new SidecarControlException("Error getting frame read time.  ASIC frequency is 0.");
        }

        //. convert to microsec 
        clkTime = 1.0 / asicFrequency;
      }

      /* pixel clock time in kHz */
      pixelClockTime = 1000.0 / clkTime;  

      logger.Debug("LineTime = " + lineClockTime + ", RowTime = " + numLinesPerFrame + ", clockTime = " + clkTime + ".");

      readTime = (int)(Math.Floor(lineClockTime * numLinesPerFrame * clkTime));

      logger.Info("Read Time in microseconds = " + readTime);

      //. return read time in microseconds
      return readTime;

    }

    private double getVoltage(string jade, string asic, string register) {
      string response;
      int regIntValue;

      //. get value from register
      response = sidecar.ReadAsicRegister(jade, asic, register, false);
      regIntValue = hexStringToInt(response);

      //. formula (for ricardo @ teledyne) is 
      //. LSB_9:0 * 2 / 1023 + LSB_10 * 1.3
      return ((regIntValue & 0x3FF) * 2.0 / 1023.0) + (((regIntValue & 0x400) >> 10) * 1.3);
    }

    private void setVoltage(string jade, string asic, string register, int voltageInHex) {
      string response;
      int regIntValue;
      int newValue;

      logger.Debug("Setting voltage: register " + register + " to " + intToHexString(voltageInHex));

      //. get value from register
      response = sidecar.ReadAsicRegister(jade, asic, register, false);
      regIntValue = hexStringToInt(response);

      //. strip off bottom 11 bits and replace with inputted voltage
      newValue = (regIntValue & 0xFC00) | voltageInHex;

      sidecar.WriteAsicRegister(jade, asic, register, intToHexString(newValue));
    }

    private bool isAsicAvailable(string jade, string asic) {
      string response;
      response = sidecar.GetAvailableAsics(jade);

      return response.Contains(asic);
    }
    private bool isJadeAvailable(string jade) {
      string response;
      response = sidecar.GetAvailableJades();
      logger.Debug("getAvailableJades response = " + response);
      return response.Contains(jade);
    }

    private bool isDataAvailable(string jade) {
      string waitVal;
      int intVal;

      /* 
       * data is available when value from jade reg 0x000E anded with
       * detector setup flag transitions from detector setup flag to 0
       */
      waitVal = sidecar.ReadJadeRegister(jade, JADE_REGISTER_DATA_AVAILABLE);


      intVal = hexStringToInt(waitVal);
      //     logger.Debug("waiting for data, 0x000E = " + waitVal + ", detectorSetupFlag = " + detectorSetupFlag);
      return ((intVal & detectorSetupFlag) == 0);
    }

    private long getAvailablePixelCount(string jade) {
      string blockVal;
      string blockVal2;
      int blockIntVal;
      int blockIntVal2;
      long fifoCounter;

      /* bits 9-15 of val give part of fifosize
      * if val = binary XXXXXXXYYYYYYYYY
      * we want XXXXXXX0000000000000000 = 0xNN0000
      *				
      * val2 = get jadeReg 0x000F.  
      * val2 = 0xZZZZ

      * we want 0xNNZZZZ
            					
      * fifo counter = (val & 0xFE00) << 7 | (getJadeReg("0x000F"))
      * fifo counter give twice the number of pixels available
      */

      blockVal = sidecar.ReadJadeRegister(jade, JADE_REGISTER_DATA_AVAILABLE);
      //logger.Debug("waiting for data, 0x000E = " + blockVal);
      Thread.Sleep(1);

      blockIntVal = hexStringToInt(blockVal);


      blockVal2 = sidecar.ReadJadeRegister(jade, JADE_REGISTER_GET_AVAILABLE_PIXEL_COUNT);
      //logger.Debug("waiting for data, 0x000F = " + blockVal2);
      Thread.Sleep(1);

      blockIntVal2 = hexStringToInt(blockVal2);

      fifoCounter = ((blockIntVal & 0xFE00) << 7) | (blockIntVal2);

      logger.Debug("fifo counter = " + fifoCounter);

      return fifoCounter * 2;
    }

    private void startRamp(string jade, string asic) {
      string response;

      response = sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP, ASIC_REGISTER_VALUE_START_RAMP);
      if (!response.Contains(SIDECAR_SUCCESS_WRITE_ASIC_REGISTER)) {
        throw new SidecarControlException("Error starting ramp.");
      }
    }
    private void abortRamp(string jade, string asic) {
      string response;

      response = sidecar.WriteAsicRegister(jade, asic, ASIC_REGISTER_RAMP, ASIC_REGISTER_VALUE_STOP_RAMP);
      if (!response.Contains(SIDECAR_SUCCESS_WRITE_ASIC_REGISTER)) {
        throw new SidecarControlException("Error stopping ramp.");
      }
    }

    public int abortExposure(int abortLevel, bool saveImage) {
      //. just set doAbort flag.  the coaddition thread will execute abort in Sidecar
      valueListener.setValue(VALUE_NAME_ABORT_IN_PROGRESS, "1");
      doAbort = abortLevel;

      saveImageOnAbort = saveImage;

      return 0;
    }

    public void setExposureExtents(string jade, string asic, ExposureExtents extents) {
      logger.Info("In SidecarControl.setExposureExtents.  Jade = " + jade
                 + ", startx =" + extents.StartX
              + ", starty =" + extents.StartY
              + ", endx =" + extents.EndX
              + ", endy =" + extents.EndY
                + ".");

      //. set subarray (HxRG)
      if (config.CodeVersionHxRG) {
        if (!extents.Equals(expExtents)) {
          setExposureSubarrayLimits(jade, asic, extents);
          reconfigureSidecar(jade, asic);

          expExtents = extents;
        }
      }

    }

    private long getEstimatedObservationTime(SidecarRamp ramp, ExposureConfiguration exposureConfig) {
      return ((ramp.Resets + ramp.Groups * (ramp.Reads + ramp.Drops) - ramp.Drops) * sidecarConfig.ReadTimeInMicros + PROCESSING_OVERHEAD_MICROS) * exposureConfig.Coadds + (COADD_OVERHEAD_MICROS * (exposureConfig.Coadds - 1));
    }

    private string getSidecarMCDVersion(string jade, string asic) {
      string response;
      int regIntValue;
      int major, minor;

      //. get value from register
      response = sidecar.ReadAsicRegister(jade, asic, ASIC_REGISTER_HXRG_CODE_VERSION, false);
      regIntValue = hexStringToInt(response);

      //. for version 1.3, value is 0x13
      //. for version 2.2, value is 0x0202
      //. for version 2.3, value is 0x0203
      if ((regIntValue & 0xFF00) == 0) {

        major = (regIntValue & 0xF0) >> 4;
        minor = regIntValue & 0x0F;
      } else {
        major = (regIntValue & 0xFF00) >> 8;
        minor = regIntValue & 0x00FF;
      }

      return major + "." + minor;
    }

    public int takeExposure(string jade, string asic, ExposureConfiguration exposureConfig, string fitsFile) {

      int readTimeInMicros;
      int numpix;
      FitsWriter fits = new FitsWriter();

      logger.Info("In SidecarControl.takeExposure.  Jade = " + jade
              + ", Asic = " + asic
              + ", resets = " + exposureConfig.Resets
              + ", itimeInMicros = " + exposureConfig.ItimeInMicros
              + ", coadds = " + exposureConfig.Coadds
              + ", sampMode = " + exposureConfig.SampMode
              + ", reads = " + exposureConfig.Reads
              + ", groups = " + exposureConfig.Groups
              + ".");

      if (isExposing) {
        setExposureStatus("Exposure cannot be executed.", "Exposure already in progress.");
        throw new SidecarControlException(ExposureStatusDetail);
      }
      isExposing = true;
      doAbort = 0;
      updateExposureExtents(jade, asic);
      exposureConfig.Extents = expExtents;

      //. reset exposure status
      initExposureStatus();
      valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "1");
      valueListener.setValue(VALUE_NAME_IMAGE_DONE, "0");

      setExposureStatus("Validating exposure settings", "Checking disk space");
      logger.Debug("Checking disk space for: <" + fitsFile + ">.");
      try {
        DirectoryInfo dirInfo = new DirectoryInfo(fitsFile);
        int remainingSpace = getRemainingDiskSpace(dirInfo.Root);
        setExposureStatus("Validating exposure settings", "Remaining disk space: " + remainingSpace + " MB");
        if (remainingSpace < config.DiskSpaceLeftWarningMB) {
          if (remainingSpace < 0) {
            setExposureStatus("Exposure cancelled", "Cannot find remaining disk space for " + dirInfo.Root);
            throw new SidecarControlException(ExposureStatusDetail);
          } else {
            setExposureStatus("Exposure cancelled", "Remaining disk space <" + remainingSpace + " MB> is less than required minimum <" + config.DiskSpaceLeftWarningMB + " MB>.");
            throw new SidecarControlException(ExposureStatusDetail);
          }
        }
      } catch (SidecarControlException ex) {
        isExposing = false;
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        throw ex;
      } catch (Exception ex) {
        isExposing = false;
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        setExposureStatus("Exposure cancelled", "Cannot determine avaialble disk space for " + fitsFile + ": " + ex.Message);
        throw new SidecarControlException(ExposureStatusDetail);
      }

      //. make copies of configuration for this exposure
      SidecarControlConfiguration exposureSidecarControlConfiguration = config.Clone();
      fits.ControlConfiguration = exposureSidecarControlConfiguration;

      //. validate fitsfile
      setExposureStatus("Validating exposure settings", "Validating fits filename");
      logger.Debug("Validating fits filename: <" + fitsFile + ">.");
      //. initFits throws exceptions from FileStream contructor, including IOException, DirectoryNotFoundException, and SecurityException
      try {
        //. the following will validate the temporary "writing" filename, 
        //. which is the filename  plus the extension specified in control config
        fits.initFits(new FitsInfo(fitsFile, arrayRows, arrayCols));
        //. validate final filename as well.
        int fileCheck;
        if ((fileCheck = validateFilename(fitsFile)) < 0) {
          switch (fileCheck) {
            case -1: 
              throw new InvalidExposureConfigurationException("Invalid filename.  File exists and not writeable.");
            case -2:
              throw new InvalidExposureConfigurationException("Invalid filename.  Directory does not exist.");
            case -3:
              throw new InvalidExposureConfigurationException("Invalid filename.  Directory is not writeable.");
          }
        }
      } catch (Exception ex) {
        isExposing = false;
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        throw new InvalidExposureConfigurationException(ex.Message);
      }
      openFitsWriters.Add(fits.currentFitsFilename, fits);

      //. make sure asic and jade are available
      if (isJadeAvailable(jade)) {
        JadeName = jade;
      } else {
        setExposureStatus("Exposure cancelled", "Jade <" + jade + "> is not available");
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        throw new SidecarControlException(ExposureStatusDetail);
      }

      if (isAsicAvailable(jade, asic)) {
        AsicName = asic;
      } else {
        setExposureStatus("Exposure cancelled", "Asic <" + asic + "> is not available");
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        throw new SidecarControlException(ExposureStatusDetail);
      }

      //. get read time.  this cannot be 0 (or negative) in a valid configuration
      if ((readTimeInMicros = getReadTime(jade, asic)) <= 0) {
        setExposureStatus(null, "Error getting read time.  Read time is non-positive.  Check to make sure hardware has been initialized and timing code has been downloaded.");
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        throw new SidecarControlException(ExposureStatusDetail);
      } else {
        logger.Debug("readTimeInMicros = " + readTimeInMicros);
      }

      //. create ramp
      try {
        setExposureStatus(null, "Validating exposure settings");
        SidecarRamp ramp = createRamp(exposureConfig, readTimeInMicros);
        sidecarConfig.ReadTimeInMicros = readTimeInMicros;
        sidecarConfig.PixelClockingSpeedkHz = pixelClockTime;

        int pixelsPerGroup;
        bool pixelCountOdd;
        numpix = exposureRows * exposureCols;
        pixelsPerGroup = numpix * ramp.Reads;
        pixelCountOdd = ((pixelsPerGroup % 2) != 0);
        logger.Debug("total number of pixels is " + numpix + ".  Pixels per groups is " + pixelsPerGroup + ". This is a odd number? " + pixelCountOdd);
        if (pixelCountOdd)
        {
          throw new InvalidExposureConfigurationException("Invalid exposure settings.  Number of pixels per group (rows * columns * reads) must be even.");
        }

        //. calculate expected read time
        valueListener.setValue(VALUE_NAME_ESTIMATED_OBSERVATION_TIME, getEstimatedObservationTime(ramp, exposureConfig).ToString());

        //. execute ramp
        setExposureStatus("Preparing to start exposure", "Setting ramp in SIDECAR");
        executeRamp(jade, asic, ramp);


        //. make sure voltages and gain settings are up to date
        getGainSetting(jade, asic);

        //. make sure Vrefmain is set correctly, if autoset is on
        if (config.AutosetVrefMain) {
          setVoltage(jade, asic, ASIC_REGISTER_VREFMAIN, SidecarConfiguration.vRefMainVoltagesForGain[sidecarConfig.GainSetting]);
        }

        getVoltages(jade, asic);
        getSidecarConfigurationRegisters(jade, asic);
        sidecarConfig.HXRG_code_version = getSidecarMCDVersion(jade, asic);

        exposureConfig.CoaddsDone = 0;
        valueListener.setValue(VALUE_NAME_COADDS_DONE, "0");

        //. create new zeroed out dataimage, errimage, qualimage
        dataimage = new float[numpix];
        errimage = new float[numpix];
        qualimage = new byte[numpix];

        int ii = 0;
        while (ii < numpix) {
          if (badpix[ii] == 0) {
            qualimage[ii] |= QUAL_BADPIX_MASK;
          }
          ii++;
        }
        SidecarConfiguration exposureSidecarConfiguration = sidecarConfig.Clone();
        fits.SidecarConfiguration = exposureSidecarConfiguration;

        CoadditionParameters param = new CoadditionParameters(exposureConfig, ramp, fits, dataimage, errimage, qualimage, exposureCols, exposureRows, fitsFile, linearize);

        Thread expThread = new Thread(this.coaddition);
        expThread.Start(param);

      } catch (InvalidExposureConfigurationException iecEx) {
        //. on error, promote exception
        setExposureStatus("Exposure cancelled", iecEx.Message);
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        throw iecEx;
      }

      return 0;
    }

    private class CoadditionParameters {
      public ExposureConfiguration ExpConfig { get; set; }
      public SidecarRamp Ramp { get; set; }
      public FitsWriter Fits { get; set; }
      public float[] Dataimage { get; set; }     /* final data array                 */
      public float[] Errimage { get; set; }     /* final noise array                */
      public byte[] Qualimage { get; set; }     /* final quality array              */
      public int ExposureRows { get; set; }
      public int ExposureCols { get; set; }
      public string FitsFilename { get; set; }
      public int ReadCount { get; set; }
      public DateTime ExposureStartTime { get; set; }
      public DateTime ExposureEndTime { get; set; }
      public bool Linearize { get; set; }
      public CoadditionParameters(ExposureConfiguration expConfig, SidecarRamp ramp, FitsWriter fits, float[] dataimage, float[] errimage, byte[] qualimage, int exposureCols, int exposureRows, string fitsFilename, bool linearize) {
        ExpConfig = expConfig;
        Ramp = ramp;
        Fits = fits;
        Dataimage = dataimage;
        Errimage = errimage;
        Qualimage = qualimage;
        ExposureCols = exposureCols;
        ExposureRows = exposureRows;
        FitsFilename = fitsFilename;
        ReadCount = 0;
        Linearize = linearize;
      }
    }

    public void coaddition(object arg) {

      //. info needed: 
      //. image size;
      long pixelCount;
      long lastPixelCount;
      int frameCount;
      int current_frame = 0;
      int current_group = 0;
      int currentBlock;
      float signal;
      bool alreadyAborted;

      int totalNumberOfPixels;
      int blockSize;
      int numBlocks;
      int secondDrops = 0;
      int ignoredReads;

      ushort weight;
      float delta;
      float adjustedSignal;
      float linearizedPixel;
      //float factor;

      string response;
      int ii, jj;

      int readTimeoutCounter = 0;
      int abortRampTimoutCounter = 0;

      bool doHor;
      bool doVert;
      int localDoAbort;

      CoadditionParameters param;
      if (!(arg is CoadditionParameters)) {
        //. this won't happen (by coding)
        isExposing = false;
        return; 
      } else {
        param = (CoadditionParameters)arg;
      }

      ExposureConfiguration expConfig = param.ExpConfig;
      SidecarRamp ramp = param.Ramp;
      FitsWriter fits = param.Fits;
      float[] dataim = param.Dataimage;
      float[] errim = param.Errimage;
      byte[] qualim = param.Qualimage;
      int expRows = param.ExposureRows;
      int expCols = param.ExposureCols;
      SidecarConfiguration sidecarConfiguration = fits.SidecarConfiguration;
      SidecarControlConfiguration controlConfiguration = fits.ControlConfiguration;
      String fitsFilename = param.FitsFilename;
      String fitsFileroot;
      int coadds_done = expConfig.CoaddsDone;
      bool doLinearize = param.Linearize;

      //. construct filename
      int dotIndex = fitsFilename.LastIndexOf(".");
      if (dotIndex == -1) {
        fitsFileroot = fitsFilename;
      } else {
        fitsFileroot = fitsFilename.Substring(0, dotIndex);
      }

      totalNumberOfPixels = expRows * expCols;
      //. get data from sidecar in one chunk
      numBlocks = 1;
      blockSize = totalNumberOfPixels / numBlocks;
      blockArray = new ushort[blockSize];
      //. array for when 

      //. only do reference pixel subtraction if full frame
      doVert = controlConfiguration.DoVerticalRPS && (expCols == arrayCols) && (expRows == arrayRows);
      doHor = controlConfiguration.DoHorizontalRPS && (expCols == arrayCols) && (expRows == arrayRows);

      logger.Debug("Vertical RPS is " + (doVert ? "enabled. Type=" + controlConfiguration.getVerticalReferencePixelSubtractionType() : "disabled."));
      logger.Debug("Horizontal RPS is " + (doHor ? "enabled. Type=" + controlConfiguration.getHorizontalReferencePixelSubtractionType() : "disabled."));

      //. make sure there is a ramp
      if (ramp == null) {
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        return;
      }
      //. make sure there is a fits filename
      if (fitsFilename == null) {
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        return;
      }

      //. drops will be negative if ignored reads are used.  see createRamp()
      if (ramp.Drops < 0) {
        secondDrops = Math.Abs(ramp.Drops);
      }

      //. don't do ignored reads for single sampling
      if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_SINGLE) {
        ignoredReads = 0;
      } else {
        ignoredReads = controlConfiguration.IgnoredInitialReads;
      }

      //. zero out sum arrays
      Array.Clear(firstread, 0, firstread.Length);
      Array.Clear(secondread, 0, secondread.Length);

      try {

        setExposureStatus("Preparing to start exposure", "Preparing FIFO");
        logger.Info("Prepping fifo for exposure");
        prepFifoForExposure(JadeName);

      } catch (SidecarControlException scEx) {
        Console.Write(scEx.StackTrace);
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        return;
      }

      try {
        logger.Info("Starting exposure");
        setExposureStatus("Starting exposure", null);

        //. make sure SIDECAR is idling
        //. get value from register
        response = sidecar.ReadAsicRegister(JadeName, AsicName, ASIC_REGISTER_RAMP, false);
        logger.Debug("Ramp register = " + response);
        if (response.CompareTo(ASIC_REGISTER_VALUE_STOP_RAMP) != 0) {
          abortRamp(JadeName, AsicName);
          do {
            if (abortRampTimoutCounter > MAX_REPEATED_ABORT_RAMP_ATTEMPTS) {
              /* abort exposure */
              logger.Error("Timeout waiting for start exposure register to reset. " + abortRampTimoutCounter + " reads exceeds maximum value " + MAX_REPEATED_ABORT_RAMP_ATTEMPTS + ".");
              setExposureStatus("Timeout waiting for start exposure register to reset. Aborting exposure.", null);
              valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
              openFitsWriters.Remove(fits.currentFitsFilename);
              fits.closeFits(true);
              isExposing = false;
              return;
            }
            response = sidecar.ReadAsicRegister(JadeName, AsicName, ASIC_REGISTER_RAMP, false);
            logger.Debug("Ramp register = " + response);
            readTimeoutCounter++;
            Thread.Sleep(WAITTIME_MS_ABORT_RAMP);
          } while (response.CompareTo(ASIC_REGISTER_VALUE_STOP_RAMP) != 0);
        }
        
        startRamp(JadeName, AsicName);

        //. timestamp start of exposure
        param.ExposureStartTime = DateTime.Now;
        param.ExposureEndTime = param.ExposureStartTime;
      } catch (SidecarControlException scEx) {
        logger.Error(scEx.StackTrace);
        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits(true);
        isExposing = false;
        return;
      }
      valueListener.setValue(VALUE_NAME_EXPOSURE_IN_PROGRESS, "1");

      setExposureStatus("Exposing", null);
      valueListener.setValue(VALUE_NAME_EXPOSING, "1");

      try {

        alreadyAborted = false;

        /* loop over coadds */
        while (coadds_done < expConfig.Coadds) {
          /* loop over groups */
          frameCount = 0;
          valueListener.setValue(VALUE_NAME_READS_DONE, "0");
          for (current_group = 0; current_group < ramp.Groups; current_group++)
          {

            while (!isDataAvailable(JadeName)) {
              /* check for abort */
              if (doAbort == 1) {
                break;
              }
              //. sleep a bit so polls aren't to rapid
              Thread.Sleep(WAITTIME_MS_READ_IS_DATA_AVAILABLE);
            }

            /* if immediate abort, break out of processing loop 
                (ignore most recent data) */

            if (doAbort == 1) {
              logger.Info("Abort " + doAbort + " received.  Aborting readout of current read.");
              abortRamp(JadeName, AsicName);
              alreadyAborted = true;
              break;
            }

            current_frame = 0;
            readTimeoutCounter = 0;
            lastPixelCount = 0;
            while (current_frame < ramp.Reads) {

              currentBlock = 0;
              while (currentBlock < numBlocks) {

                /* check for abort */
                if (doAbort == 1) {
                  break;
                }

                pixelCount = getAvailablePixelCount(JadeName);
                //. TODO warn if overflow if pixelCount = 8388607
                //. also could warn by tracking rate of increase of pixel count
                //. to predict when it's going to happen, and the abort ramp
                if (pixelCount == lastPixelCount) {
                  readTimeoutCounter++;
                  if (readTimeoutCounter > MAX_REPEATED_PIXEL_COUNT_READS) {
                    /* stop exposure */
                    logger.Error("Timeout reading frame.  Same number of pixels available " + pixelCount + " for " + readTimeoutCounter + " reads exceeds maximum value " + MAX_REPEATED_PIXEL_COUNT_READS + ".");
                    setExposureStatus("Read timeout. Aborting exposure.", null);
                    doAbort = 1;
                    saveImageOnAbort = true;
                  }
                } else {
                  readTimeoutCounter = 0;
                }
                lastPixelCount = pixelCount;

                if (pixelCount >= blockSize) {
                  for (jj = 0; jj < ((int)Math.Floor((float)(pixelCount) / (float)(blockSize))); jj++) {

                    logger.Info("Reading science data: block size = " + blockSize + ", currentBlock = " + currentBlock);
                    /* can read by blocks (must be integer factor of size of array)
                     * not too small or too much overhead */
                    valueListener.setValue(VALUE_NAME_READOUT_IN_PROGRESS, "1");
                    response = sidecar.ReadScienceData(JadeName, blockSize, ref blockArray);
                    logger.Debug("readScienceData response=" + response);

                    Array.Copy(blockArray, 0, readImage, blockSize * currentBlock, blockSize);

                    currentBlock++;
                    if (currentBlock == numBlocks) {
                      break;
                    }
                  }
                }
              } /* endwhile (currentBlock < numBlocks) */

              /* if immediate abort, break out of processing loop 
              (ignore most recent data) */
              if (doAbort == 1) {
                logger.Info("Abort " + doAbort + " received.  Aborting readout of current read (" + param.ReadCount + ").");
                if (!alreadyAborted) {
                  abortRamp(JadeName, AsicName);
                  alreadyAborted = true;
                }
                break;
              }

              param.ReadCount++;

              DateTime lastReadTime = DateTime.Now;

              logger.Info(" got frame " + (current_frame + 1) + " of " + ramp.Reads + " in group " + (current_group + 1) + " of " +
    ramp.Groups + " groups. (" + (current_group * ramp.Reads + current_frame + 1) + " of " + ramp.Reads * ramp.Groups + " total reads)");
              logger.Debug("frameCount = " + frameCount);

              if (controlConfiguration.WriteRawFrames) {
                string filename = fitsFileroot + "_" + String.Format("{0:000}", param.ReadCount) + ".fits";
                logger.Info("Writing raw frame to file <" + filename + ">.");
                try {
                  using (MemoryFailPoint rawFrameFailPoint = new MemoryFailPoint(8)) {   //. throws InsufficientMemoryException if not enough memory
                    RawFrameFitsWriter rawFits = new RawFrameFitsWriter(new FitsInfo(filename, expRows, expCols), readImage, config.FitsWritingInProgressExtension);
                    rawFrameWriterQ.enqueueFitsWriter(rawFits);
                  }
                } catch (InsufficientMemoryException ex) {
                  //                Console.Write(ex);
                  logger.Warn("Error writing raw frame: <" + filename + ">: " + ex.Message);
                }
              }


              logger.Debug("coaddition: got frame.");

              /*
               * go through pixel by pixel of new frame, assembling the 
               *  required information for linfit at each pixel.
               */

              logger.Debug("coaddition: working on frame");
              /* for the first group:
               *   for the first frame, fill qualim with bad pixel map
               *   stack reads in firstread stack
               * 
               * for rest of groups:
               *   stack in lastread stack
               */
              ii = 0;
              if (current_group == 0) {
                if (current_frame >= ignoredReads) {
                  param.ExposureEndTime = lastReadTime;
                  valueListener.setValue(VALUE_NAME_CURRENT_READ, "" + (frameCount + 1));
                  logger.Debug("Adding read " + current_frame + ", frameCount = " + frameCount);
                  while (ii < totalNumberOfPixels) {
                    firstread[ii] += (float)readImage[ii];
                    if (readImage[ii] > sidecarConfiguration.SaturationLevel) {
                      qualim[ii] |= QUAL_RAW_SATURATED_MASK;
                    }
                    ii++;
                  }
                  frameCount++;
                } else {
                  logger.Debug("Discarding read " + current_frame + ".");
                }
              } else {
                if ((current_frame >= (ignoredReads - secondDrops)) &&
                (current_frame < ramp.Reads - secondDrops)) {
                  param.ExposureEndTime = lastReadTime;
                  logger.Debug("Adding read " + current_frame + ", frameCount = " + frameCount);
                  valueListener.setValue(VALUE_NAME_CURRENT_READ, "" + (frameCount + 1));
                  while (ii < totalNumberOfPixels) {
                    secondread[ii] += (float)readImage[ii];
                    if (readImage[ii] > sidecarConfiguration.SaturationLevel) {
                      qualim[ii] |= QUAL_RAW_SATURATED_MASK;
                    }
                    ii++;
                  }
                  frameCount++;
                } else {
                  logger.Debug("Discarding read " + current_frame + ".");
                }
              }

              logger.Debug("coaddition: frame accum done");

              /* prepare for next frame */

              /* increment current_frame */
              current_frame++;
              //.local_exp->framenum++;

              logger.Debug("coaddition: done with frame (group = " + current_group + ").");
              expConfig.ReadsDone = frameCount;
              valueListener.setValue(VALUE_NAME_READS_DONE, "" + frameCount);
              valueListener.setValue(VALUE_NAME_PROGRESS, "" + (coadds_done * expConfig.Reads * ramp.Groups + frameCount) / (1.0 * expConfig.Coadds * expConfig.Reads * ramp.Groups));
              setExposureStatus(null, "Received read " + (frameCount) + "/" + ((ramp.Reads - ignoredReads) * ramp.Groups) + " of coadd " + (coadds_done + 1) + "/" + expConfig.Coadds);

              /* check for abort */
              //. todo: move to above so that doesn't abort after ignored read?
              if (doAbort == 2) {
                logger.Info("Abort " + doAbort + " received.  Aborting readout now.  read " + frameCount + " finished.");

                abortRamp(JadeName, AsicName);
                alreadyAborted = true;
                /* need to better handle if there is an error here */

                break;  //. break out of group loop

              } else if (doAbort == 1) {

                /* if haven't already aborted, abort now */
                if (!alreadyAborted) {
                  abortRamp(JadeName, AsicName);
                  alreadyAborted = true;
                  /* need to better handle if there is an error here */
                  break;
                }
              }
            } //. end loop on ramp reads
            logger.Debug("coaddition: obtained all reads for group = " + current_group + ").");

            valueListener.setValue(VALUE_NAME_GROUPS_DONE, "" + (current_group + 1));
            expConfig.GroupsDone = current_group + 1;

            ii = 0;
            if (current_group == 0) {
              while (ii < totalNumberOfPixels) {
                firstread[ii] /= (float)expConfig.Reads;
                utr_maxdel[ii] = float.NegativeInfinity;
                utr_maxw[ii] = ushort.MinValue;
                utr_mindel[ii] = float.PositiveInfinity;
                utr_minw[ii] = ushort.MaxValue;

                utr_wdeltas[ii] = 0.0F;
                utr_weights[ii] = 0;
                utr_lastread[ii] = firstread[ii];
                ii++;
              }
            } else {
              if (doLinearize) {
                logger.Debug("Performing linearization");
                while (ii < totalNumberOfPixels) {
                  secondread[ii] /= (float)expConfig.Reads;

                  adjustedSignal = secondread[ii] - firstread[ii];
                  /* if pixel > saturation level, mark as bad */
                  if (adjustedSignal > sidecarConfiguration.SaturationLevel) {
                    qualim[ii] |= QUAL_DIFFERENCE_SATURATED_MASK;
                  } else {
                    //. linearize this read
                    //. perform on difference between last read and first read, 
                    //. but rescale back to absolute value by adding back first read
                    //. after adjustment is made

                    //. note about math:  
                    //. linearize funciton:
                    //.  corrected pixel = p = a_n * x^n + a_n_1 * x^(n-1) + ... + a_1 * x + a_0
                    //.                      = (((a_n * x + a_n_1) * x + a_n_2) * x) + ... + a_1) * x + a_0
                    //.  which can be calcuated using a for loop as follows:
                    //.  p=0;
                    //.  for (ii=n-1, ii>0, ii--) {
                    //.    p = (p + a[n]) * x;
                    //.  }
                    //.  p += a[0];
                    linearizedPixel = 0.0F;
                    /*
                    factor = 1.0F;
                    for (int nn = 0; nn < linearization[ii].GetLength(0); nn++) {
                      linearizedPixel += linearization[ii][nn] * factor;
                      factor *= adjustedSignal;
                    }
                     */
                    for (int nn = linearization[ii].GetLength(0) - 1; nn > 0; nn--) {
                      linearizedPixel = (linearizedPixel + linearization[ii][nn]) * adjustedSignal;
                    }
                    secondread[ii] = linearizedPixel + firstread[ii] + linearization[ii][0];
                  }
                  //. now subtract read just before this one
                  utr_b[ii] = secondread[ii] - utr_lastread[ii];
                  utr_lastread[ii] = secondread[ii];
                  ii++;
                }
              } else {
                while (ii < totalNumberOfPixels) {
                  secondread[ii] /= (float)expConfig.Reads;

                  /* if pixel > saturation level, mark as bad */
                  if (secondread[ii] - firstread[ii] > sidecarConfiguration.SaturationLevel) {
                    qualim[ii] |= QUAL_DIFFERENCE_SATURATED_MASK;
                  }
                  //. now subtract read just before this one
                  utr_b[ii] = secondread[ii] - utr_lastread[ii];
                  utr_lastread[ii] = secondread[ii];
                  ii++;
                }
              }
              //. reference pixel subtract
              doReferencePixelSubtraction(doVert, doHor, ref utr_b);

              ii = 0;
              while (ii < totalNumberOfPixels) {


                if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_UTR) {
                  //. done with secondread, reset
                  secondread[ii] = 0.0F;
                }
                //. TODO skip bad pixels?
                weight = (ushort)(current_group * (expConfig.Groups - current_group));
                delta = utr_b[ii];

                if (delta > utr_maxdel[ii]) {
                  utr_maxdel[ii] = delta;
                  utr_maxw[ii] = weight;
                }

                if (utr_b[ii] < utr_mindel[ii]) {
                  utr_mindel[ii] = delta;
                  utr_minw[ii] = weight;
                }

                utr_wdeltas[ii] += weight * delta;
                utr_weights[ii] += weight;

                ii++;
              }

              //. optionally write to disk
              if (controlConfiguration.WriteRawGroupFrames) {
                //. only write these if groups is more than 2, otherwise final file is redundant
                if (ramp.Groups > 2) {
                  if (param.ExposureStartTime != null) {
                    expConfig.ExposureStartDate = param.ExposureStartTime.ToUniversalTime().ToString("yyyy-MM-dd");
                    expConfig.ExposureStartTime = param.ExposureStartTime.ToUniversalTime().ToString("HH:mm:ss.fff");
                  }
                  if (param.ExposureEndTime != null) {
                    expConfig.ExposureEndTime = param.ExposureEndTime.ToUniversalTime().ToString("HH:mm:ss.fff");
                  }
                  //. construct filename
                  string filename = fitsFileroot + "_g" + String.Format("{0:000}", current_group + ramp.Groups * coadds_done) + ".fits";
                  logger.Info("Writing raw group to file <" + filename + ">.");
                  //. write difference frame will full header (snapshot of this point, so make copies)
                  try {
                    using (MemoryFailPoint rawGroupFrameFailPoint = new MemoryFailPoint(16)) {   //. throws InsufficientMemoryException if not enough memory
                      RawFrameFitsWriter rawFits = new RawFrameFitsWriter(new FitsInfo(filename, expRows, expCols), utr_b, fits, expConfig.Clone(), ramp.Clone(), controlConfiguration.DetectorRotation);
                      rawFrameWriterQ.enqueueFitsWriter(rawFits);
                    }
                  } catch (InsufficientMemoryException ex) {
                    //               Console.Write(ex);
                    logger.Warn("Insufficient memory to write raw group <" + filename + ">: " + ex.Message);
                  }
                }
              }
            }


            //. this was the final group.
            //. all supported sampling modes end with
            //. reads of final group.   stop ramp to stop drop frames.
            if (current_group == ramp.Groups - 1) {

              /* kill ramp  (this should also be used for aborts) */
              //.abortRamp(JadeName, AsicName);
              /* need to better handle if there is an error here */

            } else {

              if ((doAbort == 1) || (doAbort == 2) || (doAbort == 3)) {
                logger.Info("Abort " + doAbort + " received.  Aborting readout now.  group " + current_group + " finished.");

                /* if haven't already aborted, abort now */
                if (!alreadyAborted) {
                  abortRamp(JadeName, AsicName);
                  alreadyAborted = true;
                  /* need to better handle if there is an error here */
                }
                break;
              }
            }
          } //. end loop on groups


          logger.Debug("coaddition: about to process all reads");

          setExposureStatus("Processing reads", null);

          /* process frames */
          ii = 0;

          //. benchmark 
          DateTime startTimeProcessing = DateTime.Now;


          //. TODO qualiamge for single, cds, mcds
          if (expConfig.SampMode == ExposureConfiguration.SAMPLING_MODE_SINGLE) {
            logger.Debug("coaddition: solving Single");
            setExposureStatus(null, "Solving Single");
            ii = 0;
            /* add to coadded frames
             *
             * TODO: error:
             * variance = ((read2-read1)*gain + readvar^2)
             * readvar in ADU, gain in ADU/e-
             */
            if (ramp.Groups == 1) {
              while (ii < totalNumberOfPixels) {

                dataim[ii] += firstread[ii] / expConfig.Coadds;
                errim[ii] += (firstread[ii] / (float)SidecarConfiguration.systemGainFactors[sidecarConfiguration.GainSetting] + readvar[ii] * readvar[ii]) / expConfig.Coadds;
                ii++;
              }
            } else {
              while (ii < totalNumberOfPixels) {

                dataim[ii] += secondread[ii] / expConfig.Coadds;
                errim[ii] += (secondread[ii] / (float)SidecarConfiguration.systemGainFactors[sidecarConfiguration.GainSetting] + readvar[ii] * readvar[ii]) / expConfig.Coadds;
                ii++;
              }
            }
          } else {
            /* define constant utility values */
            logger.Debug("coaddition: solving UTR");
            setExposureStatus(null, "Solving UTR");

            /* solve for desired values */
            ii = 0;
            while (ii < totalNumberOfPixels) {

              /* check to make sure we have at least 4 reads, the minimum
               * number of reads to check for glitches */
              if (expConfig.Groups > 3) {

                /* if a delta is extreme enough, take it out */
                if (utr_mindel[ii] < UTR_MINIMUM_VALID_DELTA) {
                  qualim[ii] |= QUAL_MIN_DELTA_REMOVED_MASK;
                  utr_wdeltas[ii] -= utr_mindel[ii] * utr_minw[ii];
                  utr_weights[ii] -= utr_minw[ii];
                }

                if (utr_maxdel[ii] > UTR_MAXIMUM_VALID_DELTA) {
                  qualim[ii] |= QUAL_MAX_DELTA_REMOVED_MASK;
                  utr_wdeltas[ii] -= utr_maxdel[ii] * utr_maxw[ii];
                  utr_weights[ii] -= utr_maxw[ii];
                }
              }

              if (utr_weights[ii] == 0) {
                /* if weights are all zero, mark as badpix */
                //. TODO: i don't think this can happen any more, 
                //. since we don't skip bad pixels, but we should check.
                qualim[ii] &= NOT_QUAL_BADPIX_MASK;
                utr_b[ii] = 0.0f;
              } else {

                /* add to coadded frames
                 * must divide by number of coadds since
                 * data (slope) and noise are averages
                 */

                //. the following below gives counts per second per coadd.
                //  utr_b[ii] = utr_wdeltas[ii] / (float)(utr_weights[ii] * expConfig.GroupItime);
                //}
                //dataim[ii] += utr_b[ii] / ((float)(expConfig.Coadds));

                //. for mosfire, ian prefers counts
                utr_b[ii] = utr_wdeltas[ii] / (float)(utr_weights[ii]);
              }
              dataim[ii] += utr_b[ii] * (ramp.Groups - 1) / ((float)(expConfig.Coadds));

              /*
               * write variance per coadd in errim
               * variance = (readvar/reads + signal*gain)/(rditime*reads)
               * readvar in ADU^2, gain in ADU/e-
               *
               */
              //. multiply by itime to get counts

              //. TODO investigate
              signal = (float)(utr_lastread[ii]) - firstread[ii];
              if (signal < 0) {
                signal = 0.0f;
              }

              errim[ii] += ((readvar[ii] / expConfig.Groups + signal / (float)SidecarConfiguration.systemGainFactors[sidecarConfiguration.GainSetting]) /
                (expConfig.GroupItime * expConfig.Groups)) / (float)(expConfig.Coadds);

              /* this was old for cds/mcds 
                errim[ii] += (utr_b[ii] / (float)SidecarConfiguration.systemGainFactors[sidecarConfiguration.GainSetting] + readvar[ii] * readvar[ii]) / expConfig.Coadds;
              */
              ii++;
            }  /* endwhile */
          }  /* endelse (utr) */

          /* end benchmark.  report */
          DateTime endTimeProcessing = DateTime.Now;
          TimeSpan spanProcessing = endTimeProcessing - startTimeProcessing;
          logger.Debug("Processing time elapsed = " + spanProcessing.ToString());

          /* increment coadd counter */
          coadds_done++;
          expConfig.CoaddsDone = coadds_done;
          valueListener.setValue(VALUE_NAME_COADDS_DONE, "" + coadds_done);
          valueListener.setValue(VALUE_NAME_PROGRESS, "" + coadds_done / (float)(expConfig.Coadds));
          /* if no abort, go get the next coadd */
          if (doAbort == 0) {
            //. zero out sum arrays
            Array.Clear(firstread, 0, firstread.Length);
            Array.Clear(secondread, 0, secondread.Length);
          } else {
            logger.Debug("Abort " + doAbort + " received.  Aborting readout now.  Coadd " + coadds_done + " finished");
            break;
          }
        }
        //. make a copy before next exposure can be started
        localDoAbort = doAbort;

        isExposing = false;
        valueListener.setValue(VALUE_NAME_EXPOSING, "0");
        valueListener.setValue(VALUE_NAME_READOUT_IN_PROGRESS, "0");

        string statusPrefix = "";

        if (localDoAbort > 0) {
          if (readTimeoutCounter > MAX_REPEATED_PIXEL_COUNT_READS) {
            statusPrefix = "Exposure Aborted (Read Timeout): ";
          } else {
            statusPrefix = "Exposure Aborted: ";
          }
        }
        if ((localDoAbort == 0) || ((localDoAbort > 0) && saveImageOnAbort)) {

          //. if aborted before all coadds are done, fix coadd averaging
          if (coadds_done != expConfig.Coadds) {
            ii = 0;
            while (ii < totalNumberOfPixels) {
              dataim[ii] *= (expConfig.Coadds / (float)coadds_done);
              errim[ii] *= (expConfig.Coadds / (float)coadds_done);
              ii++;
            }
          }

          valueListener.setValue(VALUE_NAME_WRITING_DATA, "1");
          setExposureStatus(statusPrefix + "Writing FITS file", null);

          /* Write FITS file */
          expConfig.Aborted = (localDoAbort > 0);

          if (param.ExposureStartTime != null) {
            expConfig.ExposureStartDate = param.ExposureStartTime.ToUniversalTime().ToString("yyyy-MM-dd");
            expConfig.ExposureStartTime = param.ExposureStartTime.ToUniversalTime().ToString("HH:mm:ss.fff");
          }
          if (param.ExposureEndTime != null) {
            expConfig.ExposureEndTime = param.ExposureEndTime.ToUniversalTime().ToString("HH:mm:ss.fff");
          }

          fits.setScienceData(dataim, expRows, expCols, controlConfiguration.DetectorRotation);
          if (controlConfiguration.WriteNoiseExtension) {
            fits.setNoiseData(errim, expRows, expCols, controlConfiguration.DetectorRotation);
          }
          if (controlConfiguration.writeQualityExtension) {
            fits.setQualityData(qualim, expRows, expCols, controlConfiguration.DetectorRotation);
          }

          //. wait for fits header but only upto configured timeout
          TimeSpan timeSinceStart = DateTime.Now - param.ExposureStartTime;
          if ((timeSinceStart.Seconds < controlConfiguration.fitsHeaderTimeoutSeconds) && !fits.fitsHeaderAdded) {
            setExposureStatus(null, "Waiting for FITS headers to be added");
          }
          while ((timeSinceStart.Seconds < controlConfiguration.fitsHeaderTimeoutSeconds) && !fits.fitsHeaderAdded) {
            Thread.Sleep(100);
            timeSinceStart = DateTime.Now - param.ExposureStartTime;
          }
          /* benchmark fits writing */
          DateTime startTimeFits = DateTime.Now;

          setExposureStatus(null, "Writing FITS data to disk");
          fits.writeFits(expConfig, ramp, controlConfiguration.WriteNoiseExtension, controlConfiguration.WriteQualityExtension);

          /* end benchmark.  report */
          DateTime endTimeFits = DateTime.Now;
          TimeSpan spanFits = endTimeFits - startTimeFits;
          logger.Debug("Fits writing time elapsed = " + spanFits.ToString());

          openFitsWriters.Remove(fits.currentFitsFilename);
          try {
            fits.closeFits(false);
          } catch (Exception ex) {
            logger.Error("Error closing/renaming FITS file <" + fits.currentFitsFilename + ">: " + ex.Message);
            setExposureStatus(null, "Error closing/renaming FITS file.");
          }
          valueListener.setValue(VALUE_NAME_WRITING_DATA, "0");

          setExposureStatus(statusPrefix + "Fits writing complete", null);
          valueListener.setValue(VALUE_NAME_LAST_FILENAME, fitsFilename);
        } else {
          openFitsWriters.Remove(fits.currentFitsFilename);
          try {
            fits.closeFits(true);
          } catch (Exception ex) {
            logger.Error("Error closing/deleting FITS file <" + fits.currentFitsFilename + ">: " + ex.Message);
            setExposureStatus(null, "Error closing/deleting FITS file.");
          }
        }

        valueListener.setValue(VALUE_NAME_OBSERVATION_IN_PROGRESS, "0");
        valueListener.setValue(VALUE_NAME_EXPOSURE_IN_PROGRESS, "0");
        valueListener.setValue(VALUE_NAME_ABORT_IN_PROGRESS, "0");
        valueListener.setValue(VALUE_NAME_IMAGE_DONE, "1");

        logger.Debug("Leaving coaddition");

      } catch (SidecarControlException scEx) {
        setExposureStatus("Cancelling exposure", scEx.Message);
        valueListener.setValue(VALUE_NAME_EXPOSURE_IN_PROGRESS, "0");
        openFitsWriters.Remove(fits.currentFitsFilename);
        fits.closeFits();
        isExposing = false;
      }
    }

    private void doReferencePixelSubtraction(bool doVert, bool doHor, float[] image) {
      int ii, jj;
      int channel;
      int channels;
      float[] refPixelSums;
      float[] refPixelAves;
      int numRefPixels;
      int rowRange;
      int channelWidth;
      int counter;
      float[][][] refPixelArrays;
      int totalNumberOfPixels = exposureRows * exposureCols;

      /* benchmark reference pixel subtraction */
            DateTime startTime = DateTime.Now;

            if (doVert) {
              if (config.VerticalRPSIsChannel) {
                logger.Debug("Doing vertical reference pixel subtraction by channel.");
                channels = sidecarConfig.DetectorOutputs;
                channelWidth = exposureCols / channels;
              } else {
                logger.Debug("Doing vertical reference pixel subtraction by column.");
                channels = exposureCols;
                channelWidth = 1;
              }
              if (config.ReferencePixelSubtractMedian) {
                refPixelArrays = new float[2][][]; //. c# automatically initializes values to 0
                refPixelArrays[0] = new float[channels][];
                refPixelArrays[1] = new float[channels][];
                refPixelAves = new float[channels];
                for (channel = 0; channel < channels; channel++) {
                  refPixelArrays[0][channel] = new float[channelWidth * 4];
                  refPixelArrays[1][channel] = new float[channelWidth * 4];
                }
                for (jj = 0; jj < 4; jj++) {
                  for (ii = 0; ii < exposureCols; ii++) {
                    refPixelArrays[0][ii / channelWidth][ii + channelWidth * (jj - (ii / channelWidth))] = image[ii + exposureCols * jj];
                    refPixelArrays[1][ii / channelWidth][ii + channelWidth * (jj - (ii / channelWidth))] = image[ii + exposureCols * (jj + 2044)];
                  }
                }
                for (channel = 0; channel < channels; channel++) {
                  Array.Sort(refPixelArrays[0][channel]);
                  Array.Sort(refPixelArrays[1][channel]);
				/* rmcgurk commented out 2013-03-15 to remove top Ref Pix from being included in median */
				//refPixelAves[channel] = (refPixelArrays[0][channel][channelWidth * 2] + refPixelArrays[0][channel][channelWidth * 2 - 1] + refPixelArrays[1][channel][channelWidth * 2] + refPixelArrays[1][channel][channelWidth * 2 - 1]) / 4.0f; 
                  refPixelAves[channel] = (refPixelArrays[0][channel][channelWidth * 2] + refPixelArrays[0][channel][channelWidth * 2 - 1] ) / 2.0f;
                }

              } else {
                //. get reference pixels 
                refPixelSums = new float[channels]; //. c# automatically initializes values to 0
                refPixelAves = new float[channels];
                for (jj = 0; jj < 4; jj++) {
                  for (ii = 0; ii < exposureCols; ii++) {
                    refPixelSums[ii / channelWidth] += image[ii + exposureCols * jj];
					/*  rmcgurk commented out 2013-03-15 to remove top Ref Pix from being included in average */
                    //refPixelSums[ii / channelWidth] += image[ii + exposureCols * (jj + 2044)];
                  }
                }
                for (channel = 0; channel < channels; channel++) {
				  /* rmcgurk commented out 2013-03-15 to remove top Ref Pix from being included in average */
                  //refPixelAves[channel] = refPixelSums[channel] / (channelWidth * 8.0f);
				  refPixelAves[channel] = refPixelSums[channel] / (channelWidth * 4.0f);
                }
              }
              //. subtract from data
              for (jj = 0; jj < exposureRows; jj++) {
                for (ii = 0; ii < exposureCols; ii++) {
                  referencePixelSubtractedImage[ii + jj * exposureCols] = image[ii + jj * exposureCols] - refPixelAves[ii / channelWidth];
                }
              }
            } else {
              for (ii = 0; ii < totalNumberOfPixels; ii++) {
                referencePixelSubtractedImage[ii] = image[ii];
              }
            }

            if (doHor) {
              if (config.HorizontalRPSIsRunning) {
                logger.Debug("Doing horizontal reference pixel subtraction by running average (" + config.RunningAverageRows + " rows).");
                rowRange = config.RunningAverageRows;
              } else {
                logger.Debug("Doing horizontal reference pixel subtraction by row.");
                rowRange = 0;
              }

              //. get reference pixels 
              channels = exposureRows;
              if (config.ReferencePixelSubtractMedian) {
                int startRow, endRow;
                refPixelArrays = new float[2][][]; //. c# automatically initializes values to 0
                refPixelArrays[0] = new float[channels][];
                refPixelArrays[1] = new float[channels][];
                refPixelAves = new float[channels];
                for (channel = 0; channel < channels; channel++) {
                  startRow = channel - rowRange;
                  if (startRow < 0) startRow = 0;
                  endRow = channel + rowRange;
                  if (endRow >= exposureRows) endRow = exposureRows - 1;
                  refPixelArrays[0][channel] = new float[4 * (endRow - startRow + 1)];
                  refPixelArrays[1][channel] = new float[4 * (endRow - startRow + 1)];
                  counter = 0;
                  for (ii = 0; ii < 4; ii++) {
                    for (jj = startRow; jj < endRow + 1; jj++) {
                      refPixelArrays[0][channel][counter] = referencePixelSubtractedImage[ii + exposureRows * jj];
                      refPixelArrays[1][channel][counter] = referencePixelSubtractedImage[ii + 2044 + exposureRows * jj];
                      counter++;
                    }
                  }

                  Array.Sort(refPixelArrays[channel]);
                  refPixelAves[channel] = (refPixelArrays[0][channel][counter / 2] + refPixelArrays[0][channel][counter / 2 - 1] + refPixelArrays[1][channel][counter / 2] + refPixelArrays[1][channel][counter / 2 - 1]) / 2.0f;
                }
              } else {
                refPixelSums = new float[channels];
                refPixelAves = new float[channels];
                channel = 0;
                while (channel < channels) {
                  numRefPixels = 0;
                  refPixelSums[channel] = 0;
                  for (ii = 0; ii < 4; ii++) {
                    for (jj = channel - rowRange; jj < channel + rowRange + 1; jj++) {
                      if ((jj >= 0) && (jj < exposureRows)) {
                        refPixelSums[channel] += referencePixelSubtractedImage[ii + exposureRows * jj];
                        refPixelSums[channel] += referencePixelSubtractedImage[ii + 2044 + exposureRows * jj];
                        numRefPixels += 2;
                      }
                    }
                  }
                  refPixelAves[channel] = refPixelSums[channel] / (float)numRefPixels;
                  //       logger.Debug("refPixelSums[" + channel + "] = " + refPixelSums[channel]+", ave= = " + refPixelSums[channel]);
                  channel++;
                }
              }
              //. subtract from data

              for (channel = 0; channel < channels; channel++) {
                for (ii = 0; ii < exposureCols; ii++) {
                  referencePixelSubtractedImage[ii + channel * exposureCols] -= refPixelAves[channel];
                }
              }
            }

            /* end benchmark.  report */
            TimeSpan span = DateTime.Now - startTime;
            logger.Debug("Reference pixel subtraction time elapsed = " + span.TotalMilliseconds + " ms");
    }

    private void doReferencePixelSubtraction(bool doVert, bool doHor, ref float[] image) {
      int ii, jj;
      int channel;
      int channels;
      float[] refPixelSums;
      float[] refPixelAves;
      int numRefPixels;
      int rowRange;
      int channelWidth;
      int counter;
      float[][][] refPixelArrays;
      int totalNumberOfPixels = exposureRows * exposureCols;

      /* benchmark reference pixel subtraction */
      DateTime startTime = DateTime.Now;

      if (doVert) {
        if (config.VerticalRPSIsChannel) {
          logger.Debug("Doing vertical reference pixel subtraction by channel.");
          channels = sidecarConfig.DetectorOutputs;
          channelWidth = exposureCols / channels;
        } else {
          logger.Debug("Doing vertical reference pixel subtraction by column.");
          channels = exposureCols;
          channelWidth = 1;
        }
        if (config.ReferencePixelSubtractMedian) {
          refPixelArrays = new float[2][][]; //. c# automatically initializes values to 0
          refPixelArrays[0] = new float[channels][];
          refPixelArrays[1] = new float[channels][];
          refPixelAves = new float[channels];
          for (channel = 0; channel < channels; channel++) {
            refPixelArrays[0][channel] = new float[channelWidth * 4];
            refPixelArrays[1][channel] = new float[channelWidth * 4];
          }
          for (jj = 0; jj < 4; jj++) {
            for (ii = 0; ii < exposureCols; ii++) {
              refPixelArrays[0][ii / channelWidth][ii + channelWidth * (jj - (ii / channelWidth))] = image[ii + exposureCols * jj];
              refPixelArrays[1][ii / channelWidth][ii + channelWidth * (jj - (ii / channelWidth))] = image[ii + exposureCols * (jj + 2044)];
            }
          }
          for (channel = 0; channel < channels; channel++) {
            Array.Sort(refPixelArrays[0][channel]);
            Array.Sort(refPixelArrays[1][channel]);
            refPixelAves[channel] = (refPixelArrays[0][channel][channelWidth * 2] + refPixelArrays[0][channel][channelWidth * 2 - 1] + refPixelArrays[1][channel][channelWidth * 2] + refPixelArrays[1][channel][channelWidth * 2 - 1]) / 4.0f;
          }

        } else {
          //. get reference pixels 
          refPixelSums = new float[channels]; //. c# automatically initializes values to 0
          refPixelAves = new float[channels];
          for (jj = 0; jj < 4; jj++) {
            for (ii = 0; ii < exposureCols; ii++) {
              refPixelSums[ii / channelWidth] += image[ii + exposureCols * jj];
              refPixelSums[ii / channelWidth] += image[ii + exposureCols * (jj + 2044)];
            }
          }
          for (channel = 0; channel < channels; channel++) {
            refPixelAves[channel] = refPixelSums[channel] / (channelWidth * 8.0f);
          }
        }
        //. subtract from data
        for (jj = 0; jj < exposureRows; jj++) {
          for (ii = 0; ii < exposureCols; ii++) {
            image[ii + jj * exposureCols] = image[ii + jj * exposureCols] - refPixelAves[ii / channelWidth];
          }
        }
      } else {
        for (ii = 0; ii < totalNumberOfPixels; ii++) {
          referencePixelSubtractedImage[ii] = image[ii];
        }
      }

      if (doHor) {
        if (config.HorizontalRPSIsRunning) {
          logger.Debug("Doing horizontal reference pixel subtraction by running average (" + config.RunningAverageRows + " rows).");
          rowRange = config.RunningAverageRows;
        } else {
          logger.Debug("Doing horizontal reference pixel subtraction by row.");
          rowRange = 0;
        }

        //. get reference pixels 
        channels = exposureRows;
        if (config.ReferencePixelSubtractMedian) {
          int startRow, endRow;
          refPixelArrays = new float[2][][]; //. c# automatically initializes values to 0
          refPixelArrays[0] = new float[channels][];
          refPixelArrays[1] = new float[channels][];
          refPixelAves = new float[channels];
          for (channel = 0; channel < channels; channel++) {
            startRow = channel - rowRange;
            if (startRow < 0) startRow = 0;
            endRow = channel + rowRange;
            if (endRow >= exposureRows) endRow = exposureRows - 1;
            refPixelArrays[0][channel] = new float[4 * (endRow - startRow + 1)];
            refPixelArrays[1][channel] = new float[4 * (endRow - startRow + 1)];
            counter = 0;
            for (ii = 0; ii < 4; ii++) {
              for (jj = startRow; jj < endRow + 1; jj++) {
                refPixelArrays[0][channel][counter] = image[ii + exposureRows * jj];
                refPixelArrays[1][channel][counter] = image[ii + 2044 + exposureRows * jj];
                counter++;
              }
            }

            Array.Sort(refPixelArrays[0][channel]);
            Array.Sort(refPixelArrays[1][channel]);
            refPixelAves[channel] = (refPixelArrays[0][channel][counter / 2] + refPixelArrays[0][channel][counter / 2 - 1] + refPixelArrays[1][channel][counter / 2] + refPixelArrays[1][channel][counter / 2 - 1]) / 2.0f;
          }
        } else {
          refPixelSums = new float[channels];
          refPixelAves = new float[channels];
          channel = 0;
          while (channel < channels) {
            numRefPixels = 0;
            refPixelSums[channel] = 0;
            for (ii = 0; ii < 4; ii++) {
              for (jj = channel - rowRange; jj < channel + rowRange + 1; jj++) {
                if ((jj >= 0) && (jj < exposureRows)) {
                  refPixelSums[channel] += image[ii + exposureRows * jj];
                  refPixelSums[channel] += image[ii + 2044 + exposureRows * jj];
                  numRefPixels += 2;
                }
              }
            }
            refPixelAves[channel] = refPixelSums[channel] / (float)numRefPixels;
            //       logger.Debug("refPixelSums[" + channel + "] = " + refPixelSums[channel]+", ave= = " + refPixelSums[channel]);
            channel++;
          }
        }
        //. subtract from data

        for (channel = 0; channel < channels; channel++) {
          for (ii = 0; ii < exposureCols; ii++) {
            image[ii + channel * exposureCols] -= refPixelAves[channel];
          }
        }
      }

      /* end benchmark.  report */
      TimeSpan span = DateTime.Now - startTime;
      logger.Debug("Reference pixel subtraction time elapsed = " + span.TotalMilliseconds + " ms");
    }

    public void testReferencePixelSubtraction() {
      int ii, jj, kk;
      int counter;
      int channel;
      int channels;
      int totalNumberOfPixels = 2048 * 2048;
      ushort[] data = new ushort[totalNumberOfPixels];
      ushort[,] data2 = new ushort[2048, 2048];
      long[] refPixelSums;
      float[] refPixelAves;
      Random rand = new Random();
      bool doVert = false;
      bool doHor = true;
      bool vertTypeChannel = true;
      bool horTypeRunning = true;
      StringBuilder outfile = new StringBuilder("");
      string dir = "Z:\\weisstest\\";
      int numRefPixels;
      int rowRange;
      int channelWidth;
      int runningAverageRows;
      bool useMedian = true;
      ushort[][] refPixelArrays;
      FitsWriter fits = new FitsWriter();

      runningAverageRows = 4;
      logger.Debug("runningAveRows=" + runningAverageRows);
      /*
       * Arrays are filled such that 0,0 is bottom left, and increases
       * horizontally first
       */


      counter = 0;
      for (jj = 0; jj < 2048; jj++) {
        for (ii = 0; ii < 32; ii++) {
          for (kk = 0; kk < 64; kk++) {
            data[counter] = (ushort)((ii + 1) * 1000 + kk * 2 + (rand.Next(19) - 9));
            counter++;
          }
        }
      }

      /* benchmark fits writing */
      DateTime startTime = DateTime.Now;

      if (doVert) {
        if (vertTypeChannel) {
          outfile.Append(dir + "vertChan");
          channels = sidecarConfig.DetectorOutputs;
          channelWidth = exposureCols / channels;
        } else {
          outfile.Append(dir + "vertCol");
          channels = exposureCols;
          channelWidth = 1;
        }
        //. get reference pixels 

        if (useMedian) {
          refPixelArrays = new ushort[channels][]; //. c# automatically initializes values to 0
          refPixelAves = new float[channels];
          for (channel = 0; channel < channels; channel++) {
            refPixelArrays[channel] = new ushort[channelWidth*8];
          }
          for (jj = 0; jj < 4; jj++) {
            for (ii = 0; ii < exposureCols; ii++) {
/*
              refPixelArrays[ii/channelWidth][ii - channelWidth*(ii/channelWidth)+jj*channelWidth] = data[ii + exposureCols * jj];
              refPixelArrays[ii / channelWidth][ii - channelWidth*(ii/channelWidth)+ (jj+4) * channelWidth] = data[ii + exposureCols * (jj + 2044)];
*/
              refPixelArrays[ii / channelWidth][ii + channelWidth * (jj - (ii / channelWidth))] = data[ii + exposureCols * jj];
              refPixelArrays[ii / channelWidth][ii + channelWidth * ((jj + 4) - (ii / channelWidth))] = data[ii + exposureCols * (jj + 2044)];
            }
          }
          for (channel = 0; channel < channels; channel++) {
            Array.Sort(refPixelArrays[channel]);
            refPixelAves[channel] = (refPixelArrays[channel][channelWidth*4]+refPixelArrays[channel][channelWidth*4-1])/2.0f;
          }

        } else {

          refPixelSums = new long[channels]; //. c# automatically initializes values to 0
          refPixelAves = new float[channels];
          for (jj = 0; jj < 4; jj++) {
            for (ii = 0; ii < exposureCols; ii++) {
              refPixelSums[ii / channelWidth] += data[ii + exposureCols * jj];
              refPixelSums[ii / channelWidth] += data[ii + exposureCols * (jj + 2044)];
            }
          }
          for (channel = 0; channel < channels; channel++) {
            refPixelAves[channel] = refPixelSums[channel] / (channelWidth * 8.0f);
          }
        }
      
      //. subtract from data
        for (jj = 0; jj < exposureRows; jj++) {
          channel = 0;
          while (channel < exposureCols) {
            dataimage[channel + jj * exposureCols] = (float)data[channel + jj * exposureCols] - refPixelAves[channel/channelWidth];
            channel++;
          }
        }
      }

      if (doHor) {
        if (horTypeRunning) {
          if (outfile.Length == 0) {
            outfile.Append(dir + "horRun");
          } else {
            outfile.Append("_horRun");
          }
          rowRange = runningAverageRows;
        } else {
          if (outfile.Length == 0) {
            outfile.Append(dir + "horRow");
          } else {
            outfile.Append("_horRow");
          }
          rowRange = 0;
        }
        //. get reference pixels 
        channels = exposureRows;
        if (useMedian) {
          int startRow, endRow;
          refPixelArrays = new ushort[channels][]; //. c# automatically initializes values to 0
          refPixelAves = new float[channels];
          for (channel = 0; channel < channels; channel++) {
            startRow = channel - rowRange;
            if (startRow < 0) startRow = 0;
            endRow = channel + rowRange;
            if (endRow >= exposureRows) endRow = exposureRows - 1;
            refPixelArrays[channel] = new ushort[8*(endRow-startRow+1)];
            counter = 0;
            for (ii = 0; ii < 4; ii++) {
              for (jj = startRow; jj < endRow + 1; jj++) {
                refPixelArrays[channel][counter] = data[ii + exposureRows * jj];
                counter++;
                refPixelArrays[channel][counter] = data[ii + 2044 + exposureRows * jj];
                counter++;
              }
            }
            
            Array.Sort(refPixelArrays[channel]);
            refPixelAves[channel] = (refPixelArrays[channel][counter/2] + refPixelArrays[channel][counter/2 - 1]) / 2.0f;
          }
        } else {
          refPixelSums = new long[channels];
          refPixelAves = new float[channels];
          channel = 0;
          while (channel < channels) {
            numRefPixels = 0;
            refPixelSums[channel] = 0;
            for (ii = 0; ii < 4; ii++) {
              for (jj = channel - rowRange; jj < channel + rowRange + 1; jj++) {
                if ((jj >= 0) && (jj < exposureRows)) {
                  refPixelSums[channel] += data[ii + exposureRows * jj];
                  refPixelSums[channel] += data[ii + 2044 + exposureRows * jj];
                  numRefPixels += 2;
                }
              }
            }
            refPixelAves[channel] = refPixelSums[channel] / (float)numRefPixels;
            //       logger.Debug("refPixelSums[" + channel + "] = " + refPixelSums[channel]+", ave= = " + refPixelSums[channel]);
            channel++;
          }
        }
        //. subtract from data
        channel = 0;
        while (channel < channels) {
          for (ii = 0; ii < exposureCols; ii++) {
            dataimage[ii + channel * exposureCols] = (float)data[ii + channel * exposureCols] - refPixelAves[channel];
          }
          channel++;
        }
      }

      /*
      for (kk = 0; kk < 100; kk++) {
        /*
        counter = 0;
        while (counter < totalNumberOfPixels) {
          data[counter] = (ushort)(counter & kk);
          counter++;
        }
         */

      /* looping through 2048x2048 elements 100 times
       * using 1d array: 593.75 ms
       * using 1d array with 2 for loops: 640.625 ms
       * using 2d array: 22312.5 ms */
      /*
      for (jj = 0; jj < 2048; jj++) {
        for (ii = 0; ii < 2048; ii++) {
          data2[ii, jj] = (ushort)(ii & jj);
        }
      }
       */
      /*
        for (jj = 0; jj < 2048; jj++) {
          for (ii = 0; ii < 2048; ii++) {
            data[ii + jj*2048] = (ushort)(ii & jj);
          }
        }
       
      }
     */
      /* end benchmark.  report */
      DateTime endTime = DateTime.Now;
      TimeSpan span = endTime - startTime;
      logger.Debug("Test time elapsed = " + span.TotalMilliseconds + " ms");

      if (outfile.Length > 0) {
        fits.initFits(new FitsInfo(outfile.ToString() + ".fits", 2048, 2048));
        fits.setScienceData(dataimage, 2048, 2048);
        fits.writeFits();
        fits.closeFits();

        RawFrameFitsWriter w = new RawFrameFitsWriter(new FitsInfo(outfile.ToString() + "_raw.fits", 2048, 2048), data, ".writing");
        w.prepareData();
        w.writeRaw();
      }
    }
    public void testRotation() {
      int counter;
      int rows=2048;
      int cols=2048;
      float[] data;
      int ii,jj,kk;
      Random rand = new Random();
      int rotation;
      StringBuilder outfile = new StringBuilder("Y:\\weisstest\\rotationTest");
      FitsWriter fits = new FitsWriter();

      data = new float[rows*cols];

      counter = 0;
      for (jj = 0; jj < 2048; jj++) {
        for (ii = 0; ii < 32; ii++) {
          for (kk = 0; kk < 64; kk++) {
            data[counter] = (float)((ii + 1) * 1000 + kk * 2 + jj);
            counter++;
          }
        }
      }

      for (rotation = 0; rotation < 4; rotation++) {

        outfile.Append(rotation < 0 ? "Neg" + Math.Abs(rotation).ToString() : rotation.ToString());

        fits.initFits(new FitsInfo(outfile.ToString() + ".fits", rows, cols));
        fits.setScienceData(data, rows, cols, rotation);
        fits.writeFits();
        fits.closeFits();
      }
    }
  }
}