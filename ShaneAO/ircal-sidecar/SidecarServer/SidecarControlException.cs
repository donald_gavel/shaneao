using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace Sidecar {
  class SidecarControlException : Exception, ISerializable {
    public SidecarControlException() : base() { }
    public SidecarControlException(string message) : base(message) { }
    public SidecarControlException(string message, System.Exception inner) : base(message, inner) { }

    // Constructor needed for serialization 
    // when exception propagates from a remoting server to the client.
    protected SidecarControlException(System.Runtime.Serialization.SerializationInfo info,
        System.Runtime.Serialization.StreamingContext context)
      : base(info, context) { }

  }
}
