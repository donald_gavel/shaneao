using System;
using System.Collections.Generic;
using System.Text;

namespace Sidecar {

  class ExposureConfiguration {

    public const int SAMPLING_MODE_SINGLE = 1;
    public const int SAMPLING_MODE_CDS = 2;
    public const int SAMPLING_MODE_MCDS = 3;
    public const int SAMPLING_MODE_UTR = 4;

    private int resets;

    public int Resets {
      get { return resets; }
      set { resets = value; }
    }

    private long itimeInMicros;

    public long ItimeInMicros {
      get { return itimeInMicros; }
      set { itimeInMicros = value; }
    }

    private float groupItime;

    public float GroupItime {
      get { return groupItime; }
      set { groupItime = value; }
    }

    private int coadds;
    public int Coadds {
      get { return coadds; }
      set { coadds = value; }
    }

    private int sampMode;

    public int SampMode {
      get { return sampMode; }
      set {
        if ((value < 1) || (value > 4)) {
          throw new InvalidExposureConfigurationException("Sampling mode <" + value + "> must be one of: 1=Single, 2=CDS, 3=MCDS/Fowler, 4=Up-the-Ramp (UTR)");
        }
        sampMode = value;
      }
    }

    private int reads;
    public int Reads {
      get { return reads; }
      set { reads = value; }
    }

    private int groups;
    public int Groups {
      get { return groups; }
      set { groups = value; }
    }

    private ExposureExtents extents;
    public ExposureExtents Extents {
      get { return extents; }
      set { extents = value; }
    }

    private string exposureStartDate;
    public string ExposureStartDate {
      get { return exposureStartDate; }
      set { exposureStartDate = value; }
    }

    private string exposureStartTime;
    public string ExposureStartTime {
      get { return exposureStartTime; }
      set { exposureStartTime = value; }
    }

    private string exposureEndTime;
    public string ExposureEndTime {
      get { return exposureEndTime; }
      set { exposureEndTime = value; }
    }

    private bool aborted;
    public bool Aborted {
      get { return aborted; }
      set { aborted = value; }
    }

    private int readsDone;
    public int ReadsDone {
      get { return readsDone; }
      set { readsDone = value; }
    }

    private int groupsDone;
    public int GroupsDone {
      get { return groupsDone; }
      set { groupsDone = value; }
    }

    private int coaddsDone;
    public int CoaddsDone {
      get { return coaddsDone; }
      set { coaddsDone = value; }
    }

    public ExposureConfiguration Clone() {
      return new ExposureConfiguration(this);
    }
    public ExposureConfiguration(ExposureConfiguration config) :
      this(config.Resets, config.ItimeInMicros, config.Coadds, config.SampMode, config.Reads, config.Groups) {
      Extents = config.Extents.Clone();
      GroupItime = config.GroupItime;
      Aborted = config.Aborted;
      ReadsDone = config.ReadsDone;
      GroupsDone = config.GroupsDone;
      CoaddsDone = config.CoaddsDone;
      ExposureStartDate = config.ExposureStartDate;
      ExposureStartTime = config.ExposureStartTime;
      ExposureEndTime = config.ExposureEndTime;
    }
    public ExposureConfiguration()
      : this(1, 10000000, 1, 2, 1) {

    }
    public ExposureConfiguration(int resets, long itimeInMicros, int coadds, int sampMode, int reads)
      : this(resets, itimeInMicros, coadds, sampMode, reads, 2) {
    }
    public ExposureConfiguration(int resets, long itimeInMicros, int coadds, int sampMode, int reads, int groups) {
      Resets = resets;
      ItimeInMicros = itimeInMicros;
      Coadds = coadds;
      SampMode = sampMode;
      Reads = reads;
      Groups = groups;
      ExposureStartDate = "";
      ExposureStartTime = "";
      ExposureEndTime = "";
      Aborted = false;
      ReadsDone = 0;
      CoaddsDone = 0;
      Extents = new ExposureExtents();
    }
    public ExposureConfiguration(int resets, long itimeInMicros, int coadds, int sampMode, int reads, int startx, int starty, int endx, int endy) 
      : this(resets, itimeInMicros, coadds, sampMode, reads, 2, startx, starty, endx, endy) {
    }

    public ExposureConfiguration(int resets, long itimeInMicros, int coadds, int sampMode, int reads, int groups, int startx, int starty, int endx, int endy) {
      Resets = resets;
      ItimeInMicros = itimeInMicros;
      Coadds = coadds;
      SampMode = sampMode;
      Reads = reads;
      ExposureStartDate = "";
      ExposureStartTime = "";
      ExposureEndTime = "";
      Aborted = false;
      ReadsDone = 0;
      CoaddsDone = 0;
      Extents = new ExposureExtents(startx, starty, endx, endy);
    }
  }
}
