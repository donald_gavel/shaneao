﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sidecar
{
	class ExposureExtents
	{
    private int startx;

    public int StartX {
      get { return startx; }
      set { startx = value; }
    }

    private int starty;

    public int StartY {
      get { return starty; }
      set { starty = value; }
    }


    private int endx;

    public int EndX {
      get { return endx; }
      set { endx = value; }
    }

    private int endy;

    public int EndY {
      get { return endy; }
      set { endy = value; }
    }

    private bool isSubarray;

    public bool IsSubarray {
      get { return isSubarray; }
      set { isSubarray = value; }
    }


    public override bool Equals(System.Object obj) {
      // If parameter is null return false.
      if (obj == null) {
        return false;
      }

      // If parameter cannot be cast to ExposureExtents return false.
      ExposureExtents p = obj as ExposureExtents;
      if ((System.Object)p == null) {
        return false;
      }

      // Return true if the fields match:
      return (startx == p.startx) && (starty == p.starty) && (endx == p.endx) && (endy == p.endy);
    }
    public override int GetHashCode() {
      return base.GetHashCode();
    }
    public ExposureExtents Clone() {
      return (ExposureExtents)(this.MemberwiseClone());
    }

    public ExposureExtents()
      : this(2048, 2048) {
    }
    public ExposureExtents(int cols, int rows)
      : this(0, 0, cols-1, rows-1) {
    }
        
    public ExposureExtents(int startx, int starty, int endx, int endy) {
      StartX = startx;
      StartY = starty;
      EndX = endx;
      EndY = endy;
      isSubarray = false;
    }

	}
}
