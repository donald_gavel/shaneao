using System;
using System. Collections. Generic;
using System. Text;

namespace Sidecar {
  class ThreeLayerData {

    float[,] dataArray;

    public float[,] DataArray {
      get { return dataArray; }
      set { dataArray = value; }
    }

    float[,] noiseArray;

    public float[,] NoiseArray {
      get { return noiseArray; }
      set { noiseArray = value; }
    }

    byte[,] qualityArray;

    public byte[,] QualityArray {
      get { return qualityArray; }
      set { qualityArray = value; }
    }

    public ThreeLayerData(int xsize, int ysize) {
      dataArray = new float[xsize, ysize];
      noiseArray = new float[xsize, ysize];
      qualityArray = new byte[xsize, ysize];
    }
  }
}
