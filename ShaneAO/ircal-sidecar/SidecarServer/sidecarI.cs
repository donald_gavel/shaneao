using System;
using System.Collections;
using System.Collections.Generic;
using SidecarASICComComponents;
using log4net;
using Sidecar;
using System.Threading;
using System.IO;

namespace Sidecar.SidecarIce {
  public sealed class SidecarI : ISidecarIceComDisp_, ValueListener {
    private SidecarASIC sidecar = null;
    private SidecarControl sidecarControl;
    private ArrayList clients = new ArrayList();
    private ArrayList missing_clients = new ArrayList();
    private Ice.Communicator communicator;

    private static readonly ILog logger = LogManager.GetLogger(typeof(SidecarI));

    public SidecarI(Ice.Communicator ic) {
      sidecar = new SidecarASICComComponents.SidecarASIC();
      communicator = ic;
      sidecarControl = new SidecarControl(sidecar, this);
    }

    public SidecarI(Ice.Communicator ic, string sidecarControlConfigFile) {
      sidecar = new SidecarASICComComponents.SidecarASIC();
      communicator = ic;
      sidecarControl = new SidecarControl(sidecar, this);
      try {
        sidecarControl.readSidecarControlConfigurationFile(sidecarControlConfigFile);
      } catch (SidecarControlException scEx) {
        
        logger.Warn(scEx.Message);
      }
    }
    private string longToHexString(long num) {
      return String.Format("0x{0:x4}", num);
    }

    private static int ushortToint(ushort data) {
      return (int)data;
    }

    private static long uintTolong(uint data) {
      return (long)data;
    }


    private static ushort intToushort(int data) {
      return (ushort)data;
    }


    private static uint longTouint(long data) {
      return (uint)data;
    }
    public void shutdown() {
      sidecarControl.stopHeartbeat();
      setValue("ICEServer", "shutdown");
    }
    public void setValue(string name, string value) {
      lock (this) {
        if (clients.Count > 0) {
          missing_clients.Clear();
          foreach (ISidecarIceCallbackPrx client in clients) {
            try {
              if (name.CompareTo("heartbeat") != 0) {
                logger.Debug("calling setValue(" + name + "=" + value + ") method on client `" +
                                      communicator.identityToString(client.ice_getIdentity()) + "':\n");
              }
              client.setValue(name, value);
            } catch (Ice.LocalException ex) {
              if (ex is Ice.ConnectionLostException) {
                logger.Info("lost client " + communicator.identityToString(client.ice_getIdentity()) + ". Removing...");
              } else if (ex is Ice.ObjectNotExistException) {
                logger.Info("client " + communicator.identityToString(client.ice_getIdentity()) + " no longer available. Removing...");
              } else {
                logger.Error("error with client `" +
                                      communicator.identityToString(client.ice_getIdentity()) + "':\n" + ex);
              }
              missing_clients.Add(client);
            }
          }
          //. remove missing clients from main client list
          if (missing_clients.Count > 0) {
            foreach (ISidecarIceCallbackPrx missing_client in missing_clients) {
              logger.Info("removing client `" +
                                      communicator.identityToString(missing_client.ice_getIdentity()) + "'");
              clients.Remove(missing_client);
            }
          }
        }
      }
    }
    public override string echo(string s, Ice.Current current__) {
      logger.Debug("echo <" + s + ">");
      return s;
    }
    public override void addCallbackClient(Ice.Identity ident, Ice.Current current__) {
      lock (this) {
        logger.Info("adding client `" + communicator.identityToString(ident) + "'");

        Ice.ObjectPrx prxbase = current__.con.createProxy(ident);
        ISidecarIceCallbackPrx client = ISidecarIceCallbackPrxHelper.uncheckedCast(prxbase);
        
        clients.Add(client);
      }
    }
    public override int setExposureExtents(string sJade, string sAsic, int startx, int starty, int endx, int endy, Ice.Current current__) {
      int result = 0;
      logger.Info("In SidecarI.setExposureExtents.  Jade = " + sJade
        + ", Asic = " + sAsic
        + ", startx =" + startx
        + ", starty =" + starty
        + ", endx =" + endx
        + ", endy =" + endy
        + ".");

      try {
        sidecarControl.setExposureExtents(sJade, sAsic, new ExposureExtents(startx, starty, endx, endy));
      } catch (SidecarControlException scEx) {
        logger.Error("Error setting exposure extents: " + scEx.Message);
        setValue("SidecarError", scEx.Message);
        result = -1;
      } catch (InvalidExposureConfigurationException iecEx) {
        logger.Error("Error setting exposure extents: " + iecEx.Message);
        setValue("SidecarError", iecEx.Message);
        result = -2;
      }
      return result;
    }
    public override int takeExposure(string sJade, string sAsic, int resets, long itimeInMicros, int coadds, int sampMode, int reads, int groups, string filename, Ice.Current current__) {
      int result = 0;
      logger.Info("In SidecarI.takeExposure.  Jade = " + sJade
          + ", Asic = " + sAsic
          + ", resets =" + resets
          + ", itimeInMicros =" + itimeInMicros
          + ", coadds =" + coadds
          + ", sampMode =" + sampMode
          + ", reads =" + reads
          + ", groups =" + groups
          + ".");

      try {
        result = sidecarControl.takeExposure(sJade, sAsic, new ExposureConfiguration(resets, itimeInMicros, coadds, sampMode, reads, groups), filename);
      } catch (SidecarControlException scEx) {
        logger.Error("Error taking exposure: " + scEx.Message);
        setValue("SidecarError", scEx.Message);
        result = -1;
      } catch (InvalidExposureConfigurationException iecEx) {
        logger.Error("Error taking exposure: " + iecEx.Message);
        setValue("SidecarError", iecEx.Message);
        result = -2;
      }

      return result;
    }
    public override int abortExposure(int abortLevel, bool saveImage, Ice.Current current__) {
      int result = 0;
      logger.Debug("in abortExposure");
      result = sidecarControl.abortExposure(abortLevel, saveImage);
      return result;
    }
    public override int validateFilename(string sFilename, Ice.Current current__) {
      int result = 0;
      logger.Debug("in validateFilename.  filename = <"+sFilename+">.");
      
      result = sidecarControl.validateFilename(sFilename);
      logger.Debug("validateFilename result = " + result);
      return result;
    }
    public override int resetASIC(string sJade, Ice.Current current__) {
      int result = 0;
      logger.Debug("in resetASIC");
      
      try {
        sidecarControl.resetASIC(sJade);
        logger.Info("SIDECAR successfully reset.");
      } catch (SidecarControlException scEx) {
        logger.Error("Error resetting ASIC: " + scEx.Message);
        result = -1;
      }
      return result;
    }
    public override int powerDownASIC(string sJade, Ice.Current current__) {
      int result = 0;
      logger.Debug("in powerDownASIC");

      try {
        sidecarControl.powerDownASIC(sJade);
        logger.Info("SIDECAR successfully powered down.");
      } catch (SidecarControlException scEx) {
        logger.Error("Error powering down ASIC: " + scEx.Message);
        result = -1;
      }
      return result;
    }

    public override int getExposureStatus(out double percentComplete, out string statusMessage, out string statusDetail, Ice.Current current__) {
      logger.Debug("getExposureStatus");
      percentComplete = sidecarControl.ExposurePercentComplete;
      statusMessage = sidecarControl.ExposureStatus;
      statusDetail = sidecarControl.ExposureStatusDetail;
      return sidecarControl.ExposureStatusID;
    }

    public override int readSidecarControlConfiguration(string configFilename, Ice.Current current__) {
      int result = 0;
      logger.Debug("reading SIDECAR control configuration file: " + configFilename);
      try {
        result = sidecarControl.readSidecarControlConfigurationFile(configFilename);
      } catch (SidecarControlException scEx) {
        logger.Error("Error reading sidecar configuration file: " + scEx.Message);
        result = -1;
      }
      return result;
    }
    public override int addFitsHeader(string fitsFilename, List<SidecarIceFitsHeaderKey> header, Ice.Current current__) {
      int result = 0;
      logger.Debug("Received Fits Header. Number of Keywords = " + header.Count);
      result = sidecarControl.addFitsHeader(fitsFilename, header);
      return result;
    }
    public override int getReadTimeInMicroseconds(string sJade, string sAsic, Ice.Current current__) {
      int result = 0;
      logger.Debug("in getReadTimeInMicroseconds");

      try {
        return sidecarControl.getReadTime(sJade, sAsic);
      } catch (SidecarControlException scEx) {
        logger.Error("Error getting read time: " + scEx.Message);
        result = -1;
      }
      return result;
    }
    public override int broadcastValue(string valueName, Ice.Current current__) {
      int result = 0;
      logger.Debug("in broadcastValue, valueName="+valueName);

      try {
        return sidecarControl.broadcastValue(valueName);
      } catch (SidecarControlException scEx) {
        logger.Error("Error initiating broadcast of value <"+valueName+">: " + scEx.Message);
        result = -1;
      }
      return result;
    }
    public override int setReferencePixelSubtractionType(int type, Ice.Current current__) {
      int result = 0;
      logger.Debug("in setReferencePixelSubtractionType");

      try {
       sidecarControl.setReferencePixelSubtractionType(type);
      } catch (SidecarControlException scEx) {
        logger.Error("Error setting reference pixel subtraction type: " + scEx.Message);
        result = -1;
      }
      return result;
    }

    public override int writeRawFrames(bool enable, Ice.Current current__) {
      int result = 0;
      logger.Debug("in writeRawFrames");

      sidecarControl.writeRawFrames(enable);
      
      return result;
    }

    public override string initializeSidecar(string sJade, string sAsic, bool downloadFirmware, Ice.Current current__) {
      SidecarInitializer si = new SidecarInitializer(this, sidecarControl, sJade, sAsic, downloadFirmware);
      Thread siThread = new Thread(new ThreadStart(si.initializeSidecar));
      
      logger.Debug("Starting sidecar initialization thread.");
      siThread.Start();

      return "initializing";
    }

    /* Jade Firmware Downloaded inner class */
    class SidecarInitializer {
      string jade;
      string asic;
      bool dlFirmware;
      SidecarI mySidecarI;
      SidecarControl mySidecarControl;
      public SidecarInitializer(SidecarI sidecarI, SidecarControl sidecarC, string sJade, string sAsic, bool downloadFirmware) {
        jade = sJade;
        asic = sAsic;
        dlFirmware = downloadFirmware;
        mySidecarI = sidecarI;
        mySidecarControl = sidecarC;
      }

      public void initializeSidecar() {
        mySidecarI.setValue("initializeSidecar", "initializing");
        try {
          mySidecarControl.initSidecar(jade, asic, dlFirmware);
          mySidecarI.setValue("initializeSidecar", "Success!");
        } catch (SidecarControlException scEx) {
          mySidecarI.setValue("initializeSidecar", scEx.Message);

        }
      }
    }

    public override string DownloadASICRegisterFile(string sJade, string sAsic, string sFilename, Ice.Current current__) {
      String result = sidecar.DownloadASICRegisterFile(sJade, sAsic, sFilename);
      logger.Debug("DownloadASICRegisterFile called: result = " + result);
      return result;
    }

    public override string DownloadASICRegisters(string sJade, string sAsic, Ice.Current current__) {
      String result = sidecar.DownloadASICRegisters(sJade, sAsic);
      logger.Debug("DownloadASICRegisters called: result = " + result);
      return result;
    }

    public override string DownloadAllConfiguredAsics(string sJade, bool MCDs, bool Registers, Ice.Current current__) {
      String result = sidecar.DownloadAllConfiguredAsics(sJade, MCDs, Registers);
      logger.Debug("DownloadAllConfiguredAsics called: result = " + result);
      return result;
    }

    public override string DownloadJADEFirmware(string sJade, Ice.Current current__) {
      JadeFirmwareDownloader dl = new JadeFirmwareDownloader(this, sidecar, sJade);
      Thread dlThread = new Thread(new ThreadStart(dl.downloadJadeFirmware));
      logger.Debug("Starting Jade Firmware download thread.");

      dlThread.Start();
      return "downloading";
    }

    /* Jade Firmware Downloaded inner class */
    class JadeFirmwareDownloader {
      string jade;
      string file;
      SidecarI mySidecarI;
      SidecarASIC mySidecar;
      public JadeFirmwareDownloader(SidecarI sidecarI, SidecarASIC sidecar, string sJade, string sFile) {
        jade = sJade;
        file = sFile;
        mySidecarI = sidecarI;
        mySidecar = sidecar;
      }
      public JadeFirmwareDownloader(SidecarI sidecarI, SidecarASIC sidecar, string sJade) : this(sidecarI, sidecar, sJade, "") { }

      public void downloadJadeFirmware() {
        String result;
        mySidecarI.setValue("downloadFirmware", "downloading");
        if (file.Length > 0) {
          result = mySidecar.DownloadJADEFirmwareFile(jade, file);
          logger.Debug("DownloadJADEFirmwareFile called: result = " + result);
        } else {
          result = mySidecar.DownloadJADEFirmware(jade);
          logger.Debug("DownloadJADEFirmware called: result = " + result);
        }
        mySidecarI.setValue("downloadFirmware", result);
      }
    }

    public override string DownloadJADEFirmwareFile(string sJade, string sFilename, Ice.Current current__) {
      JadeFirmwareDownloader dl = new JadeFirmwareDownloader(this, sidecar, sJade, sFilename);
      Thread dlThread = new Thread(new ThreadStart(dl.downloadJadeFirmware));
      logger.Debug("Starting Jade Firmware download thread.");

      dlThread.Start();
      return "downloading";
    }

    public override string DownloadJADERegisterFile(string sJade, string sFilename, Ice.Current current__) {
      String result = sidecar.DownloadJADERegisterFile(sJade, sFilename);
      logger.Debug("DownloadJADERegisterFile called: result = " + result);
      return result;
    }

    public override string DownloadJADERegisters(string sJade, Ice.Current current__) {
      String result = sidecar.DownloadJADERegisters(sJade);
      logger.Debug("DownloadJADERegisters called: result = " + result);
      return result;
    }

    public override string DownloadMCD(string sJade, string sAsic, Ice.Current current__) {
      String result = sidecar.DownloadMCD(sJade, sAsic);
      logger.Debug("DownloadMCD called: result = " + result);
      return result;
    }

    public override string DownloadMCDFile(string sJade, string sAsic, string sMCDFileName, Ice.Current current__) {
      //. if file isn't valid or does not exist, 
      //. a modal dialog is displayed with error message,
      //. and ICE server is blocked unless dialog is dismissed.

      //. fisrt, make sure file exists
      try {
        FileInfo fi = new FileInfo(sMCDFileName);
        if (!fi.Exists) {
          string errMsg = "Error downloading MCD: " + sMCDFileName + ". File does not exist.";
          logger.Error(errMsg);
          setValue("SidecarError", errMsg);
          return errMsg;
        }
        if (sidecarControl.validateMCDFile(sMCDFileName) < 0) {
          string errMsg = "Error downloading MCD: " + sMCDFileName + ". MCD badly formed.";
          logger.Error(errMsg);
          setValue("SidecarError", errMsg);
          return errMsg;
        }
      } catch (Exception ex) {
        string errMsg = "Error downloading MCD: " + sMCDFileName + ". "+ ex.Message;
        logger.Error(errMsg);
        setValue("SidecarError", errMsg);
        return errMsg;      
      }

      String result = sidecar.DownloadMCDFile(sJade, sAsic, sMCDFileName);
      logger.Debug("DownloadMCDFile called: result = " + result);
      return result;
    }

    public override string GetAsics(string sJade, Ice.Current current__) {
      String result = sidecar.GetAsics(sJade);
      logger.Debug("GetAsics called: result = " + result);
      return result;
    }

    public override int GetAutoWrite(Ice.Current current__) {
      int result = sidecar.GetAutoWrite();
      logger.Debug("GetAutoWrite called: result = " + result);
      return result;
    }

    public override string GetAvailableAsics(string sJade, Ice.Current current__) {
      String result = sidecar.GetAvailableAsics(sJade);
      logger.Debug("GetAvailableAsics called: result = " + result);
      return result;
    }

    public override string GetAvailableJades(Ice.Current current__) {
      String result = sidecar.GetAvailableJades();
      logger.Debug("GetAvailableJades called: result = " + result);
      return result;
    }

    public override int GetDMA(Ice.Current current__) {
      int result = sidecar.GetDMA();
      logger.Debug("GetDMA called: result = " + result);
      return result;
    }

    public override string GetJades(Ice.Current current__) {
      String result = sidecar.GetJades();
      logger.Debug("GetJades called: result = " + result);
      return result;
    }

    public override int GetMemoryBlockMode(Ice.Current current__) {
      int result = sidecar.GetMemoryBlockMode();
      logger.Debug("GetMemoryBlockMode called: result = " + result);
      return result;
    }

    public override string OpenApp(string sFileName, Ice.Current current__) {
      String result = sidecar.OpenApp(sFileName);
      logger.Debug("OpenApp called: result = " + result);
      return result;
    }

    public override long ReadAsicReg(string sJade, string sAsic, long addr, bool is24bit, Ice.Current current__) {
      uint result = sidecar.ReadAsicReg(sJade, sAsic, (uint)addr, is24bit);
      logger.Debug("ReadAsicReg called: result: " + addr + " = " + result);
      return (long)result;
    }

    public override string ReadAsicRegister(string sJade, string sAsic, string sMem, bool is24bit, Ice.Current current__) {
      String result = sidecar.ReadAsicRegister(sJade, sAsic, sMem, is24bit);
      logger.Debug("ReadAsicRegister called: result: " + sMem + " = " + result);
      return result;
    }

    public override string ReadAsicRegisterBlock(string sJade, string sAsic, bool is24bit, bool FIFOreset, int[] addrArray, out long[] valueArray, Ice.Current current__) {
      ushort[] addresses = new ushort[addrArray.Length];
      uint[] values = new uint[addrArray.Length];

      addresses = Array.ConvertAll(addrArray, new Converter<int, ushort>(intToushort));

      string result = sidecar.ReadAsicRegisterBlock(sJade, sAsic, ref addresses, ref values, is24bit, FIFOreset);

      logger.Debug(values[0] + ", " + values[1]);

      //addrArray = Array.ConvertAll(addresses, new Converter<ushort, int>(ushortToint));
      valueArray = Array.ConvertAll(values, new Converter<uint, long>(uintTolong));
      logger.Debug("ReadAsicRegisterBlock called: result = " + result);
      return result;
    }


    public override long ReadJadeReg(string sJade, long addr, Ice.Current current__) {
      uint result = sidecar.ReadJadeReg(sJade, (uint)addr);
      logger.Debug("ReadJadeReg called: result: " + addr + " = " + result);
      return (long)result;
    }

    public override string ReadJadeRegister(string sJade, string sMem, Ice.Current current__) {
      String result = sidecar.ReadJadeRegister(sJade, sMem);
      logger.Debug("ReadJadeRegister called: result: " + sMem + " = " + result);
      return result;
    }

    public override string ReadScienceData(string sJade, int numWords, out int[] targetArray, Ice.Current current__) {
      DateTime start = DateTime.Now;
      ushort[] targets = new ushort[numWords];
      logger.Debug("numWords = " + numWords);
      String result = sidecar.ReadScienceData(sJade, numWords, ref targets);
      //String result = "done";
      targetArray = Array.ConvertAll(targets, new Converter<ushort, int>(ushortToint));
      logger.Debug("ReadScienceData called: result = " + result);
      DateTime end = DateTime.Now;
      TimeSpan span = end - start;
      logger.Debug("time elapsed = " + span.ToString());
      return result;
    }

    public override long ReadUSBPort(string sJade, long Address, Ice.Current current__) {
      uint result = sidecar.ReadUSBPort(sJade, (uint)Address);
      logger.Debug("ReadUSBPort called: result = " + result);
      return (long)result;
    }

    public override long ReadUSBPortDirectionAsLong(string sJade, long addr, Ice.Current current__) {
      uint result = sidecar.ReadUSBPortDirection(sJade, (uint)addr);
      logger.Debug("ReadUSBPortDirectionAsLong called: result = " + result);
      return (long)result;
    }

    public override string ReadUSBPortDirectionAsString(string sJade, string addr, Ice.Current current__) {
      String result = sidecar.ReadUSBPortDirection(sJade, addr);
      logger.Debug("ReadUSBPortDirectionAsString called: result = " + result);
      return result;
    }

    public override string SetAutoWrite(int state, Ice.Current current__) {
      String result = sidecar.SetAutoWrite(state);
      logger.Debug("SetAutoWrite called: result = " + result);
      return result;
    }

    public override string SetBlockMemoryMode(int mode, Ice.Current current__) {
      String result = sidecar.SetBlockMemoryMode(mode);
      logger.Debug("SetBlockMemoryMode called: result = " + result);
      return result;
    }

    public override string SetDMA(int state, Ice.Current current__) {
      String result = sidecar.SetDMA(state);
      logger.Debug("SetDMA called: result = " + result);
      return result;
    }

    public override string UploadASICRegisters(string sJade, string sAsic, Ice.Current current__) {
      String result = sidecar.UploadASICRegisters(sJade, sAsic);
      logger.Debug("UploadASICRegisters called: result = " + result);
      return result;
    }

    public override string UploadJADERegisters(string sJade, Ice.Current current__) {
      String result = sidecar.UploadJADERegisters(sJade);
      logger.Debug("UploadJADERegisters called: result = " + result);
      return result;
    }

    public override string WriteAsicReg(string sJade, string sAsic, long addr, long Value, Ice.Current current__) {
      logger.Debug("WriteAsicReg writing " + longToHexString(Value) + " to address " + longToHexString(addr));
      String result = sidecar.WriteAsicReg(sJade, sAsic, (uint)addr, (uint)Value);
      logger.Debug("WriteAsicReg called: result = " + result);
      return result;
    }

    public override string WriteAsicRegister(string sJade, string sAsic, string addr, string Value, Ice.Current current__) {
      logger.Debug("WriteAsicRegister writing " + Value + " to address " + addr);
      String result = sidecar.WriteAsicRegister(sJade, sAsic, addr, Value);
      logger.Debug("WriteAsicRegister called: result = " + result);
      return result;
    }

    public override string WriteAsicRegisterBlock(string sJade, string sAsic, bool is24bit, int[] addrArray, long[] valueArray, Ice.Current current__) {
      ushort[] addresses = new ushort[addrArray.Length];
      uint[] values = new uint[valueArray.Length];
      addresses = Array.ConvertAll(addrArray, new Converter<int, ushort>(intToushort));
      values = Array.ConvertAll(valueArray, new Converter<long, uint>(longTouint));

      String result = sidecar.WriteAsicRegisterBlock(sJade, sAsic, ref addresses, ref values, is24bit);
      logger.Debug("WriteAsicRegisterBlock called: result = " + result);
      return result;
    }

    public override string WriteJadeReg(string sJade, long addr, long Value, Ice.Current current__) {
      logger.Debug("WriteJadeReg writing " + longToHexString(Value) + " to address " + longToHexString(addr));
      String result = sidecar.WriteJadeReg(sJade, (uint)addr, (uint)Value);
      logger.Debug("WriteJadeReg called: result = " + result);
      return result;
    }

    public override string WriteJadeRegister(string sJade, string sMem, string Value, Ice.Current current__) {
      logger.Debug("WriteAsicRegister writing " + Value + " to address " + sMem);
      String result = sidecar.WriteJadeRegister(sJade, sMem, Value);
      logger.Debug("WriteJadeRegister called: result = " + result);
      return result;
    }

    public override string WriteScienceData(string sJade, int[] targetArray, Ice.Current current__) {
      ushort[] targets = new ushort[targetArray.Length];
      targets = Array.ConvertAll(targetArray, new Converter<int, ushort>(intToushort));
      String result = sidecar.WriteScienceData(sJade, ref targets);

      logger.Debug("WriteScienceData called: result = " + result);
      return result;
    }

    public override string WriteUSBPort(string sJade, long Address, byte Data, Ice.Current current__) {
      String result = sidecar.WriteUSBPort(sJade, (uint)Address, Data);
      logger.Debug("WriteUSBPort called: result = " + result);
      return result;
    }

    public override string WriteUSBPortDirection(string sJade, long Address, byte Data, Ice.Current current__) {
      String result = sidecar.WriteUSBPortDirection(sJade, (uint)Address, Data);
      logger.Debug("WriteUSBPortDirection called: result = " + result);
      return result;
    }
  }
}
