using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Sidecar
{
	public class SidecarControlConfiguration
	{

    private const int ENABLE_VERTICAL_RPS_MASK = 0x1;
    private const int ENABLE_HORIZONTAL_RPS_MASK = 0x4;
    private const int VERTICAL_RPS_TYPE_MASK = 0x2;
    private const int HORIZONTAL_RPS_TYPE_MASK = 0x8;
    private const int RUNNING_AVERAGE_ROWS_MASK = 0xFFF0;
    private int defaultRunningAverageRows;
    
    private bool doVerticalRPS;
    public bool DoVerticalRPS { 
      get {return doVerticalRPS;}
    }
    private bool verticalRPSIsChannel;
    public bool VerticalRPSIsChannel {
      get { return verticalRPSIsChannel; }
    }
    private bool doHorizontalRPS;
    public bool DoHorizontalRPS { 
      get {return doHorizontalRPS;}
    }
    private bool horizontalRPSIsRunning;
    public bool HorizontalRPSIsRunning { 
      get {return horizontalRPSIsRunning;}
    }
    private int runningAverageRows;
    public int RunningAverageRows { 
      get {return runningAverageRows;}
    }

    public ushort ignoredInitialReads;
    public ushort IgnoredInitialReads {
      get { return ignoredInitialReads; }
      set { ignoredInitialReads = value; }
    }

    public bool codeVersionHxRG;
    public bool CodeVersionHxRG {
      get { return codeVersionHxRG; }
      set { codeVersionHxRG = value; }
    }

    public bool writeRawFrames;
    public bool WriteRawFrames {
        get { return writeRawFrames; }
        set { writeRawFrames = value; }
    }

    public bool writeRawGroupFrames;
    public bool WriteRawGroupFrames {
      get { return writeRawGroupFrames; }
      set { writeRawGroupFrames = value; }
    }

    public bool writeNoiseExtension;
    public bool WriteNoiseExtension {
      get { return writeNoiseExtension; }
      set { writeNoiseExtension = value; }
    }

    public bool writeQualityExtension;
    public bool WriteQualityExtension {
      get { return writeQualityExtension; }
      set { writeQualityExtension = value; }
    }

    public bool useEmptyPrimaryHeaderUnit;
    public bool UseEmptyPrimaryHeaderUnit
    {
      get { return useEmptyPrimaryHeaderUnit; }
      set { useEmptyPrimaryHeaderUnit = value; }
    }
    public string fitsWritingInProgressExtension;
    public string FitsWritingInProgressExtension {
      get { return fitsWritingInProgressExtension; }
      set { fitsWritingInProgressExtension = value; }
    }

    public string badPixelMapFilename;
    public string BadPixelMapFilename {
      get { return badPixelMapFilename; }
      set { badPixelMapFilename = value; }
    }

    public string readVarianceFilename;
    public string ReadVarianceFilename {
      get { return readVarianceFilename; }
      set { readVarianceFilename = value; }
    }

    public string linearizationFilename;
    public string LinearizationFilename {
      get { return linearizationFilename; }
      set { linearizationFilename = value; }
    }
    public string sidecarApplication;
    public string SidecarApplication {
      get { return sidecarApplication; }
      set { sidecarApplication = value; }
    }

    public string sidecarConfigurationFile;
    public string SidecarConfigurationFile {
      get { return sidecarConfigurationFile; }
      set { sidecarConfigurationFile = value; }
    }

    public bool autosetVrefMain;
    public bool AutosetVrefMain {
      get { return autosetVrefMain; }
      set { autosetVrefMain = value; }
    }

    public string heartbeatType;
    public string HeartbeatType {
        get { return heartbeatType; }
        set { heartbeatType = value; }
    }

    public int heartbeatPeriod;
    public int HeartbeatPeriod {
      get { return heartbeatPeriod; } 
      set { heartbeatPeriod = value; }
    }

    public int defaultReferencePixelSubtractionType;
    public int DefaultReferencePixelSubtractionType {
      get { return defaultReferencePixelSubtractionType; }
      set { defaultReferencePixelSubtractionType = value; }
    }

    public bool referencePixelSubtractMedian;
    public bool ReferencePixelSubtractMedian {
      get { return referencePixelSubtractMedian; }
      set { referencePixelSubtractMedian = value; }
    }

    public int detectorRotation;
    public int DetectorRotation {
      get { return detectorRotation; }
      set { detectorRotation = value; }
    }

    public int diskSpaceLeftWarningMB;
    public int DiskSpaceLeftWarningMB {
      get { return diskSpaceLeftWarningMB; }
      set { diskSpaceLeftWarningMB = value; }
    }

    public int fitsHeaderTimeoutSeconds;
    public int FitsHeaderTimeoutSeconds {
      get { return fitsHeaderTimeoutSeconds; }
      set { fitsHeaderTimeoutSeconds = value; }
    }

    public void setReferencePixelSubtractionType(int type) {
      //. type for hoizontal and vertical RPS are encoded in type
      doVerticalRPS = ((type & SidecarControlConfiguration.ENABLE_VERTICAL_RPS_MASK) == SidecarControlConfiguration.ENABLE_VERTICAL_RPS_MASK);
      doHorizontalRPS = ((type & SidecarControlConfiguration.ENABLE_HORIZONTAL_RPS_MASK) == SidecarControlConfiguration.ENABLE_HORIZONTAL_RPS_MASK);
      verticalRPSIsChannel = ((type & SidecarControlConfiguration.VERTICAL_RPS_TYPE_MASK) == SidecarControlConfiguration.VERTICAL_RPS_TYPE_MASK);
      horizontalRPSIsRunning = ((type & SidecarControlConfiguration.HORIZONTAL_RPS_TYPE_MASK) == SidecarControlConfiguration.HORIZONTAL_RPS_TYPE_MASK);

      int tempRunningAverageRows = (type & SidecarControlConfiguration.RUNNING_AVERAGE_ROWS_MASK) >> 4;

      if (tempRunningAverageRows != 0) {
        if (runningAverageRows > 2048) {
          throw new SidecarControlException("Invalid reference pixel subtraction type: running average rows cannot be larger than 2048");
        } else {
          runningAverageRows = tempRunningAverageRows;
        }
      } else {
        runningAverageRows = (defaultReferencePixelSubtractionType & SidecarControlConfiguration.RUNNING_AVERAGE_ROWS_MASK) >> 4;
        if (runningAverageRows == 0) {
          runningAverageRows = defaultRunningAverageRows;
        }
      }
    }

    public string getVerticalReferencePixelSubtractionType() {
      return (doVerticalRPS ? (verticalRPSIsChannel ? "channel" : "column") : "none"); 
    }

    public string getHorizontalReferencePixelSubtractionType() {
      return (doHorizontalRPS ? (horizontalRPSIsRunning ? "running-"+runningAverageRows.ToString() : "row") : "none");
    }

    //. todo: complete?
    public void validate() {
      if (ignoredInitialReads < 0) {
        throw new SidecarControlException("Invalid number of ignored initial reads: " + ignoredInitialReads + ". Must be non-negative.");
      }
      
      FileInfo fileInfo = new FileInfo(sidecarConfigurationFile);
      if (!fileInfo.Exists) {
        throw new SidecarControlException("Invalid Sidecar configuration file: " + sidecarConfigurationFile + ". File does not exist.");
      }

      if (heartbeatPeriod < 0) {
        throw new SidecarControlException("Invalid heartbeat period: " + heartbeatPeriod + ". Must be non-negative (units are ms).");
      }
      if (defaultReferencePixelSubtractionType > 0x800F) {
        throw new SidecarControlException("Invalid default reference pixel subtraction type: " + defaultReferencePixelSubtractionType + ". Must be less than 0x800F (32783) so that the number of row to use in running average is less than 2048.");
      }
    }
    public SidecarControlConfiguration Clone() {
      return (SidecarControlConfiguration)this.MemberwiseClone();
    }

    public SidecarControlConfiguration(SidecarControlConfiguration other) : this() {
      autosetVrefMain = other.AutosetVrefMain;
      codeVersionHxRG = other.CodeVersionHxRG;
      ignoredInitialReads = other.ignoredInitialReads;
      badPixelMapFilename = other.BadPixelMapFilename;
      readVarianceFilename = other.ReadVarianceFilename;
      linearizationFilename = other.LinearizationFilename;
      sidecarApplication = other.SidecarApplication;
      sidecarConfigurationFile = other.SidecarConfigurationFile;
      writeNoiseExtension = other.WriteNoiseExtension;
      writeQualityExtension = other.WriteQualityExtension;
      writeRawFrames = other.WriteRawFrames;
      writeRawGroupFrames = other.WriteRawGroupFrames;
      heartbeatType = other.HeartbeatType;
      heartbeatPeriod = other.HeartbeatPeriod;   //. ms 
      defaultReferencePixelSubtractionType = other.DefaultReferencePixelSubtractionType;
      defaultRunningAverageRows = other.defaultRunningAverageRows;
      detectorRotation = other.DetectorRotation;
      referencePixelSubtractMedian = other.ReferencePixelSubtractMedian;
      diskSpaceLeftWarningMB = other.DiskSpaceLeftWarningMB;
      fitsHeaderTimeoutSeconds = other.FitsHeaderTimeoutSeconds;
      fitsWritingInProgressExtension = other.FitsWritingInProgressExtension;
    }

    public SidecarControlConfiguration() {
      autosetVrefMain = false;
      codeVersionHxRG = true;
      ignoredInitialReads = 0;
      badPixelMapFilename = "";
      readVarianceFilename = "";
      linearizationFilename = "";
      sidecarApplication = "";
      sidecarConfigurationFile = "C:\\sidecar\\HxRG_Config\\SidecarConfigDefaults.xml";
      writeNoiseExtension = false;
      writeQualityExtension = false;
      writeRawFrames = false;
      writeRawGroupFrames = false;
      heartbeatType = "none";
      heartbeatPeriod = 1000;   //. ms 
      defaultReferencePixelSubtractionType = 67;
      defaultRunningAverageRows = (defaultReferencePixelSubtractionType & SidecarControlConfiguration.RUNNING_AVERAGE_ROWS_MASK) >> 4;
      detectorRotation = 0;
      referencePixelSubtractMedian = true;
      diskSpaceLeftWarningMB = 3000;
      fitsHeaderTimeoutSeconds = 0;
      fitsWritingInProgressExtension = ".writing";
      useEmptyPrimaryHeaderUnit = true;
    }
	}
}
