using System;
using System.Collections.Generic;
using System.Text;

namespace Sidecar {
  public class SidecarConfiguration {

    //. range [H1RG, H2RG, H4RG], default: H2RG
    public string detectorType;
    public string DetectorType {
      get { return detectorType; }
      set { detectorType = value; }
    }

    //. range [1,2,4,16,32], default: 4, mosfire: 32
    //. allowed values depend on detectorType
    public int detectorOutputs;
    public int DetectorOutputs {
      get { return detectorOutputs; }
      set { detectorOutputs = value; }
    }

    //. range [1,2,4,8], default: 1
    public int numberChannelsAveraged;
    public int NumberChannelsAveraged {
      get { return numberChannelsAveraged; }
      set { numberChannelsAveraged = value; }
    }

    //. false=cold, default: false
    public bool operatingTemperatureWarm;
    public bool OperatingTemperatureWarm {
      get { return operatingTemperatureWarm; }
      set { operatingTemperatureWarm = value; }
    }

    //. false=don't, default: false
    public bool dokTCNoiseRemoval;
    public bool DokTCNoiseRemoval {
      get { return dokTCNoiseRemoval; }
      set { dokTCNoiseRemoval = value; }
    }

    //. false=reset once per frame, default: false
    public bool resetOncePerRow;
    public bool ResetOncePerRow {
      get { return resetOncePerRow; }
      set { resetOncePerRow = value; }
    }

    //. false=normal clocking, default: false
    public bool enhancedHorizontalClocking;
    public bool EnhancedHorizontalClocking {
      get { return enhancedHorizontalClocking; }
      set { enhancedHorizontalClocking = value; }
    }

    //. false=line-by-line/pixel-by-pixel reset, default: false
    public bool doGlobalReset;
    public bool DoGlobalReset {
      get { return doGlobalReset; }
      set { doGlobalReset = value; }
    }

    //. range [0-15], default: 8
    public int gainSetting;
    public int GainSetting {
      get { return gainSetting; }
      set { gainSetting = value; }
    }

    //. range [0-2], default: 2
    //. 0=shorted preamps
    //. 1=single
    //. 2=differential
    public int preampInputMode;
    public int PreampInputMode {
      get { return preampInputMode; }
      set { preampInputMode = value; }
    }

    //. false=unbuffered, default: false
    public bool useBufferedMode;
    public bool UseBufferedMode {
      get { return useBufferedMode; }
      set { useBufferedMode = value; }
    }

    //. range [0-14], default:5
    public int dataTransmissionMode;
    public int DataTransmissionMode {
      get { return dataTransmissionMode; }
      set { dataTransmissionMode = value; }
    }

    //. range [0,1,2,4], default: 0
    public int numberSerialChannels;
    public int NumberSerialChannels {
      get { return numberSerialChannels; }
      set { numberSerialChannels = value; }
    }

    //. range [0-3], default: 3
    public int scienceBlockHeaderLength;
    public int ScienceBlockHeaderLength {
      get { return scienceBlockHeaderLength; }
      set { scienceBlockHeaderLength = value; }
    }

    //. false=CMOS, default: true
    public bool dataOutputLevelLVDS;
    public bool DataOutputLevelLVDS {
      get { return dataOutputLevelLVDS; }
      set { dataOutputLevelLVDS = value; }
    }

    public int saturationLevel;
    public int SaturationLevel {
      get { return saturationLevel; }
      set { saturationLevel = value; }
    }


    private long readTimeInMicros;
    public long ReadTimeInMicros {
      get { return readTimeInMicros; }
      set { readTimeInMicros = value; }
    }

    private double pixelClockingSpeedkHz;
    public double PixelClockingSpeedkHz
    {
      get { return pixelClockingSpeedkHz; }
      set { pixelClockingSpeedkHz = value; }
    }

    public int DetectorTypeInt { get; set; }


    //. properties below cannot be set automatically with XML
    //. values obtained from registers
    public double VReset { get; set; }
    public double DSub { get; set; }
    public double VBiasGate { get; set; }
    public double VBiasPower { get; set; }
    public double CellDrain { get; set; }
    public double Drain { get; set; }
    public double VDDA { get; set; }
    public double VDD { get; set; }
    public double VRefMain { get; set; }
    public double VPreMidRef { get; set; }
    public double VPreAmpRef1 { get; set; }
    public string HXRG_code_version { get; set; }
    public string SidecarConfigurationDumpLine1 { get; set; }
    public string SidecarConfigurationDumpLine2 { get; set; }

    public static string[] preampInputModeTypes = { "Shorted to Ground", "Single Ended", "Differential" };
    public static string[] detectorGainTypes = {"-3.01 dB (low)",
                                                 "0.00 dB (low)",
                                                 "3.01 dB (low)",
                                                 "6.02 db (low)",
                                                 "6.02 db (high)",
                                                 "9.03 db (low)",
                                                 "9.03 db (high)",
                                                 "12.04 db (low)",
                                                 "12.04 db (high)",
                                                 "15.05 db (low)",
                                                 "15.05 db (high)",
                                                 "18.06 db (low)",
                                                 "18.06 db (high)",
                                                 "21.07 db (high)",
                                                 "24.08 db (high)",
                                                 "27.09 db (high)"};

    //. detectorGainFactor is dimensionless
    public static double[] detectorGainFactors = {0.70711,
                                                   1.00000,
                                                   1.41421,
                                                   2.00000,
                                                   2.00000,
                                                   2.82843,
                                                   2.82843,
                                                   4.00000,
                                                   4.00000,
                                                   5.65685,
                                                   5.65685,
                                                   8.00000,
                                                   8.00000,
                                                   11.31371,
                                                   16.00000,
                                                   22.62742};
    //. systemGainFactor is in e-/ADU
    //. s = Vfs * C / 2^n* Ag * e
    //. s = system gain
    //. Vfs = front side voltage = 3V
    //. C = capacitnace = 4.25 E-14 farads
    //. n = 16 bits
    //. Ag = detectorGainFactor
    //. e = charge of electron = 1.602 E-19 columbs
    //.
    //. --> s = (3 * 4.25 * 10^-14) / 2^16 * Ag * 1.602 * 10^-19
    //.       = 12.144 / Ag

    public static double[] systemGainFactors = {17.17,
                                                12.14,
                                                 8.59,
                                                 6.07,
                                                 6.07,
                                                 4.29,
                                                 4.29,
                                                 3.04,
                                                 3.04,
                                                 2.15,
                                                 2.15,
                                                 1.52,
                                                 1.52,
                                                 1.07,
                                                 0.76,
                                                 0.54};

    //. the following voltages are calculated using 1.0499 for gain setting 13, and 
    //. 1.2512 for gain setting 8, and then scaling for other values.
    //. hex value is obtained using the formula:
    //. for values in this range (< 2V), V(hex) = round(V*1023/2)
    //. these values should be set in bits 9-0
    public static int[] vRefMainVoltagesForGain = {  0x32c, //. 1.5875
                                                     0x309, //. 1.5191
                                                     0x2e7, //. 1.4526
                                                     0x2c5, //. 1.3861
                                                     0x2c5, //. 1.3861
                                                     0x2a2, //. 1.3177
                                                     0x2a2, //. 1.3177
                                                     0x280, //. 1.2512
                                                     0x280, //. 1.2512
                                                     0x25e, //. 1.1848
                                                     0x25e, //. 1.1848
                                                     0x23b, //. 1.1163
                                                     0x23b, //. 1.1163
                                                     0x219, //. 1.0499
                                                     0x1f7, //. 0.9834
                                                     0x1d4  //. 0.9150
                                                  };

    public static string[] dataTransmissionModeTypes =  {"Powerdown of all Output pad buffers",
                                                          "Serial read, 16 bit data", 
                                                          "Serial read, 24 bit data split into two 1", 
                                                          "Serial read, 20 bit data", 
                                                          "Serial read, 24 bit data", 
                                                          "Parallel read, 8 bit bus, 16 bit data", 
                                                          "Parallel read, 8 bit bus, 24 bit data", 
                                                          "Parallel read, 8 bit bus, 16 bit data,one data word per clk edge (double speed)", 
                                                          "Parallel read, 12 bit bus, 12 bit data", 
                                                          "Parallel read, 12 bit bus, 24 bit data", 
                                                          "Parallel read, 12 bit bus, 24 bit data, one data word per clk edge (double speed)", 
                                                          "Parallel read, 16 bit bus, 16 bit data", 
                                                          "Parallel read, 24 bit bus, 24 bit data", 
                                                          "Parallel read, 32 bit bus, 24 bit data", 
                                                          "Parallel read, 32 bit bus, 16 bit data"
                                                        };

    public void validateConfiguration() {
      if (detectorType.ToUpper().CompareTo("H1RG") == 0) {
        DetectorTypeInt = 1;
        if ((detectorOutputs != 1) && (detectorOutputs != 2) && (detectorOutputs != 16)) {
          throw new SidecarConfigurationException("Invalid number of outputs: " + detectorOutputs + ". Must be either 1, 2, or 16 for H1RG.");
        }
      } else if (detectorType.ToUpper().CompareTo("H2RG") == 0) {
        DetectorTypeInt = 2;
        if ((detectorOutputs != 1) && (detectorOutputs != 4) && (detectorOutputs != 32)) {
          throw new SidecarConfigurationException("Invalid number of outputs: " + detectorOutputs + ". Must be either 1, 4, or 32 for H2RG.");
        }
      } else if (detectorType.ToUpper().CompareTo("H4RG") == 0) {
        DetectorTypeInt = 4;
        if ((detectorOutputs != 1) && (detectorOutputs != 4) && (detectorOutputs != 16) && (detectorOutputs != 32)) {
          throw new SidecarConfigurationException("Invalid number of outputs: " + detectorOutputs + ". Must be either 1, 4, 16, or 32 for H4RG.");
        }
      } else {
        throw new SidecarConfigurationException("Invalid detector type: " + detectorType + ". Must be one of H1RG, H2RG, or H4RG.");
      }

      if (detectorOutputs > 4) {
        if (numberChannelsAveraged != 1) {
          throw new SidecarConfigurationException("Invalid number of channels averaged: " + numberChannelsAveraged + ". Must be 1 for " + detectorOutputs + " outputs.");
        }
      } else {
        if ((numberChannelsAveraged != 1) && (numberChannelsAveraged != 2) && (numberChannelsAveraged != 4) && (numberChannelsAveraged != 8)) {
          throw new SidecarConfigurationException("Invalid number of channels averaged: " + numberChannelsAveraged + ". Must be 1, 2, 4, or 8");
        }

      }

      if ((gainSetting < 0) || (gainSetting > 15)) {
        throw new SidecarConfigurationException("Invalid gain setting: " + gainSetting + ". Must be between 0 and 15.");
      }

      if ((preampInputMode < 0) || (preampInputMode > 2)) {
        throw new SidecarConfigurationException("Invalid preamp input mode: " + preampInputMode +". Must be 0 ("+preampInputModeTypes[0]+"), 1 ("+preampInputModeTypes[1]+"), or 2 ("+preampInputModeTypes[2]+").");

      }

      if ((dataTransmissionMode < 0) || (dataTransmissionMode > 14)) {
        throw new SidecarConfigurationException("Invalid data transmission mode: " + dataTransmissionMode + ". Must be between 0 and 14.");
      }

      if ((numberSerialChannels < 0) || (numberSerialChannels > 4) || (numberSerialChannels == 3)) {
        throw new SidecarConfigurationException("Invalid number of serial channels: " + numberSerialChannels + ". Must be between 0, 1, 2, or 4.");
      }

      if ((scienceBlockHeaderLength < 0) || (scienceBlockHeaderLength > 3)) {
        throw new SidecarConfigurationException("Invalid science block header length: " + scienceBlockHeaderLength + ". Must be between 0 and 3.");
      }

    }

    public override bool Equals(object obj) {
      //.  template from http://msdn.microsfot.com/en-us/library/ms173147(VS.80).aspx

      //. if parameter is null return false
      if (obj == null) {
        return false;
      }

      //. if parameter cannot be cast to SidecarConfiguration return false
      SidecarConfiguration other = obj as SidecarConfiguration;
      if ((System.Object)other == null) {
        return false;
      }
      
      //. check values of fields.
      //. here, we only check values that are set with XML
      return (detectorType.Equals(other.DetectorType) &&
        (detectorOutputs == other.DetectorOutputs) &&
        (numberChannelsAveraged == other.NumberChannelsAveraged) &&
        (operatingTemperatureWarm == other.OperatingTemperatureWarm) &&
        (dokTCNoiseRemoval == other.DokTCNoiseRemoval) &&
        (resetOncePerRow == other.ResetOncePerRow) &&
        (enhancedHorizontalClocking == other.EnhancedHorizontalClocking) &&
        (doGlobalReset == other.DoGlobalReset) &&
        (gainSetting == other.GainSetting) &&
        (preampInputMode == other.PreampInputMode) &&
        (useBufferedMode == other.UseBufferedMode) &&
        (dataTransmissionMode == other.DataTransmissionMode) &&
        (numberSerialChannels == other.NumberSerialChannels) &&
        (scienceBlockHeaderLength == other.ScienceBlockHeaderLength) &&
        (dataOutputLevelLVDS == other.DataOutputLevelLVDS));
    }
    public override int GetHashCode() {
      return base.GetHashCode();
    }

    public SidecarConfiguration Clone() {
      return (SidecarConfiguration)this.MemberwiseClone();
    }
    public SidecarConfiguration(SidecarConfiguration other)
      : this() {
      DetectorType = other.DetectorType;
      DetectorOutputs = other.DetectorOutputs;
      NumberChannelsAveraged = other.NumberChannelsAveraged;
      OperatingTemperatureWarm = other.OperatingTemperatureWarm;
      DokTCNoiseRemoval = other.DokTCNoiseRemoval;
      ResetOncePerRow = other.ResetOncePerRow;
      EnhancedHorizontalClocking = other.EnhancedHorizontalClocking;
      DoGlobalReset = other.DoGlobalReset;
      GainSetting = other.GainSetting;
      PreampInputMode = other.PreampInputMode;
      UseBufferedMode = other.UseBufferedMode;
      DataTransmissionMode = other.DataTransmissionMode;
      NumberSerialChannels = other.NumberSerialChannels;
      ScienceBlockHeaderLength = other.ScienceBlockHeaderLength;
      DataOutputLevelLVDS = other.DataOutputLevelLVDS;
    }
    public SidecarConfiguration() {
      detectorType = "H2RG";
      DetectorTypeInt = 2;
      detectorOutputs = 4;
      numberChannelsAveraged = 1;
      operatingTemperatureWarm = false;
      dokTCNoiseRemoval = false;
      resetOncePerRow = false;
      enhancedHorizontalClocking = false;
      doGlobalReset = false;
      gainSetting = 8;
      preampInputMode = 2;
      useBufferedMode = false;
      dataTransmissionMode = 5;
      numberSerialChannels = 0;
      scienceBlockHeaderLength = 3;
      dataOutputLevelLVDS = true;
      saturationLevel = 65536;

      VReset = 0.0;
      DSub = 0.0;
      VBiasGate = 0.0;
      VBiasPower = 0.0;
      CellDrain = 0.0;
      VDDA = 0.0;
      VDD = 0.0;
      VRefMain = 0.0;
      VPreMidRef = 0.0;
      VPreAmpRef1 = 0.0;
      HXRG_code_version = "";

      ReadTimeInMicros = 1000000;
      pixelClockingSpeedkHz = 0;
    }
  }
}
