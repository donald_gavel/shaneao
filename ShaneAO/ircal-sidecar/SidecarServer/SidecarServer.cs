using System;
using Sidecar.SidecarIce;
using Sidecar;
using log4net;
using log4net.Config;

public class SidecarServer {
  private static readonly ILog logger = LogManager.GetLogger(typeof(SidecarServer));

  public static void Main(string[] args) {

    int status = 0;
    Ice.Communicator ic = null;
    Ice.Object obj;
    string log4netConfigFile = "SidecarIce.log4net";
    string sidecarConfigFile = "";
    string[] iceArgs;
    int numIceArgs=0;
    int port=10051;

    //. test code begin

    //. test code end


    XmlConfigurator.Configure(new System.IO.FileInfo(log4netConfigFile));


    iceArgs = new String[args.Length];

    //. look for server config argument
    foreach (string arg in args) {
      if (arg.StartsWith("-sidecarConfig=")) {
        sidecarConfigFile = arg.Substring(arg.IndexOf('=') + 1);
      } else if (arg.StartsWith("-log4netConfig=")) {
        log4netConfigFile = arg.Substring(arg.IndexOf('=') + 1);
      } else if (arg.StartsWith("-serverPort=")) {
        port = Int32.Parse(arg.Substring(arg.IndexOf('=') + 1));
      } else {
        iceArgs[numIceArgs] = arg;
        numIceArgs++;
      }
    }
    System.Array.Resize(ref iceArgs, numIceArgs);

    try {
      Ice.Properties props = Ice.Util.createProperties(ref iceArgs);

      // increase maximum message size large enough to
      //. return image data (512x512x4 + overhead)
      props.setProperty("Ice.MessageSizeMax", "16388");
      /* props.setProperty("Ice.ACM.Client", 0); */

      Ice.InitializationData id = new Ice.InitializationData();
      id.properties = props;

      ic = Ice.Util.initialize(id);

      Ice.ObjectAdapter adapter = ic.createObjectAdapterWithEndpoints("SidecarAdapter", "default -p "+ port);
      
      if (sidecarConfigFile.CompareTo("") != 0) {
         obj = new SidecarI(ic, sidecarConfigFile);
      } else {
         obj = new SidecarI(ic);
      }
      adapter.add(obj, ic.stringToIdentity("SidecarIce"));
      adapter.activate();


      //. set up a Control-C handler to shutdown communicator
      Console.CancelKeyPress +=
        delegate(object sender, ConsoleCancelEventArgs a) {
          Console.WriteLine("Control-C hit. Shutting down Ice Communicator...");
          if (ic != null) {
            try {
              if (ic.isShutdown()) {
                logger.Debug("Communicator already shutdown.  Exiting...");
                System.Environment.Exit(0);

              } else {
                if (obj != null) {
                  ((SidecarI)obj).shutdown();
                }
                ic.shutdown();
              }
            } catch (Ice.CommunicatorDestroyedException) {
              logger.Debug("Communicator destroyed.  Exiting...");
              System.Environment.Exit(0);
            }
          }
          //. don't terminate process here
          a.Cancel = true;
        };


      logger.Info("Server (v120810) started on port " + port + " - waiting for client");
      ic.waitForShutdown();

    } catch (Exception e) {
      logger.Error(e);
      status = -1;
    }

    Console.WriteLine("Ice Communicator shutdown.");
    if (ic != null) {
      // Clean up
      //
      try {
        ic.destroy();
      } catch (Exception e) {
        logger.Fatal(e);
        status = -2;
      }
    }
    Console.WriteLine("status = " + status);
    System.Threading.Thread.Sleep(3000);

    System.Environment.Exit(status);
  }
}
