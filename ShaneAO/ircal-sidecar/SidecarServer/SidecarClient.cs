
using System;
using Sidecar;

public class SidecarClient
{
    public static void Main(string[] args)
    {
        int status = 0;
        Ice.Communicator ic = null;
        try {
            ic = Ice.Util.initialize(ref args);
            Ice.ObjectPrx obj = ic.stringToProxy(
                    "Sidecar:default -p 10050");
            ISidecarComPrx sidecarProxy = ISidecarComPrxHelper.checkedCast(obj);
            if (sidecarProxy == null)
                throw new ApplicationException("Invalid proxy");

            int[] addrArray = new int[2] { 0x4008, 0x4009 };
            long [] valueArray = new long[2];
            
            Console.WriteLine("Attempting to read from 4008, 4009");
            Console.WriteLine(sidecarProxy.ReadAsicRegisterBlock("JADE1", "ASIC1", false, false, addrArray, out valueArray));
            if (valueArray == null || valueArray.Length != 2)
                Console.WriteLine("Read failed, return is null or incorrect length");
            else
            {
                int index = 0;
                foreach (long element in valueArray)
                    Console.WriteLine("Register {0} has value {1}", addrArray[index++], element);
            }

            Console.WriteLine("Attempting write of 5 and 7 to 4008, 4009");
            long [] writeArray = new long[2] { 6, 8 };
            Console.WriteLine(sidecarProxy.WriteAsicRegisterBlock("JADE1", "ASIC1", false, addrArray, writeArray));
            Console.WriteLine("Attempting to re-read from 4008, 4009");
            Console.WriteLine(sidecarProxy.ReadAsicRegisterBlock("JADE1", "ASIC1", false, false, addrArray, out valueArray));
            if (valueArray == null || valueArray.Length != 2)
                Console.WriteLine("Read failed, return is null or incorrect length");
            else
            {
                int index = 0;
                foreach (long element in valueArray)
                    Console.WriteLine("Index {0} has value {1}", addrArray[index++], element);
            }
           
            Console.Read();
        } catch (Exception e) {
            Console.Error.WriteLine(e);
            status = 1;
        }
        if (ic != null) {
            // Clean up
            //
            try {
                ic.destroy();
            } catch (Exception e) {
                Console.Error.WriteLine(e);
                status = 1;
            }
        }
        if(status != 0)
        {
            System.Environment.Exit(status);
        }
    }
}
