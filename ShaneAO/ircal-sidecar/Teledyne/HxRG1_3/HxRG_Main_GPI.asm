// <HEADER>
// Company Name:    	Teledyne Imaging Sensors
// Company Address:   	5212 Verdugo Way, Camarillo, CA
// Project Name:      	HxRG
// Version Number:    	1.1
// Date:              	16:24:28 06/25/09
// Copyright:         	TIS Copyright 2009 (c)
// Author(s):		   	Lance Simms, Jing Chen
// **********************
// Export Statement:
// U.S. export law as contained in the International Traffic in Arms
// Regulations (ITAR) or the Department of Commerce Export Administration
// Regulations is applicable to the information contained in this document.
// This technical information is not to be placed in the public domain,
// exported from the U.S., or given to any foreign person in the U. S.
// without the prior specific written authorization of Teledyne Imaging
// Sensors and the U.S. Department of State or U.S. Department of
// Commerce, as applicable.
// **********************
//Changes Addressed:
// SCR#321,	CR # 321,	Row timing is too long (x2 )
// A,	CR # 321,	Row timing is too long (x2 )
//**********************
// </HEADER>

//HxRG_Main.asm
//
//This is the main file for the HxRG instruction set. The include and 
//procedure files contain function definitions and arguments.
//
//INCLUDE THE DEFINITIONS FILE
.INCLUDE				HxRG_Def_User_Vals.asm
.INCLUDE				HxRG_Def_Static_Vals.asm
.INCLUDE				HxRG_Def_Addresses.asm
.INCLUDE				HxRG_Def_ClockTable.asm
.INCLUDE 			HxRG_Def_Cold_Biases.asm
.INCLUDE				HxRG_Load_Registers.asm
.INCLUDE				GPI_Voltages.asm

// shut down memory current
.LDREG h6916 h0800

// enable Hold_uC register to allow non-DMA access to instruction memory
.LDREG h6903 h0001
.LDREG h6928 h5fff
.LDREG h6928 haffd
.LDREG h6928 h5fff
.LDREG h6928 hafff

// enable dataout
.LDREG h6814 hc001    // LVDS -- Enable Probe bus and set buffer
.LDREG h6906 h55d7	 // Enable write protection for SEU Instruction Memory
.LDREG h6907 h87d5    // LVDS

// Set main clock divider --------------------------------Adjusted in HxRG_EnableDisableClocks.asm
.LDREG h6904 h0505  // uC clock = input clock 10MHz
.LDREG h6905 h0505  // divide ADC clock by 10 (5 + 5)

// Internal Analog Input Routing  ------------------------Adjusted in HxRG_EnableDisableClocks.asm
.LDREG h6190 h0580	// turn on switch 7,8,&10
.LDREG h6191 h9580	// turn on switch 7,8,&10

// setup serial interface
.LDREG h6817 h3487   // set to 3.3V levels
.LDREG h6818 h2000   // invert SCSB
.LDREG h6819 h0f40   // delay Sclk start

// setup pattern generator for HCLK ----------------------Adjusted in HxRG_EnableDisableClocks.asm
.LDREG h6419 h8600   // return signal to uC for triggering counter, 1MOhm pullup
.LDREG h641a h2a40   // pattern length = 9, enable pattern gen
.LDREG h641b h0fbf   // pattern 

//=================================================================================
//TIMERS AND TEST BUSES------------------------------------------------------------
#BeginSection CounterConfig
// Timer 0 setup -- LINE TIMER WITH HCLK
// setup pixel counter for waking up uC
.LDREG h6a00 h0000
.LDREG h6a02 h0040  // rollover - period actual number of pixels read
.LDREG h6a03 h0063  // select HCLK as the trigger (CLKIO3)

// Timer 1 setup -- LINE TIMER WITH ADCSTROBE
// Set to 1-line plus 2 pixel overhead
.LDREG h6a04 h0000
.LDREG h6a06 h0042  // 1 line + 2 pixels
.LDREG h6a07 h0086  // ADC Strobe, rollover 

// Exposure Time Counter -- Linked Counter Operation (Mode 2) 
// Link Counters 2 and 3
// Timer 2 setup
.LDREG h6a08 h0000  // set counter to 0 at startup
.LDREG h6a0a hffff  // set counter 2 to count 65536 events
.LDREG h6a0b h0106  // ADC Strobe, rollover prescalar to divide by 2

// Timer 3 setup
.LDREG h6a0c h0000  // set counter to 0 at startup
.LDREG h6a0e hffff  // counter to count 65536 events
.LDREG h6a0f h001f  // linked to counter 2 / disable rollover

// Timer 4 setup
.LDREG h6a10 h0000  // set counter to 0 at startup
.LDREG h6a12 h0015  // counter to count 1 HCLK/ADCStrobe for Enhanced Clocking
.LDREG h6a13 h0014  // select HCLK as trigger (CLKIO3)

//Enable the Counters 0,1 and 4 and enable prescalars
.LDREG h6a18 h020f  // enable counter + prescaler
#EndSection

#BeginSection Test_Bus_Init
// setup test bus
.LDREG h6a20 h8207	 // T0 = CounterRdy2 AND CounterRdy3: Exposure Counter, FunctionGenerator = B0 | A0 True
.LDREG h6a21 h2009	 // T1 = HCLK counter; use this for Enhanced Clocking
.LDREG h6a22 h0d0c	 // T2 = Sync[0] OR Sync [1]
.LDREG h6a24 h0006    // T2   Gives logical OR of Sync[0], Sync[1]
.LDREG h6a28 h1a0c    // T5 = ADCClk, T4 = Sync0
.LDREG h6a29 h0605    // T7 = CounterRdy1, T6 = CounterRdy0
#EndSection
//---------------------------------------------------------------------------------
//=================================================================================

// SETUP ADC strobe pulse////////////////////////////////////////////////////////
.LDREG h6910 hac78   //*
.LDREG h6916 h4800  //SAR Strobe, Vc = vdd for bit errors
.LDREG h6911 h2b04
.LDREG h6912 hf000  //* enable SAR
.LDREG h6913 h0500
.LDREG h6919 h2004  //10 cycle turnaround - necessary for averaging of channels
/////////////////////////////////////////////////////////////////////////////////

#BeginSection ProbeSignals
	//These are the signals coming off the test bus
	//0= SARADC_STROBE  			- Conversion Pulse for SAR ADC
	//1= PipeADC_STROBE 			- Conversion Pulse for Pipeline ADC
	//2= PreAmpAZ 		  			- AutoZero mode control for Pre-Amp
	//3= PreAmpToggle	  			- Toggle between two Pre-amp nodes
	//4= PixStrobe		  			- Pixel Strobe Signal
	//5= IF_PixStrobeRead		- Start Signal for New Pixel
	//6= RowStrobe					- Row Strobe Signal
	//7= IF_BlockStrobeRead		- Start Signal for a new block of Data
#EndSection		

//--------------------------------------------------------------------------------
//CONFIGURATION DATA - ON ACKNOWLEDGE LINE ---------------------------------------
.LDREG h6812 h0100    // enable read clock, ack telemetry

MAIN PROC

   //Necessary jump to h0100 for instructions to work
	NOP
	JMP				h0100,	ABS, LKY	
	.SETADDR 		h0100
	
	// Set memory leakage to minimum and write 000000 FFFFFF's to all locations
	MOVH	R6, 		h70
	MOVL	R6, 		h00
	SET	RX0
	CLR	RX1

	memloop:
	ST		(R6),		RX0		
	ST		(R6+1),	RX0	
	ST		(R6+2),	RX1	
	ST		(R6+3),	RX1	
	ADD	R6, 4
	JPNC	memloop,LKY
	
		// Set ADC Clk to operate AP
	MOVH	R6,h1e
	MOVL	R6,h10
	ST		(h690b),	R6
	// set memory precharge
	MOVH	R6,h78
	MOVL 	R6,h00
	ST		(h6916),	R6
	
	// Reconfigure the first time around so that the defaults are loaded
	JMP	Reconfigure,	LKY

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//IDLE MODE - Check for Reconfiguration or Expose ; If neither, reset or clock SCA
//---------------------------------------------------------------------------------------------
#BeginSection IDLE
StrtIdle:
   // Disable the Exposure Counters after getting back from exposure
   CLR	R7
	MOVL	R7,			   h0f					//Keep the HCLK and ADCStrobe Line Counters Alive
	MOVH	R7,				h02					//Keep the prescalar for the Enhanced Mode HCLK alive
   OUT	CountersPt,	   R7						//Stop the exposure time counter
	
	IN		R7,				SyncRegPt			//Take in the SyncReg through its port
	BCLR	R7,				ExpSyncBit			//Clear the bit for EXPOSE <0>
	BCLR	R7,				ReCfgSyncBit		//Clear the bit for RECONFIGURE <1>
	OUT	SyncRegPt,		R7						//Output this to the SyncReg to stay IDLE

   // Bring in the registers that tell us what to do in IDLE and store them in Shadow Register Bank B
	IN		R3,				OutModePt			// # h4002 <4>=rst-rd idle <3>=rst idle <2>=Clk SCA in drops <1>=pulse LED @ end of each frame <0>=Output Reset Frame Data
	IN		R1,				LnBlnkingPt			// # h4006 of extra pixels per line
	IN		R0,				FrmBlnkingPt		// # h4007 of extra lines per row
	SWST	RBL,				hb						// Put the configuration registers in shadow bank B

	ChkIdle:
	//Check for Reconfigure
	IN		R1,				SyncRegPt			//If h6900<0>=1, start an exposure
	BTST	R1,				ReCfgSyncBit		//If h6900<1>=1, reconfigure the SIDECAR and detector
	JPC	Reconfigure,	LKY	

	//Check for Exposure
	BTST	R1,				ExpSyncBit
	JPC	Expose,			LKY
	  
	//What should we do in IDLE mode?
	
	SWLD	RB,				h04					//Load IDLE MODE into R3
	BTST	R3,				IDLERstBit			//Check for IDLE resets with bit 3 of IDLE MODE
	JPC	IdleRst,			DLY
	SWLD	RB,				1						//Load frameblanking into R0
	BTST	R3,				IDLERstRdBit		//check for IDLE reset-reads with bit 4 of IDLE MODE
	JPC	IdleRst
	
	//If we do nothing in IDLE mode, wait until a sync bit is true ------------------------------
	WAITB T2, 				TRUE					// wait for sync bit<0> or <1> to become true
	
	//Check for Reconfigure if one of the bits becomes true
	IN		R1,				SyncRegPt			//If h6900<0>=1, start an exposure
	BTST	R1,				ReCfgSyncBit		//If h6900<1>=1, reconfigure the SIDECAR and detector
	JPC	Reconfigure,	LKY	
	
	//Check for Exposure if one of the bits becomes true
	BTST	R1,				ExpSyncBit
	JPC	Expose,			LKY
	JMP	StrtIdle,		LKY					//Go back to StrtIdle again
	//-------------------------------------------------------------------------------------------
	
	IdleRst:
	#BeginSection Frame_Control_Regs
	SWSWP	RA,				hff								//Store registers temporarily
	IN		R7,				FirstRstWRdClkInFrmPt		//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	   R6,				SecondRstWRdClkInFrmPt		//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN		R5,				ThirdStrobeRegR5Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	IN		R4,				SecondStrobeRegR4Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
   BCLR	R4,				IFBlockStrobeBit           //Do not output the data by default
	IN		R3,				FirstStrobeRegR3Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	CLR	R0										
	SWSWP	RA,				hff								//Put the exposure registers in shadow bank A
	WAITB	T7,				TRUE								//Wait until one of the sync lines is true
	
	#BeginSection	Idl_Rst_Blanking	
	CMP	R0,				0
	JPNZ	IdleRst,			DLY
	DEC	R0
	#EndSection
	
	IN		R0,				FrmProcedurePt					//Load the frame call address set by configuration
	CALL	R0,				LKY,DLY
	SWSWP	RA,				hff
	SWSWP	RA,				hff
	BTST	R3,				IDLERstRdBit					// check for reset-read idle
	JPNC	ChkIdle,			LKY
	
	#BeginSection Idle_Rd_Setup
	SWLD	RB,				1									// Load frameblanking into R0
	// setup for Idle Rd frame
	SWSWP	RA,				hd1								// update R7, R4, R0
	// no data output
	IN		R7,				FirstRstWRdClkInFrmPt		//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	   R6,				SecondRstWRdClkInFrmPt		//Determined in Configuration - HxRG_EnableDisableClocks.asm
	BCLR	R4,				IFBlockStrobeBit           //Do not output the data
	CLR	R0
	SWSWP	RA,				hd1
	#EndSection
	
	IdleRd:
	WAITB	T7,				T									// Wait 1 blank line
	#BeginSection	Idl_Rd_Blanking
	CMP	R0,				0
	JPNZ	IdleRd,			DLY
	DEC	R0
	#EndSection
	IN		R0,				FrmProcedurePt					//Load the frame call address set by configuration
	CALL	R0,				LKY,DLY
	SWSWP	RA,				hff
	SWSWP	RA,				hff
	#BeginSection Idle_Rd_CleanUp
	SWSWP	RA,				h51								// update R6, R4, & R0
	IN	   R6,				SecondRstWRdClkInFrmPt		//Determined in Configuration - HxRG_EnableDisableClocks.asm
	BSET	R4,				IFBlockStrobeBit
	CLR	R0
	SWSWP	RA,				h51
	#EndSection

	//Check for Reconfigure
	IN		R1,				SyncRegPt						//If h6900<0>=1, start an exposure
	BTST	R1,				ReCfgSyncBit					//If h6900<1>=1, reconfigure the SIDECAR and detector
	JPC	Reconfigure,	LKY	
	
	//Check for Exposure
	BTST	R1,				ExpSyncBit
	JPC	Expose,			LKY
	JMP	ChkIdle,			LKY								//Halt if prompted

#EndSection
#BeginSection RECONFIGURE
   //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//RECONFIGURATION LOOP;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   Reconfigure:
	CALL	ConfigureDataTransfer			//Configure science and configuration data transfers
	CALL	LoadDetectorSettings				//Load default serial register values for particular HxRG to addresses starting at HxRGSerReg1Addr
	CALL	PreAmpConfig						//Configure pre-amp reset mode 
	CALL	EnableDisableChannels			//Enable or Disable Channels for this mode
	CALL 	EnableDisableClocks           //Enable or Disable Clocks for this mode
   CALL  DetResetType                  //Enable or Disable Global Reset of Window and Full Frame
	CALL	PowerUpDownDetector			   //Power the detector up or down
	CALL	MainResetB							//Do a reset of the detector serial register
	CALL	SPIWriteDetectorSerialReg		//Write the HxRG serial registers with the values needed for this mode
	JMP	StrtIdle,	LKY
	
#EndSection
#BeginSection EXPOSE
	//EXPOSURE LOOPS;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	Expose:
	//Determine Exposure Type
	LD		R0,								(HxRGExpModeAddr)
	BTST	R0,								0
	JPC	FowlerExposureLoop,			LKY
	JPNC	UpTheRampExposureLoop,		LKY
	
#EndSection
#BeginSection FOWLER_EXPOSURE
	FowlerExposureLoop:
   //Take in the number of ramps to perform before going back to idle loop
	IN		R2,							NumRampsPt							// # h4009 of Number of Ramps per Exposure Loop 
	SWST	RBL,							h04									// Store this in R2 of shadow bank B
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//FOWLER EXPOSURE LOOP
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	FowlerExposure:															// Use only Counter 2 in Test Bus 0 if Fowler time is less than 1.31072 seconds
	LD		R1,		(FFCounter2Addr)										// Upper counter counts 1.31072 second intervals
	CMP	R1,		h00														// See if the time is smaller than 1.31072 seconds
	JPNZ	FowlerUseTwoCounters,		LKY								// If we are using two counters, then nothing changes
	
	FowlerUseOneCounter:
	MOV	R1,		h2007														// Inputs to TestBus 0 - CounterRdy2 - T0 = CounterRdy2  
	LD		R2,		(FFCounter1Addr)										// Set counter2 period to whatever is in FFCounter1Addr
	MOV	R3,		h0106  													// Do not link counters together; linking forces smallest period to 65536
	JMP	FowlerSetCounters,			LKY
	
	FowlerUseTwoCounters:
	MOV	R1,		h8207														// Inputs to TestBus 0 - CounterRdy2, CounterRdy3 - T0 = CounterRdy2 AND CounterRdy3
	MOV	R2,		hffff														// Need to set counter2 period full in order to link counters
	MOV	R3,		h001f														// Link the two counters together 
	FowlerSetCounters:
	ST    (h6a20), R1															// Write the TestBus0 register
	ST		(h6a0a),	R2															// Write the Counter2 period
	ST		(h6a0f),	R3															// Set the CounterConfig3 to either link or not link
	
   //..............Set up Exposure ...........//
	#BeginSection Fowler_Regs
		IN		R7,						NumRstFrmPt							// # h4000 of reset frames
		IN		R6,						NumGroupPt							// # h4004 of groups
		IN		R5,						NumRdFrmPt							// # h4001 of read frames
		IN		R4,						NumDropsPt							// # h4005 of drop frames
		IN		R3,						OutModePt							// # h4002 <4>=rst-rd idle <3>=rst idle <2>=Clk SCA in drops <1>=pulse LED @ end of each frame <0>=Output Reset Frame Data
		SWLD	RB,						h04									// # 40009 = Number of Ramps left to perform before idle
		DEC	R2																	//           Decrement by one
		IN		R1,						LnBlnkingPt							// # h4006 of extra pixels per line
		IN		R0,						FrmBlnkingPt						// # h4007 of extra lines per row
		SWSWP	RB,						hff									// Put the configuration registers in shadow bank B
	#EndSection
	#BeginSection Frame_Control_Regs
	   IN		R7,						FirstRstWRdClkInFrmPt			//Determined in Configuration - HxRG_EnableDisableClocks.asm
		IN	   R6,						SecondRstWRdClkInFrmPt			//Determined in Configuration - HxRG_EnableDisableClocks.asm
		IN		R5,						ThirdStrobeRegR5Pt				//Determined in Configuration - HxRG_PreAmpConfig.asm
		IN		R4,						SecondStrobeRegR4Pt				//Determined in Configuration - HxRG_PreAmpConfig.asm
		IN		R3,						FirstStrobeRegR3Pt				//Determined in Configuration - HxRG_PreAmpConfig.asm
		CLR	R0										
		SWSWP	RA,						hff									// Put the exposure registers in shadow bank A
	#EndSection
	
	FowlerResetConfig:
	SWLD	RB,							h08									// R3<0> = 1: Reset Data Out, = 0: No Reset Data Out 
	BTST	R3,							OutputRstBit						// output data for reset frame?
	JPC	FowlerResets,				LKY									// If Reset Data Output then Start Resets
	SWLD	RA,							h10									// Update R4 so that no data is output on reset
	BCLR	R4,							IFBlockStrobeBit					// no data output
	SWST	RAH,							h01
	
	FowlerResets:
	SWLD	RB,							h80									//Bring the number of resets left into R5
	CMP	R7,							h00									//See if any resets are left to be done
	JPZ	FirstFowlerPair,			LKY
	SWLD	RB,							h80
	DEC	R7
	SWST	RBH,							h8										//Put the remaining number of resets back in RB
	SWLD	RA,							hff
	IN		R0,							FrmProcedurePt						//Load the frame call address set by configuration
	CALL	R0
	JMP	FowlerResets
	
	FirstFowlerPair:
	IN		R7,							FirstRdClkInFrmPt					// Get the Read Clocks from memory
	IN	   R6,							SecondRdClkInFrmPt				// Get the Read Clocks from memory
	SWLD	RA,							h10									// Bring RA4 into R4 and set data output with IFBlockStrobe
	BSET	R4,							IFBlockStrobeBit								
	SWST	RAH,							hd										// Store these in the Shadow Bank RA
	
	FirstFowlerLoop:
	IN		R1,							SyncRegPt							// Check if 0 has been written to h6900<0> and halt is requested
	BTST	R1,							ExpSyncBit
	JPNC	StrtIdle																// Halt if prompted
	SWLD	RA,							hff
	IN		R0,							FrmProcedurePt						// Load the frame call address set by configuration
	CALL	R0
	SWLD	RB,							h20									// Bring the number of reads left into R5
	DEC	R5																		// Decrease the number of reads so that we count from 1 and not 0
	CMP	R5,							0
	JPNZ	FirstFowlerPair,			DLY,LKY								// Move to drop when all read frames are done
	SWST	RBH,							6										// Put R5 into RB5		
			
	StartFowlerWaitConfig:
	IN		R5,							NumRdFrmPt							// # h4001 of read frames
	SWST	RBH,							6										// Reset number of reads for next exposure			
	SWLD	RB,							h08									// R3<2> = 1: Clock SCA; 0: Don't Clock SCA
	BTST	R3,							ClkSCAInDropsBit					// Clock SCA during Fowler wait?
	JPC	StartFowlerExpCounter, 	LKY
	
	NoClockSCAInFowlerWaitStop:
	CALL	StopClocks

	StartFowlerExpCounter:
	//Load the Exposure Counters with the values in FFCounter1Addr and FFCounter2Addr
	IN		R7,				   	   	FFCounter1Pt						// Get the lower bits for the exposure counter 2
	OUT	Exp1PeriodPt,	  				R7
	IN		R7,								Exp1ConfigPt
	BCLR	R7,								7										// Set it to not rollover
	OUT	Exp1ConfigPt,					R7
	IN		R7,				   			FFCounter2Pt						// Get the lower bits for the exposure counter 3
	OUT	Exp2PeriodPt, 					R7
	IN		R7,								Exp2ConfigPt
	BCLR	R7,								7										// Set it to not rollover
	OUT	Exp2ConfigPt,					R7
	CLR	R7																			// Reset the counter to count until the exposure time
	OUT	Exp1CounterPt,					R7
	OUT	Exp2CounterPt,					R7
	MOV	R7,								h02ff
   OUT	CountersPt,						R7
	
	WaitForSecondFowlerPair:
	IN		R1,							SyncRegPt							// Check if 0 has been written to h6900<0> and halt is requested
	BTST	R1,							ExpSyncBit
	JPNC	StrtIdle
	SWLD	RA,								hff
	BCLR	R4,								IFBlockStrobeBit
	IN		R0,								FrmProcedurePt						// Load the frame call address set by configuration
	CALL	R0
	JPT0	ConfigureSecondFowlerPair,	LKY
	JMP	WaitForSecondFowlerPair,	LKY
	
	ConfigureSecondFowlerPair:
	SWLD	RB,								h08									// R3<2> = 1: Clock SCA; 0: Don't Clock SCA
	BTST	R3,								ClkSCAInDropsBit					// If the clocks were stopped during the Fowler wait, then restart them
	JPC	ReadyForSecondFowlerPair, 	LKY
	
	NoClockSCAInFowlerWaitStart:
	CALL	StartClocks

   ReadyForSecondFowlerPair:
	IN		R7,							FirstRdClkInFrmPt					// Get the Read Clocks from memory
	IN	   R6,							SecondRdClkInFrmPt				// Get the Read Clocks from memory
	SWLD	RA,							h10									// Bring RA4 into R4 and set data output
	BSET	R4,							IFBlockStrobeBit								
	SWST	RAH,							hd										// Store these in the Shadow Bank RA
	MOV	R7,							h001f									// Link the two counters together 
	ST		(h6a0f),						R7										// Set the CounterConfig3 to either link or not link
	SET	R7																		// Now set the Exposure Counter to 11 hours so that it does not interrupt
	OUT	Exp1PeriodPt,	  			R7										// Set the lower bits of the exposure counter to hffff
	OUT	Exp2PeriodPt,	  			R7										// Set the upper bits of the exposure counter to hffff
	CLR	R7																		// Reset the counter to count until the exposure time
	OUT	Exp1CounterPt,				R7										// Restart Exposure Counter 1
	OUT	Exp2CounterPt,				R7										// Restart Exposure Counter 2
	MOV	R7,							h02ff									// Activate all counters and prescalars
   OUT	CountersPt,					R7
	FCLR	T0																		// Clear the test bus so that it does not interrupt further frames
	
	SecondFowlerPair:
	IN		R1,							SyncRegPt							// Check if 0 has been written to h6900<0> and halt is requested
	BTST	R1,							ExpSyncBit
	JPNC	StrtIdle
	SWLD	RA,							hff
	IN		R0,							FrmProcedurePt						//Load the frame call address set by configuration
	CALL	R0
	SWLD	RB,							h20									//Bring the number of reads left into R5
	DEC	R5																		//Decrease the number of reads so that we count from 1 and not 0
	CMP	R5,							0
	JPNZ	SecondFowlerPair,			DLY,LKY								//Move to drop when all read frames are done
	SWST	RBH,							6										//Put R5 into RB5	

   //Stop the exposure counter for the second Fowler Pair and write it to memory
	CLR	R7
	MOVL	R7,			   			h2f
   OUT	CountersPt,	   			R7
	IN		R7,							Exp1CounterPt
	OUT	FFCounter3Pt,				R7
	IN		R7,							Exp2CounterPt
	OUT	FFCounter4Pt,				R7
	
	//Check to see if there are any more exposures to perform before going back to IDLE
	SWLD	RB,							h04									// # 40009 = Number of Ramps left to perform before idle
	CMP	R2,							h00
	JPNZ	FowlerExposure,			LKY			
	
	//Go back to IDLE if all exposures are finished					
	JMP	StrtIdle,					LKY
	
#EndSection
#BeginSection UPTHERAMP_GROUPS_EXPOSURE
   UpTheRampExposureLoop:
	//Take in the number of ramps to perform before going back to idle loop
	IN		R2,							NumRampsPt							// # h4009 of Number of Ramps per Exposure Loop 
	SWST	RBL,							h04									// Store this in R2 of shadow bank B
	
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	//UP THE RAMP CONFIGURATION
	//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	UpTheRampExposure:
	//Clear the exposure counter and Enable it on the first read
	SET	R7																//Set the Exposure Counter to 11 hours so that it does not interrupt
	OUT	Exp1PeriodPt,	  	R7										//Set the lower bits of the exposure counter to hffff
	OUT	Exp2PeriodPt,	  	R7										//Set the upper bits of the exposure counter to hffff
	CLR	R7
	OUT	Exp1CounterPt,		R7
	OUT	Exp2CounterPt,		R7
	MOV	R7,					h02ff
   OUT	CountersPt,			R7
	
	#BeginSection User_Group_Regs
		IN		R7,					NumRstFrmPt						// # h4000 of reset frames
		IN		R6,					NumGroupPt						// # h4004 of groups
		IN		R5,					NumRdFrmPt						// # h4001 of read frames
		IN		R4,					NumDropsPt						// # h4005 of drop frames
		IN		R3,					OutModePt						// # h4002 <4> rst-rd idle, <3> rst idle, <2> clkSCA in drop, <1> Pulse LED, <0> Output rst data
		SWLD	RB,					h04								// # 40009 = Number of Ramps left to perform before idle
		DEC	R2															//           Decrement by one
		IN		R1,					LnBlnkingPt						// # h4006 of extra pixels per line
		IN		R0,					FrmBlnkingPt					// # h4007 of extra lines per row
		SWSWP	RB,					hff
	#EndSection
	
	Ramp:
	#BeginSection Frame_Control_Regs
	   IN		R7,					FirstRdClkInFrmPt				//Determined in Configuration - HxRG_EnableDisableClocks.asm
		IN	   R6,					SecondRdClkInFrmPt			//Determined in Configuration - HxRG_EnableDisableClocks.asm
		IN		R5,					ThirdStrobeRegR5Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
		IN		R4,					SecondStrobeRegR4Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
		IN		R3,					FirstStrobeRegR3Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
		CLR	R0	
		SWSWP	RA,					hff
	#EndSection

	//*****************************************************************************************************
	//START OF RESETS LOOP
	ChkRst:
	#BeginSection	Reset_Init
	// Load NumRstFrm,OutMode,FrmBlnking for reset frame
	SWLD		RB,					h89								// R7=NumRstFrm,R3=OutMode,R0=FrmBlnking
	#BeginSection	Rst_Frm_Setup
	BTST		R3,					OutputRstBit					// output data for reset frame?
	JPC		RstOut,				DLY
	SWSWP		RA,					hd0								// Swap R4,R6,R7 with Shadow Bank A (take exp registers out)
	//No data output for reset
	BCLR		R4,					IFBlockStrobeBit
	RstOut:	
	IN			R7,					FirstRstWRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	  		R6,					SecondRstWRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	SWSWP		RA,					hd0								// Swap R4,R6,R7 with Shadow Bank A (put exp registers back)
	#EndSection
	
	RstFrm:
	SWLD		RB,					1									// Load frameblanking into R0	
	CLR		R1															// Clear the exposure counter so it's ready on first read
	OUT		Exp1CounterPt,		R1									// Restart Exposure Counter 1
	OUT		Exp2CounterPt,		R1									// Restart Exposure Counter 2
	CMP		R7,					0									// R7 = number of resets left to perform
	JPZ		Rdy4Grp													// Move to group when all reset frames are done
	#BeginSection Continue_useR1	
	IN			R1,					SyncRegPt						// Check if 0 has been written to h6900<0> and halt is requested
	BTST		R1,					ExpSyncBit
	JPNC		StrtIdle													// Halt if prompted
	#EndSection	

	RstBlnk:
	WAITB		T7,					T									// Wait 1 blank line
	#BeginSection	Rst_Blanking
	CMP		R0,					0
	JPZ		RstBlnk,				DLY
	DEC		R0
	#EndSection
	SWSWP		RA,					hff
	LD			R0,					(FrmProcedureAddr)			// Load the frame call address set by configuration
	CALL		R0,					LKY
	IN			R7,					FirstRstWRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	  		R6,					SecondRstWRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	SWSWP		RA,					hff
	JMP		RstFrm, 				LKY,	DLY	
	DEC		R7															// R7 -> NumResets = NumResets-1	(Remaining)

   //*****************************************************************************************************
	//BEGINNING OF READS/GROUPS - END OF RESETS
	//RX0, R5 Contain Number of Reads Left in Group
	//RX1, R6 Contain Number of Groups Left
	Rdy4Grp:
	#BeginSection Rst_CleanUp
	SWSWP		RA,					hd0								// Swap R4,R6,R7 with Shadow Bank A (take exp registers out)
	BSET		R4,					IFBlockStrobeBit				// Set bit for output of data
	IN			R7,					FirstRdClkInFrmPt				// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	   	R6,					SecondRdClkInFrmPt			// Determined in Configuration - HxRG_EnableDisableClocks.asm
	SWSWP		RA,					hd0								// Swap R4,R6,R7 with Shadow Bank A (put exp registers out)
	#EndSection
	#EndSection

	ChkGrp:
	#BeginSection	Group_Init
	// Load NumRdFrm,NumGrps,NumDrpFrm,OutMode,FrmBlnking
	SWLD		RB,					h79								// R6=NumGrps, R5=NumRdFrm, R4=NumDrpFrm, R3=OutMode-<2> is LED pulse, R0=FrmBlnking
	#BeginSection	Read_Frm_Setup								
	// setup for Read frame										
	BTST		R3,					LEDPulseBit						// R3=OutMode - <2> is LED pulse
	JPNC		GrpRmp,				DLY								// R0=FrmBlnking
	CLR		R2
	BSET		R2,					LEDSyncBit						// R2 contains Led Pulse
	#EndSection

   //A Group is finished, Check number of groups left in case only drops are being done
	GrpRmp:
	CMP		R6,					0									// See if groups are finished
	MOVX		RX1,					R6									// Store R6=NumGrps left in RX1 
	JPZ		StrtIdle,			LKY

	RdFrm:
	SWLD		RB,					1									// Load Frameblanking into R0	
	CMP		R5,					0									// Check if all of the reads are finished
	JPZ		ChkDrp													// Move to drop when all read frames are done
	IN			R1,					SyncRegPt						// Check if 0 has been written to h6900<0>
	BTST		R1,					ExpSyncBit
	JPNC		StrtIdle													// Halt if prompted

   //**************************************************************************************
	//DO GUIDE WINDOWS BEFORE READ FRAME?
	//SECTION NOT YET IMPLEMENTED
	//**************************************************************************************
	
	RdBlnk:																// Use RdBlnk to wait between reads
	WAITB		T7,					T									// Wait 1 blank line
	#BeginSection Rd_Blanking
	CMP		R0,					0
	JPNZ		RdBlnk,				DLY
	DEC		R0
	#EndSection
	SWSWP		RA,					hff
	IN			R0,					FrmProcedurePt					// Load the frame call address set by configuration
	CALL		R0,					LKY								// Start the frame
	IN			R7,					FirstRdClkInFrmPt				// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	  		R6,					SecondRdClkInFrmPt			// Determined in Configuration - HxRG_EnableDisableClocks.asm
	SWSWP		RA,					hff
	#BeginSection Rd_Led
	IN			R1,					SyncRegPt
	OR			R1,					R2
	OUT		SyncRegPt,			R1
	BCLR		R1,					LEDSyncBit
	OUT		SyncRegPt,			R1
	#EndSection
	JMP		RdFrm, 				LKY, DLY
	DEC		R5															// R5 -> NumReads = NumReads-1	(Remaining)
	
	//*****************************************************************
	//ALL READS IN THE GROUP ARE DONE NOW. CHECK DROPS
	ChkDrp:
	#BeginSection Drop_Frm_Setup
	// setup for Drop frame
	MOVX		R6,							RX1						// Store R6=NumGrps left in RX1 
	CMP		R6,							1							// Check if this is the last group (i.e. not R6=0)
	JPNZ		ConfigureDrps,				LKY						// Don't do drops after last read!!!!

   //DONE with ramp, check to see if there are any more exposures to perform before going back to IDLE
	SWLD		RB,							h04						// # 40009 = Number of Ramps left to perform before idle
	CMP		R2,							h00
	JPNZ		UpTheRampExposure,		LKY			

   //If it's the last ramp, write the exposure counter registers
	IN			R7,							Exp1CounterPt
	OUT		FFCounter3Pt,				R7
	IN			R7,							Exp2CounterPt
	OUT		FFCounter4Pt,				R7
	JMP		StrtIdle
	
	ConfigureDrps:
	BTST		R3,					ClkSCAInDropsBit				// Check to see if we should clock the SCA in the drops
	JPC		DrpOut,				LKY								// If not, then stop all of the clocks from toggling 
	
	NoClocksDuringDrops:	
	SWSWP		RA,					hf8								// Put R0,R1,R2,R3 away
	CALL		StopClocks
	IN			R7,					FirstRdClkInFrmPt				//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	   	R6,					SecondRdClkInFrmPt			//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN			R5,					ThirdStrobeRegR5Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	IN			R4,					SecondStrobeRegR4Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	BCLR		R4,					IFBlockStrobeBit
	IN			R3,					FirstStrobeRegR3Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	SWSWP		RA,					hf8								// Put away R3-R7
	JMP		DrpFrm,				LKY
	
	DrpOut:
	SWSWP		RA,					hd0								// Swap R6,R7 with Shadow Bank A (take exp registers out)
	IN			R7,					FirstRdClkInFrmPt				// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	  		R6,					SecondRdClkInFrmPt			// Determined in Configuration - HxRG_EnableDisableClocks.asm
	BCLR		R4,					IFBlockStrobeBit
	SWSWP		RA,					hd0								// Swap R6,R7 with Shadow Bank A (put exp registers back)

	#EndSection
	
	DrpFrm:
	SWLD		RB,					1									// Load frameblanking into R0	
	CMP		R4,					0									// R4=Number of drops left
	JPZ		Rry4Grp													// Move to group when all drop frames are done
	#BeginSection Continue_useR1
	IN			R1,					SyncRegPt
	BTST		R1,					ExpSyncBit
	JPNC		StrtIdle													// Halt if prompted
	#EndSection

	DrpBlnk:
	WAITB		T7,					T									// Wait 1 blank line
	#BeginSection Drop_Blanking
	CMP		R0,					0
	JPNZ		DrpBlnk,				DLY
	DEC		R0
	#EndSection
	SWSWP		RA,					hff
	IN			R0,					FrmProcedurePt					// Load the frame call address set by configuration
	CALL		R0,					LKY
	IN			R7,					FirstRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	  		R6,					SecondRdClkInFrmPt		// Determined in Configuration - HxRG_EnableDisableClocks.asm
	SWSWP		RA,					hff
	#BeginSection Drop_LED
	IN			R1,					SyncRegPt
	OR			R1,					R2
	OUT		SyncRegPt,			R1
	BCLR		R1,					8
	OUT		SyncRegPt,			R1
	#EndSection
	JMP		ChkDrp, 				LKY, DLY
	DEC		R4															// R4 -> NumDrops = NumDrops-1	(Remaining)
	
	Rry4Grp:
	#BeginSection Drop_CleanUp
	BTST		R3,					ClkSCAInDropsBit				// Check to see if the clocks were stopped
	JPC		MoveToNextGrp,		LKY								// If so, start them up
	
	StartClocksAfterDrops:
	SWSWP		RA,					hf8								// Put away R3-R7
	CALL		StartClocks
	IN			R7,					FirstRdClkInFrmPt				//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN	   	R6,					SecondRdClkInFrmPt			//Determined in Configuration - HxRG_EnableDisableClocks.asm
	IN			R5,					ThirdStrobeRegR5Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	IN			R4,					SecondStrobeRegR4Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	IN			R3,					FirstStrobeRegR3Pt			//Determined in Configuration - HxRG_PreAmpConfig.asm
	SWSWP		RA,					hf8								// Put away R3-R7
	MoveToNextGrp:
	SWSWP		RA,					h10								// Bring In R2,R3, and R4 
	BSET		R4,					IFBlockStrobeBit				// Set the IFStrobe Bit
	SWSWP		RA,					h10
	DEC		R6															// R6-> NumGroups=NumGroups-1		(Remaining)
	JMP		GrpRmp,				LKY,	DLY
	SWLD		RB,					h30								// R5=NumRdFrm, R4=NumDrpFrm
	
	#EndSection
	
#EndSection

END MAIN

#EndSection

//These subroutines contain procedures
.INCLUDE HxRG_DataTransferConfig.asm
.INCLUDE HxRG_PreAmpConfig.asm
.INCLUDE HxRG_ChannelConfig.asm
.INCLUDE HxRG_ClocksConfig.asm
.INCLUDE	HxRG_DetConfig.asm
.INCLUDE HxRG_FrameCall.asm

// <TIMESTAMP>
.LDREG	h5FFC	h11		 // Version Number
.LDREG	h5FFD	h0625		 // Date
.LDREG	h5FFE	h2009		 // Year
.LDREG	h5FFF	h1624		 // Time
// </TIMESTAMP>


// disable Hold_uC register to start microcontroller and set DivDly to 1
.LDREG h6903 h0000