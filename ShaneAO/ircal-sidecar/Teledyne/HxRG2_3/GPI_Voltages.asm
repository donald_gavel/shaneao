// Set custom voltage for GPI
// Vreset = 0.3 V
// Dsub = 0.6 V
// VrefMain = 1.0 V

.LDREG h6000 h804a    // Vreset (default: h804a)
.LDREG h6002 h80cd    // Dsub (default: h80cd)
.LDREG h602c h8200    // VrefMain PreAmp reference input (default: h8334)
