CSharpFITS has been modified in a few ways.  The first is to change the default behavior of FITS header writing, which would reorder the keywords.  We desire to leave the keywords in the order in which they were written.

src/nom/tam/fits/Header.cs
src/nom/tam/fits/HeaderCard.cs

The second change was to enable the writing of unsigned 16-bit integer data to FITS files, used to write raw detector images to disk.  To accomplish this, the following files were modified:

src/nom/tam/fits/ImageData.cs
src/nom/tam/util/BufferedDataStream.cs
src/nom/tam/util/ArrayDataIO.cs
src/nom/tam/util/HashedList.cs

a third change was to allow arbitrarily long string keyword values by using the CONTINUE keyword convention.  changes were made to Header.cs and HeaderCard.cs.
 
a fourth change was to allow multiple instances of the COMMENT and HISTORY keywords.  Changes were made to Header.cs, HeaderCard.cs, and HashedList.cs.


The zip file in this directory contains the original source distribution for CSharpFITS v1.1.

To recreate the necessary DLL, replace the files listed above with the cs files in this dir, 
and rebuild.

The DLL created this way is also in svn in the SIDECAR Server project directory.
The only reason really to do what is described in this is if you want to make more changes
to CSharpFITS.

